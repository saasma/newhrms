﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using DAL;

namespace BLL.BO
{
   
    public class GlobusCivilMainVoucher
    {
        public string COMPANY { get; set; }
        public string DEBITACCTNO { get; set; }
        public string DEBITCURRENCY { get; set; }
        public string DEBITAMOUNT { get; set; }
        public string DEBITTHEIRREF { get; set; }
        public string ORDERINGCUST { get; set; }
        public string PAYMENTDETAILS { get; set; }
        public string CREDITACCTNO { get; set; }
        public string CREDITCURRENCY { get; set; }
        public string CREDITTHEIRREF { get; set; }
        public string PROFITCENTREDEPT { get; set; }
    }
    public class CountryName
    {
        public string Country { get; set; }
    }
    public class EmployeeRet
    {
        public int EIN { get; set; }
        public DateTime RetirementDate { get; set; }
    }
    public class TargetOtherDetail
    {
        public int EmployeeId { get; set; }
        public int FormId { get; set; }
        public int TargetId { get; set; }

        public string Name { get; set; }
        public string FormName { get; set; }
        public string TargetName { get; set; }

        public double Weight { get; set; }
        public string TargetTextValue { get; set; }
        public string AchievementTextValue { get; set; }
        public string AssignedTargetTextValue { get; set; }
    }
    public class AnnualTaxItem
    {
        public int Type { get; set; }
        public int SourceId { get; set; }
        public int PayrollPeriodId { get; set; }
        public decimal Amount { get; set; }
    }

    public class EmployeeCurrentJobBo
    {
        public string EffectiveDate { get; set; }
        public DateTime EffectiveDateEng { get; set; }
        public string Level { get; set; }
        public string Position { get; set; }
        public string FunctionalTitle { get; set; }
    }
    public class AnnualTaxBO
    {
        public int RowNumber { get; set; }
        public int Type { get; set; }
        public int SourceId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public decimal Total { get; set; }
        public decimal OneTime { get; set; }
        public decimal M4Amount { get; set; }
        public decimal M4AddOn { get; set; }
        public decimal M5Amount { get; set; }
        public decimal M5AddOn { get; set; }
        public decimal M6Amount { get; set; }
        public decimal M6AddOn { get; set; }
        public decimal M7Amount { get; set; }
        public decimal M7AddOn { get; set; }
        public decimal M8Amount { get; set; }
        public decimal M8AddOn { get; set; }
        public decimal M9Amount { get; set; }
        public decimal M9AddOn { get; set; }

        public decimal M10Amount { get; set; }
        public decimal M10AddOn { get; set; }
        public decimal M11Amount { get; set; }
        public decimal M11AddOn { get; set; }
        public decimal M12Amount { get; set; }
        public decimal M12AddOn { get; set; }
        public decimal M1Amount { get; set; }
        public decimal M1AddOn { get; set; }
        public decimal M2Amount { get; set; }
        public decimal M2AddOn { get; set; }
        public decimal M3Amount { get; set; }
        public decimal M3AddOn { get; set; }

    }

    public class FlexCubeVoucherBO
    {
        public string SOURCE_CODE { get; set; } // statis = FLEXCUBE
        public string BRANCH_CODE { get; set; } // static = 999
        public int CURR_NO { get; set; }// Serial no
        public string UPLOAD_STAT { get; set; } // status = U
        public string CCY_CD { get; set; } // NPR
        public DateTime INITIATION_DATE { get; set; } // export date = 8-Jan-16

        public decimal? Amount { get; set; }
        public string ACCOUNT { get; set; } // MainCode
        public string ACCOUNT_BRANCH { get; set; } // BranchCode
        public string TXN_CODE { get; set; } // static = MSC
        public string DR_CR { get; set; } // TranCode
        public decimal? LCY_EQUIVALENT { get; set; }
        public string EXCH_RATE { get; set; }
        public DateTime VALUE_DATE { get; set; } // export date
        public string EXTERNAL_REF_NO { get; set; } // blank
        public string BATCH_NO { get; set; } // blank
        public string RESERVED_FUNDS_REF { get; set; } // blank
        public string DELETE_STAT { get; set; } // blank
        public string UPLOAD_DATE { get; set; } // blank
        public string TXT_FILE_NAME { get; set; } // blank
        public string INSTRUMENT_NO { get; set; } // blank
        public string FIN_CYCLE { get; set; } // blank
        public string PERIOD_CODE { get; set; }
        public string MIS_CODE { get; set; }
        public string REL_CUST { get; set; }
        public string ADDL_TEXT { get; set; } // TranType

    }

    public class NCCVoucherBO
    {
        public string MainCode { get; set; }
        public string TranCode { get; set; }
        public string Desc1 { get; set; }
        public string Desc2 { get; set; }
        public string Desc3 { get; set; }
        public decimal AMOUNT { get; set; }
        public decimal LCYamount { get; set; }
        public string BranchCode { get; set; }
        public string BranchEmployeeName { get; set; }
    }
    public class AllowanceBO
    {
        public string AllowanceName { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string StartDateNep { get; set; }
        public string EndDateNep { get; set; }
        public string Units { get; set; }
        public string Status { get; set; }
        public int StatusInt { get; set; }

        public DateTime start { get; set; }
        public DateTime end { get; set; }
        public int? PeriodId { get; set; }
    }
    public class LeaveFareBO
    {
        public int LeaveRequestId { get; set; }
        public int EmployeeId { get; set; }
        public int DepartmentId { get; set; }
        public int ServiceStatusId { get; set; }
        public int DesignationId { get; set; }
        public int BranchId { get; set; }
        public double TakenDays { get; set; }
        public string IsFull { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public decimal? Amount { get; set; }
        public decimal? FullAmount { get; set; }
        public int PayrollPeriodId { get; set; }
    }

    public class TargetAchievmentBO
    {
        public int EIN { get; set; }
        public int TargetID { get; set; }
        public string Name { get; set; }
        public string TargetName { get; set; }

        public double Target1 { get; set; }
        public double Achievment1 { get; set; }

        public double Target2 { get; set; }
        public double Achievment2 { get; set; }

        public double Target3 { get; set; }
        public double Achievment3 { get; set; }
        public double Target4 { get; set; }
        public double Achievment4 { get; set; }
        public double Target5 { get; set; }
        public double Achievment5 { get; set; }
        public double Target6 { get; set; }
        public double Achievment6 { get; set; }
        public double Target7 { get; set; }
        public double Achievment7 { get; set; }
        public double Target8 { get; set; }
        public double Achievment8 { get; set; }
        public double Target9 { get; set; }
        public double Achievment9 { get; set; }
        public double Target10 { get; set; }
        public double Achievment10 { get; set; }
        public double Target11 { get; set; }
        public double Achievment11 { get; set; }
        public double Target12 { get; set; }
        public double Achievment12 { get; set; }

        public int TotalRows { get; set; }
    }
    public class AppraisalRatingBO
    {
        public double Score { get; set; }
        public string Label { get; set; }
        public string LabelScore
        {
            set { }
            get
            {
                if (Score == -1)
                    return "";
                return this.Label + " (" + this.Score.ToString() + ")";
            }
        }
    }
    public class YearBO
    {
        public int FinancialDateId { get; set; }
        public string Name { get; set; }
        public int Year { get; set; }

        
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public bool IsMiddlePeriod { get; set; } // for company like oxfam when leave start month is April
    }
    public class HistoryExport
    {
        public string CreatedDate { get; set; }
        public string Type { get; set; }
        public string Amount { get; set; }
        public string AdjustedInterest { get; set; }
        public string Option { get; set; }
        public string Date { get; set; }
        public string NepaliDate { get; set; }
    }

    public class EmployeeLevelGradeBO
    {
        public int EIN { get; set; }
        public string INo { get; set; }
        public int LevelId { get; set; }
        public double Grade { get; set; }
        public int DesignationId { get; set; }
        public string Name { get; set; }
        public string CurrentLevel { get; set; }
        public string CurrentGrade { get; set; }
        public string CurrentDesignation { get; set; }

        public string NewLevel { get; set; }
        public string NewGrade { get; set; }
        public string NewDesignation { get; set; }

    }
    public class EDesignationBO
    {
        public int DesignationId { get; set; }
        public string Name { get; set; }
        public string LevelAndDesignation { get; set; }
        public int LevelId { get; set; }
    }
    public class BonusIneligibleBO
    {
        public int EIN { get; set; }
        public int BonusId { get; set; }
        public string Name { get; set; }
        public string Reason { get; set; }
    }
    public class BonusEmployeeBO
    {
        public int SN { get; set; }
        public int BonusId { get; set; }
        public int EmployeeId { get; set; }
        public string AccountNo { get; set; }
        public string Name { get; set; }
        public int? Status { get; set; }
        public string StatusName { get; set; }
        public DateTime JoinDate { get; set; }
        public string RetirementDate { get; set; }
        public double? StdWorkDays { get; set; }
        public double? UnpaidLeave { get; set; }
        public double? OtherUneligibleDays { get; set; }
        public string OtherUneligibleDaysReason { get; set; }
        public double? ActualWorkdays { get; set; }
        public int? Eligibility { get; set; }
        public string EligibilityName { get; set; }
        public decimal? Proportion { get; set; }
        public string CalculationType { get; set; }
        //public DateTime? StartDate { get; set; }
        //public DateTime? EndDate { get; set; }

        public decimal? AnnualSalary { get; set; }
        public decimal? MonthlySalary { get; set; }
        public string Cat { get; set; }
        public decimal? Bonus { get; set; }

        // public decimal SWFMonthlyBased { get; set; }
        public decimal? MaxBonus { get; set; }

        //public decimal AdvanceBonusMonthlySalaryBased { get; set; }
        public decimal AdvanceBonus { get; set; }

        public decimal? BonusPaid { get; set; }
        public decimal? FinaBonus30Percent { get; set; }
        public decimal? FinalBonus70Percent { get; set; }
        public decimal? SST { get; set; }
        public decimal? TDS { get; set; }
        public decimal? NetPaid { get; set; }

        public int NIBLFinalBonusGroupType { get; set; }


    }
    public class GratuityBO
    {
        public int GratuityRuleId { get; set; }
        public int? FromServiceYear { get; set; }
        public int? ToServiceYear { get; set; }
        public int? GratuityClassRef_ID { get; set; }
        public string ClassName { get; set; }
        public decimal? MonthsSalary { get; set; }
    }
    public class EmployeeGratuityClassBO
    {
        public int EmployeeId { get; set; }
        public string Name { get; set; }
        public int GratuityClass { get; set; }
        public string GratuityClassName { get; set; }
    }
    public class DateEngToNep
    {
        public int NepStartDay { get; set; }
        public int NepMaxDays { get; set; }
        public string CalendarTitle { get; set; }
    }

    public class siteMapNode
    {

        public string title { get; set; }

        public string url { get; set; }

        public bool IsModule { get; set; }
        public bool IsGroup { get; set; }
        public bool IsPage { get; set; }


        public List<siteMapNode> list = new List<siteMapNode>();
    }

    public class AppraisalValue
    {
        public string SelfComment { get; set; }
        public double? SelfRating { get; set; }
        public string SupervisorComment { get; set; }
        public double? SupervisorRating { get; set; }
        public int ComptencyId { get; set; }
        public int QuestionnaireId { get; set; }
        public int ReviewId { get; set; }
        public int TargetID { get; set; }

        public double? TargetAchievement { get; set; }
        public double? SelfActivityMarks { get; set; }
        public double? SupervisorActivityMarks { get; set; }

        public double Weight { get; set; }
        //public string ActivitySupervisorComment { get; set; }
    }
    public class AppraisalDynamicFormCompetency
    {
        public int CompetencyID { get; set; }
        public int CategoryID { get; set; }
        public string Name { get; set; }
        public int Order { get; set; }
        public string Description { get; set; }
    }
    public class AppraisalDynamicFormTarget
    {
        public int TargetD { get; set; }
        public string Name { get; set; }
        public int Order { get; set; }
        public double TargetValue { get; set; }
        public double Weight { get; set; }
    }
    public class AppraisalDynamicFormReview
    {
        public int ReviewID { get; set; }
        public string Name { get; set; }
        public int Order { get; set; }
    }
    public class AppraisalDynamicFormQuestionnaire
    {
        public int QuestionnareID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int Order { get; set; }
        public int CategoryID { get; set; }

        public string Number { get; set; }
    }
    public class PayrollPeriodBO
    {
        public int PayrollPeriodId { get; set; }
        public string Name { get; set; }
        public bool IsPartialTaxPaidPeriod { get; set; }
        public int Month { get; set; }
        public DateTime EndDateEng { get; set; }
    }
    public class LeaveRequestBO
    {
        public int LeaveRequestId { get; set; }
        public int LeaveTypeId { get; set; }
        public string LeaveName { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public double DaysOrHours { get; set; }

        public DateTime CreatedOn { get; set; }

        private string _status = string.Empty;
        public string Status
        {
            get
            {
                return _status;
            }
            set
            {
                _status = ((LeaveRequestStatusEnum)int.Parse(value)).ToString();
            }
        }
        public string Reason { get; set; }

        public string ApprovedBy { get; set; }
        public string Comment { get; set; }
        public string Range { get; set; }
        public string CreatedOnFormatted { get; set; }
    }

    public class ProjectReallocationEntity
    {
        public int IncomeId { get; set; }
        public string IncomeText { get; set; }

        public int FromProjectId { get; set; }
        public string FromProjectText { get; set; }

        public int ToProjectId { get; set; }
        public string ToProjectText { get; set; }
    }

    public class ProjectPayBO
    {
        public int EmployeeId { get; set; }
        public int PayrollPeriodId { get; set; }
        public int? ProjectId { get; set; }
        public string ProjectName { get; set; }
        public double? HoursOrDays { get; set; }

    }

    public class ProjectRateInputBO
    {
        public int EmployeeId { get; set; }
        public int PayrollPeriodId { get; set; }
        public int? ProjectId { get; set; }
        public string ProjectName { get; set; }
        public double? HoursOrDays { get; set; }

    }


    public class CurrencyHistory
    {
        public string Month { get; set; }
        public double? FixedRate { get; set; }
        public double? CurrentRate { get; set; }
    }

    public class EmployeeSkillSetBO
    {
        public string SkillSetName { get; set; }
        public int EmployeeId { get; set; }
        public int SkillSetId { get; set; }
    }



    public class EmployeeServiceBo
    {
        public int EmployeeId { get; set; }
        public string IdCardNo { get; set; }
        public string Name { get; set; }
        public int? DepartmentID { get; set; }
        public string Department { get; set; }
        public string Branch { get; set; }
        public string Designation { get; set; }
        public string Level { get; set; }
        public DateTime JoinDateEng { get; set; }
        public string JoinDateEngText { get; set; }
        public string JoinDateNepali { get; set; }
        public int? BranchID { get; set; }
        public int? DesignationID { get; set; }
        public string ServicePeriod { get; set; }
        public string RetirementType { get; set; }


        public DateTime? RetiringDateEng { get; set; }

        public string Status { get; set; }
    }




    public class PromotionBO
    {
        public int DetailId { get; set; }
        public int EmployeeId { get; set; }
        public string EmployeeName { get; set; }
        public string Branch { get; set; }
        public string Department { get; set; }
        public string PrevLevelId { get; set; }
        public string PrevGrade { get; set; }
        public string PrevDesignationId { get; set; }
        public string LevelId { get; set; }
        public string StepGrade { get; set; }
        public string DesignationId { get; set; }
        public string FromDate { get; set; }
        public string StatusText { get; set; }
    }


    public class DynamicReportFieldsBO
    {
        public string FieldName { get; set; }
        public string DataType { get; set; }
        public string ServerMappingFields { get; set; }
        public string Caption { get; set; }

        public string GroupName { get; set; }
        public int Type { get; set; }
        public int SourceId { get; set; }
    }

    public class EmployeeExtraFieldsValuesBO
    {
        public int ID { get; set; }
        public int TypeID { get; set; }
        public int SourceID { get; set; }
        public string Value { get; set; }
    }
    public class EmpPayBO
    {
        public string PanNo { get; set; }
        public string CITNo { get; set; }
        public string CITCode { get; set; }
        public string PFNo { get; set; }
        public string AccountNo { get; set; }
        public string PFStartDate { get; set; }

    }
    public class AbsentEmpIDBO
    {
        public int EmpID { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
    }
    [Serializable]
    public class BirthDayBo
    {
        public int EmployeeId { get; set; }
        public string Name { get; set; }
        public int? Day { get; set; }
        public string Month { get; set; }
        public int? MonthIndex { get; set; }
        public string Email { get; set; }
    }
    public class SupervisorEmails
    {
        public string SupervisorEmail { get; set; }
    }

    public class DealerDocumentBO
    {
        public string DocumentID { get; set; }
        public string name { get; set; }
        public string Period { get; set; }
        public string DateAdded { get; set; }
        public string size { get; set; }
        public string status { get; set; }
        public string progress { get; set; }
        public string SavedFileName { get; set; }
        public string FileFormat { get; set; }
        public string FileType { get; set; }
    }

    public class DealerRenewBO
    {
        public string AgreementId { get; set; }
        public string StartDate { get; set; }
        public string AgrementPeriod { get; set; }
        public string EndDate { get; set; }
    }

}
