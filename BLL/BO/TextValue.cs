﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL.BO
{
    public class TextValue
    {
        public string Text { get; set; }
        public string Value { get; set; }
        public string Other { get; set; }
        public DateTime? Date { get; set; }
        public DateTime? OtherDate { get; set; }
        public double? DaysOrHours { get; set; }
        public int ID { get; set; }
        public int Count { get; set; }

        public int? Value2 { get; set; }
        public int EIN { get; set; }
    }
}
