﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL.BO
{
    public class CompanySetting
    {
        public bool EnableSalaryProcessDay { get; set; }
        public bool AppendTaxDetailsInPayslip { get; set; }
        public bool HasHalfHourlyLeave { get; set; }public bool IsD2 { get; set; }
        public WhichCompany WhichCompany { get; set; }
        public  bool IsEncashLeaveTypeBeNegativeBalance { get; set; }
        public bool IsEncashLapseLeaveTypeBeNegativeBalance { get; set; }
        public bool IsLapseLeaveBeNegativeBalance { get; set; }
        public bool LeaveRequestCountHolidayInBetweenLeave { get; set; }
        public bool IsProjectInputType { get; set; }
        public bool IsStatusDownGradeEnable { get; set; }

        public bool LeaveRequestNegativeLeaveReqBalanceNotAllowed { get; set; }
        public int LeaveApprovalMinimumPastDays { get; set; }
        public bool LeaveHasRequestOrder { get; set; }


        public int LeaveRequestMinimumPastDays { get; set; }
        public bool DisableChangeLog { get; set; }

        public bool RemoveStyleWhileExportingReport { get; set; }

        public int LeaveRequestValidMonths { get; set; }

        public bool UseOptimumPF { get; set; }

        public bool IsNset { get; set; }

        public bool LeaveRequestToOnlyOneSupervisor { get; set; }

        /// <summary>
        /// To process & display the opposite date in HR reports
        /// </summary>
        public bool IsHRInOppositeDate { get; set; }

        /// <summary>
        /// Hide branch if only one exist for the Company for easiness
        /// </summary>
        public bool HideBranchIfOnlyOne { get; set; }

        public bool IsAllIncomeInForeignCurrency { get; set; }

        public bool AttendanceInOut { get; set; }

        public bool EnablePayrollSignFeature { get; set; }

        public bool HasLevelGradeSalary { get; set; }

        public bool RemoteAreaUsingProportionateDays { get; set; }

        public bool PayrollAllowNegativeSalarySave { get; set; }

        public bool AttendanceAutoAbsentMarkingForSalary { get; set; }

        /// <summary>
        /// Show leave import in attendance register for the client like Oppo or Frox
        /// </summary>
        public bool AttendanceShowLeaveImportInRegistry { get; set; }

        public decimal MonthlySalaryBasedOnFixedDay { get; set; }

        public bool MonthlySalaryBasedOnWeeklyHolidayDeduction { get; set; }

        public bool DoNotUseSSTRecoveryFirst { get; set; }


        public bool LeaveDeductPrevMonthUnpaidChange { get; set; }

       // public bool DateFormat_YYYY_MM_DD { get; set; }

        /*
        AddLapseLeaveValueToEncashInRetirement	true	bit	for NSET,if the lapse value to be added in encash at the time of retirement for LapseEncash leave type	3	Add Lapse To Encash On Retirement
        GratuityServiceYearPrecision	4	int	Gratutiy Service Year Rounding	NULL	NULL
        HasHalfHourlyLeave	true	bit	if company has half hourly leave or not	1	Has Hourly Leave
        HasProjectReport	true	bit	if project needed or not	4	NULL
        IsEncashLapseLeaveTypeBeNegativeBalance	true	bit	for NSET	NULL	NULL
        IsEncashLeaveTypeBeNegativeBalance	true	bit	for NSET,if company Encash leave will be negative balance & that will come in deduction in retirement	2	Encash Leave Type Negative Balance
        IsNset	true	bit	for nest deducting sat & sun in Gratuity calc	NULL	NULL
        MonthlySalaryBasedOnWeeklyHolidayDeduction	true	bit	for NSET,PSI MonthlySalaryBasedOn Month Days Or Weekly holiday Deduction	NULL	NULL
        NsetFirstYearCITCalculation	true	bit	NULL	NULL	NULL
        Use174AsFixedMonthlyLeaveHours	false	bit	for HPL, for other it will be dynamically calculated on the basic of monthly workday	NULL	NULL
        CountHolidayInBetweenLeave	false	bit	NULL	NULL	NULL
         */
    }
}
