﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL.BO
{
    public class LeaveApprovalEmployeeBO
    {
        public int? EmployeeId { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
    }
}
