﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL.BO
{
    public class PositionHistoryBO
    {
        public int? GradeId { get; set; }
        public int? StepId { get; set; }
        public int? PositionId { get; set; }
        public string Name { get; set; }
        public string PositionName { get; set; }
        public string GradeName { get; set; }
        public string StepName { get; set; }

        public string EffectiveFromDate { get; set; }
        public DateTime EffectiveFromDateEng { get; set; }
    }
}
