﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL;
using BLL.Manager;
using System.Drawing;

namespace BLL.BO
{

    public class TimesheetGridBO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string DonorCode { get; set; }

        public bool IsHoliday { get; set; }

        public int Type { get; set; } // Project = 1, Leave = 2 Could be project or leave hours


        public double? P1_26Total { get; set; }
        public double? P27 { get; set; }
        public double? P28 { get; set; }
        public double? P29 { get; set; }
        public double? P30 { get; set; }
        public double? P31 { get; set; }
        
        public double? PreviousTotal { get; set; }
        
        public double? D1 { get; set; }
        public double? D2 { get; set; }
        public double? D3 { get; set; }
        public double? D4 { get; set; }
        public double? D5 { get; set; }
        public double? D6 { get; set; }
        public double? D7 { get; set; }
        public double? D8 { get; set; }
        public double? D9 { get; set; }
        public double? D10 { get; set; }

        public double? D11 { get; set; }
        public double? D12 { get; set; }
        public double? D13 { get; set; }
        public double? D14 { get; set; }
        public double? D15 { get; set; }
        public double? D16 { get; set; }
        public double? D17 { get; set; }
        public double? D18 { get; set; }
        public double? D19 { get; set; }
        public double? D20 { get; set; }

        public double? D21 { get; set; }
        public double? D22 { get; set; }
        public double? D23 { get; set; }
        public double? D24 { get; set; }
        public double? D25 { get; set; }
        public double? D26 { get; set; }
        public double? D27 { get; set; }
        public double? D28 { get; set; }
        public double? D29 { get; set; }
        public double? D30 { get; set; }
        public double? D31 { get; set; }

        public bool P27IsHoliday { get; set; }
        public bool P28IsHoliday { get; set; }
        public bool P29IsHoliday { get; set; }
        public bool P30IsHoliday { get; set; }
        public bool P31IsHoliday { get; set; }

        public bool D1IsHoliday { get; set; }
        public bool D2IsHoliday { get; set; }
        public bool D3IsHoliday { get; set; }
        public bool D4IsHoliday { get; set; }
        public bool D5IsHoliday { get; set; }
        public bool D6IsHoliday { get; set; }
        public bool D7IsHoliday { get; set; }
        public bool D8IsHoliday { get; set; }
        public bool D9IsHoliday { get; set; }
        public bool D10IsHoliday { get; set; }

        public bool D11IsHoliday { get; set; }
        public bool D12IsHoliday { get; set; }
        public bool D13IsHoliday { get; set; }
        public bool D14IsHoliday { get; set; }
        public bool D15IsHoliday { get; set; }
        public bool D16IsHoliday { get; set; }
        public bool D17IsHoliday { get; set; }
        public bool D18IsHoliday { get; set; }
        public bool D19IsHoliday { get; set; }
        public bool D20IsHoliday { get; set; }

        public bool D21IsHoliday { get; set; }
        public bool D22IsHoliday { get; set; }
        public bool D23IsHoliday { get; set; }
        public bool D24IsHoliday { get; set; }
        public bool D25IsHoliday { get; set; }
        public bool D26IsHoliday { get; set; }
        public bool D27IsHoliday { get; set; }
        public bool D28IsHoliday { get; set; }
        public bool D29IsHoliday { get; set; }
        public bool D30IsHoliday { get; set; }
        public bool D31IsHoliday { get; set; }


        public double? Total { get; set; }
        public int TotalDays { get; set; }

    }

    public class TimesheetGridNewBO
    {
        public int? Id { get; set; }
      
        public string Name { get; set; }
        public string Code { get; set; }
        public string DonorCode { get; set; }
        public string WeekStartDate { get; set; }
        public long Activity { get; set; }
        public string Category { get; set; }
        public double ProjectPercent { get; set; }

        public bool IsHoliday { get; set; }

        public int Type { get; set; } // Project = 1, Leave = 2 Could be project or leave hours


        

        public double? D1 { get; set; }
        public double? D2 { get; set; }
        public double? D3 { get; set; }
        public double? D4 { get; set; }
        public double? D5 { get; set; }
        //public double? D6 { get; set; }
        //public double? D7 { get; set; }

        public string D1Comment { get; set; }
        public string D2Comment { get; set; }
        public string D3Comment { get; set; }
        public string D4Comment { get; set; }
        public string D5Comment { get; set; }
        //public string D6Comment { get; set; }
        //public string D7Comment { get; set; }

        public bool D1IsFixed { get; set; }
        public bool D2IsFixed { get; set; }
        public bool D3IsFixed { get; set; }
        public bool D4IsFixed { get; set; }
        public bool D5IsFixed { get; set; }
        //public bool D6IsFixed { get; set; }
        //public bool D7IsFixed { get; set; }

        public double? L1 { get; set; }
        public double? L2 { get; set; }
        public double? L3 { get; set; }
        public double? L4 { get; set; }
        public double? L5 { get; set; }
        //public double? L6 { get; set; }
        //public double? L7 { get; set; }
       
        public bool D1IsHoliday { get; set; }
        public bool D2IsHoliday { get; set; }
        public bool D3IsHoliday { get; set; }
        public bool D4IsHoliday { get; set; }
        public bool D5IsHoliday { get; set; }
        //public bool D6IsHoliday { get; set; }
        //public bool D7IsHoliday { get; set; }
        
        public double? Total { get; set; }
        public int TotalDays { get; set; }

    }

    public class TimesheetGridMonthWiseNewBO
    {
        public int? Id { get; set; }
        public int? SubProjectId { get; set; }
        public int? TaskId { get; set; }
        public string SubProjectName { get; set; }
        public string TaskName { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string DonorCode { get; set; }
        public string WeekStartDate { get; set; }

        public bool IsHoliday { get; set; }

        public int Type { get; set; } // Project = 1, Leave = 2 Could be project or leave hours




        public double? D1 { get; set; }
        public double? D2 { get; set; }
        public double? D3 { get; set; }
        public double? D4 { get; set; }
        public double? D5 { get; set; }
        public double? D6 { get; set; }
        public double? D7 { get; set; }
        public double? D8 { get; set; }
        public double? D9 { get; set; }
        public double? D10 { get; set; }
        public double? D11 { get; set; }
        public double? D12 { get; set; }
        public double? D13 { get; set; }
        public double? D14 { get; set; }
        public double? D15 { get; set; }
        public double? D16 { get; set; }
        public double? D17 { get; set; }
        public double? D18 { get; set; }
        public double? D19 { get; set; }
        public double? D20 { get; set; }
        public double? D21 { get; set; }
        public double? D22 { get; set; }
        public double? D23 { get; set; }
        public double? D24 { get; set; }
        public double? D25 { get; set; }
        public double? D26 { get; set; }
        public double? D27 { get; set; }
        public double? D28 { get; set; }
        public double? D29 { get; set; }
        public double? D30 { get; set; }
        public double? D31 { get; set; }
        public double? D32 { get; set; }

        //public string D1Comment { get; set; }
        //public string D2Comment { get; set; }
        //public string D3Comment { get; set; }
        //public string D4Comment { get; set; }
        //public string D5Comment { get; set; }
        //public string D6Comment { get; set; }
        //public string D7Comment { get; set; }

        //public bool D1IsFixed { get; set; }
        //public bool D2IsFixed { get; set; }
        //public bool D3IsFixed { get; set; }
        //public bool D4IsFixed { get; set; }
        //public bool D5IsFixed { get; set; }
        //public bool D6IsFixed { get; set; }
        //public bool D7IsFixed { get; set; }

        public double? L1 { get; set; }
        public double? L2 { get; set; }
        public double? L3 { get; set; }
        public double? L4 { get; set; }
        public double? L5 { get; set; }
        public double? L6 { get; set; }
        public double? L7 { get; set; }
        public double? L8 { get; set; }
        public double? L9 { get; set; }
        public double? L10 { get; set; }
        public double? L11 { get; set; }
        public double? L12 { get; set; }
        public double? L13 { get; set; }
        public double? L14 { get; set; }
        public double? L15 { get; set; }
        public double? L16 { get; set; }
        public double? L17 { get; set; }
        public double? L18 { get; set; }
        public double? L19 { get; set; }
        public double? L20 { get; set; }
        public double? L21 { get; set; }
        public double? L22 { get; set; }
        public double? L23 { get; set; }
        public double? L24 { get; set; }
        public double? L25 { get; set; }
        public double? L26 { get; set; }
        public double? L27 { get; set; }
        public double? L28 { get; set; }
        public double? L29 { get; set; }
        public double? L30 { get; set; }
        public double? L31 { get; set; }
        public double? L32 { get; set; }

        public bool D1IsHoliday { get; set; }
        public bool D2IsHoliday { get; set; }
        public bool D3IsHoliday { get; set; }
        public bool D4IsHoliday { get; set; }
        public bool D5IsHoliday { get; set; }
        public bool D6IsHoliday { get; set; }
        public bool D7IsHoliday { get; set; }
        public bool D8IsHoliday { get; set; }
        public bool D9IsHoliday { get; set; }
        public bool D10IsHoliday { get; set; }
        public bool D11IsHoliday { get; set; }
        public bool D12IsHoliday { get; set; }
        public bool D13IsHoliday { get; set; }
        public bool D14IsHoliday { get; set; }
        public bool D15IsHoliday { get; set; }
        public bool D16IsHoliday { get; set; }
        public bool D17IsHoliday { get; set; }
        public bool D18IsHoliday { get; set; }
        public bool D19IsHoliday { get; set; }
        public bool D20IsHoliday { get; set; }
        public bool D21IsHoliday { get; set; }
        public bool D22IsHoliday { get; set; }
        public bool D23IsHoliday { get; set; }
        public bool D24IsHoliday { get; set; }
        public bool D25IsHoliday { get; set; }
        public bool D26IsHoliday { get; set; }
        public bool D27IsHoliday { get; set; }
        public bool D28IsHoliday { get; set; }
        public bool D29IsHoliday { get; set; }
        public bool D30IsHoliday { get; set; }
        public bool D31IsHoliday { get; set; }
        public bool D32IsHoliday { get; set; }

        public double? Total { get; set; }
        public int TotalDays { get; set; }

    }
}