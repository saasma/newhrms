﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL.BO
{
    public class CalculationEmployee
    {
        public int CalculationEmployeeId { get; set; }
        public int EmployeeId { get; set; }
        public string Name { get; set; }
        public bool? HasSentInMail { get; set; }
        public string Email { get; set; }
    }
}
