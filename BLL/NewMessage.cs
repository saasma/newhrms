﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ext.Net;
using System.Web;

namespace BLL
{
    public static class NewMessage
    {
        public static void ShowNormalPopup(string message)
        {
            Ext.Net.Notification.Show(
                new NotificationConfig
                {
                    Icon = Icon.Information,

                    Html = message,
                    Title = "Information"
                }
                );

            //Ext.net.Notification.show({
            //        iconCls: 'icon-information',
            //        pinEvent: 'click',
            //        html: data,
            //        title: 'Updated'
            //    });
        }

        public static void ShowWarningPopup(string message)
        {
            Ext.Net.Notification.Show(
                new NotificationConfig
                {
                        
                    Icon = Icon.Error,

                    Html = message,
                    Title = "Warning"
                }
                );

            //Ext.net.Notification.show({
            //        iconCls: 'icon-information',
            //        pinEvent: 'click',
            //        html: data,
            //        title: 'Updated'
            //    });
        }

        public static void ShowNormalMessage(string message)
        {
            X.MessageBox.Show(
                      new MessageBoxConfig
                      {
                          Message = message,
                          Buttons = MessageBox.Button.OK,
                          Title = "Information",
                          Icon = MessageBox.Icon.INFO,
                          MinWidth = 300,
                      });
        }

        public static void ShowYesNoMessage(string message, string callback)
        {
            X.MessageBox.Show(
                     new MessageBoxConfig
                     {
                         Message = message,
                         Buttons = MessageBox.Button.YESNO,
                         Title = "Confirmation",
                         Icon = MessageBox.Icon.INFO,
                         MinWidth = 300,
                         Fn = new JFunction(callback)
                     });

            //new MessageBoxButtonConfig
            //{

            //    Message = message,
            //    Buttons = MessageBox.Button.OK,
            //    Title = "Information",
            //    Icon = MessageBox.Icon.INFO,
            //    MinWidth = 300,
            //});
        }

        public static void ShowWarningMessage(string message)
        {
            X.MessageBox.Show(
                      new MessageBoxConfig
                      {
                          Message = message,
                          Buttons = MessageBox.Button.OK,
                          Title = "Warning",
                          Icon = MessageBox.Icon.WARNING,
                          MinWidth = 300,
                      });
        }

        public static void ShowWarningMessage(string message, string callbackJavascriptCode)
        {
            X.MessageBox.Show(
                      new MessageBoxConfig
                      {
                          Message = message,
                          Buttons = MessageBox.Button.OK,
                          Title = "Warning",
                          Icon = MessageBox.Icon.WARNING,
                          MinWidth = 300,
                          Handler= callbackJavascriptCode
                      });
        }
        public static void ShowNormalMessage(string message, string callbackJavascriptFunction)
        {



            X.MessageBox.Show(
                      new MessageBoxConfig
                      {
                          Message = message,
                          Buttons = MessageBox.Button.OK,
                          Title = "Information",
                          Icon = MessageBox.Icon.INFO,
                          MinWidth = 300,
                          Fn = new JFunction(callbackJavascriptFunction)
                      });
        }

        public static String GetTextValue(string enumValue)
        {
            object optionName = HttpContext.GetGlobalResourceObject("ResourceEnums", enumValue);//.ToString();
            return optionName == null ? enumValue : optionName.ToString();
        }
    }
}
