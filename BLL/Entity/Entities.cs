﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL.Entity
{
    
    public class AttendanceImportEmployee
    {
        public int EmployeeId { get; set; }
        public List<AttendanceImportEmployeeLeave> leaves = new List<AttendanceImportEmployeeLeave>();
    }
    public class AttendanceImportEmployeeLeave
    {
        public string Name { get; set; }
        public bool NoLeave { get; set; } //in this case P will be placed
        public int EmployeeId { get; set; }
        public int LeaveTypeId { get; set; }
        public DateTime TakenDate { get; set; }
        public bool IsHalfDay { get; set; }
        public string LeaveAbbr { get; set; }
        public bool IsUnpaidLeave { get; set; }
        public bool IsAbsentLeave { get; set; }
        public bool IsValidImport { get; set; }
        public string ImportErrorMsg { get; set; }
        public int? Day { get; set; } //used for DH sunday leave to find out the day

        // if multiple leave case or first leave already approved, now second leave is being approved
        public bool HasMulitpleLeave { get; set; }
        public int OtherLeaveTypeId { get; set; }
    }
    public class LeaveTransfer
    {
        public int EmployeeId { get; set; }
        public string EmployeeName { get; set; }
        public decimal? NewBalance { get; set; }

        public string Value
        {
            get
            {
                return EmployeeId + ":" + (NewBalance==null ? "0" : NewBalance.Value.ToString("N2") );
            }
        }
        //string EmployeeIdBalance { get; set; }
    }


    public class PastSalaryBO
    {
        public int EmployeeID { get; set; }
        public string Name { get; set; }
        public decimal? CIT { get; set; }
        public decimal? SST { get; set; }
        public decimal? TDS { get; set; }
        public decimal? IncomePF { get; set; }
        public decimal? DeductionPF { get; set; }
    }

    public class ResponseStatus
    {
        public string IsSuccess { get; set; }
        public bool IsSuccessType { get; set; }

        private string _errorMesage = string.Empty;
        public string ErrorMessage
        {
            get { return _errorMesage; }
            set
            {
                _errorMesage = value;
                this.IsSuccessType = false;
            }
        }

        public ResponseStatus()
        {
            //BErrorMessage = "";
            this.IsSuccess = "true";
            this.IsSuccessType = true;
            
        }
    }
}
