﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using DAL;
using System.IO;
using System.Data.OleDb;
using System.Data;
using Utils;
using System.Web.UI.WebControls;
using BLL.BO;
using BLL.Manager;
using Utils.Calendar;

namespace BLL
{
    /// <summary>
    /// Summary description for ExcelGenerator
    /// </summary>
    public class ExcelGenerator : BaseBiz
    {

        public const string ExcelConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties='Excel 12.0 Xml;HDR=Yes'";

        #region "Employee"

        public static string[] employeesStaticColumns = {"Title", "First Name","Middle Name","Last Name", "Father's Name", "Sex", "Marital Status", "Handicapped"
                                     ,"Couple Tax Status","Has Dependent(s)","Date of Birth (Nepali)","Date of Birth (English)",

                                     "Designation","Branch","Department","Sub Department","Status"

                                     ,"Contract No","Employee ID Card No","Official Email","Phone","Ext",
                                     "PF No","PAN No","CIT No","CIT Code","CIT Effective From","CIT Amount/Rate",

                                     "Payment Mode","Bank Name","Bank Account No","Insurance Premium","Level","Grade" ,"Shift","Cost Code","Ethnicity","Location","GradeStep","Unit"

                                     };






        public static void WriteTADAClaimExportExcel(string excelTemplatePath, List<DHTADAClaimLine> claimLine)
        {

            string path = HttpContext.Current.Server.MapPath(excelTemplatePath);
            string copyPath = Path.GetDirectoryName(path) + "\\" + Guid.NewGuid() + ".xlsx";
            File.Copy(path, copyPath);

            string excelConnection = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + copyPath + ";Extended Properties='Excel 12.0 Xml;HDR=Yes'";

            try
            {
                using (OleDbConnection conn = new OleDbConnection(excelConnection))
                {
                    conn.Open();


                    StringBuilder sql = new StringBuilder(string.Format("Create Table [TADAClaim$]( {0} varchar(25),{1} varchar(200), {2} varchar(200),{3} varchar(200),{4} varchar(200),{5} money,{6} money,{7} money,{8} money,{9} money,{10} money,{11} money, {12} money, {13} money )",
                                         "[Date]", "[Eng Date]", "[From]", "[To]", "[Description]", "[Local Daily]", "[UpCountry Daily]", "[Fooding]", "[Transportation]", "[Feul]", "[Maintainence]", "[Communication]", "[Other Allowance]", "[Total]"));



                    OleDbCommand cmd1 = new OleDbCommand(sql.ToString(), conn);
                    using (cmd1)
                    {
                        cmd1.ExecuteNonQuery();
                    }



                    OleDbCommand cmd = new OleDbCommand(string.Format("INSERT INTO [TADAClaim$]({0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13}) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?)",
                         "[Date]", "[Eng Date]", "[From]", "[To]", "[Description]", "[Local Daily]", "[UpCountry Daily]", "[Fooding]", "[Transportation]", "[Feul]", "[Maintainence]", "[Communication]", "[Other Allowance]", "[Total]"), conn);


                    //OleDbCommand cmd = new OleDbCommand(@"INSERT INTO [TADAClaim$](Date, [Eng Date], From, To, Description, [Local Daily], [UpCountry Daily], Fooding, Transportation, Feul, Maintainence, Communication, [Other Allowance], Total) 
                    //            values (?,?,?,?,?,?,?,?,?,?,?,?,?,?)", conn);



                    cmd.Parameters.Add("Date", OleDbType.VarChar, 25);
                    cmd.Parameters.Add("[Eng Date]", OleDbType.VarChar, 200);
                    cmd.Parameters.Add("From", OleDbType.VarChar, 200);
                    cmd.Parameters.Add("To", OleDbType.VarChar, 200);
                    cmd.Parameters.Add("Description", OleDbType.VarChar, 200);


                    cmd.Parameters.Add("[Local Daily]", OleDbType.Currency, 4);
                    cmd.Parameters[5].IsNullable = true;
                    cmd.Parameters.Add("[UpCountry Daily]", OleDbType.Currency, 4);
                    cmd.Parameters[6].IsNullable = true;
                    cmd.Parameters.Add("Fooding", OleDbType.Currency, 4);
                    cmd.Parameters[7].IsNullable = true;
                    cmd.Parameters.Add("Transportation", OleDbType.Currency, 4);
                    cmd.Parameters[8].IsNullable = true;
                    cmd.Parameters.Add("Feul", OleDbType.Currency, 4);
                    cmd.Parameters[9].IsNullable = true;
                    cmd.Parameters.Add("Maintainence", OleDbType.Currency, 4);
                    cmd.Parameters[10].IsNullable = true;
                    cmd.Parameters.Add("Communication", OleDbType.Currency, 4);
                    cmd.Parameters[11].IsNullable = true;
                    cmd.Parameters.Add("[Other Allowance]", OleDbType.Currency, 4);
                    cmd.Parameters[12].IsNullable = true;
                    cmd.Parameters.Add("Total", OleDbType.Currency, 4);
                    cmd.Parameters[13].IsNullable = true;


                    using (cmd)
                    {
                        foreach (var row in claimLine)
                        {
                            cmd.Parameters[0].Value = row.Date.ToString();
                            cmd.Parameters[1].Value = row.LineDate.ToLongDateString();
                            cmd.Parameters[2].Value = row.LocationFrom.ToString();
                            cmd.Parameters[3].Value = row.LocationTo.ToString();
                            cmd.Parameters[4].Value = row.Description.ToString();

                            if (row.Allowance1 == null || row.Allowance1 == 0)
                                cmd.Parameters[5].Value = DBNull.Value;
                            else
                                cmd.Parameters[5].Value = row.Allowance1;
                            if (row.Allowance2 == null || row.Allowance2 == 0)
                                cmd.Parameters[6].Value = DBNull.Value;
                            else
                                cmd.Parameters[6].Value = row.Allowance2;

                            if (row.Allowance3 == null || row.Allowance3 == 0)
                                cmd.Parameters[7].Value = DBNull.Value;
                            else
                                cmd.Parameters[7].Value = row.Allowance3;
                            if (row.Allowance4 == null || row.Allowance4 == 0)
                                cmd.Parameters[8].Value = DBNull.Value;
                            else
                                cmd.Parameters[8].Value = row.Allowance4;
                            if (row.Allowance5 == null || row.Allowance5 == 0)
                                cmd.Parameters[9].Value = DBNull.Value;
                            else
                                cmd.Parameters[9].Value = row.Allowance5;
                            if (row.Allowance6 == null || row.Allowance6 == 0)
                                cmd.Parameters[10].Value = DBNull.Value;
                            else
                                cmd.Parameters[10].Value = row.Allowance6;
                            if (row.Allowance7 == null || row.Allowance7 == 0)
                                cmd.Parameters[11].Value = DBNull.Value;
                            else
                                cmd.Parameters[11].Value = row.Allowance7;
                            if (row.Allowance8 == null || row.Allowance8 == 0)
                                cmd.Parameters[12].Value = DBNull.Value;
                            else
                                cmd.Parameters[12].Value = row.Allowance8;

                            cmd.Parameters[13].Value = row.Total == null ? 0 : row.Total;

                            cmd.ExecuteNonQuery();


                        }

                    }




                }
                ExcelGenerator.WriteFile(copyPath, Path.GetFileName(excelTemplatePath));
            }
            finally
            {
                try
                {
                    File.Delete(copyPath);
                }
                catch
                {
                }
            }





        }




        public static void WriteEmployeeExportExcel(string excelTemplatePath, List<EDesignation> designations, string[] designationList, string[] branchList,
            string[] departmentList, string[] subdepartmentList, string[] incomeList, string[] deductionList, string[] leaveList, bool importIsEnglishDate, string importDateFormat
            , List<PIncome> incomes, List<PDeduction> deductions, List<LLeaveType> leaves, List<KeyValue> statusList, string[] levelList, string[] shiftList, string[] costCodeList,
            List<HCaste> ethinicityList, List<ELocation> locationList, List<EGrade> gradeStepList, string[] bankList, bool isBlankEmpList
            , string[] unitList, bool isAllEmployees)
        {

            List<EEmployee> employeeList = new List<EEmployee>();

            if (isBlankEmpList == false)
                employeeList = EmployeeManager.GetAllEmployeesForEmployeeImportExport(isAllEmployees);

            string path = HttpContext.Current.Server.MapPath(excelTemplatePath);
            string copyPath = Path.GetDirectoryName(path) + "\\" + Guid.NewGuid() + ".xlsx";
            File.Copy(path, copyPath);

            string excelConnection = string.Format(ExcelConnectionString, copyPath);

            List<UnitList> units = CommonManager.GetAllUnitList();

            OleDbCommand writeCmd = null;
            OleDbCommand cmd1 = null;
            try
            {
                OleDbConnection conn = null;
                /// Awlays remember to property dispose Command & Connection as for Larger emp like 500 if not dispose
                /// then error can come
                try
                {
                    conn = new OleDbConnection(excelConnection);
                    conn.Open();



                    StringBuilder sql = new StringBuilder("Create Table [Employee_Info$]( EIN int");
                    // bool isFirstField = false;
                    // Employee information columns
                    for (int i = 0; i < employeesStaticColumns.Length; i++)
                    {
                        sql.AppendFormat(", [{0}] varchar(200)", employeesStaticColumns[i]);
                    }
                    // Income columns
                    for (int i = 0; i < incomeList.Length; i++)
                    {
                        sql.AppendFormat(", [{0} Income]  varchar(100)", incomeList[i]);
                    }
                    // Deduction columns
                    for (int i = 0; i < deductionList.Length; i++)
                    {
                        sql.AppendFormat(", [{0} Deduction]  varchar(100)", deductionList[i]);
                    }
                    // Leaves columns
                    for (int i = 0; i < leaveList.Length; i++)
                    {
                        sql.AppendFormat(", [{0} Leave] varchar(100)", leaveList[i]);
                    }
                    sql.Append(" )");


                    cmd1 = new OleDbCommand(sql.ToString(), conn);
                    cmd1.ExecuteNonQuery();


                    WriteIntoSheet(conn, "LevelSheet", levelList);
                    WriteIntoSheet(conn, "ShiftSheet", shiftList);
                    WriteIntoSheet(conn, "CostCode", costCodeList);
                    WriteIntoSheet(conn, "EthnicityList", ethinicityList.Select(x => x.CasteName).ToArray());
                    WriteIntoSheet(conn, "LocationList", locationList.Select(x => x.Name).ToArray());
                    WriteIntoSheet(conn, "GradeStepList", gradeStepList.Select(x => x.Name).ToArray());
                    WriteIntoSheet(conn, "DesignationSheet", designationList);
                    WriteIntoSheet(conn, "BranchSheet", branchList);
                    WriteIntoSheet(conn, "DepartmentSheet", departmentList);
                    WriteIntoSheet(conn, "SubDepartmentSheet", subdepartmentList);
                    WriteIntoSheet(conn, "BankList", bankList);
                    WriteIntoSheet(conn, "UnitList", unitList);
                    WriteIntoSheet(conn, "StatusSheet", statusList.Select(x => x.Value).ToArray());


                    #region "Write Existing Employees"

                    writeCmd = GetEmployeeWriteCommand(conn, incomeList, deductionList, leaveList);

                    WriteEachEmpInExcel(conn, employeeList, importIsEnglishDate, importDateFormat, writeCmd, incomeList, deductionList, leaveList
                        , incomes, deductions, leaves, designations, shiftList, ethinicityList, locationList, gradeStepList, units);


                    #endregion

                }
                //catch (Exception ee)
                //{
                //    Log.log("Error while exporting employee", ee);
                //}
                finally
                {
                    if (conn != null)
                    {
                        if (cmd1 != null)
                            cmd1.Dispose();
                        if (writeCmd != null)
                            writeCmd.Dispose();

                        conn.Close();
                        conn.Dispose();
                    }
                }
                ExcelGenerator.WriteFile(copyPath, Path.GetFileName(excelTemplatePath));
                return;
            }
            //catch (Exception exp)
            //{

            //    Log.log("Error while exporting employee", exp);
            //}
            finally
            {
                try
                {
                    //File.Delete(copyPath);
                }
                catch
                {
                }
            }
        }

        public static void WriteInsuranceExcel(string excelTemplatePath, string[] GetProjectList, List<IIndividualInsurance> list)
        {
            string path = HttpContext.Current.Server.MapPath(excelTemplatePath);
            string copyPath = Path.GetDirectoryName(path) + "\\" + Guid.NewGuid() + ".xlsx";
            File.Copy(path, copyPath);


            string excelConnection = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + copyPath + ";Extended Properties='Excel 12.0 Xml;HDR=Yes'";

            try
            {

                using (OleDbConnection conn = new OleDbConnection(excelConnection))
                {
                    // Open the connection.
                    conn.Open();





                    // Create a new sheet in the Excel spreadsheet.
                    OleDbCommand cmd = new OleDbCommand(@"INSERT INTO [Sheet1$]([Insurance ID],EIN,Employee, [Insurance Company],[Paid By],[Policy Number],[Start Date],[End Date],[Policy Amount],Premium,[Note],[Health Insurance] ) 
                                values (?,?,?,?,?,?,?,?,?,?,?,?)", conn);


                    // Add the parameters.
                    cmd.Parameters.Add("[Insurance ID]", OleDbType.Integer, 4);
                    cmd.Parameters.Add("EIN", OleDbType.Integer, 4);
                    cmd.Parameters.Add("Employee", OleDbType.VarChar, 100);

                    cmd.Parameters.Add("[Insurance Company]", OleDbType.VarChar, 100);


                    cmd.Parameters.Add("[Paid By]", OleDbType.VarChar, 100);

                    cmd.Parameters.Add("[Policy Number]", OleDbType.VarChar, 100);
                    cmd.Parameters.Add("[Start Date]", OleDbType.VarChar, 100);
                    cmd.Parameters.Add("[End Date]", OleDbType.VarChar, 100);
                    cmd.Parameters.Add("[Policy Amount]", OleDbType.Currency, 4);
                    cmd.Parameters.Add("Premium", OleDbType.Currency, 4);
                    cmd.Parameters.Add("[Note]", OleDbType.VarChar, 500);
                    cmd.Parameters.Add("[Health Insurance]", OleDbType.VarChar, 500);
                    // Changes the Rowstate()of each DataRow to Added,
                    // so that OleDbDataAdapter will insert the rows.
                    foreach (IIndividualInsurance row in list)
                    {
                        cmd.Parameters[0].Value = row.Id;
                        cmd.Parameters[1].Value = row.EmployeeId;
                        cmd.Parameters[2].Value = row.Name;
                        cmd.Parameters[3].Value = row.InsuranceCompany == null ? "" : row.InsuranceCompany;


                        cmd.Parameters[4].Value = row.PaidBy == null ? "Employee" : row.PaidBy;

                        cmd.Parameters[5].Value = row.PolicyNo == null ? "" : row.PolicyNo;

                        cmd.Parameters[6].Value = row.StartDateEng == null ? "" : row.StartDateEng.Value.ToShortDateString();
                        cmd.Parameters[7].Value = row.EndDateEng == null ? "" : row.EndDateEng.Value.ToShortDateString();


                        cmd.Parameters[8].Value = row.PolicyAmount == null ? 0 : row.PolicyAmount;
                        cmd.Parameters[9].Value = row.Premium == null ? 0 : row.Premium;

                        cmd.Parameters[10].Value = row.Note == null ? "" : row.Note;


                        cmd.Parameters[11].Value = row.IsHealthInsurance != null && row.IsHealthInsurance.Value ? "Yes" : "";
                        //if (row.IsExpired != null && row.IsExpired.Value)
                        //    cmd.Parameters[11].Value = "Expire";
                        //else
                        //    cmd.Parameters[11].Value = "Un Expire";

                        cmd.ExecuteNonQuery();
                    }


                    WriteIntoSheet(conn, "InsuranceCompanySheet", GetProjectList);
                }

                ExcelGenerator.WriteFile(copyPath, Path.GetFileName(excelTemplatePath));
            }
            finally
            {
                try
                {
                    File.Delete(copyPath);
                }
                catch
                {
                }
            }
        }
        public static void WriteProjectEmpExcel(string excelTemplatePath, string[] GetProjectList, List<GetProjectAssociationResult> list)
        {
            string path = HttpContext.Current.Server.MapPath(excelTemplatePath);
            string copyPath = Path.GetDirectoryName(path) + "\\" + Guid.NewGuid() + ".xlsx";
            File.Copy(path, copyPath);


            string excelConnection = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + copyPath + ";Extended Properties='Excel 12.0 Xml;HDR=Yes'";

            try
            {

                using (OleDbConnection conn = new OleDbConnection(excelConnection))
                {
                    // Open the connection.
                    conn.Open();





                    // Create a new sheet in the Excel spreadsheet.
                    OleDbCommand cmd = new OleDbCommand(@"INSERT INTO [Sheet1$]([ID],EIN,Employee, [Project Name],[Activity ID],[Project Start Date],[Project End Date],[Contract ID]) 
                                values (?,?,?,?,?,?,?,?)", conn);


                    // Add the parameters.
                    cmd.Parameters.Add("[ID]", OleDbType.Integer, 4);
                    cmd.Parameters.Add("EIN", OleDbType.Integer, 4);
                    cmd.Parameters.Add("Employee", OleDbType.VarChar, 200);

                    cmd.Parameters.Add("[Project Name]", OleDbType.VarChar, 200);


                    cmd.Parameters.Add("[Activity By]", OleDbType.VarChar, 200);


                    cmd.Parameters.Add("[Project Start Date]", OleDbType.VarChar, 100);
                    cmd.Parameters.Add("[Project End Date]", OleDbType.VarChar, 100);
                    cmd.Parameters.Add("[Contract ID]", OleDbType.VarChar, 200);


                    foreach (GetProjectAssociationResult row in list)
                    {
                        cmd.Parameters[0].Value = row.ID;
                        cmd.Parameters[1].Value = row.EID;
                        cmd.Parameters[2].Value = row.EmployeeName;
                        cmd.Parameters[3].Value = row.ProjectName == null ? "" : row.ProjectName;


                        cmd.Parameters[4].Value = row.Activity == null ? "" : row.Activity.ToString();


                        cmd.Parameters[5].Value = row.StartDate == null ? "" : row.StartDate.Value.ToShortDateString();
                        cmd.Parameters[6].Value = row.EndDate == null ? "" : row.EndDate.Value.ToShortDateString();
                        cmd.Parameters[7].Value = row.ContractID == null ? "" : row.ContractID.ToString();


                        //if (row.IsExpired != null && row.IsExpired.Value)
                        //    cmd.Parameters[11].Value = "Expire";
                        //else
                        //    cmd.Parameters[11].Value = "Un Expire";

                        cmd.ExecuteNonQuery();
                    }


                    WriteIntoSheet(conn, "InsuranceCompanySheet", GetProjectList);
                }

                ExcelGenerator.WriteFile(copyPath, Path.GetFileName(excelTemplatePath));
            }
            finally
            {
                try
                {
                    File.Delete(copyPath);
                }
                catch
                {
                }
            }
        }
        public static OleDbCommand GetEmployeeWriteCommand(OleDbConnection conn, string[] incomeList, string[] deductionList, string[] leaveList)
        {
            #region "Prepare SQL query

            StringBuilder writeSql = new StringBuilder("INSERT INTO [Employee_Info$](EIN ");
            StringBuilder valueSql = new StringBuilder(" values(?");
            for (int i = 0; i < employeesStaticColumns.Length; i++)
            //foreach (string column in employeesStaticColumns)
            {
                string column = employeesStaticColumns[i];

                writeSql.AppendFormat(",[{0}] ", column);
                valueSql.AppendFormat(",?");
            }

            // Write for Incomes
            for (int i = 0; i < incomeList.Length; i++)
            {
                writeSql.AppendFormat(",[{0} Income] ", incomeList[i]);
                valueSql.AppendFormat(",?");
            }
            // Write for Deductions
            for (int i = 0; i < deductionList.Length; i++)
            {
                writeSql.AppendFormat(",[{0} Deduction] ", deductionList[i]);
                valueSql.AppendFormat(",?");
            }
            // Write for Leaves
            for (int i = 0; i < leaveList.Length; i++)
            {
                writeSql.AppendFormat(",[{0} Leave] ", leaveList[i]);
                valueSql.AppendFormat(",?");
            }

            writeSql.Append(")");
            valueSql.Append(")");


            #endregion

            #region "Parepare Command"
            // Add Parameters
            OleDbCommand writeCmd = new OleDbCommand(writeSql.ToString() + " " + valueSql.ToString(), conn);

            writeCmd.Parameters.Add("EIN", OleDbType.Integer, 4);
            for (int i = 0; i < employeesStaticColumns.Length; i++)
            {
                string column = employeesStaticColumns[i];
                writeCmd.Parameters.Add(column, OleDbType.VarChar, 200);
                writeCmd.Parameters[column].IsNullable = true;
            }

            // Write for Incomes
            for (int i = 0; i < incomeList.Length; i++)
            {
                writeCmd.Parameters.Add("[" + incomeList[i] + " Income]", OleDbType.VarChar, 100);
            }
            // Write for Deductions
            for (int i = 0; i < deductionList.Length; i++)
            {
                writeCmd.Parameters.Add("[" + deductionList[i] + " Deduction]", OleDbType.VarChar, 100);
            }
            // Write for Leaves
            for (int i = 0; i < leaveList.Length; i++)
            {

                writeCmd.Parameters.Add("[" + leaveList[i] + " Leave]", OleDbType.VarChar, 100);
            }
            #endregion

            return writeCmd;
        }


        public static void WriteExportExcel(string excelTemplatePath)
        {
            int firstPayrollPeriodId = CommonManager.GetFirstPayrollPeriod(SessionManager.CurrentCompanyId).PayrollPeriodId;



            string path = HttpContext.Current.Server.MapPath(excelTemplatePath);
            string copyPath = Path.GetDirectoryName(path) + "\\" + Guid.NewGuid() + ".xlsx";
            File.Copy(path, copyPath);

            string excelConnection = string.Format(ExcelConnectionString, copyPath);


            ExcelGenerator.WriteFile(copyPath, Path.GetFileName(excelTemplatePath));
            return;


        }


        #region "Opening Leave"
        public static void WriteOpeningLeaveExportExcel(List<EEmployee> employeeList, string excelTemplatePath, string[] leaveList, List<LLeaveType> leaves)
        {



            string path = HttpContext.Current.Server.MapPath(excelTemplatePath);
            string copyPath = Path.GetDirectoryName(path) + "\\" + Guid.NewGuid() + ".xlsx";
            File.Copy(path, copyPath);

            string excelConnection = string.Format(ExcelConnectionString, copyPath);

            OleDbCommand writeCmd = null;
            OleDbCommand cmd1 = null;
            try
            {
                OleDbConnection conn = null;
                /// Awlays remember to property dispose Command & Connection as for Larger emp like 500 if not dispose
                /// then error can come
                try
                {
                    conn = new OleDbConnection(excelConnection);
                    conn.Open();



                    StringBuilder sql = new StringBuilder("Create Table [Employee_Info$]( EIN int,[Employee Name] varchar(150),Status varchar(100)");

                    // Leaves columns
                    for (int i = 0; i < leaveList.Length; i++)
                    {
                        sql.AppendFormat(", [{0}] varchar(100)", leaveList[i]);
                    }
                    sql.Append(" )");


                    cmd1 = new OleDbCommand(sql.ToString(), conn);
                    cmd1.ExecuteNonQuery();



                    #region "Write Existing Employees"

                    writeCmd = GetEmployeeWriteCommand(conn, leaveList);

                    WriteEachEmpInExcel(conn, employeeList, writeCmd, leaveList
                        , leaves);


                    #endregion

                }
                //catch (Exception ee)
                //{
                //    Log.log("Error while exporting employee", ee);
                //}
                finally
                {
                    if (conn != null)
                    {
                        if (cmd1 != null)
                            cmd1.Dispose();
                        if (writeCmd != null)
                            writeCmd.Dispose();

                        conn.Close();
                        conn.Dispose();
                    }
                }
                ExcelGenerator.WriteFile(copyPath, Path.GetFileName(excelTemplatePath));
                return;
            }
            //catch (Exception exp)
            //{

            //    Log.log("Error while exporting employee", exp);
            //}
            finally
            {
                try
                {
                    //File.Delete(copyPath);
                }
                catch
                {
                }
            }
        }





        public static OleDbCommand GetEmployeeWriteCommand(OleDbConnection conn, string[] leaveList)
        {
            #region "Prepare SQL query

            StringBuilder writeSql = new StringBuilder("INSERT INTO [Employee_Info$](EIN,[Employee Name],Status");
            StringBuilder valueSql = new StringBuilder(" values(?,?,?");

            // Write for Leaves
            for (int i = 0; i < leaveList.Length; i++)
            {
                writeSql.AppendFormat(",[{0}] ", leaveList[i]);
                valueSql.AppendFormat(",?");
            }

            writeSql.Append(")");
            valueSql.Append(")");


            #endregion

            #region "Parepare Command"
            // Add Parameters
            OleDbCommand writeCmd = new OleDbCommand(writeSql.ToString() + " " + valueSql.ToString(), conn);

            writeCmd.Parameters.Add("EIN", OleDbType.Integer, 4);
            writeCmd.Parameters.Add("[Employee Name]", OleDbType.VarChar, 150);
            writeCmd.Parameters.Add("Status", OleDbType.VarChar, 150);

            // Write for Leaves
            for (int i = 0; i < leaveList.Length; i++)
            {

                writeCmd.Parameters.Add("[" + leaveList[i] + "]", OleDbType.VarChar, 100);
            }
            #endregion

            return writeCmd;
        }


        public static void WriteEachEmpInExcel(OleDbConnection conn, List<EEmployee> empList
                  , OleDbCommand writeCmd, string[] leaveList, List<LLeaveType> leaves)
        {

            List<TextValue> empStatusList = LeaveAttendanceManager.GetEmployeeStatusList();

            List<KeyValue> list = new JobStatus().GetMembers();

            foreach (EEmployee emp in empList)
            {


                writeCmd.Parameters[0].Value = emp.EmployeeId;

                writeCmd.Parameters[1].Value = emp.Name.Trim();

                TextValue empStatus = empStatusList.FirstOrDefault(x => x.ID == emp.EmployeeId);
                if (empStatus != null && empStatus.Value2 != null)
                {
                    KeyValue status = list.FirstOrDefault(x => x.Key == empStatus.Value2.ToString());
                    if (status != null)
                        writeCmd.Parameters[2].Value = status.Value;
                    else
                        writeCmd.Parameters[2].Value = "";
                }
                else
                    writeCmd.Parameters[2].Value = "";

                //// Write for Leaves
                ////index = 29 + incomeList.Length + deductionList.Length;
                for (int i = 0; i < leaveList.Length; i++)
                {
                    writeCmd.Parameters["[" + leaveList[i] + "]"].Value = GetLeave(emp, leaveList[i], leaves);
                }


                writeCmd.ExecuteNonQuery();
            }
        }

        #endregion

        public static string GetSubDepartment(int? subdepartmentId)
        {
            if (subdepartmentId == null)
                return "";

            return new DepartmentManager().GetSubDepartmentById(subdepartmentId.Value).Name;
        }

        public static string GetSubDepartmentForEmployeeImport(int? subdepartmentId)
        {
            if (subdepartmentId == null || subdepartmentId == -1 || subdepartmentId == 0)
                return "";

            return new DepartmentManager().GetSubDepartmentById(subdepartmentId.Value).HierarchyName; ;
        }

        public static void WriteEachEmpInExcel(OleDbConnection conn, List<EEmployee> empList, bool importIsEnglishDate, string importDateFormat
            , OleDbCommand writeCmd, string[] incomeList, string[] deductionList, string[] leaveList
            , List<PIncome> incomes, List<PDeduction> deductions, List<LLeaveType> leaves, List<EDesignation> designations, string[] shiftList
            , List<HCaste> ethinicityList, List<ELocation> locationList, List<EGrade> gradeStepList, List<UnitList> unitList)
        {

            List<KeyValue> list = new JobStatus().GetMembers();

            foreach (EEmployee emp in empList)
            {


                writeCmd.Parameters[0].Value = emp.EmployeeId;
                writeCmd.Parameters[1].Value = emp.Title.Trim();
                writeCmd.Parameters[2].Value = emp.FirstName.Trim();
                writeCmd.Parameters[3].Value = emp.MiddleName.Trim();
                writeCmd.Parameters[4].Value = emp.LastName.Trim();
                writeCmd.Parameters[5].Value = emp.FatherName.Trim();
                writeCmd.Parameters[6].Value = new Gender().GetText(emp.Gender.Value);
                writeCmd.Parameters[7].Value = emp.MaritalStatus;
                writeCmd.Parameters[8].Value = GetYesNo(emp.IsHandicapped);
                writeCmd.Parameters[9].Value = GetYesNo(emp.HasCoupleTaxStatus);
                writeCmd.Parameters[10].Value = GetYesNo(emp.HasDependent);

                // DOB
                if (emp.IsEnglishDOB != null)
                {
                    if (IsEnglish)
                    {
                        writeCmd.Parameters[11].Value = DBNull.Value;
                        writeCmd.Parameters[12].Value = emp.DateOfBirth;
                    }
                    else
                    {

                        if (emp.IsEnglishDOB == false)
                            writeCmd.Parameters[11].Value = emp.DateOfBirth;
                        else
                            writeCmd.Parameters[11].Value = DBNull.Value;

                        if (emp.IsEnglishDOB == true)
                            writeCmd.Parameters[12].Value = emp.DateOfBirth;
                        else
                            writeCmd.Parameters[12].Value = DBNull.Value;
                    }

                }
                else
                {
                    writeCmd.Parameters[11].Value = "";
                    writeCmd.Parameters[12].Value = "";
                }

                EDesignation designation = designations.FirstOrDefault(x => x.DesignationId == emp.DesignationId);
                writeCmd.Parameters[13].Value = designation.BLevel.Name + " - " + emp.EDesignation.Name;
                writeCmd.Parameters[14].Value = emp.Branch.Name;
                writeCmd.Parameters[15].Value = emp.Department.DepartmentAndBrachName;
                writeCmd.Parameters[16].Value = GetSubDepartmentForEmployeeImport(emp.SubDepartmentId);

                writeCmd.Parameters[17].Value = GetStatusText(emp, importIsEnglishDate, importDateFormat, list);

                // Contract No doesn't exists
                writeCmd.Parameters[18].Value = "";

                writeCmd.Parameters[19].Value = GetEmpty(emp.EHumanResources[0].IdCardNo);
                if (emp.EAddresses.Count > 0)
                {
                    writeCmd.Parameters[20].Value = GetEmpty(emp.EAddresses[0].CIEmail);
                    writeCmd.Parameters[21].Value = GetEmpty(emp.EAddresses[0].CIPhoneNo);
                    writeCmd.Parameters[22].Value = GetEmpty(emp.EAddresses[0].Extension);
                }
                // PF & CIT
                //if (emp.EFundDetails.Count > 0)
                {
                    writeCmd.Parameters[23].Value = emp.EFundDetails[0].PFRFNo == null ? "" : emp.EFundDetails[0].PFRFNo;
                    writeCmd.Parameters[24].Value = emp.EFundDetails[0].PANNo == null ? "" : emp.EFundDetails[0].PANNo;
                    writeCmd.Parameters[25].Value = GetEmpty(emp.EFundDetails[0].CITNo);
                    writeCmd.Parameters[26].Value = GetEmpty(emp.EFundDetails[0].CITCode);
                    writeCmd.Parameters[27].Value = GetCITEffectiveFrom(emp.EFundDetails[0], importIsEnglishDate, importDateFormat);
                    writeCmd.Parameters[28].Value = GetCITAmountOrRate(emp.EFundDetails[0]);
                }
                // Payment Mode
                writeCmd.Parameters[29].Value = emp.PPays[0].PaymentMode == null ? "" : emp.PPays[0].PaymentMode;
                writeCmd.Parameters[30].Value = emp.PPays[0].BankName == null ? "" : emp.PPays[0].BankName;
                writeCmd.Parameters[31].Value = emp.PPays[0].BankACNo == null ? "" : emp.PPays[0].BankACNo;

                // Insurance Premium Amount
                IIndividualInsurance insurance = emp.IIndividualInsurances.
                    FirstOrDefault(x => x.EmployeeId == emp.EmployeeId && x.FinancialYearId == SessionManager.CurrentCompanyFinancialDate.FinancialDateId);

                if (insurance != null && insurance.Premium != null)
                {
                    writeCmd.Parameters[32].Value = (insurance.Premium.Value);
                }
                else
                    writeCmd.Parameters[32].Value = "";

                //Level and Grade
                if (emp.PEmployeeIncomes.Count > 0 && emp.PEmployeeIncomes[0].LevelId != null)
                {
                    PEmployeeIncome empIncome = emp.PEmployeeIncomes[0];
                    if (empIncome.LevelId != null)
                    {
                        BLevel level = NewPayrollManager.GetLevelById(empIncome.LevelId.Value);
                        writeCmd.Parameters[33].Value = level.BLevelGroup.Name + " - " + level.Name;
                        writeCmd.Parameters[34].Value = empIncome.Step.Value.ToString();
                    }
                }
                else
                {
                    // show level from EDesignation
                    BLevel level = designation.BLevel;
                    writeCmd.Parameters[33].Value = level.GroupLevel;
                    writeCmd.Parameters[34].Value = DBNull.Value;
                }

                // Shift
                if (shiftList.Length > 0)
                {
                    EWorkShift empFirstShift = ShiftManager.GetEmployeeFirstShift(emp.EmployeeId);
                    if (empFirstShift != null)
                    {
                        writeCmd.Parameters[35].Value = empFirstShift.Name;
                    }
                    else
                        writeCmd.Parameters[35].Value = DBNull.Value;
                }
                else
                    writeCmd.Parameters[35].Value = DBNull.Value;

                // Cost Code
                if (emp.CostCodeId != null)
                {
                    ECostCode code = CommonManager.GetAllCostCodes().FirstOrDefault(x => x.CostCodeId == emp.CostCodeId);
                    if (code != null)
                    {
                        writeCmd.Parameters[36].Value = code.Name;
                    }
                    else
                    {
                        writeCmd.Parameters[36].Value = "";
                    }
                }
                else
                    writeCmd.Parameters[36].Value = "";

                // Ethinicity List
                if (emp.HHumanResources.Count > 0 && emp.HHumanResources[0].CasteId != null)
                {
                    HCaste caste = ethinicityList.FirstOrDefault(x => x.CasteId == emp.HHumanResources[0].CasteId);
                    if (caste != null)
                    {
                        writeCmd.Parameters[37].Value = caste.CasteName;
                    }
                    else
                    {
                        writeCmd.Parameters[37].Value = "";
                    }
                }
                else
                    writeCmd.Parameters[37].Value = "";

                // Location List
                if (emp.EHumanResources.Count > 0 && emp.EHumanResources[0].LocationId != null)
                {
                    ELocation location = locationList.FirstOrDefault(x => x.LocationId == emp.EHumanResources[0].LocationId);
                    if (location != null)
                    {
                        writeCmd.Parameters[38].Value = location.Name;
                    }
                    else
                    {
                        writeCmd.Parameters[38].Value = "";
                    }
                }
                else
                    writeCmd.Parameters[38].Value = "";

                // Grade/Step List
                if (emp.GradeId != null)
                {
                    EGrade grade = gradeStepList.FirstOrDefault(x => x.GradeId == emp.GradeId);
                    if (grade != null)
                    {
                        writeCmd.Parameters[39].Value = grade.Name;
                    }
                    else
                    {
                        writeCmd.Parameters[39].Value = "";
                    }
                }
                else
                    writeCmd.Parameters[39].Value = "";

                // Unit
                if (emp.UnitId != null)
                {
                    UnitList unit = unitList.FirstOrDefault(x => x.UnitId == emp.UnitId);
                    if (unit != null)
                    {
                        writeCmd.Parameters[40].Value = unit.Name;
                    }
                    else
                    {
                        writeCmd.Parameters[40].Value = "";
                    }
                }
                else
                    writeCmd.Parameters[40].Value = "";

                // Incomes
                for (int i = 0; i < incomeList.Length; i++)
                {
                    writeCmd.Parameters["[" + incomeList[i] + " Income]"].Value = GetIncome(emp, incomeList[i], incomes);

                }
                //// Write for Deductions
                ////index = 29 + incomeList.Length;
                for (int i = 0; i < deductionList.Length; i++)
                {
                    writeCmd.Parameters["[" + deductionList[i] + " Deduction]"].Value = GetDeduction(emp, deductionList[i], deductions);
                }
                //// Write for Leaves
                ////index = 29 + incomeList.Length + deductionList.Length;
                for (int i = 0; i < leaveList.Length; i++)
                {
                    writeCmd.Parameters["[" + leaveList[i] + " Leave]"].Value = GetLeave(emp, leaveList[i], leaves);
                }


                writeCmd.ExecuteNonQuery();
            }
        }

        public static string GetLeave(EEmployee emp, string leaveTitle, List<LLeaveType> leaves)
        {
            LLeaveType leave = leaves.Where(i => i.Title == leaveTitle).Take(1).SingleOrDefault();
            if (leave == null)
                return "";
            foreach (LEmployeeLeave empLeave in emp.LEmployeeLeaves)
            {
                if (leave.LeaveTypeId == empLeave.LeaveTypeId)
                {
                    if (leave.FreqOfAccrual == LeaveAccrue.YEARLY || leave.FreqOfAccrual == LeaveAccrue.HALFYEARLY ||
                        leave.FreqOfAccrual == LeaveAccrue.MANUALLY || leave.FreqOfAccrual == LeaveAccrue.COMPENSATORY)
                    {
                        //if (empLeave.CurrentYearBalance != null && empLeave.CurrentYearBalance != 0
                        //    && empLeave.OpeningTaken != null && empLeave.OpeningTaken != 0)
                        return empLeave.BeginningBalance + "_" +
                            (empLeave.CurrentYearBalance == null ? 0 : empLeave.CurrentYearBalance.Value) + "_" +
                            (empLeave.OpeningTaken == null ? 0 : empLeave.OpeningTaken);
                        //else
                        //    return empLeave.BeginningBalance.ToString();
                    }
                    else
                    {
                        return empLeave.BeginningBalance.ToString();
                    }

                }
            }
            return "";
        }

        /// <summary>
        /// Retrieve Deduction value for the Employee
        /// </summary>
        public static string GetDeduction(EEmployee emp, string deductionTitle, List<PDeduction> deductions)
        {
            PDeduction deduction = deductions.Where(i => i.Title == deductionTitle).Take(1).SingleOrDefault();
            foreach (PEmployeeDeduction empDeduction in emp.PEmployeeDeductions)
            {
                if (deduction.DeductionId == empDeduction.DeductionId)
                {
                    switch (deduction.Calculation)
                    {
                        case IncomeCalculation.VARIABLE_AMOUNT:
                        case IncomeCalculation.FIXED_AMOUNT:
                            if (empDeduction.Amount == null)
                                return "";
                            return GetCurrency(empDeduction.Amount);

                        case IncomeCalculation.PERCENT_OF_INCOME:
                            if (empDeduction.Rate == null)
                                return "";
                            return empDeduction.Rate.ToString();


                    }
                }
            }
            return "";
        }

        public static string GetIncome(EEmployee emp, string incomeTitle, List<PIncome> incomes)
        {
            PIncome income = incomes.Where(i => i.Title == incomeTitle).Take(1).SingleOrDefault();
            foreach (PEmployeeIncome empIncome in emp.PEmployeeIncomes)
            {
                if (empIncome.IncomeId == income.IncomeId)
                {
                    switch (income.Calculation)
                    {
                        case IncomeCalculation.VARIABLE_AMOUNT:
                        case IncomeCalculation.FIXED_AMOUNT:
                            if (empIncome.Amount == null)
                                return "";
                            return GetCurrency(empIncome.Amount);

                        case IncomeCalculation.PERCENT_OF_INCOME:
                        case IncomeCalculation.Unit_Rate:
                            if (empIncome.RatePercent == null)
                                return "";
                            return empIncome.RatePercent.ToString();

                        case IncomeCalculation.FESTIVAL_ALLOWANCE:

                            if (income.IsVariableAmount.HasValue && income.IsVariableAmount.Value)
                            {
                                if (empIncome.Amount == null)
                                    return "";
                                return GetCurrency(empIncome.Amount);
                            }
                            else
                            {
                                if (empIncome.RatePercent == null)
                                    return "";
                                return empIncome.RatePercent.ToString();
                            }

                    }
                }
            }
            return "";
        }

        public static string GetCITAmountOrRate(EFundDetail fund)
        {
            if (fund.CITIsRate == null)
                return "";

            if (fund.CITIsRate == false)
            {
                if (fund.CITAmount == null)
                    return "";
                else
                    return GetCurrency(fund.CITAmount);
            }
            else
            {
                if (fund.CITRate == null)
                    return "";
                else
                {
                    return fund.CITRate + "%";
                }
            }
        }
        public static string GetCITEffectiveFrom(EFundDetail fund, bool importIsEnglishDate, string importDateFormat)
        {
            if (fund.CITEffectiveFromMonth == null)
                return "";

            string date = fund.CITEffectiveFromMonth + "/" + fund.CITEffectiveFromYear;


            return date;
        }
        public static string GetEmpty(string value)
        {
            if (value == null)
                return "";
            return value;
        }
        public static string GetStatusText(EEmployee emp, bool importIsEnglishDate, string importDateFormat, List<KeyValue> list)
        {
            string str = "";
            string date, toDateValue;

            foreach (ECurrentStatus status in emp.ECurrentStatus)
            {
                toDateValue = "";
                date = status.FromDate;
                if (str != "")
                    str += ",";

                CustomDate cDate = CustomDate.GetCustomDateFromString(date, IsEnglish);
                CustomDate toDate = null;

                if (status.DefineToDate != null && status.DefineToDate.Value)
                    toDate = CustomDate.GetCustomDateFromString(status.ToDate, IsEnglish);

                if (IsEnglish != importIsEnglishDate)
                {
                    if (importIsEnglishDate)
                        date = CustomDate.ConvertNepToEng(cDate).ToStringWithFormat(importDateFormat);
                    else
                        date = CustomDate.ConvertEngToNep(cDate).ToStringWithFormat(importDateFormat);
                }
                else
                    date = cDate.ToStringWithFormat(importDateFormat);

                if (toDate != null)
                {
                    if (IsEnglish != importIsEnglishDate)
                    {
                        if (importIsEnglishDate)
                            toDateValue = CustomDate.ConvertNepToEng(toDate).ToStringWithFormat(importDateFormat);
                        else
                            toDateValue = CustomDate.ConvertEngToNep(toDate).ToStringWithFormat(importDateFormat);
                    }
                    else
                        toDateValue = toDate.ToStringWithFormat(importDateFormat);
                }

                KeyValue value = list.SingleOrDefault(k => k.Key == status.CurrentStatus.ToString());

                if (value == null)
                    str += status.CurrentStatusText + "_" + date;
                else
                    str += value.Value + "_" + date;

                if (toDateValue != "")
                    str += "_" + toDateValue;
            }
            return str;
        }

        public static string GetYesNo(bool? state)
        {
            if (state == null)
                return "";
            if (state.Value == true)
                return "Yes";
            else
                return "No";
        }

        public static void WriteIntoSheet(OleDbConnection conn, string sheetName, string[] list)
        {
            StringBuilder sql = new StringBuilder(string.Format("Create Table [{0}$]( Name varchar(100) )", sheetName));


            OleDbCommand cmd1 = new OleDbCommand(sql.ToString(), conn);
            using (cmd1)
            {
                cmd1.ExecuteNonQuery();
            }

            ////// Create a new sheet in the Excel spreadsheet.
            OleDbCommand cmd = new OleDbCommand(string.Format("INSERT INTO [{0}$](Name) values (?)", sheetName), conn);
            cmd.Parameters.Add("Name", OleDbType.VarChar, 100);
            using (cmd)
            {
                foreach (var item in list)
                {
                    cmd.Parameters[0].Value = item;


                    cmd.ExecuteNonQuery();
                }
            }
        }
        #endregion

        public static string GetExcelForEmployeeList(List<EEmployee> list)
        {
            StringBuilder str = new StringBuilder();
            foreach (EEmployee emp in list)
            {
                str.Append("<Row>\n");
                str.Append(
                    string.Format("<Cell><Data ss:Type=\"Number\">{0}</Data></Cell>\n", emp.EmployeeId)
                    );
                str.Append(
                    string.Format("<Cell><Data ss:Type=\"String\">{0}</Data></Cell>\n", emp.Name)
                    );

                //string[] values = emp.Title.Split(new string[] { "$$" }, StringSplitOptions.None);

                str.Append(
                    string.Format("<Cell><Data ss:Type=\"String\">{0}</Data></Cell>\n", emp.BranchValue)
                    );

                str.Append(
                    string.Format("<Cell><Data ss:Type=\"String\">{0}</Data></Cell>\n", emp.DepartmentValue)
                    );

                str.Append(
                    string.Format("<Cell><Data ss:Type=\"String\">{0}</Data></Cell>\n", emp.SubDepartmentValue)
                    );

                str.Append(
                 string.Format("<Cell><Data ss:Type=\"String\">{0}</Data></Cell>\n", emp.LevelDesignationValue)
                 );


                str.Append(
                   string.Format("<Cell><Data ss:Type=\"String\">{0}</Data></Cell>\n", emp.LevelValue)
                   );

                str.Append(
                    string.Format("<Cell><Data ss:Type=\"String\">{0}</Data></Cell>\n", emp.DesignationValue)
                    );

                str.Append(
                    string.Format("<Cell><Data ss:Type=\"String\">{0}</Data></Cell>\n", emp.FunctionalTitle)
                    );
                str.Append(
                   string.Format("<Cell><Data ss:Type=\"String\">{0}</Data></Cell>\n", emp.StatusText)
                   );


                str.Append(
                   string.Format("<Cell><Data ss:Type=\"String\">{0}</Data></Cell>\n", emp.IdCardNoValue)
                   );

                str.Append(
                    string.Format("<Cell><Data ss:Type=\"String\">{0}</Data></Cell>\n", emp.IsRetiredOrResignedValue)
                    );
                str.Append("</Row>");
            }
            return string.Format(GetTemplate(), str.ToString());
        }

        public static void WriteExcel(string content)
        {
            //Response.ClearContent();
            HttpContext.Current.Response.ClearHeaders();
            //Response.Buffer = true;
            //Response.Cache.SetCacheability(HttpCacheability.Private);
            // Set ContentType to the ContentType of our file
            HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";
            // Response.ContentType = "application/vnd.ms-excel";
            HttpContext.Current.Response.AddHeader("Content-Disposition", String.Format("attachment;filename=\"{0}\"", "Employees.xls"));

            HttpContext.Current.Response.Write(content);

            // Write data out of database into Output Stream
            HttpContext.Current.ApplicationInstance.CompleteRequest();
            HttpContext.Current.Response.End();
        }

        public static string GetTemplate()
        {
            //
            //<Cell><Data ss:Type="Number">1</Data></Cell>
            //<Cell><Data ss:Type="String">2</Data></Cell>    
            //
            return HttpContext.GetGlobalResourceObject("Templates", "EmployeeList").ToString();

            //return Resources.Resource.ExcelTemplate;
        }

        public static long? GetAmountValueFromCellForActivityCase(string columnName, DataRow row, int ein)
        {
            object value = row[columnName];

            if (value == null || DBNull.Value.Equals(value))
                return null;

            // as 999999999999994 and 999999999999995 also so comment this
            //if (value.ToString().ToLower() == "1E+015".ToLower())
            //    value = "999999999999999";


            string val = value.ToString();

            if (val == string.Empty)
                return null;

            long intValue;
            if (long.TryParse(val, out intValue))
                return intValue;

            return -1;

            //  throw new Exception("Activity should be 1-500 or 999999999999");
            //throw new InvalidValue(ein, columnName, val);
        }
        private static bool IsExponentialFormat(string str)
        {
            double dummy;
            return (str.Contains("E") || str.Contains("e")) && double.TryParse(str, out dummy);
        }
        public static DataTable ImportFromExcel(string path, string sql)
        {
            string excelConnection = string.Format(ExcelConnectionString, path);
            DataTable dtExcel = new DataTable();

            try
            {

                using (OleDbConnection conn = new OleDbConnection(excelConnection))
                {
                    // Initialize an OleDbDataAdapter object.
                    OleDbDataAdapter da = new OleDbDataAdapter(sql, conn);

                    // Fill the DataTable with data from the Excel spreadsheet.
                    da.Fill(dtExcel);
                }
            }
            catch (Exception exp)
            {
                Log.log("Error while reading from excel", exp);
                return null;
            }
            finally
            {
                try
                {
                    File.Delete(path);
                }
                catch { }

            }


            return dtExcel;
        }

        #region Excel input/output utility

        public static string GetColumnValue(string columnName, float? value)
        {
            if (value == null)
                return "";
            return string.Format("{0}='{1}'", columnName, value);
        }

        public static string GetColumnValue(string columnName, decimal? value)
        {
            if (value == null)
                return "";
            return string.Format("{0}='{1}'", columnName, value);
        }

        public static object GetValue(double? value)
        {
            if (value == null)
                return DBNull.Value;

            return value.Value;
        }

        public static object GetValue(decimal? value)
        {
            if (value == null)
                return DBNull.Value;

            return value.Value;
        }

        public static float? GetValueFromCell(string columnName, DataRow row, int ein)
        {
            object value = row[columnName];

            if (value == null || DBNull.Value.Equals(value))
                return null;

            string val = value.ToString();

            if (val == string.Empty)
                return null;

            float intValue;
            if (float.TryParse(val, out intValue))
                return intValue;

            throw new InvalidValue(ein, columnName, val);
        }
        public static int? GetIntValueFromCell(string columnName, DataRow row, int ein)
        {
            object value = row[columnName];

            if (value == null || DBNull.Value.Equals(value))
                return null;

            string val = value.ToString();

            if (val == string.Empty)
                return null;

            int intValue;
            if (int.TryParse(val, out intValue))
                return intValue;

            throw new InvalidValue(ein, columnName, val);
        }

        public static decimal? GetAmountValueFromCell(string columnName, DataRow row, int ein)
        {
            object value = row[columnName];

            if (value == null || DBNull.Value.Equals(value))
                return null;

            string val = value.ToString();

            if (val == string.Empty)
                return null;

            decimal intValue;
            if (decimal.TryParse(val, out intValue))
                return intValue;

            throw new InvalidValue(ein, columnName, val);
        }


        public static string GetStringValueFromCell(string columnName, DataRow row, int ein)
        {
            object value = row[columnName];

            if (value == null || DBNull.Value.Equals(value))
                return null;

            string val = value.ToString();

            if (val == string.Empty)
                return null;
            return val;
        }


        public class InvalidValue : Exception
        {
            public int EIN { get; set; }
            public string ColumnName { get; set; }
            public string Value { get; set; }

            public InvalidValue(int ein, string column, string value)
            {
                this.EIN = ein;
                this.ColumnName = column;
                this.Value = value;
            }
        }
        public static float? GetValueFromCell(string columnName, GridViewRow row, int ein)
        {
            object value = (row.FindControl(columnName) as TextBox).Text.Trim();

            if (value == null)
                return null;

            string val = value.ToString();

            if (val == string.Empty)
                return null;

            float intValue;
            if (float.TryParse(val, out intValue))
                return intValue;

            //for invalid value save as 0
            return 0;
        }
        #endregion

        //public void CreateTableInExcel()
        //{
        //    string filename = Server.MapPath("~/App_Data/ExcelTemplate/ShiftAllowanceTemplate.xlsx");
        //    string strExcelConn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + (filename) + ";Extended Properties='Excel 12.0 Xml;HDR=Yes'";


        //    using (OleDbConnection conn = new OleDbConnection(strExcelConn))
        //    {
        //        // Create a new sheet in the Excel spreadsheet.
        //        OleDbCommand cmd = new OleDbCommand("create table Shift(EIN int,Name varchar(50),MorningShiftHours varchar(50),EveningShiftHours varchar(50),NightShiftHours varchar(50) )", conn);

        //        // Open the connection.
        //        conn.Open();

        //        // Execute the OleDbCommand.
        //        cmd.ExecuteNonQuery();

        //    }
        //}


        public static void WriteVariableIncomeForPFExportExcel(string excelTemplatePath, List<PIncome> list,
       List<GetEmployeesForVariablePFIncomesResult> employees, List<GetEmployeesIncomeForPFVariableIncomesResult> projectPays)
        {
            string path = HttpContext.Current.Server.MapPath(excelTemplatePath);
            string copyPath = Path.GetDirectoryName(path) + "\\" + Guid.NewGuid() + ".xlsx";
            File.Copy(path, copyPath);

            string excelConnection = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + copyPath + ";Extended Properties='Excel 12.0 Xml;HDR=Yes'";

            try
            {

                using (OleDbConnection conn = new OleDbConnection(excelConnection))
                {
                    // Open the connection.
                    conn.Open();

                    StringBuilder sql = new StringBuilder("Create Table [Sheet1$](EIN int,Name varchar(50),[Level] varchar(100)");
                    for (int i = 0; i < list.Count; i++)
                    {
                        // Skip first project as it contains "All Project" case 
                        //if (i != 0)
                        {
                            sql.AppendFormat(", {0} varchar(20)", "[" + list[i].Title.Trim() + "]");
                        }
                    }
                    sql.Append(" )");

                    OleDbCommand cmd1 = new OleDbCommand(sql.ToString(), conn);
                    cmd1.ExecuteNonQuery();



                    ////// Create a new sheet in the Excel spreadsheet.
                    #region "Prepare Insert SQL"
                    string sqlInsert = "INSERT INTO [Sheet1$]{0} values {1}";
                    string sqlInsertField = "(EIN,Name,[Level]";
                    string sqlInsertValue = "(?,?,?";
                    for (int i = 0; i < list.Count; i++)
                    {
                        // Skip first project as it contains "All Project" case 
                        // if (i != 0)
                        {
                            sqlInsertField += ",[" + list[i].Title.Trim() + "]";
                            sqlInsertValue += ",?";
                        }
                    }
                    sqlInsertField += ")";
                    sqlInsertValue += ")";
                    sqlInsert = string.Format(sqlInsert, sqlInsertField, sqlInsertValue);

                    OleDbCommand cmd = new OleDbCommand(sqlInsert, conn);


                    #endregion

                    #region "Add Parameters"
                    cmd.Parameters.Add("EIN", OleDbType.Integer, 4);
                    cmd.Parameters.Add("Name", OleDbType.VarChar, 50);
                    cmd.Parameters.Add("[Level]", OleDbType.VarChar, 100);


                    for (int i = 0; i < list.Count; i++)
                    {
                        // Skip first project as it contains "All Project" case 
                        //if (i != 0)
                        {
                            cmd.Parameters.Add(list[i].Title, OleDbType.VarChar, 20);
                        }
                    }
                    #endregion



                    string Value;
                    foreach (GetEmployeesForVariablePFIncomesResult row in employees)
                    {
                        cmd.Parameters[0].Value = row.EmployeeId;
                        cmd.Parameters[1].Value = row.Name;
                        cmd.Parameters[2].Value = row.Level == null ? "" : row.Level.ToString();

                        int position = 3;
                        for (int i = 0; i < list.Count; i++)
                        {
                            Value = GetEmployeeVariablePFValue(row.EmployeeId, list[i], projectPays); ;
                            cmd.Parameters[position].Value = Value;
                            position += 1;

                        }

                        cmd.ExecuteNonQuery();
                    }
                }
                ExcelGenerator.WriteFile(copyPath, Path.GetFileName(excelTemplatePath));
            }
            finally
            {
                try
                {
                    File.Delete(copyPath);
                }
                catch
                {
                }
            }
        }


        public static void WriteVariableIncomeForExportExcel(string excelTemplatePath, List<PIncome> list,
          List<GetEmployeesForVariableIncomesResult> employees, List<GetEmployeesIncomeForVariableIncomesResult> projectPays)
        {
            string path = HttpContext.Current.Server.MapPath(excelTemplatePath);
            string copyPath = Path.GetDirectoryName(path) + "\\" + Guid.NewGuid() + ".xlsx";
            File.Copy(path, copyPath);

            string excelConnection = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + copyPath + ";Extended Properties='Excel 12.0 Xml;HDR=Yes'";

            try
            {

                using (OleDbConnection conn = new OleDbConnection(excelConnection))
                {
                    // Open the connection.
                    conn.Open();

                    StringBuilder sql = new StringBuilder("Create Table [Sheet1$]( EIN int,INo varchar(50), [Account No] varchar(100), Employee varchar(200) ");
                    for (int i = 0; i < list.Count; i++)
                    {
                        // Skip first project as it contains "All Project" case 
                        //if (i != 0)
                        {
                            sql.AppendFormat(", {0} money", "[" + list[i].Title.Trim() + "]");
                        }
                    }
                    sql.Append(" )");


                    OleDbCommand cmd1 = new OleDbCommand(sql.ToString(), conn);
                    cmd1.ExecuteNonQuery();



                    ////// Create a new sheet in the Excel spreadsheet.
                    #region "Prepare Insert SQL"
                    string sqlInsert = "INSERT INTO [Sheet1$]{0} values {1}";
                    string sqlInsertField = "(EIN,INo,[Account No],Employee";
                    string sqlInsertValue = "(?,?,?,?";
                    for (int i = 0; i < list.Count; i++)
                    {
                        // Skip first project as it contains "All Project" case 
                        // if (i != 0)
                        {
                            sqlInsertField += ",[" + list[i].Title.Trim() + "]";
                            sqlInsertValue += ",?";
                        }
                    }
                    sqlInsertField += ")";
                    sqlInsertValue += ")";
                    sqlInsert = string.Format(sqlInsert, sqlInsertField, sqlInsertValue);



                    OleDbCommand cmd = new OleDbCommand(sqlInsert, conn);
                    #endregion

                    #region "Add Parameters"
                    cmd.Parameters.Add("EIN", OleDbType.Integer, 4);
                    cmd.Parameters.Add("INo", OleDbType.VarChar, 50);
                    cmd.Parameters.Add("[Account No]", OleDbType.VarChar, 100);
                    cmd.Parameters.Add("Employee", OleDbType.VarChar, 200);

                    for (int i = 0; i < list.Count; i++)
                    {
                        // Skip first project as it contains "All Project" case 
                        //if (i != 0)
                        {
                            cmd.Parameters.Add(list[i].Title, OleDbType.Currency, 0);
                        }
                    }
                    #endregion



                    decimal amount;
                    foreach (GetEmployeesForVariableIncomesResult row in employees)
                    {
                        cmd.Parameters[0].Value = row.EmployeeId;
                        cmd.Parameters[1].Value = row.IdCardNo == null ? "" : row.IdCardNo;
                        cmd.Parameters[2].Value = row.AccountNO == null ? "" : row.AccountNO;
                        cmd.Parameters[3].Value = row.Name;
                        int position = 4;
                        for (int i = 0; i < list.Count; i++)
                        {
                            // Skip first project as it contains "All Project" case 
                            //if (i != 0)
                            {
                                amount = GetEmployeeVariableAmount(row.EmployeeId, list[i], projectPays); ;
                                cmd.Parameters[position].Value = amount;
                                position += 1;
                            }
                        }

                        cmd.ExecuteNonQuery();
                    }
                }
                ExcelGenerator.WriteFile(copyPath, Path.GetFileName(excelTemplatePath));
            }
            finally
            {
                try
                {
                    File.Delete(copyPath);
                }
                catch
                {
                }
            }
        }




        public static void WriteVariableTargetsForExportExcel(string excelTemplatePath, List<AppraisalTarget> _AppraisalTarget,
       List<EEmployee> employees, List<AppraisalEmployeeTarget> _AppraisalEmployeeTarget)
        {
            string path = HttpContext.Current.Server.MapPath(excelTemplatePath);
            string copyPath = Path.GetDirectoryName(path) + "\\" + Guid.NewGuid() + ".xlsx";
            File.Copy(path, copyPath);

            string excelConnection = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + copyPath + ";Extended Properties='Excel 12.0 Xml;HDR=Yes'";

            try
            {

                using (OleDbConnection conn = new OleDbConnection(excelConnection))
                {
                    // Open the connection.
                    conn.Open();

                    StringBuilder sql = new StringBuilder("Create Table [Sheet1$]([EIN] int,[Employee] varchar(200),[Level] varchar(200),[Designation] varchar(200),[Branch] varchar(200),[Department] varchar(200)");
                    for (int i = 0; i < _AppraisalTarget.Count; i++)
                    {
                        // Skip first project as it contains "All Project" case 
                        //if (i != 0)
                        {
                            sql.AppendFormat(", {0} money", "[" + _AppraisalTarget[i].Name.Trim() + "]");
                        }
                    }
                    sql.Append(" )");


                    OleDbCommand cmd1 = new OleDbCommand(sql.ToString(), conn);
                    cmd1.ExecuteNonQuery();



                    ////// Create a new sheet in the Excel spreadsheet.
                    #region "Prepare Insert SQL"
                    string sqlInsert = "INSERT INTO [Sheet1$]{0} values {1}";
                    string sqlInsertField = "([EIN],[Employee],[Level],[Designation],[Branch],[Department]";
                    string sqlInsertValue = "(?,?,?,?,?,?";
                    for (int i = 0; i < _AppraisalTarget.Count; i++)
                    {
                        // Skip first project as it contains "All Project" case 
                        // if (i != 0)
                        {
                            sqlInsertField += ",[" + _AppraisalTarget[i].Name.Trim() + "]";
                            sqlInsertValue += ",?";
                        }
                    }
                    sqlInsertField += ")";
                    sqlInsertValue += ")";
                    sqlInsert = string.Format(sqlInsert, sqlInsertField, sqlInsertValue);



                    OleDbCommand cmd = new OleDbCommand(sqlInsert, conn);
                    #endregion

                    #region "Add Parameters"
                    cmd.Parameters.Add("[EIN]", OleDbType.Integer, 4);
                    cmd.Parameters.Add("[Employee]", OleDbType.VarChar, 200);
                    cmd.Parameters.Add("[Level]", OleDbType.VarChar, 200);
                    cmd.Parameters.Add("[Designation]", OleDbType.VarChar, 200);
                    cmd.Parameters.Add("[Branch]", OleDbType.VarChar, 200);
                    cmd.Parameters.Add("[Department]", OleDbType.VarChar, 200);

                    for (int i = 0; i < _AppraisalTarget.Count; i++)
                    {
                        // Skip first project as it contains "All Project" case 
                        //if (i != 0)
                        {
                            cmd.Parameters.Add(_AppraisalTarget[i].Name, OleDbType.Currency, 0);
                        }
                    }
                    #endregion



                    decimal amount;
                    foreach (EEmployee row in employees)
                    {



                        cmd.Parameters[0].Value = row.EmployeeId;
                        cmd.Parameters[1].Value = row.Name;
                        BLevel _BLevel1 = NewPayrollManager.GetLevelById(row.EDesignation.LevelId.Value);
                        if (_BLevel1 != null)
                        {
                            cmd.Parameters[2].Value = _BLevel1.Name;
                        }
                        cmd.Parameters[3].Value = CommonManager.GetDesigId(row.DesignationId.Value).Name;


                        cmd.Parameters[4].Value = row.Branch.Name;
                        cmd.Parameters[5].Value = row.Department.Name;
                        int position = 6;
                        for (int i = 0; i < _AppraisalTarget.Count; i++)
                        {


                            amount = GetEmployeeAppraisalVariableAmount(row.EmployeeId, _AppraisalTarget[i], _AppraisalEmployeeTarget); ;
                            cmd.Parameters[position].Value = amount;
                            position += 1;

                        }

                        cmd.ExecuteNonQuery();
                    }
                }
                ExcelGenerator.WriteFile(copyPath, Path.GetFileName(excelTemplatePath));
            }
            finally
            {
                try
                {
                    File.Delete(copyPath);
                }
                catch
                {
                }
            }
        }



        public static void WriteDefinedIncomeForExportExcel(string excelTemplatePath, List<TextValue> xlist,
          List<TextValue> ylist, List<PIncomeDefinedAmount> values)
        {
            string path = HttpContext.Current.Server.MapPath(excelTemplatePath);
            string copyPath = Path.GetDirectoryName(path) + "\\" + Guid.NewGuid() + ".xlsx";
            File.Copy(path, copyPath);

            string excelConnection = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + copyPath + ";Extended Properties='Excel 12.0 Xml;HDR=Yes'";

            try
            {

                using (OleDbConnection conn = new OleDbConnection(excelConnection))
                {
                    // Open the connection.
                    conn.Open();

                    StringBuilder sql = new StringBuilder("Create Table [Sheet1$]( Name varchar(100) ");
                    for (int i = 0; i < xlist.Count; i++)
                    {
                        // Skip first project as it contains "All Project" case 
                        //if (i != 0)
                        {
                            sql.AppendFormat(", {0} money", "[" + xlist[i].Text.Trim() + "]");
                        }
                    }
                    sql.Append(" )");


                    OleDbCommand cmd1 = new OleDbCommand(sql.ToString(), conn);
                    cmd1.ExecuteNonQuery();



                    ////// Create a new sheet in the Excel spreadsheet.
                    #region "Prepare Insert SQL"
                    string sqlInsert = "INSERT INTO [Sheet1$]{0} values {1}";
                    string sqlInsertField = "(Name";
                    string sqlInsertValue = "(?";
                    for (int i = 0; i < xlist.Count; i++)
                    {
                        // Skip first project as it contains "All Project" case 
                        // if (i != 0)
                        {
                            sqlInsertField += ",[" + xlist[i].Text.Trim() + "]";
                            sqlInsertValue += ",?";
                        }
                    }
                    sqlInsertField += ")";
                    sqlInsertValue += ")";
                    sqlInsert = string.Format(sqlInsert, sqlInsertField, sqlInsertValue);



                    OleDbCommand cmd = new OleDbCommand(sqlInsert, conn);
                    #endregion

                    #region "Add Parameters"
                    cmd.Parameters.Add("Name", OleDbType.VarChar, 100);

                    for (int i = 0; i < xlist.Count; i++)
                    {
                        // Skip first project as it contains "All Project" case 
                        //if (i != 0)
                        {
                            cmd.Parameters.Add(xlist[i].Text, OleDbType.Currency, 0);
                        }
                    }
                    #endregion



                    decimal amount;
                    foreach (TextValue row in ylist)
                    {
                        cmd.Parameters[0].Value = row.Text;
                        int position = 1;
                        for (int i = 0; i < xlist.Count; i++)
                        {
                            // Skip first project as it contains "All Project" case 
                            //if (i != 0)
                            {
                                amount = GetEmployeeDefinedAmount(int.Parse(xlist[i].Value), int.Parse(row.Value), values); ;
                                cmd.Parameters[position].Value = amount;
                                position += 1;
                            }
                        }

                        cmd.ExecuteNonQuery();
                    }
                }
                ExcelGenerator.WriteFile(copyPath, Path.GetFileName(excelTemplatePath));
            }
            finally
            {
                try
                {
                    File.Delete(copyPath);
                }
                catch
                {
                }
            }
        }


        public static void WriteTravelAllowanceForExportExcel(string excelTemplatePath, List<TAAllowanceLocation> locationList,
          List<BLevel> levelList, List<TAAllowanceRate> values, int allowanceId)
        {
            string path = HttpContext.Current.Server.MapPath(excelTemplatePath);
            string copyPath = Path.GetDirectoryName(path) + "\\" + Guid.NewGuid() + ".xlsx";
            File.Copy(path, copyPath);

            string excelConnection = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + copyPath + ";Extended Properties='Excel 12.0 Xml;HDR=Yes'";

            try
            {

                using (OleDbConnection conn = new OleDbConnection(excelConnection))
                {
                    // Open the connection.
                    conn.Open();

                    StringBuilder sql = new StringBuilder("Create Table [Sheet1$]( [Level] varchar(100) ");
                    for (int i = 0; i < locationList.Count; i++)
                    {
                        // Skip first project as it contains "All Project" case 
                        //if (i != 0)
                        {
                            sql.AppendFormat(", {0} money", "[" + locationList[i].LocationName.Trim() + "]");
                        }
                    }
                    sql.Append(" )");


                    OleDbCommand cmd1 = new OleDbCommand(sql.ToString(), conn);
                    cmd1.ExecuteNonQuery();



                    ////// Create a new sheet in the Excel spreadsheet.
                    #region "Prepare Insert SQL"
                    string sqlInsert = "INSERT INTO [Sheet1$]{0} values {1}";
                    string sqlInsertField = "([Level]";
                    string sqlInsertValue = "(?";
                    for (int i = 0; i < locationList.Count; i++)
                    {
                        // Skip first project as it contains "All Project" case 
                        // if (i != 0)
                        {
                            sqlInsertField += ",[" + locationList[i].LocationName.Trim() + "]";
                            sqlInsertValue += ",?";
                        }
                    }
                    sqlInsertField += ")";
                    sqlInsertValue += ")";
                    sqlInsert = string.Format(sqlInsert, sqlInsertField, sqlInsertValue);



                    OleDbCommand cmd = new OleDbCommand(sqlInsert, conn);
                    #endregion

                    #region "Add Parameters"
                    cmd.Parameters.Add("Level", OleDbType.VarChar, 100);

                    for (int i = 0; i < locationList.Count; i++)
                    {
                        // Skip first project as it contains "All Project" case 
                        //if (i != 0)
                        {
                            cmd.Parameters.Add(locationList[i].LocationName, OleDbType.Currency, 0);
                        }
                    }
                    #endregion



                    decimal amount;
                    foreach (BLevel row in levelList)
                    {
                        cmd.Parameters[0].Value = row.GroupLevel;
                        int position = 1;
                        for (int i = 0; i < locationList.Count; i++)
                        {
                            // Skip first project as it contains "All Project" case 
                            //if (i != 0)
                            {
                                amount = GetTAAllowanceAmount(locationList[i].LocationId, row.LevelId, allowanceId, values); ;
                                cmd.Parameters[position].Value = amount;
                                position += 1;
                            }
                        }

                        cmd.ExecuteNonQuery();
                    }
                }
                ExcelGenerator.WriteFile(copyPath, Path.GetFileName(excelTemplatePath));
            }
            finally
            {
                try
                {
                    File.Delete(copyPath);
                }
                catch
                {
                }
            }
        }
        public static void WriteDefinedXSingleColumnIncomeForExportExcel(string excelTemplatePath, List<TextValue> xlist,
          List<PIncomeDefinedAmount> values, string columnName)
        {
            string path = HttpContext.Current.Server.MapPath(excelTemplatePath);
            string copyPath = Path.GetDirectoryName(path) + "\\" + Guid.NewGuid() + ".xlsx";
            File.Copy(path, copyPath);

            string excelConnection = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + copyPath + ";Extended Properties='Excel 12.0 Xml;HDR=Yes'";

            try
            {

                using (OleDbConnection conn = new OleDbConnection(excelConnection))
                {
                    // Open the connection.
                    conn.Open();

                    StringBuilder sql = new StringBuilder("Create Table [Sheet1$]( [" + columnName + "] varchar(100),Amount money) ");



                    OleDbCommand cmd1 = new OleDbCommand(sql.ToString(), conn);
                    cmd1.ExecuteNonQuery();



                    ////// Create a new sheet in the Excel spreadsheet.
                    #region "Prepare Insert SQL"
                    string sqlInsert = "INSERT INTO [Sheet1$]([" + columnName + "],Amount) values (?,?)";


                    OleDbCommand cmd = new OleDbCommand(sqlInsert, conn);
                    #endregion

                    #region "Add Parameters"
                    cmd.Parameters.Add("[" + columnName + "]", OleDbType.VarChar, 100);
                    cmd.Parameters.Add("Amount", OleDbType.Currency, 0);


                    #endregion


                    decimal amount;
                    int rowIndex = 0;
                    foreach (TextValue row in xlist)
                    {
                        cmd.Parameters[0].Value = row.Text;
                        amount = GetEmployeeDefinedAmount(int.Parse(xlist[rowIndex].Value), 0, values); ;
                        cmd.Parameters[1].Value = amount;

                        cmd.ExecuteNonQuery();

                        rowIndex += 1;
                    }
                }
                ExcelGenerator.WriteFile(copyPath, Path.GetFileName(excelTemplatePath));
            }
            finally
            {
                try
                {
                    File.Delete(copyPath);
                }
                catch
                {
                }
            }
        }

        public static void WriteAddOnIncomeForExportExcel(string excelTemplatePath, List<CalcGetHeaderListResult> list,
        List<CalcGetCalculationListResult> employees, List<PartialPaidDetail> amountList)
        {
            string path = HttpContext.Current.Server.MapPath(excelTemplatePath);
            string copyPath = Path.GetDirectoryName(path) + "\\" + Guid.NewGuid() + ".xlsx";
            File.Copy(path, copyPath);

            string excelConnection = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + copyPath + ";Extended Properties='Excel 12.0 Xml;HDR=Yes'";

            try
            {

                using (OleDbConnection conn = new OleDbConnection(excelConnection))
                {
                    // Open the connection.
                    conn.Open();

                    StringBuilder sql = new StringBuilder("Create Table [Sheet1$]( EIN int, [I No] varchar(60),[Account No] varchar(100), Employee varchar(100) ");
                    for (int i = 0; i < list.Count; i++)
                    {
                        // Skip first project as it contains "All Project" case 
                        //if (i != 0)
                        {
                            sql.AppendFormat(", {0} money", "[" + list[i].HeaderName.Trim() + "]");
                        }
                    }
                    sql.Append(" )");


                    OleDbCommand cmd1 = new OleDbCommand(sql.ToString(), conn);
                    cmd1.ExecuteNonQuery();



                    ////// Create a new sheet in the Excel spreadsheet.
                    #region "Prepare Insert SQL"
                    string sqlInsert = "INSERT INTO [Sheet1$]{0} values {1}";
                    string sqlInsertField = "(EIN,[I No],[Account No],Employee";
                    string sqlInsertValue = "(?,?,?,?";
                    for (int i = 0; i < list.Count; i++)
                    {
                        // Skip first project as it contains "All Project" case 
                        // if (i != 0)
                        {
                            sqlInsertField += ",[" + list[i].HeaderName.Trim() + "]";
                            sqlInsertValue += ",?";
                        }
                    }
                    sqlInsertField += ")";
                    sqlInsertValue += ")";
                    sqlInsert = string.Format(sqlInsert, sqlInsertField, sqlInsertValue);



                    OleDbCommand cmd = new OleDbCommand(sqlInsert, conn);
                    #endregion

                    #region "Add Parameters"
                    cmd.Parameters.Add("EIN", OleDbType.Integer, 4);
                    cmd.Parameters.Add("[I No]", OleDbType.VarChar, 50);
                    cmd.Parameters.Add("[Account No]", OleDbType.VarChar, 100);
                    cmd.Parameters.Add("Employee", OleDbType.VarChar, 50);

                    for (int i = 0; i < list.Count; i++)
                    {
                        // Skip first project as it contains "All Project" case 
                        //if (i != 0)
                        {
                            cmd.Parameters.Add(list[i].HeaderName, OleDbType.Currency, 0);
                            cmd.Parameters[list[i].HeaderName].IsNullable = true;
                        }
                    }
                    #endregion



                    decimal? amount;
                    foreach (CalcGetCalculationListResult row in employees)
                    {
                        cmd.Parameters[0].Value = row.EmployeeId;
                        cmd.Parameters[1].Value = row.IDCardNo == null ? "" : row.IDCardNo;
                        cmd.Parameters[2].Value = row.BankACNo == null ? "" : row.BankACNo;
                        cmd.Parameters[3].Value = row.Name;
                        int position = 4;
                        for (int i = 0; i < list.Count; i++)
                        {
                            // Skip first project as it contains "All Project" case 
                            //if (i != 0)
                            {
                                amount = GetEmployeeAmount(row.EmployeeId.Value, list[i], amountList); ;
                                if (amount == null)
                                    cmd.Parameters[position].Value = DBNull.Value;
                                else
                                    cmd.Parameters[position].Value = amount;
                                position += 1;
                            }
                        }

                        cmd.ExecuteNonQuery();
                    }
                }
                ExcelGenerator.WriteFile(copyPath, Path.GetFileName(excelTemplatePath));
            }
            finally
            {
                try
                {
                    File.Delete(copyPath);
                }
                catch
                {
                }
            }
        }

        public static void WriteNewAddOnIncomeForExportExcel(string excelTemplatePath, List<CalcGetHeaderListResult> list,
        List<CalcGetCalculationListResult> employees, List<AddOnDetail> amountList)
        {
            string path = HttpContext.Current.Server.MapPath(excelTemplatePath);
            string copyPath = Path.GetDirectoryName(path) + "\\" + Guid.NewGuid() + ".xlsx";
            File.Copy(path, copyPath);

            string excelConnection = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + copyPath + ";Extended Properties='Excel 12.0 Xml;HDR=Yes'";

            try
            {

                using (OleDbConnection conn = new OleDbConnection(excelConnection))
                {
                    // Open the connection.
                    conn.Open();

                    StringBuilder sql = new StringBuilder("Create Table [Sheet1$]( EIN int, [I No] varchar(60),[Account No] varchar(100), Employee varchar(100) ");
                    for (int i = 0; i < list.Count; i++)
                    {
                        // Skip first project as it contains "All Project" case 
                        //if (i != 0)
                        {
                            sql.AppendFormat(", {0} money", "[" + list[i].HeaderName.Trim() + "]");
                        }
                    }
                    sql.Append(" )");


                    OleDbCommand cmd1 = new OleDbCommand(sql.ToString(), conn);
                    cmd1.ExecuteNonQuery();



                    ////// Create a new sheet in the Excel spreadsheet.
                    #region "Prepare Insert SQL"
                    string sqlInsert = "INSERT INTO [Sheet1$]{0} values {1}";
                    string sqlInsertField = "(EIN,[I No],[Account No],Employee";
                    string sqlInsertValue = "(?,?,?,?";
                    for (int i = 0; i < list.Count; i++)
                    {
                        // Skip first project as it contains "All Project" case 
                        // if (i != 0)
                        {
                            sqlInsertField += ",[" + list[i].HeaderName.Trim() + "]";
                            sqlInsertValue += ",?";
                        }
                    }
                    sqlInsertField += ")";
                    sqlInsertValue += ")";
                    sqlInsert = string.Format(sqlInsert, sqlInsertField, sqlInsertValue);



                    OleDbCommand cmd = new OleDbCommand(sqlInsert, conn);
                    #endregion

                    #region "Add Parameters"
                    cmd.Parameters.Add("EIN", OleDbType.Integer, 4);
                    cmd.Parameters.Add("[I No]", OleDbType.VarChar, 50);
                    cmd.Parameters.Add("[Account No]", OleDbType.VarChar, 100);
                    cmd.Parameters.Add("Employee", OleDbType.VarChar, 50);

                    for (int i = 0; i < list.Count; i++)
                    {
                        // Skip first project as it contains "All Project" case 
                        //if (i != 0)
                        {
                            cmd.Parameters.Add(list[i].HeaderName, OleDbType.Currency, 0);
                            cmd.Parameters[list[i].HeaderName].IsNullable = true;
                        }
                    }
                    #endregion



                    decimal? amount;
                    foreach (CalcGetCalculationListResult row in employees)
                    {
                        cmd.Parameters[0].Value = row.EmployeeId;
                        cmd.Parameters[1].Value = row.IDCardNo == null ? "" : row.IDCardNo;
                        cmd.Parameters[2].Value = row.BankACNo == null ? "" : row.BankACNo;
                        cmd.Parameters[3].Value = row.Name;
                        int position = 4;
                        for (int i = 0; i < list.Count; i++)
                        {
                            // Skip first project as it contains "All Project" case 
                            //if (i != 0)
                            {
                                amount = GetEmployeeAmount(row.EmployeeId.Value, list[i], amountList); ;
                                if (amount == null)
                                    cmd.Parameters[position].Value = DBNull.Value;
                                else
                                    cmd.Parameters[position].Value = amount;
                                position += 1;
                            }
                        }

                        cmd.ExecuteNonQuery();
                    }
                }
                ExcelGenerator.WriteFile(copyPath, Path.GetFileName(excelTemplatePath));
            }
            finally
            {
                try
                {
                    File.Delete(copyPath);
                }
                catch
                {
                }
            }
        }

        public static void WriteProjectAndExportExcel(string excelTemplatePath, List<Project> list,
            List<EEmployee> employees, List<ProjectPayBO> projectPays)
        {
            string path = HttpContext.Current.Server.MapPath(excelTemplatePath);
            string copyPath = Path.GetDirectoryName(path) + "\\" + Guid.NewGuid() + ".xlsx";
            File.Copy(path, copyPath);

            string excelConnection = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + copyPath + ";Extended Properties='Excel 12.0 Xml;HDR=Yes'";

            try
            {

                using (OleDbConnection conn = new OleDbConnection(excelConnection))
                {
                    // Open the connection.
                    conn.Open();

                    StringBuilder sql = new StringBuilder("Create Table [Sheet1$]( EIN int, Employee varchar(50) ");
                    for (int i = 0; i < list.Count; i++)
                    {
                        // Skip first project as it contains "All Project" case 
                        if (i != 0)
                        {
                            sql.AppendFormat(", {0} money", "[" + list[i].Name.Trim() + "]");
                        }
                    }
                    sql.Append(" )");


                    OleDbCommand cmd1 = new OleDbCommand(sql.ToString(), conn);
                    cmd1.ExecuteNonQuery();



                    ////// Create a new sheet in the Excel spreadsheet.
                    #region "Prepare Insert SQL"
                    string sqlInsert = "INSERT INTO [Sheet1$]{0} values {1}";
                    string sqlInsertField = "(EIN,Employee";
                    string sqlInsertValue = "(?,?";
                    for (int i = 0; i < list.Count; i++)
                    {
                        // Skip first project as it contains "All Project" case 
                        if (i != 0)
                        {
                            sqlInsertField += ",[" + list[i].Name.Trim() + "]";
                            sqlInsertValue += ",?";
                        }
                    }
                    sqlInsertField += ")";
                    sqlInsertValue += ")";
                    sqlInsert = string.Format(sqlInsert, sqlInsertField, sqlInsertValue);



                    OleDbCommand cmd = new OleDbCommand(sqlInsert, conn);
                    #endregion

                    #region "Add Parameters"
                    cmd.Parameters.Add("EIN", OleDbType.Integer, 4);
                    cmd.Parameters.Add("Employee", OleDbType.VarChar, 50);

                    for (int i = 0; i < list.Count; i++)
                    {
                        // Skip first project as it contains "All Project" case 
                        if (i != 0)
                        {
                            cmd.Parameters.Add(list[i].Name, OleDbType.Currency, 0);
                        }
                    }
                    #endregion




                    foreach (EEmployee row in employees)
                    {
                        cmd.Parameters[0].Value = row.EmployeeId;
                        cmd.Parameters[1].Value = row.Name;
                        int position = 2;
                        for (int i = 0; i < list.Count; i++)
                        {
                            // Skip first project as it contains "All Project" case 
                            if (i != 0)
                            {
                                cmd.Parameters[position].Value = GetEmployeeProjectAmount(row.EmployeeId, list[i], projectPays);
                                position += 1;
                            }
                        }

                        cmd.ExecuteNonQuery();
                    }
                }
                ExcelGenerator.WriteFile(copyPath, Path.GetFileName(excelTemplatePath));
            }
            finally
            {
                try
                {
                    File.Delete(copyPath);
                }
                catch
                {
                }
            }
        }


        public static void WriteProjectActivityAndExportExcel(string excelTemplatePath, List<Project> list,
            List<EEmployee> employees)
        {
            string path = HttpContext.Current.Server.MapPath(excelTemplatePath);
            string copyPath = Path.GetDirectoryName(path) + "\\" + Guid.NewGuid() + ".xlsx";
            File.Copy(path, copyPath);

            string excelConnection = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + copyPath + ";Extended Properties='Excel 12.0 Xml;HDR=Yes'";

            try
            {

                using (OleDbConnection conn = new OleDbConnection(excelConnection))
                {
                    // Open the connection.
                    conn.Open();

                    StringBuilder sql = new StringBuilder("Create Table [Sheet1$]( EIN int, Employee varchar(200) ");
                    for (int i = 0; i < list.Count; i++)
                    {

                        sql.AppendFormat(", {0} text ", "[" + list[i].Name.Trim() + "]");

                    }
                    sql.Append(" )");


                    OleDbCommand cmd1 = new OleDbCommand(sql.ToString(), conn);
                    cmd1.ExecuteNonQuery();
                    cmd1.Dispose();


                    ////// Create a new sheet in the Excel spreadsheet.
                    #region "Prepare Insert SQL"
                    string sqlInsert = "INSERT INTO [Sheet1$]{0} values {1}";
                    string sqlInsertField = "(EIN,Employee";
                    string sqlInsertValue = "(?,?";
                    for (int i = 0; i < list.Count; i++)
                    {
                        // Skip first project as it contains "All Project" case 

                        sqlInsertField += ",[" + list[i].Name.Trim() + "]";
                        sqlInsertValue += ",?";

                    }
                    sqlInsertField += ")";
                    sqlInsertValue += ")";
                    sqlInsert = string.Format(sqlInsert, sqlInsertField, sqlInsertValue);



                    OleDbCommand cmd = new OleDbCommand(sqlInsert, conn);
                    #endregion

                    #region "Add Parameters"
                    cmd.Parameters.Add("EIN", OleDbType.Integer, 4);
                    cmd.Parameters.Add("Employee", OleDbType.VarChar, 200);

                    for (int i = 0; i < list.Count; i++)
                    {
                        // Skip first project as it contains "All Project" case 
                        cmd.Parameters.Add(list[i].Name, OleDbType.VarChar, 300);
                    }

                    #endregion




                    foreach (EEmployee row in employees)
                    {
                        cmd.Parameters[0].Value = row.EmployeeId;
                        cmd.Parameters[1].Value = row.Name;
                        int position = 2;
                        for (int i = 0; i < list.Count; i++)
                        {
                            // Skip first project as it contains "All Project" case 
                            double Activity = ProjectManager.GetProjectActivityByEmployeeID(row.EmployeeId, list[i].ProjectId);
                            cmd.Parameters[position].Value = Activity.ToString();//GetEmployeeProjectAmount(row.EmployeeId, list[i], projectPays);
                            position += 1;

                        }

                        cmd.ExecuteNonQuery();
                    }
                    cmd.Dispose();
                }
                ExcelGenerator.WriteFile(copyPath, Path.GetFileName(excelTemplatePath));
            }
            finally
            {
                try
                {
                    File.Delete(copyPath);
                }
                catch
                {
                }
            }
        }


        public static void WriteProjectRateAndExportExcel(string excelTemplatePath, List<Project> list,
            List<EEmployee> employees)
        {
            string path = HttpContext.Current.Server.MapPath(excelTemplatePath);
            string copyPath = Path.GetDirectoryName(path) + "\\" + Guid.NewGuid() + ".xlsx";
            File.Copy(path, copyPath);

            string excelConnection = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + copyPath + ";Extended Properties='Excel 12.0 Xml;HDR=Yes'";

            try
            {

                using (OleDbConnection conn = new OleDbConnection(excelConnection))
                {
                    // Open the connection.
                    conn.Open();

                    StringBuilder sql = new StringBuilder("Create Table [Sheet1$]( EIN int, Employee varchar(50) ");
                    for (int i = 0; i < list.Count; i++)
                    {

                        sql.AppendFormat(", {0} money", "[" + list[i].Name.Trim() + "]");

                    }
                    sql.Append(" )");


                    OleDbCommand cmd1 = new OleDbCommand(sql.ToString(), conn);
                    cmd1.ExecuteNonQuery();

                    cmd1.Dispose();

                    ////// Create a new sheet in the Excel spreadsheet.
                    #region "Prepare Insert SQL"
                    string sqlInsert = "INSERT INTO [Sheet1$]{0} values {1}";
                    string sqlInsertField = "(EIN,Employee";
                    string sqlInsertValue = "(?,?";
                    for (int i = 0; i < list.Count; i++)
                    {
                        // Skip first project as it contains "All Project" case 

                        sqlInsertField += ",[" + list[i].Name.Trim() + "]";
                        sqlInsertValue += ",?";

                    }
                    sqlInsertField += ")";
                    sqlInsertValue += ")";
                    sqlInsert = string.Format(sqlInsert, sqlInsertField, sqlInsertValue);



                    OleDbCommand cmd = new OleDbCommand(sqlInsert, conn);
                    #endregion

                    #region "Add Parameters"
                    cmd.Parameters.Add("EIN", OleDbType.Integer, 4);
                    cmd.Parameters.Add("Employee", OleDbType.VarChar, 50);

                    for (int i = 0; i < list.Count; i++)
                    {
                        // Skip first project as it contains "All Project" case 
                        cmd.Parameters.Add(list[i].Name, OleDbType.Double);

                    }

                    #endregion




                    foreach (EEmployee row in employees)
                    {
                        cmd.Parameters[0].Value = row.EmployeeId;
                        cmd.Parameters[1].Value = row.Name;
                        int position = 2;
                        for (int i = 0; i < list.Count; i++)
                        {
                            // Skip first project as it contains "All Project" case 
                            double Rate = ProjectManager.GetProjectRateByEmployeeID(row.EmployeeId, list[i].ProjectId);
                            cmd.Parameters[position].Value = Rate;//GetEmployeeProjectAmount(row.EmployeeId, list[i], projectPays);
                            position += 1;

                        }

                        cmd.ExecuteNonQuery();
                    }

                    cmd.Dispose();
                }
                ExcelGenerator.WriteFile(copyPath, Path.GetFileName(excelTemplatePath));
            }
            finally
            {
                try
                {
                    File.Delete(copyPath);
                }
                catch
                {
                }
            }
        }


        public static decimal GetEmployeeProjectAmount(int employeeId, Project project, List<ProjectPayBO> empProjectList)
        {
            foreach (ProjectPayBO bo in empProjectList)
            {
                if (bo.EmployeeId == employeeId && bo.ProjectId == project.ProjectId)
                {
                    if (bo.HoursOrDays == null)
                        return 0;
                    return (decimal)bo.HoursOrDays.Value;
                }
            }
            return 0;
        }

        public static decimal? GetEmployeeAmount(int employeeId, CalcGetHeaderListResult header, List<PartialPaidDetail> empProjectList)
        {
            foreach (PartialPaidDetail bo in empProjectList)
            {
                if (bo.EmployeeId == employeeId && bo.PartialPaidHeader.Type == header.Type && bo.PartialPaidHeader.SourceId == header.SourceId)
                {
                    if (bo.Amount == 0)
                        return null;
                    return bo.Amount;
                }
            }
            return null;
        }

        public static decimal? GetEmployeeAmount(int employeeId, CalcGetHeaderListResult header, List<AddOnDetail> empProjectList)
        {
            foreach (AddOnDetail bo in empProjectList)
            {
                if (bo.EmployeeId == employeeId && bo.AddOnHeader.Type == header.Type && bo.AddOnHeader.SourceId == header.SourceId)
                {
                    if (bo.Amount == 0)
                        return null;
                    return bo.Amount;
                }
            }
            return null;
        }

        public static decimal GetEmployeeAppraisalVariableAmount(int employeeId, AppraisalTarget _AppraisalTarget, List<AppraisalEmployeeTarget> _AppraisalEmployeeTarget)
        {
            foreach (AppraisalEmployeeTarget bo in _AppraisalEmployeeTarget)
            {
                if (bo.EmployeeId == employeeId && bo.TargetID == _AppraisalTarget.TargetID)
                {
                    if (bo.Value == null)
                        return 0;
                    return (decimal)bo.Value;
                }
            }
            return 0;
        }



        public static string GetEmployeeVariablePFValue(int employeeId, PIncome project, List<GetEmployeesIncomeForPFVariableIncomesResult> empProjectList)
        {
            foreach (GetEmployeesIncomeForPFVariableIncomesResult bo in empProjectList)
            {
                if (bo.EmployeeId == employeeId && bo.IncomeId == project.IncomeId)
                {

                    return "Exclude";
                }
            }
            return "";
        }


        public static decimal GetEmployeeVariableAmount(int employeeId, PIncome project, List<GetEmployeesIncomeForVariableIncomesResult> empProjectList)
        {
            foreach (GetEmployeesIncomeForVariableIncomesResult bo in empProjectList)
            {
                if (bo.EmployeeId == employeeId && bo.IncomeId == project.IncomeId)
                {
                    if (bo.Amount == null)
                        return 0;
                    return (decimal)bo.Amount.Value;
                }
            }
            return 0;
        }
        public static decimal GetEmployeeDefinedAmount(int xtype, int ytype, List<PIncomeDefinedAmount> empProjectList)
        {
            foreach (PIncomeDefinedAmount bo in empProjectList)
            {
                if (xtype == bo.XType && ytype == bo.YType)
                {
                    if (bo.Amount == null)
                        return 0;
                    return (decimal)bo.Amount.Value;
                }
            }
            return 0;
        }
        public static decimal GetTAAllowanceAmount(int locationId, int levelId, int allowanceId, List<TAAllowanceRate> empProjectList)
        {
            foreach (TAAllowanceRate bo in empProjectList)
            {
                if (bo.LocationId == locationId && bo.LevelID == levelId && bo.AllowanceID == allowanceId)
                {
                    if (bo.Rate == null)
                        return 0;
                    return (decimal)bo.Rate.Value;
                }
            }
            return 0;
        }
        public static void WriteStepGradeExcel(string excelTemplatePath, List<Step> steps, List<Grade> grades, List<PositionGradeStepAmount> amounts)
        {
            string path = HttpContext.Current.Server.MapPath(excelTemplatePath);
            string copyPath = Path.GetDirectoryName(path) + "\\" + Guid.NewGuid() + ".xlsx";
            File.Copy(path, copyPath);

            Dictionary<string, decimal> amountList = new Dictionary<string, decimal>();
            foreach (PositionGradeStepAmount entity in amounts)
            {
                amountList[entity.GradeId + ":" + entity.StepId] = entity.Amount;
            }


            string excelConnection = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + copyPath + ";Extended Properties='Excel 12.0 Xml;HDR=Yes'";

            try
            {

                using (OleDbConnection conn = new OleDbConnection(excelConnection))
                {
                    // Open the connection.
                    conn.Open();

                    StringBuilder sql = new StringBuilder("Create Table [Sheet1$]([Step/Grade] varchar(50) ");
                    StringBuilder insertSql = new StringBuilder("insert into [Sheet1$]({0}) values({1})"); //insert command with default column for Step
                    StringBuilder insertSql1 = new StringBuilder("[Step/Grade]");
                    StringBuilder insertSql2 = new StringBuilder("?");

                    for (int i = 0; i < grades.Count; i++)
                    {

                        sql.AppendFormat(", {0} float", "[" + grades[i].Name + "]");

                        insertSql1.AppendFormat(", [{0}]", grades[i].Name);

                        insertSql2.AppendFormat(", ?", grades[i].Name);
                    }
                    sql.Append(" )");


                    OleDbCommand cmd1 = new OleDbCommand(sql.ToString(), conn);
                    cmd1.ExecuteNonQuery();




                    //// Create a new sheet in the Excel spreadsheet.
                    OleDbCommand cmdInsert = new OleDbCommand(string.Format(insertSql.ToString(), insertSql1.ToString(), insertSql2.ToString()), conn);

                    cmdInsert.Parameters.Add("[Step/Grade]", OleDbType.VarChar, 50);
                    foreach (Grade grade in grades)
                    {
                        cmdInsert.Parameters.Add("[" + grade.Name + "]", OleDbType.Double, 4);
                    }


                    //cmd.Parameters.Add("Step", OleDbType.VarChar, 50);
                    //cmd.Parameters[0].Value = "";
                    int columnIndex;
                    decimal value;
                    foreach (Step row in steps)
                    {
                        columnIndex = 0;
                        cmdInsert.Parameters[columnIndex++].Value = row.Name;

                        foreach (Grade grade in grades)
                        {

                            if (amountList.ContainsKey(grade.GradeId + ":" + row.StepId))
                                cmdInsert.Parameters[columnIndex++].Value = amountList[grade.GradeId + ":" + row.StepId];
                            else
                                cmdInsert.Parameters[columnIndex++].Value = 0;
                        }

                        cmdInsert.ExecuteNonQuery();
                    }
                }
                ExcelGenerator.WriteFile(copyPath, Path.GetFileName(excelTemplatePath));
            }
            finally
            {
                try
                {
                    File.Delete(copyPath);
                }
                catch
                {
                }
            }
        }


        public static void WriteEmployeeLeaveProjectExcel(string excelTemplatePath, string[] GetProjectList, List<GetLeaveProjectEmployeesResult> list)
        {
            string path = HttpContext.Current.Server.MapPath(excelTemplatePath);
            string copyPath = Path.GetDirectoryName(path) + "\\" + Guid.NewGuid() + ".xlsx";
            File.Copy(path, copyPath);


            string excelConnection = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + copyPath + ";Extended Properties='Excel 12.0 Xml;HDR=Yes'";

            try
            {

                using (OleDbConnection conn = new OleDbConnection(excelConnection))
                {
                    // Open the connection.
                    conn.Open();





                    // Create a new sheet in the Excel spreadsheet.
                    OleDbCommand cmd = new OleDbCommand("INSERT INTO [Sheet1$](EIN,Employee, [I No],[Department],[Designation],[Project],[Manager] ) values (?,?,?,?,?,?,?)", conn);


                    // Add the parameters.

                    cmd.Parameters.Add("EIN", OleDbType.Integer, 4);
                    cmd.Parameters.Add("Employee", OleDbType.VarChar, 100);

                    cmd.Parameters.Add("[I No]", OleDbType.VarChar, 100);

                    cmd.Parameters.Add("[Department]", OleDbType.VarChar, 100);
                    cmd.Parameters.Add("[Designation]", OleDbType.VarChar, 100);
                    cmd.Parameters.Add("[Project]", OleDbType.VarChar, 100);
                    cmd.Parameters.Add("[Manager]", OleDbType.VarChar, 100);


                    // Changes the Rowstate()of each DataRow to Added,
                    // so that OleDbDataAdapter will insert the rows.
                    foreach (GetLeaveProjectEmployeesResult row in list)
                    {
                        //dr.SetAdded();
                        cmd.Parameters[0].Value = row.EmployeeId;
                        cmd.Parameters[1].Value = row.Name;

                        cmd.Parameters[2].Value = row.IdCardNo == null ? "" : row.IdCardNo;
                        //cmd.Parameters[3].Value = GetValue(row.Days_At_Kirne);

                        cmd.Parameters[3].Value = row.DepartmentName;
                        cmd.Parameters[4].Value = row.Designation;

                        cmd.Parameters[5].Value = row.ProjectName == null ? "" : row.ProjectName;

                        cmd.Parameters[6].Value = row.Manager == null ? "" : row.Manager;



                        cmd.ExecuteNonQuery();
                    }


                    WriteIntoSheet(conn, "ProjectList", GetProjectList);
                }

                ExcelGenerator.WriteFile(copyPath, Path.GetFileName(excelTemplatePath));
            }
            finally
            {
                try
                {
                    File.Delete(copyPath);
                }
                catch
                {
                }
            }
        }

        public static void WriteEmployeeSupervisorExcel
            (string excelTemplatePath, List<GetEmpSpecificLeaveDefListResult> list)
        {
            string path = HttpContext.Current.Server.MapPath(excelTemplatePath);
            string copyPath = Path.GetDirectoryName(path) + "\\" + Guid.NewGuid() + ".xlsx";
            File.Copy(path, copyPath);


            string excelConnection = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + copyPath + ";Extended Properties='Excel 12.0 Xml;HDR=Yes'";

            try
            {

                using (OleDbConnection conn = new OleDbConnection(excelConnection))
                {
                    // Open the connection.
                    conn.Open();





                    // Create a new sheet in the Excel spreadsheet.
                    OleDbCommand cmd = new OleDbCommand("INSERT INTO [Sheet1$](EIN,Employee, [Recommender1 EIN],[Recommender1 Name],[Recommender2 EIN],[Recommender2 Name],[Approval1 EIN],[Approval1 Name],[Approval2 EIN],[Approval2 Name] ) values (?,?,?,?,?,?,?,?,?,?)", conn);


                    // Add the parameters.

                    cmd.Parameters.Add("EIN", OleDbType.Integer, 4);
                    cmd.Parameters.Add("Employee", OleDbType.VarChar, 200);

                    cmd.Parameters.Add("[Recommender1 EIN]", OleDbType.VarChar, 100);
                    cmd.Parameters.Add("[Recommender1 Name]", OleDbType.VarChar, 100);
                    cmd.Parameters.Add("[Recommender2 EIN]", OleDbType.VarChar, 100);
                    cmd.Parameters.Add("[Recommender2 Name]", OleDbType.VarChar, 100);
                    cmd.Parameters.Add("[Approval1 EIN]", OleDbType.VarChar, 100);
                    cmd.Parameters.Add("[Approval1 Name]", OleDbType.VarChar, 100);
                    cmd.Parameters.Add("[Approval2 EIN]", OleDbType.VarChar, 100);
                    cmd.Parameters.Add("[Approval2 Name]", OleDbType.VarChar, 100);

                    // Changes the Rowstate()of each DataRow to Added,
                    // so that OleDbDataAdapter will insert the rows.
                    foreach (GetEmpSpecificLeaveDefListResult row in list)
                    {
                        //dr.SetAdded();
                        cmd.Parameters[0].Value = row.EmployeeId;
                        cmd.Parameters[1].Value = row.Name;

                        cmd.Parameters[2].Value = row.R1EIN == null ? "" : row.R1EIN.ToString();
                        cmd.Parameters[3].Value = row.R1Name == null ? "" : row.R1Name;
                        cmd.Parameters[4].Value = row.R2EIN == null ? "" : row.R2EIN.ToString();
                        cmd.Parameters[5].Value = row.R2Name == null ? "" : row.R2Name;

                        cmd.Parameters[6].Value = row.A1EIN == null ? "" : row.A1EIN.ToString();
                        cmd.Parameters[7].Value = row.A1Name == null ? "" : row.A1Name;
                        cmd.Parameters[8].Value = row.A2EIN == null ? "" : row.A2EIN.ToString();
                        cmd.Parameters[9].Value = row.A2Name == null ? "" : row.A2Name;


                        cmd.ExecuteNonQuery();
                    }

                }

                ExcelGenerator.WriteFile(copyPath, Path.GetFileName(excelTemplatePath));
            }
            finally
            {
                try
                {
                    File.Delete(copyPath);
                }
                catch
                {
                }
            }
        }

        public static void WriteTeamExcel(string excelTemplatePath)
        {
            string path = HttpContext.Current.Server.MapPath(excelTemplatePath);
            string copyPath = Path.GetDirectoryName(path) + "\\" + Guid.NewGuid() + ".xlsx";
            File.Copy(path, copyPath);

            List<Branch> branchList = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId).OrderBy(x => x.Name).ToList();

            string excelConnection = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + copyPath + ";Extended Properties='Excel 12.0 Xml;HDR=Yes'";

            try
            {

                using (OleDbConnection conn = new OleDbConnection(excelConnection))
                {
                    // Open the connection.
                    conn.Open();



                    WriteIntoSheet(conn, "BranchList", branchList.Select(x => x.Name).ToArray());
                }

                ExcelGenerator.WriteFile(copyPath, Path.GetFileName(excelTemplatePath));
            }
            finally
            {
                try
                {
                    File.Delete(copyPath);
                }
                catch
                {
                }
            }
        }


        public static void WriteEmployeeManagersExcel(string excelTemplatePath, string[] GetProjectList, List<EEmployee> empList)
        {
            string path = HttpContext.Current.Server.MapPath(excelTemplatePath);
            string copyPath = Path.GetDirectoryName(path) + "\\" + Guid.NewGuid() + ".xlsx";
            File.Copy(path, copyPath);


            string excelConnection = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + copyPath + ";Extended Properties='Excel 12.0 Xml;HDR=Yes'";

            try
            {

                using (OleDbConnection conn = new OleDbConnection(excelConnection))
                {
                    // Open the connection.
                    conn.Open();

                    // Create a new sheet in the Excel spreadsheet.
                    OleDbCommand cmd = new OleDbCommand("INSERT INTO [Sheet1$](EIN,Employee,[Department],[Manager] ) values (?,?,?,?)", conn);


                    // Add the parameters.

                    cmd.Parameters.Add("EIN", OleDbType.Integer, 4);
                    cmd.Parameters.Add("Employee", OleDbType.VarChar, 100);


                    cmd.Parameters.Add("[Department]", OleDbType.VarChar, 100);
                    cmd.Parameters.Add("[Manager]", OleDbType.VarChar, 100);


                    // Changes the Rowstate()of each DataRow to Added,
                    // so that OleDbDataAdapter will insert the rows.
                    foreach (var row in empList)
                    {
                        //dr.SetAdded();
                        cmd.Parameters[0].Value = row.EmployeeId;
                        cmd.Parameters[1].Value = row.Name;

                        cmd.Parameters[2].Value = row.Department.Name;

                        cmd.Parameters[3].Value = row.LastName;//row.Manager == null ? "" : row.Manager;

                        cmd.ExecuteNonQuery();
                    }


                    WriteIntoSheet(conn, "ManagerList", GetProjectList);
                }

                ExcelGenerator.WriteFile(copyPath, Path.GetFileName(excelTemplatePath));
            }
            finally
            {
                try
                {
                    File.Delete(copyPath);
                }
                catch
                {
                }
            }
        }




        public static void WriteEmployeeLeaveRequestExcel(string excelTemplatePath, string[] employeeList, List<GetLeaveApprovalSettingListResult> list, List<GetEmployeesByBranchResult> empList)
        {
            string path = HttpContext.Current.Server.MapPath(excelTemplatePath);
            string copyPath = Path.GetDirectoryName(path) + "\\" + Guid.NewGuid() + ".xlsx";
            File.Copy(path, copyPath);


            string excelConnection = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + copyPath + ";Extended Properties='Excel 12.0 Xml;HDR=Yes'";

            try
            {

                using (OleDbConnection conn = new OleDbConnection(excelConnection))
                {
                    // Open the connection.
                    conn.Open();





                    // Create a new sheet in the Excel spreadsheet.
                    OleDbCommand cmd = new OleDbCommand("INSERT INTO [Sheet1$](Branch,Team, [Recommend 1],[Recommend 2],[Approval 1],[Approval 2] ) values (?,?,?,?,?,?)", conn);


                    // Add the parameters.

                    cmd.Parameters.Add("Branch", OleDbType.VarChar, 100);
                    cmd.Parameters.Add("Team", OleDbType.VarChar, 100);

                    cmd.Parameters.Add("[Recommend 1]", OleDbType.VarChar, 100);
                    cmd.Parameters[2].IsNullable = true;

                    cmd.Parameters.Add("[Recommend 2]", OleDbType.VarChar, 100);
                    cmd.Parameters[3].IsNullable = true;
                    cmd.Parameters.Add("[Approval 1]", OleDbType.VarChar, 100);
                    cmd.Parameters[4].IsNullable = true;
                    cmd.Parameters.Add("[Approval 2]", OleDbType.VarChar, 100);
                    cmd.Parameters[5].IsNullable = true;

                    // Changes the Rowstate()of each DataRow to Added,
                    // so that OleDbDataAdapter will insert the rows.
                    int i = 0;
                    foreach (GetLeaveApprovalSettingListResult row in list)
                    {
                        //if (++i > 1)
                        //    continue;

                        //dr.SetAdded();
                        cmd.Parameters[0].Value = row.Branch;
                        cmd.Parameters[1].Value = row.Project;

                        cmd.Parameters[2].Value = DBNull.Value;
                        cmd.Parameters[2].Value = row.CC4 == null ? "" : row.CC4;
                        //cmd.Parameters[3].Value = GetValue(row.Days_At_Kirne);
                        cmd.Parameters[3].Value = DBNull.Value;
                        cmd.Parameters[3].Value = row.CC5 == null ? "" : row.CC5;
                        cmd.Parameters[4].Value = DBNull.Value;
                        cmd.Parameters[4].Value = row.ApplyTo == null ? "" : row.ApplyTo;
                        cmd.Parameters[5].Value = DBNull.Value;
                        cmd.Parameters[5].Value = row.CC1 == null ? "" : row.CC1;



                        cmd.ExecuteNonQuery();
                    }


                    WriteIntoSheet(conn, "EmployeeList", employeeList);
                }

                ExcelGenerator.WriteFile(copyPath, Path.GetFileName(excelTemplatePath));
            }
            finally
            {
                try
                {
                    File.Delete(copyPath);
                }
                catch
                {
                }
            }
        }

        public static void OxfamDesiredContribution(string excelTemplatePath, List<DAL.GetEmployeeListForOxfamOptimumPFAndCITResult> list)
        {
            string path = HttpContext.Current.Server.MapPath(excelTemplatePath);
            string copyPath = Path.GetDirectoryName(path) + "\\" + Guid.NewGuid() + ".xlsx";
            File.Copy(path, copyPath);


            string excelConnection = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + copyPath + ";Extended Properties='Excel 12.0 Xml;HDR=Yes'";

            try
            {

                using (OleDbConnection conn = new OleDbConnection(excelConnection))
                {
                    // Open the connection.
                    conn.Open();


                    // Create a new sheet in the Excel spreadsheet.
                    StringBuilder sql = new StringBuilder("Create Table [Sheet1$]( EIN int,Employee varchar(100), [Max Allowed] currency,[Desired Contribution] currency)");

                    OleDbCommand cmd = new OleDbCommand(sql.ToString(), conn);
                    cmd.ExecuteNonQuery();


                    // Create a new sheet in the Excel spreadsheet.
                    cmd = new OleDbCommand("INSERT INTO [Sheet1$](EIN,Employee, [Max Allowed],[Desired Contribution]) values (?,?,?,?)", conn);


                    // Add the parameters.

                    cmd.Parameters.Add("EIN", OleDbType.Integer, 4);
                    cmd.Parameters.Add("Employee", OleDbType.VarChar, 50);




                    cmd.Parameters.Add("[Max Allowed", OleDbType.Currency, 0);
                    cmd.Parameters[2].IsNullable = true;


                    cmd.Parameters.Add("[Desired Contribution]", OleDbType.Currency, 0);
                    cmd.Parameters[3].IsNullable = true;

                    // Changes the Rowstate()of each DataRow to Added,
                    // so that OleDbDataAdapter will insert the rows.
                    int i = 0;
                    List<EFundDetail> dbList = PayrollDataContext.EFundDetails.ToList();

                    foreach (GetEmployeeListForOxfamOptimumPFAndCITResult row in list)
                    {
                        //dr.SetAdded();
                        cmd.Parameters[0].Value = row.EID;
                        cmd.Parameters[1].Value = row.Name;



                        cmd.Parameters[2].Value = DBNull.Value;
                        if (row.Allowed != null)
                            cmd.Parameters[2].Value = GetCurrency(row.Allowed.Value);
                        //    cmd.Parameters[4].Value = row.AdjustmentAmount;

                        EFundDetail fund = dbList.FirstOrDefault(x => x.EmployeeId == row.EID);

                        cmd.Parameters[3].Value = DBNull.Value;
                        if (fund.OxfamDesiredContribution != null)
                            cmd.Parameters[3].Value = GetCurrency(fund.OxfamDesiredContribution.Value);





                        cmd.ExecuteNonQuery();
                    }


                }

                ExcelGenerator.WriteFile(copyPath, Path.GetFileName(excelTemplatePath));
            }
            finally
            {
                try
                {
                    File.Delete(copyPath);
                }
                catch
                {
                }
            }
        }
        public static void ExportAdjustment(string excelTemplatePath, List<DAL.GetIncomeDeductionAdjustmentEmployeesResult> list, string headerName)
        {
            string path = HttpContext.Current.Server.MapPath(excelTemplatePath);
            string copyPath = Path.GetDirectoryName(path) + "\\" + Guid.NewGuid() + ".xlsx";
            File.Copy(path, copyPath);

            string excelConnection = string.Format(ExcelConnectionString, copyPath);



            using (OleDbConnection conn = new OleDbConnection(excelConnection))
            {
                // Open the connection.
                conn.Open();

                StringBuilder sql = new StringBuilder("Create Table [Sheet1$]( EIN int,Employee varchar(100), [Income/Deduction Head] varchar(100),[Previous Amount] money ,[Adjustment] money,[Adjusted Amount] money,[Note] varchar(200))");

                OleDbCommand cmd1 = new OleDbCommand(sql.ToString(), conn);
                cmd1.ExecuteNonQuery();



                // Create a new sheet in the Excel spreadsheet.
                OleDbCommand cmd = new OleDbCommand("INSERT INTO [Sheet1$](EIN,Employee, [Income/Deduction Head] ,[Previous Amount] ,[Adjustment] ,[Adjusted Amount] ,[Note] ) values (?,?,?,?,?,?,?)", conn);


                // Add the parameters.

                cmd.Parameters.Add("EIN", OleDbType.Integer, 4);
                cmd.Parameters.Add("Employee", OleDbType.VarChar, 50);


                cmd.Parameters.Add("Income/Deduction Head", OleDbType.VarChar, 50);


                cmd.Parameters.Add("[Previous Amount", OleDbType.Currency, 0);
                cmd.Parameters[3].IsNullable = true;


                cmd.Parameters.Add("[Adjustment]", OleDbType.Currency, 0);
                cmd.Parameters[4].IsNullable = true;


                cmd.Parameters.Add("Adjusted Amount", OleDbType.Currency, 0);
                cmd.Parameters[5].IsNullable = true;

                cmd.Parameters.Add("Note", OleDbType.VarChar, 200);


                // Changes the Rowstate()of each DataRow to Added,
                // so that OleDbDataAdapter will insert the rows.
                foreach (GetIncomeDeductionAdjustmentEmployeesResult row in list)
                {
                    //dr.SetAdded();
                    cmd.Parameters[0].Value = row.EmployeeId;
                    cmd.Parameters[1].Value = row.Name;

                    cmd.Parameters[2].Value = headerName;// == null ? DBNull.Value : row.Days_At_Palati;
                    //cmd.Parameters[3].Value = GetValue(row.Days_At_Kirne);

                    cmd.Parameters[3].Value = row.ExistingAmount;

                    cmd.Parameters[4].Value = DBNull.Value;
                    if (row.AdjustmentAmount != null)
                        cmd.Parameters[4].Value = row.AdjustmentAmount.Value;
                    //    cmd.Parameters[4].Value = row.AdjustmentAmount;

                    cmd.Parameters[5].Value = DBNull.Value;
                    if (row.AdjustedAmount != null)
                        cmd.Parameters[5].Value = row.AdjustedAmount.Value;
                    cmd.Parameters[6].Value = row.Note;




                    cmd.ExecuteNonQuery();
                }

                // Insert the data into the Excel spreadsheet.
                //da.Update(dtSQL);

            }


            ExcelGenerator.WriteFile(copyPath, Path.GetFileName(excelTemplatePath));
        }



        public static string GetStatus(object value)
        {
            foreach (KeyValue val in new JobStatus().GetMembers())
            {
                if (val.Key == value.ToString())
                    return val.Value;
            }


            //JobStatusEnum status = (JobStatusEnum)value;

            return "";// status.ToString();
        }

        public static string GetEventName(int type, List<HR_Service_Event> eventList)
        {
            if (type == 0)
                return "";
            HR_Service_Event eve = eventList.FirstOrDefault(x => x.EventID == type);
            if (eve == null)
                return "";
            return eve.Name;
        }


        public static void ExportSalaryIncrement(string excelTemplatePath, List<DAL.GetEmployeeListForMassIncrementResult> list, string headerName)
        {
            string path = HttpContext.Current.Server.MapPath(excelTemplatePath);
            string copyPath = Path.GetDirectoryName(path) + "\\" + Guid.NewGuid() + ".xlsx";
            File.Copy(path, copyPath);

            string excelConnection = string.Format(ExcelConnectionString, copyPath);

            List<HR_Service_Event> eventList = CommonManager.GetServieEventTypes();

            using (OleDbConnection conn = new OleDbConnection(excelConnection))
            {
                // Open the connection.
                conn.Open();

                StringBuilder sql = new StringBuilder(@"Create Table [Sheet1$]( EIN int,Employee varchar(100), [Income] varchar(100),
                    [Designation] varchar(100),[Status] varchar(100),[Existing Income] money,[Increased By] money,[New Income] money,
                    [Arrear Increase] money,[Effective Date] varchar(100),[Eng Date] varchar(100),[Event] varchar(100))
                    ");

                OleDbCommand cmd1 = new OleDbCommand(sql.ToString(), conn);
                cmd1.ExecuteNonQuery();



                // Create a new sheet in the Excel spreadsheet.
                OleDbCommand cmd = new OleDbCommand(@"INSERT INTO [Sheet1$](EIN,Employee, [Income] ,[Designation] ,[Status] ,[Existing Income],[Increased By],[New Income],
                        [Arrear Increase],[Effective Date],[Eng Date],[Event] ) 
                        values (?,?,?,?,?,?,?,?,?,?,?,?)", conn);


                // Add the parameters.

                cmd.Parameters.Add("EIN", OleDbType.Integer, 4);
                cmd.Parameters.Add("Employee", OleDbType.VarChar, 100);


                cmd.Parameters.Add("Income", OleDbType.VarChar, 100);
                cmd.Parameters.Add("Designation", OleDbType.VarChar, 100);
                cmd.Parameters.Add("Status", OleDbType.VarChar, 100);

                cmd.Parameters.Add("[Existing Income]", OleDbType.Currency, 0);
                cmd.Parameters[5].IsNullable = true;

                cmd.Parameters.Add("[Increased By]", OleDbType.Currency, 0);
                cmd.Parameters[6].IsNullable = true;

                cmd.Parameters.Add("[New Income]", OleDbType.Currency, 0);
                cmd.Parameters[7].IsNullable = true;

                cmd.Parameters.Add("[Arrear Increase]", OleDbType.Currency, 0);
                cmd.Parameters[8].IsNullable = true;

                cmd.Parameters.Add("Effective Date", OleDbType.VarChar, 100);
                cmd.Parameters.Add("Eng Date", OleDbType.VarChar, 100);
                cmd.Parameters.Add("Event", OleDbType.VarChar, 100);



                // Changes the Rowstate()of each DataRow to Added,
                // so that OleDbDataAdapter will insert the rows.
                foreach (GetEmployeeListForMassIncrementResult row in list)
                {
                    //dr.SetAdded();
                    cmd.Parameters[0].Value = row.EmployeeId;
                    cmd.Parameters[1].Value = row.Name;

                    cmd.Parameters[2].Value = headerName;// == null ? DBNull.Value : row.Days_At_Palati;
                    //cmd.Parameters[3].Value = GetValue(row.Days_At_Kirne);

                    cmd.Parameters[3].Value = row.Designation;
                    cmd.Parameters[4].Value = GetStatus(row.Status);


                    cmd.Parameters[5].Value = DBNull.Value;
                    if (row.ExistingIncome != null)
                        cmd.Parameters[5].Value = row.ExistingIncome;


                    cmd.Parameters[6].Value = DBNull.Value;
                    if (row.IncrementIncome != null && row.IncrementIncome != 0)
                        cmd.Parameters[6].Value = Convert.ToDecimal(row.IncrementIncome);

                    cmd.Parameters[7].Value = DBNull.Value;
                    if (row.IncrementIncome != null && row.IncrementIncome != 0)
                        cmd.Parameters[7].Value = row.IncrementIncome.Value + (row.ExistingIncome == null ? 0 : row.ExistingIncome);


                    cmd.Parameters[8].Value = DBNull.Value;
                    if (row.RetrospectIncrement != null && row.RetrospectIncrement != 0)
                        cmd.Parameters[8].Value = row.RetrospectIncrement.Value;

                    cmd.Parameters[9].Value = row.EffectiveDate == null ? "" : row.EffectiveDate;
                    cmd.Parameters[10].Value = row.EffectiveDateEng == null ? "" : row.EffectiveDateEng.Value.ToString("yyyy/MMM/dd");
                    cmd.Parameters[11].Value = GetEventName(row.EventID, eventList);

                    cmd.ExecuteNonQuery();
                }

                // Insert the data into the Excel spreadsheet.
                //da.Update(dtSQL);

            }


            ExcelGenerator.WriteFile(copyPath, Path.GetFileName(excelTemplatePath));
        }



        public static void ExportPartialTax(string excelTemplatePath, List<DAL.GetPartialPaidTaxListResult> list, string headerName)
        {
            string path = HttpContext.Current.Server.MapPath(excelTemplatePath);
            string copyPath = Path.GetDirectoryName(path) + "\\" + Guid.NewGuid() + ".xlsx";
            File.Copy(path, copyPath);

            string excelConnection = string.Format(ExcelConnectionString, copyPath);



            using (OleDbConnection conn = new OleDbConnection(excelConnection))
            {
                // Open the connection.
                conn.Open();

                StringBuilder sql = new StringBuilder("Create Table [Sheet1$]( EIN int,Employee varchar(100),Income money,PF money,CIT money,TDS money)");

                OleDbCommand cmd1 = new OleDbCommand(sql.ToString(), conn);
                cmd1.ExecuteNonQuery();



                // Create a new sheet in the Excel spreadsheet.
                OleDbCommand cmd = new OleDbCommand("INSERT INTO [Sheet1$](EIN,Employee,Income,PF,CIT,TDS) values (?,?,?,?,?,?)", conn);


                // Add the parameters.

                cmd.Parameters.Add("EIN", OleDbType.Integer, 4);
                cmd.Parameters.Add("Employee", OleDbType.VarChar, 50);


                cmd.Parameters.Add("Income", OleDbType.Currency, 0);
                cmd.Parameters[2].IsNullable = true;

                cmd.Parameters.Add("PF", OleDbType.Currency, 0);
                cmd.Parameters[3].IsNullable = true;

                cmd.Parameters.Add("CIT", OleDbType.Currency, 0);
                cmd.Parameters[4].IsNullable = true;

                cmd.Parameters.Add("TDS", OleDbType.Currency, 0);
                cmd.Parameters[5].IsNullable = true;



                // Changes the Rowstate()of each DataRow to Added,
                // so that OleDbDataAdapter will insert the rows.
                foreach (GetPartialPaidTaxListResult row in list)
                {
                    //dr.SetAdded();
                    cmd.Parameters[0].Value = row.EmployeeId;
                    cmd.Parameters[1].Value = row.Name;


                    cmd.Parameters[2].Value = DBNull.Value;
                    if (row.IncomeAmount != null)
                        cmd.Parameters[2].Value = row.IncomeAmount.Value;

                    cmd.Parameters[3].Value = DBNull.Value;
                    if (row.PFAmount != null)
                        cmd.Parameters[3].Value = row.PFAmount.Value;

                    cmd.Parameters[4].Value = DBNull.Value;
                    if (row.CITAmount != null)
                        cmd.Parameters[4].Value = row.CITAmount.Value;

                    cmd.Parameters[5].Value = DBNull.Value;
                    if (row.PaidAmount != null)
                        cmd.Parameters[5].Value = row.PaidAmount.Value;





                    cmd.ExecuteNonQuery();
                }

                // Insert the data into the Excel spreadsheet.
                //da.Update(dtSQL);

            }


            ExcelGenerator.WriteFile(copyPath, Path.GetFileName(excelTemplatePath));
        }

        public static void ExportTax(string excelTemplatePath, List<DAL.GetForTaxHistoryResult> list)
        {


            string path = HttpContext.Current.Server.MapPath(excelTemplatePath);
            string copyPath = Path.GetDirectoryName(path) + "\\" + Guid.NewGuid() + ".xlsx";
            File.Copy(path, copyPath);

            string excelConnection = string.Format(ExcelConnectionString, copyPath);



            using (OleDbConnection conn = new OleDbConnection(excelConnection))
            {
                // Open the connection.
                conn.Open();


                // Create a new sheet in the Excel spreadsheet.
                OleDbCommand cmd = new OleDbCommand("INSERT INTO [Sheet1$](EIN,[I No],Employee, [Opening Gross Amount] ,[Opening PF] ,[Opening CIT] ,[Opening Insurance] ,[Opening SST] ,[Opening Tax Paid],[Remote Area Opening] ) values (?,?,?,?,?,?,?,?,?,?)", conn);


                // Add the parameters.

                cmd.Parameters.Add("EIN", OleDbType.Integer, 4);
                cmd.Parameters.Add("[I No]", OleDbType.VarChar, 100);
                cmd.Parameters.Add("Employee", OleDbType.VarChar, 50);

                cmd.Parameters.Add("[Opening Gross Amount]", OleDbType.Currency, 4);

                cmd.Parameters.Add("[Opening PF]", OleDbType.Currency, 4);

                cmd.Parameters.Add("[Opening CIT]", OleDbType.Currency, 4);
                cmd.Parameters.Add("[Opening Insurance]", OleDbType.Currency, 4);
                cmd.Parameters.Add("[Opening SST]", OleDbType.Currency, 4);
                cmd.Parameters.Add("[Opening Tax Paid]", OleDbType.Currency, 4);
                cmd.Parameters.Add("[Remote Area Opening", OleDbType.Currency, 4);
                cmd.Parameters[8].IsNullable = true;
                // Changes the Rowstate()of each DataRow to Added,
                // so that OleDbDataAdapter will insert the rows.
                foreach (GetForTaxHistoryResult row in list)
                {
                    //dr.SetAdded();
                    cmd.Parameters[0].Value = row.EmployeeId;
                    cmd.Parameters[1].Value = row.IdCardNo == null ? "" : row.IdCardNo;
                    cmd.Parameters[2].Value = row.Name;

                    cmd.Parameters[3].Value = row.OpeningTaxableAmount;// == null ? DBNull.Value : row.Days_At_Palati;
                    //cmd.Parameters[3].Value = GetValue(row.Days_At_Kirne);

                    cmd.Parameters[4].Value = row.OpeningPF;

                    cmd.Parameters[5].Value = row.OpeningCIT;
                    cmd.Parameters[6].Value = row.OpeningInsurance;
                    cmd.Parameters[7].Value = row.OpeningSST;
                    cmd.Parameters[8].Value = row.OpeningTaxPaid;
                    if (row.OpeningRemoteArea == null)
                        cmd.Parameters[9].Value = DBNull.Value;
                    else
                        cmd.Parameters[9].Value = row.OpeningRemoteArea;


                    cmd.ExecuteNonQuery();
                }

                // Insert the data into the Excel spreadsheet.
                //da.Update(dtSQL);

            }


            ExcelGenerator.WriteFile(copyPath, Path.GetFileName(excelTemplatePath));

        }
        public static void ExportGratuityWorkdayOpening(string excelTemplatePath, List<DAL.GetForWorkdaysResult> list, List<TextValue> accountList)
        {


            string path = HttpContext.Current.Server.MapPath(excelTemplatePath);
            string copyPath = Path.GetDirectoryName(path) + "\\" + Guid.NewGuid() + ".xlsx";
            File.Copy(path, copyPath);

            string excelConnection = string.Format(ExcelConnectionString, copyPath);



            using (OleDbConnection conn = new OleDbConnection(excelConnection))
            {
                // Open the connection.
                conn.Open();


                // Create a new sheet in the Excel spreadsheet.
                OleDbCommand cmd = new OleDbCommand("INSERT INTO [Sheet1$](EIN,[Account No],Employee, [Workdays Adjustment] ,[UPL for Gratuity] ,[Gratuity Amount Adjustment] ) values (?,?,?,?,?,?)", conn);


                // Add the parameters.

                cmd.Parameters.Add("EIN", OleDbType.Integer, 4);
                cmd.Parameters.Add("Account No", OleDbType.VarChar, 200);
                cmd.Parameters.Add("Employee", OleDbType.VarChar, 200);

                cmd.Parameters.Add("[Workdays Adjustment]", OleDbType.Double, 4);

                cmd.Parameters.Add("[UPL for Gratuity]", OleDbType.Double, 4);

                cmd.Parameters.Add("[Gratuity Amount Adjustment]", OleDbType.Double, 4);


                // Changes the Rowstate()of each DataRow to Added,
                // so that OleDbDataAdapter will insert the rows.
                foreach (GetForWorkdaysResult row in list)
                {
                    TextValue accountNo = accountList.FirstOrDefault(x => x.EIN == row.EmployeeId);
                    //dr.SetAdded();
                    cmd.Parameters[0].Value = row.EmployeeId;
                    cmd.Parameters[1].Value = (accountNo == null || accountNo.Text == null) ? "" : accountNo.Text;
                    cmd.Parameters[2].Value = row.Name;

                    cmd.Parameters[3].Value = row.WorkdayAdjustment == null ? 0 : row.WorkdayAdjustment.Value;// == null ? DBNull.Value : row.Days_At_Palati;
                    //cmd.Parameters[3].Value = GetValue(row.Days_At_Kirne);

                    cmd.Parameters[4].Value = row.UnpaidLeavesForGratuity == null ? 0 : row.UnpaidLeavesForGratuity;

                    cmd.Parameters[5].Value = row.GratuityAmountAdjustment == null ? 0 : row.GratuityAmountAdjustment.Value;

                    cmd.ExecuteNonQuery();
                }

                // Insert the data into the Excel spreadsheet.
                //da.Update(dtSQL);

            }


            ExcelGenerator.WriteFile(copyPath, Path.GetFileName(excelTemplatePath));

        }
        public static void ExportAdditoinIncome(string excelTemplatePath, List<DAL.GetAdditionaBasicListResult> list)
        {


            string path = HttpContext.Current.Server.MapPath(excelTemplatePath);
            string copyPath = Path.GetDirectoryName(path) + "\\" + Guid.NewGuid() + ".xlsx";
            File.Copy(path, copyPath);

            string excelConnection = string.Format(ExcelConnectionString, copyPath);



            using (OleDbConnection conn = new OleDbConnection(excelConnection))
            {
                // Open the connection.
                conn.Open();


                // Create a new sheet in the Excel spreadsheet.
                OleDbCommand cmd = new OleDbCommand("INSERT INTO [Sheet1$](EIN,Employee,[Income],[Amount],[Note]) values (?,?,?,?,?)", conn);


                // Add the parameters.

                cmd.Parameters.Add("EIN", OleDbType.Integer, 4);
                cmd.Parameters.Add("Employee", OleDbType.VarChar, 100);

                cmd.Parameters.Add("[Income]", OleDbType.VarChar, 100);

                cmd.Parameters.Add("[Amount]", OleDbType.Currency, 4);

                cmd.Parameters.Add("[Note]", OleDbType.VarChar, 100);

                // Changes the Rowstate()of each DataRow to Added,
                // so that OleDbDataAdapter will insert the rows.
                foreach (GetAdditionaBasicListResult row in list)
                {
                    //dr.SetAdded();
                    cmd.Parameters[0].Value = row.EmployeeId;
                    cmd.Parameters[1].Value = row.Name;

                    cmd.Parameters[2].Value = row.Income;// == null ? DBNull.Value : row.Days_At_Palati;
                    //cmd.Parameters[3].Value = GetValue(row.Days_At_Kirne);

                    cmd.Parameters[3].Value = row.Amount == null ? 0 : row.Amount.Value;

                    cmd.Parameters[4].Value = row.Notes;

                    cmd.ExecuteNonQuery();
                }

                // Insert the data into the Excel spreadsheet.
                //da.Update(dtSQL);

            }


            ExcelGenerator.WriteFile(copyPath, Path.GetFileName(excelTemplatePath));

        }
        public static void ExportPay(string excelTemplatePath, List<DAL.PPay> list, List<Bank> bankList, List<BankBranch> bankbranchList
            , List<EFundDetail> fundDetals)
        {


            string path = HttpContext.Current.Server.MapPath(excelTemplatePath);
            string copyPath = Path.GetDirectoryName(path) + "\\" + Guid.NewGuid() + ".xlsx";
            File.Copy(path, copyPath);

            string excelConnection = string.Format(ExcelConnectionString, copyPath);



            using (OleDbConnection conn = new OleDbConnection(excelConnection))
            {
                // Open the connection.
                conn.Open();


                // Create a new sheet in the Excel spreadsheet.
                using (OleDbCommand cmd = new OleDbCommand("INSERT INTO [Sheet1$](EIN,Employee, [Payment Mode],[Bank Name],[Bank Branch] ,[Bank/Salary Account No] ,[Loan Account No] ,[Advance Account No],[PF Number],[CIT Number],[CIT Code],[PAN No],[Other Retirement Fund]) values (?,?,?,?,?,?,?,?,?,?,?,?,?)", conn))
                {


                    // Add the parameters.

                    cmd.Parameters.Add("EIN", OleDbType.Integer, 4);
                    cmd.Parameters.Add("Employee", OleDbType.VarChar, 100);

                    cmd.Parameters.Add("[Payment Mode]", OleDbType.VarChar, 100);
                    cmd.Parameters.Add("[Bank Name]", OleDbType.VarChar, 100);
                    cmd.Parameters.Add("[Bank Branch]", OleDbType.VarChar, 100);

                    cmd.Parameters.Add("[Bank/Salary Account No]", OleDbType.VarChar, 100);
                    cmd.Parameters.Add("[Loan Account No]", OleDbType.VarChar, 100);
                    cmd.Parameters.Add("[Advance Account No]", OleDbType.VarChar, 100);

                    cmd.Parameters.Add("[PF Number]", OleDbType.VarChar, 100);
                    cmd.Parameters.Add("[CIT Number]", OleDbType.VarChar, 100);
                    cmd.Parameters.Add("[CIT Code]", OleDbType.VarChar, 100);
                    cmd.Parameters.Add("[PAN No]", OleDbType.VarChar, 100);
                    cmd.Parameters.Add("[Other Retirement Fund]", OleDbType.VarChar, 100);

                    // Changes the Rowstate()of each DataRow to Added,
                    // so that OleDbDataAdapter will insert the rows.
                    foreach (PPay row in list)
                    {
                        EFundDetail fund = fundDetals.FirstOrDefault(x => x.EmployeeId == row.EmployeeId);

                        //dr.SetAdded();
                        cmd.Parameters[0].Value = row.EmployeeId;
                        cmd.Parameters[1].Value = row.EEmployee.Name;

                        cmd.Parameters[2].Value = row.PaymentMode == null ? "" : row.PaymentMode;

                        cmd.Parameters[3].Value = row.BankID == null ? "" : bankList.FirstOrDefault(x => x.BankID == row.BankID).Name;
                        cmd.Parameters[4].Value = row.BankBranchID == null ? "" :
                            (bankList.FirstOrDefault(x => x.BankID == bankbranchList.FirstOrDefault(y => y.BankBranchID == row.BankBranchID).BankID).Name
                                + " - " + bankbranchList.FirstOrDefault(x => x.BankBranchID == row.BankBranchID).Name);

                        cmd.Parameters[5].Value = row.BankACNo == null ? "" : row.BankACNo;
                        cmd.Parameters[6].Value = row.LoanAccount == null ? "" : row.LoanAccount;
                        cmd.Parameters[7].Value = row.AdvanceAccount == null ? "" : row.AdvanceAccount;

                        cmd.Parameters[8].Value = fund == null || fund.PFRFNo == null ? "" : fund.PFRFNo.Trim();
                        cmd.Parameters[9].Value = fund == null || fund.CITNo == null ? "" : fund.CITNo.Trim();
                        cmd.Parameters[10].Value = fund == null || fund.CITCode == null ? "" : fund.CITCode.Trim();
                        cmd.Parameters[11].Value = fund == null || fund.PANNo == null ? "" : fund.PANNo.Trim();
                        cmd.Parameters[12].Value = fund == null || fund.HasOtherPFFund == null || fund.HasOtherPFFund == false ? "" : "Yes";
                        cmd.ExecuteNonQuery();
                    }

                    // Insert the data into the Excel spreadsheet.
                    //da.Update(dtSQL);
                }

                using (OleDbCommand cmd1 = new OleDbCommand("INSERT INTO [Bank$](Name) values (?)", conn))
                {
                    cmd1.Parameters.Add("Name", OleDbType.VarChar, 200);

                    foreach (var row in bankList)
                    {
                        cmd1.Parameters[0].Value = row.Name;
                        cmd1.ExecuteNonQuery();
                    }
                }

                using (OleDbCommand cmd1 = new OleDbCommand("INSERT INTO [BankBranch$](Name) values (?)", conn))
                {
                    cmd1.Parameters.Add("Name", OleDbType.VarChar, 200);

                    foreach (var row in bankbranchList)
                    {
                        cmd1.Parameters[0].Value = row.Bank.Name + " - " + row.Name;
                        cmd1.ExecuteNonQuery();
                    }
                }
            }


            ExcelGenerator.WriteFile(copyPath, Path.GetFileName(excelTemplatePath));

        }



        public static void ExportDevice(string excelTemplatePath, List<DAL.GetForDeviceMappingResult> list)
        {
            string path = HttpContext.Current.Server.MapPath(excelTemplatePath);
            string copyPath = Path.GetDirectoryName(path) + "\\" + Guid.NewGuid() + ".xlsx";
            File.Copy(path, copyPath);

            string excelConnection = string.Format(ExcelConnectionString, copyPath);



            using (OleDbConnection conn = new OleDbConnection(excelConnection))
            {
                // Open the connection.
                conn.Open();


                // Create a new sheet in the Excel spreadsheet.
                OleDbCommand cmd = new OleDbCommand("INSERT INTO [Sheet1$](EIN,Employee, [Device Enroll No],[Enable Manual Attendance],[Exclude Late Absent Mail],[Skip Absent/Late Deduction]) values (?,?,?,?,?,?)", conn);


                // Add the parameters.

                cmd.Parameters.Add("EIN", OleDbType.Integer, 4);
                cmd.Parameters.Add("Employee", OleDbType.VarChar, 50);

                cmd.Parameters.Add("[Device Enroll No]", OleDbType.VarChar, 50);

                cmd.Parameters.Add("[Enable Manual Attendance]", OleDbType.VarChar, 50);
                cmd.Parameters.Add("[Exclude Late Absent Mail]", OleDbType.VarChar, 50);
                cmd.Parameters.Add("[Skip Absent/Late Deduction]", OleDbType.VarChar, 50);
                //cmd.Parameters.Add("[Exclude Device Attendance]", OleDbType.VarChar, 50);
                cmd.Parameters[2].IsNullable = true;

                //cmd.Parameters[3].IsNullable = true;
                // Changes the Rowstate()of each DataRow to Added,
                // so that OleDbDataAdapter will insert the rows.
                foreach (GetForDeviceMappingResult row in list)
                {
                    //dr.SetAdded();
                    cmd.Parameters[0].Value = row.EmployeeId;
                    cmd.Parameters[1].Value = row.Name;

                    if (row.DeviceId == null)
                        cmd.Parameters[2].Value = DBNull.Value;
                    else
                        cmd.Parameters[2].Value = row.DeviceId;// == null ? DBNull.Value : row.Days_At_Palati;

                    if (row.IsEnabledManual == null)
                        cmd.Parameters[3].Value = "";
                    else
                        cmd.Parameters[3].Value = row.IsEnabledManual.Value ? "Yes" : "";

                    if (row.IsEnabledSkipLateEntry == null)
                        cmd.Parameters[4].Value = "";
                    else
                        cmd.Parameters[4].Value = row.IsEnabledSkipLateEntry.Value ? "Yes" : "";

                    if (row.SkipAbsentAndLateEntryDeduction == null)
                        cmd.Parameters[5].Value = "";
                    else
                        cmd.Parameters[5].Value = row.SkipAbsentAndLateEntryDeduction.Value ? "Yes" : "";

                    cmd.ExecuteNonQuery();
                }

                // Insert the data into the Excel spreadsheet.
                //da.Update(dtSQL);

            }


            ExcelGenerator.WriteFile(copyPath, Path.GetFileName(excelTemplatePath));
        }

        public static void ExportPromotionWithCurrentLevel(string excelTemplatePath
            , List<DAL.BLevel> list1, List<DAL.EDesignation> list3, List<EmployeeLevelGradeBO> list)
        {
            string path = HttpContext.Current.Server.MapPath(excelTemplatePath);
            string copyPath = Path.GetDirectoryName(path) + "\\" + Guid.NewGuid() + ".xlsx";
            File.Copy(path, copyPath);

            string excelConnection = string.Format(ExcelConnectionString, copyPath);



            using (OleDbConnection conn = new OleDbConnection(excelConnection))
            {
                // Open the connection.
                conn.Open();


                // Create a new sheet in the Excel spreadsheet.
                OleDbCommand cmd = new OleDbCommand("INSERT INTO [Sheet1$](EIN,Name,[I No], [Current Level],[Current Grade],[Currrent Designation]) values (?,?,?,?,?,?)", conn);


                // Add the parameters.

                cmd.Parameters.Add("EIN", OleDbType.Integer, 4);
                cmd.Parameters.Add("Name", OleDbType.VarChar, 200);
                cmd.Parameters.Add("[I No]", OleDbType.VarChar, 200);
                cmd.Parameters.Add("[Current Level]", OleDbType.VarChar, 200);

                cmd.Parameters.Add("[Current Grade]", OleDbType.Double, 200);
                cmd.Parameters[4].IsNullable = true;
                cmd.Parameters.Add("[Currrent Designation]", OleDbType.VarChar, 200);

                foreach (EmployeeLevelGradeBO row in list)
                {
                    //dr.SetAdded();
                    cmd.Parameters[0].Value = row.EIN;
                    cmd.Parameters[1].Value = row.Name;
                    cmd.Parameters[2].Value = row.INo;

                    cmd.Parameters[3].Value = row.CurrentLevel;
                    if (string.IsNullOrEmpty(row.CurrentGrade) || row.CurrentGrade == null)
                        cmd.Parameters[4].Value = DBNull.Value;
                    else
                    {
                        float val = float.Parse(row.CurrentGrade);
                        cmd.Parameters[4].Value = val;// row.CurrentGrade == null ? "" : row.CurrentGrade;
                    }
                    cmd.Parameters[5].Value = row.CurrentDesignation == null ? "" : row.CurrentDesignation;





                    cmd.ExecuteNonQuery();
                }

                // Insert the data into the Excel spreadsheet.
                //da.Update(dtSQL);
                // Create a new sheet in the Excel spreadsheet.
                using (OleDbCommand cmd1 = new OleDbCommand("INSERT INTO [Level$](Name) values (?)", conn))
                {
                    cmd1.Parameters.Add("Name", OleDbType.VarChar, 200);

                    foreach (var row in list1)
                    {
                        cmd1.Parameters[0].Value = row.BLevelGroup.Name + " - " + row.Name;
                        cmd1.ExecuteNonQuery();
                    }
                }



                using (OleDbCommand cmd2 = new OleDbCommand("INSERT INTO [Designation$](Name) values (?)", conn))
                {
                    cmd2.Parameters.Add("Name", OleDbType.VarChar, 200);

                    foreach (var row in list3)
                    {
                        cmd2.Parameters[0].Value = row.BLevel.GroupLevel + " - " + row.Name;
                        cmd2.ExecuteNonQuery();
                    }
                }
            }


            ExcelGenerator.WriteFile(copyPath, Path.GetFileName(excelTemplatePath));
        }

        public static void ExportPromotionList(string excelTemplatePath, List<DAL.BLevel> list1, List<DAL.EDesignation> list3)
        {
            string path = HttpContext.Current.Server.MapPath(excelTemplatePath);
            string copyPath = Path.GetDirectoryName(path) + "\\" + Guid.NewGuid() + ".xlsx";
            File.Copy(path, copyPath);

            string excelConnection = string.Format(ExcelConnectionString, copyPath);



            using (OleDbConnection conn = new OleDbConnection(excelConnection))
            {
                // Open the connection.
                conn.Open();


                // Create a new sheet in the Excel spreadsheet.
                OleDbCommand cmd = new OleDbCommand("INSERT INTO [Level$](Name) values (?)", conn);

                cmd.Parameters.Add("Name", OleDbType.VarChar, 200);

                foreach (var row in list1)
                {
                    cmd.Parameters[0].Value = row.BLevelGroup.Name + " - " + row.Name;
                    cmd.ExecuteNonQuery();
                }




                OleDbCommand cmd2 = new OleDbCommand("INSERT INTO [Designation$](Name) values (?)", conn);

                cmd2.Parameters.Add("Name", OleDbType.VarChar, 200);

                foreach (var row in list3)
                {
                    cmd2.Parameters[0].Value = row.BLevel.GroupLevel + " - " + row.Name;
                    cmd2.ExecuteNonQuery();
                }


            }


            ExcelGenerator.WriteFile(copyPath, Path.GetFileName(excelTemplatePath));


        }
        public static void ExportNewDesignationImportList(string excelTemplatePath, List<DAL.BLevel> list1)
        {
            string path = HttpContext.Current.Server.MapPath(excelTemplatePath);
            string copyPath = Path.GetDirectoryName(path) + "\\" + Guid.NewGuid() + ".xlsx";
            File.Copy(path, copyPath);

            string excelConnection = string.Format(ExcelConnectionString, copyPath);



            using (OleDbConnection conn = new OleDbConnection(excelConnection))
            {
                // Open the connection.
                conn.Open();


                // Create a new sheet in the Excel spreadsheet.
                OleDbCommand cmd = new OleDbCommand("INSERT INTO [Level$](Name) values (?)", conn);

                cmd.Parameters.Add("Name", OleDbType.VarChar, 200);

                foreach (var row in list1)
                {
                    cmd.Parameters[0].Value = row.BLevelGroup.Name + " - " + row.Name;
                    cmd.ExecuteNonQuery();
                }







            }


            ExcelGenerator.WriteFile(copyPath, Path.GetFileName(excelTemplatePath));


        }
        public static void ExportServiceHistoryList(string excelTemplatePath, List<HR_Service_Event> eventList, List<Branch> branchList, List<Department> depList,
                List<EDesignationBO> desigList, List<KeyValue> list)
        {
            string path = HttpContext.Current.Server.MapPath(excelTemplatePath);
            string copyPath = Path.GetDirectoryName(path) + "\\" + Guid.NewGuid() + ".xlsx";
            File.Copy(path, copyPath);

            string excelConnection = string.Format(ExcelConnectionString, copyPath);



            using (OleDbConnection conn = new OleDbConnection(excelConnection))
            {
                // Open the connection.
                conn.Open();


                // Create a new sheet in the Excel spreadsheet.
                using (OleDbCommand cmd = new OleDbCommand("INSERT INTO [Event$](Name) values (?)", conn))
                {

                    cmd.Parameters.Add("Name", OleDbType.VarChar, 200);

                    foreach (var row in eventList)
                    {
                        cmd.Parameters[0].Value = row.Name;
                        cmd.ExecuteNonQuery();
                    }
                }



                using (OleDbCommand cmd2 = new OleDbCommand("INSERT INTO [Designation$](Name) values (?)", conn))
                {

                    cmd2.Parameters.Add("Name", OleDbType.VarChar, 200);

                    foreach (var row in desigList)
                    {
                        cmd2.Parameters[0].Value = row.LevelAndDesignation;
                        cmd2.ExecuteNonQuery();
                    }
                }

                using (OleDbCommand cmd2 = new OleDbCommand("INSERT INTO [Branch$](Name) values (?)", conn))
                {

                    cmd2.Parameters.Add("Name", OleDbType.VarChar, 200);

                    foreach (var row in branchList)
                    {
                        cmd2.Parameters[0].Value = row.Name;
                        cmd2.ExecuteNonQuery();
                    }
                }

                using (OleDbCommand cmd2 = new OleDbCommand("INSERT INTO [Department$](Name) values (?)", conn))
                {

                    cmd2.Parameters.Add("Name", OleDbType.VarChar, 200);

                    foreach (var row in depList)
                    {
                        cmd2.Parameters[0].Value = row.Name;
                        cmd2.ExecuteNonQuery();
                    }
                }

                using (OleDbCommand cmd2 = new OleDbCommand("INSERT INTO [Status$](Name) values (?)", conn))
                {

                    cmd2.Parameters.Add("Name", OleDbType.VarChar, 200);

                    foreach (var row in list)
                    {
                        cmd2.Parameters[0].Value = row.Value;
                        cmd2.ExecuteNonQuery();
                    }
                }
            }


            ExcelGenerator.WriteFile(copyPath, Path.GetFileName(excelTemplatePath));


        }

        public static void ExportProvisionalCIT(string excelTemplatePath, List<DAL.GetEmployeeListForProvisionalCITResult> list)
        {
            string path = HttpContext.Current.Server.MapPath(excelTemplatePath);
            string copyPath = Path.GetDirectoryName(path) + "\\" + Guid.NewGuid() + ".xlsx";
            File.Copy(path, copyPath);

            string excelConnection = string.Format(ExcelConnectionString, copyPath);



            using (OleDbConnection conn = new OleDbConnection(excelConnection))
            {
                // Open the connection.
                conn.Open();


                // Create a new sheet in the Excel spreadsheet.
                OleDbCommand cmd = new OleDbCommand("INSERT INTO [Sheet1$](EIN,[I No],Employee,[Account No], [CIT Type],[Auto Provision],[Fixed CIT Amount],[One Time Adjustment],[Rate],[Sharwan],[Bhadra],[Ashwin],Kartik,Mangsir,Poush,Magh,Phalgun,Chaitra,Baisakh,Jestha,Ashadh,[External CIT]) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)", conn);


                // Add the parameters.

                cmd.Parameters.Add("EIN", OleDbType.Integer, 4);
                cmd.Parameters.Add("[I No]", OleDbType.VarChar, 200);
                cmd.Parameters.Add("Employee", OleDbType.VarChar, 200);
                cmd.Parameters.Add("Account No", OleDbType.VarChar, 200);

                cmd.Parameters.Add("[CIT Type]", OleDbType.VarChar, 50);
                cmd.Parameters.Add("[Auto Provision]", OleDbType.VarChar, 50);

                cmd.Parameters.Add("[Fixed CIT Amount]", OleDbType.Currency, 4);
                cmd.Parameters[6].IsNullable = true;

                cmd.Parameters.Add("[One Time Adjustment]", OleDbType.Currency, 4);
                cmd.Parameters[7].IsNullable = true;

                cmd.Parameters.Add("[Rate]", OleDbType.Currency, 4);
                cmd.Parameters[8].IsNullable = true;


                cmd.Parameters.Add("Sharwan", OleDbType.Currency, 4);
                cmd.Parameters[9].IsNullable = true;
                cmd.Parameters.Add("Bhadra", OleDbType.Currency, 4);
                cmd.Parameters[10].IsNullable = true;
                cmd.Parameters.Add("Ashwin", OleDbType.Currency, 4);
                cmd.Parameters[11].IsNullable = true;
                cmd.Parameters.Add("Kartik", OleDbType.Currency, 4);
                cmd.Parameters[12].IsNullable = true;

                cmd.Parameters.Add("Mangsir", OleDbType.Currency, 4);
                cmd.Parameters[13].IsNullable = true;
                cmd.Parameters.Add("Poush", OleDbType.Currency, 4);
                cmd.Parameters[14].IsNullable = true;
                cmd.Parameters.Add("Magh", OleDbType.Currency, 4);
                cmd.Parameters[15].IsNullable = true;
                cmd.Parameters.Add("Phalgun", OleDbType.Currency, 4);
                cmd.Parameters[16].IsNullable = true;

                cmd.Parameters.Add("Chaitra", OleDbType.Currency, 4);
                cmd.Parameters[17].IsNullable = true;
                cmd.Parameters.Add("Baisakh", OleDbType.Currency, 4);
                cmd.Parameters[18].IsNullable = true;
                cmd.Parameters.Add("Jestha", OleDbType.Currency, 4);
                cmd.Parameters[19].IsNullable = true;
                cmd.Parameters.Add("Ashadh", OleDbType.Currency, 4);
                cmd.Parameters[20].IsNullable = true;

                cmd.Parameters.Add("[External CIT]", OleDbType.Currency, 4);
                cmd.Parameters[21].IsNullable = true;

                foreach (GetEmployeeListForProvisionalCITResult row in list)
                {
                    //dr.SetAdded();
                    cmd.Parameters[0].Value = row.EmployeeId;
                    cmd.Parameters[1].Value = row.INo == null ? "" : row.INo;
                    cmd.Parameters[2].Value = row.Name;
                    cmd.Parameters[3].Value = row.BankACNo == null ? "" : row.BankACNo;

                    cmd.Parameters[4].Value = row.RegularCITType;
                    cmd.Parameters[5].Value = row.IsCITAutoProvision ? "Yes" : "No";

                    if (row.RegularCITAmountRate == 0 || row.RegularCITAmountRate == null)
                        cmd.Parameters[6].Value = DBNull.Value;
                    else
                        cmd.Parameters[6].Value = row.RegularCITAmountRate;

                    if (row.CurrentMonthAdditionalCIT == 0 || row.CurrentMonthAdditionalCIT == null)
                        cmd.Parameters[7].Value = DBNull.Value;
                    else
                        cmd.Parameters[7].Value = row.CurrentMonthAdditionalCIT;

                    if (row.CITRate == null)
                        cmd.Parameters[8].Value = DBNull.Value;
                    else
                        cmd.Parameters[8].Value = row.CITRate;


                    if (row.Month4 == 0) cmd.Parameters[9].Value = DBNull.Value;
                    else cmd.Parameters[9].Value = row.Month4;
                    if (row.Month5 == 0) cmd.Parameters[10].Value = DBNull.Value;
                    else cmd.Parameters[10].Value = row.Month5;
                    if (row.Month6 == 0) cmd.Parameters[11].Value = DBNull.Value;
                    else cmd.Parameters[11].Value = row.Month6;
                    if (row.Month7 == 0) cmd.Parameters[12].Value = DBNull.Value;
                    else cmd.Parameters[12].Value = row.Month7;

                    if (row.Month8 == 0) cmd.Parameters[13].Value = DBNull.Value;
                    else cmd.Parameters[13].Value = row.Month8;
                    if (row.Month9 == 0) cmd.Parameters[14].Value = DBNull.Value;
                    else cmd.Parameters[14].Value = row.Month9;
                    if (row.Month10 == 0) cmd.Parameters[15].Value = DBNull.Value;
                    else cmd.Parameters[15].Value = row.Month10;
                    if (row.Month11 == 0) cmd.Parameters[16].Value = DBNull.Value;
                    else cmd.Parameters[16].Value = row.Month11;

                    if (row.Month12 == 0) cmd.Parameters[17].Value = DBNull.Value;
                    else cmd.Parameters[17].Value = row.Month12;
                    if (row.Month1 == 0) cmd.Parameters[18].Value = DBNull.Value;
                    else cmd.Parameters[18].Value = row.Month1;
                    if (row.Month2 == 0) cmd.Parameters[19].Value = DBNull.Value;
                    else cmd.Parameters[19].Value = row.Month2;
                    if (row.Month3 == 0) cmd.Parameters[20].Value = DBNull.Value;
                    else cmd.Parameters[20].Value = row.Month3;




                    if (row.ExternalSettlementCIT == null)
                        cmd.Parameters[21].Value = DBNull.Value;
                    else
                        cmd.Parameters[21].Value = row.ExternalSettlementCIT;

                    cmd.ExecuteNonQuery();
                }

                // Insert the data into the Excel spreadsheet.
                //da.Update(dtSQL);

            }


            ExcelGenerator.WriteFile(copyPath, Path.GetFileName(excelTemplatePath));
        }







        public static void ExportProvisionalCITEng(string excelTemplatePath, List<DAL.GetEmployeeListForProvisionalCITResult> list)
        {
            string path = HttpContext.Current.Server.MapPath(excelTemplatePath);
            string copyPath = Path.GetDirectoryName(path) + "\\" + Guid.NewGuid() + ".xlsx";
            File.Copy(path, copyPath);

            string excelConnection = string.Format(ExcelConnectionString, copyPath);



            using (OleDbConnection conn = new OleDbConnection(excelConnection))
            {
                // Open the connection.
                conn.Open();


                // Create a new sheet in the Excel spreadsheet.
                OleDbCommand cmd = new OleDbCommand("INSERT INTO [Sheet1$](EIN,[I No],Employee,[Account No], [CIT Type],[Auto Provision],[Fixed CIT Amount],[One Time Adjustment],[Rate],[July],[August],[September],October,November,December,January,February,March,April,May,June,[External CIT]) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)", conn);


                // Add the parameters.

                cmd.Parameters.Add("EIN", OleDbType.Integer, 4);
                cmd.Parameters.Add("[I No]", OleDbType.VarChar, 200);
                cmd.Parameters.Add("Employee", OleDbType.VarChar, 200);
                cmd.Parameters.Add("Account No", OleDbType.VarChar, 200);

                cmd.Parameters.Add("[CIT Type]", OleDbType.VarChar, 50);
                cmd.Parameters.Add("[Auto Provision]", OleDbType.VarChar, 50);

                cmd.Parameters.Add("[Fixed CIT Amount]", OleDbType.Currency, 4);
                cmd.Parameters[6].IsNullable = true;

                cmd.Parameters.Add("[One Time Adjustment]", OleDbType.Currency, 4);
                cmd.Parameters[7].IsNullable = true;

                cmd.Parameters.Add("[Rate]", OleDbType.Currency, 4);
                cmd.Parameters[8].IsNullable = true;


                cmd.Parameters.Add("July", OleDbType.Currency, 4);
                cmd.Parameters[9].IsNullable = true;
                cmd.Parameters.Add("August", OleDbType.Currency, 4);
                cmd.Parameters[10].IsNullable = true;
                cmd.Parameters.Add("September", OleDbType.Currency, 4);
                cmd.Parameters[11].IsNullable = true;
                cmd.Parameters.Add("October", OleDbType.Currency, 4);
                cmd.Parameters[12].IsNullable = true;

                cmd.Parameters.Add("November", OleDbType.Currency, 4);
                cmd.Parameters[13].IsNullable = true;
                cmd.Parameters.Add("December", OleDbType.Currency, 4);
                cmd.Parameters[14].IsNullable = true;
                cmd.Parameters.Add("January", OleDbType.Currency, 4);
                cmd.Parameters[15].IsNullable = true;
                cmd.Parameters.Add("February", OleDbType.Currency, 4);
                cmd.Parameters[16].IsNullable = true;

                cmd.Parameters.Add("March", OleDbType.Currency, 4);
                cmd.Parameters[17].IsNullable = true;
                cmd.Parameters.Add("April", OleDbType.Currency, 4);
                cmd.Parameters[18].IsNullable = true;
                cmd.Parameters.Add("May", OleDbType.Currency, 4);
                cmd.Parameters[19].IsNullable = true;
                cmd.Parameters.Add("June", OleDbType.Currency, 4);
                cmd.Parameters[20].IsNullable = true;

                cmd.Parameters.Add("[External CIT]", OleDbType.Currency, 4);
                cmd.Parameters[21].IsNullable = true;

                foreach (GetEmployeeListForProvisionalCITResult row in list)
                {
                    //dr.SetAdded();
                    cmd.Parameters[0].Value = row.EmployeeId;
                    cmd.Parameters[1].Value = row.INo == null ? "" : row.INo;
                    cmd.Parameters[2].Value = row.Name;
                    cmd.Parameters[3].Value = row.BankACNo == null ? "" : row.BankACNo;

                    cmd.Parameters[4].Value = row.RegularCITType;
                    cmd.Parameters[5].Value = row.IsCITAutoProvision ? "Yes" : "No";

                    if (row.RegularCITAmountRate == 0 || row.RegularCITAmountRate == null)
                        cmd.Parameters[6].Value = DBNull.Value;
                    else
                        cmd.Parameters[6].Value = row.RegularCITAmountRate;

                    if (row.CurrentMonthAdditionalCIT == 0 || row.CurrentMonthAdditionalCIT == null)
                        cmd.Parameters[7].Value = DBNull.Value;
                    else
                        cmd.Parameters[7].Value = row.CurrentMonthAdditionalCIT;

                    if (row.CITRate == null)
                        cmd.Parameters[8].Value = DBNull.Value;
                    else
                        cmd.Parameters[8].Value = row.CITRate;


                    if (row.Month7 == 0) cmd.Parameters[9].Value = DBNull.Value;
                    else cmd.Parameters[9].Value = row.Month7;
                    if (row.Month8 == 0) cmd.Parameters[10].Value = DBNull.Value;
                    else cmd.Parameters[10].Value = row.Month8;
                    if (row.Month9 == 0) cmd.Parameters[11].Value = DBNull.Value;
                    else cmd.Parameters[11].Value = row.Month9;
                    if (row.Month10 == 0) cmd.Parameters[12].Value = DBNull.Value;
                    else cmd.Parameters[12].Value = row.Month10;

                    if (row.Month11 == 0) cmd.Parameters[13].Value = DBNull.Value;
                    else cmd.Parameters[13].Value = row.Month11;
                    if (row.Month12 == 0) cmd.Parameters[14].Value = DBNull.Value;
                    else cmd.Parameters[14].Value = row.Month12;
                    if (row.Month1 == 0) cmd.Parameters[15].Value = DBNull.Value;
                    else cmd.Parameters[15].Value = row.Month1;
                    if (row.Month2 == 0) cmd.Parameters[16].Value = DBNull.Value;
                    else cmd.Parameters[16].Value = row.Month2;

                    if (row.Month3 == 0) cmd.Parameters[17].Value = DBNull.Value;
                    else cmd.Parameters[17].Value = row.Month3;
                    if (row.Month4 == 0) cmd.Parameters[18].Value = DBNull.Value;
                    else cmd.Parameters[18].Value = row.Month4;
                    if (row.Month5 == 0) cmd.Parameters[19].Value = DBNull.Value;
                    else cmd.Parameters[19].Value = row.Month5;
                    if (row.Month6 == 0) cmd.Parameters[20].Value = DBNull.Value;
                    else cmd.Parameters[20].Value = row.Month6;




                    if (row.ExternalSettlementCIT == null)
                        cmd.Parameters[21].Value = DBNull.Value;
                    else
                        cmd.Parameters[21].Value = row.ExternalSettlementCIT;

                    cmd.ExecuteNonQuery();
                }

                // Insert the data into the Excel spreadsheet.
                //da.Update(dtSQL);

            }


            ExcelGenerator.WriteFile(copyPath, Path.GetFileName(excelTemplatePath));
        }
        public static void ExportTargetAchievement(string excelTemplatePath, List<TargetAchievmentBO> list)
        {
            string path = HttpContext.Current.Server.MapPath(excelTemplatePath);
            string copyPath = Path.GetDirectoryName(path) + "\\" + Guid.NewGuid() + ".xlsx";
            File.Copy(path, copyPath);

            string excelConnection = string.Format(ExcelConnectionString, copyPath);



            using (OleDbConnection conn = new OleDbConnection(excelConnection))
            {
                // Open the connection.
                conn.Open();


                // Create a new sheet in the Excel spreadsheet.
                OleDbCommand cmd = new OleDbCommand("INSERT INTO [Sheet1$](EIN,Employee,Target, [Baisakh Target],[Baisakh Achievement],[Jestha Target],[Jestha Achievement],[Ashadh Target],[Ashadh Achievement],[Sharwan Target],[Sharwan Achievement],[Bhadra Target],[Bhadra Achievement],[Ashwin Target],[Ashwin Achievement],[Kartik Target],[Kartik Achievement],[Mangsir Target],[Mangsir Achievement],[Poush Target],[Poush Achievement],[Magh Target],[Magh Achievement],[Falgun Target],[Falgun Achievement],[Chaitra Target],[Chaitra Achievement]) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)", conn);


                // Add the parameters.

                cmd.Parameters.Add("EIN", OleDbType.Integer, 4);
                cmd.Parameters.Add("Employee", OleDbType.VarChar, 200);
                cmd.Parameters.Add("Target", OleDbType.VarChar, 200);

                int index = 3;

                cmd.Parameters.Add("Baisakh Target", OleDbType.Double, 4);
                cmd.Parameters[index++].IsNullable = true;
                cmd.Parameters.Add("Baisakh Achievement", OleDbType.Double, 4);
                cmd.Parameters[index++].IsNullable = true;
                cmd.Parameters.Add("Jestha Target", OleDbType.Double, 4);
                cmd.Parameters[index++].IsNullable = true;
                cmd.Parameters.Add("Jestha Achievement", OleDbType.Double, 4);
                cmd.Parameters[index++].IsNullable = true;
                cmd.Parameters.Add("Ashadh Target", OleDbType.Double, 4);
                cmd.Parameters[index++].IsNullable = true;
                cmd.Parameters.Add("Ashadh Achievement", OleDbType.Double, 4);
                cmd.Parameters[index++].IsNullable = true;
                cmd.Parameters.Add("Sharwan Target", OleDbType.Double, 4);
                cmd.Parameters[index++].IsNullable = true;
                cmd.Parameters.Add("Sharwan Achievement", OleDbType.Double, 4);
                cmd.Parameters[index++].IsNullable = true;
                cmd.Parameters.Add("Bhadra Target", OleDbType.Double, 4);
                cmd.Parameters[index++].IsNullable = true;
                cmd.Parameters.Add("Bhadra Achievement", OleDbType.Double, 4);
                cmd.Parameters[index++].IsNullable = true;
                cmd.Parameters.Add("Ashwin Target", OleDbType.Double, 4);
                cmd.Parameters[index++].IsNullable = true;
                cmd.Parameters.Add("Ashwin Achievement", OleDbType.Double, 4);
                cmd.Parameters[index++].IsNullable = true;

                cmd.Parameters.Add("Kartik Target", OleDbType.Double, 4);
                cmd.Parameters[index++].IsNullable = true;
                cmd.Parameters.Add("Kartik Achievement", OleDbType.Double, 4);
                cmd.Parameters[index++].IsNullable = true;
                cmd.Parameters.Add("Mangsir Target", OleDbType.Double, 4);
                cmd.Parameters[index++].IsNullable = true;
                cmd.Parameters.Add("Mangsir Achievement", OleDbType.Double, 4);
                cmd.Parameters[index++].IsNullable = true;
                cmd.Parameters.Add("Poush Target", OleDbType.Double, 4);
                cmd.Parameters[index++].IsNullable = true;
                cmd.Parameters.Add("Poush Achievement", OleDbType.Double, 4);
                cmd.Parameters[index++].IsNullable = true;
                cmd.Parameters.Add("Magh Target", OleDbType.Double, 4);
                cmd.Parameters[index++].IsNullable = true;
                cmd.Parameters.Add("Magh Achievement", OleDbType.Double, 4);
                cmd.Parameters[index++].IsNullable = true;
                cmd.Parameters.Add("Falgun Target", OleDbType.Double, 4);
                cmd.Parameters[index++].IsNullable = true;
                cmd.Parameters.Add("Falgun Achievement", OleDbType.Double, 4);
                cmd.Parameters[index++].IsNullable = true;
                cmd.Parameters.Add("Chaitra Target", OleDbType.Double, 4);
                cmd.Parameters[index++].IsNullable = true;
                cmd.Parameters.Add("Chaitra Achievement", OleDbType.Double, 4);
                cmd.Parameters[index++].IsNullable = true;





                foreach (TargetAchievmentBO row in list)
                {
                    //dr.SetAdded();
                    cmd.Parameters[0].Value = row.EIN;
                    cmd.Parameters[1].Value = row.Name;
                    cmd.Parameters[2].Value = row.TargetName;

                    index = 3;

                    if (row.Target1 == 0) cmd.Parameters[index++].Value = DBNull.Value;
                    else cmd.Parameters[index++].Value = row.Target1;
                    if (row.Achievment1 == 0) cmd.Parameters[index++].Value = DBNull.Value;
                    else cmd.Parameters[index++].Value = row.Achievment1;
                    if (row.Target2 == 0) cmd.Parameters[index++].Value = DBNull.Value;
                    else cmd.Parameters[index++].Value = row.Target2;
                    if (row.Achievment2 == 0) cmd.Parameters[index++].Value = DBNull.Value;
                    else cmd.Parameters[index++].Value = row.Achievment2;
                    if (row.Target3 == 0) cmd.Parameters[index++].Value = DBNull.Value;
                    else cmd.Parameters[index++].Value = row.Target3;
                    if (row.Achievment3 == 0) cmd.Parameters[index++].Value = DBNull.Value;
                    else cmd.Parameters[index++].Value = row.Achievment3;
                    if (row.Target4 == 0) cmd.Parameters[index++].Value = DBNull.Value;
                    else cmd.Parameters[index++].Value = row.Target4;
                    if (row.Achievment4 == 0) cmd.Parameters[index++].Value = DBNull.Value;
                    else cmd.Parameters[index++].Value = row.Achievment4;
                    if (row.Target5 == 0) cmd.Parameters[index++].Value = DBNull.Value;
                    else cmd.Parameters[index++].Value = row.Target5;
                    if (row.Achievment5 == 0) cmd.Parameters[index++].Value = DBNull.Value;
                    else cmd.Parameters[index++].Value = row.Achievment5;
                    if (row.Target6 == 0) cmd.Parameters[index++].Value = DBNull.Value;
                    else cmd.Parameters[index++].Value = row.Target6;
                    if (row.Achievment6 == 0) cmd.Parameters[index++].Value = DBNull.Value;
                    else cmd.Parameters[index++].Value = row.Achievment6;

                    if (row.Target7 == 0) cmd.Parameters[index++].Value = DBNull.Value;
                    else cmd.Parameters[index++].Value = row.Target7;
                    if (row.Achievment7 == 0) cmd.Parameters[index++].Value = DBNull.Value;
                    else cmd.Parameters[index++].Value = row.Achievment7;
                    if (row.Target8 == 0) cmd.Parameters[index++].Value = DBNull.Value;
                    else cmd.Parameters[index++].Value = row.Target8;
                    if (row.Achievment8 == 0) cmd.Parameters[index++].Value = DBNull.Value;
                    else cmd.Parameters[index++].Value = row.Achievment8;
                    if (row.Target9 == 0) cmd.Parameters[index++].Value = DBNull.Value;
                    else cmd.Parameters[index++].Value = row.Target9;
                    if (row.Achievment9 == 0) cmd.Parameters[index++].Value = DBNull.Value;
                    else cmd.Parameters[index++].Value = row.Achievment9;
                    if (row.Target10 == 0) cmd.Parameters[index++].Value = DBNull.Value;
                    else cmd.Parameters[index++].Value = row.Target10;
                    if (row.Achievment10 == 0) cmd.Parameters[index++].Value = DBNull.Value;
                    else cmd.Parameters[index++].Value = row.Achievment10;
                    if (row.Target11 == 0) cmd.Parameters[index++].Value = DBNull.Value;
                    else cmd.Parameters[index++].Value = row.Target11;
                    if (row.Achievment11 == 0) cmd.Parameters[index++].Value = DBNull.Value;
                    else cmd.Parameters[index++].Value = row.Achievment11;
                    if (row.Target12 == 0) cmd.Parameters[index++].Value = DBNull.Value;
                    else cmd.Parameters[index++].Value = row.Target12;
                    if (row.Achievment12 == 0) cmd.Parameters[index++].Value = DBNull.Value;
                    else cmd.Parameters[index++].Value = row.Achievment12;


                    cmd.ExecuteNonQuery();
                }

                // Insert the data into the Excel spreadsheet.
                //da.Update(dtSQL);

            }


            ExcelGenerator.WriteFile(copyPath, Path.GetFileName(excelTemplatePath));
        }

        public static void ExportFixedPFDeduction(string excelTemplatePath, List<DAL.GetFixedPFDeductionEmpListResult> list)
        {
            string path = HttpContext.Current.Server.MapPath(excelTemplatePath);
            string copyPath = Path.GetDirectoryName(path) + "\\" + Guid.NewGuid() + ".xlsx";
            File.Copy(path, copyPath);

            string excelConnection = string.Format(ExcelConnectionString, copyPath);



            using (OleDbConnection conn = new OleDbConnection(excelConnection))
            {
                // Open the connection.
                conn.Open();


                // Create a new sheet in the Excel spreadsheet.
                OleDbCommand cmd = new OleDbCommand("INSERT INTO [Sheet1$](EIN,[I No],Employee,[Status], [PF Deduction Type],[PF Contribution],[PF Deduction]) values (?,?,?,?,?,?,?)", conn);


                // Add the parameters.

                cmd.Parameters.Add("EIN", OleDbType.Integer, 4);
                cmd.Parameters.Add("[I No]", OleDbType.VarChar, 200);
                cmd.Parameters.Add("Employee", OleDbType.VarChar, 200);
                cmd.Parameters.Add("[Status]", OleDbType.VarChar, 200);
                cmd.Parameters.Add("[PF Deduction Type]", OleDbType.VarChar, 50);

                cmd.Parameters.Add("[PF Contribution]", OleDbType.Currency, 4);
                cmd.Parameters[5].IsNullable = true;

                cmd.Parameters.Add("[PF Deduction]", OleDbType.Currency, 4);
                cmd.Parameters[6].IsNullable = true;



                foreach (GetFixedPFDeductionEmpListResult row in list)
                {
                    //dr.SetAdded();
                    cmd.Parameters[0].Value = row.EmployeeId;
                    cmd.Parameters[1].Value = row.IdCardNo == null ? "" : row.IdCardNo;
                    cmd.Parameters[2].Value = row.Name;
                    cmd.Parameters[3].Value = row.StatusName;

                    cmd.Parameters[4].Value = row.IsPFDeductionFixed == false ? "Default PF" : "Fixed Amount";

                    if (row.PFContribution == 0)
                        cmd.Parameters[5].Value = DBNull.Value;
                    else
                        cmd.Parameters[5].Value = row.PFContribution;

                    if (row.FixedPFDeductionAmount == null)
                        cmd.Parameters[6].Value = DBNull.Value;
                    else
                        cmd.Parameters[6].Value = row.FixedPFDeductionAmount;


                    cmd.ExecuteNonQuery();
                }

                // Insert the data into the Excel spreadsheet.
                //da.Update(dtSQL);

            }


            ExcelGenerator.WriteFile(copyPath, Path.GetFileName(excelTemplatePath));
        }

        public static void ExportEmployeeTime(string excelTemplatePath, List<DAL.AttendanceInOutTime> list)
        {
            string path = HttpContext.Current.Server.MapPath(excelTemplatePath);
            string copyPath = Path.GetDirectoryName(path) + "\\" + Guid.NewGuid() + ".xlsx";
            File.Copy(path, copyPath);

            string excelConnection = string.Format(ExcelConnectionString, copyPath);



            using (OleDbConnection conn = new OleDbConnection(excelConnection))
            {
                // Open the connection.
                conn.Open();


                // Create a new sheet in the Excel spreadsheet.
                OleDbCommand cmd = new OleDbCommand("INSERT INTO [Sheet1$]([In Time], [Out Time], [Break In], [Break Out],[Half Holiday Out Time], [HalfDay Leave In Out Time],[Public Holiday In Time],[Public Holiday Out Time], [Shift]) values (?,?,?,?,?,?,?,?,?)", conn);


                // Add the parameters.

                //cmd.Parameters.Add("EIN", OleDbType.Integer, 4);
                //cmd.Parameters["EIN"].IsNullable = true;
                //cmd.Parameters.Add("Employee", OleDbType.VarChar, 50);
                //cmd.Parameters["Employee"].IsNullable = true;
                cmd.Parameters.Add("[In Time]", OleDbType.DBTime, 50);
                cmd.Parameters.Add("[Out Time]", OleDbType.DBTime, 50);
                cmd.Parameters.Add("[Break In]", OleDbType.DBTime, 50);
                cmd.Parameters.Add("[Break Out]", OleDbType.DBTime, 50);
                cmd.Parameters.Add("[Half Holiday Out Time]", OleDbType.DBTime, 50);
                cmd.Parameters["[Half Holiday Out Time]"].IsNullable = true;
                cmd.Parameters.Add("[HalfDay Leave In Out Time]", OleDbType.DBTime, 50);
                cmd.Parameters["[HalfDay Leave In Out Time]"].IsNullable = true;
                cmd.Parameters.Add("[Public Holiday In Time]", OleDbType.DBTime, 50);
                cmd.Parameters["[Public Holiday In Time]"].IsNullable = true;
                cmd.Parameters.Add("[Public Holiday Out Time]", OleDbType.DBTime, 50);
                cmd.Parameters["[Public Holiday Out Time]"].IsNullable = true;
                cmd.Parameters.Add("[Shift]", OleDbType.VarChar, 50);
                cmd.Parameters["[Shift]"].IsNullable = true;

                // Changes the Rowstate()of each DataRow to Added,
                // so that OleDbDataAdapter will insert the rows.
                foreach (AttendanceInOutTime row in list)
                {
                    //dr.SetAdded();
                    //if (row.EmployeeId != null)
                    //    cmd.Parameters[0].Value = row.EmployeeId;
                    //else
                    //    cmd.Parameters[0].Value = DBNull.Value; ;

                    //if (row.EmployeeId != null)
                    //    cmd.Parameters[1].Value = EmployeeManager.GetEmployeeById(row.EmployeeId.Value).Name;
                    //else
                    //    cmd.Parameters[1].Value = DBNull.Value; 

                    cmd.Parameters[0].Value = row.OfficeInTime;
                    cmd.Parameters[1].Value = row.OfficeOutTime;
                    cmd.Parameters[2].Value = row.OfficeFirstHalfInTime;
                    cmd.Parameters[3].Value = row.OfficeSecondHalfOutTime;

                    if (row.HalfDayHolidayOutTime == null)
                        cmd.Parameters[4].Value = DBNull.Value;
                    else
                        cmd.Parameters[4].Value = row.HalfDayHolidayOutTime;

                    if (row.HalfDayHolidayInTime == null)
                        cmd.Parameters[5].Value = DBNull.Value;
                    else
                        cmd.Parameters[5].Value = row.HalfDayHolidayInTime;

                    if (row.PublicHolidayInTime == null)
                        cmd.Parameters[6].Value = DBNull.Value;
                    else
                        cmd.Parameters[6].Value = row.PublicHolidayInTime;

                    if (row.PublicHolidayOutTime == null)
                        cmd.Parameters[7].Value = DBNull.Value;
                    else
                        cmd.Parameters[7].Value = row.PublicHolidayOutTime;

                    if (row.ShiftID != null)
                        cmd.Parameters[8].Value = AttendanceManager.GetShiftByID(row.ShiftID.Value).Name;
                    else
                        cmd.Parameters[8].Value = DBNull.Value;




                    cmd.ExecuteNonQuery();
                }

                // Insert the data into the Excel spreadsheet.
                //da.Update(dtSQL);
                WriteIntoSheet(conn, "ShiftList", ShiftManager.GetAllShifts().Select(x => x.Name).ToList().ToArray());
            }


            ExcelGenerator.WriteFile(copyPath, Path.GetFileName(excelTemplatePath));
        }

        public static void ExportDeemedIncome(string excelTemplatePath, List<DAL.PEmployeeIncome> list, PIncome income)
        {
            string path = HttpContext.Current.Server.MapPath(excelTemplatePath);
            string copyPath = Path.GetDirectoryName(path) + "\\" + Guid.NewGuid() + ".xlsx";
            File.Copy(path, copyPath);

            string excelConnection = string.Format(ExcelConnectionString, copyPath);

            bool isAmountType = income.DeemedIsAmount.Value;

            using (OleDbConnection conn = new OleDbConnection(excelConnection))
            {
                // Open the connection.
                conn.Open();


                // Create a new sheet in the Excel spreadsheet.
                OleDbCommand cmd = null;

                if (isAmountType)
                    cmd = new OleDbCommand("INSERT INTO [Sheet1$](EIN,INo,Employee, [Amount]  ) values (?,?,?,?)", conn);
                else
                    cmd = new OleDbCommand("INSERT INTO [Sheet1$](EIN,INo,Employee, [Rate %] ) values (?,?,?,?)", conn);


                // Add the parameters.

                cmd.Parameters.Add("EIN", OleDbType.Integer, 4);
                cmd.Parameters.Add("INo", OleDbType.VarChar, 50);
                cmd.Parameters.Add("Employee", OleDbType.VarChar, 200);

                if (isAmountType)
                {
                    cmd.Parameters.Add("[Amount]", OleDbType.Currency, 4);
                    cmd.Parameters["[Amount]"].IsNullable = true;
                }
                else
                {
                    cmd.Parameters.Add("[Rate %]", OleDbType.Currency, 4);
                    cmd.Parameters["[Rate %]"].IsNullable = true;
                }

                // Changes the Rowstate()of each DataRow to Added,
                // so that OleDbDataAdapter will insert the rows.
                foreach (PEmployeeIncome row in list)
                {
                    //dr.SetAdded();
                    cmd.Parameters[0].Value = row.EmployeeId;
                    cmd.Parameters[1].Value = row.IDCardNo == null ? "" : row.IDCardNo;
                    cmd.Parameters[2].Value = row.Name;

                    if (isAmountType)
                    {


                        cmd.Parameters[3].Value = row.DeemedAmount;



                    }
                    else
                    {
                        if (row.RatePercent == null)
                            cmd.Parameters[3].Value = DBNull.Value;
                        else
                            cmd.Parameters[3].Value = row.RatePercent;
                    }



                    cmd.ExecuteNonQuery();
                }

                // Insert the data into the Excel spreadsheet.
                //da.Update(dtSQL);

            }


            ExcelGenerator.WriteFile(copyPath, Path.GetFileName(excelTemplatePath));
        }

        public static void ExportMegaLikeChildrenLikeIncome(string excelTemplatePath, List<DAL.PEmployeeIncome> list, PIncome income)
        {
            string path = HttpContext.Current.Server.MapPath(excelTemplatePath);
            string copyPath = Path.GetDirectoryName(path) + "\\" + Guid.NewGuid() + ".xlsx";
            File.Copy(path, copyPath);

            string excelConnection = string.Format(ExcelConnectionString, copyPath);




            using (OleDbConnection conn = new OleDbConnection(excelConnection))
            {
                // Open the connection.
                conn.Open();


                // Create a new sheet in the Excel spreadsheet.
                OleDbCommand cmd = null;



                cmd = new OleDbCommand("INSERT INTO [Sheet1$](EIN,Employee, [BillReceived]  ) values (?,?,?)", conn);




                // Add the parameters.

                cmd.Parameters.Add("EIN", OleDbType.Integer, 4);
                cmd.Parameters.Add("Employee", OleDbType.VarChar, 50);



                cmd.Parameters.Add("[BillReceived]", OleDbType.Currency, 4);
                cmd.Parameters["[BillReceived]"].IsNullable = true;



                // Changes the Rowstate()of each DataRow to Added,
                // so that OleDbDataAdapter will insert the rows.
                foreach (PEmployeeIncome row in list)
                {
                    //dr.SetAdded();
                    cmd.Parameters[0].Value = row.EmployeeId;
                    cmd.Parameters[1].Value = row.Name;




                    if (row.Amount == null)
                        cmd.Parameters[2].Value = DBNull.Value;
                    else
                        cmd.Parameters[2].Value = row.Amount;








                    cmd.ExecuteNonQuery();
                }

                // Insert the data into the Excel spreadsheet.
                //da.Update(dtSQL);

            }


            ExcelGenerator.WriteFile(copyPath, Path.GetFileName(excelTemplatePath));
        }

        public static void ExportLeaveAdjustment(string excelTemplatePath, List<DAL.LeaveGetLeaveAdjustmentsResult> list
           , string payrollMonth, string leave)
        {
            string path = HttpContext.Current.Server.MapPath(excelTemplatePath);
            string copyPath = Path.GetDirectoryName(path) + "\\" + Guid.NewGuid() + ".xlsx";
            File.Copy(path, copyPath);

            string excelConnection = string.Format(ExcelConnectionString, copyPath);



            using (OleDbConnection conn = new OleDbConnection(excelConnection))
            {
                // Open the connection.
                conn.Open();


                // Create a new sheet in the Excel spreadsheet.
                OleDbCommand cmd = new OleDbCommand("INSERT INTO [Sheet1$](EIN,Employee,Leave,[Payroll Month],Opening,Accured,Taken,Adjustment,Lapsed,Encashed,Closing) values (?,?,?,?,?,?,?,?,?,?,?)", conn);


                // Add the parameters.

                cmd.Parameters.Add("EIN", OleDbType.Integer, 4);
                cmd.Parameters.Add("Employee", OleDbType.VarChar, 50);
                cmd.Parameters.Add("Leave", OleDbType.VarChar, 200);
                cmd.Parameters.Add("[Payroll Month]", OleDbType.VarChar, 200);

                cmd.Parameters.Add("Opening", OleDbType.Decimal, 4);
                cmd.Parameters.Add("Accured", OleDbType.Decimal, 4);
                cmd.Parameters.Add("Taken", OleDbType.Decimal, 4);
                cmd.Parameters.Add("Adjustment", OleDbType.Decimal, 4);
                cmd.Parameters.Add("Lapsed", OleDbType.Decimal, 4);
                cmd.Parameters.Add("Encashed", OleDbType.Decimal, 4);
                cmd.Parameters.Add("Closing", OleDbType.Decimal, 4);
                //cmd.Parameters.Add("[Adjusted Closing]", OleDbType.Decimal, 4);



                // Changes the Rowstate()of each DataRow to Added,
                // so that OleDbDataAdapter will insert the rows.
                foreach (LeaveGetLeaveAdjustmentsResult row in list)
                {
                    //dr.SetAdded();
                    cmd.Parameters[0].Value = row.EmployeeId;
                    cmd.Parameters[1].Value = row.Name;

                    cmd.Parameters[2].Value = leave;// == null ? DBNull.Value : row.Days_At_Palati;
                    //cmd.Parameters[3].Value = GetValue(row.Days_At_Kirne);

                    cmd.Parameters[3].Value = payrollMonth;


                    cmd.Parameters[4].Value = row.BeginningBalance == null ? 0 : row.BeginningBalance.Value;
                    cmd.Parameters[5].Value = row.Accured == null ? 0 : row.Accured;
                    cmd.Parameters[6].Value = row.Taken == null ? 0 : row.Taken;
                    cmd.Parameters[7].Value = row.Adjusted == null ? 0 : row.Adjusted.Value;

                    cmd.Parameters[8].Value = row.Lapsed == null ? 0 : row.Lapsed.Value;
                    cmd.Parameters[9].Value = row.Encased == null ? 0 : row.Encased.Value;
                    cmd.Parameters[10].Value = row.NewBalance == null ? 0 : row.NewBalance.Value;
                    //cmd.Parameters[8].Value = row.NewBalance;


                    cmd.ExecuteNonQuery();
                }

                // Insert the data into the Excel spreadsheet.
                //da.Update(dtSQL);

            }


            ExcelGenerator.WriteFile(copyPath, Path.GetFileName(excelTemplatePath));
        }
        public static void ExportCumulativeSummary(string excelTemplatePath, List<DAL.Report_GetCumulativeSummaryResult> list
          , string payrollMonth)
        {
            string path = HttpContext.Current.Server.MapPath(excelTemplatePath);
            string copyPath = Path.GetDirectoryName(path) + "\\" + Guid.NewGuid() + ".xlsx";
            File.Copy(path, copyPath);

            string excelConnection = string.Format(ExcelConnectionString, copyPath);



            using (OleDbConnection conn = new OleDbConnection(excelConnection))
            {
                // Open the connection.
                conn.Open();


                // Create a new sheet in the Excel spreadsheet.
                OleDbCommand cmd = new OleDbCommand("INSERT INTO [Sheet1$](EIN,Employee,[Payroll Month],[Late Days],[Leave Days],[Half Days Leave Count]) values (?,?,?,?,?,?)", conn);


                // Add the parameters.

                cmd.Parameters.Add("EIN", OleDbType.Integer, 4);
                cmd.Parameters.Add("Employee", OleDbType.VarChar, 50);
                cmd.Parameters.Add("[Payroll Month]", OleDbType.VarChar, 200);

                cmd.Parameters.Add("Late Days", OleDbType.Decimal, 4);
                cmd.Parameters.Add("Leave Days", OleDbType.Decimal, 4);
                cmd.Parameters.Add("Half Days Leave Count", OleDbType.Decimal, 4);



                // Changes the Rowstate()of each DataRow to Added,
                // so that OleDbDataAdapter will insert the rows.
                foreach (Report_GetCumulativeSummaryResult row in list)
                {
                    //dr.SetAdded();
                    cmd.Parameters[0].Value = row.EIN;
                    cmd.Parameters[1].Value = row.Name;


                    cmd.Parameters[2].Value = payrollMonth;

                    cmd.Parameters[3].Value = row.LateDays == null ? 0 : row.LateDays.Value;
                    cmd.Parameters[4].Value = row.LeaveDays == null ? 0 : row.LeaveDays.Value;

                    cmd.Parameters[5].Value = row.HalfDayLeaveDaysCount == null ? 0 : row.HalfDayLeaveDaysCount.Value;

                    cmd.ExecuteNonQuery();
                }

                // Insert the data into the Excel spreadsheet.
                //da.Update(dtSQL);

            }


            ExcelGenerator.WriteFile(copyPath, Path.GetFileName(excelTemplatePath));
        }
        public static void ExportOTTotalHour(string excelTemplatePath, List<DAL.GetGeneratedOvertimeListResult> list)
        {
            string path = HttpContext.Current.Server.MapPath(excelTemplatePath);
            string copyPath = Path.GetDirectoryName(path) + "\\" + Guid.NewGuid() + ".xlsx";
            File.Copy(path, copyPath);

            string excelConnection = string.Format(ExcelConnectionString, copyPath);



            using (OleDbConnection conn = new OleDbConnection(excelConnection))
            {
                // Open the connection.
                conn.Open();


                // Create a new sheet in the Excel spreadsheet.
                using (OleDbCommand cmd = new OleDbCommand("INSERT INTO [Sheet1$](EIN,Employee,[Total OT Hrs]) values (?,?,?)", conn))
                {


                    // Add the parameters.

                    cmd.Parameters.Add("EIN", OleDbType.Integer, 4);
                    cmd.Parameters.Add("Employee", OleDbType.VarChar, 50);

                    cmd.Parameters.Add("[Total OT Hrs]", OleDbType.Decimal, 4);



                    // Changes the Rowstate()of each DataRow to Added,
                    // so that OleDbDataAdapter will insert the rows.
                    foreach (GetGeneratedOvertimeListResult row in list)
                    {
                        //dr.SetAdded();
                        cmd.Parameters[0].Value = row.EIN;
                        cmd.Parameters[1].Value = row.Name;

                        cmd.Parameters[2].Value = row.TotalOTHour;

                        cmd.ExecuteNonQuery();
                    }
                }

                // Insert the data into the Excel spreadsheet.
                //da.Update(dtSQL);

            }


            ExcelGenerator.WriteFile(copyPath, Path.GetFileName(excelTemplatePath));
        }
        public static void ExportBonus(string excelTemplatePath, List<BonusEmployeeBO> list
          )
        {
            string path = HttpContext.Current.Server.MapPath(excelTemplatePath);
            string copyPath = Path.GetDirectoryName(path) + "\\" + Guid.NewGuid() + ".xlsx";
            File.Copy(path, copyPath);

            string excelConnection = string.Format(ExcelConnectionString, copyPath);



            using (OleDbConnection conn = new OleDbConnection(excelConnection))
            {
                // Open the connection.
                conn.Open();


                // Create a new sheet in the Excel spreadsheet.
                OleDbCommand cmd = new OleDbCommand("INSERT INTO [Sheet1$](EIN,AccountNo,Employee,[Unpaid Leave],[Other Deduct Days],[Other Deduct Days Reason],[Annual Salary]) values (?,?,?,?,?,?,?)", conn);


                // Add the parameters.

                cmd.Parameters.Add("EIN", OleDbType.Integer, 4);
                cmd.Parameters.Add("AccountNo", OleDbType.VarChar, 200);
                cmd.Parameters.Add("Employee", OleDbType.VarChar, 200);


                cmd.Parameters.Add("[Unpaid Leave]", OleDbType.Decimal, 4);
                cmd.Parameters[3].IsNullable = true;
                cmd.Parameters.Add("[Other Deduct Days]", OleDbType.Decimal, 4);
                cmd.Parameters[4].IsNullable = true;
                cmd.Parameters.Add("[Other Deduct Days Reason]", OleDbType.VarChar, 200);
                cmd.Parameters.Add("[Annual Salary]", OleDbType.Decimal, 4);
                cmd.Parameters[5].IsNullable = true;

                //cmd.Parameters.Add("[Adjusted Closing]", OleDbType.Decimal, 4);



                // Changes the Rowstate()of each DataRow to Added,
                // so that OleDbDataAdapter will insert the rows.
                foreach (BonusEmployeeBO row in list)
                {
                    //dr.SetAdded();
                    cmd.Parameters[0].Value = row.EmployeeId;
                    cmd.Parameters[1].Value = row.AccountNo == null ? "" : row.AccountNo;
                    cmd.Parameters[2].Value = row.Name == null ? "" : row.Name;

                    cmd.Parameters[3].Value = GetValue(row.UnpaidLeave);
                    cmd.Parameters[4].Value = GetValue(row.OtherUneligibleDays);
                    cmd.Parameters[5].Value = row.OtherUneligibleDaysReason == null ? "" : row.OtherUneligibleDaysReason;
                    cmd.Parameters[6].Value = GetValue(row.AnnualSalary);
                    //cmd.Parameters[3].Value = GetValue(row.Days_At_Kirne);


                    cmd.ExecuteNonQuery();
                }

                // Insert the data into the Excel spreadsheet.
                //da.Update(dtSQL);

            }


            ExcelGenerator.WriteFile(copyPath, Path.GetFileName(excelTemplatePath));
        }
        public static void ExportPastSheet(string excelTemplatePath)
        {
            string path = HttpContext.Current.Server.MapPath(excelTemplatePath);
            string copyPath = Path.GetDirectoryName(path) + "\\" + Guid.NewGuid() + ".xlsx";
            File.Copy(path, copyPath);

            string excelConnection = string.Format(ExcelConnectionString, copyPath);






            ExcelGenerator.WriteFile(copyPath, Path.GetFileName(excelTemplatePath));
        }
        public static void ExportUserEmployee(string excelTemplatePath, List<GetUserListResult> list)
        {
            string path = HttpContext.Current.Server.MapPath(excelTemplatePath);
            string copyPath = Path.GetDirectoryName(path) + "\\" + Guid.NewGuid() + ".xlsx";
            File.Copy(path, copyPath);

            string excelConnection = string.Format(ExcelConnectionString, copyPath);



            using (OleDbConnection conn = new OleDbConnection(excelConnection))
            {
                // Open the connection.
                conn.Open();


                // Create a new sheet in the Excel spreadsheet.
                OleDbCommand cmd = new OleDbCommand("INSERT INTO [Sheet1$](EIN,Employee, [User Name],[Active Directory User Name] ) values (?,?,?,?)", conn);


                // Add the parameters.

                cmd.Parameters.Add("EIN", OleDbType.Integer, 4);
                cmd.Parameters.Add("Employee", OleDbType.VarChar, 100);

                cmd.Parameters.Add("[User Name]", OleDbType.VarChar, 100);

                cmd.Parameters.Add("[Active Directory User Name]", OleDbType.VarChar, 100);



                // Changes the Rowstate()of each DataRow to Added,
                // so that OleDbDataAdapter will insert the rows.
                foreach (GetUserListResult row in list)
                {
                    //dr.SetAdded();
                    cmd.Parameters[0].Value = row.EmployeeId;
                    cmd.Parameters[1].Value = row.EmployeeName;

                    cmd.Parameters[2].Value = row.UserName;
                    //cmd.Parameters[3].Value = GetValue(row.Days_At_Kirne);

                    cmd.Parameters[3].Value = row.ActiveDirectoryName == null ? "" : row.ActiveDirectoryName;




                    cmd.ExecuteNonQuery();
                }

                // Insert the data into the Excel spreadsheet.
                //da.Update(dtSQL);

            }


            ExcelGenerator.WriteFile(copyPath, Path.GetFileName(excelTemplatePath));
        }

        #region Nset

        public static void ExportNsetCIT(string excelTemplatePath, List<Nset_GetEmployeeListForCITResult> list)
        {
            string path = HttpContext.Current.Server.MapPath(excelTemplatePath);
            string copyPath = Path.GetDirectoryName(path) + "\\" + Guid.NewGuid() + ".xlsx";
            File.Copy(path, copyPath);

            string excelConnection = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + copyPath + ";Extended Properties='Excel 12.0 Xml;HDR=Yes'";


            using (OleDbConnection conn = new OleDbConnection(excelConnection))
            {
                // Open the connection.
                conn.Open();


                // Create a new sheet in the Excel spreadsheet.
                OleDbCommand cmd = new OleDbCommand("INSERT INTO [Sheet1$](EIN,Employee, [July-2011] ,[Aug-2011] ,[Sep-2011] ,[Oct-2011] ,[Nov-2011] ,[Dec-2011] ,[Jan-2012] ,[Feb-2012] ,[March-2012] ,[April-2012] ,[May-2012] ,[June-2012]) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?)", conn);


                // Add the parameters.

                cmd.Parameters.Add("EIN", OleDbType.Integer, 4);
                cmd.Parameters.Add("Employee", OleDbType.VarChar, 50);

                cmd.Parameters.Add("[July-2011]", OleDbType.Currency, 4);


                cmd.Parameters.Add("[Aug-2011]", OleDbType.Currency, 4);

                cmd.Parameters.Add("[Sep-2011]", OleDbType.Currency, 4);
                cmd.Parameters.Add("[Oct-2011]", OleDbType.Currency, 4);
                cmd.Parameters.Add("[Nov-2011]", OleDbType.Currency, 4);
                cmd.Parameters.Add("[Dec-2011]", OleDbType.Currency, 4);

                cmd.Parameters.Add("[Jan-2012]", OleDbType.Currency, 4);
                cmd.Parameters.Add("[Feb-2012]", OleDbType.Currency, 4);
                cmd.Parameters.Add("[March-2012]", OleDbType.Currency, 4);
                cmd.Parameters.Add("[April-2012]", OleDbType.Currency, 4);

                cmd.Parameters.Add("[May-2012]", OleDbType.Currency, 4);

                cmd.Parameters.Add("[June-2012]", OleDbType.Currency, 4);



                // Changes the Rowstate()of each DataRow to Added,
                // so that OleDbDataAdapter will insert the rows.
                foreach (Nset_GetEmployeeListForCITResult row in list)
                {
                    //dr.SetAdded();
                    cmd.Parameters[0].Value = row.EIN;
                    cmd.Parameters[1].Value = row.Employee;

                    cmd.Parameters[2].Value = GetValue(row.Month6);// == null ? DBNull.Value : row.Days_At_Palati;
                    //cmd.Parameters[3].Value = GetValue(row.Days_At_Kirne);

                    cmd.Parameters[3].Value = GetValue(row.Month7);

                    cmd.Parameters[4].Value = GetValue(row.Month8);
                    cmd.Parameters[5].Value = GetValue(row.Month9);
                    cmd.Parameters[6].Value = GetValue(row.Month10);
                    cmd.Parameters[7].Value = GetValue(row.Month11);

                    cmd.Parameters[8].Value = GetValue(row.Month12);
                    cmd.Parameters[9].Value = GetValue(row.Month1);
                    cmd.Parameters[10].Value = GetValue(row.Month2);
                    cmd.Parameters[11].Value = GetValue(row.Month3);

                    cmd.Parameters[12].Value = GetValue(row.Month4);

                    cmd.Parameters[13].Value = GetValue(row.Month5);




                    cmd.ExecuteNonQuery();
                }

                // Insert the data into the Excel spreadsheet.
                //da.Update(dtSQL);

            }


            ExcelGenerator.WriteFile(copyPath, Path.GetFileName(excelTemplatePath));
        }
        #endregion

        #region HPL Allowance

        public static void ExportShiftAllowanceExcel(string excelTemplatePath, List<HPL_GetEmployeeForAllowanceResult> list)
        {
            string path = HttpContext.Current.Server.MapPath(excelTemplatePath);
            string copyPath = Path.GetDirectoryName(path) + "\\" + Guid.NewGuid() + ".xlsx";
            File.Copy(path, copyPath);

            string excelConnection = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + copyPath + ";Extended Properties='Excel 12.0 Xml;HDR=Yes'";


            using (OleDbConnection conn = new OleDbConnection(excelConnection))
            {
                // Open the connection.
                conn.Open();


                // Create a new sheet in the Excel spreadsheet.
                OleDbCommand cmd = new OleDbCommand("INSERT INTO [Sheet1$](EIN,Employee, Days_At_Palati, On_Call, Shift_Afternoon,Shift_Night,Split_Shift_Morning,Split_Shift_Evening ,OT_150,OT_200,OT_300,Public_Holiday, Other_Leave, Snacks_Allowance,Special_Compensation_Allowance,Meal_Allowance,TADA,Vehicle_Allowance) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)", conn);


                // Add the parameters.

                cmd.Parameters.Add("EIN", OleDbType.Integer, 4);
                cmd.Parameters.Add("Employee", OleDbType.VarChar, 50);

                cmd.Parameters.Add("Days_At_Palati", OleDbType.Double, 4);
                //cmd.Parameters.Add("Days_At_Kirne", OleDbType.Integer, 4);

                cmd.Parameters.Add("On_Call", OleDbType.Double, 4);

                cmd.Parameters.Add("Shift_Afternoon", OleDbType.Double, 4);
                cmd.Parameters.Add("Shift_Night", OleDbType.Double, 4);
                cmd.Parameters.Add("Split_Shift_Morning", OleDbType.Double, 4);
                cmd.Parameters.Add("Split_Shift_Evening", OleDbType.Double, 4);

                cmd.Parameters.Add("OT_150", OleDbType.Double, 4);
                cmd.Parameters.Add("OT_200", OleDbType.Double, 4);
                cmd.Parameters.Add("OT_300", OleDbType.Double, 4);
                cmd.Parameters.Add("Public_Holiday", OleDbType.Double, 4);

                cmd.Parameters.Add("Other_Leave", OleDbType.Double, 4);

                cmd.Parameters.Add("Snacks_Allowance", OleDbType.Double, 4);
                cmd.Parameters.Add("Special_Compensation_Allowance", OleDbType.Double, 4);
                cmd.Parameters.Add("Meal_Allowance", OleDbType.Double, 4);
                cmd.Parameters.Add("TADA", OleDbType.Double, 4);
                cmd.Parameters.Add("Vehicle_Allowance", OleDbType.Double, 4);



                // Changes the Rowstate()of each DataRow to Added,
                // so that OleDbDataAdapter will insert the rows.
                foreach (HPL_GetEmployeeForAllowanceResult row in list)
                {
                    //dr.SetAdded();
                    cmd.Parameters[0].Value = row.EIN;
                    cmd.Parameters[1].Value = row.Employee;

                    cmd.Parameters[2].Value = GetValue(row.Days_At_Palati);// == null ? DBNull.Value : row.Days_At_Palati;
                    //cmd.Parameters[3].Value = GetValue(row.Days_At_Kirne);

                    cmd.Parameters[3].Value = GetValue(row.On_Call);

                    cmd.Parameters[4].Value = GetValue(row.Shift_Afternoon);
                    cmd.Parameters[5].Value = GetValue(row.Shift_Night);
                    cmd.Parameters[6].Value = GetValue(row.Split_Shift_Morning);
                    cmd.Parameters[7].Value = GetValue(row.Split_Shift_Evening);

                    cmd.Parameters[8].Value = GetValue(row.OT_150);
                    cmd.Parameters[9].Value = GetValue(row.OT_200);
                    cmd.Parameters[10].Value = GetValue(row.OT_300);
                    cmd.Parameters[11].Value = GetValue(row.Public_Holiday);

                    cmd.Parameters[12].Value = GetValue(row.Other_Leave);

                    cmd.Parameters[13].Value = GetValue(row.Snacks_Allowance);
                    cmd.Parameters[14].Value = GetValue(row.Special_Compensation_Allowance);
                    cmd.Parameters[15].Value = GetValue(row.Meal_Allowance);
                    cmd.Parameters[16].Value = GetValue(row.TADA);
                    cmd.Parameters[17].Value = GetValue(row.Vehicle_Allowance);


                    cmd.ExecuteNonQuery();
                }

                // Insert the data into the Excel spreadsheet.
                //da.Update(dtSQL);

            }


            ExcelGenerator.WriteFile(copyPath, Path.GetFileName(excelTemplatePath));
        }

        #endregion

        public static void WriteFile(byte[] buffer, string attachementName)
        {
            //write file 
            HttpContext.Current.Response.ClearHeaders();
            //Response.Buffer = true;
            //Response.Cache.SetCacheability(HttpCacheability.Private);
            // Set ContentType to the ContentType of our file
            HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";
            // Response.ContentType = "application/vnd.ms-excel";
            HttpContext.Current.Response.AddHeader("Content-Disposition", String.Format("attachment;filename=\"{0}\"", attachementName));

            //byte[] buffer = null;

            //using (FileStream fs = File.OpenRead(filePath))
            //{
            //    int length = (int)fs.Length;


            //    using (BinaryReader br = new BinaryReader(fs))
            //    {
            //        buffer = br.ReadBytes(length);
            //    }
            //}

            HttpContext.Current.Response.BinaryWrite(buffer);

            // Write data out of database into Output Stream


            HttpContext.Current.Response.End();


        }
        public static void WriteFile(string filePath, string attachementName)
        {
            //write file 
            HttpContext.Current.Response.ClearHeaders();
            //Response.Buffer = true;
            //Response.Cache.SetCacheability(HttpCacheability.Private);
            // Set ContentType to the ContentType of our file
            HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";
            // Response.ContentType = "application/vnd.ms-excel";
            HttpContext.Current.Response.AddHeader("Content-Disposition", String.Format("attachment;filename=\"{0}\"", attachementName));

            byte[] buffer = null;

            using (FileStream fs = File.OpenRead(filePath))
            {
                int length = (int)fs.Length;


                using (BinaryReader br = new BinaryReader(fs))
                {
                    buffer = br.ReadBytes(length);
                }
            }

            HttpContext.Current.Response.BinaryWrite(buffer);

            // Write data out of database into Output Stream
            File.Delete(filePath);

            HttpContext.Current.Response.End();


        }







        public static void WriteMailMergeExportExcel(string excelTemplatePath, List<MailMergeExcelTemp> list)
        {
            string path = HttpContext.Current.Server.MapPath(excelTemplatePath);
            string copyPath = Path.GetDirectoryName(path) + "\\" + Guid.NewGuid() + ".xlsx";
            File.Copy(path, copyPath);

            string excelConnection = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + copyPath + ";Extended Properties='Excel 12.0 Xml;HDR=Yes'";

            try
            {
                using (OleDbConnection conn = new OleDbConnection(excelConnection))
                {
                    // Open the connection.
                    conn.Open();


                    StringBuilder sql = new StringBuilder(string.Format("Create Table [Import$]( {0} varchar(50),{1} varchar(50), {2} varchar(50),{3} varchar(50),{4} varchar(50),{5} varchar(50),{6} varchar(50),{7} varchar(50),{8} varchar(50),{9} varchar(50),{10} varchar(50),{11} varchar(50) )",
                                         list[0].EmployeeID, "[" + list[0].EmployeeName + "]", "[" + list[0].Column1 + "]", "[" + list[0].Column2 + "]",
                                        "[" + list[0].Column3 + "]", "[" + list[0].Column4 + "]",
                                        "[" + list[0].Column5 + "]", "[" + list[0].Column6 + "]", "[" + list[0].Column7 + "]",
                                        "[" + list[0].Column8 + "]", "[" + list[0].Column9 + "]", "[" + list[0].Column10 + "]"));



                    OleDbCommand cmd1 = new OleDbCommand(sql.ToString(), conn);
                    using (cmd1)
                    {
                        cmd1.ExecuteNonQuery();
                    }



                    OleDbCommand cmd = new OleDbCommand(string.Format("INSERT INTO [Import$]({0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11}) values (?,?,?,?,?,?,?,?,?,?,?,?)",
                         list[0].EmployeeID, "[" + list[0].EmployeeName + "]", "[" + list[0].Column1 + "]", "[" + list[0].Column2 + "]",
                                        "[" + list[0].Column3 + "]", "[" + list[0].Column4 + "]",
                                        "[" + list[0].Column5 + "]", "[" + list[0].Column6 + "]", "[" + list[0].Column7 + "]",
                                        "[" + list[0].Column8 + "]", "[" + list[0].Column9 + "]", "[" + list[0].Column10 + "]"), conn);


                    cmd.Parameters.Add(list[0].EmployeeID, OleDbType.VarChar, 50);
                    cmd.Parameters.Add(list[0].EmployeeName, OleDbType.VarChar, 150);
                    cmd.Parameters.Add(list[0].Column1, OleDbType.VarChar, 250);
                    cmd.Parameters.Add(list[0].Column2, OleDbType.VarChar, 250);
                    cmd.Parameters.Add(list[0].Column3, OleDbType.VarChar, 250);
                    cmd.Parameters.Add(list[0].Column4, OleDbType.VarChar, 250);
                    cmd.Parameters.Add(list[0].Column5, OleDbType.VarChar, 250);
                    cmd.Parameters.Add(list[0].Column6, OleDbType.VarChar, 250);
                    cmd.Parameters.Add(list[0].Column7, OleDbType.VarChar, 250);
                    cmd.Parameters.Add(list[0].Column8, OleDbType.VarChar, 250);
                    cmd.Parameters.Add(list[0].Column9, OleDbType.VarChar, 250);
                    cmd.Parameters.Add(list[0].Column10, OleDbType.VarChar, 250);



                    using (cmd)
                    {
                        foreach (var item in list)
                        {
                            if (list.IndexOf(item) != 0)
                            {
                                cmd.Parameters[0].Value = item.EmployeeID;
                                cmd.Parameters[1].Value = item.EmployeeName;
                                cmd.Parameters[2].Value = item.Column1;
                                cmd.Parameters[3].Value = item.Column2;
                                cmd.Parameters[4].Value = item.Column3;
                                cmd.Parameters[5].Value = item.Column4;
                                cmd.Parameters[6].Value = item.Column5;
                                cmd.Parameters[7].Value = item.Column6;
                                cmd.Parameters[8].Value = item.Column7;
                                cmd.Parameters[9].Value = item.Column8;
                                cmd.Parameters[10].Value = item.Column9;
                                cmd.Parameters[11].Value = item.Column10;






                                /*
                                cmd.Parameters[1].Value = string.IsNullOrEmpty(item.Amount001) == true ? 0 : decimal.Parse(item.Amount001.ToString());
                                cmd.Parameters[2].Value = string.IsNullOrEmpty(item.Amount002) == true ? 0 : decimal.Parse(item.Amount002.ToString());
                                cmd.Parameters[3].Value = string.IsNullOrEmpty(item.Amount003) == true ? 0 : decimal.Parse(item.Amount003.ToString());
                                cmd.Parameters[4].Value = string.IsNullOrEmpty(item.Amount1) == true ? 0 : decimal.Parse(item.Amount1.ToString());
                                cmd.Parameters[5].Value = string.IsNullOrEmpty(item.Amount2) == true ? 0 : decimal.Parse(item.Amount2.ToString());
                                cmd.Parameters[6].Value = string.IsNullOrEmpty(item.Amount3) == true ? 0 : decimal.Parse(item.Amount3.ToString());
                                cmd.Parameters[7].Value = string.IsNullOrEmpty(item.Amount4) == true ? 0 : decimal.Parse(item.Amount4.ToString());
                                cmd.Parameters[8].Value = string.IsNullOrEmpty(item.Amount5) == true ? 0 : decimal.Parse(item.Amount5.ToString());
                                cmd.Parameters[9].Value = string.IsNullOrEmpty(item.Amount6) == true ? 0 : decimal.Parse(item.Amount6.ToString());
                                cmd.Parameters[10].Value = string.IsNullOrEmpty(item.Amount7) == true ? 0 : decimal.Parse(item.Amount7.ToString());
                                cmd.Parameters[11].Value = string.IsNullOrEmpty(item.Amount8) == true ? 0 : decimal.Parse(item.Amount8.ToString());
                                cmd.Parameters[12].Value = string.IsNullOrEmpty(item.Amount9) == true ? 0 : decimal.Parse(item.Amount9.ToString());
                                cmd.Parameters[13].Value = string.IsNullOrEmpty(item.Amount10) == true ? 0 : decimal.Parse(item.Amount10.ToString());
                                cmd.Parameters[14].Value = string.IsNullOrEmpty(item.Amount11) == true ? 0 : decimal.Parse(item.Amount11.ToString());
                                cmd.Parameters[15].Value = string.IsNullOrEmpty(item.Amount12) == true ? 0 : decimal.Parse(item.Amount12.ToString());
                                */
                                cmd.ExecuteNonQuery();
                            }

                        }

                    }



                }
                ExcelGenerator.WriteFile(copyPath, Path.GetFileName(excelTemplatePath));
            }
            finally
            {
                try
                {
                    File.Delete(copyPath);
                }
                catch
                {
                }
            }
        }




        public static void WriteQuestionExportExcel(string excelTemplatePath, List<AppraisalFormQuestionnaire> list)
        {
            string path = HttpContext.Current.Server.MapPath(excelTemplatePath);
            string copyPath = Path.GetDirectoryName(path) + "\\" + Guid.NewGuid() + ".xlsx";
            File.Copy(path, copyPath);

            string excelConnection = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + copyPath + ";Extended Properties='Excel 12.0 Xml;HDR=Yes'";

            try
            {
                using (OleDbConnection conn = new OleDbConnection(excelConnection))
                {
                    // Open the connection.
                    conn.Open();


                    // Create a new sheet in the Excel spreadsheet.
                    OleDbCommand cmd = new OleDbCommand("INSERT INTO [Sheet1$](Question) values (?)", conn);


                    // Add the parameters.


                    cmd.Parameters.Add("Question", OleDbType.VarChar, 200);



                    foreach (var row in list)
                    {
                        //dr.SetAdded();
                        cmd.Parameters[0].Value = row.Question;


                        cmd.ExecuteNonQuery();
                    }

                    // Insert the data into the Excel spreadsheet.
                    //da.Update(dtSQL);

                }





                ExcelGenerator.WriteFile(copyPath, Path.GetFileName(excelTemplatePath));
            }
            finally
            {
                try
                {
                    File.Delete(copyPath);
                }
                catch
                {
                }
            }
        }

        public static void WriteEducationLocationExportExcel(string excelTemplatePath, List<AppraisalEmployeeEducationLocationScore> list, List<EEmployee> employees)
        {
            string path = HttpContext.Current.Server.MapPath(excelTemplatePath);
            string copyPath = Path.GetDirectoryName(path) + "\\" + Guid.NewGuid() + ".xlsx";
            File.Copy(path, copyPath);

            string excelConnection = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + copyPath + ";Extended Properties='Excel 12.0 Xml;HDR=Yes'";

            try
            {
                using (OleDbConnection conn = new OleDbConnection(excelConnection))
                {
                    // Open the connection.
                    conn.Open();


                    // Create a new sheet in the Excel spreadsheet.
                    //  OleDbCommand cmd = new OleDbCommand("INSERT INTO [Sheet1$](Question) values (?)", conn);


                    OleDbCommand cmd = new OleDbCommand(string.Format("INSERT INTO [Sheet1$]({0},{1},{2},{3},{4}) values (?,?,?,?,?)",
                         "[EIN]", "[Employee]", "[Location Score]", "[Education Score]", "[Seniority Score]"), conn);
                    // Add the parameters.
                    cmd.Parameters.Add("[EIN]", OleDbType.Integer, 4);
                    cmd.Parameters.Add("[Employee]", OleDbType.VarChar, 200);

                    cmd.Parameters.Add("[Location Score]", OleDbType.Double, 4);
                    cmd.Parameters.Add("[Education Score]", OleDbType.Double, 4);
                    cmd.Parameters.Add("[Seniority Score]", OleDbType.Double, 4);

                    foreach (var row in list)
                    {
                        //dr.SetAdded();

                        cmd.Parameters[0].Value = row.EmployeeId;

                        EEmployee emp = employees.FirstOrDefault(x => x.EmployeeId == row.EmployeeId);

                        //string EmployeeName = EmployeeManager.GetEmployeeById(row.EmployeeId).Name;
                        cmd.Parameters[1].Value = emp == null ? "" : emp.Name;

                        cmd.Parameters[2].Value = row.LocationScore;
                        cmd.Parameters[3].Value = row.EducationScore;
                        cmd.Parameters[4].Value = row.SeniorityScore;

                        cmd.ExecuteNonQuery();
                    }

                    // Insert the data into the Excel spreadsheet.
                    //da.Update(dtSQL);

                }





                ExcelGenerator.WriteFile(copyPath, Path.GetFileName(excelTemplatePath));
            }
            finally
            {
                try
                {
                    File.Delete(copyPath);
                }
                catch
                {
                }
            }
        }




        public static void ExportPFCITOpening(string excelTemplatePath, List<DAL.GetOpeningImportResult> list)
        {


            string path = HttpContext.Current.Server.MapPath(excelTemplatePath);
            string copyPath = Path.GetDirectoryName(path) + "\\" + Guid.NewGuid() + ".xlsx";
            File.Copy(path, copyPath);

            string excelConnection = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + copyPath + ";Extended Properties='Excel 12.0 Xml;HDR=Yes'";

            try
            {
                using (OleDbConnection conn = new OleDbConnection(excelConnection))
                {
                    conn.Open();


                    StringBuilder sql = new StringBuilder(string.Format("Create Table [Sheet1$]( {0} int,{1} varchar(200), {2} varchar(200),{3} varchar(200),{4} varchar(200),{5} money,{6} money,{7} money,{8} money )",
                                         "[EmployeeId]", "[Name]", "[Branch]", "[Department]", "[Designation]", "[OpeningPF]", "[OpeningCIT]", "[OpeningPFInterest]", "[OpeningCITInterest]"));

                    //[EmployeeId],[Name], [Branch] ,[Department] ,[Designation] ,[OpeningPF] ,[OpeningCIT] ,[OpeningPFInterest],[OpeningCITInterest]

                    OleDbCommand cmd1 = new OleDbCommand(sql.ToString(), conn);
                    using (cmd1)
                    {
                        cmd1.ExecuteNonQuery();
                    }



                    OleDbCommand cmd = new OleDbCommand(string.Format("INSERT INTO [Sheet1$]({0},{1},{2},{3},{4},{5},{6},{7},{8}) values (?,?,?,?,?,?,?,?,?)",
                         "[EmployeeId]", "[Name]", "[Branch]", "[Department]", "[Designation]", "[OpeningPF]", "[OpeningCIT]", "[OpeningPFInterest]", "[OpeningCITInterest]"), conn);


                    //OleDbCommand cmd = new OleDbCommand(@"INSERT INTO [TADAClaim$](Date, [Eng Date], From, To, Description, [Local Daily], [UpCountry Daily], Fooding, Transportation, Feul, Maintainence, Communication, [Other Allowance], Total) 
                    //            values (?,?,?,?,?,?,?,?,?,?,?,?,?,?)", conn);



                    cmd.Parameters.Add("EmployeeId", OleDbType.Integer, 4);
                    cmd.Parameters.Add("[Name]", OleDbType.VarChar, 200);
                    cmd.Parameters.Add("[Branch]", OleDbType.VarChar, 200);
                    cmd.Parameters.Add("[Department]", OleDbType.VarChar, 200);
                    cmd.Parameters.Add("[Designation]", OleDbType.VarChar, 200);


                    cmd.Parameters.Add("[OpeningPF]", OleDbType.Currency, 4);
                    cmd.Parameters[5].IsNullable = true;
                    cmd.Parameters.Add("[OpeningCIT]", OleDbType.Currency, 4);
                    cmd.Parameters[6].IsNullable = true;
                    cmd.Parameters.Add("[OpeningPFInterest]", OleDbType.Currency, 4);
                    cmd.Parameters[7].IsNullable = true;
                    cmd.Parameters.Add("OpeningCITInterest", OleDbType.Currency, 4);
                    cmd.Parameters[8].IsNullable = true;

                    using (cmd)
                    {
                        foreach (var row in list)
                        {
                            cmd.Parameters[0].Value = row.EmployeeId;
                            cmd.Parameters[1].Value = row.Name;
                            cmd.Parameters[2].Value = row.Branch;
                            cmd.Parameters[3].Value = row.Department;
                            cmd.Parameters[4].Value = row.Designation;

                            if (row.OpeningPF == null || row.OpeningPF == 0)
                                cmd.Parameters[5].Value = DBNull.Value;
                            else
                                cmd.Parameters[5].Value = row.OpeningPF;
                            if (row.OpeningCIT == null || row.OpeningCIT == 0)
                                cmd.Parameters[6].Value = DBNull.Value;
                            else
                                cmd.Parameters[6].Value = row.OpeningCIT;

                            if (row.OpeningPFInterest == null || row.OpeningPFInterest == 0)
                                cmd.Parameters[7].Value = DBNull.Value;
                            else
                                cmd.Parameters[7].Value = row.OpeningPFInterest;
                            if (row.OpeningCITInterest == null || row.OpeningCITInterest == 0)
                                cmd.Parameters[8].Value = DBNull.Value;
                            else
                                cmd.Parameters[8].Value = row.OpeningCITInterest;


                            cmd.ExecuteNonQuery();


                        }

                    }




                }
                ExcelGenerator.WriteFile(copyPath, Path.GetFileName(excelTemplatePath));
            }
            finally
            {
                try
                {
                    File.Delete(copyPath);
                }
                catch
                {
                }
            }

        }



        public static void ExportPFCITYearLyInterst(string excelTemplatePath, List<DAL.GetInterestImportResult> list)
        {


            string path = HttpContext.Current.Server.MapPath(excelTemplatePath);
            string copyPath = Path.GetDirectoryName(path) + "\\" + Guid.NewGuid() + ".xlsx";
            File.Copy(path, copyPath);

            string excelConnection = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + copyPath + ";Extended Properties='Excel 12.0 Xml;HDR=Yes'";

            try
            {
                using (OleDbConnection conn = new OleDbConnection(excelConnection))
                {
                    conn.Open();


                    StringBuilder sql = new StringBuilder(string.Format("Create Table [Sheet1$]( {0} int,{1} varchar(200), {2} varchar(200),{3} varchar(200),{4} varchar(200),{5} money,{6} money )",
                                         "[EmployeeId]", "[Name]", "[Branch]", "[Department]", "[Designation]", "[PFInterest]", "[CITInterest]"));

                    //[EmployeeId],[Name], [Branch] ,[Department] ,[Designation] ,[OpeningPF] ,[OpeningCIT] ,[OpeningPFInterest],[OpeningCITInterest]

                    OleDbCommand cmd1 = new OleDbCommand(sql.ToString(), conn);
                    using (cmd1)
                    {
                        cmd1.ExecuteNonQuery();
                    }



                    OleDbCommand cmd = new OleDbCommand(string.Format("INSERT INTO [Sheet1$]({0},{1},{2},{3},{4},{5},{6}) values (?,?,?,?,?,?,?)",
                         "[EmployeeId]", "[Name]", "[Branch]", "[Department]", "[Designation]", "[PFInterest]", "[CITInterest]"), conn);


                    //OleDbCommand cmd = new OleDbCommand(@"INSERT INTO [TADAClaim$](Date, [Eng Date], From, To, Description, [Local Daily], [UpCountry Daily], Fooding, Transportation, Feul, Maintainence, Communication, [Other Allowance], Total) 
                    //            values (?,?,?,?,?,?,?,?,?,?,?,?,?,?)", conn);



                    cmd.Parameters.Add("EmployeeId", OleDbType.Integer, 4);
                    cmd.Parameters.Add("[Name]", OleDbType.VarChar, 200);
                    cmd.Parameters.Add("[Branch]", OleDbType.VarChar, 200);
                    cmd.Parameters.Add("[Department]", OleDbType.VarChar, 200);
                    cmd.Parameters.Add("[Designation]", OleDbType.VarChar, 200);


                    cmd.Parameters.Add("[PFInterest]", OleDbType.Currency, 4);
                    cmd.Parameters[5].IsNullable = true;
                    cmd.Parameters.Add("[CITInterest]", OleDbType.Currency, 4);
                    cmd.Parameters[6].IsNullable = true;

                    using (cmd)
                    {
                        foreach (var row in list)
                        {
                            cmd.Parameters[0].Value = row.EmployeeId;
                            cmd.Parameters[1].Value = row.Name;
                            cmd.Parameters[2].Value = row.Branch;
                            cmd.Parameters[3].Value = row.Department;
                            cmd.Parameters[4].Value = row.Designation;


                            if (row.PFInterest == null || row.PFInterest == 0)
                                cmd.Parameters[5].Value = DBNull.Value;
                            else
                                cmd.Parameters[5].Value = row.PFInterest;


                            if (row.CITInterest == null || row.CITInterest == 0)
                                cmd.Parameters[6].Value = DBNull.Value;
                            else
                                cmd.Parameters[6].Value = row.CITInterest;


                            cmd.ExecuteNonQuery();


                        }

                    }




                }
                ExcelGenerator.WriteFile(copyPath, Path.GetFileName(excelTemplatePath));
            }
            finally
            {
                try
                {
                    File.Delete(copyPath);
                }
                catch
                {
                }
            }

        }

        public static void ExportLoanRepayment(string excelTemplatePath, List<DAL.PEmployeeDeduction> list, PDeduction deduction)
        {
            string path = HttpContext.Current.Server.MapPath(excelTemplatePath);
            string copyPath = Path.GetDirectoryName(path) + "\\" + Guid.NewGuid() + ".xlsx";
            File.Copy(path, copyPath);

            string excelConnection = string.Format(ExcelConnectionString, copyPath);

            using (OleDbConnection conn = new OleDbConnection(excelConnection))
            {
                // Open the connection.
                conn.Open();


                // Create a new sheet in the Excel spreadsheet.
                OleDbCommand cmd = null;

                cmd = new OleDbCommand("INSERT INTO [Sheet1$]([Loan Name],EIN,[I No],[Employee Name],[Total Installment], [Monthly Repayment],[Advance Loan Amount],[Installment No],[Taken On Eng],[Start Date Eng],[End Date Eng],[No of Deductions],[Loan Account]) values (?,?,?,?,?,?,?,?,?,?,?,?,?)", conn);

                // Add the parameters.

                cmd.Parameters.Add("[Loan Name]", OleDbType.VarChar, 50);
                cmd.Parameters.Add("EIN", OleDbType.Integer, 4);
                cmd.Parameters.Add("[I No]", OleDbType.VarChar, 50);
                cmd.Parameters.Add("[Employee Name]", OleDbType.VarChar, 200);
                cmd.Parameters.Add("[Total Installment]", OleDbType.Integer, 4);
                cmd.Parameters.Add("[Monthly Repayment]", OleDbType.Currency, 4);

                cmd.Parameters.Add("[Advance Loan Amount]", OleDbType.Currency, 4);
                cmd.Parameters[6].IsNullable = true;
                cmd.Parameters.Add("[Installment No]", OleDbType.Integer, 4);
                cmd.Parameters.Add("[Taken On Eng]", OleDbType.VarChar, 50);
                cmd.Parameters[8].IsNullable = true;
                cmd.Parameters.Add("[Start Date Eng]", OleDbType.VarChar, 50);
                cmd.Parameters.Add("[End Date Eng]", OleDbType.VarChar, 50);
                cmd.Parameters[10].IsNullable = true;

                cmd.Parameters.Add("[No of Deductions]", OleDbType.Integer, 50);
                cmd.Parameters[11].IsNullable = true;

                cmd.Parameters.Add("[Loan Account]", OleDbType.VarChar, 50);



                // Changes the Rowstate()of each DataRow to Added,
                // so that OleDbDataAdapter will insert the rows.
                foreach (PEmployeeDeduction row in list)
                {
                    //dr.SetAdded();
                    cmd.Parameters[0].Value = deduction.Title;
                    cmd.Parameters[1].Value = row.EmployeeId;
                    cmd.Parameters[2].Value = row.INo == null ? "" : row.INo;
                    cmd.Parameters[3].Value = row.EmployeeName;

                    if (row.TotalInstallment == null)
                        cmd.Parameters[4].Value = DBNull.Value;
                    else
                        cmd.Parameters[4].Value = row.TotalInstallment;

                    if (row.Amount == null)
                        cmd.Parameters[5].Value = DBNull.Value;
                    else
                        cmd.Parameters[5].Value = row.Amount;

                    if (row.AdvanceLoanAmount == null)
                        cmd.Parameters[6].Value = DBNull.Value;
                    else
                        cmd.Parameters[6].Value = row.AdvanceLoanAmount;

                    if (row.InstallmentNo == null)
                        cmd.Parameters[7].Value = DBNull.Value;
                    else
                        cmd.Parameters[7].Value = row.InstallmentNo;

                    if (row.TakenOnEng == null)
                        cmd.Parameters[8].Value = DBNull.Value;
                    else
                        cmd.Parameters[8].Value = row.TakenOnEng.Value.ToString("yyyy/MM/dd");

                    if (row.StartingFromEng == null)
                        cmd.Parameters[9].Value = DBNull.Value;
                    else
                        cmd.Parameters[9].Value = row.StartingFromEng.Value.ToString("yyyy/MM/dd");


                    if (row.LastPaymentEng == null)
                        cmd.Parameters[10].Value = DBNull.Value;
                    else
                        cmd.Parameters[10].Value = row.LastPaymentEng.Value.ToString("yyyy/MM/dd");

                    if (row.NoOfInstallments == null)
                        cmd.Parameters[11].Value = DBNull.Value;
                    else
                        cmd.Parameters[11].Value = row.NoOfInstallments;

                    if (row.LoanAccount == null)
                        cmd.Parameters[12].Value = DBNull.Value;
                    else
                        cmd.Parameters[12].Value = row.LoanAccount;

                    cmd.ExecuteNonQuery();
                }

                // Insert the data into the Excel spreadsheet.
                //da.Update(dtSQL);

            }


            ExcelGenerator.WriteFile(copyPath, Path.GetFileName(excelTemplatePath));

        }

        public static void ExportEmployeeAward(string excelTemplatePath, List<int> employeeList, int cashAwardTypeId, int calculationTypeId, string date)
        {
            string path = HttpContext.Current.Server.MapPath(excelTemplatePath);
            string copyPath = Path.GetDirectoryName(path) + "\\" + Guid.NewGuid() + ".xlsx";
            File.Copy(path, copyPath);

            string awardName = "";
            string calculationType = "";

            if (calculationTypeId == (int)AwardCalculationEnum.MonthsBasicSalary)
                calculationType = "Months Basic Salary";
            else if (calculationTypeId == (int)AwardCalculationEnum.PercentageOfBasicSalary)
                calculationType = "Percentage Of BasicSalary";
            else if (calculationTypeId == (int)AwardCalculationEnum.FixedAmount)
                calculationType = "Fixed Amount";

            awardName = AwardManager.GetEmployeeCashAwardTypeById(cashAwardTypeId).Name;

            string excelConnection = string.Format(ExcelConnectionString, copyPath);


            using (OleDbConnection conn = new OleDbConnection(excelConnection))
            {
                // Open the connection.
                conn.Open();


                // Create a new sheet in the Excel spreadsheet.
                OleDbCommand cmd = null;

                cmd = new OleDbCommand("INSERT INTO [Sheet1$](EIN,[Employee Name],[Position],[Branch],[Award Name],[Type],[Amount/Rate],[Note],[Date]) values (?,?,?,?,?,?,?,?,?)", conn);

                // Add the parameters.

                cmd.Parameters.Add("EIN", OleDbType.Integer, 4);
                cmd.Parameters.Add("[Employee Name]", OleDbType.VarChar, 200);
                cmd.Parameters.Add("[Position]", OleDbType.VarChar, 50);
                cmd.Parameters.Add("[Branch]", OleDbType.VarChar, 50);

                cmd.Parameters.Add("[Award Name]", OleDbType.VarChar, 100);

                cmd.Parameters.Add("[Type]", OleDbType.VarChar, 50);
                cmd.Parameters.Add("[Amount/Rate]", OleDbType.Currency, 4);

                cmd.Parameters.Add("[Note]", OleDbType.VarChar, 200);

                cmd.Parameters.Add("[Date]", OleDbType.VarChar, 50);

                cmd.Parameters[6].IsNullable = true;
                cmd.Parameters[7].IsNullable = true;
                cmd.Parameters[8].IsNullable = true;

                int month = 0;
                int year = 0;

                CustomDate todayDate = CustomDate.GetCustomDateFromString(date, IsEnglish);
                month = todayDate.Month;
                year = todayDate.Year;

                foreach (int item in employeeList)
                {
                    cmd.Parameters[0].Value = item.ToString();

                    EEmployee employee = EmployeeManager.GetEmployeeById(item);

                    cmd.Parameters[1].Value = employee.Name;

                    cmd.Parameters[2].Value = employee.EDesignation.Name;
                    cmd.Parameters[3].Value = employee.Branch.Name;
                    cmd.Parameters[4].Value = awardName;
                    cmd.Parameters[5].Value = calculationType;

                    EmployeeCashAward empCashAward = AwardManager.GetEmployeeCashAwardById(item, cashAwardTypeId, month, year);
                    if (empCashAward != null)
                    {
                        if (empCashAward.AmountOrRate != null)
                            cmd.Parameters[6].Value = empCashAward.AmountOrRate;
                        else
                            cmd.Parameters[6].Value = DBNull.Value;

                        cmd.Parameters[7].Value = empCashAward.Notes;
                    }
                    else
                    {
                        cmd.Parameters[6].Value = DBNull.Value;
                        cmd.Parameters[7].Value = DBNull.Value;
                    }

                    cmd.Parameters[8].Value = date;

                    cmd.ExecuteNonQuery();

                }
            }

            ExcelGenerator.WriteFile(copyPath, Path.GetFileName(excelTemplatePath));
        }

        public static void ExportSILoanImport(string excelTemplatePath, List<DAL.PEmployeeDeduction> list, PDeduction deduction)
        {
            string path = HttpContext.Current.Server.MapPath(excelTemplatePath);
            string copyPath = Path.GetDirectoryName(path) + "\\" + Guid.NewGuid() + ".xlsx";
            File.Copy(path, copyPath);

            string excelConnection = string.Format(ExcelConnectionString, copyPath);

            using (OleDbConnection conn = new OleDbConnection(excelConnection))
            {
                // Open the connection.
                conn.Open();

                // Create a new sheet in the Excel spreadsheet.
                OleDbCommand cmd = null;

                cmd = new OleDbCommand("INSERT INTO [Sheet1$](EIN,[Employee Name],[Loan Type],[Loan Amount], [Loan Taken On Eng],[Interest Rate],[Market Rate],[No of Installments],[Payment Term],[Deduction Start Date Eng]) values (?,?,?,?,?,?,?,?,?,?)", conn);

                // Add the parameters.

                cmd.Parameters.Add("EIN", OleDbType.Integer, 4);
                cmd.Parameters.Add("[Employee Name]", OleDbType.VarChar, 200);
                cmd.Parameters.Add("[Loan Type]", OleDbType.VarChar, 200);
                cmd.Parameters.Add("[Loan Amount]", OleDbType.Currency, 4);
                cmd.Parameters[3].IsNullable = true;

                cmd.Parameters.Add("[Loan Taken On Eng]", OleDbType.VarChar, 50);

                cmd.Parameters.Add("[Interest Rate]", OleDbType.Currency, 4);
                cmd.Parameters[5].IsNullable = true;

                cmd.Parameters.Add("[Market Rate]", OleDbType.Currency, 4);
                cmd.Parameters[6].IsNullable = true;

                cmd.Parameters.Add("[No of Installments]", OleDbType.Integer, 4);
                cmd.Parameters[7].IsNullable = true;

                cmd.Parameters.Add("[Payment Term]", OleDbType.VarChar, 50);

                cmd.Parameters.Add("[Deduction Start Date Eng]", OleDbType.VarChar, 50);

                // Changes the Rowstate()of each DataRow to Added,
                // so that OleDbDataAdapter will insert the rows.
                foreach (PEmployeeDeduction row in list)
                {
                    //dr.SetAdded();

                    cmd.Parameters[0].Value = row.EmployeeId;
                    cmd.Parameters[1].Value = row.EmployeeName;
                    cmd.Parameters[2].Value = deduction.Title;

                    if (row.AdvanceLoanAmount == null)
                        cmd.Parameters[3].Value = DBNull.Value;
                    else
                        cmd.Parameters[3].Value = row.AdvanceLoanAmount;

                    if (row.TakenOnEng == null)
                        cmd.Parameters[4].Value = DBNull.Value;
                    else
                        cmd.Parameters[4].Value = row.TakenOnEng.Value.ToString("yyyy/MM/dd");

                    if (row.InterestRate == null)
                        cmd.Parameters[5].Value = DBNull.Value;
                    else
                        cmd.Parameters[5].Value = row.InterestRate;

                    if (row.MarketRate == null)
                        cmd.Parameters[6].Value = DBNull.Value;
                    else
                        cmd.Parameters[6].Value = row.MarketRate;

                    if (row.ToBePaidOver == null)
                        cmd.Parameters[7].Value = DBNull.Value;
                    else
                        cmd.Parameters[7].Value = row.ToBePaidOver;

                    cmd.Parameters[8].Value = "Monthly";

                    if (row.StartingFromEng == null)
                        cmd.Parameters[9].Value = DBNull.Value;
                    else
                        cmd.Parameters[9].Value = row.StartingFromEng.Value.ToString("yyyy/MM/dd");


                    cmd.ExecuteNonQuery();
                }

                // Insert the data into the Excel spreadsheet.
                //da.Update(dtSQL);

            }
            ExcelGenerator.WriteFile(copyPath, Path.GetFileName(excelTemplatePath));
        }

        public static void ExportEmpOvertime(string excelTemplatePath, string fileName)
        {
            string path = HttpContext.Current.Server.MapPath(excelTemplatePath);
            string copyPath = Path.GetDirectoryName(path) + "\\" + Guid.NewGuid() + ".xls";
            File.Copy(path, copyPath);

            // List<TextValue> list = LeaveAttendanceManager.GetEmployeeListForLeaveAssign(); ;

            string excelConnection = string.Format(ExcelConnectionString, copyPath);

            //ExcelGenerator.WriteFile(copyPath, Path.GetFileName(excelTemplatePath));
            ExcelGenerator.WriteFile(copyPath, fileName);

        }

        public static void ExportSupOvertime(string excelTemplatePath, string fileName)
        {
            string path = HttpContext.Current.Server.MapPath(excelTemplatePath);
            string copyPath = Path.GetDirectoryName(path) + "\\" + Guid.NewGuid() + ".xls";
            File.Copy(path, copyPath);

            List<TextValue> list = LeaveAttendanceManager.GetEmployeeListForLeaveAssignWithID();

            string[] empList = list.Select(x => x.Text).ToArray();

            string excelConnection = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + copyPath + ";Extended Properties='Excel 12.0 Xml;HDR=Yes'";

            try
            {
                using (OleDbConnection conn = new OleDbConnection(excelConnection))
                {
                    // Open the connection.
                    conn.Open();

                    // Create a new sheet in the Excel spreadsheet.
                    OleDbCommand cmd = new OleDbCommand("INSERT INTO [Sheet1$]([Date],Employee,[Start Time],[End Time],Reason ) values (?,?,?,?,?)", conn);
                    cmd.Parameters.Add("Date", OleDbType.VarChar, 50);
                    cmd.Parameters.Add("Employee", OleDbType.VarChar, 100);
                    cmd.Parameters.Add("Start Time", OleDbType.VarChar, 100);
                    cmd.Parameters.Add("End Time", OleDbType.VarChar, 100);

                    WriteIntoSheet(conn, "Employees", empList);
                }

                //ExcelGenerator.WriteFile(copyPath, Path.GetFileName(excelTemplatePath));
                ExcelGenerator.WriteFile(copyPath, fileName);
            }
            finally
            {
                try
                {
                    File.Delete(copyPath);
                }
                catch
                {
                }
            }
        }




        public static void WriteEmployeeManagersExcelCopy(string excelTemplatePath, string[] GetProjectList, List<EEmployee> empList)
        {
            string path = HttpContext.Current.Server.MapPath(excelTemplatePath);
            string copyPath = Path.GetDirectoryName(path) + "\\" + Guid.NewGuid() + ".xlsx";
            File.Copy(path, copyPath);


            string excelConnection = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + copyPath + ";Extended Properties='Excel 12.0 Xml;HDR=Yes'";

            try
            {

                using (OleDbConnection conn = new OleDbConnection(excelConnection))
                {
                    // Open the connection.
                    conn.Open();

                    // Create a new sheet in the Excel spreadsheet.
                    OleDbCommand cmd = new OleDbCommand("INSERT INTO [Sheet1$](EIN,Employee,[Department],[Manager] ) values (?,?,?,?)", conn);


                    // Add the parameters.

                    cmd.Parameters.Add("EIN", OleDbType.Integer, 4);
                    cmd.Parameters.Add("Employee", OleDbType.VarChar, 100);


                    cmd.Parameters.Add("[Department]", OleDbType.VarChar, 100);
                    cmd.Parameters.Add("[Manager]", OleDbType.VarChar, 100);


                    // Changes the Rowstate()of each DataRow to Added,
                    // so that OleDbDataAdapter will insert the rows.
                    foreach (var row in empList)
                    {
                        //dr.SetAdded();
                        cmd.Parameters[0].Value = row.EmployeeId;
                        cmd.Parameters[1].Value = row.Name;

                        cmd.Parameters[2].Value = row.Department.Name;

                        cmd.Parameters[3].Value = row.LastName;//row.Manager == null ? "" : row.Manager;

                        cmd.ExecuteNonQuery();
                    }


                    WriteIntoSheet(conn, "ManagerList", GetProjectList);
                }

                ExcelGenerator.WriteFile(copyPath, Path.GetFileName(excelTemplatePath));
            }
            finally
            {
                try
                {
                    File.Delete(copyPath);
                }
                catch
                {
                }
            }
        }


        public static void ExportTimeAttendance(string excelTemplatePath, string fileName)
        {
            string path = HttpContext.Current.Server.MapPath(excelTemplatePath);
            string copyPath = Path.GetDirectoryName(path) + "\\" + Guid.NewGuid() + ".xls";
            File.Copy(path, copyPath);

            List<TextValue> list = LeaveAttendanceManager.GetEmployeeListForLeaveAssignWithID();

            string[] empList = list.Select(x => x.Text).ToArray();

            string excelConnection = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + copyPath + ";Extended Properties='Excel 12.0 Xml;HDR=Yes'";

            try
            {
                using (OleDbConnection conn = new OleDbConnection(excelConnection))
                {
                    // Open the connection.
                    conn.Open();

                    // Create a new sheet in the Excel spreadsheet.
                    OleDbCommand cmd = new OleDbCommand("INSERT INTO [Sheet1$]([Date],Employee,[Start Time],[In Note],[End Time],[Out Note]) values (?,?,?,?,?,?)", conn);
                    cmd.Parameters.Add("Date", OleDbType.VarChar, 50);
                    cmd.Parameters.Add("Employee", OleDbType.VarChar, 100);
                    cmd.Parameters.Add("Start Time", OleDbType.VarChar, 100);
                    cmd.Parameters.Add("In Note", OleDbType.VarChar, 500);
                    cmd.Parameters.Add("End Time", OleDbType.VarChar, 100);
                    cmd.Parameters.Add("Out Note", OleDbType.VarChar, 500);

                    WriteIntoSheet(conn, "Employees", empList);
                }

                //ExcelGenerator.WriteFile(copyPath, Path.GetFileName(excelTemplatePath));
                ExcelGenerator.WriteFile(copyPath, fileName);
            }
            finally
            {
                try
                {
                    File.Delete(copyPath);
                }
                catch
                {
                }
            }
        }

        public static void ExportOTTimeAttendance(string excelTemplatePath, string fileName)
        {
            string path = HttpContext.Current.Server.MapPath(excelTemplatePath);
            string copyPath = Path.GetDirectoryName(path) + "\\" + Guid.NewGuid() + ".xls";
            File.Copy(path, copyPath);

            List<TextValue> list = LeaveAttendanceManager.GetEmployeeListForLeaveAssignWithID();

            string[] empList = list.Select(x => x.Text).ToArray();

            string excelConnection = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + copyPath + ";Extended Properties='Excel 12.0 Xml;HDR=Yes'";

            try
            {


                //ExcelGenerator.WriteFile(copyPath, Path.GetFileName(excelTemplatePath));
                ExcelGenerator.WriteFile(copyPath, fileName);
            }
            finally
            {
                try
                {
                    File.Delete(copyPath);
                }
                catch
                {
                }
            }
        }
        public static void ExportAdjustArrearIncrement(string excelTemplatePath, List<AdjustArrearIncrement> list, bool hideColumn)
        {
            string path = HttpContext.Current.Server.MapPath(excelTemplatePath);
            string copyPath = Path.GetDirectoryName(path) + "\\" + Guid.NewGuid() + ".xlsx";
            File.Copy(path, copyPath);

            string excelConnection = string.Format(ExcelConnectionString, copyPath);



            using (OleDbConnection conn = new OleDbConnection(excelConnection))
            {
                // Open the connection.
                conn.Open();

                StringBuilder sql = new StringBuilder("Create Table [Sheet1$]( EIN int,Employee varchar(100), [Designation] varchar(100),[Status] varchar(100),[Current Salary] money,[New Salary] money,[Total Increase] money,[Total PF] money)");

                OleDbCommand cmd1 = new OleDbCommand(sql.ToString(), conn);
                cmd1.ExecuteNonQuery();



                // Create a new sheet in the Excel spreadsheet.
                OleDbCommand cmd = new OleDbCommand("INSERT INTO [Sheet1$](EIN,Employee, Designation ,[Status] ,[Current Salary],[New Salary],[Total Increase] ,[Total PF]) values (?,?,?,?,?,?,?,?)", conn);


                // Add the parameters.

                cmd.Parameters.Add("EIN", OleDbType.Integer, 4);
                cmd.Parameters.Add("Employee", OleDbType.VarChar, 100);


                cmd.Parameters.Add("Designation", OleDbType.VarChar, 100);
                cmd.Parameters.Add("[Status]", OleDbType.VarChar, 100);

                cmd.Parameters.Add("[Current Salary]", OleDbType.Currency, 0);
                cmd.Parameters.Add("[New Salary]", OleDbType.Currency, 0);

                cmd.Parameters.Add("[Total Increase]", OleDbType.Currency, 0);
                cmd.Parameters[5].IsNullable = true;


                cmd.Parameters.Add("[Total PF]", OleDbType.Currency, 0);
                cmd.Parameters[6].IsNullable = true;

                // Changes the Rowstate()of each DataRow to Added,
                // so that OleDbDataAdapter will insert the rows.
                foreach (AdjustArrearIncrement row in list)
                {
                    //dr.SetAdded();
                    cmd.Parameters[0].Value = row.EmployeeId;
                    cmd.Parameters[1].Value = row.Name;
                    cmd.Parameters[2].Value = row.Designation;// == null ? DBNull.Value : row.Days_At_Palati;
                    cmd.Parameters[3].Value = row.Status;



                    cmd.Parameters[4].Value = DBNull.Value;
                    if (row.CurrentSalary != null)
                        cmd.Parameters[4].Value = row.CurrentSalary;
                    //    cmd.Parameters[4].Value = row.AdjustmentAmount;

                    cmd.Parameters[5].Value = DBNull.Value;
                    if (row.NewSalary != null)
                        cmd.Parameters[5].Value = row.NewSalary;

                    cmd.Parameters[6].Value = DBNull.Value;
                    if (row.TotalIncrease != null)
                        cmd.Parameters[6].Value = row.TotalIncrease;
                    //    cmd.Parameters[4].Value = row.AdjustmentAmount;

                    cmd.Parameters[7].Value = DBNull.Value;
                    if (row.TotalPF != null)
                        cmd.Parameters[7].Value = row.TotalPF;

                    cmd.ExecuteNonQuery();
                }

                // Insert the data into the Excel spreadsheet.
                //da.Update(dtSQL);

            }


            ExcelGenerator.WriteFile(copyPath, Path.GetFileName(excelTemplatePath));
        }

        public static void ExportAdjustArrearIncrementPercentType(string excelTemplatePath, List<AdjustArrearIncrement> list, bool hideColumn)
        {
            string path = HttpContext.Current.Server.MapPath(excelTemplatePath);
            string copyPath = Path.GetDirectoryName(path) + "\\" + Guid.NewGuid() + ".xlsx";
            File.Copy(path, copyPath);

            string excelConnection = string.Format(ExcelConnectionString, copyPath);



            using (OleDbConnection conn = new OleDbConnection(excelConnection))
            {
                // Open the connection.
                conn.Open();

                StringBuilder sql = new StringBuilder("Create Table [Sheet1$]( EIN int,Employee varchar(100), [Designation] varchar(100),[Status] varchar(100),[Old Salary] money,[New Salary] money,[Diff Salary] money,Rate money,[Total Increase] money,[Total PF] money)");

                OleDbCommand cmd1 = new OleDbCommand(sql.ToString(), conn);
                cmd1.ExecuteNonQuery();



                // Create a new sheet in the Excel spreadsheet.
                OleDbCommand cmd = new OleDbCommand("INSERT INTO [Sheet1$](EIN,Employee, Designation ,[Status] ,[Old Salary],[New Salary],[Diff Salary],Rate,[Total Increase] ,[Total PF]) values (?,?,?,?,?,?,?,?,?,?)", conn);


                // Add the parameters.

                cmd.Parameters.Add("EIN", OleDbType.Integer, 4);
                cmd.Parameters.Add("Employee", OleDbType.VarChar, 100);


                cmd.Parameters.Add("Designation", OleDbType.VarChar, 100);
                cmd.Parameters.Add("[Status]", OleDbType.VarChar, 100);

                cmd.Parameters.Add("[Old Salary]", OleDbType.Currency, 0);
                cmd.Parameters.Add("[New Salary]", OleDbType.Currency, 0);
                cmd.Parameters.Add("[Diff Salary]", OleDbType.Currency, 0);
                cmd.Parameters.Add("[Rate]", OleDbType.Currency, 0);

                cmd.Parameters.Add("[Total Increase]", OleDbType.Currency, 0);



                cmd.Parameters.Add("[Total PF]", OleDbType.Currency, 0);


                // Changes the Rowstate()of each DataRow to Added,
                // so that OleDbDataAdapter will insert the rows.
                foreach (AdjustArrearIncrement row in list)
                {
                    //dr.SetAdded();
                    cmd.Parameters[0].Value = row.EmployeeId;
                    cmd.Parameters[1].Value = row.Name;
                    cmd.Parameters[2].Value = row.Designation;// == null ? DBNull.Value : row.Days_At_Palati;
                    cmd.Parameters[3].Value = row.Status;

                    cmd.Parameters[4].Value = row.OldAmount;
                    cmd.Parameters[5].Value = row.NewAmount;


                    //cmd.Parameters[4].Value = DBNull.Value;
                    //if (row.CurrentSalary != null)
                    cmd.Parameters[6].Value = row.CurrentSalary;
                    //    cmd.Parameters[4].Value = row.AdjustmentAmount;

                    //cmd.Parameters[5].Value = DBNull.Value;
                    //if (row.NewSalary != null)
                    cmd.Parameters[7].Value = row.NewSalary;

                    //cmd.Parameters[6].Value = DBNull.Value;
                    //if (row.TotalIncrease != null)
                    cmd.Parameters[8].Value = row.TotalIncrease;
                    //    cmd.Parameters[4].Value = row.AdjustmentAmount;

                    //cmd.Parameters[7].Value = DBNull.Value;
                    //if (row.TotalPF != null)
                    cmd.Parameters[9].Value = row.TotalPF;

                    cmd.ExecuteNonQuery();
                }

                // Insert the data into the Excel spreadsheet.
                //da.Update(dtSQL);

            }


            ExcelGenerator.WriteFile(copyPath, Path.GetFileName(excelTemplatePath));
        }

        public static void ExportEmployeeShiftType(string excelTemplatePath)
        {
            string path = HttpContext.Current.Server.MapPath(excelTemplatePath);
            string copyPath = Path.GetDirectoryName(path) + "\\" + Guid.NewGuid() + ".xlsx";
            File.Copy(path, copyPath);

            string excelConnection = string.Format(ExcelConnectionString, copyPath);


            using (OleDbConnection conn = new OleDbConnection(excelConnection))
            {
                // Open the connection.
                conn.Open();

                // Create a new sheet in the Excel spreadsheet.
                OleDbCommand cmd = new OleDbCommand("INSERT INTO [Sheet1$]([EIN], [Employee Name], [Branch], [Department],[Type]) values (?,?,?,?,?)", conn);


                // Add the parameters.

                cmd.Parameters.Add("EIN", OleDbType.Integer, 4);
                cmd.Parameters.Add("[Employee Name]", OleDbType.VarChar, 50);
                cmd.Parameters.Add("[Branch]", OleDbType.VarChar, 50);
                cmd.Parameters.Add("[Department]", OleDbType.VarChar, 50);
                cmd.Parameters.Add("[Type]", OleDbType.VarChar, 50);
                cmd.Parameters["[Type]"].IsNullable = true;

                // Changes the Rowstate()of each DataRow to Added,
                // so that OleDbDataAdapter will insert the rows.                


                List<GetEmployee_ShiftsResult> list = ShiftManager.GetEmployee_Shifts(-1, -1, -1, null, -1, -1);

                foreach (GetEmployee_ShiftsResult row in list)
                {
                    cmd.Parameters[0].Value = row.EmployeeId;
                    cmd.Parameters[1].Value = row.Name;
                    cmd.Parameters[2].Value = row.BranchName;
                    cmd.Parameters[3].Value = row.DepartmentName;

                    if (row.ShiftType == null)
                        cmd.Parameters[4].Value = DBNull.Value;
                    else
                    {
                        if (row.ShiftType)
                            cmd.Parameters[4].Value = "Changable";
                        else
                            cmd.Parameters[4].Value = "Fixed";
                    }

                    cmd.ExecuteNonQuery();
                }


                // Insert the data into the Excel spreadsheet.
                //da.Update(dtSQL);

                List<string> typeList = new List<string>();
                typeList.Add("Fixed");
                typeList.Add("Changable");

                WriteIntoSheet(conn, "TypeList", typeList.ToArray());
            }


            ExcelGenerator.WriteFile(copyPath, Path.GetFileName(excelTemplatePath));
        }


        public static void ExportEmployeeAssignedShift(string excelTemplatePath)
        {
            string path = HttpContext.Current.Server.MapPath(excelTemplatePath);
            string copyPath = Path.GetDirectoryName(path) + "\\" + Guid.NewGuid() + ".xlsx";
            File.Copy(path, copyPath);

            string excelConnection = string.Format(ExcelConnectionString, copyPath);


            using (OleDbConnection conn = new OleDbConnection(excelConnection))
            {
                // Open the connection.
                conn.Open();

                // Create a new sheet in the Excel spreadsheet.
                OleDbCommand cmd = new OleDbCommand("INSERT INTO [Sheet1$]([ID],[EIN], [Name], [Branch], [Department], [Sub Department],[Shift],[From],[To]) values (?,?,?,?,?,?,?,?,?)", conn);


                // Add the parameters.
                cmd.Parameters.Add("[ID]", OleDbType.Integer, 4);
                cmd.Parameters.Add("[EIN]", OleDbType.Integer, 4);
                cmd.Parameters.Add("[Name]", OleDbType.VarChar, 50);
                cmd.Parameters.Add("[Branch]", OleDbType.VarChar, 50);
                cmd.Parameters.Add("[Department]", OleDbType.VarChar, 50);
                cmd.Parameters.Add("[Sub Department]", OleDbType.VarChar, 50);
                cmd.Parameters.Add("[Shift]", OleDbType.VarChar, 50);
                cmd.Parameters.Add("[From]", OleDbType.VarChar, 50);
                cmd.Parameters.Add("[To]", OleDbType.VarChar, 50);

                // Changes the Rowstate()of each DataRow to Added,
                // so that OleDbDataAdapter will insert the rows.                


                //List<GetEmployee_ShiftsResult> list = ShiftManager.GetWorkShiftById(-1, -1, -1, null, -1);



                List<DAL.EWorkShiftHistory> list = PayrollDataContext.EWorkShiftHistories.ToList();


                foreach (EWorkShiftHistory row in list)
                {
                    cmd.Parameters[0].Value = row.WorkShiftHistoryId;
                    cmd.Parameters[1].Value = row.EmployeeId.Value;
                    EEmployee _dbEmployee = EmployeeManager.GetEmployeeById(row.EmployeeId.Value);
                    cmd.Parameters[2].Value = _dbEmployee.Name;
                    cmd.Parameters[3].Value = _dbEmployee.Branch.Name == null ? "" : _dbEmployee.Branch.Name;
                    cmd.Parameters[4].Value = _dbEmployee.Department.Name == null ? "" : _dbEmployee.Department.Name;
                    cmd.Parameters[5].Value = _dbEmployee.SubDepartmentValue == null ? "" : _dbEmployee.SubDepartmentValue;
                    EWorkShift dbEWorkShift = ShiftManager.GetWorkShiftById(row.WorkShiftId.Value);
                    cmd.Parameters[6].Value = dbEWorkShift.Name == null ? "" : dbEWorkShift.Name;
                    cmd.Parameters[7].Value = row.FromDateEng.Value.Date;
                    if (row.ToDateEng == null)
                        cmd.Parameters[8].Value = DBNull.Value;
                    else
                        cmd.Parameters[8].Value = row.ToDateEng.Value.Date;

                    cmd.ExecuteNonQuery();
                }


                // Insert the data into the Excel spreadsheet.
                //da.Update(dtSQL);
                // WriteIntoSheet(conn, "TypeList", typeList.ToArray());
            }


            ExcelGenerator.WriteFile(copyPath, Path.GetFileName(excelTemplatePath));
        }


        public static void ExportEmpListForOT(string excelTemplatePath, string fileName)
        {
            string path = HttpContext.Current.Server.MapPath(excelTemplatePath);
            string copyPath = Path.GetDirectoryName(path) + "\\" + Guid.NewGuid() + ".xls";
            File.Copy(path, copyPath);


            List<EEmployee> list = EmployeeManager.GetAllEmployees();

            string[] empList = list.Select(x => x.Name).ToArray();

            string excelConnection = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + copyPath + ";Extended Properties='Excel 12.0 Xml;HDR=Yes'";

            try
            {
                using (OleDbConnection conn = new OleDbConnection(excelConnection))
                {
                    // Open the connection.
                    conn.Open();

                    // Create a new sheet in the Excel spreadsheet.
                    OleDbCommand cmd = new OleDbCommand("INSERT INTO [Sheet1$]([Date],Employee,[Start Time],[End Time],Reason ) values (?,?,?,?,?)", conn);
                    cmd.Parameters.Add("Date", OleDbType.VarChar, 50);
                    cmd.Parameters.Add("Employee", OleDbType.VarChar, 100);
                    cmd.Parameters.Add("Start Time", OleDbType.VarChar, 100);
                    cmd.Parameters.Add("End Time", OleDbType.VarChar, 100);

                    WriteIntoSheet(conn, "Employees", empList);
                }

                //ExcelGenerator.WriteFile(copyPath, Path.GetFileName(excelTemplatePath));
                ExcelGenerator.WriteFile(copyPath, fileName);
            }
            finally
            {
                try
                {
                    File.Delete(copyPath);
                }
                catch
                {
                }
            }
        }

        public static void ExportDesignationHist(string excelTemplatePath, string fileName)
        {
            string path = HttpContext.Current.Server.MapPath(excelTemplatePath);
            string copyPath = Path.GetDirectoryName(path) + "\\" + Guid.NewGuid() + ".xls";
            File.Copy(path, copyPath);

            List<EDesignation> listDesignations = NewHRManager.GetDesignations();
            string[] designationsWithLevel = listDesignations.OrderBy(x => x.LevelName).Select(x => x.LevelName).ToArray();

            string excelConnection = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + copyPath + ";Extended Properties='Excel 12.0 Xml;HDR=Yes'";

            try
            {
                using (OleDbConnection conn = new OleDbConnection(excelConnection))
                {
                    // Open the connection.
                    conn.Open();

                    // Create a new sheet in the Excel spreadsheet.
                    OleDbCommand cmd = new OleDbCommand("INSERT INTO [Sheet1$](EIN,[Old Designation],[New Designation],[Nepali Date]) values (?,?,?,?)", conn);
                    cmd.Parameters.Add("EIN", OleDbType.VarChar, 50);
                    cmd.Parameters.Add("Old Designation", OleDbType.VarChar, 100);
                    cmd.Parameters.Add("New Designation", OleDbType.VarChar, 100);
                    cmd.Parameters.Add("Nepali Date", OleDbType.VarChar, 100);

                    WriteIntoSheet(conn, "Designations", designationsWithLevel);
                }

                //ExcelGenerator.WriteFile(copyPath, Path.GetFileName(excelTemplatePath));
                ExcelGenerator.WriteFile(copyPath, fileName);
            }
            finally
            {
                try
                {
                    File.Delete(copyPath);
                }
                catch
                {
                }
            }
        }


        public static void ExportWeightTargetAchievement(string excelTemplatePath, List<TargetOtherDetail> list)
        {
            string path = HttpContext.Current.Server.MapPath(excelTemplatePath);
            string copyPath = Path.GetDirectoryName(path) + "\\" + Guid.NewGuid() + ".xlsx";
            File.Copy(path, copyPath);

            string excelConnection = string.Format(ExcelConnectionString, copyPath);

            using (OleDbConnection conn = new OleDbConnection(excelConnection))
            {
                // Open the connection.
                conn.Open();


                // Create a new sheet in the Excel spreadsheet.
                OleDbCommand cmd = new OleDbCommand("INSERT INTO [Sheet1$](EIN,Employee,Weight,[Target Assigned],Achievement) values (?,?,?,?,?)", conn);

                // Add the parameters.

                cmd.Parameters.Add("EIN", OleDbType.Integer, 4);
                cmd.Parameters.Add("Employee", OleDbType.VarChar, 200);

                int index = 2;

                cmd.Parameters.Add("Weight", OleDbType.Double, 4);
                cmd.Parameters[index++].IsNullable = true;

                cmd.Parameters.Add("Target Assigned", OleDbType.VarChar, 200);
                cmd.Parameters[index++].IsNullable = true;



                cmd.Parameters.Add("Achievement", OleDbType.VarChar, 200);
                cmd.Parameters[index++].IsNullable = true;

                foreach (TargetOtherDetail row in list)
                {
                    //dr.SetAdded();
                    cmd.Parameters[0].Value = row.EmployeeId;
                    cmd.Parameters[1].Value = row.Name;

                    index = 2;

                    if (row.Weight == 0) cmd.Parameters[index++].Value = DBNull.Value;
                    else cmd.Parameters[index++].Value = row.Weight;

                    if (row.TargetTextValue == "") cmd.Parameters[index++].Value = DBNull.Value;
                    else cmd.Parameters[index++].Value = row.TargetTextValue;

                    if (row.AchievementTextValue == "") cmd.Parameters[index++].Value = DBNull.Value;
                    else cmd.Parameters[index++].Value = row.AchievementTextValue;

                    cmd.ExecuteNonQuery();
                }

                // Insert the data into the Excel spreadsheet.
                //da.Update(dtSQL);

            }


            ExcelGenerator.WriteFile(copyPath, Path.GetFileName(excelTemplatePath));
        }


        public static void ExportEmployeeAppraisalRolloutGroup(string excelTemplatePath, List<DAL.GetEmployeeAppraisalRolloutGroupListResult> list, List<DAL.AppraisalRolloutGroup> groupList)
        {
            string path = HttpContext.Current.Server.MapPath(excelTemplatePath);
            string copyPath = Path.GetDirectoryName(path) + "\\" + Guid.NewGuid() + ".xlsx";
            File.Copy(path, copyPath);

            string excelConnection = string.Format(ExcelConnectionString, copyPath);
            using (OleDbConnection conn = new OleDbConnection(excelConnection))
            {
                // Open the connection.
                conn.Open();
                // Create a new sheet in the Excel spreadsheet.
                OleDbCommand cmd = new OleDbCommand("INSERT INTO [Sheet1$](EIN,Employee,[Group Name]) values (?,?,?)", conn);
                // Add the parameters.
                cmd.Parameters.Add("EIN", OleDbType.Integer, 4);
                cmd.Parameters.Add("Employee", OleDbType.VarChar, 200);
                cmd.Parameters.Add("[Group Name]", OleDbType.VarChar, 200);
                cmd.Parameters[0].IsNullable = true;
                foreach (GetEmployeeAppraisalRolloutGroupListResult row in list)
                {
                    //dr.SetAdded();
                    cmd.Parameters[0].Value = row.EmployeeId;
                    cmd.Parameters[1].Value = row.Name;
                    cmd.Parameters[2].Value = row.GroupName == null ? "" : row.GroupName;
                    cmd.ExecuteNonQuery();
                }
                // Insert the data into the Excel spreadsheet.
                //da.Update(dtSQL);
                using (OleDbCommand cmd1 = new OleDbCommand("INSERT INTO [RolloutGroups$](Name) values (?)", conn))
                {
                    cmd1.Parameters.Add("Name", OleDbType.VarChar, 200);

                    foreach (var row in groupList)
                    {
                        cmd1.Parameters[0].Value = row.Name;
                        cmd1.ExecuteNonQuery();
                    }
                }
            }
            ExcelGenerator.WriteFile(copyPath, Path.GetFileName(excelTemplatePath));
        }

        public static void ExportEmployeeSeniority(string excelTemplatePath, List<DAL.GetEmployeeSeniorityListResult> list)
        {
            string path = HttpContext.Current.Server.MapPath(excelTemplatePath);
            string copyPath = Path.GetDirectoryName(path) + "\\" + Guid.NewGuid() + ".xlsx";
            File.Copy(path, copyPath);

            string excelConnection = string.Format(ExcelConnectionString, copyPath);
            using (OleDbConnection conn = new OleDbConnection(excelConnection))
            {
                // Open the connection.
                conn.Open();
                // Create a new sheet in the Excel spreadsheet.
                OleDbCommand cmd = new OleDbCommand("INSERT INTO [Sheet1$](EIN,Employee,Seniority,Designation) values (?,?,?,?)", conn);
                // Add the parameters.
                cmd.Parameters.Add("EIN", OleDbType.Integer, 4);
                cmd.Parameters.Add("Employee", OleDbType.VarChar, 200);
                cmd.Parameters.Add("Seniority", OleDbType.VarChar, 50);
                cmd.Parameters.Add("Seniority", OleDbType.VarChar, 500);
                cmd.Parameters[0].IsNullable = true;
                cmd.Parameters[2].IsNullable = true;
                foreach (GetEmployeeSeniorityListResult row in list)
                {
                    cmd.Parameters[0].Value = row.EmployeeId;
                    cmd.Parameters[1].Value = row.Name;
                    cmd.Parameters[2].Value = row.Seniority == null ? "" : row.Seniority.ToString();
                    cmd.Parameters[3].Value = row.Designation == null ? "" : row.Designation.ToString();
                    cmd.ExecuteNonQuery();
                }
            }
            ExcelGenerator.WriteFile(copyPath, Path.GetFileName(excelTemplatePath));
        }


        public static void ExportEmployeeAddress(string excelTemplatePath,
            List<DAL.GetEmployeeAddressResult> list,
            List<CountryName> CountryList,
            List<ZoneList> ZoneList,
            List<DistrictList> DistrictList
            )
        {
            string path = HttpContext.Current.Server.MapPath(excelTemplatePath);
            string copyPath = Path.GetDirectoryName(path) + "\\" + Guid.NewGuid() + ".xlsx";
            File.Copy(path, copyPath);

            string excelConnection = string.Format(ExcelConnectionString, copyPath);
            using (OleDbConnection conn = new OleDbConnection(excelConnection))
            {
                // Open the connection.
                conn.Open();
                // Create a new sheet in the Excel spreadsheet.
                OleDbCommand cmd = new OleDbCommand("INSERT INTO [Sheet1$](EIN,[Employee Name],[Permanent Country],[Permanent Zone],[Permanent District],[Permanent VDC/Municipality],[Permanent Street/Colony],[Permanent Ward No],[Permanent House No],[Permanent Locality],[Permanent State],[Permanent Zip Code],[Temporary Country],[Temporary Zone],[Temporary District],[Temporary VDC/Municipality],[Temporary Street/Colony],[Temporary Ward No],[Temporary House No],[Temporary Locality],[Temporary State],[Temporary Zip Code],[Citizenship Issue Place],[Official Email],[Official Phone],[Official Extension],[Official Mobile],[Personal Email],[Personal Phone],[Personal Mobile],[Emergency Contact Name],[Emergency Relation],[Emergency Phone],[Emergency Mobile]) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)", conn);
                // Add the parameters.
                cmd.Parameters.Add("EIN", OleDbType.Integer, 4);
                cmd.Parameters.Add("[Employee Name]", OleDbType.VarChar, 200);

                cmd.Parameters.Add("[Permanent Country]", OleDbType.VarChar, 200);
                cmd.Parameters.Add("[Permanent Zone]", OleDbType.VarChar, 200);
                cmd.Parameters.Add("[Permanent District]", OleDbType.VarChar, 200);
                cmd.Parameters.Add("[Permanent VDC/Municipality]", OleDbType.VarChar, 200);
                cmd.Parameters.Add("[Permanent Street/Colony]", OleDbType.VarChar, 200);
                cmd.Parameters.Add("[Permanent Ward No]", OleDbType.VarChar, 200);
                cmd.Parameters.Add("[Permanent House No]", OleDbType.VarChar, 200);
                cmd.Parameters.Add("[Permanent Locality]", OleDbType.VarChar, 200);
                cmd.Parameters.Add("[Permanent State]", OleDbType.VarChar, 200);
                cmd.Parameters.Add("[Permanent Zip Code]", OleDbType.VarChar, 200);

                cmd.Parameters.Add("[Temporary Country]", OleDbType.VarChar, 200);
                cmd.Parameters.Add("[Temporary Zone]", OleDbType.VarChar, 200);
                cmd.Parameters.Add("[Temporary District]", OleDbType.VarChar, 200);
                cmd.Parameters.Add("[Temporary VDC/Municipality]", OleDbType.VarChar, 200);
                cmd.Parameters.Add("[Temporary Street/Colony]", OleDbType.VarChar, 200);
                cmd.Parameters.Add("[Temporary Ward No]", OleDbType.VarChar, 200);
                cmd.Parameters.Add("[Temporary House No]", OleDbType.VarChar, 200);
                cmd.Parameters.Add("[Temporary Locality]", OleDbType.VarChar, 200);
                cmd.Parameters.Add("[Temporary State]", OleDbType.VarChar, 200);
                cmd.Parameters.Add("[Temporary Zip Code]", OleDbType.VarChar, 200);
                cmd.Parameters.Add("[Citizenship Issue Place]", OleDbType.VarChar, 200);


                cmd.Parameters.Add("[Official Email]", OleDbType.VarChar, 200);
                cmd.Parameters.Add("[Official Phone]", OleDbType.VarChar, 200);
                cmd.Parameters.Add("[Official Extension]", OleDbType.VarChar, 200);
                cmd.Parameters.Add("[Official Mobile]", OleDbType.VarChar, 200);

                cmd.Parameters.Add("[Personal Email]", OleDbType.VarChar, 200);
                cmd.Parameters.Add("[Personal Phone]", OleDbType.VarChar, 200);
                cmd.Parameters.Add("[Personal Mobile]", OleDbType.VarChar, 200);

                cmd.Parameters.Add("[Emergency Contact Name]", OleDbType.VarChar, 200);
                cmd.Parameters.Add("[Emergency Relation]", OleDbType.VarChar, 200);
                cmd.Parameters.Add("[Emergency Phone]", OleDbType.VarChar, 200);
                cmd.Parameters.Add("[Emergency Mobile]", OleDbType.VarChar, 200);

                cmd.Parameters[0].IsNullable = true;
                foreach (GetEmployeeAddressResult row in list)
                {
                    cmd.Parameters[0].Value = row.EMPLOYEEID;
                    cmd.Parameters[1].Value = row.NAME;

                    cmd.Parameters[2].Value = row.PECOUNTRYID == null ? "" : row.PECOUNTRYID;
                    cmd.Parameters[3].Value = row.PEZONEID == null ? "" : row.PEZONEID;
                    cmd.Parameters[4].Value = row.PEDISTRICTID == null ? "" : row.PEDISTRICTID;
                    cmd.Parameters[5].Value = row.PEVDCMUNCIPALITY == null ? "" : row.PEVDCMUNCIPALITY;
                    cmd.Parameters[6].Value = row.PESTREET == null ? "" : row.PESTREET;
                    cmd.Parameters[7].Value = row.PEWARDNO == null ? "" : row.PEWARDNO;
                    cmd.Parameters[8].Value = row.PEHOUSENO == null ? "" : row.PEHOUSENO;
                    cmd.Parameters[9].Value = row.PELOCALITY == null ? "" : row.PELOCALITY;
                    cmd.Parameters[10].Value = row.PESTATE == null ? "" : row.PESTATE;
                    cmd.Parameters[11].Value = row.PEZIPCODE == null ? "" : row.PEZIPCODE;

                    cmd.Parameters[12].Value = row.PSCOUNTRYID == null ? "" : row.PSCOUNTRYID;
                    cmd.Parameters[13].Value = row.PSZONEID == null ? "" : row.PSZONEID;
                    cmd.Parameters[14].Value = row.PSDISTRICTID == null ? "" : row.PSDISTRICTID;
                    cmd.Parameters[15].Value = row.PSVDCMUNCIPALITY == null ? "" : row.PSVDCMUNCIPALITY;
                    cmd.Parameters[16].Value = row.PSSTREET == null ? "" : row.PSSTREET;
                    cmd.Parameters[17].Value = row.PSWARDNO == null ? "" : row.PSWARDNO;
                    cmd.Parameters[18].Value = row.PSHOUSENO == null ? "" : row.PSHOUSENO;
                    cmd.Parameters[19].Value = row.PSLOCALITY == null ? "" : row.PSLOCALITY;
                    cmd.Parameters[20].Value = row.PSSTATE == null ? "" : row.PSSTATE;
                    cmd.Parameters[21].Value = row.PSZIPCODE == null ? "" : row.PSZIPCODE;
                    cmd.Parameters[22].Value = row.PSCITISSDIS == null ? "" : row.PSCITISSDIS;

                    cmd.Parameters[23].Value = row.CIEMAIL == null ? "" : row.CIEMAIL;
                    cmd.Parameters[24].Value = row.CIPHONENO == null ? "" : row.CIPHONENO;
                    cmd.Parameters[25].Value = row.EXTENSION == null ? "" : row.EXTENSION;
                    cmd.Parameters[26].Value = row.CIMOBILENO == null ? "" : row.CIMOBILENO;

                    cmd.Parameters[27].Value = row.PERSONALEMAIL == null ? "" : row.PERSONALEMAIL;
                    cmd.Parameters[28].Value = row.PERSONALPHONE == null ? "" : row.PERSONALPHONE;
                    cmd.Parameters[29].Value = row.PERSONALMOBILE == null ? "" : row.PERSONALMOBILE;

                    cmd.Parameters[30].Value = row.EMERGENCYNAME == null ? "" : row.EMERGENCYNAME;
                    cmd.Parameters[31].Value = row.EMERGENCYRELATION == null ? "" : row.EMERGENCYRELATION;
                    cmd.Parameters[32].Value = row.EMERGENCYPHONE == null ? "" : row.EMERGENCYPHONE;
                    cmd.Parameters[33].Value = row.EMERGENCYMOBILE == null ? "" : row.EMERGENCYMOBILE;

                    cmd.ExecuteNonQuery();

                }
                // Insert the data into the Excel spreadsheet.
                //da.Update(dtSQL);
                using (OleDbCommand cmd1 = new OleDbCommand("INSERT INTO [CountryList$](Name) values (?)", conn))
                {
                    cmd1.Parameters.Add("Name", OleDbType.VarChar, 200);

                    foreach (var row in CountryList)
                    {
                        cmd1.Parameters[0].Value = row.Country;
                        cmd1.ExecuteNonQuery();
                    }
                }

                using (OleDbCommand cmd2 = new OleDbCommand("INSERT INTO [ZoneList$](Name) values (?)", conn))
                {
                    cmd2.Parameters.Add("Name", OleDbType.VarChar, 200);

                    foreach (var row in ZoneList)
                    {
                        cmd2.Parameters[0].Value = row.Zone;
                        cmd2.ExecuteNonQuery();
                    }
                }

                using (OleDbCommand cmd3 = new OleDbCommand("INSERT INTO [DistrictList$](Name) values (?)", conn))
                {
                    cmd3.Parameters.Add("Name", OleDbType.VarChar, 200);

                    foreach (var row in DistrictList)
                    {
                        cmd3.Parameters[0].Value = row.District;
                        cmd3.ExecuteNonQuery();
                    }
                }
            }
            ExcelGenerator.WriteFile(copyPath, Path.GetFileName(excelTemplatePath));
        }

        public static void ExportEmployeeInfo(string excelTemplatePath,
            List<DAL.GetEmployeeInfoResult> list,
             List<CountryList> listCountries,
            List<HBloodGroup> listBlood,
            List<HReligion> listReligion,
            List<DistrictList> listDistict,
            List<CitizenNationality> listNationality
            )
        {
            string path = HttpContext.Current.Server.MapPath(excelTemplatePath);
            string copyPath = Path.GetDirectoryName(path) + "\\" + Guid.NewGuid() + ".xlsx";
            File.Copy(path, copyPath);

            string excelConnection = string.Format(ExcelConnectionString, copyPath);
            using (OleDbConnection conn = new OleDbConnection(excelConnection))
            {
                // Open the connection.
                conn.Open();
                // Create a new sheet in the Excel spreadsheet.
                OleDbCommand cmd = new OleDbCommand("INSERT INTO [Sheet1$](EIN,INO,Name,[Blood Group],Nationality,[Citizenship No],[Citizenship Issue Date],[Citizenship Issue Place],[License Type],[License No],[License Issue Date],[License Issue Country],[Passport No],[Passport Issue Date],[Passport Valid Upto],Birthmark,[Marriage Anneversary],MotherTongue,Religion,Ethnicity) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)", conn);
                // Add the parameters.
                cmd.Parameters.Add("EIN", OleDbType.Integer, 4);
                cmd.Parameters.Add("INO", OleDbType.VarChar, 200);
                cmd.Parameters.Add("Name", OleDbType.VarChar, 200);

                cmd.Parameters.Add("[Blood Group]", OleDbType.VarChar, 200);
                cmd.Parameters.Add("Nationality", OleDbType.VarChar, 200);
                cmd.Parameters.Add("[Citizenship No]", OleDbType.VarChar, 200);
                cmd.Parameters.Add("[Citizenship Issue Date]", OleDbType.VarChar, 200);
                cmd.Parameters.Add("[Citizenship Issue Place]", OleDbType.VarChar, 200);
                cmd.Parameters.Add("[License Type]", OleDbType.VarChar, 200);
                cmd.Parameters.Add("[License No]", OleDbType.VarChar, 200);
                cmd.Parameters.Add("[License Issue Date]", OleDbType.VarChar, 200);
                cmd.Parameters.Add("[License Issue Country]", OleDbType.VarChar, 200);
                cmd.Parameters.Add("[Passport No]", OleDbType.VarChar, 200);

                cmd.Parameters.Add("[Passport Issue Date]", OleDbType.VarChar, 200);
                cmd.Parameters.Add("[Passport Valid Upto]", OleDbType.VarChar, 200);
                cmd.Parameters.Add("Birthmark", OleDbType.VarChar, 200);
                cmd.Parameters.Add("[Marriage Anneversary]", OleDbType.VarChar, 200);
                cmd.Parameters.Add("MotherTongue", OleDbType.VarChar, 200);
                cmd.Parameters.Add("Religion", OleDbType.VarChar, 200);
                cmd.Parameters.Add("Ethnicity", OleDbType.VarChar, 200);

                cmd.Parameters[0].IsNullable = true;
                foreach (GetEmployeeInfoResult row in list)
                {
                    cmd.Parameters[0].Value = row.EmployeeId;
                    cmd.Parameters[1].Value = row.IdCardNo == null ? "" : row.IdCardNo;
                    cmd.Parameters[2].Value = row.Name == null ? "" : row.Name;

                    cmd.Parameters[3].Value = row.BloodGroupName == null ? "" : row.BloodGroupName;
                    cmd.Parameters[4].Value = row.Nationality == null ? "" : row.Nationality;
                    cmd.Parameters[5].Value = row.CitizenshipNo == null ? "" : row.CitizenshipNo;
                    cmd.Parameters[6].Value = row.IssueDate == null ? "" : row.IssueDate;
                    cmd.Parameters[7].Value = row.Place == null ? "" : row.Place;
                    cmd.Parameters[8].Value = row.LiscenceTypeName == null ? "" : row.LiscenceTypeName;
                    cmd.Parameters[9].Value = row.DrivingLicenceNo == null ? "" : row.DrivingLicenceNo;
                    cmd.Parameters[10].Value = row.DrivingLicenceIssueDate == null ? "" : row.DrivingLicenceIssueDate;
                    cmd.Parameters[11].Value = row.IssuingCountry == null ? "" : row.IssuingCountry;
                    cmd.Parameters[12].Value = row.PassportNo == null ? "" : row.PassportNo;

                    cmd.Parameters[13].Value = row.IssuingDate == null ? "" : row.IssuingDate;
                    cmd.Parameters[14].Value = row.ValidUpto == null ? "" : row.ValidUpto;
                    cmd.Parameters[15].Value = row.Birthmark == null ? "" : row.Birthmark;
                    cmd.Parameters[16].Value = row.MarriageAniversary == null ? "" : row.MarriageAniversary;
                    cmd.Parameters[17].Value = row.MotherTongue == null ? "" : row.MotherTongue;
                    cmd.Parameters[18].Value = row.ReligionName == null ? "" : row.ReligionName;
                    cmd.Parameters[19].Value = row.EthnicityName == null ? "" : row.EthnicityName;
                    cmd.ExecuteNonQuery();

                }
                // Insert the data into the Excel spreadsheet.
                // da.Update(dtSQL);
                using (OleDbCommand cmd1 = new OleDbCommand("INSERT INTO [BloodGroupList$](Name) values (?)", conn))
                {
                    cmd1.Parameters.Add("Name", OleDbType.VarChar, 200);

                    foreach (var row in listBlood)
                    {
                        cmd1.Parameters[0].Value = row.BloodGroupName;
                        cmd1.ExecuteNonQuery();
                    }
                }

                using (OleDbCommand cmd2 = new OleDbCommand("INSERT INTO [CountrySheet$](Name) values (?)", conn))
                {
                    cmd2.Parameters.Add("Name", OleDbType.VarChar, 200);

                    foreach (var row in listCountries)
                    {
                        cmd2.Parameters[0].Value = row.CountryName;
                        cmd2.ExecuteNonQuery();
                    }
                }

                using (OleDbCommand cmd3 = new OleDbCommand("INSERT INTO [ReligionList$](Name) values (?)", conn))
                {
                    cmd3.Parameters.Add("Name", OleDbType.VarChar, 200);

                    foreach (var row in listReligion)
                    {
                        cmd3.Parameters[0].Value = row.ReligionName;
                        cmd3.ExecuteNonQuery();
                    }
                }

                using (OleDbCommand cmd4 = new OleDbCommand("INSERT INTO [DistictList$](Name) values (?)", conn))
                {
                    cmd4.Parameters.Add("Name", OleDbType.VarChar, 200);

                    foreach (var row in listDistict)
                    {
                        cmd4.Parameters[0].Value = row.District;
                        cmd4.ExecuteNonQuery();
                    }
                }


                using (OleDbCommand cmd5 = new OleDbCommand("INSERT INTO [CitizenNationalityList$](Name) values (?)", conn))
                {
                    cmd5.Parameters.Add("Name", OleDbType.VarChar, 200);

                    foreach (var row in listNationality)
                    {
                        cmd5.Parameters[0].Value = row.Nationality;
                        cmd5.ExecuteNonQuery();
                    }
                }
            }
            ExcelGenerator.WriteFile(copyPath, Path.GetFileName(excelTemplatePath));
        }

        public static void ExportEmployeePrevExperience(string excelTemplatePath, List<DAL.GetEmployeePrevExperienceListResult> list, List<ExperienceCategory> experienceCategories)
        {
            string path = HttpContext.Current.Server.MapPath(excelTemplatePath);
            string copyPath = Path.GetDirectoryName(path) + "\\" + Guid.NewGuid() + ".xlsx";
            File.Copy(path, copyPath);

            string excelConnection = string.Format(ExcelConnectionString, copyPath);
            using (OleDbConnection conn = new OleDbConnection(excelConnection))
            {
                //Open the connection.
                conn.Open();
                //Create a new sheet in the Excel spreadsheet.
                OleDbCommand cmd = new OleDbCommand("INSERT INTO [Sheet1$](EIN,[Employee Name],[Experience Category],Organization,Place,[Job Responsibility],[Reason for leaving],Notes,[Position]) values (?,?,?,?,?,?,?,?,?)", conn);

                //Add the parameters.
                cmd.Parameters.Add("EIN", OleDbType.Integer, 4);
                cmd.Parameters.Add("[Employee Name]", OleDbType.VarChar, 200);

                cmd.Parameters.Add("[Experience Category]", OleDbType.VarChar, 200);
                cmd.Parameters.Add("Organization", OleDbType.VarChar, 200);
                cmd.Parameters.Add("Place", OleDbType.VarChar, 200);
                cmd.Parameters.Add("[Job Responsibility]", OleDbType.VarChar, 200);
                cmd.Parameters.Add("[Reason for leaving]", OleDbType.VarChar, 200);
                cmd.Parameters.Add("Notes", OleDbType.VarChar, 200);
                cmd.Parameters.Add("[Position]", OleDbType.VarChar, 200);
                cmd.Parameters[0].IsNullable = true;
                cmd.Parameters[8].IsNullable = true;
                foreach (GetEmployeePrevExperienceListResult row in list)
                {
                    cmd.Parameters[0].Value = row.EmployeeId;
                    cmd.Parameters[1].Value = row.Name == null ? "" : row.Name;

                    cmd.Parameters[2].Value = row.Category == null ? "" : row.Category;
                    cmd.Parameters[3].Value = row.Organization == null ? "" : row.Organization;
                    cmd.Parameters[4].Value = row.Place == null ? "" : row.Place;
                    cmd.Parameters[5].Value = row.JobResponsibility == null ? "" : row.JobResponsibility;
                    cmd.Parameters[6].Value = row.ReasonForLeaving == null ? "" : row.ReasonForLeaving;
                    cmd.Parameters[7].Value = "";
                    cmd.Parameters[8].Value = row.Position == null ? "" : row.Position;
                    cmd.ExecuteNonQuery();

                }
                //        // Insert the data into the Excel spreadsheet.
                //        // da.Update(dtSQL);
                using (OleDbCommand cmd1 = new OleDbCommand("INSERT INTO [ExperienceCategoryList$](Name) values (?)", conn))
                {
                    cmd1.Parameters.Add("Name", OleDbType.VarChar, 200);

                    foreach (var row in experienceCategories)
                    {
                        cmd1.Parameters[0].Value = row.Name;
                        cmd1.ExecuteNonQuery();
                    }
                }
            }
            ExcelGenerator.WriteFile(copyPath, Path.GetFileName(excelTemplatePath));

        }


        public static void ExportEmployeeEducation(string excelTemplatePath,
            List<DAL.GetEmployeeEducationListResult> list,
            List<FixedValueEducationLevel> listEducationLevel,
             List<FixedValueEducationFaculty> listEducationFaculty,
             List<FixedValueDivision> listDivision,
             List<CountryList> listCountries
             )
        {
            string path = HttpContext.Current.Server.MapPath(excelTemplatePath);
            string copyPath = Path.GetDirectoryName(path) + "\\" + Guid.NewGuid() + ".xlsx";
            File.Copy(path, copyPath);

            string excelConnection = string.Format(ExcelConnectionString, copyPath);
            using (OleDbConnection conn = new OleDbConnection(excelConnection))
            {
                //Open the connection.
                conn.Open();
                //Create a new sheet in the Excel spreadsheet.
                OleDbCommand cmd = new OleDbCommand("INSERT INTO [Sheet1$](EIN,[Employee Name],[Course Name],[Level],[Faculty],[Institute],[University],[Country],[Percentage/Grade],[Division],[Passed Year],[Major Subjects]) values (?,?,?,?,?,?,?,?,?,?,?,?)", conn);

                //Add the parameters.
                cmd.Parameters.Add("EIN", OleDbType.Integer, 4);
                cmd.Parameters.Add("[Employee Name]", OleDbType.VarChar, 200);

                cmd.Parameters.Add("[Course Name]", OleDbType.VarChar, 200);
                cmd.Parameters.Add("[Level]", OleDbType.VarChar, 200);
                cmd.Parameters.Add("[Faculty]", OleDbType.VarChar, 200);
                cmd.Parameters.Add("[Institute]", OleDbType.VarChar, 200);
                cmd.Parameters.Add("[University]", OleDbType.VarChar, 200);
                cmd.Parameters.Add("[Country]", OleDbType.VarChar, 200);
                cmd.Parameters.Add("[Percentage/Grade]", OleDbType.VarChar, 200);
                cmd.Parameters.Add("[Division]", OleDbType.VarChar, 200);
                cmd.Parameters.Add("[Passed Year]", OleDbType.VarChar, 200);
                cmd.Parameters.Add("[Major Subjects]", OleDbType.VarChar, 200);

                cmd.Parameters[0].IsNullable = true;
                cmd.Parameters[10].IsNullable = true;
                foreach (GetEmployeeEducationListResult row in list)
                {
                    cmd.Parameters[0].Value = row.EmployeeId;
                    cmd.Parameters[1].Value = row.Name == null ? "" : row.Name;

                    cmd.Parameters[2].Value = row.Course == null ? "" : row.Course;
                    cmd.Parameters[3].Value = row.Level == null ? "" : row.Level;
                    cmd.Parameters[4].Value = row.Faculty == null ? "" : row.Faculty;
                    cmd.Parameters[5].Value = row.Institution == null ? "" : row.Institution;
                    cmd.Parameters[6].Value = row.University == null ? "" : row.University;
                    cmd.Parameters[7].Value = row.Country == null ? "" : row.Country;
                    cmd.Parameters[8].Value = row.Percentage == null ? "" : row.Percentage;
                    cmd.Parameters[9].Value = row.Division == null ? "" : row.Division;
                    cmd.Parameters[10].Value = row.PassedYear == null ? "" : row.PassedYear.ToString(); ;
                    cmd.Parameters[11].Value = row.MajorSubjects == null ? "" : row.MajorSubjects;
                    cmd.ExecuteNonQuery();

                }
                //        // Insert the data into the Excel spreadsheet.
                //        // da.Update(dtSQL);
                using (OleDbCommand cmd1 = new OleDbCommand("INSERT INTO [LevelSheet$](Name) values (?)", conn))
                {
                    cmd1.Parameters.Add("Name", OleDbType.VarChar, 200);

                    foreach (var row in listEducationLevel)
                    {
                        cmd1.Parameters[0].Value = row.Name;
                        cmd1.ExecuteNonQuery();
                    }
                }

                using (OleDbCommand cmd2 = new OleDbCommand("INSERT INTO [FacultySheet$](Name) values (?)", conn))
                {
                    cmd2.Parameters.Add("Name", OleDbType.VarChar, 200);

                    foreach (var row in listEducationFaculty)
                    {
                        cmd2.Parameters[0].Value = row.Name;
                        cmd2.ExecuteNonQuery();
                    }
                }

                using (OleDbCommand cmd3 = new OleDbCommand("INSERT INTO [CountrySheet$](Name) values (?)", conn))
                {
                    cmd3.Parameters.Add("Name", OleDbType.VarChar, 200);

                    foreach (var row in listCountries)
                    {
                        cmd3.Parameters[0].Value = row.CountryName;
                        cmd3.ExecuteNonQuery();
                    }
                }


                using (OleDbCommand cmd4 = new OleDbCommand("INSERT INTO [DivisionSheet$](Name) values (?)", conn))
                {
                    cmd4.Parameters.Add("Name", OleDbType.VarChar, 200);

                    foreach (var row in listDivision)
                    {
                        cmd4.Parameters[0].Value = row.Division;
                        cmd4.ExecuteNonQuery();
                    }
                }
            }
            ExcelGenerator.WriteFile(copyPath, Path.GetFileName(excelTemplatePath));

        }


        public static void ExportEmployeeFamilyLists(string excelTemplatePath,
           List<DAL.GetEmployeeFamilyMemberListResult> list,
           List<FixedValueFamilyRelation> listFamilyRelations,
            List<HrFamilyOccupation> occupations,
            List<HBloodGroup> BloodGroup,
            List<CitizenNationality> NationalityList,
            List<DocumentDocumentType> DocumentTypeList
            )
        {
            string path = HttpContext.Current.Server.MapPath(excelTemplatePath);
            string copyPath = Path.GetDirectoryName(path) + "\\" + Guid.NewGuid() + ".xlsx";
            File.Copy(path, copyPath);

            string excelConnection = string.Format(ExcelConnectionString, copyPath);
            using (OleDbConnection conn = new OleDbConnection(excelConnection))
            {
                //Open the connection.
                conn.Open();
                //Create a new sheet in the Excel spreadsheet.
                OleDbCommand cmd = new OleDbCommand("INSERT INTO [Sheet1$](EIN,[Employee Name],[Relation],[Name],[Date Of Birth],[Date of Birth Eng],[Dependent],[Occupation],[Contact Number],[Gender],[Blood Group],[Nationality],[Document Type],[Document ID],[Document Issue Date],[Document Issue Place]) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)", conn);

                //Add the parameters.
                cmd.Parameters.Add("EIN", OleDbType.Integer, 4);
                cmd.Parameters.Add("[Employee Name]", OleDbType.VarChar, 200);
                cmd.Parameters.Add("[Relation]", OleDbType.VarChar, 200);
                cmd.Parameters.Add("[Name]", OleDbType.VarChar, 200);

                cmd.Parameters.Add("[Date Of Birth]", OleDbType.VarChar, 200);
                cmd.Parameters.Add("[Date of Birth Eng]", OleDbType.VarChar, 200);
                cmd.Parameters.Add("[Dependent]", OleDbType.VarChar, 200);
                cmd.Parameters.Add("[Occupation]", OleDbType.VarChar, 200);

                cmd.Parameters.Add("[Contact Number]", OleDbType.VarChar, 200);
                cmd.Parameters.Add("[Gender]", OleDbType.VarChar, 200);
                cmd.Parameters.Add("[Blood Group]", OleDbType.VarChar, 200);
                cmd.Parameters.Add("[Nationality]", OleDbType.VarChar, 200);

                cmd.Parameters.Add("[Document Type]", OleDbType.VarChar, 200);
                cmd.Parameters.Add("[Document ID]", OleDbType.VarChar, 200);
                cmd.Parameters.Add("[Document Issue Date]", OleDbType.VarChar, 200);
                cmd.Parameters.Add("[Document Issue Place]", OleDbType.VarChar, 200);

                cmd.Parameters[0].IsNullable = true;
                cmd.Parameters[5].IsNullable = true;
                foreach (GetEmployeeFamilyMemberListResult row in list)
                {
                    cmd.Parameters[0].Value = row.EmployeeId;
                    cmd.Parameters[1].Value = row.Name == null ? "" : row.Name;
                    cmd.Parameters[2].Value = row.Relation == null ? "" : row.Relation;
                    cmd.Parameters[3].Value = row.Name == null ? "" : row.Name;

                    cmd.Parameters[4].Value = row.DateOfBirth == null ? "" : row.DateOfBirth;
                    cmd.Parameters[5].Value = "";
                    cmd.Parameters[6].Value = row.IsDependent == null ? "" : row.IsDependent;
                    cmd.Parameters[7].Value = row.Occupation == null ? "" : row.Occupation;

                    cmd.Parameters[8].Value = row.ContactNumber == null ? "" : row.ContactNumber;
                    cmd.Parameters[9].Value = row.Gender == null ? "" : row.Gender;
                    cmd.Parameters[10].Value = row.BloodGroup == null ? "" : row.BloodGroup;
                    cmd.Parameters[11].Value = row.Nationality == null ? "" : row.Nationality;

                    cmd.Parameters[12].Value = row.DocumentIdType == null ? "" : row.DocumentIdType;
                    cmd.Parameters[13].Value = row.DocumentId == null ? "" : row.DocumentId;
                    cmd.Parameters[14].Value = row.DocumentIssueDate == null ? "" : row.DocumentIssueDate;
                    cmd.Parameters[15].Value = row.DocumentIssuePlace == null ? "" : row.DocumentIssuePlace;
                    cmd.ExecuteNonQuery();

                }
                //        // Insert the data into the Excel spreadsheet.
                //        // da.Update(dtSQL);
                using (OleDbCommand cmd1 = new OleDbCommand("INSERT INTO [RelationList$](Name) values (?)", conn))
                {
                    cmd1.Parameters.Add("Name", OleDbType.VarChar, 200);

                    foreach (var row in listFamilyRelations)
                    {
                        cmd1.Parameters[0].Value = row.Name;
                        cmd1.ExecuteNonQuery();
                    }
                }

                using (OleDbCommand cmd2 = new OleDbCommand("INSERT INTO [OccupationList$](Name) values (?)", conn))
                {
                    cmd2.Parameters.Add("Name", OleDbType.VarChar, 200);

                    foreach (var row in occupations)
                    {
                        cmd2.Parameters[0].Value = row.Occupation;
                        cmd2.ExecuteNonQuery();
                    }
                }

                using (OleDbCommand cmd3 = new OleDbCommand("INSERT INTO [BloodGroupList$](Name) values (?)", conn))
                {
                    cmd3.Parameters.Add("Name", OleDbType.VarChar, 200);

                    foreach (var row in BloodGroup)
                    {
                        cmd3.Parameters[0].Value = row.BloodGroupName;
                        cmd3.ExecuteNonQuery();
                    }
                }


                using (OleDbCommand cmd4 = new OleDbCommand("INSERT INTO [NationalityList$](Name) values (?)", conn))
                {
                    cmd4.Parameters.Add("Name", OleDbType.VarChar, 200);

                    foreach (var row in NationalityList)
                    {
                        cmd4.Parameters[0].Value = row.Nationality;
                        cmd4.ExecuteNonQuery();
                    }
                }
                using (OleDbCommand cmd5 = new OleDbCommand("INSERT INTO [DocumentTypeList$](Name) values (?)", conn))
                {
                    cmd5.Parameters.Add("Name", OleDbType.VarChar, 200);

                    foreach (var row in DocumentTypeList)
                    {
                        cmd5.Parameters[0].Value = row.DocumentTypeName;
                        cmd5.ExecuteNonQuery();
                    }
                }
            }

            ExcelGenerator.WriteFile(copyPath, Path.GetFileName(excelTemplatePath));

        }




        public static void ExportEmpMaritalTaxStatusDetails(string excelTemplatePath, List<GetEmployeeMaritalAndTaxStatusListResult> list)
        {
            string path = HttpContext.Current.Server.MapPath(excelTemplatePath);
            string copyPath = Path.GetDirectoryName(path) + "\\" + Guid.NewGuid() + ".xlsx";
            File.Copy(path, copyPath);

            string excelConnection = string.Format(ExcelConnectionString, copyPath);

            using (OleDbConnection conn = new OleDbConnection(excelConnection))
            {
                // Open the connection.
                conn.Open();


                // Create a new sheet in the Excel spreadsheet.
                OleDbCommand cmd = new OleDbCommand("INSERT INTO [Sheet1$](EIN,Employee,Branch,Department,[Marital Status],[Has Couple Tax Status]) values (?,?,?,?,?,?)", conn);

                // Add the parameters.

                cmd.Parameters.Add("EIN", OleDbType.Integer, 5);
                cmd.Parameters.Add("Employee", OleDbType.VarChar, 200);


                cmd.Parameters.Add("Branch", OleDbType.VarChar, 200);
                cmd.Parameters.Add("Department", OleDbType.VarChar, 200);
                cmd.Parameters.Add("[Marital Status]", OleDbType.VarChar, 50);
                cmd.Parameters.Add("[Has Couple Tax Status]", OleDbType.VarChar, 50);
                cmd.Parameters["[Has Couple Tax Status]"].IsNullable = true;

                foreach (GetEmployeeMaritalAndTaxStatusListResult row in list)
                {
                    //dr.SetAdded();
                    cmd.Parameters[0].Value = row.EmployeeId;
                    cmd.Parameters[1].Value = row.Name;

                    cmd.Parameters[2].Value = row.Branch;
                    cmd.Parameters[3].Value = row.Department;
                    cmd.Parameters[4].Value = row.MaritalStatus;
                    if (row.HasCoupleTaxStatus != null)
                    {
                        if (row.HasCoupleTaxStatus.Value)
                            cmd.Parameters[5].Value = "Yes";
                        else
                            cmd.Parameters[5].Value = "No";
                    }
                    else
                        cmd.Parameters[5].Value = "";

                    cmd.ExecuteNonQuery();
                }

            }


            ExcelGenerator.WriteFile(copyPath, Path.GetFileName(excelTemplatePath));
        }

        public static void WriteHoldPaymentExcel(string excelTemplatePath)
        {
            string path = HttpContext.Current.Server.MapPath(excelTemplatePath);
            string copyPath = Path.GetDirectoryName(path) + "\\" + Guid.NewGuid() + ".xlsx";
            File.Copy(path, copyPath);

            List<Branch> branchList = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId).OrderBy(x => x.Name).ToList();

            string excelConnection = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + copyPath + ";Extended Properties='Excel 12.0 Xml;HDR=Yes'";

            try
            {

                ExcelGenerator.WriteFile(copyPath, Path.GetFileName(excelTemplatePath));
            }
            finally
            {
                try
                {
                    File.Delete(copyPath);
                }
                catch
                {
                }
            }
        }



        public static void ExportEmployeeInsuranceClaim(string excelTemplatePath,
            List<DAL.GetEmployeeInsuranceClaimListByLotResult> list)
        {
            string path = HttpContext.Current.Server.MapPath(excelTemplatePath);
            string copyPath = Path.GetDirectoryName(path) + "\\" + Guid.NewGuid() + ".xlsx";
            File.Copy(path, copyPath);

            string excelConnection = string.Format(ExcelConnectionString, copyPath);
            using (OleDbConnection conn = new OleDbConnection(excelConnection))
            {
                // Open the connection.
                conn.Open();
                // Create a new sheet in the Excel spreadsheet.
                OleDbCommand cmd = new OleDbCommand("INSERT INTO [Sheet1$]([Staff ID],[Control Codes],Name,[Spouse/Children],[Claim of],Address,Description,[Claim received date],[Claim Amount],[Claim Amount received Date],[Received Amount],Remarks,ClaimID) values (?,?,?,?,?,?,?,?,?,?,?,?,?)", conn);
                // Add the parameters.
                cmd.Parameters.Add("[Staff ID]", OleDbType.Integer, 4);
                cmd.Parameters.Add("[Control Codes]", OleDbType.VarChar, 200);

                cmd.Parameters.Add("Name", OleDbType.VarChar, 200);
                cmd.Parameters.Add("[Spouse/Children]", OleDbType.VarChar, 200);
                cmd.Parameters.Add("[Claim of]", OleDbType.VarChar, 200);
                cmd.Parameters.Add("Address", OleDbType.VarChar, 200);
                cmd.Parameters.Add("Description", OleDbType.VarChar, 200);
                cmd.Parameters.Add("[Claim received date]", OleDbType.VarChar, 200);
                cmd.Parameters.Add("[Claim Amount]", OleDbType.VarChar, 200);
                cmd.Parameters.Add("[Claim Amount received Date]", OleDbType.VarChar, 200);
                cmd.Parameters.Add("[Received Amount]", OleDbType.VarChar, 200);
                cmd.Parameters.Add("Remarks", OleDbType.VarChar, 200);
                cmd.Parameters.Add("ClaimID", OleDbType.VarChar, 200);


                cmd.Parameters[0].IsNullable = true;
                cmd.Parameters[1].IsNullable = true;
                cmd.Parameters[12].IsNullable = true;
                foreach (GetEmployeeInsuranceClaimListByLotResult row in list)
                {
                    cmd.Parameters[0].Value = row.EmployeeID;
                    cmd.Parameters[1].Value = "";

                    cmd.Parameters[2].Value = row.Name == null ? "" : row.Name;
                    cmd.Parameters[3].Value = row.ClaimOfFamilyName == null ? "" : row.ClaimOfFamilyName;
                    cmd.Parameters[4].Value = row.RelationName == null ? "" : row.RelationName;
                    cmd.Parameters[5].Value = "";
                    cmd.Parameters[6].Value = "";
                    cmd.Parameters[7].Value = row.ClaimDate == null ? "" : row.ClaimDate.ToString("yyyy-MM-dd");
                    cmd.Parameters[8].Value = row.TotalClaimAmount == null ? "" : string.Format("{0:N2}", row.TotalClaimAmount);
                    cmd.Parameters[9].Value = row.ReceivedDate == null ? "" : Convert.ToDateTime(row.ReceivedDate).Date.ToString("yyyy-MM-dd");
                    cmd.Parameters[10].Value = row.ReceivedAmount == null ? "" : row.ReceivedAmount.ToString();
                    cmd.Parameters[11].Value = row.Remarks == null ? "" : row.Remarks;
                    cmd.Parameters[12].Value = row.ClaimID;

                    cmd.ExecuteNonQuery();

                }
                // Insert the data into the Excel spreadsheet.
                //da.Update(dtSQL);
                //using (OleDbCommand cmd1 = new OleDbCommand("INSERT INTO [CountryList$](Name) values (?)", conn))
                //{
                //    cmd1.Parameters.Add("Name", OleDbType.VarChar, 200);

                //    foreach (var row in CountryList)
                //    {
                //        cmd1.Parameters[0].Value = row.Country;
                //        cmd1.ExecuteNonQuery();
                //    }
                //}

            }
            ExcelGenerator.WriteFile(copyPath, Path.GetFileName(excelTemplatePath));
        }
        
        public static void WriteEmpChangeStatusExportSheet(string excelTemplatePath, List<HR_Service_Event> eventTypes, List<KeyValue> statusList)
        {
            string path = HttpContext.Current.Server.MapPath(excelTemplatePath);
            string copyPath = Path.GetDirectoryName(path) + "\\" + Guid.NewGuid() + ".xlsx";
            File.Copy(path, copyPath);

            string excelConnection = string.Format(ExcelConnectionString, copyPath);

            using (OleDbConnection conn = new OleDbConnection(excelConnection))
            {
                // Open the connection.
                conn.Open();

                WriteIntoSheet(conn, "StatusSheet", statusList.Select(x => x.Value).ToArray());
                WriteIntoSheet(conn, "EventTypeList", eventTypes.Select(x => x.Name).ToArray());
            }



            ExcelGenerator.WriteFile(copyPath, Path.GetFileName(excelTemplatePath));
        }




    }
}
