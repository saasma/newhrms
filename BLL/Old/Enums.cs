﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL
{
    public enum EnglishMonthEnum
    {
        April = 4
    }
    public enum SalaryWorkday
    {
        DefaultAcutal = 1,
        BasedOnWeeklyHolidayDeduction = 2,
        FixedDays = 3
    }
    public enum NIBLIncomeVerificationReportEnum
    {
        Talab = 1,
        Bhatta = 2,
        Motor = 3,
        Telephone = 4,
        Abash = 5,
        GarBhada = 6,
        Bonus = 7,
        Ausadhi = 8,
        Dashain = 9,
        InsuranceIncome = 10,
        Reward = 11,


        PFDeduction = 20,
        CIT = 21,
        InsurancePremium = 22,
        MarriedSingleDiscountLimit = 23,
        OnePercentTax = 24,
        FifteenPercentTax = 25,
        TwentyFivePercentTax = 26,
        ThirtyFivePercentTax = 27,

    }

    public enum GlobusVoucherCategoryIDEnum
    {
        MainVoucher = 1,
        AdvanceVoucher = 2, //civil advance voucher type
        NetPayAndDeductionsVoucher = 3,// civil net pay,and loan repayment types

    }

    public enum OvertimeAllowanceType
    {
        NIBLLikeAutoGeneration = 1,
        RequestType = 2
    }
    public enum BonusEligiblity
    {
        NotEligible = 0,
        Full = 1,
        Proportionate = 2
    }
    public enum LoanAdjustmentType
    {
        FullSettlement = 1,
        PartialSettlement = 2,
        LoanAdded = 3,
        InterestRateChange = 4,
        AdjustInterest = 5,
        FreshLoanAdded = 6
    }

    public enum LoanAdjustmentInstallmentOption
    {
        ReduceAmount = 1,
        ReducePeriod = 2,
        DefineNewInstallment = 3,
        DefineNewInstallmentWithFixedAmount = 4
    }
    public enum LoanAdjustmentPayment
    {
        DeductFromSalary = 1,
        ExternalSettlement = 2
    }
    public enum AddLoanInstallmentOption
    {
        SameInstallmentAmount = 1,
        SameInstallmentPeriod = 2,
        DefineNewInstallment = 3,
        DefineNewInstallmentWithFixedAmount = 4
    }
    //public enum ServiceEventTable
    //{
    //    CurrentStatus=1,
    //    DesignationHistory = 2,
    //    BranchDepartmentHistory = 3,
    //    DeputationEmployee = 4,
    //    PEmployeeIncrement = 5,
    //    ActingEmployee = 6,
    //    Retirement = 7,
    //    ReAppointment = 8
    //}
    //public enum ServiceGroupEnum
    //{
    //    Retirement = 1, // Retirement,Resign,Termination
    //    Increment = 2// Increment,Annual Increment,Promotion

    //}

    // always use pre defined, don't allow to change user, as we need to know Reappointment type like 
    // from service event to show service period and other processing
    public enum ServiceEventType
    {
        Appointment = 1,
        ReAppointment = 2, // Re Hire
        //Termination = 3,
        DisciplinaryAction = 4,
        Increment = 5,
        ////Resign = 6,
        Promotion = 12,
        Transfer = 15,
        Retirement = 16,
        Demotion = 19,
        StopPayment = 21,
        Reward = 25,
        Other = 40
        //AnnualIncrement=24,
        //DepartmentTransfer = 26
    }

    public enum PayrollPeriodType
    {
        Normal = 1,
        OldAddOn = 2,
        NewAddOn = 3
    }
    //public enum EmployeeServiceHistoryType
    //{
    //    Retired = 100,
    //    ReHire = 101
    //}
    public enum LeaveEncashmentRounding
    {
        NoRounding = 1,
        RoundingUp = 2,
        RoundingDown = 3
    }
    public enum LeaveEncashmentTreatmentOnRetirement
    {
        Standard = 1,
        AllFifteenPercentBenefit = 2,
        AllRegular = 3
    }
    public enum TimeSheetEditableState
    {
        CurrentMonth = 1, //whole cell is editable
        PreviousMonth = 2, // only from 27 cell is editable
        PastMonth = 3
    }
    public enum TimeSheetStatus
    {
        NotSubmitted = -1,
        Draft = 0,
        AwaitingApproval = 1, // submitted
        Approved = 2,
        ReviewedByHR = 3,
        Rejected = 10,
        Reviewed = 11
    }

    public enum AttendanceInOutMode
    {
        In = 0,
        Out = 1
    }
    public enum LeaveRequestStatusEnum
    {
        Request = 1, //When request is made by the requesting Employee,
        Approved = 2, //When approved
        Denied = 3, //When Denied instead of Approved
        ReDraft = 4, //Request leave can be again redirect to the requesting employee in which case it will be in ReDraft state
        Cancelled = 5, //Approved Leave can be cancelled by the requesting or approval Employee,& those leaves are totally left while counting as leave
        Recommended = 6
    }

    public enum Role
    {
        Administrator = 1,
        Employee = 2
    }

    public enum PageModeList
    {
        NotSet,
        Insert,
        Update,
        UpdateEmployee
    }

    public enum ProjectType
    {
        EachProjectPercent = 1,
        ProjectPercent = 2,
        Input = 3
    }

    public enum TempSalaryType
    {
        Optimum = 1,
        IncomeDeductionTaxAdjustment = 2
    }

    public enum SettlementDetailType
    {
        LeaveEncash = 3,
        DashainBonus = 8,

        Advance = 1,
        Loan = 2,
        LoanInterest = 10,
        LoanRepayment = 9,

        // Refundable advance like for Laptop given to emp & payment reduced from advance & later
        // returned in ret/res
        RefundableAdvance = 4, // Not Default or common
        ExcessiveLeave = 5 // Not default or Common

        , Gratuity = 12 // bring gratuity in retirement if setting is enabled

    }

    public enum MonetoryChangeLogTypeEnum
    {
        Income = 1,
        Deduction = 2,
        Insurance = 3,
        Increment = 4,
        Grade = 5,
        Leave = 6,
        PF = 7,
        Information = 8,
        Branch = 9,
        Status = 10,
        CIT = 11,
        NoCIT = 12,
        MedicalTaxCredit = 13,
        Loan = 14,
        Advance = 15,
        Adjustment = 16,
        OptimumPFAndCIT = 17,
        FinancialYear = 18,
        PayrollPeriod = 19,
        Company = 20,
        PastSalaryChanged = 21,
        EmployeeAdded = 22,
        Gender = 23,
        TaxStatus = 24,
        LoanType = 25,
        NoPF = 30,
        NoTax = 31,

        AdditionalIncome = 32,
        Reward = 33,
        StopPayment = 35,
        Salary = 36,
        Name = 40,
        HireDate = 41,
        HireStatus = 42,
        Department = 43,
        Designation = 44,
        AddOn = 45,
        LeaveOpening = 46,
        Transfer = 47,
        Timesheet = 50,
        NoGratuity = 53,
        AllowanceType = 51,
        OvertimeType = 52,
        AttendanceTimeDeleted = 55,
        Appraisal = 60,
        PayGroup = 61,
        Holiday = 62,
        User = 65,
        PastRegularAdjustment = 70,
        TravelRequest = 71,
        HasCoupleTaxStatus = 72,
        Attendance = 73
    }

    public enum EmailLogTypeEnum
    {
        LeaveRequest = 1,
        LeaveRecommended = 5,
        LeaveApprovalToEmployee = 4,
        LeaveApprovalToOtherEmployee = 6,
        Overtime = 2,
        AllowanceRequest = 3,
        AllowanceApproval = 7
    }

    //public enum LogOtherRemarksType
    //{
    //    Amount,
    //    Rate,
    //    Installment

    //}

    public enum LogBoolean
    {
        Yes,
        No
    }

    public enum LogActionEnum
    {
        Add = 1,
        Update = 2,
        Delete = 3,
        Attach = 4, //Associate Income/Deduction to Employee
        Detach = 5 //Remove Income/Deduction from Employee
    }

    //public enum NonMonetory
    public enum ResignationRequestStatus
    {
        Pending = 0,
        ExitProcess = 1,
        Approved = 2,
        Rejected = 3,
    }

    public enum ExitClearanceForms
    {
        FinanceClearanceForm = 1,
        AssetCustodianForm = 2,
        LibraryClearanceForm = 3
    }

    public enum ClearanceDetailToShow
    {
        LoanDetail=1,
        AdvanceDetail =2,
        RefundableDeposit=3,
        AssetInCustody=4
    }


    public enum EmailContentType
    {
        SalaryMail = 1,
        SendNewPasswordInMail = 2,
        UserNameChanged = 5,
        PayrollSignoffApproval = 6,

        DayTypeLeaveRequest = 3,
        //DayTypeLeaveRequestForward = 20,
        DayTypeLeaveApproval = 4,

        BankDepositEmail = 8,

        // Appraisals
        AppraisalEmployeeRollout = 10,
        AppraisalSupervisor = 11,
        AppraisalEmployeeReview = 12,
        AppraisalHigherLevel = 13,

        TravelRequestReview = 15,
        TravelRequestAdvanceNotificationToEmployee = 21,

        ProjectNotfill = 16,
        TimeSheetSendForApproval = 17,
        TimeSheetApprovedOrReject = 18,
        CancelLeaveRequest = 20,
        TimesheetSetAsDraft = 30,
        LateIn = 31,
        Absent = 32,
        AbsentWeeklyForEmployee = 33,
        AbsentWeeklyForSuperviser = 34,
        TimeAttendanceCommentApprovedOrReject = 35,
        TimeRequestApprovedOrReject = 36,
        BirthdayNotification = 37,
        BirthdayWishNotification = 38,
        TrainingEmail = 40,
        AttendanceAbsentdailyforSuperviser = 41,

            ResignationFormClearance =7
    }
    public enum AccountingVoucherTypeEnum
    {
        Pumori = 1,
        FinnaleFlex = 2,
        GLOBUS = 3
    }
    public enum WhichCompany
    {
        OtherTesting = 0,
        Default = 1,
        Smart = 2,
        HPL = 3,
        D2 = 4,
        Sanchaya = 5,
        PSI = 10,
        GMC = 11,
        NSET = 12,
        BritishSchool = 13,
        AceTravels = 15,
        RBB = 20,
        Yeti = 21,
        Mega = 25,
        DishHome = 27,
        Sangrila = 30,
        Citizen = 31,
        Rigo = 32,
        Care = 33,
        NIBL = 34,
        Triveni = 35,
        Century = 40,
        Heifer = 42,
        LRPB = 43,
        Prabhu = 44,
        Janata = 45,
        Suvhidha = 46,
        Jyoti = 47,
        Quest = 50,
        Civil = 51,
        MBL = 52,
        Sana = 53 // sana kisan
        , Oxfam = 54
        , ArghakhachiCement = 55
         , WDN = 56
        , NCC = 58
            , Neco = 59
            , SaveTheChidlren = 60

    }

    public enum AttendanceSaveErrorStatus
    {
        NoError = 0,
        LeaveRequestConflict = 1,
        UPLChangedForSalaryAlreadySaved = 2
    }


    public enum VoucherGroupTypeEnum
    {
        Positive = 1,
        Negative = 2,
        Assets = 3,
        Income = 4
    }

    public enum VoucherGroupEnum
    {
        SUMBranch = 1,
        SumCompany = 2,
        Employeewise = 3,
        BranchTransaction = 4,
        VoucherBalance = 5,
        LoanEmployeeWise = 6,
        NetSalary = 7
    }

    public enum LeaveApprovalNotificationType
    {
        None = 0,
        AllEmployees = 1,
        BranchEmployees = 2,
        DepartmentEmployees = 3,
        TeamMembers = 4
    }

    public enum LeaveRequestManagementMethod

    {
        LeaveTeams = 0,

        LeaveAuthorities = 1,

        DefineEachSupervisorToEachEmployee = 2
    }

    public enum SalaryCaluclationChangedType
    {
        Saved = 1,
        Deleted = 2,
        DeleteAll = 3,
        Approved = 4
    }

    public enum OvertimePeriodStatus
    {

        NotEditable = 0,
        Distributed = 1
    }
    public enum OvertimePeriodDetailStatus
    {
        Saved = 0,
        Approved = 1,
        Rejected = 2
    }

    public enum OvertimeStatusEnum
    {
        Pending = 0,
        Recommended = 1,
        Approved = 2,
        Rejected = 3,
        Forwarded = 4
    }
    public enum TADAStatusEnum
    {
        Saved = -1,
        Pending = 0,
        Recommended = 1,
        Approved = 2,
        Rejected = 3,
        Forwarded = 4
    }

    public enum OvertimeCalculationType
    {
        BasedOnSalary = 1,
        FixedRate = 2,
        BasedOnMonthlyHour = 3 // For HPL, Overtime 150%	(SALARY/174)*HOURS*150% cases
    }

    public enum AllowanceEveningCounterCalculationType
    {
        Default = 1,
        Type365Days = 2 // For HPL, (SALARY*12/365)*DAYS*15%
        , BasedOnSalary = 4
    }

    public enum VariableFormType
    {
        Overtime = 1,
        MedicalReimbursement = 2,
        EducationReimbursement = 3,
        ShiftAllowance = 4,
        TADA = 5,
        EveningCounter = 6
    }

}
