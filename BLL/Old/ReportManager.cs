﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BLL.Manager;
using DAL;

using BLL;
using System.Web;
using BLL.BO;

namespace BLL
{
    public class ReportManager : BaseBiz
    {
        #region HR Reports


        public static List<Report_HR_TeamEmployeeLeaveBalanceResult> GetEmployeeLeaveBalance(
           int mode,int filterEIN)
        {
            int daysDiff = 0; int remainingMonth = 0;
            PayrollPeriod payrollPeriod = CommonManager.GetLastPayrollPeriod();
            if (payrollPeriod.Month != SessionManager.CurrentCompany.LeaveStartMonth.Value)
            {

                daysDiff = LeaveAttendanceManager.GetYearlyLeaveMiddleJoiningDays(payrollPeriod, ref remainingMonth);
            }

            List<LLeaveType> leaveList = PayrollDataContext.LLeaveTypes.Where(x => x.FreqOfAccrual != LeaveAccrue.MANUALLY).OrderBy(x => x.Order).ToList();

            List<Report_HR_TeamEmployeeLeaveBalanceResult> list =  PayrollDataContext.Report_HR_TeamEmployeeLeaveBalance
                (SessionManager.CurrentCompanyId,filterEIN,mode,SessionManager.CurrentLoggedInEmployeeId).ToList();

            List<string> eins = list.Select(x => x.EmployeeId.ToString()).ToList();
            string einList = string.Join(",", eins.ToArray());

            List<Report_HR_TeamEmployeeLeaveBalanceDetailsResult> leaveBalances = PayrollDataContext.Report_HR_TeamEmployeeLeaveBalanceDetails
                (payrollPeriod.PayrollPeriodId, SessionManager.CurrentCompanyId, daysDiff, remainingMonth, false, einList).ToList();


            List<Report_HR_TeamEmployeeCountResult> countLeavesReqquest = PayrollDataContext.Report_HR_TeamEmployeeCount(
                einList).ToList();

            
            List<Report_HR_TeamEmployeeTimeRequestCountResult> countTimeRequest = PayrollDataContext.Report_HR_TeamEmployeeTimeRequestCount(
                einList).ToList();


            // set leave balance
            foreach (Report_HR_TeamEmployeeLeaveBalanceResult emp in list)
            {
                int index = 1;
                foreach (LLeaveType leave in leaveList)
                {
                    Report_HR_TeamEmployeeLeaveBalanceDetailsResult leaveBal =
                        leaveBalances.FirstOrDefault(x => x.EmployeeId == emp.EmployeeId && x.LeaveTypeId == leave.LeaveTypeId);

                    if (leaveBal != null)
                    {
                        if (leaveBal.NewBalance == 0)
                            leaveBal.NewBalance = null;

                        switch (index)
                        {
                            case 1: emp.LeaveBalance1 = leaveBal.NewBalance; break;
                            case 2: emp.LeaveBalance2 = leaveBal.NewBalance; break;
                            case 3: emp.LeaveBalance3 = leaveBal.NewBalance; break;
                            case 4: emp.LeaveBalance4 = leaveBal.NewBalance; break;
                            case 5: emp.LeaveBalance5 = leaveBal.NewBalance; break;
                            case 6: emp.LeaveBalance6 = leaveBal.NewBalance; break;
                            case 7: emp.LeaveBalance7 = leaveBal.NewBalance; break;
                            case 8: emp.LeaveBalance8 = leaveBal.NewBalance; break;
                            case 9: emp.LeaveBalance9 = leaveBal.NewBalance; break;
                            case 10: emp.LeaveBalance10 = leaveBal.NewBalance; break;
                            case 11: emp.LeaveBalance11 = leaveBal.NewBalance; break;
                            case 12: emp.LeaveBalance12 = leaveBal.NewBalance; break;
                            case 13: emp.LeaveBalance13 = leaveBal.NewBalance; break;
                            case 14: emp.LeaveBalance14 = leaveBal.NewBalance; break;
                            case 15: emp.LeaveBalance15 = leaveBal.NewBalance; break;
                        }

                    }
                    index += 1;
                }

            }


            // set leave and time reques/recommend count
            foreach (Report_HR_TeamEmployeeLeaveBalanceResult emp in list)
            {

                Report_HR_TeamEmployeeCountResult requestLeave = countLeavesReqquest.Where(
                    x => x.EmployeeId == emp.EmployeeId && x.Status == (int)LeaveRequestStatusEnum.Request).FirstOrDefault();

                Report_HR_TeamEmployeeCountResult recommendLeave = countLeavesReqquest.Where(
                    x => x.EmployeeId == emp.EmployeeId && x.Status == (int)LeaveRequestStatusEnum.Recommended).FirstOrDefault();

                emp.LeaveRequestCount = requestLeave == null || requestLeave.Total == null ? null : requestLeave.Total;
                emp.LeaveRecommendedCount = recommendLeave == null || recommendLeave.Total == null ? null : recommendLeave.Total;
                


                Report_HR_TeamEmployeeTimeRequestCountResult requestTime = countTimeRequest.Where(
                    x => x.EmployeeId == emp.EmployeeId && x.Status == (int)AttendanceRequestStatusEnum.Saved).FirstOrDefault();

                Report_HR_TeamEmployeeTimeRequestCountResult recommendTime = countTimeRequest.Where(
                    x => x.EmployeeId == emp.EmployeeId && x.Status == (int)AttendanceRequestStatusEnum.Recommended).FirstOrDefault();

                emp.TimeRequestCount = requestTime == null || requestTime.Total == null ? null : requestTime.Total;
                emp.TimeRecommendedCount = recommendTime == null || recommendTime.Total == null ? null : recommendTime.Total;
               
            }

            return list;
        }

        public static List<Report_Pay_SalarySummaryResult> GetSalarySummary(int month, int year, string empName,int branchId,
            string departmentIdList, int subDepartmentId, int costcodeId,int programId,int type,bool isForAllocationReport,int projectId,bool? isRetOnly,int empId)
        {
            return
                PayrollDataContext.Report_Pay_SalarySummary(month, year, SessionManager.CurrentCompanyId, empName,branchId
                , departmentIdList, subDepartmentId, costcodeId, programId, type, isForAllocationReport,projectId,isRetOnly,empId).ToList();
        }
        public static List<Report_Pay_AddonResult> GetAddOn(int addOnId,int branchId,int depId,string empSearch,int periodId)
        {
            return
                PayrollDataContext.Report_Pay_Addon(addOnId, branchId, depId, empSearch, periodId).ToList();
        }

        public static List<Report_Pay_PrevMonthAttendanceDeductionResult> GetAttendanceSummary(int month, int year)
        {
            return
                PayrollDataContext.Report_Pay_PrevMonthAttendanceDeduction(month, year, SessionManager.CurrentCompanyId
                ).ToList();
        }

        public static List<Report_Pay_SalaryExtractDepartmentWiseResult> GetDepartmentWiseSalaryExtract(int month, int year,int monthend,int yearend)
        {
            return
                PayrollDataContext.Report_Pay_SalaryExtractDepartmentWise(month, year,monthend,yearend, SessionManager.CurrentCompanyId).ToList();
        }
        public static List<Report_Pay_EmployeeVarianceDetailsResult> GetEmployeeVarianceReport(int payrollStartId, int payrollEndId, int? employeeId)
        {
            List<Report_Pay_EmployeeVarianceDetailsResult> list
                 = PayrollDataContext.Report_Pay_EmployeeVarianceDetails(payrollStartId, payrollEndId, SessionManager.CurrentCompanyId, employeeId)
                .ToList();
            return list;

        }
        public static List<Report_Pay_SalarySummary_MultipleMonthsResult> GetSalarySummaryForMultiple(int payrollStartId, int payrollEndId, string empName, int branchId, int departmentId,int subDepartmentId)
        {
            List<Report_Pay_SalarySummary_MultipleMonthsResult> list
                 = PayrollDataContext.Report_Pay_SalarySummary_MultipleMonths(payrollStartId, payrollEndId, SessionManager.CurrentCompanyId, empName, branchId, departmentId,subDepartmentId)
                .ToList();

            

            List<Report_Pay_SalarySummary_MultipleMonthsResult> newList = new List<Report_Pay_SalarySummary_MultipleMonthsResult>();
            // append multiple rows of the same emp into one
            //filter row so that only one object for multiple employee
            foreach (Report_Pay_SalarySummary_MultipleMonthsResult item in list)
            {

                Report_Pay_SalarySummary_MultipleMonthsResult currentItem = newList.Where(i => i.EmployeeId == item.EmployeeId).SingleOrDefault();

                if (currentItem == null)
                    newList.Add(item);
                else
                {
                    currentItem.List += item.List;
                }
            }

            StringBuilder changeList = new StringBuilder();
            //add for sum
            foreach (Report_Pay_SalarySummary_MultipleMonthsResult item in newList)
            {
                Dictionary<string, decimal> eachHeaders = new Dictionary<string, decimal>();
                if (item.List != null)
                {
                    string[] strs = item.List.Split(new char[] { ';' });
                    foreach (string header in strs)
                    {
                        string[] eachHeaderValue = header.Split(new char[] { ':' });
                        if (eachHeaderValue.Length >= 2)
                        {
                            string key = eachHeaderValue[0] + ":" + eachHeaderValue[1];
                            if (eachHeaders.ContainsKey(key))
                            {
                                eachHeaders[key] = eachHeaders[key] + Convert.ToDecimal(eachHeaderValue[2]);
                            }
                            else
                            {
                                eachHeaders.Add(key, Convert.ToDecimal(eachHeaderValue[2]));
                            }
                        }
                    }

                }
                changeList.Length = 0;
                //create separated string
                foreach (string key in eachHeaders.Keys)
                {
                    changeList.Append(
                        string.Format(";{0}:{1}",key, eachHeaders[key])
                        );
                }
                item.List = changeList.ToString();
            }

            return newList;
        }


        public static List<Report_Pay_DepartmentBranchWiseResult> ReportPayDepartmentBranchWise(int payrollStartId, int payrollEndId,int BranchID,int DepartmentID,int ShowType)
        {

            return PayrollDataContext.Report_Pay_DepartmentBranchWise(payrollStartId, payrollEndId, BranchID, DepartmentID, ShowType).ToList();
        }

        public static List<CalcGetHeaderListResult> GetHeaderList(int payrollStartId, int payrollEndId)
        {


            List<Report_Pay_SalarySummary_Multiple_ForHeaderListResult> list =

                PayrollDataContext.Report_Pay_SalarySummary_Multiple_ForHeaderList(payrollStartId, payrollEndId, SessionManager.CurrentCompanyId).ToList();


        
            Dictionary<string, CalcGetHeaderListResult> keys = new Dictionary<string, CalcGetHeaderListResult>();

            foreach (Report_Pay_SalarySummary_Multiple_ForHeaderListResult item in list)
            {
                CalcGetHeaderListResult newItem = new CalcGetHeaderListResult();
                newItem.Type = item.Type;
                newItem.SourceId = item.SourceId;
                newItem.HeaderName = item.HeaderName;

                if (!keys.ContainsKey(newItem.Type + ":" + newItem.SourceId))
                {
                    keys.Add(newItem.Type + ":" + newItem.SourceId, newItem);
                }
                

            }

            return keys.Values.ToList();

                    
        }

        public static List<Variance_GetAllPayVarianceReportHeaderResult> GetALLVarianceHeaderList()
        {
            return PayrollDataContext.Variance_GetAllPayVarianceReportHeader().OrderBy(x => x.HeaderName).ToList();
        }

        public static List<CalcGetHeaderListResult> GetVarianceHeaderList(int payrollStartId, int payrollEndId)
        {


            List<Variance_GetPayVarianceReportHeaderResult> list =

                PayrollDataContext.Variance_GetPayVarianceReportHeader(payrollStartId, payrollEndId).ToList();



            Dictionary<string, CalcGetHeaderListResult> keys = new Dictionary<string, CalcGetHeaderListResult>();

            foreach (Variance_GetPayVarianceReportHeaderResult item in list)
            {
                CalcGetHeaderListResult newItem = new CalcGetHeaderListResult();
                newItem.Type = item.Type;
                newItem.SourceId = item.SourceId;
                newItem.HeaderName = item.HeaderName;

                if (!keys.ContainsKey(newItem.Type + ":" + newItem.SourceId))
                {
                    keys.Add(newItem.Type + ":" + newItem.SourceId, newItem);
                }


            }

            return keys.Values.ToList();


        }

        public static List<GetYearEndSummaryHeaderResult> GetYearEndSummaryHeaders(int yearId)
        {
            return
                PayrollDataContext.GetYearEndSummaryHeader(yearId).ToList();
        }

        public static List<CalcGetHeaderListResult> GetRetirementHeaderList()
        {


            List<CalcGetRetirementHeaderListResult> list =

                PayrollDataContext.CalcGetRetirementHeaderList(SessionManager.CurrentCompanyId).ToList();



            Dictionary<string, CalcGetHeaderListResult> keys = new Dictionary<string, CalcGetHeaderListResult>();

            foreach (CalcGetRetirementHeaderListResult item in list)
            {
                CalcGetHeaderListResult newItem = new CalcGetHeaderListResult();
                newItem.Type = item.Type;
                newItem.SourceId = item.SourceId;
                newItem.HeaderName = item.HeaderName;
                if (item.IsRetirement == 1)
                    newItem.IsRetirement = true;
                else
                    newItem.IsRetirement = false;

                if (!keys.ContainsKey(newItem.Type + ":" + newItem.SourceId  + ":" + newItem.IsRetirement))
                {
                    keys.Add(newItem.Type + ":" + newItem.SourceId + ":" + newItem.IsRetirement, newItem);
                }


            }

            return keys.Values.ToList();


        }
        #endregion


        public static List<SalaryVariance_SummaryResult> GetSalaryVarianceSummary(int periodId, ref int? total)
        {
            int? totalsaved = 0;

            List<SalaryVariance_SummaryResult> list
                 = PayrollDataContext.SalaryVariance_Summary(periodId, ref total, ref totalsaved).ToList();

            return list;
        }
        public static List<SalaryVariance_SalaryChangeResult> GetSalaryChange(int periodId)
        {
            List<SalaryVariance_SalaryChangeResult> list
                 = PayrollDataContext.SalaryVariance_SalaryChange(periodId).ToList();

            return list;
        }


        public static List<Report_EmployeeHRDetailsListResult>
            GetEmployeeHRDetails(string empName, int branchID, int depID, int subDepID,int empid)
        {
            return
                PayrollDataContext.Report_EmployeeHRDetailsList(empName, branchID, depID, subDepID,

                SessionManager.CurrentCompanyId, "",empid).ToList();
        }

        public static List<TextValue> GetConsolidatedLeaveHeaderList(int payrollStartId, int payrollEndId)
        {

            List<PayrollPeriod> list = CommonManager.GetPayrollList(payrollStartId, payrollEndId);


            List<TextValue> headerList = new List<TextValue>();

            headerList.Add(new TextValue { Text = "Name", Value = "EmployeeName" });
            headerList.Add(new TextValue { Text = "Leave", Value = "Leave" });
            headerList.Add(new TextValue { Text = "Op", Value = "Opening" });

            foreach (PayrollPeriod payroll in list)
            {
                headerList.Add(new TextValue { Text = "+", Value = "Earned" + payroll.Name });
                headerList.Add(new TextValue { Text = "-", Value = "Taken" + payroll.Name });
                headerList.Add(new TextValue { Text = "Adj", Value = "ManualAdjustment" + payroll.Name });
                headerList.Add(new TextValue { Text = "Cl", Value = "Closing" + payroll.Name });
            }

            //headerList.Add(new TextValue { Text = "+", Value = "Total_Earned" });
            //headerList.Add(new TextValue { Text = "-", Value = "Total_Taken" });



            return headerList;


        }

        public static List<Report_HR_ConsolidatedLeaveBalanceResult> GetLeaveBalances(int payrollStartId,int payrollEndId,
            string empName,int leaveTypeId,int branchId,int departmentId,int employeeId,string depname,bool isEmployeeLogin)
        {
            return PayrollDataContext.Report_HR_ConsolidatedLeaveBalance
                (payrollStartId,payrollEndId,SessionManager.CurrentCompanyId,empName,
                leaveTypeId, branchId, departmentId, -1, employeeId, depname, isEmployeeLogin).ToList();
        }

       


        public static string PaySlipLogoAbsolutePath
        {
            get
            {
                //return HttpContext.Current.Server.MapPath("~/images/PaySlip_Company_Logo-145x80.png");
                return HttpContext.Current.Server.MapPath("~/images/PaySlip_Logo.png");
            }
        }
		
        public static List<GetServiceYearReportResult> GetServiceYearList(DateTime? from,DateTime? to,
            int branchId, int departmentId, bool? isMonth, double? value, int dateFrom)
        {
            string depIDList = SessionManager.CustomRoleDeparmentIDList;

            List<GetServiceYearReportResult> list =  PayrollDataContext.GetServiceYearReport(SessionManager.CurrentCompanyId,from,to,
                branchId, departmentId, isMonth, value, dateFrom, depIDList).OrderBy(x=>x.NAME).ToList();

            foreach (GetServiceYearReportResult item in list)
            {
                if (item.StatusOrJoinDate != null)
                {
                    item.ServicePeriod = EmployeeManager.GetServicePeriodUptoDateByEmployeeID(
                        item.StatusOrJoinDate.Value, to.Value);
                }
            }

            return list;
        }


        public static List<Report_GetRemoteAreaEmployeeListResult> GetRemoteAreaList()
        {
            return PayrollDataContext.Report_GetRemoteAreaEmployeeList().ToList();
        }

    }
}
