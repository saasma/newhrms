﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;
using System.Xml.Xsl;
using System.Text;
using System.Reflection;
using System.IO;
using System.Data;
using System.Diagnostics;
namespace Bll
{
    public static class ExportTable
    {
        public static DataTable TableToExport{get;set;}
    }

    public class ExcelHelper
    {
       

        ///// <summary>
        ///// Export the Ext Grid data into xls template for download
        ///// </summary>
        ///// <param name="downloadFileName"></param>
        ///// <param name="xml"></param>
        //public static void ExportExtGridToExcelTemplate(string downloadFileName,XmlNode xml)
        //{
        //    HttpResponse response =  HttpContext.Current.Response;
        //    response.Clear();
        //    response.ContentType = "application/vnd.ms-excel";
        //    response.AddHeader("Content-Disposition", "attachment; filename=" + downloadFileName + ".xls");
        //    XslCompiledTransform xtExcel = new XslCompiledTransform();
        //    xtExcel.Load(HttpContext.Current.Server.MapPath("~/Files/GridExcelExport.xsl"));
        //    xtExcel.Transform(xml, null, response.OutputStream);
        //    response.End();
        //}

        public static void ExportToExcel<T>(string name, List<T> list, List<String> hiddenColumnList, List<string> totalColumnList)
        {

            int columnCount = 0;
            List<int> hiddenColumn = new List<int>();
            List<int> totalColumn = new List<int>();

            Dictionary<int, decimal> columnTotal = new Dictionary<int, decimal>();
            DateTime StartTime = Utils.Helper.Util.GetCurrentDateAndTime();
            StringBuilder rowData = new StringBuilder();
            PropertyInfo[] properties = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            rowData.Append("<Row >");
            Boolean HasDepthLevel = false;
            //contains depth level value
            int depthIndex = 0;
            foreach (PropertyInfo p in properties)
            {
                if (p.Name.ToString().Equals("DepthLevel"))
                {
                    HasDepthLevel = true;
                    depthIndex = columnCount;
                }
                if (p.PropertyType.Name != "EntityCollection`1" && p.PropertyType.Name != "EntityReference`1" && p.PropertyType.Name != p.Name)
                {

                    if (hiddenColumnList.Contains(p.Name))
                    {

                        hiddenColumn.Add(columnCount);

                    }
                  
                    else
                    {
                        // track the column to calculate the total value
                        if (totalColumnList.Contains(p.Name))
                        {
                            totalColumn.Add(columnCount);
                            columnTotal.Add(columnCount, 0);
                        }

                        rowData.Append("<Cell ss:StyleID=\"s44\"><Data ss:Type=\"String\">" + p.Name + "</Data></Cell>");
                    }
                    columnCount++;

                }
                else
                    break;

            }

            rowData.Append("</Row>");
            decimal total = 0;
            foreach (T item in list)
            {
                int paddingColumn = 0;
                rowData.Append("<Row>");
                for (int x = 0; x < columnCount; x++) //each (PropertyInfo p in properties)
                {
                    if (!hiddenColumn.Contains(x)) //skip hidden column
                    {
                        object o = properties[x].GetValue(item, null);
                        string value = o == null ? "" : o.ToString();

                        //total calculation
                        if (totalColumn.Contains(x))
                        {
                            if (value.Equals(""))
                            {
                                value = "0";
                            }
                            total = decimal.Parse(value) + columnTotal[x];
                            columnTotal.Remove(x);
                            columnTotal.Add(x, total);

                            //For trial balance padding
                            //if (HasDepthLevel && paddingColumn == 0)
                            //{
                            //    object obj = properties[depthIndex].GetValue(item, null);
                            //    string spacing = GetDepthLevelSpacing(obj);
                            //    rowData.Append("<Cell ss:StyleID=\"s63\"><Data ss:Type=\"Number\">" + spacing + value + "</Data></Cell>");
                            //}
                            //else
                            {
                                rowData.Append("<Cell ss:StyleID=\"s63\"><Data ss:Type=\"Number\">" + value + "</Data></Cell>");
                            }


                        }
                        else
                        {

                            //if (HasDepthLevel && paddingColumn == 0)
                            //{

                            //    object obj = properties[depthIndex].GetValue(item, null);
                            //    string spacing = GetDepthLevelSpacing(obj);
                            //    rowData.Append("<Cell><Data ss:Type=\"String\">" + spacing + value + "</Data></Cell>");
                            //}
                            //else
                            {
                                rowData.Append("<Cell><Data ss:Type=\"String\">" + value + "</Data></Cell>");
                            }
                        }

                        //increament spacing for column so that spacing for the next column will not be applied
                        paddingColumn++;

                    }
                }

                rowData.Append("</Row >");

            }

            Boolean IsfirstColumn = true;
            rowData.Append("<Row >");
            for (int x = 0; x < columnCount; x++)
            {
                string value = "";
                if (!hiddenColumn.Contains(x))
                {
                    if (IsfirstColumn)
                    {
                        //Total label
                        rowData.Append("<Cell ss:StyleID=\"sTotal\"><Data ss:Type=\"String\">" + "Total" + "</Data></Cell>");
                        IsfirstColumn = false;
                    }
                    else if (totalColumn.Contains(x))
                    {
                        rowData.Append("<Cell ss:StyleID=\"s14\" ><Data ss:Type=\"Number\">" + columnTotal[x] + "</Data></Cell>");

                    }
                    else
                    {
                        rowData.Append("<Cell ss:StyleID=\"sTotal\"><Data ss:Type=\"String\">" + value + "</Data></Cell>");
                    }
                }

            }
            rowData.Append("</Row>");
            string url = HttpContext.Current.Server.MapPath("~/App_Data/ExcelTemplate.xml");
            StringBuilder sheet = new StringBuilder();
            sheet.AppendFormat(GetXmlString(url), properties.Count(), list.Count() + 2, rowData.ToString());
            System.Diagnostics.Debug.Print(StartTime.ToString() + " - " + Utils.Helper.Util.GetCurrentDateAndTime());
            System.Diagnostics.Debug.Print((Utils.Helper.Util.GetCurrentDateAndTime() - StartTime).ToString());
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.Charset = "";
            string attachment = "attachment; filename=\"" + name + "\".xls";
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.AddHeader("content-disposition", attachment);
            HttpContext.Current.Response.Write(sheet.ToString());
            HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";
            HttpContext.Current.Response.End();

        }


        //Dictionary key = column name; value = new column name
        public static void ExportToExcel<T>(string name, List<T> list, List<String> hiddenColumnList, List<string> totalColumnList,Dictionary<string,string> columnRenameList,List<string> numericColumnList)
        {
            
            int columnCount = 0;
            List<int> hiddenColumn = new List<int>();
            List<int> totalColumn = new List<int>();

            //numeric data type column
            List<int> numericColum = new List<int>();
            Dictionary<int, decimal> columnTotal = new Dictionary<int, decimal>();
            DateTime StartTime = Utils.Helper.Util.GetCurrentDateAndTime(); ;
            StringBuilder rowData = new StringBuilder();
            PropertyInfo[] properties = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            rowData.Append("<Row >");
            Boolean HasDepthLevel = false;
            //contains depth level value
            int depthIndex = 0;
            foreach (PropertyInfo p in properties)
            {
                if(p.Name.ToString().Equals("DepthLevel"))
                {
                    HasDepthLevel = true;
                    depthIndex = columnCount;
                }
                if (p.PropertyType.Name != "EntityCollection`1" && p.PropertyType.Name != "EntityReference`1" && p.PropertyType.Name != p.Name)
                {
                   
                    if (hiddenColumnList.Contains(p.Name))
                    {
                        
                        hiddenColumn.Add(columnCount);

                    }
                    
                    else
                    {
                        // track the column to calculate the total value
                        if (totalColumnList.Contains(p.Name))
                        {
                            totalColumn.Add(columnCount);
                            columnTotal.Add(columnCount, 0);
                        }
                        else if (numericColumnList.Contains(p.Name))
                        {
                            //track numeric colum to set the data type numeric
                            numericColum.Add(columnCount);
                        }

                        //prepare column header name
                        string headerName = string.Empty;
                        if (columnRenameList.ContainsKey(p.Name))
                        {
                            headerName = columnRenameList[p.Name];
                        }
                        else
                        {
                            headerName = p.Name;
                        }
                        rowData.Append("<Cell ss:StyleID=\"s44\"><Data ss:Type=\"String\">" + headerName + "</Data></Cell>");
                    }
                    columnCount++;
                    
                }
                else
                    break;

            }

            rowData.Append("</Row>");
            decimal total = 0;
            foreach (T item in list)
            {
                int paddingColumn = 0;
                rowData.Append("<Row>");
                for (int x = 0; x < columnCount; x++) //each (PropertyInfo p in properties)
                {
                    if (!hiddenColumn.Contains(x)) //skip hidden column
                    {
                        object o = properties[x].GetValue(item, null);
                        string cellValue = o == null ? "" : o.ToString();
                        string value = o == null ? "" : o.ToString();

                        //total calculation
                        if (totalColumn.Contains(x))
                        {
                            if (value.Equals(""))
                            {
                                value = "0";
                            }
                            total = decimal.Parse(value) + columnTotal[x];
                            columnTotal.Remove(x);
                            columnTotal.Add(x, total);

                            //////For trial balance padding
                            ////if (HasDepthLevel && paddingColumn == 0)
                            ////{
                            ////    object obj = properties[depthIndex].GetValue(item, null);
                            ////    string spacing = GetDepthLevelSpacing(obj);
                            ////    //text bold style
                            ////    if ((int)obj<1)
                            ////    {
                            ////        rowData.Append("<Cell ss:StyleID=\"s66\"><Data ss:Type=\"Number\">" + spacing + cellValue + "</Data></Cell>");
                            ////    }
                            ////    else
                            ////        rowData.Append("<Cell ss:StyleID=\"s63\"><Data ss:Type=\"Number\">" + spacing + cellValue + "</Data></Cell>");
                            ////}
                            ////else
                            {
                                rowData.Append("<Cell ss:StyleID=\"s63\"><Data ss:Type=\"Number\">" + cellValue + "</Data></Cell>");
                            }


                        }
                        else
                        {   
                            
                            //if (HasDepthLevel && paddingColumn == 0)
                            //{
                             
                            //    object obj = properties[depthIndex].GetValue(item, null);
                            //    string spacing = GetDepthLevelSpacing(obj);
                            //    //text bold style
                            //    if ((int)obj < 1)
                            //    {
                            //        rowData.Append("<Cell  ss:StyleID=\"s66\"><Data ss:Type=\"String\">" + spacing + cellValue + "</Data></Cell>");
                            //    }
                            //    else
                            //        rowData.Append("<Cell><Data ss:Type=\"String\">" + spacing + cellValue + "</Data></Cell>");
                            //}
                            if (numericColum.Contains(x)) //sets data type numeric
                            {
                                rowData.Append("<Cell ss:StyleID=\"s63\"><Data ss:Type=\"Number\">" + cellValue + "</Data></Cell>");
                            }
                            else
                            {
                                rowData.Append("<Cell><Data ss:Type=\"String\">" + cellValue + "</Data></Cell>");
                            }
                        }

                        //increament spacing for column so that spacing for the next column will not be applied
                        paddingColumn++;
                        
                    }
                }

                rowData.Append("</Row >");
                
            }
           
            Boolean IsfirstColumn = true;
            rowData.Append("<Row >");
            for (int x = 0; x < columnCount; x++)
            {
                string value = "";
                if (!hiddenColumn.Contains(x))
                {
                    if (IsfirstColumn)
                    {
                        //Total label
                        rowData.Append("<Cell ss:StyleID=\"sTotal\"><Data ss:Type=\"String\">" + "Total" + "</Data></Cell>");
                        IsfirstColumn = false;
                    }
                    else if (totalColumn.Contains(x))
                    {
                        rowData.Append("<Cell ss:StyleID=\"s14\" ><Data ss:Type=\"Number\">" + columnTotal[x] + "</Data></Cell>");

                    }
                    else
                    {
                        rowData.Append("<Cell ss:StyleID=\"sTotal\"><Data ss:Type=\"String\">" + value + "</Data></Cell>");
                    }
                }

            }
            rowData.Append("</Row>");
            string url = HttpContext.Current.Server.MapPath("~/App_Data/ExcelTemplate.xml");
            StringBuilder sheet = new StringBuilder();
            sheet.AppendFormat(GetXmlString(url), properties.Count(), list.Count() + 2, rowData.ToString());
            System.Diagnostics.Debug.Print(StartTime.ToString() + " - " + Utils.Helper.Util.GetCurrentDateAndTime());
            System.Diagnostics.Debug.Print((Utils.Helper.Util.GetCurrentDateAndTime() - StartTime).ToString());
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.ClearHeaders();
            HttpContext.Current.Response.Charset = "";
            string attachment = "attachment; filename=\"" + name + "\".xls";
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.AddHeader("content-disposition", attachment);
            HttpContext.Current.Response.Write(sheet.ToString());
            HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";
            HttpContext.Current.Response.End();
           
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="name"></param>
        /// <param name="list"></param>
        /// <param name="hiddenColumnList"></param>
        /// <param name="totalColumnList"></param>
        /// <param name="columnRenameList"></param>
        /// <param name="numericColumnList"></param>
        /// <param name="title"></param>
        /// <param name="propertyNamesOrderList"></param>
        public static void ExportToExcel<T>(string name, List<T> list, List<String> hiddenColumnList, List<string> totalColumnList, Dictionary<string, string> columnRenameList, List<string> numericColumnList, Dictionary<string, string> title, List<String> propertyNamesOrderList)
        {

            int columnCount = 0;
            List<int> hiddenColumn = new List<int>();
            List<int> totalColumn = new List<int>();
            //numeric data type column
            List<int> numericColum = new List<int>();
            Dictionary<int, decimal> columnTotal = new Dictionary<int, decimal>();
            DateTime StartTime = Utils.Helper.Util.GetCurrentDateAndTime();
            StringBuilder rowData = new StringBuilder();

            //set title
            if (title != null && title.Count != 0)
            {
                foreach (var v in title)
                {
                    rowData.Append("<Row>");
                    rowData.AppendFormat("<Cell ss:StyleID=\"s70\"><Data ss:Type=\"String\">{0}</Data></Cell>", v.Key);
                    rowData.AppendFormat("<Cell ss:StyleID=\"s70\"><Data ss:Type=\"String\">{0}</Data></Cell>", v.Value);
                    rowData.Append("</Row>");

                }

                rowData.Append("<Row></Row>");
            }
            //add empty row
           
            PropertyInfo[] properties = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);


            if (propertyNamesOrderList != null && propertyNamesOrderList.Count != 0)
            {
                List<PropertyInfo> orderList = new List<PropertyInfo>();
                foreach (string property in propertyNamesOrderList)
                {
                    foreach (PropertyInfo p in properties)
                    {
                        if (p.Name == property)
                        {
                            orderList.Add(p);
                        }
                    }
                }

                foreach (PropertyInfo p in properties)
                {
                    if (orderList.Any(x => x.Name == p.Name) == false)
                    {
                        orderList.Add(p);
                    }
                }

                properties = orderList.ToArray();

            }
            rowData.Append("<Row >");
            Boolean HasDepthLevel = false;
            //contains depth level value
            int depthIndex = 0;
            foreach (PropertyInfo p in properties)
            {
                if (p.Name.ToString().Equals("DepthLevel"))
                {
                    HasDepthLevel = true;
                    depthIndex = columnCount;
                }
                if (p.PropertyType.Name != "EntitySet`1" && p.PropertyType.Name != "EntityCollection`1" && p.PropertyType.Name != "EntityReference`1" && p.PropertyType.Name != p.Name)
                {

                    if (hiddenColumnList.Contains(p.Name))
                    {

                        hiddenColumn.Add(columnCount);

                    }

                    else
                    {
                        // track the column to calculate the total value
                        if (totalColumnList.Contains(p.Name))
                        {
                            totalColumn.Add(columnCount);
                            columnTotal.Add(columnCount, 0);
                        }
                        else if (numericColumnList.Contains(p.Name))
                        {
                            //track numeric colum to set the data type numeric
                            numericColum.Add(columnCount);
                        }

                        //prepare column header name
                        string headerName = string.Empty;
                        if (columnRenameList.ContainsKey(p.Name))
                        {
                            headerName = columnRenameList[p.Name];
                        }
                        else
                        {
                            headerName = p.Name;
                        }
                        rowData.AppendFormat("<Cell ss:StyleID=\"s44\"><Data ss:Type=\"String\">{0}</Data></Cell>", headerName);
                    }
                    columnCount++;

                }
                else
                    break;

            }

            rowData.Append("</Row>");
            decimal total = 0;
            foreach (T item in list)
            {
                int paddingColumn = 0;
                rowData.Append("<Row>");
                for (int x = 0; x < columnCount; x++) //each (PropertyInfo p in properties)
                {
                    if (!hiddenColumn.Contains(x)) //skip hidden column
                    {
                        object o = properties[x].GetValue(item, null);
                        string cellValue = o == null ? "" : o.ToString();
                        string value = o == null ? "" : o.ToString();

                        //total calculation
                        if (totalColumn.Contains(x))
                        {
                            if (value.Equals(""))
                            {
                                value = "0";
                            }
                            total = decimal.Parse(value) + columnTotal[x];
                            columnTotal.Remove(x);
                            columnTotal.Add(x, total);

                            //For trial balance padding
                            //if (HasDepthLevel && paddingColumn == 0)
                            //{
                            //    object obj = properties[depthIndex].GetValue(item, null);
                            //    string spacing = GetDepthLevelSpacing(obj);
                            //    //text bold style
                            //    if ((int)obj < 1)
                            //    {
                            //        rowData.Append(String.Format("<Cell ss:StyleID=\"s66\"><Data ss:Type=\"Number\">{0}{1}</Data></Cell>", spacing, cellValue));
                            //    }
                            //    else
                            //        rowData.Append(String.Format("<Cell ss:StyleID=\"s63\"><Data ss:Type=\"Number\">{0}{1}</Data></Cell>", spacing, cellValue));
                            //}
                            //else
                            {
                                rowData.AppendFormat("<Cell ss:StyleID=\"s63\"><Data ss:Type=\"Number\">{0}</Data></Cell>", cellValue);
                            }


                        }
                        else
                        {

                            //if (HasDepthLevel && paddingColumn == 0)
                            //{

                            //    object obj = properties[depthIndex].GetValue(item, null);
                            //    string spacing = GetDepthLevelSpacing(obj);
                            //    //text bold style
                            //    if ((int)obj < 1)
                            //    {
                            //        rowData.Append(String.Format("<Cell  ss:StyleID=\"s66\"><Data ss:Type=\"String\">{0}{1}</Data></Cell>", spacing, cellValue));
                            //    }
                            //    else
                            //        rowData.Append(String.Format("<Cell><Data ss:Type=\"String\">{0}{1}</Data></Cell>", spacing, cellValue));
                            //}
                             if (numericColum.Contains(x)) //sets data type numeric
                            {
                                rowData.AppendFormat("<Cell ss:StyleID=\"s63\"><Data ss:Type=\"Number\">{0}</Data></Cell>", cellValue);
                            }
                            else
                            {
                                rowData.AppendFormat("<Cell><Data ss:Type=\"String\">{0}</Data></Cell>", cellValue);
                            }
                        }

                        //increament spacing for column so that spacing for the next column will not be applied
                        paddingColumn++;

                    }
                }

                rowData.Append("</Row >");

            }

            Boolean IsfirstColumn = true;
            if (totalColumnList.Count != 0)
            {
                rowData.Append("<Row >");
                for (int x = 0; x < columnCount; x++)
                {
                    string value = "";
                    if (!hiddenColumn.Contains(x))
                    {
                        if (IsfirstColumn)
                        {
                            //Total label
                            rowData.Append("<Cell ss:StyleID=\"sTotal\"><Data ss:Type=\"String\">" + "Total" + "</Data></Cell>");
                            IsfirstColumn = false;
                        }
                        else if (totalColumn.Contains(x))
                        {
                            rowData.AppendFormat("<Cell ss:StyleID=\"s14\" ><Data ss:Type=\"Number\">{0}</Data></Cell>", columnTotal[x]);

                        }
                        else
                        {
                            rowData.AppendFormat("<Cell ss:StyleID=\"sTotal\"><Data ss:Type=\"String\">{0}</Data></Cell>", value);
                        }
                    }

                }
                rowData.Append("</Row>");
            }

            string url = HttpContext.Current.Server.MapPath("~/App_Data/ExcelTemplate.xml");
            StringBuilder sheet = new StringBuilder();
            //here 1 is for empty row
            sheet.AppendFormat(GetXmlString(url), properties.Count(), list.Count() + 2 + title.Count() + 1, rowData.ToString());
            System.Diagnostics.Debug.Print(StartTime.ToString() + " - " + Utils.Helper.Util.GetCurrentDateAndTime());
            System.Diagnostics.Debug.Print((Utils.Helper.Util.GetCurrentDateAndTime() - StartTime).ToString());
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.Charset = "";
            string attachment = "attachment; filename=\"" + name + "\".xls";
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.AddHeader("content-disposition", attachment);
            HttpContext.Current.Response.Write(sheet.ToString());
            HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";
            HttpContext.Current.Response.End();

        }

        public static void ExportToExcel<T>(string name, List<T> list, List<String> hiddenColumnList, List<string> totalColumnList, 
            Dictionary<string, string> columnRenameList, List<string> numericColumnList, List<string> integerColumnList,
            List<string> dateColumnList,Dictionary<string, string> title, List<String> propertyNamesOrderList)
        {

            int columnCount = 0;
            List<int> hiddenColumn = new List<int>();
            List<int> totalColumn = new List<int>();
            //numeric data type column
            List<int> numericColum = new List<int>();
            List<int> integerColum = new List<int>();
            List<int> dateColum = new List<int>();
            Dictionary<int, decimal> columnTotal = new Dictionary<int, decimal>();
            DateTime StartTime = Utils.Helper.Util.GetCurrentDateAndTime();
            StringBuilder rowData = new StringBuilder();

            //set title
            if (title != null)
            {
                foreach (var v in title)
                {
                    rowData.Append("<Row>");
                    rowData.Append(String.Format("<Cell ss:StyleID=\"s70\"><Data ss:Type=\"String\">{0}</Data></Cell>", v.Key));
                    rowData.Append(String.Format("<Cell ss:StyleID=\"s70\"><Data ss:Type=\"String\">{0}</Data></Cell>", v.Value));
                    rowData.Append("</Row>");

                }
            }
            //add empty row
            rowData.Append("<Row></Row>");
            PropertyInfo[] properties = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);


            if (propertyNamesOrderList != null && propertyNamesOrderList.Count != 0)
            {
                List<PropertyInfo> orderList = new List<PropertyInfo>();
                foreach (string property in propertyNamesOrderList)
                {
                    foreach (PropertyInfo p in properties)
                    {
                        if (p.Name == property)
                        {
                            orderList.Add(p);
                        }
                    }
                }

                foreach (PropertyInfo p in properties)
                {
                    if (orderList.Any(x => x.Name == p.Name) == false)
                    {
                        orderList.Add(p);
                    }
                }

                properties = orderList.ToArray();

            }
            rowData.Append("<Row >");
            Boolean HasDepthLevel = false;
            //contains depth level value
            int depthIndex = 0;
            foreach (PropertyInfo p in properties)
            {
                if (p.Name.ToString().Equals("DepthLevel"))
                {
                    HasDepthLevel = true;
                    depthIndex = columnCount;
                }
                if (p.PropertyType.Name != "EntityCollection`1" && p.PropertyType.Name != "EntityReference`1" && p.PropertyType.Name != p.Name)
                {

                    if (hiddenColumnList.Contains(p.Name))
                    {

                        hiddenColumn.Add(columnCount);

                    }

                    else
                    {
                        // track the column to calculate the total value
                        if (totalColumnList.Contains(p.Name))
                        {
                            totalColumn.Add(columnCount);
                            columnTotal.Add(columnCount, 0);
                        }
                        else if (numericColumnList.Contains(p.Name))
                        {
                            //track numeric colum to set the data type numeric
                            numericColum.Add(columnCount);
                        }
                        else if (integerColumnList.Contains(p.Name))
                        {
                            //track numeric colum to set the data type numeric
                            integerColum.Add(columnCount);
                        }
                        else if (dateColumnList.Contains(p.Name))
                        {
                            dateColum.Add(columnCount);
                        }

                        //prepare column header name
                        string headerName = string.Empty;
                        if (columnRenameList.ContainsKey(p.Name))
                        {
                            headerName = columnRenameList[p.Name];
                        }
                        else
                        {
                            headerName = p.Name;
                        }
                        rowData.AppendFormat("<Cell ss:StyleID=\"s44\"><Data ss:Type=\"String\">{0}</Data></Cell>", headerName);
                    }
                    columnCount++;

                }
                else
                    break;

            }

            rowData.Append("</Row>");
            decimal total = 0;
            foreach (T item in list)
            {
                int paddingColumn = 0;
                rowData.Append("<Row>");
                for (int x = 0; x < columnCount; x++) //each (PropertyInfo p in properties)
                {
                    if (!hiddenColumn.Contains(x)) //skip hidden column
                    {
                        object o = properties[x].GetValue(item, null);
                        string cellValue = o == null ? "" : o.ToString();
                        string value = o == null ? "" : o.ToString();

                        //total calculation
                        if (totalColumn.Contains(x))
                        {
                            if (value.Equals(""))
                            {
                                value = "0";
                            }
                            total = decimal.Parse(value) + columnTotal[x];
                            columnTotal.Remove(x);
                            columnTotal.Add(x, total);

                            //For trial balance padding
                            //if (HasDepthLevel && paddingColumn == 0)
                            //{
                            //    object obj = properties[depthIndex].GetValue(item, null);
                            //    string spacing = GetDepthLevelSpacing(obj);
                            //    //text bold style
                            //    if ((int)obj < 1)
                            //    {
                            //        rowData.Append(String.Format("<Cell ss:StyleID=\"s66\"><Data ss:Type=\"Number\">{0}{1}</Data></Cell>", spacing, cellValue));
                            //    }
                            //    else
                            //        rowData.Append(String.Format("<Cell ss:StyleID=\"s63\"><Data ss:Type=\"Number\">{0}{1}</Data></Cell>", spacing, cellValue));
                            //}
                            //else
                            {
                                rowData.AppendFormat("<Cell ss:StyleID=\"s63\"><Data ss:Type=\"Number\">{0}</Data></Cell>", cellValue);
                            }


                        }
                        else
                        {

                            //if (HasDepthLevel && paddingColumn == 0)
                            //{

                            //    object obj = properties[depthIndex].GetValue(item, null);
                            //    string spacing = GetDepthLevelSpacing(obj);
                            //    //text bold style
                            //    if ((int)obj < 1)
                            //    {
                            //        rowData.Append(String.Format("<Cell  ss:StyleID=\"s66\"><Data ss:Type=\"String\">{0}{1}</Data></Cell>", spacing, cellValue));
                            //    }
                            //    else
                            //        rowData.Append(String.Format("<Cell><Data ss:Type=\"String\">{0}{1}</Data></Cell>", spacing, cellValue));
                            //}
                            if (numericColum.Contains(x)) //sets data type numeric
                            {
                                rowData.AppendFormat("<Cell ss:StyleID=\"s63\"><Data ss:Type=\"Number\">{0}</Data></Cell>", cellValue);
                            }
                            else if (integerColum.Contains(x)) //sets data type numeric
                            {
                                rowData.AppendFormat("<Cell ss:StyleID=\"s67\"><Data ss:Type=\"Number\">{0}</Data></Cell>", cellValue);
                            }
                            else if (dateColum.Contains(x)) //sets data type numeric
                            {
                                DateTime date;
                                if (DateTime.TryParse(cellValue, out date))
                                    cellValue = date.ToString("yyyy-MM-dd");
                                else
                                    cellValue = "";

                                rowData.AppendFormat("<Cell ss:StyleID=\"s65\"><Data ss:Type=\"DateTime\">{0}</Data></Cell>", cellValue);
                            }
                            else
                            {
                                rowData.AppendFormat("<Cell><Data ss:Type=\"String\">{0}</Data></Cell>", cellValue);
                            }
                        }

                        //increament spacing for column so that spacing for the next column will not be applied
                        paddingColumn++;

                    }
                }

                rowData.Append("</Row >");

            }

            Boolean IsfirstColumn = true;
            if (totalColumnList.Count != 0)
            {
                rowData.Append("<Row >");
                for (int x = 0; x < columnCount; x++)
                {
                    string value = "";
                    if (!hiddenColumn.Contains(x))
                    {
                        if (IsfirstColumn)
                        {
                            //Total label
                            rowData.Append("<Cell ss:StyleID=\"sTotal\"><Data ss:Type=\"String\">" + "Total" + "</Data></Cell>");
                            IsfirstColumn = false;
                        }
                        else if (totalColumn.Contains(x))
                        {
                            rowData.AppendFormat("<Cell ss:StyleID=\"s14\" ><Data ss:Type=\"Number\">{0}</Data></Cell>", columnTotal[x]);

                        }
                        else
                        {
                            rowData.AppendFormat("<Cell ss:StyleID=\"sTotal\"><Data ss:Type=\"String\">{0}</Data></Cell>", value);
                        }
                    }

                }
                rowData.Append("</Row>");
            }

            string url = HttpContext.Current.Server.MapPath("~/App_Data/ExcelTemplateIntegerDate.xml");
            StringBuilder sheet = new StringBuilder();
            //here 1 is for empty row
            sheet.AppendFormat(GetXmlString(url), properties.Count(), list.Count() + 2 + title.Count() + 1, rowData.ToString());
            System.Diagnostics.Debug.Print(StartTime.ToString() + " - " + Utils.Helper.Util.GetCurrentDateAndTime());
            System.Diagnostics.Debug.Print((Utils.Helper.Util.GetCurrentDateAndTime() - StartTime).ToString());
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.Charset = "";
            string attachment = "attachment; filename=\"" + name + "\".xls";
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.AddHeader("content-disposition", attachment);
            HttpContext.Current.Response.Write(sheet.ToString());
            HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";
            HttpContext.Current.Response.End();

        }

        public static void ExportToExcel<T>(string name, List<T> list, List<String> hiddenColumnList, List<string> totalColumnList,
              Dictionary<string, string> columnRenameList, List<string> numericColumnList, List<string> numericWithOutCommaColumnList, 
              List<string> integerColumnList,List<string> dateColumnList, Dictionary<string, string> title, List<String> propertyNamesOrderList)
        {

            int columnCount = 0;
            List<int> hiddenColumn = new List<int>();
            List<int> totalColumn = new List<int>();
            //numeric data type column
            List<int> numericColum = new List<int>();
            List<int> numericWithOutCommanColumn = new List<int>();
            List<int> integerColum = new List<int>();
            List<int> dateColum = new List<int>();
            Dictionary<int, decimal> columnTotal = new Dictionary<int, decimal>();
            DateTime StartTime = Utils.Helper.Util.GetCurrentDateAndTime();
            StringBuilder rowData = new StringBuilder();

            //set title
            if (title != null)
            {
                foreach (var v in title)
                {
                    rowData.Append("<Row>");
                    rowData.Append(String.Format("<Cell ss:StyleID=\"s70\"><Data ss:Type=\"String\">{0}</Data></Cell>", v.Key));
                    rowData.Append(String.Format("<Cell ss:StyleID=\"s70\"><Data ss:Type=\"String\">{0}</Data></Cell>", v.Value));
                    rowData.Append("</Row>");

                }


                //add empty row
                if (title.Keys.Count > 0)
                    rowData.Append("<Row></Row>");
            }
           
            PropertyInfo[] properties = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);


            if (propertyNamesOrderList != null && propertyNamesOrderList.Count != 0)
            {
                List<PropertyInfo> orderList = new List<PropertyInfo>();
                foreach (string property in propertyNamesOrderList)
                {
                    foreach (PropertyInfo p in properties)
                    {
                        if (p.Name == property)
                        {
                            orderList.Add(p);
                        }
                    }
                }

                foreach (PropertyInfo p in properties)
                {
                    if (orderList.Any(x => x.Name == p.Name) == false)
                    {
                        orderList.Add(p);
                    }
                }

                properties = orderList.ToArray();

            }
            rowData.Append("<Row>");
            Boolean HasDepthLevel = false;
            //contains depth level value
            int depthIndex = 0;
            foreach (PropertyInfo p in properties)
            {
                if (p.Name.ToString().Equals("DepthLevel"))
                {
                    HasDepthLevel = true;
                    depthIndex = columnCount;
                }
                if (p.PropertyType.Name != "EntityCollection`1" && p.PropertyType.Name != "EntityReference`1" && p.PropertyType.Name != p.Name)
                {

                    if (hiddenColumnList.Contains(p.Name))
                    {

                        hiddenColumn.Add(columnCount);

                    }

                    else
                    {
                        // track the column to calculate the total value
                        if (totalColumnList.Contains(p.Name))
                        {
                            totalColumn.Add(columnCount);
                            columnTotal.Add(columnCount, 0);
                        }
                        else if (numericColumnList.Contains(p.Name))
                        {
                            //track numeric colum to set the data type numeric
                            numericColum.Add(columnCount);
                        }
                        else if (numericWithOutCommaColumnList.Contains(p.Name))
                        {
                            //track numeric colum to set the data type numeric
                            numericWithOutCommanColumn.Add(columnCount);
                        }
                        else if (integerColumnList.Contains(p.Name))
                        {
                            //track numeric colum to set the data type numeric
                            integerColum.Add(columnCount);
                        }
                        else if (dateColumnList.Contains(p.Name))
                        {
                            dateColum.Add(columnCount);
                        }

                        //prepare column header name
                        string headerName = string.Empty;
                        if (columnRenameList.ContainsKey(p.Name))
                        {
                            headerName = columnRenameList[p.Name];
                        }
                        else
                        {
                            headerName = p.Name;
                        }
                        rowData.AppendFormat("<Cell ss:StyleID=\"s44\"><Data ss:Type=\"String\">{0}</Data></Cell>", headerName);
                    }
                    columnCount++;

                }
                else
                    break;

            }

            rowData.Append("</Row>");
            decimal total = 0;
            foreach (T item in list)
            {
                int paddingColumn = 0;
                rowData.Append("<Row>");
                for (int x = 0; x < columnCount; x++) //each (PropertyInfo p in properties)
                {
                    if (!hiddenColumn.Contains(x)) //skip hidden column
                    {
                        object o = properties[x].GetValue(item, null);
                        string cellValue = o == null ? "" : o.ToString();
                        string value = o == null ? "" : o.ToString();

                        //total calculation
                        if (totalColumn.Contains(x))
                        {
                            if (value.Equals(""))
                            {
                                value = "0";
                            }
                            total = decimal.Parse(value) + columnTotal[x];
                            columnTotal.Remove(x);
                            columnTotal.Add(x, total);

                            //For trial balance padding
                            //if (HasDepthLevel && paddingColumn == 0)
                            //{
                            //    object obj = properties[depthIndex].GetValue(item, null);
                            //    string spacing = GetDepthLevelSpacing(obj);
                            //    //text bold style
                            //    if ((int)obj < 1)
                            //    {
                            //        rowData.Append(String.Format("<Cell ss:StyleID=\"s66\"><Data ss:Type=\"Number\">{0}{1}</Data></Cell>", spacing, cellValue));
                            //    }
                            //    else
                            //        rowData.Append(String.Format("<Cell ss:StyleID=\"s63\"><Data ss:Type=\"Number\">{0}{1}</Data></Cell>", spacing, cellValue));
                            //}
                            //else
                            {
                                rowData.AppendFormat("<Cell ss:StyleID=\"s63\"><Data ss:Type=\"Number\">{0}</Data></Cell>", cellValue);
                            }


                        }
                        else
                        {

                            //if (HasDepthLevel && paddingColumn == 0)
                            //{

                            //    object obj = properties[depthIndex].GetValue(item, null);
                            //    string spacing = GetDepthLevelSpacing(obj);
                            //    //text bold style
                            //    if ((int)obj < 1)
                            //    {
                            //        rowData.Append(String.Format("<Cell  ss:StyleID=\"s66\"><Data ss:Type=\"String\">{0}{1}</Data></Cell>", spacing, cellValue));
                            //    }
                            //    else
                            //        rowData.Append(String.Format("<Cell><Data ss:Type=\"String\">{0}{1}</Data></Cell>", spacing, cellValue));
                            //}
                            if (numericColum.Contains(x)) //sets data type numeric
                            {
                                rowData.AppendFormat("<Cell ss:StyleID=\"s63\"><Data ss:Type=\"Number\">{0}</Data></Cell>", cellValue);
                            }
                            else if (numericWithOutCommanColumn.Contains(x)) //sets data type numeric
                            {
                                rowData.AppendFormat("<Cell ss:StyleID=\"s62\"><Data ss:Type=\"Number\">{0}</Data></Cell>", cellValue);
                            }
                            else if (integerColum.Contains(x)) //sets data type numeric
                            {
                                rowData.AppendFormat("<Cell ss:StyleID=\"s67\"><Data ss:Type=\"Number\">{0}</Data></Cell>", cellValue);
                            }
                            else if (dateColum.Contains(x)) //sets data type numeric
                            {
                                DateTime date;
                                if (DateTime.TryParse(cellValue, out date))
                                    cellValue = date.ToString("yyyy-MM-dd");
                                else
                                    cellValue = "";

                                rowData.AppendFormat("<Cell ss:StyleID=\"s65\"><Data ss:Type=\"DateTime\">{0}</Data></Cell>", cellValue);
                            }
                            else
                            {
                                rowData.AppendFormat("<Cell><Data ss:Type=\"String\">{0}</Data></Cell>", cellValue);
                            }
                        }

                        //increament spacing for column so that spacing for the next column will not be applied
                        paddingColumn++;

                    }
                }

                rowData.Append("</Row >");

            }

            Boolean IsfirstColumn = true;
            if (totalColumnList.Count != 0)
            {
                rowData.Append("<Row >");
                for (int x = 0; x < columnCount; x++)
                {
                    string value = "";
                    if (!hiddenColumn.Contains(x))
                    {
                        if (IsfirstColumn)
                        {
                            //Total label
                            rowData.Append("<Cell ss:StyleID=\"sTotal\"><Data ss:Type=\"String\">" + "Total" + "</Data></Cell>");
                            IsfirstColumn = false;
                        }
                        else if (totalColumn.Contains(x))
                        {
                            rowData.AppendFormat("<Cell ss:StyleID=\"s14\" ><Data ss:Type=\"Number\">{0}</Data></Cell>", columnTotal[x]);

                        }
                        else
                        {
                            rowData.AppendFormat("<Cell ss:StyleID=\"sTotal\"><Data ss:Type=\"String\">{0}</Data></Cell>", value);
                        }
                    }

                }
                rowData.Append("</Row>");
            }

            string url = HttpContext.Current.Server.MapPath("~/App_Data/ExcelTemplateIntegerDate.xml");
            StringBuilder sheet = new StringBuilder();
            //here 1 is for empty row
            sheet.AppendFormat(GetXmlString(url), properties.Count(), list.Count() + 2 + title.Count() + 1, rowData.ToString());
            System.Diagnostics.Debug.Print(StartTime.ToString() + " - " + Utils.Helper.Util.GetCurrentDateAndTime());
            System.Diagnostics.Debug.Print((Utils.Helper.Util.GetCurrentDateAndTime() - StartTime).ToString());
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.Charset = "";
            string attachment = "attachment; filename=\"" + name + "\".xls";
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.AddHeader("content-disposition", attachment);
            HttpContext.Current.Response.Write(sheet.ToString());
            HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";
            HttpContext.Current.Response.End();

        }
        /// <summary>
        /// Export into excel with title
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="name"></param>
        /// <param name="list"></param>
        /// <param name="hiddenColumnList"></param>
        /// <param name="totalColumnList"></param>
        /// <param name="columnRenameList"></param>
        /// <param name="numericColumnList"></param>
        /// <param name="title">Dictionary type with field name and its value</param>
        public static void ExportToExcel<T>(string name, List<T> list, List<String> hiddenColumnList, List<string> totalColumnList, Dictionary<string, string> columnRenameList, List<string> numericColumnList,Dictionary<string,string> title)
        {

            int columnCount = 0;
            List<int> hiddenColumn = new List<int>();
            List<int> totalColumn = new List<int>();
            //numeric data type column
            List<int> numericColum = new List<int>();
            Dictionary<int, decimal> columnTotal = new Dictionary<int, decimal>();
            DateTime StartTime = Utils.Helper.Util.GetCurrentDateAndTime();
            StringBuilder rowData = new StringBuilder();

            //set title
            if (title != null)
            {
                foreach (var v in title)
                {
                    rowData.Append("<Row >");
                    rowData.Append(String.Format("<Cell ss:StyleID=\"s70\"><Data ss:Type=\"String\">{0}</Data></Cell>", v.Key));
                    rowData.Append(String.Format("<Cell ss:StyleID=\"s70\"><Data ss:Type=\"String\">{0}</Data></Cell>", v.Value));
                    rowData.Append("</Row >");

                }
            }
            //add empty row
            rowData.Append("<Row></Row>");
            PropertyInfo[] properties = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            rowData.Append("<Row >");
            Boolean HasDepthLevel = false;
            //contains depth level value
            int depthIndex = 0;
            foreach (PropertyInfo p in properties)
            {
                if (p.Name.ToString().Equals("DepthLevel"))
                {
                    HasDepthLevel = true;
                    depthIndex = columnCount;
                }
                if (p.PropertyType.Name != "EntityCollection`1" && p.PropertyType.Name != "EntityReference`1" && p.PropertyType.Name != p.Name)
                {

                    if (hiddenColumnList.Contains(p.Name))
                    {

                        hiddenColumn.Add(columnCount);

                    }

                    else
                    {
                        // track the column to calculate the total value
                        if (totalColumnList.Contains(p.Name))
                        {
                            totalColumn.Add(columnCount);
                            columnTotal.Add(columnCount, 0);
                        }
                        else if (numericColumnList.Contains(p.Name))
                        {
                            //track numeric colum to set the data type numeric
                            numericColum.Add(columnCount);
                        }

                        //prepare column header name
                        string headerName = string.Empty;
                        if (columnRenameList.ContainsKey(p.Name))
                        {
                            headerName = columnRenameList[p.Name];
                        }
                        else
                        {
                            headerName = p.Name;
                        }
                        rowData.AppendFormat("<Cell ss:StyleID=\"s44\"><Data ss:Type=\"String\">{0}</Data></Cell>", headerName);
                    }
                    columnCount++;

                }
                else
                    break;

            }

            rowData.Append("</Row>");
            decimal total = 0;
            foreach (T item in list)
            {
                int paddingColumn = 0;
                rowData.Append("<Row>");
                for (int x = 0; x < columnCount; x++) //each (PropertyInfo p in properties)
                {
                    if (!hiddenColumn.Contains(x)) //skip hidden column
                    {
                        object o = properties[x].GetValue(item, null);
                        string cellValue = o == null ? "" : o.ToString();
                        string value = o == null ? "" : o.ToString();

                        //total calculation
                        if (totalColumn.Contains(x))
                        {
                            if (value.Equals(""))
                            {
                                value = "0";
                            }
                            total = decimal.Parse(value) + columnTotal[x];
                            columnTotal.Remove(x);
                            columnTotal.Add(x, total);

                            //For trial balance padding
                            //if (HasDepthLevel && paddingColumn == 0)
                            //{
                            //    object obj = properties[depthIndex].GetValue(item, null);
                            //    string spacing = GetDepthLevelSpacing(obj);
                            //    //text bold style
                            //    if ((int)obj < 1)
                            //    {
                            //        rowData.Append(String.Format("<Cell ss:StyleID=\"s66\"><Data ss:Type=\"Number\">{0}{1}</Data></Cell>", spacing, cellValue));
                            //    }
                            //    else
                            //        rowData.Append(String.Format("<Cell ss:StyleID=\"s63\"><Data ss:Type=\"Number\">{0}{1}</Data></Cell>", spacing, cellValue));
                            //}
                            //else
                            {
                                rowData.AppendFormat("<Cell ss:StyleID=\"s63\"><Data ss:Type=\"Number\">{0}</Data></Cell>", cellValue);
                            }


                        }
                        else
                        {

                            //if (HasDepthLevel && paddingColumn == 0)
                            //{

                            //    object obj = properties[depthIndex].GetValue(item, null);
                            //    string spacing = GetDepthLevelSpacing(obj);
                            //    //text bold style
                            //    if ((int)obj < 1)
                            //    {
                            //        rowData.Append(String.Format("<Cell  ss:StyleID=\"s66\"><Data ss:Type=\"String\">{0}{1}</Data></Cell>", spacing, cellValue));
                            //    }
                            //    else
                            //        rowData.Append(String.Format("<Cell><Data ss:Type=\"String\">{0}{1}</Data></Cell>", spacing, cellValue));
                            //}
                            if (numericColum.Contains(x)) //sets data type numeric
                            {
                                rowData.AppendFormat("<Cell ss:StyleID=\"s63\"><Data ss:Type=\"Number\">{0}</Data></Cell>", cellValue);
                            }
                            else
                            {
                                rowData.AppendFormat("<Cell><Data ss:Type=\"String\">{0}</Data></Cell>", cellValue);
                            }
                        }

                        //increament spacing for column so that spacing for the next column will not be applied
                        paddingColumn++;

                    }
                }

                rowData.Append("</Row >");

            }

            Boolean IsfirstColumn = true;
            if (totalColumnList.Count != 0)
            {
                rowData.Append("<Row >");
                for (int x = 0; x < columnCount; x++)
                {
                    string value = "";
                    if (!hiddenColumn.Contains(x))
                    {
                        if (IsfirstColumn)
                        {
                            //Total label
                            rowData.Append("<Cell ss:StyleID=\"sTotal\"><Data ss:Type=\"String\">" + "Total" + "</Data></Cell>");
                            IsfirstColumn = false;
                        }
                        else if (totalColumn.Contains(x))
                        {
                            rowData.AppendFormat("<Cell ss:StyleID=\"s14\" ><Data ss:Type=\"Number\">{0}</Data></Cell>", columnTotal[x]);

                        }
                        else
                        {
                            rowData.AppendFormat("<Cell ss:StyleID=\"sTotal\"><Data ss:Type=\"String\">{0}</Data></Cell>", value);
                        }
                    }

                }
                rowData.Append("</Row>");
            }

            string url = HttpContext.Current.Server.MapPath("~/App_Data/ExcelTemplate.xml");
            StringBuilder sheet = new StringBuilder();
            //here 1 is for empty row
            sheet.AppendFormat(GetXmlString(url), properties.Count(), list.Count() + 2+title.Count()+1, rowData.ToString());
            System.Diagnostics.Debug.Print(StartTime.ToString() + " - " + Utils.Helper.Util.GetCurrentDateAndTime());
            System.Diagnostics.Debug.Print((Utils.Helper.Util.GetCurrentDateAndTime() - StartTime).ToString());
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.Charset = "";
            string attachment = "attachment; filename=\"" + name + "\".xls";
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.AddHeader("content-disposition", attachment);
            HttpContext.Current.Response.Write(sheet.ToString());
            HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";
            HttpContext.Current.Response.End();

        }


        public static string GetCellTag(string spacing, string value)
        {
            if( (spacing + value ).Trim() != "")
                return ("<Cell ss:StyleID=\"s63\"><Data ss:Type=\"Number\">" + spacing + value + "</Data></Cell>");
            return ("<Cell ss:StyleID=\"s63\" /Cell>");
        }

        //return row spacing for trial spacing
        //public static string GetDepthLevelSpacing(object obj)
        //{
        //    string spacing = string.Empty;
        //   // object obj = properties[depthIndex].GetValue(item, null);
        //    string spaceValue = obj == null ? "" : obj.ToString();
        //    int spaceCount = int.Parse(spaceValue);
        //    do
        //    {
        //        spaceCount--;
        //        spacing = "   " + spacing;

        //    }
        //    while (spaceCount >= 0);
        //    return spacing;
        //}

       

        static string GetXmlString(string strFile)
        {
            // Load the xml file into XmlDocument object.
            XmlDocument xmlDoc = new XmlDocument();
            try
            {
                xmlDoc.Load(strFile);
            }
            catch (XmlException e)
            {
                //Console.WriteLine(e.Message);
            }
            // Now create StringWriter object to get data from xml document.
            StringWriter sw = new StringWriter();
            XmlTextWriter xw = new XmlTextWriter(sw);
            xmlDoc.WriteTo(xw);
            return sw.ToString();
        }

        /// <summary>
        /// Export data table to excel file
        /// </summary>
        /// <param name="dt">Data table source</param>
        /// 
        public static void ExportDynamicDtToExcel(DataTable dt)
        {

            if (dt.Rows.Count > 0)
            {
                string attachment = "attachment; filename=SalarySheet.xls";
                HttpContext.Current.Response.ClearContent();
                HttpContext.Current.Response.AddHeader("content-disposition", attachment);
                HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";
                string tab = "";
                foreach (DataColumn dc in dt.Columns)
                {
                    HttpContext.Current.Response.Write(tab + dc.Caption);
                    tab = "\t";
                }
                HttpContext.Current.Response.Write("\n");
                int i;
                foreach (DataRow dr in dt.Rows)
                {
                    tab = "";
                    for (i = 0; i < dt.Columns.Count; i++)
                    {
                        HttpContext.Current.Response.Write(tab + dr[i].ToString());
                        tab = "\t";
                    }
                    HttpContext.Current.Response.Write("\n");
                }
                HttpContext.Current.Response.End();

            }
        }
        public static void ExportDynamicDtToExcel(DataTable dt,string excelName)
        {

            if (dt.Rows.Count > 0)
            {
                string attachment = "attachment; filename=\"" + excelName + "\"";
                HttpContext.Current.Response.ClearContent();
                HttpContext.Current.Response.AddHeader("content-disposition", attachment);
                HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";
                string tab = "";
                foreach (DataColumn dc in dt.Columns)
                {
                    HttpContext.Current.Response.Write(tab + dc.Caption);
                    tab = "\t";
                }
                HttpContext.Current.Response.Write("\n");
                int i;
                foreach (DataRow dr in dt.Rows)
                {
                    tab = "";
                    for (i = 0; i < dt.Columns.Count; i++)
                    {
                        HttpContext.Current.Response.Write(tab + dr[i].ToString());
                        tab = "\t";
                    }
                    HttpContext.Current.Response.Write("\n");
                }
                HttpContext.Current.Response.End();

            }
        }
        public static void ExportDtToExcel(DataTable dt)
        {

            if (dt.Rows.Count > 0)
            {
                string attachment = "attachment; filename=SalarySheet.xls";
                HttpContext.Current.Response.ClearContent();
                HttpContext.Current.Response.AddHeader("content-disposition", attachment);
                HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";
                string tab = "";
                foreach (DataColumn dc in dt.Columns)
                {
                    HttpContext.Current.Response.Write(tab + dc.ColumnName);
                    tab = "\t";
                }
                HttpContext.Current.Response.Write("\n");
                int i;
                foreach (DataRow dr in dt.Rows)
                {
                    tab = "";
                    for (i = 0; i < dt.Columns.Count; i++)
                    {
                        HttpContext.Current.Response.Write(tab + dr[i].ToString());
                        tab = "\t";
                    }
                    HttpContext.Current.Response.Write("\n");
                }
                HttpContext.Current.Response.End();

            }
        }

    }
}