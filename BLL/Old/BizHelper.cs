﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Web;
using System.Web.UI.WebControls;
using DAL;
using Utils.Calendar;

namespace BLL
{
    public class BizHelper
    {

        public static ListItem[] GetMembersAsListItem(ISequence obj)
        {
            List<KeyValue> values = obj.GetMembers();
            List<ListItem> items = new List<ListItem>();
            foreach(KeyValue value in values)
            {
                items.Add(new ListItem(value.Value, value.Key));
            }
            return items.ToArray();
        }


        public static void Load(ISequence obj, DropDownList ddl)
        {
            ddl.Items.AddRange(GetMembersAsListItem(obj));
        }

        public static DateTime GetConvertedToEngDate(CustomDate date)
        {

            if (SessionManager.CurrentCompany.IsEnglishDate == false)
                date = CustomDate.ConvertNepToEng(date);

            return new DateTime(date.Year, date.Month, date.Day);

        }

        /// <summary>
        /// Dangerous as it effects reference table value
        /// Copy an object to destination object, only matching fields will be copied
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sourceObject">An object with matching fields of the destination object</param>
        /// <param name="destObject">Destination object, must already be created</param>
        public static void CopyObject<T>(object sourceObject, ref T destObject,string[] skippingNames)
        {



            //	If either the source, or destination is null, return
            if (sourceObject == null || destObject == null)
                return;

            //	Get the type of each object
            Type sourceType = sourceObject.GetType();
            Type destType = destObject.GetType();

            //	Loop through the source properties
            foreach (PropertyInfo p in sourceType.GetProperties())
            {
                //	Get the matching property in the destination object
                PropertyInfo destObj = destType.GetProperty(p.Name);
                //	If there is none, skip
                if (destObj == null)
                    continue;

                if (skippingNames != null && skippingNames.Contains(p.Name))
                    continue;

                
                object value = p.GetValue(sourceObject, null);
                //	Set the value in the destination
                if (value != null)
                {
                    destObj.SetValue(destObject, value, null);
                }
            }
        }

    }
}
