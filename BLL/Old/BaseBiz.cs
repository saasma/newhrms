﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;
using Utils;
using System.IO;
using System.Web;
using Utils.Base;
using Utils.Calendar;
using BLL.Manager;
using System.Reflection;

namespace BLL
{

    //public  enum ResultType
    //{
    //    Success,
    //    NotChanged,
    //    NoRecord
    //}

    //http://blogs.vertigo.com/personal/keithc/Blog/Lists/Posts/Post.aspx?ID=11

    public abstract class BaseBiz : DAL.BaseBiz
    {

        public static T GetFromCache<T>(string cacheId)
        {

            object value = MyCache.GetFromCacheFromBLL( cacheId);
            if (value != null)
                return (T)value;

            return default(T);

        }

        public static DateTime GetCurrentDateAndTime()
        {


            return Utils.Helper.Util.GetCurrentDateAndTime();
        }

        public static string GetAppropriateDate(DateTime datetime)
        {
            if (IsEnglish)
            {
                //return datetime.Day + "/" + datetime.Month + "/" + datetime.Year;
                return datetime.Year + "/" + datetime.Month.ToString().PadLeft(2, '0') + "/" + datetime.Day.ToString().PadLeft(2, '0');
            }
            else
            {
                CustomDate d = new CustomDate(datetime.Day, datetime.Month, datetime.Year, true);
                return CustomDate.ConvertEngToNep(d).ToString();
            }
        }

        public static string GetAppropriateDateWithName(DateTime datetime)
        {
            if (IsEnglish)
            {
                //return datetime.Day + "/" + datetime.Month + "/" + datetime.Year;
                return datetime.Year + "/" + datetime.Month.ToString().PadLeft(2, '0') + "/" + datetime.Day.ToString().PadLeft(2, '0');
            }
            else
            {
                CustomDate d = new CustomDate(datetime.Day, datetime.Month, datetime.Year, true);
                return CustomDate.ConvertEngToNep(d).ToStringShortMonthName();
            }
        }

        public static string GetString(object value)
        {
            if (value == null)
                return "";
            return value.ToString();
        }

        #region "Change Log"

        public static bool LogEnabled
        {
            get
            {
                return CommonManager.CompanySetting.DisableChangeLog == false;
            }
        }

        /// <summary>
        /// Checks if the text contains the characters which is invalid for XML in sql server
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static bool IsInvalidForSavingInXMLForm(string text)
        {
            if( text.Contains("&") || text.Contains("'") || text.Contains("\"") || text.Contains(">") || text.Contains("<") )
                return true;

            return false;
            //output = input.Replace("&", "&amp;");
            //output = output.Replace("'", "&apos;");
            //output = output.Replace("\"", "&quot;");
            //output = output.Replace(">", "&gt;");
            //output = output.Replace("<", "&lt;");
        }

        public static EmailLog GetEmailLog(int employeeId,string description, EmailLogTypeEnum type, int sourceId, string toEmail, string subject
            ,bool isSuccess,string errorMessage)
        {
            EmailLog log = new EmailLog();

            log.LogID = Guid.NewGuid();
            log.UserID = SessionManager.CurrentLoggedInUserID;
            log.SentDateTime = GetCurrentDateAndTime();
            log.EmailType = (int)type;
            log.SourceId = sourceId;
            log.ToEmail = toEmail;
            log.Subject = subject;
            log.EmployeeId = employeeId;
            log.Description = description;
            log.IsSuccess = isSuccess;
            log.ErrorMessage = errorMessage;
            return log;
        }

        public static ChangeMonetory GetMonetoryChangeLog(int employeeId, byte type, string subType, object before, 
            object after,LogActionEnum action)
        {
            ChangeMonetory change = new ChangeMonetory();
            change.CompanyId = SessionManager.CurrentCompanyId;
            change.UserName =SessionManager.UserName;
            change.EmployeeId = employeeId;
            change.DateEng = GetCurrentDateAndTime();
            change.Type = type;
            change.SubType = subType;
            change.Action = (byte)action;

            decimal value;
            if (before != null)
            {
                if (decimal.TryParse(before.ToString(), out value))
                    change.Before = GetCurrency(value);
                else
                    change.Before = (before.ToString());
            }

            if (after != null)
            {
                if (decimal.TryParse(after.ToString(), out value))
                    change.After = GetCurrency(value);
                else
                    change.After = (after.ToString());
            }
           

            return change;
        }

        public static ChangeUserActivity GetChangeUserActivityLog(string remarks)
        {
            ChangeUserActivity log = new ChangeUserActivity();
            log.CompanyId = SessionManager.CurrentCompanyId;
            log.UserName = SessionManager.UserName;

            log.DateEng = GetCurrentDateAndTime();
            
            log.Remarks = remarks;

            return log;
        }

        public static ChangeUserActivity GetChangeUserActivityLog(string remarks,string userName)
        {
            ChangeUserActivity log = new ChangeUserActivity();
            log.CompanyId = SessionManager.CurrentCompanyId;
            log.UserName = userName;

            log.DateEng = GetCurrentDateAndTime();

            log.Remarks = remarks;

            return log;
        }

        public static ChangeSetting GetSettingChangeLog(byte type, string subType, string before,
           string after,  LogActionEnum action)
        {
            ChangeSetting change = new ChangeSetting();
            change.CompanyId = SessionManager.CurrentCompanyId;
            change.UserName = SessionManager.UserName;

            change.DateEng = GetCurrentDateAndTime();
            change.Type = type;
            change.SubType = subType;
            change.Action = (byte)action;

            change.Before = before;
            change.After = after;

         

            return change;
        }
        

        /// <summary>
        /// Fixed columns to be skipped while updating
        /// </summary>
        private static string[] skipFixedColumns =
            new string[] { "CreatedOn", "CreatedBy", "EditSequence" };

        /// <summary>
        /// Copy an object to destination object, only matching fields will be copied
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="dbEntity">An object with matching fields of the destination object</param>
        /// <param name="entity">Destination object, must already be created</param>
        /// <param name="skippingNames">Property names that don't need to be updated like ID, CreatedOn, CreatedBy</param> 
        public static void AddToChangeSetting<T>(object dbEntity, T entity, 
            MonetoryChangeLogTypeEnum logType,string title,params string[] skippingNames
            )
        {

            //	If either the source, or destination is null, return
            if (dbEntity == null || entity == null)
                return;

            //	Get the type of each object
            Type sourceType = dbEntity.GetType();
            Type destType = entity.GetType();

            //	Loop through the source properties
            foreach (PropertyInfo p in sourceType.GetProperties())
            {
                //	Get the matching property in the destination object
                PropertyInfo destObj = destType.GetProperty(p.Name);

                //	If there is none, skip
                if (destObj == null || destObj.CanWrite == false)
                    continue;

                // if type if not String & IsClass then skip
                if (destObj.PropertyType.IsClass && !destObj.PropertyType.FullName.Equals("System.String"))
                    continue;


                if (skippingNames != null && (skippingNames.Contains(p.Name) || skipFixedColumns.Contains(p.Name)))
                    continue;


                object dbValue = p.GetValue(dbEntity, null);
                object value = p.GetValue(entity, null);
                //	Set the value in the destination
                if (dbValue!= null && value != null &&  !dbValue.Equals( value))
                {
                    PayrollDataContext.ChangeSettings.InsertOnSubmit(
                        GetSettingChangeLog((byte)logType, title + " : " + p.Name, GetString(dbValue),
                        GetString(value), LogActionEnum.Update));
                }
                    // sometime the db value will be null
                else if (dbValue == null && value != null)
                {
                    PayrollDataContext.ChangeSettings.InsertOnSubmit(
                       GetSettingChangeLog((byte)logType, title + " : " + p.Name, "",
                       GetString(value), LogActionEnum.Update));
                }
            }
        }


        /// <summary>
        /// Copy an object to destination object, only matching fields will be copied
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="dbEntity">An object with matching fields of the destination object</param>
        /// <param name="entity">Destination object, must already be created</param>
        /// <param name="skippingNames">Property names that don't need to be updated like ID, CreatedOn, CreatedBy</param> 
        public static void AddToMonetoryChange<T>(object dbEntity, T entity,
            MonetoryChangeLogTypeEnum logType,  int employeeId, params string[] changeTrackFields
            )
        {

            //	If either the source, or destination is null, return
            if (dbEntity == null || entity == null)
                return;

            //	Get the type of each object
            Type sourceType = dbEntity.GetType();
            Type destType = entity.GetType();

            //	Loop through the source properties
            foreach (PropertyInfo p in sourceType.GetProperties())
            {
                //	Get the matching property in the destination object
                PropertyInfo destObj = destType.GetProperty(p.Name);

                //	If there is none, skip
                if (destObj == null || destObj.CanWrite == false)
                    continue;

                // if type if not String & IsClass then skip
                if (destObj.PropertyType.IsClass && !destObj.PropertyType.FullName.Equals("System.String"))
                    continue;


                if (!changeTrackFields.Contains(p.Name))
                    continue;


                object dbValue = p.GetValue(dbEntity, null);
                object value = p.GetValue(entity, null);
                //	Set the value in the destination
                if (dbValue != null && value != null && !dbValue.Equals(value))
                {
                    PayrollDataContext.ChangeMonetories.InsertOnSubmit(
                        GetMonetoryChangeLog(employeeId, (byte)logType, p.Name
                        ,                     
                        GetString(dbValue),
                        GetString(value), LogActionEnum.Update));
                }
            }
        }

        #endregion

        public static void SaveToCache<T>(string cacheId, T value)
        {
            MyCache.SaveToCacheFromBLL(cacheId, value);
        }

        public static void DeleteFromCache(string cacheId)
        {
            MyCache.DeleteFromGlobalCache( cacheId);
        }

        /// <summary>
        /// Converts the date input to eng date by checking if company date is of english or nepali
        /// </summary>
        /// <param name="inputDate"></param>
        /// <param name="isCompanyDateEng">Not Null if called while saving compnay as no record has been inserted or else Null</param>
        /// <returns></returns>
        public static DateTime GetEngDate(string inputDate, bool? isCompanyDateEng)
        {
            if (isCompanyDateEng == null)
                isCompanyDateEng = SessionManager.CurrentCompany.IsEnglishDate;

            CustomDate date = CustomDate.GetCustomDateFromString(inputDate, isCompanyDateEng.Value);

            if (isCompanyDateEng.Value)
            {
                return new DateTime(date.Year, date.Month, date.Day);
            }
            else // convert to eng first
            {

                date = CustomDate.ConvertNepToEng(date);
                return new DateTime(date.Year, date.Month, date.Day);
            }

        }

        public  static  bool IsEnglish
        {
            get { return SessionManager.CurrentCompany.IsEnglishDate; }
        }
       
        public  static string GetCurrency(object amount)
        {
            return  BaseHelper.GetCurrency(amount,SessionManager.DecimalPlaces);
        }
     }
}
