﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.SessionState;
using BLL.Manager;
using DAL;
using BLL;
using Utils.Security;
using Utils.Helper;

namespace BLL
{
    public class SessionManager :BaseBiz
    {

        public static bool IsPayPermissionForCustomRole
        {
            get
            {
                if (SessionManager.IsCustomRole)
                {
                    UUser user = UserManager.GetUserByUserName(HttpContext.Current.User.Identity.Name);
                    int currentPageId = UserManager.GetPageIdByPageUrl("employee.aspx");

                    bool permissionExists = false;
                    List<UPermissionSection> sections = UserManager.GetAccessibleSectionsForPage(user.RoleId.Value, currentPageId);

                    foreach (UPermissionSection section in sections)
                    {
                        if (section.PageSectionId == 2)
                        {
                            permissionExists = true;
                        }
                    }

                    if (permissionExists == false)
                    {
                        return false;
                    }
                    return true;
                }
                else
                    return true;
            }
        }

        public static HttpSessionState Session
        {
            get { return HttpContext.Current.Session; }
        }
        public static int CurrentCompanyId
        {
            get
            {
                try
                {

                    int  cId =  GetFromCache<int>("DefaultCompanyId");

                    if (cId == 0)
                    {
                        CompanyManager mgr = new CompanyManager();
                        cId =  mgr.GetDefault();

                        SaveToCache<int>("DefaultCompanyId", cId);
                    }


                    return cId;
                }
                    // rerurn -1 as it is used in Error.aspx page also
                catch
                {
                    return -1;
                }
                
                //TODO: remove all this assinging
                
                //if (Session["CurrentCompanyId"] != null)
                //    return int.Parse(Session["CurrentCompanyId"].ToString());
                //else
                //{
                //    HttpContext.Current.Response.Redirect("~/CP/ManageCompany.aspx", true);
                //    HttpContext.Current.Response.End();
                //}
                //return 0;
            }
            //set
            //{
            //    Session["CurrentCompanyId"] = value;
            //}
        }

        public  static int DecimalPlaces
        {
            get
            {
                string roundOff = GetFromCache<string>(CacheKey.DecimalPlaces.ToString());
                if (!string.IsNullOrEmpty(roundOff))
                {
                    if (roundOff == CompanyNetSalaryRoundOff.TWO_DECIMAL)
                        return 2;
                    else
                        return 0;
                }

                roundOff = SessionManager.CurrentCompany.NetSalaryRoundOff;

                SaveToCache<string>(CacheKey.DecimalPlaces.ToString(), roundOff);

                if (roundOff == CompanyNetSalaryRoundOff.TWO_DECIMAL)
                    return 2;
                return 0;
            }
        }

        public static bool IsEnglish
        {
            get
            {
                return SessionManager.CurrentCompany.IsEnglishDate;
            }
        }
        public static int CompanyDeleteId
        {
            get
            {
                if (Session["CompanyDeleteId"] != null)
                    return int.Parse(Session["CompanyDeleteId"].ToString());
                else
                    return 0;
            }
            set
            {
                Session["CompanyDeleteId"] = value;
            }
        }

        public static Company CurrentCompany
        {
            get
            {
                

                if (CurrentCompanyId != 0)
                {
                    CompanyManager mgr = new CompanyManager();
                    return mgr.GetById(CurrentCompanyId);
                }
                else
                {
                    if (HttpContext.Current != null)
                    {
                        if (HttpContext.Current.Request.Url.ToString().ToLower()
                            .Contains("addeditcompany.aspx") == false &&
                            HttpContext.Current.Request.Url.ToString().ToLower().Contains("changepassword.aspx") == false)
                        {
                            HttpContext.Current.Response.Redirect("~/CP/addeditcompany.aspx", true);
                            HttpContext.Current.Response.End();
                        }
                    }
                    //HttpContext.Current.Response.Redirect("~/CP/AddEditCompany.aspx", true);
                    //HttpContext.Current.Response.End();
                }
                return null;
            }        
        }

        public static int CurrentLoggedInEmployeeId
        {
            get
            {

                if (HttpContext.Current.User.Identity.IsAuthenticated == false)
                {
                    HttpContext.Current.Response.Redirect("~/Default.aspx", true);
                }

                if( HttpContext.Current.User.IsInRole( Role.Employee.ToString()))
                {

                    if (HttpContext.Current != null && HttpContext.Current.Items != null)
                    {
                        string key = "SaveEINForEmployee";
                        if (HttpContext.Current.Items[key] != null)
                            return int.Parse(HttpContext.Current.Items[key].ToString());

                        int ein = UserManager.GetUserEmployeeId(HttpContext.Current.User.Identity.Name);

                        HttpContext.Current.Items[key] = ein;

                        return ein;
                    }
                    else
                        return UserManager.GetUserEmployeeId(HttpContext.Current.User.Identity.Name);

                }
                return 0;
            }
        }
        public static Guid CurrentLoggedInUserID
        {
            get
            {
                return UserManager.GetUserId(HttpContext.Current.User.Identity.Name);
                //if (HttpContext.Current.User.Identity.Name.Contains("\\"))
                //    return UserManager.GetUserByLDAP(GetPlainUserName(HttpContext.Current.User.Identity.Name)).EmployeeId.Value;
                //else
                {
                    //UUser user = UserManager.GetUserByUserName(HttpContext.Current.User.Identity.Name);
                    // can be when user name is changed then redirect
                   // return user.UserID;
                }


            }
        }
        public static FinancialDate CurrentCompanyFinancialDate
        {
            get
            {
                if (CurrentCompanyId != 0)
                {
                    CompanyManager mgr = new CompanyManager();
                    return mgr.GetCurrentFinancialDate(CurrentCompanyId);
                }
                return null;
            }
        }

       

        public static PEmployeeDeduction EmployeeDeduction
        {
            get
            {   
                if (Session["EmployeeDeduction"] != null)
                    return Session["EmployeeDeduction"] as PEmployeeDeduction;
                return null;
            }
            set
            {
                Session["EmployeeDeduction"] = value;
            }
        }

        public static int EmployeeDisplayRecodsPerPage
        {
            get
            {
                if (Session["EmployeeDisplayRecodsPerPage"] == null)
                    return 0;
                return int.Parse(Session["EmployeeDisplayRecodsPerPage"].ToString());
            }
            set
            {
                Session["EmployeeDisplayRecodsPerPage"] = value;
            }
        }

        public static bool EmployeeSaved
        {
            get
            {
                if (Session["EmployeeSaved"] == null)
                    return false;
                return bool.Parse(Session["EmployeeSaved"].ToString());
            }
            set
            {
                Session["EmployeeSaved"] = value;
            }
        }

        public static bool IsCustomRole
        {
            get
            {

                if( HttpContext.Current.User.IsInRole(Role.Administrator.ToString()))
                {
                    return false;
                }
                else if( HttpContext.Current.User.IsInRole(Role.Employee.ToString()))
                    return false;

                else
                    return true;

                
            }
        }

        public static URole CustomRole
        {
            get
            {

                if (IsCustomRole == false)
                    return null;

                if (User == null)
                    return null;

                return User.URole;
            }
        }

        public static bool IsReadOnlyUser
        {
            get
            {
                if (SessionManager.IsCustomRole && SessionManager.CustomRole.IsReadOnly != null && SessionManager.CustomRole.IsReadOnly.Value)
                    return true;
                return false;
            }
        }
        public static string CustomRoleDeparmentIDList
        {
            get
            {
                if (IsCustomRole && CustomRole.HasDepartmentAssigned != null && CustomRole.HasDepartmentAssigned.Value)
                {
                    List<string> list = new List<string>();
                    foreach (URoleDepartment dep in CustomRole.URoleDepartments)
                    {
                        list.Add(dep.DepartmentId.ToString());
                    }
                    return string.Join(",", list.ToArray());
                }
                else
                    return null;
            }
        }
        public static string CustomRoleBranchIDList
        {
            get
            {
                if (IsCustomRole && CustomRole.HasDepartmentAssigned != null && CustomRole.HasDepartmentAssigned.Value)
                {
                    List<string> list = new List<string>();
                    foreach (URoleBranch dep in CustomRole.URoleBranches)
                    {
                        list.Add(dep.BranchId.ToString());
                    }
                    return string.Join(",", list.ToArray());
                }
                else
                    return null;
            }
        }
        public static List<int> CustomRoleDeparmentList
        {
            get
            {
                if (IsCustomRole && CustomRole != null && CustomRole.HasDepartmentAssigned != null && CustomRole.HasDepartmentAssigned.Value)
                {
                    List<int> list = new List<int>();
                    foreach (URoleDepartment dep in CustomRole.URoleDepartments)
                    {
                        list.Add(dep.DepartmentId);
                    }
                    return list;
                }
                else
                    return new List<int> { };
            }
        }
        public static List<int> CustomRoleBranchList
        {
            get
            {
                if (IsCustomRole && CustomRole != null && CustomRole.HasDepartmentAssigned != null && CustomRole.HasDepartmentAssigned.Value)
                {
                    List<int> list = new List<int>();
                    foreach (URoleBranch dep in CustomRole.URoleBranches)
                    {
                        list.Add(dep.BranchId);
                    }
                    return list;
                }
                else
                    return new List<int> { };
            }
        }
        public static string GetPlainUserName(string ldapUser)
        {
            return ldapUser.Substring(ldapUser.LastIndexOf("\\") + 1);
        }

        public static UUser User
        {
            get
            {
               


                // below code will prevent for employee login
                //if (HttpContext.Current.User.Identity.IsAuthenticated == false)
                //{
                //    HttpContext.Current.Response.Redirect("~/Default.aspx", true);
                //}

                if(HttpContext.Current.Items["CurrentUser"] != null)
                    return HttpContext.Current.Items["CurrentUser"] as UUser;


               

                UUser user =  UserManager.GetUserByUserName(HttpContext.Current.User.Identity.Name);

                HttpContext.Current.Items["CurrentUser"] = user;

                return user;

            }
        }

        public static bool IsLDAPLogin
        {
            get
            {
                return SecurityHelper.IsLDAPLogin();
                //return HttpContext.Current.User.Identity.Name.Contains("\\");
            }

        }

        public static string UserName
        {
            get
            {
                if (HttpContext.Current.User.Identity.IsAuthenticated == false)
                {
                    HttpContext.Current.Response.Redirect("~/Default.aspx", true);
                }
                //if (HttpContext.Current.User.Identity.Name.Contains("\\"))
                //    return UserManager.GetUserByLDAP(GetPlainUserName(HttpContext.Current.User.Identity.Name)).UserName;
                //else
                    return HttpContext.Current.User.Identity.Name;
            }
        }
    }
}
