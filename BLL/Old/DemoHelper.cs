﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BLL.Manager;
using Utils.Security;
using Utils;

namespace BLL
{
    public class DemoHelper
    {
        public const bool isDemoVersion = false;


        public static bool IsValidInstallation()
        {
            if (!isDemoVersion)
                return true;

            string value = CommonManager.GetCombinedValue();

            if (string.IsNullOrEmpty(value))
                return false;

            try
            {
                string installedDate = AsymmCrypto.Decrypt(value);
            }
            catch(Exception exp)
            {
                Log.log("Error descrypting", exp);
                return false;
            }
            return true;
        }

        public static int GetRemainingDays()
        {
            //if (!isDemoVersion)
                return 0;//not reqd

            //string value = CommonManager.GetCombinedValue();


            //DateTime
            //    installedDate = DateTime.Parse(AsymmCrypto.Decrypt(value));


            //DateTime currentDate = DateTime.Parse(DateTime.Now.ToShortDateString());

            //int diff = (installedDate.AddDays(30) - currentDate).Days;

            //if (diff <= 0 || diff > 30)
            //    return -1;

            ////if (diff <= 30)
            //return diff;

            //return -1;//already expired
        }
    }
}
