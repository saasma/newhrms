﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Utils.Calendar;
using DAL;

namespace BLL
{
    public class DateManager : BaseBiz
    {
        //public static string AddYearToNepaliDate(CustomDate date,int years)
        //{
        //    if (date.IsEnglishDate.Value)
        //        return "";

        //    date = new CustomDate(date.Day, date.Month, date.Year + years,false);

        //    DateTime engDate = date.EnglishDate;
        //    // subtract one day for upto days
        //    engDate = engDate.AddDays(-1);

        //    CustomDate engCustomDate = new CustomDate(engDate.Day,engDate.Month,engDate.Year,true);

        //    return engCustomDate.ToString();
        //}

        public static string AddYearToNepaliDate(string date, int years)
        {


            CustomDate date1 = CustomDate.GetCustomDateFromString(date,false);
            date1.Year += years;
            date1 = new CustomDate(date1.Day, date1.Month, date1.Year, false);

            DateTime engDate = date1.EnglishDate;
            // subtract one day for upto days
            engDate = engDate.AddDays(-1);

            CustomDate engCustomDate = new CustomDate(engDate.Day, engDate.Month, engDate.Year, true);


            return CustomDate.ConvertEngToNep(engCustomDate).ToString();
        }

        public static DateTime GetStartDate(DateTime date)
        {
            return new DateTime(date.Year, date.Month, date.Day, 0, 0, 0);
        }

        public static DateTime GetEndDate(DateTime date)
        {
            return new DateTime(date.Year, date.Month, date.Day, 23, 59, 59);
        }


        /// <summary>
        /// Get the month list for the Company as english or nepali
        /// </summary>
        /// <returns></returns>
        public static List<KeyValue> GetCurrentMonthListForCompany(bool isEnglish)
        {

            //bool isEnglish;
            //if (isNew)
            //    isEnglish = true;
            //else
            //    isEnglish =  SessionManager.CurrentCompany.IsEnglishDate;

            string[] months = null;

            if (isEnglish)
                months = DateHelper.EngFullMonths;
            else
                months = DateHelper.NepMonths;

            List<KeyValue> list = new List<KeyValue>();

            for (int i = 1; i < months.Length; i++)
            {
                KeyValue month = new KeyValue(i.ToString(), months[i]);
                list.Add(month);
            }
            return list;
        }


        public static string GetOtherYearForPayrollPeriod(PayrollPeriod period, bool isEnglish)
        {
            CustomDate start, end;
            if (isEnglish)
            {
                start = CustomDate.ConvertEngToNep(CustomDate.GetCustomDateFromString(period.StartDate, true));
                end = CustomDate.ConvertEngToNep(CustomDate.GetCustomDateFromString(period.EndDate, true));
            }
            else
            {
                start = CustomDate.ConvertNepToEng(CustomDate.GetCustomDateFromString(period.StartDate, false));
                end = CustomDate.ConvertNepToEng(CustomDate.GetCustomDateFromString(period.EndDate, false));
            }

            if (start.Year == end.Year)
                return start.Year.ToString();
            else
                return start.Year + "/" + end.Year;
        }

        /// <summary>
        /// Get the month list for the Company as english or nepali
        /// </summary>
        /// <returns></returns>
        public static List<KeyValue> GetCurrentMonthList()
        {
            bool isEnglish = SessionManager.CurrentCompany.IsEnglishDate;

            string[] months = null;

            if (isEnglish)
                months = DateHelper.EngFullMonths;
            else
                months = DateHelper.NepMonths;

            List<KeyValue> list = new List<KeyValue>();

            for (int i = 1; i < months.Length; i++)
            {
                KeyValue month = new KeyValue(i.ToString(), months[i]);
                list.Add(month);
            }
            return list;
        }

        public static List<KeyValue> GetYearListForPayrollPeriod()
        {
            bool isEnglish = SessionManager.CurrentCompany.IsEnglishDate;

            int start;

            if (isEnglish)
                start = 2008;
            else
                start = 2064;
            List<KeyValue> list = new List<KeyValue>();

            for (int i = 1; i <= 12; i++)
            {
                KeyValue year = new KeyValue(start.ToString(), start.ToString());

                list.Add(year);
                start += 1;
            }
            return list;

        }

        public static CustomDate GetTodayDate()
        {
            bool isEnglish = SessionManager.CurrentCompany.IsEnglishDate;
            DateTime date = GetCurrentDateAndTime();
            CustomDate todayDate = new CustomDate(date.Day, date.Month, date.Year, true);
            if (isEnglish)
                return todayDate;
            else
            {
                return CustomDate.ConvertEngToNep(todayDate);
            }
        }
        public static CustomDate GetTodayNepaliDate()
        {
            DateTime date = GetCurrentDateAndTime();
            CustomDate todayDate = new CustomDate(date.Day, date.Month, date.Year, true);

            //CustomDate todayDate = new CustomDate(DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year, true);

            return CustomDate.ConvertEngToNep(todayDate);

        }



        //public static string GetNamedDate(object date)
        //{
        //    bool isEnglish = SessionManager.CurrentCompany.IsEnglishDate;
        //    CustomDate cd = CustomDate.GetCustomDateFromString(date.ToString(), isEnglish);
        //    return cd.ToStringMonthEng();
        //}

        public static string GetNamedMonthForDate(object date)
        {
            bool isEnglish = SessionManager.CurrentCompany.IsEnglishDate;
            CustomDate cd = CustomDate.GetCustomDateFromString(date.ToString(), isEnglish);
            return DateHelper.GetMonthName(cd.Month, isEnglish);
        }

       

        public static bool IsSecondDateGreaterThan(CustomDate date1, CustomDate date2)
        {
            
            //check year
            if (date2.Year > date1.Year)
            {
                return true;
            }
            else if (date2.Year < date1.Year)
                return false;

            if (date2.Month > date1.Month)
                return true;
            else if (date2.Month < date1.Month)
                return false;

            //if greater than
            if (date2.Day > date1.Day)
            {

                //alert("Financial ending date must be greater than the starting date.");

                return true;
            }
            else
                return false;
        }
    }
}
