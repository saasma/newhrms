﻿
namespace BLL
{
    /// <summary>
    /// Used by the controls/pages which need Disabling controls after first salary generation
    /// </summary>
    public interface IDisableAfterCalculation
    {
        void DisableControlsAfterSalary();
    }
}
