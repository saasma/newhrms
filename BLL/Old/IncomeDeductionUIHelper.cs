﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using DAL;

namespace BLL
{
    public static class IncomeDeductionUIHelper
    {
        /// Adds the selected incomes for this income
        /// </summary>
        //public static void AddSelectedIncomes(int currentIncomeId,PIncome entity, CheckBoxList chkListIncomes)
        //{
        //    // TODO: for non variable of festival amount, while saving for first time normal selection
        //    // not working so retrieved value using name, later check why it is not working, not good
        //    // way to retrive like this

        //    string id = chkListIncomes.ClientID.Replace("_", "$") + "${0}";
        //    int count = 0;
        //    foreach (ListItem item in chkListIncomes.Items)
        //    {
        //        string itemName = String.Format(id, count++);

        //        if (HttpContext.Current.Request.Form[itemName] != null)
        //        {
        //            PIncomeIncome income = new PIncomeIncome();
        //            if (entity.IncomeId != 0)
        //                income.IncomeId = currentIncomeId;
        //            income.OtherIncomeId = Int32.Parse(item.Value);

        //            entity.PIncomeIncomes.Add(income);

        //            item.Selected = true;
        //        }
        //    }
        //}


         

        /// <summary>
        /// Set the selected incomes
        /// </summary>
        public static void SetSelectedIncomes(int currentIncomeId,PIncome entity, CheckBoxList chkListIncomes)
        {
            foreach (ListItem item in chkListIncomes.Items)
            {
                if (item.Selected)
                {
                    PIncomeIncome income = new PIncomeIncome();
                    if (entity.IncomeId != 0)
                        income.IncomeId = currentIncomeId;

                    income.OtherIncomeId = Int32.Parse(item.Value);

                    entity.PIncomeIncomes.Add(income);

                    item.Selected = true;
                }
            }
            //foreach (PIncomeIncome income in entity.PIncomeIncomes)
            //{
            //    ListItem item = chkListIncomes.Items.FindByValue(income.OtherIncomeId.ToString());
            //    if (item != null)
            //        item.Selected = true;
            //}
        }
        public static void AddSelectedIncomes(LLeaveType entity, CheckBoxList chkListIncomes)
        {
            foreach (ListItem item in chkListIncomes.Items)
            {
                if (item.Selected)
                {
                    LLeaveEncashmentIncome income = new LLeaveEncashmentIncome();
               

                    income.IncomeId = Int32.Parse(item.Value);

                    entity.LLeaveEncashmentIncomes.Add(income);

                    item.Selected = true;
                }
            }
            //foreach (PIncomeIncome income in entity.PIncomeIncomes)
            //{
            //    ListItem item = chkListIncomes.Items.FindByValue(income.OtherIncomeId.ToString());
            //    if (item != null)
            //        item.Selected = true;
            //}
        }
        /// <summary>
        /// Set the selected incomes
        /// </summary>
        public static void SetSelectedIncomes(PIncome entity, CheckBoxList chkListIncomes)
        {
            foreach (PIncomeIncome income in entity.PIncomeIncomes)
            {
                ListItem item = chkListIncomes.Items.FindByValue(income.OtherIncomeId.ToString());
                if (item != null)
                    item.Selected = true;
            }
        }
        public static void SetSelectedIncomes(LLeaveType entity, CheckBoxList chkListIncomes)
        {
            foreach (LLeaveEncashmentIncome income in entity.LLeaveEncashmentIncomes)
            {
                ListItem item = chkListIncomes.Items.FindByValue(income.IncomeId.ToString());
                if (item != null)
                    item.Selected = true;
            }
        }
        public static void SetSelectedMonths(PIncome entity, CheckBoxList chkListIncomes)
        {
            foreach (PIncomeMonth income in entity.PIncomeMonths)
            {
                ListItem item = chkListIncomes.Items.FindByValue(income.Month.ToString());
                if (item != null)
                    item.Selected = true;
            }
        }
    }
}
