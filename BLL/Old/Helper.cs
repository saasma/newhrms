﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BLL.Manager;
using Utils.Security;
using Utils;
using Ext.Net;

namespace BLL
{
    public class Helper
    {

     

        public static void ShowExtWarningMessage(string message)
        {
            X.MessageBox.Show(
                                      new MessageBoxConfig
                                      {
                                          Message = message,
                                          Buttons = MessageBox.Button.OK,
                                          Title = "Warning",
                                          Icon = MessageBox.Icon.WARNING,
                                          MinWidth = 300
                                      });
        }
    }
}
