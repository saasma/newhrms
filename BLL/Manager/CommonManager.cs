﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text.RegularExpressions;
using System.Transactions;
using DAL;
using Utils.Calendar;
using System;
using System.Web.UI.WebControls;
using Utils;
using System.Web;
using Utils.Helper;
using Microsoft.SqlServer.Management.Smo;
using Microsoft.SqlServer.Management.Common;
using System.IO;
using System.IO.Compression;
using BLL.BO;
using System.Text;




namespace BLL.Manager
{
    public class CommonManager : BaseBiz
    {

        public static bool ShowAppraisalDashboard()
        {
            Setting setting = Setting;
            if (setting.ShowAppraisalDashboard != null && setting.ShowAppraisalDashboard.Value)
                return true;

            if (PayrollDataContext.AppraisalForms.Any())
                return true;

            if (PayrollDataContext.AppraisalPeriods.Any())
                return true;

            return false;
        }

        /// <summary>
        /// Generate api key using sql server function
        /// </summary>
        /// <returns></returns>
        public static string GenerateAPIKey()
        {
            return PayrollDataContext.GenerateAPIKey();
        }

        /// <summary>
        /// Verifies if the key is valid or not
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static bool IsAPIKeyValid(string key)
        {
            key = key.ToLower().Trim();

            if (string.IsNullOrEmpty(key))
                return false;

            string systemKey = GenerateAPIKey().ToLower().Trim();


            return key.Equals(systemKey);

        }

        #region "Fixed Values"

        public static QueryBuilderList GetQueryBuilder(Guid id)
        {
            return PayrollDataContext.QueryBuilderLists
                .FirstOrDefault(x => x.QueryID == id);
        }

        public static List<QueryBuilderList> GetAllQueries()
        {
            return
                PayrollDataContext.QueryBuilderLists.OrderBy(x => x.Name).ToList();
        }
        public static PayrollPeriod GetLastPeriodForYear(int yearId)
        {
            PayrollPeriod period =

                PayrollDataContext.PayrollPeriods.Where(x => x.FinancialDateId == yearId)
                .OrderByDescending(x => x.PayrollPeriodId).FirstOrDefault();

            return period;
        }

        public static PayrollPeriod GetLeaveStartPeriodForTheMonth(int year, int month, int periodId)
        {
            PayrollPeriod period =
                (
                  from p in PayrollDataContext.PayrollPeriods

                  where p.Year == year - 1 && p.Month == month + 1
                  select p
                ).FirstOrDefault();

            if (period == null)
            {
                period = PayrollDataContext.PayrollPeriods
                    .Where(x => x.PayrollPeriodId <= periodId)
                    .OrderBy(x => x.PayrollPeriodId).FirstOrDefault();
            }

            return period;
        }

        public static void SetPeriodForMiddleSharwanJulyYearlyLeavePeriod(int yearId,
            ref int? startid, ref int? endId,
            ref DateTime? startDateLeaveTaken, ref DateTime? endDateLeaveTaken, ref int? currentLeavePeriodIdForAccural,
            ref int? lastYearOrYearOpeningPayrollPeriodId
            , ref DateTime? yearStart, ref DateTime? yearEnd, int empId)

        {
            startid = null;
            endId = null;

            bool hasMultipleYear = PayrollDataContext.FinancialDates.Count() >= 2;

            currentLeavePeriodIdForAccural = null;

            // logic when leave start month is Sharwan or July or Middle month
            FinancialDate year = PayrollDataContext.FinancialDates.FirstOrDefault(x => x.FinancialDateId == yearId);

            PayrollPeriod startPeriod = PayrollDataContext.PayrollPeriods.Where(x => x.FinancialDateId == yearId)
                .OrderBy(x => x.PayrollPeriodId).FirstOrDefault();

            PayrollPeriod endPeriod = PayrollDataContext.PayrollPeriods.Where(x => x.FinancialDateId == yearId)
                .OrderByDescending(x => x.PayrollPeriodId).FirstOrDefault();

            // Case : if current year
            if (hasMultipleYear == false || yearId == PayrollDataContext.FinancialDates.OrderByDescending(
                x => x.FinancialDateId).FirstOrDefault().FinancialDateId)
            {
                PayrollPeriod currentPeriod = CommonManager.GetLastPayrollPeriod();

                bool isAttendnaceSaved = false;

                currentLeavePeriodIdForAccural = currentPeriod.PayrollPeriodId;

                // for now if report being generated for single employee and atte saved then process from attendence table to show lapse/encash
                if (empId != null && empId != 0 && empId != -1)
                {
                    isAttendnaceSaved = PayrollDataContext.Attendences.Any(x => x.EmployeeId == empId && x.PayrollPeriodId == currentPeriod.PayrollPeriodId);

                    //// if has monthy leave type then allow for accural for all month of current year
                    //if (PayrollDataContext.LLeaveTypes.Any(x => x.FreqOfAccrual == LeaveAccrue.MONTHLY) && isAttendnaceSaved == false)
                    //{
                    //    currentLeavePeriodIdForAccural = currentPeriod.PayrollPeriodId;
                    //}

                }

                // if all period attendance is already saved for the year
                if (CalculationManager.IsAllEmployeeSavedFinally(currentPeriod.PayrollPeriodId, false)
                    && isAttendnaceSaved)
                {
                    startid = startPeriod.PayrollPeriodId;
                    endId = endPeriod.PayrollPeriodId;

                }
                else
                {
                    currentLeavePeriodIdForAccural = currentPeriod.PayrollPeriodId;

                    if (currentPeriod.PayrollPeriodId == startPeriod.PayrollPeriodId)
                    {
                        // if this is the first software period then don't us this report

                        //only from date not from LeaveAdjustment
                        startDateLeaveTaken = startPeriod.StartDateEng.Value;
                        endDateLeaveTaken = year.EndingDateEng.Value;


                    }
                    else
                    {
                        // partial from LeaveAdjustment and rem from start date
                        startid = startPeriod.PayrollPeriodId;

                        PayrollPeriod lastPeriod =
                               (
                                   from p in PayrollDataContext.PayrollPeriods
                                   where p.PayrollPeriodId < currentPeriod.PayrollPeriodId
                                   && p.FinancialDateId == endPeriod.FinancialDateId
                                   orderby p.PayrollPeriodId descending
                                   select p
                               ).FirstOrDefault();

                        if (lastPeriod != null)
                        {
                            endId = lastPeriod.PayrollPeriodId;

                        }
                        else
                        {
                            startid = null;
                            endId = null;
                        }


                        //only from date not from LeaveAdjustment
                        startDateLeaveTaken = currentPeriod.StartDateEng.Value;
                        endDateLeaveTaken = year.EndingDateEng.Value;
                    }

                }

            }
            // Case : for first year which has no current period so retrive using saved LeaveAdjustment
            else
            {
                startid = startPeriod.PayrollPeriodId;
                endId = endPeriod.PayrollPeriodId;
            }


            if (startid != null)
            {
                int id = startid.Value;
                PayrollPeriod lastPeriod =
                 (
                     from p in PayrollDataContext.PayrollPeriods
                     where p.PayrollPeriodId < id
                     orderby p.PayrollPeriodId descending
                     select p
                 ).FirstOrDefault();
                if (lastPeriod != null)
                    lastYearOrYearOpeningPayrollPeriodId = lastPeriod.PayrollPeriodId;
                else
                    lastYearOrYearOpeningPayrollPeriodId = startid;
            }
            else
            {
                lastYearOrYearOpeningPayrollPeriodId =
                    (
                        from p in PayrollDataContext.PayrollPeriods
                        where p.FinancialDateId != yearId
                        orderby p.PayrollPeriodId descending
                        select p.PayrollPeriodId
                    ).FirstOrDefault();
            }

            yearStart = year.StartingDateEng;
            yearEnd = year.EndingDateEng;
        }

        /// <summary>
        /// For Oxfam like compnay where leave start month is April
        /// </summary>
        /// <param name="year">for 2013/2014 period, 2013 will be passed</param>
        /// <param name="startid"></param>
        /// <param name="endId"></param>
        /// <param name="startDateLeaveTaken"></param>
        /// <param name="endDateLeaveTaken"></param>
        /// <param name="currentLeavePeriodIdForAccural"></param>
        /// <param name="lastYearOrYearOpeningPayrollPeriodId"></param>
        /// <param name="yearStart"></param>
        /// <param name="yearEnd"></param>
        /// <param name="empId"></param>
        public static void SetPeriodForAprilToMarchYearlyLeavePeriod(int year,
           ref int? startid, ref int? endId,
           ref DateTime? startDateLeaveTaken, ref DateTime? endDateLeaveTaken, ref int? currentLeavePeriodIdForAccural,
           ref int? lastYearOrYearOpeningPayrollPeriodId
           , ref DateTime? yearStart, ref DateTime? yearEnd, int empId)

        {
            startid = null;
            endId = null;

            DateTime dateYearStart = new DateTime(year, (int)EnglishMonthEnum.April, 1);
            DateTime dateYearEnd = new DateTime(year + 1, ((int)EnglishMonthEnum.April) - 1, DateTime.DaysInMonth(year + 1, ((int)EnglishMonthEnum.April) - 1));

            //bool hasMultipleYear = PayrollDataContext.FinancialDates.Count() >= 2;

            currentLeavePeriodIdForAccural = null;

            // logic when leave start month is Sharwan or July or Middle month
            //FinancialDate year = PayrollDataContext.FinancialDates.FirstOrDefault(x => x.FinancialDateId == yearId);

            PayrollPeriod startPeriod = PayrollDataContext.PayrollPeriods.Where(x => //x.FinancialDateId == yearId)
                   (x.Year == year && x.Month >= (int)EnglishMonthEnum.April) || (x.Year == year + 1 && x.Month < (int)EnglishMonthEnum.April))
                .OrderBy(x => x.PayrollPeriodId).FirstOrDefault();

            PayrollPeriod endPeriod = PayrollDataContext.PayrollPeriods.Where(x => //x.FinancialDateId == yearId)
                    (x.Year == year && x.Month >= (int)EnglishMonthEnum.April) || (x.Year == year + 1 && x.Month < (int)EnglishMonthEnum.April))
                .OrderByDescending(x => x.PayrollPeriodId).FirstOrDefault();


            PayrollPeriod currentPeriod = CommonManager.GetLastPayrollPeriod();

            // Case : if current year
            if ((currentPeriod.Year == year && currentPeriod.Month >= (int)EnglishMonthEnum.April) || (currentPeriod.Year == year + 1 && currentPeriod.Month < (int)EnglishMonthEnum.April))
            {
                // PayrollPeriod currentPeriod = CommonManager.GetLastPayrollPeriod();

                bool isAttendnaceSaved = false;

                currentLeavePeriodIdForAccural = currentPeriod.PayrollPeriodId;

                // for now if report being generated for single employee and atte saved then process from attendence table to show lapse/encash
                if (empId != null && empId != 0 && empId != -1)
                {
                    isAttendnaceSaved = PayrollDataContext.Attendences.Any(x => x.EmployeeId == empId && x.PayrollPeriodId == currentPeriod.PayrollPeriodId);

                    //// if has monthy leave type then allow for accural for all month of current year
                    //if (PayrollDataContext.LLeaveTypes.Any(x => x.FreqOfAccrual == LeaveAccrue.MONTHLY) && isAttendnaceSaved == false)
                    //{
                    //    currentLeavePeriodIdForAccural = currentPeriod.PayrollPeriodId;
                    //}

                }

                // if all period attendance is already saved for the year
                if (CalculationManager.IsAllEmployeeSavedFinally(currentPeriod.PayrollPeriodId, false)
                    && isAttendnaceSaved)
                {
                    startid = startPeriod.PayrollPeriodId;
                    endId = endPeriod.PayrollPeriodId;

                }
                else
                {
                    currentLeavePeriodIdForAccural = currentPeriod.PayrollPeriodId;

                    if (currentPeriod.PayrollPeriodId == startPeriod.PayrollPeriodId)
                    {
                        // if this is the first software period then don't us this report

                        //only from date not from LeaveAdjustment
                        startDateLeaveTaken = startPeriod.StartDateEng.Value;
                        endDateLeaveTaken = dateYearEnd;


                    }
                    else
                    {
                        // partial from LeaveAdjustment and rem from start date
                        startid = startPeriod.PayrollPeriodId;

                        PayrollPeriod lastPeriod =
                               (
                                   from p in PayrollDataContext.PayrollPeriods
                                   where p.PayrollPeriodId < currentPeriod.PayrollPeriodId
                                   && p.FinancialDateId == endPeriod.FinancialDateId
                                   orderby p.PayrollPeriodId descending
                                   select p
                               ).FirstOrDefault();

                        if (lastPeriod != null)
                        {
                            endId = lastPeriod.PayrollPeriodId;

                        }
                        else
                        {
                            startid = null;
                            endId = null;
                        }


                        //only from date not from LeaveAdjustment
                        startDateLeaveTaken = currentPeriod.StartDateEng.Value;
                        endDateLeaveTaken = dateYearEnd;
                    }

                }

            }
            // Case : for first year which has no current period so retrive using saved LeaveAdjustment
            else
            {
                startid = startPeriod.PayrollPeriodId;
                endId = endPeriod.PayrollPeriodId;
            }


            if (startid != null)
            {
                int id = startid.Value;
                PayrollPeriod lastPeriod =
                 (
                     from p in PayrollDataContext.PayrollPeriods
                     where p.PayrollPeriodId < id
                     orderby p.PayrollPeriodId descending
                     select p
                 ).FirstOrDefault();
                if (lastPeriod != null)
                    lastYearOrYearOpeningPayrollPeriodId = lastPeriod.PayrollPeriodId;
                else
                    lastYearOrYearOpeningPayrollPeriodId = startid;
            }
            else
            {
                lastYearOrYearOpeningPayrollPeriodId =
                    (
                        from p in PayrollDataContext.PayrollPeriods
                        where //p.FinancialDateId != yearId
                               (p.Year == year && p.Month < (int)EnglishMonthEnum.April) || (p.Year < year)
                        orderby p.PayrollPeriodId descending
                        select p.PayrollPeriodId
                    ).FirstOrDefault();
            }

            yearStart = dateYearStart;
            yearEnd = dateYearEnd;
        }
        public static List<TextValue> GetUniqueDesignationList()
        {
            return
                (
                from d in PayrollDataContext.EDesignations
                select new TextValue
                {
                    Text = d.Name,
                    Value = d.Name
                }
                ).Distinct().ToList();
        }

        public static void SetPeriodForBaisakhJanuaaryYearlyLeavePeriod(int year,
            ref int? startid, ref int? endId,
            ref DateTime? startDateLeaveTaken, ref DateTime? endDateLeaveTaken, ref int? currentLeavePeriodIdForAccural,
            ref int? lastYearOrYearOpeningPayrollPeriodId
            , ref DateTime? yearStart, ref DateTime? yearEnd, int empId)
        {
            startid = null;
            endId = null;
            startDateLeaveTaken = null;
            endDateLeaveTaken = null;
            currentLeavePeriodIdForAccural = null;

            List<int> yearList =
                 (
                     from p in PayrollDataContext.PayrollPeriods
                     orderby p.Year
                     select p.Year.Value
                 ).Distinct().ToList();

            CustomDate yearStartDate = new CustomDate(1, 1, year, IsEnglish);
            CustomDate yearEndDate = new CustomDate(
                    DateHelper.GetTotalDaysInTheMonth(year, 12, IsEnglish), 12, year, IsEnglish);
            yearStart = yearStartDate.EnglishDate;
            yearEnd = yearEndDate.EnglishDate;

            PayrollPeriod startPeriod = PayrollDataContext.PayrollPeriods.Where(x => x.Year == year)
                   .OrderBy(x => x.PayrollPeriodId).FirstOrDefault();
            startid = startPeriod.PayrollPeriodId;

            PayrollPeriod endPeriod = PayrollDataContext.PayrollPeriods.Where(x => x.Year == year)
               .OrderByDescending(x => x.PayrollPeriodId).FirstOrDefault();
            endId = endPeriod.PayrollPeriodId;

            // is last year
            if (yearList[yearList.Count - 1] == year)
            {
                PayrollPeriod currentPeriod = CommonManager.GetLastPayrollPeriod();

                bool isAttendnaceSaved = false;

                // always set current month accural period, as we have removed accural in procedure
                currentLeavePeriodIdForAccural = currentPeriod.PayrollPeriodId;

                // for now if report being generated for single employee and atte saved then process from attendence table to show lapse/encash
                if (empId != null && empId != 0 && empId != -1)
                {
                    isAttendnaceSaved = PayrollDataContext.Attendences.Any(x => x.EmployeeId == empId && x.PayrollPeriodId == currentPeriod.PayrollPeriodId);

                    //// if has monthy leave type then allow for accural for all month of current year
                    //if (PayrollDataContext.LLeaveTypes.Any(x => x.FreqOfAccrual == LeaveAccrue.MONTHLY) && isAttendnaceSaved == false)
                    //{
                    //    currentLeavePeriodIdForAccural = currentPeriod.PayrollPeriodId;
                    //}

                }


                // if month is the start of the leave year then process to set leave accural month
                if (startid == endId)
                {
                    startid = null;
                    endId = null;

                    // if first period of the year then get Accural month and leave period details
                    currentLeavePeriodIdForAccural = currentPeriod.PayrollPeriodId; //startid;

                    //only from date not from LeaveAdjustment
                    startDateLeaveTaken = currentPeriod.StartDateEng.Value;
                    endDateLeaveTaken = yearEnd.Value;

                }
                // if all emp not saved for last period
                else if (CalculationManager.IsAllEmployeeSavedFinally(currentPeriod.PayrollPeriodId, false) == false && isAttendnaceSaved == false)
                {
                    int lastId = endId.Value;
                    // second last period
                    endId = PayrollDataContext.PayrollPeriods.Where(x => x.Year == year && x.PayrollPeriodId < lastId)
                        .OrderByDescending(x => x.PayrollPeriodId).FirstOrDefault().PayrollPeriodId;

                    startDateLeaveTaken = currentPeriod.StartDateEng;
                    endDateLeaveTaken = new CustomDate(
                        DateHelper.GetTotalDaysInTheMonth(year, 12, IsEnglish), 12, year, IsEnglish).EnglishDate;

                }
            }



            if (startid != null)
            {
                int id = startid.Value;
                PayrollPeriod lastPeriod =
                 (
                     from p in PayrollDataContext.PayrollPeriods
                     where p.PayrollPeriodId < id
                     orderby p.PayrollPeriodId descending
                     select p
                 ).FirstOrDefault();
                if (lastPeriod != null)
                    lastYearOrYearOpeningPayrollPeriodId = lastPeriod.PayrollPeriodId;
                else
                    lastYearOrYearOpeningPayrollPeriodId = startid;
            }
            else
            {
                lastYearOrYearOpeningPayrollPeriodId =
                    (
                        from p in PayrollDataContext.PayrollPeriods
                        where p.Year != year
                        orderby p.PayrollPeriodId descending
                        select p.PayrollPeriodId
                    ).FirstOrDefault();
            }

        }

        public static void SetPeriodForPeriodicLeaveTaken(DateTime start, DateTime end,
          ref DateTime? PayrollStartDate, ref DateTime? PayrollEndDate,
            ref DateTime? startDateLeaveTaken, ref DateTime? endDateLeaveTaken)
        {

            PayrollPeriod currentPeriod = GetLastPayrollPeriod();

            if (CalculationManager.IsAllEmployeeSavedFinally(currentPeriod.PayrollPeriodId, false) == true)
            {
                PayrollStartDate = start;
                PayrollEndDate = end;

                startDateLeaveTaken = null;
                endDateLeaveTaken = null;
            }
            else
            {
                if (end < currentPeriod.StartDateEng)
                {
                    PayrollStartDate = start;
                    PayrollEndDate = end;

                    startDateLeaveTaken = null;
                    endDateLeaveTaken = null;
                }
                else if (start >= currentPeriod.StartDateEng)
                {
                    PayrollStartDate = null;
                    PayrollEndDate = null;

                    startDateLeaveTaken = start;
                    endDateLeaveTaken = end;
                }
                // middle case
                else
                {
                    PayrollStartDate = start;
                    PayrollEndDate = currentPeriod.StartDateEng.Value.AddDays(-1);

                    startDateLeaveTaken = currentPeriod.StartDateEng;
                    endDateLeaveTaken = end;
                }
            }

        }

        public static List<YearBO> GetYearList()
        {
            return
                (

                    from p in PayrollDataContext.PayrollPeriods
                    orderby p.Year
                    select new YearBO
                    {
                        Name = p.Year.ToString(),
                        Year = p.Year.Value
                    }
                ).ToList();
        }
        public static List<YearBO> GetBaisakhJanuaryLeaveYearList()
        {
            List<int> yearList =
                (
                    from p in PayrollDataContext.PayrollPeriods
                    orderby p.Year
                    select p.Year.Value
                ).Distinct().ToList();

            List<YearBO> leaveYearList = new List<YearBO>();

            int id = 0;
            foreach (int year in yearList)
            {
                leaveYearList.Add(new YearBO { FinancialDateId = id--, Name = year.ToString() });
            }


            return leaveYearList;
        }

        /// <summary>
        /// For company like Oxfam where leave start month is April, we could create table with LeaveYear to record start/end date for year period
        /// so instead hardcode start/end date
        /// </summary>
        /// <returns></returns>
        public static List<YearBO> GetAprilToMarchLeaveYearList()
        {
            List<PayrollPeriod> periods =
                (
                    from p in PayrollDataContext.PayrollPeriods
                    orderby p.PayrollPeriodId
                    select p
                ).ToList();


            List<YearBO> leaveYearList = new List<YearBO>();

            int id = 0;
            foreach (PayrollPeriod period in periods)
            {
                YearBO year = new YearBO { IsMiddlePeriod = true };


                DateTime start = DateTime.Now;
                DateTime end = DateTime.Now;


                if (period.Month >= 4) // april for current year case
                {
                    start = new DateTime(period.StartDateEng.Value.Year, 4, 1);
                    end = new DateTime(period.StartDateEng.Value.Year + 1, 3, DateTime.DaysInMonth(period.StartDateEng.Value.Year + 1, 3));
                }
                else
                {
                    start = new DateTime(period.StartDateEng.Value.Year - 1, 4, 1);
                    end = new DateTime(period.StartDateEng.Value.Year, 3, DateTime.DaysInMonth(period.StartDateEng.Value.Year, 3));
                }

                year.Name = start.Year + "/" + end.Year;
                year.StartDate = start.ToString("yyyy/MMM/dd");
                year.EndDate = end.ToString("yyyy/MMM/dd");

                year.FinancialDateId = start.Year;

                bool exists = false;

                foreach (var item in leaveYearList)
                {
                    if (item.Name.Equals(year.Name))
                    {
                        exists = true;
                        break;
                    }

                }

                if (exists == false)

                    leaveYearList.Add(year);

            }


            return leaveYearList;
        }

        public static List<EDesignationBO> GetDesignationLevelList()
        {
            return
                (
                from d in PayrollDataContext.EDesignations
                join l in PayrollDataContext.BLevels on d.LevelId equals l.LevelId
                orderby (l.Name + " - " + d.Name)
                select new EDesignationBO
                {
                    DesignationId = d.DesignationId,
                    Name = d.Name,
                    LevelAndDesignation = l.Name + " - " + d.Name,
                    LevelId = l.LevelId
                }

                ).ToList();
        }
        public static bool CheckIfDepartmentExistsForBranch(int branchId, int depId)
        {
            return
                PayrollDataContext.BranchDepartments.Any(x => x.BranchID == branchId && x.DepartmentID == depId);
        }
        public static bool IsServiceHistoryEnabled
        {
            set { }
            get
            {
                return CommonManager.CompanySetting.WhichCompany == WhichCompany.NIBL
                    ||
                    (CommonManager.Setting.EnableServiceHistory != null && CommonManager.Setting.EnableServiceHistory.Value);
            }
        }

        public static List<FixedValueEducationFaculty> GetEducationFacultyList()
        {
            return PayrollDataContext.FixedValueEducationFaculties.OrderBy(x => x.Name).ToList();
        }

        public static List<FixedValueEducationLevel> GetEducationLevelList()
        {
            return PayrollDataContext.FixedValueEducationLevels.OrderBy(x => x.Name).ToList();
        }
        public static List<FixedValueFamilyRelation> GetRelationList()
        {
            return PayrollDataContext.FixedValueFamilyRelations.OrderBy(x => x.Name).ToList();
        }
        public static List<DocumentDocumentType> GetDocumentDocumentTypeList()
        {
            return PayrollDataContext.DocumentDocumentTypes.OrderBy(x => x.DocumentTypeName).ToList();
        }
        public static List<HealthCondition> GetHealthConditionList()
        {
            return PayrollDataContext.HealthConditions.OrderBy(x => x.ConditionTypeName).ToList();
        }
        public static List<HReligion> GetReligionList()
        {
            return PayrollDataContext.HReligions.OrderBy(x => x.ReligionName).ToList();
        }
        public static List<CitizenNationality> GetCitizenshipNationaliyList()
        {
            return PayrollDataContext.CitizenNationalities.OrderBy(x => x.Nationality).ToList();
        }
        public static List<TrainingType> GetTrainingTypeList()
        {
            return PayrollDataContext.TrainingTypes.OrderBy(x => x.TrainingTypeName).ToList();
        }
        public static List<PublicationType> GetPublicationTypeList()
        {
            return PayrollDataContext.PublicationTypes.OrderBy(x => x.PublicationTypeName).ToList();
        }
        public static List<HireMethod> GetHireMethodList()
        {
            return PayrollDataContext.HireMethods.OrderBy(x => x.HireMethodName).ToList();
        }

        public static string GetFamilyMemberNameByRelation(int EmployeeID, string relation)
        {
            List<HFamily> _HFamily = PayrollDataContext.HFamilies.Where(x => x.Relation.ToLower() == relation.ToLower() && x.EmployeeId == EmployeeID).ToList();
            if (_HFamily.Any())
                return _HFamily[0].Name;
            else
                return "";

        }


        public static List<HFamily> GetAllActiveFamilyListByEmpID(int EmployeeID)
        {
            return PayrollDataContext.HFamilies.Where(x => x.EmployeeId == EmployeeID && x.Status == 1).ToList();
        }




        #endregion

        #region "Approval Flow case"


        public static bool CanChangeDocuemnt(FlowTypeEnum type, int status, int employeeId, int currentEmployeeId, int documentID)
        {
            return PayrollDataContext.CanChangeDocumentUsingApprovalFlow((int)type,
                       status, employeeId, currentEmployeeId, documentID).Value;
        }

        public static bool CanViewDocument(FlowTypeEnum type, int status, int employeeId, int currentEmployeeId, int documentID)
        {
            // This function does not work for Appraisal specific flow, need to work for these case 
            // if worked properly this function can be used to show the form like appraisal for change user

            //return PayrollDataContext.CanViewDocumentUsingApprovalFlow((int)type,
            //            status, employeeId, currentEmployeeId, documentID).Value;

            return PayrollDataContext.CanChangeDocumentUsingApprovalFlow((int)type,
                        status, employeeId, currentEmployeeId, documentID).Value;
        }
        public static GetDocumentNextStepUsingApprovalFlowResult GetDocumentNextStep(int currentStepID, FlowTypeEnum type
               , int employeeId, int documentId, bool processForCurrentStatus)
        {
            return PayrollDataContext.GetDocumentNextStepUsingApprovalFlow((int)type, currentStepID
                , employeeId, documentId, processForCurrentStatus)
                .FirstOrDefault();
        }

        public static ApprovalFlow GetDocumentPrevStep(int currentStepID, FlowTypeEnum type)
        {
            return PayrollDataContext.ApprovalFlows.Where(x => x.FlowType == (int)type && x.StepID < currentStepID)
                .OrderByDescending(x => x.StepID).FirstOrDefault();
        }


        #endregion

        #region "Salary Email"

        public static EmailContent GetMailContent(int type)
        {
            return
                PayrollDataContext.EmailContents.SingleOrDefault(x => x.ContentType == type);
        }

        public static bool SendSignOffApprovalRequest()
        {
            EmailContent dbMailContent = CommonManager.GetMailContent((int)EmailContentType.PayrollSignoffApproval);

            string subject = dbMailContent.Subject;

            string body = dbMailContent.Body;

            PayrollPeriod lastPeriod = CommonManager.GetLastPayrollPeriod();

            if (lastPeriod == null)
                return false;

            body = string.Format(body, lastPeriod.Name);

            string url = (UrlHelper.GetRootUrl());// + "/Employee");
            if (url[url.Length - 1] != '/')
                url += "/";

            url += "CP/Calculation.aspx";

            body = body.Replace("#Url#", url);

            List<UUser> userList = PayrollDataContext.UUsers.Where(x => x.EnablePayrollSignOff != null && x.EnablePayrollSignOff == true).ToList();
            bool mailSent = false;
            foreach (UUser user in userList)
            {
                if (!string.IsNullOrEmpty(user.Email))
                {
                    if (SMTPHelper.SendMail(user.Email, body, subject, "", ""))
                        mailSent = true;
                    //mailSent = true;
                }
            }

            return mailSent;
        }

        public static void SaveOrUpdateEmailContent(EmailContent content)
        {
            EmailContent dbContent = GetMailContent(content.ContentType.Value);
            if (dbContent == null)
            {
                content.CompayId = SessionManager.CurrentCompanyId;
                PayrollDataContext.EmailContents.InsertOnSubmit(content);
                SaveChangeSet();
            }
            else
            {
                dbContent.Body = content.Body;
                dbContent.Subject = content.Subject;
                UpdateChangeSet();
            }
        }

        #endregion

        public static List<CountryName> GetCountryNames()
        {



            return
                (from c in PayrollDataContext.CountryLists
                 orderby c.CountryName
                 select new CountryName
                 {
                     Country = c.CountryName
                 }).ToList();

        }
        public static List<ZoneList> GetZoneList()
        {
            return PayrollDataContext.ZoneLists.OrderBy(x => x.ZoneId).ToList();
        }

        public static List<DistrictList> GetDistrictNames()
        {
            return PayrollDataContext.DistrictLists.OrderBy(x => x.DistrictId).ToList();
        }

        public static SettingTA SettingTA
        {
            get
            {
                SettingTA set = PayrollDataContext.SettingTAs.FirstOrDefault();
                if (set == null)
                    return new SettingTA();
                return set;
            }
        }

        #region "Backup and Restore"

        public static string GetDatabaseName()
        {
            SqlConnection conn = new SqlConnection(Config.ConnectionString);
            return conn.Database;
        }

        public static string TakeBacup(string fileName)
        {
            string location = CommonManager.GetLocation();

            location += "\\" + fileName.Trim();


            string backupSql =
                string.Format("BACKUP DATABASE {0} TO DISK='{1}' WITH NOINIT", GetDatabaseName(), location);

            try
            {
                SqlConnection conn = new SqlConnection(Config.ConnectionString);

                using (conn)
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand(backupSql, conn);
                    cmd.ExecuteNonQuery();

                }

                //Compress file
                FileStream sourceFile = File.OpenRead(location);
                FileStream destFile = File.Create(location + ".zip");

                GZipStream compStream = new GZipStream(destFile, CompressionMode.Compress);

                try
                {


                    byte[] buffer = new byte[2048];
                    int bytesRead;
                    while ((bytesRead = sourceFile.Read(buffer, 0, buffer.Length)) > 0)
                    {
                        compStream.Write(buffer, 0, bytesRead);
                    }



                }
                finally
                {
                    compStream.Dispose();
                    sourceFile.Dispose();
                    destFile.Dispose();
                }
                //delete .bak file
                File.Delete(location);

                return "";
            }
            catch (Exception exp)
            {
                return exp.Message;
            }

        }

        public static string GetLocation()
        {
            string location = HttpContext.Current.Server.MapPath(Config.DefaultBackupLocation);
            return location;
        }

        //public static bool ExecuteSql(string sqlStr)
        //{
        //    //db restore            
        //    SqlConnection connection = new SqlConnection(Config.ConnectionString);


        //    using (connection)
        //    {
        //        BaseBiz.SetConnectionPwd(connection);


        //        Regex regex = new Regex("^GO|Go", RegexOptions.IgnoreCase | RegexOptions.Multiline);
        //        string[] lines = regex.Split(sqlStr);

        //        SqlTransaction transaction = connection.BeginTransaction();
        //        using (SqlCommand cmd = connection.CreateCommand())
        //        {
        //            cmd.Connection = connection;
        //            cmd.Transaction = transaction;

        //            foreach (string line in lines)
        //            {
        //                if (line.Length > 0)
        //                {
        //                    cmd.CommandText = line;
        //                    cmd.CommandType = CommandType.Text;

        //                    try
        //                    {
        //                        cmd.ExecuteNonQuery();
        //                    }
        //                    catch (SqlException)
        //                    {
        //                        transaction.Rollback();
        //                        throw;
        //                    }
        //                }
        //            }
        //        }

        //        transaction.Commit();
        //    }
        //    return true;
        //}



        public static DataSet ExecuteSql(string sqlStr)
        {
            //db restore            
            SqlConnection connection = new SqlConnection(Config.ConnectionString);


            using (connection)
            {



                BaseBiz.SetConnectionPwd(connection);

                Server server = new Server(new ServerConnection(connection));
                //server.ConnectionContext.ExecuteNonQuery(sqlStr);

                return server.ConnectionContext.ExecuteWithResults(sqlStr);
            }
            //return null;
        }
        #endregion

        public static List<HR_Service_Event> GetServieEventTypes()
        {
            return PayrollDataContext
                .HR_Service_Events.OrderBy(x => x.Name).ToList();
        }

        public static List<HR_Service_Event> GetServieEventTypesExcludingRetirementType()
        {
            return PayrollDataContext
                .HR_Service_Events.Where(x => x.GroupID != (int)ServiceEventType.Retirement).OrderBy(x => x.Name).ToList();
        }
        public static string GetServieName(int eventId)
        {
            HR_Service_Event ev = PayrollDataContext
                .HR_Service_Events.FirstOrDefault(x => x.EventID == eventId);

            if (ev != null)
                return ev.Name;
            return "";
        }
        public static void SaveNeededTableIfNotExists()
        {
            bool changed = false;
            if (PayrollDataContext.Settings.Any() == false)
            {
                changed = true;
                Setting setting = new Setting();
                PayrollDataContext.Settings.InsertOnSubmit(setting);
            }

            if (PayrollDataContext.OvertimeRoundings.Any() == false)
            {
                changed = true;
                OvertimeRounding setting = new OvertimeRounding { MinTime = 0, MaxTime = 0, rounding = 0, OvertimePermitedDays = 0 };
                PayrollDataContext.OvertimeRoundings.InsertOnSubmit(setting);
            }
            if (changed)
                PayrollDataContext.SubmitChanges();
        }

        public static bool HasBankList()
        {
            return PayrollDataContext.Banks.Any();
        }

        public static string GetEmployeeMenuDashBoardFileName()
        {
            //if (CommonManager.CompanySetting.WhichCompany==WhichCompany.Care)
            //    return "~/EmpMenus/d2.xml";
            return "~/EmpMenus/Dashboard/EmpDashboardRigo.xml";
        }

        public static Bank GetBank(int bankId)
        {
            return PayrollDataContext.Banks.SingleOrDefault(x => x.BankID == bankId);
        }
        public static List<StatusName> GetStatusName()
        {
            return PayrollDataContext.StatusNames.ToList();
        }
        public static void DeleteStatus()
        {
            PayrollDataContext.StatusNames.DeleteAllOnSubmit(GetStatusName());
            DeleteChangeSet();
        }
        public static void SaveStatusName(List<StatusName> list)
        {
            List<StatusName> dblist = GetStatusName();

            MyCache.DeleteFromGlobalCache("StatusList");

            if (dblist.Count > 0)
            {
                //for (int i = 0; i < dblist.Count; i++)
                //{
                //    dblist[i].Name = list[i].Name;
                //}
                PayrollDataContext.StatusNames.DeleteAllOnSubmit(PayrollDataContext.StatusNames.ToList());

                PayrollDataContext.StatusNames.InsertAllOnSubmit(list);
                SaveChangeSet();
            }
            else
            {
                PayrollDataContext.StatusNames.InsertAllOnSubmit(list);
                SaveChangeSet();
            }

        }

        public List<CountryList> GetAllCountries()
        {

            List<CountryList> list = null;

            list = GetFromCache<List<CountryList>>("CountryList");
            if (list != null)
                return list;


            list = PayrollDataContext.CountryLists.OrderBy(e => e.CountryName).ToList();

            SaveToCache<List<CountryList>>("CountryList", list);

            return list;


        }

        public List<DistrictList> GetAllDisticts()
        {

            List<DistrictList> list = null;

            list = GetFromCache<List<DistrictList>>("DistrictList");
            if (list != null)
                return list;


            list = PayrollDataContext.DistrictLists.OrderBy(e => e.District).ToList();

            SaveToCache<List<DistrictList>>("DistrictList", list);

            return list;


        }
        public List<CitizenNationality> GetAllNationality()
        {

            List<CitizenNationality> list = null;

            list = GetFromCache<List<CitizenNationality>>("CitizenNationalityList");
            if (list != null)
                return list;


            list = PayrollDataContext.CitizenNationalities.OrderBy(e => e.NationalityId).ToList();

            SaveToCache<List<CitizenNationality>>("CitizenNationalityList", list);

            return list;


        }

        public static List<HBloodGroup> GetAllBloodGroup()
        {

            List<HBloodGroup> list = null;

            list = GetFromCache<List<HBloodGroup>>("BloodGroup");
            if (list != null)
                return list;


            list = PayrollDataContext.HBloodGroups.OrderBy(e => e.Id).ToList();

            SaveToCache<List<HBloodGroup>>("BloodGroup", list);

            return list;


        }

        public static List<HReligion> GetAllReligionList()
        {

            List<HReligion> list = null;

            list = GetFromCache<List<HReligion>>("ReligionList");
            if (list != null)
                return list;


            list = PayrollDataContext.HReligions.OrderBy(e => e.ReligionName).ToList();

            SaveToCache<List<HReligion>>("ReligionList", list);

            return list;


        }


        public static void ResetCache()
        {
            MyCache.Reset();
        }

        public static string GetCombinedValue()
        {
            CalculationConstant entity =
                PayrollDataContext.CalculationConstants.SingleOrDefault();

            if (entity == null)
                return "";


            return entity.CombinedValue;
        }

        public static bool IsFirstSalaryGenerated()
        {
            PayrollPeriod period =
                PayrollDataContext.PayrollPeriods.
                    Where(p => p.CompanyId == SessionManager.CurrentCompanyId)
                    .FirstOrDefault();

            if (period != null)
            {
                CCalculation cal = PayrollDataContext.CCalculations.Where(
                    c => c.PayrollPeriodId == period.PayrollPeriodId)
                    .FirstOrDefault();

                if (cal == null)
                    return false;
                else
                {
                    return true;
                }
            }
            return false;
        }

        public static bool SaveResignation(string expectedDate, string reason)
        {
            try
            {
                ResignationRequest resignationReq = new ResignationRequest();
                resignationReq.ExpectedDate = Convert.ToDateTime(expectedDate);
                resignationReq.Reason = reason;
                resignationReq.CurrentStatus = (int)ResignationRequestStatus.Pending;
                resignationReq.EmployeeName = SessionManager.UserName;
                resignationReq.EmployeeID = SessionManager.CurrentLoggedInUserID;
                resignationReq.CreatedDate = System.DateTime.Now;
                PayrollDataContext.ResignationRequests.InsertOnSubmit(resignationReq);
                return SaveChangeSet();
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static List<ResignationRequest> ResignationReqListByStatus(int status)
        {
            var resignationReqList = PayrollDataContext.ResignationRequests.Where(c => c.CurrentStatus == status).ToList();
            foreach (var req in resignationReqList)
            {
                req.DaysCount = DaysCount(req.ExpectedDate.ToString());
               
            }

            return resignationReqList;
        }

        public static bool UpdateResignationReqStatus(string id, string status, List<int> clearanceFormsId)
        {
            try
            {
                string body = string.Empty;
                string subject = string.Empty;

                ResignationRequest dbResignationReq = PayrollDataContext.ResignationRequests.Where(c => c.ID == Convert.ToInt32(id)).FirstOrDefault();
                if (dbResignationReq == null)
                    return false;
                if (status == "ExitProcess")
                    dbResignationReq.CurrentStatus = (int)ResignationRequestStatus.ExitProcess;
                if (status == "Rejected")
                    dbResignationReq.CurrentStatus = (int)ResignationRequestStatus.Rejected;
                if (status == "Approved")
                    dbResignationReq.CurrentStatus = (int)ResignationRequestStatus.Approved;
                dbResignationReq.ModifiedDate = System.DateTime.Now;
                PayrollDataContext.SubmitChanges();

                foreach (var formId in clearanceFormsId)
                {
                    MandatoryClearanceForm mandatoryForms = new MandatoryClearanceForm();
                    mandatoryForms.ResignationRequestID = Convert.ToInt32(id);
                    mandatoryForms.ExitClearanceFormID = formId;
                    PayrollDataContext.MandatoryClearanceForms.InsertOnSubmit(mandatoryForms);
                    UpdateChangeSet();

                    var clearanceFormDetail = PayrollDataContext.ExitClearanceFormTypes.Where(x => x.ID == formId).FirstOrDefault();
                    EmailContent content = CommonManager.GetMailContent((int)EmailContentType.ResignationFormClearance);

                    if (content != null)
                    {
                        body = content.Body;
                        subject = content.Subject;

                        subject = subject
                            .Replace("#EmployeeName#", dbResignationReq.EmployeeName)
                            .Replace("#ClearanceForm#", clearanceFormDetail.ClearanceFormName);

                        body = body
                            .Replace("#Employee#", SessionManager.UserName)
                            .Replace("#ClearanceForm#", clearanceFormDetail.ClearanceFormName)
                            .Replace("#Requester#", dbResignationReq.EmployeeName);
                    }

                    PayrollMessage msg = new PayrollMessage();
                    msg.Body = body;
                    msg.Subject = subject;
                    msg.ReceivedTo = PayrollDataContext.UUsers.Where(x => x.UserID == clearanceFormDetail.PersonInPositionId).FirstOrDefault().UserName;
                    msg.SendBy = dbResignationReq.EmployeeName;
                    msg.IsRead = false;
                    msg.ReceivedDate = CustomDate.GetTodayDate(IsEnglish).ToString();
                    msg.ReceivedDateEng = CustomDate.GetTodayDate(IsEnglish).EnglishDate;
                    PayrollDataContext.PayrollMessages.InsertOnSubmit(msg);

                    UpdateChangeSet();
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

        }

        public static ResignationRequest GetResignationReqById(string id)
        {
            ResignationRequest request = PayrollDataContext.ResignationRequests.Where(c => c.ID == Convert.ToInt32(id)).FirstOrDefault();
            return request;
        }

        public static int DaysCount(string expectedDate)
        {
            TimeSpan difference = (Convert.ToDateTime(expectedDate) - System.DateTime.Now);
            var days = Convert.ToInt32(difference.TotalDays);

            return days;
        }

        public static ResignationRequest RequestCheck()
        {
            var resignationReq = PayrollDataContext.ResignationRequests.Where(c => c.EmployeeID == SessionManager.CurrentLoggedInUserID).FirstOrDefault();
            return resignationReq;
        }

        public static string GetExitClearanceFormTypeById()
        {
            return PayrollDataContext.ExitClearanceFormTypes.Where(x => x.PersonInPositionId == SessionManager.CurrentLoggedInUserID).First().ClearanceFormName;
        }

        public static string FilterResignationStatus(int? statusId)
        {
            if (statusId == (int)ResignationRequestStatus.Pending)
            {
                return ResignationRequestStatus.Pending.ToString();
            }
            if (statusId == (int)ResignationRequestStatus.ExitProcess)
            {
                return ResignationRequestStatus.ExitProcess.ToString();
            }
            if (statusId == (int)ResignationRequestStatus.Approved)
            {
                return ResignationRequestStatus.Approved.ToString();
            }
            if (statusId == (int)ResignationRequestStatus.Rejected)
            {
                return ResignationRequestStatus.Rejected.ToString();
            }
            return "";
        }

        public static bool DeleteResignationReq()
        {
            ResignationRequest regReq = PayrollDataContext.ResignationRequests.Where(c => c.EmployeeID == SessionManager.CurrentLoggedInUserID).FirstOrDefault();
            if (regReq != null)
            {
                PayrollDataContext.ResignationRequests.DeleteOnSubmit(regReq);
                PayrollDataContext.SubmitChanges();
                return true;
            }
            return false;
        }

        public static List<ResignationRequest> GetAssignedResignationDetail()
        {
            List<ResignationRequest> resignationReqList = new List<ResignationRequest>();
            List<ExitClearanceFormType> exitClearanceFormType = PayrollDataContext.ExitClearanceFormTypes.Where(x => x.PersonInPositionId == SessionManager.CurrentLoggedInUserID).ToList();
            if (exitClearanceFormType.Count == 0)
            {
                return resignationReqList;
            }
            List<MandatoryClearanceForm> MandatoryFormsReqId = new List<MandatoryClearanceForm>();
            List<int> resignationReqIds = new List<int>();
            foreach (var formTypeId in exitClearanceFormType)
            {
                int id = PayrollDataContext.MandatoryClearanceForms.Where(x => x.ExitClearanceFormID == formTypeId.ID).First().ResignationRequestID;
                resignationReqIds.Add(id);
            }
            if (resignationReqIds != null)
            {
                foreach (var id in resignationReqIds)
                {
                    resignationReqList.Add(PayrollDataContext.ResignationRequests.Where(x => x.ID == id).FirstOrDefault());
                }
            }
            return resignationReqList;
        }

        public static List<ExitClearanceFormType> GetAllExitClearanceFormType()
        {
            return PayrollDataContext.ExitClearanceFormTypes.OrderBy(e => e.ID).ToList();
        }

        public static List<ResignationRequest> GetAssignedResignationReq()
        {
            int formId = PayrollDataContext.ExitClearanceFormTypes.Where(x => x.PersonInPositionId == SessionManager.CurrentLoggedInUserID).FirstOrDefault().ID;
            var resignationReqDetail = PayrollDataContext.MandatoryClearanceForms.Where(x => x.ExitClearanceFormID == formId).ToList();
            List<ResignationRequest> assignedRequests = new List<ResignationRequest>();
            foreach (var resignationReqId in resignationReqDetail)
            {
                assignedRequests.Add(PayrollDataContext.ResignationRequests.Where(x => x.ID == resignationReqId.ResignationRequestID).FirstOrDefault());
            }
            return assignedRequests;
        }

        public static List<UUser> GetAllUsers()
        {
            return PayrollDataContext.UUsers.OrderBy(x => x.EmployeeId).ToList();
        }

        public static List<ExitClearanceDetailType> GetAllClearanceDetailTypes()
        {
            return PayrollDataContext.ExitClearanceDetailTypes.OrderBy(x => x.ID).ToList();
        }

        public static bool SaveClearanceFormDetail(string formName, List<int> clearanceFormDetailIds,string assignedToUserName, string assignedToUserId)
        {
            try
            {
                string formDetailId = string.Empty;
                foreach (var id in clearanceFormDetailIds)
                {
                    formDetailId = id + ",";
                }
                ExitClearanceFormType newForm = new ExitClearanceFormType();
                newForm.ClearanceFormName = formName;
                newForm.PersonInPositionName = assignedToUserName;
                newForm.PersonInPositionId = new Guid(assignedToUserId);
                newForm.ExitClearanceDetailId = formDetailId.TrimEnd(',');
                PayrollDataContext.ExitClearanceFormTypes.InsertOnSubmit(newForm);                
                UpdateChangeSet();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static ExitClearanceFormType GetClearanceFormDetailById(int id)
        {
            ExitClearanceFormType listClearanceFormType = PayrollDataContext.ExitClearanceFormTypes.Where(x => x.ID == id).FirstOrDefault();
            return listClearanceFormType;
        }

        public static bool IsD2MonthInDollar(int year, int month)
        {
            if (year < 2070)
                return true;
            else if (year == 2070 && month == 1)
                return true;
            return false;
        }
        public static ZoneList GetZoneById(int zoneId)
        {
            return PayrollDataContext.ZoneLists.SingleOrDefault(x => x.ZoneId == zoneId);
        }
        public static DistrictList GetDistrictById(int districtId)
        {
            return PayrollDataContext.DistrictLists.SingleOrDefault(x => x.DistrictId == districtId);
        }
        public static CountryList GetCountryById(int countryId)
        {
            return PayrollDataContext.CountryLists.SingleOrDefault(x => x.CountryId == countryId);
        }
        public List<DistrictList> GetAllDistricts(int zoneId)
        {

            List<DistrictList> list = null;

            list = GetFromCache<List<DistrictList>>("DistrictList" + zoneId);
            if (list != null)
                return list;


            list = PayrollDataContext.DistrictLists.Where(e => e.ZoneId == zoneId).OrderBy(e => e.District).ToList();

            SaveToCache<List<DistrictList>>("DistrictList" + zoneId, list);

            return list;


        }

        public List<DistrictList> GetRemoteDistricts()
        {
            return PayrollDataContext.DistrictLists.Where(e => e.IsInRemoteArea == true).OrderBy(e => e.District).ToList();

        }

        public List<VDCList> GetAllVDCs(int districtId)
        {

            List<VDCList> list = null;

            list = GetFromCache<List<VDCList>>("VDCList" + districtId);
            if (list != null)
                return list;


            list = PayrollDataContext.VDCLists.Where(e => e.DistrictId == districtId)
                .OrderBy(e => e.Name).ToList();

            SaveToCache<List<VDCList>>("VDCList" + districtId, list);

            return list;


        }
        public List<ZoneList> GetAllZones()
        {


            List<ZoneList> list = null;

            list = GetFromCache<List<ZoneList>>("ZoneList");
            if (list != null)
                return list;


            list = PayrollDataContext.ZoneLists.OrderBy(e => e.Zone).ToList();

            SaveToCache<List<ZoneList>>("ZoneList", list);

            return list;


        }

        #region "Grade History"

        public static List<EGradeHistory> GetGradeHistory(int employeeId)
        {
            return PayrollDataContext
                .EGradeHistories.Where(g => g.EmployeeId == employeeId)
                .ToList();
        }

        public static EGradeHistory GetGradeHistoryById(int gradeHistoryId)
        {
            return
                PayrollDataContext.EGradeHistories
                .Where(g => g.GradeHistoryId == gradeHistoryId)
                .SingleOrDefault();
        }
        public static EGradeHistory GetPreviousGradeHistory(int currentGradeHistory)
        {
            return
                PayrollDataContext.EGradeHistories
                .Where(g => g.GradeHistoryId < currentGradeHistory)
                .OrderByDescending(g => g.GradeHistoryId)
                .Take(1).SingleOrDefault();
        }

        public static bool SaveGradeHistory(EGradeHistory history)
        {
            PayrollDataContext.EGradeHistories.InsertOnSubmit(history);
            return SaveChangeSet();
        }


        public static void UpdateGradeHistory(EGradeHistory entity, bool isFirstOne)
        {
            EGradeHistory dbEntity = PayrollDataContext.EGradeHistories
                .Where(g => g.GradeHistoryId == entity.GradeHistoryId)
                .SingleOrDefault();

            if (dbEntity == null)
                return;

            dbEntity.FromDate = entity.FromDate;
            dbEntity.FromDateEng = entity.FromDateEng;
            dbEntity.GradeId = entity.GradeId;

            //if only one row then update GradeId in EEmployee table also
            if (isFirstOne)
            {
                EEmployee employee = PayrollDataContext.EEmployees
                    .Where(e => e.EmployeeId == entity.EmployeeId)
                    .SingleOrDefault();
                employee.GradeId = entity.GradeId;
            }

            UpdateChangeSet();
        }


        #endregion

        #region "Workshift History"

        public static List<EWorkShiftHistory> GetWorkShiftHistory(int employeeId)
        {
            return PayrollDataContext
                .EWorkShiftHistories.Where(g => g.EmployeeId == employeeId)
                .ToList();
        }

        public static EWorkShiftHistory GetWorkShiftHistoryById(int historyId)
        {
            return
                PayrollDataContext.EWorkShiftHistories
                .Where(g => g.WorkShiftHistoryId == historyId)
                .SingleOrDefault();
        }
        public static EWorkShiftHistory GetPreviousWorkShiftHistory(int currentHistory)
        {
            return
                PayrollDataContext.EWorkShiftHistories
                .Where(g => g.WorkShiftHistoryId < currentHistory)
                .OrderByDescending(g => g.WorkShiftHistoryId)
                .Take(1).SingleOrDefault();
        }

        public static bool SaveWorkShiftHistory(EWorkShiftHistory history)
        {
            PayrollDataContext.EWorkShiftHistories.InsertOnSubmit(history);
            return SaveChangeSet();
        }


        public static void UpdateWorkShiftHistory(EWorkShiftHistory entity, bool isFirstOne)
        {
            EWorkShiftHistory dbEntity = PayrollDataContext.EWorkShiftHistories
                .Where(g => g.WorkShiftHistoryId == entity.WorkShiftHistoryId)
                .SingleOrDefault();

            if (dbEntity == null)
                return;

            dbEntity.FromDate = entity.FromDate;
            dbEntity.FromDateEng = entity.FromDateEng;
            dbEntity.WorkShiftId = entity.WorkShiftId;

            //if only one row then update GradeId in EEmployee table also
            if (isFirstOne)
            {
                EEmployee employee = PayrollDataContext.EEmployees
                    .Where(e => e.EmployeeId == entity.EmployeeId)
                    .SingleOrDefault();
                employee.WorkShiftId = entity.WorkShiftId;
            }

            UpdateChangeSet();
        }


        #endregion

        #region "Location History"

        public static List<ELocationHistory> GetLocationHistory(int employeeId)
        {
            return PayrollDataContext
                .ELocationHistories.Where(g => g.EmployeeId == employeeId)
                .ToList();
        }
        //public static List<BranchDepartmentHistory> GetBranchDepartmentHistory(int employeeId)
        //{
        //    return PayrollDataContext
        //        .BranchDepartmentHistories.Where(g => g.EmployeeId == employeeId)
        //        .OrderBy(x=>x.BranchDepartmentId)
        //        .ToList();
        //}
        public static List<BranchDepartmentHistory> GetBranchDepartmentHistory(int employeeId, bool isDepartmentTransfer)
        {
            List<BranchDepartmentHistory> list = PayrollDataContext
                .BranchDepartmentHistories.Where(g => g.EmployeeId == employeeId)
                .Where(x =>
                    (isDepartmentTransfer == true && x.IsDepartmentTransfer != null && x.IsDepartmentTransfer.Value == true)
                    ||
                    (isDepartmentTransfer == false && (x.IsDepartmentTransfer == null || x.IsDepartmentTransfer == false))
                    )
                .OrderBy(x => x.FromDateEng)
                .ToList();


            foreach (BranchDepartmentHistory item in list)
            {
                if (item.FromBranchId != null)
                    item.FromBranch = new BranchManager().GetById(item.FromBranchId.Value).Name;
                if (item.FromDepartmentId != null)
                    item.FromDepartment = new DepartmentManager().GetById(item.FromDepartmentId.Value).Name;
                item.Branch = new BranchManager().GetById(item.BranchId.Value).Name;
                item.Department = new DepartmentManager().GetById(item.DepeartmentId.Value).Name;
            }

            return list;
        }
        public static List<DesignationHistory> GetDesignationHistory(int employeeId)
        {
            List<DesignationHistory> list = PayrollDataContext
                .DesignationHistories.Where(g => g.EmployeeId == employeeId)
                .OrderBy(x => x.EmployeeDesignationId)
                .ToList();


            foreach (DesignationHistory item in list)
            {
                item.Designation = new CommonManager().GetDesignationById(item.DesignationId.Value).Name;
            }

            return list;
        }


        public static string GetStartingDesignationOfEmployee(int employeeId)
        {
            DesignationHistory list = PayrollDataContext
                .DesignationHistories.Where(g => g.EmployeeId == employeeId)
                .OrderBy(x => x.FromDateEng)
                .ToList().FirstOrDefault();
            if (list != null)
                return new CommonManager().GetDesignationById(list.DesignationId.Value).Name;
            else
                return EmployeeManager.GetEmployeeById(employeeId).EDesignation.Name;
        }

        public static string GetStartingLevelOfEmployee(int employeeId)
        {
            DesignationHistory list = PayrollDataContext
                .DesignationHistories.Where(g => g.EmployeeId == employeeId)
                .OrderBy(x => x.FromDateEng)
                .ToList().FirstOrDefault();
            if (list != null)
            {
                int levelid = PayrollDataContext.EDesignations.SingleOrDefault(X => X.DesignationId == list.DesignationId).LevelId.Value;
                return PayrollDataContext.BLevels.SingleOrDefault(x => x.LevelId == levelid).Name;
            }
            else return "";
        }

        public static string GetStartingBranchOfEmployee(int employeeId)
        {
            BranchDepartmentHistory list = PayrollDataContext
                .BranchDepartmentHistories.Where(g => g.EmployeeId == employeeId)
                .OrderBy(x => x.FromDateEng)
                .FirstOrDefault();
            if (list != null)
            {

                return PayrollDataContext.Branches.SingleOrDefault(x => x.BranchId == list.BranchId).Name;
            }
            else
                return EmployeeManager.GetEmployeeById(employeeId).Branch.Name;
        }

        public static string GetStartingDepartmentOfEmployee(int employeeId)
        {
            BranchDepartmentHistory list = PayrollDataContext
                .BranchDepartmentHistories.Where(g => g.EmployeeId == employeeId)
                .OrderBy(x => x.FromDateEng)
                .ToList().FirstOrDefault();
            if (list != null)
            {

                return PayrollDataContext.Departments.SingleOrDefault(x => x.DepartmentId == list.DepeartmentId).Name;
            }
            else
                return EmployeeManager.GetEmployeeById(employeeId).Department.Name;

        }





        public static ELocationHistory GetLocationHistoryById(int historyId)
        {
            return
                PayrollDataContext.ELocationHistories
                .Where(g => g.LocationHistoryId == historyId)
                .SingleOrDefault();
        }
        public static BranchDepartmentHistory GetBranchDepartmentHistoryById(int historyId)
        {
            return
                PayrollDataContext.BranchDepartmentHistories
                .Where(g => g.BranchDepartmentId == historyId)
                .SingleOrDefault();
        }
        public static EmployeeServiceHistory GetServiceHistory(int historyId)
        {
            return
                PayrollDataContext.EmployeeServiceHistories
                .Where(g => g.EventSourceID == historyId)
                .SingleOrDefault();
        }
        public static DesignationHistory GetDesignationHistoryById(int historyId)
        {
            return
                PayrollDataContext.DesignationHistories
                .Where(g => g.EmployeeDesignationId == historyId)
                .SingleOrDefault();
        }
        public static ELocationHistory GetPreviousLocationHistory(int currentHistory)
        {
            ELocationHistory current = PayrollDataContext.ELocationHistories.SingleOrDefault(x => x.LocationHistoryId == currentHistory);
            return
                PayrollDataContext.ELocationHistories
                .Where(g => g.EmployeeId == current.EmployeeId && g.LocationHistoryId < currentHistory)
                .OrderByDescending(g => g.LocationHistoryId)
                .Take(1).SingleOrDefault();
        }

        public static BranchDepartmentHistory GetPreviousBranchDepartmentHistory(int currentHistory)
        {
            BranchDepartmentHistory current = PayrollDataContext.BranchDepartmentHistories.SingleOrDefault(x => x.BranchDepartmentId == currentHistory);
            return
                PayrollDataContext.BranchDepartmentHistories
                .Where(g => g.EmployeeId == current.EmployeeId && g.BranchDepartmentId < currentHistory)
                .OrderByDescending(g => g.FromDateEng)
                .Take(1).SingleOrDefault();
        }
        public static DesignationHistory GetPreviousDesignationHistory(int currentHistory)
        {
            DesignationHistory current = PayrollDataContext.DesignationHistories.SingleOrDefault(x => x.EmployeeDesignationId == currentHistory);
            return
                PayrollDataContext.DesignationHistories
                .Where(g => g.EmployeeId == current.EmployeeId && g.EmployeeDesignationId < currentHistory)
                .OrderByDescending(g => g.EmployeeDesignationId)
                .Take(1).SingleOrDefault();
        }
        public static bool SaveLocationHistory(ELocationHistory history)
        {
            PayrollDataContext.ELocationHistories.InsertOnSubmit(history);
            return SaveChangeSet();
        }

        public static void SaveFirstIBranchfNotExists(int employeeId)
        {
            // save first set branch in history
            if (!PayrollDataContext.BranchDepartmentHistories.Any(x => x.EmployeeId == employeeId))
            {
                EEmployee employee = PayrollDataContext.EEmployees
                   .Where(e => e.EmployeeId == employeeId)
                   .SingleOrDefault();

                BranchDepartmentHistory first = new BranchDepartmentHistory();
                first.EmployeeId = employeeId;
                //first.FromBranchId = employee.BranchId;
                //first.FromDepartmentId = employee.DepartmentId;
                first.BranchId = employee.BranchId;
                first.DepeartmentId = employee.DepartmentId;

                first.SubDepartmentId = employee.SubDepartmentId;
                first.FromSubDepartmentId = employee.SubDepartmentId;

                ECurrentStatus firstStatus = EmployeeManager.GetFirstStatus(employeeId);

                first.FromDate = firstStatus.FromDate;
                first.FromDateEng = firstStatus.FromDateEng;
                first.IsFirst = true;
                first.EventID = (int)ServiceEventType.Appointment;

                PayrollDataContext.BranchDepartmentHistories.InsertOnSubmit(first);

                PayrollDataContext.SubmitChanges();
            }


        }
        public static void SaveFirstIfDesignationNotExists(int employeeId)
        {
            // save first set branch in history
            if (!PayrollDataContext.DesignationHistories.Any(x => x.EmployeeId == employeeId))
            {
                EEmployee employee = PayrollDataContext.EEmployees
                   .Where(e => e.EmployeeId == employeeId)
                   .SingleOrDefault();

                DesignationHistory first = new DesignationHistory();
                first.EmployeeId = employeeId;
                first.DesignationId = employee.DesignationId;


                ECurrentStatus firstStatus = EmployeeManager.GetFirstStatus(employeeId);

                first.FromDate = firstStatus.FromDate;
                first.FromDateEng = firstStatus.FromDateEng;
                first.IsFirst = true;
                first.EventID = (int)ServiceEventType.Appointment;
                PayrollDataContext.DesignationHistories.InsertOnSubmit(first);

                PayrollDataContext.SubmitChanges();
            }


        }
        public static int GetLastBranchId(int empid)
        {
            BranchDepartmentHistory prevHistory = PayrollDataContext.BranchDepartmentHistories
                .Where(x => x.EmployeeId == empid)
                .OrderByDescending(x => x.FromDateEng).FirstOrDefault();

            if (prevHistory == null)
                return new EmployeeManager().GetById(empid).BranchId.Value;

            return prevHistory.BranchId.Value;
        }
        public static bool SaveBranchDepartmentHistory(BranchDepartmentHistory history)
        {
            EEmployee employee = PayrollDataContext.EEmployees
                   .Where(e => e.EmployeeId == history.EmployeeId)
                   .SingleOrDefault();

            history.CreatedBy = SessionManager.CurrentLoggedInUserID;
            history.ModifiedBy = SessionManager.CurrentLoggedInUserID;
            history.CreatedOn = DateTime.Now;
            history.ModifiedOn = history.CreatedOn;
            history.IsFirst = false;
            //set prev branch as FromBranchId

            BranchDepartmentHistory prevHistory = PayrollDataContext.BranchDepartmentHistories
                .Where(x => x.EmployeeId == history.EmployeeId && x.FromDateEng < history.FromDateEng)
                .OrderByDescending(x => x.FromDateEng).FirstOrDefault();

            if (prevHistory != null)
            {
                history.FromBranchId = prevHistory.BranchId;
                history.FromDepartmentId = prevHistory.DepeartmentId;
            }
            else
            {
                EEmployee emp = EmployeeManager.GetEmployeeById(history.EmployeeId.Value);
                history.FromBranchId = emp.BranchId;
                history.FromDepartmentId = emp.DepartmentId;
            }

            if (history.SubDepartmentId != null)
            {
                history.FromSubDepartmentId = employee.SubDepartmentId;
                employee.SubDepartmentId = history.SubDepartmentId;
            }


            PayrollDataContext.BranchDepartmentHistories.InsertOnSubmit(history);

            // set auto branch for department transfer
            if (history.IsDepartmentTransfer != null && history.IsDepartmentTransfer.Value)
            {
                if (prevHistory != null && prevHistory.BranchId != null)
                {
                    history.BranchId = prevHistory.BranchId;
                }
                else
                    history.BranchId = employee.BranchId;
            }

            employee.BranchId = history.BranchId;
            employee.DepartmentId = history.DepeartmentId;

            return SaveChangeSet();
        }

        public static string GetFiscalYearText(int periodid)
        {
            PayrollPeriod peroid = GetPayrollPeriod(periodid);
            FinancialDate year = PayrollDataContext.FinancialDates
                .FirstOrDefault(x => x.FinancialDateId == peroid.FinancialDateId);

            CustomDate start = CustomDate.GetCustomDateFromString(year.StartingDate, IsEnglish);

            CustomDate end = CustomDate.GetCustomDateFromString(year.EndingDate, IsEnglish);

            return start.Year + "/" + end.Year.ToString().Substring(2, 2);
        }

        public static bool SaveDesignationHistory(DesignationHistory history)
        {
            EEmployee employee = PayrollDataContext.EEmployees
                   .Where(e => e.EmployeeId == history.EmployeeId)
                   .SingleOrDefault();


            history.CreatedBy = SessionManager.CurrentLoggedInUserID;
            history.ModifiedBy = SessionManager.CurrentLoggedInUserID;
            history.CreatedOn = DateTime.Now;
            history.ModifiedOn = history.CreatedOn;
            history.IsFirst = false;
            PayrollDataContext.DesignationHistories.InsertOnSubmit(history);



            employee.DesignationId = history.DesignationId;

            return SaveChangeSet();
        }
        public static void UpdateLocationHistory(ELocationHistory entity, bool isFirstOne)
        {
            ELocationHistory dbEntity = PayrollDataContext.ELocationHistories
                .Where(g => g.LocationHistoryId == entity.LocationHistoryId)
                .SingleOrDefault();

            if (dbEntity == null)
                return;

            dbEntity.FromDate = entity.FromDate;
            dbEntity.FromDateEng = entity.FromDateEng;
            dbEntity.LocationId = entity.LocationId;

            //if only one row then update GradeId in EEmployee table also
            if (isFirstOne)
            {
                EHumanResource employee = PayrollDataContext.EHumanResources
                    .Where(e => e.EmployeeId == entity.EmployeeId)
                    .SingleOrDefault();
                employee.LocationId = entity.LocationId;
            }

            UpdateChangeSet();
        }

        public static bool IsLastBranchTransfer(int branchDepartmentId, int employeeId)
        {
            BranchDepartmentHistory lastest = PayrollDataContext
                 .BranchDepartmentHistories
                 .Where(x => x.EmployeeId == employeeId && x.FromBranchId != x.BranchId)
                 .OrderByDescending(x => x.FromDateEng).FirstOrDefault();


            if (lastest == null)
                return true;

            if (lastest.BranchDepartmentId != branchDepartmentId)
                return false;

            return true;
        }

        public static bool IsLastDepartmentTransfer(int branchDepartmentId, int employeeId)
        {
            BranchDepartmentHistory lastest = PayrollDataContext
                 .BranchDepartmentHistories
                 .Where(x => x.EmployeeId == employeeId && x.IsDepartmentTransfer != null
                 && x.IsDepartmentTransfer.Value)
                 .OrderByDescending(x => x.FromDateEng).FirstOrDefault();


            if (lastest == null)
                return true;

            if (lastest.BranchDepartmentId != branchDepartmentId)
                return false;

            return true;
        }

        public static Status DeleteBranchTransfer(int branchDepId)
        {
            BranchDepartmentHistory dbEntity = PayrollDataContext.BranchDepartmentHistories
                .FirstOrDefault(x => x.BranchDepartmentId == branchDepId);

            Status status = new Status();

            if (dbEntity == null)
            {
                status.ErrorMessage = "Record does not exists.";
                return status;
            }

            //BranchDepartmentHistory latestHistory = PayrollDataContext.BranchDepartmentHistories
            //    .Where(x => x.EmployeeId == dbEntity.EmployeeId)
            //    .OrderByDescending(x => x.FromDateEng).FirstOrDefault();
            //if (latestHistory != null && latestHistory.BranchDepartmentId != branchDepId)
            //{
            //    status.ErrorMessage = "Only last branch transfer can be deleted.";
            //    return status;
            //}

            EEmployee employee = new EmployeeManager().GetById(dbEntity.EmployeeId.Value);

            BranchDepartmentHistory secondLastBranch = PayrollDataContext.BranchDepartmentHistories
                .Where(x => x.EmployeeId == dbEntity.EmployeeId && x.BranchDepartmentId != dbEntity.BranchDepartmentId)
                .OrderByDescending(x => x.FromDateEng).FirstOrDefault();


            // if there is second last branch transer then set that one
            if (secondLastBranch != null)
            {
                employee.BranchId = secondLastBranch.BranchId;
            }
            else if (dbEntity.FromBranchId != null)
            {
                employee.BranchId = dbEntity.FromBranchId;
            }
            else
            {
                employee.BranchId = dbEntity.BranchId;
            }


            // if there is second last branch transer then set that one
            if (secondLastBranch != null && secondLastBranch.DepeartmentId != null)
            {
                employee.DepartmentId = secondLastBranch.DepeartmentId;
            }
            else if (dbEntity.FromDepartmentId != null)
            {
                employee.DepartmentId = dbEntity.FromDepartmentId;
            }
            else if (dbEntity.DepeartmentId != null)
            {
                employee.DepartmentId = dbEntity.DepeartmentId;
            }
            else
                employee.DepartmentId = employee.DepartmentId;



            // Log
            PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(dbEntity.EmployeeId.Value, (byte)MonetoryChangeLogTypeEnum.Transfer
                                     , "Branch Transfer",

                                     (dbEntity.BranchId != null ? BranchManager.GetBranchById(dbEntity.BranchId.Value).Name : "")
                                     + ", " +
                                     (dbEntity.DepeartmentId != null ? DepartmentManager.GetDepartmentById(dbEntity.DepeartmentId.Value).Name : "")
                                     ,
                                     "", LogActionEnum.Delete));


            PayrollDataContext.BranchDepartmentHistories.DeleteOnSubmit(dbEntity);
            PayrollDataContext.SubmitChanges();

            return status;
        }

        public static Status DeleteDesignatioChange(int designChangeID)
        {
            DesignationHistory dbEntity = PayrollDataContext.DesignationHistories
                .FirstOrDefault(x => x.EmployeeDesignationId == designChangeID);

            Status status = new Status();

            if (dbEntity == null)
            {
                status.ErrorMessage = "Record does not exists.";
                return status;
            }


            // if employee is of level/grade type and do not allow to delete current designation making designation in EEmployee-Designation-LevelId table
            // mismatch to PEmployeeIncome-LevelId
            // do not allow to save as it will make current level/designation in mis-match in EEmployee and PEmployeeIncome table
            if (EmployeeManager.HasLevelGrade(dbEntity.EmployeeId.Value))
            {
                //BLevel level = NewHRManager.GetEmployeeCurrentLevel(history.EmployeeId.Value);
                //EDesignation designation = CommonManager.GetDesigId(history.DesignationId.Value);

                //if (level != null && level.LevelId != designation.LevelId)
                {
                    status.ErrorMessage = @"Level assigned employee designation can not be delete from here,"
                       + " please use promotion page to delete the level and designation.";

                    //status.ErrorMessage = "Current level is " + level.Name + ", new level can not be changed from this page,"
                    //    + " please use promotion page to change the level.";
                    return status;
                }
            }
            //BranchDepartmentHistory latestHistory = PayrollDataContext.BranchDepartmentHistories
            //    .Where(x => x.EmployeeId == dbEntity.EmployeeId)
            //    .OrderByDescending(x => x.FromDateEng).FirstOrDefault();
            //if (latestHistory != null && latestHistory.BranchDepartmentId != branchDepId)
            //{
            //    status.ErrorMessage = "Only last branch transfer can be deleted.";
            //    return status;
            //}

            EEmployee employee = new EmployeeManager().GetById(dbEntity.EmployeeId.Value);

            DesignationHistory secondLastBranch = PayrollDataContext.DesignationHistories
                .Where(x => x.EmployeeId == dbEntity.EmployeeId && x.EmployeeDesignationId != dbEntity.EmployeeDesignationId)
                .OrderByDescending(x => x.FromDateEng).FirstOrDefault();


            // if there is second last branch transer then set that one
            if (secondLastBranch != null)
            {
                employee.DesignationId = secondLastBranch.DesignationId;
            }
            else if (dbEntity.FromDesignationId != null)
            {
                employee.DesignationId = dbEntity.FromDesignationId;
            }
            else
            {
                employee.DesignationId = dbEntity.DesignationId;
            }






            // Log
            PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(dbEntity.EmployeeId.Value, (byte)MonetoryChangeLogTypeEnum.Information
                                     , "Designation Change",

                                     (dbEntity.DesignationId != null ? CommonManager.GetDesigId(dbEntity.DesignationId.Value).Name : "")

                                     ,
                                     "", LogActionEnum.Delete));


            PayrollDataContext.DesignationHistories.DeleteOnSubmit(dbEntity);
            PayrollDataContext.SubmitChanges();

            return status;
        }
        public static Status DeleteServiceHistory(int historyID)
        {
            EmployeeServiceHistory dbEntity = PayrollDataContext.EmployeeServiceHistories
                .FirstOrDefault(x => x.EventSourceID == historyID);

            Status status = new Status();

            if (dbEntity == null)
            {
                status.ErrorMessage = "Record does not exists.";
                return status;
            }

            if (PayrollDataContext.EmployeeServiceHistories.Any(x => x.EmployeeId == dbEntity.EmployeeId &&
                x.EventSourceID != dbEntity.EventSourceID) == false)
            {
                status.ErrorMessage = "All event could not be deleted.";
                return status;
            }

            // if event is being used in increment then first ask to delete increment 
            if (PayrollDataContext.PEmployeeIncrements.Any(x => x.EventSourceID == historyID))
            {
                status.ErrorMessage = "Current service event already being used in the salary, so please delete the salary increment first.";
                return status;
            }

            //BranchDepartmentHistory branch = PayrollDataContext.BranchDepartmentHistories.FirstOrDefault(x => x.EventSourceID == dbEntity.EventSourceID);
            //if (branch != null)
            //    PayrollDataContext.BranchDepartmentHistories.DeleteOnSubmit(branch);

            //DesignationHistory desig = PayrollDataContext.DesignationHistories.FirstOrDefault(x => x.EventSourceID == dbEntity.EventSourceID);
            //if (desig != null)
            //    PayrollDataContext.DesignationHistories.DeleteOnSubmit(desig);

            //ECurrentStatus status1 = PayrollDataContext.ECurrentStatus.FirstOrDefault(x => x.EventSourceID == dbEntity.EventSourceID);
            //if (status1 != null)
            //    PayrollDataContext.ECurrentStatus.DeleteOnSubmit(status1);
            //BranchDepartmentHistory latestHistory = PayrollDataContext.BranchDepartmentHistories
            //    .Where(x => x.EmployeeId == dbEntity.EmployeeId)
            //    .OrderByDescending(x => x.FromDateEng).FirstOrDefault();
            //if (latestHistory != null && latestHistory.BranchDepartmentId != branchDepId)
            //{
            //    status.ErrorMessage = "Only last branch transfer can be deleted.";
            //    return status;
            //}



            PayrollDataContext.EmployeeServiceHistories.DeleteOnSubmit(dbEntity);
            PayrollDataContext.SubmitChanges();

            List<EmployeeServiceHistory> historyList = BLL.BaseBiz.PayrollDataContext.EmployeeServiceHistories.Where(x => x.EmployeeId == dbEntity.EmployeeId)
               .OrderBy(x => x.DateEng).ThenBy(x => x.CreatedOn).ToList();

            ResetStatusHistory(dbEntity.EmployeeId, historyList);
            ResetBranchDepartmentHistory(dbEntity.EmployeeId, historyList);
            ResetDesignationHistory(dbEntity.EmployeeId, historyList);

            SetLatestBranchDepDesignationHistoryToEmployee(dbEntity.EmployeeId);

            return status;
        }
        public static void UpdateBranchDepartmentHistory(BranchDepartmentHistory entity)
        {
            BranchDepartmentHistory dbEntity = PayrollDataContext.BranchDepartmentHistories
                .Where(g => g.BranchDepartmentId == entity.BranchDepartmentId)
                .SingleOrDefault();

            if (dbEntity == null)
                return;

            if (entity.IsDepartmentTransfer != null && entity.IsDepartmentTransfer.Value)
            {
                entity.BranchId = dbEntity.BranchId;
            }

            dbEntity.FromDate = entity.FromDate;
            dbEntity.FromDateEng = entity.FromDateEng;
            dbEntity.BranchId = entity.BranchId;
            dbEntity.DepeartmentId = entity.DepeartmentId;
            dbEntity.Note = entity.Note;

            dbEntity.LetterNumber = entity.LetterNumber;
            dbEntity.LetterDate = entity.LetterDate;
            dbEntity.LetterDateEng = entity.LetterDateEng;
            dbEntity.SpecialResponsibility = entity.SpecialResponsibility;
            dbEntity.DepartureDate = entity.DepartureDate;
            dbEntity.DepartureDateEng = entity.DepartureDateEng;
            dbEntity.SubDepartmentId = entity.SubDepartmentId;

            dbEntity.EventID = entity.EventID;

            dbEntity.ModifiedOn = DateTime.Now;
            dbEntity.ModifiedBy = SessionManager.CurrentLoggedInUserID;

            // Update for From Fields
            BranchDepartmentHistory prevHistory = PayrollDataContext.BranchDepartmentHistories
                .Where(x => x.EmployeeId == entity.EmployeeId && x.FromDateEng < entity.FromDateEng)
                .OrderByDescending(x => x.FromDateEng).FirstOrDefault();
            if (prevHistory != null)
            {
                dbEntity.FromBranchId = prevHistory.BranchId;
                dbEntity.FromDepartmentId = prevHistory.DepeartmentId;
            }
            else
            {
                dbEntity.FromBranchId = null; ;
                dbEntity.FromDepartmentId = null;
            }



            UpdateChangeSet();

            // Assign lastest from history as insert can be done for past month also

            BranchDepartmentHistory latestHistory = PayrollDataContext.BranchDepartmentHistories
                .Where(x => x.EmployeeId == entity.EmployeeId)
                .OrderByDescending(x => x.FromDateEng).FirstOrDefault();

            if (latestHistory != null)
            {
                EEmployee employee = PayrollDataContext.EEmployees
                    .Where(e => e.EmployeeId == entity.EmployeeId)
                    .SingleOrDefault();

                employee.BranchId = latestHistory.BranchId;
                employee.DepartmentId = latestHistory.DepeartmentId;

                if (entity.SubDepartmentId != null)
                    employee.SubDepartmentId = latestHistory.SubDepartmentId;
            }


            UpdateChangeSet();
        }


        public static void UpdatePromotionHistory(LevelGradeChangeDetail entity)
        {
            LevelGradeChangeDetail dbEntity = PayrollDataContext.LevelGradeChangeDetails
                .FirstOrDefault(x => x.DetailId == entity.DetailId);

            if (dbEntity == null)
                return;

            dbEntity.FromDate = entity.FromDate;
            dbEntity.FromDateEng = entity.FromDateEng;

            dbEntity.PrevLevelName = entity.PrevLevelName;
            dbEntity.LevelName = entity.LevelName;

            UpdateChangeSet();
        }
        public static Status SaveUpdateServiceHistory(EmployeeServiceHistory entity)
        {
            Status status = new Status();

            if (entity.EventSourceID == 0 && entity.EventID == (int)ServiceEventType.ReAppointment)
            {
                status.ErrorMessage = "Re-Hire can be done from retirement module only.";
                return status;
            }

            EmployeeServiceHistory dbEntity = PayrollDataContext.EmployeeServiceHistories
                .Where(g => g.EventSourceID == entity.EventSourceID)
                .SingleOrDefault();



            GetEmployeeCurrentBranchDepartmentResult branchDep =
             PayrollDataContext.GetEmployeeCurrentBranchDepartment(entity.DateEng, entity.EmployeeId)
             .FirstOrDefault();

            int branchIdByDate = branchDep.BranchId.Value;
            int departmentIdByDate = branchDep.DepartmentId.Value;
            int statusIdByDate = PayrollDataContext.GetEmployeeStatusForDate(entity.DateEng, entity.EmployeeId).Value;
            int designationIdByDate = PayrollDataContext.GetEmployeeCurrentPositionForDate(entity.DateEng, entity.EmployeeId).Value;



            // insert mode
            if (entity.EventSourceID == 0)
            {
                entity.CreatedBy = SessionManager.CurrentLoggedInUserID;
                entity.CreatedOn = DateTime.Now;
                entity.ModifiedBy = entity.CreatedBy;
                entity.ModifiedOn = entity.CreatedOn;
                entity.PayrollPeriodId = CommonManager.GetLastPayrollPeriod().PayrollPeriodId;
                PayrollDataContext.EmployeeServiceHistories.InsertOnSubmit(entity);
                PayrollDataContext.SubmitChanges();

            }
            else
            {

                // if Appointment row is being edited and date is being changed, then change Salary Calculate date also
                if (entity.EventID == (int)ServiceEventType.Appointment)
                {
                    EHumanResource hr = PayrollDataContext.EHumanResources.FirstOrDefault(x => x.EmployeeId == entity.EmployeeId);
                    hr.SalaryCalculateFrom = entity.Date;
                    hr.SalaryCalculateFromEng = entity.DateEng;
                }


                dbEntity.EventID = entity.EventID;
                dbEntity.Date = entity.Date;
                dbEntity.DateEng = entity.DateEng;

                dbEntity.ToDate = entity.ToDate;
                dbEntity.ToDateEng = entity.ToDateEng;

                dbEntity.BranchId = entity.BranchId;
                dbEntity.DepartmentId = entity.DepartmentId;
                dbEntity.SubDepartmentId = entity.SubDepartmentId;
                dbEntity.LevelId = entity.LevelId;
                dbEntity.DesignationId = entity.DesignationId;
                dbEntity.StatusId = entity.StatusId;

                dbEntity.Notes = entity.Notes;

                dbEntity.LetterNo = entity.LetterNo;
                dbEntity.LetterDate = entity.LetterDate;
                dbEntity.LetterDateEng = entity.LetterDateEng;

                dbEntity.ModifiedOn = DateTime.Now;
                dbEntity.ModifiedBy = SessionManager.CurrentLoggedInUserID;
                if (dbEntity.PayrollPeriodId == null)
                    dbEntity.PayrollPeriodId = CommonManager.GetLastPayrollPeriod().PayrollPeriodId;



            }


            // Process to insert or update respective tables 
            // Assign lastest from history as insert can be done for past month also
            //BranchDepartmentHistory latestHistory = PayrollDataContext.BranchDepartmentHistories.FirstOrDefault(x => x.EventSourceID == entity.EventSourceID);
            //if (latestHistory != null)
            //{
            //    GetBranchHistoryFromService(entity, latestHistory);
            //}
            //else if(isBranchDepChanged)
            //{
            //    latestHistory = new BranchDepartmentHistory();
            //    latestHistory.EmployeeId = entity.EmployeeId;
            //    latestHistory.IsFirst = false;
            //    latestHistory.EventSourceID = entity.EventSourceID;
            //    latestHistory.FromBranchId = branchIdByDate;
            //    latestHistory.FromDepartmentId = departmentIdByDate;
            //    GetBranchHistoryFromService(entity, latestHistory);
            //    PayrollDataContext.BranchDepartmentHistories.InsertOnSubmit(latestHistory);
            //}

            //DesignationHistory desig = PayrollDataContext.DesignationHistories.FirstOrDefault(x => x.EventSourceID == entity.EventSourceID);
            //if (desig != null)
            //{
            //    GetDesignationHistoryFromServie(entity, desig);
            //}
            //else if(isDesignationChanged)
            //{
            //    desig = new DesignationHistory();
            //    desig.EmployeeId = entity.EmployeeId;
            //    desig.IsFirst = false;
            //    desig.EventSourceID = entity.EventSourceID;
            //    desig.FromDesignationId = designationIdByDate;
            //    GetDesignationHistoryFromServie(entity, desig);
            //    PayrollDataContext.DesignationHistories.InsertOnSubmit(desig);
            //}


            // call to update in respective table
            UpdateChangeSet();


            List<EmployeeServiceHistory> historyList = BLL.BaseBiz.PayrollDataContext.EmployeeServiceHistories.Where(x => x.EmployeeId == entity.EmployeeId)
               .OrderBy(x => x.DateEng).ThenBy(x => x.CreatedOn).ToList();


            ResetStatusHistory(entity.EmployeeId, historyList);

            ResetBranchDepartmentHistory(entity.EmployeeId, historyList);

            ResetDesignationHistory(entity.EmployeeId, historyList);

            //ECurrentStatus status = PayrollDataContext.ECurrentStatus.FirstOrDefault(x => x.EventSourceID == entity.EventSourceID);
            //if (status != null)
            //{
            //    // update status table
            //    GetStatusChangeFromService(entity, status);
            //}
            //else if(isStatusChanged)
            //{
            //    // insert status table
            //    status = new ECurrentStatus();
            //    status.EmployeeId = entity.EmployeeId;
            //    status.EventSourceID = entity.EventSourceID;
            //    GetStatusChangeFromService(entity, status);
            //    PayrollDataContext.ECurrentStatus.InsertOnSubmit(status);
            //}





            SetLatestBranchDepDesignationHistoryToEmployee(entity.EmployeeId);

            return status;
        }

        private static void ResetStatusHistory(int empId, List<EmployeeServiceHistory> historyList)
        {
            bool isHistoryChanged = false;

            // Reset status list
            List<ECurrentStatus> dbStatusList = BLL.BaseBiz.PayrollDataContext.ECurrentStatus.Where(x => x.EmployeeId == empId)
                .OrderByDescending(x => x.FromDateEng).ToList();

            List<EmployeeServiceHistory> newCorrectStatusList = new List<EmployeeServiceHistory>();


            EmployeeServiceHistory lastStatus = null;
            // Case 1 : Create new correct list
            foreach (EmployeeServiceHistory history in historyList)
            {
                if (lastStatus == null)
                {
                    newCorrectStatusList.Add(history);
                    lastStatus = history;
                }
                else
                {
                    if (lastStatus.StatusId != history.StatusId.Value)
                    {
                        newCorrectStatusList.Add(history);
                        lastStatus = history;
                    }
                }
            }


            // Case 2 : delete from ECurrentStatus table if not exists in EmployeeServiceHistory table
            for (int i = dbStatusList.Count - 1; i >= 0; i--)
            {
                if (historyList.Any(x => x.DateEng == dbStatusList[i].FromDateEng
                    && x.StatusId == dbStatusList[i].CurrentStatus) == false)
                {
                    isHistoryChanged = true;
                    PayrollDataContext.ECurrentStatus.DeleteOnSubmit(dbStatusList[i]);
                    dbStatusList.Remove(dbStatusList[i]);
                }
            }

            // Secondly Insert/Update
            foreach (EmployeeServiceHistory history in newCorrectStatusList)
            {
                List<ECurrentStatus> statusList = dbStatusList.Where(x => x.FromDateEng == history.DateEng).ToList();
                if (statusList.Count > 0)
                {
                    foreach (ECurrentStatus status in statusList)
                    {
                        if (status.CurrentStatus != history.StatusId)
                        {
                            status.CurrentStatus = history.StatusId.Value;
                            isHistoryChanged = true;
                        }
                    }
                }
                else
                {
                    ECurrentStatus status = new ECurrentStatus();
                    status.EmployeeId = empId;
                    GetStatusChangeFromService(history, status);
                    PayrollDataContext.ECurrentStatus.InsertOnSubmit(status);
                    isHistoryChanged = true;
                }
            }

            // if any changes to ECurrentStatus like table then process to insert/update
            if (isHistoryChanged)
                UpdateChangeSet();
        }

        private static void ResetBranchDepartmentHistory(int empId, List<EmployeeServiceHistory> historyList)
        {
            bool isHistoryChanged = false;

            // Reset status list
            List<BranchDepartmentHistory> dbList = BLL.BaseBiz.PayrollDataContext.BranchDepartmentHistories.Where(x => x.EmployeeId == empId)
                .OrderByDescending(x => x.FromDateEng).ToList();

            List<EmployeeServiceHistory> newCorrectList = new List<EmployeeServiceHistory>();


            EmployeeServiceHistory last = null;
            // Case 1 : Create new correct list
            foreach (EmployeeServiceHistory history in historyList)
            {
                if (last == null)
                {
                    newCorrectList.Add(history);
                    last = history;
                }
                else
                {
                    if (last.BranchId != history.BranchId.Value || last.DepartmentId != history.DepartmentId.Value)
                    {
                        newCorrectList.Add(history);
                        history.FromBranchId = last.BranchId;
                        history.FromDepId = last.DepartmentId;
                        last = history;
                    }
                }
            }


            // Case 2 : delete from ECurrentStatus table if not exists in EmployeeServiceHistory table
            for (int i = dbList.Count - 1; i >= 0; i--)
            {
                if (historyList.Any(x => x.DateEng == dbList[i].FromDateEng
                    && x.BranchId == dbList[i].BranchId && x.DepartmentId == dbList[i].DepeartmentId) == false)
                {
                    isHistoryChanged = true;
                    PayrollDataContext.BranchDepartmentHistories.DeleteOnSubmit(dbList[i]);
                    dbList.Remove(dbList[i]);
                }
            }

            // Secondly Insert/Update
            foreach (EmployeeServiceHistory history in newCorrectList)
            {
                List<BranchDepartmentHistory> statusList = dbList.Where(x => x.FromDateEng == history.DateEng).ToList();
                if (statusList.Count > 0)
                {
                    foreach (BranchDepartmentHistory status in statusList)
                    {
                        if (status.BranchId != history.BranchId || status.DepeartmentId != history.DepartmentId)
                        {
                            status.DepeartmentId = history.DepartmentId.Value;
                            status.BranchId = history.BranchId.Value;
                            status.FromBranchId = history.FromBranchId;
                            status.FromDepartmentId = history.FromDepId;
                            isHistoryChanged = true;
                        }
                    }
                }
                else
                {
                    BranchDepartmentHistory status = new BranchDepartmentHistory();
                    status.EmployeeId = empId;
                    GetBranchHistoryFromService(history, status);
                    status.FromBranchId = history.FromBranchId;
                    status.FromDepartmentId = history.FromDepId;
                    PayrollDataContext.BranchDepartmentHistories.InsertOnSubmit(status);
                    isHistoryChanged = true;
                }
            }

            // if any changes to ECurrentStatus like table then process to insert/update
            if (isHistoryChanged)
                UpdateChangeSet();
        }

        private static void ResetDesignationHistory(int empId, List<EmployeeServiceHistory> historyList)
        {
            bool isHistoryChanged = false;

            // Reset status list
            List<DesignationHistory> dbList = BLL.BaseBiz.PayrollDataContext.DesignationHistories.Where(x => x.EmployeeId == empId)
                .OrderByDescending(x => x.FromDateEng).ToList();

            List<EmployeeServiceHistory> newCorrectList = new List<EmployeeServiceHistory>();


            EmployeeServiceHistory last = null;
            // Case 1 : Create new correct list
            foreach (EmployeeServiceHistory history in historyList)
            {
                if (last == null)
                {
                    newCorrectList.Add(history);
                    last = history;
                }
                else
                {
                    if (last.DesignationId != history.DesignationId)
                    {
                        newCorrectList.Add(history);
                        last = history;
                    }
                }
            }


            // Case 2 : delete from ECurrentStatus table if not exists in EmployeeServiceHistory table
            for (int i = dbList.Count - 1; i >= 0; i--)
            {
                if (historyList.Any(x => x.DateEng == dbList[i].FromDateEng
                    && x.DesignationId == dbList[i].DesignationId) == false)
                {
                    isHistoryChanged = true;
                    PayrollDataContext.DesignationHistories.DeleteOnSubmit(dbList[i]);
                    dbList.Remove(dbList[i]);
                }
            }

            // Secondly Insert/Update
            foreach (EmployeeServiceHistory history in newCorrectList)
            {
                List<DesignationHistory> statusList = dbList.Where(x => x.FromDateEng == history.DateEng).ToList();
                if (statusList.Count > 0)
                {
                    foreach (DesignationHistory status in statusList)
                    {
                        if (status.DesignationId != history.DesignationId)
                        {
                            status.DesignationId = history.DesignationId.Value;
                            isHistoryChanged = true;
                        }
                    }
                }
                else
                {
                    DesignationHistory status = new DesignationHistory();
                    status.EmployeeId = empId;
                    GetDesignationHistoryFromServie(history, status);
                    PayrollDataContext.DesignationHistories.InsertOnSubmit(status);
                    isHistoryChanged = true;
                }
            }

            // if any changes to ECurrentStatus like table then process to insert/update
            if (isHistoryChanged)
                UpdateChangeSet();
        }

        public static bool IsSaveEventExistsThenShowWarning(EmployeeServiceHistory ent)
        {
            if (PayrollDataContext.EmployeeServiceHistories.Any(
                x => x.EmployeeId == ent.EmployeeId && x.DateEng == ent.DateEng && x.EventID == ent.EventID))
            {
                return true;
            }
            return false;
        }

        public static bool ImportServiceHistory(List<EmployeeServiceHistory> list)
        {

            SetConnectionPwd(PayrollDataContext.Connection);
            PayrollDataContext.Transaction = PayrollDataContext.Connection.BeginTransaction();

            try
            {
                foreach (EmployeeServiceHistory entity in list)
                {

                    //EmployeeServiceHistory dbEntity = PayrollDataContext.EmployeeServiceHistories
                    //    .Where(g => g.EventSourceID == entity.EventSourceID)
                    //    .SingleOrDefault();

                    bool isBranchDepChanged = false;
                    bool isStatusChanged = false;
                    bool isDesignationChanged = false;

                    GetEmployeeCurrentBranchDepartmentResult branchDep =
                     PayrollDataContext.GetEmployeeCurrentBranchDepartment(entity.DateEng, entity.EmployeeId)
                     .FirstOrDefault();

                    int branchIdByDate = branchDep.BranchId.Value;
                    int departmentIdByDate = branchDep.DepartmentId.Value;
                    int statusIdByDate = PayrollDataContext.GetEmployeeStatusForDate(entity.DateEng, entity.EmployeeId).Value;
                    int designationIdByDate = PayrollDataContext.GetEmployeeCurrentPositionForDate(entity.DateEng, entity.EmployeeId).Value;

                    if (entity.BranchId == null)
                        entity.BranchId = branchDep.BranchId;
                    if (entity.DepartmentId == null)
                        entity.DepartmentId = branchDep.DepartmentId;
                    if (entity.StatusId == null)
                        entity.StatusId = statusIdByDate;
                    if (entity.DesignationId == null)
                        entity.DesignationId = designationIdByDate;


                    if (branchIdByDate != entity.BranchId || departmentIdByDate != entity.DepartmentId)
                        isBranchDepChanged = true;
                    if (statusIdByDate != entity.StatusId)
                        isStatusChanged = true;
                    if (designationIdByDate != entity.DesignationId)
                        isDesignationChanged = true;


                    entity.CreatedBy = SessionManager.CurrentLoggedInUserID;
                    entity.CreatedOn = DateTime.Now;
                    entity.ModifiedBy = entity.CreatedBy;
                    entity.ModifiedOn = entity.CreatedOn;
                    entity.PayrollPeriodId = CommonManager.GetLastPayrollPeriod().PayrollPeriodId;
                    PayrollDataContext.EmployeeServiceHistories.InsertOnSubmit(entity);
                    PayrollDataContext.SubmitChanges();



                    // Process to insert or update respective tables 
                    // Assign lastest from history as insert can be done for past month also
                    if (isBranchDepChanged)
                    {
                        BranchDepartmentHistory latestHistory = new BranchDepartmentHistory();
                        latestHistory.EmployeeId = entity.EmployeeId;
                        latestHistory.IsFirst = false;
                        latestHistory.EventSourceID = entity.EventSourceID;
                        latestHistory.FromBranchId = branchIdByDate;
                        latestHistory.FromDepartmentId = departmentIdByDate;
                        GetBranchHistoryFromService(entity, latestHistory);
                        PayrollDataContext.BranchDepartmentHistories.InsertOnSubmit(latestHistory);
                    }

                    if (isDesignationChanged)
                    {
                        DesignationHistory desig = new DesignationHistory();
                        desig.EmployeeId = entity.EmployeeId;
                        desig.IsFirst = false;
                        desig.EventSourceID = entity.EventSourceID;
                        desig.FromDesignationId = designationIdByDate;
                        GetDesignationHistoryFromServie(entity, desig);
                        PayrollDataContext.DesignationHistories.InsertOnSubmit(desig);
                    }

                    if (isStatusChanged)
                    {
                        ECurrentStatus status = new ECurrentStatus();
                        status.EmployeeId = entity.EmployeeId;
                        status.EventSourceID = entity.EventSourceID;
                        GetStatusChangeFromService(entity, status);
                        PayrollDataContext.ECurrentStatus.InsertOnSubmit(status);
                    }

                    // call to update in respective table
                    UpdateChangeSet();



                    SetLatestBranchDepDesignationHistoryToEmployee(entity.EmployeeId);
                }



                PayrollDataContext.Transaction.Commit();
            }
            catch (Exception exp)
            {
                Log.log("Error while importing service history : ", exp);
                PayrollDataContext.Transaction.Rollback();
                return false;
            }

            return true;

        }

        private static void SetLatestBranchDepDesignationHistoryToEmployee(int empId)
        {
            EEmployee emp = EmployeeManager.GetEmployeeById(empId);

            // update latest in EEmployee table also
            BranchDepartmentHistory latestBranch = PayrollDataContext.BranchDepartmentHistories.Where(x => x.EmployeeId == empId)
                .OrderByDescending(x => x.FromDateEng).FirstOrDefault();
            if (latestBranch != null)
            {
                emp.BranchId = latestBranch.BranchId;
                emp.DepartmentId = latestBranch.DepeartmentId;
            }

            DesignationHistory latestDesig = PayrollDataContext.DesignationHistories.Where(x => x.EmployeeId == empId)
                .OrderByDescending(x => x.FromDateEng).FirstOrDefault();
            if (latestDesig != null)
            {
                emp.DesignationId = latestDesig.DesignationId;
            }

            UpdateChangeSet();
        }

        private static void GetStatusChangeFromService(EmployeeServiceHistory entity, ECurrentStatus status)
        {
            status.CurrentStatus = entity.StatusId.Value;
            status.FromDate = entity.Date;
            status.FromDateEng = entity.DateEng;
            status.EventID = entity.EventID;
            status.Note = entity.Notes;

            if (!string.IsNullOrEmpty(entity.ToDate))
            {
                status.DefineToDate = true;
                status.ToDate = entity.ToDate;
                status.ToDateEng = entity.ToDateEng;
            }
            else
            {
                status.DefineToDate = false;
                status.ToDate = entity.ToDate;
                status.ToDateEng = entity.ToDateEng;
            }
        }

        private static void GetDesignationHistoryFromServie(EmployeeServiceHistory entity, DesignationHistory desig)
        {
            desig.DesignationId = entity.DesignationId;
            desig.FromDate = entity.Date;
            desig.FromDateEng = entity.DateEng;
            desig.EventID = entity.EventID;
            desig.Note = entity.Notes;
        }

        private static void GetBranchHistoryFromService(EmployeeServiceHistory entity, BranchDepartmentHistory latestHistory)
        {
            latestHistory.BranchId = entity.BranchId;
            latestHistory.DepeartmentId = entity.DepartmentId;
            latestHistory.SubDepartmentId = entity.SubDepartmentId;
            latestHistory.FromDate = entity.Date;
            latestHistory.FromDateEng = entity.DateEng;
            latestHistory.EventID = entity.EventID;
            latestHistory.Note = entity.Notes;
            latestHistory.LetterDate = entity.LetterDate;
            latestHistory.LetterDateEng = entity.LetterDateEng;
            latestHistory.LetterNumber = entity.LetterNo;
        }
        public static void UpdateDesignationHistory(DesignationHistory entity)
        {
            DesignationHistory dbEntity = PayrollDataContext.DesignationHistories
                .Where(g => g.EmployeeDesignationId == entity.EmployeeDesignationId)
                .SingleOrDefault();

            if (dbEntity == null)
                return;

            dbEntity.FromDate = entity.FromDate;
            dbEntity.FromDateEng = entity.FromDateEng;
            dbEntity.DesignationId = entity.DesignationId;
            dbEntity.Note = entity.Note;

            dbEntity.ModifiedOn = DateTime.Now;
            dbEntity.ModifiedBy = SessionManager.CurrentLoggedInUserID;


            //if only one row then update GradeI d in EEmployee table also
            //if (dbEntity.IsFirst.Value)
            {
                EEmployee employee = PayrollDataContext.EEmployees
                    .Where(e => e.EmployeeId == entity.EmployeeId)
                    .SingleOrDefault();
                employee.DesignationId = entity.DesignationId;
            }

            UpdateChangeSet();
        }
        #endregion

        #region "Designation"

        public EDesignation GetDesignationById(int designationId)
        {
            return PayrollDataContext.EDesignations.SingleOrDefault
                (d => d.DesignationId == designationId);
        }

        public static int GetLevelIDForDesignation(int designationId)
        {
            return
                (
                from d in PayrollDataContext.EDesignations
                where d.DesignationId == designationId
                select d.LevelId
                ).FirstOrDefault().Value;
        }
        public Bank GetBankById(int bankId)
        {
            return PayrollDataContext.Banks.SingleOrDefault
                (d => d.BankID == bankId);
        }
        public BankBranch GetBankBranchById(int bankId)
        {
            return PayrollDataContext.BankBranches.SingleOrDefault
                (d => d.BankBranchID == bankId);
        }
        public VoucherHPLDepartment GetVoucherDepById(int id)
        {
            return PayrollDataContext.VoucherHPLDepartments.SingleOrDefault
                (d => d.VoucherDepartmentID == id);
        }
        public List<EDesignation> GetAllDesignations()
        {

            List<EDesignation> list = null;

            list = GetFromCache<List<EDesignation>>("GetAllDesignations");
            if (list != null)
                return list;


            list = PayrollDataContext.EDesignations
                .OrderBy(e => e.Name).ToList();

            SaveToCache<List<EDesignation>>("GetAllDesignations", list);

            return list;
        }

        public List<EDesignationBO> GetAllDesignationsNew()
        {

            List<EDesignationBO> list = null;

            list = GetFromCache<List<EDesignationBO>>("GetAllDesignationsNew");
            if (list != null)
                return list;


            list =
                (
                from d in PayrollDataContext.EDesignations
                join l in PayrollDataContext.BLevels on d.LevelId equals l.LevelId
                select new EDesignationBO
                {
                    DesignationId = d.DesignationId,
                    Name = d.Name,
                    LevelAndDesignation = l.Name + " - " + d.Name,
                    LevelId = l.LevelId
                }
                ).ToList();


            SaveToCache<List<EDesignationBO>>("GetAllDesignationsNew", list);

            return list;
        }
        /// <summary>
        /// Return the Level name as for NIBL it will be "Position"
        /// </summary>
        /// <returns></returns>
        public static string GetLevelName
        {
            get
            {
                if (CommonManager.CompanySetting.WhichCompany == WhichCompany.NIBL)
                    return "Position";
                return "Level";
            }
        }
        public static string GetHandicappedName
        {
            get
            {
                if (CommonManager.CompanySetting.WhichCompany == WhichCompany.Care)
                    return "Differently Abled";
                return "Handicapped";
            }
        }
        public static string GetGenderName
        {
            get
            {
                if (CommonManager.CompanySetting.WhichCompany == WhichCompany.Care)
                    return "Sex";
                return "Gender";
            }
        }
        public static bool DesignationNameAlreadyExists(string designationName, int designationID)
        {
            return
                PayrollDataContext.EDesignations.Any(x => x.DesignationId != designationID
                    && x.Name.ToLower() == designationName.Trim().ToLower());
        }

        public static bool BankNameAlreadyExists(string name, int id)
        {
            return
                PayrollDataContext.Banks.Any(x => x.BankID != id
                    && x.Name.ToLower() == name.Trim().ToLower());
        }
        public static bool VoucherDepNameAlreadyExists(string name, int id)
        {
            return
                PayrollDataContext.VoucherHPLDepartments.Any(x => x.VoucherDepartmentID != id
                    && x.Name.ToLower() == name.Trim().ToLower());
        }
        public bool Save(EDesignation entity)
        {
            int order = 1;

            EDesignation lastOrderDesg = PayrollDataContext.EDesignations.OrderByDescending(x => x.Order).FirstOrDefault();
            if (lastOrderDesg != null && lastOrderDesg.Order != null)
            {
                order = lastOrderDesg.Order.Value + 1;
            }
            entity.Order = order;
            PayrollDataContext.EDesignations.InsertOnSubmit(entity);
            return SaveChangeSet();
        }
        public bool Save(Bank entity)
        {
            PayrollDataContext.Banks.InsertOnSubmit(entity);
            return SaveChangeSet();
        }
        public bool Save(BankBranch entity)
        {
            PayrollDataContext.BankBranches.InsertOnSubmit(entity);
            return SaveChangeSet();
        }
        public bool Save(VoucherHPLDepartment entity)
        {
            PayrollDataContext.VoucherHPLDepartments.InsertOnSubmit(entity);
            return SaveChangeSet();
        }
        public bool Update(EDesignation entity)
        {
            EDesignation dbEntity = PayrollDataContext.EDesignations.SingleOrDefault
                (d => d.DesignationId == entity.DesignationId);
            if (dbEntity == null)
                return false;

            dbEntity.Name = entity.Name;
            dbEntity.Code = entity.Code;
            //dbEntity.Order = entity.Order;
            dbEntity.LevelId = entity.LevelId;
            dbEntity.GroupId = entity.GroupId;
            dbEntity.CategoryId = entity.CategoryId;
            UpdateChangeSet();
            return true;
        }
        public bool Update(Bank entity)
        {

            Bank dbEntity = PayrollDataContext.Banks.SingleOrDefault
                (d => d.BankID == entity.BankID);


            if (dbEntity == null)
                return false;

            dbEntity.Name = entity.Name;
            dbEntity.ACNumber = entity.ACNumber;

            return UpdateChangeSet();
        }
        public bool Update(BankBranch entity)
        {

            BankBranch dbEntity = PayrollDataContext.BankBranches.SingleOrDefault
                (d => d.BankBranchID == entity.BankID);


            if (dbEntity == null)
                return false;

            dbEntity.Name = entity.Name;
            return UpdateChangeSet();
        }
        public bool Update(VoucherHPLDepartment entity)
        {

            VoucherHPLDepartment dbEntity = PayrollDataContext.VoucherHPLDepartments.SingleOrDefault
                (d => d.VoucherDepartmentID == entity.VoucherDepartmentID);


            if (dbEntity == null)
                return false;

            dbEntity.Name = entity.Name;
            dbEntity.Location = entity.Location;


            return UpdateChangeSet();
        }
        public static List<VoucherHPLDepartment> GetVoucherGroupsForEmp()
        {
            return PayrollDataContext.VoucherHPLDepartments.OrderBy(x => x.Name).ToList();
        }
        public bool Delete(EDesignation entity)
        {
            return PayrollDataContext.DeleteDesignation(entity.DesignationId) == 1;
        }
        public bool Delete(Bank entity)
        {
            return PayrollDataContext.DeleteBank(entity.BankID) == 1;
        }

        public bool Delete(BankBranch entity)
        {
            return true;
        }

        public bool Delete(VoucherHPLDepartment entity)
        {

            VoucherHPLDepartment db = PayrollDataContext.VoucherHPLDepartments.SingleOrDefault(x
                => x.VoucherDepartmentID == entity.VoucherDepartmentID);
            PayrollDataContext.VoucherHPLDepartments.DeleteOnSubmit(db);
            PayrollDataContext.SubmitChanges();
            return true;

        }
        #endregion
        #region "Tier"

        public ETier GetTierById(int tierId)
        {
            return PayrollDataContext.ETiers.SingleOrDefault
                (d => d.TierId == tierId);
        }

        public List<ETier> GetAllTiers()
        {

            List<ETier> list = null;

            list = GetFromCache<List<ETier>>("GetAllTiers");
            if (list != null)
                return list;


            list = PayrollDataContext.ETiers.Where(e => e.CompanyId == SessionManager.CurrentCompanyId)
                .OrderBy(e => e.Name).ToList();

            SaveToCache<List<ETier>>("GetAllTiers", list);

            return list;
        }


        public bool Save(ETier entity)
        {
            PayrollDataContext.ETiers.InsertOnSubmit(entity);
            return SaveChangeSet();
        }
        public bool Update(ETier entity)
        {
            ETier dbEntity = PayrollDataContext.ETiers.SingleOrDefault
                (d => d.TierId == entity.TierId);
            if (dbEntity == null)
                return false;

            dbEntity.Name = entity.Name;


            return UpdateChangeSet();
        }

        public bool Delete(ETier entity)
        {
            //if (PayrollDataContext.EEmployees.Any(x => x.TierId == entity.TierId))
            //    return false;
            //ETier dbEntity = PayrollDataContext.ETiers.SingleOrDefault(x => x.TierId == entity.TierId);
            //PayrollDataContext.ETiers.DeleteOnSubmit(dbEntity);
            //PayrollDataContext.SubmitChanges();
            return true;
        }

        #endregion

        #region "SkillSet"

        public static void AddEditSkillSetToEmployee(SkillSetEmployee entity)
        {
            SkillSetEmployee dbEntity = PayrollDataContext.SkillSetEmployees
                .SingleOrDefault(x => x.EmployeeId == entity.EmployeeId && x.SkillSetId == entity.SkillSetId);

            if (dbEntity == null)
            {
                PayrollDataContext.SkillSetEmployees.InsertOnSubmit(entity);
            }
            else
            {
                dbEntity.LevelID = entity.LevelID;
            }

            PayrollDataContext.SubmitChanges();
        }
        public static SkillSetEmployee GetEmployeeSkillSetId(int skillSetId, int employeeId)
        {
            SkillSetEmployee dbEntity = PayrollDataContext.SkillSetEmployees
                .SingleOrDefault(x => x.EmployeeId == employeeId && x.SkillSetId == skillSetId);
            return dbEntity;
        }
        public static void DeleteSkillSetToEmployee(SkillSetEmployee entity)
        {
            SkillSetEmployee dbEntity = PayrollDataContext.SkillSetEmployees
               .SingleOrDefault(x => x.EmployeeId == entity.EmployeeId && x.SkillSetId == entity.SkillSetId);

            if (dbEntity != null)
            {
                PayrollDataContext.SkillSetEmployees.DeleteOnSubmit(dbEntity);
            }
            PayrollDataContext.SubmitChanges();
        }

        public static string GetEmployeeSkillSetHTML(int employeeId)
        {
            List<EmployeeSkillSetBO> source = GetEmployeesSkillSet(employeeId);
            StringBuilder str = new StringBuilder();
            bool first = true;
            foreach (EmployeeSkillSetBO obj in source)
            {
                //if (first == false)
                //    str.Append("<br />");
                str.Append("<li>" + obj.SkillSetName + "</li>");
                first = false;
            }

            return str.ToString();
        }

        public static string GetEmployeeSkillSetHTMLForReport(int employeeId)
        {
            List<EmployeeSkillSetBO> source = GetEmployeesSkillSet(employeeId);
            StringBuilder str = new StringBuilder();
            bool first = true;
            foreach (EmployeeSkillSetBO obj in source)
            {
                //if (first == false)
                //    str.Append("<br />");
                str.Append(obj.SkillSetName + "\n");
                first = false;
            }

            return str.ToString();
        }
        public static List<EmployeeSkillSetBO> GetEmployeesSkillSet(int employeeId)
        {
            List<EmployeeSkillSetBO> list =
                (
                from se in PayrollDataContext.SkillSetEmployees
                join s in PayrollDataContext.SkillSets on se.SkillSetId equals s.SkillSetId

                join l in PayrollDataContext.SkillSetCompetencyLevels on se.LevelID equals l.LevelID into dd
                from ddd in dd.DefaultIfEmpty()

                orderby s.Name
                where se.EmployeeId == employeeId
                select new EmployeeSkillSetBO
                {
                    EmployeeId = employeeId,
                    SkillSetId = s.SkillSetId,
                    SkillSetName = s.Name + (ddd.Name == null ? "" : " - " + ddd.Name)
                }
                ).ToList();


            return list;
        }

        public SkillSet GetSkillSetId(int skillSetId)
        {
            return PayrollDataContext.SkillSets.SingleOrDefault
                (d => d.SkillSetId == skillSetId);
        }
        public SkillSetCompetencyLevel GetSkillSetLevelId(int levelId)
        {
            return PayrollDataContext.SkillSetCompetencyLevels.SingleOrDefault
                (d => d.LevelID == levelId);
        }
        public List<SkillSet> GetAllSkillSet()
        {

            List<SkillSet> list = null;




            list = PayrollDataContext.SkillSets
                .OrderBy(e => e.Name).ToList();



            return list;
        }

        public bool Save(SkillSet entity)
        {
            PayrollDataContext.SkillSets.InsertOnSubmit(entity);
            return SaveChangeSet();
        }
        public bool Update(SkillSet entity)
        {
            SkillSet dbEntity = PayrollDataContext.SkillSets.SingleOrDefault
                (d => d.SkillSetId == entity.SkillSetId);
            if (dbEntity == null)
                return false;

            dbEntity.Name = entity.Name;


            return UpdateChangeSet();
        }

        public bool Delete(SkillSet entity)
        {
            if (PayrollDataContext.SkillSetEmployees.Any(x => x.SkillSetId == entity.SkillSetId))
                return false;
            SkillSet dbEntity = PayrollDataContext.SkillSets.SingleOrDefault(x => x.SkillSetId == entity.SkillSetId);

            if (entity != null)
                PayrollDataContext.SkillSets.DeleteOnSubmit(dbEntity);
            PayrollDataContext.SubmitChanges();
            return true;
        }

        public static void UpdateSkillSet(List<SkillSetEmployee> list, int employeeId)
        {


            PayrollDataContext.SkillSetEmployees.DeleteAllOnSubmit(
                   PayrollDataContext.SkillSetEmployees.Where(x => x.EmployeeId == employeeId));

            PayrollDataContext.SkillSetEmployees.InsertAllOnSubmit(list);

            //PayrollDataContext.SubmitChanges();
        }

        public static List<SkillSetEmployee> GetEmployeeSkillSet(int employeeId)
        {
            return
                PayrollDataContext.SkillSetEmployees.Where(x => x.EmployeeId == employeeId).ToList();
        }

        public static List<SkillSetCompetencyLevel> GetCompetencyLevels()
        {
            return PayrollDataContext.SkillSetCompetencyLevels.OrderBy(x => x.Order).ToList();
        }



        public bool Save(SkillSetCompetencyLevel entity)
        {
            PayrollDataContext.SkillSetCompetencyLevels.InsertOnSubmit(entity);
            return SaveChangeSet();
        }
        public bool Update(SkillSetCompetencyLevel entity)
        {
            SkillSetCompetencyLevel dbEntity = PayrollDataContext.SkillSetCompetencyLevels.SingleOrDefault
                (d => d.LevelID == entity.LevelID);
            if (dbEntity == null)
                return false;

            dbEntity.Name = entity.Name;
            dbEntity.Order = entity.Order;

            return UpdateChangeSet();
        }

        public bool Delete(SkillSetCompetencyLevel entity)
        {
            if (PayrollDataContext.SkillSetEmployees.Any(x => x.LevelID == entity.LevelID))
                return false;

            SkillSetCompetencyLevel dbEntity = PayrollDataContext.SkillSetCompetencyLevels.SingleOrDefault(x => x.LevelID == entity.LevelID);

            if (entity != null)
                PayrollDataContext.SkillSetCompetencyLevels.DeleteOnSubmit(dbEntity);
            PayrollDataContext.SubmitChanges();
            return true;
        }

        #endregion

        #region "Qualification"

        public List<QualificationList> GetAllQualifications()
        {
            return PayrollDataContext.QualificationLists.ToList();
        }
        #endregion

        #region "Grade"

        public static EGrade GetGradeById(int gradeId)
        {
            return PayrollDataContext.EGrades.Where
                (e => e.GradeId == gradeId).SingleOrDefault();
        }

        public List<EGrade> GetAllGrades()
        {

            List<EGrade> list = null;

            list = GetFromCache<List<EGrade>>("GetAllGrades");
            if (list != null)
                return list;


            list = PayrollDataContext.EGrades//.Where(e => e.CompanyId == SessionManager.CurrentCompanyId)
               .OrderBy(e => e.Name).ToList();

            SaveToCache<List<EGrade>>("GetAllGrades", list);

            return list;



        }

        public bool Save(EGrade entity)
        {
            PayrollDataContext.EGrades.InsertOnSubmit(entity);
            return SaveChangeSet();
        }
        public bool Update(EGrade entity)
        {
            EGrade dbEntity = PayrollDataContext.EGrades.SingleOrDefault
                (d => d.GradeId == entity.GradeId);
            if (dbEntity == null)
                return false;

            dbEntity.Name = entity.Name;

            return UpdateChangeSet();
        }

        public bool Delete(EGrade entity)
        {
            return PayrollDataContext.DeleteGrade(entity.GradeId) == 1;
        }





        #endregion

        #region "Castes"


        public List<HCaste> GetAllCastes()
        {
            return PayrollDataContext.HCastes.ToList();
        }
        public static List<HolidayGroup> GetAllHolidayGroups()
        {
            return PayrollDataContext.HolidayGroups.ToList();
        }
        public static List<UnitList> GetAllUnitList()
        {
            return PayrollDataContext.UnitLists.OrderBy(x => x.Name).ToList();
        }
        public static HCaste GetCastesById(int casteId)
        {
            return PayrollDataContext.HCastes.Where(c => c.CasteId == casteId).SingleOrDefault();
        }

        public static bool IsUnitEnabled
        {
            get
            {
                return PayrollDataContext.UnitLists.Any();
            }
        }

        public bool Save(HCaste entity)
        {
            PayrollDataContext.HCastes.InsertOnSubmit(entity);
            return SaveChangeSet();
        }
        public bool Update(HCaste entity)
        {
            HCaste dbEntity = PayrollDataContext.HCastes.SingleOrDefault
                (d => d.CasteId == entity.CasteId);
            if (dbEntity == null)
                return false;

            dbEntity.CasteName = entity.CasteName;

            return UpdateChangeSet();
        }

        public bool Delete(HCaste entity)
        {
            return PayrollDataContext.DeleteCaste(entity.CasteId) == 1;
        }





        #endregion

        #region "Location"

        public ELocation GetLocationById(int LocationId)
        {
            return PayrollDataContext.ELocations.SingleOrDefault
                (d => d.LocationId == LocationId);
        }

        public List<ELocation> GetAllLocations()
        {

            List<ELocation> list = null;

            list = GetFromCache<List<ELocation>>("ELocation");
            if (list != null)
                return list;


            list = PayrollDataContext.ELocations
                .OrderBy(e => e.Name).ToList();

            SaveToCache<List<ELocation>>("ELocation", list);

            return list;


        }

        public bool Save(ELocation entity)
        {
            PayrollDataContext.ELocations.InsertOnSubmit(entity);
            return SaveChangeSet();
        }
        public bool Update(ELocation entity)
        {
            ELocation dbEntity = PayrollDataContext.ELocations.SingleOrDefault
                (d => d.LocationId == entity.LocationId);
            if (dbEntity == null)
                return false;

            dbEntity.Name = entity.Name;

            return UpdateChangeSet();
        }

        public bool Delete(ELocation entity)
        {
            return PayrollDataContext.DeleteLocation(entity.LocationId) == 1;
        }

        #endregion

        #region "Programme"

        public EProgramme GetProgrammeById(int ProgrammeId)
        {
            return PayrollDataContext.EProgrammes.SingleOrDefault
                (d => d.ProgrammeId == ProgrammeId);
        }

        public List<EProgramme> GetAllProgrammes()
        {

            List<EProgramme> list = null;

            list = GetFromCache<List<EProgramme>>("EProgramme");
            if (list != null)
                return list;


            list = PayrollDataContext.EProgrammes.Where(e => e.CompanyId == SessionManager.CurrentCompanyId)
                .OrderBy(e => e.Name).ToList();

            SaveToCache<List<EProgramme>>("EProgramme", list);

            return list;


        }

        public bool Save(EProgramme entity)
        {
            PayrollDataContext.EProgrammes.InsertOnSubmit(entity);
            return SaveChangeSet();
        }
        public bool Update(EProgramme entity)
        {
            EProgramme dbEntity = PayrollDataContext.EProgrammes.SingleOrDefault
                (d => d.ProgrammeId == entity.ProgrammeId);
            if (dbEntity == null)
                return false;

            dbEntity.Name = entity.Name;

            return UpdateChangeSet();
        }

        public bool Delete(EProgramme entity)
        {
            return PayrollDataContext.DeleteProgramme(entity.ProgrammeId) == 1;
        }

        #endregion

        #region "Workshift"


        public List<EWorkShift> GetAllWorkShift()
        {

            List<EWorkShift> list = null;

            list = GetFromCache<List<EWorkShift>>("GetAllWorkShift");
            if (list != null)
                return list;


            list = PayrollDataContext.EWorkShifts.Where(e => e.CompanyId == SessionManager.CurrentCompanyId)
               .OrderBy(e => e.Name).ToList();

            SaveToCache<List<EWorkShift>>("GetAllWorkShift", list);

            return list;


        }

        public bool Save(EWorkShift entity)
        {
            PayrollDataContext.EWorkShifts.InsertOnSubmit(entity);
            return SaveChangeSet();
        }
        public bool Update(EWorkShift entity)
        {
            EWorkShift dbEntity = PayrollDataContext.EWorkShifts.SingleOrDefault
                (d => d.WorkShiftId == entity.WorkShiftId);
            if (dbEntity == null)
                return false;

            dbEntity.Name = entity.Name;

            return UpdateChangeSet();
        }

        public bool Delete(EWorkShift entity)
        {
            return PayrollDataContext.DeleteWorkShift(entity.WorkShiftId) == 1;
        }


        #endregion

        #region "CostCodes"



        public static List<ECostCode> GetAllCostCodes()
        {

            List<ECostCode> list = null;

            list = GetFromCache<List<ECostCode>>("ECostCode");
            if (list != null)
                return list;


            list = PayrollDataContext.ECostCodes.Where(e => e.CompanyId == SessionManager.CurrentCompanyId)
               .OrderBy(e => e.Name).ToList();

            SaveToCache<List<ECostCode>>("ECostCode", list);

            return list;


        }

        public bool Save(ECostCode entity)
        {
            PayrollDataContext.ECostCodes.InsertOnSubmit(entity);
            return SaveChangeSet();
        }
        public bool Update(ECostCode entity)
        {
            ECostCode dbEntity = PayrollDataContext.ECostCodes.SingleOrDefault
                (d => d.CostCodeId == entity.CostCodeId);
            if (dbEntity == null)
                return false;

            dbEntity.Name = entity.Name;

            return UpdateChangeSet();
        }

        public bool Delete(ECostCode entity)
        {
            return PayrollDataContext.DeleteCostCode(entity.CostCodeId) == 1;
        }


        #endregion

        #region "Service provider"

        public ServiceProvider GetServiceProvider()
        {
            List<ServiceProvider> list = PayrollDataContext.ServiceProviders.ToList();
            if (list.Count > 0)
                return list[0];
            return null;
        }

        public bool Save(ServiceProvider entity)
        {
            PayrollDataContext.ServiceProviders.InsertOnSubmit(entity);
            return SaveChangeSet();
        }

        public bool Update(ServiceProvider entity)
        {
            ServiceProvider dbEntity = PayrollDataContext.ServiceProviders.SingleOrDefault(
                e => e.Id == entity.Id);
            if (dbEntity == null)
                return false;

            dbEntity.CompanyName = entity.CompanyName;
            dbEntity.Address = entity.Address;
            dbEntity.VatPanNo = entity.VatPanNo;
            dbEntity.FaxNo = entity.FaxNo;
            dbEntity.PhoneNo = entity.PhoneNo;
            dbEntity.Email = entity.Email;
            dbEntity.Website = entity.Website;

            return UpdateChangeSet();
        }


        #endregion


        #region "Remote area"

        public string GetRemoteAreaValue(int vdcId)
        {

            VDCList vdc = PayrollDataContext.VDCLists.SingleOrDefault(e => e.VDCId == vdcId);

            if (vdc == null)
                return "";
            return vdc.RemoteArea.Value;
        }

        #endregion



        public List<IRO> GetAllIRO()
        {
            return PayrollDataContext.IROs.ToList();
        }
        public List<IRO_TSO> GetAllTSO()
        {
            return PayrollDataContext.IRO_TSOs.ToList();
        }

        public static List<IRO_TSO> GetTSOByIROId(int IROId)
        {
            return PayrollDataContext.IRO_TSOs
                .Where(t => t.IROId == IROId)
                .OrderBy(t => t.Name)
                .ToList();
        }



        #region "Funds insert/update"

        public bool SaveOrUpdateFunds(PFRFFund pf)//, EmployeeInsurance ei,CitizenInventmentTrust cit)
        {
            using (TransactionScope scope = new TransactionScope())
            {
                //SaveOrUpdateCIT(cit);
                SaveOrUpdatePFRF(pf);
                //SaveOrUpdateEI(ei);             
                scope.Complete();
            }
            return true;
        }



        public bool SaveOrUpdatePFRF(PFRFFund entity)
        {
            if (entity == null)
                return false;

            PFRFFund dbEntity = PayrollDataContext.PFRFFunds.SingleOrDefault(
                c => c.CompanyId == entity.CompanyId);

            //PayrollDataContext.GetPFRFByCompanyId(entity.CompanyId).SingleOrDefault();
            if (dbEntity == null)
                PayrollDataContext.PFRFFunds.InsertOnSubmit(entity);
            else
            {
                dbEntity.PFRFNo = entity.PFRFNo;
                // dbEntity.IsMultipleGroup = entity.IsMultipleGroup;
                dbEntity.IsApproved = entity.IsApproved;
                dbEntity.ApprovalDate = entity.ApprovalDate;
                dbEntity.EmployeeContribution = entity.EmployeeContribution;
                dbEntity.CompanyContribution = entity.CompanyContribution;
            }

            PayrollDataContext.SubmitChanges();

            return true;
        }




        #endregion


        public static CalculationConstant CalculationConstant
        {
            get
            {
                CalculationConstant entity = GetFromCache<CalculationConstant>(CacheKey.CalculationConstant.ToString());
                if (entity != null)
                    return entity;


                entity = PayrollDataContext.CalculationConstants.ToList()[0];

                SaveToCache<CalculationConstant>(CacheKey.CalculationConstant.ToString(), entity);

                return entity;
            }
        }

        public List<HBloodGroup> GetAllBloodGroups()
        {
            return PayrollDataContext.HBloodGroups.ToList();
        }


        #region "Gratuity"


        public static void SaveGratuityRuleIncome(int incomeId)
        {
            GratuityRuleIncome entity = new GratuityRuleIncome();
            entity.IncomeId = incomeId;
            PayrollDataContext.GratuityRuleIncomes.InsertOnSubmit(entity);
            SaveChangeSet();
        }

        public static Bank GetFirstBank()
        {
            return PayrollDataContext.Banks.OrderBy(x => x.BankID).FirstOrDefault();
        }
        public static void DeleteAndSaveAllGratuityIncomes(string incomeList, List<GratuityStatus> statuList,
            bool fromStatusChange, bool gratuityStoppay, bool gratuityAbsent, bool gratuityUPL)
        {
            PayrollDataContext.DeleteAndSaveAllGratuityIncomes(incomeList, SessionManager.CurrentCompanyId);

            List<GratuityStatus> dbStatus = PayrollDataContext.GratuityStatus.ToList();
            PayrollDataContext.GratuityStatus.DeleteAllOnSubmit(dbStatus);
            PayrollDataContext.GratuityStatus.InsertAllOnSubmit(statuList);

            Setting setting = PayrollDataContext.Settings.FirstOrDefault();
            setting.GratuityFromStatusChange = fromStatusChange;
            setting.GratuityDeductStopPayment = gratuityStoppay;
            setting.GratuityDeductLWPUPL = gratuityUPL;
            setting.GratuityDeductAbsent = gratuityAbsent;

            CommonManager.Setting.GratuityFromStatusChange = fromStatusChange;
            CommonManager.Setting.GratuityDeductStopPayment = gratuityStoppay;
            CommonManager.Setting.GratuityDeductLWPUPL = gratuityUPL;
            CommonManager.Setting.GratuityDeductAbsent = gratuityAbsent;

            PayrollDataContext.SubmitChanges();

        }

        public static void DeleteAndSaveAllCITIncomes(List<int> incomeIDS)
        {


            List<CITIncome> incomeList = PayrollDataContext.CITIncomes.Where(c => c.CompanyId == SessionManager.CurrentCompanyId)
                .ToList();

            PayrollDataContext.CITIncomes.DeleteAllOnSubmit(incomeList);

            foreach (int incomeId in incomeIDS)
            {
                PayrollDataContext.CITIncomes.InsertOnSubmit(
                    new CITIncome { CompanyId = SessionManager.CurrentCompanyId, IncomeId = incomeId });
            }

            PayrollDataContext.SubmitChanges();

        }

        //public void SaveDeleteGratuityRule(int incomeId,bool saveAll,bool removeAll)
        //{
        //    if (saveAll)
        //    {
        //        PayrollDataContext.SaveAllDeleteAllGratuityIncome(SessionManager.CurrentCompanyId,
        //            saveAll, removeAll);
        //    }
        //    else if (removeAll)
        //    {
        //        PayrollDataContext.SaveAllDeleteAllGratuityIncome(SessionManager.CurrentCompanyId,
        //           saveAll, removeAll);
        //    }
        //    else
        //    {
        //        GratuityRuleIncome income = PayrollDataContext.GratuityRuleIncomes.
        //            SingleOrDefault(e => e.IncomeId == incomeId);

        //        if (income == null)
        //        {
        //            GratuityRuleIncome entity = new GratuityRuleIncome();
        //            entity.IncomeId = incomeId;
        //            PayrollDataContext.GratuityRuleIncomes.InsertOnSubmit(entity);
        //            SaveChangeSet();
        //        }
        //        else
        //        {

        //            PayrollDataContext.GratuityRuleIncomes.DeleteOnSubmit(income);
        //            PayrollDataContext.SubmitChanges();
        //        }
        //    }
        //}

        public static List<BonusEmployeeBO> GetBonusEmployee(int bonusId)
        {

            //(from p in PayrollDataContext.PayrollPeriods
            //                                  join c in PayrollDataContext.CCalculations on p.PayrollPeriodId equals c.PayrollPeriodId
            //                                  // Don't check here employee specific as Stopped Payment employee will not come
            //                                  join e in PayrollDataContext.CCalculationEmployees on c.CalculationId equals e.CalculationId


            return
                (
                from b in PayrollDataContext.BonusEmployees
                join e in PayrollDataContext.EEmployees on b.EmployeeId equals e.EmployeeId

                join lt in PayrollDataContext.PPays on e.EmployeeId equals lt.EmployeeId into temp
                from lt in temp.DefaultIfEmpty()

                where b.BonusId == bonusId
                orderby e.Name
                select new BonusEmployeeBO
                {
                    EmployeeId = e.EmployeeId,
                    Name = e.Name,
                    UnpaidLeave = b.UnpaidLeave,
                    OtherUneligibleDays = b.OtherUneligibleDays,
                    OtherUneligibleDaysReason = b.OtherUneligibleDaysReason,
                    AnnualSalary = b.AnnualSalary,
                    AccountNo = (lt == null ? "" : lt.BankACNo)
                }
                ).ToList();
        }

        public static List<BonusEmployeeBO> GetBonusList(int bonusId, int currentPage, int pageSize, ref int total, string searchEmp
            , int eligiblity)
        {
            searchEmp = searchEmp.ToLower();

            Bonus bonus = PayrollDataContext.Bonus.FirstOrDefault(x => x.BonusId == bonusId);
            AddOnHeader sstHeader = null;
            AddOnHeader tdsHeader = null;

            bool isFinalBonusWithEligibilityGroup = false;
            if (bonus.IsFinalBonus != null && bonus.IsFinalBonus.Value && eligiblity == 1)
                isFinalBonusWithEligibilityGroup = true;

            sstHeader = PayrollDataContext.AddOnHeaders.FirstOrDefault(x => x.AddOnId == bonus.AddOnId && x.Type == (int)
                 CalculationColumnType.SST);
            tdsHeader = PayrollDataContext.AddOnHeaders.FirstOrDefault(x => x.AddOnId == bonus.AddOnId && x.Type == (int)
                 CalculationColumnType.TDS);

            List<AddOnDetail> sstTDSList = PayrollDataContext.AddOnDetails.Where(
                x => x.AddOnHeaderId == (sstHeader == null ? 0 : sstHeader.AddOnHeaderId)
                || x.AddOnHeaderId == (tdsHeader == null ? 0 : tdsHeader.AddOnHeaderId)).ToList();

            List<BonusEmployeeBO> list =
                (
                from b in PayrollDataContext.BonusEmployees
                join e in PayrollDataContext.EEmployees on b.EmployeeId equals e.EmployeeId
                join h in PayrollDataContext.EHumanResources on e.EmployeeId equals h.EmployeeId
                join lt in PayrollDataContext.PPays on e.EmployeeId equals lt.EmployeeId into temp
                from lt in temp.DefaultIfEmpty()

                    //join sst in PayrollDataContext.AddOnDetails on (sstHeader == null ? 0 : sstHeader.AddOnHeaderId) equals sst.AddOnHeaderId

                    //   into temp1 from sst in temp1.DefaultIfEmpty()

                where b.BonusId == bonusId
                  && (searchEmp.Length == 0 || e.Name.ToLower().Contains(searchEmp))
                  &&
                  (
                     (
                       eligiblity == 1 && (b.Eligibility == (int)BonusEligiblity.Full || b.Eligibility == (int)BonusEligiblity.Proportionate)
                     )
                     ||
                     (eligiblity == 0 && b.Eligibility == (int)BonusEligiblity.NotEligible)

                    )
                orderby e.Name
                select new BonusEmployeeBO
                {
                    EmployeeId = e.EmployeeId,
                    AccountNo = (temp == null ? "" : lt.BankACNo),
                    BonusId = b.BonusId,
                    Name = e.Name,
                    Status = b.Status,
                    JoinDate = e.ECurrentStatus.OrderBy(x => x.FromDateEng).FirstOrDefault().FromDateEng,
                    RetirementDate =
                    (
                        e.IsRetired == false ? "" : h.DateOfRetirementEng.ToString()
                    ),
                    StdWorkDays = b.StdWorkDays,
                    UnpaidLeave = b.UnpaidLeave,
                    OtherUneligibleDays = b.OtherUneligibleDays,
                    OtherUneligibleDaysReason = b.OtherUneligibleDaysReason,
                    ActualWorkdays = b.ActualWorkdays,
                    Eligibility = b.Eligibility,
                    Proportion = b.Proportion,
                    AnnualSalary = b.AnnualSalary,
                    MonthlySalary = b.MonthlySalary,
                    Cat = b.BonusCategory,
                    Bonus = b.Bonus,

                    //SWFMonthlyBased = b.MaximumBonus == null ? 0 :b.MaximumBonus.Value,
                    MaxBonus = b.MaximumBonus,

                    BonusPaid = b.BonusToBePaid,
                    AdvanceBonus = b.AdvanceBonus == null ? 0 : b.AdvanceBonus.Value,
                    FinaBonus30Percent = b.FinaBonus30Percent == null ? 0 : b.FinaBonus30Percent,
                    FinalBonus70Percent = b.FinalBonus70Percent == null ? 0 : b.FinalBonus70Percent,
                    //AdvanceBonusMonthlySalaryBased = b.AdvanceBonus == null ? 0 : b.AdvanceBonus.Value,
                    NIBLFinalBonusGroupType = b.NIBLFinalBonusGroupType == null ? 0 : b.NIBLFinalBonusGroupType.Value,
                    CalculationType = b.NIBLFinalBonusGroupType != null && b.NIBLFinalBonusGroupType == 3 ? "Calculation" : "Monthly Salary"

                }
                ).ToList();

            int count = 0;
            foreach (BonusEmployeeBO item in list)
                item.SN = ++count;

            total = list.Count;
            //Paging code
            //if (start >= 0 && limit > 0)
            {
                list = list.Skip(currentPage * pageSize).Take(pageSize).ToList();
            }



            List<StatusName> statusNames = new JobStatus().GetStatusList();

            foreach (BonusEmployeeBO item in list)
            {


                if (!string.IsNullOrEmpty(item.RetirementDate))
                    item.RetirementDate = Convert.ToDateTime(item.RetirementDate).ToString("yyyy-MMM-dd");

                if (item.Status != null)
                {
                    StatusName name = statusNames.FirstOrDefault(x => x.StatusId == item.Status);
                    if (name != null)
                        item.StatusName = name.Name;
                }

                if (item.Eligibility != null)
                {
                    if (item.Eligibility == 1)
                        item.EligibilityName = "Eligible";
                    else if (item.Eligibility == 2)
                        item.EligibilityName = "Proportionate";
                    else
                        item.EligibilityName = "Not Eligible";
                }

                if (isFinalBonusWithEligibilityGroup)
                {
                    if (item.NIBLFinalBonusGroupType == 1)
                        item.Cat = "PartTime / Contractual";
                    else if (item.NIBLFinalBonusGroupType == 2)
                        item.Cat = "Below six month";
                    else if (item.NIBLFinalBonusGroupType == 3)
                        item.Cat = "Six month";
                }

                if (sstHeader != null)
                {
                    AddOnDetail sstAddOn = sstTDSList.FirstOrDefault(x => x.EmployeeId == item.EmployeeId && x.AddOnHeaderId == sstHeader.AddOnHeaderId);
                    item.SST = sstAddOn == null ? 0 : sstAddOn.Amount;
                }

                if (tdsHeader != null)
                {
                    AddOnDetail tdsAddOn = sstTDSList.FirstOrDefault(x => x.EmployeeId == item.EmployeeId && x.AddOnHeaderId == tdsHeader.AddOnHeaderId);
                    item.TDS = tdsAddOn == null ? 0 : tdsAddOn.Amount;
                }

                if (bonus.IsFinalBonus != null && bonus.IsFinalBonus.Value)
                {
                    item.NetPaid = Convert.ToDecimal(item.FinalBonus70Percent) - Convert.ToDecimal(item.SST) - Convert.ToDecimal(item.TDS);
                }
                else
                {
                    item.NetPaid = Convert.ToDecimal(item.BonusPaid) - Convert.ToDecimal(item.SST) - Convert.ToDecimal(item.TDS);
                }

                //if (item.NIBLFinalBonusGroupType == 3)
                //{
                //    item.AdvanceBonusMonthlySalaryBased = 0;
                //    item.SWFMonthlyBased = 0;
                //}
                //else
                //{
                //    item.AdvanceBonus = 0;
                //    item.MaxBonus = 0;
                //}
            }

            return list;
        }

        public static List<GetGeneratedOvertimeListResult> GetOvertimeList(int overtimeId, int currentPage, int pageSize, ref int total, string searchEmp
           , int eligiblity, int desid, int branchid)
        {

            return PayrollDataContext.GetGeneratedOvertimeList(overtimeId, desid, branchid, searchEmp).ToList();
        }
        public static List<GetFestivalPaymentEmployeeListResult> GetFestivalPaymentList(int festivalId, int currentPage, int pageSize, ref int total, string searchEmp
           , int eligiblity, string designationName, int branchid)
        {

            if (designationName == "--Select Designation--")
                designationName = "";

            List<GetFestivalPaymentEmployeeListResult> list =
                PayrollDataContext.GetFestivalPaymentEmployeeList(festivalId, designationName, branchid, searchEmp, currentPage, pageSize).ToList();

            if (list.Count > 0)
                total = list[0].TotalRows.Value;

            return list;
        }
        public static List<TextValue> GetOvertimeDesignationList(int overtimeId)
        {
            return
                (
                from o in PayrollDataContext.OvertimePeriodEmployees
                join d in PayrollDataContext.EDesignations on o.DesignationId equals d.DesignationId
                select new TextValue
                {
                    ID = d.DesignationId,
                    Text = d.Name
                }
                ).Distinct()
                .OrderBy(x => x.Text).ToList();

        }
        public static void ImportBonus(List<BonusEmployeeBO> list, int bonusId)
        {
            List<BonusEmployee> dbList = PayrollDataContext.BonusEmployees.Where(x => x.BonusId == bonusId).ToList();
            foreach (BonusEmployeeBO item in list)
            {
                BonusEmployee dbEntity = dbList.FirstOrDefault(x => x.EmployeeId == item.EmployeeId);
                if (dbEntity != null)
                {
                    dbEntity.UnpaidLeave = item.UnpaidLeave;
                    dbEntity.OtherUneligibleDays = item.OtherUneligibleDays;
                    dbEntity.OtherUneligibleDaysReason = item.OtherUneligibleDaysReason;
                    dbEntity.AnnualSalary = item.AnnualSalary;
                }
            }
            PayrollDataContext.SubmitChanges();

            CommonManager.GenerateBonus(bonusId, false, true);

        }

        public static bool GenerateBonus(int bonusId, bool generateEmpList, bool calculateBonus)
        {

            Bonus dbEntity = PayrollDataContext.Bonus.SingleOrDefault(
                g => g.BonusId == bonusId);

            if (IsBonusEditable(bonusId) == false)
                return false;

            PayrollDataContext.GenerateEmployeeListForBonus(bonusId, generateEmpList, calculateBonus);

            return true;
        }

        public static bool GenerateFestivalPayment(int festivalId)
        {

            PayrollDataContext.GenerateFestivalPaymment(festivalId);

            return true;
        }
        public static bool GenerateOvertime(int overtimeId, bool processedApprovedOT, int managerSupervisorId)
        {
            if (IsOvertimeEditable(overtimeId) == false)
                return false;

            PayrollDataContext.GenerateOvertime(overtimeId, processedApprovedOT, managerSupervisorId);

            return true;
        }
        public List<GratuityBO> GetAllGratuity(int companyId)
        {

            List<GratuityBO> list =
                (
                   from s in PayrollDataContext.GratuityRules
                   join l in PayrollDataContext.GratuityClasses on s.GratuityClassRef_ID equals l.GratuityClassID into dd
                   from ddd in dd.DefaultIfEmpty()


                   select new GratuityBO
                   {
                       GratuityRuleId = s.GratuityRuleId,
                       FromServiceYear = s.FromServiceYear,
                       ToServiceYear = s.ToServiceYear,
                       GratuityClassRef_ID = s.GratuityClassRef_ID,
                       ClassName = (ddd == null ? "" : ddd.Name),
                       MonthsSalary = s.MonthsSalary

                   }
                )
                 .OrderBy(g => g.ClassName)
                .ThenBy(g => g.FromServiceYear)
                .ToList();

            return list;
        }
        public List<DAL.OvertimeAutoGroup> GetAutoOvertimeGroups(int companyId)
        {

            List<OvertimeAutoGroup> list =
                (
                   from s in PayrollDataContext.OvertimeAutoGroups
                   orderby s.Name
                   select s

                )
                .ToList();

            List<BLevel> levelList = NewPayrollManager.GetAllParentLevelList();

            foreach (OvertimeAutoGroup item in list)
            {
                foreach (OvertimeAutoGroupLevel level in item.OvertimeAutoGroupLevels.ToList())
                {
                    if (item.LevelList == "")
                        item.LevelList = levelList.FirstOrDefault(x => x.LevelId == level.LevelId).Name;
                    else
                        item.LevelList += ", " + levelList.FirstOrDefault(x => x.LevelId == level.LevelId).Name;
                }
            }

            return list;
        }
        public List<GratuityClass> GetAllGratuityClass(int companyId)
        {
            return PayrollDataContext.GratuityClasses
                .OrderBy(g => g.Name)
                .ToList();
        }

        public static void UpdateEmpGratuityClass(int empId, int classId)
        {
            EHumanResource entity = PayrollDataContext.EHumanResources.FirstOrDefault(x => x.EmployeeId == empId);
            if (classId == 0 || classId == -1)
                entity.GratuityClassRef_ID = null;
            else
                entity.GratuityClassRef_ID = classId;
            PayrollDataContext.SubmitChanges();
        }
        public GratuityRule GetGratuityRule(int gratuityRuleId)
        {
            return PayrollDataContext.GratuityRules.SingleOrDefault(
                g => g.GratuityRuleId == gratuityRuleId);
        }
        public OvertimePeriod GetOvertimePeriod(int periodId)
        {
            return PayrollDataContext.OvertimePeriods.SingleOrDefault(
                g => g.OvertimeID == periodId);
        }
        public FestivalPaymentPeriod GetFestivalPaymentPeriod(int FestivalId)
        {
            return PayrollDataContext.FestivalPaymentPeriods.SingleOrDefault(
                g => g.FestivalId == FestivalId);
        }
        public OvertimeAutoGroup GetOTGroup(int groupID)
        {
            return PayrollDataContext.OvertimeAutoGroups.SingleOrDefault(
                g => g.OTGroupId == groupID);
        }
        public Bonus GetBonus(int bonusId)
        {
            return PayrollDataContext.Bonus.FirstOrDefault(x => x.BonusId == bonusId);
        }
        public BonusEmployee GetBonus(int bonusId, int empId)
        {
            return PayrollDataContext.BonusEmployees.FirstOrDefault(x => x.BonusId == bonusId && x.EmployeeId == empId);
        }
        public GratuityClass GetGratuityClass(int gratuityRuleId)
        {
            return PayrollDataContext.GratuityClasses.SingleOrDefault(
                g => g.GratuityClassID == gratuityRuleId);
        }
        public bool SaveGratuityRule(GratuityRule entity)
        {
            PayrollDataContext.GratuityRules.InsertOnSubmit(entity);
            return SaveChangeSet();
        }

        public static Status PostFestivalDashainToAddOn(int festivalId)
        {
            FestivalPaymentPeriod period = PayrollDataContext.FestivalPaymentPeriods.FirstOrDefault
                (x => x.FestivalId == festivalId);

            Status status = new Status();

            if (period.AddOnId == null || period.AddOnId == 0)
            {

                // create add on 
                AddOn add = new AddOn();
                add.PayrollPeriodId = period.PayrollPeriodId;
                add.Name = period.Name;
                //entity.ShowAfterPayrollPeriodId = int.Parse(ddlShowAfterPeriod.SelectedValue);
                add.ShowInPaySlip = true;
                add.DoNotShowPaymentAmount = true;
                PayrollDataContext.AddOns.InsertOnSubmit(add);
                SaveChangeSet();

                List<PIncome> incomes = new List<PIncome>();
                List<PDeduction> deductions = new List<PDeduction>();

                PIncome item = new PIncome();
                item.Type = 1;
                item.SourceId = period.IncomeId.Value;
                item.Title = PayManager.GetIncome(item.SourceId).Title;
                item.AddOnHeaderId = 0;

                item.UseIncomeSettings = false;

                item.AddToRegularSalary = false;

                incomes.Add(item);

                period.AddOnId = add.AddOnId;

                SaveChangeSet();

                PartialAddOnManager.SaveAddOnIncome(period.PayrollPeriodId.Value, add.AddOnId, incomes, deductions, true, null);
            }


            // update amount
            AddOnHeader addOnHeader = PayrollDataContext.AddOnHeaders
               .FirstOrDefault(x => x.AddOnId == period.AddOnId && x.Type == 1 && x.SourceId == period.IncomeId.Value);

            if (addOnHeader != null)
            {
                List<FestivalPaymentEmployee> dbList =
                    PayrollDataContext.FestivalPaymentEmployees.Where(x => x.FestivalId == period.FestivalId).ToList();

                List<AddOnDetail> addOnDetail = PayrollDataContext.AddOnDetails
                    .Where(x => x.AddOnHeaderId == addOnHeader.AddOnHeaderId).ToList();

                List<AddOnDetail> newAddOnDetailList = new List<AddOnDetail>();

                foreach (FestivalPaymentEmployee emp in dbList)
                {

                    AddOnDetail dbAddOn = addOnDetail.FirstOrDefault(x => x.EmployeeId == emp.EmployeeId);

                    if (dbAddOn != null)
                    {
                        dbAddOn.Amount = emp.Amount == null ? 0 : emp.Amount.Value;
                    }
                    else
                    {
                        dbAddOn = new AddOnDetail();
                        dbAddOn.AddOnHeaderId = addOnHeader.AddOnHeaderId;
                        dbAddOn.Amount = emp.Amount == null ? 0 : emp.Amount.Value;
                        dbAddOn.EmployeeId = emp.EmployeeId;


                        newAddOnDetailList.Add(dbAddOn);
                    }
                }

                PayrollDataContext.AddOnDetails.InsertAllOnSubmit(newAddOnDetailList);
            }

            UpdateChangeSet();

            return status;

        }

        public static Status PostOvertimeToAddOn(int overtimeId)
        {
            Status status = new Status();

            Setting setting = OvertimeManager.GetSetting();
            if (setting.OvertimeIncomeId == null | setting.OvertimeIncomeId == -1)
            {
                status.ErrorMessage = "Overtime income has to be set from Income / Pay setting page.";
                return status;
            }

            if (IsOvertimeEditable(overtimeId) == false)
            {
                status.ErrorMessage = "Past period overtime not editable.";
                return status;
            }

            OvertimePeriod period = PayrollDataContext.OvertimePeriods.FirstOrDefault
                (x => x.OvertimeID == overtimeId);

            if (period.PaidPayrollPeriodId == -1)
            {
                status.ErrorMessage = "Payroll Period need to select for OT for posting.";
                return status;
            }

            if (period.AddOnId == null || period.AddOnId == 0)
            {

                // create add on 
                AddOn add = new AddOn();
                add.PayrollPeriodId = period.PaidPayrollPeriodId;
                add.Name = period.Name;
                //entity.ShowAfterPayrollPeriodId = int.Parse(ddlShowAfterPeriod.SelectedValue);
                add.ShowInPaySlip = true;
                add.DoNotShowPaymentAmount = true;
                PayrollDataContext.AddOns.InsertOnSubmit(add);
                SaveChangeSet();

                List<PIncome> incomes = new List<PIncome>();
                List<PDeduction> deductions = new List<PDeduction>();

                PIncome item = new PIncome();
                item.Type = 1;
                item.SourceId = setting.OvertimeIncomeId.Value;
                item.Title = PayManager.GetIncome(item.SourceId).Title;
                item.AddOnHeaderId = 0;

                item.UseIncomeSettings = false;

                item.AddToRegularSalary = false;

                incomes.Add(item);

                period.AddOnId = add.AddOnId;

                SaveChangeSet();

                PartialAddOnManager.SaveAddOnIncome(period.PaidPayrollPeriodId, add.AddOnId, incomes, deductions, true, null);
            }


            // update amount
            AddOnHeader addOnHeader = PayrollDataContext.AddOnHeaders
               .FirstOrDefault(x => x.AddOnId == period.AddOnId && x.Type == 1 && x.SourceId == setting.OvertimeIncomeId.Value);

            if (addOnHeader != null)
            {
                List<OvertimePeriodEmployee> dbList =
                    PayrollDataContext.OvertimePeriodEmployees.Where(x => x.OvertimeID == period.OvertimeID).ToList();

                List<AddOnDetail> addOnDetail = PayrollDataContext.AddOnDetails
                    .Where(x => x.AddOnHeaderId == addOnHeader.AddOnHeaderId).ToList();

                List<AddOnDetail> newAddOnDetailList = new List<AddOnDetail>();

                foreach (OvertimePeriodEmployee emp in dbList)
                {

                    AddOnDetail dbAddOn = addOnDetail.FirstOrDefault(x => x.EmployeeId == emp.EmployeeId);

                    if (dbAddOn != null)
                    {
                        dbAddOn.Amount = emp.OTAmount == null ? 0 : emp.OTAmount;
                    }
                    else
                    {
                        dbAddOn = new AddOnDetail();
                        dbAddOn.AddOnHeaderId = addOnHeader.AddOnHeaderId;
                        dbAddOn.Amount = emp.OTAmount == null ? 0 : emp.OTAmount;
                        dbAddOn.EmployeeId = emp.EmployeeId;


                        newAddOnDetailList.Add(dbAddOn);
                    }
                }

                PayrollDataContext.AddOnDetails.InsertAllOnSubmit(newAddOnDetailList);
            }

            UpdateChangeSet();

            return status;

        }

        public static Status PostArrearToAddOn(int payrollPeriodId)
        {
            Status status = new Status();

            CCalculation calculation = PayrollDataContext.CCalculations.FirstOrDefault(x => x.PayrollPeriodId == payrollPeriodId);
            if (calculation != null && calculation.IsFinalSaved != null && calculation.IsFinalSaved.Value)
            {
                status.ErrorMessage = "Final saved already done, can not change anything.";
                return status;
            }




            // create add on 
            AddOn arrearAddOn = PayrollDataContext.AddOns.FirstOrDefault(
                x => x.PayrollPeriodId == payrollPeriodId && x.AddOnType == (int)AddOnTypeEnum.ArrearType);

            if (arrearAddOn == null)
            {
                status.ErrorMessage = "Arrear type Add-On should be created from Payroll -> Add-On before posting.";
                return status;
            }

            List<CCalculationEmployee> salarySavedEmployees =
                (
                    from e in PayrollDataContext.CCalculationEmployees
                    join c in PayrollDataContext.CCalculations on e.CalculationId equals c.CalculationId
                    where c.PayrollPeriodId == payrollPeriodId
                    select e
                ).ToList();

            List<AddOnHeader> headerList =
                (
                    from h in PayrollDataContext.AddOnHeaders
                    join a in PayrollDataContext.AddOns on h.AddOnId equals a.AddOnId
                    where a.PayrollPeriodId == payrollPeriodId && a.AddOnId == arrearAddOn.AddOnId
                    select h
                ).ToList();

            List<PIncome> incomes = PayManager.GetIncomeList();

            List<AddOnDetail> dbAddOnDetails =
                (
                from a in PayrollDataContext.AddOns
                join h in PayrollDataContext.AddOnHeaders on a.AddOnId equals h.AddOnId
                join ad in PayrollDataContext.AddOnDetails on h.AddOnHeaderId equals ad.AddOnHeaderId
                where h.AddOnId == arrearAddOn.AddOnId
                select ad
                ).ToList();

            List<int> incomeIDList = PayrollDataContext.RetrospectIncrements
                .Where(x => x.PayrollPeriodId == payrollPeriodId && Convert.ToDecimal(x.CalculatedAmount) != 0).Select(x => x.IncomeId).ToList();

            //List<int> incomeIDList = increments.Select(x => x.IncomeId).ToList();

            foreach (var incomeId in incomeIDList)
            {
                if (headerList.Any(x => (x.Type == 1 || x.Type == 25) && x.SourceId == incomeId) == false)
                {
                    PIncome income = PayManager.GetIncome(incomeId);
                    if (income != null && income.Calculation != IncomeCalculation.DEEMED_INCOME)
                    {
                        status.ErrorMessage = "Income " + income.Title + " does not exists in the Add-On list, please add the income from Add-On -> Select Incomes before posting.";
                        return status;
                    }
                }
            }


            DateTime createdOn = DateTime.Now;

            List<RetrospectIncrement> increments = PayrollDataContext.RetrospectIncrements.Where(x => x.PayrollPeriodId == payrollPeriodId).ToList();
            List<AddOnDetail> addOnList = new List<AddOnDetail>();

            bool isPosted = false;

            List<int> einList = increments.Select(x => x.EmployeeId).Distinct().ToList();

            foreach (var item in increments)
            {

                if (salarySavedEmployees.Any(x => x.EmployeeId == item.EmployeeId))
                    continue;

                PIncome income = incomes.FirstOrDefault(x => x.IncomeId == item.IncomeId);
                if (incomes != null && income.Calculation == IncomeCalculation.DEEMED_INCOME)
                    continue;

                isPosted = true;
                //einList.Add(item.EmployeeId);


                // update amount
                AddOnHeader addOnHeader = headerList
                   .FirstOrDefault(x => x.AddOnId == arrearAddOn.AddOnId && x.Type == 1 && x.SourceId == item.IncomeId);

                if (addOnHeader != null)
                {

                    //foreach (OvertimePeriodEmployee emp in dbList)
                    {

                        AddOnDetail dbAddOn = dbAddOnDetails.FirstOrDefault(x => x.EmployeeId == item.EmployeeId
                            && x.AddOnHeaderId == addOnHeader.AddOnHeaderId);

                        if (dbAddOn != null)
                        {
                            dbAddOn.Amount = item.AdjustedAmount == null ? 0 : item.AdjustedAmount.Value;
                        }
                        else
                        {
                            dbAddOn = new AddOnDetail();
                            dbAddOn.AddOnHeaderId = addOnHeader.AddOnHeaderId;
                            dbAddOn.Amount = item.AdjustedAmount == null ? 0 : item.AdjustedAmount.Value;
                            dbAddOn.EmployeeId = item.EmployeeId;
                            dbAddOn.CreatedOn = createdOn;

                            addOnList.Add(dbAddOn);
                        }
                    }


                }
            }

            if (isPosted)
            {

                AddOnHeader addOnHeaderPF = headerList
                  .FirstOrDefault(x => x.AddOnId == arrearAddOn.AddOnId && x.Type == (int)CalculationColumnType.IncomePF
                      && x.SourceId == (int)CalculationColumnType.IncomePF);
                AddOnHeader addOnHeaderDeductionPF = headerList
                  .FirstOrDefault(x => x.AddOnId == arrearAddOn.AddOnId && x.Type == (int)CalculationColumnType.DeductionPF
                      && x.SourceId == (int)CalculationColumnType.DeductionPF);


                // process for pf and deduction pf also
                foreach (var ein in einList)
                {
                    decimal totalIncomePF = increments.Where(x => x.EmployeeId == ein).Sum(x => Convert.ToDecimal(x.AdjustedPFAmount));
                    if (totalIncomePF > 0)
                    {
                        if (addOnHeaderPF == null)
                        {
                            status.ErrorMessage = "Income PF not defined in Add-On header list.";
                            return status;
                        }
                        if (addOnHeaderDeductionPF == null)
                        {
                            status.ErrorMessage = "Deduction PF not defined in Add-On header list.";
                            return status;
                        }


                        // Income PF
                        AddOnDetail dbAddOn = dbAddOnDetails.FirstOrDefault(x => x.EmployeeId == ein
                           && x.AddOnHeaderId == addOnHeaderPF.AddOnHeaderId);

                        if (dbAddOn != null)
                        {
                            dbAddOn.Amount = totalIncomePF;
                        }
                        else
                        {
                            dbAddOn = new AddOnDetail();
                            dbAddOn.AddOnHeaderId = addOnHeaderPF.AddOnHeaderId;
                            dbAddOn.Amount = totalIncomePF;
                            dbAddOn.EmployeeId = ein;
                            dbAddOn.CreatedOn = createdOn;

                            addOnList.Add(dbAddOn);
                        }

                        // Deduction PF
                        AddOnDetail dbAddOnDeduction = dbAddOnDetails.FirstOrDefault(x => x.EmployeeId == ein
                           && x.AddOnHeaderId == addOnHeaderDeductionPF.AddOnHeaderId);

                        if (dbAddOnDeduction != null)
                        {
                            dbAddOnDeduction.Amount = totalIncomePF * 2; // double of income pf
                        }
                        else
                        {
                            dbAddOnDeduction = new AddOnDetail();
                            dbAddOnDeduction.AddOnHeaderId = addOnHeaderDeductionPF.AddOnHeaderId;
                            dbAddOnDeduction.Amount = totalIncomePF * 2;
                            dbAddOnDeduction.EmployeeId = ein;
                            dbAddOnDeduction.CreatedOn = createdOn;

                            addOnList.Add(dbAddOnDeduction);
                        }
                    }
                }

                PayrollDataContext.AddOnDetails.InsertAllOnSubmit(addOnList);
                UpdateChangeSet();

            }
            else
            {
                status.ErrorMessage = "No list for posting.";
            }


            return status;

        }

        public static Status SetArrearZero(int payrollPeriodId)
        {
            Status status = new Status();

            CCalculation calculation = PayrollDataContext.CCalculations.FirstOrDefault(x => x.PayrollPeriodId == payrollPeriodId);
            if (calculation != null && calculation.IsFinalSaved != null && calculation.IsFinalSaved.Value)
            {
                status.ErrorMessage = "Final saved already done, can not change anything.";
                return status;
            }






            List<CCalculationEmployee> salarySavedEmployees =
                (
                    from e in PayrollDataContext.CCalculationEmployees
                    join c in PayrollDataContext.CCalculations on e.CalculationId equals c.CalculationId
                    where c.PayrollPeriodId == payrollPeriodId
                    select e
                ).ToList();

            List<PIncome> incomes = PayManager.GetIncomeList();

            DateTime createdOn = DateTime.Now;

            List<RetrospectIncrement> increments = PayrollDataContext.RetrospectIncrements.Where(x => x.PayrollPeriodId == payrollPeriodId).ToList();

            bool isPosted = false;

            List<int> einList = new List<int>();
            foreach (var item in increments)
            {

                if (salarySavedEmployees.Any(x => x.EmployeeId == item.EmployeeId))
                    continue;

                PIncome income = incomes.FirstOrDefault(x => x.IncomeId == item.IncomeId);
                if (income != null && income.Calculation == IncomeCalculation.DEEMED_INCOME)
                    continue;

                isPosted = true;
                einList.Add(item.EmployeeId);



                item.AdjustedAmount = 0;
                item.AdjustedPFAmount = 0;
                item.ModifiedOn = createdOn;
                item.ModifiedBy = SessionManager.User.UserID;
            }

            PayrollDataContext.SubmitChanges();


            return status;

        }
        public static Status PostBonusToAddOn(int bonusId)
        {
            Status status = new Status();

            Bonus bonus = PayrollDataContext.Bonus.FirstOrDefault(x => x.BonusId == bonusId);

            if (bonus.IncomeId == null | bonus.IncomeId == -1)
            {
                status.ErrorMessage = "Bonus income has to be set.";
                return status;
            }

            if (IsBonusEditable(bonusId) == false)
            {
                status.ErrorMessage = "Bonus not editable.";
                return status;
            }



            if (bonus.AddOnId == null || bonus.AddOnId == 0 || bonus.AddOnId == -1)
            {

                // create add on 
                AddOn add = new AddOn();
                add.PayrollPeriodId = bonus.PaidPayrollPeriodId;
                add.Name = bonus.Name;
                //entity.ShowAfterPayrollPeriodId = int.Parse(ddlShowAfterPeriod.SelectedValue);
                add.ShowInPaySlip = true;
                add.DoNotShowPaymentAmount = true;
                PayrollDataContext.AddOns.InsertOnSubmit(add);
                SaveChangeSet();

                List<PIncome> incomes = new List<PIncome>();
                List<PDeduction> deductions = new List<PDeduction>();

                PIncome item = new PIncome();
                item.Type = 1;
                item.SourceId = bonus.IncomeId.Value;
                item.Title = PayManager.GetIncome(item.SourceId).Title;
                item.AddOnHeaderId = 0;

                item.UseIncomeSettings = false;

                item.AddToRegularSalary = false;

                incomes.Add(item);

                bonus.AddOnId = add.AddOnId;

                SaveChangeSet();

                PartialAddOnManager.SaveAddOnIncome(bonus.PaidPayrollPeriodId.Value, add.AddOnId, incomes, deductions, true, null);
            }


            // update amount
            AddOnHeader addOnHeader = PayrollDataContext.AddOnHeaders
               .FirstOrDefault(x => x.AddOnId == bonus.AddOnId && x.Type == 1 && x.SourceId == bonus.IncomeId.Value);

            if (addOnHeader != null)
            {
                List<BonusEmployee> dbList =
                    PayrollDataContext.BonusEmployees.Where(x => x.BonusId == bonus.BonusId).ToList();

                List<AddOnDetail> addOnDetail = PayrollDataContext.AddOnDetails
                    .Where(x => x.AddOnHeaderId == addOnHeader.AddOnHeaderId).ToList();

                List<AddOnDetail> newAddOnDetailList = new List<AddOnDetail>();

                foreach (BonusEmployee emp in dbList)
                {

                    AddOnDetail dbAddOn = addOnDetail.FirstOrDefault(x => x.EmployeeId == emp.EmployeeId);

                    if (dbAddOn != null)
                    {
                        //dbAddOn.Amount = emp.BonusToBePaid == null ? 0 : emp.BonusToBePaid.Value;

                        if (bonus.IsFinalBonus != null && bonus.IsFinalBonus.Value)
                        {
                            dbAddOn.Amount = emp.FinalBonus70Percent == null ? 0 : emp.FinalBonus70Percent.Value;
                        }
                        else
                        {

                            dbAddOn.Amount = emp.BonusToBePaid == null ? 0 : emp.BonusToBePaid.Value;
                        }

                    }
                    else
                    {
                        dbAddOn = new AddOnDetail();
                        dbAddOn.AddOnHeaderId = addOnHeader.AddOnHeaderId;

                        if (bonus.IsFinalBonus != null && bonus.IsFinalBonus.Value)
                        {
                            dbAddOn.Amount = emp.FinalBonus70Percent == null ? 0 : emp.FinalBonus70Percent.Value;
                        }
                        else
                        {

                            dbAddOn.Amount = emp.BonusToBePaid == null ? 0 : emp.BonusToBePaid.Value;
                        }
                        dbAddOn.EmployeeId = emp.EmployeeId;


                        newAddOnDetailList.Add(dbAddOn);
                    }
                }

                PayrollDataContext.AddOnDetails.InsertAllOnSubmit(newAddOnDetailList);
            }

            UpdateChangeSet();

            return status;

        }
        public Status SaveOvertimePeriod(OvertimePeriod entity)
        {
            Status status = new Status();

            Setting setting = OvertimeManager.GetSetting();

            if (setting.OvertimeIncomeId == null | setting.OvertimeIncomeId == -1)
            {
                status.ErrorMessage = "Overtime income has to be set from Income / Pay setting page.";
                return status;
            }



            PayrollDataContext.OvertimePeriods.InsertOnSubmit(entity);
            SaveChangeSet();

            // create add on 
            //AddOn add = new AddOn();
            //add.PayrollPeriodId = entity.PaidPayrollPeriodId;
            //add.Name = entity.Name;

            //add.ShowInPaySlip = false;
            //PayrollDataContext.AddOns.InsertOnSubmit(add);
            //SaveChangeSet();

            //List<PIncome> incomes = new List<PIncome>();
            //List<PDeduction> deductions = new List<PDeduction>();

            //PIncome item = new PIncome();
            //item.Type = 1;
            //item.SourceId = setting.OvertimeIncomeId.Value;
            //item.Title = PayManager.GetIncome(item.SourceId).Title;
            //item.AddOnHeaderId = 0;

            //item.UseIncomeSettings = false;

            //item.AddToRegularSalary = false;

            //incomes.Add(item);

            //entity.AddOnId = add.AddOnId;

            //PartialAddOnManager.SaveAddOnIncome(entity.PaidPayrollPeriodId, add.AddOnId, incomes, deductions,true);


            return status;
        }

        public Status SaveFestivalPeriod(FestivalPaymentPeriod entity)
        {
            Status status = new Status();


            if (entity.PayrollPeriodId != -1)
            {

                if (entity.PayrollPeriodId != CommonManager.GetLastPayrollPeriod().PayrollPeriodId)
                {
                    status.ErrorMessage = "Festival type can be added for the last period only.";
                    return status;
                }

                entity.YearId = CommonManager.GetPayrollPeriod(entity.PayrollPeriodId.Value).FinancialDateId.Value;
            }

            PayrollDataContext.FestivalPaymentPeriods.InsertOnSubmit(entity);
            SaveChangeSet();


            return status;
        }
        public bool SaveOTGroup(OvertimeAutoGroup entity)
        {
            PayrollDataContext.OvertimeAutoGroups.InsertOnSubmit(entity);
            return SaveChangeSet();
        }
        public Status SaveBonus(Bonus entity)
        {
            //PayrollDataContext.Bonus.InsertOnSubmit(entity);
            //return SaveChangeSet();

            Status status = new Status();

            //Setting setting = OvertimeManager.GetSetting();

            //if (setting.OvertimeIncomeId == null | setting.OvertimeIncomeId == -1)
            //{
            //    status.ErrorMessage = "Overtime income has to be set from Income / Pay setting page.";
            //    return status;
            //}



            PayrollDataContext.Bonus.InsertOnSubmit(entity);
            SaveChangeSet();


            // create add on 
            //AddOn add = new AddOn();
            //add.PayrollPeriodId = entity.PaidPayrollPeriodId;
            //add.Name = entity.Name;
            ////entity.ShowAfterPayrollPeriodId = int.Parse(ddlShowAfterPeriod.SelectedValue);
            //add.ShowInPaySlip = false;
            //PayrollDataContext.AddOns.InsertOnSubmit(add);
            //SaveChangeSet();

            //List<PIncome> incomes = new List<PIncome>();
            //List<PDeduction> deductions = new List<PDeduction>();

            ////PIncome item = new PIncome();
            ////item.Type = 1;
            ////item.SourceId = setting.OvertimeIncomeId.Value;
            ////item.Title = PayManager.GetIncome(item.SourceId).Title;
            ////item.AddOnHeaderId = 0;

            ////item.UseIncomeSettings = false;

            ////item.AddToRegularSalary = false;

            ////incomes.Add(item);

            //entity.AddOnId = add.AddOnId;

            //PartialAddOnManager.SaveAddOnIncome(entity.PaidPayrollPeriodId.Value, add.AddOnId, incomes, deductions, true);


            return status;
        }

        public bool SaveIneligibleBonus(BonusIneligibleEmployee entity)
        {
            PayrollDataContext.BonusIneligibleEmployees.InsertOnSubmit(entity);
            return SaveChangeSet();
        }
        public bool SaveGratuityClass(GratuityClass entity)
        {
            PayrollDataContext.GratuityClasses.InsertOnSubmit(entity);
            return SaveChangeSet();
        }

        public static void DeleteGratuityRule(int gratuityRuleId)
        {
            GratuityRule rule =
                PayrollDataContext.GratuityRules.Where(g => g.GratuityRuleId == gratuityRuleId)
                .SingleOrDefault();

            if (rule != null)
            {
                PayrollDataContext.GratuityRules.DeleteOnSubmit(rule);
                PayrollDataContext.SubmitChanges();
            }
        }
        public static bool DeleteGratuityClass(int gratuityRuleId)
        {
            if (PayrollDataContext.EHumanResources.Any(x => x.GratuityClassRef_ID == gratuityRuleId))
                return false;


            GratuityClass rule =
                PayrollDataContext.GratuityClasses.Where(g => g.GratuityClassID == gratuityRuleId)
                .SingleOrDefault();

            if (rule != null)
            {
                PayrollDataContext.GratuityClasses.DeleteOnSubmit(rule);
                PayrollDataContext.SubmitChanges();
            }

            return true;
        }
        public static bool DeleteOTGroup(int groupID)
        {

            OvertimeAutoGroup rule =
                PayrollDataContext.OvertimeAutoGroups.Where(g => g.OTGroupId == groupID)
                .SingleOrDefault();

            if (rule != null)
            {
                PayrollDataContext.OvertimeAutoGroups.DeleteOnSubmit(rule);
                PayrollDataContext.SubmitChanges();
            }

            return true;
        }
        public static bool DeleteIneligibleBonus(int bonusId, int empId)
        {
            BonusIneligibleEmployee entity = PayrollDataContext
                .BonusIneligibleEmployees.FirstOrDefault(x => x.BonusId == bonusId && x.EmployeeId == empId);
            if (entity != null)
            {
                PayrollDataContext.BonusIneligibleEmployees.DeleteOnSubmit(entity);
                PayrollDataContext.SubmitChanges();
                return true;
            }

            return false;
        }

        public static bool IsBonusEditable(int bonusId)
        {

            Bonus rule =
                PayrollDataContext.Bonus.Where(g => g.BonusId == bonusId)
                .SingleOrDefault();

            bool isEditable = (rule != null && rule.Status == 1);

            if (isEditable)
                return false;

            return
                 (rule.PaidPayrollPeriodId != null && CommonManager.GetLastPayrollPeriod().PayrollPeriodId == rule.PaidPayrollPeriodId);


        }
        public static bool IsOvertimeEditable(int overtimeId)
        {

            OvertimePeriod rule =
                PayrollDataContext.OvertimePeriods.Where(g => g.OvertimeID == overtimeId)
                .SingleOrDefault();

            bool isEditable = (rule != null && rule.Status == 1);

            if (isEditable)
                return false;

            if (rule.PaidPayrollPeriodId == -1)
                return true;

            return
                 (rule.PaidPayrollPeriodId != null && CommonManager.GetLastPayrollPeriod().PayrollPeriodId == rule.PaidPayrollPeriodId);


        }
        public static bool DeleteBonus(int bonusId)
        {


            Bonus rule =
                PayrollDataContext.Bonus.Where(g => g.BonusId == bonusId)
                .SingleOrDefault();



            if (IsBonusEditable(bonusId))
            {
                List<BonusEmployee> list = PayrollDataContext.BonusEmployees.Where(x => x.BonusId == bonusId).ToList();

                PayrollDataContext.BonusEmployees.DeleteAllOnSubmit(list);

                PayrollDataContext.Bonus.DeleteOnSubmit(rule);
                PayrollDataContext.SubmitChanges();
            }
            else
                return false;

            return true;
        }
        public static bool IsGratuityRuleValid(GratuityRule entity)
        {

            List<GratuityRule> rules = PayrollDataContext.GratuityRules
                 .Where(g =>

                      g.CompanyId == SessionManager.CurrentCompanyId &&
                      g.ApplicableTo.Equals(entity.ApplicableTo) &&

                      (entity.GratuityRuleId == 0 || entity.GratuityRuleId != g.GratuityRuleId) &&


                     (


                     (entity.FromServiceYear >= g.FromServiceYear && entity.FromServiceYear < g.ToServiceYear)
                     ||
                     (entity.ToServiceYear > g.FromServiceYear && entity.ToServiceYear <= g.ToServiceYear)
                     )

                     ).ToList();

            if (rules.Count > 0)
                return false;
            return true;
        }
        public static bool IsOvertimeValid(OvertimePeriod entity)
        {

            return true;

        }
        public static string IsOTGroupValid(OvertimeAutoGroup entity)
        {
            foreach (OvertimeAutoGroupLevel item in entity.OvertimeAutoGroupLevels)
            {

                OvertimeAutoGroupLevel level
                    = PayrollDataContext.OvertimeAutoGroupLevels
                    .FirstOrDefault(x => x.LevelId == item.LevelId && x.OTGroupId != entity.OTGroupId);

                if (level != null)
                {
                    return string.Format("Level \"{0}\" is already associated with the OT group \"{1}\", please remove from that group before adding in this group.",
                        NewPayrollManager.GetLevelById(item.LevelId).Name, GetOTGroup1(level.OTGroupId).Name);

                }
            }
            return "";
        }

        public static string GetLevelListNotDefinedInOTGroup()
        {
            List<BLevel> list =
                (
                from se in PayrollDataContext.BLevels

                join l in PayrollDataContext.OvertimeAutoGroupLevels on se.LevelId equals l.LevelId into dd
                from ddd in dd.DefaultIfEmpty()
                where ddd.LevelId == null
                orderby se.Order

                select se
                ).ToList();

            string value = "";

            foreach (BLevel item in list)
            {
                if (value == "")
                    value = item.Name;
                else
                    value += ", " + item.Name;
            }

            return value;
        }

        public static OvertimeAutoGroup GetOTGroup1(int groupID)
        {
            return PayrollDataContext.OvertimeAutoGroups.FirstOrDefault(x => x.OTGroupId == groupID);
        }
        public static bool IsGratuityClassValid(GratuityClass entity)
        {

            if (PayrollDataContext.GratuityClasses.Any(x => x.IsDefault == true && entity.IsDefault && x.GratuityClassID != entity.GratuityClassID))
            {
                return false;
            }

            return true;
        }

        public List<GratuityStatus> GetGratuityStatus()
        {
            return PayrollDataContext.GratuityStatus.ToList();
        }
        public List<GetGratuityIncomeForCompanyResult> GetGratuityIncomeListForCompany(int companyId)
        {
            return PayrollDataContext.GetGratuityIncomeForCompany(companyId).ToList();
        }

        public List<CITIncome> GetCITIncomeListForCompany(int companyId)
        {
            return PayrollDataContext.CITIncomes.Where(c => c.CompanyId == companyId).ToList();
        }

        public bool UpdateGratuityRule(GratuityRule entity)
        {
            GratuityRule dbEntity = PayrollDataContext.GratuityRules.SingleOrDefault(
                g => g.GratuityRuleId == entity.GratuityRuleId);

            if (dbEntity == null)
                return false;


            dbEntity.FromServiceYear = entity.FromServiceYear;
            dbEntity.ToServiceYear = entity.ToServiceYear;
            dbEntity.MonthsSalary = entity.MonthsSalary;
            dbEntity.ApplicableTo = entity.ApplicableTo;

            dbEntity.GratuityClassRef_ID = entity.GratuityClassRef_ID;
            //dbEntity.GratuityRuleIncomes.Clear();

            //dbEntity.GratuityRuleIncomes.AddRange(entity.GratuityRuleIncomes);

            return UpdateChangeSet();
        }
        public bool UpdateOvertimePeriod(OvertimePeriod entity)
        {
            OvertimePeriod dbEntity = PayrollDataContext.OvertimePeriods.SingleOrDefault(
                g => g.OvertimeID == entity.OvertimeID);

            if (dbEntity == null)
                return false;

            if (entity.AddOnId != null && entity.AddOnId != -1)
            {
                if (IsOvertimeEditable(entity.OvertimeID) == false)
                    return false;
            }


            dbEntity.Name = entity.Name;
            dbEntity.Status = entity.Status;
            dbEntity.StartDate = entity.StartDate;
            dbEntity.EndDate = entity.EndDate;

            if (entity.AddOnId == null || entity.AddOnId == -1)
                dbEntity.PaidPayrollPeriodId = entity.PaidPayrollPeriodId;

            //dbEntity.GratuityRuleIncomes.Clear();

            //dbEntity.GratuityRuleIncomes.AddRange(entity.GratuityRuleIncomes);

            UpdateChangeSet();
            return true;
        }

        public bool UpdateFestivalEmployee(FestivalPaymentEmployee entity)
        {
            FestivalPaymentEmployee dbEntity = BLL.BaseBiz.PayrollDataContext.FestivalPaymentEmployees
               .FirstOrDefault(x => x.EmployeeId == entity.EmployeeId && x.FestivalId == entity.FestivalId);

            if (dbEntity == null)
                return false;




            dbEntity.Amount = entity.Amount;
            dbEntity.TotalWorkDays = entity.TotalWorkDays;
            dbEntity.Remarks = entity.Remarks;
            dbEntity.PreventReset = entity.PreventReset;


            UpdateChangeSet();
            return true;
        }
        public bool UpdateFestivalPeriod(FestivalPaymentPeriod entity)
        {
            FestivalPaymentPeriod dbEntity = PayrollDataContext.FestivalPaymentPeriods.SingleOrDefault(
                g => g.FestivalId == entity.FestivalId);

            if (dbEntity == null)
                return false;


            if (dbEntity.AddOnId != null && dbEntity.AddOnId != -1 && dbEntity.AddOnId != 0)
            {
                if (dbEntity.PayrollPeriodId != entity.PayrollPeriodId)
                {
                    return false;
                }
            }

            if (entity.PayrollPeriodId != -1 &&
                entity.PayrollPeriodId != CommonManager.GetLastPayrollPeriod().PayrollPeriodId)
            {
                return false;
            }

            dbEntity.Name = entity.Name;
            dbEntity.Status = entity.Status;
            dbEntity.IsArrearType = entity.IsArrearType;
            dbEntity.ArrearFestivalRef_ID = entity.ArrearFestivalRef_ID;
            dbEntity.PayrollPeriodId = entity.PayrollPeriodId;

            dbEntity.YearId = CommonManager.GetPayrollPeriod(entity.PayrollPeriodId.Value).FinancialDateId.Value;

            //dbEntity.GratuityRuleIncomes.Clear();

            //dbEntity.GratuityRuleIncomes.AddRange(entity.GratuityRuleIncomes);

            UpdateChangeSet();
            return true;
        }
        public bool UpdateOTGroup(OvertimeAutoGroup entity)
        {
            OvertimeAutoGroup dbEntity = PayrollDataContext.OvertimeAutoGroups.SingleOrDefault(
                g => g.OTGroupId == entity.OTGroupId);

            if (dbEntity == null)
                return false;


            dbEntity.Name = entity.Name;
            dbEntity.IsOTAllowed = entity.IsOTAllowed;
            dbEntity.GraceMinuteToBeAddedInStandardHour = entity.GraceMinuteToBeAddedInStandardHour;
            dbEntity.MaxOTMinuteInADay = entity.MaxOTMinuteInADay;
            dbEntity.MaxOTMinuteInHoliday = entity.MaxOTMinuteInHoliday;

            dbEntity.CalculationType = entity.CalculationType;
            dbEntity.MinOTHourInADay = entity.MinOTHourInADay;
            dbEntity.MinOTHourInHoliday = entity.MinOTHourInHoliday;

            dbEntity.StandardWorkMinOverridingFromShiftForOT = entity.StandardWorkMinOverridingFromShiftForOT;

            dbEntity.OvertimeAutoGroupLevels.Clear();

            dbEntity.OvertimeAutoGroupLevels.AddRange(entity.OvertimeAutoGroupLevels);
            //dbEntity.GratuityRuleIncomes.Clear();

            //dbEntity.GratuityRuleIncomes.AddRange(entity.GratuityRuleIncomes);

            return UpdateChangeSet();
        }

        public bool UpdateBonus(Bonus entity)
        {
            Bonus dbEntity = PayrollDataContext.Bonus.SingleOrDefault(
                g => g.BonusId == entity.BonusId);

            if (dbEntity == null)
                return false;

            if (entity.AddOnId != null && entity.AddOnId != -1)
            {
                if (IsBonusEditable(entity.BonusId) == false)
                    return false;
            }

            dbEntity.YearID = entity.YearID;
            dbEntity.PaidPayrollPeriodId = entity.PaidPayrollPeriodId;
            dbEntity.Name = entity.Name;
            dbEntity.BonusAmount = entity.BonusAmount;
            dbEntity.DistributionDateEng = entity.DistributionDateEng;
            dbEntity.DistributionDate = entity.DistributionDate;
            dbEntity.Status = entity.Status;
            dbEntity.IncomeId = entity.IncomeId;

            dbEntity.BonusStatus.Clear();
            dbEntity.BonusStatus.AddRange(entity.BonusStatus);

            dbEntity.BonusIncomes.Clear();
            dbEntity.BonusIncomes.AddRange(entity.BonusIncomes);

            dbEntity.StartDate = entity.StartDate;
            dbEntity.EndDate = entity.EndDate;
            dbEntity.IsFinalBonus = entity.IsFinalBonus;
            dbEntity.AdvanceBonusRef_Id = entity.AdvanceBonusRef_Id;

            UpdateChangeSet();
            return true;
        }
        public bool UpdateBonusEmp(BonusEmployee entity)
        {
            BonusEmployee dbEntity = PayrollDataContext.BonusEmployees.SingleOrDefault(
                g => g.BonusId == entity.BonusId && g.EmployeeId == entity.EmployeeId);

            if (dbEntity == null)
                return false;


            dbEntity.UnpaidLeave = entity.UnpaidLeave;
            dbEntity.OtherUneligibleDays = entity.OtherUneligibleDays;
            dbEntity.AnnualSalary = entity.AnnualSalary;

            return UpdateChangeSet();
        }
        public bool UpdateGratuityClass(GratuityClass entity)
        {
            GratuityClass dbEntity = PayrollDataContext.GratuityClasses.SingleOrDefault(
                g => g.GratuityClassID == entity.GratuityClassID);

            if (dbEntity == null)
                return false;


            dbEntity.Name = entity.Name;
            dbEntity.IsDefault = entity.IsDefault;

            //dbEntity.GratuityRuleIncomes.Clear();

            //dbEntity.GratuityRuleIncomes.AddRange(entity.GratuityRuleIncomes);

            return UpdateChangeSet();
        }
        #endregion


        #region "Payroll period"

        public static int GetTotalPeriodCount()
        {
            return PayrollDataContext.PayrollPeriods.Count();
        }

        public static List<PayrollPeriod> GetPayrollList(int payrollStartId, int payrollEndId)
        {

            List<PayrollPeriod> list
                = PayrollDataContext.PayrollPeriods
                .Where(x => x.PayrollPeriodId >= payrollStartId && x.PayrollPeriodId <= payrollEndId)
                .OrderBy(x => x.PayrollPeriodId).ToList();

            return list;
        }
        public static void SetFirstAndLastFinalSavedPayrollPeriod(DropDownList ddlYear, DropDownList ddlMonth)
        {
            int companyId = SessionManager.CurrentCompanyId;

            PayrollPeriod payrollStart = PayrollDataContext.PayrollPeriods.Where(
                p => p.CompanyId == companyId).FirstOrDefault();

            if (payrollStart == null)
                return;

            PayrollPeriod payrollEnd = PayrollDataContext.PayrollPeriods.Where(
                p => p.CompanyId == companyId)
                .OrderByDescending(p => p.PayrollPeriodId)
                .FirstOrDefault();

            //PayrollPeriod payrollEnd = 
            //    (
            //        from p in PayrollDataContext.PayrollPeriods
            //            join c in PayrollDataContext.CCalculations on p.PayrollPeriodId equals c.PayrollPeriodId

            //            where p.CompanyId == companyId 
            //            orderby  p.PayrollPeriodId descending 
            //            select  p
            //    ).FirstOrDefault();

            if (payrollStart == null)
                return;

            if (payrollEnd == null)
                payrollEnd = payrollStart;

            for (int year = payrollStart.Year.Value; year <= payrollEnd.Year.Value; year++)
            {
                ddlYear.Items.Add(year.ToString());
            }



            for (int month = 1; month <= 12; month++)
            {
                ListItem item = new ListItem(DateHelper.GetMonthShortName(month, IsEnglish), month.ToString());
                ddlMonth.Items.Add(item);
            }

            //select last 
            ddlYear.ClearSelection();

            ListItem selItem = ddlYear.Items.FindByValue(payrollEnd.Year.ToString());
            selItem.Selected = true;

            ddlMonth.ClearSelection();
            selItem = ddlMonth.Items.FindByValue(payrollEnd.Month.ToString());
            selItem.Selected = true;
        }

        public static PayrollPeriod GetPayrollPeriod(int payrollPeriodId)
        {
            return PayrollDataContext.PayrollPeriods.SingleOrDefault(e => e.PayrollPeriodId == payrollPeriodId);
        }

        public static bool IsPayrollPeriodAlreadyExists(int payrollPeriodId, string name)
        {
            PayrollPeriod dbEntity = PayrollDataContext.PayrollPeriods.SingleOrDefault
                  (e => (e.Name == name && e.CompanyId == SessionManager.CurrentCompanyId && e.PayrollPeriodId != payrollPeriodId));

            if (dbEntity != null)
                return true;
            return false;
        }

        public static PayrollPeriod GetFirstPayrollPeriod(int companyId)
        {
            return PayrollDataContext.PayrollPeriods.Where(e => e.CompanyId == companyId)
                .Take(1).SingleOrDefault();
        }

        public static PayrollPeriod GetPayrollPeriod(int month, int year)
        {
            PayrollPeriod dbEntity = PayrollDataContext.PayrollPeriods.SingleOrDefault
                  (e => (e.Month == month && e.Year == year && e.CompanyId == SessionManager.CurrentCompanyId));

            return dbEntity;
        }
        public static PayrollPeriod GetPreviousPayrollPeriod(int payrollPeriodId)
        {
            PayrollPeriod dbEntity = PayrollDataContext.PayrollPeriods
                .OrderByDescending(x => x.PayrollPeriodId)
                .Where(e => e.PayrollPeriodId < payrollPeriodId && e.CompanyId == SessionManager.CurrentCompanyId)
                .Take(1).SingleOrDefault();


            return dbEntity;
        }


        public static PayrollPeriod GetNextPayrollPeriod(int currentPayrollPeriodId)
        {

            PayrollPeriod period = null;

            if (currentPayrollPeriodId == 0)
                period = CommonManager.GetLastPayrollPeriod();
            else
                period = GetPayrollPeriod(currentPayrollPeriodId);

            string nextStarting;
            CustomDate date;
            if (period != null)
            {
                date = CustomDate.GetCustomDateFromString(period.EndDate, IsEnglish);
                nextStarting = date.IncrementByOneDay().ToString();

            }
            //is the first payroll period
            else
            {
                return null;
            }
            date = CustomDate.GetCustomDateFromString(nextStarting, IsEnglish);


            PayrollPeriod newPayrollPeriod = new PayrollPeriod();
            newPayrollPeriod.CompanyId = SessionManager.CurrentCompanyId;
            newPayrollPeriod.Name = DateHelper.GetMonthName(date.Month, IsEnglish) + "/" + date.Year;
            newPayrollPeriod.Month = date.Month;
            newPayrollPeriod.Year = date.Year;
            newPayrollPeriod.FinancialDateId = SessionManager.CurrentCompanyFinancialDate.FinancialDateId;

            newPayrollPeriod.TotalDays = DateHelper.GetTotalDaysInTheMonth(newPayrollPeriod.Year.Value, newPayrollPeriod.Month,
                                                                    SessionManager.CurrentCompany.IsEnglishDate);
            newPayrollPeriod.StartDate = new CustomDate(1, newPayrollPeriod.Month, newPayrollPeriod.Year.Value, IsEnglish).ToString(); //"1/" + newPayrollPeriod.Month + "/" + newPayrollPeriod.Year.Value;
            newPayrollPeriod.StartDateEng = GetEngDate(newPayrollPeriod.StartDate, null);
            newPayrollPeriod.EndDate = new CustomDate(newPayrollPeriod.TotalDays.Value, newPayrollPeriod.Month, newPayrollPeriod.Year.Value, IsEnglish).ToString(); //newPayrollPeriod.TotalDays + "/" + newPayrollPeriod.Month + "/" + newPayrollPeriod.Year.Value;
            newPayrollPeriod.EndDateEng = GetEngDate(newPayrollPeriod.EndDate, null);

            // Check if Payroll Period already exists then return null
            if (CommonManager.GetPayrollPeriod(newPayrollPeriod.Month, newPayrollPeriod.Year.Value) != null)
                return null;

            return newPayrollPeriod;
        }

        /// <summary>
        /// Generates the PayrollPeriod automatically at the time of Salary final save
        /// </summary>
        public static void CreateNewPayrollPeriodAutomaticallyInFinalSave(int currentPayrollPeriodId)
        {
            PayrollPeriod period = GetNextPayrollPeriod(currentPayrollPeriodId);
            if (period == null)
                return;

            CommonManager mgr = new CommonManager();
            mgr.SavePayrollPeriod(period);

        }


        static bool IsMonthYearGreaterThan(CustomDate start, CustomDate end)
        {
            if (end.Year > start.Year)
                return true;
            else if (end.Year < start.Year)
                return false;

            if (end.Month >= start.Month)
                return true;
            else if (end.Month < start.Month)
                return false;

            return true;
        }

        /// <summary>
        /// Check if the payroll date is within the current fiscal year
        /// </summary>
        /// <param name="month"></param>
        /// <param name="year"></param>
        /// <returns></returns>
        public static bool IsValidPayrollPeriod(int month, int year)
        {
            CustomDate startDate = CustomDate.GetCustomDateFromString(SessionManager.CurrentCompanyFinancialDate.StartingDate,
               SessionManager.CurrentCompany.IsEnglishDate);
            CustomDate endDate = CustomDate.GetCustomDateFromString(SessionManager.CurrentCompanyFinancialDate.EndingDate,
                            SessionManager.CurrentCompany.IsEnglishDate);

            //int month = int.Parse(ddlMonths.SelectedValue);
            //int year = int.Parse(ddlYears.SelectedValue);
            CustomDate date = new CustomDate(1, month, year, SessionManager.CurrentCompany.IsEnglishDate);

            // Greater than startDate
            if (!IsMonthYearGreaterThan(startDate, date))
            {

                return false;
            }


            if (!IsMonthYearGreaterThan(date, endDate))
            {

                return false;
            }
            return true;
        }

        /// <summary>
        /// If we are already using new web services  AttendanceImportNew like atte import, auto scehdular email for late/absent case 
        /// and we want to secure AttendanceImport by not making accessible then we can set this value
        /// and calling without api key function will throw exception
        /// </summary>
        /// <returns></returns>
        public static bool IsOldAPIWithoutKeyAccessible()
        {

            bool? disable = PayrollDataContext.Settings.FirstOrDefault().DisabeOldAPIWithoutKey;
            if (disable != null && disable.Value)
                return false;

            return true;
        }

        public static void WriteToLogFile(string strMessage)
        {


            string TodayDate = GetCurrentDateAndTime().Date.ToString("MM-dd-yyyy");

            string logFile = AppDomain.CurrentDomain.BaseDirectory + @"\Log _" + TodayDate + ".txt";
            string outputFile = logFile;//"Log" + "_" + TodayDate + ".txt";
            // Write to the file:
            string line = DateTime.Now.ToString() + " | ";
            line += strMessage;

            FileStream fs = new FileStream(outputFile, FileMode.Append, FileAccess.Write, FileShare.None);
            StreamWriter swFromFileStream = new StreamWriter(fs);
            swFromFileStream.WriteLine(line);
            swFromFileStream.Flush();
            swFromFileStream.Close();
        }

        public bool SavePayrollPeriod(PayrollPeriod entity)
        {



            PayrollPeriod dbEntity = PayrollDataContext.PayrollPeriods.SingleOrDefault
                (e => (e.Name == entity.Name && e.CompanyId == entity.CompanyId));
            if (dbEntity != null)
            {
                if (dbEntity.PayrollPeriodId != entity.PayrollPeriodId)
                    return false;
            }
            else
            {
                // Is date is within the current fiscal year
                if (!IsValidPayrollPeriod(entity.Month, entity.Year.Value))
                    return false;
            }

            SetConnectionPwd(PayrollDataContext.Connection);
            PayrollDataContext.Transaction = PayrollDataContext.Connection.BeginTransaction();


            try
            {

                entity.Created = entity.Modified = DateTime.Now;
                entity.TotalDays = DateHelper.GetTotalDaysInTheMonth(entity.Year.Value, entity.Month,
                                                                     SessionManager.CurrentCompany.IsEnglishDate);
                entity.StartDate = new CustomDate(1, entity.Month, entity.Year.Value, IsEnglish).ToString();
                entity.StartDateEng = GetEngDate(entity.StartDate, null);
                entity.EndDate = new CustomDate(entity.TotalDays.Value, entity.Month, entity.Year.Value, IsEnglish).ToString();
                entity.EndDateEng = GetEngDate(entity.EndDate, null);



                #region "Change Logs"

                PayrollDataContext.ChangeSettings.InsertOnSubmit(GetSettingChangeLog((byte)MonetoryChangeLogTypeEnum.PayrollPeriod,
                    "", "-", entity.Name, LogActionEnum.Add));

                #endregion


                PayrollDataContext.PayrollPeriods.InsertOnSubmit(entity);
                PayrollDataContext.SubmitChanges();


                //Leave accured generate for all existing leaves of the company
                //List<LLeaveType> companyLeaves = PayrollDataContext.LLeaveTypes
                //    .Where(l => l.CompanyId == SessionManager.CurrentCompanyId).ToList();
                //foreach (var leave in companyLeaves)
                //{
                //    PayrollDataContext.LeaveGenerateAccured(leave.LeaveTypeId,
                //                                            null, SessionManager.CurrentCompanyId, true, true);
                //}

                PayrollDataContext.FiscalYearFirstPayrollPeriodCreation(entity.PayrollPeriodId, entity.CompanyId.Value);

                PayrollDataContext.Transaction.Commit();
            }
            catch (Exception exp)
            {
                Log.log("Error while saving payroll period", exp);
                PayrollDataContext.Transaction.Rollback();
                return false;
            }
            finally
            {
                //if (PayrollDataContext.Connection != null)
                //{
                //    PayrollDataContext.Connection.Close();
                //}
            }

            // clear current year PayrollPeriod list
            MyCache.Reset();

            return true;



            //else
            //{
            //    PayrollPeriod dbbEntity = PayrollDataContext.PayrollPeriods.SingleOrDefault(e => e.PayrollPeriodId == entity.PayrollPeriodId);
            //    dbbEntity.TotalDays = DateHelper.GetTotalDaysInTheMonth(entity.Year.Value, entity.Month,
            //        SessionManager.CurrentCompany.IsEnglishDate);
            //    dbbEntity.Name = entity.Name;
            //    dbbEntity.Month = entity.Month;
            //    dbbEntity.Year = entity.Year;

            //    dbbEntity.StartDate = "1/" + entity.Month + "/" + entity.Year.Value;
            //    dbbEntity.StartDateEng = GetEngDate(dbbEntity.StartDate, null);
            //    dbbEntity.EndDate = dbbEntity.TotalDays + "/" + entity.Month + "/" + entity.Year.Value;
            //    dbbEntity.EndDateEng = GetEngDate(dbbEntity.EndDate, null);
            //    dbbEntity.Modified = DateTime.Now;
            //    UpdateChangeSet();
            //    return true;
            //}
        }

        public static bool IsPayrollFinalSaved(int payrollPeriodId)
        {
            CCalculation calculation = PayrollDataContext.CCalculations.FirstOrDefault(x => x.PayrollPeriodId == payrollPeriodId);
            if (calculation != null)
            {
                return calculation.IsFinalSaved.Value;
            }
            return false;
        }

        /// <summary>
        /// Returns the last payroll period, can be finally saved also so if valid needed then use
        /// GetValidLastPayrollPeriod method
        /// </summary>
        /// <returns></returns>
        public static PayrollPeriod GetLastPayrollPeriod()
        {

            return PayrollDataContext.PayrollPeriods.OrderByDescending(x => x.PayrollPeriodId).FirstOrDefault();


        }

        /// <summary>
        /// Returns the last payroll period for the Employee in which Calculation is not saved
        /// </summary>
        /// <returns></returns>
        public static PayrollPeriod GetValidLastPayrollPeriod(int employeeId)
        {
            int currentFinancialDate = PayrollDataContext.FinancialDates.Where(
                f => f.CompanyId == SessionManager.CurrentCompanyId && f.IsCurrent == true)
                .First().FinancialDateId;

            PayrollPeriod payrollPeriod = null;

            // Get last Calculation saved PayrollPeriod
            PayrollPeriod lastSavedPayroll = (from p in PayrollDataContext.PayrollPeriods
                                              join c in PayrollDataContext.CCalculations on p.PayrollPeriodId equals c.PayrollPeriodId
                                              // Don't check here employee specific as Stopped Payment employee will not come
                                              join e in PayrollDataContext.CCalculationEmployees on c.CalculationId equals e.CalculationId

                                              //where e.EmployeeId == employeeId
                                              orderby p.PayrollPeriodId descending
                                              select p
             ).Take(1).SingleOrDefault();

            if (lastSavedPayroll == null)
            {
                payrollPeriod =
                    PayrollDataContext.PayrollPeriods.Where(
                        e => e.FinancialDateId == currentFinancialDate)
                        .OrderByDescending(e => e.PayrollPeriodId).FirstOrDefault();
            }
            // else get the PayrollPeriod after save PayrollPeriod
            else
            {
                payrollPeriod =
                    PayrollDataContext.PayrollPeriods.Where(
                        e => e.FinancialDateId == currentFinancialDate && e.PayrollPeriodId > lastSavedPayroll.PayrollPeriodId)
                        .OrderBy(e => e.PayrollPeriodId).FirstOrDefault();
            }


            if (payrollPeriod == null)
                return null;


            if (CalculationManager.IsAllEmployeeSavedFinally(payrollPeriod.PayrollPeriodId, false))
                return null;

            return payrollPeriod;
            //.OrderByDescending(e => e.Month)
            //.OrderByDescending(e => e.Year).ToList();))

        }

        public static PayrollPeriod GetValidLastPayrollPeriodForIncrement(int employeeId)
        {
            int currentFinancialDate = PayrollDataContext.FinancialDates.Where(
                f => f.CompanyId == SessionManager.CurrentCompanyId && f.IsCurrent == true)
                .First().FinancialDateId;

            PayrollPeriod payrollPeriod = null;

            // Get last Calculation saved PayrollPeriod
            PayrollPeriod lastSavedPayroll = (from p in PayrollDataContext.PayrollPeriods
                                              join c in PayrollDataContext.CCalculations on p.PayrollPeriodId equals c.PayrollPeriodId
                                              // Don't check here employee specific as Stopped Payment employee will not come
                                              join e in PayrollDataContext.CCalculationEmployees on c.CalculationId equals e.CalculationId

                                              where e.EmployeeId == employeeId
                                              orderby p.PayrollPeriodId descending
                                              select p
             ).Take(1).SingleOrDefault();

            if (lastSavedPayroll == null)
            {
                payrollPeriod =
                    PayrollDataContext.PayrollPeriods.Where(
                        e => e.FinancialDateId == currentFinancialDate)
                        .OrderByDescending(e => e.PayrollPeriodId).FirstOrDefault();
            }
            // else get the PayrollPeriod after save PayrollPeriod
            else
            {
                payrollPeriod =
                    PayrollDataContext.PayrollPeriods.Where(
                        e => e.FinancialDateId == currentFinancialDate && e.PayrollPeriodId > lastSavedPayroll.PayrollPeriodId)
                        .OrderBy(e => e.PayrollPeriodId).FirstOrDefault();
            }


            if (payrollPeriod == null)
                return null;


            if (CalculationManager.IsAllEmployeeSavedFinally(payrollPeriod.PayrollPeriodId, false))
                return null;

            return payrollPeriod;
            //.OrderByDescending(e => e.Month)
            //.OrderByDescending(e => e.Year).ToList();))

        }
        public static PayrollPeriod RetiredValidPayrollPeriod(int employeeId)
        {
            PayrollPeriod payroll = GetLastPayrollPeriod();
            if (payroll == null)
                return null;

            bool isSavedForThisPayroll =
                (
                from ce in PayrollDataContext.CCalculationEmployees
                join c in PayrollDataContext.CCalculations on ce.CalculationId equals c.CalculationId
                where c.PayrollPeriodId == payroll.PayrollPeriodId && ce.EmployeeId == employeeId
                select ce
                ).Any();

            if (isSavedForThisPayroll)
                return null;

            return payroll;

        }

        /// <summary>
        /// Returns the last payroll period which is not final saved
        /// </summary>
        /// <returns></returns>
        public static PayrollPeriod GetValidLastPayrollPeriod()
        {
            int currentFinancialDate = PayrollDataContext.FinancialDates.Where(
                f => f.CompanyId == SessionManager.CurrentCompanyId && f.IsCurrent == true)
                .First().FinancialDateId;


            PayrollPeriod payrollPeriod =
                PayrollDataContext.PayrollPeriods.Where(
                    e => e.FinancialDateId == currentFinancialDate)
                    .OrderByDescending(e => e.PayrollPeriodId).FirstOrDefault();

            if (payrollPeriod == null)
                return null;


            if (CalculationManager.IsAllEmployeeSavedFinally(payrollPeriod.PayrollPeriodId, false))
                return null;

            return payrollPeriod;
            //.OrderByDescending(e => e.Month)
            //.OrderByDescending(e => e.Year).ToList();))

        }

        public int GetTotalDays(int payrollPeriodId)
        {
            var q = from e in PayrollDataContext.PayrollPeriods
                    where e.PayrollPeriodId == payrollPeriodId
                    select new { TotalDays = e.TotalDays };

            foreach (var c in q)
                return c.TotalDays.Value;
            return 0;
        }

        public static List<BLL.BO.PayrollPeriodBO> GetCurrentYear(int companyId)
        {
            List<BLL.BO.PayrollPeriodBO> list
                = GetFromCache<List<BLL.BO.PayrollPeriodBO>>(CacheKey.CurrentYearPayrollPeriodList.ToString() + "Converted" + companyId);

            if (list != null)
                return list;

            list = (from p in PayrollDataContext.PayrollPeriods
                    join f in PayrollDataContext.FinancialDates on p.FinancialDateId equals f.FinancialDateId
                    where f.IsCurrent == true && f.CompanyId == companyId
                    orderby p.PayrollPeriodId
                    select new BLL.BO.PayrollPeriodBO { Name = p.Name, PayrollPeriodId = p.PayrollPeriodId }).ToList();

            SaveToCache<List<BLL.BO.PayrollPeriodBO>>(CacheKey.CurrentYearPayrollPeriodList.ToString() + "Converted" + companyId, list);

            return list;
        }

        public static List<BLL.BO.PayrollPeriodBO> GetYear(int yearId)
        {

            List<BLL.BO.PayrollPeriodBO> list = (from p in PayrollDataContext.PayrollPeriods
                                                 join f in PayrollDataContext.FinancialDates on p.FinancialDateId equals f.FinancialDateId
                                                 where f.FinancialDateId == yearId
                                                 orderby p.PayrollPeriodId
                                                 select new BLL.BO.PayrollPeriodBO { Name = p.Name, PayrollPeriodId = p.PayrollPeriodId }).ToList();



            return list;
        }

        public static List<BLL.BO.PayrollPeriodBO> GetAllYear(int companyId)
        {

            return (from p in PayrollDataContext.PayrollPeriods
                    join f in PayrollDataContext.FinancialDates on p.FinancialDateId equals f.FinancialDateId
                    where f.CompanyId == companyId
                    orderby p.PayrollPeriodId
                    select new BLL.BO.PayrollPeriodBO { Name = p.Name, PayrollPeriodId = p.PayrollPeriodId }).ToList();



        }

        public static bool IsLastPeriod(int payrollPeriodId)
        {
            PayrollPeriod lastPeriod = PayrollDataContext.PayrollPeriods
                .Where(x => x.CompanyId == SessionManager.CurrentCompanyId)
                .OrderByDescending(x => x.EndDateEng)
                .Take(1).SingleOrDefault();

            if (lastPeriod != null)
            {
                return lastPeriod.PayrollPeriodId == payrollPeriodId;
            }

            return false;
        }

        public static string DeleteLastPeriod(int payrollPeriodId)
        {

            if (IsLastPeriod(payrollPeriodId))
            {
                PayrollPeriod period = GetPayrollPeriod(payrollPeriodId);
                PayrollDataContext.PayrollPeriods.DeleteOnSubmit(period);

                // unlock ot and allowance also
                List<OvertimeRequest> list = PayrollDataContext.OvertimeRequests.Where(x => x.PayrollPeriodId == payrollPeriodId).ToList();
                foreach (var item in list)
                    item.PayrollPeriodId = null;

                List<EveningCounterRequest> list11 = PayrollDataContext.EveningCounterRequests.Where(x => x.PayrollPeriodId == payrollPeriodId).ToList();
                foreach (var item in list11)
                    item.PayrollPeriodId = null;


                List<AttendanceDeduction> list2 = PayrollDataContext.AttendanceDeductions.Where(x => x.PayrollPeriodId == payrollPeriodId).ToList();
                PayrollDataContext.AttendanceDeductions.DeleteAllOnSubmit(list2);

                PayrollDataContext.ChangeSettings.InsertOnSubmit(GetSettingChangeLog((byte)MonetoryChangeLogTypeEnum.PayrollPeriod,
                   period.Name + " period deleted.", "", "", LogActionEnum.Delete));

                PayrollDataContext.SubmitChanges();
                return "Payroll Period successfully deleted.";
            }

            return "Only last Payroll Period can be deleted.";
        }
        public static string UnlockLastPeriod(int payrollPeriodId)
        {

            if (IsLastPeriod(payrollPeriodId))
            {
                PayrollPeriod period = CommonManager.GetPayrollPeriod(payrollPeriodId);

                CCalculation calc = PayrollDataContext.CCalculations.SingleOrDefault(x => x.PayrollPeriodId == payrollPeriodId);
                if (calc == null)
                {
                    return "Payroll Period successfully unlocked.";
                }
                calc.IsFinalSaved = false;

                List<CCalculationEmployee> list = PayrollDataContext.CCalculationEmployees.Where(x => x.CalculationId == calc.CalculationId).ToList();
                foreach (CCalculationEmployee emp in list)
                {
                    emp.IsFinalSaved = false;
                }

                PayrollDataContext.ChangeSettings.InsertOnSubmit(GetSettingChangeLog((byte)MonetoryChangeLogTypeEnum.PayrollPeriod,
                 period.Name + " period salary unlocked.", "", "", LogActionEnum.Update));

                PayrollDataContext.SubmitChanges();
            }

            return "Payroll Period successfully unlocked.";
        }
        public static List<System.Web.UI.WebControls.ListItem> AppendMiddleTaxPaidList(List<PayrollPeriodBO> list, bool appendPartial
            , bool isPayslip)
        {
            List<System.Web.UI.WebControls.ListItem> itemList = new List<ListItem>();

            List<PartialTax> partialList = PayrollDataContext.PartialTaxes.OrderBy(x => x.PayrollPeriodId).ToList();
            List<AddOn> addOnList = PayrollDataContext.AddOns.ToList();

            foreach (var item in addOnList)
            {
                if (item.ShowAfterPayrollPeriodId == null || item.ShowAfterPayrollPeriodId == -1 || item.ShowAfterPayrollPeriodId == 0)
                    item.ShowAfterPayrollPeriodId = item.PayrollPeriodId;
            }

            for (int i = list.Count - 1; i >= 0; i--)
            {
                if (appendPartial)
                {
                    PartialTax tax = partialList.FirstOrDefault(x => x.ShowAfterPayrollPeriodId == list[i].PayrollPeriodId);

                    if (tax != null)
                    {
                        // old payroll period
                        if (isPayslip == false || (tax.ShowInPayslip != null && tax.ShowInPayslip.Value))
                            itemList.Insert(0, new ListItem { Text = "__" + tax.Name, Value = tax.PayrollPeriodId.ToString() + ":" + ((int)PayrollPeriodType.OldAddOn) });
                    }

                    // New Add On
                    List<AddOn> periodNewAddon = addOnList.Where(x =>
                         x.ShowAfterPayrollPeriodId == list[i].PayrollPeriodId).OrderByDescending(x => x.AddOnId).ToList();

                    foreach (AddOn addOn in periodNewAddon)
                    {
                        if (isPayslip == false || (addOn.ShowInPaySlip != null && addOn.ShowInPaySlip.Value))
                            itemList.Insert(0, new ListItem { Text = "__" + addOn.Name, Value = addOn.PayrollPeriodId.ToString() + ":" + ((int)PayrollPeriodType.NewAddOn) + ":" + addOn.AddOnId });
                    }
                }

                // normal payroll period
                itemList.Insert(0, new ListItem { Text = list[i].Name, Value = list[i].PayrollPeriodId.ToString() + ":" + ((int)PayrollPeriodType.Normal) });
            }

            return itemList;
        }

        public static List<PayrollPeriod> GetCurrentYearPayrollList(int companyId)
        {
            List<PayrollPeriod> list
                = GetFromCache<List<PayrollPeriod>>(CacheKey.CurrentYearPayrollPeriodList.ToString() + companyId);

            if (list != null)
                return list;

            list = (from p in PayrollDataContext.PayrollPeriods
                    join f in PayrollDataContext.FinancialDates on p.FinancialDateId equals f.FinancialDateId
                    where f.IsCurrent == true && f.CompanyId == companyId
                    orderby p.PayrollPeriodId
                    select p).ToList();

            SaveToCache<List<PayrollPeriod>>(CacheKey.CurrentYearPayrollPeriodList.ToString() + companyId, list);

            return list;
        }

        #endregion


        #region "Financial dates"

        public static FinancialDate GetCurrentFinancialYear()
        {
            int companyId = SessionManager.CurrentCompanyId;
            var query = from f in PayrollDataContext.FinancialDates
                        where f.IsCurrent == true && f.CompanyId == companyId
                        select f;

            return query.FirstOrDefault();
        }

        public static List<FinancialDate> GetAllYears()
        {
            return PayrollDataContext.FinancialDates.OrderBy(x => x.FinancialDateId).ToList();
        }

        public static List<PayrollPeriod> GetYearPayrollPeriods(int yearId)
        {
            return
                PayrollDataContext.PayrollPeriods.Where(x => x.FinancialDateId == yearId).ToList()
                .OrderBy(x => x.PayrollPeriodId).ToList();
        }

        public List<FinancialDate> GetAllFinancialDates()
        {
            int cid = SessionManager.CurrentCompanyId;
            return
                PayrollDataContext.FinancialDates.Where(f => f.CompanyId == cid)
                    .OrderBy(f => f.FinancialDateId).ToList();
        }

        public static FinancialDate GetFinancialYearByDate(DateTime startDateEng, DateTime endDateEng)
        {

            FinancialDate date =
                PayrollDataContext.FinancialDates.Where(f => startDateEng >= f.StartingDateEng && startDateEng <= f.EndingDateEng)
                .Take(1).SingleOrDefault();

            if (date != null)
                return date;

            date =
                PayrollDataContext.FinancialDates.Where(f => endDateEng >= f.StartingDateEng && endDateEng <= f.EndingDateEng)
                .Take(1).SingleOrDefault();

            return date;
        }

        public static FinancialDate GetFiancialDateById(int id)
        {
            return
                PayrollDataContext.FinancialDates.Where(f => f.FinancialDateId == id).SingleOrDefault();
        }

        public static void UpdateFinancialDate(FinancialDate date)
        {
            // clear current year PayrollPeriod list
            DeleteFromCache(CacheKey.CurrentYearPayrollPeriodList.ToString() + SessionManager.CurrentCompanyId);

            FinancialDate dbEntity =
                PayrollDataContext.FinancialDates.Where(i => i.FinancialDateId == date.FinancialDateId).SingleOrDefault();

            if (dbEntity == null)
                return;


            #region "Change Logs"
            if (GetFinancialYearLog(dbEntity) != GetFinancialYearLog(date))
                PayrollDataContext.ChangeSettings.InsertOnSubmit(GetSettingChangeLog((byte)MonetoryChangeLogTypeEnum.FinancialYear,
                    "", GetFinancialYearLog(dbEntity), GetFinancialYearLog(date), LogActionEnum.Update));

            #endregion

            dbEntity.StartingDate = date.StartingDate;
            dbEntity.StartingDateEng = GetEngDate(date.StartingDate, SessionManager.IsEnglish);
            dbEntity.EndingDate = date.EndingDate;
            dbEntity.EndingDateEng = GetEngDate(date.EndingDate, SessionManager.IsEnglish);

            // if single period exists then edit it
            List<PayrollPeriod> periods = PayrollDataContext.PayrollPeriods.ToList();
            if (periods.Count == 1)
            {
                PayrollPeriod period = periods[0];
                // if start date has been changed then update period with new month date
                if (period.StartDateEng != dbEntity.StartingDateEng)
                {
                    period.StartDate = dbEntity.StartingDate;
                    period.StartDateEng = dbEntity.StartingDateEng;

                    CustomDate startDate = CustomDate.GetCustomDateFromString(period.StartDate, IsEnglish);
                    CustomDate endDate = new CustomDate(
                        DateHelper.GetTotalDaysInTheMonth(startDate.Year, startDate.Month, IsEnglish),
                        startDate.Month, startDate.Year, IsEnglish);

                    period.EndDate = endDate.ToString();
                    period.EndDateEng = endDate.EnglishDate;

                    period.TotalDays = DateHelper.GetTotalDaysInTheMonth(startDate.Year, startDate.Month, IsEnglish);
                    period.Month = startDate.Month;
                    period.Year = startDate.Year;

                    period.Name = DateHelper.GetMonthName(startDate.Month, IsEnglish) + "/" + startDate.Year.ToString();

                }
            }

            UpdateChangeSet();
        }

        public static string GetFinancialYearLog(FinancialDate date)
        {
            return date.StartingDate + "-" + date.EndingDate;
        }

        public static bool HasSingleYear()
        {
            return PayrollDataContext.FinancialDates.Count() <= 1;
        }

        public static bool CreateFinancialDate(FinancialDate date)
        {
            // clear current year PayrollPeriod list
            DeleteFromCache(CacheKey.CurrentYearPayrollPeriodList.ToString() + SessionManager.CurrentCompanyId);

            date.StartingDateEng = GetEngDate(date.StartingDate, SessionManager.IsEnglish);
            date.EndingDateEng = GetEngDate(date.EndingDate, SessionManager.IsEnglish);
            date.IsCurrent = true;

            FinancialDate current = PayrollDataContext.FinancialDates.Where(
                f => f.IsCurrent == true && f.CompanyId == SessionManager.CurrentCompanyId).SingleOrDefault();
            current.IsCurrent = false;

            SetConnectionPwd(PayrollDataContext.Connection);
            PayrollDataContext.Transaction = PayrollDataContext.Connection.BeginTransaction();

            try
            {

                #region "Change Logs"
                PayrollDataContext.ChangeSettings.InsertOnSubmit(GetSettingChangeLog((byte)MonetoryChangeLogTypeEnum.FinancialYear,
                    "", "-", GetFinancialYearLog(date), LogActionEnum.Add));

                #endregion


                PayrollDataContext.FinancialDates.InsertOnSubmit(date);
                PayrollDataContext.SubmitChanges();

                PayrollDataContext.CalcNewYearCreatedProcessing(date.FinancialDateId);

                PayrollDataContext.Transaction.Commit();
            }
            catch (Exception exp)
            {
                Log.log("Error while creating financial date : " + date.StartingDate, exp);
                PayrollDataContext.Transaction.Rollback();
                return false;
            }
            finally
            {
                //if (PayrollDataContext.Connection != null)
                //{
                //    PayrollDataContext.Connection.Close();
                //}
            }


            return true;
        }

        #endregion

        #region "Bonus"

        public static List<BonusBO> GetBonusList()
        {
            List<BonusBO> list =

                (
                    from b in PayrollDataContext.Bonus
                    join p in PayrollDataContext.PayrollPeriods on b.PaidPayrollPeriodId equals p.PayrollPeriodId
                        into temp
                    from newTemp in temp.DefaultIfEmpty()

                    orderby b.YearID
                    select new BonusBO
                    {
                        BonusId = b.BonusId,
                        YearID = b.YearID,
                        BonusAmount = b.BonusAmount,
                        DistributedAmount = ((b.Status != null && b.Status.Value == 1) ?
                            PayrollDataContext.BonusEmployees.Where(x => x.BonusId == b.BonusId).Sum(x => x.BonusToBePaid) : null
                        ),
                        Name = b.Name,
                        DistributionDateEng = b.DistributionDateEng,
                        NoOfEmployees = PayrollDataContext.BonusEmployees.Where(
                            x => x.BonusId == b.BonusId && (x.Eligibility == (int)BonusEligiblity.Full || x.Eligibility == (int)BonusEligiblity.Proportionate)).Count()
                            ,
                        TotalAnnualSalary = b.TotalAnnualSalary,
                        BonusPercent = b.BonusPercent,

                        StartDate = b.StartDate,
                        EndDate = b.EndDate,
                        PaidPeriod = (newTemp == null ? "" : newTemp.Name)
                    }
                ).ToList();

            //foreach (BonusBO item in list)
            //{
            //    FinancialDate date = PayrollDataContext.FinancialDates.FirstOrDefault(x => x.FinancialDateId ==
            //        item.YearID);
            //    date.SetName(IsEnglish);
            //    item.Year = date.Name;
            //}

            return list;

        }
        public static List<OvertimeBO> GetOvertimeList()
        {
            List<OvertimeBO> list =

                (
                    from b in PayrollDataContext.OvertimePeriods

                    join l in PayrollDataContext.PayrollPeriods on b.PaidPayrollPeriodId equals l.PayrollPeriodId into dd
                    from ddd in dd.DefaultIfEmpty()

                        //join p in PayrollDataContext.PayrollPeriods on b.PaidPayrollPeriodId equals p.PayrollPeriodId

                    orderby b.StartDate
                    select new OvertimeBO
                    {
                        OvertimeID = b.OvertimeID,
                        StartDate = b.StartDate,
                        EndDate = b.EndDate,
                        Name = b.Name,
                        Period = (ddd == null ? "" : ddd.Name),
                        Status = b.Status
                    }
                ).ToList();

            //foreach (BonusBO item in list)
            //{
            //    FinancialDate date = PayrollDataContext.FinancialDates.FirstOrDefault(x => x.FinancialDateId ==
            //        item.YearID);
            //    date.SetName(IsEnglish);
            //    item.Year = date.Name;
            //}

            return list;

        }
        public static List<FestivalPaymentPeriod> GetFestivalPeriodList()
        {
            List<FestivalPaymentPeriod> list =

                (
                    from b in PayrollDataContext.FestivalPaymentPeriods
                    orderby b.PayrollPeriodId
                    select b
                ).ToList();

            //foreach (BonusBO item in list)
            //{
            //    FinancialDate date = PayrollDataContext.FinancialDates.FirstOrDefault(x => x.FinancialDateId ==
            //        item.YearID);
            //    date.SetName(IsEnglish);
            //    item.Year = date.Name;
            //}

            return list;

        }
        public static List<BonusIneligibleBO> GetBonusIneligibleList(int bonusId)
        {
            List<BonusIneligibleBO> list =

                (
                    from b in PayrollDataContext.BonusIneligibleEmployees
                    join e in PayrollDataContext.EEmployees on b.EmployeeId equals e.EmployeeId
                    where b.BonusId == bonusId
                    orderby e.Name
                    select new BonusIneligibleBO
                    {
                        BonusId = b.BonusId,
                        EIN = e.EmployeeId,
                        Name = e.Name + " - " + e.EmployeeId.ToString(),
                        Reason = b.Reason
                    }
                ).ToList();

            //foreach (BonusBO item in list)
            //{
            //    FinancialDate date = PayrollDataContext.FinancialDates.FirstOrDefault(x => x.FinancialDateId ==
            //        item.YearID);
            //    date.SetName(IsEnglish);
            //    item.Year = date.Name;
            //}

            return list;

        }
        public static List<BonusGetEligibilityEmployeesResult> GetEmployeeForEligibility(
            int departmentId, int branchId)
        {
            return PayrollDataContext.BonusGetEligibilityEmployees(SessionManager.CurrentCompanyId,
                                                                   branchId, departmentId).ToList();
        }

        #endregion

        #region "Company Settings"

        //public static Setting _setting = null;

        public static Setting Setting
        {
            //set { }
            get
            {
                Setting _setting = GetFromCache<Setting>("SettingCache");
                if (_setting == null)
                {
                    _setting = PayrollDataContext.Settings.FirstOrDefault();
                    SaveToCache<Setting>("SettingCache", _setting);
                }
                return _setting;
            }
        }

        //private static BLL.BO.CompanySetting _CompanySetting = null;
        public static BLL.BO.CompanySetting CompanySetting
        {
            //set
            //{
            //    _CompanySetting = value;
            //}
            get
            {
                BO.CompanySetting _CompanySetting = GetFromCache<BO.CompanySetting>("CompanySettingCache");



                // Comment below like for setting change testing
                if (_CompanySetting == null)
                {
                    _CompanySetting = new BO.CompanySetting();

                    List<DAL.CompanySetting> settings = PayrollDataContext.CompanySettings.ToList();
                    DAL.CompanySetting setting;
                    //setting = settings.Where(s => s.Key == "IsD2").SingleOrDefault();
                    //_CompanySetting.IsD2 = setting == null ? false : bool.Parse(setting.Value);


                    setting = settings.Where(s => s.Key == "AttendanceShowLeaveImportInRegistry").SingleOrDefault();
                    _CompanySetting.AttendanceShowLeaveImportInRegistry = setting == null ? false : bool.Parse(setting.Value);

                    setting = settings.Where(s => s.Key == "LeaveRequestToOnlyOneSupervisor").SingleOrDefault();
                    _CompanySetting.LeaveRequestToOnlyOneSupervisor = setting == null ? false : bool.Parse(setting.Value);

                    setting = settings.Where(s => s.Key == "IsNset").SingleOrDefault();
                    _CompanySetting.IsNset = setting == null ? false : bool.Parse(setting.Value);

                    setting = settings.Where(s => s.Key == "IsStatusDownGradeEnable").SingleOrDefault();
                    _CompanySetting.IsStatusDownGradeEnable = setting == null ? false : bool.Parse(setting.Value);

                    setting = settings.Where(s => s.Key == "HasHalfHourlyLeave").SingleOrDefault();
                    _CompanySetting.HasHalfHourlyLeave = setting == null ? false : bool.Parse(setting.Value);
                    setting = settings.Where(s => s.Key == "IsEncashLeaveTypeBeNegativeBalance").SingleOrDefault();
                    _CompanySetting.IsEncashLeaveTypeBeNegativeBalance = setting == null ? false : bool.Parse(setting.Value);

                    setting = settings.Where(s => s.Key == "IsEncashLapseLeaveTypeBeNegativeBalance").SingleOrDefault();
                    _CompanySetting.IsEncashLapseLeaveTypeBeNegativeBalance = setting == null ? false : bool.Parse(setting.Value);

                    setting = settings.Where(s => s.Key == "IsLapseLeaveBeNegativeBalance").SingleOrDefault();
                    _CompanySetting.IsLapseLeaveBeNegativeBalance = setting == null ? false : bool.Parse(setting.Value);


                    //To counnd holiday in between leave as Holiday as not, default is Yes
                    setting = settings.Where(s => s.Key == "LeaveApprovalMinimumPastDays").SingleOrDefault();
                    _CompanySetting.LeaveApprovalMinimumPastDays = setting == null ? 0 : int.Parse(setting.Value);

                    setting = settings.Where(s => s.Key == "LeaveRequestMinimumPastDays").SingleOrDefault();
                    _CompanySetting.LeaveRequestMinimumPastDays = setting == null ? 0 : int.Parse(setting.Value);

                    setting = settings.Where(s => s.Key == "LeaveRequestCountHolidayInBetweenLeave").SingleOrDefault();
                    _CompanySetting.LeaveRequestCountHolidayInBetweenLeave = setting == null ? true : bool.Parse(setting.Value);


                    //if the project is of Percent or Input type, default is Percent type
                    setting = settings.Where(s => s.Key == "IsProjectInputType").SingleOrDefault();
                    _CompanySetting.IsProjectInputType = setting == null ? true : bool.Parse(setting.Value);

                    // to user Optimum PF also or not as in normal case Optimum CIT is only used
                    setting = settings.Where(s => s.Key == "UseOptimumPF").SingleOrDefault();
                    _CompanySetting.UseOptimumPF = setting == null ? false : bool.Parse(setting.Value);

                    //if the project is of Percent or Input type, default is Percent type
                    setting = settings.Where(s => s.Key == "LeaveRequestNegativeLeaveReqBalanceNotAllowed").SingleOrDefault();
                    _CompanySetting.LeaveRequestNegativeLeaveReqBalanceNotAllowed = setting == null ? false : bool.Parse(setting.Value);

                    //if the project is of Percent or Input type, default is Percent type
                    setting = settings.Where(s => s.Key == "LeaveHasRequestOrder").SingleOrDefault();
                    _CompanySetting.LeaveHasRequestOrder = setting == null ? false : bool.Parse(setting.Value);

                    setting = settings.Where(s => s.Key == "DisableChangeLog").SingleOrDefault();
                    _CompanySetting.DisableChangeLog = setting == null ? false : bool.Parse(setting.Value);



                    setting = settings.Where(s => s.Key == "RemoveStyleWhileExportingReport").SingleOrDefault();
                    _CompanySetting.RemoveStyleWhileExportingReport = setting == null ? false : bool.Parse(setting.Value);


                    setting = settings.Where(s => s.Key == "LeaveRequestValidMonths").SingleOrDefault();
                    _CompanySetting.LeaveRequestValidMonths = setting == null ? 2 : int.Parse(setting.Value);



                    setting = settings.Where(s => s.Key == "Company").SingleOrDefault();
                    _CompanySetting.WhichCompany = setting == null ? WhichCompany.Default : ((WhichCompany)int.Parse(setting.Value));

                    setting = settings.Where(s => s.Key == "IsHRInOppositeDate").SingleOrDefault();
                    _CompanySetting.IsHRInOppositeDate = setting == null ? false : bool.Parse(setting.Value);



                    setting = settings.Where(s => s.Key == "HideBranchIfOnlyOne").SingleOrDefault();
                    _CompanySetting.HideBranchIfOnlyOne = setting == null ? false : bool.Parse(setting.Value);

                    setting = settings.Where(s => s.Key == "EnableSalaryProcessDay").SingleOrDefault();
                    _CompanySetting.EnableSalaryProcessDay = setting == null ? false : bool.Parse(setting.Value);

                    setting = settings.Where(s => s.Key == "AppendTaxDetailsInPayslip").SingleOrDefault();
                    _CompanySetting.AppendTaxDetailsInPayslip = setting == null ? false : bool.Parse(setting.Value);

                    setting = settings.Where(s => s.Key == "IsAllIncomeInForeignCurrency").SingleOrDefault();
                    _CompanySetting.IsAllIncomeInForeignCurrency = setting == null ? false : bool.Parse(setting.Value);


                    setting = settings.Where(s => s.Key == "AttendanceInOut").SingleOrDefault();
                    _CompanySetting.AttendanceInOut = setting == null ? false : bool.Parse(setting.Value);

                    setting = settings.Where(s => s.Key == "HasLevelGradeSalary").SingleOrDefault();
                    _CompanySetting.HasLevelGradeSalary = setting == null ? false : bool.Parse(setting.Value);

                    setting = settings.Where(s => s.Key == "EnablePayrollSignFeature").SingleOrDefault();
                    _CompanySetting.EnablePayrollSignFeature = setting == null ? false : bool.Parse(setting.Value);


                    setting = settings.Where(s => s.Key == "RemoteAreaUsingProportionateDays").SingleOrDefault();
                    _CompanySetting.RemoteAreaUsingProportionateDays = setting == null ? false : bool.Parse(setting.Value);

                    setting = settings.Where(s => s.Key == "PayrollAllowNegativeSalarySave").SingleOrDefault();
                    _CompanySetting.PayrollAllowNegativeSalarySave = setting == null ? false : bool.Parse(setting.Value);

                    setting = settings.Where(s => s.Key == "MonthlySalaryBasedOnFixedDay").SingleOrDefault();
                    _CompanySetting.MonthlySalaryBasedOnFixedDay = setting == null ? 0 : decimal.Parse(setting.Value);


                    setting = settings.Where(s => s.Key == "MonthlySalaryBasedOnWeeklyHolidayDeduction").SingleOrDefault();
                    _CompanySetting.MonthlySalaryBasedOnWeeklyHolidayDeduction = setting == null ? false : bool.Parse(setting.Value);

                    setting = settings.Where(s => s.Key == "AttendanceAutoAbsentMarkingForSalary").SingleOrDefault();
                    _CompanySetting.AttendanceAutoAbsentMarkingForSalary = setting == null ? false : bool.Parse(setting.Value);


                    setting = settings.Where(s => s.Key == "DoNotUseSSTRecoveryFirst").SingleOrDefault();
                    _CompanySetting.DoNotUseSSTRecoveryFirst = setting == null ? false : bool.Parse(setting.Value);



                    setting = settings.Where(s => s.Key == "LeaveDeductPrevMonthUnpaidChange").SingleOrDefault();
                    _CompanySetting.LeaveDeductPrevMonthUnpaidChange = setting == null ? false : bool.Parse(setting.Value);
                    //setting = settings.Where(s => s.Key == "DateFormat_YYYY_MM_DD").SingleOrDefault();
                    //_CompanySetting.DateFormat_YYYY_MM_DD = setting == null ? false : bool.Parse(setting.Value);

                    //if (_CompanySetting.DateFormat_YYYY_MM_DD != Config.DateFormat_YYYY_MM_DD)
                    //{
                    //    try
                    //    {
                    //        HttpContext.Current.Response.Write("Date format of YYYY_MM_DD doesn't match between Company Setting & web.config.");
                    //        HttpContext.Current.Response.Flush();
                    //    }
                    //    catch
                    //    {

                    //    }
                    //}

                    SaveToCache<BO.CompanySetting>("CompanySettingCache", _CompanySetting);

                }
                return _CompanySetting;
            }
        }



        #endregion

        #region Admin Settings


        public static Boolean UpdateCalculationConstant(CalculationConstant CaCo)
        {
            CalculationConstant uCaCo = PayrollDataContext.CalculationConstants.FirstOrDefault();
            uCaCo.CompanyMonthlyHours = CaCo.CompanyMonthlyHours;
            uCaCo.CompanyLeaveHoursInADay = CaCo.CompanyLeaveHoursInADay;
            uCaCo.CompanyHasHourlyLeave = CaCo.CompanyHasHourlyLeave;
            uCaCo.CompanyIsHPL = CaCo.CompanyIsHPL;
            uCaCo.GratuityDaysInYear = CaCo.GratuityDaysInYear;
            uCaCo.GratuityPermanentYears = CaCo.GratuityPermanentYears;
            UpdateChangeSet();
            return true;
        }

        public static List<DAL.CompanySetting> GetCompanySetting()
        {
            return PayrollDataContext.CompanySettings.OrderBy(x => x.Key).ToList();
        }

        public static void UpdateCompanySetting(string key, string value)
        {
            DAL.CompanySetting setting = PayrollDataContext.CompanySettings.SingleOrDefault(x => x.Key == key);
            if (setting != null)
                setting.Value = value;
            else
            {
                setting = new DAL.CompanySetting();
                setting.Key = key;
                setting.Value = value;
                PayrollDataContext.CompanySettings.InsertOnSubmit(setting);
            }
            // PayrollDataContext.SubmitChanges();
        }

        public static void UpdateCompanySettingInCompanyCreation(SalaryWorkday workdayType, string workdayValue,
            bool remoteAreaUsingProportionateDays,
            bool doNotUseSSTRecoveryFirst,
            bool enableLevelwiseSalary,
            bool deductionPreviousMonthUPLAbsAttendnaceInCurrentMonthSalary)
        {
            UpdateCompanySetting("RemoteAreaUsingProportionateDays", remoteAreaUsingProportionateDays.ToString().ToLower());
            UpdateCompanySetting("DoNotUseSSTRecoveryFirst", doNotUseSSTRecoveryFirst.ToString().ToLower());
            UpdateCompanySetting("HasLevelGradeSalary", enableLevelwiseSalary.ToString().ToLower());
            UpdateCompanySetting("LeaveDeductPrevMonthUnpaidChange", deductionPreviousMonthUPLAbsAttendnaceInCurrentMonthSalary.ToString().ToLower());

            // workday change
            switch (workdayType)
            {
                case SalaryWorkday.DefaultAcutal:
                    UpdateCompanySetting("MonthlySalaryBasedOnFixedDays", "0");
                    UpdateCompanySetting("MonthlySalaryBasedOnWeeklyHolidayDeduction", "false");
                    break;
                case SalaryWorkday.BasedOnWeeklyHolidayDeduction:
                    UpdateCompanySetting("MonthlySalaryBasedOnFixedDays", "0");
                    UpdateCompanySetting("MonthlySalaryBasedOnWeeklyHolidayDeduction", "true");
                    break;
                case SalaryWorkday.FixedDays:
                    UpdateCompanySetting("MonthlySalaryBasedOnFixedDays", workdayValue);
                    UpdateCompanySetting("MonthlySalaryBasedOnWeeklyHolidayDeduction", "false");
                    break;
            }

            PayrollDataContext.SubmitChanges();
        }

        public static Boolean UpdateCompanySetting(List<DAL.CompanySetting> companysettings)
        {
            List<DAL.CompanySetting> cs = PayrollDataContext.CompanySettings.OrderBy(x => x.Key).ToList();
            for (int i = 0; i < cs.Count; i++)
            {
                cs[i].Value = companysettings[i].Value;
            }
            UpdateChangeSet();
            return true;
        }

        public static PayrollPeriod GetLastFinalSalaryPayrollPeriod()
        {
            return
                (
                from p in PayrollDataContext.PayrollPeriods
                join c in PayrollDataContext.CCalculations on p.PayrollPeriodId equals c.PayrollPeriodId
                where c.IsFinalSaved == true
                orderby p.PayrollPeriodId descending
                select p
                ).Take(1).SingleOrDefault();
        }

        public static void GetLastPayrollPeriod(ref bool readingSumForMiddleFiscalYearStartedReqd, ref int startingPayrollPeriodId, ref int endingPayrollPeriodId)
        {
            int payrollPeriodId = -1;
            startingPayrollPeriodId = -1;
            endingPayrollPeriodId = -1;
            readingSumForMiddleFiscalYearStartedReqd = false;
            PayrollPeriod payrollPeriod = CommonManager.GetLastPayrollPeriod();

            if (payrollPeriod != null)
            {
                payrollPeriodId = payrollPeriod.PayrollPeriodId;
                PayrollPeriod startingMonthParollPeriodInFiscalYear = CalculationManager.GenerateForPastIncome(
                         payrollPeriodId, ref readingSumForMiddleFiscalYearStartedReqd, ref startingPayrollPeriodId, ref endingPayrollPeriodId);
            }
        }
        #endregion


        #region "Currency setting"

        public static List<BO.CurrencyHistory> GetCurrencyHistory()
        {
            return
                (
                from c in PayrollDataContext.CCalculations
                join p in PayrollDataContext.PayrollPeriods on c.PayrollPeriodId equals p.PayrollPeriodId
                where p.CompanyId == SessionManager.CurrentCompanyId
                select new BO.CurrencyHistory
                {
                    Month = p.Name,
                    CurrentRate = c.CurrentRate,
                    FixedRate = c.FixedRate
                }
                ).ToList();
        }

        public static CurrencyRate GetCurrencyRate()
        {
            return PayrollDataContext.CurrencyRates.SingleOrDefault();
        }

        public static CurrencyRate GetCurrencyRateForSalarySavedPayroll(int payrollPeriod)
        {
            CCalculation calculation =
                PayrollDataContext.CCalculations.SingleOrDefault(p => p.PayrollPeriodId == payrollPeriod);
            if (calculation != null)
            {
                return new CurrencyRate { FixedRateDollar = calculation.FixedRate, CurrentRateDollar = calculation.CurrentRate };
            }
            return null;
        }

        public static bool UpdateCurrencyRate(CurrencyRate rate)
        {
            CurrencyRate dbEntity = GetCurrencyRate();
            if (dbEntity != null)
            {
                dbEntity.CurrentRateDollar = rate.CurrentRateDollar;
                dbEntity.FixedRateDollar = rate.FixedRateDollar;
            }
            else
            {
                PayrollDataContext.CurrencyRates.InsertOnSubmit(rate);
            }

            return UpdateChangeSet();

        }


        #endregion

        #region "Change Log"



        public static List<GetMonetoryChangeLogsResult> GetMonetoryChangeLogs(
            DateTime startDate, DateTime? endDate, string searchText, int currentPage, int? pageSize, ref int total)
        {

            currentPage -= 1;

            List<GetMonetoryChangeLogsResult> list =
                PayrollDataContext.GetMonetoryChangeLogs(currentPage, pageSize, SessionManager.CurrentCompanyId, startDate, endDate, searchText).ToList();

            total = 0;
            if (list.Count > 0)
                total = list[0].TotalRows.Value;

            return list;



        }
        public static List<GetMonetoryChangeLogsForSignOffResult> GetMonetoryChangeLogsForSignOff(
           DateTime startDate, DateTime? endDate, string searchText, int currentPage, int? pageSize, ref int total, int type)
        {

            currentPage -= 1;

            List<GetMonetoryChangeLogsForSignOffResult> list =
                PayrollDataContext.GetMonetoryChangeLogsForSignOff(currentPage, pageSize, SessionManager.CurrentCompanyId, startDate, endDate, searchText, type).ToList();

            int employeeId = 0;
            for (int i = 0; i < list.Count; i++)
            {
                if (employeeId == list[i].EIN)
                {
                    //list[i].EIN = 0;
                    list[i].EmployeeName = "";
                }
                else
                {
                    if (list[i].EIN != null)
                        employeeId = list[i].EIN.Value;
                }

                list[i].ActionValue = ((LogActionEnum)list[i].Action).ToString();
                list[i].TypeValue = ((MonetoryChangeLogTypeEnum)list[i].Action).ToString() + " : " + list[i].SubType;
            }


            total = 0;
            if (list.Count > 0)
                total = list[0].TotalRows.Value;

            return list;



        }
        public static List<GetSettingChangeLogsResult> GetSettingChangeLogs(
         DateTime startDate, DateTime endDate, string searchText, int currentPage, int? pageSize, ref int total)
        {

            currentPage -= 1;

            List<GetSettingChangeLogsResult> list =
                PayrollDataContext.GetSettingChangeLogs(currentPage, pageSize, SessionManager.CurrentCompanyId, startDate, endDate, searchText).ToList();

            total = 0;
            if (list.Count > 0)
                total = list[0].TotalRows.Value;

            return list;



        }


        public static List<GetUserActivityChangeLogsResult> GetUserChangeLogs(
            DateTime startDate, DateTime endDate, string searchText, int currentPage, int? pageSize, ref int total)
        {

            currentPage -= 1;

            List<GetUserActivityChangeLogsResult> list =
                PayrollDataContext.GetUserActivityChangeLogs(currentPage, pageSize, SessionManager.CurrentCompanyId, startDate, endDate, searchText).ToList();

            total = 0;
            if (list.Count > 0)
                total = list[0].TotalRows.Value;

            return list;



        }

        public static List<GetEmailLogsResult> GetEmailLogs(
            DateTime startDate, DateTime endDate, string searchText, int currentPage, int? pageSize, ref int total)
        {

            currentPage -= 1;

            List<GetEmailLogsResult> list =
                PayrollDataContext.GetEmailLogs(currentPage, pageSize, SessionManager.CurrentCompanyId, startDate, endDate, searchText).ToList();

            foreach (var item in list)
            {
                item.Action = ((EmailLogTypeEnum)item.EmailType).ToString();
            }


            total = 0;
            if (list.Count > 0)
                total = list[0].TotalRows.Value;

            return list;



        }
        #endregion


        #region "Check list"
        public static List<CheckList> GetAllCheckList()
        {
            return
                PayrollDataContext.CheckLists.OrderBy(x => x.Order).ToList();
        }
        public static CheckList GetCheckListById(int checkListId)
        {
            return PayrollDataContext.CheckLists.SingleOrDefault(x => x.CheckListId == checkListId);
        }
        public static void SaveOrUpdate(CheckList entity)
        {
            if (entity.CheckListId == 0)
                PayrollDataContext.CheckLists.InsertOnSubmit(entity);
            else
            {
                CheckList dbEntity = GetCheckListById(entity.CheckListId);
                dbEntity.Description = entity.Description;
                dbEntity.Order = entity.Order;

            }
            PayrollDataContext.SubmitChanges();
        }
        public static void MarkAll(bool isCompleted)
        {
            List<CheckList> list = GetAllCheckList();
            foreach (CheckList entity in list)
            {
                entity.IsCompleted = isCompleted;
            }
            PayrollDataContext.SubmitChanges();
        }
        public static void ToggleCheckListState(int checkListId)
        {
            CheckList dbEntity = GetCheckListById(checkListId);
            dbEntity.IsCompleted = !dbEntity.IsCompleted;
            PayrollDataContext.SubmitChanges();
        }
        public static void DeleteCheckList(int checkListId)
        {
            CheckList dbEntity = GetCheckListById(checkListId);
            if (dbEntity != null)
                PayrollDataContext.CheckLists.DeleteOnSubmit(dbEntity);
            PayrollDataContext.SubmitChanges();
        }
        public static bool IsAllCheckListCompleted(ref int total, ref int completed)
        {

            int remaining = PayrollDataContext.CheckLists.Count(x => x.IsCompleted == false);

            if (remaining > 0)
            {
                total = PayrollDataContext.CheckLists.Count();
                completed = remaining;
            }

            return remaining == 0;
        }
        #endregion

        #region Favourite Menu




        public static void UpdateFavMenuStatus(String menuItemID, int type)
        {
            FavouriteMenuItem oldStatus = PayrollDataContext.FavouriteMenuItems.Where(
                s => s.MenuItemID == menuItemID && s.UserRefID == SessionManager.User.UserID).SingleOrDefault();

            if (oldStatus != null)
            {
                FavouriteMenuItem newStatus = oldStatus;
                if (oldStatus.IsFavourite != null)
                {
                    newStatus.IsFavourite = !oldStatus.IsFavourite;
                }
                else
                {
                    newStatus.IsFavourite = false;
                }

                CopyObject<FavouriteMenuItem>(newStatus, ref oldStatus);

                PayrollDataContext.SubmitChanges();
            }
        }

        public static void InsertFavMenuXMLItem(List<FavouriteMenuItem> favItemList)
        {
            return;
            //PayrollDataContext.FavouriteMenuItems.InsertAllOnSubmit(favItemList);
            //PayrollDataContext.SubmitChanges();
        }

        public static void ResetFavMenuItem(int type)
        {
            List<FavouriteMenuItem> oldList = PayrollDataContext.FavouriteMenuItems.Where(s => s.Type == type && s.UserRefID == SessionManager.User.UserID).ToList();
            List<FavouriteMenuItem> newList = new List<FavouriteMenuItem>();
            newList = oldList;
            foreach (FavouriteMenuItem fav in oldList)
            {
                fav.IsFavourite = false;
            }

            PayrollDataContext.FavouriteMenuItems.DeleteAllOnSubmit(oldList);
            PayrollDataContext.FavouriteMenuItems.InsertAllOnSubmit(newList);
            PayrollDataContext.SubmitChanges();
        }
        public static int TotalFavouriteItem(int type)
        {
            return PayrollDataContext.FavouriteMenuItems.Where(s => s.Type == type && s.UserRefID == SessionManager.User.UserID && s.IsFavourite == true).Count();
        }
        #endregion

        public static void UpdateAttendanceCommentThreshold(int i)
        {
            PayrollDataContext.Settings.FirstOrDefault().AttendanceCommentByEmpThreshold = i;
            CommonManager.Setting.AttendanceCommentByEmpThreshold = i;
            PayrollDataContext.SubmitChanges();

        }



        public static BLevel getLevelByName(string LevelName)
        {
            BLevel inst = null;

            if (PayrollDataContext.BLevels.Any(x => (x.BLevelGroup.Name + " - " + x.Name).Trim().ToLower() == LevelName.Trim().ToLower()))
            {
                return PayrollDataContext.BLevels.FirstOrDefault(x => (x.BLevelGroup.Name + " - " + x.Name).Trim().ToLower() == LevelName.Trim().ToLower());
            }

            if (PayrollDataContext.BLevels.Any(x => x.Name.Trim().ToLower() == LevelName.Trim().ToLower()))
            {
                return PayrollDataContext.BLevels.FirstOrDefault(x => x.Name.Trim().ToLower() == LevelName.Trim().ToLower());
            }

            return inst;
        }

        public static Grade getGradeByName(string LevelName)
        {
            Grade inst = null;
            if (PayrollDataContext.Grades.Any(x => x.Name.Trim().ToLower() == LevelName.Trim().ToLower()))
            {
                return PayrollDataContext.Grades.FirstOrDefault(x => x.Name.Trim().ToLower() == LevelName.Trim().ToLower());
            }

            return inst;
        }

        public static EDesignation getDesignationByName(string LevelName, int levelId)
        {
            EDesignation inst = null;


            if (PayrollDataContext.EDesignations.Any(x => x.LevelId == levelId && (x.BLevel.BLevelGroup.Name + " - " + x.BLevel.Name + " - " + x.Name).Trim().ToLower() == LevelName.Trim().ToLower()))
            {
                return PayrollDataContext.EDesignations.FirstOrDefault(x => x.LevelId == levelId && (x.BLevel.BLevelGroup.Name + " - " + x.BLevel.Name + " - " + x.Name).Trim().ToLower() == LevelName.Trim().ToLower());
            }


            if (PayrollDataContext.EDesignations.Any(x => x.LevelId == levelId && x.Name.Trim().ToLower() == LevelName.Trim().ToLower()))
            {
                return PayrollDataContext.EDesignations.FirstOrDefault(x => x.LevelId == levelId && x.Name.Trim().ToLower() == LevelName.Trim().ToLower());
            }

            return inst;
        }


        public static EDesignation GetDesigId(int desId)
        {
            return PayrollDataContext.EDesignations.FirstOrDefault(x => x.DesignationId == desId);
        }
        public static void UpdateEmployeeTimeAttReqDays(int i)
        {
            PayrollDataContext.Settings.FirstOrDefault().EmpTimeAttRequestThreshold = i;
            PayrollDataContext.SubmitChanges();

        }

        public static string GetAutomaticFileLocation()
        {
            string location = "";
            if (string.IsNullOrEmpty(Config.AutomaticBackupLocation))
                location = "";
            else
            {
                location = Config.AutomaticBackupLocation;
                //location = HttpContext.Current.Server.MapPath(Config.AutomaticBackupLocation);
            }
            return location;
        }

        public static void UpdateEmployeeActivityThreshold(int i)
        {
            PayrollDataContext.Settings.FirstOrDefault().EmployeeActvityThreshold = i;
            PayrollDataContext.SubmitChanges();

        }

        public static string GetLogFileLocation()
        {
            string location = HttpContext.Current.Server.MapPath(Config.LogFileLoc);
            return location;
        }

        public static string GetDesignationNameByEmpId(int emp)
        {
            return
               (
               from e in PayrollDataContext.EEmployees
               join d in PayrollDataContext.EDesignations on e.DesignationId equals d.DesignationId
               where e.EmployeeId == emp
               select d.Name).FirstOrDefault();
        }

        public static string GetLevelNameByEmpId(int emp)
        {
            return
               (
               from e in PayrollDataContext.EEmployees
               join d in PayrollDataContext.EDesignations on e.DesignationId equals d.DesignationId
               join l in PayrollDataContext.BLevels on d.LevelId equals l.LevelId
               where e.EmployeeId == emp
               select l.Name).FirstOrDefault();
        }

        public static string GetBloodGroupNameById(int id)
        {
            return PayrollDataContext.HBloodGroups.Where(x => x.Id == id).SingleOrDefault().BloodGroupName.ToString();
        }
    }
}


