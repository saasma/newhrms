﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using DAL;
using Utils;
using System.Xml.Linq;
using Utils.Calendar;
using BLL.Entity;
using BLL.BO;
namespace BLL.Manager
{

    public class AllowanceManager : BaseBiz
    {

        #region "Mega Children/Vechicle allowance settings"

        public static List<MG_AllowanceSetting> GetAllowanceIncomeList()
        {
            List<MG_AllowanceSetting> list = PayrollDataContext.MG_AllowanceSettings.ToList();

            foreach (MG_AllowanceSetting item in list)
            {
                item.Name = new PayManager().GetIncomeById(item.IncomeId).Title;
            }
            return list;
        }

        public static List<TextValue> GetIncomeEmployee(int incomeId)
        {
            return
                (
                    from ei in PayrollDataContext.PEmployeeIncomes
                    join e in PayrollDataContext.EEmployees on ei.EmployeeId equals e.EmployeeId
                    where ei.IncomeId == incomeId
                    orderby e.Name
                    select new TextValue
                    {
                        Text = e.Name,
                        Value = e.EmployeeId.ToString()
                    }
                ).ToList();
        }

        public static List<MG_Opening> GetAllowanceOpeningIncomeList(int incomeId)
        {
            List<MG_Opening> list = PayrollDataContext.MG_Openings.Where(x=>x.IncomeId==incomeId).ToList();

            foreach (MG_Opening item in list)
            {
                item.Name = new EmployeeManager().GetById(item.EmployeeId).Name;
            }
            return list.OrderBy(x => x.Name).ToList();
        }

        public static List<PIncome> GetAllowanceIncomes()
        {
            List<PIncome> list =
                (
                    from p in PayrollDataContext.PIncomes
                    join ss in PayrollDataContext.MG_AllowanceSettings on p.IncomeId equals ss.IncomeId
                    select p
                ).ToList();

            return list;
        }

        #region ChildAllowance
        public static List<PIncome> GetChildAllowanceIncomeList()
        {
            List<PIncome> list = PayrollDataContext.PIncomes.Where(x => (x.Calculation == "FixedAmount" || x.Calculation == "VariableAmount")
                && x.IsChildrenAllowance != null && x.IsChildrenAllowance.Value).ToList();
            return list;
        }

        public static List<HFamily> GetChildernAllowanceFamilyMemberOfEmployee(int EmployeeID)
        {
            List<HFamily> list = PayrollDataContext.HFamilies.Where(x => (x.Relation == "son" || x.Relation == "Daughter") && x.EmployeeId==EmployeeID ).ToList();
            foreach (HFamily item in list)
            {
                item.Name = item.Name + " (" + item.Relation + ")";
            }
            return list;
        }

        public static DateTime? GetDateOfBirthOfFamilyMember(int FamilyID)
        {
            HFamily _HFamily = PayrollDataContext.HFamilies.SingleOrDefault(x => x.FamilyId == FamilyID);
            if (_HFamily != null)
            {
                return _HFamily.DateOfBirthEng;
            }
            else return null;
        }

      

        public static List<GetChildrenAllowanceResult> GetChildAllowanceList(int start, int limit, int EmployeeID,DateTime?StartDate,DateTime?EndDate,int IncomeID)
        {
            List<GetChildrenAllowanceResult> list = PayrollDataContext.GetChildrenAllowance(start, limit, EmployeeID, StartDate, EndDate, IncomeID).ToList();
            return list;
        }


        public static ChildrenAllowance GetChildrenAllowance(int AllownaceID)
        {
            return PayrollDataContext.ChildrenAllowances.FirstOrDefault(x => x.ChildrenAllowanceID == AllownaceID);
        }

        public static Status InsertUpdateChildrenAllowance(ChildrenAllowance entity, bool isInsert)
        {
            Status status = new Status();
            entity.ModifiedBy = SessionManager.CurrentLoggedInUserID;
            entity.ModifiedDate = GetCurrentDateAndTime();

            if (isInsert)
            {

                entity.CreatedBy = entity.ModifiedBy = SessionManager.CurrentLoggedInUserID;
                entity.CreatedDate = entity.ModifiedDate = GetCurrentDateAndTime();

                PayrollDataContext.ChildrenAllowances.InsertOnSubmit(entity);
                SaveChangeSet();
            }
            else
            {
                ChildrenAllowance dbEntity = PayrollDataContext.ChildrenAllowances.SingleOrDefault(x => x.ChildrenAllowanceID == entity.ChildrenAllowanceID);
                if (dbEntity != null)
                {
                    CopyObject<ChildrenAllowance>(entity, ref dbEntity);
                    UpdateChangeSet();
                }
                else
                {
                    status.ErrorMessage = "Allowance does not exists.";
                }

            }
            return status;
        }
        public static Status DeleteChildrenAllowance(int ChildrenAllowanceID)
        {
            Status status = new Status();
            ChildrenAllowance dbRecord = PayrollDataContext.ChildrenAllowances.SingleOrDefault(c => c.ChildrenAllowanceID == ChildrenAllowanceID);
            PayrollDataContext.ChildrenAllowances.DeleteOnSubmit(dbRecord);
            DeleteChangeSet();
            return status;
        }

        #endregion
        public static Status SaveAllowanceIncome(List<PEmployeeIncome> list, ref int count, PIncome income)
        {
            count = 0;
            Status status = new Status();

            foreach (PEmployeeIncome empIncome in list)
            {
                     PEmployeeIncome dbEmployeeIncome = PayrollDataContext.PEmployeeIncomes.SingleOrDefault
                     (x => x.IncomeId == income.IncomeId && x.EmployeeId == empIncome.EmployeeId);

                count += 1;

                if (dbEmployeeIncome != null)
                {
                    dbEmployeeIncome.Amount = empIncome.Amount;

                    dbEmployeeIncome.IsValid = true;

                }
                //if not associated then associate it
                else
                {
                    PEmployeeIncome newIncome = new PEmployeeIncome();
                    newIncome.EmployeeId = empIncome.EmployeeId;
                    newIncome.IncomeId = income.IncomeId;
                 

                        newIncome.Amount = empIncome.Amount;
                       


                    newIncome.IsValid = true;
                    newIncome.IsEnabled = true;

                    if (EmployeeManager.IsEmployeeIDExists(empIncome.EmployeeId) == false)
                    {
                        status.ErrorMessage = string.Format("Employee ID \"{0}\" does not exists.", empIncome.EmployeeId);
                        return status;
                    }

                    PayrollDataContext.PEmployeeIncomes.InsertOnSubmit(newIncome);
                }



            }

            UpdateChangeSet();

            return status;

        }

        public static MG_AllowanceSetting GetAllowance(int incomeAllowanceId)
        {
            return PayrollDataContext.MG_AllowanceSettings.FirstOrDefault(x => x.IncomeId == incomeAllowanceId);
        }
        public static MG_Opening GetOpening(int incomeId,int employeeId)
        {
            return PayrollDataContext.MG_Openings.FirstOrDefault(x => x.IncomeId == incomeId && x.EmployeeId==employeeId);
        }
        public static Status InsertUpdateAllowance(MG_AllowanceSetting entity, bool isInsert)
        {
            Status status = new Status();


            if (isInsert)
            {
                entity.CreatedBy = entity.ModifiedBy = SessionManager.CurrentLoggedInUserID;
                entity.CreatedOn = entity.ModifiedOn = GetCurrentDateAndTime();

               

                PayrollDataContext.MG_AllowanceSettings.InsertOnSubmit(entity);

            }
            else
            {


                MG_AllowanceSetting dbEntity = PayrollDataContext.MG_AllowanceSettings.SingleOrDefault(x => x.IncomeId == entity.IncomeId);
                //CopyObject<BLevel>(entity, ref dbEntity, "CreatedOn", "CreatedBy");

                dbEntity.AllocatedAmount = entity.AllocatedAmount;

                dbEntity.ModifiedBy = SessionManager.CurrentLoggedInUserID;
                dbEntity.ModifiedOn = GetCurrentDateAndTime();


            }

            PayrollDataContext.SubmitChanges();

            return status;
        }


      

        public static Status InsertUpdateOpening(MG_Opening entity, bool isInsert)
        {
            Status status = new Status();

            

            if (isInsert && !PayrollDataContext.MG_Openings.Any(x=>x.IncomeId == entity.IncomeId && x.EmployeeId == entity.EmployeeId))
            {


                PayrollDataContext.MG_Openings.InsertOnSubmit(entity);

            }
            else
            {


                MG_Opening dbEntity = PayrollDataContext.MG_Openings.SingleOrDefault(x => x.IncomeId == entity.IncomeId
                    && x.EmployeeId==entity.EmployeeId);
                //CopyObject<BLevel>(entity, ref dbEntity, "CreatedOn", "CreatedBy");

                dbEntity.TotalAllocatedAmount = entity.TotalAllocatedAmount;

                dbEntity.TotalAmountPaid = entity.TotalAmountPaid;
                dbEntity.TotalBillReceived = entity.TotalBillReceived;


            }

            PayrollDataContext.SubmitChanges();

            return status;
        }

        #endregion

        public static List<EveningCounterType> GetAllowanceTypes()
        {
            List<EveningCounterType> list = PayrollDataContext.EveningCounterTypes.OrderBy(x => x.Name).ToList();
            PayManager mgr = new PayManager();
            foreach (EveningCounterType item in list)
            {
                if (item.IncomeId != null)
                    item.Income = mgr.GetIncomeById(item.IncomeId.Value).Title;

                item.IsProportionate =
                    (item.IsProportionOnTotalMonthDays != null && item.IsProportionOnTotalMonthDays.Value)
                    ? "Yes" : "";

            }

            return list;
        }

        public static Status InsertUpdateEveningCounterType(EveningCounterType counter, bool isInsert)
        {
            Status status = new Status();

            if (PayrollDataContext.OvertimeTypes.Any(x => x.IncomeId == counter.IncomeId))
            {
                status.ErrorMessage = "Income set in Overtime types cannot be set in Evening counter.";
                return status;
            }

            if (PayrollDataContext.EveningCounterTypes.Any(x => x.Name.ToLower().Trim() == counter.Name.ToLower().Trim()
                && x.EveningCounterTypeId != counter.EveningCounterTypeId))
            {
                status.ErrorMessage = "Evening Counter name already exists.";
                return status;
            }

            if (counter.CalculationType == (int)AllowanceEveningCounterCalculationType.BasedOnSalary)
            {
                if (counter.EveningCounterTypeIncomes.Count <= 0)
                {
                    status.ErrorMessage = "Income(s) should be selected for Based on salary option.";
                    return status;
                }
            }

            if (isInsert)
                PayrollDataContext.EveningCounterTypes.InsertOnSubmit(counter);
            else
            {
                EveningCounterType dbType = PayrollDataContext.EveningCounterTypes.FirstOrDefault(x => x.EveningCounterTypeId == counter.EveningCounterTypeId);



                if (dbType.Name != counter.Name)
                    PayrollDataContext.ChangeSettings.InsertOnSubmit(GetSettingChangeLog((byte)MonetoryChangeLogTypeEnum.AllowanceType, "Name",
                        dbType.Name, counter.Name, LogActionEnum.Update));

                if (dbType.Rate != counter.Rate)
                    PayrollDataContext.ChangeSettings.InsertOnSubmit(GetSettingChangeLog((byte)MonetoryChangeLogTypeEnum.AllowanceType, "Rate",
                        GetString(dbType.Rate),GetString(counter.Rate), LogActionEnum.Update));

                if (dbType.IncomeId != counter.IncomeId)
                    PayrollDataContext.ChangeSettings.InsertOnSubmit(GetSettingChangeLog((byte)MonetoryChangeLogTypeEnum.AllowanceType, "Income Id",
                        GetString(dbType.IncomeId), GetString(counter.IncomeId), LogActionEnum.Update));

                if (dbType.CalculationType != counter.CalculationType)
                    PayrollDataContext.ChangeSettings.InsertOnSubmit(GetSettingChangeLog((byte)MonetoryChangeLogTypeEnum.AllowanceType, "Calculation Type",
                        GetString(dbType.CalculationType), GetString(counter.CalculationType), LogActionEnum.Update));

                if (dbType.IsProportionOnTotalMonthDays != counter.IsProportionOnTotalMonthDays)
                    PayrollDataContext.ChangeSettings.InsertOnSubmit(GetSettingChangeLog((byte)MonetoryChangeLogTypeEnum.AllowanceType, "Proportion Days",
                        GetString(dbType.IsProportionOnTotalMonthDays), GetString(counter.IsProportionOnTotalMonthDays), LogActionEnum.Update));

                if (dbType.DoNowShowDaysWarning != counter.DoNowShowDaysWarning)
                    PayrollDataContext.ChangeSettings.InsertOnSubmit(GetSettingChangeLog((byte)MonetoryChangeLogTypeEnum.AllowanceType, "Do Now Show Days Warning",
                        GetString(dbType.DoNowShowDaysWarning), GetString(counter.DoNowShowDaysWarning), LogActionEnum.Update));

                if (dbType.DoNotShowInEmployeePortal != counter.DoNotShowInEmployeePortal)
                    PayrollDataContext.ChangeSettings.InsertOnSubmit(GetSettingChangeLog((byte)MonetoryChangeLogTypeEnum.AllowanceType, "Do Now Show in Employee Portal",
                        GetString(dbType.DoNotShowInEmployeePortal), GetString(counter.DoNotShowInEmployeePortal), LogActionEnum.Update));

                dbType.EveningCounterTypeIncomes.Clear();

                dbType.EveningCounterTypeIncomes.AddRange(counter.EveningCounterTypeIncomes);

                dbType.Name = counter.Name;
                dbType.Rate = counter.Rate;
                dbType.IncomeId = counter.IncomeId;
                dbType.CalculationType = counter.CalculationType;
                dbType.IsProportionOnTotalMonthDays = counter.IsProportionOnTotalMonthDays;
                dbType.DoNowShowDaysWarning = counter.DoNowShowDaysWarning;
                dbType.DoNotShowInEmployeePortal = counter.DoNotShowInEmployeePortal;
            }


            PayrollDataContext.SubmitChanges();
            return status;
        }


        public static Status DeleteEveningCounter(int overtimeTypeId)
        {
            Status status = new Status();
            if (PayrollDataContext.EveningCounterRequests.Any(x => x.EveningCounterTypeId == overtimeTypeId))
            {
                status.ErrorMessage = "Evening Counter type can not be deleted.";
                return status;
            }

            EveningCounterType dbType = PayrollDataContext.EveningCounterTypes.FirstOrDefault(x => x.EveningCounterTypeId == overtimeTypeId);
            PayrollDataContext.EveningCounterTypes.DeleteOnSubmit(dbType);
            PayrollDataContext.SubmitChanges();
            return status;
        }


        public static Status DeleteEveningCounterRequest(int overtimeTypeId)
        {
            Status status = new Status();

            EveningCounterRequest dbType = PayrollDataContext.EveningCounterRequests.FirstOrDefault(x => x.CounterRequestID == overtimeTypeId);

            if (dbType != null)
            {
                if (dbType.Status != (int)EveningCounterStatusEnum.Pending)
                {
                    status.ErrorMessage = ((EveningCounterStatusEnum)dbType.Status).ToString() + " request cannot be deleted.";
                    return status;
                }

                PayrollDataContext.EveningCounterRequests.DeleteOnSubmit(dbType);
                PayrollDataContext.SubmitChanges();
            }
            return status;
        }

        public static EveningCounterType GetEveningCounterType(int overtimeId)
        {
            return PayrollDataContext.EveningCounterTypes
                .Where(o => o.EveningCounterTypeId == overtimeId)
                .SingleOrDefault();
        }

        public static List<EveningCounterType> GetAllEveningCounterList()
        {
            return PayrollDataContext.EveningCounterTypes.OrderBy(x=>x.Name).ToList();
        }

        public static List<AllowanceBO> GetAllowanceHistoryForEmployee(int employeeId,int skipId)
        {
            FinancialDate year = CommonManager.GetCurrentFinancialYear();
            DateTime start = year.StartingDateEng.Value;

            List<AllowanceBO> list = 
            (
                from r in PayrollDataContext.EveningCounterRequests
                join t in PayrollDataContext.EveningCounterTypes on r.EveningCounterTypeId equals t.EveningCounterTypeId
                where r.EmployeeID == employeeId && r.Status != (int)EveningCounterStatusEnum.Rejected
                 && r.CounterRequestID != skipId
                 && r.EndDate >= start
                orderby r.StartDate
                select new AllowanceBO
                {
                    AllowanceName = t.Name,
                    start = r.StartDate.Value,//.ToString("yyyy-MMM-dd"),
                    end = r.EndDate.Value,//.ToString("yyyy-MMM-dd"),

                    Units = r.Days == null ? "0" : r.Days.ToString(),
                    StatusInt = r.Status.Value,
                    PeriodId = r.PayrollPeriodId

                }
                ).ToList();


            foreach (AllowanceBO item in list)
            {
                item.StartDate = item.start.ToString("yyyy-MMM-dd");
                item.EndDate = item.end.ToString("yyyy-MMM-dd");

                DateTime  date = Convert.ToDateTime(item.StartDate);
                item.StartDate += ", " + CustomDate.ConvertEngToNep(new CustomDate(date.Day, date.Month, date.Year, true)).ToStringMonthDaySkipYear();

                date = Convert.ToDateTime(item.EndDate);
                item.EndDate += ", " + CustomDate.ConvertEngToNep(new CustomDate(date.Day, date.Month, date.Year, true)).ToStringMonthDaySkipYear();

                item.Status = ((EveningCounterStatusEnum)item.StatusInt).ToString();

                if (item.StatusInt == (int)EveningCounterStatusEnum.Forwarded && item.PeriodId != null && item.PeriodId != 0)
                {
                    item.Status = "Posted - " + CommonManager.GetPayrollPeriod(item.PeriodId.Value).Name;
                }

                item.Units = ((int)double.Parse(item.Units)).ToString();
            }

            return list;
        }

        public static string AllowanceDaysOrHourLabel
        {
            set{}
            get
            {
                //if (CommonManager.CompanySetting.WhichCompany == WhichCompany.HPL)
                //    return "Units";
                return "Units";
            }
        }

        public static Status InsertUpdateEveningRequest(EveningCounterRequest request, bool isInsert)
        {
            Status status = new Status();

            DateTime today = SessionManager.GetCurrentDateAndTime();
            today = new DateTime(today.Year, today.Month, today.Day);

            // as can be in future also
            //if (request.EndDate > today)
            //{                
            //    status.ErrorMessage = "Request date cannot be in Future.";
            //    return status;
            //}

            EveningCounterRequest reqExists =  PayrollDataContext.EveningCounterRequests.FirstOrDefault(x => 
                x.EmployeeID == request.EmployeeID
                && x.Status != (int)EveningCounterStatusEnum.Rejected      
                && x.EveningCounterTypeId == request.EveningCounterTypeId
                && x.CounterRequestID != request.CounterRequestID
                &&
                (
                    (x.StartDate >= request.StartDate && x.StartDate <= request.EndDate) ||
                    (x.EndDate >= request.StartDate && x.EndDate <= request.EndDate) ||
                    (request.StartDate >= x.StartDate && request.StartDate <= x.EndDate) ||
                    (request.EndDate >= x.StartDate && request.EndDate <= x.EndDate)
                ));
            
            if(reqExists != null)
            {
                status.ErrorMessage = "Request already exists for this date and allowance type.";
                return status;
            }

            // Validate that the start/end date should lie in same nepali calendar month
            if (IsEnglish == false)
            {
                CustomDate dateStart = CustomDate.GetCustomDateFromString(GetAppropriateDate(request.StartDate.Value), false);
                CustomDate dateEnd = CustomDate.GetCustomDateFromString(GetAppropriateDate(request.EndDate.Value), false);

                if (CommonManager.CompanySetting.WhichCompany != WhichCompany.HPL)
                {
                    if (dateStart.Month != dateEnd.Month || dateStart.Year != dateEnd.Year)
                    {
                        status.ErrorMessage = "End date should also lie in the same Start date month of \"" +
                            DateHelper.GetMonthName(dateStart.Month, false) + "\".";
                        return status;
                    }
                }
                request.MonthName = DateHelper.GetMonthName(dateStart.Month, false);

                request.MonthTotalDays = DateHelper.GetTotalDaysInTheMonth(
                    dateStart.Year, dateStart.Month, false);
            }

          


            if (isInsert)
            {
                PayrollDataContext.EveningCounterRequests.InsertOnSubmit(request);
            }
            else
            {
                EveningCounterRequest dbType = PayrollDataContext.EveningCounterRequests.FirstOrDefault(x => x.CounterRequestID == request.CounterRequestID);

                if (dbType.Status != (int)EveningCounterStatusEnum.Pending)
                {
                    status.ErrorMessage = "Only Requests with pending status can be updated";
                    return status;
                }
                dbType.MonthTotalDays = request.MonthTotalDays;
                dbType.StartDate = request.StartDate;
                dbType.EndDate = request.EndDate;
                dbType.MonthName = request.MonthName;
                dbType.Days = request.Days;
                dbType.Reason = request.Reason;
                dbType.Status = request.Status;
                dbType.EveningCounterTypeId = request.EveningCounterTypeId;
                dbType.ApprovalID = request.ApprovalID;
                dbType.RecommendedBy = request.RecommendedBy;
            }


            EveningCounterType allowanceType = PayrollDataContext.EveningCounterTypes.FirstOrDefault(x => x.EveningCounterTypeId == request.EveningCounterTypeId);
            if (allowanceType != null && allowanceType.IncomeId != null)
            {
                if (PayrollDataContext.PEmployeeIncomes.Any(x => x.EmployeeId == request.EmployeeID && x.IncomeId == allowanceType.IncomeId.Value) == false)
                {
                    PEmployeeIncome overtimeInc = new PEmployeeIncome();
                    overtimeInc.EmployeeId = request.EmployeeID.Value;
                    overtimeInc.IncomeId = allowanceType.IncomeId.Value;
                    overtimeInc.IsValid = true;
                    overtimeInc.IsEnabled = true;

                    PayrollDataContext.PEmployeeIncomes.InsertOnSubmit(overtimeInc);

                }
            }


            PayrollDataContext.SubmitChanges();
            SendEmail(request, true);
            return status;
        }




        public static List<EveningCounterRequest> GetEveningCounterRequestForEmp(int EmpID)
        {
            List<EveningCounterRequest> iList = new List<EveningCounterRequest>();
            iList = PayrollDataContext.EveningCounterRequests.Where(x => x.EmployeeID == EmpID).OrderByDescending(x=>x.StartDate.Value).ToList();
            foreach (var val1 in iList)
            {

                val1.StatusStr = ((EveningCounterStatusEnum)val1.Status).ToString();


                if (val1.EveningCounterTypeId != null)
                {
                    val1.EveningCounterTypeStr = GetEveningCounterType(val1.EveningCounterTypeId.Value).Name;
                }
            }
            return iList;
        }





        public static EveningCounterRequest GetEveningCounterByID(int requestID)
        {
            EveningCounterRequest iInstance = new EveningCounterRequest();
            if (PayrollDataContext.EveningCounterRequests.Any(x => x.CounterRequestID == requestID))
            {
                iInstance = PayrollDataContext.EveningCounterRequests.FirstOrDefault(x => x.CounterRequestID == requestID);
            }
            return iInstance;
        }

        public static EveningCounterRequest getEveningCounterRequestByID(int RequestID)
        {
            EveningCounterRequest request = new EveningCounterRequest();
            
            if(PayrollDataContext.EveningCounterRequests.Any(x=>x.CounterRequestID == RequestID))
            {
                request = PayrollDataContext.EveningCounterRequests.FirstOrDefault(x => x.CounterRequestID == RequestID);          
            }

            if (request != null)
            {
                if (request.EmployeeID != null)
                {
                    request.EmpName = EmployeeManager.GetEmployeeById(request.EmployeeID.Value).Name;
                }
            }

            return request;
        }

        public static bool RecommendOrApprove(List<EveningCounterRequest> requests, out int count)
        {


            Dictionary<string, string> inserted = new System.Collections.Generic.Dictionary<string, string>();

            count = 0;
            EveningCounterRequest dbEntity;
            foreach (EveningCounterRequest req in requests)
            {

                dbEntity = PayrollDataContext.EveningCounterRequests.Where(x => x.CounterRequestID == req.CounterRequestID).FirstOrDefault();
                if (dbEntity != null && dbEntity.Status == (int)EveningCounterStatusEnum.Pending)
                {

                    //dbEntity.StartDate = req.StartDate;
                    //dbEntity.EndDate = req.EndDate;
                    dbEntity.Status = (int)EveningCounterStatusEnum.Recommended;
                    dbEntity.RecommendedBy = SessionManager.CurrentLoggedInEmployeeId;
                    dbEntity.RecommendedOn = GetCurrentDateAndTime();

                    if (LeaveRequestManager.CanApprove(dbEntity.EmployeeID.Value, SessionManager.CurrentLoggedInEmployeeId, PreDefindFlowType.Overtime))
                    {
                        dbEntity.ApprovedBy = SessionManager.CurrentLoggedInEmployeeId;
                        dbEntity.ApprovedOn = GetCurrentDateAndTime();
                        dbEntity.Status = (int)EveningCounterStatusEnum.Approved;
                    }
                    else
                        SendEmail(dbEntity, false);

                    count += 1;



                }
                else if (dbEntity != null && dbEntity.Status == (int)EveningCounterStatusEnum.Recommended
                    && LeaveRequestManager.CanApprove(dbEntity.EmployeeID.Value, SessionManager.CurrentLoggedInEmployeeId, PreDefindFlowType.Overtime))
                {
                    //dbEntity.StartDate = req.StartDate;
                    //dbEntity.EndDate = req.EndDate;
                    dbEntity.Status = (int)EveningCounterStatusEnum.Approved;
                    dbEntity.ApprovedBy = SessionManager.CurrentLoggedInEmployeeId;
                    dbEntity.ApprovedOn = GetCurrentDateAndTime();

                    count += 1;
                }
            }

            PayrollDataContext.SubmitChanges();
            return true;
        }

        //public static Status ApproveRequest(EveningCounterRequest requestInstance)
        //{
        //    EveningCounterRequest dbEntity = new EveningCounterRequest();
        //    Status status = new Status();
        //    if (PayrollDataContext.EveningCounterRequests.Any(x => x.CounterRequestID == requestInstance.CounterRequestID))
        //    {

        //        dbEntity = PayrollDataContext.EveningCounterRequests.Where(x => x.CounterRequestID == requestInstance.CounterRequestID).SingleOrDefault();

        //        if (dbEntity.Status != (int)OvertimeStatusEnum.Recommended)
        //        {
        //            status.ErrorMessage = ((OvertimeStatusEnum)dbEntity.Status).ToString() + " request cannot be approved.";
        //            return status;
        //        }

        //        //dbEntity.StartDate = requestInstance.StartDate;
        //        //dbEntity.EndDate = requestInstance.EndDate;
        //        //dbEntity.Days = requestInstance.Days;
        //        dbEntity.Status = requestInstance.Status;
        //        dbEntity.ApprovedBy = SessionManager.CurrentLoggedInEmployeeId;
        //        dbEntity.ApprovedOn = GetCurrentDateAndTime();
        //    }
        //    PayrollDataContext.SubmitChanges();
        //    return status;
        //}

        public static Status ApproveRequest(EveningCounterRequest requestInstance, List<RequestEditHistory> edits)
        {

            EveningCounterRequest dbEntity = PayrollDataContext.EveningCounterRequests.Where(x => x.CounterRequestID == requestInstance.CounterRequestID).SingleOrDefault();

            Status status = new Status();

            if (dbEntity.Status != (int)EveningCounterStatusEnum.Recommended && dbEntity.Status != (int)EveningCounterStatusEnum.Pending)
            {
                status.ErrorMessage = ((EveningCounterStatusEnum)dbEntity.Status).ToString() + " request can not be approved.";
                return status;
            }


            EveningCounterRequest request = new EveningCounterRequest();
            CopyObject<EveningCounterRequest>(dbEntity, ref request, "");
            request.StartDate = requestInstance.StartDate;
            request.EndDate = requestInstance.EndDate;
            request.Days = requestInstance.Days;

            status = ValidateAllowance(request);
            if (status.IsSuccess == false)
                return status;


            dbEntity.StartDate = request.StartDate;
            dbEntity.EndDate = request.EndDate;
            dbEntity.Days = request.Days;
            dbEntity.MonthName = request.MonthName;
            dbEntity.MonthTotalDays = request.MonthTotalDays;
            dbEntity.Status = requestInstance.Status;
            dbEntity.ApprovedBy = SessionManager.CurrentLoggedInEmployeeId;
            dbEntity.ApprovedOn = GetCurrentDateAndTime();

            foreach (var editHist in edits)
            {
                editHist.RequestID = dbEntity.CounterRequestID;

                if (SessionManager.User.EEmployee != null && SessionManager.User.EEmployee.EmployeeId != null)
                {
                    editHist.ModifiedBy = SessionManager.User.EEmployee.EmployeeId.ToString();
                    editHist.ModifiedByName = SessionManager.User.EEmployee.Name;
                }
                else if (SessionManager.User != null && SessionManager.User.UserID != null)
                {
                    editHist.ModifiedBy = SessionManager.User.UserID.ToString();
                    editHist.ModifiedByName = SessionManager.User.UserName;
                }
                //editHist.ModifiedBy = SessionManager.User.UserID;

                editHist.ModifiedOn = CommonManager.GetCurrentDateAndTime();
                editHist.RequestType = (int)RequestHistoryTypeEnum.EveningCounter;
            }

            PayrollDataContext.RequestEditHistories.InsertAllOnSubmit(edits);



            PayrollDataContext.SubmitChanges();

            SendApprovedEmailNotification(dbEntity);

            return status;
        }


        public static Status ValidateAllowance(EveningCounterRequest requestInstance)
        {
            Status status = new Status();

            if (requestInstance.EveningCounterTypeId == null ||
                requestInstance.EveningCounterTypeId == 0 ||
                requestInstance.EveningCounterTypeId == -1)
            {
                status.ErrorMessage = "Allowance type is required.";
                return status;
            }

            if (requestInstance.RecommendedBy == null ||
                requestInstance.RecommendedBy == 0 ||
                requestInstance.RecommendedBy == -1)
            {
                status.ErrorMessage = "Recomender is required.";
                return status;
            }

            if (requestInstance.ApprovalID == null ||
                requestInstance.ApprovalID == 0 ||
                requestInstance.ApprovalID == -1)
            {
                status.ErrorMessage = "Approval is required.";
                return status;
            }

            if (requestInstance.StartDate == null ||
                requestInstance.StartDate.Value == DateTime.MinValue)
            {
                status.ErrorMessage = "Start date is required.";
                return status;
            }

            if (requestInstance.EndDate == null ||
               requestInstance.EndDate.Value == DateTime.MinValue)
            {
                status.ErrorMessage = "End date is required.";
                return status;
            }

            if (requestInstance.StartDate > requestInstance.EndDate)
            {
                status.ErrorMessage = "Start date should not be greater then End date.";
                return status;
            }

            if (requestInstance.Days == null ||
                requestInstance.Days == 0)
            {
                status.ErrorMessage = "Allowance days is required.";
                return status;
            }


            int daysFromDate = (requestInstance.EndDate.Value - requestInstance.StartDate.Value).Days + 1;

            EveningCounterType allowanceType = PayrollDataContext.EveningCounterTypes.FirstOrDefault(x => x.EveningCounterTypeId
                == requestInstance.EveningCounterTypeId);


            if (CommonManager.CompanySetting.WhichCompany != WhichCompany.HPL && (allowanceType.DoNowShowDaysWarning != true))
            {
                if (requestInstance.Days.Value > daysFromDate)
                {
                    status.ErrorMessage = "Number of days between start and end date is Higher than Expected.";
                    return status;
                }
            }


            DateTime today = SessionManager.GetCurrentDateAndTime();
            today = new DateTime(today.Year, today.Month, today.Day);



            if (PayrollDataContext.EveningCounterRequests.Any(x => x.EmployeeID == requestInstance.EmployeeID
                && x.Status != (int)EveningCounterStatusEnum.Rejected
                && x.EveningCounterTypeId == requestInstance.EveningCounterTypeId
                && x.CounterRequestID != requestInstance.CounterRequestID
                &&
                (
                    (x.StartDate >= requestInstance.StartDate && x.StartDate <= requestInstance.EndDate) ||
                    (x.EndDate >= requestInstance.StartDate && x.EndDate <= requestInstance.EndDate) ||
                    (requestInstance.StartDate >= x.StartDate && requestInstance.StartDate <= x.EndDate) ||
                    (requestInstance.EndDate >= x.StartDate && requestInstance.EndDate <= x.EndDate)
                ))
                )
            {
                status.ErrorMessage = "Request already exists for this date and allowance";
                return status;
            }

            // Validate that the start/end date should lie in same nepali calendar month
            if (IsEnglish == false)// && CommonManager.CompanySetting.WhichCompany != WhichCompany.HPL)
            {
                CustomDate dateStart = CustomDate.GetCustomDateFromString(GetAppropriateDate(requestInstance.StartDate.Value), false);
                CustomDate dateEnd = CustomDate.GetCustomDateFromString(GetAppropriateDate(requestInstance.EndDate.Value), false);
                if (CommonManager.CompanySetting.WhichCompany != WhichCompany.HPL)
                {
                    if (dateStart.Month != dateEnd.Month || dateStart.Year != dateEnd.Year)
                    {
                        status.ErrorMessage = "End date should also lie in the same Start date month of \"" +
                            DateHelper.GetMonthName(dateStart.Month, false) + "\".";
                        return status;
                    }
                }

                requestInstance.MonthName = DateHelper.GetMonthName(dateStart.Month, false);

                requestInstance.MonthTotalDays = DateHelper.GetTotalDaysInTheMonth(
                    dateStart.Year, dateStart.Month, false);
            }

            return status;
        }


        public static Status RecommendRequest(EveningCounterRequest requestInstance, List<RequestEditHistory> edits)
        {

            EveningCounterRequest dbEntity = PayrollDataContext.EveningCounterRequests.Where(x => x.CounterRequestID == requestInstance.CounterRequestID).SingleOrDefault();
            
            Status status = new Status();

            if (dbEntity.Status != (int)EveningCounterStatusEnum.Pending)
            {
                status.ErrorMessage = ((EveningCounterStatusEnum)dbEntity.Status).ToString() + " request can not be recommended.";
                return status;
            }


            EveningCounterRequest request = new EveningCounterRequest();
            CopyObject<EveningCounterRequest>(dbEntity, ref request, "");
            request.StartDate = requestInstance.StartDate;
            request.EndDate = requestInstance.EndDate;
            request.Days = requestInstance.Days;

            status = ValidateAllowance(request);
            if (status.IsSuccess == false)
                return status;


            dbEntity.StartDate = request.StartDate;
            dbEntity.EndDate = request.EndDate;
            dbEntity.Days = request.Days;
            dbEntity.MonthName = request.MonthName;
            dbEntity.MonthTotalDays = request.MonthTotalDays;
            dbEntity.Status = requestInstance.Status;
            dbEntity.RecommendedBy = SessionManager.CurrentLoggedInEmployeeId;
            dbEntity.RecommendedOn = GetCurrentDateAndTime();


            foreach (var editHist in edits)
            {
                editHist.RequestID = dbEntity.CounterRequestID;

                if (SessionManager.User.EEmployee != null && SessionManager.User.EEmployee.EmployeeId != null)
                {
                    editHist.ModifiedBy = SessionManager.User.EEmployee.EmployeeId.ToString();
                    editHist.ModifiedByName = SessionManager.User.EEmployee.Name;
                }
                else if (SessionManager.User != null && SessionManager.User.UserID != null)
                {
                    editHist.ModifiedBy = SessionManager.User.UserID.ToString();
                    editHist.ModifiedByName = SessionManager.User.UserName;
                }
                //editHist.ModifiedBy = SessionManager.User.UserID;

                editHist.ModifiedOn = CommonManager.GetCurrentDateAndTime();
                editHist.RequestType = (int)RequestHistoryTypeEnum.EveningCounter;
            }

            PayrollDataContext.RequestEditHistories.InsertAllOnSubmit(edits);

            PayrollDataContext.SubmitChanges();

            SendEmail(dbEntity, false);

            return status;
        }

        public static bool RejectRequest(int requestID)
        {
            EveningCounterRequest dbEntity = new EveningCounterRequest();
            dbEntity = PayrollDataContext.EveningCounterRequests.Where(x => x.CounterRequestID == requestID).SingleOrDefault();
            if (dbEntity != null)
            {
                dbEntity.Status = (int)EveningCounterStatusEnum.Rejected;

                dbEntity.RejectedBy = SessionManager.CurrentLoggedInEmployeeId;
                dbEntity.RejectedOn = GetCurrentDateAndTime();

                PayrollDataContext.SubmitChanges();
            }
            return true;
        }

        public static bool ForwardRequestInBulk(List<EveningCounterRequest> requests, out int count)
        {
            Dictionary<string, string> inserted = new System.Collections.Generic.Dictionary<string, string>();

            count = 0;
            EveningCounterRequest reqInstance;
            foreach (EveningCounterRequest req in requests)
            {
                reqInstance = new EveningCounterRequest();
                reqInstance = PayrollDataContext.EveningCounterRequests.Where(x => x.CounterRequestID == req.CounterRequestID).FirstOrDefault();
                if (reqInstance != null && reqInstance.Status == (int)OvertimeStatusEnum.Approved)
                {
                    reqInstance.Status = (int)EveningCounterStatusEnum.Forwarded;
                    reqInstance.ForwardedBy = SessionManager.CurrentLoggedInUserID;
                    reqInstance.ForwardedOn = CommonManager.GetCurrentDateAndTime();
                    count += 1;

                    EveningCounterType overtimeType = PayrollDataContext.EveningCounterTypes.FirstOrDefault(x => x.EveningCounterTypeId == reqInstance.EveningCounterTypeId);
                    if (overtimeType != null && overtimeType.IncomeId != null)
                    {
                        if (PayrollDataContext.PEmployeeIncomes.Any(x => x.EmployeeId == reqInstance.EmployeeID && x.IncomeId == overtimeType.IncomeId.Value) == false)
                        {
                            // prevent double insertion in this mode
                            if (inserted.ContainsKey(reqInstance.EmployeeID.Value + ":" + overtimeType.IncomeId.Value) == false)
                            {
                                PEmployeeIncome overtimeInc = new PEmployeeIncome();
                                overtimeInc.EmployeeId = reqInstance.EmployeeID.Value;
                                overtimeInc.IncomeId = overtimeType.IncomeId.Value;
                                overtimeInc.IsValid = true;
                                overtimeInc.IsEnabled = true;

                                PayrollDataContext.PEmployeeIncomes.InsertOnSubmit(overtimeInc);
                                inserted.Add(reqInstance.EmployeeID + ":" + overtimeType.IncomeId.Value, "");
                            }
                        }
                    }
                }
            }

            PayrollDataContext.SubmitChanges();
            return true;
        }


        public static bool RejectRequestInBulk(List<EveningCounterRequest> requests, out int count)
        {
            Dictionary<string, string> inserted = new System.Collections.Generic.Dictionary<string, string>();

            count = 0;
            EveningCounterRequest reqInstance;
            foreach (EveningCounterRequest req in requests)
            {
                reqInstance = new EveningCounterRequest();
                reqInstance = PayrollDataContext.EveningCounterRequests.Where(x => x.CounterRequestID == req.CounterRequestID).FirstOrDefault();
                if (reqInstance != null && reqInstance.Status == (int)OvertimeStatusEnum.Approved)
                {
                    reqInstance.Status = (int)EveningCounterStatusEnum.Rejected;
                    reqInstance.ForwardedBy = SessionManager.CurrentLoggedInUserID;
                    reqInstance.ForwardedOn = CommonManager.GetCurrentDateAndTime();
                    count += 1;

                   
                }
            }

            PayrollDataContext.SubmitChanges();
            return true;
        }

        public static ResponseStatus isRequestedDateValid(int? empID, DateTime? reqDate, bool isEdit)
        {
            ResponseStatus status = new ResponseStatus();
            status.IsSuccessType = true;


            DateTime today = SessionManager.GetCurrentDateAndTime();
            today = new DateTime(today.Year, today.Month, today.Day);


            //if (reqDate.Value > today)
            //{
            //    status.IsSuccessType = false;
            //    status.ErrorMessage = "Evening Counter Request date can not be in Future";
            //    return status;
            //}




            //int PermitedDays = int.Parse(PayrollDataContext.OvertimeRoundings.First().OvertimePermitedDays.Value.ToString());
            //DateTime PermitedDate = today.AddDays(-PermitedDays);
            //if (reqDate.Value < PermitedDate)
            //{
            //    status.IsSuccessType = false;
            //    status.ErrorMessage = "Can not request Overtime in the past days.";
            //    return status;
            //}



            if (isEdit == false)
            {

                if (
                    PayrollDataContext.EveningCounterRequests.Any(x => x.EmployeeID == empID
                        && x.StartDate == reqDate && x.Status != (int)OvertimeStatusEnum.Rejected
                       ))
                {

                    status.IsSuccessType = false;
                    status.ErrorMessage = "Evening Counter request already exists for this day and time.";
                    return status;
                }
            }

            return status;
        }


        public static List<GetEveningCounterRequestForEmpSPResult> GetEveningCounterRequestForEmpReport(int start, int pagesize, int EmoloyeeID, int period, int status, int eveningCounterTypeId)
        {
            List<GetEveningCounterRequestForEmpSPResult> resultSet = PayrollDataContext.GetEveningCounterRequestForEmpSP(start, 999999, EmoloyeeID, period, status, eveningCounterTypeId).ToList();

            foreach (var val1 in resultSet)
            {
                val1.StatusStr = ((EveningCounterStatusEnum)val1.Status).ToString();

                if (val1.EveningCounterTypeId != null)
                {
                    val1.EveningCounterTypeStr = GetEveningCounterType(val1.EveningCounterTypeId.Value).Name;
                }
            }


            return resultSet;
        }



        public static TAAllowanceLocation GetLocationByID(string LocationID)
        {
            TAAllowanceLocation loc = new TAAllowanceLocation();
            if (!string.IsNullOrEmpty(LocationID))
            {
                if (PayrollDataContext.TAAllowanceLocations.Any(x => x.LocationId == int.Parse(LocationID)))
                {
                    loc = PayrollDataContext.TAAllowanceLocations.FirstOrDefault(x => x.LocationId == int.Parse(LocationID));
                }
            }
            return loc;
        }

        public static List<GetRequestEditHistoryResult> GetAllowanceHistory(int RequestID, int RequestType)
        {

            //return PayrollDataContext.GetRequestEditHistory(RequestID, RequestType).ToList();
            List<GetRequestEditHistoryResult> thisList = new List<GetRequestEditHistoryResult>();
            thisList = PayrollDataContext.GetRequestEditHistory(RequestID, RequestType).ToList();
            return thisList;
        }

        public static void SendEmail(EveningCounterRequest obj, bool SendEmailToRecommender)
        {
            string to = "", bcc = "", body = "", subject = "";

            string allowance = GetEveningCounterType(obj.EveningCounterTypeId.Value).Name;
            string employeeName = EmployeeManager.GetEmployeeById(obj.EmployeeID.Value).Name;
           
            //body = "You are requested to approve the allowance of " + EmployeeManager.GetEmployeeById(obj.EmployeeID.Value).Name + " starting from " +
            //    obj.StartDate + " to " + obj.EndDate;

            if (SendEmailToRecommender)
            {
                to = UserManager.GetEmployeeUserEmail(obj.RecommendedBy.Value).Trim();
                subject = string.Format("{0} Allowance Recommendation of {1}.", allowance, employeeName);
                body = string.Format("You are requested to recommend the {0} allowance of {1}, starting from {2} to {3}. ", allowance, employeeName, obj.StartDate, obj.EndDate);
            }
            else
            {
                to = UserManager.GetEmployeeUserEmail(obj.ApprovalID.Value).Trim();
                subject = string.Format("{0} Allowance Approval of {1}.", allowance, employeeName);

                body = string.Format("You are requested to approve the {0} allowance of {1}, starting from {2} to {3}. ", allowance, employeeName, obj.StartDate, obj.EndDate);
            }

            if (to == "")
                return;

            // If email log is enabled then process to track email sending as we need synchronous process
            if (CommonManager.Setting.EnableEmailLog != null && CommonManager.Setting.EnableEmailLog.Value)
            {
                string errorMsg = "";
                bool emailStatus = Utils.Helper.SMTPHelper.SendMail(to, body, subject, bcc, "", ref errorMsg);

                PayrollDataContext.EmailLogs.InsertOnSubmit(GetEmailLog(obj.EmployeeID.Value, "", EmailLogTypeEnum.AllowanceRequest, obj.CounterRequestID,
                    to + ":to,bcc:" + (bcc == null ? "" : bcc), subject, emailStatus, errorMsg));
                PayrollDataContext.SubmitChanges();

            }
            else
                Utils.Helper.SMTPHelper.SendAsyncMail(to, body, subject, bcc);
        }
        public static void SendApprovedEmailNotification(EveningCounterRequest obj)
        {
            string to = "", bcc = "", body = "", subject = "";

            string allowance = GetEveningCounterType(obj.EveningCounterTypeId.Value).Name;
            //string employeeName = EmployeeManager.GetEmployeeById(obj.EmployeeID.Value).Name;

            //body = "You are requested to approve the allowance of " + EmployeeManager.GetEmployeeById(obj.EmployeeID.Value).Name + " starting from " +
            //    obj.StartDate + " to " + obj.EndDate;

            
            {
                to = UserManager.GetEmployeeUserEmail(obj.EmployeeID.Value).Trim();
                subject = string.Format("Your allowance \"{0}\" from {1} to {2} has been approved.", allowance, 
                    obj.StartDate.Value.ToString("dd-MMM-yyyy"),
                    obj.EndDate.Value.ToString("dd-MMM-yyyy"));

                body = string.Format("<br><br>Your allowance \"{0}\" from {1} to {2} has been approved.<br><br>Regards<br>HR", allowance,
                    obj.StartDate.Value.ToString("dd-MMM-yyyy"),
                    obj.EndDate.Value.ToString("dd-MMM-yyyy"));
            }

            if (to == "")
                return;

            // If email log is enabled then process to track email sending as we need synchronous process
            if (CommonManager.Setting.EnableEmailLog != null && CommonManager.Setting.EnableEmailLog.Value)
            {
                string errorMsg = "";
                bool emailStatus = Utils.Helper.SMTPHelper.SendMail(to, body, subject, bcc, "", ref errorMsg);

                PayrollDataContext.EmailLogs.InsertOnSubmit(GetEmailLog(obj.EmployeeID.Value, "", EmailLogTypeEnum.AllowanceApproval, obj.CounterRequestID,
                    to + ":to,bcc:" + (bcc == null ? "" : bcc), subject, emailStatus, errorMsg));
                PayrollDataContext.SubmitChanges();

            }
            else
                Utils.Helper.SMTPHelper.SendAsyncMail(to, body, subject, bcc);
        }
    }
}
   
