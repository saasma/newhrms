﻿using System.Collections.Generic;
using System.Linq;
using DAL;
using Utils.Calendar;
using System;

namespace BLL.Manager
{
    public class StopPaymentManager : BaseBiz
    {

        public InsertUpdateStatus ValidateNotUsedStopPayment(StopPayment entity)
        {
            InsertUpdateStatus status = new InsertUpdateStatus();
            status.IsSuccess = true;
            //Check validation
            
            // Full month stop payment can not be done if there is Advance adjustment in any paryoll period month
            //if (dbEntity == null && loan.PayrollPeriodId != 0)
            {
              //  PayrollPeriod payrollPeriod = CommonManager.GetPayrollPeriod(loan.PayrollPeriodId);
                //if (PayrollDataContext.AdvanceAdjustments.Any(x => x.EmployeeId == entity.EmployeeId &&
                //    payrollPeriod.StartDateEng >= x.EngFromDate && payrollPeriod.EndDateEng <= x.EngToDate))
                List<PayrollPeriod> periods =
                (
                from a in PayrollDataContext.AdvanceAdjustments
                join p in PayrollDataContext.PayrollPeriods on a.PayrollPeriodId equals p.PayrollPeriodId
                where a.EmployeeId == entity.EmployeeId &&
                 p.StartDateEng >= entity.EngFromDate && p.EndDateEng <= entity.EngToDate
                select p
                ).ToList();

                if (periods.Count > 0)
                {
                    status.ErrorMessage =
                        string.Format("Advance adjustment exists for this stop payment period, for the month of {0}, can not save this stop payment.",
                        periods[0].Name);
                    status.IsSuccess = false;
                    return status;
                }
            }
            
            //1. could not be before salary saved Payroll Period Date
            PayrollPeriod payrollPeriod = CommonManager.GetValidLastPayrollPeriod(entity.EmployeeId.Value);
            if (payrollPeriod != null)
            {
                bool isCalculationSaved =
                    CalculationManager.IsCalculationSavedForEmployee(payrollPeriod.PayrollPeriodId, entity.EmployeeId.Value);

                if (isCalculationSaved)
                {
                    if (entity.EngFromDate < payrollPeriod.EndDateEng)
                    {
                        status.ErrorMessage = "Start date can not be before the salary saved Payroll Period.";
                        status.IsSuccess = false;
                        return status;
                    }
                }
                else
                {
                    if (entity.EngToDate < (payrollPeriod.StartDateEng.Value.AddDays(-1)))
                    {
                        status.ErrorMessage = "Date can not be before the salary saved Payroll Period.";
                        status.IsSuccess = false;
                        return status;
                    }
                }
            }


            //2. can not be within the same month
            CustomDate from = CustomDate.GetCustomDateFromString(entity.FromDate, IsEnglish);
            CustomDate to = CustomDate.GetCustomDateFromString(entity.ToDate, IsEnglish);
            int totalDays =  DateHelper.GetTotalDaysInTheMonth(to.Year, to.Month, IsEnglish);
            // for same month, start date should be the first day & end date should be the last day of the month
            if (from.Month == to.Month && from.Year == to.Year
                && (from.Day != 1 || to.Day != totalDays))
            {
                status.ErrorMessage = "If Start and End date are in the same month then start day should be the first day & end date should be the last day of the month.";
                status.IsSuccess = false;
                return status;
            }

            //3. New Stop payment start date should not lie in final salary saved payroll period
            PayrollPeriod finalSavedPayrollPeriod = 
                (
                    from p in PayrollDataContext.PayrollPeriods
                    join c in PayrollDataContext.CCalculations on p.PayrollPeriodId equals c.PayrollPeriodId
                    where c.IsFinalSaved == true && entity.EngToDate < p.EndDateEng
                        && p.CompanyId== SessionManager.CurrentCompanyId
                    select p
                ).FirstOrDefault();
            //for new Stop Payment only
            if (finalSavedPayrollPeriod != null && entity.StopPaymentId==0)
            {
                status.ErrorMessage = "Start date should not lie in already final salary saved payroll period.";
                status.IsSuccess = false;
                return status;
            }

            //4. New Stop payment start date should not lie in initial salary saved payroll period for this employee
            PayrollPeriod savedPayrollPeriod =
                (
                    from p in PayrollDataContext.PayrollPeriods
                    join c in PayrollDataContext.CCalculations on p.PayrollPeriodId equals c.PayrollPeriodId
                    join ce in PayrollDataContext.CCalculationEmployees on
                        new { c.CalculationId, entity.EmployeeId } equals new { ce.CalculationId, ce.EmployeeId }

                    where entity.EngFromDate <= p.EndDateEng && p.CompanyId == SessionManager.CurrentCompanyId
                    select p
                ).FirstOrDefault();
            //for new Stop Payment only
            if (savedPayrollPeriod != null && entity.StopPaymentId == 0)
            {
                status.ErrorMessage = "Start date should not lie in already salary saved payroll period.";
                status.IsSuccess = false;
                return status;
            }

            //get before stop payment
            StopPayment lastStopPayment = PayrollDataContext.StopPayments
                .Where(x => x.EmployeeId == entity.EmployeeId
                 && (x.StopPaymentId == 0 || x.StopPaymentId != entity.StopPaymentId))
                .OrderByDescending(x => x.StopPaymentId)
                .Take(1).SingleOrDefault();

            if (lastStopPayment != null)
            {
                from = CustomDate.GetCustomDateFromString(lastStopPayment.ToDate, IsEnglish);
                to = CustomDate.GetCustomDateFromString(entity.FromDate, IsEnglish);


                if (lastStopPayment.EngToDate >= entity.EngFromDate
                 || (from.Month == to.Month && from.Year == to.Year))
                {
                    status.ErrorMessage = "Last Stop Payment date can not intercept current stop payment.";
                    status.IsSuccess = false;
                    return status;
                }
            }

            return status;

        }

        public InsertUpdateStatus Save(StopPayment entity)
        {

            InsertUpdateStatus status = ValidateNotUsedStopPayment(entity);

            if (!status.IsSuccess)
                return status;


            entity.CreatedDate = GetCurrentDateAndTime();

            PayrollDataContext.ChangeMonetories.InsertOnSubmit(
                   GetMonetoryChangeLog(entity.EmployeeId.Value, (byte)MonetoryChangeLogTypeEnum.StopPayment
                   , "", "", entity.FromDate + " - " + entity.ToDate, LogActionEnum.Add));

            PayrollDataContext.StopPayments.InsertOnSubmit(entity);
            status.IsSuccess = SaveChangeSet();

            return status;
        }


        public bool IsStopPaymentDeletable(int stopPaymentId)
        {
            StopPayment entity = GetStopPaymentById(stopPaymentId);
            if (entity == null)
                return true;

            //4. New Stop payment start date should not lie in initial salary saved payroll period for this employee
            PayrollPeriod savedPayrollPeriod =
                (
                    from p in PayrollDataContext.PayrollPeriods
                    join c in PayrollDataContext.CCalculations on p.PayrollPeriodId equals c.PayrollPeriodId
                  

                    where c.IsFinalSaved == true &&  entity.EngFromDate <= p.EndDateEng
                     && p.CompanyId== SessionManager.CurrentCompanyId
                    select p
                ).FirstOrDefault();


            return savedPayrollPeriod != null;
        }

        public bool IsStopPaymentAlreadyUsed(int stopPaymentId)
        {
            StopPayment entity = GetStopPaymentById(stopPaymentId);
            if (entity == null)
                return true;
            PayrollPeriod payrollPeriod = CommonManager.GetValidLastPayrollPeriod(entity.EmployeeId.Value);
            if (payrollPeriod != null)
            {
                bool isCalculationSaved =
                    CalculationManager.IsCalculationSavedForEmployee(payrollPeriod.PayrollPeriodId, entity.EmployeeId.Value);

                if (isCalculationSaved)
                {
                    if (entity.EngFromDate <= payrollPeriod.EndDateEng)
                    {
                        return true;
                    }
                }
                else
                {
                    if (entity.EngFromDate < payrollPeriod.StartDateEng)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public bool IsStopPaymentEnded(int stopPaymentId)
        {
            StopPayment entity = GetStopPaymentById(stopPaymentId);
            if (entity == null)
                return true;

            PayrollPeriod payrollPeriod = CommonManager.GetLastPayrollPeriod();
            if (payrollPeriod != null)
            {
                bool isCalculationSaved =
                    CalculationManager.IsCalculationSavedForEmployee(payrollPeriod.PayrollPeriodId, entity.EmployeeId.Value);

                if (isCalculationSaved)
                {
                    if (entity.EngToDate <= payrollPeriod.EndDateEng)
                    {
                        return true;
                    }
                }
                else
                {
                    if (entity.EngToDate < payrollPeriod.StartDateEng)
                    {
                        return true;
                    }
                }
            }
            return false;
        }


        public List<GetStopPaymentsResult> GetStopPayments(int companyId,bool showArchieve)
        {
            string customRoleDeparmentList = SessionManager.CustomRoleDeparmentIDList;

            DateTime? fromDate = null;
            if (!showArchieve)
            {
                // Get last final saved payroll period
                PayrollPeriod lastPayroll = CommonManager.GetLastPayrollPeriod();
                if (lastPayroll != null)
                {
                    bool isPayrollFinalSaved = CommonManager.IsPayrollFinalSaved(lastPayroll.PayrollPeriodId);
                    if (isPayrollFinalSaved)
                        fromDate = lastPayroll.EndDateEng.Value.AddDays(1);
                    else
                        fromDate = lastPayroll.StartDateEng.Value;
                }
            }

            return PayrollDataContext.GetStopPayments(companyId, customRoleDeparmentList,fromDate).ToList();
        }

        public StopPayment GetStopPaymentById(int stopPaymentId)
        {
            return PayrollDataContext.StopPayments.SingleOrDefault(e => e.StopPaymentId == stopPaymentId);
        }

        public bool Delete(int stopPayment)
        {
            StopPayment payment = GetStopPaymentById(stopPayment);

            PayrollDataContext.ChangeMonetories.InsertOnSubmit(
                 GetMonetoryChangeLog(payment.EmployeeId.Value, (byte)MonetoryChangeLogTypeEnum.StopPayment
                 , "", payment.FromDate + " - " + payment.ToDate, "", LogActionEnum.Delete));

            PayrollDataContext.StopPayments.DeleteOnSubmit(payment);
            return DeleteChangeSet();
        }


        //public bool IsStopPaymentAlreadExists(int employeeId)
        //{
        //    StopPayment entity = PayrollDataContext.StopPayments.SingleOrDefault
        //        (e => (e.EmployeeId == employeeId && e.ToDate == null));
        //    return entity != null;
        //}

        //public bool UpdateToDate(int stopPaymentId, CustomDate toDate)
        //{
        //    StopPayment dbEntity = PayrollDataContext.StopPayments.SingleOrDefault
        //       (e => e.StopPaymentId == stopPaymentId);

        //    dbEntity.ToDate = toDate.ToString();
        //    dbEntity.EngToDate = BizHelper.GetConvertedToEngDate(toDate);


        //    return UpdateChangeSet();
        //}

        public InsertUpdateStatus Update(StopPayment entity)
        {
            StopPayment dbEntity = PayrollDataContext.StopPayments.SingleOrDefault
                (e => e.StopPaymentId == entity.StopPaymentId);

            InsertUpdateStatus status = new InsertUpdateStatus();
            status.IsSuccess = true;

            if (dbEntity == null)
                return status;


            bool isStopPaymentUsed = IsStopPaymentAlreadyUsed(entity.StopPaymentId);

            if (!isStopPaymentUsed)
            {
                status = ValidateNotUsedStopPayment(entity);
                if (!status.IsSuccess)
                    return status;
            }
            else
            {
                //3. Stop payment end date should not lie in final salary saved payroll period
                PayrollPeriod finalSavedPayrollPeriod =
                    (
                        from p in PayrollDataContext.PayrollPeriods
                        join c in PayrollDataContext.CCalculations on p.PayrollPeriodId equals c.PayrollPeriodId
                        where c.IsFinalSaved == true && entity.EngToDate < p.EndDateEng
                        select p
                    ).FirstOrDefault();
                if (finalSavedPayrollPeriod != null)
                {
                    status.ErrorMessage = "End date should not lie in already final saved payroll period.";
                    status.IsSuccess = false;
                    return status;
                }
            }


            PayrollDataContext.ChangeMonetories.InsertOnSubmit(
                 GetMonetoryChangeLog(entity.EmployeeId.Value, (byte)MonetoryChangeLogTypeEnum.StopPayment
                 , "", dbEntity.FromDate + " - " + dbEntity.ToDate, entity.FromDate + " - " + entity.ToDate, LogActionEnum.Update));

            dbEntity.EmployeeId = entity.EmployeeId;
            dbEntity.FromDate = entity.FromDate;
            dbEntity.EngFromDate = entity.EngFromDate;

            dbEntity.ToDate = entity.ToDate;
            dbEntity.EngToDate = entity.EngToDate;

            dbEntity.Notes = entity.Notes;

            dbEntity.TreatLikeRetirementForTax = entity.TreatLikeRetirementForTax;
            dbEntity.ExcludePeriodInWorkDaysCount = entity.ExcludePeriodInWorkDaysCount;


            



            UpdateChangeSet();
            return status;
        }

    }
}