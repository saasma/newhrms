﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using DAL;
using Utils;
using System.Web.UI;
using Utils.Calendar;
using System.Threading;
using System.Xml;
using Utils.Helper;
using BLL.BO;
using System.Web;
using System.Text;
using BLL.Entity;

namespace BLL.Manager
{
    /// <summary>
    /// Delegate type for passing to check if the column is the required type or not
    /// </summary>   
    public delegate bool IsRequiredColumn(CalculationColumnType type);

    public class CalculationManager : BaseBiz
    {
        public const string incomeColor = "#1B93D0";
        public const string deductionColor = "#1B93D0";
        public const string netColor = "#1B93D0";
        public const string idIncomeAdjCheckbox = "chkIncomeAdj";
        public const string idDeductionAdjCheckbox = "chkDeductionAdj";

        #region "Database access types"


       /*

        public static string GetIncomeList(int employeeId, int payrollPeriodId, int companyId,
            bool isFiscalYearStartingMonthExists, int startId, int endId,bool dumpTax) // int notWorkedMonthInFiscalYear, int passedMonthWorked,

        {

            DbConnection conn = new SqlConnection(Config.ConnectionString);

            //PayrollDataContext.Connection;
            List<EEmployee> employees = new List<EEmployee>();
            using (conn)
            {
                DbCommand selectCommand = conn.CreateCommand();
                selectCommand.CommandText = "[CalGetIncomeListSP]";
                selectCommand.CommandType = CommandType.StoredProcedure;



                selectCommand.Parameters.Add(new SqlParameter("@CompanyId", SqlDbType.Int));
                selectCommand.Parameters.Add(new SqlParameter("@EmployeeId", SqlDbType.Int));
                selectCommand.Parameters.Add(new SqlParameter("@PayrollPeriodId", SqlDbType.Int));
                selectCommand.Parameters.Add(new SqlParameter("@IsFiscalYearStartingMonthExists", SqlDbType.Bit));
                //selectCommand.Parameters.Add(new SqlParameter("@NotWorkedMonthInFiscalYear", SqlDbType.Int));
               // selectCommand.Parameters.Add(new SqlParameter("@PassedMonthWorked", SqlDbType.Int));
                selectCommand.Parameters.Add(new SqlParameter("@PayrollStartId", SqlDbType.Int));
                selectCommand.Parameters.Add(new SqlParameter("@PayrollEndId", SqlDbType.Int));
                selectCommand.Parameters.Add(new SqlParameter("@DumpTax", SqlDbType.Bit));
                selectCommand.Parameters.Add(new SqlParameter("@OptimumPFAndCITCase", SqlDbType.Bit));

                selectCommand.Parameters.Add(new SqlParameter("@TaxAdjustmentCalcCase", SqlDbType.Bit));

                selectCommand.Parameters.Add(new SqlParameter("@ListData", SqlDbType.VarChar,2000));
                selectCommand.Parameters["@ListData"].Direction = ParameterDirection.Output;

                selectCommand.Parameters["@CompanyId"].Value = companyId;
                selectCommand.Parameters["@EmployeeId"].Value = employeeId;
                selectCommand.Parameters["@PayrollPeriodId"].Value = payrollPeriodId;

                selectCommand.Parameters["@IsFiscalYearStartingMonthExists"].Value = isFiscalYearStartingMonthExists;
                //selectCommand.Parameters["@NotWorkedMonthInFiscalYear"].Value = notWorkedMonthInFiscalYear;
                //selectCommand.Parameters["@PassedMonthWorked"].Value = passedMonthWorked;
                selectCommand.Parameters["@PayrollStartId"].Value = startId;
                selectCommand.Parameters["@PayrollEndId"].Value = endId;
                selectCommand.Parameters["@DumpTax"].Value = dumpTax;
               // selectCommand.Parameters["@OptimumPFAndCITCasex"].Value = false;//always false from here
                selectCommand.Parameters["@ListData"].Value = "";

                selectCommand.Parameters["@TaxAdjustmentCalcCase"].Value = true;

                SetConnectionPwd(conn);

                DbDataReader reader = selectCommand.ExecuteReader();
                using (reader)
                {
                    while (reader.Read())
                    {


                        string list = DBNull.Value.Equals(reader["List"])
                                          ? ""
                                          : reader["List"].ToString();

                        string DumpTaxText =

                            DBNull.Value.Equals(reader["DumpTaxText"])
                                          ? ""
                                          : reader["DumpTaxText"].ToString();

                        if (!string.IsNullOrEmpty(DumpTaxText))
                        {
                            try
                            {
                                System.IO.File.WriteAllText(HttpContext.Current.Server.MapPath("~/App_Data/dump.html"), DumpTaxText);
                            }
                            catch (Exception exp1)
                            {

                            }
                        }

                        return list;
                    }
                }
                return "";
            }
        }
	*/
        //public static void ClearVariableAndIncomeAjdustmentOnFirstLoad()
        //{
        //    PayrollDataContext.ClearVariableAndAjdustmentIncomeOnFirstLoad(SessionManager.CurrentCompanyId,null);
        //}

        //public  static  string HandleToReCalcTaxAfterAmountChangedInGrid(int empid,
        //    CalculationColumnType columnType,int sourceId,decimal amount,int payrollPeriodId,bool isResignedOrRetiredOnly)
        //{
        //    //first update into EEmployee table for income adj. change or PEmployeeIncome or PEmployeeDeduction
        //    switch (columnType)
        //    {
        //        case CalculationColumnType.IncomeAdjustment:
        //            EmployeeManager mgr = new EmployeeManager();
        //            EEmployee emp =  EmployeeManager.GetEmployeeById(empid);
        //            emp.TempIncomeAdjustmentAmount = amount;
        //            mgr.Update(emp,null);
        //            break;
        //        case CalculationColumnType.Income:
        //            PayManager.UpdateTempEmployeeIncome(empid, sourceId, amount);
        //            break;
        //        case CalculationColumnType.Deduction:
                    
        //            PayManager.UpdateTempEmployeeDeduction(empid, sourceId, amount);
        //            break;
        //    }

        //    int? total = 0;
        //    List<CalcGetCalculationListResult> result = GetCalculationList(SessionManager.CurrentCompanyId, payrollPeriodId, 0,
        //                                    8, ref total, isResignedOrRetiredOnly, empid,"",(int)SalaryType.Combined);

        //    if (result.Count > 0)
        //        return result[0].IncomeList;
        //    return "";
        //}

        public static CCalculation GetCalculation(int payrollperidId)
        {
            
            return
                PayrollDataContext.CCalculations.Where(c => c.PayrollPeriodId == payrollperidId).FirstOrDefault();
        }
        public static CCalculation GetCalculationById(int calculationID)
        {

            return
                PayrollDataContext.CCalculations.Where(c => c.CalculationId == calculationID).FirstOrDefault();
        }
        public static CurrencyDetail CurrencyDetail
        {
            get
            {
                return PayrollDataContext.CurrencyDetails.FirstOrDefault();
            }
        }

        public static void InsertHeaderAfterSalaryAlreadySaved(int type, int sourceId,int periodId)
        {
            // for income or deemed income type type only
            if (type == 1 || type == 25)
            {
                PIncome income = PayManager.GetIncome(sourceId);

                CCalculation calculation = CalculationManager.GetCalculation(periodId);
                if (calculation != null)
                {
                    if (PayrollDataContext.CCalculationHeaders.Any(x => x.CalculationId == calculation.CalculationId
                        && x.Type == type && x.SourceId == sourceId) == false)
                    {
                        CCalculationHeader header = new CCalculationHeader();
                        header.CalculationId = calculation.CalculationId;
                        header.HeaderName = income.Title;
                        header.IsIncome = true;
                        header.SourceId = sourceId;
                        header.Type = type;

                        PayrollDataContext.CCalculationHeaders.InsertOnSubmit(header);

                        PayrollDataContext.SubmitChanges();
                    }
                }
            }
        }

        public static string GetIncomeOrDeductionNotMappedForVoucher(int payrollPeriodId)
        {
            return PayrollDataContext.GetUnMappedVoucherHeader(payrollPeriodId);
        }

       
        public static decimal? GetPayrollCashPaidSum(int month,int year,int branchId,int depId)
        {

            decimal? sum
                = PayrollDataContext.Report_Pay_Cash(
                month, year, SessionManager.CurrentCompanyId, branchId, depId, -1, -1)
                .Sum(x => x.Amount);

            return sum;

        }

        public static void GenerateAddOnTax(int payrollPeriodid, bool? payAllTaxNow)
        {

            bool readingSumForMiddleFiscalYearStartedReqd = false;
            //contains the id of starting-ending payroll period to be read for history of regular income
            int startingPayrollPeriodId = 0, endingPayrollPeriodId = 0;
            // int notWorkedMonthInFiscalYear = 0, passedMonthWorked = 0;


            PayrollPeriod startingMonthParollPeriodInFiscalYear =
                GenerateForPastIncome(
                    payrollPeriodid, ref readingSumForMiddleFiscalYearStartedReqd,
                    ref startingPayrollPeriodId, ref endingPayrollPeriodId);

            PayrollDataContext.GenerateAddOnTax(payrollPeriodid, startingPayrollPeriodId, endingPayrollPeriodId,
                SessionManager.CurrentCompanyId, payAllTaxNow);
        
        }

        public static bool IsEmployeeIncomeSavedInSalary(int incomeId, int empId)
        {
            return
                (
                from e in PayrollDataContext.CCalculationEmployees
                join cd in PayrollDataContext.CCalculationDetails on e.CalculationEmployeeId equals cd.CalculationEmployeeId
                join ch in PayrollDataContext.CCalculationHeaders on cd.CalculationHeaderId equals ch.CalculationHeaderId
                where e.EmployeeId == empId && ch.Type == 1 && ch.SourceId == incomeId
                select cd
                ).Any();
        }
        
        public static string GenerateNewAddOnTax(int payrollPeriodid, bool? payAllTaxNow, int addOnId, bool
            userOtherAddOnThisMonth,int? empId,bool dumpTax)
        {

            bool readingSumForMiddleFiscalYearStartedReqd = false;
            //contains the id of starting-ending payroll period to be read for history of regular income
            int startingPayrollPeriodId = 0, endingPayrollPeriodId = 0;
            // int notWorkedMonthInFiscalYear = 0, passedMonthWorked = 0;


            PayrollPeriod startingMonthParollPeriodInFiscalYear =
                GenerateForPastIncome(
                    payrollPeriodid, ref readingSumForMiddleFiscalYearStartedReqd,
                    ref startingPayrollPeriodId, ref endingPayrollPeriodId);

           GenerateNewAddOnTaxResult result =  PayrollDataContext.GenerateNewAddOnTax(payrollPeriodid, startingPayrollPeriodId, endingPayrollPeriodId,
                SessionManager.CurrentCompanyId, payAllTaxNow, addOnId, userOtherAddOnThisMonth, dumpTax, empId)
                .FirstOrDefault();

           return result == null || result.TaxDetails == null ? "" : result.TaxDetails;
        }
        /// <summary>
        /// Checks if the current festival income already used in tax if from beginning
        /// option is selected
        /// </summary>
        /// <param name="employeeId"></param>
        /// <param name="incomeId"></param>
        /// <returns>true if already used in this fiscal year, otherwise false</returns>
        public static bool IsIncomeTaxAddedFromBeginningCase(int employeeId, int incomeId)
        {
            //get last payroll period
            PayrollPeriod payroll = CommonManager.GetLastPayrollPeriod();

            if (payroll != null)
            {
                bool readingSumForMiddleFiscalYearStartedReqd = false;
                //contains the id of starting-ending payroll period to be read for history of regular income
                int startingPayrollPeriodId = 0, endingPayrollPeriodId = 0;
                // int notWorkedMonthInFiscalYear = 0, passedMonthWorked = 0;


                PayrollPeriod startingMonthParollPeriodInFiscalYear =
                    GenerateForPastIncome(
                        payroll.PayrollPeriodId, ref readingSumForMiddleFiscalYearStartedReqd,
                        ref startingPayrollPeriodId, ref endingPayrollPeriodId);

                //as the current one which may not be completely saved so check this condition
                if (payroll.PayrollPeriodId > endingPayrollPeriodId)
                    endingPayrollPeriodId = payroll.PayrollPeriodId;

                //now check if the income exists within startingPayrollPeriodId and endingPayrollPeriodId
                if (startingPayrollPeriodId != 0)
                {
                    List<EmployeeIncomeTaxFromBeginning> list =
                        PayrollDataContext.EmployeeIncomeTaxFromBeginnings
                        .Where(e => e.EmployeeId == employeeId && e.IncomeId == incomeId
                            && (e.PayrollPeriodId >= startingPayrollPeriodId && e.PayrollPeriodId <= endingPayrollPeriodId))
                            .ToList();

                    if (list.Count > 0)
                    {
                        int taxUsedPayrollId = list[0].PayrollPeriodId;
                        //Now check the emp salary saved for this payroll then only true
                        return IsCalculationSavedForEmployee(taxUsedPayrollId, employeeId);
                    }
                            
                }
            }
            return false;
        }

        ///TODO: Don't remove this case as Equivalent function in db named "ChangePayrollPeriodRangeForTaxDueToStopPayment" exists
        ///for validity checking
        /// <summary>
        /// Filter the payroll period id due to stop payment "TreatLikeRetirementForTax =  true" option
        /// </summary>
        //public static void FilterPayrollPeriodForStopPayment(int employeeId, ref int startingPayrollPeriodId, ref int endingPayrollPeriodId
        //    , int currentPayrollPeriod)
        //{
        //    if (startingPayrollPeriodId == 0 && endingPayrollPeriodId == 0)
        //        return;
        //    PayrollPeriod startPayroll = CommonManager.GetPayrollPeriod(startingPayrollPeriodId);
        //    PayrollPeriod endPayroll = CommonManager.GetPayrollPeriod(endingPayrollPeriodId);
        //    PayrollPeriod currentPayroll = CommonManager.GetPayrollPeriod(currentPayrollPeriod);

        //    //get StopPayment whose end date lies in between the starting & ending payroll
        //    StopPayment stopPayment =
        //        (
        //            from s in PayrollDataContext.StopPayments
        //            where s.EmployeeId == employeeId
        //                && s.EngToDate >= startPayroll.StartDateEng && s.EngToDate <= currentPayroll.EndDateEng
        //            orderby s.EngToDate descending
        //            select s

        //        ).Take(1).SingleOrDefault();

        //    if (stopPayment != null && stopPayment.TreatLikeRetirementForTax == true)
        //    {
        //        //if end date lies in current payroll then 0 & 0
        //        if (stopPayment.EngToDate >= currentPayroll.StartDateEng && stopPayment.EngToDate <= currentPayroll.EndDateEng)
        //        {
        //            startingPayrollPeriodId = 0;
        //            endingPayrollPeriodId = 0;
        //            return;
        //        }


        //        //find the starting payroll in which the end date lies & that payroll period is withing starting & ending payroll                
        //        int value = startingPayrollPeriodId;
        //        PayrollPeriod actualStartingPayroll
        //            =
        //            (
        //                from p in PayrollDataContext.PayrollPeriods
        //                where stopPayment.EngToDate >= p.StartDateEng && stopPayment.EngToDate <= p.EndDateEng
        //                && p.PayrollPeriodId > value
        //                select p

        //            ).SingleOrDefault();

        //        if (actualStartingPayroll != null)
        //        {
        //            startingPayrollPeriodId = actualStartingPayroll.PayrollPeriodId;
        //        }
        //    }

        //}

        public static List<CalcGetCalculationListResult> GetPartialPaidEmployeeList(
            int companyId, int payrollPeriodId, int? currentPage, int? pageSize, ref int? total,int employeeId,string search)
        {
            List<CalcGetCalculationListResult> list = new List<CalcGetCalculationListResult>();
            int tot = 0;
            List<GetPartialPaidTaxListResult> dbList = PayManager.GetPartialPaidTaxList(payrollPeriodId, -1, -1, employeeId, currentPage.Value, pageSize.Value, ref tot, search);
            total = tot;
        //    string incomeList = "";
            PartialTax tax = PayrollDataContext.PartialTaxes.SingleOrDefault(x => x.PayrollPeriodId == payrollPeriodId);

            foreach (GetPartialPaidTaxListResult item in dbList)
            {
                item.IncomeList += ";1:" + (tax == null ? 0 : tax.IncomeId) + ":" + Convert.ToDecimal(item.IncomeAmount);
                item.IncomeList += ";" + ((int)CalculationColumnType.IncomePF) + ":" + ((int)CalculationColumnType.IncomePF) + ":" + Convert.ToDecimal(item.PFAmount);
                item.IncomeList += ";" + ((int)CalculationColumnType.DeductionCIT) + ":" + ((int)CalculationColumnType.DeductionCIT) + ":" + Convert.ToDecimal(item.CITAmount);
                item.IncomeList += ";" + ((int)CalculationColumnType.TDS) + ":" + ((int)CalculationColumnType.TDS) + ":" + Convert.ToDecimal(item.PaidAmount);
                item.IncomeList += ";" + ((int)CalculationColumnType.DeductionPF) + ":" + ((int)CalculationColumnType.DeductionPF) + ":" + (Convert.ToDecimal(item.DeductionPFAmount));

                item.IncomeList += ";" + ((int)CalculationColumnType.IncomeGross) + ":" + ((int)CalculationColumnType.IncomeGross) + ":" + Convert.ToDecimal(0);
                item.IncomeList += ";" + ((int)CalculationColumnType.DeductionTotal) + ":" + ((int)CalculationColumnType.DeductionTotal) + ":" + Convert.ToDecimal(0);

                item.IncomeList += ";" + ((int)CalculationColumnType.NetSalary) + ":" + ((int)CalculationColumnType.NetSalary) + ":" +
                    (Convert.ToDecimal(item.IncomeAmount) + Convert.ToDecimal(item.PFAmount) -
                    Convert.ToDecimal(item.CITAmount) - Convert.ToDecimal(item.PaidAmount) - (Convert.ToDecimal(item.PFAmount) * 2));

                list.Add(new CalcGetCalculationListResult
                {
                    EmployeeId = item.EmployeeId,
                    IsFinalSaved = 1,
                    IsAttendanceComplete = 1,
                    Name = item.Name,
                    IncomeList = item.IncomeList
                    , SalaryType= (int)SalaryType.Combined
                    , IsRetiredOrResigned=0
                    ,IDCardNo = item.IdCardNo
                    ,BankACNo = item.AccountNo
                    ,IsAddOn = true
                });
            }


            return list;
        }

        public static List<CalcGetCalculationListResult> GetAddOnEmployeeList(
            int companyId, int payrollPeriodId, int addOnId, int? currentPage, int? pageSize, ref int? total, 
            int employeeId, string searchEmp, bool showAllWithBlankEmpAlso,bool applyDateFilter,DateTime? addOnDateFilter
            ,int branchId,int depId,int unitId)
        {
            List<CalcGetCalculationListResult> list = new List<CalcGetCalculationListResult>();
            int tot = 0;
            List<GetAddOnEmployeeListResult> dbList = PayManager.GetAddOnList
                (payrollPeriodId, addOnId, branchId, depId, employeeId, currentPage.Value, pageSize.Value, 
                ref tot, searchEmp, showAllWithBlankEmpAlso, applyDateFilter, addOnDateFilter, unitId);
            total = tot;
            //    string incomeList = "";
            //PartialTax tax = PayrollDataContext.PartialTaxes.SingleOrDefault(x => x.PayrollPeriodId == payrollPeriodId);

            foreach (GetAddOnEmployeeListResult item in dbList)
            {
                //item.IncomeList += ";1:" + (tax == null ? 0 : tax.IncomeId) + ":" + Convert.ToDecimal(item.IncomeAmount);
                //item.IncomeList += ";" + ((int)CalculationColumnType.IncomePF) + ":" + ((int)CalculationColumnType.IncomePF) + ":" + Convert.ToDecimal(item.PFAmount);
                //item.IncomeList += ";" + ((int)CalculationColumnType.DeductionCIT) + ":" + ((int)CalculationColumnType.DeductionCIT) + ":" + Convert.ToDecimal(item.CITAmount);
                //item.IncomeList += ";" + ((int)CalculationColumnType.TDS) + ":" + ((int)CalculationColumnType.TDS) + ":" + Convert.ToDecimal(item.PaidAmount);
                //item.IncomeList += ";" + ((int)CalculationColumnType.DeductionPF) + ":" + ((int)CalculationColumnType.DeductionPF) + ":" + (Convert.ToDecimal(item.DeductionPFAmount));

                item.IncomeList += ";" + ((int)CalculationColumnType.IncomeGross) + ":" + ((int)CalculationColumnType.IncomeGross) + ":" + Convert.ToDecimal(0);
                item.IncomeList += ";" + ((int)CalculationColumnType.DeductionTotal) + ":" + ((int)CalculationColumnType.DeductionTotal) + ":" + Convert.ToDecimal(0);

                item.IncomeList += ";" + ((int)CalculationColumnType.NetSalary) + ":" + ((int)CalculationColumnType.NetSalary) + ":" + Convert.ToDecimal(0);// +":" +
                //(Convert.ToDecimal(item.IncomeAmount) + Convert.ToDecimal(item.PFAmount) -
                //Convert.ToDecimal(item.CITAmount) - Convert.ToDecimal(item.PaidAmount) - (Convert.ToDecimal(item.PFAmount) * 2));

                list.Add(new CalcGetCalculationListResult
                {
                    EmployeeId = item.EmployeeId,
                    IsFinalSaved = 1,
                    IsAttendanceComplete = 1,
                    Name = item.Name,
                    IncomeList = item.IncomeList
                    ,
                    SalaryType = (int)SalaryType.Combined
                    ,
                    IsRetiredOrResigned = 0
                    ,
                    IDCardNo = item.IdCardNo
                    ,
                    BankACNo = item.AccountNo
                    ,
                    IsAddOn = true
                });
            }


            return list;
        }
        public static List<CalcGetCalculationListResult> GetHoldPaymentCalculationDetails(
           int companyId, int payrollPeriodId, int? currentPage, int? pageSize, ref int? total)
        {

            List<HoldPayment> employeeList = PayrollDataContext.HoldPayments.Where(x => x.PayrollPeriodId == payrollPeriodId).ToList();

            // if salary not final saved & preview is clicked then regenerate the salary for Hold Payment for that employee only
            if (IsPayrollEditable(payrollPeriodId) == true)
            {
                bool readingSumForMiddleFiscalYearStartedReqd = false;
                //contains the id of starting-ending payroll period to be read for history of regular income
                int startingPayrollPeriodId = 0, endingPayrollPeriodId = 0;
                // int notWorkedMonthInFiscalYear = 0, passedMonthWorked = 0;

                PayrollPeriod startingMonthParollPeriodInFiscalYear =
                    GenerateForPastIncome(
                        payrollPeriodId, ref readingSumForMiddleFiscalYearStartedReqd,
                        ref startingPayrollPeriodId, ref endingPayrollPeriodId);


                foreach (HoldPayment item in employeeList)
                {
                    PayrollDataContext.CalcProcessToSaveHoldPayment(payrollPeriodId, readingSumForMiddleFiscalYearStartedReqd, startingPayrollPeriodId, endingPayrollPeriodId, item.EmployeeId,
                        LeaveAttendanceManager.GetTotalDaysInFiscalYearForPeriod(payrollPeriodId));
                }
            }

            List<CalcGetCalculationListResult> list = new List<CalcGetCalculationListResult>();
            int tot = 0;
            
            total = tot;
            string incomeList = "";
            //PartialTax tax = PayrollDataContext.PartialTaxes.SingleOrDefault(x => x.PayrollPeriodId == payrollPeriodId);

            foreach (HoldPayment item in employeeList)
            {
                List<HoldPaymentDetail> itemList = PayrollDataContext.HoldPaymentDetails.Where(x => x.HoldPaymentId == item.HoldPaymentId).ToList();

                incomeList = "";
                foreach (HoldPaymentDetail detail in itemList)
                {
                    //skip SST & TDS
                    if (detail.Type != (int)CalculationColumnType.SST && detail.Type != (int)CalculationColumnType.TDS)
                        incomeList += ";" + detail.Type + ":" + detail.SourceId + ":" + Convert.ToDecimal(detail.Amount);
                }


                list.Add(new CalcGetCalculationListResult
                {
                    EmployeeId = item.EmployeeId,
                    IsFinalSaved = 1,
                    IsAttendanceComplete = 1,
                    Name = EmployeeManager.GetEmployeeById(item.EmployeeId.Value).Name,
                    IncomeList = incomeList
                    ,
                    SalaryType = (int)SalaryType.Combined
                    ,
                    IsRetiredOrResigned = 0
                });
            }


            return list;
        }
        public static bool HasAdjustment(int payrollperiodId)
        {
            return
                PayrollDataContext.IncomeDeductionAdjustments.Any(x => x.PayrollPeriodId == payrollperiodId);
        }
        public static bool HasRetrospectIncrement(int payrollperiodId)
        {
            return
                PayrollDataContext.RetrospectIncrements.Any(x => x.PayrollPeriodId == payrollperiodId);

        }
        public static List<CalcGetCalculationListResult> GetCalculationList
            (int companyId, int payrollPeriodId, int? currentPage, int? pageSize, ref int? total,bool isRetiredOrResignedOnly,int? employeeId,
            string empSearchText, int salaryType, int branchId, int departmentId, string statuList,int subDepartmentId
            ,int unitId)
        {

            //first save Contract to date before calculating salary for this Period
            Setting setting = CalculationManager.GetSetting();
            if (setting != null && setting.ContractUptoDay != null)
            {

                PayrollPeriod payroll = CommonManager.GetPayrollPeriod(payrollPeriodId);
                CustomDate payrollCD = CustomDate.GetCustomDateFromString(payroll.StartDate, IsEnglish);
                CustomDate date = new CustomDate(setting.ContractUptoDay.Value, payrollCD.Month, payrollCD.Year, IsEnglish);
                // if value changed then only update
                if (setting.ContractUptoDateEng == null ||setting.ContractUptoDate != date.ToString() || setting.ContractUptoDateEng != date.EnglishDate)
                {
                    setting.ContractUptoDate = date.ToString();
                    setting.ContractUptoDateEng = date.EnglishDate;
                    PayrollDataContext.SubmitChanges();
                }
            }

            if (setting != null && setting.SalaryProcessUptoDay != null && GetCalculation(payrollPeriodId) == null )
            {
                PayrollPeriod payroll = CommonManager.GetPayrollPeriod(payrollPeriodId);
                CustomDate payrollCD = CustomDate.GetCustomDateFromString(payroll.StartDate, IsEnglish);
                CustomDate date = new CustomDate(setting.SalaryProcessUptoDay.Value, payrollCD.Month, payrollCD.Year, IsEnglish);
                PayrollPeriod period = CommonManager.GetPayrollPeriod(payrollPeriodId);
                // if value changed then only update
                //if (setting.SalaryProcessUptoDay == null || setting.ContractUptoDate != date.ToString() || setting.ContractUptoDateEng != date.EnglishDate)
                {
                    period.SalaryProcessUptoDate = date.ToString();
                    period.SalaryProcessUptoDateEng = date.EnglishDate;

                    CustomDate monthEndDate = new CustomDate(DateHelper.GetTotalDaysInTheMonth(payrollCD.Year,payrollCD.Month,IsEnglish)
                        , payrollCD.Month, payrollCD.Year, IsEnglish);
                    DateTime endEnghDate = monthEndDate.EnglishDate;

                    period.SalaryProcessTotalDays = (endEnghDate - period.SalaryProcessUptoDateEng.Value).TotalDays + 1;

                    PayrollDataContext.SubmitChanges();
                }


            }

            //If sal. not started from the start of fiscal year then we need to get the value from entry so
            //do we need to add this value or not, if current fiscal year starting month salary exsits then we don't
            bool readingSumForMiddleFiscalYearStartedReqd = false;
            //contains the id of starting-ending payroll period to be read for history of regular income
            int startingPayrollPeriodId = 0, endingPayrollPeriodId = 0;
          // int notWorkedMonthInFiscalYear = 0, passedMonthWorked = 0;


            bool dumpTax = Config.DumpTaxCalculation && Config.DumpTaxEmployeeId == employeeId;

            PayrollPeriod startingMonthParollPeriodInFiscalYear =
                GenerateForPastIncome(
                    payrollPeriodId, ref readingSumForMiddleFiscalYearStartedReqd,
                    ref startingPayrollPeriodId, ref endingPayrollPeriodId);

            PayrollPeriod currentMonthPayroll = CommonManager.GetPayrollPeriod(payrollPeriodId);

            total = 0;
            //PayrollDataContext.Connection.ConnectionTimeout = PayrollDataContext.Connection.ConnectionTimeout * 2;

           

            List<CalcGetCalculationListResult> result =
                PayrollDataContext.CalcGetCalculationList(companyId, payrollPeriodId, currentPage, pageSize,
                                                          isRetiredOrResignedOnly, employeeId, readingSumForMiddleFiscalYearStartedReqd,
                                                          startingPayrollPeriodId, endingPayrollPeriodId, empSearchText,salaryType,branchId,departmentId,subDepartmentId,statuList
                                                          ,
                                                          LeaveAttendanceManager.GetTotalDaysInFiscalYearForPeriod(payrollPeriodId)
                                                          , LeaveAttendanceManager.GetLeaveYearStartDate(payrollPeriodId)
                                                          ,unitId
                                                          ).ToList();
            if (result.Count > 0)
                total = result[0].TotalRows.Value;

            int empStartingPayrollId, empEndingPayrollId;

         
            return result;
        }



        public static int GetLastPeriodForYear(int yearId)
        {
            return
                PayrollDataContext.PayrollPeriods.Where(x => x.FinancialDateId == yearId)
                .OrderByDescending(x => x.PayrollPeriodId).FirstOrDefault().PayrollPeriodId;
        }

        public class AnnualTaxDetailsBO
        {
            public int Month { get; set; }
            public string Data { get; set; }
            public int PayrollPeriodId { get; set; }

            public decimal TotalRetirementFundContribution { get; set; }
            public decimal DeductionLimit { get; set; }
            public decimal OneThirdofGrossSalary { get; set; }
            public decimal EligibleRetirementFundDeducton { get; set; }
            public decimal EligibleLifeInsuranceDeduction { get; set; }

            public decimal AnnualTaxableAmount { get; set; }
            
            public decimal OnePercent { get; set; }
            public decimal FifteenPercent { get; set; }
            public decimal TwentyFivePercent { get; set; }
            public decimal Surcharge { get; set; }
            public decimal LessFemaleRebate { get; set; }

            public decimal SSTAddonPaid { get; set; }
            public decimal TDSAddonPaid { get; set; }
            public decimal TotalSSTfortheYear { get; set; }
            public decimal TotalTDSfortheYear { get; set; }
            public decimal SSTPaidinPastMonth { get; set; }
            public decimal TDSPaidinPastMonth { get; set; }
            public decimal RemainingSST { get; set; }
            public decimal RemainingTDS { get; set; }

            public decimal SSTthisMonth { get; set; }
            public decimal TDSthisMonth { get; set; }
        }


        public static List<AnnualTaxDetailsBO> GetTaxDetails(int startPeriodId, int periodId,int empId)
        {
            List<AnnualTaxDetailsBO> list = 
                (
                    from e in PayrollDataContext.EmployeeRegularIncomeHistories
                    join p in PayrollDataContext.PayrollPeriods on e.PayrollPeriodId equals p.PayrollPeriodId
                    where e.EmployeeId == empId 
                        &&  
                        (
                            (startPeriodId != 0 && (p.PayrollPeriodId >= startPeriodId && p.PayrollPeriodId <= periodId))
                        || (startPeriodId == 0 && (p.PayrollPeriodId == periodId))
                        )
                    orderby p.PayrollPeriodId
                    select new AnnualTaxDetailsBO
                    {
                        Month = p.Month,
                        Data = e.TaxDetailDescription,
                        PayrollPeriodId = p.PayrollPeriodId
                    }
                ).ToList();

            foreach (var item in list)
            {
                if (item.Data == null)
                    continue;

                // tax data
                string[] rows = item.Data.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);

                foreach (string strRow in rows)
                {
                   string[] values = strRow.Split(new char[] { ':' });

                    if (values.Length <= 1)
                        continue;

                    decimal amount = 0;

                    decimal.TryParse(values[1], out amount);

                    if (values[0].ToLower().Contains("Sum of CIT And PF".ToLower()))
                        item.TotalRetirementFundContribution = amount;
                    else if (values[0].ToLower().Contains("Limit".ToLower()))
                        item.DeductionLimit = amount;
                    else if (values[0].ToLower().Contains("1/3rd of Taxable Income".ToLower()))
                        item.OneThirdofGrossSalary = amount;
                    else if (values[0].ToLower().Contains("Min of a, b or c".ToLower()))
                        item.EligibleRetirementFundDeducton = amount;
                    else if (values[0].ToLower().Contains("Insurance Premium".ToLower()))
                        item.EligibleLifeInsuranceDeduction = amount;
                    else if (values[0].ToLower().Contains("Annual Taxable Amount".ToLower()))
                        item.AnnualTaxableAmount = amount;
                    
                    // tax
                    else if (values[0].ToLower().Contains(TaxItem.OnePercentSST.ToLower()))
                        item.OnePercent = amount;
                    else if (values[0].ToLower().Contains(TaxItem.FifteenPercentTDS.ToLower()))
                        item.FifteenPercent = amount;
                    else if (values[0].ToLower().Contains(TaxItem.TwentyFivePercentTDS.ToLower()))
                        item.TwentyFivePercent = amount;
                    else if (values[0].ToLower().Contains(TaxItem.Surchange.ToLower()))
                        item.Surcharge = amount;
                    else if (values[0].ToLower().Contains(TaxItem.LessFemaleRebate.ToLower()))
                        item.LessFemaleRebate = amount;

                    // tota tax
                    else if (values[0].ToLower().Contains(TaxItem.OnePercentSST.ToLower()))
                        item.OnePercent = amount;
                    else if (values[0].ToLower().Contains(TaxItem.FifteenPercentTDS.ToLower()))
                        item.FifteenPercent = amount;
                    else if (values[0].ToLower().Contains(TaxItem.TwentyFivePercentTDS.ToLower()))
                        item.TwentyFivePercent = amount;
                    else if (values[0].ToLower().Contains(TaxItem.Surchange.ToLower()))
                        item.Surcharge = amount;
                    else if (values[0].ToLower().Contains(TaxItem.LessFemaleRebate.ToLower()))
                        item.LessFemaleRebate = amount;

                    else if (values[0].ToLower().Contains(TaxItem.SSTAddonPaid.ToLower()))
                        item.SSTAddonPaid = amount;
                    else if (values[0].ToLower().Contains(TaxItem.TDSAddonPaid.ToLower()))
                        item.TDSAddonPaid = amount;
                    else if (values[0].ToLower().Contains(TaxItem.TotalSSTfortheYear.ToLower()))
                        item.TotalSSTfortheYear = amount;
                    else if (values[0].ToLower().Contains(TaxItem.TotalTDSfortheYear.ToLower()))
                        item.TotalTDSfortheYear = amount;
                    else if (values[0].ToLower().Contains(TaxItem.SSTPaidinPastMonth.ToLower()))
                        item.SSTPaidinPastMonth = amount;

                    else if (values[0].ToLower().Contains(TaxItem.TDSPaidinPastMonth.ToLower()))
                        item.TDSPaidinPastMonth = amount;
                    else if (values[0].ToLower().Contains(TaxItem.RemainingSST.ToLower()))
                        item.RemainingSST = amount;
                    else if (values[0].ToLower().Contains(TaxItem.RemainingTDS.ToLower()))
                        item.RemainingTDS = amount;
                    else if (values[0].ToLower().Contains(TaxItem.SSTthisMonth.ToLower()))
                        item.SSTthisMonth = amount;
                    else if (values[0].ToLower().Contains(TaxItem.TDSthisMonth.ToLower()))
                        item.TDSthisMonth = amount;
                }
            }

            return list;
        }


        public static string GetSalaryCalcForTaxDetails(int employeeId, int companyId, int payrollPeriodId)
        {



            bool readingSumForMiddleFiscalYearStartedReqd = false;
            //contains the id of starting-ending payroll period to be read for history of regular income
            int startingPayrollPeriodId = 0, endingPayrollPeriodId = 0;
            // int notWorkedMonthInFiscalYear = 0, passedMonthWorked = 0;


            //bool dumpTax = Config.DumpTaxCalculation && Config.DumpTaxEmployeeId == employeeId;
            string list = "";


            PayrollPeriod currentMonthPayroll = CommonManager.GetPayrollPeriod(payrollPeriodId);


            // if saved calculation & has tax details in history
            if (CalculationManager.IsCalculationSavedForEmployee(payrollPeriodId, employeeId)
                && PayrollDataContext.EmployeeRegularIncomeHistories
                .Any(x => x.EmployeeId == employeeId && x.PayrollPeriodId == payrollPeriodId && x.TaxDetailDescription != null && x.TaxDetailDescription != ""))
            {
                list = PayrollDataContext.EmployeeRegularIncomeHistories
                  .SingleOrDefault(x => x.EmployeeId == employeeId && x.PayrollPeriodId == payrollPeriodId && x.TaxDetailDescription != null)
                  .TaxDetailDescription;

                if (!string.IsNullOrEmpty(list))
                    return list;
            }
            else if (CalculationManager.IsCalculationSavedForEmployee(payrollPeriodId, employeeId))
                return "";

            bool isHoldpaymentRelasedThisMonth = false;
            if (PayrollDataContext.HoldPayments.Any(x => x.EmployeeId == employeeId && x.ReleasedPayrollPeriodId == payrollPeriodId))
                isHoldpaymentRelasedThisMonth = true;

            bool isAshadhMonthAndCalculateTaxWithoutCurrentMonthSalary = false;

            // for ashadh allow to bring in the tax details, checked for regular retired employee case also
            if (IsEnglish == false)
            {
                 EHumanResource hr = PayrollDataContext.EHumanResources.FirstOrDefault(x => x.EmployeeId == employeeId);

                if (currentMonthPayroll.Month == 3
                    && 
                    // if final saved then only we will not need to forecast 
                   //CalculationManager.IsAllEmployeeSavedFinally(payrollPeriodId,false) 
                   PayrollDataContext.PayrollPeriods.OrderByDescending(x=>x.EndDateEng).FirstOrDefault().PayrollPeriodId != payrollPeriodId
                   )
                    //(
                    //    // if not valid for current month or already retired in past month
                    //    PayrollDataContext.CalcIsValidEmployeeForCalculation(employeeId, payrollPeriodId) == true)
                    //    ||
                    //    (
                    //       (hr.EEmployee.IsResigned.Value && hr.DateOfResignationEng < currentMonthPayroll.StartDateEng)
                    //        ||
                    //        (hr.EEmployee.IsRetired != null && hr.EEmployee.IsRetired.Value && hr.DateOfRetirementEng < currentMonthPayroll.StartDateEng)                            
                    //    )
                    //)
                    //&& // tax details should not exists


                {
                    isAshadhMonthAndCalculateTaxWithoutCurrentMonthSalary = true;
                }
            }
            //else
            //{
            //    if (currentMonthPayroll.Month == 6)
            //        isAshadhMonthAndCalculateTaxWithoutCurrentMonthSalary = true;
            //}


            if (isAshadhMonthAndCalculateTaxWithoutCurrentMonthSalary == false)
            {
                if (isHoldpaymentRelasedThisMonth == false)
                {
                    // if employee already retired before this month then return empty,
                    // new change show if already retired in past also to make consolidated gross and tax gross equal, so comment below code

                    EHumanResource hr = PayrollDataContext.EHumanResources.FirstOrDefault(x => x.EmployeeId == employeeId);
                    if (hr.EEmployee.IsResigned != null && hr.EEmployee.IsResigned.Value && hr.DateOfResignationEng < currentMonthPayroll.StartDateEng)
                    {
                        return "";
                    }
                    if (hr.EEmployee.IsRetired != null && hr.EEmployee.IsRetired.Value && hr.DateOfRetirementEng < currentMonthPayroll.StartDateEng)
                    {
                        return "";
                    }
                }

                // if not valid for salary calculation like becuase of stop payment then return empty
                // new change show if already retired in past also to make consolidated gross and tax gross equal, so comment below code
                if (PayrollDataContext.CalcIsValidEmployeeForCalculation(employeeId, payrollPeriodId) == false)
                    return "";
            }


            PayrollPeriod lastNotSavedPayrollPeriod = CommonManager.GetValidLastPayrollPeriod();
            // if payroll period not saved month then 
            if (lastNotSavedPayrollPeriod != null && lastNotSavedPayrollPeriod.PayrollPeriodId == currentMonthPayroll.PayrollPeriodId)
            {
                GenerateForPastIncome(
                    payrollPeriodId, ref readingSumForMiddleFiscalYearStartedReqd,
                    ref startingPayrollPeriodId, ref endingPayrollPeriodId);

                int empStartingPayrollId = startingPayrollPeriodId;
                int empEndingPayrollId = endingPayrollPeriodId;

                double totalDaysInFiscalYear = LeaveAttendanceManager.GetTotalDaysInFiscalYearForPeriod(payrollPeriodId);

                PayrollDataContext.CalGetIncomeListSP(
                    employeeId, payrollPeriodId, companyId, readingSumForMiddleFiscalYearStartedReqd, empStartingPayrollId, empEndingPayrollId
                    , true, false, false, ref list, null, totalDaysInFiscalYear, LeaveAttendanceManager.GetLeaveYearStartDate(payrollPeriodId), isAshadhMonthAndCalculateTaxWithoutCurrentMonthSalary);


            }
           //  if not saved then process without saving or change
            //else
            //{
            //    GenerateForPastIncome(
            //        payrollPeriodId, ref readingSumForMiddleFiscalYearStartedReqd,
            //        ref startingPayrollPeriodId, ref endingPayrollPeriodId);

            //    int empStartingPayrollId = startingPayrollPeriodId;
            //    int empEndingPayrollId = endingPayrollPeriodId;

            //    double totalDaysInFiscalYear = LeaveAttendanceManager.GetTotalDaysInFiscalYearForPeriod(payrollPeriodId);

            //    PayrollDataContext.CalGetIncomeListSP(
            //        employeeId, payrollPeriodId, companyId, readingSumForMiddleFiscalYearStartedReqd, empStartingPayrollId, empEndingPayrollId
            //        , true, false, false, ref list, null, totalDaysInFiscalYear, LeaveAttendanceManager.GetLeaveYearStartDate(payrollPeriodId), true);


            //}
           

          

            

            return list;

        }

        public static List<CalculationValue> GetSalaryCalcForRetirement(int employeeId, int companyId)
        {
            List<CalculationValue> listing = new List<CalculationValue>();

            int payrollPeriodId = CommonManager.GetLastPayrollPeriod().PayrollPeriodId;

            bool readingSumForMiddleFiscalYearStartedReqd = false;
            //contains the id of starting-ending payroll period to be read for history of regular income
            int startingPayrollPeriodId = 0, endingPayrollPeriodId = 0;
            // int notWorkedMonthInFiscalYear = 0, passedMonthWorked = 0;


            //bool dumpTax = Config.DumpTaxCalculation && Config.DumpTaxEmployeeId == employeeId;
            string list = "";


            PayrollPeriod currentMonthPayroll = CommonManager.GetPayrollPeriod(payrollPeriodId);


            PayrollPeriod lastNotSavedPayrollPeriod = CommonManager.GetValidLastPayrollPeriod();
            // if payroll period not saved month then 
            if (lastNotSavedPayrollPeriod != null && lastNotSavedPayrollPeriod.PayrollPeriodId == currentMonthPayroll.PayrollPeriodId)
            {
                GenerateForPastIncome(
                    payrollPeriodId, ref readingSumForMiddleFiscalYearStartedReqd,
                    ref startingPayrollPeriodId, ref endingPayrollPeriodId);

                int empStartingPayrollId = startingPayrollPeriodId;
                int empEndingPayrollId = endingPayrollPeriodId;

                int? total = 0;
                List<CalcGetCalculationListResult> calculationList =
                    GetCalculationList(SessionManager.CurrentCompanyId, payrollPeriodId, 0, int.MaxValue, ref total, true, employeeId, "",
                    (int)SalaryType.Combined, -1, -1,"",-1,-1);

                if (calculationList.Count > 0)
                {
                    list = calculationList[0].IncomeList;
                }

                //PayrollDataContext.CalGetIncomeListSP(
                //    employeeId, payrollPeriodId, companyId, readingSumForMiddleFiscalYearStartedReqd, empStartingPayrollId, empEndingPayrollId
                //    , false, false, false, ref list);

                //if (list == null)
                //    list = "";

                //list += ";" + PayrollDataContext.CalGetDeductionList(employeeId, payrollPeriodId, false, false, 0, 0);

                if (!string.IsNullOrEmpty(list))
                {

                    string[] rows = list.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);

                    foreach (string strRow in rows)
                    {
                        string[] values = strRow.Split(new char[] { ':' });

                        if (values[1] == "")
                            continue;

                        CalculationValue value = null;
                        if (values[2] == "-")
                        {
                            value = new CalculationValue()
                            {
                                Type = int.Parse(values[0]),
                                SourceId = int.Parse(values[1]),
                                Amount = 0
                            };
                        }
                        else
                        {
                            value = new CalculationValue()
                            {
                                Type = int.Parse(values[0]),
                                SourceId = int.Parse(values[1]),
                                Amount = decimal.Parse(values[2])
                            };
                        }


                        
                        if (values.Length <= 1)
                            continue;


                        listing.Add(value);
                    }
                }
            }

            return listing;
        }

        public static void GetRetirementCalculation(PayrollPeriod payrollPeriod, int employeeId,bool IsPastRetired, out List<CalculationValue> incomeList, out List<CalculationValue> deductionList, out List<CalculationValue> otherincomeList,
            out List<CalculationValue> otherdeductionList)
        {
            // 2. Get Calculation
            List<CalculationValue> list = new List<CalculationValue>();

            if (IsPastRetired == false)
            {
                list = CalculationManager.GetSalaryCalcForRetirement(employeeId, SessionManager.CurrentCompanyId)
                    // skip deemed Income
                    .Where(x => x.Type != (int)CalculationColumnType.DeemedIncome).ToList();
            }
            else
            {
                List<GetRetiredSettlementListDetailsResult> details = BLL.BaseBiz.PayrollDataContext.GetRetiredSettlementListDetails(
                    employeeId.ToString()).ToList();

                foreach (GetRetiredSettlementListDetailsResult item in details.Where(x => x.IsRetirement == 0))
                {
                    CalculationValue value = new CalculationValue
                    {
                        Type = item.Type == null ? 0 : item.Type.Value,
                        SourceId = item.SourceId == null ? 0 : item.SourceId.Value,
                        Amount = item.Amount == null ? 0 : item.Amount.Value
                    };
                    list.Add(value);
                }

            }

            // remove all except non incomes & deduction
            for (int i = list.Count - 1; i >= 0; i--)
            {

                if (CalculationValue.IsColumTypeDeduction(list[i].ColumnType) == false
                    && CalculationValue.IsColumTypeIncome(list[i].ColumnType) == false
                    )
                    list.RemoveAt(i);
            }


            List<DAL.IncomeDeductionAdjustment> adjustmentList = BLL.BaseBiz.PayrollDataContext.IncomeDeductionAdjustments.Where(
               x => x.EmployeeId == employeeId && x.PayrollPeriodId == payrollPeriod.PayrollPeriodId).ToList();
            List<EmployeeIncomeForLeaveEncasementCalc> defaultIncomes = BLL.BaseBiz.PayrollDataContext.EmployeeIncomeForLeaveEncasementCalcs
                .Where(x => x.EmployeeId == employeeId && x.PayrollPeriodId == payrollPeriod.PayrollPeriodId).ToList();


            MyCache.DeleteFromGlobalCache(CacheKey.PayrollPeriodPayHeaderList.ToString() + SessionManager.CurrentCompanyId + payrollPeriod.PayrollPeriodId);
            List<CalcGetHeaderListResult> headerList = CalculationManager.GetIncomeHeaderList(SessionManager.CurrentCompanyId, payrollPeriod.PayrollPeriodId)
                .Where(x => x.Type != (int)CalculationColumnType.DeemedIncome).ToList();


            incomeList = new List<CalculationValue>();
            deductionList = new List<CalculationValue>();

            foreach (CalculationValue item in list)
            {
                CalcGetHeaderListResult header = headerList.FirstOrDefault(x => x.Type == item.Type && x.SourceId == item.SourceId);
                if (header != null)
                {
                    item.HeaderName = header.HeaderName;
                    if (CalculationValue.IsColumTypeIncome(item.ColumnType))
                    {
                        CalculationColumnType calculationType = (CalculationColumnType)item.Type;



                        if (adjustmentList.Any(x => x.Type == item.Type && x.IncomeDeductionId == item.SourceId))
                            item.AmountWithOutAdjustment = Convert.ToDecimal(adjustmentList.FirstOrDefault(x => x.Type == item.Type && x.IncomeDeductionId == item.SourceId)
                                .ExistingAmount);

                        //for incomes comes from matrix withtou direct associate like for Defined income
                        if (item.Amount != 0 && item.AmountWithOutAdjustment == 0)
                            item.AmountWithOutAdjustment = item.Amount;


                        if (item.Type == 1 && defaultIncomes.Any(x => x.IncomeId == item.SourceId))
                            item.FullAmount = Convert.ToDecimal(defaultIncomes.FirstOrDefault(x => x.IncomeId == item.SourceId).Amount);



                        incomeList.Add(item);
                    }
                    else
                    {

                        //for incomes comes from matrix withtou direct associate like for Defined income
                        if (item.Amount != 0 && item.AmountWithOutAdjustment == 0)
                            item.AmountWithOutAdjustment = item.Amount;

                        if (adjustmentList.Any(x => x.Type == item.Type && x.IncomeDeductionId == item.SourceId))
                        {
                            item.AmountWithOutAdjustment = Convert.ToDecimal(adjustmentList.FirstOrDefault(x => x.Type == item.Type && x.IncomeDeductionId == item.SourceId)
                                    .ExistingAmount);
                        }

                        if (item.SourceId + ":" + item.Type == ((int)CalculationColumnType.DeductionCIT + ":" + (int)CalculationColumnType.DeductionCIT))
                        {
                            if (BLL.BaseBiz.PayrollDataContext.HasCIT(employeeId, payrollPeriod.PayrollPeriodId).Value)
                                item.HasCIT = true;
                            else
                                item.HasCIT = false;
                        }
                        deductionList.Add(item);
                    }
                }


            }

            otherincomeList = new List<CalculationValue>();
            otherdeductionList = new List<CalculationValue>();
            PayManager.SetOtherRetirementIncomesAndDeductions(employeeId, payrollPeriod, ref otherincomeList, ref otherdeductionList);
        }

        public static void GetRetireRehireCalculation(PayrollPeriod payrollPeriod, int employeeId, out List<CalculationValue> incomeList, out List<CalculationValue> deductionList, out List<CalculationValue> otherincomeList, out List<CalculationValue> otherdeductionList)
        {
            // 2. Get Calculation
            incomeList = new List<CalculationValue>();
            deductionList = new List<CalculationValue>();
            otherincomeList = new List<CalculationValue>();
            otherdeductionList = new List<CalculationValue>();

            List<RetireReHireDetail> list = PayrollDataContext.RetireReHireDetails
                .Where(x => x.EmployeeId == employeeId && x.PayrollPeriodId == payrollPeriod.PayrollPeriodId).ToList();


            // Income portions
            foreach (var item in list.Where(x=>x.RetirementPortionType == (int)RetirementPortionTypeEnum.Income))
            {
                CalculationValue detail = new CalculationValue();
                incomeList.Add(detail);
                detail.Type = item.Type;
                detail.SourceId = item.SourceId;
                detail.HeaderName = item.HeaderName;
                detail.FullAmount = item.FullAmount;
                detail.AmountWithOutAdjustment = item.AmountWithOutAdjustment;
                detail.Amount = item.Amount;
            }

            // Deductions portions
            foreach (var item in list.Where(x => x.RetirementPortionType == (int)RetirementPortionTypeEnum.Deduction))
            {
                CalculationValue detail = new CalculationValue();
                deductionList.Add(detail);
               
                detail.Type = item.Type;
                detail.SourceId = item.SourceId;
                detail.HeaderName = item.HeaderName;

                detail.FullAmount = item.AmountWithOutAdjustment;
                detail.AmountWithOutAdjustment = item.AmountWithOutAdjustment;
                detail.Amount = item.Amount;
            }

            // Other Income portions
            foreach (var item in list.Where(x => x.RetirementPortionType == (int)RetirementPortionTypeEnum.OtherIncome))
            {
                CalculationValue detail = new CalculationValue();
                otherincomeList.Add(detail);
               
                detail.Type = item.Type;
                detail.SourceId = item.SourceId;
                detail.HeaderName = item.HeaderName;

                detail.FullAmount = item.FullAmount;
                detail.AdjustedAmount = item.AmountWithOutAdjustment;
                detail.TDS = item.TDS;
                detail.TDSAdjustment = item.TDSAdjustment;
                detail.NetPayable = item.Amount;
            }

            // Other Deductions portions
            foreach (var item in list.Where(x => x.RetirementPortionType == (int)RetirementPortionTypeEnum.OtherDeduction))
            {
                CalculationValue detail = new CalculationValue();
                otherdeductionList.Add(detail);
               
                detail.Type = item.Type;
                detail.SourceId = item.SourceId;
                detail.HeaderName = item.HeaderName;

                detail.FullAmount = item.AmountWithOutAdjustment;
                detail.AmountWithOutAdjustment = item.AmountWithOutAdjustment;
                detail.Amount = item.Amount;
                detail.ExcludeFromRetirement = item.Excluded;
            }
           
        }
        public static List<CalcGetNewCalculationListResult> GetNewCalculationList
           (int companyId, int payrollPeriodId, int? currentPage, int? pageSize, ref int? total, bool isRetiredOrResignedOnly, int? employeeId)
        {

            //List<CalcGetNewCalculationListResult> list =  GetFromCache<List<CalcGetNewCalculationListResult>>(payrollPeriodId + ":" + currentPage + ":" + pageSize);
            //if (list != null)
            //{
            //    if (list.Count > 0)
            //        total = list[0].TotalRows.Value;
            //    return list;
            //}

            //If sal. not started from the start of fiscal year then we need to get the value from entry so
            //do we need to add this value or not, if current fiscal year starting month salary exsits then we don't
            bool readingSumForMiddleFiscalYearStartedReqd = false;
            //contains the id of starting-ending payroll period to be read for history of regular income
            int startingPayrollPeriodId = 0, endingPayrollPeriodId = 0;
            // int notWorkedMonthInFiscalYear = 0, passedMonthWorked = 0;


            bool dumpTax = Config.DumpTaxCalculation && Config.DumpTaxEmployeeId == employeeId;

            PayrollPeriod startingMonthParollPeriodInFiscalYear =
                GenerateForPastIncome(
                    payrollPeriodId, ref readingSumForMiddleFiscalYearStartedReqd,
                    ref startingPayrollPeriodId, ref endingPayrollPeriodId);

            PayrollPeriod currentMonthPayroll = CommonManager.GetPayrollPeriod(payrollPeriodId);

            total = 0;
            List<CalcGetNewCalculationListResult> result =
                PayrollDataContext.CalcGetNewCalculationList(companyId, payrollPeriodId, currentPage, pageSize,
                                                          isRetiredOrResignedOnly, employeeId
                                                          ,readingSumForMiddleFiscalYearStartedReqd,startingPayrollPeriodId
                                                          ,endingPayrollPeriodId).ToList();

          

            if (result.Count > 0)
                total = result[0].TotalRows.Value;

            int empStartingPayrollId, empEndingPayrollId;

            //foreach (var listResult in result)
            {
                //empStartingPayrollId = startingPayrollPeriodId;
                //empEndingPayrollId = endingPayrollPeriodId;

                //Change starting payroll period id due to Stop Payment
                //FilterPayrollPeriodForStopPayment(listResult.EmployeeId.Value, ref empStartingPayrollId, ref empEndingPayrollId,
                //    payrollPeriodId);

                //if from calculation then attach income list
                //if (listResult.IsFromCalcTable == 0)
                {
                    //string incomeOtherPart = GetIncomeList(listResult.EmployeeId.Value, payrollPeriodId, companyId,
                    //    readingSumForMiddleFiscalYearStartedReqd, empStartingPayrollId, empEndingPayrollId, dumpTax);

                    //listResult.IncomeList += incomeOtherPart;

                    //if (string.IsNullOrEmpty(incomeOtherPart))
                    //{
                    //    Log.log(string.Format("Salary could not be generated for emp:{0},payroll:{1}", listResult.EmployeeId, payrollPeriodId), "");
                    //    listResult.IsSalaryGenerationUnsuccessfull = true;
                    //}

                }
              
            }


            //GetFromCache<List<CalcGetNewCalculationListResult>>(payrollPeriodId + ":" + currentPage + ":" + pageSize);
            //SaveToCache<List<CalcGetNewCalculationListResult>>(payrollPeriodId + ":" + currentPage + ":" + pageSize, result);

            return result;
        }

        

        /// <summary>
        /// Retrieves passed payroll period id starting to ending to for past amount calculation &
        /// readingFromPast table required or not if payroll from starting month of fiscal year doesn't exists
        /// </summary>
        public static PayrollPeriod GenerateForPastIncome(int payrollPeriodId, ref bool readingSumForMiddleFiscalYearStartedReqd
            , ref int startingPayrollPeriodId, ref int endingPayrollPeriodId)
        {

            int payrollReqdMonth;
            PayrollPeriod current = CommonManager.GetPayrollPeriod(payrollPeriodId);
            PayrollPeriod staringPayrollInFiscalYear = null;
            List<PayrollPeriod> prevHistoryPeriods;
            CalculationConstant constant = PayrollDataContext.CalculationConstants.FirstOrDefault();

            int yearCount = PayrollDataContext.FinancialDates.Count();

            if (SessionManager.CurrentCompany.IsEnglishDate)
                payrollReqdMonth = constant.FiscalYearStartingMonthForEng.Value;
            else
            {
                payrollReqdMonth = constant.FiscalYearStartingMonthForNep.Value;
            }
           
            int month = 0,year =0;

            //if current month is not the starting of fiscal year, if yes don't need to do anything as passed will be 0
            if (current.Month != payrollReqdMonth)
            {
                //if current month > fiscal starting month, then fiscal month year lies in the same month
                
                prevHistoryPeriods = GetPayrollPeriodListForCurrentFiscalYear(payrollPeriodId, ref staringPayrollInFiscalYear
                    ,ref month,ref year);


                

                //if starting month of fiscal year doesn't exists, then we need to get the info from "EmployeeRegularIncomeFromPast" table,
                //so readingSumForMiddleFiscalYearStartedReqd = true
                // or for first year also always try to pick History
                if (staringPayrollInFiscalYear == null || yearCount == 1)
                {
                    readingSumForMiddleFiscalYearStartedReqd = true;

                }



                if (prevHistoryPeriods.Count > 0)
                {
                    startingPayrollPeriodId = prevHistoryPeriods[0].PayrollPeriodId;
                    endingPayrollPeriodId = prevHistoryPeriods[prevHistoryPeriods.Count - 1].PayrollPeriodId;
                }

                return staringPayrollInFiscalYear;
            }


            return null;
        }

        #region "New Salary Generate/Save Methods"

        public static int CountRetiring(DateTime fromdate, DateTime to)
        {
            return 
                (
                    from e in PayrollDataContext.EEmployees
                    join h in PayrollDataContext.EHumanResources on e.EmployeeId equals h.EmployeeId
                    where 
                    (
                        (e.IsRetired == true && h.DateOfRetirementEng >= fromdate && h.DateOfRetirementEng <= to)
                        ||
                        (e.IsResigned == true && h.DateOfResignationEng >= fromdate && h.DateOfResignationEng <= to)
                    ) && h.SalaryCalculateFromEng <= to
                    select h
                ).Count();
        }
        public static int CountRetiring(int calculationId)
        {
            return
                PayrollDataContext.CCalculationEmployees.Count(x=>x.IsRetiredOrResigned==true && x.CalculationId==calculationId);
        }
        public static string GetHeaderName(int payrollPeroidId,int type,int sourceId)
        {
            List<CalcGetHeaderListResult> headerList = CalculationManager.GetIncomeHeaderList(SessionManager.CurrentCompanyId, payrollPeroidId);

            CalcGetHeaderListResult header = headerList.FirstOrDefault(x => x.Type == type && x.SourceId == sourceId);
            if (header != null)
                return header.HeaderName;

            CalculationColumnType typeValue = (CalculationColumnType)type;

            switch (typeValue)
            {
                case CalculationColumnType.Income:
                case CalculationColumnType.DeemedIncome:
                    return new PayManager().GetIncomeById(sourceId).Title;
                case CalculationColumnType.Deduction:
                    return new PayManager().GetDeductionById(sourceId).Title;
                case CalculationColumnType.IncomeLeaveEncasement:
                    return "Leave Encashment";
                case CalculationColumnType.SST:
                    return "SST";
                case CalculationColumnType.TDS:
                    return "TDS";
                case CalculationColumnType.DeductionCIT:
                    return "CIT";
                case CalculationColumnType.IncomePF:
                    return "Income PF";
                case CalculationColumnType.DeductionPF:
                    return "Deduction PF";
                
            }


            return "";
        }

        public static bool GenerateSalary(int payrollPeroidId, int branchId, int department, bool isRetResOnly)
        {
            int? total=0;
            List<CalcGetNewCalculationListResult> empList
             = GetNewCalculationList(SessionManager.CurrentCompanyId, payrollPeroidId,null,null, ref total, isRetResOnly, null);
            
            List<CalcGetHeaderListResult> headerList = CalculationManager.GetIncomeHeaderList(SessionManager.CurrentCompanyId, payrollPeroidId);
             
            Dictionary<string, string> headerDictionary = new Dictionary<string, string>();
             foreach (CalcGetHeaderListResult header1 in headerList)
            {
                string key = header1.Type + ":" + header1.SourceId;
                if (!headerDictionary.ContainsKey(key) )
                    headerDictionary.Add(key, "");
            }

           
            bool hasAdjIncome = false;
            bool hasAdjDeduction = false;
            string adjIncome = "";
            string adjDeduction = "";

            bool hasPF = SessionManager.CurrentCompany.HasPFRFFund;
            double empContribution = 0;
            double companyContribution = 0;
            string pfRFNo = string.Empty;

            if (hasPF)
            {
                empContribution = SessionManager.CurrentCompany.PFRFFunds[0].EmployeeContribution.Value;
                companyContribution = SessionManager.CurrentCompany.PFRFFunds[0].CompanyContribution.Value;
                pfRFNo = SessionManager.CurrentCompany.PFRFFunds[0].PFRFNo;
            }

            StringBuilder xmlHeaders = new StringBuilder();
            StringBuilder xmlEmployees = new StringBuilder();
            StringBuilder xmlCells = new StringBuilder();
            StringBuilder xmlIncludeds = new StringBuilder();
            //StringBuilder xmlCalculationLoanAdvance  = new 

            xmlHeaders.Append("<root>");
            //add income headers
            //TableRow rowHeader = (System.Web.UI.WebControls.TableRow)gvw.HeaderRow;
            CalcGetHeaderListResult header;
            for (int j = 0; j < headerList.Count ; j++)
            {

                //if (rowHeader.Cells[j].Controls.Count > 0)
                {
                    //Label lblHeader = (Label)rowHeader.Cells[j].Controls[0];
                    header = headerList[j];
                    //TextBox txtAdjustment = null;

                    CalculationColumnType columnType =
                        (CalculationColumnType)header.Type;

                   
                    //TODO: rem for adjustment
                    //if (columnType == CalculationColumnType.IncomeAdjustment)
                    //{
                    //    CheckBox chk = (CheckBox)rowHeader.Cells[j].Controls[2];
                    //    hasAdjIncome = chk.Checked;
                    //    //Don add adjustment if not selected
                    //    if (!chk.Checked)
                    //        continue;
                    //    txtAdjustment = (TextBox)rowHeader.FindControl("txtIncomeAdj");
                    //}

                    //else if (columnType == CalculationColumnType.DeductionAdjustment)
                    //{
                    //    CheckBox chk = (CheckBox)rowHeader.Cells[j].Controls[2];
                    //    hasAdjDeduction = chk.Checked;
                    //    //Don add adjustment if not selected
                    //    if (!chk.Checked)
                    //        continue;
                    //    txtAdjustment = (TextBox)rowHeader.FindControl("txtDeductionAdj");
                    //}
                    //else if (columnType == CalculationColumnType.IncomeResterospect)
                    //{
                    //    txtAdjustment = (TextBox)rowHeader.FindControl("txtRetrospectIncrement");
                    //}

                    string isIncome = "";
                    if (CalculationValue.IsColumTypeIncome(columnType))
                        isIncome = "1";
                    else if (CalculationValue.IsColumTypeDeduction(columnType))
                        isIncome = "0";

                    xmlHeaders.AppendFormat("<row SourceId='{0}' Type='{1}' HeaderName='{2}' IsIncome='{3}' />",
                                            header.SourceId, header.Type,
                                            header.HeaderName,
                                            //(txtAdjustment == null)
                                            //    ? lblHeader.Attributes["HeaderName"]
                                            //    : (Request.Form[txtAdjustment.UniqueID]),
                                                isIncome);
                }
            }
            xmlHeaders.Append("</root>");

            bool employeeSaved = false;

            xmlEmployees.Append("<root>");
            //add cell values
            xmlCells.Append("<root>");
            xmlIncludeds.Append("<root>");
            CalcGetNewCalculationListResult emp;
            for (int i = 0; i < empList.Count; i++)
            {
                emp = empList[i];
                //GridViewRow gridViewRow = gvw.Rows[i];
                //if (gridViewRow.FindControl("lblId") == null)
                //    continue;

                int empId = emp.EmployeeId.Value;//(int)gvw.DataKeys[gridViewRow.RowIndex]["EmployeeId"];  //int.Parse(((Label)gridViewRow.FindControl("lblId")).Text);
                int calculationId = emp.CalculationId.Value;// //(int)gvw.DataKeys[gridViewRow.RowIndex]["CalculationId"];//int.Parse(((HiddenField)gridViewRow.FindControl("txtCalulationId")).Value);
                //HiddenField adjustmentIncome = gridViewRow.FindControl("adjIncome") as HiddenField;
                //HiddenField adjustmentDeduction = gridViewRow.FindControl("adjDeduction") as HiddenField;

                string isRetiredOrResigned = emp.IsRetiredOrResigned.ToString();  //gvw.DataKeys[gridViewRow.RowIndex]["IsRetiredOrResigned"].ToString();
                string isFinalSaved = emp.IsFinalSaved.ToString(); //gvw.DataKeys[gridViewRow.RowIndex]["IsFinalSaved"].ToString();

                //HiddenField hfRetRegState = (HiddenField)gridViewRow.FindControl("hfRetRegState");
                //HiddenField hfFinalSaved = (HiddenField)gridViewRow.FindControl("hfFinalSaved");

                //If final saved then don't save again
                if (isFinalSaved == "1")
                    continue;

                string adjIncomeComment = string.Empty, adjDeductionComment = string.Empty;

                //if (hasAdjIncome && adjustmentIncome != null && !string.IsNullOrEmpty(adjustmentIncome.Value))
                //{
                //    adjIncomeComment = adjustmentIncome.Value;
                //}
                //if (hasAdjDeduction && adjustmentDeduction != null && !string.IsNullOrEmpty(adjustmentDeduction.Value))
                //{
                //    adjDeductionComment = adjustmentDeduction.Value;
                //}

                //if (calculationId == 0 || editMode)
                {
                    employeeSaved = true;

                    // append Employee info
                    xmlEmployees.AppendFormat(
                        "<row EmployeeId='{0}' AdjustmentIncome='{1}' AdjustmentDeduction='{2}' IsRetiredOrResigned='{3}' />", empId,
                        adjIncomeComment, adjDeductionComment, isRetiredOrResigned
                        );

                    bool isRetrospectNegative = false;

                    for (int j = 0; j < headerList.Count ; j++)
                    {
                        CalcGetHeaderListResult empHeader = headerList[j];

                        //TextBox txt = (TextBox)gridViewRow.Cells[j].Controls[0];
                        //Label lblHeader = (Label)rowHeader.Cells[j].Controls[0];

                        //if (!txt.Text.Equals(System.Resources.Messages.EmptySalaryAmountText))
                        {
                            //if true then when viewed input doesn't existed later from another page it was associated so don't save
                            //if (Request.Form[txt.UniqueID] == null)
                            //{
                            //    continue;
                            //}

                            decimal? amount = emp.GetCellValue(empHeader.Type.Value, empHeader.SourceId.Value,
                                SessionManager.DecimalPlaces,headerDictionary); //decimal.Parse(Request.Form[txt.UniqueID].ToString());

                            if (amount == null)
                                continue;
                            
                            //If adjustment has not been selected then don't add in the list
                            CalculationColumnType columnType =
                                (CalculationColumnType)empHeader.Type;

                            

                           

                            //Check & append CalculationIncluded information like Special Events id, Income Increments id
                            if (!string.IsNullOrEmpty( emp.GetList(empHeader.Type.Value, empHeader.SourceId.Value) )) //(txt.Attributes["CalculationIncluded"] != null)
                            {
                                //Can contain multiple if Special event type
                                string[] inclduedList = emp.GetList(empHeader.Type.Value, empHeader.SourceId.Value).Split(new char[] { ',' });
                                if (inclduedList.Length > 0)
                                {
                                    foreach (var s in inclduedList)
                                    {
                                        if (!string.IsNullOrEmpty(s))
                                        {
                                            string[] typeSource = s.Split(new char[] { '=' });
                                            xmlIncludeds.AppendFormat(
                                                "<row EmployeeId='{0}' Type='{1}' SourceId='{2}' />",
                                                empId, typeSource[0], typeSource[1]
                                                );
                                        }
                                    }
                                }
                            }


                            //if (columnType == CalculationColumnType.IncomeAdjustment && !hasAdjIncome)
                            //    continue;
                            //if (columnType == CalculationColumnType.DeductionAdjustment && !hasAdjDeduction)
                            //    continue;




                            xmlCells.AppendFormat("<row SourceId='{0}' Type='{1}' EmployeeId='{2}' Amount='{3}' />",
                                                  empHeader.SourceId, empHeader.Type, empId,
                                //txt.Text
                                                  amount
                                );
                        }
                    }

                }
            }
            xmlCells.Append("</root>");
            xmlEmployees.Append("</root>");
            xmlIncludeds.Append("</root>");





            if (employeeSaved)
            {
                new CalculationManager().SaveUpdateCalculation(payrollPeroidId, hasAdjIncome, adjIncome, hasAdjDeduction, adjDeduction,
                                              hasPF, pfRFNo, empContribution, companyContribution,
                                              xmlHeaders.ToString(), xmlEmployees.ToString(), xmlCells.ToString(),
                                              xmlIncludeds.ToString());



            }
            return employeeSaved;

        }

        #endregion


        private static List<PayrollPeriod> GetPayrollPeriodListForCurrentFiscalYear(int payrollPeriodId, ref PayrollPeriod staringPayrollInFiscalYear
            ,ref int fiscalStartMonth,ref int fiscalStartYear)
        {
            //int payrollReqdYear = 0, startingMonthForFiscalYear;
            PayrollPeriod current = CommonManager.GetPayrollPeriod(payrollPeriodId);

            CalculationConstant constant = PayrollDataContext.CalculationConstants.FirstOrDefault();
            if (SessionManager.CurrentCompany.IsEnglishDate)
                fiscalStartMonth = constant.FiscalYearStartingMonthForEng.Value;
            else
            {
                fiscalStartMonth = constant.FiscalYearStartingMonthForNep.Value;
            }

            List<PayrollPeriod> prevHistoryPeriods = new List<PayrollPeriod>();

            if (current.Month > fiscalStartMonth)
            {
                fiscalStartYear =
                    CustomDate.GetCustomDateFromString(current.StartDate,
                                                       SessionManager.CurrentCompany.IsEnglishDate).
                        Year;


            }
            //if current month < fiscal starting month, the fiscal month year lies in the prev. year
            else if (current.Month < fiscalStartMonth)
            {
                fiscalStartYear =
                    CustomDate.GetCustomDateFromString(current.StartDate,
                                                       SessionManager.CurrentCompany.IsEnglishDate).
                        Year - 1;
            }
            else if (current.Month == fiscalStartMonth)
                fiscalStartYear = current.Year.Value;

            int month = fiscalStartMonth, year = fiscalStartYear;

            //Try to get the PayrollPeriod for the starting month of current fiscal year
            staringPayrollInFiscalYear =
                PayrollDataContext.PayrollPeriods.Where(p => p.Month == month
                                                             && p.Year == year
                                                             && p.CompanyId == SessionManager.CurrentCompanyId).FirstOrDefault();

            //get the payroll periods that lies in the current fiscal year for retriving amount from History table
            //for tax calculation
            prevHistoryPeriods = PayrollDataContext.PayrollPeriods
                .Where(

                    p => (
                             //if year matches then month >= starting month of fiscal year
                         (p.Year == year && p.Month >= month)
                         ||
                         // If next year, then mon < starting month of fiscal year       
                         (p.Year > year && p.Month < month)
                         )
                         && p.PayrollPeriodId != payrollPeriodId
                         // Previous payroll period only as for nset future attendance filling future payroll as enabled
                         && p.PayrollPeriodId < payrollPeriodId 
                         && p.CompanyId == SessionManager.CurrentCompanyId
                )
                .OrderBy(p => p.PayrollPeriodId)
                .ToList();

            return prevHistoryPeriods;
        }

        public static void ClearHeaderCache(int companyId, int payrollPeriodId)
        {
            DeleteFromCache(CacheKey.PayrollPeriodPayHeaderList.ToString() + companyId + payrollPeriodId);
        }

        /// <summary>
        /// Retrive the list of Incomes of the company to be displayed in Calculation list
        /// </summary>
        public static List<CalcGetHeaderListResult> GetIncomeHeaderList(int companyId, int payrollPeriodId)
        {
            //retrieve from cache only when salary has already being saved as for other cases header can be changed
            // & will be difficult to track
            List<CalcGetHeaderListResult> list = null;

            if (CalculationManager.IsCalculationSaved(payrollPeriodId) != null)
            {

                list =
                    GetFromCache<List<CalcGetHeaderListResult>>(CacheKey.PayrollPeriodPayHeaderList.ToString() + companyId + payrollPeriodId);

                if (list != null)
                    return list;
            }


            list =  PayrollDataContext.CalcGetHeaderList(payrollPeriodId, companyId)
                .ToList();

            SaveToCache<List<CalcGetHeaderListResult>>(CacheKey.PayrollPeriodPayHeaderList.ToString() + companyId + payrollPeriodId, list);

            return list;
        }

        public static List<CalcGetHeaderListResult> GetHeaderListForVoucher()
        {
            

            List<CalcGetHeaderListResult> list = new List<CalcGetHeaderListResult>();

            list.AddRange(

                (
                  from i in PayrollDataContext.PIncomes
                  where //i.IsEnabled == true &&
                  i.Calculation!= IncomeCalculation.DEEMED_INCOME

                  select new CalcGetHeaderListResult
                  {
                      Type = 1,
                      SourceId = i.IncomeId,
                      HeaderName = i.Title,
                      CalculationText = HttpContext.GetGlobalResourceObject("FixedValues", i.Calculation).ToString()
                  }
                )

                );

            //if (CommonManager.CompanySetting.WhichCompany == WhichCompany.HPL)
            //{

            //    //Head work
            //    list.Add(new CalcGetHeaderListResult
            //    {
            //        ColumnType = CalculationColumnType.IncomeSiteAllowance,
            //        HeaderName = "Headworks Allowance",
            //        Order = 2,
            //        Type = (int)CalculationColumnType.IncomeSiteAllowance,
            //        SourceId = (int)CalculationColumnType.IncomeSiteAllowance
            //    });
            //    // On call
            //    list.Add(new CalcGetHeaderListResult
            //    {
            //        ColumnType = CalculationColumnType.IncomeOnCallAllowance,
            //        HeaderName = "OnCall Allowance",
            //        Order = 2,
            //        Type = (int)CalculationColumnType.IncomeOnCallAllowance,
            //        SourceId = (int)CalculationColumnType.IncomeOnCallAllowance
            //    });
            //    // shift
            //    list.Add(new CalcGetHeaderListResult
            //    {
            //        ColumnType = CalculationColumnType.IncomeShiftAfternoon,
            //        HeaderName = "Shift Allowance",
            //        Order = 2,
            //        Type = (int)CalculationColumnType.IncomeShiftAfternoon,
            //        SourceId = (int)CalculationColumnType.IncomeShiftAfternoon
            //    });
            //    // Overtime
            //    list.Add(new CalcGetHeaderListResult
            //    {
            //        ColumnType = CalculationColumnType.IncomeOvertime150,
            //        HeaderName = "Overtime Allowance",
            //        Order = 2,
            //        Type = (int)CalculationColumnType.IncomeOvertime150,
            //        SourceId = (int)CalculationColumnType.IncomeOvertime150
            //    });
            //}

            List<PDeduction> deductions = PayrollDataContext.PDeductions.Where(x => x.IsEnabled == true).ToList();
            foreach (var item in deductions)
            {
                CalcGetHeaderListResult header = new CalcGetHeaderListResult();
                header.Type = 8;
                header.SourceId = item.DeductionId;
                header.HeaderName = item.Title;
                header.CalculationText = HttpContext.GetGlobalResourceObject("FixedValues", item.Calculation) == null ? item.Calculation :
                  HttpContext.GetGlobalResourceObject("FixedValues", item.Calculation).ToString();

                list.Add(header);
            }

            //list.AddRange(
            //    (
            //      from i in PayrollDataContext.PDeductions
            //      where i.IsEnabled == true
            //      select new CalcGetHeaderListResult
            //      {
            //          Type = 8,
            //          SourceId = i.DeductionId,
            //          HeaderName = i.Title,
            //          CalculationText = HttpContext.GetGlobalResourceObject("FixedValues", i.Calculation).ToString()
            //      }
            //    )
            //    );

            // HPL like Loan Interest
            list.AddRange(
                (
                  from i in PayrollDataContext.PDeductions
                  where i.IsEnabled == true && i.Calculation=="LoanInstallment"
                  select new CalcGetHeaderListResult
                  {
                      Type = 18,
                      SourceId = i.DeductionId,
                      HeaderName = i.Title + " Interest",
                      CalculationText = HttpContext.GetGlobalResourceObject("FixedValues", i.Calculation).ToString()
                  }
                )
                );



            list.Add(new CalcGetHeaderListResult
            {
                ColumnType = CalculationColumnType.IncomeLeaveEncasement,
                HeaderName = "Leave Encashment",
                Order = 2,
                Type = (int)CalculationColumnType.IncomeLeaveEncasement,
                SourceId = (int)CalculationColumnType.IncomeLeaveEncasement
            });

            list.Add(new CalcGetHeaderListResult
            {
                ColumnType = CalculationColumnType.IncomePF,
                HeaderName = "Income PF",
                Order = 2,
                Type = (int)CalculationColumnType.IncomePF,
                SourceId = (int)CalculationColumnType.IncomePF
            });



            list.Add(new CalcGetHeaderListResult
            {
                ColumnType = CalculationColumnType.DeductionCIT,
                HeaderName = "CIT",
                Order = 2,
                Type = (int)CalculationColumnType.DeductionCIT,
                SourceId = (int)CalculationColumnType.DeductionCIT
            });

            //if (forImport == false)
            {
                list.Add(new CalcGetHeaderListResult
                {
                    ColumnType = CalculationColumnType.DeductionPF,
                    HeaderName = "Deduction PF",
                    Order = 2,
                    Type = (int)CalculationColumnType.DeductionPF,
                    SourceId = (int)CalculationColumnType.DeductionPF
                });
            }
            list.Add(new CalcGetHeaderListResult
            {
                ColumnType = CalculationColumnType.SST,
                HeaderName = "SST",
                Order = 2,
                Type = (int)CalculationColumnType.SST,
                SourceId = (int)CalculationColumnType.SST
            });
            list.Add(new CalcGetHeaderListResult
            {
                ColumnType = CalculationColumnType.TDS,
                HeaderName = "TDS",
                Order = 2,
                Type = (int)CalculationColumnType.TDS,
                SourceId = (int)CalculationColumnType.TDS
            });


            if (CommonManager.Setting.AccountingVoucherEnum != null && CommonManager.Setting.AccountingVoucherEnum == (int)AccountingVoucherTypeEnum.GLOBUS)
            {

                list.Add(new CalcGetHeaderListResult
                {
                    ColumnType = CalculationColumnType.NetSalary,
                    HeaderName = "Net Salary",
                    Order = 2,
                    Type = (int)CalculationColumnType.NetSalary,
                    SourceId = (int)CalculationColumnType.NetSalary
                });
            }
          

            return list;
        }

        public static List<CalcGetHeaderListResult> GetHeaderListForTaxCertificate()
        {


            List<CalcGetHeaderListResult> list = new List<CalcGetHeaderListResult>();

            list.AddRange(

                (
                  from i in PayrollDataContext.PIncomes
                  //where //i.IsEnabled == true &&
                  //i.Calculation != IncomeCalculation.DEEMED_INCOME

                  select new CalcGetHeaderListResult
                  {
                      Type = 1,
                      SourceId = i.IncomeId,
                      HeaderName = i.Title,
                      CalculationText = HttpContext.GetGlobalResourceObject("FixedValues", i.Calculation).ToString()
                  }
                )

                );

          





            list.Add(new CalcGetHeaderListResult
            {
                ColumnType = CalculationColumnType.IncomeLeaveEncasement,
                HeaderName = "Leave Encashment",
                Order = 2,
                Type = (int)CalculationColumnType.IncomeLeaveEncasement,
                SourceId = (int)CalculationColumnType.IncomeLeaveEncasement
            });

            list.Add(new CalcGetHeaderListResult
            {
                ColumnType = CalculationColumnType.IncomePF,
                HeaderName = "Income PF",
                Order = 2,
                Type = (int)CalculationColumnType.IncomePF,
                SourceId = (int)CalculationColumnType.IncomePF
            });



            return list;
        }
        public static Setting GetSetting()
        {
            return CommonManager.Setting;
            //Setting setting = PayrollDataContext.Settings.FirstOrDefault();
            //return setting;
        }

        public static void SaveUpdateSetting(Setting entity)
        {
            Setting setting = PayrollDataContext.Settings.FirstOrDefault();
            if (setting != null)
            {
                setting.ContractUptoDay = entity.ContractUptoDay;



                PayrollPeriod payroll = CommonManager.GetLastPayrollPeriod();
                CustomDate payrollCD = CustomDate.GetCustomDateFromString(payroll.StartDate, IsEnglish);
                CustomDate date = new CustomDate(setting.ContractUptoDay.Value, payrollCD.Month, payrollCD.Year, IsEnglish);
                // if value changed then only update
                if (setting.ContractUptoDateEng == null || setting.ContractUptoDate != date.ToString() || setting.ContractUptoDateEng != date.EnglishDate)
                {
                    setting.ContractUptoDate = date.ToString();
                    setting.ContractUptoDateEng = date.EnglishDate;
                }


            }
            else
            {
                PayrollDataContext.Settings.InsertOnSubmit(entity);
            }
            PayrollDataContext.SubmitChanges();
        }
        public static void SaveUpdateSalaryProcessingSetting(Setting entity)
        {
            Setting setting = PayrollDataContext.Settings.FirstOrDefault();
            if (setting != null)
            {
                setting.SalaryProcessUptoDay = entity.SalaryProcessUptoDay;
            }
            else
            {
                PayrollDataContext.Settings.InsertOnSubmit(entity);
            }
            PayrollDataContext.SubmitChanges();
        }

        
        public static List<CalcGetHeaderListResult> GetPartialTaxPaidHeaderList(int payrollPeriodId,bool forImport)
        {
           
            List<CalcGetHeaderListResult> list = PartialAddOnManager.GetAddOnHeaderListForSelection(payrollPeriodId);
           
            list.Add(new CalcGetHeaderListResult
            {
                ColumnType = CalculationColumnType.IncomePF,
                HeaderName = "Income PF",
                Order = 2,
                Type = (int) CalculationColumnType.IncomePF,
                SourceId = (int)CalculationColumnType.IncomePF
            });



            list.Add(new CalcGetHeaderListResult
            {
                ColumnType = CalculationColumnType.DeductionCIT,
                HeaderName = "CIT",
                Order = 2,
                Type = (int)CalculationColumnType.DeductionCIT,
                SourceId = (int)CalculationColumnType.DeductionCIT
            });

            //if (forImport == false)
            {
                list.Add(new CalcGetHeaderListResult
                {
                    ColumnType = CalculationColumnType.DeductionPF,
                    HeaderName = "Deduction PF",
                    Order = 2,
                    Type = (int)CalculationColumnType.DeductionPF,
                    SourceId = (int)CalculationColumnType.DeductionPF
                });
            }
            list.Add(new CalcGetHeaderListResult
            {
                ColumnType = CalculationColumnType.TDS,
                HeaderName = "TDS",
                Order = 2,
                Type = (int)CalculationColumnType.TDS,
                SourceId = (int)CalculationColumnType.TDS
            });

            if (forImport == false)
            {
                list.Add(new CalcGetHeaderListResult
                {
                    ColumnType = CalculationColumnType.IncomeGross,
                    HeaderName = "Gross Income",
                    Order = 2,
                    Type = (int)CalculationColumnType.IncomeGross,
                    SourceId = (int)CalculationColumnType.IncomeGross
                });
                list.Add(new CalcGetHeaderListResult
                {
                    ColumnType = CalculationColumnType.DeductionTotal,
                    HeaderName = "Total Deduction",
                    Order = 2,
                    Type = (int)CalculationColumnType.DeductionTotal,
                    SourceId = (int)CalculationColumnType.DeductionTotal
                }); list.Add(new CalcGetHeaderListResult
                {
                    ColumnType = CalculationColumnType.NetSalary,
                    HeaderName = "Net Salary",
                    Order = 2,
                    Type = (int)CalculationColumnType.NetSalary,
                    SourceId = (int)CalculationColumnType.NetSalary
                });
            }
            return list;
        }

        public static List<CalcGetHeaderListResult> GetAddOnHeaderList(int addOnId, bool forImport)
        {

            List<CalcGetHeaderListResult> list = PartialAddOnManager.GetAddOnHeaderListNew(addOnId);

            //list.Add(new CalcGetHeaderListResult
            //{
            //    ColumnType = CalculationColumnType.IncomePF,
            //    HeaderName = "Income PF",
            //    Order = 2,
            //    Type = (int)CalculationColumnType.IncomePF,
            //    SourceId = (int)CalculationColumnType.IncomePF
            //});



            //list.Add(new CalcGetHeaderListResult
            //{
            //    ColumnType = CalculationColumnType.DeductionCIT,
            //    HeaderName = "CIT",
            //    Order = 2,
            //    Type = (int)CalculationColumnType.DeductionCIT,
            //    SourceId = (int)CalculationColumnType.DeductionCIT
            //});

            ////if (forImport == false)
            //{
            //    list.Add(new CalcGetHeaderListResult
            //    {
            //        ColumnType = CalculationColumnType.DeductionPF,
            //        HeaderName = "Deduction PF",
            //        Order = 2,
            //        Type = (int)CalculationColumnType.DeductionPF,
            //        SourceId = (int)CalculationColumnType.DeductionPF
            //    });
            //}
            //list.Add(new CalcGetHeaderListResult
            //{
            //    ColumnType = CalculationColumnType.TDS,
            //    HeaderName = "TDS",
            //    Order = 2,
            //    Type = (int)CalculationColumnType.TDS,
            //    SourceId = (int)CalculationColumnType.TDS
            //});

            if (forImport == false)
            {
                list.Add(new CalcGetHeaderListResult
                {
                    ColumnType = CalculationColumnType.IncomeGross,
                    HeaderName = "Gross Income",
                    Order = 2,
                    Type = (int)CalculationColumnType.IncomeGross,
                    SourceId = (int)CalculationColumnType.IncomeGross
                });
                list.Add(new CalcGetHeaderListResult
                {
                    ColumnType = CalculationColumnType.DeductionTotal,
                    HeaderName = "Total Deduction",
                    Order = 2,
                    Type = (int)CalculationColumnType.DeductionTotal,
                    SourceId = (int)CalculationColumnType.DeductionTotal
                }); list.Add(new CalcGetHeaderListResult
                {
                    ColumnType = CalculationColumnType.NetSalary,
                    HeaderName = "Net Salary",
                    Order = 2,
                    Type = (int)CalculationColumnType.NetSalary,
                    SourceId = (int)CalculationColumnType.NetSalary
                });
            }
            return list;
        }

        /// <summary>
        /// Retrive the list of Incomes of the company to be displayed in Calculation list
        /// </summary>
        public static List<CalcGetHeaderListResult> GetPaySummaryReportHeaderList(int companyId, int payrollPeriodId)
        {
            List<Report_Pay_SalarySummary_HeaderListResult> list= PayrollDataContext.Report_Pay_SalarySummary_HeaderList(payrollPeriodId, companyId)
                .ToList();

            List<CalcGetHeaderListResult> newList = new List<CalcGetHeaderListResult>();

            foreach (Report_Pay_SalarySummary_HeaderListResult item in list)
            {
                CalcGetHeaderListResult newItem = new CalcGetHeaderListResult();
                newItem.Type = item.Type;
                newItem.SourceId = item.SourceId;
                newItem.HeaderName = item.HeaderName;

                newList.Add(newItem);
            }
            return newList;


        }

        /// <summary>
        /// Retrive the list of Incomes of the company to be displayed in Calculation list
        /// </summary>
        public static List<CalcGetHeaderListResult> GetAddOnHeaderList(int addOndId,int periodId)
        {
            List<CalcGetHeaderListResult> list = new List<CalcGetHeaderListResult>();


            if (addOndId != 0)
            {

                list =
                    (
                        from m in PayrollDataContext.AddOnHeaders
                        where m.AddOnId == addOndId
                        select new CalcGetHeaderListResult
                        {
                            Type = m.Type,
                            SourceId = m.SourceId,
                            HeaderName = m.Title
                        }
                    ).ToList();

                list.Add(new CalcGetHeaderListResult
                {
                    Type = (int)CalculationColumnType.IncomeGross,
                    SourceId = (int)CalculationColumnType.IncomeGross,
                    HeaderName = "Gross Income"
                });

                list.Add(new CalcGetHeaderListResult
                {
                    Type = (int)CalculationColumnType.DeductionTotal,
                    SourceId = (int)CalculationColumnType.DeductionTotal,
                    HeaderName = "Total Deduction"
                });

                list.Add(new CalcGetHeaderListResult
                {
                    Type = (int)CalculationColumnType.NetSalary,
                    SourceId = (int)CalculationColumnType.NetSalary,
                    HeaderName = "Net Salary"
                });
            }

            else
            {
                list = GetPartialTaxPaidHeaderList(periodId, false);
            }
            return list;


        }

        public class AddOnName
        {
            public int SN { get; set; }
            public int AddOnId { get; set; }
            public string Name { get; set; }
            public string Period { get; set; }
            public int PayrollPeriodId { get; set; }
        }
        public static List<AddOnName> GetAddOnList(int yearId)
        {
            List<AddOnName> list = new List<AddOnName>();

                //(
                //    from a in PayrollDataContext.AddOns
                //    join p in PayrollDataContext.PayrollPeriods on a.PayrollPeriodId equals p.PayrollPeriodId
                //    where p.FinancialDateId == yearId
                //    orderby p.PayrollPeriodId
                //    select new AddOnName
                //    {
                //        AddOnId = a.AddOnId,
                //        Name = a.Name,
                //        Period = p.Name,
                //        PayrollPeriodId = p.PayrollPeriodId
                //    }
                //).ToList();

            // single add on only
            List<GetSingleAddOnListResult> addOnList = PayrollDataContext.GetSingleAddOnList(yearId).ToList();

            foreach (var item in addOnList)
            {
                list.Add(new AddOnName
                {
                    AddOnId = item.AddOnId,
                    Name = item.Name,
                    Period = item.Period,
                    PayrollPeriodId = item.PayrollPeriodId
                });
            }


            return list;
        }
        public static AddOn GetAddOnId(int addOnId)
        {
            return
                (
                    from a in PayrollDataContext.AddOns
                    where a.AddOnId == addOnId
                    select a
                ).FirstOrDefault();
        }
        public  static  bool HasPayrollPeriodRetirementOrResignedEmp(int payrollPeriodId)
        {
            return PayrollDataContext.CalcHasPayrollPeriodRetiredOrResigned(payrollPeriodId).Value;
        }

        public static  int GetTotalApplicableForSalary(int payrollPeriodId,bool isRetiredResignedOnly)
        {
            int cid = SessionManager.CurrentCompanyId;

            return PayrollDataContext.CalcGetTotalEmployeeForSalary(payrollPeriodId, cid, isRetiredResignedOnly)
                .Value;
        }
        public static int GetTotalApplicableForFestivalIncome(int payrollPeriodId, bool isRetiredResignedOnly,int incomeId)
        {
            int cid = SessionManager.CurrentCompanyId;

            return PayrollDataContext.CalcGetTotalEmployeeFestivalIncomeAssociated(payrollPeriodId, cid, isRetiredResignedOnly,incomeId)
                .Value;
        }
        public static PayrollPeriod GetEmpFirstSalaryGeneratedPayrollPeriod(int employeeId, int currentPayrollPeriodId)
        {
            int? val =
                PayrollDataContext.GetEmpFirstSalaryGeneratedPayrollPeriod(currentPayrollPeriodId, employeeId);

            if(val==null)
                return null;

            return CommonManager.GetPayrollPeriod(val.Value);
        }

        public static ResponseStatus ChangePastSavedSalary(List<PastSalaryBO> pastList,int payrollPeriodId)
            //(int employeeId, int payrollPeriodId, int typeId, int sourceId, decimal newValue)
        {


          //  CalculationColumnType type = (CalculationColumnType)typeId;
            CCalculation cal = GetCalculation(payrollPeriodId);
            ResponseStatus status = new ResponseStatus();
            
            if (cal != null)
            {
                CCalculationHeader pfIncomeheader = PayrollDataContext.CCalculationHeaders.SingleOrDefault(x => x.CalculationId == cal.CalculationId && x.Type == (int)CalculationColumnType.IncomePF && x.SourceId == (int)CalculationColumnType.IncomePF);
                CCalculationHeader pfDeductionheader = PayrollDataContext.CCalculationHeaders.SingleOrDefault(x => x.CalculationId == cal.CalculationId && x.Type == (int)CalculationColumnType.DeductionPF && x.SourceId == (int)CalculationColumnType.DeductionPF);
              
                CCalculationHeader citheader = PayrollDataContext.CCalculationHeaders.SingleOrDefault(x => x.CalculationId == cal.CalculationId && x.Type == (int)CalculationColumnType.DeductionCIT && x.SourceId == (int)CalculationColumnType.DeductionCIT);
                CCalculationHeader sstheader = PayrollDataContext.CCalculationHeaders.SingleOrDefault(x => x.CalculationId == cal.CalculationId && x.Type == (int)CalculationColumnType.SST && x.SourceId == (int)CalculationColumnType.SST);
                CCalculationHeader tdsheader = PayrollDataContext.CCalculationHeaders.SingleOrDefault(x => x.CalculationId == cal.CalculationId && x.Type == (int)CalculationColumnType.TDS && x.SourceId == (int)CalculationColumnType.TDS);


                CCalculationHeader totalDeductionHeader = PayrollDataContext.CCalculationHeaders.SingleOrDefault(x => x.CalculationId == cal.CalculationId && x.Type == (int)CalculationColumnType.DeductionTotal && x.SourceId == (int)CalculationColumnType.DeductionTotal);
                CCalculationHeader netHeader = PayrollDataContext.CCalculationHeaders.SingleOrDefault(x => x.CalculationId == cal.CalculationId && x.Type == (int)CalculationColumnType.NetSalary && x.SourceId == (int)CalculationColumnType.NetSalary);
                CCalculationHeader incomeGrossHeader = PayrollDataContext.CCalculationHeaders.SingleOrDefault(x => x.CalculationId == cal.CalculationId && x.Type == (int)CalculationColumnType.IncomeGross && x.SourceId == (int)CalculationColumnType.IncomeGross);

                foreach (PastSalaryBO newSalary in pastList)
                {

                    CCalculationEmployee employee = PayrollDataContext.CCalculationEmployees.SingleOrDefault(x => x.CalculationId == cal.CalculationId && x.EmployeeId == newSalary.EmployeeID);

                    CCalculationDetail pfIncomesalary = null;
                    CCalculationDetail pfDeductionsalary = null;

                    if (pfIncomeheader != null)
                    {
                        pfIncomesalary = PayrollDataContext.CCalculationDetails.SingleOrDefault(x => x.CalculationEmployeeId == employee.CalculationEmployeeId
                             && x.CalculationHeaderId == pfIncomeheader.CalculationHeaderId);
                        pfDeductionsalary = PayrollDataContext.CCalculationDetails.SingleOrDefault(x => x.CalculationEmployeeId == employee.CalculationEmployeeId
                            && x.CalculationHeaderId == pfDeductionheader.CalculationHeaderId);
                    }

                    CCalculationDetail citsalary = null;

                    if (citheader != null)
                        citsalary = PayrollDataContext.CCalculationDetails.SingleOrDefault(x => x.CalculationEmployeeId == employee.CalculationEmployeeId
                             && x.CalculationHeaderId == citheader.CalculationHeaderId);

                    CCalculationDetail sstsalary = PayrollDataContext.CCalculationDetails.SingleOrDefault(x => x.CalculationEmployeeId == employee.CalculationEmployeeId
                         && x.CalculationHeaderId == sstheader.CalculationHeaderId);
                    CCalculationDetail tdssalary = PayrollDataContext.CCalculationDetails.SingleOrDefault(x => x.CalculationEmployeeId == employee.CalculationEmployeeId
                        && x.CalculationHeaderId == tdsheader.CalculationHeaderId);
                    
                    CCalculationDetail totalDeductionSalary = PayrollDataContext.CCalculationDetails.SingleOrDefault(x => x.CalculationEmployeeId == employee.CalculationEmployeeId
                        && x.CalculationHeaderId == totalDeductionHeader.CalculationHeaderId);
                    CCalculationDetail incomeGrossSalary = PayrollDataContext.CCalculationDetails.SingleOrDefault(x => x.CalculationEmployeeId == employee.CalculationEmployeeId
                        && x.CalculationHeaderId == incomeGrossHeader.CalculationHeaderId);
                    CCalculationDetail netSalary = PayrollDataContext.CCalculationDetails.SingleOrDefault(x => x.CalculationEmployeeId == employee.CalculationEmployeeId
                       && x.CalculationHeaderId == netHeader.CalculationHeaderId);

                    EmployeeRegularIncomeHistory history = PayrollDataContext.EmployeeRegularIncomeHistories.SingleOrDefault(x => x.EmployeeId == newSalary.EmployeeID && x.PayrollPeriodId == payrollPeriodId);
                    
                    //decimal? before = null;
                    CCalculationDetail salary = null;
                    //Income PF
                    if (newSalary.IncomePF != null)
                    {
                        if (pfIncomesalary != null)
                        {
                            //first deduct old value & add new value
                            incomeGrossSalary.Amount -= pfIncomesalary.Amount;
                            incomeGrossSalary.Amount += newSalary.IncomePF.Value;
                            pfIncomesalary.Amount = newSalary.IncomePF.Value;
                            PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(newSalary.EmployeeID, (byte)MonetoryChangeLogTypeEnum.PastSalaryChanged, "Income PF", pfIncomesalary.Amount, newSalary.IncomePF, LogActionEnum.Update));
                        }
                        else
                        {
                            salary = new CCalculationDetail();
                            salary.Amount = newSalary.IncomePF.Value;
                            incomeGrossSalary.Amount += newSalary.IncomePF.Value;
                            PayrollDataContext.CCalculationDetails.InsertOnSubmit(salary);
                            PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(newSalary.EmployeeID, (byte)MonetoryChangeLogTypeEnum.PastSalaryChanged, "Income PF", null, newSalary.IncomePF, LogActionEnum.Update));

                        }

                        EmployeeRegularIncomeHistory empCalculationHistory = PayrollDataContext.EmployeeRegularIncomeHistories.SingleOrDefault(x => x.EmployeeId == newSalary.EmployeeID &&
                            x.PayrollPeriodId == payrollPeriodId);
                        if (empCalculationHistory != null)
                            empCalculationHistory.IncomePF = newSalary.IncomePF;

                    }

                    //Deduciton PF
                    if (newSalary.DeductionPF != null)
                    {
                        if (pfDeductionsalary != null)
                        {
                            //first deduct old value & add new value
                            totalDeductionSalary.Amount -= pfDeductionsalary.Amount;
                            totalDeductionSalary.Amount += newSalary.DeductionPF.Value;
                            pfDeductionsalary.Amount = newSalary.DeductionPF.Value;
                            PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(newSalary.EmployeeID, (byte)MonetoryChangeLogTypeEnum.PastSalaryChanged, "Deduction PF", pfDeductionsalary.Amount, newSalary.DeductionPF, LogActionEnum.Update));
                        }
                        else
                        {
                            salary = new CCalculationDetail();
                            salary.Amount = newSalary.DeductionPF.Value;
                            totalDeductionSalary.Amount += newSalary.DeductionPF.Value;
                            PayrollDataContext.CCalculationDetails.InsertOnSubmit(salary);
                            PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(newSalary.EmployeeID, (byte)MonetoryChangeLogTypeEnum.PastSalaryChanged, "Deduction PF", null, newSalary.DeductionPF, LogActionEnum.Update));

                        }

                        EmployeeRegularIncomeHistory empCalculationHistory = PayrollDataContext.EmployeeRegularIncomeHistories.SingleOrDefault(x => x.EmployeeId == newSalary.EmployeeID &&
                           x.PayrollPeriodId == payrollPeriodId);
                        if (empCalculationHistory != null)
                            empCalculationHistory.PF = newSalary.DeductionPF;
                    }


                    //CIT
                    if (citheader != null)
                    {
                        if (newSalary.CIT != null)
                        {
                            if (citsalary != null)
                            {
                                //first deduct old value & add new value
                                totalDeductionSalary.Amount -= citsalary.Amount;
                                totalDeductionSalary.Amount += newSalary.CIT.Value;
                                citsalary.Amount = newSalary.CIT.Value;
                                PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(newSalary.EmployeeID, (byte)MonetoryChangeLogTypeEnum.PastSalaryChanged, "CIT", citsalary.Amount, newSalary.CIT, LogActionEnum.Update));
                            }
                            else
                            {
                                salary = new CCalculationDetail();
                                salary.Amount = newSalary.CIT.Value;
                                totalDeductionSalary.Amount += newSalary.CIT.Value;
                                PayrollDataContext.CCalculationDetails.InsertOnSubmit(salary);
                                PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(newSalary.EmployeeID, (byte)MonetoryChangeLogTypeEnum.PastSalaryChanged, "CIT", null, newSalary.CIT, LogActionEnum.Update));

                            }
                        }
                    }

                    //SST
                    if (newSalary.SST != null)
                    {
                        history.TaxPaidSST = newSalary.SST.Value;
                        if (sstsalary != null)
                        {
                            //first deduct old value & add new value
                            totalDeductionSalary.Amount -= sstsalary.Amount;
                            totalDeductionSalary.Amount += newSalary.SST.Value;
                            sstsalary.Amount = newSalary.SST.Value;
                            PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(newSalary.EmployeeID, (byte)MonetoryChangeLogTypeEnum.PastSalaryChanged, "SST", sstsalary.Amount, newSalary.SST, LogActionEnum.Update));
                        }
                        else
                        {
                            salary = new CCalculationDetail();
                            salary.Amount = newSalary.SST.Value;
                            totalDeductionSalary.Amount += newSalary.SST.Value;
                            PayrollDataContext.CCalculationDetails.InsertOnSubmit(salary);
                            PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(newSalary.EmployeeID, (byte)MonetoryChangeLogTypeEnum.PastSalaryChanged, "SST", null, newSalary.SST, LogActionEnum.Update));

                        }
                    }
                    //TDS
                    if (newSalary.TDS != null)
                    {
                        history.TaxPaid = newSalary.TDS.Value;
                        if (tdssalary != null)
                        {
                            //first deduct old value & add new value
                            totalDeductionSalary.Amount -= tdssalary.Amount;
                            totalDeductionSalary.Amount += newSalary.TDS.Value;
                            tdssalary.Amount = newSalary.TDS.Value;
                            PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(newSalary.EmployeeID, (byte)MonetoryChangeLogTypeEnum.PastSalaryChanged, "TDS", tdssalary.Amount, newSalary.TDS, LogActionEnum.Update));
                        }
                        else
                        {
                            salary = new CCalculationDetail();
                            salary.Amount = newSalary.TDS.Value;
                            totalDeductionSalary.Amount += newSalary.TDS.Value;
                            PayrollDataContext.CCalculationDetails.InsertOnSubmit(salary);
                            PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(newSalary.EmployeeID, (byte)MonetoryChangeLogTypeEnum.PastSalaryChanged, "TDS", null, newSalary.TDS, LogActionEnum.Update));

                        }
                    }
                    
                  

                    netSalary.Amount = incomeGrossSalary.Amount - totalDeductionSalary.Amount;

                    if (netSalary.Amount < 0)
                    {
                        status.ErrorMessage += string.Format("Net amount can not be negative for \"{0}\".</br>", EmployeeManager.GetEmployeeById(newSalary.EmployeeID).Name);
                      
                    }

                    

                }

                if (status.IsSuccessType == true)
                {

                    PayrollDataContext.SubmitChanges();
                    status.IsSuccessType = true;

                }

            }
            else
                status.ErrorMessage = "Payroll doesn't exists.";

            return status;
        }

        public static CCalculation IsCalculationSaved(int payrollPeriodId)
        {
            CCalculation calculation=
                PayrollDataContext.CCalculations.Where(c => c.PayrollPeriodId == payrollPeriodId)
                    .FirstOrDefault();

            return calculation;
        }
        public static bool IsPayrollFinalSaved(int payrollPeriodId)
        {
            CCalculation calc = 
                PayrollDataContext.CCalculations.SingleOrDefault(
                    c => c.PayrollPeriodId == payrollPeriodId);

            if (calc == null)
                return false;
            if (calc.IsFinalSaved == null)
                return false;
            return calc.IsFinalSaved.Value;
        }
        public static bool IsCalculationSavedForEmployee(int payrollPeriodId,int employeeId)
        {
            return PayrollDataContext.IsCalcualtionSavedForEmployee(payrollPeriodId, employeeId).Value;
        }
        public static bool IsCalculatedSavedForEmployee(int employeeId)
        {
            return PayrollDataContext.CCalculationEmployees.Any(e => e.EmployeeId == employeeId);
        }
       
        /// <summary>
        /// Save calculation cell value for first phase
        /// </summary>
        public bool SaveUpdateCalculation(int payrollPeriodId, bool hasAdjIncome, string adjIncome, bool hasAdjDeduction,
            string adjDeduction, bool hasPF, string pfRFNo, double employeeContribution, double companyContribution, 
            string xmlHeaders, string xmlEmployees, string xmlCells, string xmlIncluded)
        {

            // only last period salary saving allowed
            if (CommonManager.GetLastPayrollPeriod().PayrollPeriodId != payrollPeriodId)
                return false;


            bool readingSumForMiddleFiscalYearStartedReqd = false;
            //contains the id of starting-ending payroll period to be read for history of regular income
            int startingPayrollPeriodId = 0, endingPayrollPeriodId = 0;

            PayrollPeriod startingMonthParollPeriodInFiscalYear =
                GenerateForPastIncome(
                    payrollPeriodId, ref readingSumForMiddleFiscalYearStartedReqd,
                    ref startingPayrollPeriodId, ref endingPayrollPeriodId);

            ChangeUserActivity log = GetChangeUserActivityLog("Calculation saved for " + CommonManager.GetPayrollPeriod(payrollPeriodId).Name + ".");
            PayrollDataContext.ChangeUserActivities.InsertOnSubmit(log);

            PayrollPeriodSignOff entity = new PayrollPeriodSignOff();
            entity.SignOffDate = log.DateEng;
            entity.SignOffUserID = SessionManager.User.UserID;
            entity.Type = (int)SalaryCaluclationChangedType.Saved;
            entity.PayrollPeriodId = payrollPeriodId;
            PayrollDataContext.PayrollPeriodSignOffs.InsertOnSubmit(entity);

            

            PayrollDataContext.CalcSaveCalculation(payrollPeriodId, hasAdjIncome, adjIncome, hasAdjDeduction,
                                                   adjDeduction, hasPF, pfRFNo, employeeContribution, companyContribution,
                                                   XElement.Parse(xmlHeaders), XElement.Parse(xmlEmployees),
                                                   XElement.Parse(xmlCells), XElement.Parse(xmlIncluded)
                                                   , readingSumForMiddleFiscalYearStartedReqd,
                                                          startingPayrollPeriodId, endingPayrollPeriodId,SessionManager.User.UserID,log.DateEng,
                                                          LeaveAttendanceManager.GetTotalDaysInFiscalYearForPeriod(payrollPeriodId),SessionManager.UserName);

            CCalculation calculation = IsCalculationSaved(payrollPeriodId);
            if (calculation != null && CalculationManager.GetSetting().ContractUptoDay != null)
            {
                //calculation = IsCalculationSaved(payrollPeriodId);

                int period = calculation.PayrollPeriodId;
                PayrollPeriod payroll = CommonManager.GetPayrollPeriod(period);
                CustomDate payrollCD = CustomDate.GetCustomDateFromString(payroll.StartDate, IsEnglish);
                CustomDate date = new CustomDate(CalculationManager.GetSetting().ContractUptoDay.Value, payrollCD.Month, payrollCD.Year, IsEnglish);

                //lblUnitRateToDate.Text = date.ToString();

                
                calculation.UnitRateUptoDate = date.ToString();
                if (!string.IsNullOrEmpty(calculation.UnitRateUptoDate))
                    calculation.UnitRateUptoDateEng = GetEngDate(calculation.UnitRateUptoDate, IsEnglish);
                

            }

            

            PayrollDataContext.SubmitChanges();


            return true;
        }


        public static void ApproveSalary(int payrollPeriodId)
        {

            PayrollPeriodSignOff entity = new PayrollPeriodSignOff();
            entity.SignOffDate = DateTime.Now;
            entity.SignOffUserID = SessionManager.User.UserID;
            entity.Type = (int)SalaryCaluclationChangedType.Approved;
            entity.PayrollPeriodId = payrollPeriodId;
            PayrollDataContext.PayrollPeriodSignOffs.InsertOnSubmit(entity);
            PayrollDataContext.SubmitChanges();
        }

        /// <summary>
        /// Final save for tax generation
        /// </summary>
        public static void SaveFinally(int payrollPeriodId, bool forRetiredResignedOnly)
        {

            bool readingSumForMiddleFiscalYearStartedReqd = false;
            //contains the id of starting-ending payroll period to be read for history of regular income
            int startingPayrollPeriodId = 0, endingPayrollPeriodId = 0;

            PayrollPeriod startingMonthParollPeriodInFiscalYear =
                GenerateForPastIncome(
                    payrollPeriodId, ref readingSumForMiddleFiscalYearStartedReqd,
                    ref startingPayrollPeriodId, ref endingPayrollPeriodId);

            PayrollDataContext.CalcFinalSave(payrollPeriodId, forRetiredResignedOnly, readingSumForMiddleFiscalYearStartedReqd, startingPayrollPeriodId, endingPayrollPeriodId
                , LeaveAttendanceManager.GetTotalDaysInFiscalYearForPeriod(payrollPeriodId), LeaveAttendanceManager.GetLeaveYearStartDate(payrollPeriodId));

            

            PayrollDataContext.ChangeUserActivities.InsertOnSubmit(
                 GetChangeUserActivityLog("Calculation final saved for " + CommonManager.GetPayrollPeriod(payrollPeriodId).Name + "."));
            PayrollDataContext.SubmitChanges();


            if (!forRetiredResignedOnly)
                CommonManager.CreateNewPayrollPeriodAutomaticallyInFinalSave(payrollPeriodId);
        }


        public int GetTotalEmployeeInitialSave(int payrollPeriodId, bool isRetiredResignedOnly)
        {
            return PayrollDataContext.CalcGetEmployeesInitialSaved(payrollPeriodId, isRetiredResignedOnly).Value;
        }

        /// <summary>
        /// Used to check if all the Employees are saved finally in the calcualtion
        /// </summary>
        public static bool IsAllEmployeeSavedFinally(int payrollPeriodId, bool isRetiredResignedOnly)
        {
            return PayrollDataContext.CalcHasAllEmployeesSavedFinal(payrollPeriodId, isRetiredResignedOnly).Value;
        }

        public  static  bool IsPayrollEditable(int payrollPeriodId)
        {
            return PayrollDataContext.CalcModeIsPayrollEditable(payrollPeriodId).Value;
        }

        public static bool IsNextPayrollAtteSaved(int payrollPeriodId)
        {
            PayrollPeriod nextPayroll =  PayrollDataContext.PayrollPeriods
                .Where(x => x.PayrollPeriodId > payrollPeriodId && x.CompanyId == SessionManager.CurrentCompanyId)
                .OrderBy(x => x.PayrollPeriodId).FirstOrDefault();

            if (nextPayroll == null)
                return false;

            return LeaveAttendanceManager.IsAttendanceSaved(nextPayroll.PayrollPeriodId);
        }
        

        /// <summary>
        /// Returns true of any empoloyee salary is not being used
        /// </summary>
        /// <param name="payrollPeriodId"></param>
        /// <returns></returns>
        public static bool IsPayrollNotSavedAtAll(int payrollPeriodId)
        {
            return PayrollDataContext.IsPayrollNotSavedAtAll(payrollPeriodId).Value;
        }

        /// <summary>
        /// Delete(s) the row calculation
        /// </summary>
        /// <returns>Delete all including header/from calculation table also after deleting all employees</returns>
        public bool DeleteAllCalculation(int calculationId,  string payrollName)
        {
            int periodId = 0;

            CCalculation calc = CalculationManager.GetCalculationById(calculationId);
            if (calc != null)
                periodId = calc.PayrollPeriodId;

            // only last period salary deletion allowed
            if (CommonManager.GetLastPayrollPeriod().PayrollPeriodId != periodId)
                return false;

            bool status = PayrollDataContext.CalcDeleteAllCalculation(calculationId,SessionManager.UserName)
                      .SingleOrDefault().Success == "1";

            //if (status)
            {
                PayrollDataContext.ChangeUserActivities.InsertOnSubmit(
                GetChangeUserActivityLog("Calculation deleted for " + payrollName + "."));

                PayrollPeriodSignOff entity = new PayrollPeriodSignOff();
                entity.SignOffDate = DateTime.Now;
                entity.SignOffUserID = SessionManager.User.UserID;
                entity.PayrollPeriodId = periodId;
                entity.Type = (int)SalaryCaluclationChangedType.DeleteAll;
                PayrollDataContext.PayrollPeriodSignOffs.InsertOnSubmit(entity);

                PayrollDataContext.SubmitChanges();
            }
            return status;
        }


        /// <summary>
        /// Delete(s) the row calculation
        /// </summary>
        /// <returns>Delete all including header/from calculation table also after deleting all employees</returns>
        public bool DeleteCalculation(int calculationId, string xmlEmployeees,string payrollName)
        {

            int periodId = 0;

            CCalculation calc = CalculationManager.GetCalculationById(calculationId);
            if (calc != null)
                periodId = calc.PayrollPeriodId;

            // only last period salary deletion allowed
            if (CommonManager.GetLastPayrollPeriod().PayrollPeriodId != periodId)
                return false;

            bool status =  PayrollDataContext.CalcDeleteCalculation(calculationId, (xmlEmployeees),SessionManager.UserName)
                       .SingleOrDefault().Success == "1";

            //if (status)
            {
                PayrollDataContext.ChangeUserActivities.InsertOnSubmit(
                GetChangeUserActivityLog("Calculation deleted for " + payrollName + "."));

                PayrollPeriodSignOff entity = new PayrollPeriodSignOff();
                entity.SignOffDate = DateTime.Now;
                entity.SignOffUserID = SessionManager.User.UserID;
                entity.Type = (int)SalaryCaluclationChangedType.Deleted;
                entity.PayrollPeriodId = periodId;
                PayrollDataContext.PayrollPeriodSignOffs.InsertOnSubmit(entity);

                PayrollDataContext.SubmitChanges();
            }
            return status;

        }

        #endregion


        /// <summary>
        /// Checks if the amount value for the income is changable or not
        /// </summary>
        public static  bool IsAmountChangable(CalcGetHeaderListResult incomeHeader)
        {
            PayManager mgr = new PayManager();
            if (incomeHeader.Type == (int)CalculationColumnType.Income)
            {
                
                PIncome income = mgr.GetIncomeById(incomeHeader.SourceId.Value);
                if (income.Calculation == IncomeCalculation.VARIABLE_AMOUNT
                || (income.Calculation==IncomeCalculation.FESTIVAL_ALLOWANCE && income.IsVariableAmount.Value))
                    return true;
            }
            else if(incomeHeader.Type == (int)CalculationColumnType.Deduction)
            {
                PDeduction deduction = mgr.GetDeductionById(incomeHeader.SourceId.Value);
                if (deduction.Calculation == DeductionCalculation.VARIABLE)
                    return true;
            }
            return false;
        }

        /// <summary>
        /// Checks if the cell is income taxable variable type or not
        /// </summary>
        public static bool IsTypeTaxable(CalcGetHeaderListResult incomeHeader)
        {
            PayManager mgr = new PayManager();
            if (incomeHeader.Type == (int)CalculationColumnType.Income)
            {

                PIncome income = mgr.GetIncomeById(incomeHeader.SourceId.Value);
                if (
                    (
                    (income.Calculation == IncomeCalculation.VARIABLE_AMOUNT )
                     || (income.Calculation==IncomeCalculation.FESTIVAL_ALLOWANCE && income.IsVariableAmount.Value)) 
                    //&& income.IsSubjectOfIncomeTax && !income.IsCountAsReimursement)
                    && income.IsTaxable)
                    return true;
            }
            else if (incomeHeader.Type == (int)CalculationColumnType.Deduction)
            {
                PDeduction deduction = mgr.GetDeductionById(incomeHeader.SourceId.Value);
                if (deduction.Calculation == DeductionCalculation.VARIABLE
                    && deduction.IsExemptFromIncomeTax != null  && deduction.IsExemptFromIncomeTax.Value)
                    return true;
            }
            return false;
        }

        public static void DisableAllCells(GridView gvw)
        {

            for (int i = 0; i < gvw.Rows.Count; i++)
            {
                GridViewRow row = gvw.Rows[i];

                for (int j = 0; j < row.Cells.Count - 1; j++)
                {
                    TextBox txt = row.Cells[j].Controls[0] as TextBox;
                    if (txt != null)
                    {
                        MakeReadonly(txt);
                    }
                }
            }
        }

        public static string GetIncomesZeroizeInCurrentPeriod(int periodId)
        {
            List<GetIncomeListMarkedAsZeroizeResult> list =
                PayrollDataContext.GetIncomeListMarkedAsZeroize(periodId).ToList();

            if (list.Count == 0)
                return "";

            string incomes = "";

            foreach(var item in list)
            {
                if (incomes == "")
                    incomes = item.Title;
                else
                    incomes += ", " + item.Title;
            }

            return incomes;
        }

        /// <summary>
        /// Register code for up/down arrow movement and income/deduction list control IDs for summing
        /// </summary>
        public static void RegisterForTextBoxKeysMovement(GridView gvw)
        {
            return;
            //for (int i = 0; i < gvw.Rows.Count; i++)
            //{
            //    GridViewRow row = gvw.Rows[i];
            //    HiddenField incomeList = (HiddenField)row.FindControl("incomeListId");
            //    HiddenField deductionList = (HiddenField)row.FindControl("deductionListId");
            //    //if( incomeList==null)
            //    //    continue;
            //    incomeList.Value = "";
            //    deductionList.Value = "";
            //    for (int j = 2; j < row.Cells.Count-1; j++)
            //    {
            //        //for up
            //        if (i != 0)
            //        {

            //            TextBox txtCurrent = (TextBox) row.Cells[j].Controls[0];
            //            TextBox txtUp = (TextBox) gvw.Rows[i - 1].Cells[j].Controls[0];

            //            txtCurrent.Attributes.Add("upText", txtUp.ClientID);

            //        }
            //        //for down
            //        if (i != gvw.Rows.Count - 1)
            //        {
            //            TextBox txtCurrent = (TextBox) row.Cells[j].Controls[0];
            //            TextBox txtDown = (TextBox) gvw.Rows[i + 1].Cells[j].Controls[0];

            //            txtCurrent.Attributes.Add("downText", txtDown.ClientID);
            //        }
            //        //for left
            //        if (j != 2)
            //        {
            //            TextBox txtCurrent = (TextBox) row.Cells[j].Controls[0];
            //            TextBox txtLeft = (TextBox) gvw.Rows[i].Cells[j - 1].Controls[0];

            //            txtCurrent.Attributes.Add("leftText", txtLeft.ClientID);
            //        }

            //        //for right
            //        if (j < row.Cells.Count - 2)
            //        {
            //            TextBox txtCurrent = (TextBox)row.Cells[j].Controls[0];
            //            TextBox txtRight = (TextBox)gvw.Rows[i].Cells[j + 1].Controls[0];

            //            txtCurrent.Attributes.Add("rightText", txtRight.ClientID);
            //        }


            //        //for summing/totalling
            //        TextBox txt = (TextBox) row.Cells[j].Controls[0];
            //        CalculationColumnType type = (CalculationColumnType)Int32.Parse(txt.Attributes["columnType"]);
                    
            //        if(CalculationValue.IsColumTypeIncome(type))
            //        {
            //            incomeList.Value += txt.ClientID + ",";   
            //        }
            //        else if (CalculationValue.IsColumTypeDeduction(type))
            //        {
            //            deductionList.Value += txt.ClientID + ",";   
            //        }
            //    }

            //    //remove last "," character
            //    if( incomeList.Value.Length>0)
            //    incomeList.Value = incomeList.Value.Remove(incomeList.Value.Length - 1);
            //    if(deductionList.Value.Length>0)
            //    deductionList.Value = deductionList.Value.Remove(deductionList.Value.Length - 1);
            //}
        }

        public static Label GetLabelInHeader(ControlCollection ctls)
        {
            foreach (var ctl in ctls)
            {
                if ((ctl as Label) != null)
                    return ctl as Label;
            }
            return null;
        }


        public static  void AddCell(GridViewRow row,string color,bool isBottomBorder)
        {
            TableCell left = new TableHeaderCell();
            //left.ColumnSpan = count;
            //left.HorizontalAlign = HorizontalAlign.Center;
            left.BackColor = ColorTranslator.FromHtml(color);
            left.Style.Add("border-bottom", String.Format("1px solid {0}", color));
            //left.Text = Utils.Helper.Util.GetTextResource("CalculationIncomesTitle");
            row.Cells.Add(left);
        }

        



        public static void CreateGroupInHeader(object sender,bool isDeleteColumnVisible)
        {
            GridView grid = sender as GridView;
            TableHeaderCell cell;
            TableCell left;

            if (grid != null && grid.Rows.Count > 0)
            {
                ///add group table
                GridViewRow row = new GridViewRow(0, -1,
                                                  DataControlRowType.Header, DataControlRowState.Normal);

                GridViewRow headerRow = grid.HeaderRow;


                ///1. Empty cell for Employee Id & Name
                AddCell(row, "#1B93D0", true);
                AddCell(row, "#1B93D0", true);
                AddCell(row, "#1B93D0", true);

                ///2. Incomes, find the column span for Incomes
                int count = 0;
                int i = 3;

                ///Add d2 columns
                if (CommonManager.CompanySetting.IsD2)
                {
                    count = GetSimilarColumnCount(grid, headerRow, i, new IsRequiredColumn(CalculationValue.IsD2Column));
                    if (count != 0)
                    {
                        AddGroupColumn(row, count, "USD");
                        i += count;
                    }
                }
                // Add for multi Currency like british school case 
                if (CommonManager.CompanySetting.WhichCompany==WhichCompany.BritishSchool|| CommonManager.CompanySetting.IsAllIncomeInForeignCurrency)
                {
                    count = GetSimilarColumnCount(grid, headerRow, i, new IsRequiredColumn(CalculationValue.IsD2Column));
                    if (count != 0)
                    {
                        if (CommonManager.CompanySetting.IsAllIncomeInForeignCurrency)
                            AddGroupColumn(row, count, CalculationManager.CurrencyDetail.CurrencyShortName);
                        else
                            AddGroupColumn(row, count, "GBP");
                        i += count;
                    }
                }
                ///For Income heads
                count = GetSimilarColumnCount(grid, headerRow, i, new IsRequiredColumn(CalculationValue.IsColumTypeIncome));
                if (count != 0)
                {
                    AddGroupColumn(row, count, Util.GetTextResource("CalculationIncomesTitle"));
                    i += count;
                }

                
                
                //if (CommonManager.CalculationConstant.CompanyIsHPL.Value)
                //{
                //    ///4. HPL Unit Columns                   
                //    count = GetSimilarColumnCount(grid, headerRow, i, new IsRequiredColumn(CalculationValue.IsHPLUnitColumn));
                //    if (count != 0)
                //    {
                //        AddGroupColumn(row, count, Util.GetTextResource("CalculationOvertimeAndAllowancesUnitsText"));
                //        i += count;
                //    }


                //    ///5. HPL Overtime and Allowances Amount columns                   
                //    count = GetSimilarColumnCount(grid, headerRow, i, new IsRequiredColumn(CalculationValue.IsHPLAllowanceOrOvertimeColumn));
                //    if (count != 0)
                //    {
                //        AddGroupColumn(row, count, Util.GetTextResource("CalculationOvertimeAndAllowancesAmountText"));
                //        i += count;
                //    }


                //    ///6. Overtime total
                //    AddSingleEmptyGroupColumn(row);
                //    i += 1;
                //}

                
                
                

                ///5. Gross total, column for Income Total with rowspan
                AddSingleEmptyGroupColumn(row);
                i += 1;



                ///3. Column for Deemed income     
                count = 0;
                for (; i < grid.Columns.Count; i++)
                {
                    Label lbl = GetLabelInHeader(headerRow.Cells[i].Controls);
                    if (lbl != null)
                    {
                        CalculationColumnType columnType = (CalculationColumnType)Int32.Parse(lbl.Attributes["Type"]);
                        if (CalculationValue.IsColumTypeDeemedIncome(columnType))
                            count += 1;
                        else
                        {
                            break;
                        }
                    }
                }
                if (count != 0) //as optional
                {
                    left = new TableHeaderCell();
                    left.ColumnSpan = count;
                    left.HorizontalAlign = HorizontalAlign.Center;
                    left.BackColor = ColorTranslator.FromHtml(deductionColor);
                    left.Text = Util.GetTextResource("CalculationDeemedIncomesHeader");
                    row.Cells.Add(left);

                }

                ///6. Deduction columns                
                count = 0;
                for (; i < grid.Columns.Count; i++)
                {
                    Label lbl = GetLabelInHeader(headerRow.Cells[i].Controls);
                    if (lbl != null)
                    {
                        CalculationColumnType columnType = (CalculationColumnType)Int32.Parse(lbl.Attributes["Type"]);
                        if (CalculationValue.IsColumTypeDeduction(columnType))
                            count += 1;
                        else
                        {
                            break;
                        }
                    }
                }
                left = new TableHeaderCell();
                left.ColumnSpan = count;
                left.HorizontalAlign = HorizontalAlign.Center;
                left.BackColor = ColorTranslator.FromHtml(deductionColor);
                left.Text = Util.GetTextResource("CalculationDeductionsTitle");
                row.Cells.Add(left);


                ///7. Total Deduction 
                cell = new TableHeaderCell();
                cell.BackColor = ColorTranslator.FromHtml(deductionColor);
                cell.Style.Add("border-bottom", String.Format("1px solid {0}", deductionColor));
                row.Cells.Add(cell);

                ///8. Net Salary 
                cell = new TableHeaderCell();
                cell.BackColor = ColorTranslator.FromHtml(netColor);
                cell.Style.Add("border-bottom", String.Format("1px solid {0}", netColor));
                row.Cells.Add(cell);

                //9.  Bank AC No 
                cell = new TableHeaderCell();
                cell.BackColor = ColorTranslator.FromHtml(netColor);
                cell.Style.Add("border-bottom", String.Format("1px solid {0}", netColor));
                row.Cells.Add(cell);

                // Net Salary GBR
                if (CommonManager.CompanySetting.WhichCompany == WhichCompany.BritishSchool)
                {
                    cell = new TableHeaderCell();
                    cell.BackColor = ColorTranslator.FromHtml(netColor);
                    cell.Style.Add("border-bottom", String.Format("1px solid {0}", netColor));
                    row.Cells.Add(cell);
                }
                
                //for delete column
                //if (isDeleteColumnVisible)
                if (grid.Columns[grid.Columns.Count - 1].Visible)
                    AddCell(row, "#1C9AC8", true);

                Table t = grid.Controls[0] as Table;
                if (t != null)
                {
                    t.Rows.AddAt(0, row);
                }
                

            }
        }

        private static void AddSingleEmptyGroupColumn(GridViewRow row)
        {
            TableCell cell = new TableHeaderCell();
            cell.BackColor = ColorTranslator.FromHtml(incomeColor);
            cell.Style.Add("border-bottom", String.Format("1px solid {0}", incomeColor));
            row.Cells.Add(cell);
            
        }

        private static void AddGroupColumn(GridViewRow row, int count,string columnGroupText)
        {
            TableHeaderCell left = new TableHeaderCell();
            left.ColumnSpan = count;
            left.HorizontalAlign = HorizontalAlign.Center;
            left.BackColor = ColorTranslator.FromHtml(incomeColor);
            left.Text = columnGroupText;
            row.Cells.Add(left);
           
        }

       

        private static int GetSimilarColumnCount(GridView grid, GridViewRow headerRow, int columnStartIndex,IsRequiredColumn columnCheckingMethod)
        {
            int count = 0;
            for (; columnStartIndex < grid.Columns.Count; columnStartIndex++)
            {
                Label lbl = GetLabelInHeader(headerRow.Cells[columnStartIndex].Controls);
                if (lbl != null)
                {
                    CalculationColumnType columnType = (CalculationColumnType)Int32.Parse(lbl.Attributes["Type"]);
                    //if (CalculationValue.IsHPLUnitColumn(columnType))
                    if( columnCheckingMethod(columnType))
                        count += 1;
                    else
                    {
                        break;
                    }
                }
            }
            return count;
        }

        public static List<GetCalculationEmployeeListResult> GetCalculationEmployeeList(int payrollPeriodId, 
            string empName, int currentPage, int pageSize, ref int totalRecords,int BranchId,int addOnId)
        {
            CCalculation calculation = GetCalculation(payrollPeriodId);
            

            //var result = from c in PayrollDataContext.CCalculationEmployees
            //             join e in PayrollDataContext.EEmployees on c.EmployeeId equals e.EmployeeId
            //             join a in PayrollDataContext.EAddresses on e.EmployeeId equals a.EmployeeId
            //             where c.CalculationId==calculation.CalculationId
            //              && ( empName == string.Empty || e.Name.Contains( empName))
            //             select new CalculationEmployee
            //             {
            //                 EmployeeId = e.EmployeeId,
            //                 Name = e.Name,
            //                 HasSentInMail = c.HasSentInMail,
            //                 CalculationEmployeeId = c.CalculationEmployeeId,
            //                 Email = a.CIEmail
            //             };

            List<GetCalculationEmployeeListResult> list = PayrollDataContext.GetCalculationEmployeeList(calculation == null ? 0 : calculation.CalculationId,addOnId, empName, currentPage, pageSize, BranchId).ToList();
            if (list.Count() > 0)
                totalRecords = list[0].TotalRows.Value;
            else
                totalRecords = 0;
            return list;
        }

        public static void UpdateCalculationEmployeeForMail(int calculationEmployeeId)
        {
            CCalculationEmployee dbEntity = PayrollDataContext.CCalculationEmployees
                .Where(e => e.CalculationEmployeeId == calculationEmployeeId)
                .SingleOrDefault();

            if (dbEntity != null)
            {
                dbEntity.HasSentInMail = true;
            }

            UpdateChangeSet();
        }

        public static  void MakeReadonly(TextBox txt)
        {
            txt.ReadOnly = true;
            txt.CssClass = "calculationInput calculationInputDisabled";
            txt.Attributes.Remove("onblur");
        }

        public static List<DashboardSalaryDetailsOfLastSixMonthsResult> GetDashboardSalaryOfLastSixMonths()
        {
            return PayrollDataContext.DashboardSalaryDetailsOfLastSixMonths().ToList();
        }
    }
}
