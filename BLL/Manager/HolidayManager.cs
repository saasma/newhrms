﻿using System;
using System.Collections.Generic;
using System.Linq;
using DAL;
using Utils.Calendar;

namespace BLL.Manager
{
    public class HolidayManager : BaseBiz
    {
        public void Save(HolidayList entity)
        {
            entity.Created = GetCurrentDateAndTime();
            PayrollDataContext.HolidayLists.InsertOnSubmit(entity);
            PayrollDataContext.SubmitChanges();
        }
        public List<GetHolidayListResult> GetHolidayList()
        {
            return PayrollDataContext.GetHolidayList(SessionManager.CurrentCompanyId,(int)AttendaceColumnType.Holiday).ToList();
        }

        
        
        

        




        public bool Exists(string date, string description)
        {
          HolidayList hList =  PayrollDataContext.HolidayLists.SingleOrDefault(h => h.Date.ToLower() == date.ToLower() && h.Description.ToLower() == description.Trim().ToLower());
          if (hList == null)
              return false;
          else
              return true;
        }

        public static  string GetRemarkText(int applyTo)
        {
            if (applyTo == -1)
                return "All";
            else if (applyTo == 0)
                return "Female Only";


            

            return CommonManager.GetCastesById(applyTo).CasteName;

        }

        public bool DeleteWeeklyHoliday(int weeklyHolidayId)
        {
            //AttedenceDetail attendence = PayrollDataContext.AttedenceDetails.FirstOrDefault
            //    (e => (e.Type == 2 && e.TypeValue == weeklyHolidayId));


            //if (attendence != null)
            //    return false;

            WeeklyHoliday dbEntity = PayrollDataContext.WeeklyHolidays
                .SingleOrDefault(e => e.ID == weeklyHolidayId);
            if (dbEntity == null)
                return false;
            PayrollDataContext.WeeklyHolidays.DeleteOnSubmit(dbEntity);
            PayrollDataContext.SubmitChanges();
            return true;

        }


        public bool DeleteHoliday(int holidayId)
        {

           

            //if (attendence != null)
            //    return false;

            HolidayList dbEntity = PayrollDataContext.HolidayLists
               .SingleOrDefault(e => e.ID == holidayId);
            if (dbEntity == null)
                return false;


            List<DAL.AttedenceDetail> list = PayrollDataContext.AttedenceDetails.Where
               (e => (e.Type == (int)AttendenceCellType.Holiday 
                   && e.DateEng.Value == dbEntity.EngDate.Value)).ToList();

            foreach (var item in list)
            {
                item.DayValue = "P";
                item.Type = 0;
                item.TypeValue = 0;
            }


            PayrollDataContext.ChangeSettings.InsertOnSubmit(
                  GetSettingChangeLog((byte)MonetoryChangeLogTypeEnum.Holiday,"Holiday on " + dbEntity.Date + "(" + 
                  dbEntity.EngDate.Value.ToString("yyyy-MM-dd").ToString() + ") " +  dbEntity.Description + " deleted.", "", "", LogActionEnum.Delete));



           
            PayrollDataContext.HolidayLists.DeleteOnSubmit(dbEntity);
            PayrollDataContext.SubmitChanges();
            return true;

        }

        public bool IsHolidayExists(string date, string description)
        {
            HolidayList hList = PayrollDataContext.HolidayLists.SingleOrDefault(h => h.Date.ToLower() == date.ToLower() && h.Description.ToLower() == description.Trim().ToLower());
            if (hList == null)
                return false;
            else
                return true;
        }

        public void Save(WeeklyHoliday entity)
        {
            entity.Created = DateTime.Now;
            PayrollDataContext.WeeklyHolidays.InsertOnSubmit(entity);
            PayrollDataContext.SubmitChanges();
        }
        public bool IsWeeklyHolidayExists(int payrollPeriod, int weekDay)
        {

            return PayrollDataContext.IsWeeklyHolidayExists(SessionManager.CurrentCompanyId, weekDay) == 1;

        }
        public List<GetWeeklyHolidayResult> GetWeeklyHoliday()
        {
            return PayrollDataContext.GetWeeklyHoliday(SessionManager.CurrentCompanyId,(int) AttendaceColumnType.WeeklyHoliday).ToList();
        }

        public List<GetHolidaysForAttendenceResult> GetMonthsHolidaysForAttendence(int month, int year, int companyId, int payrollPeriodId, DateTime? startDateEng, DateTime? endDateEng, int? employeeId)
        {


            //holiday list
            List<GetHolidaysForAttendenceResult> holidays = GetMonthsHolidaysForAttendence(month, year, companyId, payrollPeriodId, startDateEng, endDateEng);

            if (employeeId == null)
                employeeId = 0;

            if (employeeId == 0 || employeeId ==-1)
                return holidays;

            List<GetHolidaysForAttendenceResult> newHolidays = new List<GetHolidaysForAttendenceResult>();

            foreach (GetHolidaysForAttendenceResult branchHoliday in holidays)
            {
                // exlucde non weekly female for male
                if ((branchHoliday.IsWeekly == null || branchHoliday.IsWeekly == false) && branchHoliday.Type == 0 && employeeId != 0)
                {
                    if (PayrollDataContext.EEmployees.Any(x => x.EmployeeId == employeeId && x.Gender == 0))
                        newHolidays.Add(branchHoliday);

                }
                    // check for Holiday group or old caste type and remove if not eligible
                else if (Convert.ToBoolean(branchHoliday.IsWeekly) == false && branchHoliday.Type >= 1 && employeeId != 0)
                {
                    int? holidayGroup = PayrollDataContext.HHumanResources.FirstOrDefault(x => x.EmployeeId == employeeId).HolidayGroupId;
                    if (holidayGroup == branchHoliday.Type)
                        newHolidays.Add(branchHoliday);

                }
                else if (branchHoliday.DateEng != null && !string.IsNullOrEmpty(branchHoliday.BranchIDList))
                {
                    int branchId = BranchManager.GetEmployeeCurrentBranch(employeeId.Value, branchHoliday.DateEng.Value);
                    //if (branchId != branchHoliday.BranchId)
                    if (branchHoliday.BranchList.Contains(branchId))
                        newHolidays.Add(branchHoliday);
                }
                else
                    newHolidays.Add(branchHoliday);



            }



            //foreach (GetHolidaysForAttendenceResult h in holidays)
            //{

            //    if (h.Type == -1)
            //        h.Abbreviation = HolidaysConstant.National_Holiday;
            //    else if (h.Type == 0)
            //        h.Abbreviation = HolidaysConstant.Female_Holiday;
            //    else
            //        h.Abbreviation = HolidaysConstant.Caste_Holiday;
            //}

            return newHolidays;
        }
        // should not access from outside as no emp id
        private List<GetHolidaysForAttendenceResult> GetMonthsHolidaysForAttendence(int month, int year, int companyId, int payrollPeriodId,DateTime? startDateEng,DateTime? endDateEng)
        {
            //holiday list
            List<GetHolidaysForAttendenceResult> holidays = PayrollDataContext.GetHolidaysForAttendence(month, year, companyId, payrollPeriodId,startDateEng,endDateEng).ToList();
            foreach (GetHolidaysForAttendenceResult h in holidays)
            {
                if (h.Type == -1)
                    h.Abbreviation = HolidaysConstant.National_Holiday;
                else if (h.Type == 0)
                    h.Abbreviation = HolidaysConstant.Female_Holiday;
                else
                    h.Abbreviation = HolidaysConstant.Caste_Holiday;
            }


            //for attendance page
            if (payrollPeriodId != 0 && startDateEng==null)
            {

                PayrollPeriod payrollPeriod = CommonManager.GetPayrollPeriod(payrollPeriodId);

                //now get & prepare for weekly holidays as list only contains for each week day so prepare for whole months
                List<GetWeeklyHolidaysForAttendenceResult> weeklyHolidays = PayrollDataContext.GetWeeklyHolidaysForAttendence(month, year, companyId, startDateEng, endDateEng).ToList();
                foreach (GetWeeklyHolidaysForAttendenceResult weeklyHoliday in weeklyHolidays)
                {
                    //get first date
                    DateTime date = new DateTime(year, month, 1);
                    IEnumerable<DateTime> weekDates = GetWeeks(month, year, (DayOfWeek)weeklyHoliday.DayOfWeek, payrollPeriod);
                    foreach (DateTime weekDate in weekDates)
                    {
                        GetHolidaysForAttendenceResult newHoliday = new GetHolidaysForAttendenceResult();
                        newHoliday.Id = weeklyHoliday.Id;
                        newHoliday.Name = "Weekly";//simply name
                        newHoliday.IsWeekly = weeklyHoliday.IsWeekly;
                        newHoliday.Type = weeklyHoliday.Type.Value;
                        newHoliday.BranchIDList = "";

                        //if english
                        if (IsEnglish)
                            newHoliday.Day = weekDate.Day;
                        //for nepali date,convert the date to nepali
                        else
                        {
                            CustomDate dateEng = new CustomDate(weekDate.Day, weekDate.Month, weekDate.Year, true);
                            newHoliday.Day = CustomDate.ConvertEngToNep(dateEng).Day;
                        }

                        // Remove the National holiday before adding the weekly holiday
                        //holidays.RemoveAll(x => x.Day == newHoliday.Day);

                        newHoliday.IsNationalHoliday = weeklyHoliday.IsNationalHoliday;
                       
                        // full day
                        if(weeklyHoliday.Type==1)
                            newHoliday.Abbreviation = HolidaysConstant.Weekly_Holiday;
                        else
                            newHoliday.Abbreviation = HolidaysConstant.HalfWeekly_Holiday;
                        holidays.Add(newHoliday);
                    }
                }
            }
            //for Leave Request page
            else
            {
                //now get & prepare for weekly holidays as list only contains for each week day so prepare for whole months
                List<GetWeeklyHolidaysForAttendenceResult> weeklyHolidays = PayrollDataContext.GetWeeklyHolidaysForAttendence(month, year, companyId, startDateEng, endDateEng).ToList();
                //iterate each date & find out if it is weekly holiday or not
                for (DateTime date = startDateEng.Value; date <= endDateEng; date = date.AddDays(1))
                {
                    foreach (GetWeeklyHolidaysForAttendenceResult weekHoliday in weeklyHolidays)
                    {
                        if ((int)date.DayOfWeek == weekHoliday.DayOfWeek)
                        {
                            GetHolidaysForAttendenceResult newHoliday = new GetHolidaysForAttendenceResult();
                            newHoliday.Id = weekHoliday.Id;
                            newHoliday.Name = "Weekly";//simply name
                            newHoliday.IsWeekly = weekHoliday.IsWeekly;
                            newHoliday.Type = weekHoliday.Type.Value;
                            newHoliday.BranchIDList = "";
                            newHoliday.DateEng = date;

                            ////if english
                            //if (IsEnglish)
                            //    newHoliday.Day = weekDate.Day;
                            ////for nepali date,convert the date to nepali
                            //else
                            //{
                            //    CustomDate dateEng = new CustomDate(weekDate.Day, weekDate.Month, weekDate.Year, true);
                            //    newHoliday.Day = CustomDate.ConvertEngToNep(dateEng).Day;
                            //}

                            // Remove the National holiday before adding the weekly holiday
                            //holidays.RemoveAll(x => x.DateEng == newHoliday.DateEng);

                            newHoliday.IsNationalHoliday = weekHoliday.IsNationalHoliday;
                            // full day
                            if (weekHoliday.Type == 1)
                                newHoliday.Abbreviation = HolidaysConstant.Weekly_Holiday;
                            else
                                newHoliday.Abbreviation = HolidaysConstant.HalfWeekly_Holiday;
                            holidays.Add(newHoliday);
                        }
                    }
                }


            }

            return holidays;
        }

        private IEnumerable<DateTime> GetWeeks(int month,int year, DayOfWeek startDay,PayrollPeriod payrollPeriod)
        {
            var list = new List<DateTime>();
            //DateTime first = new DateTime(year, month, 1);
            DateTime first = payrollPeriod.StartDateEng.Value;

            for (DateTime i = first; i <= payrollPeriod.EndDateEng.Value; i = i.AddDays(1))
            {
                if (i.DayOfWeek == startDay)
                    list.Add(i);
            }

            return list;
        }

        public  static  bool IsWeeklyHoliday(int day, List<GetHolidaysForAttendenceResult> holidays)
        {
            foreach (var holiday in holidays)
            {
                if (holiday.IsWeekly.Value && holiday.Day.Value == day)
                    return true;
            }
            return false;
        }

        public static FinancialDate GetFinancialDateById(int financialDateId)
        {
            return PayrollDataContext.FinancialDates.SingleOrDefault(x => x.FinancialDateId == financialDateId);
        }

        public static WeeklyHoliday GetWeeklyHolidayById(int id)
        {
            return PayrollDataContext.WeeklyHolidays.SingleOrDefault(x => x.ID == id);
        }

        public static Status SaveUpdateWeeklyHoliday(WeeklyHoliday entity)
        {
            Status status = new Status();

            HolidayManager holidayMgr = new HolidayManager();

            if (entity.ID.Equals(0))
            {
                if (holidayMgr.IsWeeklyHolidayExists(entity.EffectiveFrom.Value, entity.DayOfWeek))
                {
                    status.ErrorMessage = "Weekly holiday for this day of week already exists.";
                    status.IsSuccess = false;
                    return status;
                }

                entity.Created = DateTime.Now;
                PayrollDataContext.WeeklyHolidays.InsertOnSubmit(entity);
            }
            else
            {
                WeeklyHoliday dbObj = GetWeeklyHolidayById(entity.ID);

                WeeklyHoliday objDb = PayrollDataContext.WeeklyHolidays.SingleOrDefault(x => x.EffectiveFrom == entity.EffectiveFrom && x.DayOfWeek == entity.DayOfWeek);
                if (objDb != null && objDb.ID != dbObj.ID)
                {
                    status.ErrorMessage = "Weekly holiday for this day of week already exists.";
                    status.IsSuccess = false;
                    return status;
                }

                if (dbObj == null)
                {
                    status.ErrorMessage = "Weekly holiday not found.";
                    status.IsSuccess = false;
                    return status;
                }

                dbObj.DayOfWeek = entity.DayOfWeek;
                dbObj.IsFullDay = entity.IsFullDay;
                dbObj.EffectiveFrom = entity.EffectiveFrom;
                dbObj.ModifiedBy = SessionManager.CurrentLoggedInUserID;
                dbObj.ModifiedOn = DateTime.Now;
            }

            PayrollDataContext.SubmitChanges();
            status.IsSuccess = true;
            return status;
        }

        public static List<GetAnnualHolidayListResult> GetAnnualHolidayList(int currentPate, int pageSize, DateTime? startDate, DateTime? endDate)
        {
            return PayrollDataContext.GetAnnualHolidayList(SessionManager.CurrentCompanyId, (int)AttendaceColumnType.Holiday, startDate, endDate, currentPate, pageSize).ToList();
        }

        public static Status SaveAnnualHolidays(List<HolidayList> list)
        {
            Status status = new Status();
            PayrollDataContext.HolidayLists.InsertAllOnSubmit(list);
            PayrollDataContext.SubmitChanges();
            status.IsSuccess = true;
            return status;
        }

        public static HolidayList GetAnnualHolidayById(int id)
        {
            return PayrollDataContext.HolidayLists.SingleOrDefault(x => x.ID == id);
        }

        public static Status UpdateAnnualHoliday(HolidayList obj)
        {
            Status status = new Status();

            HolidayList dbObj = GetAnnualHolidayById(obj.ID);
            if (dbObj == null)
            {
                status.ErrorMessage = "Annual Holiday is not found.";
                status.IsSuccess =false;
                return status;
            }

            HolidayList dbAnnulaHoliday = PayrollDataContext.HolidayLists.SingleOrDefault(x => x.EngDate.Value.Date == obj.EngDate.Value.Date && x.Description == obj.Description);
            if (dbAnnulaHoliday != null && obj.ID != dbAnnulaHoliday.ID)
            {
                status.ErrorMessage = string.Format("Holiday already exists for the date {0}", obj.Date);
                status.IsSuccess = false;
                return status;
            }
            
            dbObj.Date = obj.Date;
            dbObj.EngDate = obj.EngDate;
            dbObj.IsNationalHoliday = obj.IsNationalHoliday;
            dbObj.Description = obj.Description;
            dbObj.AppliesTo = obj.AppliesTo;
            dbObj.BranchIDList = obj.BranchIDList;
            dbObj.ModifiedBy = SessionManager.CurrentLoggedInUserID;
            dbObj.ModifiedOn = GetCurrentDateAndTime();
            PayrollDataContext.SubmitChanges();

            status.IsSuccess = true;
            return status;
        }
    }
}
