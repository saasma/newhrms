﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using DAL;
using Utils;
using System.Xml.Linq;
using Utils.Calendar;
using BLL.Entity;
using System.Web;
using System.IO;
using BLL.BO;

namespace BLL.Manager
{
    public class SalaryManager : BaseBiz
    {

        public static bool IsValidSalaryChangeGradeFittingForDate(DateTime date)
        {
            if (PayrollDataContext.BLevelDates.Any(x => x.EffectiveFromEng == date))
            {
                return true;
            }
            return false;
        }

        public static LevelGradeChange GetUnapprovedPromotion(bool lastApprovedAlso)
        {
            //int unapprovedPromotion = (int)LevelGradeChangeStatusType.Saved;
            //if (PayrollDataContext.LevelGradeChangeDetails.Any(x =>x.Type == (int)LevelGradeChangeType.Promotion))
            //{
            //    LevelGradeChangeDetail detail =
            //        PayrollDataContext.LevelGradeChangeDetails.FirstOrDefault(x => x.Type == (int)LevelGradeChangeType.Promotion);
            //    return detail.LevelGradeChange;
            //}

            int unapprovedPromotion = (int)LevelGradeChangeStatusType.Saved;
            if (PayrollDataContext.LevelGradeChangeDetails.Any(x => x.Status == unapprovedPromotion
                && x.Type == (int)LevelGradeChangeType.Promotion))
            {
                LevelGradeChangeDetail detail =
                    PayrollDataContext.LevelGradeChangeDetails.FirstOrDefault(x => x.Status == unapprovedPromotion && x.Type == (int)LevelGradeChangeType.Promotion);
                return detail.LevelGradeChange;
            }

            if (lastApprovedAlso)
            {
                LevelGradeChangeDetail detail =
                    PayrollDataContext.LevelGradeChangeDetails.OrderByDescending(x=>x.FromDateEng).FirstOrDefault(x => x.Status == (int)LevelGradeChangeStatusType.Approved
                        && x.Type == (int)LevelGradeChangeType.Promotion);
                if (detail != null)
                    return detail.LevelGradeChange;
            }

            return null;
        }

        public static LevelGradeChange GetUnapprovedGradeFitting()
        {
            //int unapprovedPromotion = (int)LevelGradeChangeStatusType.Saved;
            //if (PayrollDataContext.LevelGradeChangeDetails.Any(x => x.Type == (int)LevelGradeChangeType.AnnualGradeFitting))
            //{
            //    LevelGradeChangeDetail detail =
            //        PayrollDataContext.LevelGradeChangeDetails.FirstOrDefault(x => x.Type == (int)LevelGradeChangeType.AnnualGradeFitting);
            //    return detail.LevelGradeChange;
            //}
            int unapprovedPromotion = (int)LevelGradeChangeStatusType.Saved;
            if (PayrollDataContext.LevelGradeChangeDetails.Any(x => x.Status == unapprovedPromotion && x.Type == (int)LevelGradeChangeType.AnnualGradeFitting))
            {
                LevelGradeChangeDetail detail =
                    PayrollDataContext.LevelGradeChangeDetails.FirstOrDefault(x => x.Status == unapprovedPromotion && x.Type == (int)LevelGradeChangeType.AnnualGradeFitting);
                return detail.LevelGradeChange;
            }
            return null;
        }
        public static LevelGradeChange GetUnapprovedSalaryChange()
        {
            //int unapprovedPromotion = (int)LevelGradeChangeStatusType.Saved;
            //if (PayrollDataContext.LevelGradeChangeDetails.Any(x => x.Type == (int)LevelGradeChangeType.SalaryStructureChange))
            //{
            //    LevelGradeChangeDetail detail =
            //        PayrollDataContext.LevelGradeChangeDetails.FirstOrDefault(x =>x.Type == (int)LevelGradeChangeType.SalaryStructureChange);
            //    return detail.LevelGradeChange;
            //}
            //return null;
            int unapprovedPromotion = (int)LevelGradeChangeStatusType.Saved;
            if (PayrollDataContext.LevelGradeChangeDetails.Any(x => x.Status == unapprovedPromotion && x.Type == (int)LevelGradeChangeType.SalaryStructureChange))
            {
                LevelGradeChangeDetail detail =
                    PayrollDataContext.LevelGradeChangeDetails.FirstOrDefault(x => x.Status == unapprovedPromotion && x.Type == (int)LevelGradeChangeType.SalaryStructureChange);
                return detail.LevelGradeChange;
            }
            return null;
        }
        public static List<GetEmployeeListForAnnualGradeFittingResult> GetEmployeeListForAnnualGradeFitting(DateTime fromDate)
        {
            List<GetEmployeeListForAnnualGradeFittingResult> list
                = PayrollDataContext.GetEmployeeListForAnnualGradeFitting(fromDate).ToList();
            return list;

        }
        public static List<GetEmployeeListForSalaryChangeGradeFittingResult> GetEmployeeListForSalaryChangeGradeFitting
            (DateTime fromDate)
        {
            List<GetEmployeeListForSalaryChangeGradeFittingResult> list
                = PayrollDataContext.GetEmployeeListForSalaryChangeGradeFitting(fromDate).ToList();

            foreach (GetEmployeeListForSalaryChangeGradeFittingResult item in list)
            {
                item.StepGrade = NewPayrollManager.GetNewGrade(
                    item.PrevLevelId.Value, item.PrevGrade.Value, item.PrevLevelId.Value, 
                    fromDate, LevelGradeChangeType.SalaryStructureChange);
            }

            return list;

        }

        public static List<LevelGradeChange> GetPromotionNames()
        {

            List<LevelGradeChange> list = PayrollDataContext.LevelGradeChanges.OrderByDescending(x => x.CreatedOn.Value).ToList();

            foreach(var item in list)
            {
                if(item.ApprovedPayrollPeriodId != null)
                {
                    PayrollPeriod period = CommonManager.GetPayrollPeriod(item.ApprovedPayrollPeriodId.Value);
                    if (period != null)
                        item.Name += " (" + DateHelper.GetMonthShortName(period.Month, IsEnglish) + " " + period.Year + ")";
                }
            }

            return list;

        }

        public static Status ApprovePromotionSalaryChangeStatus(LevelGradeChange promotion,LevelGradeChangeStatusType levelStatus)
        {
            Status status = new Status();
            LevelGradeChange dbPromotion = null;
            if (promotion.Type == (int)LevelGradeChangeType.Promotion)
                dbPromotion = GetUnapprovedPromotion(false);
            else
                dbPromotion = GetUnapprovedSalaryChange();

            EmployeeManager mgr = new EmployeeManager();
            PayrollPeriod lastSavedPeriod = CommonManager.GetLastFinalSalaryPayrollPeriod();
            PayManager payMgr = new PayManager();

            PIncome basicIncome = payMgr.GetBasicIncome(SessionManager.CurrentCompanyId);
            PayrollPeriod lastPeriod = CommonManager.GetLastPayrollPeriod();

            SetConnectionPwd(PayrollDataContext.Connection);
            PayrollDataContext.Transaction = PayrollDataContext.Connection.BeginTransaction();

            try
            {
                if (dbPromotion != null)
                {
                    if (levelStatus == LevelGradeChangeStatusType.Approved)
                    {
                        dbPromotion.ApprovedBy = SessionManager.User.UserID;
                        dbPromotion.ApprovedOn = GetCurrentDateAndTime();
                        if (lastPeriod != null)
                            dbPromotion.ApprovedPayrollPeriodId = lastPeriod.PayrollPeriodId;
                    }

                    foreach (LevelGradeChangeDetail emp in promotion.LevelGradeChangeDetails)
                    {
                        LevelGradeChangeDetail dbEmp = dbPromotion.LevelGradeChangeDetails.FirstOrDefault
                            (x => x.EmployeeId == emp.EmployeeId);

                        if (dbEmp == null)
                        {
                            status.ErrorMessage = string.Format("Employee \"{0}\" should be saved first for approval.",
                                mgr.GetById(emp.EmployeeId.Value).Name);
                            return status;
                        }
                        // only change when in Saved status
                        if ((LevelGradeChangeStatusType)dbEmp.Status == LevelGradeChangeStatusType.Saved)
                        {
                            dbEmp.DesignationId = emp.DesignationId;
                            dbEmp.LevelId = emp.LevelId;
                            dbEmp.StepGrade = emp.StepGrade;
                            dbEmp.FromDate = emp.FromDate;
                            dbEmp.FromDateEng = emp.FromDateEng;
                            dbEmp.Status = emp.Status;

                            status.Count += 1;


                            // Approve Promotion Processing
                            if (levelStatus == LevelGradeChangeStatusType.Approved)
                            {
                                PEmployeeIncome empIncome = payMgr.GetEmployeeIncome(emp.EmployeeId.Value, basicIncome.IncomeId);
                                bool isStatusBeingChangedFromContractToPermanent = empIncome.LevelId == null;
                                empIncome.IsValid = true;
                                empIncome.IsEnabled = true;

                                PEmployeeIncrement empIncrement = new PEmployeeIncrement();
                                empIncrement.EffectiveFromDate = dbEmp.FromDate;
                                empIncrement.EffectiveFromDateEng = dbEmp.FromDateEng.Value;
                                empIncrement.EmployeeIncomeId = empIncome.EmployeeIncomeId;
                                empIncrement.IsReterospect = false;
                                empIncrement.Type = promotion.Type;
                                empIncrement.SourceId = dbEmp.DetailId;
                                empIncrement.OldLevelId = dbEmp.PrevLevelId;
                                empIncrement.OldStepGrade = dbEmp.PrevStepGrade;
                                empIncrement.NewLevelId = dbEmp.LevelId;
                                empIncrement.NewStepGrade = dbEmp.StepGrade;

                                // Emp Income update                           
                                empIncome.LevelId = dbEmp.LevelId;
                                empIncome.Step = dbEmp.StepGrade;
                                    

                                if (lastSavedPeriod != null && empIncrement.EffectiveFromDateEng <= lastSavedPeriod.EndDateEng)
                                {
                                    empIncrement.IsReterospect = true;

                                    // if the Promotion Change is back dated then we need to save first Entry from Effective date using Level/Grade
                                    // in that month & increment for each successive period when Level/Grade has been changed
                                    List<PEmployeeIncrement> levelGradeChangeAfterEffectiveDateList
                                        = PayrollDataContext.PEmployeeIncrements.Where(x => x.EmployeeIncomeId == empIncrement.EmployeeIncomeId
                                            && x.EffectiveFromDateEng > empIncrement.EffectiveFromDateEng)
                                            .OrderBy(x => x.EffectiveFromDateEng).ThenBy(x => x.Id).ToList();

                                    // if has multiple increment on same date then remove the previous
                                    levelGradeChangeAfterEffectiveDateList = RemoveDuplicateOnSameDate(levelGradeChangeAfterEffectiveDateList);

                                    if (levelGradeChangeAfterEffectiveDateList.Count > 0)
                                    {
                                        PEmployeeIncrement firstIncrement = new PEmployeeIncrement();
                                        CopyObject<PEmployeeIncrement>(empIncrement, ref firstIncrement);

                                        empIncrement.EmployeeIncomeId = empIncome.EmployeeIncomeId;
                                        empIncrement.OldLevelId = NewHRManager.GetEmployeeLevelUsingDate(emp.EmployeeId.Value, empIncrement.EffectiveFromDateEng).LevelId;
                                        empIncrement.OldStepGrade = NewHRManager.GetEmployeeGradeStepUsingDate(emp.EmployeeId.Value, empIncrement.EffectiveFromDateEng);
                                        if (promotion.Type == (int)LevelGradeChangeType.SalaryStructureChange)
                                            empIncrement.NewLevelId = empIncrement.OldLevelId;
                                        else
                                            empIncrement.NewLevelId = emp.LevelId.Value;

                                        empIncrement.NewStepGrade = NewPayrollManager.GetNewGrade(
                                            empIncrement.OldLevelId.Value, empIncrement.OldStepGrade.Value, empIncrement.NewLevelId.Value,
                                            empIncrement.EffectiveFromDateEng, (LevelGradeChangeType)promotion.Type);
                                        //empIncrement.OldLevelId = emp.LevelId;// assign new level

                                        //int newGrade = empIncrement.OldStepGrade.Value;
                                        empIncrement.PayrollPeriodId = CommonManager.GetLastPayrollPeriod().PayrollPeriodId;
                                        PayrollDataContext.PEmployeeIncrements.InsertOnSubmit(empIncrement);

                                        //int newLevel = empIncrement.NewLevelId.Value;
                                        //int newGrade = empIncrement.NewStepGrade.Value;

                                        for (int i = 0; i < levelGradeChangeAfterEffectiveDateList.Count; i++)
                                        {
                                            PEmployeeIncrement item = levelGradeChangeAfterEffectiveDateList[i];

                                            PEmployeeIncrement increment = new PEmployeeIncrement();
                                            CopyObject<PEmployeeIncrement>(item, ref increment);

                                            increment.IsReterospect = true;
                                            increment.EmployeeIncomeId = empIncome.EmployeeIncomeId;
                                            increment.EffectiveFromDate = item.EffectiveFromDate;
                                            increment.EffectiveFromDateEng = item.EffectiveFromDateEng;
                                            increment.OldLevelId = item.NewLevelId;//NewHRManager.GetEmployeeLevelUsingDate(emp.EmployeeId.Value, increment.EffectiveFromDateEng).LevelId;
                                            increment.OldStepGrade = item.NewStepGrade;//NewHRManager.GetEmployeeGradeStepUsingDate(emp.EmployeeId.Value, increment.EffectiveFromDateEng);
                                            //increment.OldLevelId = item.NewLevelId;
                                            //increment.OldStepGrade = item.NewStepGrade;
                                            if (promotion.Type == (int)LevelGradeChangeType.SalaryStructureChange)
                                            {
                                                //if (increment.EffectiveFromDateEng >= empIncrement.EffectiveFromDateEng)
                                                //    increment.NewLevelId = emp.LevelId.Value;//newLevel;
                                                //else
                                                increment.NewLevelId = increment.OldLevelId;
                                            }
                                            else
                                                increment.NewLevelId = emp.LevelId.Value;
                                            //increment.NewLevelId = emp.LevelId; // Assign New Level
                                            //increment.OldStepGrade = empIncrement.OldStepGrade;
                                            //if (increment.EffectiveFromDateEng >= empIncrement.EffectiveFromDateEng)
                                            //    increment.NewStepGrade = emp.StepGrade;                                                
                                            //else
                                                increment.NewStepGrade = NewPayrollManager.GetNewGrade(increment.OldLevelId.Value, increment.OldStepGrade.Value, increment.NewLevelId.Value,
                                                     increment.EffectiveFromDateEng, (LevelGradeChangeType)promotion.Type);

                                            //newLevel = increment.NewLevelId.Value;
                                            //newGrade = increment.NewStepGrade.Value;
                                            // if last then current Grade will be set
                                            //if (i == levelGradeChangeAfterEffectiveDateList.Count - 1 && increment.OldLevelId == empIncome.LevelId)
                                            //    increment.OldStepGrade = empIncome.Step;

                                            //newGrade = increment.OldStepGrade.Value;
                                                increment.PayrollPeriodId = CommonManager.GetLastPayrollPeriod().PayrollPeriodId;
                                            PayrollDataContext.PEmployeeIncrements.InsertOnSubmit(increment);
                                        }
                                    }
                                    else
                                    {
                                        // If not Level/Grade change has occured after Effective date
                                        empIncrement.PayrollPeriodId = CommonManager.GetLastPayrollPeriod().PayrollPeriodId;
                                        PayrollDataContext.PEmployeeIncrements.InsertOnSubmit(empIncrement);
                                    }

                                }
                                else
                                {
                                    empIncrement.PayrollPeriodId = CommonManager.GetLastPayrollPeriod().PayrollPeriodId;
                                    PayrollDataContext.PEmployeeIncrements.InsertOnSubmit(empIncrement);
                                }


                                #region Case : When Employee status is being changed from Contract (no level/grade) to Permanent (having level/grade)
                                // for calculating salary like basic set the followings
                                // AmountOld = Contract basic salary
                                // OldLevelId = null
                                // OldStepGrade = null
                                if (isStatusBeingChangedFromContractToPermanent)
                                {
                                    empIncrement.OldLevelId = null;
                                    empIncrement.OldStepGrade = null;
                                    empIncrement.AmountOld = empIncome.Amount;
                                }
                                #endregion

                                if (promotion.Type == (int)LevelGradeChangeType.Promotion)
                                {
                                    #region "New Post/Designation Save history"
                                    // Update for Employee Post/Designation after Promotion
                                    EEmployee dbEmployee = mgr.GetById(emp.EmployeeId.Value);

                                    // Designation History
                                    if (!PayrollDataContext.DesignationHistories.Any(x => x.EmployeeId == emp.EmployeeId.Value))
                                    {
                                        DesignationHistory first = new DesignationHistory();
                                        first.EmployeeId = dbEmployee.EmployeeId;
                                        first.FromDesignationId = dbEmployee.DesignationId;
                                        first.DesignationId = dbEmployee.DesignationId;
                                        ECurrentStatus firstStatus = EmployeeManager.GetFirstStatus(dbEmp.EmployeeId.Value);
                                        first.FromDate = firstStatus.FromDate;
                                        first.FromDateEng = firstStatus.FromDateEng;
                                        first.IsFirst = true;
                                        PayrollDataContext.DesignationHistories.InsertOnSubmit(first);
                                    }

                                    // Save new designation History
                                    dbEmployee.DesignationId = dbEmp.DesignationId;

                                    DesignationHistory history = new DesignationHistory();
                                    history.FromDesignationId = emp.PrevDesignationId;
                                    history.DesignationId = dbEmployee.DesignationId;
                                    history.FromDate = dbEmp.FromDate;
                                    history.FromDateEng = dbEmp.FromDateEng;
                                    history.EmployeeId = dbEmp.EmployeeId.Value;
                                    history.IsFirst = false;
                                    PayrollDataContext.DesignationHistories.InsertOnSubmit(history);

                                    #endregion
                                }
                            }

                        }



                    }
                    PayrollDataContext.SubmitChanges();
                    PayrollDataContext.Transaction.Commit();
                }
            }
            catch (Exception exp)
            {
                //Log.log("Error while approving promotion/salary change :" + exp);
                PayrollDataContext.Transaction.Rollback();
                status.ErrorMessage = exp.Message;
                return status;
            }
            finally
            {

                //if (PayrollDataContext.Connection != null)
                //{
                //    PayrollDataContext.Connection.Close();
                //}
            }

            return status;
        }

        public static List<PEmployeeIncrement> RemoveDuplicateOnSameDate(List<PEmployeeIncrement> list)
        {
            List<PEmployeeIncrement> removalList = new List<PEmployeeIncrement>();

            for (int i = 0; i < list.Count; i++)
            {
                if (i < list.Count - 1)
                {
                    if (list[i].EffectiveFromDate.Equals(list[i + 1].EffectiveFromDate))
                    {
                        removalList.Add(list[i]);
                    }
                }
            }


            for (int i = list.Count - 1; i >= 0; i--)
            {
                if (removalList.Any(x => x.Id == list[i].Id))
                {
                    list.RemoveAt(i);
                }
            }
            return list;
        }

        public static Status ChangeGradeFittingStatus(LevelGradeChange gradeFitting, LevelGradeChangeStatusType gradeFittingStatus)
        {
            Status status = new Status();
            LevelGradeChange dbGradeFitting = null;

            if (gradeFitting.Type == (int)LevelGradeChangeType.AnnualGradeFitting)
                dbGradeFitting = GetUnapprovedGradeFitting();
            else
                dbGradeFitting = GetUnapprovedSalaryChange();

            EmployeeManager mgr = new EmployeeManager();
            PayrollPeriod lastSavedPeriod = CommonManager.GetLastFinalSalaryPayrollPeriod();
            PayManager payMgr = new PayManager();

            PIncome basicIncome = payMgr.GetBasicIncome(SessionManager.CurrentCompanyId);

            if (dbGradeFitting != null)
            {
                if (gradeFittingStatus == LevelGradeChangeStatusType.Approved)
                {
                    dbGradeFitting.ApprovedBy = SessionManager.User.UserID;
                dbGradeFitting.ApprovedOn = GetCurrentDateAndTime();
                }

                foreach (LevelGradeChangeDetail emp in gradeFitting.LevelGradeChangeDetails)
                {
                    LevelGradeChangeDetail dbEmp = dbGradeFitting.LevelGradeChangeDetails.FirstOrDefault
                        (x => x.EmployeeId == emp.EmployeeId);

                    if (dbEmp == null)
                    {
                        status.ErrorMessage = string.Format("Employee \"{0}\" can not be inserted in approval/rejection mode.",
                            mgr.GetById(emp.EmployeeId.Value).Name);
                        return status;
                    }
                    // only change when in Saved status
                    if ((LevelGradeChangeStatusType)dbEmp.Status == LevelGradeChangeStatusType.Saved)
                    {
                       
                        dbEmp.StepGrade = emp.StepGrade;
                        dbEmp.FromDate = emp.FromDate;
                        dbEmp.FromDateEng = emp.FromDateEng;

                        dbEmp.Status = emp.Status;

                        status.Count += 1;


                        // Approve Promotion Processing
                        if (gradeFittingStatus == LevelGradeChangeStatusType.Approved)
                        {
                            PEmployeeIncome empIncome = payMgr.GetEmployeeIncome(emp.EmployeeId.Value, basicIncome.IncomeId);
                            empIncome.IsValid = true;
                            empIncome.IsEnabled = true;

                            PEmployeeIncrement empIncrement = new PEmployeeIncrement();
                            empIncrement.EffectiveFromDate = dbEmp.FromDate;
                            empIncrement.EffectiveFromDateEng = dbEmp.FromDateEng.Value;
                            empIncrement.EmployeeIncomeId = empIncome.EmployeeIncomeId;
                            empIncrement.IsReterospect = false;
                            if (lastSavedPeriod != null && empIncrement.EffectiveFromDateEng <= lastSavedPeriod.EndDateEng)
                            {
                                empIncrement.IsReterospect = true;
                            }
                            empIncrement.Type = dbEmp.Type;
                            empIncrement.SourceId = dbEmp.DetailId;

                            empIncrement.OldLevelId = dbEmp.PrevLevelId;
                            empIncrement.OldStepGrade = dbEmp.PrevStepGrade;
                            empIncrement.NewLevelId = dbEmp.LevelId;
                            empIncrement.NewStepGrade = dbEmp.StepGrade;

                            // Emp Income update                           
                            empIncome.LevelId = dbEmp.LevelId;
                            empIncome.Step = dbEmp.StepGrade;


                            empIncrement.PayrollPeriodId = CommonManager.GetLastPayrollPeriod().PayrollPeriodId;
                            PayrollDataContext.PEmployeeIncrements.InsertOnSubmit(empIncrement);
                        }

                    }



                }
                PayrollDataContext.SubmitChanges();
            }

            return status;
        }

        public static Status DeleteSavedPromotion(int promotionDetailId)
        {
            LevelGradeChangeDetail detail = PayrollDataContext.LevelGradeChangeDetails
                .FirstOrDefault(x => x.DetailId == promotionDetailId);

            Status status = new Status();

            if (detail != null)
            {
                if (detail.Status == (int)LevelGradeChangeStatusType.Approved)
                {
                    status.ErrorMessage = "Approved promotion can not be deleted.";
                    return status;
                }
                else
                {
                    PayrollDataContext.LevelGradeChangeDetails.DeleteOnSubmit(detail);

                    PayrollDataContext.SubmitChanges();
                }
            }
            else
            {
                status.ErrorMessage = "Promotion not found.";
                return status;
            }

            return status;
        }

        public static Status InsertUpdatePromotion(LevelGradeChange promotion,ref bool isInsert)
        {

            Status status = new Status();
            promotion.Type = (int)LevelGradeChangeType.Promotion;


            LevelGradeChange dbPromotion = GetUnapprovedPromotion(false);

            if (dbPromotion == null)
            {
                isInsert = true;
                status.Count = promotion.LevelGradeChangeDetails.Count;

                promotion.CreatedBy = SessionManager.User.UserID;
                promotion.CreatedOn = GetCurrentDateAndTime();

                PayrollDataContext.LevelGradeChanges.InsertOnSubmit(promotion);
                PayrollDataContext.SubmitChanges();
            }
            else
            {
                isInsert = false;

                promotion.ModifiedBy = SessionManager.User.UserID;
                promotion.ModifiedOn = GetCurrentDateAndTime();

                //foreach (LevelGradeChangeDetail emp in promotion.LevelGradeChangeDetails)
                for(int i=0;i<promotion.LevelGradeChangeDetails.Count;i++)
                {
                    LevelGradeChangeDetail emp = promotion.LevelGradeChangeDetails[i];

                    LevelGradeChangeDetail dbEmp = dbPromotion.LevelGradeChangeDetails.FirstOrDefault
                        (x => x.EmployeeId == emp.EmployeeId);
                    if (dbEmp == null)
                    {
                        dbPromotion.LevelGradeChangeDetails.Add(emp);

                        status.Count += 1;
                    }
                    // only change when in Saved status
                    else if ((LevelGradeChangeStatusType)dbEmp.Status == LevelGradeChangeStatusType.Saved)
                    {
                        dbEmp.DesignationId = emp.DesignationId;
                        dbEmp.LevelId = emp.LevelId;
                        dbEmp.StepGrade = emp.StepGrade;
                        dbEmp.FromDate = emp.FromDate;
                        dbEmp.FromDateEng = emp.FromDateEng;

                        status.Count += 1;
                    }
                }
                PayrollDataContext.SubmitChanges();
            }

            return status;
        }

        public static Status ImportPromotionHistory(List<LevelGradeChangeDetail> promotion, ref bool isInsert)
        {

            Status status = new Status();


            LevelGradeChange dbPromotion = PayrollDataContext.LevelGradeChanges.FirstOrDefault(x => x.LevelGradeChangeId == 0);

            if (dbPromotion == null)
            {
                dbPromotion = new LevelGradeChange();
                dbPromotion.LevelGradeChangeId = 0;
                dbPromotion.Name = "Past History Import";
                dbPromotion.Type = 2;
                dbPromotion.FromDate = "";
                dbPromotion.FromDateEng = Convert.ToDateTime("2000/1/1");
                dbPromotion.CreatedOn = Convert.ToDateTime("2000/1/1");
                dbPromotion.ApprovedOn = Convert.ToDateTime("2000/1/1");
                dbPromotion.ModifiedOn = Convert.ToDateTime("2000/1/1");

                PayrollDataContext.LevelGradeChanges.InsertOnSubmit(dbPromotion);
                PayrollDataContext.SubmitChanges();
            }


            // check if already exists
            LevelGradeChangeDetail first = promotion.FirstOrDefault();
            if (first != null)
            {
                if (PayrollDataContext.LevelGradeChangeDetails.Any(x =>
                     x.EmployeeId == first.EmployeeId && x.FromDateEng == first.FromDateEng && x.LevelName.Equals(first.LevelName)
                         && x.PrevLevelName.Equals(first.PrevLevelName)))
                {
                    status.ErrorMessage = "History already imported.";
                    return status;
                }
            }

            foreach (var item in promotion)
                item.LevelGradeChangeId = dbPromotion.LevelGradeChangeId;

            PayrollDataContext.LevelGradeChangeDetails.InsertAllOnSubmit(promotion);
            PayrollDataContext.SubmitChanges();


            //PayrollDataContext.SubmitChanges();


            return status;
        }

        public static Status ValidateGradeFittingSaveApproval(LevelGradeChange gradeFitting)
        {
            PayrollPeriod lastPeriod = CommonManager.GetLastPayrollPeriod();
            CustomDate date = CustomDate.GetCustomDateFromString(gradeFitting.FromDate, IsEnglish);
            Status status = new Status();
            if (date.Day != 1)
            {
                status.ErrorMessage = "Annual grade fitting date must be first day of the month.";
                return status;
            }

            if (gradeFitting.FromDate != lastPeriod.StartDate)
            {
                status.ErrorMessage = "Annual grade fitting can be done for current period " + lastPeriod.StartDate + " only.";
                return status;
            }

            return status;

        }

        public static Status InsertUpdateGradeFitting(LevelGradeChange gradeFitting, ref bool isInsert)
        {

            Status status = new Status();


            LevelGradeChange dbGradeFitting = null;

            if (gradeFitting.Type == (int)LevelGradeChangeType.AnnualGradeFitting)
                dbGradeFitting = GetUnapprovedGradeFitting();
            else
                dbGradeFitting = GetUnapprovedSalaryChange();

            if (dbGradeFitting == null)
            {
                isInsert = true;
                status.Count = gradeFitting.LevelGradeChangeDetails.Count;
                gradeFitting.CreatedBy = SessionManager.User.UserID;
                gradeFitting.CreatedOn = GetCurrentDateAndTime();
                PayrollDataContext.LevelGradeChanges.InsertOnSubmit(gradeFitting);
                PayrollDataContext.SubmitChanges();
            }
            else
            {
                dbGradeFitting.ModifiedBy = SessionManager.User.UserID;
                dbGradeFitting.ModifiedOn = GetCurrentDateAndTime();

                isInsert = false;
                //foreach (LevelGradeChangeDetail emp in gradeFitting.LevelGradeChangeDetails)
                for (int i = 0; i < gradeFitting.LevelGradeChangeDetails.Count; i++)
                {
                    LevelGradeChangeDetail emp = gradeFitting.LevelGradeChangeDetails[i];

                    LevelGradeChangeDetail dbEmp = dbGradeFitting.LevelGradeChangeDetails.FirstOrDefault
                        (x => x.EmployeeId == emp.EmployeeId);
                    if (dbEmp == null)
                    {
                        dbGradeFitting.LevelGradeChangeDetails.Add(emp);
                        status.Count += 1;
                    }
                    // only change when in Saved status
                    else if ((LevelGradeChangeStatusType)dbEmp.Status == LevelGradeChangeStatusType.Saved)
                    {
                                 
                        dbEmp.StepGrade = emp.StepGrade;
                        dbEmp.FromDate = emp.FromDate;
                        dbEmp.FromDateEng = emp.FromDateEng;

                        status.Count += 1;
                    }
                }
                PayrollDataContext.SubmitChanges();
            }

            return status;
        }

        /// <summary>
        /// Set Level/Grade from Salary change date to each next date when Level/Grade changed, 
        /// For this case Level can not be changed
        /// </summary>
        /// <param name="gradeFitting"></param>
        /// <returns></returns>
        //public static Status ApproveSalaryChangeGradeFitting(
        //    LevelGradeChange gradeFitting)
        //{
        //    Status status = new Status();
        //    LevelGradeChange dbGradeFitting = null;

            
        //    dbGradeFitting = GetUnapprovedSalaryChange();

        //    EmployeeManager mgr = new EmployeeManager();
        //    PayrollPeriod lastSavedPeriod = CommonManager.GetLastFinalSalaryPayrollPeriod();
        //    PayManager payMgr = new PayManager();

        //    PIncome basicIncome = payMgr.GetBasicIncome(SessionManager.CurrentCompanyId);

        //    if (dbGradeFitting != null)
        //    {
        //        foreach (LevelGradeChangeDetail emp in gradeFitting.LevelGradeChangeDetails)
        //        {
        //            LevelGradeChangeDetail dbEmp = dbGradeFitting.LevelGradeChangeDetails.FirstOrDefault
        //                (x => x.EmployeeId == emp.EmployeeId);

        //            if (dbEmp == null)
        //            {
        //                status.ErrorMessage = string.Format("Employee \"{0}\" can not be inserted in approval/rejection mode.",
        //                    mgr.GetById(emp.EmployeeId.Value).Name);
        //                return status;
        //            }
        //            // only change when in Saved status
        //            if ((LevelGradeChangeStatusType)dbEmp.Status == LevelGradeChangeStatusType.Saved)
        //            {

        //                dbEmp.StepGrade = emp.StepGrade;
        //                dbEmp.FromDate = emp.FromDate;
        //                dbEmp.FromDateEng = emp.FromDateEng;
        //                dbEmp.Status = emp.Status;

        //                status.Count += 1;


        //                // Approve Promotion Processing
        //                //if (gradeFittingStatus == LevelGradeChangeStatusType.Approved)
        //                {
        //                    PEmployeeIncome empIncome = payMgr.GetEmployeeIncome(emp.EmployeeId.Value, basicIncome.IncomeId);
        //                    empIncome.IsValid = true;
        //                    empIncome.IsEnabled = true;

        //                    PEmployeeIncrement empIncrement = new PEmployeeIncrement();
        //                    empIncrement.EffectiveFromDate = dbEmp.FromDate;
        //                    empIncrement.EffectiveFromDateEng = dbEmp.FromDateEng.Value;
        //                    empIncrement.EmployeeIncomeId = empIncome.EmployeeIncomeId;
        //                    empIncrement.IsReterospect = false;                            
        //                    empIncrement.Type = dbEmp.Type;
        //                    empIncrement.SourceId = dbEmp.DetailId;
        //                    empIncrement.OldLevelId = dbEmp.PrevLevelId;
        //                    empIncrement.OldStepGrade = dbEmp.PrevStepGrade;
        //                    empIncrement.NewLevelId = dbEmp.LevelId;
        //                    empIncrement.NewStepGrade = dbEmp.StepGrade;
        //                    // Emp Income update                           
        //                    empIncome.LevelId = dbEmp.LevelId;
        //                    empIncome.Step = dbEmp.StepGrade;

        //                    if (lastSavedPeriod != null && empIncrement.EffectiveFromDateEng <= lastSavedPeriod.EndDateEng)
        //                    {
        //                        empIncrement.IsReterospect = true; 

        //                        // if the Salary Change is back dated then we need to save first Entry from Effective date using Level/Grade
        //                        // in that month & increment for each successive period when Level/Grade has been changed
        //                        List<PEmployeeIncrement> levelGradeChangeAfterEffectiveDateList
        //                            = PayrollDataContext.PEmployeeIncrements.Where(x => x.EmployeeIncomeId == empIncrement.EmployeeIncomeId
        //                                && x.EffectiveFromDateEng > empIncrement.EffectiveFromDateEng)
        //                                .OrderBy(x => x.EffectiveFromDateEng).ToList();

        //                        if (levelGradeChangeAfterEffectiveDateList.Count > 0)
        //                        {
        //                            PEmployeeIncrement firstIncrement = new PEmployeeIncrement();
        //                            CopyObject<PEmployeeIncrement>(empIncrement, ref firstIncrement);

        //                            empIncrement.EmployeeIncomeId = empIncome.EmployeeIncomeId;
        //                            empIncrement.OldLevelId = NewHRManager.GetEmployeeLevelUsingDate(emp.EmployeeId.Value, empIncrement.EffectiveFromDateEng).LevelId;
        //                            empIncrement.NewLevelId = emp.LevelId.Value;
        //                            empIncrement.OldStepGrade = NewHRManager.GetEmployeeGradeStepUsingDate(emp.EmployeeId.Value, empIncrement.EffectiveFromDateEng);
        //                            empIncrement.OldStepGrade = NewPayrollManager.GetNewGrade(empIncrement.OldLevelId.Value, empIncrement.OldStepGrade.Value, empIncrement.OldLevelId.Value,
        //                                empIncrement.EffectiveFromDateEng, LevelGradeChangeType.SalaryStructureChange);

        //                            int newGrade = empIncrement.OldStepGrade.Value;
        //                            PayrollDataContext.PEmployeeIncrements.InsertOnSubmit(empIncrement);

        //                            //foreach (PEmployeeIncrement item in levelGradeChangeAfterEffectiveDateList)
        //                            for(int i=0;i<levelGradeChangeAfterEffectiveDateList.Count;i++)
        //                            {
        //                                PEmployeeIncrement item = levelGradeChangeAfterEffectiveDateList[i];

        //                                PEmployeeIncrement increment = new PEmployeeIncrement();
        //                                CopyObject<PEmployeeIncrement>(item, ref increment);

        //                                increment.IsReterospect = true;
        //                                increment.EmployeeIncomeId = empIncome.EmployeeIncomeId;
        //                                increment.EffectiveFromDate = item.EffectiveFromDate;
        //                                increment.EffectiveFromDateEng = item.EffectiveFromDateEng;
        //                                increment.OldLevelId = item.OldLevelId;
        //                                increment.OldStepGrade = newGrade;
        //                                increment.OldStepGrade = NewPayrollManager.GetNewGrade(empIncrement.OldLevelId.Value, newGrade, empIncrement.OldLevelId.Value,
        //                                    increment.EffectiveFromDateEng, LevelGradeChangeType.SalaryStructureChange);

        //                                // if last then current Grade will be set
        //                                if (i == levelGradeChangeAfterEffectiveDateList.Count - 1 && increment.OldLevelId == empIncome.LevelId)
        //                                    increment.OldStepGrade = empIncome.Step;
                                        
        //                                newGrade = increment.OldStepGrade.Value;

        //                                PayrollDataContext.PEmployeeIncrements.InsertOnSubmit(increment);
        //                            }
        //                        }
        //                        else
        //                        {
        //                            // If not Level/Grade change has occured after Effective date
        //                            PayrollDataContext.PEmployeeIncrements.InsertOnSubmit(empIncrement);
        //                        }

        //                    }
        //                    else
        //                    {
        //                        PayrollDataContext.PEmployeeIncrements.InsertOnSubmit(empIncrement);
        //                    }
        //                }

        //            }



        //        }
        //        PayrollDataContext.SubmitChanges();
        //    }

        //    return status;
        //}
    }
}
