﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;
using Utils.Calendar;
using BLL.BO;

namespace BLL.Manager
{

    public class PositionGradeStepAmountManager : BaseBiz
    {
        #region Grade
        public static List<Grade> GetAllGrades(int companyId)
        {
            return PayrollDataContext.Grades.OrderBy(x => x.Order).Where(x => x.CompanyId == companyId).ToList();
        }

        //public static List<Grade> GetPostionGradeStepAmount(int companyId)
        //{
        //    return null;
        //}

        public static Grade GetGradeById(int gradeId)
        {
            return PayrollDataContext.Grades.SingleOrDefault(x=>x.GradeId==gradeId);
        }

        public static bool SaveGrade(Grade grade, ref string errMsg)
        {
            Grade dbGrade = PayrollDataContext.Grades.Where(x => (x.Name == grade.Name ) && (x.CompanyId == grade.CompanyId)).Take(1).SingleOrDefault();
            if (dbGrade != null)
            {
                errMsg = "Grade name already exists."; return false;
            }


            PayrollDataContext.Grades.InsertOnSubmit(grade);
            return SaveChangeSet();

        }

        public static bool UpdateGrade(Grade grade, ref string errMsg)
        {
            var result = (from g in PayrollDataContext.Grades
                         where( (g.Name == grade.Name ) && g.GradeId != grade.GradeId && g.CompanyId == grade.CompanyId)
                         
                         select g)
                         .ToList();
            if (result.Count > 0)
            {
                errMsg = "Grade name already exists."; return false;
            }

            Grade obGrade = new Grade();
            obGrade = PayrollDataContext.Grades.Where(x => x.GradeId == grade.GradeId).SingleOrDefault();
            obGrade.Name = grade.Name;
            obGrade.Order = grade.Order;
            obGrade.ModifiedBy = SessionManager.CurrentLoggedInEmployeeId;
            obGrade.ModifiedOn = System.DateTime.Now;
            UpdateChangeSet();
            return true;
        }

        public static bool CanItemBeDeleted(int gradeId, ref string errMessage)
        {
            List<PositionGradeStepAmount> list = PayrollDataContext.PositionGradeStepAmounts.Where(x => x.GradeId == gradeId).ToList();
            if (list.Count > 0)
            {

                errMessage = "Grade is in use, can not be deleted.";
                return false;

            }
            else
                return true;
        }

        public static Boolean DeleteGrade(int gradeId)
        {
            Grade dbGrade = PayrollDataContext.Grades.Where(x => x.GradeId == gradeId).SingleOrDefault();
            PayrollDataContext.Grades.DeleteOnSubmit(dbGrade);
            return DeleteChangeSet();
        }

        #endregion

        #region Step

        public static List<Step> GetAllSteps(int companyId)
        {
            return PayrollDataContext.Steps.OrderBy(x => x.StepId).Where(x => x.CompanyId == companyId).ToList();
        }

        public static Step GetStepById(int stepId)
        {
            return PayrollDataContext.Steps.SingleOrDefault(x => x.StepId == stepId);
        }

        public static bool SaveStep(Step step, ref string errMsg)
        {
            Step dbStep = PayrollDataContext.Steps.Where(x => (x.Name == step.Name ) && (x.CompanyId == step.CompanyId) ).Take(1).SingleOrDefault();
            if (dbStep != null)
            {
                errMsg = "Step name already exists."; return false;
            }


            PayrollDataContext.Steps.InsertOnSubmit(step);
            return SaveChangeSet();
        }

        public static bool UpdateStep(Step step, ref string errMsg)
        {
            var result = (from s in PayrollDataContext.Steps
                          where ((s.Name == step.Name ) && s.StepId != step.StepId && s.CompanyId == step.CompanyId)

                          select s)
                         .ToList();
            if (result.Count > 0)
            {
                errMsg = "Step name already exists."; return false;
            }

            Step obStep = new Step();
            obStep = PayrollDataContext.Steps.Where(x => x.StepId == step.StepId).SingleOrDefault();
            obStep.Name = step.Name;
            obStep.Order = step.Order;

            obStep.ModifiedBy = SessionManager.CurrentLoggedInEmployeeId;
            obStep.ModifiedOn = System.DateTime.Now;
            return UpdateChangeSet();
        }
        
        public static bool CanStepBeDeleted(int id, ref string errMessage)
        {
            List<PositionGradeStepAmount> list = PayrollDataContext.PositionGradeStepAmounts.Where(x => x.StepId == id).ToList();
            if (list.Count > 0)
            {

                errMessage = "Step is in use, can not be deleted.";
                return false;

            }
            else
                return true;
        }
        

        public static bool DeleteStep(int id)
        {
            Step dbStep = PayrollDataContext.Steps.Where(x => x.StepId == id).SingleOrDefault();
            PayrollDataContext.Steps.DeleteOnSubmit(dbStep);
            return DeleteChangeSet();
        }
        #endregion

        #region Position
        public static List<Position> GetAllPositions(int companyId)
        {
            return PayrollDataContext.Positions.OrderBy(x => x.Order).Where(x => x.CompanyId == companyId).ToList();
        }

        
        public static List<EvaluationPeriod> GetAllPeriods(int companyId)
        {
            return PayrollDataContext.EvaluationPeriods.OrderBy(x => x.FromDateEng).Where(x => x.CompanyId == companyId).ToList();
        }
        public static Position GetPositionById(int positionId)
        {
            return PayrollDataContext.Positions.SingleOrDefault(x => x.PositionId == positionId);
        }

        public static bool SavePosition(Position position, ref string errMsg)
        {
            Position dbGrade = PayrollDataContext.Positions.Where(x => (x.Name == position.Name) && (x.CompanyId == position.CompanyId)).Take(1).SingleOrDefault();
            if (dbGrade != null)
            {
                errMsg = "Position name already exists."; return false;
            }


            PayrollDataContext.Positions.InsertOnSubmit(position);
            return SaveChangeSet();

        }

       

        public static bool SavePeriod(EvaluationPeriod period, ref string errMsg)
        {
            EvaluationPeriod dbGrade = PayrollDataContext.EvaluationPeriods.Where(x => (x.Name == period.Name) && (x.CompanyId == period.CompanyId)).Take(1).SingleOrDefault();
            if (dbGrade != null)
            {
                errMsg = "Duplicate name. Please enter unique name."; return false;
            }

            period.FromDateEng = GetEngDate(period.FromDate, true);
            period.ToDateEng = GetEngDate(period.ToDate, true);
            period.PeriodDateName = GetEvalulationPeriodName(period);

            PayrollDataContext.EvaluationPeriods.InsertOnSubmit(period);
            return SaveChangeSet();

        }

        public static string GetEvalulationPeriodName(EvaluationPeriod period)
        {
            CustomDate from = CustomDate.GetCustomDateFromString(period.FromDate, true);

            CustomDate to = CustomDate.GetCustomDateFromString(period.ToDate, true);


            return DateHelper.GetMonthShortName(from.Month, true) + "-" + from.Year + " : " +
                DateHelper.GetMonthShortName(to.Month, true) + "-" + to.Year;



        }

        public static List<Evaluation> GetEvaluationHistory(int employeeId, int evalulationPeriodId)
        {

            DateTime? currentEvaluationDate = 
                (
                    from ev in PayrollDataContext.EvaluationPeriods 
                    where ev.EvaluationPeriodId == evalulationPeriodId
                    select ev.FromDateEng
                ).SingleOrDefault();


            var q =
                (
                    from e in PayrollDataContext.Evaluations
                    join ev in PayrollDataContext.EvaluationPeriods on e.EvalulationPeriodId equals ev.EvaluationPeriodId
                    where e.EmployeeId == employeeId && ev.FromDateEng < currentEvaluationDate
                    select e
                ).ToList();

            return q;
        }

        public static bool UpdatePeriod(EvaluationPeriod period, ref string errMsg)
        {

            var result = (from p in PayrollDataContext.EvaluationPeriods
                          where ((p.Name == period.Name) && p.EvaluationPeriodId != period.EvaluationPeriodId && p.CompanyId == period.CompanyId)

                          select p)
                         .ToList();
            if (result.Count > 0)
            {
                errMsg = "Duplicate name. Please enter unique name."; return false;
            }

            EvaluationPeriod obPeriod = new EvaluationPeriod();
            obPeriod = PayrollDataContext.EvaluationPeriods.Where(x => x.EvaluationPeriodId == period.EvaluationPeriodId).SingleOrDefault();
            
            obPeriod.Name = period.Name;
            obPeriod.FromDate = period.FromDate;
            obPeriod.FromDateEng = GetEngDate(period.FromDate, true);
            obPeriod.ToDate = period.ToDate;
            obPeriod.ToDateEng = GetEngDate(period.ToDate, true);
            obPeriod.IsActive = period.IsActive;
            obPeriod.PeriodDateName = GetEvalulationPeriodName(obPeriod);

           // obPeriod.Order = rating.Order;
            obPeriod.CreatedBy = period.CreatedBy;
            obPeriod.CreatedOn = period.CreatedOn;
            obPeriod.ModifiedBy = SessionManager.CurrentLoggedInEmployeeId;
            obPeriod.ModifiedOn = System.DateTime.Now;
            return UpdateChangeSet();

        }



        public static bool UpdatePosition(Position position, ref string errMsg)
        {
            var result = (from p in PayrollDataContext.Positions
                          where ( (p.Name == position.Name ) && p.PositionId != position.PositionId && p.CompanyId == position.CompanyId)

                          select p)
                         .ToList();
            if (result.Count > 0)
            {
                errMsg = "Position name already exists."; return false;
            }

            Position obPosition = new Position();
            obPosition = PayrollDataContext.Positions.Where(x => x.PositionId == position.PositionId).SingleOrDefault();
            obPosition.Name = position.Name;
            obPosition.Order = position.Order;
            obPosition.ModifiedBy = SessionManager.CurrentLoggedInEmployeeId;
            obPosition.ModifiedOn = System.DateTime.Now;
            return UpdateChangeSet();

        }

        public static bool CanPositionBeDeleted(int positionId, ref string errMessage)
        {
            List<PositionGradeStepAmount> list = PayrollDataContext.PositionGradeStepAmounts.Where(x => x.PositionId == positionId).ToList();
            if (list.Count > 0)
            {
                errMessage = "Position is in use, can no be deleted.";
                return false;
            }
            else
                return true;
        }
        public static bool CanRatingBeDeleted(int ratingId, ref string errMessage)
        {
            if (PayrollDataContext.Evaluations.Any(x => x.RatingId == ratingId))
            {
                errMessage = "Rating is in use, can not be deleted.";
                return false;
            }

            return true;
        }
        public static bool CanEvalulationPeriodBeDeleted(int evaluationPeriod, ref string errMessage)
        {
            if (PayrollDataContext.Evaluations.Any(x => x.EvalulationPeriodId == evaluationPeriod))
            {
                errMessage = "Evaluation Period is in use, can not be deleted.";
                return false;
            }

            return true;
        }

        public static Boolean DeletePeriod(int periodId)
        {
            EvaluationPeriod dbPeriod = PayrollDataContext.EvaluationPeriods.Where(x => x.EvaluationPeriodId == periodId).SingleOrDefault();
            PayrollDataContext.EvaluationPeriods.DeleteOnSubmit(dbPeriod);
            return DeleteChangeSet();
        }

        public static Boolean DeletePosition(int positionId)
        {
            Position dbPosition = PayrollDataContext.Positions.Where(x => x.PositionId == positionId).SingleOrDefault();
            PayrollDataContext.Positions.DeleteOnSubmit(dbPosition);
            return DeleteChangeSet();
        }

        #endregion

        #region Rating
        public static List<EvalulationRating> GetAllRatings(int companyId)
        {
            return PayrollDataContext.EvalulationRatings.OrderBy(x => x.Order).Where(x => x.CompanyId == companyId).ToList();
        }

      
        public static bool SaveRating(EvalulationRating rating, ref string errMsg)
        {
            EvalulationRating dbGrade = PayrollDataContext.EvalulationRatings.Where(x => (x.Name == rating.Name ) && (x.CompanyId == rating.CompanyId)).Take(1).SingleOrDefault();
            if (dbGrade != null)
            {
                errMsg = "Rating name already exists."; return false;
            }


            PayrollDataContext.EvalulationRatings.InsertOnSubmit(rating);
            return SaveChangeSet();

        }


        public static bool UpdateRating(EvalulationRating rating, ref string errMsg)
        {

            var result = (from p in PayrollDataContext.EvalulationRatings
                          where ((p.Name == rating.Name ) && p.RatingId != rating.RatingId && p.CompanyId == rating.CompanyId)

                          select p)
                         .ToList();
            if (result.Count > 0)
            {
                errMsg = "Rating name already exists."; return false;
            }

            EvalulationRating obRating = new EvalulationRating();
            obRating = PayrollDataContext.EvalulationRatings.Where(x => x.RatingId == rating.RatingId).SingleOrDefault();
            obRating.Name = rating.Name;
            obRating.Order = rating.Order;
            obRating.CreatedBy = rating.CreatedBy;
            obRating.CreatedOn = rating.CreatedOn;
            obRating.ModifiedBy = SessionManager.CurrentLoggedInEmployeeId;
            obRating.ModifiedOn = System.DateTime.Now;
            return UpdateChangeSet();

        }

        public static Boolean DeleteRating(int ratingId)
        {
            EvalulationRating dbRating = PayrollDataContext.EvalulationRatings.Where(x => x.RatingId == ratingId).SingleOrDefault();
            PayrollDataContext.EvalulationRatings.DeleteOnSubmit(dbRating);
            return DeleteChangeSet();
        }

        #endregion

        public static decimal? GetPositionGradeAmount(int? positionId, int? gradeId, int? stepId)
        {
            PositionGradeStepAmount amount =  PayrollDataContext.PositionGradeStepAmounts
                .SingleOrDefault(g => (g.PositionId == positionId && g.GradeId == gradeId && g.StepId == stepId));
            if (amount != null)
                return amount.Amount;
            return 0;
        }
       
        public static List<GetEmployeeListGradeStepResult> GetEmployeeListGradeStep(int? branchId, int? departmentId, string name, int? positionId, int? gradeId, int? stepId
            ,int start,int limit)
        {
            List<GetEmployeeListGradeStepResult>  list = 
                PayrollDataContext.GetEmployeeListGradeStep(SessionManager.CurrentCompanyId, branchId, departmentId, name, positionId, gradeId, stepId,start,limit).ToList();
            return list;
        }

        public static bool InitialSaveEmployeeGradeStepPosition(PEmployeeIncome empIncome)
        {
            PEmployeeIncome eIncome = PayrollDataContext.PEmployeeIncomes.Where(x => x.EmployeeIncomeId == empIncome.EmployeeIncomeId).SingleOrDefault();

            //log for Position/Grade/Step change
            if (LogEnabled ||
                eIncome.PositionId==null
                ||
                (empIncome.PositionId != eIncome.PositionId || empIncome.GradeId != eIncome.GradeId || empIncome.StepId != eIncome.StepId) )
                PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(
                     empIncome.EmployeeId,(byte) MonetoryChangeLogTypeEnum.Income,
                     MonetoryChangeLogTypeEnum.Grade.ToString() +" : " +empIncome.PIncome.Title, "-", 
                     PayManager.GetLogForPositionGradeStepChange(empIncome.PositionId, empIncome.GradeId, empIncome.StepId)
                     ,LogActionEnum.Add));

            eIncome.PositionId = empIncome.PositionId;
            eIncome.GradeId = empIncome.GradeId;
            eIncome.StepId = empIncome.StepId;

            eIncome.IsValid = true;
           

            return UpdateChangeSet();
        }

        public static List<EvaluationPeriod> GetEvaluationPeriodList()
        {
            return PayrollDataContext.EvaluationPeriods.Where(x => x.CompanyId == SessionManager.CurrentCompanyId).ToList();
        }

        public static List<GetEvaluationInfoResult> GetEvaluationPeriod(int start, int limit, int companyId, int evaluationId
            ,int branchId,int departmentId,int subDepartmentId,int designationId,string empSearch,int employeeId)
        {

            string depIDList = SessionManager.CustomRoleDeparmentIDList;
            
            List<GetEvaluationInfoResult> list =
                PayrollDataContext.GetEvaluationInfo(companyId, evaluationId, branchId, departmentId, subDepartmentId, designationId,
                empSearch, start, limit, employeeId, depIDList).ToList();

        

            return list;

        }

        public static bool SaveUpdateEvalulation(GetEvaluationInfoResult entity)
        {
            Evaluation dbEntity = PayrollDataContext.Evaluations.SingleOrDefault
                (e => e.EmployeeId == entity.EmployeeId && e.EvalulationPeriodId == entity.EvaluationPeriodId);

            if (dbEntity == null)
            {
                dbEntity = new Evaluation();
                dbEntity.EmployeeId = entity.EmployeeId;
                dbEntity.EvalulationPeriodId = entity.EvaluationPeriodId;
                dbEntity.RatingId = entity.RatingId;
                dbEntity.EvalulationDate = entity.EvalulationDate;
                dbEntity.EvalulationDateEng = GetEngDate(entity.EvalulationDate, true);
                dbEntity.Comment = entity.Comment;

                PayrollDataContext.Evaluations.InsertOnSubmit(dbEntity);
                return SaveChangeSet();
            }
            else
            {
                dbEntity.RatingId = entity.RatingId;
                dbEntity.EvalulationDate = entity.EvalulationDate;
                dbEntity.EvalulationDateEng = GetEngDate(entity.EvalulationDate, true);
                dbEntity.Comment = entity.Comment;

                
                return UpdateChangeSet();
            }
        }


        public static EvaluationPeriod GetEvaluatioPeriod(int evaluationId)
        {
            return PayrollDataContext.EvaluationPeriods.SingleOrDefault(e => e.EvaluationPeriodId == evaluationId);
        }

      

        public static int? GetEmployeeCurrentPosition(int pEmployeeId)
        {
            var result = (from i in PayrollDataContext.PEmployeeIncomes
                          join income in PayrollDataContext.PIncomes on i.IncomeId equals income.IncomeId 
                        where i.EmployeeId == pEmployeeId 
                        where income.IsGrade == true
                        select i
                        ).FirstOrDefault();
            if (result != null)
                return result.PositionId;
            return null;
           
        }

        public static List<BO.PositionHistoryBO> GetEmployeeGradeHistory(int employeeId)
        {
            var result = (from i in PayrollDataContext.PEmployeeIncomes
                          join inc in PayrollDataContext.PEmployeeIncrements on i.EmployeeIncomeId equals inc.EmployeeIncomeId
                          join g in PayrollDataContext.Grades on inc.GradeId equals g.GradeId
                          where (i.EmployeeId == employeeId)
                          select new BLL.BO.PositionHistoryBO
                          {
                              GradeId = g.GradeId,
                              Name = g.Name,
                              EffectiveFromDate = inc.EffectiveFromDate,
                              EffectiveFromDateEng = inc.EffectiveFromDateEng
                          }).ToList();
            return result;
        }

        public static List<BLL.BO.PositionHistoryBO> GetEmployeePositionHistory(int employeeId)
        {
            var result = (from i in PayrollDataContext.PEmployeeIncomes
                          join inc in PayrollDataContext.PEmployeeIncrements on i.EmployeeIncomeId equals inc.EmployeeIncomeId
                          join p in PayrollDataContext.Positions on inc.PositionId equals p.PositionId
                          where (i.EmployeeId == employeeId)
                          select new BLL.BO.PositionHistoryBO
                          {
                              PositionId = p.PositionId,
                              Name = p.Name,
                              EffectiveFromDate = inc.EffectiveFromDate,
                              EffectiveFromDateEng = inc.EffectiveFromDateEng
                          }).ToList();
            return result;

        }

        public static List<BO.PositionHistoryBO> GetEmployeePositionGradeStepHistory(int employeeId)
        {

            List<Position> positions = GetAllPositions(SessionManager.CurrentCompanyId);
            List<Grade> grades = GetAllGrades(SessionManager.CurrentCompanyId);
            List<Step> steps = GetAllSteps(SessionManager.CurrentCompanyId);


            var result = (from i in PayrollDataContext.PEmployeeIncomes
                          join inc in PayrollDataContext.PEmployeeIncrements on i.EmployeeIncomeId equals inc.EmployeeIncomeId
                          join p in PayrollDataContext.Positions on inc.PositionId equals p.PositionId
                          where (i.EmployeeId == employeeId)
                          select new BLL.BO.PositionHistoryBO
                          {
                              //StepName = steps.SingleOrDefault(s=>s.StepId == inc.StepId).Name,
                              //PositionName = positions.SingleOrDefault(p => p.PositionId == inc.PositionId).Name,
                              //GradeName = grades.SingleOrDefault(s => s.GradeId == inc.GradeId).Name,
                              PositionId = inc.PositionId,
                              GradeId = inc.GradeId,
                              StepId =inc.StepId,
                              EffectiveFromDate = inc.EffectiveFromDate,
                              EffectiveFromDateEng = inc.EffectiveFromDateEng
                          }).ToList();


            foreach (PositionHistoryBO entity in result)
            {
                Position postion = positions.SingleOrDefault(p => p.PositionId == entity.PositionId);
                entity.PositionName = postion != null ? postion.Name : "";

                Grade grade = grades.SingleOrDefault(s => s.GradeId == entity.GradeId);
                entity.GradeName = grade != null ? grade.Name : "";
                
                Step step = steps.SingleOrDefault(s => s.StepId == entity.StepId);

                entity.StepName = step != null ? step.Name : "";
            }

            return result;
        
        }
        
        public static int? GetEmployeeCurrentGrade(int pEmployeeId)
        {
            var result = (from i in PayrollDataContext.PEmployeeIncomes
                          join income in PayrollDataContext.PIncomes on i.IncomeId equals income.IncomeId
                          where i.EmployeeId == pEmployeeId
                          where income.IsGrade == true
                          select i
                       ).FirstOrDefault();
            if (result != null)
                return result.GradeId;
            return null;
        }

        public static int? GetEmployeeCurrentStep(int pEmployeeId)
        {
            var result = (from i in PayrollDataContext.PEmployeeIncomes
                          join income in PayrollDataContext.PIncomes on i.IncomeId equals income.IncomeId
                          where i.EmployeeId == pEmployeeId
                          where income.IsGrade == true
                          select i
                      ).FirstOrDefault();
            if (result != null)
                return result.StepId;
            return null;
        }

        public static List<BO.PositionHistoryBO> GetEmployeeStepHistory(int employeeId)
        {
            var result = (from i in PayrollDataContext.PEmployeeIncomes
                          join inc in PayrollDataContext.PEmployeeIncrements on i.EmployeeIncomeId equals inc.EmployeeIncomeId
                          join s in PayrollDataContext.Steps on inc.StepId equals s.StepId
                          where (i.EmployeeId == employeeId)
                          select new BLL.BO.PositionHistoryBO
                          {
                              StepId = s.StepId,
                              Name = s.Name,
                              EffectiveFromDate = inc.EffectiveFromDate,
                              EffectiveFromDateEng = inc.EffectiveFromDateEng
                          }).ToList();
            return result;
        }

    }

}
