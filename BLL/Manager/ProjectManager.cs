﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;
using BLL.BO;

namespace BLL.Manager
{

    public class ProjectManager : BaseBiz
    {
        public static string AddProjectToEmployee(EEmployee emp, bool addToAllEmployees)
        {
            string str = string.Empty;

            EEmployee dbEmployee = PayrollDataContext.EEmployees.SingleOrDefault(x => x.EmployeeId == emp.EmployeeId);
            string projectList = "";

            foreach (ProjectEmployee item in emp.ProjectEmployees)
            {
                if (projectList == "")
                    projectList = item.ProjectId.ToString();
                else
                    projectList += "," + item.ProjectId.ToString();
            }

            List<ProjectEmployee> newList = emp.ProjectEmployees.ToList();
            List<ProjectEmployee> oldList = dbEmployee.ProjectEmployees.ToList();

            foreach (ProjectEmployee item in oldList)
            {
                if (!newList.Any(x => x.ProjectId == item.ProjectId))
                {
                    if (PayrollDataContext.TimesheetProjects.Any(x => x.ProjectId == item.ProjectId))
                    {
                        if (str == string.Empty)
                            str = ProjectManager.GetProjectById(item.ProjectId).Name;
                        else
                            str += ", " + ProjectManager.GetProjectById(item.ProjectId).Name;
                    }
                }
            }

            if (str != string.Empty)
            {
                str += " is being used, cannot be removed for the employee \"" + dbEmployee.Name + "\".";
                return str;
            }

            dbEmployee.ProjectEmployees.Clear();

            dbEmployee.ProjectEmployees.AddRange(newList.ToList());

            //add Projects to other employees
            if (addToAllEmployees)
                PayrollDataContext.AddProjectToAllEmployees(projectList, emp.EmployeeId);

            PayrollDataContext.SubmitChanges();


            return string.Empty;
        }

        public static Status SaveUpdateProjectAssocation(INGOProjectEmployee obj)
        {
            Status status = new Status();


            // If Employee assign start/end date cross project start/end date

            if (PayrollDataContext.Projects.Any(x => x.ProjectId == obj.ProjectId && obj.StartDate < x.StartDateEng))
            {
                status.ErrorMessage = "Employee assign start date is before the start date of the project.";
                status.IsSuccess = false;
                return status;
            }

            if (obj.EndDate != null && PayrollDataContext.Projects.Any(x => x.ProjectId == obj.ProjectId && obj.EndDate > x.EndDateEng))
            {
                status.ErrorMessage = "Employee assign end date is after the end date of the project.";
                status.IsSuccess = false;
                return status;
            }


            //if (obj.EndDate != null)
            {
                if (PayrollDataContext.INGOProjectEmployees.Any(x => x.ProjectId == obj.ProjectId && x.EmployeeId == obj.EmployeeId
                        && obj.ID != x.ID
                        &&
                        (
                        (obj.StartDate >= x.StartDate && obj.StartDate <= x.EndDate) ||
                        (obj.EndDate >= x.StartDate && obj.EndDate <= x.EndDate) ||

                        (x.StartDate >= obj.StartDate && x.StartDate <= obj.EndDate) ||
                        (x.EndDate >= obj.StartDate && x.EndDate <= obj.EndDate)
                        )
                        )

                        )
                {
                    status.ErrorMessage = "This project already assigned for this date.";
                    status.IsSuccess = false;
                    return status;
                }
            }


            if (obj.ID == 0)
            {
                if (PayrollDataContext.INGOProjectEmployees.Any(x => x.ProjectId == obj.ProjectId && x.EmployeeId == obj.EmployeeId && x.EndDate == null))
                {
                    status.ErrorMessage = "This Project not closed for this employee.";
                    status.IsSuccess = false;
                    return status;
                }




            }


            //if (PayrollDataContext.ProjectEmployees.Any(x => x.ProjectId == obj.ProjectId && x.EmployeeId==obj.EmployeeId && x.ID != obj.ID ))
            //{
            //    status.ErrorMessage = "Project already exists for this employee.";
            //    status.IsSuccess = false;
            //    return status;
            //}

            //if (PayrollDataContext.Projects.Any(x => x.StartDateEng > obj.StartDate && x.EndDateEng <= obj.StartDate))
            //{
            //    status.ErrorMessage = "";
            //    status.IsSuccess = false;
            //    return status;
            //}


            if (obj.ID.Equals(0))
            {
                PayrollDataContext.INGOProjectEmployees.InsertOnSubmit(obj);
                PayrollDataContext.SubmitChanges();
                status.IsSuccess = true;
            }
            else
            {
                INGOProjectEmployee dbEntity = PayrollDataContext.INGOProjectEmployees.SingleOrDefault(x => x.ID == obj.ID);

                dbEntity.StartDate = obj.StartDate;
                dbEntity.EndDate = obj.EndDate;
                PayrollDataContext.SubmitChanges();
                status.IsSuccess = true;
            }

            return status;
        }

        public static INGOProjectEmployee GetMonthlyProjectAssociationByID(int AssociationID)
        {
            return PayrollDataContext.INGOProjectEmployees.SingleOrDefault(x => x.ID == AssociationID);
        }

        public static List<Project> GetDestPrjListByEmpId(int employeeId)
        {
            return (from pe in PayrollDataContext.ProjectEmployees
                    join prj in PayrollDataContext.Projects on pe.ProjectId equals prj.ProjectId
                    join emp in PayrollDataContext.EEmployees on pe.EmployeeId equals emp.EmployeeId
                    where prj.CompanyId == SessionManager.CurrentCompanyId && pe.EmployeeId == employeeId
                    orderby prj.Name
                    select prj).ToList();
        }

        public static ProjectTask GetProjectTaskById(int taskId)
        {
            return PayrollDataContext.ProjectTasks.SingleOrDefault(x => x.TaskId == taskId);
        }


        public static ProjectSubProject GetProjectSubProjectById(int subProjectId)
        {
            return PayrollDataContext.ProjectSubProjects.SingleOrDefault(x => x.SubProjectId == subProjectId);
        }


        public static string GetProjectList(int EmployeeId)
        {
            var list = (from pe in PayrollDataContext.ProjectEmployees
                        join prj in PayrollDataContext.Projects on pe.ProjectId equals prj.ProjectId
                        join emp in PayrollDataContext.EEmployees on pe.EmployeeId equals emp.EmployeeId
                        where pe.EmployeeId == EmployeeId
                        orderby prj.Name
                        select prj.Name).ToList();

            return String.Join(",", list.ToArray());
        }

        public static List<Project> GetAllProjectList()
        {
            return PayrollDataContext.Projects.OrderBy(x => x.Name).ToList();
        }

        //public static bool IsValidSartEndDateForProject(DateTime StartDate,DateTime EndDate,int projectID,int employeeId)
        //{
        //   List<ProjectEmployee> _list =  PayrollDataContext.ProjectEmployees
        //       .Where(x => 
        //            (
        //                (x.StartDate >= StartDate && x.StartDate <= EndDate) || 
        //                (x.EndDate >= StartDate && x.EndDate <= EndDate)||
        //                (StartDate >= x.StartDate && StartDate <= x.EndDate) || 
        //                (EndDate >= x.StartDate && EndDate <= x.EndDate) ||
        //                (x.StartDate <= StartDate && x.EndDate >= EndDate)
        //            )
        //            && x.ProjectId ==projectID && x.EmployeeId==employeeId).ToList();
        //   if (_list.Any())
        //       return true;
        //   else
        //       return false;
        //}

        public static Status SaveUpdateProjectAssocation(ProjectEmployee obj)
        {
            Status status = new Status();


            // If Employee assign start/end date cross project start/end date

            if (PayrollDataContext.Projects.Any(x => x.ProjectId==obj.ProjectId && obj.StartDate < x.StartDateEng))
            {
                status.ErrorMessage = "Employee assign start date is before the start date of the project.";
                status.IsSuccess = false;
                return status;
            }

            if (obj.EndDate != null && PayrollDataContext.Projects.Any(x => x.ProjectId == obj.ProjectId && obj.EndDate > x.EndDateEng))
            {
                status.ErrorMessage = "Employee assign end date is after the end date of the project.";
                status.IsSuccess = false;
                return status;
            }


            //if (obj.EndDate != null)
            {
                if (PayrollDataContext.ProjectEmployees.Any(x => x.ProjectId == obj.ProjectId && x.EmployeeId == obj.EmployeeId
                        && obj.ID != x.ID
                        &&
                        (
                        (obj.StartDate >= x.StartDate && obj.StartDate <= x.EndDate) ||
                        (obj.EndDate >= x.StartDate && obj.EndDate <= x.EndDate) ||

                        (x.StartDate >= obj.StartDate && x.StartDate <= obj.EndDate)||
                        (x.EndDate >= obj.StartDate && x.EndDate <= obj.EndDate)
                        )               
                        )

                        )
                {
                    status.ErrorMessage = "This project already assigned for this date.";
                    status.IsSuccess = false;
                    return status;
                }
            }


            if (obj.ID == 0)
            {
                if (PayrollDataContext.ProjectEmployees.Any(x => x.ProjectId == obj.ProjectId && x.EmployeeId == obj.EmployeeId && x.EndDate == null))
                {
                    status.ErrorMessage = "This Project not closed for this employee.";
                    status.IsSuccess = false;
                    return status;
                }

               


            }


            //if (PayrollDataContext.ProjectEmployees.Any(x => x.ProjectId == obj.ProjectId && x.EmployeeId==obj.EmployeeId && x.ID != obj.ID ))
            //{
            //    status.ErrorMessage = "Project already exists for this employee.";
            //    status.IsSuccess = false;
            //    return status;
            //}

            //if (PayrollDataContext.Projects.Any(x => x.StartDateEng > obj.StartDate && x.EndDateEng <= obj.StartDate))
            //{
            //    status.ErrorMessage = "";
            //    status.IsSuccess = false;
            //    return status;
            //}


            if (obj.ID.Equals(0))
            {
                PayrollDataContext.ProjectEmployees.InsertOnSubmit(obj);
                PayrollDataContext.SubmitChanges();
                status.IsSuccess = true;
            }
            else
            {
                ProjectEmployee dbEntity = PayrollDataContext.ProjectEmployees.SingleOrDefault(x => x.ID == obj.ID);
            
                dbEntity.StartDate = obj.StartDate;
                dbEntity.EndDate = obj.EndDate;
                dbEntity.ProjectId = obj.ProjectId;
                dbEntity.ContractID = obj.ContractID;
                PayrollDataContext.SubmitChanges();
                status.IsSuccess = true;
            }

            return status;
        }

        public static Status SaveUpdateProjectAssocation(List<GetProjectAssociationResult> list)
        {
            Status status = new Status();

            int row = 1;

            List<ProjectEmployeeActivity> dbActivityList = PayrollDataContext.ProjectEmployeeActivities.ToList();

            List<ProjectEmployee> dbProjectEmployeeList = PayrollDataContext.ProjectEmployees.ToList();

            foreach (GetProjectAssociationResult obj in list)
            {
                row += 1;
                // If Employee assign start/end date cross project start/end date

                if (PayrollDataContext.Projects.Any(x => x.ProjectId == obj.ProjectId && obj.StartDate < x.StartDateEng))
                {
                    Project project = PayrollDataContext.Projects.FirstOrDefault(x => x.ProjectId == obj.ProjectId && obj.StartDate < x.StartDateEng);
                    status.ErrorMessage = "Employee assign start date " + obj.StartDate.Value.ToShortDateString() + " is before the start date " + project.StartDateEng.Value.ToShortDateString()
                        + "  of the project " + project.Name + " for the row " + row;
                    status.IsSuccess = false;
                    return status;
                }

                if (obj.EndDate != null && PayrollDataContext.Projects.Any(x => x.ProjectId == obj.ProjectId && obj.EndDate > x.EndDateEng))
                {
                    Project project = PayrollDataContext.Projects.FirstOrDefault(x => x.ProjectId == obj.ProjectId && obj.EndDate > x.EndDateEng);
                    status.ErrorMessage = "Employee assign end date " + obj.EndDate.Value.ToShortDateString() + " is after the end date " + project.EndDateEng.Value.ToShortDateString()
                        + " of the project " + project.Name + " for the row " + row; ;
                    status.IsSuccess = false;
                    return status;
                }



                ProjectEmployee dbProjectEmp = dbProjectEmployeeList.FirstOrDefault(x => x.ProjectId == obj.ProjectId && x.EmployeeId == obj.EID
                         &&
                        (
                        (obj.StartDate >= x.StartDate && obj.StartDate <= x.EndDate) ||
                        (obj.EndDate >= x.StartDate && obj.EndDate <= x.EndDate) ||

                        (x.StartDate >= obj.StartDate && x.StartDate <= obj.EndDate) ||
                        (x.EndDate >= obj.StartDate && x.EndDate <= obj.EndDate)
                        )
                        );



                //if (PayrollDataContext.ProjectEmployees.Any(x => x.ProjectId == obj.ProjectId && x.EmployeeId == obj.EID
                //        && obj.ID != x.ID
                //        &&
                //        (
                //        (obj.StartDate >= x.StartDate && obj.StartDate <= x.EndDate) ||
                //        (obj.EndDate >= x.StartDate && obj.EndDate <= x.EndDate) ||

                //        (x.StartDate >= obj.StartDate && x.StartDate <= obj.EndDate) ||
                //        (x.EndDate >= obj.StartDate && x.EndDate <= obj.EndDate)
                //        )
                //        )

                //        )
                //{
                //    status.ErrorMessage = "Project  " + obj.ProjectName + " already assigned for this date for employee " + 
                //        obj.EmployeeName+ " for the row " + row;
                //    status.IsSuccess = false;
                //    return status;
                //}
                


                if (obj.ID == 0)
                {
                    if (PayrollDataContext.ProjectEmployees.Any(x => x.ProjectId == obj.ProjectId && x.EmployeeId == obj.EID && x.EndDate == null))
                    {
                        //status.ErrorMessage = "This Project not closed for this employee.";
                        status.ErrorMessage = "Project  " + obj.ProjectName + " not closed for employee " +
                            obj.EmployeeName + " for the row " + row;
                        status.IsSuccess = false;
                        return status;
                    }

                }



                if (obj.ID.Equals(0) && dbProjectEmp == null)
                {
                    dbProjectEmp = new ProjectEmployee();
                    dbProjectEmp.EmployeeId = obj.EID;
                    dbProjectEmp.ProjectId = obj.ProjectId;
                    dbProjectEmp.StartDate = obj.StartDate.Value;
                    if(obj.EndDate!=null)
                    dbProjectEmp.EndDate = obj.EndDate;
                    dbProjectEmp.ID = obj.ID;
                    dbProjectEmp.ContractID = obj.ContractID;
                    PayrollDataContext.ProjectEmployees.InsertOnSubmit(dbProjectEmp);
                    
                    status.IsSuccess = true;
                }
                else
                {
                    ProjectEmployee dbEntity = PayrollDataContext.ProjectEmployees.FirstOrDefault(x => x.ID == obj.ID);
                    dbEntity.StartDate = obj.StartDate.Value;
                    if(obj.EndDate!=null)
                        dbEntity.EndDate = obj.EndDate;
                    dbEntity.ProjectId = obj.ProjectId;
                    dbEntity.ContractID = obj.ContractID;
                    
                    status.IsSuccess = true;
                }


                ProjectEmployeeActivity dbActivity = dbActivityList
                    .FirstOrDefault(x => x.EmployeeId == obj.EID && x.ProjectId == obj.ProjectId);
                if (dbActivity != null)
                    dbActivity.Activity = obj.Activity;
                else
                {
                    dbActivity = new ProjectEmployeeActivity();
                    dbActivity.EmployeeId = obj.EID;
                    dbActivity.ProjectId = obj.ProjectId;
                    dbActivity.Activity = obj.Activity;
                    PayrollDataContext.ProjectEmployeeActivities.InsertOnSubmit(dbActivity);
                }

            }

            PayrollDataContext.SubmitChanges();

            return status;
        }

        public static Status DeleteProjectAssociation(int ID)
        {
            Status status = new Status();

            //if (PayrollDataContext.TimesheetProjects.Any(x => x.ProjectId == ID))
            //{
            //    status.ErrorMessage = "Publication type is in use, cannot be deleted.";
            //    status.IsSuccess = false;
            //    return status;
            //}

            ProjectEmployee dbEntity = PayrollDataContext.ProjectEmployees.SingleOrDefault(x => x.ID == ID);
            PayrollDataContext.ProjectEmployees.DeleteOnSubmit(dbEntity);
            PayrollDataContext.SubmitChanges();
            status.IsSuccess = true;
            return status;
        }

        public static ProjectEmployee GetProjectAssociationByID(int AssociationID)
        {
            return PayrollDataContext.ProjectEmployees.SingleOrDefault(x => x.ID == AssociationID);
        }


        public static List<GetEmployeeListForProjectEmpResult> GetEmployeeListForPrjEmp(int currentPage, int pageSize, int employeeId, ref int total)
        {
            currentPage -= 1;
            List<GetEmployeeListForProjectEmpResult> list = PayrollDataContext.GetEmployeeListForProjectEmp(currentPage, pageSize, SessionManager.CurrentCompanyId, employeeId).ToList();
            foreach (var item in list)
            {
                item.ProjectNameList = ProjectManager.GetProjectList(item.EmployeeId);
            }
            total = list.Count;
            return list;
        }
        public static List<Project> GetProjects()
        {

            List<Project> projects =
                PayrollDataContext.Projects.Where(pp => pp.CompanyId == SessionManager.CurrentCompanyId)
                    .ToList();
            Project p = new Project();
            p.ProjectId = 0;
            p.Name = "All Projects";
            projects.Insert(0,p);

            return projects;
        }

        public static List<Project> GetProjectsAssociatedWithIncome(int incomeId)
        {
            return

                (
                from p in PayrollDataContext.Projects
                join pi in PayrollDataContext.ProjectIncomes on p.ProjectId equals pi.ProjectId
                where pi.IncomeId == incomeId
                select p
                ).OrderBy(x=>x.Name).ToList();
        }

        public static List<Project> GetProjectsForLatestPayroll()
        {

            PayrollPeriod lastPayroll = CommonManager.GetLastPayrollPeriod();

            if (lastPayroll == null)
                return new List<Project>();

            List<Project> projects =
                PayrollDataContext.Projects.Where(
                pp => pp.CompanyId == SessionManager.CurrentCompanyId
                &&
                (
                    lastPayroll.StartDateEng >= pp.StartDateEng && lastPayroll.StartDateEng <= pp.EndDateEng
                    ||
                    lastPayroll.EndDateEng >= pp.StartDateEng && lastPayroll.EndDateEng <= pp.EndDateEng

                )
                )
                   .OrderBy(x=>x.Name) .ToList();
            Project p = new Project();
            p.ProjectId = 0;
            p.Name = "All Projects";
            projects.Insert(0, p);

            return projects;
        }

        public static bool DeleteProject(int projectId)
        {
            int count = PayrollDataContext.ProjectIncomes.Where(p => p.ProjectId == projectId).Count();
            if (count > 0)
                return false;
            count = PayrollDataContext.ProjectPays.Where(p => p.ProjectId == projectId).Count();

            if (count > 0)
                return false;

            Project entity = GetProjectById(projectId);
            if (entity != null)
            {
                PayrollDataContext.Projects.DeleteOnSubmit(entity);
                return DeleteChangeSet();
            }
            return false;
        }

        public static Project GetProjectById(int projectId)
        {
            return PayrollDataContext.Projects.SingleOrDefault(p => p.ProjectId == projectId);
        }

        public static bool SaveUpdateProject(Project project)
        {
            project.ModifedOn = GetCurrentDateAndTime();
            project.ModifiedBy = SessionManager.User.UserName;
            project.StartDateEng = GetEngDate(project.StartDate, IsEnglish);
            if (string.IsNullOrEmpty(project.EndDate))
                project.EndDateEng = project.StartDateEng.Value.Date.AddYears(100);
            else
                project.EndDateEng = GetEngDate(project.EndDate, IsEnglish);

            if (PayrollDataContext.Projects.Any(x => x.Name.ToLower() == project.Name.ToLower() && x.ProjectId != project.ProjectId))
            {
                return false;
            }

            if (project.ProjectId == 0)
            {
                project.CreatedOn = GetCurrentDateAndTime();
                project.CreatedBy = SessionManager.User.UserName;
                PayrollDataContext.Projects.InsertOnSubmit(project);
                return SaveChangeSet();
            }
            else
            {
                Project dbEntity = PayrollDataContext.Projects.SingleOrDefault(p => p.ProjectId == project.ProjectId);
                if (dbEntity != null)
                {
                    CopyObject<Project>(project, ref dbEntity,"");
                    UpdateChangeSet();
                    return true;
                }
            }
            return false;
        }


        public static List<GetProjectsResult> GetProjectList(DateTime startDate, DateTime endDate, bool isAll, string searchText, int currentPage, int pageSize, ref int total)
        {

            currentPage -= 1;

            List<GetProjectsResult> list =
                PayrollDataContext.GetProjects(currentPage, pageSize,SessionManager.CurrentCompanyId,isAll,startDate,endDate, searchText).ToList();

            total = 0;
            if (list.Count > 0)
                total = list[0].TotalRows.Value;

            return list;


                
        }

       
        public static bool SaveProjectReallocation(ProjectReallocation entity)
        {
        //    ProjectReallocation dbEntity =
        //        PayrollDataContext.ProjectReallocations.SingleOrDefault(
        //            p => p.IncomeId == entity.IncomeId &&
        //        (

        //            entity.FromProjectId == p.FromProjectId || entity.FromProjectId == p.ToProjectId
                    
        //                //(p.FromProjectId == entity.FromProjectId || p.FromProjectId == entity.ToProjectId)
        //                //||
        //                //(p.ToProjectId == entity.FromProjectId || p.ToProjectId == entity.ToProjectId)
        //        ));

        //    if (dbEntity != null)
        //        return false;


            PayrollDataContext.ProjectReallocations.InsertOnSubmit(entity);
            return SaveChangeSet();

        }
        public static bool DeleteProjectReallocation(ProjectReallocation entity)
        {
            ProjectReallocation dbEntity =
                PayrollDataContext.ProjectReallocations
                .SingleOrDefault(p => p.IncomeId == entity.IncomeId
                    && p.FromProjectId == entity.FromProjectId && p.ToProjectId == entity.ToProjectId);

            if (dbEntity != null)
                PayrollDataContext.ProjectReallocations.DeleteOnSubmit(dbEntity);
            return DeleteChangeSet();
        }

        public static List<ProjectReallocationEntity> GetProjectReallocation()
        {
            List<ProjectReallocationEntity> list =
                (
                    from p in PayrollDataContext.ProjectReallocations
                    join p1 in PayrollDataContext.Projects on p.FromProjectId equals p1.ProjectId
                    join p2 in PayrollDataContext.Projects on p.ToProjectId equals p2.ProjectId
                    //left join 
                    join i in PayrollDataContext.PIncomes on p.IncomeId equals i.IncomeId into i1
                     from i2 in i1.DefaultIfEmpty()
                    
                    select new ProjectReallocationEntity
                    {
                        IncomeId = p.IncomeId,
                        IncomeText = i2 != null ? i2.Title : "Provident Fund",

                        FromProjectId = p1.ProjectId,
                        FromProjectText = p1.Name,

                        ToProjectId = p2.ProjectId,
                        ToProjectText = p2.Name
                    }

                    ).ToList();

            return list;
        }

        public static List<PIncome> GetIncomesForProjects()
        {
            List<PIncome> list =  PayrollDataContext.PIncomes.Where(ii => ii.CompanyId == SessionManager.CurrentCompanyId)
                .OrderBy(ii => ii.Order).ToList();

            if (SessionManager.CurrentCompany.HasPFRFFund)
            {

                PIncome i = new PIncome();
                i.IncomeId = 0;
                i.Title = SessionManager.CurrentCompany.PFRFName;
                list.Insert(0, i);

                PIncome ii = new PIncome();
                ii.IncomeId = -2;
                ii.Title = "Leave Encashment";

                list.Insert(1, ii);


                ii = new PIncome();
                ii.IncomeId = -3;
                ii.Title = "Gratuity";
                list.Insert(1, ii);
            }
            return list;
        }

        public static void SaveProjectIncomeList(List<ProjectIncome> list )
        {
            List<ProjectIncome> dbList = PayrollDataContext.ProjectIncomes.ToList();
          

            foreach (var projectIncome in dbList)
            {
                PayrollDataContext.ProjectIncomes.DeleteOnSubmit(projectIncome);
            }

            //DeleteChangeSet();

            foreach (var projectIncome in list)
            {
                PayrollDataContext.ProjectIncomes.InsertOnSubmit(projectIncome);
            }
            SaveChangeSet();
        }

        public static List<ProjectIncome> GetSelectedProjectIncomes()
        {
            return
                (from pi in PayrollDataContext.ProjectIncomes
                 join p in PayrollDataContext.Projects on pi.ProjectId equals p.ProjectId
                 where p.CompanyId == SessionManager.CurrentCompanyId
                 
                 select pi
                ).ToList();

        }


        public static void SaveProjectRateContribution(List<ProjectEmployeeContribution> projectPays)
        {
            // Update Hours or Percent


            foreach (var project in projectPays)
            {

                ProjectEmployeeContribution dbProjectContribution = PayrollDataContext.ProjectEmployeeContributions.SingleOrDefault(p => p.ProjectId == project.ProjectId && p.EmployeeId == project.EmployeeId);
                if (dbProjectContribution != null)//update
                {
                    dbProjectContribution.Rate = project.Rate;
                }
                else
                {
                    if (project.Rate > 0)
                    {
                        PayrollDataContext.ProjectEmployeeContributions.InsertOnSubmit(project);
                    }
                }
            }

            SaveChangeSet();
        }



        public static void SaveProjectEmployeeActivity(List<ProjectEmployeeActivity> projectPays)
        {
            // Update Hours or Percent


            foreach (var project in projectPays)
            {

                ProjectEmployeeActivity dbProjectEmployeeActivity = PayrollDataContext.ProjectEmployeeActivities.SingleOrDefault(p => p.ProjectId == project.ProjectId && p.EmployeeId == project.EmployeeId);
                if (dbProjectEmployeeActivity != null)//update
                {
                    dbProjectEmployeeActivity.Activity = project.Activity;
                }
                else
                {
                    if (project.Activity > 0)
                    {
                        PayrollDataContext.ProjectEmployeeActivities.InsertOnSubmit(project);
                    }
                }
            }

            SaveChangeSet();
        }


        public static void UpdateProjectPay(List<ProjectPay> projectPays, int payrollPeriodId, int incomeId)
        {
            // Update Hours or Percent
            if (incomeId < 0)
            {

                List<ProjectPay> ListProjectsPay = PayrollDataContext.ProjectPays.Where(p => p.PayrollPeriodId == payrollPeriodId).ToList();
                foreach (ProjectPay item in projectPays)
                {
                    ProjectPay dbProjectPay = ListProjectsPay.SingleOrDefault(p => p.EmployeeId == item.EmployeeId && p.ProjectId == item.ProjectId);
                    if (dbProjectPay != null)
                        dbProjectPay.HoursOrDays = item.HoursOrDays;
                    else
                    {
                        PayrollDataContext.ProjectPays.InsertOnSubmit(item);
                    }
                }
                SaveChangeSet();
            }
            // Change Generated Report values
            else
            {

                List<ProjectReport> projects = PayrollDataContext.ProjectReports.Where(p => p.PayrollPeriodId == payrollPeriodId).ToList();
                List<ProjectPayBO> list = new List<ProjectPayBO>();

                // Iterate for each Amount & update
                foreach (ProjectPay pay in projectPays)
                {

                    ProjectReportDetail detail =
                        (from h in PayrollDataContext.ProjectReportHeaders
                         join e in PayrollDataContext.ProjectReportEmployees on h.ProjectReportId equals e.ProjectReportId
                         join pr in PayrollDataContext.ProjectReports on e.ProjectReportId equals pr.ProjectReportId

                         join d in PayrollDataContext.ProjectReportDetails
                         on new { h.ProjectReportHeaderId, e.ProjectReportEmployeeId } equals new { d.ProjectReportHeaderId, d.ProjectReportEmployeeId }

                         where e.EmployeeId == pay.EmployeeId &&
                            pr.PayrollPeriodId == payrollPeriodId && pr.ProjectId == pay.ProjectId && h.IncomeId == incomeId && h.Type == 3

                         select d
                          ).SingleOrDefault();



                    if (detail != null)
                    {
                        if (pay.HoursOrDays == null)
                            detail.Amount = 0;
                        else
                            detail.Amount = (decimal)pay.HoursOrDays;
                    }
                    // else if row doesn't exists then try to insert

                }

                UpdateChangeSet();
            }

        }


        public static void SaveProjectPay(List<ProjectPay> projectPays, int payrollPeriodId, int incomeId)
        {
            // Update Hours or Percent


            if (incomeId < 0)
            {
                List<ProjectPay> dbProjectsPay = PayrollDataContext.ProjectPays.Where(p => p.PayrollPeriodId == payrollPeriodId).ToList();

                foreach (var project in dbProjectsPay)
                {
                    PayrollDataContext.ProjectPays.DeleteOnSubmit(project);
                }

                foreach (var projectIncome in projectPays)
                {
                    PayrollDataContext.ProjectPays.InsertOnSubmit(projectIncome);
                }
                SaveChangeSet();
            }
            // Change Generated Report values
            else
            {

                List<ProjectReport> projects = PayrollDataContext.ProjectReports.Where(p => p.PayrollPeriodId == payrollPeriodId).ToList();
                List<ProjectPayBO> list = new List<ProjectPayBO>();

                // Iterate for each Amount & update
                foreach (ProjectPay pay in projectPays)
                {

                    ProjectReportDetail detail =
                        (from h in PayrollDataContext.ProjectReportHeaders
                         join e in PayrollDataContext.ProjectReportEmployees on h.ProjectReportId equals e.ProjectReportId
                         join pr in PayrollDataContext.ProjectReports on e.ProjectReportId equals pr.ProjectReportId

                         join d in PayrollDataContext.ProjectReportDetails
                         on new { h.ProjectReportHeaderId, e.ProjectReportEmployeeId } equals new { d.ProjectReportHeaderId, d.ProjectReportEmployeeId }

                         where e.EmployeeId == pay.EmployeeId &&
                            pr.PayrollPeriodId == payrollPeriodId && pr.ProjectId == pay.ProjectId && h.IncomeId == incomeId && h.Type == 3

                         select d
                          ).SingleOrDefault();



                    if (detail != null)
                    {
                        if (pay.HoursOrDays == null)
                            detail.Amount = 0;
                        else
                            detail.Amount = (decimal)pay.HoursOrDays;
                    }
                    // else if row doesn't exists then try to insert

                }

                UpdateChangeSet();
            }

        }



        public static double GetProjectRateByEmployeeID(int EmployeeID,int ProjectID)
        {
            // Get Hours or Percent if no income selected
            ProjectEmployeeContribution _ProjectEmployeeContributions = PayrollDataContext.ProjectEmployeeContributions.SingleOrDefault(x => x.ProjectId == ProjectID && x.EmployeeId == EmployeeID);
            if (_ProjectEmployeeContributions != null)
                return _ProjectEmployeeContributions.Rate.Value;
            else return 0;

        }


        public static double GetProjectActivityByEmployeeID(int EmployeeID, int ProjectID)
        {
            // Get Hours or Percent if no income selected
            ProjectEmployeeActivity _ProjectEmployeeActivity = PayrollDataContext.ProjectEmployeeActivities.SingleOrDefault(x => x.ProjectId == ProjectID && x.EmployeeId == EmployeeID);
            if (_ProjectEmployeeActivity != null && _ProjectEmployeeActivity.Activity != null)
                return _ProjectEmployeeActivity.Activity.Value;
            else return 0;

        }


        public static List<ProjectPayBO> GetProjectRateInputs()
        {
            // Get Hours or Percent if no income selected
           
                return (
                    from p in PayrollDataContext.ProjectPays
                    select new ProjectPayBO
                    {
                        EmployeeId = p.EmployeeId,
                        PayrollPeriodId = p.PayrollPeriodId,
                        ProjectId = p.ProjectId,
                        ProjectName = p.ProjectName,
                        HoursOrDays = p.HoursOrDays
                    }
                    ).ToList();
            
        }

        public static List<ProjectPayBO> GetProjectPays(int payrollPeriodId, int incomeId)
        {
            // Get Hours or Percent if no income selected
            if (incomeId < 0)
            {
                return (
                    from p in PayrollDataContext.ProjectPays
                    where p.PayrollPeriodId == payrollPeriodId
                    select new ProjectPayBO
                    {
                        EmployeeId = p.EmployeeId,
                        PayrollPeriodId = p.PayrollPeriodId,
                        ProjectId = p.ProjectId,
                        ProjectName = p.ProjectName,
                        HoursOrDays = p.HoursOrDays
                    }
                    ).ToList();
            }
            // If Income selected then get the Project Report amount to be modified
            else
            {

                List<ProjectReport> projects = PayrollDataContext.ProjectReports.Where(p => p.PayrollPeriodId == payrollPeriodId).ToList();
                List<ProjectPayBO> list = new List<ProjectPayBO>();

                foreach (ProjectReport project in projects)
                {

                    list.AddRange
                    (
                        (from h in PayrollDataContext.ProjectReportHeaders
                         join e in PayrollDataContext.ProjectReportEmployees on h.ProjectReportId equals e.ProjectReportId

                         join d in PayrollDataContext.ProjectReportDetails
                         on new { h.ProjectReportHeaderId, e.ProjectReportEmployeeId } equals new { d.ProjectReportHeaderId, d.ProjectReportEmployeeId }

                         where e.ProjectReportId == project.ProjectReportId && h.IncomeId == incomeId && h.Type == 3
                         select new ProjectPayBO
                         {
                             EmployeeId = e.EmployeeId,
                             PayrollPeriodId = payrollPeriodId,
                             ProjectId = project.ProjectId,
                             ProjectName = project.ProjectName,
                             HoursOrDays = (double)d.Amount
                         }
                       )
                    .ToList()
                   );

                }
                return list;

            }

        }
        public static List<EEmployee> GetProjectPaysEmployeeList(int payrollPeriodId, int incomeId)
        {
            //if (incomeId < 0 && IsDashainMonth(payrollPeriodId) == false)
            //{
            //    return
            //           (
            //               from pp in PayrollDataContext.ProjectPays
            //               join e in PayrollDataContext.EEmployees on pp.EmployeeId equals e.EmployeeId

            //               where pp.PayrollPeriodId == payrollPeriodId
            //               orderby e.EmployeeId
            //               select e
            //           ).Distinct().ToList();

            //}
            //else
            {
                List<GetEmployeeListForProjectReportResult> empList =
                    PayrollDataContext.GetEmployeeListForProjectReport(
                    payrollPeriodId, null, 0, SessionManager.CurrentCompanyId).ToList();

                List<EEmployee> list = new List<EEmployee>();
                foreach (GetEmployeeListForProjectReportResult result in empList)
                {
                    list.Add(new EEmployee { EmployeeId = result.EmployeeId.Value, Name = result.Name });
                }

                return list;
            }
        }

        /// <summary>
        /// Employee Filter for Project as in NSET we need to bring reg in dashain year 
        /// </summary>
        public static bool IsDashainMonth(int payrollPeriodId)
        {
            int? OfMonth =
                (
                from p in PayrollDataContext.PIncomes
                where p.CompanyId == SessionManager.CurrentCompanyId && p.IsApplyOnceFor == true
                select p.OFMonth
                ).FirstOrDefault();

            if (OfMonth == null || OfMonth <= 0)
            {
                return false;

            }

            PayrollPeriod payroll = CommonManager.GetPayrollPeriod(payrollPeriodId);

            if (OfMonth == payroll.Month)
            {
                return true;
            }
            return false;
        }

        public static List<Project> GetProjectForPays(int payrollPeriodId)
        {
            return
                (
                    from pp in PayrollDataContext.ProjectPays
                    join p in PayrollDataContext.Projects on pp.ProjectId equals p.ProjectId
                    where pp.PayrollPeriodId == payrollPeriodId
                    orderby p.ProjectId
                   select p 
                ).Distinct().OrderBy(x=>x.Name).ToList();
        }

        public static List<Project> GetProjectListForReport(int payrollperiodId)
        {
            return
                (
                    from pp in PayrollDataContext.ProjectReports
                    join p in PayrollDataContext.Projects on pp.ProjectId equals p.ProjectId
                    where pp.PayrollPeriodId == payrollperiodId && p.CompanyId== SessionManager.CurrentCompanyId
                    orderby p.ProjectId
                    select p
                ).Distinct().OrderBy(x=>x.Name).ToList();
        }

        public static List<Report_Project_HeaderListResult> GetProjectPayHeaderList(int payrollPeriodId,int companyId,int projectId)
        {
            return PayrollDataContext.Report_Project_HeaderList(payrollPeriodId, companyId, projectId).ToList();
        }

        public static  List<Report_Project_EmployeeListResult> GetEmployeePayEmployeeList(int payrollPeriodId,int projectId)
        {
            return PayrollDataContext.Report_Project_EmployeeList(payrollPeriodId, projectId).ToList();
        }

        public static List<Report_Project_Consolidated_HeaderListResult> GetConslidatedHeaderist(int payrollFromId,int payrollToId)
        {
             List<Report_Project_Consolidated_HeaderListResult>  list=
                PayrollDataContext.Report_Project_Consolidated_HeaderList(payrollFromId, payrollToId,
                                                                          SessionManager.CurrentCompanyId)
                    .ToList();

            //add total for each GroupName
            if( list.Count>0)
            {
                string groupName = list[0].HeaderName;
                for(int i=0;i<list.Count;i++)
                {
                    if( groupName != list[i].HeaderName)
                    {
                        Report_Project_Consolidated_HeaderListResult totalHeader = new Report_Project_Consolidated_HeaderListResult();
                        totalHeader.Type = 5;//list[i].Type;
                        totalHeader.SourceId = 5;
                        //totalHeader.ProjectName = list[i - 1].ProjectName;
                        totalHeader.ProjectName = "Total";
                        totalHeader.ProjectId = list[i - 1].ProjectId;

                       

                        totalHeader.HeaderName = list[i-1].HeaderName;
                        //new Group name
                        groupName = list[i].HeaderName;

                        list.Insert(i, totalHeader);

                        i++;
                    }
                }

                //add total for last group
                Report_Project_Consolidated_HeaderListResult lastTotalHeader = new Report_Project_Consolidated_HeaderListResult();
                lastTotalHeader.Type = 5;//list[i].Type;
                lastTotalHeader.SourceId = 5;
                //totalHeader.ProjectName = list[i - 1].ProjectName;
                lastTotalHeader.ProjectName = "Total";
                lastTotalHeader.ProjectId = list[list.Count - 1].ProjectId;
                lastTotalHeader.HeaderName = list[list.Count - 1].HeaderName;
                list.Insert(list.Count, lastTotalHeader);

            }

            return list;
        }



        public static bool UpdateProjectSetting(int hoursInAMonth)
        {
            PayrollDataContext.UpdateProjectSetting(hoursInAMonth);
            return true;
        }

        public static int TotalHourOrDaysInAMonth
        {
            get
            {
                int? value =
                PayrollDataContext.GetProjectSetting(0);

                if (value == null)
                    return 0;
                return value.Value;
            }
        }

        public static List<Report_Project_ConsolidatedEmployeeListResult> GetConsolidatedEmployeeList(int payrollFrom,int payrollTo
            ,int costCodeId,int programID)
        {
            return
                PayrollDataContext.Report_Project_ConsolidatedEmployeeList(payrollFrom, payrollTo,costCodeId,programID)
                    .ToList();
        }

        public static List<GetProjectAssociationResult> GetProjectAssociation(int StartRow, int PageSize, string Sort, int? EmployeeID, int?ProjectID , DateTime? StartDate, DateTime? EndDate)
        {
            List<GetProjectAssociationResult> list = PayrollDataContext.GetProjectAssociation(StartRow, PageSize, Sort,EmployeeID, ProjectID, StartDate, EndDate).ToList();
            return list;
        }

        public static List<GetMonthlyProjectAssociationResult> GetMonthlyProjectAssociation(int StartRow, int PageSize, string Sort, int? EmployeeID, int? ProjectID, DateTime? StartDate, DateTime? EndDate)
        {
            List<GetMonthlyProjectAssociationResult> list = PayrollDataContext.GetMonthlyProjectAssociation
                (StartRow, PageSize, Sort, EmployeeID, ProjectID, StartDate, EndDate).ToList();
            return list;
        }
    }

}
