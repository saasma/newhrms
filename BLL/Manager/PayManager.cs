﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using DAL;
using Utils;
using System.Xml.Linq;
using Utils.Calendar;
using BLL.Entity;
using BLL.BO;

namespace BLL.Manager
{
    public class PayManager : BaseBiz
    {
        //public static void DeleteCurrentMonthLoanAdustment(int period, int empId, int deductionid)
        //{
        //    LoanAdjustment entity = PayrollDataContext.LoanAdjustments.FirstOrDefault
        //            (x => x.EmployeeId == empId && x.PayrollPeriodId == period && x.DeductionId == deductionid);

        //    if (entity != null)
        //    {
        //        PayrollDataContext.LoanAdjustments.DeleteOnSubmit(entity);
        //        PayrollDataContext.SubmitChanges();
        //    }
        //}
        
        /// <summary>
        /// Check if arrear increment exists for Level/Grade company
        /// </summary>
        /// <returns></returns>
        public static bool HasArrearIncrementInLastPeriod()
        {
            if (CommonManager.CompanySetting.HasLevelGradeSalary == false)
                return false;

            PayrollPeriod period = CommonManager.GetLastPayrollPeriod();

            if (period == null)
                return false;

            if (CalculationManager.IsAllEmployeeSavedFinally(period.PayrollPeriodId, false) == true)
                return false;

            if (PayrollDataContext.RetrospectIncrements.Any(x => x.PayrollPeriodId == period.PayrollPeriodId))
            {
                return true;
            }

            return PayrollDataContext.IsArrearIncrementExists(period.PayrollPeriodId).Value;
        }
        public static void SavePayrollSignOff(PayrollPeriodSignOff entity)
        {
            PayrollDataContext.PayrollPeriodSignOffs.InsertOnSubmit(entity);
            PayrollDataContext.SubmitChanges();
        }

        public static List<int> niblCompulsoryIncomesID = new List<int> { 2, 9, 35 };
        public static List<int> niblCompulsoryHomeSalaryIncomesID = new List<int> { 2, 9, 35, 36, 37, 38 };

        public static List<TextValue> GetEmployeeIncomes(int empId, DateTime date,DateTime? createdOn, string eventType,int? type,int? sourceID,int? statusId
            ,int totalEvents,string eventName)
        {

            // for NIBL
            

            // if promotion then pick from increment table having first rows
            //if (eventType.Trim().ToLower() == ServiceEventType.Promotion.ToString().ToLower())
            //{
            String result = String.Join(",", niblCompulsoryIncomesID.Select(item => item.ToString()).ToArray());
            //}
            //else
            //{
                List<TextValue> list =  
                    (
                       from ii in PayrollDataContext.GetIncomeAmountForEvent(empId,date,createdOn,
                          result, type, sourceID, totalEvents, eventName)
                       select new TextValue
                       {
                           Text = ii.Title,
                           Value = GetCurrency(ii.Amount),
                           Value2 = ii.IncomeId
                       }
                    ).ToList();

           // }


            PIncome basic = new PayManager().GetBasicIncome(SessionManager.CurrentCompanyId);

            Company company = SessionManager.CurrentCompany;
            if (statusId != null && statusId >= company.PFRFFunds[0].EffectiveFrom && list.Any(x => x.Value2 == basic.IncomeId))
            {
                list.Add(new TextValue { Text = "PF", Value = 
                    GetCurrency((Convert.ToDecimal(0.1) * Convert.ToDecimal(list.FirstOrDefault(x => x.Value2 == basic.IncomeId).Value))) });
            }


            TextValue total = new TextValue
            {
                Text = "Total",
                Value = GetCurrency(list.Sum(x => Convert.ToDecimal(x.Value)))
            };


            list.Add(total);

            return list;
        }
        public static List<TextValue> GetEmployeeIncomesNew(int empId, DateTime date, int eventSourceID,int? statusId)
        {

            // for NIBL


            // if promotion then pick from increment table having first rows
            //if (eventType.Trim().ToLower() == ServiceEventType.Promotion.ToString().ToLower())
            //{
            String result = String.Join(",", niblCompulsoryIncomesID.Select(item => item.ToString()).ToArray());
            //}
            //else
            //{
            List<TextValue> list =
                (
                   from ii in PayrollDataContext.GetIncomeAmountForEventNew(empId, date,
                      result, eventSourceID)
                   select new TextValue
                   {
                       Text = ii.Title,
                       Value = GetCurrency(ii.Amount),
                       Value2 = ii.IncomeId
                   }
                ).ToList();

            // }


            PIncome basic = new PayManager().GetBasicIncome(SessionManager.CurrentCompanyId);

            Company company = SessionManager.CurrentCompany;
            if (statusId != null && statusId >= company.PFRFFunds[0].EffectiveFrom && list.Any(x => x.Value2 == basic.IncomeId))
            {
                list.Add(new TextValue
                {
                    Text = "PF",
                    Value =
                        GetCurrency((Convert.ToDecimal(0.1) * Convert.ToDecimal(list.FirstOrDefault(x => x.Value2 == basic.IncomeId).Value)))
                });
            }


            TextValue total = new TextValue
            {
                Text = "Total",
                Value = GetCurrency(list.Sum(x => Convert.ToDecimal(x.Value)))
            };


            list.Add(total);

            return list;
        }


        public static string GetSalarySaveRemainingList(int payrollPeriodId)
        {
            List<GetSalaryNotSaveEmployeeListResult> list =
                PayrollDataContext.GetSalaryNotSaveEmployeeList(payrollPeriodId).ToList();

            string str = "";
            for (int i = 0; i < list.Count; i++)
            {
                if(str=="")
                    str = list[i].Name + " - " + list[i].EmployeeId;
                else
                    str += ", " +  list[i].Name + " - " + list[i].EmployeeId;
            }

            if (list.Count == 11)
                str += " ...";

            return str;
        }

        public static string GetCalculationHistory(int payrollPeriodId)
        {
            
            //EmployeeManager empMgr = new EmployeeManager();
            List<PayrollPeriodSignOff> statuses = PayrollDataContext.PayrollPeriodSignOffs
                .Where(x => x.PayrollPeriodId == payrollPeriodId).OrderByDescending(x => x.SignOffDate).ToList();

            StringBuilder str = new StringBuilder("<table class='tableLightColor' style='margin-top:10px'>");
            str.Append("<tr> <th style='width:100px;text-align:left'>Action</th> <th style='width:150px;text-align:left'>User</th> <th style='width:150px;text-align:left'>Date</th> </tr>");

           
            foreach(PayrollPeriodSignOff item in statuses)
            {
                UUser user = UserManager.GetUserByUserID(item.SignOffUserID.Value);

                str.AppendFormat(
                    @"<tr>
                        <td class='statusWidth' style='padding-left:3px;'>  {0}</td>
                        <td>&nbsp;{1}</td> 
                        <td> &nbsp;{2} </td>                    
                        
                    </tr>",
                    ((SalaryCaluclationChangedType)item.Type).ToString(),
                    (user ==null ? "" : (string.IsNullOrEmpty(user.NameIfNotEmployee) ? user.UserName : user.NameIfNotEmployee)),
                    item.SignOffDate.Value.ToString()

                    );
            }
            str.Append("</table>");
            return str.ToString();
        }

        public static string GetLoanAdjustmentHistory(int empId,int deductionId)
        {

            PEmployeeDeduction empDed = GetEmployeeDeduction(empId, deductionId);

            //EmployeeManager empMgr = new EmployeeManager();
            List<LoanAdjustment> statuses = PayrollDataContext.LoanAdjustments
                .Where(x => x.EmployeeId==empId && x.DeductionId == deductionId)
                .OrderBy(x => x.CreatedOn).ToList();

            List<LoanSIChangedHistory> freshLoanAddedHistory =
                PayrollDataContext.LoanSIChangedHistories.Where(x => x.EmployeeId == empId && x.LoanDeductionId == deductionId).ToList();

            foreach (LoanSIChangedHistory item in freshLoanAddedHistory)
            {
                LoanAdjustment adj = new LoanAdjustment();
                adj.CreatedBy = item.ChangedBy;
                adj.CreatedOn = item.ChangedDate;
                adj.AdjustmentType = (int)(LoanAdjustmentType.FreshLoanAdded);
                adj.FreshLoanHistory = item;

                statuses.Add(adj);
            }

            statuses = statuses.OrderBy(x => x.CreatedOn).ToList();


            StringBuilder str = new StringBuilder("<table class='tableLightColor' style='margin-top:10px'>");
            str.Append(@"<tr> <th style='width:90px;text-align:left'>Created Date</th> <th style='width:120px;text-align:left'>Type</th> <th style='width:90px;text-align:left'>Amount</th>
                <th style='width:120px;text-align:left'>Adjusted Interest</th>
                <th style='width:400px;text-align:left'>Option</th><th style='width:90px;text-align:left'>Date</th><th style='width:90px;text-align:left'>Nepali Date</th>
                </tr>");


            foreach (LoanAdjustment item in statuses)
            {

                if (item.AdjustmentType ==(int) (LoanAdjustmentType.FreshLoanAdded))
                {
                    str.AppendFormat(
                        @"<tr>
                        <td> &nbsp; {0}</td>
                        <td>&nbsp;{1}</td> 
                        <td> &nbsp;{2} </td>   

                        <td> &nbsp;{6} </td>                  
                        <td> &nbsp; {3}</td>
                        <td>&nbsp;{4}</td> 
                        <td> &nbsp;{5} </td>  

                    </tr>",

                              (item.CreatedOn != null ? item.CreatedOn.Value.ToString("yyyy-MMM-dd") : ""),
                              (item.AdjustmentType != null ? GetLoanAdjustmentEnumText((LoanAdjustmentType)item.AdjustmentType.Value) : ""),
                              (GetCurrency(item.FreshLoanHistory.LoanAmount)),
                              (
                                GetLoanOptionText(item)
                              ),
                              (item.FreshLoanHistory.TakenDate.ToString("yyyy-MMM-dd")),
                              GetAppropriateDate(item.FreshLoanHistory.TakenDate),
                              ""

                        );
                }
                else
                {

                    str.AppendFormat(
                        @"<tr>
                        <td> &nbsp; {0}</td>
                        <td>&nbsp;{1}</td> 
                        <td> &nbsp;{2} </td>   

                        <td> &nbsp;{6} </td>                  
                        <td> &nbsp; {3}</td>
                        <td>&nbsp;{4}</td> 
                        <td> &nbsp;{5} </td>  

                    </tr>",

                              (item.CreatedOn != null ? item.CreatedOn.Value.ToString("yyyy-MMM-dd") : ""),
                              (item.AdjustmentType != null ? GetLoanAdjustmentEnumText((LoanAdjustmentType)item.AdjustmentType.Value) : ""),
                              (item.AdjustmentAmount != null ? GetCurrency(item.AdjustmentAmount) : ""),
                              (
                                GetLoanOptionText(item)
                              ),
                              (item.AdjustmentDateEng != null ? item.AdjustmentDateEng.Value.ToString("yyyy-MMM-dd") : ""),
                              item.AdjustmentDate,
                              GetCurrency(item.IPMTAdjustment)

                        );
                }
            }
            str.Append("</table>");
            return str.ToString();
        }

       
        public static List<HistoryExport> GetLoanAdjustmentHistoryExport(int empId, int deductionId)
        {
            List<HistoryExport> list = new List<HistoryExport>();

            PEmployeeDeduction empDed = GetEmployeeDeduction(empId, deductionId);

            //EmployeeManager empMgr = new EmployeeManager();
            List<LoanAdjustment> statuses = PayrollDataContext.LoanAdjustments
                .Where(x => x.EmployeeId == empId && x.DeductionId == deductionId)//&& x.UniqueId == empDed.UniqueId)
                .OrderBy(x => x.CreatedOn).ToList();


            //            StringBuilder str = new StringBuilder("<table class='tableLightColor' style='margin-top:10px'>");
            //            str.Append(@"<tr> <th style='width:90px;text-align:left'>Created Date</th> <th style='width:90px;text-align:left'>Type</th> <th style='width:90px;text-align:left'>Amount</th>
            //                <th style='width:90px;text-align:left'>Adjusted Interest</th>
            //                <th style='width:400px;text-align:left'>Option</th><th style='width:90px;text-align:left'>Date</th><th style='width:90px;text-align:left'>Nepali Date</th>
            //                </tr>");


            foreach (LoanAdjustment item in statuses)
            {
                HistoryExport history = new HistoryExport();



                history.CreatedDate = (item.CreatedOn != null ? item.CreatedOn.Value.ToString("yyyy-MMM-dd") : "");

                history.Type = (item.AdjustmentType != null ? GetLoanAdjustmentEnumText((LoanAdjustmentType)item.AdjustmentType.Value) : "");
                history.Amount = (item.AdjustmentAmount != null ? GetCurrency(item.AdjustmentAmount) : "");
                history.AdjustedInterest = item.IPMTAdjustment == null ? "" : item.IPMTAdjustment.ToString();
                history.Option = GetLoanOptionText(item);
                history.Date = (item.AdjustmentDateEng != null ? item.AdjustmentDateEng.Value.ToString("yyyy-MMM-dd") : "");
                history.NepaliDate = item.AdjustmentDate;

                list.Add(history);
            }
            return list;
        }
        public static string GetLoanOptionText(LoanAdjustment entity)
        {
            if (entity == null)
                return "";

           
            // prev adjustment before this code
            if (entity.AdjustmentType == null)
                return "Partial settlement : " + (entity.AdjustedPPMT == null ? "" : entity.AdjustedPPMT.ToString())
                    + " : " + (entity.AdjustedInterestOnLoan == null  ? "" :  entity.AdjustedInterestOnLoan.ToString()); //old adjustment

           

            LoanAdjustmentType type = (LoanAdjustmentType)entity.AdjustmentType.Value;

            switch (type)
            {
                case LoanAdjustmentType.FreshLoanAdded:
                    return
                        (
                            "Interest : " + entity.FreshLoanHistory.InterestRate + ", Installment : " + entity.FreshLoanHistory.ToBePaidOver
                        )
                        // if interest rate also changed
                        +
                        (
                            entity.InterestPrev != entity.InterestNew ?
                            (
                               " Rate Changed : " +
                                (
                                    "Rate - " + entity.InterestPrev.ToString() + " > " + entity.InterestNew.ToString() + " | " +
                                    "Marke Rate - " + entity.MarketRatePrev.ToString() + " > " + entity.MarketRateNew.ToString()
                                )
                            ) : ""
                        )
                        ;
                case LoanAdjustmentType.FullSettlement:
                    return "Full settlement : " + ((LoanAdjustmentPayment)entity.AdjustmentPaymentOption).ToString() ;
                case LoanAdjustmentType.PartialSettlement:
                    return "Partial settlement : " + ((LoanAdjustmentPayment)entity.AdjustmentPaymentOption).ToString() + " : " +
                         ((LoanAdjustmentInstallmentOption)entity.AdjustmentInstallmentOption).ToString() +
                          " : " +
                        (
                            (
                                entity.AdjustmentInstallmentOption == (int)LoanAdjustmentInstallmentOption.DefineNewInstallmentWithFixedAmount
                                ? GetCurrency(entity.NewFixedInstallment) :
                                (entity.AddLoanNewInstallment != null ? entity.AddLoanNewInstallment.ToString() : "")
                            )
                        )
                        // if interest rate also changed
                        +
                        (
                            entity.InterestPrev != entity.InterestNew ?
                            (
                               " Rate Changed : " +
                                (
                                    "Rate - " + entity.InterestPrev.ToString() + " > " + entity.InterestNew.ToString() + " | " +
                                    "Marke Rate - " + entity.MarketRatePrev.ToString() + " > " + entity.MarketRateNew.ToString()
                                )
                            ) : ""
                        )
                        ;

                case LoanAdjustmentType.LoanAdded:
                    return  (((AddLoanInstallmentOption)entity.AdjustmentInstallmentOption.Value)).ToString()
                        + " : " +
                        (
                            entity.AdjustmentInstallmentOption == (int)LoanAdjustmentInstallmentOption.DefineNewInstallmentWithFixedAmount
                                ? GetCurrency(entity.NewFixedInstallment) :
                            (entity.AddLoanNewInstallment != null ? entity.AddLoanNewInstallment.ToString() : "")
                        )
                        // if interest rate also changed
                        + 
                        (
                            entity.InterestPrev != entity.InterestNew ?
                            (
                               "  Rate Changed  : " +
                                (
                                    "Rate - " + entity.InterestPrev.ToString() + " > " + entity.InterestNew.ToString() + " | " +
                                    "Marke Rate - " + entity.MarketRatePrev.ToString() + " > " + entity.MarketRateNew.ToString()
                                )
                            ) : ""
                        )
                        ;
                case LoanAdjustmentType.InterestRateChange:
                    return
                        (
                            "Rate - " + entity.InterestPrev.ToString() + " > " + entity.InterestNew.ToString() + " | " +
                            "Marke Rate - " + entity.MarketRatePrev.ToString() + " > " + entity.MarketRateNew.ToString()
                        );
                case LoanAdjustmentType.AdjustInterest:
                    return "";
            }

            return "";
        }

        public static string GetLoanAdjustmentEnumText(LoanAdjustmentType type)
        {
            switch (type)
            {
                case LoanAdjustmentType.FullSettlement:
                    return "Loan Adjusted";
                case LoanAdjustmentType.PartialSettlement:
                    return "Loan Adjusted";
                case LoanAdjustmentType.LoanAdded:
                    return "Loan Added";
                case LoanAdjustmentType.InterestRateChange:
                    return "Changed Interest Rate";
                case LoanAdjustmentType.AdjustInterest:
                    return "Adjusted Interest";
                case LoanAdjustmentType.FreshLoanAdded:
                    return "Fresh Loan Added"; 
            }

            return "";
        }

        /// <summary>
        /// Return true if sign off not being done for latest changes
        /// </summary>
        /// <returns></returns>
        public static bool HasSignOffNotBeingDoneForLatestChanges(ref DateTime lastDate)
        {
            PayrollPeriodSignOff latestSignOff =
                PayrollDataContext.PayrollPeriodSignOffs.Where(x=>x.Type==(int)SalaryCaluclationChangedType.Approved)
                .OrderByDescending(x => x.SignOffId).FirstOrDefault();

            if (latestSignOff == null)
            {

                latestSignOff =
                    PayrollDataContext.PayrollPeriodSignOffs.FirstOrDefault();

                if (latestSignOff != null)
                    lastDate = latestSignOff.SignOffDate.Value;
                else
                    lastDate = DateTime.Now;


                return true;
            }

            lastDate = latestSignOff.SignOffDate.Value;

            if (PayrollDataContext.EmployeeServiceHistories.Any(x => x.CreatedOn != null && x.CreatedOn > latestSignOff.SignOffDate.Value))
                return true;

            if (PayrollDataContext.HoldPayments.Any(x => x.CreatedDate >= latestSignOff.SignOffDate.Value))
                return true;

            if (PayrollDataContext.StopPayments.Any(x => x.CreatedDate >= latestSignOff.SignOffDate.Value))
                return true;

            //if (PayrollDataContext.ChangeMonetories
            //    .Any(x => x.DateEng.Value > latestSignOff.SignOffDate.Value &&

            //        (
            //            x.Type == (byte)MonetoryChangeLogTypeEnum.Income || x.Type == (byte)MonetoryChangeLogTypeEnum.Deduction ||
            //            x.Type == (byte)MonetoryChangeLogTypeEnum.Insurance || x.Type == (byte)MonetoryChangeLogTypeEnum.Increment ||
            //            x.Type == (byte)MonetoryChangeLogTypeEnum.PF || x.Type == (byte)MonetoryChangeLogTypeEnum.Status ||
            //            x.Type == (byte)MonetoryChangeLogTypeEnum.CIT || x.Type == (byte)MonetoryChangeLogTypeEnum.NoCIT ||
            //            x.Type == (byte)MonetoryChangeLogTypeEnum.MedicalTaxCredit || x.Type == (byte)MonetoryChangeLogTypeEnum.Loan ||
            //            x.Type == (byte)MonetoryChangeLogTypeEnum.Advance || x.Type == (byte)MonetoryChangeLogTypeEnum.OptimumPFAndCIT ||
            //            x.Type==(byte)MonetoryChangeLogTypeEnum.EmployeeAdded ||
            //            x.Type==(byte)MonetoryChangeLogTypeEnum.Gender ||
            //            x.Type==(byte)MonetoryChangeLogTypeEnum.TaxStatus
            //        )
            //    )
            //    )
            //{
            //    return true;
            //}

            return false;
        }

        public static List<Bank> GetBankList()
        {
            return
                PayrollDataContext.Banks.OrderBy(x => x.Name).ToList();
        }
        public static List<BankBranch> GetBankBranchList()
        {
            List<BankBranch> list =
                PayrollDataContext.BankBranches.OrderBy(x => x.Name).ToList();

            foreach (BankBranch item in list)
            {
                Bank bank = CommonManager.GetBank(item.BankID);
                if (bank != null)
                    item.BankName = bank.Name;
            }

            return list.OrderBy(x => x.BankName).ThenBy(x => x.Name).ToList();
        }

        public static List<BankBranch> GetBankBranchList(int bankId)
        {
            List<BankBranch> list =
                PayrollDataContext.BankBranches.Where(x=>x.BankID == bankId).OrderBy(x => x.Name).ToList();

            foreach (BankBranch item in list)
            {
                Bank bank = CommonManager.GetBank(item.BankID);
                if (bank != null)
                    item.BankName = bank.Name;
            }

            return list.OrderBy(x => x.BankName).ThenBy(x => x.Name).ToList();
        }
        public static List<VoucherHPLDepartment> GetVoucherDepList()
        {
            return
                PayrollDataContext.VoucherHPLDepartments.OrderBy(x => x.Name).ToList();
        }
        #region "Deduction deposit list"
        public bool Save(PDeductionDepositList entity)
        {
            PayrollDataContext.PDeductionDepositLists.InsertOnSubmit(entity);
            return SaveChangeSet();
        }

        public bool Update(PDeductionDepositList entity)
        {
            PDeductionDepositList dbEntity = PayrollDataContext.PDeductionDepositLists.SingleOrDefault(c => c.DeductionDepositListId == entity.DeductionDepositListId);
            if (dbEntity == null)
                return false;

            dbEntity.Title = entity.Title;
            return UpdateChangeSet();
        }
        public bool Delete(PDeductionDepositList entity)
        {
            return PayrollDataContext.DeleteDeductionDepositList(entity.DeductionDepositListId) == 1;
        }

        public List<GetDepositListByCompanyResult> GetDepositList(int companyId)
        {
            return PayrollDataContext.GetDepositListByCompany(companyId).ToList();
        }
        #endregion
        /// <summary>
        /// For new case we are allowing to create bank accont & choose bank in dropdown for Bank table used case
        /// 
        /// </summary>
        /// <returns></returns>
        public static bool IsBankDropDown()
        {
            // if bank created & used in any employee then only show
            if (PayrollDataContext.Banks.Any())
            {
                return true;
            }
            return false;
        }
        public static CalcGetIncomeValueResult GetCalcuatedForIncome(int employeeId, int incomeId,
            int payrollPeriodId)
        {
            bool leaveDeductionReqd = true;
            bool addTaxFromBeginningCase = false;

            return PayrollDataContext.CalcGetIncomeValue(employeeId, incomeId, payrollPeriodId, leaveDeductionReqd, addTaxFromBeginningCase,0,0,false)
                .SingleOrDefault();
        }

        public static List<CITIncome> GetCITIncomes()
        {
            return PayrollDataContext.CITIncomes.Where(x => x.CompanyId == SessionManager.CurrentCompanyId).ToList();
        }

        public static List<PEmployeeIncrement> GetSalaryIncrements(int employeeId)
        {
            return
                PayrollDataContext.PEmployeeIncrements.Where(x => x.PEmployeeIncome.EmployeeId == employeeId)
                .OrderBy(x => x.PEmployeeIncome.IncomeId)
                .ThenBy(x=>x.EffectiveFromDateEng).ToList();
        }

        #region "Income"

        public static List<CCalculationIncomeChangeHistory> GetIncomeChangeHistory(int incomeId, int employeeId)
        {
            List<CCalculationIncomeChangeHistory> list =
                PayrollDataContext.CCalculationIncomeChangeHistories.Where
                (c => c.IncomeId == incomeId && c.EmployeeId == employeeId)
                .OrderBy(c => c.Id).ToList();


            //set To date by checking if from date exists in next row, if yes, then current row has to date
            for (int i = 0; i < list.Count; i++)
            {
                //CCalculationIncomeChangeHistory history = list[i];
                list[i].ToDate = "-";
                if ((i + 1) < list.Count && !string.IsNullOrEmpty(list[i + 1].FromDate))
                {
                    CustomDate date = CustomDate.GetCustomDateFromString(list[i + 1].FromDate, IsEnglish);
                    list[i].ToDate = date.DecrementByOneDay().ToString();
                }
            }


            return list;
        }


        public static bool HasIncomeIncrement(int incomeId)
        {
            return PayrollDataContext.HasIncomeIncrement(incomeId).Value;
        }

        /// <summary>
        /// Check if the Income is used in the Calculation
        /// </summary>
        public static bool HasIncomeIncludedInCalculation(int incomeId, bool checkAsOneTime)
        {


            return PayrollDataContext.CalcHasIncomeIncluded(incomeId, (int)CalculationColumnType.Income, checkAsOneTime)
                .Value;

        }

        public bool Save(PIncome entity)
        {

            SetConnectionPwd(PayrollDataContext.Connection);
            PayrollDataContext.Transaction = PayrollDataContext.Connection.BeginTransaction();
            try
            {
                if (entity.IsApplyOnceFor && entity.OFHasEligiblity.Value)
                {
                    entity.OFEligibilityDateEng = GetEngDate(entity.OFEligibilityDate, null);
                }
                if (entity.IsApplyOnceFor && !string.IsNullOrEmpty(entity.OFWorkdaysFromDate))
                    entity.OFWorkdaysFromDateEng = GetEngDate(entity.OFWorkdaysFromDate, IsEnglish);

                #region "Change Log"

                PayrollDataContext.ChangeSettings.InsertOnSubmit(
                   GetSettingChangeLog((byte)MonetoryChangeLogTypeEnum.Income, entity.Title, null, null, LogActionEnum.Add));

                #endregion

                

                PayrollDataContext.PIncomes.InsertOnSubmit(entity);
                PayrollDataContext.SubmitChanges();

                // if defined type created then create blank date row in PIncomeDate
                if (entity.IsDefinedType != null && entity.IsDefinedType.Value)
                {
                    PIncomeDate date = new PIncomeDate();
                    date.EffectiveFrom = null;
                    date.EffectiveFromEng = null;
                    date.Name = "Starting";
                    date.IncomeId = entity.IncomeId;
                    PayrollDataContext.PIncomeDates.InsertOnSubmit(date);
                }

                PayrollDataContext.SubmitChanges();
                PayrollDataContext.Transaction.Commit();
                return true;
            }
            catch (Exception exp)
            {
                Log.log("Error while updating income id : " + entity.IncomeId, exp);
                PayrollDataContext.Transaction.Rollback();
                return false;
            }
            finally
            {
                //if (PayrollDataContext.Connection != null)
                //{
                //    PayrollDataContext.Connection.Close();
                //}
            }




            //return true;
        }

        public PIncome GetBasicIncome(int companyId)
        {
            return PayrollDataContext.PIncomes.SingleOrDefault(
                e => e.CompanyId == companyId && e.IsBasicIncome == true);
        }


        public PEmployeeIncome GetEmployeeIncome(int employeeIncomeId)
        {
            return PayrollDataContext.PEmployeeIncomes.SingleOrDefault(
                e => e.EmployeeIncomeId == employeeIncomeId);
        }

        public PEmployeeIncome GetEmployeeIncome(int employeeId, int incomeid)
        {
            return PayrollDataContext.PEmployeeIncomes.FirstOrDefault(
                e => e.EmployeeId == employeeId && e.IncomeId == incomeid);
        }
        public static PEmployeeIncome GetEmployeeIncomeById(int employeeId, int incomeid)
        {
            return PayrollDataContext.PEmployeeIncomes.FirstOrDefault(
                e => e.EmployeeId == employeeId && e.IncomeId == incomeid);
        }

        public static void EnableIncomeIFAlreadyAssociated(int employeeId, int incomeid)
        {
            PEmployeeIncome income = new PayManager().GetEmployeeIncome(employeeId, incomeid);
            if (income != null)
            {
                income.IsEnabled = true;
            }
            PayrollDataContext.SubmitChanges();
        }
        public static bool UpdateTempEmployeeIncome(int employeeId, int incomeid, decimal amount)
        {
            PEmployeeIncome inc = PayrollDataContext.PEmployeeIncomes.SingleOrDefault(
                e => e.EmployeeId == employeeId && e.IncomeId == incomeid);

            inc.TempAmount = amount;

            return UpdateChangeSet();
        }

        public static bool UpdateTempEmployeeDeduction(int employeeId, int deductionId, decimal amount)
        {
            PEmployeeDeduction inc = PayrollDataContext.PEmployeeDeductions.SingleOrDefault(
                e => e.EmployeeId == employeeId && e.DeductionId == deductionId);

            inc.TempAmount = amount;

            return UpdateChangeSet();
        }

        /// <summary>
        /// Get the current increment for the PEmployeeIncome
        /// </summary>
        /// <param name="employeeIncomeId"></param>
        /// <returns></returns>
        public static PEmployeeIncrement GetCurrentIncrement(int employeeIncomeId)
        {
            //Get last increment
            var increment = PayrollDataContext.PEmployeeIncrements.Where(
                e => e.EmployeeIncomeId == employeeIncomeId)
                .OrderByDescending(e => e.Id)
                .FirstOrDefault();

            if (increment == null)
                return null;

            //Check if increment is included in calculation or not
            bool isIncluded = PayrollDataContext.CCalculationIncludeds.Where(
                i => i.Type == ((int)CalculationColumnType.Increment)
                     && i.SourceId == increment.Id
                       ).Count() > 0;

            if (isIncluded)
                return null;
            return increment;
        }

        public static PEmployeeIncrement GetLastIncludedIncrement(int employeeIncomeId)
        {
            GetLastIncludedIncrementResult result = PayrollDataContext.GetLastIncludedIncrement(employeeIncomeId)
                .SingleOrDefault();

            if (result == null)
                return null;

            return PayrollDataContext.PEmployeeIncrements.Where(i => i.Id == result.Id).SingleOrDefault();
        }



        public bool Update(PIncome entity)
        {

            PIncome dbEntity = PayrollDataContext.PIncomes.Where(i => i.IncomeId == entity.IncomeId).SingleOrDefault();
            if (dbEntity == null)
                return false;

            dbEntity.EditSequence = entity.EditSequence;

            

            bool isCalculationTypeChanged = (dbEntity.Calculation != entity.Calculation);

            // if Calculation is being changed from Fixed to Variable or Variable to Fixed then don't process for Calculation change
            if (
                (dbEntity.Calculation == IncomeCalculation.FIXED_AMOUNT && entity.Calculation == IncomeCalculation.VARIABLE_AMOUNT) ||
                (dbEntity.Calculation == IncomeCalculation.VARIABLE_AMOUNT && entity.Calculation == IncomeCalculation.FIXED_AMOUNT)
                )
                isCalculationTypeChanged = false;

            double? ratePercent = entity.RatePercentForAll;
            SetIncomeForUpdate(entity, dbEntity);
            dbEntity.RatePercentForAll = ratePercent;

            SetConnectionPwd(PayrollDataContext.Connection);
            PayrollDataContext.Transaction = PayrollDataContext.Connection.BeginTransaction();
            try
            {
                UpdateChangeSet();
                // Second setup, set IsValid = false for other employees if the Calculation type is changed 
                if (isCalculationTypeChanged)
                    PayrollDataContext.UpdateIncomeForOtherEmployees(dbEntity.IncomeId, 0);


                PayrollDataContext.Transaction.Commit();
                return true;
            }
            catch (Exception exp)
            {
                Log.log("Error while updating income id : " + entity.IncomeId, exp);
                PayrollDataContext.Transaction.Rollback();
                return false;
            }
            finally
            {
                //if (PayrollDataContext.Connection != null)
                //{
                //    PayrollDataContext.Connection.Close();
                //}
            }
            //return false;


        }
        public static void SaveRetrospectiveIncrement(int incomeId, int payrollPeriodId, string employeeIDList)
        {
            PayrollDataContext.SaveRetrospectIncrement(incomeId, employeeIDList, payrollPeriodId);
        }
        public string DoMassIncrement(List<PEmployeeIncome> incomes, List<PEmployeeIncrement> increments,PayrollPeriod payrollPeriod)
        {
                     

            SetConnectionPwd(PayrollDataContext.Connection);
            PayrollDataContext.Transaction = PayrollDataContext.Connection.BeginTransaction();

            try
            {

                bool isCalculationTypeChanged = false; //PEmployeeIncome dbEntity = null;
                for (int i = 0; i < incomes.Count; i++)
                {
                    PEmployeeIncome empIncome = incomes[i];
                    PEmployeeIncome dbEmpIncome = GetEmployeeIncome(empIncome.EmployeeId, empIncome.IncomeId);

                    //dbEmpIncome.RatePercent = empIncome.RatePercent;
                    //dbEmpIncome.Amount = empIncome.Amount;

                    empIncome.IsEnabled = dbEmpIncome.IsEnabled;

                    PEmployeeIncrement empInc = increments.Where(e => e.EmployeeIncomeId == dbEmpIncome.EmployeeIncomeId).SingleOrDefault();

                    // check if increment already exists form emp pay tab
                    PEmployeeIncrement dbIncrement = GetCurrentIncrement(dbEmpIncome.EmployeeIncomeId);
                    if (empInc != null && dbIncrement != null)
                    {
                        PayrollDataContext.Transaction.Rollback();
                        return "Increment already exists for \"" + dbEmpIncome.EEmployee.Name + "\", so please reload the page.";
                    }

                    EmployeeServiceHistory serviceHistory = null;
                    // Validate for service history while incrementing associated Event must exists
                    if (CommonManager.IsServiceHistoryEnabled && empInc != null && empInc.Id == 0)
                    {
                        serviceHistory = PayrollDataContext.EmployeeServiceHistories
                            .Where(x => x.EmployeeId == GetEmployeeIncome(dbEmpIncome.EmployeeIncomeId).EmployeeId)
                            .Where(x => x.DateEng == GetEngDate(empInc.EffectiveFromDate, null) && x.EventID == empInc.EventID && x.PayrollPeriodId == CommonManager.GetLastPayrollPeriod().PayrollPeriodId)
                            .FirstOrDefault();

                        if (serviceHistory == null)
                        {
                            PayrollDataContext.Transaction.Rollback();
                            return string.Format("Service event \"{0}\" has not been created for the date \"{1}\" for the employee \"{2}\" - {3}.",
                                CommonManager.GetServieName(empInc.EventID.Value), empInc.EffectiveFromDate + ", " + GetEngDate(empInc.EffectiveFromDate, null).ToString("yyyy-MMM-dd")
                                , dbEmpIncome.EEmployee.Name,dbEmpIncome.EmployeeId);
                            
                        }
                    }

                    // Process for increment,call before setting employee values
                    bool isIncrementDeleted = UpdateIncrement(empInc, dbEmpIncome, empIncome, serviceHistory);
                    SetPEmployeeIncome(dbEmpIncome, empIncome, isIncrementDeleted, empInc);

                    dbEmpIncome.IsValid = true;
                  

                    // when Increment is being saved, then check if Promotion exists on increment date
                    // which is not being associated with incremented income then associate that Promotion(designation change) to 
                    // the current increment
                    //if (empInc != null)
                    //{
                    //    empInc.PromotionDesignationChangeID =
                    //        PayrollDataContext.GetUnsedPromotionID(empIncome.EmployeeId, empIncome.IncomeId, empInc.EffectiveFromDateEng
                    //        );
                    //}


                }
                
                UpdateChangeSet();

                if (payrollPeriod != null)
                    PayManager.SaveRetrospectiveIncrement(incomes[0].IncomeId, payrollPeriod.PayrollPeriodId,
                                        string.Join(",", incomes.Select(x => x.EmployeeId.ToString()).ToArray()));


                PayrollDataContext.Transaction.Commit();
                return "";
            }
            catch (Exception exp)
            {
                Log.log("Mass increment : " , exp);
                PayrollDataContext.Transaction.Rollback();
                return "Error saving mass increment : " + exp.Message;
            }
            finally
            {
                if (PayrollDataContext.Connection != null)
                {
                    PayrollDataContext.Connection.Close();
                }
            }
            //return "";

        }

       

        public static string ImportMassIncrement(List<GetEmployeeListForMassIncrementResult> incomes, int incomeId)
        {
            PayrollPeriod lastPeriod = CommonManager.GetLastPayrollPeriod();

            SetConnectionPwd(PayrollDataContext.Connection);
            PayrollDataContext.Transaction = PayrollDataContext.Connection.BeginTransaction();
            PIncome income = GetIncome(incomeId);
            bool isPercentTypeIncome = income.Calculation == IncomeCalculation.PERCENT_OF_INCOME ? true : false;

            int importCount = 0;

            List<int> einList = incomes.Select(x=>x.EmployeeId).ToList();

            List<PEmployeeIncome> dbEmployeeIncomes = PayrollDataContext.PEmployeeIncomes.Where(x => einList.Contains(x.EmployeeId)
                && x.IncomeId == incomeId).ToList();

            try
            {

                bool isCalculationTypeChanged = false; //PEmployeeIncome dbEntity = null;
                for (int i = 0; i < incomes.Count; i++)
                {
                    GetEmployeeListForMassIncrementResult empIncome = incomes[i];
                    PEmployeeIncome dbEmpIncome = dbEmployeeIncomes.FirstOrDefault(x => x.EmployeeId == empIncome.EmployeeId && x.IncomeId == incomeId); //GetEmployeeIncomeById(empIncome.EmployeeId, incomeId);

                    if (dbEmpIncome == null)
                    {
                        return "Income is not associated with ein : " + empIncome.EmployeeId + ", apply or associate the income before salary changes.";
                    }
                   


                    // as new amount could be 0 also, so comment below code
                    //if (empIncome.NewIncome == 0 && empIncome.IncreasedBy==0)
                    //    continue;

                    PEmployeeIncrement dbEmpIncrement = null;

                    int? incrementId = PayrollDataContext.GetNotUsedIncrementId(dbEmpIncome.EmployeeIncomeId, empIncome.EffectiveDateEng.Value,empIncome.EventID,empIncome.EmployeeId);

                    if (incrementId != null && incrementId != 0)
                    {
                        dbEmpIncrement = PayrollDataContext.PEmployeeIncrements
                             .FirstOrDefault(e => e.Id == incrementId);
                    }




                    //if Amount changed
                    if (isPercentTypeIncome == false)
                        PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(
                            empIncome.EmployeeId, (byte)MonetoryChangeLogTypeEnum.Income, MonetoryChangeLogTypeEnum.Increment + " : " + income.Title + " : " + (empIncome.EffectiveDate), dbEmpIncome.Amount, empIncome.NewIncome, LogActionEnum.Add)
                            );
                    if (isPercentTypeIncome)
                        PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(
                            empIncome.EmployeeId, (byte)MonetoryChangeLogTypeEnum.Income, MonetoryChangeLogTypeEnum.Increment + " : " + income.Title + " : " + (empIncome.EffectiveDate), dbEmpIncome.RatePercent == null ? 0 : dbEmpIncome.RatePercent, empIncome.NewIncome, LogActionEnum.Add)
                            );


                    // update increment
                    if (dbEmpIncrement != null)
                    {
                        dbEmpIncrement.AmountNew = empIncome.NewIncome;
                        dbEmpIncrement.RatePercentNew = dbEmpIncome.RatePercent;

                        dbEmpIncrement.EffectiveFromDate = empIncome.EffectiveDate;
                        dbEmpIncrement.EffectiveFromDateEng = empIncome.EffectiveDateEng.Value;
                        dbEmpIncrement.EventID = empIncome.EventID;
                        dbEmpIncrement.IsReterospect = false;

                        if (dbEmpIncrement.EffectiveFromDateEng < lastPeriod.StartDateEng)
                            dbEmpIncrement.IsReterospect = true;

                        dbEmpIncrement.CreatedBy = SessionManager.User.UserID;

                        if (isPercentTypeIncome)
                            dbEmpIncome.RatePercent = (double)empIncome.NewIncome;
                        else
                            dbEmpIncome.Amount = empIncome.NewIncome;

                    }
                        // insert increment
                    else
                    {
                        dbEmpIncrement = new PEmployeeIncrement();
                        dbEmpIncrement.EmployeeIncomeId = dbEmpIncome.EmployeeIncomeId;
                        dbEmpIncrement.AmountNew = empIncome.NewIncome;
                        dbEmpIncrement.RatePercentNew = dbEmpIncome.RatePercent;

                        dbEmpIncrement.EffectiveFromDate = empIncome.EffectiveDate;
                        dbEmpIncrement.EffectiveFromDateEng = empIncome.EffectiveDateEng.Value;
                        dbEmpIncrement.EventID = empIncome.EventID;
                        dbEmpIncrement.IsReterospect = false;

                        if (dbEmpIncrement.EffectiveFromDateEng < lastPeriod.StartDateEng)
                            dbEmpIncrement.IsReterospect = true;

                        // get prev amount
                        int? incId = PayrollDataContext.GetNotUsedIncrementId(dbEmpIncome.EmployeeIncomeId, null,null,null);
                        if (incId != null && incId != 0)
                        {
                            PEmployeeIncrement tempInc = PayrollDataContext.PEmployeeIncrements
                                 .FirstOrDefault(e => e.Id == incId);
                            if (tempInc != null)
                            {
                                // For record if salary increment from 1,000 to 2,000 and then 2,000 to 3,000
                                // Case 1 :Correct for record purpose
                                //dbEmpIncrement.AmountOld = tempInc.AmountNew;
                                // Case 2 :Correct for old amount calculation
                                //dbEmpIncrement.AmountOld = tempInc.AmountOld;

                                // now we will set using day, if Day = 1 then record logic as not old amount calculation is used
                                // else for calculation purpose

                                //CustomDate date = CustomDate.GetCustomDateFromString(dbEmpIncrement.EffectiveFromDate, IsEnglish);
                                //if (date.Day == 1)
                                //{
                                //    dbEmpIncrement.AmountOld = tempInc.AmountNew;
                                //}
                                //else
                                //{
                                //    dbEmpIncrement.AmountOld = tempInc.AmountOld;
                                //}

                                // for old period calculation
                                dbEmpIncrement.AmountOld = tempInc.AmountNew;// tempInc.AmountOld == null ? 0 : tempInc.AmountOld.Value;
                                // old amount for display in reports
                                dbEmpIncrement.AmountOldForDisplay = tempInc.AmountNew;

                                //here last incremented amount need to set as old amount for current increment otherwise nibl
                                // retire/rehire caluclation will not match like "Tulsi Ram Shrestha" case
                            }
                        }
                        else
                            dbEmpIncrement.AmountOld = dbEmpIncome.Amount == null ? 0 : dbEmpIncome.Amount.Value;
                        
                        dbEmpIncrement.RatePercentOld = dbEmpIncome.RatePercent;
                        dbEmpIncrement.CreatedBy = SessionManager.User.UserID;
                        dbEmpIncrement.CreatedOn = lastPeriod.StartDateEng;

                        if (isPercentTypeIncome)
                            dbEmpIncome.RatePercent = (double)empIncome.NewIncome;
                        else
                            dbEmpIncome.Amount = empIncome.NewIncome;
                        dbEmpIncrement.PayrollPeriodId = CommonManager.GetLastPayrollPeriod().PayrollPeriodId;


                        EmployeeServiceHistory serviceHistory = null;
                        // Validate for service history while incrementing associated Event must exists
                        if (CommonManager.IsServiceHistoryEnabled)
                        {
                            serviceHistory = PayrollDataContext.EmployeeServiceHistories
                                .Where(x => x.EmployeeId == dbEmpIncome.EmployeeId)
                                .Where(x => x.DateEng == empIncome.EffectiveDateEng && x.EventID == empIncome.EventID && x.PayrollPeriodId == CommonManager.GetLastPayrollPeriod().PayrollPeriodId)
                                .OrderByDescending(x=>x.CreatedOn) // get last created event as for NIBL, same Annual Increment could happen multiple times for same month for same date
                                .FirstOrDefault();

                            if (serviceHistory == null)
                            {
                                PayrollDataContext.Transaction.Rollback();
                                return string.Format("Service event \"{0}\" has not been created for the date \"{1}\" for the employee \"{2}\" - {3}.",
                                    CommonManager.GetServieName(empIncome.EventID), empIncome.EffectiveDate + ", " + empIncome.EffectiveDateEng.Value.ToString("yyyy-MMM-dd")
                                    , dbEmpIncome.EEmployee.Name, dbEmpIncome.EmployeeId);

                            }
                        }

                        if (serviceHistory != null)
                            dbEmpIncrement.EventSourceID = serviceHistory.EventSourceID;

                        PayrollDataContext.PEmployeeIncrements.InsertOnSubmit(dbEmpIncrement);
                    }

                    importCount += 1;
                    // check if increment already exists form emp pay tab
                    //PEmployeeIncrement dbIncrement = GetCurrentIncrement(dbEmpIncome.EmployeeIncomeId);
                    //if (empInc != null && dbIncrement != null)
                    //{
                    //    PayrollDataContext.Transaction.Rollback();
                    //    return "Increment already exists for \"" + dbEmpIncome.EEmployee.Name + "\", so please reload the page.";
                    //}

                   

                    // Process for increment,call before setting employee values
                    //bool isIncrementDeleted = UpdateIncrement(empInc, dbEmpIncome, empIncome);
                    //SetPEmployeeIncome(dbEmpIncome, empIncome, isIncrementDeleted, empInc);

                    dbEmpIncome.IsValid = true;


                    // when Increment is being saved, then check if Promotion exists on increment date
                    // which is not being associated with incremented income then associate that Promotion(designation change) to 
                    // the current increment
                    //if (dbEmpIncrement != null)
                    //{
                    //    dbEmpIncrement.PromotionDesignationChangeID =
                    //        PayrollDataContext.GetUnsedPromotionID(empIncome.EmployeeId, incomeId, empIncome.EffectiveDateEng
                    //        );
                    //}


                }

                UpdateChangeSet();

                if (lastPeriod != null)
                    PayManager.SaveRetrospectiveIncrement(incomeId, lastPeriod.PayrollPeriodId,
                                        string.Join(",", incomes.Select(x => x.EmployeeId.ToString()).ToArray()));


                PayrollDataContext.Transaction.Commit();

                return "Import successfull for " + importCount + " employees.";
            }
            catch (Exception exp)
            {
                Log.log("Mass increment : ", exp);
                PayrollDataContext.Transaction.Rollback();
                return "Error saving mass increment : " + exp.ToString();
            }
            finally
            {
                if (PayrollDataContext.Connection != null)
                {
                    PayrollDataContext.Connection.Close();
                }
            }
            //return "";

        }

        public static List<PEmployeeIncome> GetPEmployeeIncomes(int incomeId)
        {
            List<PEmployeeIncome> list =
               PayrollDataContext.PEmployeeIncomes.Where(x => x.IncomeId == incomeId).OrderBy(x => x.EmployeeId).ToList();

            foreach (PEmployeeIncome inc in list)
            {
                inc.Name = EmployeeManager.GetEmployeeById(inc.EmployeeId).Name;
                inc.IDCardNo = PayrollDataContext.EHumanResources
                    .FirstOrDefault(x => x.EmployeeId == inc.EmployeeId).IdCardNo;
            }

            return list;
        }

        /// <summary>
        /// Set Employee Incomes with Increments
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="increment"></param>
        public bool SetEmployeeIncomeInformation(PEmployeeIncome entity, PEmployeeIncrement increment, 
            ref bool isCalculationTypeChanged,ref PEmployeeIncome dbEntity,EmployeeServiceHistory serviceHistory)
        {
            dbEntity = GetEmployeeIncome(entity.EmployeeIncomeId);
            if (dbEntity == null)
                return false;

            isCalculationTypeChanged = (dbEntity.PIncome.Calculation != entity.PIncome.Calculation);

            dbEntity.EditSequence = entity.EditSequence;
            //preserve Income rate percent if update for employee
            double? ratePercent = dbEntity.PIncome.RatePercentForAll;
            SetIncomeForUpdate(entity.PIncome, dbEntity.PIncome);
            entity.PIncome.RatePercentForAll = ratePercent;

            // Process for increment,call before setting employee values
            bool isIncrementDeleted = UpdateIncrement(increment, dbEntity, entity,serviceHistory);

            SetPEmployeeIncome(dbEntity, entity, isIncrementDeleted, increment);

            return true;
        }

        public ResponseStatus Update(PEmployeeIncome entity, PEmployeeIncrement increment, PayrollPeriod payrollPeriod,
            PayrollPeriod lastPeriod)
        {
            ResponseStatus status = new ResponseStatus();
            bool isCalculationTypeChanged = false;
            PEmployeeIncome dbEntity = null;

            EmployeeServiceHistory serviceHistory = null;

            // Validate for service history while incrementing associated Event must exists
            if (CommonManager.IsServiceHistoryEnabled && increment != null && increment.Id == 0)
            {
                serviceHistory = PayrollDataContext.EmployeeServiceHistories
                    .Where(x => x.EmployeeId == GetEmployeeIncome(entity.EmployeeIncomeId).EmployeeId)
                    .Where(x => x.DateEng == GetEngDate(increment.EffectiveFromDate, null) && x.EventID == increment.EventID && x.PayrollPeriodId == lastPeriod.PayrollPeriodId)
                    .FirstOrDefault();

                if (serviceHistory == null)
                {
                    status.ErrorMessage = string.Format("Service event \"{0}\" has not been created for the date \"{1}\".",
                        CommonManager.GetServieName(increment.EventID.Value), increment.EffectiveFromDate + ", " + GetEngDate(increment.EffectiveFromDate, null).ToString("yyyy-MMM-dd"));
                    return status;
                }
            }

            if (!SetEmployeeIncomeInformation(entity, increment, ref isCalculationTypeChanged, ref dbEntity, serviceHistory))
            {
                status.ErrorMessage = "Income is not associated with the employee.";
                return status;
            }

            

            // if not Retrospect increment but salary is already saved for this PayrollPeriod then show warning
            if (dbEntity != null && lastPeriod != null && CalculationManager.IsCalculationSavedForEmployee(lastPeriod.PayrollPeriodId, dbEntity.EmployeeId))
            {
                status.ErrorMessage = "Salary is already saved for current period.";
                return status;
            }


            SetConnectionPwd(PayrollDataContext.Connection);
            PayrollDataContext.Transaction = PayrollDataContext.Connection.BeginTransaction();
            try
            {
                UpdateChangeSet();
                // Second setup, set IsValid = false for other employees if the Calculation type is changed 
                if (isCalculationTypeChanged)
                    PayrollDataContext.UpdateIncomeForOtherEmployees(dbEntity.PIncome.IncomeId, dbEntity.EmployeeId);

                if (payrollPeriod != null && dbEntity != null)
                    PayrollDataContext.SaveRetrospectIncrement(dbEntity.IncomeId, dbEntity.EmployeeId.ToString(), payrollPeriod.PayrollPeriodId);

                // If employee is new one, who has not been saved in calculation then set for Increment for salary report
                if (CommonManager.CompanySetting.HasLevelGradeSalary == false
                    && CalculationManager.IsCalculatedSavedForEmployee(dbEntity.EmployeeId) == false
                    && IsFixedIncome(dbEntity.PIncome.IncomeId))
                {
                    PEmployeeIncrement firstIncrement = PayrollDataContext.PEmployeeIncrements
                        .FirstOrDefault(x => x.EmployeeIncomeId == entity.EmployeeIncomeId);
                    if (firstIncrement == null)
                    {
                        ECurrentStatus firstStatus = EmployeeManager.GetFirstStatus(dbEntity.EmployeeId);

                        firstIncrement = new PEmployeeIncrement();
                        firstIncrement.EmployeeIncomeId = entity.EmployeeIncomeId;
                        firstIncrement.AmountNew = entity.Amount;
                        firstIncrement.AmountOld = 0;
                        firstIncrement.CreatedBy = SessionManager.User.UserID;
                        // set Payroll Period start date as we need to group differnt incomes like Basic,Allowance
                        firstIncrement.CreatedOn = lastPeriod.StartDateEng;
                        firstIncrement.EffectiveFromDate = firstStatus.FromDate;
                        firstIncrement.EffectiveFromDateEng = firstStatus.FromDateEng;
                        firstIncrement.EventID = (int)ServiceEventType.Appointment;
                        firstIncrement.IsReterospect = false;

                        EmployeeServiceHistory appointmentHistory = PayrollDataContext.EmployeeServiceHistories
                            .Where(x => x.EmployeeId == dbEntity.EmployeeId && x.EventID == (int)ServiceEventType.Appointment)
                            .OrderBy(x => x.DateEng).FirstOrDefault();
                        if (appointmentHistory != null)
                            firstIncrement.EventSourceID = appointmentHistory.EventSourceID;
                        firstIncrement.PayrollPeriodId = CommonManager.GetLastPayrollPeriod().PayrollPeriodId;
                        PayrollDataContext.PEmployeeIncrements.InsertOnSubmit(firstIncrement);
                    }
                    else
                    {
                        ECurrentStatus firstStatus = EmployeeManager.GetFirstStatus(dbEntity.EmployeeId);

                        
                        firstIncrement.EmployeeIncomeId = entity.EmployeeIncomeId;
                        firstIncrement.AmountNew = entity.Amount;
                        firstIncrement.AmountOld = 0;
                        firstIncrement.CreatedBy = SessionManager.User.UserID;
                        firstIncrement.CreatedOn = lastPeriod.StartDateEng;
                        firstIncrement.EffectiveFromDate = firstStatus.FromDate;
                        firstIncrement.EffectiveFromDateEng = firstStatus.FromDateEng;
                        firstIncrement.EventID =(int) ServiceEventType.Appointment;
                        firstIncrement.IsReterospect = false;

                        EmployeeServiceHistory appointmentHistory = PayrollDataContext.EmployeeServiceHistories
                             .Where(x => x.EmployeeId == dbEntity.EmployeeId && x.EventID == (int)ServiceEventType.Appointment)
                             .OrderBy(x => x.DateEng).FirstOrDefault();
                        if (appointmentHistory != null)
                            firstIncrement.EventSourceID = appointmentHistory.EventSourceID;
                    }
                    PayrollDataContext.SubmitChanges();
                }

                PayrollDataContext.Transaction.Commit();
                return status;
            }
            catch (Exception exp)
            {
                Log.log("Error while updating emp income :" + entity.EmployeeIncomeId, exp);
                PayrollDataContext.Transaction.Rollback();
                status.ErrorMessage = exp.Message;
                return status;
            }
            finally
            {
                //if (PayrollDataContext.Connection != null)
                //{
                //    PayrollDataContext.Connection.Close();
                //}
            }


        }

        private static void SetIncomeForUpdate(PIncome entity, PIncome dbEntity)
        {
            dbEntity.Title = entity.Title;
            dbEntity.Abbreviation = entity.Abbreviation;
            dbEntity.Notes = entity.Notes;

            #region "Change Log"

            AddToChangeSetting(dbEntity, entity, MonetoryChangeLogTypeEnum.Income, entity.Title,
                "Title", "Abbreviation","TempAmount","Order","EditSequence","IncomeId","CompanyId","OFEligibilityDateEng",
                "OFWorkdaysFromDateEng","IsBasicIncome","IsNotRegular");

            #endregion

            dbEntity.IsDefinedType = entity.IsDefinedType;
            dbEntity.XType = entity.XType;
            dbEntity.YType = entity.YType;
               
            dbEntity.IsPFCalculated = entity.IsPFCalculated;
            dbEntity.IsGrade = entity.IsGrade;
            dbEntity.IsCountForLeaveEncasement = entity.IsCountForLeaveEncasement;
          
            dbEntity.IsUpdatedWithLeave = entity.IsUpdatedWithLeave;
            dbEntity.IsChildrenAllowance = entity.IsChildrenAllowance;
            dbEntity.Calculation = entity.Calculation;
            dbEntity.UnitRateBasedOnWorkdays = entity.UnitRateBasedOnWorkdays;
            dbEntity.IsTaxable = entity.IsTaxable;
            dbEntity.IsApplyOnceFor = entity.IsApplyOnceFor;
            dbEntity.OFMonth = entity.OFMonth;
            dbEntity.OFHasEligiblity = entity.OFHasEligiblity;
            if (entity.IsApplyOnceFor && entity.OFHasEligiblity.Value && dbEntity.OFEligibilityDate != entity.OFEligibilityDate)
            {
                dbEntity.OFEligibilityDateEng = GetEngDate(entity.OFEligibilityDate, null);
            }

            dbEntity.OFCountWorkdaysFrom = entity.OFCountWorkdaysFrom;
            dbEntity.OFWorkdaysFromDate = entity.OFWorkdaysFromDate;
            if (entity.IsApplyOnceFor && !string.IsNullOrEmpty(entity.OFWorkdaysFromDate))
                dbEntity.OFWorkdaysFromDateEng = GetEngDate(entity.OFWorkdaysFromDate, IsEnglish);

            dbEntity.OFEligibilityDate = entity.OFEligibilityDate;
            dbEntity.OFEmployeeStatus = entity.OFEmployeeStatus;
            dbEntity.OFMinimumDays = entity.OFMinimumDays;
            dbEntity.OFProportionateDays = entity.OFProportionateDays;
            dbEntity.ProportionateBased = entity.ProportionateBased;
            dbEntity.DeductUPLAbsentForFestivalWorkdays = entity.DeductUPLAbsentForFestivalWorkdays;
            dbEntity.DeductAbsentOnlyForFestivalWorkdays = entity.DeductAbsentOnlyForFestivalWorkdays;
            dbEntity.DeductStopPaymentForFestivalWorkdays = entity.DeductStopPaymentForFestivalWorkdays;
            dbEntity.ExcludeRetiringBeforeEligiblityDate = entity.ExcludeRetiringBeforeEligiblityDate;
            dbEntity.IsLateIncomeType = entity.IsLateIncomeType;

            dbEntity.IsVariableAmount = entity.IsVariableAmount;
            dbEntity.IsCalculatedAmount = entity.IsCalculatedAmount;
            dbEntity.IsEnabled = entity.IsEnabled;

            dbEntity.MinAmount = entity.MinAmount;
            dbEntity.MaxAmount = entity.MaxAmount;

            dbEntity.DeemedIsAmount = entity.DeemedIsAmount;
            dbEntity.DeemedIsYearlyIncome = entity.DeemedIsYearlyIncome;
            dbEntity.DeemedMonth = entity.DeemedMonth;
            dbEntity.DeemedIsVariableType = entity.DeemedIsVariableType;

            dbEntity.PIncomeIncomes.Clear();
            dbEntity.PIncomeIncomes.AddRange(entity.PIncomeIncomes);

            dbEntity.PIncomeStatus.Clear();
            dbEntity.PIncomeStatus.AddRange(entity.PIncomeStatus);

            dbEntity.IsNotRegular = entity.IsNotRegular;
            dbEntity.PIncomeMonths.Clear();
            dbEntity.PIncomeMonths.AddRange(entity.PIncomeMonths);

            dbEntity.DoNotShowThisFestivalTypeInSalary = entity.DoNotShowThisFestivalTypeInSalary;
            dbEntity.IsTreatLikeOneTimeIncome = entity.IsTreatLikeOneTimeIncome;
            dbEntity.AddTaxFromTheBeginning = entity.AddTaxFromTheBeginning;
            dbEntity.IncomeSpecificExchangeRate = entity.IncomeSpecificExchangeRate;
            dbEntity.AmountAlreadyPaid = entity.AmountAlreadyPaid;

            // Unit Rate
            dbEntity.UnitRateHalfdayCounting = entity.UnitRateHalfdayCounting;
            dbEntity.UnitRateDeductHolidayWeeklyAndNational = entity.UnitRateDeductHolidayWeeklyAndNational;
            dbEntity.UnitRateDeductPaidLeaves = entity.UnitRateDeductPaidLeaves;
            dbEntity.UnitRateDeductUnpaidLeaves = entity.UnitRateDeductUnpaidLeaves;
            dbEntity.UnitRateDeductHalfdayHoliday = entity.UnitRateDeductHalfdayHoliday;
            dbEntity.UnitRateDeductHalfdayPaidLeave = entity.UnitRateDeductHalfdayPaidLeave;
            dbEntity.UnitRateProportionatePreviousMonth = entity.UnitRateProportionatePreviousMonth;
        }

        public List<PEmployeeIncrement> GetIncrementHistory(int employeeIncomeId)
        {
            return
                PayrollDataContext.PEmployeeIncrements.Where(
                i => (i.EmployeeIncomeId == employeeIncomeId))// && i.IsIncluded == true))
                .OrderBy(i => i.Id).ToList();
        }

        public static string GetLogForPositionGradeStepChange(int? positionId, int? gradeId, int? stepId)
        {
            string str = "";
            if (positionId != null && positionId != 0)
            {
                str = PositionGradeStepAmountManager.GetPositionById(positionId.Value).Name + ":";
            }
            if (gradeId != null && gradeId != 0)
            {
                str += PositionGradeStepAmountManager.GetGradeById(gradeId.Value).Name + ":";
            }

            if (stepId != null && stepId != 0)
            {
                str += PositionGradeStepAmountManager.GetGradeById(gradeId.Value).Name;
            }
            return str;
        }

        /// <summary>
        /// Update increment for the Employee Income
        /// </summary>
        private static bool UpdateIncrement(PEmployeeIncrement increment, PEmployeeIncome dbEntity, PEmployeeIncome entityForLog, EmployeeServiceHistory serviceHistory)
        {
            PayrollPeriod lastPeriod = CommonManager.GetLastPayrollPeriod();

            //Check if not passed & current found then delete
            if (increment == null)
            {
                PEmployeeIncrement dbEntityIncrement = GetCurrentIncrement(dbEntity.EmployeeIncomeId);
                if (dbEntityIncrement != null)
                {
                    dbEntity.PEmployeeIncrements.Remove
                       (dbEntityIncrement);

                    #region "Change Log"
                    if (CommonManager.CompanySetting.DisableChangeLog == false)
                    {
                        if (dbEntity.PIncome.IsGrade.HasValue == false || dbEntity.PIncome.IsGrade == false)
                        {
                            //if Amount changed
                            if (dbEntityIncrement.AmountOld.HasValue)
                                PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(
                                    dbEntity.EmployeeId, (byte)MonetoryChangeLogTypeEnum.Income, MonetoryChangeLogTypeEnum.Increment.ToString() + " : "+ dbEntity.PIncome.Title,entityForLog.Amount, dbEntityIncrement.AmountOld,  LogActionEnum.Delete)
                                    );
                            if (dbEntityIncrement.RatePercentOld.HasValue)
                                PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(
                                    dbEntity.EmployeeId, (byte)MonetoryChangeLogTypeEnum.Income, MonetoryChangeLogTypeEnum.Increment.ToString() + " : " + dbEntity.PIncome.Title, entityForLog.RatePercent, dbEntityIncrement.RatePercentOld,LogActionEnum.Delete)
                                    );
                            if (dbEntityIncrement.MarketValueOld.HasValue)
                                PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(
                                    dbEntity.EmployeeId, (byte)MonetoryChangeLogTypeEnum.Income, MonetoryChangeLogTypeEnum.Increment.ToString() + " : " + dbEntity.PIncome.Title, entityForLog.MarketValue, dbEntityIncrement.MarketValueOld, LogActionEnum.Delete)
                                    );
                        }
                        else
                        {
                            //log for Position/Grade/Step change
                           
                            PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(
                                 dbEntity.EmployeeId, (byte)MonetoryChangeLogTypeEnum.Income,
                                 MonetoryChangeLogTypeEnum.Grade.ToString() + " : " +  dbEntity.PIncome.Title,
                                 GetLogForPositionGradeStepChange(entityForLog.PositionId,entityForLog.GradeId,entityForLog.StepId) ,
                                 GetLogForPositionGradeStepChange(dbEntityIncrement.PositionId,dbEntityIncrement.GradeId,dbEntityIncrement.StepId)
                                 ,LogActionEnum.Delete
                                 ));
                        }
                    }
                    #endregion

                    //Set old values if increment is cancelled
                    dbEntity.Amount = dbEntityIncrement.AmountOld;
                    dbEntity.RatePercent = dbEntityIncrement.RatePercentOld;
                    dbEntity.MarketValue = dbEntityIncrement.MarketValueOld;
                  
                    dbEntity.PositionId = dbEntityIncrement.PositionId;
                    dbEntity.GradeId = dbEntityIncrement.GradeId;
                    dbEntity.StepId = dbEntityIncrement.StepId;

                   

                    return true;
                }

                return false;
            }

            increment.EffectiveFromDateEng = GetEngDate(increment.EffectiveFromDate, null);

            //Save to insert
            if (increment.Id == 0)
            {

                #region "Change Log"
                if (CommonManager.CompanySetting.DisableChangeLog == false)
                {
                    if (dbEntity.PIncome.IsGrade.HasValue == false || dbEntity.PIncome.IsGrade == false)
                    {

                        //if Amount changed
                        if (dbEntity.Amount.HasValue)
                            PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(
                                dbEntity.EmployeeId,(byte) MonetoryChangeLogTypeEnum.Income,MonetoryChangeLogTypeEnum.Increment+" : "+dbEntity.PIncome.Title + " : " + (increment != null ? increment.EffectiveFromDate : ""), dbEntity.Amount, entityForLog.Amount, LogActionEnum.Add)
                                );
                        if (dbEntity.RatePercent.HasValue)
                            PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(
                                dbEntity.EmployeeId, (byte)MonetoryChangeLogTypeEnum.Income, MonetoryChangeLogTypeEnum.Increment + " : " + dbEntity.PIncome.Title + " : " + (increment != null ? increment.EffectiveFromDate : ""), dbEntity.RatePercent, entityForLog.RatePercent, LogActionEnum.Add)
                                );
                        if (dbEntity.MarketValue.HasValue)
                            PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(
                                dbEntity.EmployeeId, (byte)MonetoryChangeLogTypeEnum.Income, MonetoryChangeLogTypeEnum.Increment + " : " + dbEntity.PIncome.Title + " : " + (increment != null ? increment.EffectiveFromDate : ""), dbEntity.MarketValue, entityForLog.MarketValue, LogActionEnum.Add)
                                );
                    }
                    else
                    {
                        //log for Position/Grade/Step change
                        PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(
                             dbEntity.EmployeeId,(byte) MonetoryChangeLogTypeEnum.Income,
                             MonetoryChangeLogTypeEnum.Grade.ToString() +" : "+ dbEntity.PIncome.Title, 
                             GetLogForPositionGradeStepChange(dbEntity.PositionId, dbEntity.GradeId, dbEntity.StepId),
                             GetLogForPositionGradeStepChange(entityForLog.PositionId, entityForLog.GradeId, entityForLog.StepId), LogActionEnum.Add
                             ));
                    }
                }
                #endregion

                //Save amount as it needed in first time only
                increment.AmountOld = dbEntity.Amount;
                increment.RatePercentOld = dbEntity.RatePercent;
                increment.MarketValueOld = dbEntity.MarketValue;

                increment.PositionId = dbEntity.PositionId;
                increment.GradeId = dbEntity.GradeId;
                increment.StepId = dbEntity.StepId;

                increment.AmountNew = entityForLog.Amount;
                increment.RatePercentNew = entityForLog.RatePercent;
                increment.CreatedBy = SessionManager.User.UserID;
                increment.CreatedOn = lastPeriod.StartDateEng;
                increment.PayrollPeriodId = CommonManager.GetLastPayrollPeriod().PayrollPeriodId;
                if(serviceHistory != null)
                    increment.EventSourceID = serviceHistory.EventSourceID;
                dbEntity.PEmployeeIncrements.Add(increment);
            }
            //Update
            else
            {
                PEmployeeIncrement dbEntityIncrement = dbEntity.PEmployeeIncrements
                    .Where(i => i.Id == increment.Id).FirstOrDefault();

                if (dbEntityIncrement != null)
                {

                    #region "Change Logs"
                    if (dbEntityIncrement.EffectiveFromDate != increment.EffectiveFromDate)
                        PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(dbEntityIncrement.PEmployeeIncome.EmployeeId,
                            (byte)MonetoryChangeLogTypeEnum.Income, MonetoryChangeLogTypeEnum.Increment.ToString()
                            + " : " + dbEntityIncrement.PEmployeeIncome.PIncome.Title, dbEntityIncrement.EffectiveFromDate,
                            increment.EffectiveFromDate,  LogActionEnum.Update));

                    #endregion

                    dbEntityIncrement.EffectiveFromDate = increment.EffectiveFromDate;
                    dbEntityIncrement.EffectiveFromDateEng = increment.EffectiveFromDateEng;
                    dbEntityIncrement.IsReterospect = increment.IsReterospect;

                    dbEntityIncrement.AmountNew = entityForLog.Amount;
                    dbEntityIncrement.RatePercentNew = entityForLog.RatePercent;

                    dbEntityIncrement.EventID = increment.EventID;
                }
            }
            return false;
        }


        public static void SetReasonForPayVarianceReport(int period1, int period2, List<Variance_GetPayVarianceReportEmployeeListDetailsResult> dataList
            ,string empIDList)
        {
            PayrollPeriod p1 = CommonManager.GetPayrollPeriod(period1);
            PayrollPeriod p2 = CommonManager.GetPayrollPeriod(period2);


            List<GetPayVarianceReasonResult> list = GetPayVarianceReason(p1.StartDateEng.Value, p1.EndDateEng.Value, p2.StartDateEng.Value, p2.EndDateEng.Value, empIDList);

            foreach (Variance_GetPayVarianceReportEmployeeListDetailsResult item in dataList)
            {
                GetPayVarianceReasonResult reason = list.FirstOrDefault(x => x.EIN == item.EmployeeId);
                if (reason != null)
                {
                    item.Reason1 = reason.Reason1;
                    item.Reason2 = reason.Reason2;
                }
            }

        }

        public static List<GetPayVarianceReasonResult> GetPayVarianceReason(DateTime from1, DateTime to1, DateTime from2,
            DateTime to2, string empIDList)
        {
            List<GetPayVarianceReasonResult> list = PayrollDataContext.GetPayVarianceReason(from1, to1, from2, to2, empIDList).ToList();

            List<GetPayVarianceReasonResult> newList = new List<GetPayVarianceReasonResult>();
            for(int i=0;i<list.Count;i++)
            {
                GetPayVarianceReasonResult item  = list[i];

                if (newList.Any(x => x.EIN == item.EIN) == false)
                {
                    GetPayVarianceReasonResult newItem = new GetPayVarianceReasonResult() { Reason1="",Reason2=""};
                    newList.Add(newItem);
                    newItem.EIN = item.EIN;

                    List<GetPayVarianceReasonResult> myList = list.Where(x => x.EIN == item.EIN).ToList();

                    for (int j = 0; j < myList.Count; j++)
                    {
                        GetPayVarianceReasonResult innerItem = myList[j];
                        if (!string.IsNullOrEmpty(innerItem.Reason1))
                        {
                            if (newItem.Reason1 == "")
                                newItem.Reason1 = innerItem.Reason1;
                            else
                                newItem.Reason1 += ", " + innerItem.Reason1;
                        }

                        if (!string.IsNullOrEmpty(innerItem.Reason2))
                        {
                            if (newItem.Reason2 == "")
                                newItem.Reason2 = innerItem.Reason2;
                            else
                                newItem.Reason2 += ", " + innerItem.Reason2;
                        }
                    }
                }
            }

            return newList;
        }


        public void SetPEmployeeIncome(PEmployeeIncome dbEntity, PEmployeeIncome entity, bool isIncrementDeleted, PEmployeeIncrement increment)
        {
            //If increment is deleted don't set the value here it will be set in the method UpdateIncrement,to
            //preserve the old values
            if (!isIncrementDeleted)
            {

                #region "Change Log"
                //if increment has not been added in this save, as for increment it is added from another place
                if (increment == null || increment.Id != 0)
                {
                    if (LogEnabled)
                    {
                        if (dbEntity.PIncome.IsGrade.HasValue == false || dbEntity.PIncome.IsGrade == false)
                        {
                            //if Amount changed
                            if ((  dbEntity.PIncome.Calculation==IncomeCalculation.FIXED_AMOUNT || dbEntity.PIncome.Calculation==IncomeCalculation.VARIABLE_AMOUNT || dbEntity.PIncome.Calculation==IncomeCalculation.Unit_Rate )
                                
                                && dbEntity.Amount != entity.Amount)
                                PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(
                                    dbEntity.EmployeeId, (byte)MonetoryChangeLogTypeEnum.Income, dbEntity.PIncome.Title, dbEntity.Amount, entity.Amount, LogActionEnum.Update)
                                    );
                            else if ( ( dbEntity.PIncome.Calculation==IncomeCalculation.FESTIVAL_ALLOWANCE  && dbEntity.PIncome.Calculation==IncomeCalculation.PERCENT_OF_INCOME ) && dbEntity.RatePercent != entity.RatePercent)
                                PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(
                                    dbEntity.EmployeeId, (byte)MonetoryChangeLogTypeEnum.Income, dbEntity.PIncome.Title, dbEntity.RatePercent, entity.RatePercent, LogActionEnum.Update)
                                    );
                            else// (dbEntity.MarketValue != entity.MarketValue)
                                PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(
                                    dbEntity.EmployeeId, (byte)MonetoryChangeLogTypeEnum.Income, dbEntity.PIncome.Title, dbEntity.MarketValue, entity.MarketValue, LogActionEnum.Update)
                                    );
                        }
                        else
                        {

                            //log for Position/Grade/Step change
                            if (dbEntity.PositionId != entity.PositionId || dbEntity.GradeId != entity.GradeId || dbEntity.StepId != entity.StepId)
                                PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(
                                     dbEntity.EmployeeId, (byte)MonetoryChangeLogTypeEnum.Income,
                                     MonetoryChangeLogTypeEnum.Grade.ToString() + " : " + dbEntity.PIncome.Title,
                                     GetLogForPositionGradeStepChange(dbEntity.PositionId, dbEntity.GradeId, dbEntity.StepId),
                                     GetLogForPositionGradeStepChange(entity.PositionId, entity.GradeId, entity.StepId), LogActionEnum.Update
                                     ));
                        }
                    }
                }
                #endregion


                if (dbEntity.PIncome.Calculation == IncomeCalculation.FESTIVAL_ALLOWANCE && dbEntity.ApplyFestivalIncome != entity.ApplyFestivalIncome 
                    && !(dbEntity.ApplyFestivalIncome == null && entity.ApplyFestivalIncome == false))
                {
                    PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(
                        dbEntity.EmployeeId, (byte)MonetoryChangeLogTypeEnum.Income, dbEntity.PIncome.Title + " - Apply Festival Income", dbEntity.ApplyFestivalIncome, entity.ApplyFestivalIncome, LogActionEnum.Update)
                        );
                }

                dbEntity.ApplyFestivalIncome = entity.ApplyFestivalIncome;

                dbEntity.Amount = entity.Amount;
                dbEntity.RatePercent = entity.RatePercent;
                dbEntity.MarketValue = entity.MarketValue;

                dbEntity.PositionId = entity.PositionId;
                dbEntity.GradeId = entity.GradeId;
                dbEntity.StepId = entity.StepId;

            }

            dbEntity.IsValid = entity.IsValid;

            if (dbEntity.PIncome.IsBasicIncome == false)
            {
                if (dbEntity.IsEnabled != entity.IsEnabled)
                {
                    PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(dbEntity.EmployeeId, (byte)MonetoryChangeLogTypeEnum.Income,
                        dbEntity.PIncome.Title + " : Enabled", dbEntity.IsEnabled, entity.IsEnabled, LogActionEnum.Update));
                }

                dbEntity.IsEnabled = entity.IsEnabled;
            }
            dbEntity.IsForeignAllowance = entity.IsForeignAllowance;
            dbEntity.IsNotSourceSalaryMatrix = true;
            dbEntity.SkipMinMaxChecking = entity.SkipMinMaxChecking;
        }

        public static bool IsDeductionDeletable(int deductionId)
        {
            if (PayrollDataContext.PEmployeeDeductions
                .Where(i => i.DeductionId == deductionId)
                .Count() <= 0)
                return true;
            return false;
        }
        public static bool IsLoanDeletable(int loanId)
        {
            return true;
        }
        public static bool DeleteDeduction(int deductionId)
        {
            if (IsDeductionDeletable(deductionId))
            {

                PDeduction dbEntity = PayrollDataContext.PDeductions.SingleOrDefault(
                    c => (c.DeductionId == deductionId));


                if (dbEntity == null)
                    return false;

                System.Data.Linq.ChangeSet cs = PayrollDataContext.GetChangeSet();
                PayrollDataContext.PDeductions.DeleteOnSubmit(dbEntity);
                PayrollDataContext.SubmitChanges();
                return (cs.Deletes.Count == 1);
            }
            return false;
        }
        //public static bool DeleteLoan(int deductionId)
        //{
        //    if (IsLoanDeletable(deductionId))
        //    {

        //        LoanType dbEntity = PayrollDataContext.LoanTypes.SingleOrDefault(
        //            c => (c.LoanTypeId == deductionId));


        //        if (dbEntity == null)
        //            return false;

        //        System.Data.Linq.ChangeSet cs = PayrollDataContext.GetChangeSet();
        //        PayrollDataContext.LoanTypes.DeleteOnSubmit(dbEntity);
        //        PayrollDataContext.SubmitChanges();
        //        return (cs.Deletes.Count == 1);
        //    }
        //    return false;
        //}

        public static bool IsFixedIncome(int incomeId)
        {
            return PayrollDataContext.PIncomes.Any(x => x.IncomeId == incomeId &&
                x.Calculation == IncomeCalculation.FIXED_AMOUNT);
        }
        public static bool IsIncomeDeletable(int incomeId, bool isBasicIncome)
        {
            if (incomeId == 0)
                return false;

            if (isBasicIncome)
                return false;
            //if (PayrollDataContext.PEmployeeIncomesIsIncomeDeletable
            //    .Where(i => i.IncomeId == incomeId)
            //    .Count() <= 0)
            //    return true;
            if (PayrollDataContext.CCalculationHeaders.Any(x => (x.Type == (int)CalculationColumnType.Income 
                    || x.Type == (int)CalculationColumnType.DeemedIncome) 
                    && x.SourceId == incomeId) == true)
                return false;

            if (PayrollDataContext.AddOnHeaders.Any(x => (x.Type == (int)CalculationColumnType.Income
                   || x.Type == (int)CalculationColumnType.DeemedIncome)
                   && x.SourceId == incomeId) == true)
                return false;

            if (PayrollDataContext.PEmployeeIncomes.Any(x => x.IncomeId == incomeId))
                return false;

            return true;
        }

        public static bool DeleteIncome(int incomeId)
        {
            PIncome dbEntity = PayrollDataContext.PIncomes.SingleOrDefault(
                    c => (c.IncomeId == incomeId));
            
            if (IsIncomeDeletable(incomeId, dbEntity.IsBasicIncome))
            {
                if (dbEntity == null)
                    return false;

                System.Data.Linq.ChangeSet cs = PayrollDataContext.GetChangeSet();
                PayrollDataContext.PIncomes.DeleteOnSubmit(dbEntity);
                PayrollDataContext.SubmitChanges();
                return (cs.Deletes.Count == 1);
            }
            return false;
        }


        public bool Delete(int empIncomeId)
        {
            PEmployeeIncome dbEntity = PayrollDataContext.PEmployeeIncomes.SingleOrDefault(
                c => (c.EmployeeIncomeId == empIncomeId));


            if (dbEntity == null)
                return false;

            // If income is Deemed Loan type then don't allow to de associate 
            if(PayrollDataContext.PDeductions.Any(x=>x.MarketValueIncomeId == dbEntity.IncomeId))
            {
                return false;
            }

            if (dbEntity.PIncome.IsBasicIncome)
                return false;

            bool isDeleteValid = !EmployeeManager.HasEmployeeAndIncomeIncludedInCalculation(dbEntity.IncomeId, dbEntity.EmployeeId);

            if (isDeleteValid)
            {
                #region "Change Log"
                PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(
                      dbEntity.EmployeeId, (byte)MonetoryChangeLogTypeEnum.Income, dbEntity.PIncome.Title, dbEntity.Amount, null, LogActionEnum.Detach)
                      );
                #endregion

                PayrollDataContext.PEmployeeIncomes.DeleteOnSubmit(dbEntity);
            }
            else
            {
                PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(
                     dbEntity.EmployeeId, (byte)MonetoryChangeLogTypeEnum.Income, "Enabled", dbEntity.IsEnabled.ToString(), "false", LogActionEnum.Update)
                     );
                dbEntity.IsEnabled = false;
            }
            //System.Data.Linq.ChangeSet cs = PayrollDataContext.GetChangeSet();
            //PayrollDataContext.PEmployeeIncomes.DeleteOnSubmit(dbEntity);
            PayrollDataContext.SubmitChanges();
            return true;//(cs.Deletes.Count == 1);
        }

        /// <summary>
        /// Retrive the last increment that has not been effective in calculation
        /// </summary>
        //public PEmployeeIncrement GetNotIncludedIncrement(int empIncomeId)
        //{

        //    return PayrollDataContext.PEmployeeIncrements.Where
        //        (e => (e.EmployeeIncomeId == empIncomeId))// && e.IsIncluded == false))
        //        .SingleOrDefault();
        //}

        //public PEmployeeIncome GetEmpIncome(int incomeId, int employeeId)
        //{
        //    PEmployeeIncome dbEntity = PayrollDataContext.PEmployeeIncomes.SingleOrDefault(
        //       c => (c.IncomeId == incomeId && c.EmployeeId == employeeId));
        //    return dbEntity;
        //}

        public static List<EmployeeIncomeListResult> GetEmployeeIncomeList(int employeeId)
        {
            return PayrollDataContext.EmployeeIncomeList(employeeId).ToList();
        }

        public static DateTime GetGratuityStartDateForNIBL(int employeeId,DateTime PayrollEndDate)
        {
            int statusIdByDate = PayrollDataContext.GetEmployeeStatusForDate(PayrollEndDate, employeeId).Value;
            return PayrollDataContext.GetGratuityStartDateForNIBL(employeeId, PayrollEndDate, statusIdByDate).Value;
        }

        public static List<EmployeeDeductionListResult> GetEmployeeDeductionListForPersonalDetails(int employeeId)
        {
            StringBuilder html = new StringBuilder("");
            List<EmployeeDeductionListResult> deductions = PayManager.GetEmployeeDeductionList(employeeId);
            int lastPayrollPeroidId = 0;

            //get last payroll period having calculation generated
            EmployeeIncomeForLeaveEncasementCalc entity = PayrollDataContext.EmployeeIncomeForLeaveEncasementCalcs
                    .Where(e => e.EmployeeId == employeeId)
                   .OrderByDescending(e => e.PayrollPeriodId)
                   .Take(1)
                   .SingleOrDefault();

            int calculationId = 0;
            if (entity != null)
            {
                lastPayrollPeroidId = entity.PayrollPeriodId;
                CCalculation calc = CalculationManager.GetCalculation(lastPayrollPeroidId);
                if (calc != null)
                    calculationId = calc.CalculationId;
            }

            for (int i = deductions.Count - 1; i >= 0; i--)
            {
                EmployeeDeductionListResult deduction = deductions[i];


                decimal? amount = PayrollDataContext.GetSalaryColumnValue
                    (calculationId, employeeId, (int)CalculationColumnType.Deduction, deduction.DeductionId);


                if (amount != null)
                {
                    deduction.Amount = amount;
                }
                else
                    deductions.RemoveAt(i);
            }



            return deductions;
        }

        public static List<EmployeeIncomeListResult> GetEmployeeIncomeListWithoutAnyDeduction(int employeeId,ref decimal? incomePf,ref decimal? deductionPf)
        {
           int lastCalculationId =
                (from p in PayrollDataContext.PayrollPeriods
                 join c in PayrollDataContext.CCalculations on p.PayrollPeriodId equals c.PayrollPeriodId
                 orderby p.PayrollPeriodId descending
                 select c.CalculationId).Take(1).SingleOrDefault();
         //   int lastPayrollPeroidId = lastPayrol != null ? lastPayrol.PayrollPeriodId : 0;

            List<EmployeeIncomeListResult> incomelist = PayrollDataContext.EmployeeIncomeList(employeeId).ToList();
            foreach (EmployeeIncomeListResult result in incomelist)
                result.Value = GetCurrency(result.Amount);
            //assign the value from "EmployeeIncomeForLeaveEncasementCalc" table with out any deduction
            //foreach (EmployeeIncomeListResult income in incomelist)

            decimal pfCalculationAmount = 0;

            //if (CommonManager.CompanySetting.IsD2)
            //{
            //    for (int i = incomelist.Count - 1; i >= 0; i--)
            //    {
            //        EmployeeIncomeListResult income = incomelist[i];
            //        PIncome incomeEntity = new PayManager().GetIncomeById(income.IncomeId);
            //        PEmployeeIncome empIncome = new PayManager().GetEmployeeIncome(employeeId, income.IncomeId);


            //        if (income.Amount != null && incomeEntity.IsEnabled == true && empIncome.IsEnabled == true && income.Amount != 0)
            //        {
            //            decimal? beforeConvAmount = income.Amount;

            //            income.Amount = PayrollDataContext.GetConvertedAmount(
            //                income.Amount, income.IsBasicIncome, true, 0, income.IncomeId).Value;

            //            income.Value = GetCurrency(income.Amount) + " ($" + 
                            
            //                (beforeConvAmount == null ? "" : string.Format("{0:#}", beforeConvAmount))  + ")";

            //            if( incomeEntity.IsPFCalculated)
            //                pfCalculationAmount += income.Amount.Value;

            //        }
            //    }
            //}

            //remove empty
            for (int i = incomelist.Count - 1; i >= 0; i--)
            {
                EmployeeIncomeListResult income = incomelist[i];

                if (income.Amount == null || income.Amount==0)
                    incomelist.RemoveAt(i);
            }


            incomePf = (((decimal)0.1) * pfCalculationAmount);
            deductionPf = incomePf * 2;

            //incomePf = PayrollDataContext.GetSalaryColumnValue(
            //    lastCalculationId, employeeId,
            //                     (int)CalculationColumnType.IncomePF, (int)CalculationColumnType.IncomePF);

            //deductionPf = PayrollDataContext.GetSalaryColumnValue(lastCalculationId, employeeId,
            //    (int)CalculationColumnType.DeductionPF, (int)CalculationColumnType.DeductionPF);


            if (incomePf != null)
            {
                EmployeeIncomeListResult pfIncome = new EmployeeIncomeListResult();
                pfIncome.Title = "PF";
                pfIncome.Value = GetCurrency(incomePf.Value);

                if (incomePf.Value != null && incomePf.Value != 0)
                    incomelist.Add(pfIncome);
            }





            return incomelist;
        }


        /// <summary>
        /// Retrieve loan deduction of the employee to display in personal details page
        /// </summary>
        public static List<PEmployeeDeduction> GetEmployeeLoanForPersonalDetails(int employeeId)
        {
            PayManager mgr = new PayManager();
            List<EmployeeDeductionListResult> deductions = GetEmployeeDeductionList(employeeId);
            List<PEmployeeDeduction> loanDeductions = new List<PEmployeeDeduction>();
            foreach (EmployeeDeductionListResult ded in deductions)
            {
                if (ded.Calculation == DeductionCalculation.LOAN)
                {
                    PEmployeeDeduction loanDed = mgr.GetEmployeeDeduction(ded.EmployeeDeductionId);
                    loanDeductions.Add(loanDed);
                }

            }

            return loanDeductions;
        }

        /// <summary>
        /// Retrieve loan deduction of the employee to display in personal details page
        /// </summary>
        public static List<PEmployeeDeduction> GetEmployeeAdvanceForPersonalDetails(int employeeId)
        {
            PayManager mgr = new PayManager();
            List<EmployeeDeductionListResult> deductions = GetEmployeeDeductionList(employeeId);
            List<PEmployeeDeduction> loanDeductions = new List<PEmployeeDeduction>();
            foreach (EmployeeDeductionListResult ded in deductions)
            {
                if (ded.Calculation == DeductionCalculation.Advance)
                {
                    PEmployeeDeduction loanDed = mgr.GetEmployeeDeduction(ded.EmployeeDeductionId);
                    loanDeductions.Add(loanDed);
                }

            }

            return loanDeductions;
        }

        public static void ReverseSourceMatrixOrAmount(int empIncomeId)
        {
            PEmployeeIncome empIncome = new PayManager().GetEmployeeIncome(empIncomeId);
            if (empIncome != null)
            {
                //if (empIncome.IsNotSourceSalaryMatrix == null || empIncome.IsNotSourceSalaryMatrix == false)
                //    empIncome.IsNotSourceSalaryMatrix = true;
                //else
               empIncome.IsNotSourceSalaryMatrix = false;
            }
            PayrollDataContext.SubmitChanges();
        }

        public string GetHTMLEmployeeIncomeList(int employeeId)
        {
            StringBuilder html = new StringBuilder();

            bool isReadOnlyUser = SessionManager.IsReadOnlyUser;

            List<EmployeeIncomeListResult> incomes = GetEmployeeIncomeList(employeeId);

            PIncome basicIncome = GetBasicIncome(SessionManager.CurrentCompanyId);

            html.Append("<table class='table table-bordered table-condensed table-striped table-primary' >");
            html.Append("<thead><tr><th>SN</th> <th style='width:220px;text-align:left'>Income</th> <th style='width:150px;;text-align:left'>Type</th> <th style='width:100px;;text-align:left'>Source</th> <th style='width:100px;text-align:right'>Rate</th><th style='width:100px;text-align:right'>Amount</th> <th style='width:100px;' class='editDeleteTH'> Action </th> <th style='width:120px;' class='editDeleteTH'></th> <th style='width:230px'></th> </tr> </thead>");
            int index = 0;

            // list income sorting applied first and unapplied later
            List<EmployeeIncomeListResult> applied = new List<EmployeeIncomeListResult>();
            List<EmployeeIncomeListResult> unapplied = new List<EmployeeIncomeListResult>();
            foreach (EmployeeIncomeListResult income in incomes)
            {
                if (income.Source.ToLower() == "not applied")
                {
                    unapplied.Add(income);
                }
                else
                    applied.Add(income);
            }

            incomes.Clear();
            incomes.AddRange(applied.OrderBy(x => x.Title).ToList());
            incomes.AddRange(unapplied.OrderBy(x => x.Title).ToList());

            foreach (EmployeeIncomeListResult income in incomes)
            {
                index += 1;
                string rowBackColor = !(income.IsIncomeEnabled.Value && income.IsEmployeeIncomeEnabled.Value)
                    ? "disabledIncomeDeduction" : "";
                bool isUnApplied = false;

                if (income.Source.ToLower() == "not applied")
                {
                    rowBackColor = "unappliedIncomeDeduction";
                    isUnApplied = true;
                }


                string unApplyOrSourceMatrixOrSourceAmount = "Un-Apply";
                if (income.IsMatrixTypeIncome != null && income.IsMatrixTypeIncome.Value)
                {
                    if (IsDeleteValid(income, employeeId) && income.Source == "Salary Matrix")
                        unApplyOrSourceMatrixOrSourceAmount = "";
                    else if (IsDeleteValid(income, employeeId))// if(IsDeleteValid(income, employeeId) && income.Source == "Salary Amount")
                        unApplyOrSourceMatrixOrSourceAmount = "Restore Matrix";
                }
                else if (IsDeleteValid(income, employeeId))
                    unApplyOrSourceMatrixOrSourceAmount = "Un-Apply";

                string applyOrEditText;
                // If salary matrix & income is not associated then ..
                if (income.Source == "Salary Matrix" && income.EmployeeIncomeId == 0)
                    applyOrEditText = "Change";
                else
                    applyOrEditText = "Apply";

                bool showEditElseApply;
                // if Income is not applyed then Apply or Employee Income is disabled then "Apply" or else "Edit"
                if (income.EmployeeIncomeId == 0)
                    showEditElseApply = true;
                else
                    showEditElseApply = false;

                if (isReadOnlyUser)
                {
                    html.Append(
                        string.Format(@"<tr class='{4}'>
                            <td> {0} </td>
                            <td> {5} </td>
                            <td style='text-align:right'> {1} </td>
                            <td style='text-align:right'> {6} </td>
                            <td  class='editDeleteTD' style='text-align:center;'> 
                               <span style='display:none'>{3}{2}</span> &nbsp;
                            </td> <td></td></tr>",
                            income.Title,
                            income.GetRate(SessionManager.DecimalPlaces),
                            (IsDeleteValid(income, employeeId) ? "" : "hidden"), //if basic salary hide delete button
                            income.EmployeeIncomeId,
                            rowBackColor,
                            Utils.Helper.Util.GetConstantResource(income.Calculation),
                            income.GetAmount(SessionManager.DecimalPlaces)
                        )
                        );
                }
                else
                {
                    html.Append(
                        string.Format(@"<tr class='{4}'  onmouseover='this.originalClass=this.className;this.className=""selected"";' onmouseout='this.className=this.originalClass;'>
                            <td> {14} </td>
                            <td> {0} </td>
                            <td> {5} </td>
                            <td> {11} </td>
                            <td style='text-align:right'> {1} </td>
                             <td style='text-align:right'> {8} </td>
                            <td  class='editDeleteTD' style='text-align:center;'> 
                                <a  style='width:50px;display:{7};cursor:pointer' onclick='return popupUpdateIncomeCall({3})' >Change</a>
                                 <a   style='width:50px;display:{10};cursor:pointer;text-decoration:none'  onclick='return applyIncome({9},this)'>{12}</a>
                            </td>
                            <td  class='editDeleteTD' style='text-align:center;'> 
                                <a   style='width:70px;display:{2};cursor:pointer;text-decoration:none' onclick='return deleteIncomeCall({3},{6},this);'>{13}</a>
                             </td>                           
                            </tr>",
                            income.Title,
                            income.GetRate(SessionManager.DecimalPlaces),
                            (IsDeleteValid(income, employeeId) ? "" : "none"), //if basic salary hide delete button
                            income.EmployeeIncomeId,
                            rowBackColor,
                           Utils.Helper.Util.GetConstantResource(income.Calculation),
                           employeeId,
                           (showEditElseApply == true ? "none" : ""),
                           income.GetAmount(SessionManager.DecimalPlaces),
                           income.IncomeId,
                           (showEditElseApply == false ? "none" : ""),
                           income.Source,
                           applyOrEditText,
                           unApplyOrSourceMatrixOrSourceAmount,
                           index
                        )
                        );
                }


                #region "Additional Basic"
                // If Basic income then check & add for Additional Basic
                bool isBasicIncome = GetIncomeById(income.IncomeId).IsBasicIncome;
                //if (GetIncomeById(income.IncomeId).IsBasicIncome)
                {
                    PEmployeeIncome empIncome = GetEmployeeIncome(employeeId, basicIncome.IncomeId);
                    if (empIncome != null && empIncome.LevelId != null)
                    {
                        List<GradeAdditionlBasic> addiotionBasicList
                             = PayrollDataContext.GradeAdditionlBasics.Where(x => x.EmployeeId == employeeId && x.LevelId == empIncome.LevelId
                             && ((x.IncomeId == null && isBasicIncome) || (x.IncomeId==income.IncomeId))).ToList();
                        decimal amount = addiotionBasicList.Sum(x => x.Amount).Value;
                        if (amount != 0)
                        {
                            html.Append(
                        string.Format(@"<tr class='{4}'   onmouseover='this.originalClass=this.className;this.className=""selected"";' onmouseout='this.className=this.originalClass;'>
                            <td></td>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp; {0} </td>
                            <td> {5} </td>
                            <td> {11} </td>
                            <td style='text-align:right'> {1} </td>
                             <td style='text-align:right'> {8} </td>
                            <td  class='editDeleteTD' style='text-align:center;'> 
                                <input type='image' style='display:none;border-width: 0px;;display:{7}' onclick='return popupUpdateIncomeCall({3})' src='../images/edit.gif' />
                                 <a   style='display:none;width:50px;display:{10};cursor:pointer;text-decoration:none'  onclick='return applyIncome({9})'  >Apply</a>
                            </td>
                            <td  class='editDeleteTD' style='text-align:center;'> 
                                <a   style='display:none;width:50px;display:{2};cursor:pointer;text-decoration:none' onclick='return deleteIncomeCall({3},{6});'>Un-Apply</a>
                             </td>                           
                            </tr>",
                            "Includes Additional Income",
                            income.GetRate(SessionManager.DecimalPlaces),
                            (IsDeleteValid(income, employeeId) ? "" : "none"), //if basic salary hide delete button
                            income.EmployeeIncomeId,
                            rowBackColor,
                           Utils.Helper.Util.GetConstantResource(income.Calculation),
                           employeeId,
                           (income.EmployeeIncomeId == 0 ? "none" : ""),
                           GetCurrency(amount),
                           income.IncomeId,
                           (income.EmployeeIncomeId != 0 ? "none" : ""),
                           ""
                        )
                        );
                        }
                    }
                }
                #endregion

            }

            html.Append("</table>");

            return html.ToString();
        }

        public string GetHTMLSalaryDetailsForAppraisal(int employeeId)
        {
            StringBuilder html = new StringBuilder();

            bool isReadOnlyUser = SessionManager.IsReadOnlyUser;

            List<EmployeeIncomeListResult> incomes = GetEmployeeIncomeList(employeeId);


            html.Append("<table class='table table-bordered table-condensed table-striped table-primary' style='width:inherit' >");
            html.Append("<thead><tr>");

            foreach (EmployeeIncomeListResult income in incomes)
            {
                PIncome inc = GetIncomeById(income.IncomeId);
                if (inc.Calculation == IncomeCalculation.DEEMED_INCOME ||
                    inc.Calculation == IncomeCalculation.FESTIVAL_ALLOWANCE ||
                    inc.Calculation == IncomeCalculation.VARIABLE_AMOUNT)
                    continue;

                string amount = income.GetAmount(SessionManager.DecimalPlaces);

                if (amount != "-" && Convert.ToDecimal(amount) != 0)
                {
                    html.Append("<th>" + income.Title + "</th>");
                }
            }
            html.Append("<th>Gross Salary</th></tr></thead>");
            html.Append("<tr>");
            decimal gross = 0;
            foreach (EmployeeIncomeListResult income in incomes)
            {
                PIncome inc = GetIncomeById(income.IncomeId);
                if (inc.Calculation == IncomeCalculation.DEEMED_INCOME ||
                    inc.Calculation == IncomeCalculation.FESTIVAL_ALLOWANCE ||
                    inc.Calculation == IncomeCalculation.VARIABLE_AMOUNT)
                    continue;

                string amount = income.GetAmount(SessionManager.DecimalPlaces);

                if (amount != "-" && Convert.ToDecimal(amount) != 0)
                {
                    gross += Convert.ToDecimal(amount);
                    html.Append("<td>" + GetCurrency(amount) + "</td>");
                }
            }

            html.Append("<td>" + GetCurrency(gross) + "</td>");
            html.Append("</tr>");



            html.Append("</table>");

            return html.ToString();
        }

        /// <summary>
        /// Checks if Income can be un-attached or not
        /// </summary>
        private static bool IsDeleteValid(EmployeeIncomeListResult income, int employeeId)
        {
            // as for quest, basic salary can also be overridden
            if (CommonManager.CompanySetting.WhichCompany != WhichCompany.Quest &&
                CommonManager.CompanySetting.WhichCompany != WhichCompany.Triveni)
            {
                if (income.IsBasicIncome)
                    return false;

            }
            PEmployeeIncome empIncome = new PayManager().GetEmployeeIncome(employeeId, income.IncomeId);

            if (empIncome == null)
                return false;

            if (empIncome.IsEnabled == false)
                return false;

            return true;

            //return !EmployeeManager.HasEmployeeAndIncomeIncludedInCalculation(income.IncomeId, employeeId);
        }

        /// <summary>
        /// Checks if Income can be un-attached or not
        /// </summary>
        private static bool IsDeleteValid(EmployeeDeductionListResult deduction, int employeeId)
        {
            return !EmployeeManager.HasEmployeeAndDeductionIncludedInCalculation(deduction.DeductionId, employeeId);
        }

        private static bool IsDeleteValid(EmployeeDeductionListResult deduction, int employeeId,int employeeIncomeId)
        {
            if (employeeIncomeId == 0)
                return false;

            return !EmployeeManager.HasEmployeeAndDeductionIncludedInCalculation(deduction.DeductionId, employeeId);
        }

        public List<GetIncomeListForEmployeeResult> GetIncomeListForEmp(int empId)
        {
            return PayrollDataContext.GetIncomeListForEmployee(empId, SessionManager.CurrentCompanyId).ToList();
        }

        public static List<PDeduction> GetDeductionListByCompany(int companyId)
        {
            return PayrollDataContext.PDeductions.Where(c => c.CompanyId == companyId)
                .OrderBy(c => c.Title).ToList();
            //.GetIncomeListByCompany(companyId).ToList();
        }
        //public static List<LoanType> GetLoanListByCompany(int companyId)
        //{
        //    return PayrollDataContext.LoanTypes//.Where(c => c.CompanyId == companyId)
        //        .OrderBy(c => c.Title).ToList();
        //    //.GetIncomeListByCompany(companyId).ToList();
        //}
        public static List<PDeduction> GetDeductionListByCompanyForEmployeeImport(int companyId)
        {
            return PayrollDataContext.PDeductions.Where(c => c.CompanyId == companyId &&
                    (c.Calculation != DeductionCalculation.Advance && c.Calculation != DeductionCalculation.LOAN 
                    && c.Calculation !=DeductionCalculation.LOAN_Repayment))
                .OrderBy(c => c.Title).ToList();
            //.GetIncomeListByCompany(companyId).ToList();
        }

        public static List<PIncome> GetIncomeListByCompany(int companyId)
        {
            return PayrollDataContext.PIncomes.Where(c => c.CompanyId == companyId)
                .OrderBy(c => c.Title).ToList();
            //.GetIncomeListByCompany(companyId).ToList();
        }
        public static List<PIncome> GetFixedIncomeListByCompany(int companyId)
        {
            return PayrollDataContext.PIncomes.Where(c => c.Calculation==IncomeCalculation.FIXED_AMOUNT && c.CompanyId == companyId)
                .OrderBy(c => c.Title).ToList();
            //.GetIncomeListByCompany(companyId).ToList();
        }
        public static List<PIncome> GetFixedAndPercentIncomeListByCompany(int companyId)
        {
            return PayrollDataContext.PIncomes.Where(c => (c.Calculation == IncomeCalculation.FIXED_AMOUNT
                || c.Calculation == IncomeCalculation.PERCENT_OF_INCOME) && c.CompanyId == companyId)
                .OrderBy(c => c.Title).ToList();
            //.GetIncomeListByCompany(companyId).ToList();
        }
        public static List<PIncome> GetFestivalIncomeList(int companyId)
        {
            return PayrollDataContext.PIncomes.Where(c => c.Calculation == IncomeCalculation.FESTIVAL_ALLOWANCE && c.CompanyId == companyId)
                .OrderBy(c => c.Title).ToList();
            //.GetIncomeListByCompany(companyId).ToList();
        }
        public static List<PIncome> GetIncomeListByCompanyForCIT(int companyId)
        {
            return PayrollDataContext.PIncomes.Where(c => c.CompanyId == companyId && c.Calculation != IncomeCalculation.DEEMED_INCOME)
                .OrderBy(c => c.Title).ToList();
            //.GetIncomeListByCompany(companyId).ToList();
        }   

        public static List<PIncome> GetIncomeListByCompanyForImportEmployee(int companyId)
        {
            return PayrollDataContext.PIncomes.Where(c => c.CompanyId == companyId && c.Calculation != IncomeCalculation.DEEMED_INCOME)
                .OrderBy(c => c.Title).ToList();
            //.GetIncomeListByCompany(companyId).ToList();
        }

        public List<GetIncomeForIncomesResult> GetIncomeListForIncome(int companyId, int currentIncomeId)
        {
            return PayrollDataContext.GetIncomeForIncomes(companyId, currentIncomeId).ToList();
        }



        public static List<PIncome> GetIncomeList()
        {
            return PayrollDataContext.PIncomes.Where(i=>i.CompanyId == SessionManager.CurrentCompanyId).ToList();
        }

        public static Dictionary<int, int> GetIncomeOrderValues()
        {
            List<KeyValue> list
                =
                (from i in PayrollDataContext.PIncomes
                 where i.CompanyId == SessionManager.CurrentCompanyId
                 select new KeyValue
                            {
                                Key = i.IncomeId.ToString(),
                                Value = (i.Order != null ? i.Order.ToString() : "0")
                            }

                ).ToList();


            Dictionary<int, int> incomeOrders = new Dictionary<int, int>();

            foreach (var keyValue in list)
            {
                incomeOrders.Add(int.Parse(keyValue.Key), int.Parse(keyValue.Value));
            }

            if (SessionManager.CurrentCompany.PFOrder != null)
                incomeOrders.Add(0, SessionManager.CurrentCompany.PFOrder.Value);
            return incomeOrders;
        }

        public static Dictionary<int, int> GetDeductionOrderValues()
        {
            List<KeyValue> list
                =
                (from i in PayrollDataContext.PDeductions
                 where i.CompanyId == SessionManager.CurrentCompanyId
                 select new KeyValue
                 {
                     Key = i.DeductionId.ToString(),
                     Value = (i.Order != null ? i.Order.ToString() : "0")
                 }

                ).ToList();


            Dictionary<int, int> incomeOrders = new Dictionary<int, int>();

            foreach (var keyValue in list)
            {
                incomeOrders.Add(int.Parse(keyValue.Key), int.Parse(keyValue.Value));
            }

            return incomeOrders;
        }


        public static void UpdateIncomeOrder(int incomeId, int order)
        {
            if (incomeId == 0)
            {
                SessionManager.CurrentCompany.PFOrder = order;
                PayrollDataContext.SubmitChanges();
                return;
            }

            PayManager p = new PayManager();
            PIncome income = p.GetIncomeById(incomeId);
            if (income != null)
            {
                income.Order = order;
            }
            UpdateChangeSet();
        }

        public static void UpdateDeductionOrder(int deductionId, int order)
        {
            PayManager p = new PayManager();
            PDeduction entity = p.GetDeductionById(deductionId);
            if (entity != null)
            {
                entity.Order = order;
            }
            UpdateChangeSet();
        }

        //public List<PIncome> GetIncomeListByCompany(int companyId, int selfId)
        //{
        //    //return PayrollDataContext.PIncomes
        //    //    .Where(c => (c.CompanyId == companyId && c.IncomeId != selfId))
        //    //    .OrderBy(c => c.Title).ToList();

        //    return
        //        (
        //            from i in PayrollDataContext.PIncomes
        //            join ii in PayrollDataContext.PIncomeIncomes on i.IncomeId equals ii.IncomeId
        //            where i.CompanyId == companyId && i.IncomeId != selfId && (selfId != ii.OtherIncomeId)
        //            orderby i.Title
        //            select i
        //        ).ToList<PIncome>();
        //}

        public static bool DeleteRetrospectGenerated(int incomeId,int type)
        {

            PayrollPeriod lastPeriod = CommonManager.GetLastPayrollPeriod();
            if (lastPeriod == null)
                return false;

            CCalculation calc = CalculationManager.IsCalculationSaved(lastPeriod.PayrollPeriodId);
            if (calc != null)
            {
                return false;
            }

            List<RetrospectIncrement> list = PayrollDataContext.RetrospectIncrements.Where(
                x => x.IncomeId == incomeId && x.Type==type && x.PayrollPeriodId == lastPeriod.PayrollPeriodId && x.IsGenerated != null && x.IsGenerated==true).ToList();

            PayrollDataContext.RetrospectIncrements.DeleteAllOnSubmit(list);
            PayrollDataContext.SubmitChanges();

            return true;

        }

        

        public static bool IsRetrospectGeneratedExists(int incomeId,int type)
        {
            PayrollPeriod lastPeriod = CommonManager.GetLastPayrollPeriod();
            if (lastPeriod == null)
                return false;

            return
                PayrollDataContext.RetrospectIncrements
                .Any(x => x.IncomeId == incomeId && x.Type==type &&
                    x.IsGenerated != null && x.IsGenerated == true
                    && x.PayrollPeriodId == lastPeriod.PayrollPeriodId);
        }



        public static List<PIncome> GetOtherIncomes(int incomeId)
        {
            return
            (
                from i in PayrollDataContext.PIncomes
                join oi in PayrollDataContext.PIncomeIncomes on i.IncomeId equals oi.IncomeId
                orderby i.Title
                where oi.OtherIncomeId == incomeId
                    //if Deemed income then bring percent of Basic only
                    && (i.Calculation != IncomeCalculation.DEEMED_INCOME || Convert.ToBoolean(i.DeemedIsAmount) == false)
                    && i.Calculation != IncomeCalculation.FESTIVAL_ALLOWANCE


                select i
                ).ToList();
        }
        
        public bool AddIncomeToEmployee(int incomeId, int employeeId, bool addToAllEmployees, int companyId,int designationId
            ,int status,int branchId)
        {
            return PayrollDataContext.AddIncomeToEmployee(incomeId, employeeId, addToAllEmployees, companyId,SessionManager.UserName
                ,designationId,status,branchId) == 1;
        }

        public PIncome GetIncomeById(int incomeId)
        {
            return PayrollDataContext.PIncomes.SingleOrDefault(c => c.IncomeId == incomeId);
        }
        public static PIncome GetIncome(int incomeId)
        {
            return PayrollDataContext.PIncomes.SingleOrDefault(c => c.IncomeId == incomeId);
        }
        public PIncome GetIncomeByName(string title)
        {
            return PayrollDataContext.PIncomes.FirstOrDefault(c => c.Title.ToLower()==title.ToLower().Trim());
        }
        public Boolean IsTaxCalculatedForEmployee(int pEmployeeId)
        {
            if (PayrollDataContext.calcIsTaxCalculatedForEmployee(pEmployeeId).Equals(true))
                return true;
            return false;
        }
        //public List<PEmployeeIncome> GetPercentOfSaleIncome(int? currentPage, int? pageSize, ref int tempCount, int? companyId, string sortBy, 
        //    string employeeName)
        //{
        //    currentPage -= 1;
        //    DbConnection conn = new SqlConnection(Config.ConnectionString);

        //        //PayrollDataContext.Connection;
        //    List<PEmployeeIncome> POSIncome = new List<PEmployeeIncome>();
        //    using (conn)
        //    {                
        //        DbCommand selectCommand = conn.CreateCommand();
        //        selectCommand.CommandText = "GetPercentOFSaleIncome";
        //        selectCommand.CommandType = System.Data.CommandType.StoredProcedure;


        //        selectCommand.Parameters.Add(new SqlParameter("@currentPage", SqlDbType.Int));
        //        selectCommand.Parameters.Add(new SqlParameter("@pageSize", SqlDbType.Int));
        //        selectCommand.Parameters.Add(new SqlParameter("@sortBy", SqlDbType.VarChar));
        //        selectCommand.Parameters.Add(new SqlParameter("@total", SqlDbType.Int));
        //        selectCommand.Parameters.Add(new SqlParameter("@CompanyId",SqlDbType.Int));
        //        selectCommand.Parameters.Add(new SqlParameter("@EmployeeName", SqlDbType.VarChar));    

        //        selectCommand.Parameters["@currentPage"].Value = currentPage;
        //        selectCommand.Parameters["@pageSize"].Value = pageSize;
        //        selectCommand.Parameters["@sortBy"].Value = sortBy;
        //        selectCommand.Parameters["@CompanyId"].Value = companyId;
        //        selectCommand.Parameters["@EmployeeName"].Value = employeeName;
        //        selectCommand.Parameters["@total"].Direction = ParameterDirection.Output;

        //        if (conn.State != ConnectionState.Open)
        //            conn.Open();

        //        DbDataReader reader = selectCommand.ExecuteReader();
        //        using (reader)
        //        {
        //            while (reader.Read())
        //            {
        //                PEmployeeIncome emp = new PEmployeeIncome();
        //                emp.EEmployee = PayrollDataContext.EEmployees.Single(e=> e.EmployeeId == (int)reader["EmployeeId"]);
        //                emp.PIncome = PayrollDataContext.PIncomes.Single(p=> p.Title == reader["Title"].ToString());
        //                emp.EEmployee = PayrollDataContext.EEmployees.Single(e=> e.Name == reader["Name"].ToString());
        //                emp.RatePercent = Convert.ToDouble(reader["RatePercent"]);
        //                emp.Amount = Convert.ToDecimal(reader["Amount"]);
        //                emp.EmployeeIncomeId = (int)reader["EmployeeIncomeId"];
        //                POSIncome.Add(emp);
        //            }
        //        }
        //        tempCount = (int)selectCommand.Parameters["@total"].Value;

        //        return POSIncome;
        //    }
        //}

        public void UpdateEmployeeIncome(PEmployeeIncome obPEmpInc)
        {
            PEmployeeIncome ei = PayrollDataContext.PEmployeeIncomes.Single(e => e.EmployeeIncomeId == obPEmpInc.EmployeeIncomeId);
            ei.Amount = obPEmpInc.Amount;
            PayrollDataContext.SubmitChanges();

        }

        public static void UpdateEmployeeIncome(int incomeId,int employeeId, decimal amount)
        {
            PEmployeeIncome ei = PayrollDataContext.PEmployeeIncomes.Single(e => e.IncomeId == incomeId && e.EmployeeId==employeeId);
            ei.Amount = amount;
            PayrollDataContext.SubmitChanges();
        }
        public static void UpdateEmployeeDeduction(int deductionId, int employeeId, decimal amount)
        {
            PEmployeeDeduction ei = PayrollDataContext.PEmployeeDeductions.Single(e => e.DeductionId == deductionId && e.EmployeeId == employeeId);
            ei.Amount = amount;
            PayrollDataContext.SubmitChanges();
        }
        #endregion


        #region "Deduction"

        /// <summary>
        /// Check if the Income is used in the Calculation
        /// </summary>
        public static bool HasDeductionIncludedInCalculation(int deductionId)
        {


            return PayrollDataContext.CCalculationHeaders.Where(
                d => d.SourceId == deductionId &&
                     ((CalculationColumnType)d.Type == CalculationColumnType.Deduction)
                       ).Count() > 0;

        }

        public static List<GetLoanAdvDeductionHistoryResult> GetLoanAdvForHistory(int employeeId, int deductionId)
        {
            //return
            //    PayrollDataContext.CCalculationLoanAdvances
            //        .Where(l => l.DeductionId == deductionId && l.EmployeeId == employeeId)
            //        .ToList();

            return PayrollDataContext.GetLoanAdvDeductionHistory(employeeId, deductionId)
                .ToList();

        }

        public PDeduction GetDeductionById(int deductionId)
        {
            return PayrollDataContext.PDeductions.SingleOrDefault(c => c.DeductionId == deductionId);
        }
        //public LoanType GetLoanById(int deductionId)
        //{
        //    return PayrollDataContext.LoanTypes.SingleOrDefault(c => c.LoanTypeId == deductionId);
        //}
        public static string GetHTMLEmployeeDeductionList(int employeeId)
        {
            StringBuilder html = new StringBuilder();
            List<EmployeeDeductionListResult> deductions = GetEmployeeDeductionList(employeeId);

            PayrollPeriod lastPeriod = CommonManager.GetLastPayrollPeriod();

            int periodId = 0;
            if (lastPeriod != null)
                periodId = lastPeriod.PayrollPeriodId;

            bool isReadOnlyUser = SessionManager.IsReadOnlyUser;

            html.Append("<table class='table table-bordered table-condensed table-striped table-primary'>");
            html.Append("<thead><tr> <th style='width:250px'>Deduction</th><th style='width:160px'>Type</th>  <th style='width:100px'>Rate/Amount</th> <th  style='width:60px' class='editDeleteTH'> </th> <th></th> </tr> </thead>");
            foreach (EmployeeDeductionListResult deduction in deductions)
            {

                string rowBackColor = (deduction.IsEnabled != null && deduction.IsEnabled == false) ? "red" : "";
                if (isReadOnlyUser == false)
                {
                    html.Append(
                        string.Format(@"<tr  style='background:{4}'   onmouseover='this.originalClass=this.className;this.className=""selected"";' onmouseout='this.className=this.originalClass;'>
                            <td> {0} </td>
                            <td>{3} </td> 
                            <td style='text-align:right'>{1} </td> 
                            <td class='editDeleteTD'> 
                            <input type='image' style='border-width: 0px;' onclick='return popupUpdateDeductionCall({2});' src='../images/edit.gif' />
                            <input type='image' style='border-width: 0px;visibility:{4}' onclick='return deleteDeductionCall({2},{6});' src='../images/delete.gif' />
                            </td> <td></td> </tr>",
                            deduction.Title,
                            deduction.RateAmount(SessionManager.DecimalPlaces, periodId),
                            deduction.EmployeeDeductionId,
                           Utils.Helper.Util.GetConstantResource(deduction.Calculation),
                           (IsDeleteValid(deduction, employeeId) ? "" : "hidden"),
                           rowBackColor, deduction.EmployeeId

                        )
                        );
                }
                else
                {
                    html.Append(
                        string.Format(@"<tr  style='background:{4}'   onmouseover='this.originalClass=this.className;this.className=""selected"";' onmouseout='this.className=this.originalClass;'>
                            <td> {0} </td>
                            <td>{3} </td> 
                            <td style='text-align:right'>{1} </td> 
                            <td class='editDeleteTD'> 
                            <span style='display:none'> {2}{4} </span> &nbsp;
                            </td> </tr>",
                            deduction.Title,
                            deduction.RateAmount(SessionManager.DecimalPlaces, periodId),
                            deduction.EmployeeDeductionId,
                           Utils.Helper.Util.GetConstantResource(deduction.Calculation),
                           (IsDeleteValid(deduction, employeeId) ? "" : "hidden"),
                           rowBackColor
                        )
                        );
                }
            }

            html.Append("</table>");

            return html.ToString();
        }


        public static List<EmployeeDeductionListResult> GetEmployeeDeductionList(int employeeId)
        {
            return PayrollDataContext.EmployeeDeductionList(employeeId).ToList();
        }
        public List<GetDeductionListForEmployeeResult> GetDeductionListForEmp(int empId)
        {
            return PayrollDataContext.GetDeductionListForEmployee(empId, SessionManager.CurrentCompanyId).ToList();
        }

       
        public bool AddDeductionToEmployee(int deductionId, int employeeId, bool addToAllEmployees, int companyId)
        {
            return PayrollDataContext.AddDeductionToEmployee(deductionId, employeeId, addToAllEmployees, companyId,SessionManager.UserName) == 1;
        }

        public bool RemoveDeductionFromEmployee(PEmployeeDeduction entity)
        {
            PEmployeeDeduction dbEntity = PayrollDataContext.PEmployeeDeductions.SingleOrDefault(
                  c => c.EmployeeDeductionId == entity.EmployeeDeductionId);


            if (dbEntity == null)
                return false;


            #region "Change Log"
            PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(
                  dbEntity.EmployeeId, (byte)MonetoryChangeLogTypeEnum.Deduction, dbEntity.PDeduction.Title, dbEntity.Amount, null,  LogActionEnum.Detach)
                  );
            #endregion


            System.Data.Linq.ChangeSet cs = PayrollDataContext.GetChangeSet();
            PayrollDataContext.PEmployeeDeductions.DeleteOnSubmit(dbEntity);
            PayrollDataContext.SubmitChanges();
            return (cs.Deletes.Count == 1);
        }

        public bool Save(PEmployeeDeduction entity)
        {
            PayrollDataContext.PEmployeeDeductions.InsertOnSubmit(entity);
            return SaveChangeSet();
        }

        public bool Save(PDeduction entity)
        {
            PayrollDataContext.PDeductions.InsertOnSubmit(entity);

            // if Loan emi or simple interest type is being created then automatically
            // create Deemed Income for Market Value difference

            if (entity.Calculation == DeductionCalculation.LOAN)
            {
                PIncome marketValueDeemedIncome = new PIncome();
                marketValueDeemedIncome.Abbreviation = entity.Abbreviation;
                marketValueDeemedIncome.Title = entity.Title;
                marketValueDeemedIncome.Calculation = IncomeCalculation.DEEMED_INCOME;
                marketValueDeemedIncome.CompanyId = entity.CompanyId;
                marketValueDeemedIncome.DeemedIsAmount = true;
                marketValueDeemedIncome.DeemedIsYearlyIncome = false;
                marketValueDeemedIncome.EditSequence = 0;
                marketValueDeemedIncome.IsBasicIncome = false;
                marketValueDeemedIncome.IsPFCalculated = false;
                marketValueDeemedIncome.IsCountForLeaveEncasement = false;
                marketValueDeemedIncome.IsUpdatedWithLeave = true;
                marketValueDeemedIncome.IsTaxable = true;
                marketValueDeemedIncome.IsVariableAmount = true;
                marketValueDeemedIncome.IsCalculatedAmount = false;
                marketValueDeemedIncome.IsApplyOnceFor = false;
                marketValueDeemedIncome.IsEnabled = true;
                marketValueDeemedIncome.AddTaxFromTheBeginning = false;

                PayrollDataContext.PIncomes.InsertOnSubmit(marketValueDeemedIncome);
                PayrollDataContext.SubmitChanges();

                entity.MarketValueIncomeId = marketValueDeemedIncome.IncomeId;

            }

            return SaveChangeSet();
        }
        //public bool Save(LoanType entity)
        //{
        //    PayrollDataContext.LoanTypes.InsertOnSubmit(entity);
        //    return SaveChangeSet();
        //}
        public PDeduction GetById(PDeduction entity)
        {
            PDeduction dbEntity = PayrollDataContext.PDeductions.SingleOrDefault(
               c => c.DeductionId == entity.DeductionId);

            return dbEntity;
        }

        public PEmployeeDeduction GetEmployeeDeduction(int employeeDeductionId)
        {
            PEmployeeDeduction ded = PayrollDataContext.PEmployeeDeductions.
                SingleOrDefault(p => (p.EmployeeDeductionId == employeeDeductionId));

            //set unpaid balance as balance
            if (ded.PDeduction.Calculation == DeductionCalculation.LOAN)
            {
                List<CCalculationLoanAdvance> loans =
                PayrollDataContext.CCalculationLoanAdvances
                    .Where(c => c.UniqueId == ded.UniqueId)
                    .OrderByDescending(c => c.LoanAdvanceDetailId)
                    .ToList();

                if (loans.Count > 0)
                {
                    ded.Balance = loans[0].UnpaidBalance;
                }
            }
            return ded;
        }

        //public static PEmployeeDeduction GetEmployeeDeduction(int empDeductionId)
        //{
        //    PEmployeeDeduction ded = PayrollDataContext.PEmployeeDeductions.
        //        SingleOrDefault(p => (p.EmployeeDeductionId == empDeductionId));

        //    return ded;
        //}
        public static PEmployeeDeduction GetEmployeeDeduction(int employeeId, int deductionId)
        {
            PEmployeeDeduction ded = PayrollDataContext.PEmployeeDeductions.
                SingleOrDefault(p => (p.EmployeeId == employeeId && p.DeductionId == deductionId));

            //set unpaid balance as balance
            if (ded.PDeduction.Calculation == DeductionCalculation.LOAN)
            {
                List<CCalculationLoanAdvance> loans =
                PayrollDataContext.CCalculationLoanAdvances
                    .Where(c => c.UniqueId == ded.UniqueId)
                    .OrderByDescending(c => c.LoanAdvanceDetailId)
                    .ToList();

                if (loans.Count > 0)
                {
                    ded.Balance = loans[0].UnpaidBalance;
                }
            }
            return ded;
        }

        //[Obsolete()]
        //public PEmployeeDeduction GetEmployeeDeduction(int employeeId,int deductionId)
        //{
        //    return PayrollDataContext.PEmployeeDeductions.
        //        SingleOrDefault(p => (p.EmployeeId == employeeId && p.DeductionId == deductionId));
        //}


        public bool Update(PDeduction entity)
        {
            PDeduction dbEntity = PayrollDataContext.PDeductions
                .Where(d => d.DeductionId == entity.DeductionId).SingleOrDefault();
            if (dbEntity == null)
                return false;

            bool isCalculationTypeChanged = dbEntity.Calculation != entity.Calculation;
            dbEntity.EditSequence = entity.EditSequence;


            SetDeductionForUpdate(dbEntity, entity);

            SetConnectionPwd(PayrollDataContext.Connection);
            PayrollDataContext.Transaction = PayrollDataContext.Connection.BeginTransaction();
            try
            {
                UpdateChangeSet();
                // Second setup, set IsValid = false for other employees if the Calculation type is changed 
                if (isCalculationTypeChanged)
                    PayrollDataContext.UpdateDeductionForOtherEmployees(dbEntity.DeductionId,
                   0);


                PayrollDataContext.Transaction.Commit();
                return true;
            }
            catch (Exception exp)
            {
                Log.log("Error while updating deduction : " + entity.DeductionId, exp);
                PayrollDataContext.Transaction.Rollback();
                return false;
            }
            finally
            {
                //if (PayrollDataContext.Connection != null)
                //{
                //    PayrollDataContext.Connection.Close();
                //}
            }
            //return true;
        }

        //public bool Update(LoanType entity)
        //{
        //    LoanType dbEntity = PayrollDataContext.LoanTypes
        //        .Where(d => d.LoanTypeId == entity.LoanTypeId).SingleOrDefault();
        //    if (dbEntity == null)
        //        return false;

        //    bool isCalculationTypeChanged = dbEntity.Calculation != entity.Calculation;
        //    //dbEntity.EditSequence = entity.EditSequence;


        //    SetLoanForUpdate(dbEntity, entity);

        //    SetConnectionPwd(PayrollDataContext.Connection);
        //    PayrollDataContext.Transaction = PayrollDataContext.Connection.BeginTransaction();
        //    try
        //    {
        //        UpdateChangeSet();
        //        // Second setup, set IsValid = false for other employees if the Calculation type is changed 
        //        //if (isCalculationTypeChanged)
        //        //    PayrollDataContext.UpdateDeductionForOtherEmployees(dbEntity.DeductionId,
        //        //   0);


        //        PayrollDataContext.Transaction.Commit();
        //        return true;
        //    }
        //    catch (Exception exp)
        //    {
        //        Log.log("Error while updating loan : " + entity.LoanTypeId, exp);
        //        PayrollDataContext.Transaction.Rollback();
        //        return false;
        //    }
        //    finally
        //    {
        //        //if (PayrollDataContext.Connection != null)
        //        //{
        //        //    PayrollDataContext.Connection.Close();
        //        //}
        //    }
        //    return false;
        //}


        public static void SetDeductionForUpdate(PDeduction dbEntity, PDeduction entity)
        {

            #region "Change Log"

            AddToChangeSetting(dbEntity, entity, MonetoryChangeLogTypeEnum.Deduction, entity.Title,
                "Title", "Abbreviation", "Order", "EditSequence", "DeductionId", "CompanyId");

            #endregion

            dbEntity.Notes = entity.Notes;
            dbEntity.Title = entity.Title;
            dbEntity.Abbreviation = entity.Abbreviation;
            dbEntity.DeductionDepositListId = entity.DeductionDepositListId;
            dbEntity.Calculation = entity.Calculation;
            dbEntity.IsExemptFromIncomeTax = entity.IsExemptFromIncomeTax;
            dbEntity.HasVariableAmount = entity.HasVariableAmount;
            dbEntity.IsEnabled = entity.IsEnabled;
            // Sepecial case like when company give emp laptop & in returns the emp has to pay certain amt in certian interval
            // & the emp gets that amount at the time of Ret/Reg
            dbEntity.IsRefundableAdvance = entity.IsRefundableAdvance;
            dbEntity.RatePercentForAll = entity.RatePercentForAll;
            dbEntity.EnableStatusFilter = entity.EnableStatusFilter;
            dbEntity.PDeductionIncomes.Clear();
            dbEntity.PDeductionIncomes.AddRange(entity.PDeductionIncomes);

            dbEntity.PDeductionStatus.Clear();
            dbEntity.PDeductionStatus.AddRange(entity.PDeductionStatus);

        }

        //public static void SetLoanForUpdate(LoanType dbEntity, LoanType entity)
        //{

        //    #region "Change Log"

        //    AddToChangeSetting(dbEntity, entity, MonetoryChangeLogTypeEnum.LoanType, entity.Title,
        //        "Title", "Abbreviation", "Order", "EditSequence", "LoanId");

        //    #endregion

        //    dbEntity.Title = entity.Title;
        //    dbEntity.IsDifferenceType = entity.IsDifferenceType;
        //    dbEntity.Abbreviation = entity.Abbreviation;
        //    dbEntity.ParentLoanTypeId = entity.ParentLoanTypeId;
        //    dbEntity.EligibilityStatus = entity.EligibilityStatus;
        //    dbEntity.EligibilityType = entity.EligibilityType;
        //    dbEntity.EligibilityYears = entity.EligibilityYears;
        //    dbEntity.Calculation = entity.Calculation;
        //    dbEntity.CalculationValue = entity.CalculationValue;
        //    dbEntity.IsCalcualteProportionately = entity.IsCalcualteProportionately;
        //    dbEntity.ProportionateRemainingServiceYear = entity.ProportionateRemainingServiceYear;
        //    //dbEntity.CalculationValueBasedOn = entity.CalculationValueBasedOn;
        //    dbEntity.CalculationValueType = entity.CalculationValueType;
        //    // Sepecial case like when company give emp laptop & in returns the emp has to pay certain amt in certian interval
        //    // & the emp gets that amount at the time of Ret/Reg
        //    //dbEntity.IsRefundableAdvance = entity.IsRefundableAdvance;


        //    dbEntity.LoanMustHaveAnies.Clear();
        //    dbEntity.LoanMustHaveAnies.AddRange(entity.LoanMustHaveAnies);

        //    dbEntity.LoanShouldNotHaves.Clear();
        //    dbEntity.LoanShouldNotHaves.AddRange(entity.LoanShouldNotHaves);

        //    dbEntity.LoanTypeSalaryIncludes.Clear();
        //    dbEntity.LoanTypeSalaryIncludes.AddRange(entity.LoanTypeSalaryIncludes);

        //    dbEntity.LoanTypeDefinedAmounts.Clear();
        //    dbEntity.LoanTypeDefinedAmounts.AddRange(entity.LoanTypeDefinedAmounts);
        //}

        public void SaveEMIInstallment(PEmployeeDeduction dbEntity, PEmployeeDeduction entity, string uniqueID)
        {
            int startMonth = CustomDate.GetCustomDateFromString(entity.StartingFrom, IsEnglish).Month;
            int startYear = CustomDate.GetCustomDateFromString(entity.StartingFrom, IsEnglish).Year;

            //for (int i = 1; i <= entity.NoOfInstallments; i++)
            //{
            //    EMIAdjustment adj = new EMIAdjustment { DeductionId = entity.DeductionId, EmployeeId = entity.EmployeeId, UniqueId = uniqueID };
            //    adj.Month = startMonth;
            //    adj.Year = startYear;

            //}
        }

        public bool Update(PEmployeeDeduction entity)
        {
            PEmployeeDeduction dbEntity = GetEmployeeDeduction(entity.EmployeeDeductionId);

            if (dbEntity == null)
                return false;

            bool isCalculationTypeChanged = dbEntity.PDeduction.Calculation != entity.PDeduction.Calculation;
            dbEntity.EditSequence = entity.EditSequence;

            SetDeductionForUpdate(dbEntity.PDeduction, entity.PDeduction);
            SetPEmployeeDeduction(dbEntity, entity);

            string uniqueID = "";

            bool isUnitqueIDGenerated = false;

            if (dbEntity.PDeduction.Calculation == DeductionCalculation.LOAN
                || dbEntity.PDeduction.Calculation == DeductionCalculation.Advance)
            {
                // if new click from calcuation then only generate unique id generating in each update changes UniqueID
                // as if values changed from LoanAdjustment also
                if (entity.GenerateUniqueIDForFreshLoan)
                    uniqueID = PEmployeeDeduction.GenerateUniqueId(dbEntity);
                else
                    uniqueID = PEmployeeDeduction.GetUniqueId(dbEntity);


                if (dbEntity.PDeduction.Calculation == DeductionCalculation.LOAN)
                {
                    LoanSIChangedHistory dbhistory = PayrollDataContext.LoanSIChangedHistories
                        .FirstOrDefault(x => x.EmployeeId == dbEntity.EmployeeId && x.LoanDeductionId == dbEntity.DeductionId && x.UniqueID == uniqueID);

                    if (dbhistory == null)
                    {
                        dbhistory = new LoanSIChangedHistory();
                        PayrollDataContext.LoanSIChangedHistories.InsertOnSubmit(dbhistory);
                    }

                    dbhistory.EmployeeId = dbEntity.EmployeeId;
                    dbhistory.LoanDeductionId = dbEntity.DeductionId;
                    dbhistory.ChangedBy = SessionManager.User.UserID;
                    dbhistory.ChangedDate = GetCurrentDateAndTime();
                    dbhistory.LoanAmount = entity.AdvanceLoanAmount == null ? 0 : entity.AdvanceLoanAmount.Value;
                    dbhistory.TakenDate = GetEngDate(entity.TakenOn, IsEnglish);
                    dbhistory.StartDate = GetEngDate(entity.StartingFrom, IsEnglish);
                    dbhistory.InterestRate = entity.InterestRate.Value;
                    dbhistory.ToBePaidOver = entity.ToBePaidOver.Value;
                    dbhistory.MarketRate = entity.MarketRate.Value;
                    dbhistory.UniqueID = uniqueID;
                }


                if (dbEntity.UniqueId != null && dbEntity.UniqueId.Equals(uniqueID) == false)
                {
                    isUnitqueIDGenerated = true;


                }
                dbEntity.UniqueId = uniqueID;



            }


            // this has been commented so check side effect for it
            //if (isUnitqueIDGenerated)
            //    dbEntity.SINewPPMTAmount = 0;

            SetConnectionPwd(PayrollDataContext.Connection);
            PayrollDataContext.Transaction = PayrollDataContext.Connection.BeginTransaction();
            try
            {
                UpdateChangeSet();
                // Second setup, set IsValid = false for other employees if the Calculation type is changed 
                if (isCalculationTypeChanged)
                    PayrollDataContext.UpdateDeductionForOtherEmployees(dbEntity.PDeduction.DeductionId,
                   dbEntity.EmployeeId);

                

                PayrollDataContext.Transaction.Commit();
                return true;
            }
            catch (Exception exp)
            {
                Log.log("Error while updating emp ded : " + entity.EmployeeDeductionId, exp);
                PayrollDataContext.Transaction.Rollback();
                return false;
            }
            finally
            {
                //if (PayrollDataContext.Connection != null)
                //{
                //    PayrollDataContext.Connection.Close();
                //}
            }
            //return false;



        }



        public void SetPEmployeeDeduction(PEmployeeDeduction dbEntity, PEmployeeDeduction entity)
        {

            #region "Change Log"

            //for Amount
            if (entity.Amount.HasValue && dbEntity.Amount != entity.Amount)
                PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(
                    dbEntity.EmployeeId,(byte) MonetoryChangeLogTypeEnum.Deduction, dbEntity.PDeduction.Title, dbEntity.Amount, entity.Amount,
                    LogActionEnum.Update)
                    );
            //for Percent
            else if (entity.Rate.HasValue && dbEntity.Rate != entity.Rate)
                PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(
                    dbEntity.EmployeeId, (byte)MonetoryChangeLogTypeEnum.Deduction, dbEntity.PDeduction.Title, GetString( dbEntity.Rate)+ "%" ,GetString( entity.Rate) +"%", 
                     LogActionEnum.Update)
                    );
            //for Advance/Loan
            else if (entity.InstallmentAmount.HasValue && dbEntity.InstallmentAmount != entity.InstallmentAmount)
                PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(
                    dbEntity.EmployeeId, (byte)MonetoryChangeLogTypeEnum.Deduction, dbEntity.PDeduction.Title, dbEntity.InstallmentAmount, entity.InstallmentAmount, 
                   LogActionEnum.Update)
                    );

            if (dbEntity.LoanAccount != entity.LoanAccount)
                PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(
                    dbEntity.EmployeeId, (byte)MonetoryChangeLogTypeEnum.Deduction, dbEntity.PDeduction.Title, dbEntity.LoanAccount, entity.LoanAccount,
                   LogActionEnum.Update)
                    );

            #endregion

            dbEntity.AdvanceLoanAmount = entity.AdvanceLoanAmount;
            dbEntity.Amount = entity.Amount;
            dbEntity.CutOff = entity.CutOff;

            dbEntity.Rate = entity.Rate;
            dbEntity.AdvanceLoanAmount = entity.AdvanceLoanAmount;

            dbEntity.TakenOn = entity.TakenOn;
            if (dbEntity.TakenOn != null)
                dbEntity.TakenOnEng = GetEngDate(entity.TakenOn, IsEnglish);
            dbEntity.StartingFrom = entity.StartingFrom;
            if (dbEntity.StartingFrom != null)
                dbEntity.StartingFromEng = GetEngDate(entity.StartingFrom, IsEnglish);
            dbEntity.LastPayment = entity.LastPayment;
            if (dbEntity.LastPayment != null)
                dbEntity.LastPaymentEng = GetEngDate(entity.LastPayment, IsEnglish);

            dbEntity.AdvanceLoanAmount = entity.AdvanceLoanAmount;
            dbEntity.AmountWithInitialInterest = entity.AmountWithInitialInterest;
            
            dbEntity.TakenOn = entity.TakenOn;
            dbEntity.InterestRate = entity.InterestRate;
            dbEntity.MarketRate = entity.MarketRate;
            dbEntity.ToBePaidOver = entity.ToBePaidOver;
            dbEntity.PaymentTerms = entity.PaymentTerms;
            dbEntity.InterestAmount = entity.InterestAmount;
            dbEntity.LoanAccount = entity.LoanAccount;
            dbEntity.InstallmentNo = entity.InstallmentNo;
            dbEntity.TotalInstallment = entity.TotalInstallment;
            dbEntity.LoanFixedInstallmentAmount = entity.LoanFixedInstallmentAmount;
            if (dbEntity.LoanFixedInstallmentAmount != null && dbEntity.LoanFixedInstallmentAmount.Value)
                dbEntity.SINewPPMTAmount = entity.InstallmentAmount;
            else
                dbEntity.SINewPPMTAmount = 0;
            dbEntity.InstallmentAmount = entity.InstallmentAmount;
            //as paid & rem installment are generated dynamically will be
            //generated dynamically from CCalcualtionLoanAdvance table
            //dbEntity.Paid = entity.Paid;
            //dbEntity.RemainingInstallments = entity.RemainingInstallments;
            dbEntity.Balance = entity.Balance;
            dbEntity.NoOfInstallments = entity.NoOfInstallments;
            dbEntity.ExcludeLoanInstallment = entity.ExcludeLoanInstallment;

            dbEntity.IsValid = entity.IsValid;
        }




        #endregion

        public static void DeleteAllEmployeeAdjustemntForPeriod(int payrollperiodId, int employeeId)
        {
            List<IncomeDeductionAdjustment> list = PayrollDataContext.IncomeDeductionAdjustments
                .Where(x => x.PayrollPeriodId == payrollperiodId && x.EmployeeId == employeeId).ToList();

            PayrollDataContext.IncomeDeductionAdjustments.DeleteAllOnSubmit(list);

            List<SettlementLeaveDetail> leaves = PayrollDataContext.SettlementLeaveDetails
                .Where(x => x.EmployeeId == employeeId && x.PayrollPeriodId == payrollperiodId).ToList();
            foreach (SettlementLeaveDetail item in leaves)
            {
                if (item.LeaveEncashmentDaysOrHours != null)
                {
                    if (item.LeaveEncashmentDaysOrHours > 0)
                    {
                        item.RetirementAdjustment = 0;
                    }
                }
            }

            List<SettlementDetail> details = PayrollDataContext.SettlementDetails
               .Where(x => x.EmployeeId == employeeId && x.PayrollPeriodId == payrollperiodId).ToList();

            foreach (SettlementDetail item in details)
            {
                SettlementDetailType type = (SettlementDetailType)item.Type;

                switch (type)
                {

                    case SettlementDetailType.DashainBonus:
                        item.RetirementAdjustment = 0;
                        break;

                    case SettlementDetailType.Gratuity:
                        item.RetirementAdjustment = 0;
                        item.TDSAdjustment = 0;
                        break;
                }
            }

            PayrollDataContext.SubmitChanges();
        }

        public static void DeleteRetirement(int payrollperiodId, int employeeId)
        {
            EEmployee emp = new EmployeeManager().GetById(employeeId);

            if (emp.IsResigned != null && emp.IsResigned.Value)
            {

                PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(employeeId, (byte)MonetoryChangeLogTypeEnum.Information
                        , "Resignation", emp.EHumanResources[0].DateOfResignation, "", LogActionEnum.Update));
            }

            if (emp.IsRetired != null && emp.IsRetired.Value)
            {

                PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(employeeId, (byte)MonetoryChangeLogTypeEnum.Information
                        , "Retirement", emp.EHumanResources[0].DateOfRetirement, "", LogActionEnum.Update));
            }
            emp.IsResigned = false;
            emp.IsRetired = false;




            List<IncomeDeductionAdjustment> list = PayrollDataContext.IncomeDeductionAdjustments
                .Where(x => x.PayrollPeriodId == payrollperiodId && x.EmployeeId == employeeId).ToList();

            PayrollDataContext.IncomeDeductionAdjustments.DeleteAllOnSubmit(list);

            List<SettlementLeaveDetail> leaves = PayrollDataContext.SettlementLeaveDetails
                .Where(x => x.EmployeeId == employeeId && x.PayrollPeriodId == payrollperiodId).ToList();
            PayrollDataContext.SettlementLeaveDetails.DeleteAllOnSubmit(leaves);


            List<SettlementLeaveBalanceAdjustment> adjustments = PayrollDataContext.SettlementLeaveBalanceAdjustments
                .Where(x => x.EmployeeId == employeeId && x.PayrollPeriodId == payrollperiodId).ToList();
            PayrollDataContext.SettlementLeaveBalanceAdjustments.DeleteAllOnSubmit(adjustments);


            List<SettlementDetail> details = PayrollDataContext.SettlementDetails
               .Where(x => x.EmployeeId == employeeId && x.PayrollPeriodId == payrollperiodId).ToList();
            PayrollDataContext.SettlementDetails.DeleteAllOnSubmit(details);

            if (emp.EHumanResources[0].RetirementEventSourceRef_ID != null)
            {
                EmployeeServiceHistory retirementHistory = PayrollDataContext.EmployeeServiceHistories
                    .FirstOrDefault(x => x.EventSourceID == emp.EHumanResources[0].RetirementEventSourceRef_ID);
                if (retirementHistory != null)
                    PayrollDataContext.EmployeeServiceHistories.DeleteOnSubmit(retirementHistory);
                emp.EHumanResources[0].RetirementEventSourceRef_ID = null;
            }

            PayrollDataContext.SubmitChanges();

            PayrollPeriod period = CommonManager.GetPayrollPeriod(payrollperiodId);
            EmployeeManager.ResaveAttendanceOfEmployee(employeeId, period);
          

            
        }
        public static List<KeyValue> GetEmployeesIncomeDeductionList(int payrollPeriodId)
        {
            List<KeyValue> values = new List<KeyValue>();

            List<GetEmployeesIncomeDeductionListResult> list = PayrollDataContext.GetEmployeesIncomeDeductionList(SessionManager.CurrentCompanyId, payrollPeriodId)
                .ToList();

            foreach (GetEmployeesIncomeDeductionListResult item in list)
            {
                KeyValue value = new KeyValue();

                if (item.IsIncome.Value)
                {
                    value.Value = item.SourceId.ToString() + ":" + ((int)CalculationColumnType.Income).ToString();
                    value.Key = item.HeaderName + " (Income)";
                }
                else
                {
                    value.Value = item.SourceId.ToString() + ":" + ((int)CalculationColumnType.Deduction).ToString();
                    value.Key = item.HeaderName + " (Deduction)";
                }
                values.Add(value);
            }

            // do not bring cit here, cit can be adjusted from CIT Page

            //KeyValue cit = new KeyValue("CIT",
            //   ((int)CalculationColumnType.DeductionCIT).ToString() + ":" + ((int)CalculationColumnType.DeductionCIT).ToString());
            //values.Add(cit);

            // add SST and TDS
            KeyValue sst = new KeyValue("SST",
                ((int)CalculationColumnType.SST).ToString() + ":" + ((int)CalculationColumnType.SST).ToString());
            values.Add(sst);

            KeyValue tds = new KeyValue("TDS",
               ((int)CalculationColumnType.TDS).ToString() + ":" + ((int)CalculationColumnType.TDS).ToString());
            values.Add(tds);


            // Income PF(Employer contribution)
            KeyValue pfIncome = new KeyValue("PF (Income)",
              ((int)CalculationColumnType.IncomePF).ToString() + ":" + ((int)CalculationColumnType.IncomePF).ToString());
            values.Add(pfIncome);


            KeyValue entity = new KeyValue("PF (Deduction)",
              ((int)CalculationColumnType.DeductionPF).ToString() + ":" + ((int)CalculationColumnType.DeductionPF).ToString());
            values.Add(entity);


            // Leave Encashment
            KeyValue leaveEncash = new KeyValue("Leave Encashment (Income)",
                   ((int)CalculationColumnType.IncomeLeaveEncasement).ToString() + ":" + ((int)CalculationColumnType.IncomeLeaveEncasement).ToString());
            values.Add(leaveEncash);

            //#region HPL Fixed Allowance Incomes
            //if (CommonManager.CalculationConstant.CompanyIsHPL != null && CommonManager.CalculationConstant.CompanyIsHPL.Value)
            //{
            //    DAL.HPLAllowanceRate rate = HPLAllowanceManager.GetAllowanceRate();
            //    KeyValue allowance = new KeyValue();


            //    allowance = new KeyValue
            //    {
            //        Key = (string.IsNullOrEmpty(rate.PalatiHeader) ? "Head work allowance" : rate.PalatiHeader) + "(Allowance)",
            //        Value = (int)CalculationColumnType.IncomeSiteAllowance + ":" + (int)CalculationColumnType.IncomeSiteAllowance
            //    };
            //    values.Add(allowance);


            //    allowance = new KeyValue
            //    {
            //        Key = (string.IsNullOrEmpty(rate.OnCallHeader) ? "On call allowance" : rate.OnCallHeader) + "(Allowance)",
            //        Value = (int)CalculationColumnType.IncomeOnCallAllowance + ":" + (int)CalculationColumnType.IncomeOnCallAllowance
            //    };
            //    values.Add(allowance);

            //    allowance = new KeyValue
            //    {
            //        Key = (string.IsNullOrEmpty(rate.ShiftAfternoonHeader) ? "Shift afternoon" : rate.ShiftAfternoonHeader) + "(Allowance)",
            //        Value = (int)CalculationColumnType.IncomeShiftAfternoon + ":" + (int)CalculationColumnType.IncomeShiftAfternoon
            //    };
            //    values.Add(allowance);

            //    allowance = new KeyValue
            //    {
            //        Key = (string.IsNullOrEmpty(rate.ShiftNightHeader) ? "Shift night" : rate.ShiftNightHeader) + "(Allowance)",
            //        Value = (int)CalculationColumnType.IncomeShiftNight + ":" + (int)CalculationColumnType.IncomeShiftNight
            //    };
            //    values.Add(allowance);

            //    allowance = new KeyValue
            //    {
            //        Key = (string.IsNullOrEmpty(rate.SplitShiftMorningHeader) ? "Shift morning" : rate.SplitShiftMorningHeader) + "(Allowance)",
            //        Value = (int)CalculationColumnType.IncomeShiftMorning + ":" + (int)CalculationColumnType.IncomeShiftMorning
            //    };
            //    values.Add(allowance);

            //    allowance = new KeyValue
            //    {
            //        Key = (string.IsNullOrEmpty(rate.SplitShiftEveningHeader) ? "Shift evening" : rate.SplitShiftEveningHeader) + "(Allowance)",
            //        Value = (int)CalculationColumnType.IncomeShiftEvening + ":" + (int)CalculationColumnType.IncomeShiftEvening
            //    };
            //    values.Add(allowance);

            //    allowance = new KeyValue
            //    {
            //        Key = (string.IsNullOrEmpty(rate.OT150Header) ? "Overtime 150%" : rate.OT150Header) + "(Allowance)",
            //        Value = (int)CalculationColumnType.IncomeOvertime150 + ":" + (int)CalculationColumnType.IncomeOvertime150
            //    };
            //    values.Add(allowance);

            //    allowance = new KeyValue
            //    {
            //        Key = (string.IsNullOrEmpty(rate.OT200Header) ? "Overtime 200%" : rate.OT200Header) + "(Allowance)",
            //        Value = (int)CalculationColumnType.IncomeOvertime200 + ":" + (int)CalculationColumnType.IncomeOvertime200
            //    };
            //    values.Add(allowance);

            //    allowance = new KeyValue
            //    {
            //        Key = (string.IsNullOrEmpty(rate.OT300Header) ? "Overtime 300%" : rate.OT300Header) + "(Allowance)",
            //        Value = (int)CalculationColumnType.IncomeOvertime300 + ":" + (int)CalculationColumnType.IncomeOvertime300
            //    };
            //    values.Add(allowance);


            //    allowance = new KeyValue
            //    {
            //        Key = (string.IsNullOrEmpty(rate.PublicHolidayHeader) ? "Public holiday" : rate.PublicHolidayHeader) + "(Allowance)",
            //        Value = (int)CalculationColumnType.IncomeOvertimePublicHoliday + ":" + (int)CalculationColumnType.IncomeOvertimePublicHoliday
            //    };
            //    values.Add(allowance);

                

            //}

            //#endregion

            return values;
        }



        #region Loan adjustment
        public static List<EEmployee> GetSimpleLoansEmployees(int deductionLoanId)
        {

            var query = from p in PayrollDataContext.PEmployeeDeductions
                        join e in PayrollDataContext.EEmployees on p.EmployeeId equals e.EmployeeId
                        where p.DeductionId == deductionLoanId && p.IsValid == true 
                            
                        orderby e.Name
                        select e;



            return query.ToList();
        }


        public static List<PDeduction> GetSimpleLoans()
        {
            return
                PayrollDataContext.PDeductions
                    .Where(d => d.Calculation==DeductionCalculation.LOAN
            && d.CompanyId == SessionManager.CurrentCompanyId && d.IsEMI == false)
                    .ToList();

        }

        public static List<PDeduction> GetEMILoans()
        {
            return
                PayrollDataContext.PDeductions
                    .Where(d => d.Calculation == DeductionCalculation.LOAN
            && d.CompanyId == SessionManager.CurrentCompanyId && d.IsEMI == true
            )
                    .ToList();

        }

        public static List<PDeduction> GetAdvances()
        {
            return
                PayrollDataContext.PDeductions
                    .Where(d => d.CompanyId == SessionManager.CurrentCompanyId && d.Calculation==DeductionCalculation.Advance)
                    .OrderBy(a=>a.Order)
                    .ToList();

        }

        public static LoanAdjustment GetLastNotUsedInCalculationLoanAdjustment(int employeeId, int deductionLoanId)
        {
            PEmployeeDeduction ded = PayManager.GetEmployeeDeduction(employeeId, deductionLoanId);

            LoanAdjustment lastLoan = PayrollDataContext.LoanAdjustments
                .Where(l => l.EmployeeId == employeeId && l.DeductionId == deductionLoanId && l.UniqueId.Equals(ded.UniqueId))
                .OrderByDescending(l => l.PayrollPeriodId)
                .Take(1).SingleOrDefault();

            if (lastLoan != null)
            {
                //if payroll period not saved then return the loan object
                if (!CalculationManager.IsCalculationSavedForEmployee(lastLoan.PayrollPeriodId, employeeId))
                    return lastLoan;

            }

            return null;
        }


        public static List<PIncome> GetVariablePFIncomes()
        {
            return PayrollDataContext.PIncomes.Where(p => p.IsPFCalculated == true).ToList();
        }

        public static List<PIncome> GetVariableIncomes(List<int> ids,bool isDeduction)
        {
            List<PIncome> incomes = new List<PIncome>();

            if (isDeduction == false)
            {
                foreach (int id in ids)
                {
                    incomes.Add(PayrollDataContext.PIncomes.SingleOrDefault(p => p.IncomeId == id));
                }
            }
            else
            {
                foreach (int id in ids)
                {
                    PDeduction ded = PayrollDataContext.PDeductions.SingleOrDefault(p => p.DeductionId == id);
                    if (ded != null)
                        incomes.Add(new PIncome { IncomeId = ded.DeductionId, Title = ded.Title });

                }
            }
            return incomes;
        }

        public static List<GetEmployeesForVariableIncomesResult> GetEmployeeForVariableIncomes(string incomeID,bool isDeduction)
        {
            string branchList = null;
            if (SessionManager.IsCustomRole)
                branchList = SessionManager.CustomRoleBranchIDList;


            PayrollPeriod payroll =  CommonManager.GetLastPayrollPeriod();
            int payrollid = payroll == null ? 0 : payroll.PayrollPeriodId;
            return
                PayrollDataContext.GetEmployeesForVariableIncomes(incomeID, payrollid, isDeduction,branchList).ToList();
        }


        public static List<GetEmployeesForVariablePFIncomesResult> GetEmployeesForVariablePFIncomes()
        {
            return PayrollDataContext.GetEmployeesForVariablePFIncomes().ToList();
        }


        public static ResponseStatus SaveVariablePFIncome(List<GetEmployeesIncomeForPFVariableIncomesResult> projectPays,
          bool isDeduction, ref int count)
        {
            count = 0;
            ResponseStatus status = new ResponseStatus();
            PayManager mgr = new PayManager();

            // first delete all
            List<ExcludePFIncome> list = PayrollDataContext.ExcludePFIncomes.ToList();
            PayrollDataContext.ExcludePFIncomes.DeleteAllOnSubmit(list);
            PayrollDataContext.SubmitChanges();

            List<PEmployeeIncome> empIncomeList = new List<PEmployeeIncome>();
            List<PEmployeeDeduction> empDeductionList = new List<PEmployeeDeduction>();

            foreach (GetEmployeesIncomeForPFVariableIncomesResult empIncome in projectPays)
            {

                //PEmployeeIncome income = empIncomeList.FirstOrDefault
                //     (x => x.IncomeId == empIncome.IncomeId && x.EmployeeId == empIncome.EmployeeId);

                    List<ExcludePFIncome> _dbExcludePFIncomeList = PayrollDataContext.ExcludePFIncomes.Where(x =>x.EmployeeId==empIncome.EmployeeId).ToList();
                    PayrollDataContext.ExcludePFIncomes.DeleteAllOnSubmit(_dbExcludePFIncomeList);
                    count += 1;

                    PIncome dbIncome = mgr.GetIncomeById(empIncome.IncomeId);
                    ExcludePFIncome newIncome = new ExcludePFIncome();
                    newIncome.EmployeeId = empIncome.EmployeeId;
                    newIncome.IncomeId = empIncome.IncomeId;
                    if (EmployeeManager.IsEmployeeIDExists(empIncome.EmployeeId) == false)
                    {
                        status.ErrorMessage = string.Format("Employee ID \"{0}\" does not exists.", empIncome.EmployeeId);
                        return status;
                    }

                    PayrollDataContext.ExcludePFIncomes.InsertOnSubmit(newIncome);
            }

            UpdateChangeSet();

            return status;

        }
        public static Status ImportRetirementDate(List<EmployeeRet> list, ref int count, ref string pastRetirementList)
        {
            Status status = new Status();
            count = 0;
            pastRetirementList = "";

            foreach (var item in list)
            {

                EEmployee emp = PayrollDataContext.EEmployees.FirstOrDefault(x => x.EmployeeId == item.EIN);

                bool setChangeRetirementDate = false;

                
                // if not retired
                //if (emp.IsRetired == null || emp.IsRetired == false)
                //{
                //    setChangeRetirementDate = true;
                //}
                //    // if retired with future date then change also
                //else if (emp.IsRetired != null && emp.IsRetired.Value && emp.EHumanResources[0].DateOfRetirementEng > item.RetirementDate)
                //{
                //    setChangeRetirementDate = true;
                //}
                //    // if already retired in past date then do not change anything show list only
                //else
                //{
                //    if (pastRetirementList == "")
                //        pastRetirementList = item.EIN.ToString();
                //    else
                //        pastRetirementList += ", " + item.EIN;
                //}

                // if already not retiremd
                //if (setChangeRetirementDate)
                {
                    count += 1;

                    emp.IsRetired = true;

                    DateTime engDate = item.RetirementDate;
                    CustomDate cdate = new CustomDate(engDate.Day, engDate.Month, engDate.Year, true);
                    string date = "";

                    if (IsEnglish)
                        date = cdate.ToString();
                    else
                        date = CustomDate.ConvertEngToNep(cdate).ToString();

                    string beforeValue = "";

                    if (emp.IsRetired != null && emp.IsRetired.Value)
                    {
                        beforeValue = emp.EHumanResources[0].DateOfRetirement;
                    }


                    emp.EHumanResources[0].DateOfRetirement = date;
                    emp.EHumanResources[0].DateOfRetirementEng = engDate;


                   

                    PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(
                                       item.EIN, (byte)MonetoryChangeLogTypeEnum.Information,
                                       "Retirement date imported", beforeValue, date, LogActionEnum.Add)
                                       );

                }
                
            }


            PayrollDataContext.SubmitChanges();

            return status;

        }
        public static ResponseStatus SaveVariableIncome(List<GetEmployeesIncomeForVariableIncomesResult> projectPays,
            bool isDeduction,ref int count)
        {
            count = 0;
            ResponseStatus status = new ResponseStatus();
            PayManager mgr = new PayManager();


            List<PEmployeeIncome> empIncomeList = new List<PEmployeeIncome>();
            List<PEmployeeDeduction> empDeductionList = new List<PEmployeeDeduction>();
            List<PIncome> incomeList = GetIncomeList();
            List<int> einList = projectPays.Select(x => x.EmployeeId).Distinct().ToList();
            List<int> idList = projectPays.Select(x => x.IncomeId).Distinct().ToList();

            if (isDeduction == false)
            {
                empIncomeList = PayrollDataContext.PEmployeeIncomes
                    .Where(x => einList.Contains(x.EmployeeId) && idList.Contains(x.IncomeId)).ToList();

            }
            else
            {
                empDeductionList = PayrollDataContext.PEmployeeDeductions
                    .Where(x => einList.Contains(x.EmployeeId) && idList.Contains(x.DeductionId)).ToList();
            }

            foreach (GetEmployeesIncomeForVariableIncomesResult empIncome in projectPays)
            {
               

                if (isDeduction == false)
                {
                    PEmployeeIncome income = empIncomeList.FirstOrDefault
                         (x => x.IncomeId == empIncome.IncomeId && x.EmployeeId == empIncome.EmployeeId);

                    bool isUnitRateIncomeType = incomeList.Any(x => x.IncomeId == empIncome.IncomeId && x.Calculation == IncomeCalculation.Unit_Rate);

                    count += 1;

                    if (income != null)
                    {

                        if (income.Amount != empIncome.Amount)
                        {
                            PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(
                                       empIncome.EmployeeId, (byte)MonetoryChangeLogTypeEnum.Income,
                                       income.PIncome.Title, income.Amount, empIncome.Amount, LogActionEnum.Update)
                                       );

                            // if previous amount is not 0 then only prevent to change
                            if (Convert.ToDecimal(income.Amount) != 0)
                            {
                                // here is should check if there is amount in previous salary and show warning instead of 
                                // checking previous increment


                                // validate for Fixed income it should not have gone to Increment
                                if (income.PIncome.Calculation == IncomeCalculation.FIXED_AMOUNT
                                    && EmployeeManager.HasEmployeeAndIncomeIncludedInCalculation(income.IncomeId, income.EmployeeId))
                                //&& PayrollDataContext.PEmployeeIncrements.Any(x => x.EmployeeIncomeId == income.EmployeeIncomeId))
                                {
                                    status.ErrorMessage = string.Format("Fixed type of income \"{1}\" is already used in the salary of employee :  \"{0}\", so its amount can not be changed like variable incomes, please use mass increment to change the amount.",
                                         EmployeeManager.GetEmployeeName(income.EmployeeId) + " - " +  empIncome.EmployeeId, income.PIncome.Title);

                                    //status.ErrorMessage = string.Format("For Employee ID \"{0}\", Income \"{1}\" is already in increment status, so its amount can not be changed like variable incomes.",
                                    //    empIncome.EmployeeId, income.PIncome.Title);
                                    return status;
                                }
                            }
                        }
                        income.IsValid = true;
                        income.Amount = empIncome.Amount;
                        if (isUnitRateIncomeType)
                            income.RatePercent = empIncome.Amount == null ? 0 : (double)empIncome.Amount;

                    }
                    //if not associated then associate it
                    else
                    {
                        PIncome dbIncome = mgr.GetIncomeById(empIncome.IncomeId);

                        PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(
                                   empIncome.EmployeeId, (byte)MonetoryChangeLogTypeEnum.Income,
                                   dbIncome.Title, null, empIncome.Amount, LogActionEnum.Update)
                                   );

                        PEmployeeIncome newIncome = new PEmployeeIncome();
                        newIncome.EmployeeId = empIncome.EmployeeId;
                        newIncome.IncomeId = empIncome.IncomeId;
                        newIncome.Amount = empIncome.Amount;
                        if (isUnitRateIncomeType)
                            newIncome.RatePercent = empIncome.Amount == null ? 0 : (double)empIncome.Amount;
                        newIncome.IsValid = true;
                        newIncome.IsEnabled = true;

                        if (EmployeeManager.IsEmployeeIDExists(empIncome.EmployeeId) == false)
                        {
                            status.ErrorMessage = string.Format("Employee ID \"{0}\" does not exists.", empIncome.EmployeeId);
                            return status;
                        }

                        PayrollDataContext.PEmployeeIncomes.InsertOnSubmit(newIncome);
                    }
                }
                else
                {
                    PEmployeeDeduction deduction = empDeductionList.FirstOrDefault
                         (x => x.DeductionId == empIncome.IncomeId && x.EmployeeId == empIncome.EmployeeId);

                    count += 1;

                    if (deduction != null)
                    {
                        if (deduction.Amount != empIncome.Amount)
                            PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(
                                      empIncome.EmployeeId, (byte)MonetoryChangeLogTypeEnum.Deduction,
                                      deduction.PDeduction.Title, deduction.Amount, empIncome.Amount, LogActionEnum.Update)
                                      );

                        deduction.IsValid = true;
                        deduction.Amount = empIncome.Amount;

                    }
                    else
                    {
                        PDeduction dbDeduction = mgr.GetDeductionById(empIncome.IncomeId);

                        PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(
                                   empIncome.EmployeeId, (byte)MonetoryChangeLogTypeEnum.Deduction,
                                   dbDeduction.Title, null, empIncome.Amount, LogActionEnum.Update)
                                   );

                        PEmployeeDeduction ded = new PEmployeeDeduction();
                        ded.EmployeeId = empIncome.EmployeeId;
                        ded.DeductionId = empIncome.IncomeId;
                        ded.Amount = empIncome.Amount;
                        ded.IsValid = true;

                        if (EmployeeManager.IsEmployeeIDExists(empIncome.EmployeeId) == false)
                        {
                            status.ErrorMessage = string.Format("Employee ID \"{0}\" does not exists.", empIncome.EmployeeId);
                            return status;
                        }

                        PayrollDataContext.PEmployeeDeductions.InsertOnSubmit(ded);
                    }
                }

               

            }
            
            UpdateChangeSet();

            return status;

        }


       

        public static bool HasDeemedIncome()
        {
            return
                PayrollDataContext.PIncomes.Any(x => x.Calculation == "DeemedIncome");
        }
        public static ResponseStatus SaveDeemedIncome(List<PEmployeeIncome> list, ref int count,PIncome income)
        {
            count = 0;
            ResponseStatus status = new ResponseStatus();

            foreach (PEmployeeIncome empIncome in list)
            {


               
                    PEmployeeIncome dbEmployeeIncome = PayrollDataContext.PEmployeeIncomes.SingleOrDefault
                         (x => x.IncomeId == income.IncomeId && x.EmployeeId == empIncome.EmployeeId);

                    count += 1;

                    if (dbEmployeeIncome != null)
                    {

                        if (income.DeemedIsAmount != null && income.DeemedIsAmount.Value)
                        {
                            dbEmployeeIncome.Amount = empIncome.Amount;
                            dbEmployeeIncome.MarketValue = empIncome.MarketValue;
                        }
                        else
                        {
                            dbEmployeeIncome.RatePercent = empIncome.RatePercent;
                        }

                        dbEmployeeIncome.IsValid = true;

                    }
                    //if not associated then associate it
                    else
                    {
                        PEmployeeIncome newIncome = new PEmployeeIncome();
                        newIncome.EmployeeId = empIncome.EmployeeId;
                        newIncome.IncomeId = income.IncomeId;
                        if (income.DeemedIsAmount != null && income.DeemedIsAmount.Value)
                        {
                            newIncome.Amount = empIncome.Amount;
                            newIncome.MarketValue = empIncome.MarketValue;
                        }
                        else
                        {
                            newIncome.RatePercent = empIncome.RatePercent;
                        }
                        newIncome.IsValid = true;
                        newIncome.IsEnabled = true;

                        if (EmployeeManager.IsEmployeeIDExists(empIncome.EmployeeId) == false)
                        {
                            status.ErrorMessage = string.Format("Employee ID \"{0}\" does not exists.", empIncome.EmployeeId);
                            return status;
                        }

                        PayrollDataContext.PEmployeeIncomes.InsertOnSubmit(newIncome);
                    }
              


            }

            UpdateChangeSet();

            return status;

        }
        public static List<GetEmployeesIncomeForVariableIncomesResult> GetEmployeesIncomeForVariableIncomes(string incomeID, bool isDeduction)
        {
            PayrollPeriod payroll = CommonManager.GetLastPayrollPeriod();
            int payrollid = payroll == null ? 0 : payroll.PayrollPeriodId;
            return
                PayrollDataContext.GetEmployeesIncomeForVariableIncomes(incomeID, payrollid,isDeduction).ToList();
        }

        public static List<GetEmployeesIncomeForPFVariableIncomesResult> GetEmployeesIncomeForPFVariableIncomes()
        {
            PayrollPeriod payroll = CommonManager.GetLastPayrollPeriod();
            int payrollid = payroll == null ? 0 : payroll.PayrollPeriodId;
            return
                PayrollDataContext.GetEmployeesIncomeForPFVariableIncomes().ToList();
        }


        public static AdvanceAdjustment GetLastNotUsedInCalculationAdvanceAdjustment(int employeeId, int deductionLoanId)
        {
			 PEmployeeDeduction ded = PayManager.GetEmployeeDeduction(employeeId, deductionLoanId);
			  
            AdvanceAdjustment lastAdv = PayrollDataContext.AdvanceAdjustments
                .Where(l => l.EmployeeId == employeeId && l.DeductionId == deductionLoanId && l.UniqueId.Equals(ded.UniqueId))
                .OrderByDescending(l => l.PayrollPeriodId)
                .Take(1).SingleOrDefault();

            if (lastAdv != null)
            {
                //if payroll period not saved then return the loan object
                if (!CalculationManager.IsCalculationSavedForEmployee(lastAdv.PayrollPeriodId, employeeId))
                    return lastAdv;

            }
            
            return null;
        }

        //public static decimal CalculateSimpleInterestRate(decimal opening, double interestRate)
        //{
        //    return opening * ((decimal)interestRate / 100 / 12);
        //}

        public static decimal CalculateSimpleInterestRate(decimal opening, double interestRate,double daysInTheMonth)
        {
            //if (CommonManager.CompanySetting.WhichCompany == WhichCompany.HPL)
            //{
            //    return opening * ((decimal)interestRate / 100 / 12);
            //}

            if (daysInTheMonth == 0)
                return 0;

            opening = (opening * ((decimal)interestRate / 100));// / 365) / days;
            return opening / (decimal)(365.0 / daysInTheMonth);
        }

        //public static decimal CalculateSimpleInterestRate(decimal opening, double interestRate, double days)
        //{
        //    return opening * ((decimal)interestRate) * (decimal)days / (decimal)365.0;
        //}

        //public static decimal CalculateEMIRate(decimal opening, double interestRate)
        //{
        //    return opening * ((decimal)interestRate / 100 / 12);
        //}

        /// <summary>
        /// Ref "EMI/Loan Adjustment.xlsx", can be used for Simple loan only
        /// </summary>
        /// <param name="deductionId"></param>
        /// <param name="employeeId"></param>
        /// <returns></returns>
        public static List<GetLoanAdjustmentInstallmentsResult> GetLoanAdjustments(int deductionId, int employeeId, ref bool hasCurrentPayrollPeriod,ref bool isInstallmentComplete
            , double interestRate, decimal? newPPMT, int noOfPayments)
        {
            List<GetLoanAdjustmentInstallmentsResult> installments
                = PayrollDataContext.GetLoanAdjustmentInstallments(employeeId, deductionId).ToList();
            PEmployeeDeduction empDeduction = GetEmployeeDeduction(employeeId, deductionId);


            // Installment Complete 
            //if (installments.Count == empDeduction.ToBePaidOver.Value)
            //{
            //    isInstallmentComplete = true;
            //    return installments;
            //}

            //get last loan adjustment that is not being used in calculation so that the adjustment calculation can be done using this object value
            LoanAdjustment lastNotUsedLoanAdjustment = GetLastNotUsedInCalculationLoanAdjustment(employeeId, deductionId);



            int validPayrollPeriodId = 0;
            PayrollPeriod validPayrollPeriod = CommonManager.GetValidLastPayrollPeriod();
            if (validPayrollPeriod != null)
            {
                if (CalculationManager.IsCalculationSavedForEmployee(validPayrollPeriod.PayrollPeriodId, employeeId))
                    validPayrollPeriod = null;
                else
                    validPayrollPeriodId = validPayrollPeriod.PayrollPeriodId;
            }


            int numberOfInstallments = 0;
            int startingMonth = 0, startingYear;
            decimal opening, closing, ppmt, ipmt;
            bool notUsedInCalculation = installments.Count == 0;
            //  int loanYear;

            //if not used in calculation then generate first row
            if (installments.Count <= 0)
            {

                GetLoanAdjustmentInstallmentsResult firstItem = new GetLoanAdjustmentInstallmentsResult();
                firstItem.RemainingInstallments = empDeduction.ToBePaidOver;
                firstItem.Month = CustomDate.GetCustomDateFromString(empDeduction.StartingFrom, IsEnglish).Month;
                firstItem.Year = CustomDate.GetCustomDateFromString(empDeduction.StartingFrom, IsEnglish).Year;
                startingYear = firstItem.Year.Value;
                firstItem.Opening = empDeduction.AdvanceLoanAmount;
                firstItem.PPMT = empDeduction.AdvanceLoanAmount / (decimal) noOfPayments;

                if (newPPMT != null && newPPMT != 0)
                {
                    firstItem.PPMT = newPPMT;

                    if (firstItem.Opening < newPPMT)
                        firstItem.PPMT = firstItem.Opening;
                }

                firstItem.IPMT = CalculateSimpleInterestRate(empDeduction.AdvanceLoanAmount.Value, interestRate
                    , DateHelper.GetTotalDaysInTheMonth(firstItem.Year.Value,firstItem.Month,IsEnglish));// empDeduction.AdvanceLoanAmount * ((decimal)empDeduction.InterestRate / 100 / 12);

                CustomDate takenOn = CustomDate.GetCustomDateFromString(empDeduction.TakenOn,IsEnglish);
                CustomDate startFrom = CustomDate.GetCustomDateFromString(empDeduction.StartingFrom,IsEnglish);

                // For first month, if Taken On and Starting From lies in same month and Taken on is not the first day then calculate proprotinately 
                if (validPayrollPeriod != null && validPayrollPeriod.StartDateEng == empDeduction.StartingFromEng
                    && takenOn.Day != 1
                    && takenOn.Month == startFrom.Month
                    && takenOn.Year == startFrom.Year)
                {
                    //firstItem.IPMT = empDeduction.AdvanceLoanAmount.Value * ((decimal)empDeduction.InterestRate.Value / 100 / 12);

                    int totayDaysInStartingMonth = DateHelper.GetTotalDaysInTheMonth(takenOn.Year, takenOn.Month, IsEnglish);
                    int daysDiffBetTakenOnStartingFrom = totayDaysInStartingMonth - (empDeduction.TakenOnEng.Value - empDeduction.StartingFromEng.Value).Days;
                    firstItem.IPMT = empDeduction.AdvanceLoanAmount.Value * (decimal)(interestRate / 100.0) * (daysDiffBetTakenOnStartingFrom / (decimal)365.0);
                }
                    
                
                firstItem.Closing = firstItem.Opening - firstItem.PPMT;
                firstItem.EmployeeId = employeeId;
                firstItem.DeductionId = deductionId;
                firstItem.IsPayrollPeriodSaved = false;
                firstItem.DoNotAdjustLoan = false;


                if (lastNotUsedLoanAdjustment != null && startingYear == lastNotUsedLoanAdjustment.PayrollPeriod.Year &&
                    firstItem.Month == lastNotUsedLoanAdjustment.PayrollPeriod.Month)
                {
                    firstItem.AdjustmentPPMT = lastNotUsedLoanAdjustment.PPMTAdjustment;
                    firstItem.AdjustmentIPMT = lastNotUsedLoanAdjustment.IPMTAdjustment;
                    firstItem.AdjustedPPMT = lastNotUsedLoanAdjustment.AdjustedPPMT;
                    firstItem.AdjustedInterest = lastNotUsedLoanAdjustment.AdjustedInterestOnLoan;
                    firstItem.AdjustedClosing = lastNotUsedLoanAdjustment.AdjustedClosing;
                    firstItem.DoNotAdjustLoan = lastNotUsedLoanAdjustment.DoNotAdjustLoan;

                    firstItem.Addition = lastNotUsedLoanAdjustment.Addition;
                }
                else
                {
                    firstItem.AdjustedPPMT = firstItem.PPMT;
                    firstItem.AdjustedInterest = firstItem.IPMT;
                    firstItem.AdjustedClosing = firstItem.Closing;
                }


                firstItem.PayrollPeriodId = validPayrollPeriodId;
                if (validPayrollPeriodId != 0)
                {
                    // if current period is Magh and Loan start month is falgun then check the Month,Year for this case
                    PayrollPeriod period = CommonManager.GetPayrollPeriod(validPayrollPeriodId);
                    if (period != null && firstItem.Month == period.Month && firstItem.Year == period.Year)
                    {
                        firstItem.IsCurrentValidPayrollPeriod = 1;
                        hasCurrentPayrollPeriod = true;
                
                    }

                   

                }
                else
                    firstItem.IsCurrentValidPayrollPeriod = 0;

                installments.Add(firstItem);


                //set for remaining row calculation
                numberOfInstallments = 1;
                startingMonth = firstItem.Month;
                // startingYear = firstItem.Year.Value;
                opening = firstItem.Opening.Value;
                closing = firstItem.AdjustedClosing.Value;

            }
            else
            {
                numberOfInstallments = installments.Count;
                //modify to change rem installments & other
                int reminingInstallmentsChange = noOfPayments;

                for (int index = 0; index < installments.Count; index++)
                {
                    GetLoanAdjustmentInstallmentsResult loanInstallment = installments[index];

                    loanInstallment.RemainingInstallments = (reminingInstallmentsChange--);
                    //set initial ppmt,ipmt & closing as we haven't store these values directly
                    if (loanInstallment.AdjustmentPPMT != null)
                        loanInstallment.PPMT -= loanInstallment.AdjustmentPPMT;
                    else
                    {
                        loanInstallment.AdjustedPPMT = loanInstallment.PPMT;
                        loanInstallment.AdjustedClosing = loanInstallment.Closing;
                    }

                    if (loanInstallment.AdjustmentIPMT != null)
                        loanInstallment.IPMT -= loanInstallment.AdjustmentIPMT;
                    else
                        loanInstallment.AdjustedInterest = loanInstallment.IPMT;

                    //from second row set opening as the closing of previos row
                    if (index != 0)
                        loanInstallment.Opening = installments[index - 1].AdjustedClosing;
                    loanInstallment.Closing = loanInstallment.Opening - loanInstallment.PPMT;
                }


                startingMonth = installments[installments.Count - 1].Month;
                startingYear = installments[installments.Count - 1].Year.Value;
                opening = installments[installments.Count - 1].Opening.Value;
                closing = installments[installments.Count - 1].AdjustedClosing.Value;

            }


            //now prepare to add the remaining months
            int remainingInstallments = noOfPayments - numberOfInstallments;

            //if (noOfPayments < 2000)
            {

                for (int i = numberOfInstallments; i < noOfPayments; i++)
                {
                    //increment month for future
                    startingMonth += 1;
                    if (startingMonth > 12)
                    {
                        startingMonth = startingMonth % 12;
                        startingYear += 1;
                    }

                    opening = closing;


                    GetLoanAdjustmentInstallmentsResult item = new GetLoanAdjustmentInstallmentsResult();
                    item.RemainingInstallments = remainingInstallments;

                    item.Month = startingMonth;
                    item.Year = startingYear;
                    item.Opening = opening;

                    if (remainingInstallments == 0)
                        continue;

                    item.PPMT = item.Opening / (decimal)remainingInstallments;

                    if (newPPMT != null && newPPMT != 0)
                    {
                        item.PPMT = newPPMT;

                        if (item.Opening < newPPMT)
                            item.PPMT = item.Opening;


                    }


                    #region "Prevent rem ppmt last than 2 rs"
                    // For remaining PPMT is less than 2 Rs then add that in last PPMT
                    if (item.Opening - item.PPMT <= 2)
                    {
                        item.PPMT = item.Opening;
                    }
                    #endregion

                    item.IPMT = CalculateSimpleInterestRate(item.Opening.Value, interestRate
                            , DateHelper.GetTotalDaysInTheMonth(item.Year.Value, item.Month, IsEnglish));// item.Opening * ((decimal)empDeduction.InterestRate / 100 / 12);
                    item.Closing = item.Opening - item.PPMT;
                    item.EmployeeId = employeeId;
                    item.DeductionId = deductionId;
                    item.IsPayrollPeriodSaved = false;
                    item.DoNotAdjustLoan = false;

                    if (notUsedInCalculation == false && lastNotUsedLoanAdjustment != null &&
                        //loanYear == lastNotUsedLoanAdjustment.PayrollPeriod.Year &&
                          lastNotUsedLoanAdjustment.PayrollPeriod.Year == item.Year && item.Month == lastNotUsedLoanAdjustment.PayrollPeriod.Month)
                    {
                        item.AdjustmentPPMT = lastNotUsedLoanAdjustment.PPMTAdjustment;
                        item.AdjustmentIPMT = lastNotUsedLoanAdjustment.IPMTAdjustment;
                        item.AdjustedPPMT = lastNotUsedLoanAdjustment.AdjustedPPMT;
                        item.AdjustedInterest = lastNotUsedLoanAdjustment.AdjustedInterestOnLoan;
                        item.AdjustedClosing = lastNotUsedLoanAdjustment.AdjustedClosing;
                        item.DoNotAdjustLoan = lastNotUsedLoanAdjustment.DoNotAdjustLoan;

                        item.Addition = lastNotUsedLoanAdjustment.Addition;
                    }
                    else
                    {
                        //adjustment assignment
                        item.AdjustedPPMT = item.PPMT;
                        item.AdjustedInterest = item.IPMT;
                        item.AdjustedClosing = item.Closing;
                    }

                    //for first row only
                    if (notUsedInCalculation == false && validPayrollPeriod != null
                        && validPayrollPeriod.Year == item.Year && item.Month == validPayrollPeriod.Month)
                    {
                        item.PayrollPeriodId = validPayrollPeriodId;
                        if (validPayrollPeriodId != 0)
                        {
                            item.IsCurrentValidPayrollPeriod = 1;
                            hasCurrentPayrollPeriod = true;
                        }
                        else
                            item.IsCurrentValidPayrollPeriod = 0;

                    }
                    else
                    {
                        item.PayrollPeriodId = 0;
                        item.IsCurrentValidPayrollPeriod = 0;
                    }

                    closing = item.AdjustedClosing.Value;

                    installments.Add(item);

                    remainingInstallments -= 1;


                    /// if has large periods
                    if(hasCurrentPayrollPeriod && noOfPayments > 2000)
                        break;
                }
            }

            // if last installment last column Closing has value 0.1 - 0.9 then add in 
            //if (installments.Count > 0 && installments[installments.Count - 1].Closing != null &&
            //    installments[installments.Count - 1].Closing.Value <= (decimal)5)
            //{
            //    GetLoanAdjustmentInstallmentsResult lastInst = installments[installments.Count - 1];
            //    if (lastInst.AdjustmentPPMT == null && lastInst.AdjustmentIPMT == null)
            //    {
            //        lastInst.PPMT = lastInst.Opening;

            //        lastInst.IPMT = CalculateSimpleInterestRate(lastInst.Opening.Value, interestRate
            //                , DateHelper.GetTotalDaysInTheMonth(lastInst.Year.Value, lastInst.Month, IsEnglish));// item.Opening * ((decimal)empDeduction.InterestRate / 100 / 12);
            //        lastInst.Closing = lastInst.Opening - lastInst.PPMT;


            //    }

            //}

            //if (installments.Count > 0 && installments[installments.Count - 1].AdjustedClosing != null &&
            //    installments[installments.Count - 1].AdjustedClosing.Value >= (decimal)0 && installments[installments.Count - 1].AdjustedClosing.Value <= (decimal)0.9)
            //{
            //    installments[installments.Count - 1].AdjustedClosing = 0;
            //}

            return installments;
        }

        /// <summary>
        /// Ref "EMI/Loan Adjustment.xlsx", can be used for Simple loan only
        /// </summary>
        /// <param name="deductionId"></param>
        /// <param name="employeeId"></param>
        /// <returns></returns>
        public static List<GetLoanAdjustmentInstallmentsResult> GetEMIAdjustments(int deductionId, int employeeId, 
            ref bool hasCurrentPayrollPeriod, ref bool isInstallmentComplete)
        {
            List<GetLoanAdjustmentInstallmentsResult> installments
                = PayrollDataContext.GetLoanAdjustmentInstallments(employeeId, deductionId).ToList();

          

            PEmployeeDeduction empDeduction = GetEmployeeDeduction(employeeId, deductionId);
            PayrollPeriod period = CommonManager.GetLastPayrollPeriod();

            //get last loan adjustment that is not being used in calculation so that the adjustment calculation can be done using this object value
            LoanAdjustment lastNotUsedLoanAdjustment = GetLastNotUsedInCalculationLoanAdjustment(employeeId, deductionId);
            

            int validPayrollPeriodId = 0;
            PayrollPeriod validPayrollPeriod = CommonManager.GetValidLastPayrollPeriod();
            if (validPayrollPeriod != null)
            {
                if (CalculationManager.IsCalculationSavedForEmployee(validPayrollPeriod.PayrollPeriodId, employeeId))
                    validPayrollPeriod = null;
                else
                    validPayrollPeriodId = validPayrollPeriod.PayrollPeriodId;
            }


            int numberOfInstallments = 0;
            int startingMonth = 0, startingYear;
            decimal opening, closing;
            bool notUsedInCalculation = installments.Count == 0;
            //  int loanYear;

            //if not used in calculation then generate first row
            if (installments.Count <= 0)
            {

                GetLoanAdjustmentInstallmentsResult firstItem = new GetLoanAdjustmentInstallmentsResult();
                firstItem.RemainingInstallments = empDeduction.ToBePaidOver;
                firstItem.Month = CustomDate.GetCustomDateFromString(empDeduction.StartingFrom, IsEnglish).Month;
                firstItem.Year = CustomDate.GetCustomDateFromString(empDeduction.StartingFrom, IsEnglish).Year;
                startingYear = firstItem.Year.Value;
                firstItem.Opening = empDeduction.AdvanceLoanAmount;
                firstItem.EMI = empDeduction.InstallmentAmount.Value;
                firstItem.PPMT = PayrollDataContext.GetPPMTForAdjustment(empDeduction.EmployeeId, empDeduction.DeductionId,
                    empDeduction.RemainingInstallments, empDeduction.NoOfInstallments - empDeduction.RemainingInstallments).Value;
                firstItem.IPMT = firstItem.EMI - firstItem.PPMT;
             
                //firstItem.Closing = firstItem.Opening - firstItem.PPMT;
                //firstItem.ClosingBalance = firstItem.Opening.Value +
                //    ((empDeduction.InstallmentAmount.Value * empDeduction.NoOfInstallments.Value) - empDeduction.AdvanceLoanAmount.Value)
                //    - empDeduction.InstallmentAmount.Value;
               
                firstItem.EmployeeId = employeeId;
                firstItem.DeductionId = deductionId;
                firstItem.IsPayrollPeriodSaved = false;
                firstItem.DoNotAdjustLoan = false;


                if (lastNotUsedLoanAdjustment != null && startingYear == lastNotUsedLoanAdjustment.PayrollPeriod.Year &&
                    firstItem.Month == lastNotUsedLoanAdjustment.PayrollPeriod.Month)
                {
                    //firstItem.AdjustmentPPMT = lastNotUsedLoanAdjustment.PPMTAdjustment;
                    //firstItem.AdjustmentIPMT = lastNotUsedLoanAdjustment.IPMTAdjustment;
                    firstItem.InstallmentMonths = lastNotUsedLoanAdjustment.InstallmentMonths;

                    firstItem.AdjustedPPMT = lastNotUsedLoanAdjustment.PPMTAdjustment;
                    firstItem.AdjustedInterest = lastNotUsedLoanAdjustment.IPMTAdjustment;
                    //firstItem.Closing = lastNotUsedLoanAdjustment.AdjustedClosing;
                    firstItem.PPMT = lastNotUsedLoanAdjustment.PPMTAdjustment;
                    firstItem.IPMT = lastNotUsedLoanAdjustment.IPMTAdjustment;

                    if (lastNotUsedLoanAdjustment.InstallmentMonths == null)
                        firstItem.InstallmentMonths = 1;
                    else
                        firstItem.InstallmentMonths = lastNotUsedLoanAdjustment.InstallmentMonths.Value;

                    numberOfInstallments = firstItem.InstallmentMonths.Value;

                    
                   // firstItem.ClosingBalance = firstItem.Opening.Value +
                   //((empDeduction.InstallmentAmount.Value * empDeduction.NoOfInstallments.Value) - empDeduction.AdvanceLoanAmount.Value)
                   //- (empDeduction.InstallmentAmount.Value * numberOfInstallments);

                    //firstItem.AdjustedInterest = lastNotUsedLoanAdjustment.AdjustedInterestOnLoan;
                    //firstItem.AdjustedClosing = lastNotUsedLoanAdjustment.AdjustedClosing;
                    //firstItem.DoNotAdjustLoan = lastNotUsedLoanAdjustment.DoNotAdjustLoan;
                }
                else
                {
                    firstItem.AdjustedPPMT = firstItem.PPMT;
                    firstItem.AdjustedInterest = firstItem.IPMT;
                    //firstItem.AdjustedClosing = firstItem.Closing;
                    numberOfInstallments = 1;
                }


                firstItem.PayrollPeriodId = validPayrollPeriodId;
                if (validPayrollPeriodId != 0)
                {
                    firstItem.IsCurrentValidPayrollPeriod = 1;
                    hasCurrentPayrollPeriod = true;
                }
                else
                    firstItem.IsCurrentValidPayrollPeriod = 0;

                installments.Add(firstItem);


                //set for remaining row calculation
                
                startingMonth = firstItem.Month;
                // startingYear = firstItem.Year.Value;
                //opening = firstItem.Opening.Value;
                //closing = firstItem.Closing.Value;

            }
            else
            {
                //numberOfInstallments = installments.Count;

                numberOfInstallments = 0;
                foreach (GetLoanAdjustmentInstallmentsResult item in installments)
                {
                    if (item.InstallmentMonths == null)
                        numberOfInstallments += 1;
                    else
                        numberOfInstallments += item.InstallmentMonths.Value;
                }

                //modify to change rem installments & other
                int reminingInstallmentsChange = empDeduction.ToBePaidOver.Value - numberOfInstallments;

                for (int index = 0; index < installments.Count; index++)
                {
                    GetLoanAdjustmentInstallmentsResult loanInstallment = installments[index];

                    loanInstallment.EMI = empDeduction.InstallmentAmount.Value;

                    loanInstallment.RemainingInstallments = (reminingInstallmentsChange--);

                    //if (loanInstallment.Closing == null)
                    //    loanInstallment.Closing = 0;

                   

                    //loanInstallment.AdjustedPPMT = lastNotUsedLoanAdjustment.PPMTAdjustment;
                    //loanInstallment.AdjustedInterest = lastNotUsedLoanAdjustment.IPMTAdjustment;
                    //loanInstallment.Closing = lastNotUsedLoanAdjustment.AdjustedClosing;
                    //loanInstallment.PPMT = lastNotUsedLoanAdjustment.PPMTAdjustment;
                    //loanInstallment.IPMT = lastNotUsedLoanAdjustment.IPMTAdjustment;

                    //set initial ppmt,ipmt & closing as we haven't store these values directly
                    //if (loanInstallment.AdjustmentPPMT != null)
                    //    loanInstallment.PPMT -= loanInstallment.AdjustmentPPMT;
                    //else
                    //{
                    //    loanInstallment.AdjustedPPMT = loanInstallment.PPMT;
                    //    loanInstallment.AdjustedClosing = loanInstallment.Closing;
                    //}

                    //if (loanInstallment.AdjustmentIPMT != null)
                    //    loanInstallment.IPMT -= loanInstallment.AdjustmentIPMT;
                    //else
                    //    loanInstallment.AdjustedInterest = loanInstallment.IPMT;

                    //from second row set opening as the closing of previos row
                    //if (index != 0)
                    //    loanInstallment.Opening = installments[index - 1].AdjustedClosing;
                    //loanInstallment.Closing = loanInstallment.Opening - loanInstallment.PPMT;
                }


                startingMonth = installments[installments.Count - 1].Month;
                startingYear = installments[installments.Count - 1].Year.Value;
                //opening = installments[installments.Count - 1].Opening.Value;
                //closing = installments[installments.Count - 1].Closing == null ? 0 : installments[installments.Count - 1].Closing.Value;

            }


            //now prepare to add the remaining months
            int remainingInstallments = empDeduction.NoOfInstallments.Value - numberOfInstallments;
            //int i = numberOfInstallments - 1;

            int i = numberOfInstallments; //(lastNotUsedLoanAdjustment.InstallmentMonths

            //if (lastNotUsedLoanAdjustment != null && lastNotUsedLoanAdjustment.InstallmentMonths != null)
            //    i += lastNotUsedLoanAdjustment.InstallmentMonths.Value;
            //else
            //    i += 1;

            for (; i < empDeduction.ToBePaidOver; )
            {
                //increment month for future
                startingMonth += 1;
                if (startingMonth > 12)
                {
                    startingMonth = startingMonth % 12;
                    startingYear += 1;
                }

                //opening = closing;


                GetLoanAdjustmentInstallmentsResult item = new GetLoanAdjustmentInstallmentsResult();
                item.RemainingInstallments = remainingInstallments;

                item.Month = startingMonth;
                item.Year = startingYear;
                //item.Opening = opening;

                item.EMI = empDeduction.InstallmentAmount.Value;
                item.PPMT = PayrollDataContext.GetPPMTForAdjustment(empDeduction.EmployeeId, empDeduction.DeductionId,
                    remainingInstallments, empDeduction.NoOfInstallments - remainingInstallments).Value;
                item.IPMT = item.EMI - item.PPMT;
                //item.PPMT = item.Opening / remainingInstallments;
                //item.IPMT = CalculateSimpleInterestRate(item.Opening.Value, empDeduction.InterestRate.Value);// item.Opening * ((decimal)empDeduction.InterestRate / 100 / 12);
                //item.Closing = item.Opening - item.PPMT;
                item.EmployeeId = employeeId;
                item.DeductionId = deductionId;
                item.IsPayrollPeriodSaved = false;
                item.DoNotAdjustLoan = false;

                if (notUsedInCalculation == false && lastNotUsedLoanAdjustment != null &&
                    //loanYear == lastNotUsedLoanAdjustment.PayrollPeriod.Year &&
                       lastNotUsedLoanAdjustment.PayrollPeriod.Year == item.Year 
                       && item.Month == lastNotUsedLoanAdjustment.PayrollPeriod.Month)
                {
                    //firstItem.AdjustmentPPMT = lastNotUsedLoanAdjustment.PPMTAdjustment;
                    //firstItem.AdjustmentIPMT = lastNotUsedLoanAdjustment.IPMTAdjustment;
                    item.InstallmentMonths = lastNotUsedLoanAdjustment.InstallmentMonths;

                    item.AdjustedPPMT = lastNotUsedLoanAdjustment.PPMTAdjustment;
                    item.AdjustedInterest = lastNotUsedLoanAdjustment.IPMTAdjustment;
                    //item.Closing = lastNotUsedLoanAdjustment.AdjustedClosing;
                    item.PPMT = lastNotUsedLoanAdjustment.PPMTAdjustment;
                    item.IPMT = lastNotUsedLoanAdjustment.IPMTAdjustment;

                    //if(item.InstallmentMonths ==numberOfInstallments)
                    //    item.InstallmentMonths =1;

                    //i += (item.InstallmentMonths - 1);

                }
                else
                    item.InstallmentMonths = 1;
               

                //for first row only
                if (notUsedInCalculation == false && validPayrollPeriod != null
                    && validPayrollPeriod.Year == item.Year && item.Month == validPayrollPeriod.Month)
                {
                    item.PayrollPeriodId = validPayrollPeriodId;
                    if (validPayrollPeriodId != 0)
                    {
                        item.IsCurrentValidPayrollPeriod = 1;
                        hasCurrentPayrollPeriod = true;
                    }
                    else
                        item.IsCurrentValidPayrollPeriod = 0;

                }
                else
                {
                    item.PayrollPeriodId = 0;
                    item.IsCurrentValidPayrollPeriod = 0;
                }

                //closing = item.Closing.Value;

                installments.Add(item);

                i += item.InstallmentMonths.Value;
                remainingInstallments -= (item.InstallmentMonths == null ? 1 : item.InstallmentMonths.Value);
            }

           
            //calculate Closing,Opening & Closing Balance
            decimal openingPrinciple = empDeduction.AdvanceLoanAmount.Value;
            for (int j = 0; j < installments.Count; j++)
            {
                installments[j].Opening = openingPrinciple;
                installments[j].Closing = installments[j].Opening - installments[j].PPMT;

                if (installments[j].Closing.Value < (decimal)0.9)
                    installments[j].Closing = 0;


                if (openingPrinciple == empDeduction.AdvanceLoanAmount.Value)
                {
                    if (installments[j].InstallmentMonths == null)
                        installments[j].InstallmentMonths = 1;

                    installments[j].ClosingBalance = installments[j].Opening.Value +

                       ((empDeduction.InstallmentAmount.Value * empDeduction.NoOfInstallments.Value) - empDeduction.AdvanceLoanAmount.Value)
                       - (empDeduction.InstallmentAmount.Value * installments[j].InstallmentMonths.Value);
                }

                if (installments[j].InstallmentMonths != 0)
                {
                    openingPrinciple = installments[j].Closing.Value;
                }
            }

            return installments;
        }

        public static decimal? GetAdvanceInstallmentAmount(int deductionId,int employeeId)
        {
            int periodid = 0;
            PayrollPeriod period = CommonManager.GetLastPayrollPeriod();
            if (period != null)
                periodid = period.PayrollPeriodId;

            return PayrollDataContext.GetAdvanceCurrentInstallmentAmount(deductionId, employeeId, periodid);

        }


        /// <summary>
        /// Ref "EMI/Loan Adjustment.xlsx", can be used for Simple loan only
        /// </summary>
        /// <param name="deductionId"></param>
        /// <param name="employeeId"></param>
        /// <returns></returns>
        public static List<GetAdvanceAdjustmentInstallmentsResult> GetAdvanceAdjustments(int deductionId, int employeeId, ref bool hasCurrentPayrollPeriod)
        {
            List<GetAdvanceAdjustmentInstallmentsResult> installments
                = PayrollDataContext.GetAdvanceAdjustmentInstallments(employeeId, deductionId).ToList();


            bool firstSkipMonthNotUsedInCalculation = false;


            PEmployeeDeduction empDeduction = GetEmployeeDeduction(employeeId, deductionId);

            // Installment Complete 
            //if (installments.Count == empDeduction.ToBePaidOver.Value)
            //    return installments;

            //get last loan adjustment that is not being used in calculation so that the adjustment calculation can be done using this object value
            AdvanceAdjustment lastNotUsedLoanAdjustment = GetLastNotUsedInCalculationAdvanceAdjustment(employeeId, deductionId);


            int validPayrollPeriodId = 0;
            PayrollPeriod validPayrollPeriod = CommonManager.GetLastPayrollPeriod();
            if (validPayrollPeriod != null)
            {
                if (CalculationManager.IsCalculationSavedForEmployee(validPayrollPeriod.PayrollPeriodId, employeeId))
                    validPayrollPeriod = null;
                else
                    validPayrollPeriodId = validPayrollPeriod.PayrollPeriodId;
            }


            int numberOfInstallments = 0;
            int startingMonth = 0, startingYear;
            decimal opening, closing, installment;
            bool notUsedInCalculation = installments.Count == 0;
            bool isCurrentMonthAlreadyFound = false; // to check if current payroll month already found & skip the same month in other year
            int skipMonths = 0;

            //if not used in calculation then generate first row
            if (installments.Count <= 0)
            {
                bool skipThisMonth = false;
                if (lastNotUsedLoanAdjustment != null && lastNotUsedLoanAdjustment.IsSkipThisMonth != null && lastNotUsedLoanAdjustment.IsSkipThisMonth.Value)
                {
                    firstSkipMonthNotUsedInCalculation = true;
                    skipThisMonth = true;
                }
                else
                    skipThisMonth = false;

                GetAdvanceAdjustmentInstallmentsResult firstItem = new GetAdvanceAdjustmentInstallmentsResult();
                firstItem.RemainingInstallments = empDeduction.ToBePaidOver;
                firstItem.Month = CustomDate.GetCustomDateFromString(empDeduction.StartingFrom, IsEnglish).Month;
                firstItem.Year = CustomDate.GetCustomDateFromString(empDeduction.StartingFrom, IsEnglish).Year;
                startingYear = firstItem.Year.Value;
                firstItem.Opening = empDeduction.AdvanceLoanAmount;
                firstItem.InstallmentAmount = empDeduction.InstallmentAmount.Value; //empDeduction.AdvanceLoanAmount / empDeduction.ToBePaidOver;
                //firstItem.IPMT = CalculateSimpleInterestRate(empDeduction.AdvanceLoanAmount.Value, empDeduction.InterestRate.Value);// empDeduction.AdvanceLoanAmount * ((decimal)empDeduction.InterestRate / 100 / 12);
                firstItem.Closing = firstItem.Opening - firstItem.InstallmentAmount;
                firstItem.EmployeeId = employeeId;
                firstItem.DeductionId = deductionId;
                firstItem.IsPayrollPeriodSaved = false;
                firstItem.IsSkipThisMonth = skipThisMonth;

                if (lastNotUsedLoanAdjustment != null && startingYear == lastNotUsedLoanAdjustment.PayrollPeriod.Year &&
                    firstItem.Month == lastNotUsedLoanAdjustment.PayrollPeriod.Month)
                {
                    firstItem.Adjustment = lastNotUsedLoanAdjustment.Adjustment;

                    firstItem.Adjusted = lastNotUsedLoanAdjustment.Adjusted;

                    firstItem.AdjustedClosing = lastNotUsedLoanAdjustment.AdjustedClosing;

                }
                else
                {
                    firstItem.Adjusted = firstItem.InstallmentAmount;
                    //firstItem.AdjustedInterest = firstItem.IPMT;
                    firstItem.AdjustedClosing = firstItem.Closing;
                }


                firstItem.PayrollPeriodId = validPayrollPeriodId;
                //if installment is not greater than current payroll period then only valid
                if (validPayrollPeriodId != 0 )//&& (firstItem.Month==startingMonth && firstItem.Year==startingYear)  )
                {
                    firstItem.IsCurrentValidPayrollPeriod = 1;
                    hasCurrentPayrollPeriod = true;
                    isCurrentMonthAlreadyFound = true;
                }
                else
                    firstItem.IsCurrentValidPayrollPeriod = 0;

                installments.Add(firstItem);


                //set for remaining row calculation
                numberOfInstallments = 1;

                if (skipThisMonth)
                    numberOfInstallments = 0;

                startingMonth = firstItem.Month;
                // startingYear = firstItem.Year.Value;
                opening = firstItem.Opening.Value;
                closing = firstItem.AdjustedClosing.Value;

            }
            else
            {
                numberOfInstallments = installments.Count;
                //modify to change rem installments & other
                int reminingInstallmentsChange = empDeduction.ToBePaidOver.Value;

                for (int index = 0; index < installments.Count; index++)
                {
                    GetAdvanceAdjustmentInstallmentsResult loanInstallment = installments[index];

                    loanInstallment.RemainingInstallments = (reminingInstallmentsChange--);
                    //set initial ppmt,ipmt & closing as we haven't store these values directly
                    if (loanInstallment.Adjustment != null)
                        loanInstallment.InstallmentAmount -= loanInstallment.Adjustment.Value;
                   

                    //if (loanInstallment.Adjustment != null)
                    //    loanInstallment.IPMT -= loanInstallment.AdjustmentIPMT;
                    //else
                    //    loanInstallment.AdjustedInterest = loanInstallment.IPMT;

                    //from second row set opening as the closing of previos row
                    if (index != 0)
                        loanInstallment.Opening = installments[index - 1].AdjustedClosing;
                    loanInstallment.Closing = loanInstallment.Opening - loanInstallment.InstallmentAmount;

                    if (loanInstallment.Adjustment == null)
                    {
                        //  else
                        {
                            loanInstallment.Adjusted = loanInstallment.InstallmentAmount;
                            loanInstallment.AdjustedClosing = loanInstallment.Closing;
                        }
                    }


                    if (loanInstallment.IsSkipThisMonth != null && loanInstallment.IsSkipThisMonth.Value)
                    {
                        skipMonths += 1;
                        reminingInstallmentsChange++;
                    }
                }


                
               

                startingMonth = installments[installments.Count - 1].Month;
                startingYear = installments[installments.Count - 1].Year.Value;
                opening = installments[installments.Count - 1].Opening.HasValue
                              ? installments[installments.Count - 1].Opening.Value
                              : 0;
                closing = installments[installments.Count - 1].AdjustedClosing.HasValue
                              ? installments[installments.Count - 1].AdjustedClosing.Value
                              : 0;

            }

            skipMonths = 0;
            foreach (GetAdvanceAdjustmentInstallmentsResult item in installments)
            {
                if (item.IsSkipThisMonth != null && item.IsSkipThisMonth.Value)
                    skipMonths += 1;
            }


            //now prepare to add the remaining months
            int remainingInstallments = empDeduction.NoOfInstallments.Value - numberOfInstallments;
            
            remainingInstallments += skipMonths;

            //if true then subtract for first one
            if (firstSkipMonthNotUsedInCalculation)
                remainingInstallments -= 1;

            int total = empDeduction.ToBePaidOver.Value + skipMonths;

            if (firstSkipMonthNotUsedInCalculation)
                total -= 1;

            for (int i = numberOfInstallments; i < total; i++)
            {

                bool skipThisMonth = false; 
              

                //increment month for future
                startingMonth += 1;
                if (startingMonth > 12)
                {
                    startingMonth = startingMonth % 12;
                    startingYear += 1;
                }

                opening = closing;


                GetAdvanceAdjustmentInstallmentsResult item = new GetAdvanceAdjustmentInstallmentsResult();
                item.RemainingInstallments = remainingInstallments;

                item.Month = startingMonth;
                item.Year = startingYear;
                item.Opening = opening;
                if (remainingInstallments != 0)
                    item.InstallmentAmount = item.Opening.Value / remainingInstallments;
                else
                    break;

                //item.IPMT = CalculateSimpleInterestRate(item.Opening.Value, empDeduction.InterestRate.Value);// item.Opening * ((decimal)empDeduction.InterestRate / 100 / 12);
                item.Closing = item.Opening - item.InstallmentAmount;
                item.EmployeeId = employeeId;
                item.DeductionId = deductionId;
                item.IsPayrollPeriodSaved = false;
             
                //item.DoNotAdjustLoan = false;

                if (isCurrentMonthAlreadyFound==false &&  notUsedInCalculation == false && lastNotUsedLoanAdjustment != null &&
                    //loanYear == lastNotUsedLoanAdjustment.PayrollPeriod.Year &&
                      startingYear == item.Year && item.Month == lastNotUsedLoanAdjustment.PayrollPeriod.Month)
                {
                    item.Adjustment = lastNotUsedLoanAdjustment.Adjustment;
                    // item.AdjustmentIPMT = lastNotUsedLoanAdjustment.IPMTAdjustment;
                    item.Adjusted = lastNotUsedLoanAdjustment.Adjusted;
                    // item.AdjustedInterest = lastNotUsedLoanAdjustment.AdjustedInterestOnLoan;
                    item.AdjustedClosing = lastNotUsedLoanAdjustment.AdjustedClosing;
                    //  item.DoNotAdjustLoan = lastNotUsedLoanAdjustment.DoNotAdjustLoan;


                    if (lastNotUsedLoanAdjustment != null && lastNotUsedLoanAdjustment.IsSkipThisMonth != null && lastNotUsedLoanAdjustment.IsSkipThisMonth.Value)
                    {

                        skipThisMonth = true;
                        total += 1;
                        skipMonths += 1;
                        item.IsSkipThisMonth = skipThisMonth;
                    }
                    else
                        skipThisMonth = false;
                }
                else
                {
                    //adjustment assignment
                    item.Adjusted = item.InstallmentAmount;
                    //item.AdjustedInterest = item.IPMT;
                    item.AdjustedClosing = item.Closing;
                }



                //for first row only
                //first check if curret month already found, then only further process to remove year conflict
                if (isCurrentMonthAlreadyFound==false &&  notUsedInCalculation == false && validPayrollPeriod != null && startingYear == item.Year && item.Month == validPayrollPeriod.Month)
                {
                    item.PayrollPeriodId = validPayrollPeriodId;
                    if (validPayrollPeriodId != 0)
                    {
                        item.IsCurrentValidPayrollPeriod = 1;
                        hasCurrentPayrollPeriod = true;
                        isCurrentMonthAlreadyFound = true;
                    }
                    else
                        item.IsCurrentValidPayrollPeriod = 0;

                }
                else
                {
                    item.PayrollPeriodId = 0;
                    item.IsCurrentValidPayrollPeriod = 0;
                }

                closing = item.AdjustedClosing.Value;

                installments.Add(item);

                if (skipThisMonth == false)
                    remainingInstallments -= 1;
              //  else
                //    remainingInstallments -= 1;
            }

			   //if installment is in future then disable all 
            if (installments.Count > 0 && validPayrollPeriod != null)
            {
                if (installments[0].Year == validPayrollPeriod.Year && installments[0].Month > validPayrollPeriod.Month)               
                    installments[0].IsCurrentValidPayrollPeriod = 0;                
                else if( installments[0].Year   > validPayrollPeriod.Year)
                    installments[0].IsCurrentValidPayrollPeriod = 0;
            }
            return installments;
        }


        public static Status DeleteCurrentMonthSimpleInterestLoan(int empId, int dedId, int periodId)
        {
            Status status = new Status();


            if (CalculationManager.IsCalculationSavedForEmployee(periodId, empId))
            {
                status.ErrorMessage = "Salary already saved for the month " +
                    CommonManager.GetPayrollPeriod(periodId).Name + ", adjustment can not be changed.";
                return status;
            }


            LoanAdjustment dbAdj = PayrollDataContext.LoanAdjustments.FirstOrDefault(
                x => x.EmployeeId == empId && x.DeductionId == dedId && x.PayrollPeriodId == periodId);

            if (dbAdj != null)
            {
                PEmployeeDeduction empDed = GetEmployeeDeduction(empId, dedId);

                // as for other adjustment also later interest rate could be changed
                if (dbAdj.InterestPrev != null && dbAdj.InterestPrev != 0)
                {
                    empDed.InterestRate = dbAdj.InterestPrev;
                    empDed.MarketRate = dbAdj.MarketRatePrev;
                }

                if (dbAdj.AdjustmentType != null && dbAdj.AdjustmentType == (int)LoanAdjustmentType.AdjustInterest)
                { }
                else if (dbAdj.AdjustmentType != null && dbAdj.AdjustmentType == (int)LoanAdjustmentType.InterestRateChange)
                {
                   
                }
                else
                {
                    empDed.SINewPPMTAmount = dbAdj.SINewPPMTAmountOrNewPPMTPrev;
                    empDed.ToBePaidOver = dbAdj.ToBePaidPrev;
                }

                PayrollDataContext.LoanAdjustments.DeleteOnSubmit(dbAdj);

                PayrollDataContext.SubmitChanges();

                return status;
            }

            return status;
            
        }

        public static string GetLoanAdjustmentLog(LoanAdjustment loan)
        {
            return "PPMT:" + GetCurrency(loan.PPMTAdjustment) + ",IPMT:" + GetCurrency(loan.IPMTAdjustment) + "," +
                   GetString(loan.DoNotAdjustLoan);
        }

        public static string GetAdvanceAdjustmentLog(AdvanceAdjustment adv)
        {
            return GetCurrency(adv.Adjustment) +","+ GetString(adv.IsSkipThisMonth);
        }

        /// <summary>
        /// Save/Update Simple Load Adjustment
        /// </summary>
        /// <param name="loan"></param>
        /// <param name="interestRate"></param>
        /// <param name="opening"></param>
        /// <param name="ppmt"></param>
        /// <param name="ipmt"></param>
        public static Status SaveUpdate(LoanAdjustment loan, double interestRate, decimal opening, decimal ppmt, decimal ipmt,string title
            , decimal newPPMT, int noOfPayments)
        {

            Status status = new Status();

            if (CalculationManager.IsCalculationSavedForEmployee(loan.PayrollPeriodId, loan.EmployeeId))
            {
                status.ErrorMessage = "Salary already saved for the month " +
                    CommonManager.GetPayrollPeriod(loan.PayrollPeriodId).Name + ", adjustment can not be changed.";
                return status;
            }

            PEmployeeDeduction empDed = PayManager.GetEmployeeDeduction(loan.EmployeeId, loan.DeductionId);

            empDed.InterestRate = interestRate;

            string month = "";
            if (loan.PayrollPeriodId != 0)
            {
                month = CommonManager.GetPayrollPeriod(loan.PayrollPeriodId).Name;
            }

            if (empDed.ToBePaidOver != noOfPayments)
            {
                PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(loan.EmployeeId, (byte)MonetoryChangeLogTypeEnum.Loan,
                  title + " : No of Payments : " + month, empDed.ToBePaidOver,
                  noOfPayments, LogActionEnum.Update));

                empDed.ToBePaidOver = noOfPayments;
                empDed.NoOfInstallments = noOfPayments;

            }

            
            
            if (empDed.SINewPPMTAmount != newPPMT)
            {
                PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(loan.EmployeeId, (byte)MonetoryChangeLogTypeEnum.Loan,
                  title + " : New PPMT Adjustment : " + month, empDed.SINewPPMTAmount,
                  newPPMT, LogActionEnum.Update));
            }

            empDed.SINewPPMTAmount = newPPMT;

            LoanAdjustment dbEntity = PayrollDataContext.LoanAdjustments.Where
                (l => l.EmployeeId == loan.EmployeeId && l.PayrollPeriodId == loan.PayrollPeriodId && l.DeductionId == loan.DeductionId)
                .SingleOrDefault();


            //set adjusted closing
            if (loan.PPMTAdjustment != null)
            {

                //if do not adjust loan not checked, then normal case to use ppmt adjustment
                if (!loan.DoNotAdjustLoan.Value)
                    loan.AdjustedClosing = opening - ppmt - loan.PPMTAdjustment + Convert.ToDecimal(loan.Addition);
                else
                    loan.AdjustedClosing = opening - ppmt + Convert.ToDecimal(loan.Addition);

                loan.AdjustedPPMT = loan.PPMTAdjustment + ppmt;
            }
            else
            {
                loan.AdjustedClosing = opening - ppmt + Convert.ToDecimal(loan.Addition);
                loan.AdjustedPPMT = ppmt;
            }

            if (loan.IPMTAdjustment != null)
            {
                loan.AdjustedInterestOnLoan = loan.IPMTAdjustment + ipmt;
            }
            else
            {
                loan.AdjustedInterestOnLoan = ipmt;
            }

            loan.UniqueId = empDed.UniqueId;

          
            // final processing
            if (loan.AdjustmentType == (int)LoanAdjustmentType.FullSettlement)
            {
                //as for full settlement, this is the final installment, so there is not installment after this
                loan.AdjustedClosing = 0;
            }


            if (dbEntity == null)
            {
                PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(loan.EmployeeId, (byte)MonetoryChangeLogTypeEnum.Loan,
                   title + " : Loan Adjustment : " + month, "-",
                   GetLoanAdjustmentLog(loan), LogActionEnum.Add));

                loan.CreatedBy = SessionManager.User.UserID;
                loan.CreatedOn = GetCurrentDateAndTime();

                PayrollDataContext.LoanAdjustments.InsertOnSubmit(loan);
                SaveChangeSet();
            }
            else
            {
                if(dbEntity.PPMTAdjustment != loan.PPMTAdjustment || dbEntity.IPMTAdjustment != loan.IPMTAdjustment 
                    || dbEntity.DoNotAdjustLoan != loan.DoNotAdjustLoan)
                    PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(loan.EmployeeId, (byte)MonetoryChangeLogTypeEnum.Loan,
                       title + " : Loan Adjustment", GetLoanAdjustmentLog(dbEntity), GetLoanAdjustmentLog(loan), LogActionEnum.Update));

                dbEntity.AdjustedClosing = loan.AdjustedClosing;
                dbEntity.IPMTAdjustment = loan.IPMTAdjustment;
                dbEntity.PPMTAdjustment = loan.PPMTAdjustment;
                dbEntity.AdjustedPPMT = loan.AdjustedPPMT;
                dbEntity.AdjustedInterestOnLoan = loan.AdjustedInterestOnLoan;
                dbEntity.DoNotAdjustLoan = loan.DoNotAdjustLoan;
                dbEntity.UniqueId = loan.UniqueId;

                dbEntity.AdjustmentType = loan.AdjustmentType;
                dbEntity.AdjustmentPaymentOption = loan.AdjustmentPaymentOption;
                dbEntity.AdjustmentInstallmentOption = loan.AdjustmentInstallmentOption;
                dbEntity.AdjustmentDate = loan.AdjustmentDate;
                dbEntity.AdjustmentDateEng = loan.AdjustmentDateEng;

                //if (dbEntity.ToBePaidPrev != loan.ToBePaidPrev)
                //{
                //    dbEntity.ToBePaidPrev = loan.ToBePaidPrev;
                //    dbEntity.ToBePaidNew = loan.ToBePaidNew;
                //}

                UpdateChangeSet();
            }

            return status;
        }


        /// <summary>
        /// When other adjustment is done and interest rate is changed then use this function to update
        /// </summary>
       
        public static Status ChangeInterestRateAfterOtherAdjustmentUpdate(LoanAdjustment loan, 
            double interestRate, decimal opening, decimal ppmt, decimal ipmt, string title
            , decimal newPPMT, int noOfPayments)
        {

            Status status = new Status();

            if (CalculationManager.IsCalculationSavedForEmployee(loan.PayrollPeriodId, loan.EmployeeId))
            {
                status.ErrorMessage = "Salary already saved for the month " +
                    CommonManager.GetPayrollPeriod(loan.PayrollPeriodId).Name + ", adjustment can not be changed.";
                return status;
            }

            PEmployeeDeduction empDed = PayManager.GetEmployeeDeduction(loan.EmployeeId, loan.DeductionId);

            LoanAdjustment dbEntity = PayrollDataContext.LoanAdjustments.Where
              (l => l.EmployeeId == loan.EmployeeId && l.PayrollPeriodId == loan.PayrollPeriodId && l.DeductionId == loan.DeductionId)
              .SingleOrDefault();

            if (dbEntity.InterestPrev != null)
            {
                status.ErrorMessage = "Interest rate already changed, please delete current adjustment and change the rate.";
                return status;
            }



           // if (dbEntity.InterestPrev != null)
                dbEntity.InterestPrev = empDed.InterestRate;
          
           // if (dbEntity.MarketRatePrev != null)
                dbEntity.MarketRatePrev = empDed.MarketRate;

            dbEntity.InterestNew = loan.InterestNew;           
            dbEntity.MarketRateNew = loan.MarketRateNew;

            empDed.InterestRate = loan.InterestNew;
            empDed.MarketRate = loan.MarketRateNew;

            string month = "";
            if (loan.PayrollPeriodId != 0)
            {
                month = CommonManager.GetPayrollPeriod(loan.PayrollPeriodId).Name;
            }

            
            

          
          

            if (dbEntity.IPMTAdjustment != null)
            {
                dbEntity.AdjustedInterestOnLoan = dbEntity.IPMTAdjustment + ipmt;
            }
            else
            {
                dbEntity.AdjustedInterestOnLoan = ipmt;
            }

            //loan.UniqueId = empDed.UniqueId;



            UpdateChangeSet();

            return status;
        }

        /// <summary>
        /// Mark the loan as paid creating external settlement and interest adjustment internally
        /// </summary>
        /// <returns></returns>
        public static Status TerminateOrMarkAsLoanPaid(int employeeDeductionId,ref bool isDeletion)
        {
            isDeletion = false;
            Status status = new Status();
            PEmployeeDeduction empDed = PayrollDataContext.PEmployeeDeductions.FirstOrDefault(x => x.EmployeeDeductionId == employeeDeductionId);

            PayrollPeriod period = CommonManager.GetLastPayrollPeriod();

            if (CalculationManager.IsCalculationSavedForEmployee(period.PayrollPeriodId, empDed.EmployeeId))
            {
                status.ErrorMessage = "Salary already saved for the current period, delete the salary first and try to terminate the loan.";
                return status;
            }

            if (empDed.PDeduction.Calculation == DeductionCalculation.LOAN && empDed.PDeduction.IsEMI != null
                && empDed.PDeduction.IsEMI.Value)
            {
                    status.ErrorMessage = "Simple interest type of loan can only be adjusted.";
                    return status;
            }

            if (PayrollDataContext.LoanAdjustments.Any(x => x.EmployeeId == empDed.EmployeeId
                && x.DeductionId == empDed.DeductionId && x.PayrollPeriodId == period.PayrollPeriodId))
            {

                status = DeleteCurrentMonthSimpleInterestLoan(empDed.EmployeeId, empDed.DeductionId, period.PayrollPeriodId);

                if (status.IsSuccess)
                {
                    isDeletion = true;
                    return status;
                }

                status.ErrorMessage = "Loan has been already terminated/adjusted, please delete the adjustment from Loan adjustment and try to terminate the loan.";
                return status;
            }

            bool hasCurrentPayrollPeriod = false;
            bool isInstallmentComplete = false;
            List<GetLoanAdjustmentInstallmentsResult> loans = PayManager.GetLoanAdjustments(
                    empDed.DeductionId, empDed.EmployeeId, ref hasCurrentPayrollPeriod, ref isInstallmentComplete,
                    empDed.InterestRate.Value, 0, empDed.NoOfInstallments.Value);


            LoanAdjustment adj = new LoanAdjustment();
            adj.EmployeeId = empDed.EmployeeId;
            adj.PayrollPeriodId = period.PayrollPeriodId;
            adj.DeductionId = empDed.DeductionId;
            adj.UniqueId = empDed.UniqueId;
                       
            GetLoanAdjustmentInstallmentsResult item = loans.FirstOrDefault(x => x.PayrollPeriodId == period.PayrollPeriodId);
            if (item == null )
            {
                status.ErrorMessage = "Loan is not for processing.";
                return status;
            }

            adj.IPMTAdjustment = item.IPMT * -1;
            adj.AdjustedPPMT = item.Opening;
            adj.AdjustmentAmount = item.Opening;
            adj.AdjustedInterestOnLoan = 0;
            adj.AdjustedClosing = 0;
            adj.DoNotAdjustLoan = false;
            adj.AdjustmentType = (int)LoanAdjustmentType.FullSettlement;
            adj.AdjustmentPaymentOption = (int)LoanAdjustmentPayment.ExternalSettlement;
            adj.AdjustmentDate = period.StartDate;
            adj.AdjustmentDateEng = period.StartDateEng;
            adj.ToBePaidPrev = empDed.ToBePaidOver;
            adj.CreatedBy = SessionManager.CurrentLoggedInUserID;
            adj.CreatedOn = GetCurrentDateAndTime();

            PayrollDataContext.LoanAdjustments.InsertOnSubmit(adj);
            PayrollDataContext.SubmitChanges();

            return status;
        }

        /// <summary>
        /// Save/Update Simple Load Adjustment
        /// </summary>
        /// <param name="loan"></param>
        /// <param name="interestRate"></param>
        /// <param name="opening"></param>
        /// <param name="ppmt"></param>
        /// <param name="ipmt"></param>
        public static Status SaveUpdateIntertestAdjustment
            (LoanAdjustment loan,  decimal opening, decimal ppmt, decimal ipmt)
        {

            Status status = new Status();

            if (CalculationManager.IsCalculationSavedForEmployee(loan.PayrollPeriodId, loan.EmployeeId))
            {
                status.ErrorMessage = "Salary already saved for the month " +
                    CommonManager.GetPayrollPeriod(loan.PayrollPeriodId).Name + ", adjustment can not be changed.";
                return status;
            }

            PEmployeeDeduction empDed = PayManager.GetEmployeeDeduction(loan.EmployeeId, loan.DeductionId);

        

            string month = "";
            if (loan.PayrollPeriodId != 0)
            {
                month = CommonManager.GetPayrollPeriod(loan.PayrollPeriodId).Name;
            }

           

            LoanAdjustment dbEntity = PayrollDataContext.LoanAdjustments.Where
                (l => l.EmployeeId == loan.EmployeeId && l.PayrollPeriodId == loan.PayrollPeriodId && l.DeductionId == loan.DeductionId)
                .SingleOrDefault();


            //set adjusted closing
            if (loan.PPMTAdjustment != null)
            {

                //if do not adjust loan not checked, then normal case to use ppmt adjustment
                if (!loan.DoNotAdjustLoan.Value)
                    loan.AdjustedClosing = opening - ppmt - loan.PPMTAdjustment + Convert.ToDecimal(loan.Addition);
                else
                    loan.AdjustedClosing = opening - ppmt + Convert.ToDecimal(loan.Addition);

                loan.AdjustedPPMT = loan.PPMTAdjustment + ppmt;
            }
            else
            {
                loan.AdjustedClosing = opening - ppmt + Convert.ToDecimal(loan.Addition);
                loan.AdjustedPPMT = ppmt;
            }

            if (loan.IPMTAdjustment != null)
            {
                loan.AdjustedInterestOnLoan = loan.IPMTAdjustment + ipmt;
            }
            else
            {
                loan.AdjustedInterestOnLoan = ipmt;
            }

            loan.UniqueId = empDed.UniqueId;



            if (dbEntity == null)
            {
                PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(loan.EmployeeId, (byte)MonetoryChangeLogTypeEnum.Loan,
                   empDed.PDeduction.Title + " : Loan Adjustment : " + month, "-",
                   GetLoanAdjustmentLog(loan), LogActionEnum.Add));

                loan.CreatedBy = SessionManager.User.UserID;
                loan.CreatedOn = GetCurrentDateAndTime();

                PayrollDataContext.LoanAdjustments.InsertOnSubmit(loan);
                SaveChangeSet();
            }
            else
            {
                if (dbEntity.PPMTAdjustment != loan.PPMTAdjustment || dbEntity.IPMTAdjustment != loan.IPMTAdjustment
                    || dbEntity.DoNotAdjustLoan != loan.DoNotAdjustLoan)
                    PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(loan.EmployeeId, (byte)MonetoryChangeLogTypeEnum.Loan,
                       empDed.PDeduction.Title + " : Loan Adjustment", GetLoanAdjustmentLog(dbEntity), GetLoanAdjustmentLog(loan), LogActionEnum.Update));


                //loan.AdjustedInterestOnLoan = loan.IPMTAdjustment + ipmt;

                // first deduct prev adjustment
                dbEntity.AdjustedInterestOnLoan = Convert.ToDecimal(dbEntity.AdjustedInterestOnLoan) -
                    Convert.ToDecimal(dbEntity.IPMTAdjustment);



                dbEntity.IPMTAdjustment = loan.IPMTAdjustment;

                dbEntity.AdjustedInterestOnLoan += dbEntity.IPMTAdjustment;
               
                //if (dbEntity.ToBePaidPrev != loan.ToBePaidPrev)
                //{
                //    dbEntity.ToBePaidPrev = loan.ToBePaidPrev;
                //    dbEntity.ToBePaidNew = loan.ToBePaidNew;
                //}

                UpdateChangeSet();
            }

            return status;
        }
        public static AddOn GetAddOn(int addOnId)
        {
            return PayrollDataContext.AddOns.FirstOrDefault(x => x.AddOnId == addOnId);
        }
        public static Status DeleteAddOn(int addOnId)
        {
            Status status = new Status();
            AddOn db = PayrollDataContext.AddOns.FirstOrDefault(x => x.AddOnId == addOnId);
            PayrollPeriod period = CommonManager.GetLastPayrollPeriod();
            if (db.PayrollPeriodId != period.PayrollPeriodId)
            {
                status.ErrorMessage = "Current period Add-On can only be deleted.";
                return status;
            }

            bool hasNonZeroItem = false;
                //PayrollDataContext.AddOnHeaders.Join
                //(
                //    from h in PayrollDataContext.AddOnHeaders
                //    join d in PayrollDataContext.AddOnDetails on h.AddOnHeaderId equals d.AddOnHeaderId
                //    where h.AddOnId == addOnId
                //        && d.Amount != 0
                //    select d
                //).Any();

            if (hasNonZeroItem == false)
            {
                PayrollDataContext.AddOns.DeleteOnSubmit(db);
                PayrollDataContext.SubmitChanges();
            }
            else
            {
                status.ErrorMessage = "This add-on can not be deleted.";
            }
            return status;
        }
        public static Status SaveUpdateAddOn(AddOn addon)
        {

            if (addon.ShowAfterPayrollPeriodId == null || addon.ShowAfterPayrollPeriodId == -1 || addon.ShowAfterPayrollPeriodId == 0)
                addon.ShowAfterPayrollPeriodId = addon.PayrollPeriodId;

            Status status = new Status();
            if (addon.AddOnId == 0)
            {
                addon.DoNotShowPaymentAmount = true;
                PayrollDataContext.AddOns.InsertOnSubmit(addon);

                // if leave fare type is being added, then automatically set Leave fare income from leave fare setting
                if (addon.AddOnType != null && addon.AddOnType == (int)AddOnTypeEnum.LeaveFare)
                {
                    Setting setting = OvertimeManager.GetSetting();
                    if (setting.LeaveFareIncomeId != null && setting.LeaveFareIncomeId != -1)
                    {
                        AddOnHeader leaveFareHead = new AddOnHeader();
                        leaveFareHead.Type = 1;
                        leaveFareHead.SourceId = setting.LeaveFareIncomeId.Value;
                        //// for Janata multiple leave fare may need to enter in same year
                        //if (CommonManager.CompanySetting.WhichCompany == WhichCompany.Janata)
                        //    leaveFareHead.AddToRegularSalary = true;
                        //else
                            leaveFareHead.AddToRegularSalary = false;
                        leaveFareHead.UseIncomeSetting = true;
                        leaveFareHead.Title = new PayManager().GetIncomeById(setting.LeaveFareIncomeId.Value).Title;
                        addon.AddOnHeaders.Add(leaveFareHead);

                    }
                }
            }

            else
            {
                AddOn db = PayrollDataContext.AddOns.FirstOrDefault(x => x.AddOnId == addon.AddOnId);
                db.Name = addon.Name;
                db.ShowInPaySlip = addon.ShowInPaySlip;
                db.ShowAfterPayrollPeriodId = addon.ShowAfterPayrollPeriodId;
                db.DoNotShowPaymentAmount = true;
                db.AddOnType = addon.AddOnType;
            }

            PayrollDataContext.SubmitChanges();
            return status;
        }
        public static List<AddOn> GetAddOnForPayrollPeriod(int periodId)
        {
            return
                PayrollDataContext.AddOns.Where(x => x.PayrollPeriodId == periodId)
                .OrderBy(x => x.AddOnId).ToList();
        }

        /// <summary>
        /// Save/Update Simple Load Adjustment
        /// </summary>
        /// <param name="loan"></param>
        /// <param name="interestRate"></param>
        /// <param name="opening"></param>
        /// <param name="ppmt"></param>
        /// <param name="ipmt"></param>
        public static void SaveUpdateEMIAdjustment(LoanAdjustment loan, int interestRate, decimal opening, decimal ppmt, decimal ipmt, string title)
        {

            PEmployeeDeduction empDed = PayManager.GetEmployeeDeduction(loan.EmployeeId, loan.DeductionId);

            

            LoanAdjustment dbEntity = PayrollDataContext.LoanAdjustments.Where
                (l => l.EmployeeId == loan.EmployeeId && l.PayrollPeriodId == loan.PayrollPeriodId && l.DeductionId == loan.DeductionId)
                .SingleOrDefault();


            //set adjusted closing
            //if (loan.PPMTAdjustment != null)
            //{

            //    //if do not adjust loan not checked, then normal case to use ppmt adjustment
            //    if (!loan.DoNotAdjustLoan.Value)
            //        loan.AdjustedClosing = opening - ppmt - loan.PPMTAdjustment;
            //    else
            //        loan.AdjustedClosing = opening - ppmt;

            //    loan.AdjustedPPMT = loan.PPMTAdjustment + ppmt;
            //}
            //else
            //{
            //    loan.AdjustedClosing = opening - ppmt;
            //    loan.AdjustedPPMT = ppmt;
            //}

            //if (loan.IPMTAdjustment != null)
            //{
            //    loan.AdjustedInterestOnLoan = loan.IPMTAdjustment + ipmt;
            //}
            //else
            //{
            //    loan.AdjustedInterestOnLoan = ipmt;
            //}

            loan.UniqueId = empDed.UniqueId;



            if (dbEntity == null)
            {
                PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(loan.EmployeeId, (byte)MonetoryChangeLogTypeEnum.Loan,
                   title + " : Loan Adjustment", "-",
                   GetLoanAdjustmentLog(loan), LogActionEnum.Add));

                PayrollDataContext.LoanAdjustments.InsertOnSubmit(loan);
                SaveChangeSet();
            }
            else
            {
                if (dbEntity.PPMTAdjustment != loan.PPMTAdjustment || dbEntity.IPMTAdjustment != loan.IPMTAdjustment
                    || dbEntity.DoNotAdjustLoan != loan.DoNotAdjustLoan)
                    PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(loan.EmployeeId, (byte)MonetoryChangeLogTypeEnum.Loan,
                       title + " : Loan Adjustment", GetLoanAdjustmentLog(dbEntity), GetLoanAdjustmentLog(loan), LogActionEnum.Update));

                dbEntity.AdjustedClosing = loan.AdjustedClosing;
                dbEntity.IPMTAdjustment = loan.IPMTAdjustment;
                dbEntity.PPMTAdjustment = loan.PPMTAdjustment;
                dbEntity.AdjustedPPMT = loan.AdjustedPPMT;
                dbEntity.AdjustedInterestOnLoan = loan.AdjustedInterestOnLoan;
                dbEntity.DoNotAdjustLoan = loan.DoNotAdjustLoan;
                dbEntity.UniqueId = loan.UniqueId;
                dbEntity.InstallmentMonths = loan.InstallmentMonths;
                dbEntity.IsEMI = loan.IsEMI;
                UpdateChangeSet();
            }

        }
        /// <summary>
        /// Save/Update Simple Load Adjustment
        /// </summary>
        /// <param name="loan"></param>
        /// <param name="interestRate"></param>
        /// <param name="opening"></param>
        /// <param name="ppmt"></param>
        /// <param name="ipmt"></param>
        public static ResponseStatus SaveUpdate(AdvanceAdjustment loan, decimal opening, decimal installment, int remainingMonth,string title)
        {
            ResponseStatus status = new ResponseStatus();

            PEmployeeDeduction employeeDeduction = PayManager.GetEmployeeDeduction(loan.EmployeeId, loan.DeductionId);

            //Change the installment amount, now it will be the new one

            AdvanceAdjustment dbEntity = PayrollDataContext.AdvanceAdjustments.Where
                (l => l.EmployeeId == loan.EmployeeId && l.PayrollPeriodId == loan.PayrollPeriodId && l.DeductionId == loan.DeductionId)
                .SingleOrDefault();

            // Validate if the current payroll period month has full stop payment then don't allow to save adjustment
            if (dbEntity == null && loan.PayrollPeriodId != 0)
            {
                PayrollPeriod payrollPeriod = CommonManager.GetPayrollPeriod(loan.PayrollPeriodId);
                if (PayrollDataContext.StopPayments.Any(x => x.EmployeeId == loan.EmployeeId &&
                    payrollPeriod.StartDateEng >= x.EngFromDate && payrollPeriod.EndDateEng <= x.EngToDate))
                {
                    status.ErrorMessage =
                        string.Format("Full month Stop payment exists, for the month of {0}, can not save the adjustment.", payrollPeriod.Name);
                    return status;
                }
            }

            //Delete if not change
            if (dbEntity != null && loan.IsSkipThisMonth != null && loan.IsSkipThisMonth.Value == false && loan.Adjustment == null)
            {
                PayrollDataContext.AdvanceAdjustments.DeleteOnSubmit(dbEntity);
                DeleteChangeSet();
            }

            if (loan.IsSkipThisMonth == false)
            {

                //set adjusted closing
                if (loan.Adjustment != null)
                {
                    loan.AdjustedClosing = opening - installment - loan.Adjustment;
                    loan.Adjusted = loan.Adjustment + installment;
                }
                else
                {
                    loan.AdjustedClosing = opening - installment;
                    loan.Adjusted = installment;
                }

                //// Calculate new Installment Amount
                if (remainingMonth != 0)
                    loan.NewInstallmentAmount = loan.AdjustedClosing / remainingMonth;
                else
                {
                    loan.NewInstallmentAmount = loan.AdjustedClosing;
                }
               
            }
            else
            {
                loan.Adjusted = null;
                loan.Adjustment = null;
                loan.AdjustedClosing = opening;
                loan.NewInstallmentAmount = installment;
            }


            loan.UniqueId = employeeDeduction.UniqueId;
            if (dbEntity == null)
            {
                PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(loan.EmployeeId, (byte)MonetoryChangeLogTypeEnum.Advance,
                 title + " : Adv Adjustment", "-",
                 GetAdvanceAdjustmentLog(loan), LogActionEnum.Add));

                PayrollDataContext.AdvanceAdjustments.InsertOnSubmit(loan);
                SaveChangeSet();
            }
            else
            {

                if (dbEntity.Adjustment != loan.Adjustment 
                  || dbEntity.IsSkipThisMonth != loan.IsSkipThisMonth)
                    PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(loan.EmployeeId, (byte)MonetoryChangeLogTypeEnum.Advance,
                       title + " : Adv Adjustment", GetAdvanceAdjustmentLog(dbEntity), GetAdvanceAdjustmentLog(loan), LogActionEnum.Update));

                dbEntity.AdjustedClosing = loan.AdjustedClosing;
                //dbEntity.IPMTAdjustment = loan.IPMTAdjustment;
                dbEntity.Adjustment = loan.Adjustment;
                dbEntity.Adjusted = loan.Adjusted;

                dbEntity.NewInstallmentAmount = loan.NewInstallmentAmount;
                dbEntity.UniqueId = loan.UniqueId;

                dbEntity.IsSkipThisMonth = loan.IsSkipThisMonth;
               // dbEntity.AdjustedInterestOnLoan = loan.AdjustedInterestOnLoan;
               // dbEntity.DoNotAdjustLoan = loan.DoNotAdjustLoan;
                UpdateChangeSet();
            }
            return status;
        }
        #endregion


        public List<PEmployeeIncome> GetPercentOfSaleIncome(int? currentPage, int? pageSize, ref int tempCount, int? companyId, string sortBy,
              string employeeName)
        {
            currentPage -= 1;
            DbConnection conn = new SqlConnection(Config.ConnectionString);

            //PayrollDataContext.Connection;
            List<PEmployeeIncome> POSIncome = new List<PEmployeeIncome>();
            using (conn)
            {
                DbCommand selectCommand = conn.CreateCommand();
                selectCommand.CommandText = "GetPercentOFSaleIncome";
                selectCommand.CommandType = System.Data.CommandType.StoredProcedure;


                selectCommand.Parameters.Add(new SqlParameter("@currentPage", SqlDbType.Int));
                selectCommand.Parameters.Add(new SqlParameter("@pageSize", SqlDbType.Int));
                selectCommand.Parameters.Add(new SqlParameter("@sortBy", SqlDbType.VarChar));
                selectCommand.Parameters.Add(new SqlParameter("@total", SqlDbType.Int));
                selectCommand.Parameters.Add(new SqlParameter("@CompanyId", SqlDbType.Int));
                selectCommand.Parameters.Add(new SqlParameter("@EmployeeName", SqlDbType.VarChar));

                selectCommand.Parameters["@currentPage"].Value = currentPage;
                selectCommand.Parameters["@pageSize"].Value = pageSize;
                selectCommand.Parameters["@sortBy"].Value = sortBy;
                selectCommand.Parameters["@CompanyId"].Value = companyId;
                selectCommand.Parameters["@EmployeeName"].Value = employeeName;
                selectCommand.Parameters["@total"].Direction = ParameterDirection.Output;

                if (conn.State != ConnectionState.Open)
                    conn.Open();

                DbDataReader reader = selectCommand.ExecuteReader();
                using (reader)
                {
                    while (reader.Read())
                    {
                        PEmployeeIncome emp = new PEmployeeIncome();
                        emp.EEmployee = PayrollDataContext.EEmployees.Single(e => e.EmployeeId == (int)reader["EmployeeId"]);
                        emp.PIncome = PayrollDataContext.PIncomes.Single(p => p.Title == reader["Title"].ToString());
                        emp.EEmployee = PayrollDataContext.EEmployees.Single(e => e.Name == reader["Name"].ToString());
                        emp.RatePercent = Convert.ToDouble(reader["RatePercent"]);
                        emp.Amount = Convert.ToDecimal(reader["Amount"]);
                        emp.EmployeeIncomeId = (int)reader["EmployeeIncomeId"];
                        POSIncome.Add(emp);
                    }
                }
                tempCount = (int)selectCommand.Parameters["@total"].Value;

                return POSIncome;
            }
        }

        public Boolean UpdateEmployeeIncome(List<PEmployeeIncome> lstPEmplyeeIncome)
        {
            Boolean uStatus = true;
            // This is a slow process, will have to change.
            foreach (PEmployeeIncome obPEInc in lstPEmplyeeIncome)
            {
                int uRow = 0;
                uRow = PayrollDataContext.CalcUpdatePercentOfSale(obPEInc.EmployeeIncomeId, obPEInc.Amount);
                if (uRow == 1)
                    uStatus = false;

            }

            return uStatus;
        }

        #region "Optimum PF and CIT"



        public static List<GetEmployeeListForOptimumPFAndCITResult> GetEmployeeListForOptimum(int payrollperiodId, int citRoundingValue,
            int currentPage, int pageSize, ref int totalRecords,int branchId,int departmentId,string name)
        {

            totalRecords = 0;
            int payrollPeriodId = payrollperiodId;
            List<GetEmployeeListForOptimumPFAndCITResult> list = new List<GetEmployeeListForOptimumPFAndCITResult>();

            //If sal. not started from the start of fiscal year then we need to get the value from entry so
            //do we need to add this value or not, if current fiscal year starting month salary exsits then we don't
            bool readingSumForMiddleFiscalYearStartedReqd = false;
            //contains the id of starting-ending payroll period to be read for history of regular income
            int startingPayrollPeriodId = 0, endingPayrollPeriodId = 0;
            // int notWorkedMonthInFiscalYear = 0, passedMonthWorked = 0;


            PayrollPeriod startingMonthParollPeriodInFiscalYear =
               CalculationManager.GenerateForPastIncome(
                    payrollPeriodId, ref readingSumForMiddleFiscalYearStartedReqd,
                    ref startingPayrollPeriodId, ref endingPayrollPeriodId);


          

            list = PayrollDataContext.GetEmployeeListForOptimumPFAndCIT(payrollPeriodId, SessionManager.CurrentCompanyId, readingSumForMiddleFiscalYearStartedReqd,
               startingPayrollPeriodId, endingPayrollPeriodId, citRoundingValue, currentPage, pageSize
               ,branchId,departmentId,name,LeaveAttendanceManager.GetTotalDaysInFiscalYearForPeriod(payrollperiodId)).ToList();
            if (list.Count() > 0)
                totalRecords = list[0].TotalRows.Value;
            return list;



        }
        public static List<GetEmployeeListForOxfamOptimumPFAndCITResult> GetEmployeeListForOxfamOptimum(int payrollperiodId, int citRoundingValue,
          int currentPage, int pageSize, ref int totalRecords, int branchId, int departmentId, string name)
        {

            totalRecords = 0;
            int payrollPeriodId = payrollperiodId;
            List<GetEmployeeListForOxfamOptimumPFAndCITResult> list = new List<GetEmployeeListForOxfamOptimumPFAndCITResult>();

            //If sal. not started from the start of fiscal year then we need to get the value from entry so
            //do we need to add this value or not, if current fiscal year starting month salary exsits then we don't
            bool readingSumForMiddleFiscalYearStartedReqd = false;
            //contains the id of starting-ending payroll period to be read for history of regular income
            int startingPayrollPeriodId = 0, endingPayrollPeriodId = 0;
            // int notWorkedMonthInFiscalYear = 0, passedMonthWorked = 0;


            PayrollPeriod startingMonthParollPeriodInFiscalYear =
               CalculationManager.GenerateForPastIncome(
                    payrollPeriodId, ref readingSumForMiddleFiscalYearStartedReqd,
                    ref startingPayrollPeriodId, ref endingPayrollPeriodId);




            list = PayrollDataContext.GetEmployeeListForOxfamOptimumPFAndCIT(payrollPeriodId, SessionManager.CurrentCompanyId, readingSumForMiddleFiscalYearStartedReqd,
               startingPayrollPeriodId, endingPayrollPeriodId, citRoundingValue, currentPage, pageSize
               , branchId, departmentId, name, LeaveAttendanceManager.GetTotalDaysInFiscalYearForPeriod(payrollperiodId)).ToList();
            if (list.Count() > 0)
                totalRecords = list[0].TotalRows.Value;
            return list;



        }
        public static OptimumPFAndCIT GetOptimumPFAndCIT(int employeeId)
        {
            return PayrollDataContext.OptimumPFAndCITs.SingleOrDefault(
                o => o.EmployeeId == employeeId);
        }

        public static bool UpdateOptimum(OptimumPFAndCIT entity)
        {
            OptimumPFAndCIT dbEntity = PayrollDataContext.OptimumPFAndCITs.SingleOrDefault(
                o => o.EmployeeId == entity.EmployeeId);


            if( dbEntity.AdjustedPF != entity.AdjustedPF || dbEntity.AdjustedCIT != entity.AdjustedCIT)
                PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(entity.EmployeeId, (byte)MonetoryChangeLogTypeEnum.OptimumPFAndCIT
                      , "", GetOptimumLog(dbEntity), GetOptimumLog(entity), LogActionEnum.Update));


            if (dbEntity != null)
            {
                dbEntity.AdjustedPF = entity.AdjustedPF;
                dbEntity.AdjustedCIT = entity.AdjustedCIT;
            }

            return UpdateChangeSet();
        }

        public static string GetOptimumLog(OptimumPFAndCIT entity)
        {
            return "PF:" + GetCurrency(entity.AdjustedPF) + " ,CIT:" + GetCurrency(entity.AdjustedCIT);
        }

        public static bool SaveDeleteOptimumList(OptimumPFAndCIT entity)
        {
            OptimumPFAndCIT dbEntity = PayrollDataContext.OptimumPFAndCITs.SingleOrDefault(
                o => o.EmployeeId == entity.EmployeeId);


            #region "Change Logs"
            if (dbEntity == null)
            {
                PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(entity.EmployeeId, (byte)MonetoryChangeLogTypeEnum.OptimumPFAndCIT
                   , "", "-", GetOptimumLog(entity), LogActionEnum.Add));
            }
            else
            {
                PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(entity.EmployeeId, (byte)MonetoryChangeLogTypeEnum.OptimumPFAndCIT
                    , "", GetOptimumLog(entity), "-", LogActionEnum.Delete));
            }
            #endregion

            if (dbEntity == null)
            {
                //if Total Optimum CIT generated but not enabled then enable the CIT from here for this employee
                if (entity.TotalOptimumCIT.HasValue && entity.TotalOptimumCIT.Value != 0)
                {
                    EFundDetail fund = EmployeeManager.GetEmployeeFundDetail(entity.EmployeeId);
                    if (fund != null && fund.HasCIT==false )
                    {
                        PayrollPeriod payrollPeriod = CommonManager.GetPayrollPeriod(entity.PayrollPeriodId);
                        fund.HasCIT = true;
                        fund.CITEffectiveFromMonth = payrollPeriod.Month;
                        fund.CITEffectiveFromYear = payrollPeriod.Year;
                    }
                }



                PayrollDataContext.OptimumPFAndCITs.InsertOnSubmit(entity);
            }
            else
                PayrollDataContext.OptimumPFAndCITs.DeleteOnSubmit(dbEntity);

            PayrollDataContext.SubmitChanges();

            //process for specific change for NSET for first year
            //SetDiff(entity);


            return true;
        }

        //private static void SetDiff(OptimumPFAndCIT entity)
        //{
        //    int payrollPeriodId = entity.PayrollPeriodId;


        //    //If sal. not started from the start of fiscal year then we need to get the value from entry so
        //    //do we need to add this value or not, if current fiscal year starting month salary exsits then we don't
        //    bool readingSumForMiddleFiscalYearStartedReqd = false;
        //    //contains the id of starting-ending payroll period to be read for history of regular income
        //    int startingPayrollPeriodId = 0, endingPayrollPeriodId = 0;
        //    // int notWorkedMonthInFiscalYear = 0, passedMonthWorked = 0;


        //    PayrollPeriod startingMonthParollPeriodInFiscalYear =
        //       CalculationManager.GenerateForPastIncome(
        //            payrollPeriodId, ref readingSumForMiddleFiscalYearStartedReqd,
        //            ref startingPayrollPeriodId, ref endingPayrollPeriodId);


        //    //CalculationManage

        //    PayrollDataContext.UpdateCITDiffForFirstYearForNSET(
        //        entity.EmployeeId, entity.PayrollPeriodId, SessionManager.CurrentCompanyId,
        //        readingSumForMiddleFiscalYearStartedReqd, startingPayrollPeriodId, endingPayrollPeriodId
        //        , entity.TotalOptimumCIT.Value);

        //}

        #endregion

        #region Overtime

        public static List<GetOvertimeEmployeesResult> GetOvertimes(int payrollPeriodId, int branchid, int depid, int shiftid, string empName, int currentPage, int pageSize, ref int totalRecords)
        {
            List<GetOvertimeEmployeesResult> list = PayrollDataContext.GetOvertimeEmployees(SessionManager.CurrentCompanyId, payrollPeriodId, branchid, depid, shiftid, empName, currentPage, pageSize).ToList();
            if (list.Count() > 0)
                totalRecords = list[0].TotalRows.Value;
            else
                totalRecords = 0;
            return list;
        }

        public static List<GetPartialPaidTaxListResult> GetPartialPaidTaxList
            (int payrollPeriodId, int branchid, int depid, int employeeId, int currentPage, int pageSize, ref int totalRecords,string search)
        {
            List<GetPartialPaidTaxListResult> list = PayrollDataContext.GetPartialPaidTaxList(SessionManager.CurrentCompanyId, payrollPeriodId, 
                branchid, depid, employeeId,
                   currentPage, pageSize,search).ToList();
            if (list.Count() > 0)
                totalRecords = list[0].TotalRows.Value;
            else
                totalRecords = 0;
            return list;
        }
        public static List<GetAddOnEmployeeListResult> GetAddOnList
            (int payrollPeriodId, int addOnId, int branchid, int depid, int employeeId, int currentPage, int pageSize, 
            ref int totalRecords, string searchEmp, bool showAllWithBlankEmpAlso, bool applyDateFilter, DateTime? addOnDateFilter,int unitId)
        {
            List<GetAddOnEmployeeListResult> list = PayrollDataContext.GetAddOnEmployeeList
                (SessionManager.CurrentCompanyId, payrollPeriodId,addOnId,
                branchid, depid, employeeId,
                   currentPage, pageSize, searchEmp, showAllWithBlankEmpAlso,applyDateFilter,addOnDateFilter,unitId).ToList();
            if (list.Count() > 0)
                totalRecords = list[0].TotalRows.Value;
            else
                totalRecords = 0;
            return list;
        }
        public static List<GetIncomeDeductionAdjustmentEmployeesResult> GetIncomeDeductionAdjustment(int payrollPeriodId, int branchid, int depid, int employeeId, int incomeDeductionId, int type, int currentPage, int pageSize, ref int totalRecords)
        {
            //If sal. not started from the start of fiscal year then we need to get the value from entry so
            //do we need to add this value or not, if current fiscal year starting month salary exsits then we don't
            bool readingSumForMiddleFiscalYearStartedReqd = false;
            //contains the id of starting-ending payroll period to be read for history of regular income
            int startingPayrollPeriodId = 0, endingPayrollPeriodId = 0;
            // int notWorkedMonthInFiscalYear = 0, passedMonthWorked = 0;


            PayrollPeriod startingMonthParollPeriodInFiscalYear =
               CalculationManager.GenerateForPastIncome(
                    payrollPeriodId, ref readingSumForMiddleFiscalYearStartedReqd,
                    ref startingPayrollPeriodId, ref endingPayrollPeriodId);

            //Generate Temporary Salary
            //PayrollDataContext.GenerateTempSalary(payrollPeriodId, SessionManager.CurrentCompanyId, readingSumForMiddleFiscalYearStartedReqd,
            //startingPayrollPeriodId, endingPayrollPeriodId, (int)TempSalaryType.IncomeDeductionTaxAdjustment,type );

            List<GetIncomeDeductionAdjustmentEmployeesResult> list = PayrollDataContext.GetIncomeDeductionAdjustmentEmployees(SessionManager.CurrentCompanyId, payrollPeriodId, branchid, depid, employeeId,
                    incomeDeductionId, type,readingSumForMiddleFiscalYearStartedReqd,startingPayrollPeriodId,endingPayrollPeriodId,currentPage, pageSize,LeaveAttendanceManager.GetTotalDaysInFiscalYearForPeriod(payrollPeriodId)).ToList();
            if (list.Count() > 0)
                totalRecords = list[0].TotalRows.Value;
            else
                totalRecords = 0;
            return list;
        }

        public static List<GetFestivalIncomeDeductionAdjustmentEmployeesResult> GetFestivalIncomeDeductionAdjustment
            (int yearId,  int employeeId, int incomeDeductionId,  int currentPage, int pageSize, ref int totalRecords)
        {
           

            //Generate Temporary Salary
            //PayrollDataContext.GenerateTempSalary(payrollPeriodId, SessionManager.CurrentCompanyId, readingSumForMiddleFiscalYearStartedReqd,
            //startingPayrollPeriodId, endingPayrollPeriodId, (int)TempSalaryType.IncomeDeductionTaxAdjustment,type );

            List<GetFestivalIncomeDeductionAdjustmentEmployeesResult> list = PayrollDataContext
                .GetFestivalIncomeDeductionAdjustmentEmployees( yearId, employeeId,
                    incomeDeductionId,currentPage, pageSize).ToList();
            if (list.Count() > 0)
                totalRecords = list[0].TotalRows.Value;
            else
                totalRecords = 0;
            return list;
        }

        public static List<GetPastRegularAdjustmentEmployeesResult> GetPastRegularAdjustment
            (int yearId, int employeeId, int currentPage, int pageSize, ref int totalRecords)
        {


            //Generate Temporary Salary
            //PayrollDataContext.GenerateTempSalary(payrollPeriodId, SessionManager.CurrentCompanyId, readingSumForMiddleFiscalYearStartedReqd,
            //startingPayrollPeriodId, endingPayrollPeriodId, (int)TempSalaryType.IncomeDeductionTaxAdjustment,type );

            List<GetPastRegularAdjustmentEmployeesResult> list = PayrollDataContext
                .GetPastRegularAdjustmentEmployees(yearId, employeeId,
                     currentPage, pageSize).ToList();
            if (list.Count() > 0)
                totalRecords = list[0].TotalRows.Value;
            else
                totalRecords = 0;
            return list;
        }

        public static void SaveOvertimePay(string xml)
        {
            PayrollDataContext.SaveOvertimePay(XElement.Parse(xml));
        }


        public static void GenerateRetrospectForPercentOfIncome(int incomeId, int payrollPeriodId)
        {
            PIncome income = new PayManager().GetIncomeById(incomeId);
            string otherIDList = "";
            foreach (PIncomeIncome inc in income.PIncomeIncomes)
            {
                if (otherIDList == "")
                    otherIDList = inc.OtherIncomeId.ToString();
                else
                    otherIDList += "," + inc.OtherIncomeId;
            }

            PayrollDataContext.GenerateRetrospectIncrement(incomeId, payrollPeriodId, otherIDList);
        }

        public static int GetPercentOfIncomeFirstOtherIncomeId(int incomeId)
        {
            PIncomeIncome inc = 
                PayrollDataContext.PIncomeIncomes.Where(x => x.IncomeId == incomeId)
                .FirstOrDefault();
            if (inc != null)
                return inc.OtherIncomeId;
            return 0;
        }

        public static bool IfPercentOfIncomeAlreadyRetrospectIncrement(int incomeId, int payrollPeriodId)
        {
            return
                PayrollDataContext.RetrospectIncrements.Any(x => x.IncomeId == incomeId && x.PayrollPeriodId == payrollPeriodId);
        }

        public static void UpdateRetirementOtherIncomeAdjustment(string retirementNotes, List<CalculationValue> list,List<CalculationValue> listOtherDeduction,
            int payrollPeriodId, int employeeId)
        {

            EHumanResource hr = PayrollDataContext.EHumanResources.FirstOrDefault(x => x.EmployeeId == employeeId);
            hr.RetirementNotes = retirementNotes;

            List<SettlementLeaveDetail> leaves = PayrollDataContext.SettlementLeaveDetails
                .Where(x => x.EmployeeId == employeeId && x.PayrollPeriodId == payrollPeriodId).ToList();

            foreach (SettlementLeaveDetail item in leaves)
            {
                if (item.LeaveEncashmentDaysOrHours != null)
                {
                    if (item.LeaveEncashmentDaysOrHours > 0)
                    {

                        CalculationValue inputValue = list.SingleOrDefault(x => x.Type == (int)SettlementDetailType.LeaveEncash
                            && x.SourceId == (int)SettlementDetailType.LeaveEncash);

                        if (inputValue != null)
                        {
                            item.RetirementAdjustment = inputValue.Amount - inputValue.AmountWithOutAdjustment;

                            item.TDSAdjustment = inputValue.TDSAdjustment;
                        }
                    }

                }
            }

            List<SettlementDetail> details = PayrollDataContext.SettlementDetails
               .Where(x => x.EmployeeId == employeeId && x.PayrollPeriodId == payrollPeriodId).ToList();

            foreach (SettlementDetail item in details)
            {
                SettlementDetailType type = (SettlementDetailType)item.Type;

                switch (type)
                {

                    case SettlementDetailType.DashainBonus:

                        CalculationValue inputValue = list.SingleOrDefault(x => x.Type == (int)SettlementDetailType.DashainBonus
                            && x.SourceId == (int)SettlementDetailType.DashainBonus);

                        if (inputValue != null)
                            item.RetirementAdjustment = inputValue.Amount - inputValue.AmountWithOutAdjustment;

                        break;
                    case SettlementDetailType.Gratuity:

                        CalculationValue inputGratuity = list.SingleOrDefault(x => x.Type == (int)SettlementDetailType.Gratuity
                            && x.SourceId == (int)SettlementDetailType.Gratuity);

                        if (inputGratuity != null)
                        {
                            item.RetirementAdjustment = inputGratuity.Amount - inputGratuity.AmountWithOutAdjustment;

                            item.TDSAdjustment = inputGratuity.TDSAdjustment;
                        }

                        break;

                }
            }

            foreach (CalculationValue ded in listOtherDeduction)
            {
                SettlementDetail dbDetail = details.FirstOrDefault(x => x.SettlementId == ded.SettlementId);
                if (dbDetail != null)
                {
                    dbDetail.RetirementAdjustment = ded.Amount - ded.AmountWithOutAdjustment;
                    dbDetail.ExcludeFromRetirement = ded.ExcludeFromRetirement;
                }
            }

            PayrollDataContext.SubmitChanges();
        }

        /// <summary>
        /// Add Other incomes like Dashian,Retirement leave encashment in regular incomeList
        /// </summary>
        /// <param name="incomeList"></param>
        /// <returns></returns>
        public static void SetOtherRetirementIncomesAndDeductions(int employeeId, PayrollPeriod period, ref List<CalculationValue> incomeList, ref List<CalculationValue> deductionList)
        {
            //List<CalculationValue> incomeList = new List<CalculationValue>();
            //List<CalculationValue> deductionList = new List<CalculationValue>();

            // 1. Add Retirement leave encashment or Excessive leave
            List<SettlementLeaveDetail> leaves = PayrollDataContext.SettlementLeaveDetails.Where(x => x.EmployeeId == employeeId && x.PayrollPeriodId == period.PayrollPeriodId).ToList();
            foreach (SettlementLeaveDetail item in leaves)
            {
                if (item.LeaveEncashmentDaysOrHours != null)
                {
                    //if (item.LeaveEncashmentDaysOrHours > 0)
                    {
                        incomeList.Add(new CalculationValue
                        {
                            Type = (int)SettlementDetailType.LeaveEncash,
                            SourceId = (int)SettlementDetailType.LeaveEncash,
                            FullAmount = Convert.ToDecimal(item.LeaveEncashmentAmount),

                            AdjustedAmount = Convert.ToDecimal(item.LeaveEncashmentAmount)
                                    + Convert.ToDecimal(item.RetirementAdjustment),

                            TDS = 
                                (
                                    Convert.ToDecimal(item.LeaveEncashmentAmount) + Convert.ToDecimal(item.RetirementAdjustment))
                                    * Convert.ToDecimal(15.0 / 100.0
                                ),

                            TDSAdjustment = (item.TDSAdjustment == null ? 0 : item.TDSAdjustment.Value),

                            NetPayable =
                            (Convert.ToDecimal(item.LeaveEncashmentAmount) + Convert.ToDecimal(item.RetirementAdjustment))
                            -
                            (
                                (
                                    Convert.ToDecimal(item.LeaveEncashmentAmount) + Convert.ToDecimal(item.RetirementAdjustment))
                                    * Convert.ToDecimal(15.0 / 100.0
                                )
                            ) - (item.TDSAdjustment == null ? 0 : item.TDSAdjustment.Value),

                            // deduct 15% percent tax
                            //AmountWithOutAdjustment = Convert.ToDecimal(item.LeaveEncashmentAmount) - 
                            //        (Convert.ToDecimal(item.LeaveEncashmentAmount) * Convert.ToDecimal(15.0 / 100.0)),
                            //Amount =
                            //    Convert.ToDecimal(item.LeaveEncashmentAmount) -
                            //    ((Convert.ToDecimal(item.LeaveEncashmentAmount) * Convert.ToDecimal(15.0 / 100.0)))
                            //    + Convert.ToDecimal(item.RetirementAdjustment),
                            HeaderName = "Retirement Leave Encashment"
                        });
                    }
                    //else if (item.LeaveEncashmentDaysOrHours < 0)
                    //{
                    //    deductionList.Add(new CalculationValue
                    //    {
                    //        Type = (int)SettlementDetailType.ExcessiveLeave,
                    //        SourceId = (int)SettlementDetailType.ExcessiveLeave,
                    //        AmountWithOutAdjustment = Convert.ToDecimal(item.LeaveEncashmentAmount),
                    //        Amount = Convert.ToDecimal(item.LeaveEncashmentAmount) + Convert.ToDecimal(item.RetirementAdjustment),
                    //        HeaderName = "Excessive Leave"
                    //    });
                    //}
                }
            }

            List<SettlementDetail> details = PayrollDataContext.SettlementDetails
                .Where(x => x.EmployeeId == employeeId && x.PayrollPeriodId == period.PayrollPeriodId).ToList();

            foreach (SettlementDetail item in details)
            {
                SettlementDetailType type = (SettlementDetailType)item.Type;

                switch (type)
                {
                    case SettlementDetailType.Advance:
                        deductionList.Add(new CalculationValue
                        {
                            Type = (int)type,
                            SourceId = (int)type,
                            AmountWithOutAdjustment = Convert.ToDecimal(item.Value),
                            Amount = Convert.ToDecimal(item.Value) + Convert.ToDecimal(item.RetirementAdjustment),
                            HeaderName = "Rem - " + item.Name,
                            ExcludeFromRetirement = item.ExcludeFromRetirement == null ? false : item.ExcludeFromRetirement.Value,
                            SettlementId = item.SettlementId
                        });
                        break;
                    case SettlementDetailType.Gratuity:
                        incomeList.Add(new CalculationValue
                        {
                            Type = (int)SettlementDetailType.Gratuity,
                            SourceId = (int)SettlementDetailType.Gratuity,
                            FullAmount = Convert.ToDecimal(item.Value),

                            AdjustedAmount = Convert.ToDecimal(item.Value)
                                    + Convert.ToDecimal(item.RetirementAdjustment),

                            TDS =
                                (
                                    Convert.ToDecimal(item.Value) + Convert.ToDecimal(item.RetirementAdjustment))
                                    * Convert.ToDecimal(15.0 / 100.0
                                ),

                            TDSAdjustment = (item.TDSAdjustment == null ? 0 : item.TDSAdjustment.Value),

                            NetPayable =
                            (Convert.ToDecimal(item.Value) + Convert.ToDecimal(item.RetirementAdjustment))
                            -
                            (
                                (
                                    Convert.ToDecimal(item.Value) + Convert.ToDecimal(item.RetirementAdjustment))
                                    * Convert.ToDecimal(15.0 / 100.0
                                )
                            ) - (item.TDSAdjustment == null ? 0 : item.TDSAdjustment.Value),

                            // deduct 15% percent tax
                            //AmountWithOutAdjustment = Convert.ToDecimal(item.LeaveEncashmentAmount) - 
                            //        (Convert.ToDecimal(item.LeaveEncashmentAmount) * Convert.ToDecimal(15.0 / 100.0)),
                            //Amount =
                            //    Convert.ToDecimal(item.LeaveEncashmentAmount) -
                            //    ((Convert.ToDecimal(item.LeaveEncashmentAmount) * Convert.ToDecimal(15.0 / 100.0)))
                            //    + Convert.ToDecimal(item.RetirementAdjustment),
                            HeaderName = "Gratuity"
                        });
                        break;
                    case SettlementDetailType.DashainBonus:
                        incomeList.Add(new CalculationValue
                        {
                            Type = (int)type,
                            SourceId = (int)type,
                            FullAmount = Convert.ToDecimal(item.Value),
                            // deduct 15% percent tax
                            AmountWithOutAdjustment = Convert.ToDecimal(item.Value) -
                                    (Convert.ToDecimal(item.Value) * Convert.ToDecimal(15.0 / 100.0)),
                            Amount =
                                Convert.ToDecimal(item.Value) -
                                ((Convert.ToDecimal(item.Value) * Convert.ToDecimal(15.0 / 100.0)))
                                + Convert.ToDecimal(item.RetirementAdjustment),
                            HeaderName = "Dashain",
                            SettlementId = item.SettlementId
                        });
                        break;
                    case SettlementDetailType.Loan:
                         deductionList.Add(new CalculationValue
                        {
                            Type = (int)type,
                            SourceId = (int)type,
                            AmountWithOutAdjustment = Convert.ToDecimal(GetCurrency( item.Value)),
                            Amount = Convert.ToDecimal(item.Value) + Convert.ToDecimal(item.RetirementAdjustment),
                            HeaderName = "Rem - " + item.Name,
                            ExcludeFromRetirement = item.ExcludeFromRetirement == null ? false : item.ExcludeFromRetirement.Value,
                            SettlementId = item.SettlementId
                        });
                        break;
                    case SettlementDetailType.LoanInterest:
                        deductionList.Add(new CalculationValue
                        {
                            Type = (int)type,
                            SourceId = (int)type,
                            AmountWithOutAdjustment = Convert.ToDecimal(GetCurrency( item.Value)),
                            Amount = Convert.ToDecimal(item.Value) + Convert.ToDecimal(item.RetirementAdjustment),
                            HeaderName = "Rem - " + item.Name,
                            ExcludeFromRetirement = item.ExcludeFromRetirement == null ? false : item.ExcludeFromRetirement.Value,
                            SettlementId = item.SettlementId
                        });
                        break;
                    case SettlementDetailType.LoanRepayment:
                        deductionList.Add(new CalculationValue
                        {
                            Type = (int)type,
                            SourceId = (int)type,
                            AmountWithOutAdjustment = Convert.ToDecimal(GetCurrency( item.Value)),
                            Amount = Convert.ToDecimal(item.Value) + Convert.ToDecimal(item.RetirementAdjustment),
                            HeaderName = "Rem - " + item.Name,
                            ExcludeFromRetirement = item.ExcludeFromRetirement == null ? false : item.ExcludeFromRetirement.Value,
                            SettlementId = item.SettlementId
                        });
                        break;
                    case SettlementDetailType.RefundableAdvance:
                         deductionList.Add(new CalculationValue
                        {
                            Type = (int)type,
                            SourceId = (int)type,
                            AmountWithOutAdjustment = Convert.ToDecimal(item.Value),
                            Amount = Convert.ToDecimal(item.Value) + Convert.ToDecimal(item.RetirementAdjustment),
                            HeaderName = "Refundable - " + item.Name,
                            ExcludeFromRetirement = item.ExcludeFromRetirement == null ? false : item.ExcludeFromRetirement.Value,
                            SettlementId = item.SettlementId
                        });
                        break;
                }

            }

         
        } 

        /// <summary>
        /// Change NoCIT option for the employee from Retirement
        /// </summary>
        /// <param name="employeeId"></param>
        /// <param name="noCIT"></param>
        /// <param name="period"></param>
        public static void ChangeNoCITFromRetirement(int employeeId, bool noCIT,PayrollPeriod period)
        {
            EFundDetail dbEntity = PayrollDataContext.EFundDetails.FirstOrDefault(x => x.EmployeeId == employeeId);

            //No    CIT checked
            if (dbEntity.NoCITContribution == false && noCIT == true)
            {
                PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(dbEntity.EmployeeId.Value, (byte)MonetoryChangeLogTypeEnum.NoCIT,
                    "", "-", EmployeeManager.GetCITDuration(dbEntity), LogActionEnum.Add));
            }
            else if (dbEntity.NoCITContribution == true && noCIT == true && dbEntity.NoCITToMonth != period.Month && dbEntity.NoCITToYear != period.Year)
            {
                PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(dbEntity.EmployeeId.Value, (byte)MonetoryChangeLogTypeEnum.NoCIT,
                    "", EmployeeManager.GetCITDuration(dbEntity), period.Month + "/" + dbEntity.NoCITToYear, LogActionEnum.Update));
            }
            else if (dbEntity.NoCITContribution == true && noCIT == false)
            {
                PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(dbEntity.EmployeeId.Value, (byte)MonetoryChangeLogTypeEnum.NoCIT,
                    "", EmployeeManager.GetCITDuration(dbEntity), "-", LogActionEnum.Delete));
            }

            if ((dbEntity.NoCITContribution != null && dbEntity.NoCITContribution.Value) || noCIT)
            {
                dbEntity.NoCITContribution = true;
                dbEntity.NoCITFromMonth = period.Month;
                dbEntity.NoCITFromYear = period.Year;

                dbEntity.NoCITToMonth = period.Month;
                dbEntity.NoCITToYear = period.Year;

                dbEntity.NoCITFromEng = new CustomDate(1, dbEntity.NoCITFromMonth.Value, dbEntity.NoCITFromYear.Value, IsEnglish).EnglishDate;
                dbEntity.NoCITToEng = new CustomDate(DateHelper.GetTotalDaysInTheMonth(dbEntity.NoCITToYear.Value, dbEntity.NoCITToMonth.Value, IsEnglish)
                    , dbEntity.NoCITToMonth.Value, dbEntity.NoCITToYear.Value, IsEnglish).EnglishDate;
            }
            else
            {
                dbEntity.NoCITContribution = false;
                //dbEntity.NoCITFromMonth = period.Month;
                //dbEntity.NoCITFromYear = period.Year;

                //dbEntity.NoCITToMonth = period.Month;
                //dbEntity.NoCITToYear = period.Year;

                //dbEntity.NoCITFromEng = new CustomDate(1, dbEntity.NoCITFromMonth.Value, dbEntity.NoCITFromYear.Value, IsEnglish).EnglishDate;
                //dbEntity.NoCITToEng = new CustomDate(DateHelper.GetTotalDaysInTheMonth(dbEntity.NoCITToYear.Value, dbEntity.NoCITToMonth.Value, IsEnglish)
                //    , dbEntity.NoCITToMonth.Value, dbEntity.NoCITToYear.Value, IsEnglish).EnglishDate;

            }

            

            PayrollDataContext.SubmitChanges();
        }
        public static void SaveIncomeDeductionAdjustment(string xml,string title)
        {
            PayrollDataContext.SaveIncomeDeductionAdjustment(XElement.Parse(xml),title,SessionManager.UserName,
                SessionManager.CurrentCompanyId);
        }
        public static void SaveFestivalIncomeDeductionAdjustment(string xml, string title)
        {
            PayrollDataContext.SaveFestivalIncomeDeductionAdjustment(XElement.Parse(xml), title, SessionManager.UserName,
                SessionManager.CurrentCompanyId);
        }
        public static void ReSaveRetrospectIncrement(string xml)
        {
            PayrollDataContext.ReSaveRetrospectIncrement(XElement.Parse(xml), SessionManager.UserName,
                SessionManager.CurrentCompanyId);
        }
        public static PartialTax GetPartialTax(int payrollPeriodId)
        {
            return
                PayrollDataContext.PartialTaxes.SingleOrDefault(x => x.PayrollPeriodId == payrollPeriodId);
        }

        public static void SavePartialTax(string name, int incomeId, int payrollPeriodId, int showAfterPeriodId, bool showInPayslip)
        {

            PartialTax dbTax = PayrollDataContext.PartialTaxes.SingleOrDefault(x => x.PayrollPeriodId == payrollPeriodId);
            if (dbTax != null)
            {
                dbTax.Name = name;
                dbTax.IncomeId = incomeId;
                dbTax.ShowAfterPayrollPeriodId = showAfterPeriodId;
                dbTax.ShowInPayslip = showInPayslip;
            }
            else
            {
                dbTax = new PartialTax();
                dbTax.ShowInPayslip = showInPayslip;
                dbTax.PayrollPeriodId = payrollPeriodId;
                dbTax.Name = name;
                dbTax.IncomeId = incomeId;
                dbTax.ShowAfterPayrollPeriodId = showAfterPeriodId;
                PayrollDataContext.PartialTaxes.InsertOnSubmit(dbTax);
            }
            PayrollDataContext.SubmitChanges();
        }



        public static int SavePartialtaxList(List<PartialPaidDetail> list,int payrollPeriodId,List<int> empIDList)
        {
            int importedEmployee = 0;
            List<int> savedEmployeeList  = new List<int>();

            PayrollPeriod period = CommonManager.GetPayrollPeriod(payrollPeriodId);

            CCalculation calc = PayrollDataContext.CCalculations.SingleOrDefault(x=>x.PayrollPeriodId==payrollPeriodId);
            if(calc != null)
            {
                savedEmployeeList = PayrollDataContext.CCalculationEmployees.Where(x => x.CalculationId == calc.CalculationId).Select(x=>x.EmployeeId.Value).ToList();
            }

            foreach (int employeeID in empIDList)
            {

                if (!savedEmployeeList.Any(x => x == employeeID))
                {
                    importedEmployee += 1;

                    PartialTaxPaid dbtax = PayrollDataContext.PartialTaxPaids.SingleOrDefault(x => x.EmployeeId == employeeID
                        && x.PayrollPeriodId == payrollPeriodId);

                    if (dbtax == null)
                    {
                        dbtax = new PartialTaxPaid { EmployeeId = employeeID, PayrollPeriodId = payrollPeriodId, PaidAmount = 0, CITAmount = 0, PFAmount = 0, DeductionPFAmount = 0 };
                        PayrollDataContext.PartialTaxPaids.InsertOnSubmit(dbtax);
                    }
                }
            }

            PayrollDataContext.SubmitChanges();

            List<ChangeMonetory> dbList = new List<ChangeMonetory>();

            foreach (PartialPaidDetail tax in list)
            {
                if (savedEmployeeList.Any(x => x == tax.EmployeeId))
                {
                    continue;
                }

                // Imported Log
                if (dbList.Any(x => x.EmployeeId == tax.EmployeeId) == false)
                {
                    dbList.Add(GetMonetoryChangeLog(
                                        tax.EmployeeId, (byte)MonetoryChangeLogTypeEnum.AddOn, period.Name + " Add On Import", "", "", LogActionEnum.Add)
                                        );
                }

                if (tax.PartialPaidHeader.Type == (int)(CalculationColumnType.IncomePF) && tax.PartialPaidHeader.SourceId == (int)(CalculationColumnType.IncomePF))
                {
                    PartialTaxPaid dbtax = PayrollDataContext.PartialTaxPaids.SingleOrDefault(x => x.EmployeeId == tax.EmployeeId
                        && x.PayrollPeriodId == payrollPeriodId);
                    
                        dbtax.PFAmount = tax.Amount;
                }
                else if (tax.PartialPaidHeader.Type == (int)(CalculationColumnType.DeductionPF) && tax.PartialPaidHeader.SourceId == (int)(CalculationColumnType.DeductionPF))
                {
                    PartialTaxPaid dbtax = PayrollDataContext.PartialTaxPaids.SingleOrDefault(x => x.EmployeeId == tax.EmployeeId
                        && x.PayrollPeriodId == payrollPeriodId);

                    dbtax.DeductionPFAmount = tax.Amount;
                }
                else if (tax.PartialPaidHeader.Type == (int)(CalculationColumnType.DeductionCIT) && tax.PartialPaidHeader.SourceId == (int)(CalculationColumnType.DeductionCIT))
                {
                    PartialTaxPaid dbtax = PayrollDataContext.PartialTaxPaids.SingleOrDefault(x => x.EmployeeId == tax.EmployeeId
                        && x.PayrollPeriodId == payrollPeriodId);
                   
                        dbtax.CITAmount = tax.Amount;
                }
                else if (tax.PartialPaidHeader.Type == (int)(CalculationColumnType.TDS) && tax.PartialPaidHeader.SourceId == (int)(CalculationColumnType.TDS))
                {
                    PartialTaxPaid dbtax = PayrollDataContext.PartialTaxPaids.SingleOrDefault(x => x.EmployeeId == tax.EmployeeId
                        && x.PayrollPeriodId == payrollPeriodId);
                    
                       
                        dbtax.PaidAmount = tax.Amount;
                }
                else
                {
                    PartialPaidDetail dbDetail =
                        (
                        from h in PayrollDataContext.PartialPaidHeaders
                        join d in PayrollDataContext.PartialPaidDetails on h.PartialPaidHeaderId equals d.PartialPaidHeaderId

                        where h.Type == tax.PartialPaidHeader.Type && h.SourceId == tax.PartialPaidHeader.SourceId
                            && d.EmployeeId == tax.EmployeeId && h.PayrollPeriodId == payrollPeriodId
                        select d
                        ).SingleOrDefault();

                    if (dbDetail == null)
                    {
                        PartialPaidHeader header = PayrollDataContext.PartialPaidHeaders.SingleOrDefault(x => x.PayrollPeriodId == payrollPeriodId
                            && x.Type == tax.PartialPaidHeader.Type && x.SourceId == tax.PartialPaidHeader.SourceId);
                        if (header != null)
                        {
                            PayrollDataContext.PartialPaidDetails.InsertOnSubmit(
                                new PartialPaidDetail { Amount = tax.Amount, EmployeeId = tax.EmployeeId, PartialPaidHeaderId = header.PartialPaidHeaderId }
                                );
                        }
                    }
                    else
                    {
                        dbDetail.Amount = tax.Amount;
                    }
                }
            }

            PayrollDataContext.ChangeMonetories.InsertAllOnSubmit(dbList);
            PayrollDataContext.SubmitChanges();
            return importedEmployee;
        }

        public static int ImportAddOnList(List<AddOnDetail> list, int addonId, int payrollPeriodId, List<CalcGetHeaderListResult> headerList
            ,ref string message)
        {
            int importedEmployee = 0;
           
            PayrollPeriod period = CommonManager.GetPayrollPeriod(payrollPeriodId);

            List<AddOnDetail> dbAddOnList =
                     (
                     from h in PayrollDataContext.AddOnHeaders
                     join d in PayrollDataContext.AddOnDetails on h.AddOnHeaderId equals d.AddOnHeaderId
                     where h.AddOnId == addonId
                     select d
                     ).ToList();


            List<int> salarySavedEINList =
                (
                from c in PayrollDataContext.CCalculations
                join p in PayrollDataContext.PayrollPeriods on c.PayrollPeriodId equals p.PayrollPeriodId
                join ce in PayrollDataContext.CCalculationEmployees on c.CalculationId equals ce.CalculationId
                where p.PayrollPeriodId == payrollPeriodId
                select ce.EmployeeId.Value
                ).ToList();


            AddOn addOn = PayrollDataContext.AddOns.FirstOrDefault(x => x.AddOnId == addonId);

            List<AddOnHeader> dbHeaderList = PayrollDataContext.AddOnHeaders.Where(x => x.AddOnId == addonId).ToList();
                        DateTime createdOn = GetCurrentDateAndTime();

            List<ChangeMonetory> dbList = new List<ChangeMonetory>();
            List<AddOnDetail> dbNewDetailList = new List<AddOnDetail>();

            foreach (AddOnDetail tax in list)
            {

                if (salarySavedEINList.Contains(tax.EmployeeId))
                {
                    
                    message = "Salary already saved for employee " + tax.EmployeeId + " : " + EmployeeManager.GetEmployeeName(
                        tax.EmployeeId) + ".";
                    return 0;
                }
               
                AddOnDetail dbDetail =
                    dbAddOnList.FirstOrDefault(x =>
                        x.AddOnHeader.Type == tax.AddOnHeader.Type
                        && x.AddOnHeader.SourceId == tax.AddOnHeader.SourceId
                        && x.EmployeeId == tax.EmployeeId);

                AddOnHeader header = null;

                
                if (dbDetail == null)
                {
                    header = dbHeaderList.FirstOrDefault(x => x.AddOnId == addonId
                        && x.Type == tax.AddOnHeader.Type && x.SourceId == tax.AddOnHeader.SourceId);

                    if (header != null && tax.Amount != 0)
                    {
                        dbNewDetailList.Add(//PayrollDataContext.AddOnDetails.InsertOnSubmit(
                            new AddOnDetail
                            {
                                Amount = tax.Amount,
                                EmployeeId = tax.EmployeeId,
                                AddOnHeaderId = header.AddOnHeaderId,
                                CreatedOn = createdOn
                            }
                            );


                        CalcGetHeaderListResult addonHeader = headerList.FirstOrDefault(x => x.Type == tax.AddOnHeader.Type && x.SourceId == tax.AddOnHeader.SourceId);

                        dbList.Add(GetMonetoryChangeLog(
                            tax.EmployeeId, (byte)MonetoryChangeLogTypeEnum.AddOn, period.Name + " :" + addOn.Name  + " : " + " Add On Import " + (addonHeader == null ? "" : addonHeader.HeaderName), "", tax.Amount, LogActionEnum.Add)
                                        );

                    }
                }
                else
                {

                    CalcGetHeaderListResult addonHeader = headerList.FirstOrDefault(x => x.Type == tax.AddOnHeader.Type && x.SourceId == tax.AddOnHeader.SourceId);

                    if (dbDetail.Amount != tax.Amount)
                    {

                        dbList.Add(GetMonetoryChangeLog(
                            tax.EmployeeId, (byte)MonetoryChangeLogTypeEnum.AddOn, period.Name + " :" + addOn.Name  + " : " + " Add On Import " + (addonHeader == null ? "" : addonHeader.HeaderName), dbDetail.Amount, tax.Amount, LogActionEnum.Update)
                                        );
                    }
                    if (dbDetail.Amount == 0)
                        dbDetail.CreatedOn = createdOn;

                    dbDetail.Amount = tax.Amount;


                   
                }


                // Imported Log
                //if (dbList.Any(x => x.EmployeeId == tax.EmployeeId) == false)
                {
                    
                  

                 
                }


            }


            PayrollDataContext.AddOnDetails.InsertAllOnSubmit(dbNewDetailList);
            PayrollDataContext.ChangeMonetories.InsertAllOnSubmit(dbList);
            PayrollDataContext.SubmitChanges();
            return importedEmployee;
        }

        public static int UpdateFixedDeductionPFAndCITForOxfam(List<GetEmployeeListForOxfamOptimumPFAndCITResult> list)
        {
            int importedEmployee = 0;





            importedEmployee = list.Count;

            List<ChangeMonetory> dbList = new List<ChangeMonetory>();

            PayrollPeriod period = CommonManager.GetValidLastPayrollPeriod();

            if (period != null)
            {

                foreach (GetEmployeeListForOxfamOptimumPFAndCITResult item in list)
                {

                    EFundDetail fund = PayrollDataContext.EFundDetails.FirstOrDefault(x => x.EmployeeId == item.EID);

                    dbList.Add(GetMonetoryChangeLog(
                        item.EID, (byte)MonetoryChangeLogTypeEnum.CIT, period.Name + " CIT Update ", fund.CITAmount == null ? "" : GetCurrency(fund.CITAmount), item.NewCIT == null ? "" : GetCurrency(item.NewCIT), LogActionEnum.Update)
                                            );

                    dbList.Add(GetMonetoryChangeLog(
                        item.EID, (byte)MonetoryChangeLogTypeEnum.PF, period.Name + " PF Update ", item.CurrentDeductionPF == null ? "" : GetCurrency(item.CurrentDeductionPF), item.NewPF == null ? "" : GetCurrency(item.NewPF), LogActionEnum.Update)
                                            );


                    fund.CITIsRate = false;
                    fund.CITAmount = (decimal)(item.NewCIT == null ? 0 : item.NewCIT);

                    fund.IsPFDeductionFixed = true;
                    fund.FixedPFDeductionAmount = (decimal)(item.NewPF == null ? 0 : item.NewPF);


                }

            }

            PayrollDataContext.ChangeMonetories.InsertAllOnSubmit(dbList);
            PayrollDataContext.SubmitChanges();
            return importedEmployee;
        }

        public static void InsertUpdateAddonAmount(int ein, decimal amount, double? pfPercent)
        {

            EFundDetail fund = PayrollDataContext.EFundDetails.FirstOrDefault(x => x.EmployeeId == ein);

            fund.OxfamDesiredContribution = amount;
            fund.OxfamPFPercentage = pfPercent;

            PayrollDataContext.SubmitChanges();


        }
        public static Status InsertUpdateAddonAmount(AddOnDetail tax, int addOnId, int ein)
        {

            Status status = new Status();
            AddOn addon = PayrollDataContext.AddOns.FirstOrDefault(x => x.AddOnId == addOnId);
            if (addon == null)
            {
                status.ErrorMessage = "Add on does not exists.";
                return status;
            }

            if (CommonManager.CompanySetting.WhichCompany != WhichCompany.LRPB)
            {
                if (CalculationManager.IsCalculationSavedForEmployee(addon.PayrollPeriodId.Value, ein))
                {
                    status.ErrorMessage = "Salary already saved for this period, add on amount can not be changed.";
                    return status;
                }
            }

            PayrollPeriod period = CommonManager.GetPayrollPeriod(addon.PayrollPeriodId.Value);

            List<AddOnDetail> dbAddOnList =
                     (
                     from h in PayrollDataContext.AddOnHeaders
                     join d in PayrollDataContext.AddOnDetails on h.AddOnHeaderId equals d.AddOnHeaderId
                     where h.AddOnId == addOnId && d.EmployeeId==ein
                     select d
                     ).ToList();

            DateTime createdOn = GetCurrentDateAndTime();

            List<ChangeMonetory> dbList = new List<ChangeMonetory>();

            //foreach (AddOnDetail tax in list)
            {


                // Imported Log
                //if (dbList.Any(x => x.EmployeeId == tax.EmployeeId) == false)
                //{
                //    dbList.Add(GetMonetoryChangeLog(
                //                        tax.EmployeeId, (byte)MonetoryChangeLogTypeEnum.AddOn, period.Name + " Add On Import", "", "", LogActionEnum.Add)
                //                        );
                //}


                AddOnDetail dbDetail =
                    dbAddOnList.FirstOrDefault(x =>
                        x.AddOnHeader.Type == tax.AddOnHeader.Type
                        && x.AddOnHeader.SourceId == tax.AddOnHeader.SourceId
                        && x.EmployeeId == tax.EmployeeId);

                // save log
                dbList.Add(GetMonetoryChangeLog(
                                    tax.EmployeeId, (byte)MonetoryChangeLogTypeEnum.AddOn, period.Name + " Add On Change",
                                    (dbDetail == null ? "" : dbDetail.Amount.ToString()), tax.Amount, LogActionEnum.Add)
                                    );


                if (dbDetail == null)
                {
                    AddOnHeader header = PayrollDataContext.AddOnHeaders.SingleOrDefault(x => x.AddOnId == addOnId
                        && x.Type == tax.AddOnHeader.Type && x.SourceId == tax.AddOnHeader.SourceId);
                    if (header != null)
                    {
                        PayrollDataContext.AddOnDetails.InsertOnSubmit(
                            new AddOnDetail
                            {
                                Amount = tax.Amount,
                                EmployeeId = tax.EmployeeId,
                                AddOnHeaderId = header.AddOnHeaderId,
                                CreatedOn = createdOn
                            }
                            );
                    }
                }
                else
                {
                    dbDetail.Amount = tax.Amount;
                }

            }



            PayrollDataContext.SubmitChanges();
            return status;
        }


        public static Status InsertUpdatePastRegularAdjustment(int yearId,List<PastRegularIncomeAdjustment> list)
        {

            List<ChangeMonetory> dbList = new List<ChangeMonetory>();

            foreach (PastRegularIncomeAdjustment item in list)
            {


                // Imported Log
                //if (dbList.Any(x => x.EmployeeId == tax.EmployeeId) == false)
                //{
                //    dbList.Add(GetMonetoryChangeLog(
                //                        tax.EmployeeId, (byte)MonetoryChangeLogTypeEnum.AddOn, period.Name + " Add On Import", "", "", LogActionEnum.Add)
                //                        );
                //}


                PastRegularIncomeAdjustment dbDetail =
                    PayrollDataContext.PastRegularIncomeAdjustments.FirstOrDefault(x => x.YearId == item.YearId
                    && x.EmployeeId == item.EmployeeId);


                FinancialDate year = CommonManager.GetFiancialDateById(yearId);
                year.SetName(IsEnglish);

                if (dbDetail == null)
                {
                    // save log
                    dbList.Add(GetMonetoryChangeLog(
                                        item.EmployeeId, (byte)MonetoryChangeLogTypeEnum.PastRegularAdjustment, " for " + year.Name,
                                        "", item.AdjustmentAmount.ToString(), LogActionEnum.Add)
                                        );

                    PayrollDataContext.PastRegularIncomeAdjustments.InsertOnSubmit(item);

                    
                }
                else
                {
                    // save log
                    dbList.Add(GetMonetoryChangeLog(
                                        item.EmployeeId, (byte)MonetoryChangeLogTypeEnum.PastRegularAdjustment, " for " + year.Name,
                                        dbDetail.AdjustmentAmount.ToString(), item.AdjustmentAmount.ToString(), LogActionEnum.Update)
                                        );

                    dbDetail.ExistingAmount = item.ExistingAmount;
                    dbDetail.AdjustmentAmount = item.AdjustmentAmount;
                    dbDetail.Note = item.Note;
                }

            }

            PayrollDataContext.ChangeMonetories.InsertAllOnSubmit(dbList);

            PayrollDataContext.SubmitChanges();
            Status status = new Status();
            return status;
        }

        public static List<TextValue> GetAddOnDateList(int addOnid)
        {
            List<TextValue> list =
                (
                    from a in PayrollDataContext.AddOns
                    join h in PayrollDataContext.AddOnHeaders on a.AddOnId equals h.AddOnId
                    join d in PayrollDataContext.AddOnDetails on h.AddOnHeaderId equals d.AddOnHeaderId
                    where h.Type == 1 && a.AddOnId == addOnid
                    orderby d.CreatedOn
                    select new TextValue
                    {
                        Text = (d.CreatedOn == null ? "" : GetFormatteeDate(d.CreatedOn))
                    }
                    
                ).Distinct().ToList();

            return list;

        }
        public static string GetFormatteeDate(DateTime? date)
        {
            if (date == null)
                return "";
            return date.Value.ToString("yyyy-MMM-dd HH:mm:ss");
        }
        public static void PostOvertimePay(int payrollPeriodId)
        {
            PayrollDataContext.PostOvertimePay(payrollPeriodId);
        }

        public static void DeleteOvertime(int payrollPeriodId, string xml)
        {
            PayrollDataContext.DeleteOvertime(payrollPeriodId, XElement.Parse(xml));
        }


        public static void DeleteIncomeDeductionAdjustment(string xml,string title)
        {
            PayrollDataContext.DeleteIncomeDeductionAdjustment(XElement.Parse(xml)
                ,title,SessionManager.UserName,SessionManager.CurrentCompanyId);
        }
        public static void DeleteFestivalIncomeDeductionAdjustment(List<FestivalIncomeAdjustment> list)
        {

            foreach (var item in list)
            {
                FestivalIncomeAdjustment dbEntity = PayrollDataContext
                    .FestivalIncomeAdjustments.FirstOrDefault(x => x.YearId == item.YearId && x.EmployeeId == item.EmployeeId
                        && x.IncomeDeductionId == item.IncomeDeductionId);
                if (dbEntity != null)
                    PayrollDataContext.FestivalIncomeAdjustments.DeleteOnSubmit(dbEntity);
            }

            PayrollDataContext.SubmitChanges();
        }

        public static void DeletePastRegularIncomeAdjustment(List<PastRegularIncomeAdjustment> list)
        {

            foreach (var item in list)
            {
                FinancialDate year = CommonManager.GetFiancialDateById(item.YearId);
                year.SetName(IsEnglish);

                PastRegularIncomeAdjustment dbEntity = PayrollDataContext
                    .PastRegularIncomeAdjustments.FirstOrDefault(x => x.YearId == item.YearId && x.EmployeeId == item.EmployeeId
                        );
                if (dbEntity != null)
                {
                    PayrollDataContext.PastRegularIncomeAdjustments.DeleteOnSubmit(dbEntity);

                    // save log
                    PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(
                                        item.EmployeeId, (byte)MonetoryChangeLogTypeEnum.PastRegularAdjustment, " for " + year.Name,
                                        dbEntity.AdjustmentAmount.ToString(), "", LogActionEnum.Delete)
                                        );

                }
            }

            PayrollDataContext.SubmitChanges();
        }

        public static void DeletePartialTaxList(List<PartialTaxPaid> list)
        {
            foreach (PartialTaxPaid tax in list)
            {
                PartialTaxPaid dbTax = PayrollDataContext.PartialTaxPaids.SingleOrDefault
                    (x => x.EmployeeId == tax.EmployeeId && x.PayrollPeriodId == tax.PayrollPeriodId);

                if (dbTax != null)
                {
                    PayrollDataContext.PartialTaxPaids.DeleteOnSubmit(dbTax);
                }
            }

            PayrollDataContext.SubmitChanges();
        }

        #endregion

        #region Mass Increment
        /// <summary>
        /// Gives all employee which matches input parameter for bulk increment of salary.
        /// </summary>
        /// <param name="incomeId"></param>
        /// <param name="statusId"></param>
        /// <param name="gradeId"></param>
        /// <param name="designationId"></param>
        /// <param name="companyId"></param>
        /// <returns>Generic list of employees for example: List<GetEmployeeListForMassIncrementResult> </returns>
        public static List<GetEmployeeListForMassIncrementResult> GetEmployeeListForMassIncrementResult(int incomeId, int statusId, int gradeId, int designationId, int companyId, int payrollPeriodId, int currentPage, int pageSize, ref int? total,string sortBy,int departmentId
            ,int filterType,decimal filterAmount,int filterIncomeId,string empSearch,int groupId,int levelId,int filterEventID)
        {
            currentPage -= 1;

            List<GetEmployeeListForMassIncrementResult> list = 
                PayrollDataContext.GetEmployeeListForMassIncrement((int)incomeId, (int)statusId, (int)gradeId, groupId,levelId,
                (int)designationId, (int)companyId, payrollPeriodId,departmentId, (int)currentPage,
                (int)pageSize, sortBy, ref total, filterType, filterAmount, filterIncomeId, empSearch,filterEventID).ToList();

            total = 0;
            if (list.Count > 0)
                total = list[0].TotalRows.Value;

            return list;

        }

        public static List<GetRetrospectIncrementResult>
            GetRetrospectIncrementList(int incomeId,int type, int currentPage, int pageSize, int payrollPeriodId, ref int? total
            ,int empid)
        {
            currentPage -= 1;

            List<GetRetrospectIncrementResult> list =
                PayrollDataContext.GetRetrospectIncrement(incomeId, type, currentPage, pageSize, payrollPeriodId, empid).ToList();

            total = 0;
            if (list.Count > 0)
                total = list[0].TotalRows.Value;

            return list;

        }

        public static List<PIncome> GetIncomeListForMassIncrement(int companyId)
        {
            var query = from i in PayrollDataContext.PIncomes
                        where 
                        (   
                            (i.Calculation == IncomeCalculation.FIXED_AMOUNT //|| i.Calculation == IncomeCalculation.VARIABLE_AMOUNT 
                            || i.Calculation == IncomeCalculation.PERCENT_OF_INCOME
                            )
                            //&& (i.IsGrade == null || i.IsGrade==false)
                            && (i.IsEnabled == null || i.IsEnabled==true)
                        )
                        where (i.CompanyId == companyId)
                        select i;
            return query.ToList();
        }
        public static List<PIncome> GetUnitRateIncomes()
        {
            var query = from i in PayrollDataContext.PIncomes
                        where
                        (
                            i.Calculation == IncomeCalculation.Unit_Rate
                        )
                        orderby i.Title
                        select i;
            return query.ToList();
        }
        public static List<PIncome> GetDeemedIncomeList()
        {
            return
                PayrollDataContext.PIncomes.Where(x => x.Calculation == IncomeCalculation.DEEMED_INCOME
                && (x.CompanyId == SessionManager.CurrentCompanyId))
                .OrderBy(x => x.Title).ToList();
        }
        public static List<PIncome> GetVariableIncomeList(int companyId, bool isDeduction)
        {
            PayrollPeriod payroll = CommonManager.GetLastPayrollPeriod();
            List<PIncome> query = new List<PIncome>();
            if (isDeduction == false)
            {
                if (CommonManager.CompanySetting.IsD2)
                {
                    query = (from i in PayrollDataContext.PIncomes
                             where
                             (
                                (
                                 // for d2 show all along with Fixed for the Jestha 2070 as dollar is be removed
                                     (payroll != null && payroll.Year == 2070 && payroll.Month == 2)                                ||
                                 i.Calculation == IncomeCalculation.VARIABLE_AMOUNT
                                 ||
                                 (i.Calculation == IncomeCalculation.FESTIVAL_ALLOWANCE 
                                    && i.IsVariableAmount != null && i.IsVariableAmount.Value)
                                )


                                 && (i.IsGrade == null || i.IsGrade == false)
                             )
                             where (i.CompanyId == companyId) && i.IsEnabled == true
                             orderby i.Title
                             select i).ToList();
                }
                else
                {
                    List<int> childrenWalfareMegaLikeIncomesList = 
                        PayrollDataContext.MG_AllowanceSettings.Select(x=>x.IncomeId).ToList();

                    query = (from i in PayrollDataContext.PIncomes
                             where
                             (
                                (                                
                                 i.Calculation == IncomeCalculation.VARIABLE_AMOUNT
                                 //||
                                 //i.Calculation==IncomeCalculation.FIXED_AMOUNT
                                 ||
                                 (i.Calculation == IncomeCalculation.FESTIVAL_ALLOWANCE && i.IsVariableAmount != null && i.IsVariableAmount.Value)
                                )

                                // Remove Children allowance like case of Mega in variable list
                                && childrenWalfareMegaLikeIncomesList.Contains(i.IncomeId)==false
                                 && (i.IsGrade == null || i.IsGrade == false)
                             )
                             where (i.CompanyId == companyId) && i.IsEnabled == true
                             orderby i.Title
                             select i).ToList();
                }

                return query.ToList();
            }
            else
            {
                var deds = from i in PayrollDataContext.PDeductions
                           where
                           (
                              i.Calculation == IncomeCalculation.VARIABLE_AMOUNT

                           )
                           where (i.CompanyId == companyId)
                           orderby i.Title
                           select i;

                List<PIncome> incomes = new List<PIncome>();
                foreach (PDeduction ded in deds)
                {
                    PIncome inc = new PIncome { IncomeId = ded.DeductionId, Title = ded.Title };
                    incomes.Add(inc);
                }

                return incomes;
            }
        }

        public static List<PIncome> GetIncomeListForSalaryExportImport(int companyId, bool isDeduction, bool ShowFixedIncomeAlso)
        {
            PayrollPeriod payroll = CommonManager.GetLastPayrollPeriod();
            List<PIncome> query = new List<PIncome>();
            if (isDeduction == false)
            {
                if (CommonManager.CompanySetting.HasLevelGradeSalary && ShowFixedIncomeAlso)
                {
                    query = (from i in PayrollDataContext.PIncomes
                             where
                             (
                                (
                                 i.Calculation == IncomeCalculation.VARIABLE_AMOUNT
                                 ||
                                 i.Calculation == IncomeCalculation.Unit_Rate
                                 ||
                                 ( i.Calculation == IncomeCalculation.FIXED_AMOUNT && (i.IsDefinedType == null || i.IsDefinedType==false))
                                 ||
                                 (i.Calculation == IncomeCalculation.FESTIVAL_ALLOWANCE && i.IsVariableAmount != null && i.IsVariableAmount.Value)
                                )
                                 && (i.IsGrade == null || i.IsGrade == false)
                             )
                             where (i.CompanyId == companyId) && i.IsEnabled == true
                             orderby i.Title
                             select i).ToList();
                }
                else if (CommonManager.CompanySetting.HasLevelGradeSalary)
                {
                    query = (from i in PayrollDataContext.PIncomes
                             where
                             (
                                (
                                 i.Calculation == IncomeCalculation.VARIABLE_AMOUNT
                                    ||
                                 i.Calculation == IncomeCalculation.Unit_Rate
                                 ||
                                 (i.Calculation == IncomeCalculation.FESTIVAL_ALLOWANCE && i.IsVariableAmount != null && i.IsVariableAmount.Value)
                                )

                                 && (i.IsGrade == null || i.IsGrade == false)
                             )
                             where (i.CompanyId == companyId) && i.IsEnabled == true
                             orderby i.Title
                             select i).ToList();
                }
                else
                {
                    //List<int> childrenWalfareMegaLikeIncomesList =
                    //    PayrollDataContext.MG_AllowanceSettings.Select(x => x.IncomeId).ToList();

                    if (ShowFixedIncomeAlso)
                    {
                        query = (from i in PayrollDataContext.PIncomes
                                 where
                                 (
                                    (
                                     i.Calculation == IncomeCalculation.VARIABLE_AMOUNT
                                     ||
                                     i.Calculation == IncomeCalculation.FIXED_AMOUNT
                                     ||
                                     i.Calculation == IncomeCalculation.Unit_Rate
                                    ||
                                     (i.Calculation == IncomeCalculation.FESTIVAL_ALLOWANCE && i.IsVariableAmount != null && i.IsVariableAmount.Value)
                                    )
                                     && (i.IsGrade == null || i.IsGrade == false)
                                 )
                                 where (i.CompanyId == companyId) && i.IsEnabled == true
                                 orderby i.Title
                                 select i).ToList();
                    }
                    else
                    {
                        query = (from i in PayrollDataContext.PIncomes
                                 where
                                 (
                                    (
                                     i.Calculation == IncomeCalculation.VARIABLE_AMOUNT
                                     ||
                                     i.Calculation == IncomeCalculation.Unit_Rate
                                 ||
                                     (i.Calculation == IncomeCalculation.FESTIVAL_ALLOWANCE && i.IsVariableAmount != null && i.IsVariableAmount.Value)
                                    )
                                     && (i.IsGrade == null || i.IsGrade == false)
                                 )
                                 where (i.CompanyId == companyId) && i.IsEnabled == true
                                 orderby i.Title
                                 select i).ToList();
                    }
                }

                return query.ToList();
            }
            else
            {
                var deds = from i in PayrollDataContext.PDeductions
                           where
                           (
                              i.Calculation == IncomeCalculation.VARIABLE_AMOUNT
                              ||
                              i.Calculation == IncomeCalculation.FIXED_AMOUNT

                           )
                           where (i.CompanyId == companyId)
                           orderby i.Title
                           select i;

                List<PIncome> incomes = new List<PIncome>();
                foreach (PDeduction ded in deds)
                {
                    PIncome inc = new PIncome { IncomeId = ded.DeductionId, Title = ded.Title };
                    incomes.Add(inc);
                }

                return incomes;
            }
        }
        public static string GetIncomeTypeByIncomeId(int incomeId)
        {
            var result = from i in PayrollDataContext.PIncomes
                         where i.IncomeId == incomeId
                         select i.Calculation;
            if (result == null || result.Count() == 0)
                return string.Empty;
            else
                return result.FirstOrDefault().ToString();
        }
        #endregion


        #region "Nset CIT For First year"



        public static List<Nset_GetEmployeeListForCITResult> GetNsetCITList()
        {
            return PayrollDataContext.Nset_GetEmployeeListForCIT(SessionManager.CurrentCompanyId).ToList();
        }

        public static void SaveNsetCIT(string xml)
        {
            PayrollDataContext.Nset_SaveCIT(XElement.Parse(xml));
        }

        public static void DeleteNsetCIT(int empId)
        {
            NsetCITForFirstYear entity = PayrollDataContext.NsetCITForFirstYears.SingleOrDefault(c => c.EmployeeId == empId);
            if (entity != null)
            {
                PayrollDataContext.NsetCITForFirstYears.DeleteOnSubmit(entity);
                DeleteChangeSet();
            }
        }
        #endregion

        public static bool CheckIfMultipleGradeTypeDefined(PEmployeeIncome empIncome)
        {
            if (empIncome.PIncome.IsGrade.HasValue && empIncome.PIncome.IsGrade.Value)
            {

                int incomeId = PayrollDataContext.PEmployeeIncomes.SingleOrDefault(x => x.EmployeeIncomeId == empIncome.EmployeeIncomeId)
                    .PIncome.IncomeId;

                PIncome dbSavedGrade = PayrollDataContext.PIncomes
                    .SingleOrDefault(p => p.CompanyId == SessionManager.CurrentCompanyId && p.IsGrade == true);

                if (dbSavedGrade != null && dbSavedGrade.IncomeId != incomeId)
                    return true;    
            }
            return false;
        }

        public static bool CheckIfMultipleGradeTypeDefined(PIncome income)
        {
            if (income.IsGrade.HasValue && income.IsGrade.Value)
            {

              

                PIncome dbSavedGrade = PayrollDataContext.PIncomes
                    .SingleOrDefault(p => p.CompanyId == SessionManager.CurrentCompanyId && p.IsGrade == true);

                if (dbSavedGrade != null && dbSavedGrade.IncomeId != income.IncomeId)
                    return true;
            }
            return false;
        }

#region "Position Grade And Step"

        public static List<Position> GetAllPosition()
        {
            return
                PayrollDataContext.Positions.Where(p => p.CompanyId == SessionManager.CurrentCompanyId)
                .OrderBy(p => p.Order).ToList();
        }
        public static List<Grade> GetAllGrade()
        {
            return
                PayrollDataContext.Grades.Where(p => p.CompanyId == SessionManager.CurrentCompanyId)
                .OrderBy(p => p.Order).ToList();
        }
        public static List<Step> GetAllStep()
        {
            List<Step> steps = 
                PayrollDataContext.Steps.Where(p => p.CompanyId == SessionManager.CurrentCompanyId)
                .OrderBy(p => p.Order).ToList();

            Step defaultStep = new Step();
            defaultStep.StepId = 0;
            defaultStep.Name = "0";
            steps.Insert(0, defaultStep);

            return steps; 
        }

        //public static List<PositionGradeStepAmount> GetGradeStepAmount(int positionId)
        //{
        //    return PayrollDataContext.PositionGradeStepAmounts.Where(p => p.PositionId == positionId)
        //        .ToList();
        //}

        public static bool SaveUpdateGradeStepAmount(List<PositionGradeStepAmount> amounts)
        {

            foreach (PositionGradeStepAmount entity in amounts)
            {
                PositionGradeStepAmount dbEntity = PayrollDataContext.PositionGradeStepAmounts
                    .Where(a => a.PositionId == entity.PositionId && a.GradeId == entity.GradeId && a.StepId == entity.StepId).SingleOrDefault();

                if (dbEntity != null)
                    dbEntity.Amount = entity.Amount;
                else
                    PayrollDataContext.PositionGradeStepAmounts.InsertOnSubmit(entity);
            }


            PayrollDataContext.SubmitChanges();
            return true;
        }

        public static List<PositionGradeStepAmount> GetGradeStepAmounts(int positionId)
        {
            return PayrollDataContext.PositionGradeStepAmounts.Where(p => p.PositionId == positionId).ToList();
        }

#endregion

        public static List<AdjustArrearIncrement> GetAdjustArrearIncrementList(int incomeId, int type, int payrollPeriodId)
        {
            List<AdjustArrearIncrement> list = new List<AdjustArrearIncrement>();

            list = (from ri in PayrollDataContext.RetrospectIncrements
                    join ee in PayrollDataContext.EEmployees on ri.EmployeeId equals ee.EmployeeId
                    join d in PayrollDataContext.EDesignations on ee.DesignationId equals d.DesignationId
                    
                    //join ei in PayrollDataContext.PEmployeeIncomes on new { EmployeeId = ri.EmployeeId, IncId = incomeId } equals new { EmployeeId = ei.EmployeeId, IncId = ei.IncomeId }
                    
                    where ri.IncomeId == incomeId && ri.Type == type && ri.PayrollPeriodId == payrollPeriodId
                    select new AdjustArrearIncrement
                    {
                        EmployeeId = ri.EmployeeId,
                        Name = ee.Name,
                        Designation = d.Name,
                        Status = JobStatus.GetStatusForDisplay(EmployeeManager.GetEmployeeCurrentStatus(ri.EmployeeId)),
                        TotalIncrease = ri.AdjustedAmount.Value,
                        TotalPF= ri.AdjustedPFAmount.Value,
                       CurrentSalary = ri.CurrentAmount == null ? 0 : ri.CurrentAmount.Value,
                       NewSalary = ri.IncreasedAmount == null ? 0 : ri.IncreasedAmount.Value,
                       OldAmount = ri.OldAmount == null ? 0 : ri.OldAmount.Value,
                       NewAmount = ri.NewAmount == null ? 0 :ri.NewAmount.Value

                    }
                   ).ToList();

            return list;
        }

        public static Status UpdateRetrospectIncrement(List<AdjustArrearIncrement> list, int payrollPeriodId, int incomeId, int type, ref int importedCount)
        {
            Status status = new Status();
            importedCount = 0;
            int employeeId = 0;

            foreach (AdjustArrearIncrement obj in list)
            {
                employeeId = obj.EmployeeId;

                importedCount++;

                RetrospectIncrement dbRetrospectIncrement = PayrollDataContext.RetrospectIncrements.SingleOrDefault(x => x.EmployeeId == obj.EmployeeId && x.PayrollPeriodId == payrollPeriodId
                    && x.IncomeId == incomeId && x.Type == type);

                if (dbRetrospectIncrement != null)
                {
                    dbRetrospectIncrement.AdjustedAmount = obj.TotalIncrease;
                    dbRetrospectIncrement.AdjustedPFAmount = obj.TotalPF;
                }
                else
                {
                    status.ErrorMessage = "RetrospectIncrement is not found for employee - " + EmployeeManager.GetEmployeeById(employeeId).Name;
                    status.IsSuccess = false;
                }
            }
            PayrollDataContext.SubmitChanges();

            status.IsSuccess = true;
            return status;
        }

        public static void ChangeIncomeOrder(int sourceId, int destId, string mode)
        {
            PIncome dbSource = PayrollDataContext.PIncomes.SingleOrDefault(x => x.IncomeId == sourceId);
            PIncome dbDest = PayrollDataContext.PIncomes.SingleOrDefault(x => x.IncomeId == destId);
            bool isDropBefore = mode.ToLower().Trim().Contains("before") ? true : false;
            int? order = null;
            if (isDropBefore)
            {

                // now change order of other elements after dest
                List<PIncome> otherList = PayrollDataContext.PIncomes
                    .Where(x => x.Order > dbDest.Order).OrderBy(x => x.Order).ToList();

                order = dbDest.Order;
                dbSource.Order = dbDest.Order;
                dbDest.Order = ++order;

                foreach (PIncome item in otherList)
                {
                    if (item.IncomeId != dbSource.IncomeId)
                        item.Order = ++order;
                }


            }
            else
            {
                // now change order of other elements after dest
                List<PIncome> otherList = PayrollDataContext.PIncomes
                    .Where(x => x.Order > dbDest.Order).OrderBy(x => x.Order).ToList();

                order = dbDest.Order;
                dbSource.Order = ++order;

                foreach (PIncome item in otherList)
                {
                    if (item.IncomeId != dbSource.IncomeId)
                        item.Order = ++order;
                }
            }

            PayrollDataContext.SubmitChanges();

            order = 1;
            List<PIncome> allList = PayrollDataContext.PIncomes.OrderBy(x => x.Order).ToList();
            foreach (PIncome item in allList)
            {
                item.Order = order++;
            }

            PayrollDataContext.SubmitChanges();
        }

        public static Status EnableDisablePIncome(int incomeId)
        {
            Status status = new Status();
            PIncome dbEntity = PayrollDataContext.PIncomes.SingleOrDefault(
                    c => (c.IncomeId == incomeId));

            if (dbEntity == null)
            {
                status.ErrorMessage = "Income not found.";
                status.IsSuccess = false;
                return status;
            }

            if (dbEntity.IsEnabled == true)
                status.ErrorMessage = "disabled";
            else
                status.ErrorMessage = "enabled";

            dbEntity.IsEnabled = !dbEntity.IsEnabled;
            PayrollDataContext.SubmitChanges();
            status.IsSuccess = true;
           
            return status;
        }

        public static void ChangeDeductionOrder(int sourceId, int destId, string mode)
        {
            PDeduction dbSource = PayrollDataContext.PDeductions.SingleOrDefault(x => x.DeductionId == sourceId);
            PDeduction dbDest = PayrollDataContext.PDeductions.SingleOrDefault(x => x.DeductionId == destId);
            bool isDropBefore = mode.ToLower().Trim().Contains("before") ? true : false;
            int? order = null;
            if (isDropBefore)
            {

                // now change order of other elements after dest
                List<PDeduction> otherList = PayrollDataContext.PDeductions
                    .Where(x => x.Order > dbDest.Order).OrderBy(x => x.Order).ToList();

                order = dbDest.Order;
                dbSource.Order = dbDest.Order;
                dbDest.Order = ++order;

                foreach (PDeduction item in otherList)
                {
                    if (item.DeductionId != dbSource.DeductionId)
                        item.Order = ++order;
                }


            }
            else
            {
                // now change order of other elements after dest
                List<PDeduction> otherList = PayrollDataContext.PDeductions
                    .Where(x => x.Order > dbDest.Order).OrderBy(x => x.Order).ToList();

                order = dbDest.Order;
                dbSource.Order = ++order;

                foreach (PDeduction item in otherList)
                {
                    if (item.DeductionId != dbSource.DeductionId)
                        item.Order = ++order;
                }
            }

            PayrollDataContext.SubmitChanges();

            order = 1;
            List<PDeduction> allList = PayrollDataContext.PDeductions.OrderBy(x => x.Order).ToList();
            foreach (PDeduction item in allList)
            {
                item.Order = order++;
            }

            PayrollDataContext.SubmitChanges();
        }

        public static Status EnableDisablePDeduction(int deductionId)
        {
            Status status = new Status();
            PDeduction dbEntity = PayrollDataContext.PDeductions.SingleOrDefault(
                    c => (c.DeductionId == deductionId));

            if (dbEntity == null)
            {
                status.ErrorMessage = "Deduction not found.";
                status.IsSuccess = false;
                return status;
            }

            if (dbEntity.IsEnabled == true)
                status.ErrorMessage = "disabled";
            else
                status.ErrorMessage = "enabled";

            dbEntity.IsEnabled = !dbEntity.IsEnabled;
            PayrollDataContext.SubmitChanges();
            status.IsSuccess = true;

            return status;
        }
    }

    public class AdjustArrearIncrement
    {
        public int EmployeeId { get; set; }
        public string Name { get; set; }
        public string Designation { get; set; }
        public string Status { get; set; }
        public decimal CurrentSalary { get; set; }
        public decimal NewSalary { get; set; }
        public decimal TotalIncrease { get; set; }
        public decimal TotalPF { get; set; }
        public decimal OldAmount { get; set; }
        public decimal NewAmount { get; set; }
    }
}
