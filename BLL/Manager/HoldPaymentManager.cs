﻿using System.Collections.Generic;
using System.Linq;
using DAL;
using Utils.Calendar;
using System;

namespace BLL.Manager
{
    public class HoldPaymentManager : BaseBiz
    {

        public InsertUpdateStatus ValidateNotUsedStopPayment(StopPayment entity)
        {
            InsertUpdateStatus status = new InsertUpdateStatus();
            status.IsSuccess = true;
            //Check validation
            
            // Full month stop payment can not be done if there is Advance adjustment in any paryoll period month
            //if (dbEntity == null && loan.PayrollPeriodId != 0)
            {
              //  PayrollPeriod payrollPeriod = CommonManager.GetPayrollPeriod(loan.PayrollPeriodId);
                //if (PayrollDataContext.AdvanceAdjustments.Any(x => x.EmployeeId == entity.EmployeeId &&
                //    payrollPeriod.StartDateEng >= x.EngFromDate && payrollPeriod.EndDateEng <= x.EngToDate))
                List<PayrollPeriod> periods =
                (
                from a in PayrollDataContext.AdvanceAdjustments
                join p in PayrollDataContext.PayrollPeriods on a.PayrollPeriodId equals p.PayrollPeriodId
                where a.EmployeeId == entity.EmployeeId &&
                 p.StartDateEng >= entity.EngFromDate && p.EndDateEng <= entity.EngToDate
                select p
                ).ToList();

                if (periods.Count > 0)
                {
                    status.ErrorMessage =
                        string.Format("Advance adjustment exists for this stop payment period, for the month of {0}, can not save this stop payment.",
                        periods[0].Name);
                    status.IsSuccess = false;
                    return status;
                }
            }
            
            //1. could not be before salary saved Payroll Period Date
            PayrollPeriod payrollPeriod = CommonManager.GetValidLastPayrollPeriod(entity.EmployeeId.Value);
            if (payrollPeriod != null)
            {
                bool isCalculationSaved =
                    CalculationManager.IsCalculationSavedForEmployee(payrollPeriod.PayrollPeriodId, entity.EmployeeId.Value);

                if (isCalculationSaved)
                {
                    if (entity.EngFromDate < payrollPeriod.EndDateEng)
                    {
                        status.ErrorMessage = "Start date can not be before the salary saved Payroll Period.";
                        status.IsSuccess = false;
                        return status;
                    }
                }
                else
                {
                    if (entity.EngToDate < (payrollPeriod.StartDateEng.Value.AddDays(-1)))
                    {
                        status.ErrorMessage = "Date can not be before the salary saved Payroll Period.";
                        status.IsSuccess = false;
                        return status;
                    }
                }
            }


            //2. can not be within the same month
            CustomDate from = CustomDate.GetCustomDateFromString(entity.FromDate, IsEnglish);
            CustomDate to = CustomDate.GetCustomDateFromString(entity.ToDate, IsEnglish);
            int totalDays =  DateHelper.GetTotalDaysInTheMonth(to.Year, to.Month, IsEnglish);
            // for same month, start date should be the first day & end date should be the last day of the month
            if (from.Month == to.Month && from.Year == to.Year
                && (from.Day != 1 || to.Day != totalDays))
            {
                status.ErrorMessage = "If Start and End date are in the same month then start day should be the first day & end date should be the last day of the month.";
                status.IsSuccess = false;
                return status;
            }

            //3. New Stop payment start date should not lie in final salary saved payroll period
            PayrollPeriod finalSavedPayrollPeriod = 
                (
                    from p in PayrollDataContext.PayrollPeriods
                    join c in PayrollDataContext.CCalculations on p.PayrollPeriodId equals c.PayrollPeriodId
                    where c.IsFinalSaved == true && entity.EngToDate < p.EndDateEng
                        && p.CompanyId== SessionManager.CurrentCompanyId
                    select p
                ).FirstOrDefault();
            //for new Stop Payment only
            if (finalSavedPayrollPeriod != null && entity.StopPaymentId==0)
            {
                status.ErrorMessage = "Start date should not lie in already final salary saved payroll period.";
                status.IsSuccess = false;
                return status;
            }

            //4. New Stop payment start date should not lie in initial salary saved payroll period for this employee
            PayrollPeriod savedPayrollPeriod =
                (
                    from p in PayrollDataContext.PayrollPeriods
                    join c in PayrollDataContext.CCalculations on p.PayrollPeriodId equals c.PayrollPeriodId
                    join ce in PayrollDataContext.CCalculationEmployees on
                        new { c.CalculationId, entity.EmployeeId } equals new { ce.CalculationId, ce.EmployeeId }

                    where entity.EngFromDate <= p.EndDateEng && p.CompanyId == SessionManager.CurrentCompanyId
                    select p
                ).FirstOrDefault();
            //for new Stop Payment only
            if (savedPayrollPeriod != null && entity.StopPaymentId == 0)
            {
                status.ErrorMessage = "Start date should not lie in already salary saved payroll period.";
                status.IsSuccess = false;
                return status;
            }

            //get before stop payment
            StopPayment lastStopPayment = PayrollDataContext.StopPayments
                .Where(x => x.EmployeeId == entity.EmployeeId
                 && (x.StopPaymentId == 0 || x.StopPaymentId != entity.StopPaymentId))
                .OrderByDescending(x => x.StopPaymentId)
                .Take(1).SingleOrDefault();

            if (lastStopPayment != null)
            {
                from = CustomDate.GetCustomDateFromString(lastStopPayment.ToDate, IsEnglish);
                to = CustomDate.GetCustomDateFromString(entity.FromDate, IsEnglish);


                if (lastStopPayment.EngToDate >= entity.EngFromDate
                 || (from.Month == to.Month && from.Year == to.Year))
                {
                    status.ErrorMessage = "Last Stop Payment date can not intercept current stop payment.";
                    status.IsSuccess = false;
                    return status;
                }
            }

            return status;

        }

        public InsertUpdateStatus IsValidHoldPayment(HoldPayment entity)
        {
            InsertUpdateStatus status = new InsertUpdateStatus();
            if( 
                (from ce in PayrollDataContext.CCalculationEmployees
                join c in PayrollDataContext.CCalculations on ce.CalculationId equals c.CalculationId
                where ce.EmployeeId==entity.EmployeeId && c.PayrollPeriodId==entity.PayrollPeriodId
                select ce
                ).Any())
            {
                status.ErrorMessage = "Salary already saved for this period, so can not create hold payment.";
                status.IsSuccess = false;
                return status;
            }


            PayrollPeriod payrollPeriod = CommonManager.GetPayrollPeriod(entity.PayrollPeriodId.Value);
            if (
                (
                from sp in PayrollDataContext.StopPayments
                where sp.EmployeeId == entity.EmployeeId &&
               (
                (payrollPeriod.StartDateEng >= sp.EngFromDate && payrollPeriod.StartDateEng <= sp.EngToDate)
                ||
                (payrollPeriod.EndDateEng >= sp.EngFromDate && payrollPeriod.EndDateEng <= sp.EngToDate)
                )
                select sp
                ).Any())
            {
                status.ErrorMessage = "Stop payment already exists for period so can not create hold payment.";
                status.IsSuccess = false;
                return status;
            }
                

            return status;
        }

        public static Status SaveUpdate(PastRetirementAdjustment entity)
        {
            PastRetirementAdjustment dbEntity = PayrollDataContext.PastRetirementAdjustments.FirstOrDefault
                (x => x.EmployeeId == entity.EmployeeId);

            if (dbEntity == null)
            {
                entity.CreatedBy = SessionManager.CurrentLoggedInUserID;
                entity.CreatedOn = DateTime.Now;
                PayrollDataContext.PastRetirementAdjustments.InsertOnSubmit(entity);

            }
            else
            {
                dbEntity.Notes = entity.Notes;
                dbEntity.CreatedBy = SessionManager.CurrentLoggedInUserID;
                dbEntity.CreatedOn = DateTime.Now;
                dbEntity.PayrollPeriodId = entity.PayrollPeriodId;
            }

            PayrollDataContext.SubmitChanges();
            return new Status();
        }

        public InsertUpdateStatus Save(HoldPayment entity)
        {

            InsertUpdateStatus status = new InsertUpdateStatus();// ValidateNotUsedStopPayment(entity);

            entity.IsReleased = false;
            entity.CreatedDate = DateTime.Now;


            if (PayrollDataContext.HoldPayments.Where(x => x.EmployeeId == entity.EmployeeId && x.PayrollPeriodId == entity.PayrollPeriodId).Any())
            {
                status.ErrorMessage = "Hold payment already exists for this employee in this period.";
                return status;
            }

            status = IsValidHoldPayment(entity);

            if (!status.IsSuccess)
                return status;

            PayrollDataContext.HoldPayments.InsertOnSubmit(entity);
            status.IsSuccess = SaveChangeSet();

            return status;
        }


        public bool IsHoldPaymentDeletable(int holdPaymentId)
        {
            HoldPayment entity = GetHoldPaymentById(holdPaymentId);
            if (entity == null)
                return true;

            //4. New Stop payment start date should not lie in initial salary saved payroll period for this employee
            PayrollPeriod savedPayrollPeriod = CommonManager.GetLastPayrollPeriod();

            return savedPayrollPeriod.PayrollPeriodId == entity.PayrollPeriodId;
        }

        public bool IsStopPaymentAlreadyUsed(int stopPaymentId)
        {
            HoldPayment entity = GetHoldPaymentById(stopPaymentId);
            if (entity == null)
                return true;
            PayrollPeriod payrollPeriod = CommonManager.GetLastPayrollPeriod();
            return payrollPeriod.PayrollPeriodId != entity.PayrollPeriodId;
        }


        public static List<PastRetirementAdjustment> GetPastAdjustmentList()
        {
            List<PastRetirementAdjustment> list = PayrollDataContext.PastRetirementAdjustments.ToList();

            foreach (PastRetirementAdjustment item in list)
            {
                item.Name = EmployeeManager.GetEmployeeName(item.EmployeeId);
                item.PeriodName = CommonManager.GetPayrollPeriod(item.PayrollPeriodId).Name;
            }

            return list;
        }

        public List<GetHoldPaymentsResult> GetHoldPayments(bool currentMonth, string EmpName, int payPerFrom, int payPerTo, int currentPage, int pageSize, ref int totalRecords)
        {
            string customRoleDeparmentList = SessionManager.CustomRoleDeparmentIDList;

            //DateTime? fromDate = null;
            //if (!showArchieve)
            //{
            //    // Get last final saved payroll period
            //    PayrollPeriod lastPayroll = CommonManager.GetLastPayrollPeriod();
            //    if (lastPayroll != null)
            //    {
            //        bool isPayrollFinalSaved = CommonManager.IsPayrollFinalSaved(lastPayroll.PayrollPeriodId);
            //        if (isPayrollFinalSaved)
            //            fromDate = lastPayroll.EndDateEng.Value.AddDays(1);
            //        else
            //            fromDate = lastPayroll.StartDateEng.Value;
            //    }
            //}

            int payrollPeriodId = CommonManager.GetLastPayrollPeriod().PayrollPeriodId;

            List<GetHoldPaymentsResult> lst = new List<GetHoldPaymentsResult>();

            lst = PayrollDataContext.GetHoldPayments(SessionManager.CurrentCompanyId, customRoleDeparmentList, null,
                payrollPeriodId, currentMonth, string.IsNullOrEmpty(EmpName) ? null : EmpName, payPerFrom, payPerTo, currentPage, pageSize
                ).OrderByDescending(x => x.HoldPaymentId).ToList();
            if (lst.Count > 0)
                totalRecords = lst[0].TotalRows.Value;
            else
                totalRecords = 0;
            return lst;
        }


        public InsertUpdateStatus ReleaseHoldPaymentInCurrentMonth(int holdPaymentId)
        {
            InsertUpdateStatus status = new InsertUpdateStatus();

            HoldPayment dbHoldPayment = GetHoldPaymentById(holdPaymentId);

            if (dbHoldPayment.IsReleased != null && dbHoldPayment.IsReleased == true)
            {
                status.ErrorMessage = "Released hold payment can not be changed.";
                status.IsSuccess = false;
                return status;
            }

            PayrollPeriod lastPayrollPeriod = CommonManager.GetLastPayrollPeriod();

            dbHoldPayment.ReleasedPayrollPeriodId = lastPayrollPeriod.PayrollPeriodId;

            SaveChangeSet();

            return status;
        }

        public InsertUpdateStatus DeleteTheReleaseOfHoldPaymentInCurrentMonth(int holdPaymentId)
        {
            InsertUpdateStatus status = new InsertUpdateStatus();

            HoldPayment dbHoldPayment = GetHoldPaymentById(holdPaymentId);

            if (dbHoldPayment.IsReleased != null && dbHoldPayment.IsReleased == true)
            {
                status.ErrorMessage = "Released hold payment can not be changed.";
                status.IsSuccess = false;
                return status;
            }

            dbHoldPayment.IsReleased = false;
            dbHoldPayment.ReleasedPayrollPeriodId = null;

            SaveChangeSet();

            return status;
        }

        public HoldPayment GetHoldPaymentById(int stopPaymentId)
        {
            return PayrollDataContext.HoldPayments.SingleOrDefault(e => e.HoldPaymentId == stopPaymentId);
        }

        public bool Delete(int holdPaymentId)
        {
            HoldPayment holdPayment = GetHoldPaymentById(holdPaymentId);
            if (holdPayment != null)
            {
                List<HoldPaymentDetail> details =
                    (
                    from hd in PayrollDataContext.HoldPaymentDetails
                    join h in PayrollDataContext.HoldPayments on hd.HoldPaymentId equals h.HoldPaymentId
                    where h.EmployeeId == holdPayment.EmployeeId && h.PayrollPeriodId == holdPayment.PayrollPeriodId
                    select hd

                    ).ToList();

                PayrollDataContext.HoldPaymentDetails.DeleteAllOnSubmit(details);
                PayrollDataContext.HoldPayments.DeleteOnSubmit(holdPayment);
                return DeleteChangeSet();
            }
            else
                return false;
        }
        public static bool DeletePastAdj(int empId)
        {
            PastRetirementAdjustment db = PayrollDataContext.PastRetirementAdjustments.FirstOrDefault(x => x.EmployeeId == empId);
            if (db != null)
                PayrollDataContext.PastRetirementAdjustments.DeleteOnSubmit(db);
            PayrollDataContext.SubmitChanges();
            return true;
        }

        //public bool IsStopPaymentAlreadExists(int employeeId)
        //{
        //    StopPayment entity = PayrollDataContext.StopPayments.SingleOrDefault
        //        (e => (e.EmployeeId == employeeId && e.ToDate == null));
        //    return entity != null;
        //}

        //public bool UpdateToDate(int stopPaymentId, CustomDate toDate)
        //{
        //    StopPayment dbEntity = PayrollDataContext.StopPayments.SingleOrDefault
        //       (e => e.StopPaymentId == stopPaymentId);

        //    dbEntity.ToDate = toDate.ToString();
        //    dbEntity.EngToDate = BizHelper.GetConvertedToEngDate(toDate);


        //    return UpdateChangeSet();
        //}

        public InsertUpdateStatus Update(HoldPayment entity)
        {
            HoldPayment dbEntity = PayrollDataContext.HoldPayments.SingleOrDefault
                (e => e.HoldPaymentId == entity.HoldPaymentId);

            InsertUpdateStatus status = new InsertUpdateStatus();

            status = IsValidHoldPayment(entity);
            if (status.IsSuccess == false)
                return status;

            status.IsSuccess = true;

            if (dbEntity == null)
                return status;


            bool isStopPaymentUsed = IsStopPaymentAlreadyUsed(entity.HoldPaymentId);

          
            dbEntity.EmployeeId = entity.EmployeeId;
          

            dbEntity.Notes = entity.Notes;


            UpdateChangeSet();
            return status;
        }

        public InsertUpdateStatus SaveEmployeesHoldPayment(List<int> employeeIds)
        {

            InsertUpdateStatus status = new InsertUpdateStatus();// ValidateNotUsedStopPayment(entity);

            List<HoldPayment> holdPaymentInsertList = new List<HoldPayment>();
            PayrollPeriod currentPeriod = CommonManager.GetLastPayrollPeriod();
            int payrollPeriodId = currentPeriod.PayrollPeriodId;
            DateTime dateTimeNow = DateTime.Now;

            foreach (var employeeId in employeeIds)
            {
                HoldPayment entity = new HoldPayment();
                entity.EmployeeId = employeeId;
                entity.PayrollPeriodId = payrollPeriodId;
                entity.IsReleased = false;
                entity.CreatedDate = dateTimeNow;

                EEmployee emp = EmployeeManager.GetEmployeeById(employeeId);
                if (PayrollDataContext.HoldPayments.Where(x => x.EmployeeId == entity.EmployeeId && x.PayrollPeriodId == entity.PayrollPeriodId).Any())
                {
                    status.IsSuccess = false;
                    status.ErrorMessage =  string.Format("Hold payment already exists in this period for {0} (EIN-{1}).", emp.Name, emp.EmployeeId);
                    return status;
                }

                status = IsValidHoldPayment(entity);

                if (!status.IsSuccess)
                {
                    status.IsSuccess = false;
                    status.ErrorMessage = status.ErrorMessage.TrimEnd('.') + string.Format(", for {0} (EIN-{1}).", emp.Name, emp.EmployeeId);
                    return status;

                }

                holdPaymentInsertList.Add(entity);
            }


            PayrollDataContext.HoldPayments.InsertAllOnSubmit(holdPaymentInsertList);

            status.IsSuccess = SaveChangeSet();

            return status;
        }

    }
}