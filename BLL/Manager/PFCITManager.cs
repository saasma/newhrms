﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using DAL;
using Utils;
using System.Xml.Linq;
using Utils.Calendar;
using BLL.Entity;
using BLL.BO;
using Utils.Base;

namespace BLL.Manager
{

    public class PFCITManager : BaseBiz
    {
        public static List<GetEmployeeForPFCITResult> GetEmployeeForPFCIT(string EmpName)
        {
            List<GetEmployeeForPFCITResult> list = PayrollDataContext.GetEmployeeForPFCIT(SessionManager.CurrentCompanyId, EmpName).ToList();
            return list;
        }


        public static List<GetPFCITBalanceReportHeaderResult> GetPFHeader()
        {
            List<GetPFCITBalanceReportHeaderResult> origList = new List<GetPFCITBalanceReportHeaderResult>();
            List<GetPFCITBalanceReportHeaderResult> thisList = new List<GetPFCITBalanceReportHeaderResult>();
            GetPFCITBalanceReportHeaderResult instance = new GetPFCITBalanceReportHeaderResult();
            origList = PayrollDataContext.GetPFCITBalanceReportHeader(SessionManager.CurrentCompanyId).ToList();

            string year;
            string year1Full;
            string year2Full;
            foreach (var val1 in origList)
            {

                //year =  +"/"+ (int.Parse(val1.HeaderName.Split('/').FirstOrDefault()) + 1).ToString();

                year1Full = val1.HeaderName.Split('/').FirstOrDefault();
                year2Full = (int.Parse(val1.HeaderName.Split('/').FirstOrDefault()) + 1).ToString();

                if (year1Full.Length == 4 && year2Full.Length == 4)
                {
                    year = year1Full.Substring(year1Full.Length - 2) + "/" + year2Full.Substring(year2Full.Length - 2);
                }
                else 
                    year = year1Full+ "/" +year2Full;

                instance = new GetPFCITBalanceReportHeaderResult();
                instance.SourceId = val1.SourceId;
                instance.Type = val1.Type;
                instance.HeaderName = "Contribution "+ year;

                thisList.Add(instance);

                instance = new GetPFCITBalanceReportHeaderResult();
                instance.SourceId = val1.SourceId;
                instance.Type = val1.Type;
                instance.HeaderName = "Interest " + year;
                thisList.Add(instance);
            }

            return thisList;
        }


        public static string GetCellValueForPFGrid(int empID, int YearID, string Type)
        {

            if(Type.StartsWith("Contribution"))
            {
                return BaseHelper.GetCurrency(GetPFAmount(empID, YearID), SessionManager.DecimalPlaces);
            }
            else if(Type.StartsWith("Interest"))
            {
                return BaseHelper.GetCurrency(GetPFInterest(empID, YearID), SessionManager.DecimalPlaces);
            }


            return "";
        }


        public static string GetCellValueForCITGrid(int empID, int YearID, string Type)
        {

            if (Type.StartsWith("Contribution"))
            {
                return BaseHelper.GetCurrency(GetCITAmount(empID, YearID), SessionManager.DecimalPlaces);
            }
            else if (Type.StartsWith("Interest"))
            {
                return BaseHelper.GetCurrency(GetCITInterest(empID, YearID), SessionManager.DecimalPlaces);
            }


            return "";
        }


        #region GETPFCIT AMOUNT AND INTEREST
        public static decimal GetPFAmount(int empID, int YearID)
        {
            if (PayrollDataContext.PFCITYearlyInterests.Any(x => x.EmployeeId == empID && x.FinancialDateId == YearID))
            {
                return Convert.ToDecimal
                                        (PayrollDataContext.PFCITYearlyInterests
                                        .FirstOrDefault(x => x.EmployeeId == empID && x.FinancialDateId == YearID).PFAmount);
            }
            return 0;
        }

        public static decimal GetPFInterest(int empID, int YearID)
        {
            if (PayrollDataContext.PFCITYearlyInterests.Any(x => x.EmployeeId == empID && x.FinancialDateId == YearID))
            {
                return Convert.ToDecimal
                                        (PayrollDataContext.PFCITYearlyInterests
                                        .FirstOrDefault(x => x.EmployeeId == empID && x.FinancialDateId == YearID).PFInterest);
            }
            return 0;
        }

        public static decimal GetCITAmount(int empID, int YearID)
        {
            if (PayrollDataContext.PFCITYearlyInterests.Any(x => x.EmployeeId == empID && x.FinancialDateId == YearID))
            {
                return Convert.ToDecimal
                                        (PayrollDataContext.PFCITYearlyInterests
                                        .FirstOrDefault(x => x.EmployeeId == empID && x.FinancialDateId == YearID).CITAmount);
                
            }
            return 0;
        }

        public static decimal GetCITInterest(int empID, int YearID)
        {
            if (PayrollDataContext.PFCITYearlyInterests.Any(x => x.EmployeeId == empID && x.FinancialDateId == YearID))
            {
                return Convert.ToDecimal
                                        (PayrollDataContext.PFCITYearlyInterests
                                        .FirstOrDefault(x => x.EmployeeId == empID && x.FinancialDateId == YearID).CITInterest);
            }
            return 0;
        }

        #endregion



        public static List<GetAllPFContributionResult> BindInnerGridListForPF()
        {
            List<GetAllPFContributionResult> thisList = new List<GetAllPFContributionResult>();
            thisList = PayrollDataContext.GetAllPFContribution().ToList();
            return thisList;
        }

        public static List<GetAllCITContributionResult> BindInnerGridListForCIT()
        {
            List<GetAllCITContributionResult> thisList = new List<GetAllCITContributionResult>();
            thisList = PayrollDataContext.GetAllCITContribution().ToList();
            return thisList;
        }


        public static List<GetAllPFContributionAndOpeningResult> GetBindListForYearWisePFReport(int yearID,int empID)
        {
            List<GetAllPFContributionAndOpeningResult> thisList = new List<GetAllPFContributionAndOpeningResult>();

            List<GetAllPFContributionReportResult> midColumnData = new List<GetAllPFContributionReportResult>();
            List<GetAllPFContributionReportResult> tempMidColData = new List<GetAllPFContributionReportResult>();

            midColumnData = PayrollDataContext.GetAllPFContributionReport(yearID, empID).ToList();
            List<string> amountList = new List<string>();
            List<string> monthListOverall = new List<string>();

            foreach (var val1 in midColumnData)
            {
                monthListOverall.Add(val1.PayrollPeriodId.ToString());
            }

            monthListOverall = monthListOverall.Distinct().ToList();
            

            //thisList = PayrollDataContext.GetAllCITContribution().ToList();
            thisList = PayrollDataContext.GetAllPFContributionAndOpening(yearID, empID).ToList();



            foreach (var val1 in thisList)
            {
                tempMidColData = midColumnData.Where(x => x.EmployeeId == val1.EmployeeId).ToList();

                //foreach (var val2 in tempMidColData)
                //{
                //    amountList.Add(val2.PFTotal.ToString());
                //}


                foreach (var val2 in midColumnData)
                {
                    if (val2 != null)
                    {
                        int i = val2.MonthNum;
                        string value = "";
                        if (tempMidColData.Any(x => x.MonthNum == i))
                        {
                            value = tempMidColData.FirstOrDefault(x => x.MonthNum == i).PFTotal.ToString();
                            value = GetCurrency(value);

                            if (i == 1)
                            { val1.Month10 = value; }
                            if (i == 2)
                            { val1.Month11 = value; }
                            if (i == 3)
                            { val1.Month12 = value; }
                            if (i == 4)
                            { val1.Month1 = value; }
                            if (i == 5)
                            { val1.Month2 = value; }
                            if (i == 6)
                            { val1.Month3 = value; }
                            if (i == 7)
                            { val1.Month4 = value; }
                            if (i == 8)
                            { val1.Month5 = value; }
                            if (i == 9)
                            { val1.Month6 = value; }
                            if (i == 10)
                            { val1.Month7 = value; }
                            if (i == 11)
                            { val1.Month8 = value; }
                            if (i == 12)
                            { val1.Month9 = value; }
                        }
                    }
                }
                
                /*
                for (int i = 1; i <= monthListOverall.Count(); i++)
                {
                    int monthID = int.Parse(monthListOverall[i - 1]);

                    if(midColumnData.Any(x=>x.PayrollPeriodId== monthID && x.EmployeeId == val1.EmployeeId))
                    {
                        string value = midColumnData.FirstOrDefault(x => x.PayrollPeriodId == monthID && x.EmployeeId == val1.EmployeeId).PFTotal.ToString();

                        value = GetCurrency(value);

                        if (i == 1)
                        { val1.Month1 = value; }
                        if (i == 2)
                        { val1.Month2 = value; }
                        if (i == 3)
                        { val1.Month3 = value; }
                        if (i == 4)
                        { val1.Month4 = value; }
                        if (i == 5)
                        { val1.Month5 = value; }
                        if (i == 6)
                        { val1.Month6 = value; }
                        if (i == 7)
                        { val1.Month7 = value; }
                        if (i == 8)
                        { val1.Month8 = value; }
                        if (i == 9)
                        { val1.Month9 = value; }
                        if (i == 10)
                        { val1.Month10 = value; }
                        if (i == 11)
                        { val1.Month11 = value; }
                        if (i == 12)
                        { val1.Month12 = value; }


                    }

                }
                */
                

            }

            foreach (var val1 in thisList)
            {
                decimal month1 = 0, month2 = 0, month3 = 0, month4 = 0, month5 = 0, month6 = 0;
                decimal month7 = 0, month8 = 0, month9 = 0, month10 = 0, month11 = 0, month12 = 0;

                decimal OpeningBalanceContribution = 0, OpeningBalanceInterest = 0, InterestForTheYear = 0;

                if (!string.IsNullOrEmpty(val1.InterestThisYear))
                { InterestForTheYear = decimal.Parse(val1.InterestThisYear); }

                if (val1.PFOpening != null)
                { OpeningBalanceContribution = val1.PFOpening.Value; }

                if (val1.PFOpeningInterest != null)
                { OpeningBalanceInterest = val1.PFOpeningInterest.Value; }

                if (!string.IsNullOrEmpty(val1.Month1))
                {   month1 = decimal.Parse(val1.Month1);    }
                
                if (!string.IsNullOrEmpty(val1.Month2))
                { month2 = decimal.Parse(val1.Month2); }

                if (!string.IsNullOrEmpty(val1.Month3))
                { month3 = decimal.Parse(val1.Month3); }

                if (!string.IsNullOrEmpty(val1.Month4))
                { month4 = decimal.Parse(val1.Month4); }

                if (!string.IsNullOrEmpty(val1.Month5))
                { month5 = decimal.Parse(val1.Month5); }

                if (!string.IsNullOrEmpty(val1.Month6))
                { month6 = decimal.Parse(val1.Month6); }

                if (!string.IsNullOrEmpty(val1.Month7))
                { month7 = decimal.Parse(val1.Month7); }

                if (!string.IsNullOrEmpty(val1.Month8))
                { month8 = decimal.Parse(val1.Month8); }

                if (!string.IsNullOrEmpty(val1.Month9))
                { month9 = decimal.Parse(val1.Month9); }

                if (!string.IsNullOrEmpty(val1.Month10))
                { month10 = decimal.Parse(val1.Month10); }

                if (!string.IsNullOrEmpty(val1.Month11))
                { month11 = decimal.Parse(val1.Month11); }

                if (!string.IsNullOrEmpty(val1.Month12))
                { month12 = decimal.Parse(val1.Month12); }



                decimal totContribution = month1 + month2 + month3 + month4 + month5 + month6 +
                                            month7 + month8 + month9 + month10 + month11 + month12;

                val1.TotContribution = totContribution.ToString();

                val1.ClosingBalContribution = (totContribution + OpeningBalanceContribution).ToString();

                val1.ClosingBalInterest = (OpeningBalanceInterest + InterestForTheYear).ToString();

                val1.TotBalance = (decimal.Parse(val1.ClosingBalInterest) + decimal.Parse(val1.ClosingBalContribution)).ToString();


                if (!string.IsNullOrEmpty(val1.TotContribution))
                {
                    val1.TotContribution = GetCurrency(val1.TotContribution);
                }
                if (!string.IsNullOrEmpty(val1.ClosingBalContribution))
                {
                    val1.ClosingBalContribution = GetCurrency(val1.ClosingBalContribution);
                }

                if (!string.IsNullOrEmpty(val1.ClosingBalInterest))
                {
                    val1.ClosingBalInterest = GetCurrency(val1.ClosingBalInterest);
                }

                if (!string.IsNullOrEmpty(val1.TotBalance))
                {
                    val1.TotBalance = GetCurrency(val1.TotBalance);
                }


                

                if (val1.InterestThisYear != null)
                {
                    val1.InterestThisYear = GetCurrency(val1.InterestThisYear);
                }


            }

            return thisList;
        }





        public static List<GetAllCITContributionAndOpeningResult> GetBindListForYearWiseCITReport(int yearID, int empID)
        {
            List<GetAllCITContributionAndOpeningResult> thisList = new List<GetAllCITContributionAndOpeningResult>();

            List<GetAllCITContributionReportResult> midColumnData = new List<GetAllCITContributionReportResult>();
            List<GetAllCITContributionReportResult> tempMidColData = new List<GetAllCITContributionReportResult>();

            midColumnData = PayrollDataContext.GetAllCITContributionReport(yearID, empID).ToList();
            List<string> amountList = new List<string>();
            List<string> monthListOverall = new List<string>();

            foreach (var val1 in midColumnData)
            {
                monthListOverall.Add(val1.PayrollPeriodId.ToString());
            }

            monthListOverall = monthListOverall.Distinct().ToList();


            //thisList = PayrollDataContext.GetAllCITContribution().ToList();
            thisList = PayrollDataContext.GetAllCITContributionAndOpening(yearID, empID).ToList();



            foreach (var val1 in thisList)
            {
                tempMidColData = midColumnData.Where(x => x.EmployeeId == val1.EmployeeId).ToList();

                foreach (var val2 in midColumnData)
                {
                    if (val2 != null)
                    {
                        int i = val2.MonthNum;
                        string value = "";
                        if (tempMidColData.Any(x => x.MonthNum == i))
                        {
                            value = tempMidColData.FirstOrDefault(x => x.MonthNum == i).CITTotal.ToString();
                            value = GetCurrency(value);

                            if (i == 1)
                            { val1.Month10 = value; }
                            if (i == 2)
                            { val1.Month11 = value; }
                            if (i == 3)
                            { val1.Month12 = value; }
                            if (i == 4)
                            { val1.Month1 = value; }
                            if (i == 5)
                            { val1.Month2 = value; }
                            if (i == 6)
                            { val1.Month3 = value; }
                            if (i == 7)
                            { val1.Month4 = value; }
                            if (i == 8)
                            { val1.Month5 = value; }
                            if (i == 9)
                            { val1.Month6 = value; }
                            if (i == 10)
                            { val1.Month7 = value; }
                            if (i == 11)
                            { val1.Month8 = value; }
                            if (i == 12)
                            { val1.Month9 = value; }
                        }
                    }
                }

              


            }

            foreach (var val1 in thisList)
            {
                decimal month1 = 0, month2 = 0, month3 = 0, month4 = 0, month5 = 0, month6 = 0;
                decimal month7 = 0, month8 = 0, month9 = 0, month10 = 0, month11 = 0, month12 = 0;

                decimal OpeningBalanceContribution = 0, OpeningBalanceInterest = 0, InterestForTheYear = 0;

                if (!string.IsNullOrEmpty(val1.InterestThisYear))
                { InterestForTheYear = decimal.Parse(val1.InterestThisYear); }

                if (val1.CITOpening != null)
                { OpeningBalanceContribution = val1.CITOpening.Value; }

                if (val1.CITOpeningInterest != null)
                { OpeningBalanceInterest = val1.CITOpeningInterest.Value; }

                if (!string.IsNullOrEmpty(val1.Month1))
                { month1 = decimal.Parse(val1.Month1); }

                if (!string.IsNullOrEmpty(val1.Month2))
                { month2 = decimal.Parse(val1.Month2); }

                if (!string.IsNullOrEmpty(val1.Month3))
                { month3 = decimal.Parse(val1.Month3); }

                if (!string.IsNullOrEmpty(val1.Month4))
                { month4 = decimal.Parse(val1.Month4); }

                if (!string.IsNullOrEmpty(val1.Month5))
                { month5 = decimal.Parse(val1.Month5); }

                if (!string.IsNullOrEmpty(val1.Month6))
                { month6 = decimal.Parse(val1.Month6); }

                if (!string.IsNullOrEmpty(val1.Month7))
                { month7 = decimal.Parse(val1.Month7); }

                if (!string.IsNullOrEmpty(val1.Month8))
                { month8 = decimal.Parse(val1.Month8); }

                if (!string.IsNullOrEmpty(val1.Month9))
                { month9 = decimal.Parse(val1.Month9); }

                if (!string.IsNullOrEmpty(val1.Month10))
                { month10 = decimal.Parse(val1.Month10); }

                if (!string.IsNullOrEmpty(val1.Month11))
                { month11 = decimal.Parse(val1.Month11); }

                if (!string.IsNullOrEmpty(val1.Month12))
                { month12 = decimal.Parse(val1.Month12); }



                decimal totContribution = month1 + month2 + month3 + month4 + month5 + month6 +
                                            month7 + month8 + month9 + month10 + month11 + month12;

                val1.TotContribution = totContribution.ToString();

                val1.ClosingBalContribution = (totContribution + OpeningBalanceContribution).ToString();

                val1.ClosingBalInterest = (OpeningBalanceInterest + InterestForTheYear).ToString();

                val1.TotBalance = (decimal.Parse(val1.ClosingBalInterest) + decimal.Parse(val1.ClosingBalContribution)).ToString();


                if (!string.IsNullOrEmpty(val1.TotContribution))
                {
                    val1.TotContribution = GetCurrency(val1.TotContribution);
                }
                if (!string.IsNullOrEmpty(val1.ClosingBalContribution))
                {
                    val1.ClosingBalContribution = GetCurrency(val1.ClosingBalContribution);
                }

                if (!string.IsNullOrEmpty(val1.ClosingBalInterest))
                {
                    val1.ClosingBalInterest = GetCurrency(val1.ClosingBalInterest);
                }

                if (!string.IsNullOrEmpty(val1.TotBalance))
                {
                    val1.TotBalance = GetCurrency(val1.TotBalance);
                }

                if (val1.InterestThisYear != null)
                {
                    val1.InterestThisYear = GetCurrency(val1.InterestThisYear);
                }


            }

            return thisList;
        }



    }


}
    