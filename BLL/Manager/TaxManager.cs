﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;
using System.Xml.Linq;
using BLL.BO;
using System;
using System.Data.SqlClient;

namespace BLL.Manager
{
    public class TaxManager : BaseBiz
    {
        public static List<GetForDeviceMappingResult> GetForDeviceMapping(string searchEmpName, int currentPage, int pageSize, ref int totalRows)
        {
            totalRows = 0;
            int companyId = SessionManager.CurrentCompanyId;
            List<GetForDeviceMappingResult> list = PayrollDataContext.GetForDeviceMapping(
                companyId, searchEmpName, currentPage, pageSize)
                .ToList();
            if (list.Count > 0)
                totalRows = list[0].TotalRows.Value;
            return list;
        }

        public static List<GetForDeviceMappingResult> GetForDeviceMappingWebServiceCalling()
        {
            List<GetForDeviceMappingResult> list = PayrollDataContext.GetForDeviceMapping(
                -1, "", 0, 9999)
                .ToList();
            return list;
        }

        public static List<GetEmployeeListForProvisionalCITResult> GetListForPrivisionalCIT
            (string searchEmpName, int currentPage, int pageSize, ref int totalRows)
        {
            totalRows = 0;
            int companyId = SessionManager.CurrentCompanyId;
            List<GetEmployeeListForProvisionalCITResult> list = PayrollDataContext.GetEmployeeListForProvisionalCIT(
                companyId, searchEmpName, currentPage, pageSize)
                .ToList();

            foreach (GetEmployeeListForProvisionalCITResult item in list)
                item.ProcessRow();


            if (list.Count > 0)
                totalRows = list[0].TotalRows.Value;
            return list;
        }
        public static List<GetFixedPFDeductionEmpListResult> GetFixedPFList
          (string searchEmpName, int currentPage, int pageSize, ref int totalRows)
        {
            totalRows = 0;
            int companyId = SessionManager.CurrentCompanyId;
            List<GetFixedPFDeductionEmpListResult> list = PayrollDataContext.GetFixedPFDeductionEmpList(
                companyId, searchEmpName, currentPage, pageSize)
                .ToList();




            if (list.Count > 0)
                totalRows = list[0].TotalRows.Value;
            return list;
        }

        public static void SaveForPrivisionalCIT(string xml, string xmlMonths)
        {
            PayrollDataContext.SaveProvisionalCIT(XElement.Parse(xml), XElement.Parse(xmlMonths));
        }
        public static void SaveFixedPFDeduction(string xml)
        {
            PayrollDataContext.SaveFixedPFDeduction(XElement.Parse(xml));
        }

        public static List<GetForTaxHistoryResult> GetForTaxHistory(string searchEmpName, int currentPage, int pageSize, ref int totalRows)
        {
            totalRows = 0;
            int companyId = SessionManager.CurrentCompanyId;
            List<GetForTaxHistoryResult> list = PayrollDataContext.GetForTaxHistory(
                companyId, searchEmpName, CommonManager.IsFirstSalaryGenerated(), currentPage, pageSize)
                .ToList();
            if (list.Count > 0)
                totalRows = list[0].TotalRows.Value;
            return list;
        }
        public static List<PPay> GetAccountListForExport()
        {
            return PayrollDataContext.PPays.ToList();
        }
        public static List<GetOpeningImportResult> GetForPFOpening(string searchEmpName, int currentPage, int pageSize, ref int totalRows)
        {
            totalRows = 0;
            int companyId = SessionManager.CurrentCompanyId;
            List<GetOpeningImportResult> list = PayrollDataContext.GetOpeningImport(
                companyId, searchEmpName, CommonManager.IsFirstSalaryGenerated(), currentPage, pageSize)
                .ToList();
            if (list.Count > 0)
                totalRows = list[0].TotalRows.Value;
            return list;
        }

        public static List<GetInterestImportResult> GetForPFYearlyInterest(string searchEmpName, int currentPage, int pageSize, ref int totalRows, int YearID)
        {
            totalRows = 0;
            int companyId = SessionManager.CurrentCompanyId;
            List<GetInterestImportResult> list = PayrollDataContext.GetInterestImport(
                companyId, searchEmpName, YearID, currentPage, pageSize)
                .ToList();
            if (list.Count > 0)
                totalRows = list[0].TotalRows.Value;
            return list;
        }

        public static List<GetInterestImportResult> GetForPFCITYearlyInterest(string searchEmpName, int currentPage, int pageSize, ref int totalRows, int FinancialYearID)
        {
            totalRows = 0;
            int companyId = SessionManager.CurrentCompanyId;
            List<GetInterestImportResult> list = PayrollDataContext.GetInterestImport(
                companyId, searchEmpName, FinancialYearID, currentPage, pageSize)
                .ToList();
            if (list.Count > 0)
                totalRows = list[0].TotalRows.Value;
            return list;
        }

        public static List<EmployeeGratuityClassBO> GetEmployeeGratuityClassList(string searchEmpName, int currentPage, int pageSize, ref int totalRows)
        {
            List<EmployeeGratuityClassBO> list =
                (
                    from e in PayrollDataContext.EEmployees
                    join h in PayrollDataContext.EHumanResources on e.EmployeeId equals h.EmployeeId
                    //join g in PayrollDataContext.GratuityClasses on temp

                    join p in PayrollDataContext.GratuityClasses on h.GratuityClassRef_ID equals p.GratuityClassID into ps
                    from p in ps.DefaultIfEmpty()

                    where
                    (searchEmpName.Length == 0 || e.Name.ToLower().Contains(searchEmpName)) &&
                    (
                        (
                            e.IsResigned == false ||
                            h.DateOfResignationEng >= DateTime.Now.Date
                            )
                        ||
                         (
                            e.IsRetired == false ||
                            h.DateOfRetirementEng >= DateTime.Now.Date
                            )
                    )
                    orderby e.Name
                    select new EmployeeGratuityClassBO
                    {
                        EmployeeId = h.EmployeeId,
                        Name = e.Name,
                        GratuityClass = (p == null ? 0 : p.GratuityClassID),
                        GratuityClassName = (p == null ? "" : p.Name)
                    }

                ).ToList();


            totalRows = list.Count;
            //Paging code
            if (currentPage >= 0 && pageSize > 0)
            {
                list = list.Skip(currentPage).Take(pageSize).ToList();
            }


            return list;
        }

        public static List<AttendanceInOutTime> GetTimes()
        {
            int companyId = SessionManager.CurrentCompanyId;
            List<AttendanceInOutTime> list = PayrollDataContext.AttendanceInOutTimes
                .OrderByDescending(x => x.DateEng).ToList();

            List<EWorkShift> shifts = PayrollDataContext.EWorkShifts.ToList();

            foreach (AttendanceInOutTime time in list)
            {
                if (time.ShiftID != null &&
                    shifts.Any(x => x.WorkShiftId == time.ShiftID))
                {
                    time.ShiftName = shifts.FirstOrDefault(x => x.WorkShiftId == time.ShiftID).Name;
                }
            }


            return list;
        }



        public static void SaveForTax(string xml)
        {
            PayrollDataContext.SaveTaxForHistory(XElement.Parse(xml));
        }
        public static void SavePay(List<PPay> list, List<EFundDetail> fundList)
        {
            List<Bank> banks = PayrollDataContext.Banks.ToList();

            //List<int> einList = list.Select(x => x.EmployeeId.Value).ToList();

            List<PPay> payList = PayrollDataContext.PPays.ToList();
            List<EFundDetail> dbFundList = PayrollDataContext.EFundDetails.ToList();

            foreach (PPay item in list)
            {
                PPay db = payList.FirstOrDefault(x => x.EmployeeId == item.EmployeeId);
                EFundDetail dbfund = dbFundList.FirstOrDefault(x => x.EmployeeId == item.EmployeeId);
                EFundDetail fund = fundList.FirstOrDefault(x => x.EmployeeId == item.EmployeeId);

                if (db == null)
                {
                    db = new PPay();
                    db.EmployeeId = item.EmployeeId;
                    db.PaymentMode = PaymentMode.BANK_DEPOSIT;

                }
                else
                {

                    #region "Change Logs"
                    if (db.PaymentMode != item.PaymentMode)
                    {
                        PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(db.EmployeeId.Value, (byte)MonetoryChangeLogTypeEnum.Information,
                            "Payment Mode", db.PaymentMode, item.PaymentMode, LogActionEnum.Update));
                    }


                    //if (db.BankName != item.BankName)
                    //{
                    //    PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(db.EmployeeId.Value, (byte)MonetoryChangeLogTypeEnum.Information,
                    //        "Bank Name", db.BankName, item.BankName, LogActionEnum.Update));
                    //}
                    if (db.BankID != item.BankID)
                    {
                        PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(db.EmployeeId.Value, (byte)MonetoryChangeLogTypeEnum.Information,
                            "Bank Name", db.BankID, item.BankID, LogActionEnum.Update));
                    }
                    if (db.BankACNo != item.BankACNo)
                    {
                        PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(db.EmployeeId.Value, (byte)MonetoryChangeLogTypeEnum.Information,
                            "Bank a/c", db.BankACNo, item.BankACNo, LogActionEnum.Update));
                    }
                    if (db.LoanAccount != item.LoanAccount)
                    {
                        PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(db.EmployeeId.Value, (byte)MonetoryChangeLogTypeEnum.Information,
                            "Loan Account", db.LoanAccount, item.LoanAccount, LogActionEnum.Update));
                    }
                    if (db.AdvanceAccount != item.AdvanceAccount)
                    {
                        PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(db.EmployeeId.Value, (byte)MonetoryChangeLogTypeEnum.Information,
                            "Advance Account", db.AdvanceAccount, item.AdvanceAccount, LogActionEnum.Update));
                    }

                    #endregion
                }

                db.PaymentMode = item.PaymentMode;
                db.BankACNo = item.BankACNo;
                db.LoanAccount = item.LoanAccount;
                db.AdvanceAccount = item.AdvanceAccount;
                // set default bank for Bank payment ode if only single bank in the company
                //if (item.PaymentMode == PaymentMode.BANK_DEPOSIT && banks.Count == 1
                //    && db.BankID == null)
                //{
                //    db.BankID = banks[0].BankID;
                //}
                db.BankID = item.BankID;
                db.BankBranchID = item.BankBranchID;


                dbfund.PFRFNo = fund.PFRFNo;
                dbfund.CITNo = fund.CITNo;
                dbfund.CITCode = fund.CITCode;
                dbfund.PANNo = fund.PANNo;
                dbfund.HasOtherPFFund = fund.HasOtherPFFund;

            }

            PayrollDataContext.SubmitChanges();
        }
        public static Status SaveForDeviceMapping(string xml)
        {
            Status status = new Status();
            status.IsSuccess = false;
            try
            {
                PayrollDataContext.SaveDevice(XElement.Parse(xml)); //insertion code 
                status.IsSuccess = true;
            }
            catch (SqlException e)
            {
                if (e.Number == 2601 || e.Number == 2627)/* PK/UKC violation */
                {
                    status.IsSuccess = false;
                    status.ErrorMessage = "Unable to save,unique constraint violations.Duplicate device id.";
                }
                else
                {
                    status.IsSuccess = false;
                    status.ErrorMessage = "Unable to save.";
                }
            }
            return status;
        }

        public static decimal GetTotalTaxPaid(int startPayrollId, int endPayrollId, int employeeId)
        {
            return PayrollDataContext.GetTaxPaid(startPayrollId, endPayrollId, employeeId).Value;
        }

        public string GetHTMLEmployeeTaxList(int employeeId)
        {
            //don't return empty string otherwise false in js
            StringBuilder html = new StringBuilder();
            PTax tax = GetPayTaxByEmployee(employeeId);

            html.Append("<table class='grid payLeaveTbl'>");
            html.Append("<tr> <th style='width:150px'>Tax</th> <th style='50px' class='editDeleteTH'> &nbsp; </th> </tr>");


            if (tax != null)
            {


                html.Append(
                    string.Format(@"<tr><td> {0} </td><td class='editDeleteTD'> 
                            <input type='image' style='border-width: 0px;' onclick='return popupTaxCall({1});' src='../images/edit.gif' />
                            <input type='image' style='border-width: 0px;' onclick='return deleteTaxCall({1});' src='../images/delete.gif' />
                            </td> </tr>",
                        tax.Title, tax.TaxId
                    )
                    );





            }
            html.Append("</table>");
            return html.ToString();
        }



        public PTax GetPayTaxByCompany(int companyId)
        {
            PTax tax = PayrollDataContext.PTaxes.SingleOrDefault(
                t => t.CompanyId == companyId);
            return tax;
        }

        public PTax GetPayTaxByEmployee(int employeeId)
        {
            var taxes = from t in PayrollDataContext.PTaxes
                        join ET in PayrollDataContext.PEmployeeTaxes on t.TaxId equals ET.TaxId
                        where ET.EmployeeId == employeeId
                        select t;


            foreach (PTax tax in taxes)
            {
                return tax;
            }
            return null;
        }
        public bool Save(PTax tax, PEmployeeTax empTax)
        {
            PayrollDataContext.PTaxes.InsertOnSubmit(tax);
            PayrollDataContext.SubmitChanges();

            empTax.TaxId = tax.TaxId;
            return SavePayTaxForEmployee(empTax);
        }

        public bool SavePayTaxForEmployee(PEmployeeTax empTax)
        {
            PayrollDataContext.PEmployeeTaxes.InsertOnSubmit(empTax);
            return SaveChangeSet();
        }

        //public bool RemoveTaxForEmployee(

        public bool Update(PTax entity, PEmployeeTax empTax)
        {
            PTax dbEntity = PayrollDataContext.PTaxes.SingleOrDefault(
                t => t.TaxId == entity.TaxId);

            if (dbEntity == null)
                return false;

            dbEntity.Title = entity.Title;
            dbEntity.Abbreviation = entity.Abbreviation;
            dbEntity.PaidBy = entity.PaidBy;


            PTax tax = GetPayTaxByEmployee(empTax.EmployeeId);
            if (tax == null)
            {
                empTax.TaxId = entity.TaxId;
                SavePayTaxForEmployee(empTax);
            }
            UpdateChangeSet();
            return true;
        }

        public bool Delete(PEmployeeTax entity)
        {
            PEmployeeTax dbEntity = PayrollDataContext.PEmployeeTaxes.SingleOrDefault(
                c => (c.EmployeeId == entity.EmployeeId && c.TaxId == entity.TaxId));

            if (dbEntity == null)
                return false;

            System.Data.Linq.ChangeSet cs = PayrollDataContext.GetChangeSet();
            PayrollDataContext.PEmployeeTaxes.DeleteOnSubmit(dbEntity);
            PayrollDataContext.SubmitChanges();
            return (cs.Deletes.Count == 1);
        }

        #region "Medical tax credit"

        public List<GetMedicalTaxCreditListResult> GetMedicalTaxCreditList(int companyId, string empSearchText, int currentPage, int pageSize, ref int total)
        {
            List<GetMedicalTaxCreditListResult> list = PayrollDataContext.GetMedicalTaxCreditList(companyId, empSearchText, currentPage, pageSize).ToList();
            total = 0;
            if (list.Count > 0)
                total = list[0].TotalRows.Value;

            return list;

        }

        public void SaveMedicalTaxList(int companyId, string empIDs)
        {
            PayrollDataContext.SaveMedicalTaxCreditList(companyId, empIDs);
        }

        public bool SaveUpdateMedicalTaxDetail(int medicalTaxCreditDetailId, int medicalTaxCreditId, string date,
            string refNo, string institution, decimal amount, string notes)
        {
            return PayrollDataContext.SaveUpdateMedicalTaxDetail(medicalTaxCreditDetailId, medicalTaxCreditId,
                date, refNo, institution, amount, notes) == 1;
        }

        public bool UpdateMedicalTaxCreditPYAmount(int medicalTaxCreditId, decimal pyAmount)
        {

            MedicalTaxCredit credit = PayrollDataContext.MedicalTaxCredits.SingleOrDefault(x => x.MedicalTaxCreditId == medicalTaxCreditId);

            if (credit.PYAmount != pyAmount)
            {
                PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(credit.EmployeeId.Value, (byte)MonetoryChangeLogTypeEnum.MedicalTaxCredit,
                    "Last Year Amount", credit.PYAmount, pyAmount, LogActionEnum.Update));

                SaveChangeSet();
            }


            return PayrollDataContext.UpdateMedicalTaxCreditPYAmount(medicalTaxCreditId, pyAmount) >= 1;
        }

        public static decimal GetAdjustableAmount(int employeeId)
        {
            MedicalTaxCredit entity = PayrollDataContext.MedicalTaxCredits
                .Where(m => m.EmployeeId == employeeId && m.IsCurrent == true).SingleOrDefault();
            if (entity == null)
                return 0;
            return entity.AdjustableAmount.Value;
        }

        public bool DeleteMedicalTaxCreditDetail(int medicalTaxCreditDetailId)
        {
            MedicalTaxCreditDetail detail =
                GetMedicalTaxCreditDetail(medicalTaxCreditDetailId);

            if (detail != null)
                PayrollDataContext.MedicalTaxCreditDetails.DeleteOnSubmit(detail);

            PayrollDataContext.SubmitChanges();
            PayrollDataContext.UpdateMedicalTaxCredit(detail.MedicalTaxCreditId);


            return true;
        }

        public static string InsertUpdateImportedMedicalTaxDetail(List<MedicalTaxCreditDetail> list, ref int rowImportedCount)
        {
            //int rowImportedCount = 0;

            int medicalTaxId = 0;
            foreach (MedicalTaxCreditDetail item in list)
            {
                rowImportedCount += 1;

                //MedicalTaxCreditDetail dbItem = PayrollDataContext.MedicalTaxCreditDetails
                //    .Where(x => x.Date == item.Date && x.EmployeeId == item.EmployeeId).FirstOrDefault();

                MedicalTaxCreditDetail dbItem =
                    (
                    from d in PayrollDataContext.MedicalTaxCreditDetails
                    join m in PayrollDataContext.MedicalTaxCredits on d.MedicalTaxCreditId equals m.MedicalTaxCreditId
                    where m.EmployeeId == item.EmployeeId && d.Date == item.Date
                    select d
                    ).FirstOrDefault();

                if (dbItem != null)
                {
                    dbItem.Amount = item.Amount;
                    dbItem.RefNo = item.RefNo;
                    dbItem.Institution = item.Institution;
                    dbItem.Notes = item.Notes;

                    medicalTaxId = dbItem.MedicalTaxCreditId.Value;
                }
                else
                {
                    MedicalTaxCredit dbTax = PayrollDataContext.MedicalTaxCredits
                    .Where(x => x.EmployeeId == item.EmployeeId && x.IsCurrent == true).FirstOrDefault();
                    if (dbTax != null)
                    {
                        item.MedicalTaxCreditId = dbTax.MedicalTaxCreditId;
                        PayrollDataContext.MedicalTaxCreditDetails.InsertOnSubmit(item);

                        medicalTaxId = dbTax.MedicalTaxCreditId;
                    }
                    else
                    {
                        dbTax = new MedicalTaxCredit();
                        dbTax.EmployeeId = item.EmployeeId;
                        dbTax.AdjustableAmount = 0;
                        dbTax.CarriedFWDToNextYear = 0;
                        dbTax.CompanyId = SessionManager.CurrentCompanyId;
                        dbTax.CYExpense = 0;
                        dbTax.IsCurrent = true;
                        dbTax.PYAmount = 0;


                        dbTax.MedicalTaxCreditDetails.Add(item);

                        PayrollDataContext.MedicalTaxCredits.InsertOnSubmit(dbTax);

                        PayrollDataContext.SubmitChanges();

                        medicalTaxId = dbTax.MedicalTaxCreditId;
                    }
                }

                PayrollDataContext.SubmitChanges();
                if (medicalTaxId != 0)
                    PayrollDataContext.UpdateMedicalTaxCredit(medicalTaxId);
            }
            // errro validation like msg
            return "";
        }

        public List<MedicalTaxCreditDetail> GetMedicalTaxDetailsList(int medicalTaxCreditId)
        {
            return PayrollDataContext.MedicalTaxCreditDetails.Where(
                e => e.MedicalTaxCreditId == medicalTaxCreditId).ToList();
        }

        public MedicalTaxCredit GetMedicalTaxCredit(int medicalTaxCreditId)
        {
            return PayrollDataContext.MedicalTaxCredits.SingleOrDefault(
                e => e.MedicalTaxCreditId == medicalTaxCreditId);
        }

        public MedicalTaxCreditDetail GetMedicalTaxCreditDetail(int medicalTaxCreditDetailId)
        {
            return PayrollDataContext.MedicalTaxCreditDetails.SingleOrDefault(
                e => e.MedicalTaxCreditDetailId == medicalTaxCreditDetailId);
        }
        #endregion

        public static Status ImportPFCITOpening(List<PFCITOpening> inserInstance)
        {
            Status status = new Status();

            if (inserInstance != null && inserInstance.Count > 0)
            {

                PFCITOpening dbEntity = new PFCITOpening();

                foreach (var val1 in inserInstance)
                {
                    if (PayrollDataContext.PFCITOpenings.Any(x => x.EmployeeId == val1.EmployeeId))
                    {
                        dbEntity = new PFCITOpening();
                        dbEntity = PayrollDataContext.PFCITOpenings.FirstOrDefault(x => x.EmployeeId == val1.EmployeeId);
                        dbEntity.OpeningPF = val1.OpeningPF;
                        dbEntity.OpeningPFInterest = val1.OpeningPFInterest;
                        dbEntity.OpeningCIT = val1.OpeningCIT;
                        dbEntity.OpeningCITInterest = val1.OpeningCITInterest;
                        PayrollDataContext.SubmitChanges();
                    }
                    else
                    {
                        PayrollDataContext.PFCITOpenings.InsertOnSubmit(val1);
                        PayrollDataContext.SubmitChanges();
                    }
                }

                //PayrollDataContext.PFCITOpenings.InsertAllOnSubmit(inserInstance);
                //PayrollDataContext.PayrollDataContext.SubmitChanges();
            }

            status.IsSuccess = true;
            return status;
        }



        public static Status ImportPFCITYearlyInterest(List<PFCITYearlyInterest> insertInstance)
        {
            Status status = new Status();

            if (insertInstance != null && insertInstance.Count > 0)
            {
                PFCITYearlyInterest dbEntity = new PFCITYearlyInterest();

                foreach (var val1 in insertInstance)
                {
                    dbEntity = new PFCITYearlyInterest();
                    if (PayrollDataContext.PFCITYearlyInterests.Any(x => x.EmployeeId == val1.EmployeeId && x.FinancialDateId == val1.FinancialDateId))
                    {
                        dbEntity = PayrollDataContext.PFCITYearlyInterests.FirstOrDefault(x => x.EmployeeId == val1.EmployeeId && x.FinancialDateId == val1.FinancialDateId);
                        dbEntity.PFInterest = val1.PFInterest;
                        dbEntity.CITInterest = val1.CITInterest;
                        PayrollDataContext.SubmitChanges();
                    }
                    else
                    {
                        PayrollDataContext.PFCITYearlyInterests.InsertOnSubmit(val1);
                        PayrollDataContext.SubmitChanges();
                    }
                }


                //PayrollDataContext.PFCITYearlyInterests.InsertAllOnSubmit(insertInstance);
                //PayrollDataContext.SubmitChanges();
            }

            status.IsSuccess = true;
            return status;
        }




    }
}
