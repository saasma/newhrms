﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using DAL;
using Ext.Net;
using Utils;
namespace BLL.Manager
{
    public class PayrollMessageManager:BaseBiz
    {

        public static Boolean SaveMessages(PayrollMessage message)
        {
            PayrollDataContext.PayrollMessages.InsertOnSubmit(message);
            return SaveChangeSet();
        }

        public static Paging<GetPayrollMessageByUserNameResult> GetPayrollMessageByUserName(string UserName, int start, int limit)
        {
            try
            {
                List<GetPayrollMessageByUserNameResult> payrollMessages = 
                    PayrollDataContext.GetPayrollMessageByUserName(UserName,start,limit).ToList();

                //if ((start + limit) > payrollMessages.Count)
                //{
                //    limit = payrollMessages.Count - start;
                //}

                //List<GetPayrollMessageByUserNameResult> rangePlants = (start < 0 || limit < 0) ? payrollMessages : payrollMessages.GetRange(start, limit);

                int total = 0;
                if (payrollMessages.Count > 0)
                    total = payrollMessages[0].TotalRows.Value;

                return new Paging<GetPayrollMessageByUserNameResult>(payrollMessages, total);
            }
            catch (Exception exp)
            {
                Log.log("Error while retrieving payroll message.", exp);
                return new Paging<GetPayrollMessageByUserNameResult>();
            }
        }

        public static Boolean DeletePayrollMessage(int messageId)
        {
            PayrollDataContext.PayrollMessages.DeleteOnSubmit(PayrollDataContext.PayrollMessages.Where(x => x.MessageId == messageId).SingleOrDefault());
            return DeleteChangeSet();
        }

        public static Boolean DeletePayrollMessageInBulk(List<int> messageIds, out int rowsAffected)
        {
            rowsAffected = 0;
            List<PayrollMessage> messages = new List<PayrollMessage>();
            PayrollMessage msg = null;
            foreach (int msgId in messageIds)
            {
                msg = PayrollDataContext.PayrollMessages.Where(x => x.MessageId == msgId).SingleOrDefault();
                if (msg != null)
                {
                    messages.Add(msg);
                }
            }
            PayrollDataContext.PayrollMessages.DeleteAllOnSubmit(messages);

            rowsAffected = PayrollDataContext.GetChangeSet().Deletes.Count;
            PayrollDataContext.SubmitChanges();
            
            //if (rowsAffected > 0)
            //    return true;

            return true;
        }

        public static Boolean MarkAsRead(List<int> messageIds, out int rowsAffected)
        {

            rowsAffected = 0;
            List<PayrollMessage> messages = new List<PayrollMessage>();
            PayrollMessage msg = null;
            foreach (int msgId in messageIds)
            {
                msg = PayrollDataContext.PayrollMessages.Where(x=>x.MessageId == msgId).SingleOrDefault();
                if (msg != null)
                {
                    msg.IsRead = true;
                    messages.Add(msg);
                }

            }

            rowsAffected = PayrollDataContext.GetChangeSet().Updates.Count;
            PayrollDataContext.SubmitChanges();
            
            if (rowsAffected > 0)
                return true;

            return false;
        }

        //public static int GetUnreadPayrollMessage(string UserName)
        //{
        //    return (int) PayrollDataContext.GetUnreadPayrollMessage(UserName);
        //}

        public static List<PayrollMessage> GetMessages(PayrollMessage message, List<string> EmployeeList)
        {
            List<PayrollMessage> messages = new List<PayrollMessage>();
            PayrollMessage msg = null;
            foreach (string userName in EmployeeList)
            {
                msg = new PayrollMessage();
                msg.Subject = message.Subject;
                msg.Body = message.Body;
                msg.SendBy = message.SendBy;
                msg.ReceivedDate = message.ReceivedDate;
                msg.ReceivedDateEng = message.ReceivedDateEng;
                msg.ReceivedTo = userName;
                msg.IsRead = message.IsRead;
                msg.DueDateEng = message.DueDateEng;
                messages.Add(msg);
            }
            return messages;
        }

        public static Boolean SendMessages(PayrollMessage message, List<string> EmployeeList, PayrollDataContext PayrollDataContext)
        {
            List<PayrollMessage> messages = GetMessages(message, EmployeeList);

            PayrollDataContext.PayrollMessages.InsertAllOnSubmit(messages);

            SaveChangeSet();
            return true;
        }

        public static Boolean SendMessages(PayrollMessage message, List<string> EmployeeList)
        {
            List<PayrollMessage> messages = GetMessages(message, EmployeeList);


            PayrollDataContext.PayrollMessages.InsertAllOnSubmit(messages);
            return SaveChangeSet();
        }


    }
}
