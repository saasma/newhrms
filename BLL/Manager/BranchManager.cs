﻿using System.Collections.Generic;
using System.Linq;
using DAL;
using System;

namespace BLL.Manager
{
    public class BranchManager : BaseBiz
    {
        
        public bool Save(Branch entity)
        {
            if (PayrollDataContext.Branches.Any(x => x.Name.ToLower() == entity.Name.ToLower()))
            {
                return false;
            }
            entity.IsHeadOffice = false;
            PayrollDataContext.Branches.InsertOnSubmit(entity);
            PayrollDataContext.SubmitChanges();

            DeleteFromCache("GetEmployeeAddressReportResult");

            return true;
        }

        public static List<BranchDepartmentHistory> GetTransferListForAppraisal(int employeeId)
        {
            List<BranchDepartmentHistory> list = PayrollDataContext.BranchDepartmentHistories
                .Where(x => x.EmployeeId == employeeId && (x.IsFirst == false || x.IsFirst == false))
                .OrderBy(x => x.FromDateEng).ToList();

            foreach (BranchDepartmentHistory item in list)
            {
                if(item.IsDepartmentTransfer == null || item.IsDepartmentTransfer==false)
                {
                    item.FromBranch = new BranchManager().GetById(item.FromBranchId.Value).Name;
                    item.ToBranch = new BranchManager().GetById(item.BranchId.Value).Name;
                    item.Type = "Branch";
                }
                else
                {
                    item.FromBranch = new DepartmentManager().GetById(item.FromDepartmentId.Value).Name;
                    item.ToBranch = new DepartmentManager().GetById(item.DepeartmentId.Value).Name;
                    item.Type = "Department";
                }
                //item.FromDate = item.FromDateEng.Value.ToShortDateString();
            }

            return list;
        }

        public static List<GetEmployeeDesignationHistoryForAppraisalResult> GetHistoryForAppraisal(int employeeId)
        {
            List<GetEmployeeDesignationHistoryForAppraisalResult> list = PayrollDataContext.GetEmployeeDesignationHistoryForAppraisal
                (employeeId).ToList();

           

            return list;
        }
        public static List<BranchDepartmentHistory> GetBranchTransferList(bool isDepartmentTransfer,
            int start, int limit, ref int total,string searchEmp,int empID)
        {
            searchEmp = searchEmp.ToLower();

            //commented for new pages, uncomment later on
            start = (start * limit);

            List<BranchDepartmentHistory> list =
                (
                from t in PayrollDataContext.BranchDepartmentHistories
                join e in PayrollDataContext.EEmployees on t.EmployeeId equals e.EmployeeId
                where (t.IsFirst == null || t.IsFirst==false)
                 && (searchEmp.Length==0 || e.Name.ToLower().Contains(searchEmp))
                 &&(empID==-1 || empID == e.EmployeeId)
                 //&&(
                 //   (isDepartmentTransfer == true && t.IsDepartmentTransfer != null && t.IsDepartmentTransfer.Value == true)
                 //   ||
                 //   (isDepartmentTransfer == false && (t.IsDepartmentTransfer == null || t.IsDepartmentTransfer == false))
                 //   )
                orderby t.FromDateEng descending
                select t
                ).ToList();
                //PayrollDataContext.BranchDepartmentHistories.Where(x=>x.FromBranchId != x.BranchId)
                //.OrderByDescending(x => x.FromDateEng)
                //.ToList();

            total = list.Count;
            //Paging code
            if (start >= 0 && limit > 0)
            {
                list = list.Skip(start).Take(limit).ToList();
            }

            EmployeeManager empMgr = new EmployeeManager();
            BranchManager bMgr = new BranchManager();
            DepartmentManager depMgr = new DepartmentManager();
            EEmployee emp = null;
            foreach (BranchDepartmentHistory item in list)
            {
                emp = empMgr.GetById(item.EmployeeId.Value);
                item.EmployeeName = emp.Name;
                item.INo = emp.EHumanResources[0].IdCardNo;
                int years, months, days, hours;
                int minutes, seconds, milliseconds;

                NewHelper.GetElapsedTime(item.FromDateEng.Value, BLL.BaseBiz.GetCurrentDateAndTime(), out years, out months, out days, out hours, out minutes, out seconds, out milliseconds);
                string text = "";
                if (years != 0)
                    text = " " + years + " Y";
                if (months != 0)
                    text += " " + months + " M";

                if (text != "")
                text += " and ";

                if (days != 0)
                text += days + " D";


                item.TimeElapsed = text;
                //if (isDepartmentTransfer == false)
                {

                    if (item.FromBranchId != null)
                    {
                        item.FromBranch = bMgr.GetById(item.FromBranchId.Value).NameAndCode;
                    }

                    if (item.BranchId != null)
                    {
                        item.ToBranch = bMgr.GetById(item.BranchId.Value).NameAndCode;
                    }
                }
                //else
                {
                    if (item.FromDepartmentId != null)
                    {
                        item.FromDepartment = depMgr.GetById(item.FromDepartmentId.Value).Name;
                    }

                    if (item.DepeartmentId != null)
                    {
                        item.ToDepartment = depMgr.GetById(item.DepeartmentId.Value).Name;
                    }
                }
            }

            return list;
        }


        public static int GetEmployeeCurrentBranch(int employeeId, DateTime date)
        {
            return PayrollDataContext.GetCurrentBranch(employeeId, date).Value;
        }


        public static List<Branch> GetRegionalBranchList(int currentBranchId)
        {
            return
                PayrollDataContext.Branches.Where(x =>x.BranchId != currentBranchId &&
                    x.IsRegionalOffice != null && x.IsRegionalOffice.Value)
                .OrderBy(x => x.Name).ToList();
        }
        public static Status SaveUpdateDepartmentsToBranch(Branch branch,bool addDepartmentToAllBranches)
        {
            Status status = new Status();

            Branch dbBranch =PayrollDataContext.Branches.SingleOrDefault(x=>x.BranchId==branch.BranchId);
            DepartmentManager mgr = new DepartmentManager();

            string depIDList = "";
            foreach (BranchDepartment item in branch.BranchDepartments)
            {
                if (depIDList == "")
                    depIDList = item.DepartmentID.ToString();
                else
                    depIDList += "," + item.DepartmentID;
            }

            List<BranchDepartment> newList = branch.BranchDepartments.ToList();
            List<BranchDepartment> oldList = dbBranch.BranchDepartments.ToList();
            List<BranchDepartment> deleteList = new List<BranchDepartment>();

            foreach (BranchDepartment item in oldList)
            {
                if (!newList.Any(x => x.DepartmentID == item.DepartmentID))
                {

                    if (PayrollDataContext.EEmployees.Any(x => x.BranchId == branch.BranchId && x.DepartmentId == item.DepartmentID) ||
                        PayrollDataContext.BranchDepartmentHistories.Any(x => x.BranchId == branch.BranchId && x.DepeartmentId == item.DepartmentID))
                    {

                        if (status.ErrorMessage == "")
                            status.ErrorMessage = mgr.GetById(item.DepartmentID).Name;
                        else
                            status.ErrorMessage += ", " + mgr.GetById(item.DepartmentID).Name;
                    }
                }
            }

            if (!string.IsNullOrEmpty(status.ErrorMessage))
            {
                status.ErrorMessage += " departments are being used, can not be removed for the branch \"" + dbBranch.Name + "\".";
                return status;
            }

          //preseve department head
            foreach (BranchDepartment item in dbBranch.BranchDepartments)
            {
                if (item.HeadEmployeeId != null)
                {
                    //currentBranchDepartmentHeadList.Add(item.DepartmentID, item.HeadEmployeeId.Value);
                    BranchDepartment newItem = newList.FirstOrDefault(x => x.DepartmentID == item.DepartmentID);
                    if (newItem != null)
                    {
                        newItem.HeadEmployeeId = item.HeadEmployeeId;
                    }
                }
            }

            dbBranch.BranchDepartments.Clear();
            dbBranch.BranchDepartments.AddRange(newList.ToList());

            // add department to other branches also
            if (addDepartmentToAllBranches)
            {
              
                PayrollDataContext.AddDepartmentToAllBranch(depIDList, branch.BranchId);
            }

            PayrollDataContext.SubmitChanges();


            return status;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="entity"></param>
        /// <returns>true if updated,false if not, as the record may have been deleted also</returns>
        public bool Update(Branch entity)
        {
            if (PayrollDataContext.Branches.Any(x => x.Name.ToLower() == entity.Name.ToLower()
                && x.BranchId != entity.BranchId))
            {
                return false;
            }

            Branch dbEntity = PayrollDataContext.Branches.SingleOrDefault(c => c.BranchId == entity.BranchId);
           
            if (dbEntity == null)
                return false;


            dbEntity.Name = entity.Name;
            dbEntity.Code = entity.Code;
            dbEntity.ResponsiblePerson = entity.ResponsiblePerson;
            dbEntity.Address = entity.Address;
            dbEntity.Fax = entity.Fax;
            dbEntity.AreaCode = entity.AreaCode;
            dbEntity.Phone = entity.Phone;
            dbEntity.Email = entity.Email;

            #region "Change Logs"
             CommonManager mgr = new CommonManager();


             if (dbEntity.IsManualAttendance != entity.IsManualAttendance)
             {
                 PayrollDataContext.ChangeSettings.InsertOnSubmit(GetSettingChangeLog((byte)MonetoryChangeLogTypeEnum.Information, dbEntity.Name + " : " + "Manual Attendance",
                     "-", entity.IsManualAttendance.ToString(), LogActionEnum.Add));
             }

            //if reg is set
            if (dbEntity.IsInRemoteArea == false && entity.IsInRemoteArea == true)
            {
                PayrollDataContext.ChangeSettings.InsertOnSubmit(GetSettingChangeLog((byte)MonetoryChangeLogTypeEnum.Branch,dbEntity.Name+ " : "+  "Remote Area",
                    "-", mgr.GetRemoteAreaValue(entity.VDCId.Value),  LogActionEnum.Add));
            }
            //if reg set is changed
            else if (dbEntity.IsInRemoteArea == true && entity.IsInRemoteArea == true && dbEntity.VDCId != entity.VDCId)
            {
                PayrollDataContext.ChangeSettings.InsertOnSubmit(GetSettingChangeLog((byte)MonetoryChangeLogTypeEnum.Branch, dbEntity.Name + " : " + "Remote Area",
                    mgr.GetRemoteAreaValue(dbEntity.VDCId.Value), mgr.GetRemoteAreaValue(entity.VDCId.Value),  LogActionEnum.Update));
            }
            //if ret date is removed
            else if (dbEntity.IsInRemoteArea == true && entity.IsInRemoteArea == false)
            {

                PayrollDataContext.ChangeSettings.InsertOnSubmit(GetSettingChangeLog((byte)MonetoryChangeLogTypeEnum.Branch, dbEntity.Name + " : " + "Remote Area",
                   mgr.GetRemoteAreaValue(dbEntity.VDCId.Value), "-",  LogActionEnum.Delete));
            }
            #endregion

            dbEntity.IsRegionalOffice = entity.IsRegionalOffice;
            dbEntity.RegionalBranchId = entity.RegionalBranchId;
            dbEntity.RegionalHeadId = entity.RegionalHeadId;
            dbEntity.BranchHeadId = entity.BranchHeadId;
            dbEntity.BID = entity.BID;

            dbEntity.IsInRemoteArea = entity.IsInRemoteArea;            
            dbEntity.VDCId = entity.VDCId;
            dbEntity.IsManualAttendance = entity.IsManualAttendance;

            dbEntity.BankName = entity.BankName;
            dbEntity.BankAccountNo = entity.BankAccountNo;

            dbEntity.HasSeperateRetirementFund = entity.HasSeperateRetirementFund;
            dbEntity.RetirementFundACNo = entity.RetirementFundACNo;
            dbEntity.HasSeperateEmpInsurance = entity.HasSeperateEmpInsurance;
            dbEntity.EmpInsuranceACNo = entity.EmpInsuranceACNo;
            dbEntity.IsActive = entity.IsActive;
            dbEntity.InactiveDate = entity.InactiveDate;
            dbEntity.InactiveDateEng = entity.InactiveDateEng;
            //dbEntity.RemoteAreaId = entity.RemoteAreaId;

            
            PayrollDataContext.SubmitChanges();

            DeleteFromCache("GetEmployeeAddressReportResult");

            return true;
        }

        public int Delete(Branch entity,ref string msg)
        {
            Branch dbEntity = PayrollDataContext.Branches.SingleOrDefault(c => c.BranchId == entity.BranchId);
            if (dbEntity == null)
                return 0;
           

            return PayrollDataContext.DeleteBranch(dbEntity.BranchId,ref msg);            
        }

        public Branch GetById(int branchId)
        {
            return PayrollDataContext.Branches.SingleOrDefault(c => c.BranchId == branchId);
        }
        public static Branch GetBranchById(int branchId)
        {
            return PayrollDataContext.Branches.SingleOrDefault(c => c.BranchId == branchId);
        }
        public static Branch GetHeadOffice()
        {
            return PayrollDataContext.Branches.FirstOrDefault(c => c.IsHeadOffice != null && c.IsHeadOffice.Value);
        }
        public static String GetDepartmentListTextForBranch(int branchID)
        {
            var list =
                (
                from b in PayrollDataContext.BranchDepartments
                join d in PayrollDataContext.Departments on b.DepartmentID equals d.DepartmentId
                where b.BranchID == branchID
                orderby d.Name
                select d.Name
                ).ToList();

            return String.Join(",", list.ToArray());

        }
        public static List<Branch> GetBranchesByCompany(int companyId)
        {


            List<Branch> list = null;

            list = GetFromCache<List<Branch>>("GetBranchesByCompany" + companyId);
            if (list != null)
                return list;


            list = PayrollDataContext.Branches.Where(
                b => b.CompanyId == companyId).OrderBy(b => b.Name).ToList();

            SaveToCache<List<Branch>>("GetBranchesByCompany" + companyId, list);

            return list;



        }

        public static bool HasMultiBranch()
        {
            return GetBranchesByCompany(SessionManager.CurrentCompanyId).Count > 1 ;
        }

        //public List<Branch> GetAllBranches()
        //{
        //    return PayrollDataContext.Branches.ToList();
        //}

        internal static Branch getBranchByBranchName(string BranchName)
        {
            if (PayrollDataContext.Branches.Any(x => x.Name == BranchName))
            {
                return PayrollDataContext.Branches.FirstOrDefault(x => x.Name == BranchName);
            }
            return new Branch();
        }


        public static List<BranchDepartmentHistory> GetBranchTransferListNew(bool isDepartmentTransfer,
            int start, int limit, ref int total, string searchEmp, int empID, DateTime? fromDate, DateTime? toDate, int? eventType)
        {
            searchEmp = searchEmp.ToLower();

            
                
            //commented for new pages, uncomment later on
            //start = (start * limit);

            List<BranchDepartmentHistory> list =
                (
                from t in PayrollDataContext.BranchDepartmentHistories
                join e in PayrollDataContext.EEmployees on t.EmployeeId equals e.EmployeeId
                where (t.IsFirst == null || t.IsFirst == false)
                && (eventType == null || eventType == t.EventID)
                 && (searchEmp.Length == 0 || e.Name.ToLower().Contains(searchEmp))
                 && (empID == -1 || empID == e.EmployeeId)
                 //&& (
                 //   (isDepartmentTransfer == true && t.IsDepartmentTransfer != null && t.IsDepartmentTransfer.Value == true)
                 //   ||
                 //   (isDepartmentTransfer == false && (t.IsDepartmentTransfer == null || t.IsDepartmentTransfer == false))
                 //   )
                && (fromDate == new DateTime() || t.FromDateEng >= fromDate)
                && (toDate == new DateTime() || t.FromDateEng <= toDate)
                &&
                (
                    CommonManager.CompanySetting.WhichCompany != WhichCompany.NIBL
                    ||
                    (t.FromBranchId != null)
                )
                orderby t.FromDateEng descending
                select t
                ).ToList();
            //PayrollDataContext.BranchDepartmentHistories.Where(x=>x.FromBranchId != x.BranchId)
            //.OrderByDescending(x => x.FromDateEng)
            //.ToList();

            total = list.Count;
            //Paging code
            if (start >= 0 && limit > 0)
            {
                list = list.Skip(start).Take(limit).ToList();
            }

            EmployeeManager empMgr = new EmployeeManager();
            BranchManager bMgr = new BranchManager();
            DepartmentManager depMgr = new DepartmentManager();
            EEmployee emp = null;
            foreach (BranchDepartmentHistory item in list)
            {
                emp = empMgr.GetById(item.EmployeeId.Value);
                item.EmployeeName = emp.Name;
                item.INo = emp.EHumanResources[0].IdCardNo;

                //int years, months, days, hours;
                //int minutes, seconds, milliseconds;

                //NewHelper.GetElapsedTime(item.FromDateEng.Value, BLL.BaseBiz.GetCurrentDateAndTime(), out years, out months, out days, out hours, out minutes, out seconds, out milliseconds);
                //string text = "";
                //if (years != 0)
                //    text = " " + years + " Y";
                //if (months != 0)
                //    text += " " + months + " M";

                //if (text != "")
                //    text += " and ";

                //if (days != 0)
                //    text += days + " D";


                //item.TimeElapsed = text;

                //if (isDepartmentTransfer == false)
                {

                    if (item.FromBranchId != null)
                    {
                        item.FromBranch = bMgr.GetById(item.FromBranchId.Value).Name;
                    }

                    if (item.BranchId != null)
                    {
                        item.ToBranch = bMgr.GetById(item.BranchId.Value).Name;
                    }

                    if (item.DepeartmentId != null)
                    {
                        item.ToDepartment = depMgr.GetById(item.DepeartmentId.Value).Name;
                    }
                }
                //else
                {
                    if (item.FromDepartmentId != null)
                    {
                        item.FromDepartment = depMgr.GetById(item.FromDepartmentId.Value).Name;
                    }

                    //if (item.DepeartmentId != null)
                    //{
                    //    item.ToDepartment = depMgr.GetById(item.DepeartmentId.Value).Name;
                    //}
                }
            }

            return list;
        }


        public static List<GetTransferListResult> GetTranserList(
           int start, int limit, ref int total, string searchEmp, int empID, DateTime? fromDate, DateTime? toDate, int? eventType,bool showJoinedtransferAlso)
        {
            searchEmp = searchEmp.ToLower();

            List<GetTransferListResult> list = PayrollDataContext
                .GetTransferList(start, limit, searchEmp, empID, fromDate, toDate, eventType, showJoinedtransferAlso).ToList();


            if (list.Count > 0 && list[0].TotalRows != null)
                total = list[0].TotalRows.Value;

            return list;
        }

        public static List<GetServiceHistoryNewResult> GetNewServiceHistory(
           int start, int limit, ref int total, string searchEmp, int empID, DateTime? fromDate, DateTime? toDate, int? eventType)
        {
            searchEmp = searchEmp.ToLower();

            List<GetServiceHistoryNewResult> list = PayrollDataContext
                .GetServiceHistoryNew(start, limit, searchEmp, empID, fromDate, toDate, eventType).ToList();

            //foreach (GetServiceHistoryNewResult item in list)
            //{
            //    if (item.DateEng != null)
            //        item.Date = BLL.BaseBiz.GetAppropriateDate(item.DateEng);
            //}

            if (list.Count > 0 && list[0].TotalRows != null)
                total = list[0].TotalRows.Value;

            return list;
        }

        public static string IsActiveEmployeeAssociatedWithBranch(int branchId, DateTime? inactiveDate)
        {
            List<string> list = (from emp in PayrollDataContext.EEmployees
                         join eh in PayrollDataContext.EHumanResources on emp.EmployeeId equals eh.EmployeeId
                         where (emp.BranchId == branchId) &&
                          (emp.IsRetired == null || emp.IsRetired == false || (emp.IsRetired == true && eh.DateOfRetirementEng >= inactiveDate)) &&
                          (emp.IsResigned == null || emp.IsResigned == false || (emp.IsResigned == true && eh.DateOfResignationEng >= inactiveDate))
                         select emp.Name).ToList();

            if (list.Count > 0)
            {
                string msg = "";
                foreach (var item in list)
                {
                    if (msg == "")
                        msg = item;
                    else
                        msg += ", " + item;
                }
                msg += " employees are already active for this branch, change the branch for these employees before making the branch in-active.";

                return msg;
            }
            return "";
        }
    }
}
