﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using DAL;
using Utils;
using System.Xml.Linq;
using Utils.Calendar;
using BLL.Entity;
using BLL.BO;
namespace BLL.Manager
{

    public class LoanManager : BaseBiz
    {
        public static List<PDeduction> GetRepaymentLoanList()
        {
            return PayrollDataContext.PDeductions.Where(x => x.Calculation == DeductionCalculation.LOAN_Repayment).OrderBy(x => x.Title).ToList();
        }

        public static PDeduction GetPDeductionById(int deductionId)
        {
            return PayrollDataContext.PDeductions.SingleOrDefault(x => x.DeductionId == deductionId);
        }            

        public static List<PEmployeeDeduction> GetPEmployeeDeductions(int deductionId)
        {
            List<PEmployeeDeduction> list = PayrollDataContext.PEmployeeDeductions.Where(x => x.DeductionId == deductionId).OrderBy(x => x.EmployeeId).ToList();

            foreach (PEmployeeDeduction obj in list)
            {
                EEmployee emp = EmployeeManager.GetEmployeeById(obj.EmployeeId);
                obj.EmployeeName = emp.Name;
                obj.INo = emp.EHumanResources[0].IdCardNo;
            }

            return list;
        }

        public static ResponseStatus SaveLoanEmployeeDeduction(List<PEmployeeDeduction> list, ref int count, PDeduction deduction)
        {
            count = 0;
            ResponseStatus status = new ResponseStatus();

            foreach (PEmployeeDeduction empDeduction in list)
            {
                PEmployeeDeduction dbEmployeeDeduction = PayrollDataContext.PEmployeeDeductions.SingleOrDefault(
                (x => x.DeductionId == deduction.DeductionId && x.EmployeeId == empDeduction.EmployeeId));
                count += 1;

                if(dbEmployeeDeduction != null)
                {
                    LoanChangedHistory obj = new LoanChangedHistory();
                    obj.DeductionLoanId = deduction.DeductionId;
                    obj.EmployeeId = empDeduction.EmployeeId;
                    obj.MonthlyAmount = dbEmployeeDeduction.Amount;
                    obj.AdvanceLoanAmount = dbEmployeeDeduction.AdvanceLoanAmount;
                    obj.StartDate = dbEmployeeDeduction.StartingFrom;
                    obj.EndDate = dbEmployeeDeduction.LastPayment;
                    obj.ChangedBy = SessionManager.CurrentLoggedInUserID;
                    obj.ChangedDate = System.DateTime.Now;
                    PayrollDataContext.LoanChangedHistories.InsertOnSubmit(obj);

                    dbEmployeeDeduction.TotalInstallment = empDeduction.TotalInstallment;
                    dbEmployeeDeduction.Amount = empDeduction.Amount;
                    dbEmployeeDeduction.AdvanceLoanAmount = empDeduction.AdvanceLoanAmount;
                    dbEmployeeDeduction.InstallmentNo = empDeduction.InstallmentNo;
                    dbEmployeeDeduction.StartingFrom = empDeduction.StartingFrom;
                    dbEmployeeDeduction.StartingFromEng = empDeduction.StartingFromEng;
                    dbEmployeeDeduction.LastPayment = empDeduction.LastPayment;                    
                    dbEmployeeDeduction.LastPaymentEng = empDeduction.LastPaymentEng;
                    dbEmployeeDeduction.NoOfInstallments = empDeduction.NoOfInstallments;
                    dbEmployeeDeduction.LoanAccount = empDeduction.LoanAccount;
                    dbEmployeeDeduction.TakenOnEng = empDeduction.TakenOnEng;
                    dbEmployeeDeduction.TakenOn = empDeduction.TakenOn;
                    dbEmployeeDeduction.IsValid = true;
                }
                else
                {
                    PEmployeeDeduction obj = new PEmployeeDeduction();
                    obj.EmployeeId = empDeduction.EmployeeId;
                    obj.TotalInstallment = empDeduction.TotalInstallment;
                    obj.Amount = empDeduction.Amount;
                    obj.AdvanceLoanAmount = empDeduction.AdvanceLoanAmount;
                    obj.InstallmentNo = empDeduction.InstallmentNo;
                    obj.StartingFrom = empDeduction.StartingFrom;
                    obj.StartingFromEng = empDeduction.StartingFromEng;
                    obj.LastPayment = empDeduction.LastPayment;
                    obj.LastPaymentEng = empDeduction.LastPaymentEng;
                    obj.NoOfInstallments = empDeduction.NoOfInstallments;
                    obj.LoanAccount = empDeduction.LoanAccount;
                    obj.DeductionId = deduction.DeductionId;
                    obj.TakenOn = empDeduction.TakenOn;
                    obj.TakenOnEng = empDeduction.TakenOnEng;
                    obj.IsValid = true;

                    if (EmployeeManager.IsEmployeeIDExists(empDeduction.EmployeeId) == false)
                    {
                        status.ErrorMessage = string.Format("Employee ID \"{0}\" does not exists.", empDeduction.EmployeeId);
                        return status;
                    }

                    PayrollDataContext.PEmployeeDeductions.InsertOnSubmit(obj);
                }
            }

            UpdateChangeSet();

            return status;
        }


        public static List<GetLoanRepaymentResult> GetPEmployeeDeductionsList(int start, int limit, int deductionId,int ein)
        {
            //List<PEmployeeDeduction> list = PayrollDataContext.PEmployeeDeductions.Where(x => x.DeductionId == deductionId).OrderBy(x => x.EmployeeId).ToList();
            //foreach (var item in list)
            //{
            //    EEmployee emp = EmployeeManager.GetEmployeeById(item.EmployeeId);
            //    item.EmployeeName = emp.Name;

            //    if (item.StartingFrom != null)
            //    {
            //        string[] startDate = item.StartingFrom.Split('/');
            //        item.DateFrom = startDate[0].ToString() + " - " + DateHelper.GetMonthName(int.Parse(startDate[1].ToString()), IsEnglish).ToString();
            //    }

            //    if (item.LastPayment != null)
            //    {
            //        string[] endDate = item.LastPayment.Split('/');
            //        item.DateTo = endDate[0].ToString() + " - " + DateHelper.GetMonthName(int.Parse(endDate[1].ToString()), IsEnglish).ToString();
            //    }

            //    item.INo = emp.EHumanResources[0].IdCardNo;

            //}

            //return list;

            

            List<GetLoanRepaymentResult> list = PayrollDataContext.GetLoanRepayment(start, limit,deductionId,ein).ToList();
            foreach (var item in list)
            {
              //  EEmployee emp = EmployeeManager.GetEmployeeById(item.EmployeeId);
              //  item.EmployeeName = emp.Name;

                if (item.StartingFrom != null)
                {
                    string[] startDate = item.StartingFrom.Split('/');
                    item.DateFrom = startDate[0].ToString() + " - " + DateHelper.GetMonthName(int.Parse(startDate[1].ToString()), IsEnglish).ToString();
                }

                if (item.LastPayment != null)
                {
                    string[] endDate = item.LastPayment.Split('/');
                    item.DateTo = endDate[0].ToString() + " - " + DateHelper.GetMonthName(int.Parse(endDate[1].ToString()), IsEnglish).ToString();
                }

              //  item.INo = emp.EHumanResources[0].IdCardNo;

            }

            return list;
        }

        public static ResponseStatus SaveSILoanImport(List<PEmployeeDeduction> list, ref int count, PDeduction deduction)
        {
            count = 0;
            ResponseStatus status = new ResponseStatus();

            List<PEmployeeDeduction> newList = new List<PEmployeeDeduction>();

            foreach (PEmployeeDeduction empDeduction in list)
            {
                PEmployeeDeduction dbEmployeeDeduction = PayrollDataContext.PEmployeeDeductions.SingleOrDefault(
                (x => x.DeductionId == deduction.DeductionId && x.EmployeeId == empDeduction.EmployeeId));

                count += 1;

                if (dbEmployeeDeduction != null)
                {
                    LoanChangedHistory obj = new LoanChangedHistory();
                    obj.DeductionLoanId = deduction.DeductionId;
                    obj.EmployeeId = empDeduction.EmployeeId;
                    obj.MonthlyAmount = dbEmployeeDeduction.AdvanceLoanAmount / 12;
                    obj.AdvanceLoanAmount = dbEmployeeDeduction.AdvanceLoanAmount;
                    obj.StartDate = dbEmployeeDeduction.StartingFrom;
                    obj.EndDate = dbEmployeeDeduction.LastPayment;
                    obj.ChangedBy = SessionManager.CurrentLoggedInUserID;
                    obj.ChangedDate = System.DateTime.Now;
                    PayrollDataContext.LoanChangedHistories.InsertOnSubmit(obj);

                    dbEmployeeDeduction.AdvanceLoanAmount = empDeduction.AdvanceLoanAmount;
                    dbEmployeeDeduction.TakenOn = empDeduction.TakenOn;
                    dbEmployeeDeduction.TakenOnEng = empDeduction.TakenOnEng;
                    dbEmployeeDeduction.InterestRate = empDeduction.InterestRate;
                    dbEmployeeDeduction.MarketRate = empDeduction.MarketRate;
                    dbEmployeeDeduction.ToBePaidOver = empDeduction.ToBePaidOver;
                    dbEmployeeDeduction.StartingFrom = empDeduction.StartingFrom;
                    dbEmployeeDeduction.StartingFromEng = empDeduction.StartingFromEng;
                    dbEmployeeDeduction.LastPayment = empDeduction.LastPayment;
                    dbEmployeeDeduction.LastPaymentEng = empDeduction.LastPaymentEng;
                    dbEmployeeDeduction.PaymentTerms = empDeduction.PaymentTerms == null ? "1" : empDeduction.PaymentTerms;
                    dbEmployeeDeduction.AmountWithInitialInterest = empDeduction.AmountWithInitialInterest;
                    dbEmployeeDeduction.InstallmentAmount = empDeduction.InstallmentAmount;
                    dbEmployeeDeduction.Balance = empDeduction.Balance;
                    dbEmployeeDeduction.InterestAmount = empDeduction.InterestAmount;
                    dbEmployeeDeduction.NoOfInstallments = empDeduction.NoOfInstallments;

                    dbEmployeeDeduction.IsValid = true;

                    newList.Add(dbEmployeeDeduction);
                }
                else
                {
                    PEmployeeDeduction obj = new PEmployeeDeduction();
                    obj.EmployeeId = empDeduction.EmployeeId;
                    obj.DeductionId = deduction.DeductionId;
                    obj.AdvanceLoanAmount = empDeduction.AdvanceLoanAmount;
                    obj.TakenOn = empDeduction.TakenOn;
                    obj.TakenOnEng = empDeduction.TakenOnEng;
                    obj.InterestRate = empDeduction.InterestRate;
                    obj.MarketRate = empDeduction.MarketRate;
                    obj.ToBePaidOver = empDeduction.ToBePaidOver;
                    obj.StartingFrom = empDeduction.StartingFrom;
                    obj.StartingFromEng = empDeduction.StartingFromEng;
                    obj.LastPayment = empDeduction.LastPayment;
                    obj.LastPaymentEng = empDeduction.LastPaymentEng;
                    obj.AmountWithInitialInterest = empDeduction.AmountWithInitialInterest;
                    obj.InstallmentAmount = empDeduction.InstallmentAmount;
                    obj.Balance = empDeduction.Balance;
                    obj.InterestAmount = empDeduction.InterestAmount;
                    obj.NoOfInstallments = empDeduction.NoOfInstallments;

                    obj.PaymentTerms = "1"; //1  for Monthly
                    
                    obj.IsValid = true;

                    if (EmployeeManager.IsEmployeeIDExists(empDeduction.EmployeeId) == false)
                    {
                        status.ErrorMessage = string.Format("Employee ID \"{0}\" does not exists.", empDeduction.EmployeeId);
                        return status;
                    }

                    newList.Add(obj);
                    PayrollDataContext.PEmployeeDeductions.InsertOnSubmit(obj);

                    
                }
            }

            UpdateChangeSet();

            foreach (var item in newList)
            {
                PEmployeeDeduction obj = PayrollDataContext.PEmployeeDeductions.Single(x => x.EmployeeDeductionId == item.EmployeeDeductionId);
                obj.UniqueId = item.EmployeeDeductionId + "-" + ((int)item.AdvanceLoanAmount).ToString() //skip decimal portion as it may differ from database 
                       + item.TakenOn + item.InterestRate + item.MarketRate + item.ToBePaidOver + item.PaymentTerms + item.StartingFrom;

                PayrollDataContext.SubmitChanges();
            }


            return status;
        }
        
        public static List<PDeduction> GetSILoanList()
        {
            return PayrollDataContext.PDeductions
                .Where(x => x.Calculation == DeductionCalculation.LOAN
                && (x.IsEMI==false)).OrderBy(x => x.Title).ToList();
        }

        public static List<PDeduction> GetEMILoanList()
        {
            return PayrollDataContext.PDeductions
                .Where(x => x.Calculation == DeductionCalculation.LOAN
                && (x.IsEMI == null || x.IsEMI == true)).OrderBy(x => x.Title).ToList();
        }

        public static List<PEmployeeDeduction> GetSILoanImportList(int deductionId)
        {
            List<PEmployeeDeduction> list = PayrollDataContext.PEmployeeDeductions.Where(x => x.DeductionId == deductionId).OrderBy(x => x.EmployeeId).ToList();
            foreach (var item in list)
            {
                if(item.PaymentTerms != null)
                    item.PaymentTerms = new EMILoadPaymentTerm().GetPaymentTerm(int.Parse(item.PaymentTerms));
                EEmployee emp = EmployeeManager.GetEmployeeById(item.EmployeeId);
                item.EmployeeName = emp.Name;
                item.INo = emp.EHumanResources[0].IdCardNo;

                if (item.TakenOn != null)
                {
                    string[] takenDate = item.TakenOn.Split('/');
                    item.TakenDate = takenDate[0].ToString() + " - " + DateHelper.GetMonthName(int.Parse(takenDate[1].ToString()), IsEnglish).ToString();
                }

                if (item.StartingFrom != null)
                {
                    string[] startDate = item.StartingFrom.Split('/');
                    item.DateFrom = startDate[0].ToString() + " - " + DateHelper.GetMonthName(int.Parse(startDate[1].ToString()), IsEnglish).ToString();
                }

                if (item.LastPayment != null)
                {
                    string[] endDate = item.LastPayment.Split('/');
                    item.DateTo = endDate[0].ToString() + " - " + DateHelper.GetMonthName(int.Parse(endDate[1].ToString()), IsEnglish).ToString();
                }

               
            }

            return list;

        }

    }
}
    