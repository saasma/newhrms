﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using DAL;
using Utils;
using System.Xml.Linq;
using Utils.Calendar;
using BLL.Entity;
using BLL.BO;
namespace BLL.Manager
{

    public class MailMergeManager : BaseBiz
    {
        public static List<MailMergeTemplateHeader> GetAllHeaderList(int? type)
        {
            if (type == null)
                return PayrollDataContext.MailMergeTemplateHeaders.Where(x => x.Type > 0).OrderBy(y => y.Type).ToList();
            else
                return PayrollDataContext.MailMergeTemplateHeaders.OrderBy(y => y.Type).ToList();

        }



        public static Status InsertExcelRowsTemp(List<MailMergeExcelTemp> mmRows)
        {
            Status status = new Status();
            if (PayrollDataContext.MailMergeExcelTemps.Any())
            {
                PayrollDataContext.MailMergeExcelTemps.DeleteAllOnSubmit(PayrollDataContext.MailMergeExcelTemps.ToList());
            }
            PayrollDataContext.MailMergeExcelTemps.InsertAllOnSubmit(mmRows);
            PayrollDataContext.SubmitChanges();

            return status;
        }



        public static List<MailMergeExcelTemp> getMailMergeExcelTempRows()
        {

            List<MailMergeExcelTemp> list = PayrollDataContext.MailMergeExcelTemps.ToList();

            foreach (MailMergeExcelTemp item in list)
            {
                int ein = 0;

                if (int.TryParse(item.EmployeeID, out ein))
                {
                    EAddress add = PayrollDataContext.EAddresses.FirstOrDefault(x => x.EmployeeId == ein);
                    if (add != null)
                        item.Email = add.CIEmail;
                }
            }

            return list;
        }

        public static Status UpdateHeaders(List<MailMergeTemplateHeader> lines)
        {
            Status status = new Status();
            List<MailMergeTemplateHeader> dbEntity = new List<MailMergeTemplateHeader>();
            dbEntity = GetAllHeaderList(null);
            foreach (var items in dbEntity)
            {
                int index = items.Type - 1;
                items.HeaderText = lines[index].HeaderText;

            }
            PayrollDataContext.SubmitChanges();

            status.IsSuccess = true;
            return status;
        }


        public static Status InsertMailHistory(List<DAL.MailMerge> lines)
        {
            Status status = new Status();
            PayrollDataContext.MailMerges.InsertAllOnSubmit(lines);
            PayrollDataContext.SubmitChanges();

            status.IsSuccess = true;
            return status;
        }



        public static List<DAL.MailMerge> getAllEmailHistory()
        {
            EmployeeManager mgr = new EmployeeManager();
            List<MailMerge> iList = new List<MailMerge>();
            iList = PayrollDataContext.MailMerges.ToList();
            foreach (var item in iList)
            {
                item.CreatedByName = UserManager.GetUserByUserID(item.CreatedBy.Value).UserName;
                if (item.EmployeeId != null)
                    item.EmployeeName = mgr.GetById(item.EmployeeId.Value).Name;
            }

            return iList;
        }

        public static void InsertAllPayrollMessage(List<PayrollMessage> payrollMsg)
        {
            if (payrollMsg != null)
            {
                PayrollDataContext.PayrollMessages.InsertAllOnSubmit(payrollMsg);
            }
        }
    }

    public partial class MailMergeRow
    {
        public string EmployeeID { get; set; }
        public string EmployeeName { get; set; }
        public string Column1 { get; set; }
        public string Column2 { get; set; }
        public string Column3 { get; set; }
        public string Column4 { get; set; }
        public string Column5 { get; set; }
        public string Column6 { get; set; }
        public string Column7 { get; set; }
        public string Column8 { get; set; }
        public string Column9 { get; set; }
        public string Column10 { get; set; }
    }

    public partial class EmailBody
    {
        public string ToAddress { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public int EmployeeID { get; set; }
        public string EmpUserID { get; set; }
    }

}
    
