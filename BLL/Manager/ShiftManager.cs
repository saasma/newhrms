﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using DAL;
using Utils;
using System.Xml.Linq;
using Utils.Calendar;
using BLL.Entity;
using BLL.BO;

namespace BLL.Manager
{
    public class ShiftManager:BaseBiz
    {
        public static EWorkShift GetShiftById(int shiftId)
        {
            return PayrollDataContext.EWorkShifts.SingleOrDefault(x => x.WorkShiftId == shiftId);
        }
        public static EWorkShift GetShiftByName(string ShiftName)
        {
            return PayrollDataContext.EWorkShifts.SingleOrDefault(x => x.Name == ShiftName);
        }

        public static List<ColorList> GetAllColors()
        {
            return PayrollDataContext.ColorLists.OrderBy(x => x.ColorName).ToList();
        }

        public static EWorkShift GetEmployeeFirstShift(int employeeId)
        {
            EWorkShiftHistory item = 
                PayrollDataContext.EWorkShiftHistories.FirstOrDefault(x => x.EmployeeId == employeeId);

            if (item != null)
                return item.EWorkShift;
            return null;
        }
        public static Status InsertUpdateShift(EWorkShift entity, bool isInsert)
        {
            Status status = new Status();

            if(PayrollDataContext.EWorkShifts.Any(x=>x.IsDefault != null && x.IsDefault.Value && x.WorkShiftId != entity.WorkShiftId 
                && entity.IsDefault != null && entity.IsDefault.Value))
            {
                status.ErrorMessage = "Default shift already exists.";
                return status;
            }

            if (isInsert)
            {
                PayrollDataContext.EWorkShifts.InsertOnSubmit(entity);
            }
            else
            {
                EWorkShift dbEntity = PayrollDataContext.EWorkShifts.SingleOrDefault(x => x.WorkShiftId == entity.WorkShiftId);

                dbEntity.Name = entity.Name;
                dbEntity.ShortName = entity.ShortName;
                //dbEntity.ShiftStart = entity.ShiftStart;
                //dbEntity.ShiftEnd = entity.ShiftEnd;
                //dbEntity.LunchOut = entity.LunchOut;
                //dbEntity.LunchBack = entity.LunchBack;

                // if default shift is being removed then validate 
                //if (dbEntity.IsDefault != null && dbEntity.IsDefault.Value && entity.IsDefault == false)
                //{

                //}

                dbEntity.IsDefault = entity.IsDefault;
                dbEntity.Color = entity.Color;
                dbEntity.Type = entity.Type;
            }

            PayrollDataContext.SubmitChanges();
            return status;
        }

        public static List<EWorkShift> GetAllShifts()
        {
            return PayrollDataContext.EWorkShifts.OrderBy(x => x.Name).ToList();
        }


        public static Status DeleteEmployeeWorkShift(int workShiftHistoryID)
        {
            Status status = new Status();

            EWorkShiftHistory entity = PayrollDataContext.EWorkShiftHistories.SingleOrDefault(x => x.WorkShiftHistoryId == workShiftHistoryID);
            PayrollDataContext.EWorkShiftHistories.DeleteOnSubmit(entity);
            PayrollDataContext.SubmitChanges();
            return status;
        }


        public static Status DeleteShiftById(int workShiftId)
        {
            Status status = new Status();

            EWorkShift entity = PayrollDataContext.EWorkShifts.SingleOrDefault(x => x.WorkShiftId == workShiftId);

            PayrollDataContext.EWorkShifts.DeleteOnSubmit(entity);
            PayrollDataContext.SubmitChanges();

            return status;
        }

        public static EWorkShift GetWorkShiftById(int workShiftId)
        {
            EWorkShift entity = PayrollDataContext.EWorkShifts.SingleOrDefault(x => x.WorkShiftId == workShiftId);

            return entity;
        }

        public static AttendanceInOutTime GetShiftTime(int workShiftId,DateTime fromDate)
        {
            AttendanceInOutTime time = 
                PayrollDataContext.AttendanceInOutTimes
                .OrderByDescending(x => x.DateEng)
                .Where(x => x.DateEng >= fromDate && x.ShiftID == workShiftId)
                .FirstOrDefault();

            if (time == null)
                time = PayrollDataContext.AttendanceInOutTimes
                    .FirstOrDefault(x => x.ShiftID == workShiftId);

            return time;
        }

        public static object GetShiftGroups()
        {
            var shiftGroups = PayrollDataContext.ShiftGroups
                                .Join(PayrollDataContext.ShiftGroupEmployees,
                                s => s.ShiftGroupID, e => e.ShiftGroupId, (s, e)
                                => new { s, e })
                                .GroupBy(grp => new { grp.s.ShiftGroupID, grp.s.Name })

                                .Select(result => new { result.Key.ShiftGroupID, result.Key.Name, EmpNo=result.Count()});

            return shiftGroups.ToList();
        }

        public static List<GetEmployee_ShiftsResult> GetEmployee_Shifts(int? branchId, int? departmentId, int? employeeId,
               bool? type,int shiftId,int? LevelID)
        {
            List<GetEmployee_ShiftsResult> iList = new List<GetEmployee_ShiftsResult>();

            iList = PayrollDataContext.GetEmployee_Shifts(branchId, departmentId, employeeId, type, shiftId, LevelID).ToList();

            foreach (GetEmployee_ShiftsResult item in iList)
            {
                item.DefaultWorkShiftId = item.WorkShiftId;
            }

            return iList;   
        }

        


        public static void MoveResizeShift(int workShiftHistoryId,int newEmployeeId, DateTime start, DateTime end)
        {

            EWorkShiftHistory history = CommonManager.GetWorkShiftHistoryById(workShiftHistoryId);

            history.FromDateEng = new DateTime(start.Year, start.Month, start.Day);
            history.FromDate = GetAppropriateDate(history.FromDateEng.Value);
            history.EmployeeId = newEmployeeId;

            AttendanceInOutTime time = ShiftManager.GetShiftTime(history.WorkShiftId.Value, history.FromDateEng.Value);
            EWorkShift shift = ShiftManager.GetShiftById(history.WorkShiftId.Value);

            history.FromDateEngWithShiftTime = new DateTime(
                history.FromDateEng.Value.Year, history.FromDateEng.Value.Month, history.FromDateEng.Value.Day,
                time.OfficeInTime.Value.Hours, time.OfficeInTime.Value.Minutes, 0);

            if (history.ToDateEng != null)
            {
                history.ToDateEng = new DateTime(end.Year, end.Month, end.Day);
                history.ToDate = GetAppropriateDate(history.ToDateEng.Value);

                int hour, min;

                // for overnight set mid night as date could not touch other day
                if (shift.Type == 2)
                {
                    hour = 23;
                    min = 59;
                }
                else
                {
                    hour = time.OfficeOutTime.Value.Hours;
                    min = time.OfficeOutTime.Value.Minutes;
                }

                history.ToDateEngWithShiftTime = new DateTime(
                    history.ToDateEng.Value.Year, history.ToDateEng.Value.Month, history.ToDateEng.Value.Day,
                    hour, min, 0);
            }

            PayrollDataContext.SubmitChanges();


        }

        public static Status InsertUpdateShiftImport(List<EWorkShiftHistory> entity)
        {
            Status status = new Status();
            foreach (EWorkShiftHistory item in entity)
            {
                EWorkShiftHistory dbHistory = PayrollDataContext.EWorkShiftHistories.SingleOrDefault(x => x.WorkShiftHistoryId == item.WorkShiftHistoryId);
                if (dbHistory != null)
                {
                    CopyObject<EWorkShiftHistory>(item, ref dbHistory, "");
                    dbHistory.ToDate = item.ToDate;
                    dbHistory.ToDateEng = item.ToDateEng;
                    dbHistory.ToDateEngWithShiftTime = item.ToDateEngWithShiftTime;
                }
                else
                    PayrollDataContext.EWorkShiftHistories.InsertOnSubmit(item);
            }
            PayrollDataContext.SubmitChanges();
            return status;
        }

        public static Status InsertUpdateShift(bool isInsert,EWorkShiftHistory entity)
        {
            Status status = new Status();


            entity.FromDate = GetAppropriateDate(entity.FromDateEng.Value);
            if (entity.ToDateEng != null)
                entity.ToDate = GetAppropriateDate(entity.ToDateEng.Value);

            //EWorkShiftHistory sameDate = PayrollDataContext.EWorkShiftHistories
            //    .Where(x => x.EmployeeId.Value == entity.EmployeeId.Value && x.FromDate == entity.FromDate)
            //    .FirstOrDefault();
            
            //if (sameDate != null)
            //{
            //    PayrollDataContext.EWorkShiftHistories.DeleteOnSubmit(sameDate);
            //}
            if (isInsert)
                PayrollDataContext.EWorkShiftHistories.InsertOnSubmit(entity);
            else
            {
                EWorkShiftHistory dbHistory = PayrollDataContext.EWorkShiftHistories.FirstOrDefault(x => x.WorkShiftHistoryId
                    == entity.WorkShiftHistoryId);

                CopyObject<EWorkShiftHistory>(entity, ref dbHistory, "");

                dbHistory.ToDate = entity.ToDate;
                dbHistory.ToDateEng = entity.ToDateEng;
                dbHistory.ToDateEngWithShiftTime = entity.ToDateEngWithShiftTime;
            }

            PayrollDataContext.SubmitChanges();

            return status;
        }

        public static List<GetEmployeeShiftListResult> GetEmployeeShiftList(
            DateTime startDate, DateTime endDate,
          
            string employee)
        {
            int searchEmployeeId = 0;
            int.TryParse(employee, out searchEmployeeId);

            return PayrollDataContext.GetEmployeeShiftList(
                startDate, endDate,  searchEmployeeId).ToList();


        }

        public static Status SaveUpdateShiftSchedule(List<EWorkShiftHistory> empShiftLines,DateTime date)
        {
            Status status = new Status();

            string appropriateDate = GetAppropriateDate(date);
            DateTime engDate = GetEngDate(appropriateDate,IsEnglish);
            List<EWorkShiftHistory> history = new List<EWorkShiftHistory>();

            List<int> empIDList = empShiftLines.Select(x=>x.EmployeeId.Value).ToList();

            // 1. First delete saved entry on this date for the employee list
            List<EWorkShiftHistory> dbList = PayrollDataContext.EWorkShiftHistories
                .Where(x=> empIDList.Contains(x.EmployeeId.Value) && x.FromDate == appropriateDate).ToList();

            PayrollDataContext.EWorkShiftHistories.DeleteAllOnSubmit(dbList);

            for (int i = 0; i < empShiftLines.Count; i++)
            {
                EWorkShiftHistory source = empShiftLines[i];

                source.FromDate = appropriateDate;
                source.FromDateEng = engDate;
            }

            PayrollDataContext.EWorkShiftHistories.InsertAllOnSubmit(empShiftLines);

            PayrollDataContext.SubmitChanges();

            return status;
        }

        public static Status UpdateShiftSchedule(List<GetEmployee_ShiftsResult> empShiftLines)
        {
            Status status = new Status();



            for (int i = 0; i < empShiftLines.Count; i++)
            {

                EEmployee emp = PayrollDataContext.EEmployees.FirstOrDefault(x => x.EmployeeId == empShiftLines[i].EmployeeId);

                if (emp != null)
                {
                    emp.IsShiftTypeChangable = empShiftLines[i].ShiftType;
                }
            }

        

            PayrollDataContext.SubmitChanges();

            return status;
        }

        public static List<GetEmployee_ShiftsForManagerResult> GetEmployee_ShiftsForManager(int? branchId, int? departmentId, int? employeeId,
               bool? type, int shiftId)
        {
            List<GetEmployee_ShiftsForManagerResult> iList = new List<GetEmployee_ShiftsForManagerResult>();
            iList = PayrollDataContext.GetEmployee_ShiftsForManager(SessionManager.CurrentLoggedInEmployeeId, SessionManager.CurrentCompanyId, branchId, departmentId, type, shiftId).ToList();
            return iList;
        }

    }
}
