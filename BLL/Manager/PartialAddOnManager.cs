﻿using System.Collections.Generic;
using System.Linq;
using DAL;
using Utils.Calendar;
using System;
using BLL.BO;

namespace BLL.Manager
{
    public class PartialAddOnManager : BaseBiz
    {

        public static void DeleteIncomeHead(int payrollPeriodId, List<PIncome> incomeList)
        {
            if (CalculationManager.IsCalculationSaved(payrollPeriodId) != null)
            {
                return;
            }

            foreach (PIncome income in incomeList)
            {
                PartialPaidHeader dbHeader = PayrollDataContext.PartialPaidHeaders.SingleOrDefault(x => x.PayrollPeriodId == payrollPeriodId
                    && x.Type == income.Type && x.SourceId == income.SourceId);

                if (dbHeader != null)
                    PayrollDataContext.PartialPaidHeaders.DeleteOnSubmit(dbHeader);
            }

            PayrollDataContext.SubmitChanges();
        }

        public static string GetAddOnId(int addOnId)
        {
            return
                PayrollDataContext.AddOns.FirstOrDefault(x => x.AddOnId == addOnId).Name;
        }
        public static Status DeleteAddOnIncomeHead(int addOnId,int payrollPeriodId, List<PIncome> incomeList)
        {
            Status status = new Status();


            if (CalculationManager.IsCalculationSaved(payrollPeriodId) != null)
            {
                status.ErrorMessage = "Salary already saved, can not be deleted.";
                return status;
            }

            foreach (PIncome income in incomeList)
            {
                AddOnHeader dbHeader = PayrollDataContext.AddOnHeaders.SingleOrDefault(x => x.AddOnId == addOnId
                    && x.Type == income.Type && x.SourceId == income.SourceId);

                if (dbHeader != null)
                    PayrollDataContext.AddOnHeaders.DeleteOnSubmit(dbHeader);
            }

            PayrollDataContext.SubmitChanges();
            return status;
        }
        public static void DeleteDeductionHead(int payrollPeriodId, List<PDeduction> incomeList)
        {

            if (CalculationManager.IsCalculationSaved(payrollPeriodId) != null)
            {               
                return;
            }

            foreach (PDeduction income in incomeList)
            {
                PartialPaidHeader dbHeader = PayrollDataContext.PartialPaidHeaders.SingleOrDefault(x => x.PayrollPeriodId == payrollPeriodId
                    && x.Type == (int)CalculationColumnType.Deduction && x.SourceId == income.DeductionId);

                if (dbHeader != null)
                    PayrollDataContext.PartialPaidHeaders.DeleteOnSubmit(dbHeader);
            }

            PayrollDataContext.SubmitChanges();
        }

        public static void DeleteAddOnDeductionHead(int payrollPeriodId,int addOnId, List<PDeduction> incomeList)
        {

            if (CalculationManager.IsCalculationSaved(payrollPeriodId) != null)
            {
                return;
            }

            foreach (PDeduction income in incomeList)
            {
                AddOnHeader dbHeader = PayrollDataContext.AddOnHeaders.SingleOrDefault(x => x.AddOnId == addOnId
                    && x.Type == (int)CalculationColumnType.Deduction && x.SourceId == income.DeductionId);

                if (dbHeader != null)
                    PayrollDataContext.AddOnHeaders.DeleteOnSubmit(dbHeader);
            }

            PayrollDataContext.SubmitChanges();
        }
        public static void SaveIncome(int payrollPeriodId, List<PIncome> incomeList, List<PDeduction> deductions)//, List<PIncome> fixedIncomeList)
        {
            // generate incomes also
            List<PIncome> generationReqdIncomes = new List<PIncome>();
            string incomeIdList = "";


            foreach (PIncome income in incomeList)
            {

                if (income.Type == 1 && income.UseIncomeSettings)
                {
                    generationReqdIncomes.Add(income);
                    //if (incomeIdList == "")
                    //    incomeIdList = income.IncomeId.ToString();
                    //else
                    //    incomeIdList += "," + income.IncomeId.ToString();
                }

                if (!PayrollDataContext.PartialPaidHeaders.Any(x => x.PayrollPeriodId == payrollPeriodId && x.Type == income.Type && x.SourceId == income.SourceId))
                {

                    PayrollDataContext.PartialPaidHeaders.InsertOnSubmit(new PartialPaidHeader
                    {
                        PayrollPeriodId = payrollPeriodId,
                        Type = income.Type,
                        SourceId = income.SourceId,
                        Title = income.Title
                        ,
                        AddToRegularSalary = income.AddToRegularSalary
                    });
                }
                else
                {
                    PartialPaidHeader header = PayrollDataContext.PartialPaidHeaders.FirstOrDefault(
                        x => x.PayrollPeriodId == payrollPeriodId && x.Type == income.Type && x.SourceId == income.SourceId);
                    if (header != null)
                    {
                        header.AddToRegularSalary = income.AddToRegularSalary;
                    }
                }

            }

            foreach (PDeduction deduction in deductions)
            {

               

                if (!PayrollDataContext.PartialPaidHeaders.Any(x => x.PayrollPeriodId == payrollPeriodId && x.Type == (int)CalculationColumnType.Deduction
                    && x.SourceId == deduction.DeductionId))
                {

                    PayrollDataContext.PartialPaidHeaders.InsertOnSubmit(new PartialPaidHeader { PayrollPeriodId = payrollPeriodId, Type = (int)CalculationColumnType.Deduction,
                                                                                                 SourceId = deduction.DeductionId,
                                                                                                 Title = deduction.Title
                    });
                }


            }
            //foreach (PIncome income in fixedIncomeList)
            //{
            //    if (!PayrollDataContext.PartialPaidHeaders.Any(x => x.PayrollPeriodId == payrollPeriodId && x.Type == income.Type && x.SourceId == income.SourceId))
            //    {
            //        PayrollDataContext.PartialPaidHeaders.InsertOnSubmit(new PartialPaidHeader { PayrollPeriodId = payrollPeriodId, Type = income.Type, SourceId = income.SourceId, Title = income.Title });
            //    }
            //}

            PayrollDataContext.SubmitChanges();

            foreach(PIncome item in generationReqdIncomes)
            {
                PartialPaidHeader header = PayrollDataContext.PartialPaidHeaders
                    .FirstOrDefault(x=>x.PayrollPeriodId == payrollPeriodId && x.Type==1 
                        && x.SourceId == item.SourceId);
                if(header != null)
                {
                    if (incomeIdList == "")
                        incomeIdList = header.PartialPaidHeaderId.ToString();
                    else
                        incomeIdList += "," + header.PartialPaidHeaderId.ToString();
                }
            }



            PayrollDataContext.GenerateAddOnPaySalary(payrollPeriodId, "", incomeIdList);


        }

        public static List<TextValue> GetAddOnEmpList(int addOnId)
        {
            return
                (
                from a in PayrollDataContext.AddOnDetails
                join h in PayrollDataContext.AddOnHeaders on a.AddOnHeaderId equals h.AddOnHeaderId
                join e in PayrollDataContext.EEmployees on a.EmployeeId equals e.EmployeeId
                where h.AddOnId == addOnId
                orderby e.Name
                select new TextValue
                {
                    ID = e.EmployeeId,
                    Text = e.Name
                }

                ).Distinct().ToList();
        }

        public static List<TextValue> GetRetiredEmpList(int month,int year)
        {
            PayrollPeriod period = CommonManager.GetPayrollPeriod(month, year);
            if (period == null)
                return new List<TextValue>();

            return
                (
                from a in PayrollDataContext.CCalculationEmployees
                join c in PayrollDataContext.CCalculations on a.CalculationId equals c.CalculationId
                join e in PayrollDataContext.EEmployees on a.EmployeeId equals e.EmployeeId
                where c.PayrollPeriodId == period.PayrollPeriodId && a.IsRetiredOrResigned==true
                orderby e.Name
                select new TextValue
                {
                    ID = e.EmployeeId,
                    Text = e.Name
                }

                ).Distinct().ToList();
        }

        public static void SaveAddOnIncome(int payrollPeriodId,int addOnId,
            List<PIncome> incomeList, List<PDeduction> deductions,bool addTaxHeaderOnly,int? empId)//, List<PIncome> fixedIncomeList)
        {
            // generate incomes also
            List<PIncome> generationReqdIncomes = new List<PIncome>();
            string incomeIdList = "";


            foreach (PIncome income in incomeList)
            {

                if (income.Type == 1 && income.UseIncomeSettings)
                {
                    generationReqdIncomes.Add(income);
                }

                if (!PayrollDataContext.AddOnHeaders.Any(x => x.AddOnId == addOnId && x.Type == income.Type && x.SourceId == income.SourceId))
                {

                    PayrollDataContext.AddOnHeaders.InsertOnSubmit(new AddOnHeader
                    {
                        AddOnId = addOnId,
                        Type = income.Type,
                        SourceId = income.SourceId,
                        Title = income.Title
                        ,
                        AddToRegularSalary = income.AddToRegularSalary,
                        UseIncomeSetting = income.UseIncomeSettings
                    }
                        );
                }
                else
                {
                    AddOnHeader header = PayrollDataContext.AddOnHeaders.FirstOrDefault(
                        x => x.AddOnId == addOnId && x.Type == income.Type && x.SourceId == income.SourceId);
                    if (header != null)
                    {
                        header.AddToRegularSalary = income.AddToRegularSalary;
                        header.UseIncomeSetting = income.UseIncomeSettings;
                    }
                }


            }

            foreach (PDeduction deduction in deductions)
            {



                if (!PayrollDataContext.AddOnHeaders.Any(x => x.AddOnId == addOnId && x.Type == (int)CalculationColumnType.Deduction
                    && x.SourceId == deduction.DeductionId))
                {

                    PayrollDataContext.AddOnHeaders.InsertOnSubmit(new AddOnHeader
                    {
                        AddOnId = addOnId,
                        Type = (int)CalculationColumnType.Deduction,
                        SourceId = deduction.DeductionId,
                        Title = deduction.Title
                    });
                }


            }

            if (addTaxHeaderOnly == false)
            {
                // Add Default SST,TDS,CIT deductions by default with Income,Deduction PF
                if (!PayrollDataContext.AddOnHeaders.Any(x => x.AddOnId == addOnId && x.Type == (int)CalculationColumnType.DeductionCIT && x.SourceId == (int)CalculationColumnType.DeductionCIT))
                {
                    PayrollDataContext.AddOnHeaders.InsertOnSubmit(new AddOnHeader
                    {
                        AddOnId = addOnId,
                        Type = (int)CalculationColumnType.DeductionCIT,
                        SourceId = (int)CalculationColumnType.DeductionCIT,
                        Title = "CIT"
                    });
                }
            }

            if (!PayrollDataContext.AddOnHeaders.Any(x => x.AddOnId == addOnId && x.Type == (int)CalculationColumnType.SST && x.SourceId == (int)CalculationColumnType.SST))
            {
                PayrollDataContext.AddOnHeaders.InsertOnSubmit(new AddOnHeader
                {
                    AddOnId = addOnId,
                    Type = (int)CalculationColumnType.SST,
                    SourceId = (int)CalculationColumnType.SST,
                    Title = "SST"
                });
            }
            if (!PayrollDataContext.AddOnHeaders.Any(x => x.AddOnId == addOnId && x.Type == (int)CalculationColumnType.TDS && x.SourceId == (int)CalculationColumnType.TDS))
            {
                PayrollDataContext.AddOnHeaders.InsertOnSubmit(new AddOnHeader
                {
                    AddOnId = addOnId,
                    Type = (int)CalculationColumnType.TDS,
                    SourceId = (int)CalculationColumnType.TDS,
                    Title = "TDS"
                });
            }

            if (addTaxHeaderOnly == false)
            {

                if (!PayrollDataContext.AddOnHeaders.Any(x => x.AddOnId == addOnId && x.Type == (int)CalculationColumnType.IncomePF && x.SourceId == (int)CalculationColumnType.IncomePF))
                {
                    PayrollDataContext.AddOnHeaders.InsertOnSubmit(new AddOnHeader
                    {
                        AddOnId = addOnId,
                        Type = (int)CalculationColumnType.IncomePF,
                        SourceId = (int)CalculationColumnType.IncomePF,
                        Title = "Income PF"
                    });
                }

                if (!PayrollDataContext.AddOnHeaders.Any(x => x.AddOnId == addOnId && x.Type == (int)CalculationColumnType.DeductionPF && x.SourceId == (int)CalculationColumnType.DeductionPF))
                {
                    PayrollDataContext.AddOnHeaders.InsertOnSubmit(new AddOnHeader
                    {
                        AddOnId = addOnId,
                        Type = (int)CalculationColumnType.DeductionPF,
                        SourceId = (int)CalculationColumnType.DeductionPF,
                        Title = "Deduction PF"
                    });
                }
            }

            PayrollDataContext.SubmitChanges();

            //if (addTaxHeaderOnly == false)
            //{
            //    incomeIdList = GenerateIncomeAmountForMultiAddOn(payrollPeriodId, addOnId, empId, generationReqdIncomes, incomeIdList);
            //}

        }

        public static string GenerateIncomeAmountForMultiAddOn(int payrollPeriodId, int addOnId, int? empId, 
            List<PIncome> incomeList)
        {
            string incomeIdList = "";

            foreach (PIncome item in incomeList)
            {
                AddOnHeader header = PayrollDataContext.AddOnHeaders
                    .FirstOrDefault(x => x.AddOnId == addOnId && x.Type == 1
                        && x.SourceId == item.SourceId);
                if (header != null)
                {
                    if (header.Type == 1 && header.UseIncomeSetting != null && header.UseIncomeSetting.Value)
                    {
                        if (incomeIdList == "")
                            incomeIdList = header.AddOnHeaderId.ToString();
                        else
                            incomeIdList += "," + header.AddOnHeaderId.ToString();
                    }


                    
                }
            }



            PayrollDataContext.GenerateNewAddOnPaySalary(payrollPeriodId, addOnId, "", incomeIdList, empId);
            return incomeIdList;
        }

        public static void ClearValue(int payrollPeriodId, int type, int sourceId)
        {
             PartialPaidHeader header = PayrollDataContext.PartialPaidHeaders
                    .FirstOrDefault(x=>x.PayrollPeriodId == payrollPeriodId && x.Type==type 
                        && x.SourceId == sourceId);
             if (header != null)
             {

                 PayrollDataContext.ClearAddOnPaySalary(payrollPeriodId, header.PartialPaidHeaderId.ToString());
             }
        }

        public static void ClearAddOnValue(int payrollPeriodId,int addOnId, int type, int sourceId)
        {
            AddOnHeader header = PayrollDataContext.AddOnHeaders
                   .FirstOrDefault(x => x.AddOnId == addOnId && x.Type == type
                       && x.SourceId == sourceId);
            if (header != null)
            {

                PayrollDataContext.ClearNewAddOnPaySalary(addOnId, payrollPeriodId, header.AddOnHeaderId.ToString());
            }
        }
        public static void ClearAddOnValue(int payrollPeriodId, int addOnId, int addOnheaderId)
        {
            //AddOnHeader header = PayrollDataContext.AddOnHeaders
            //       .FirstOrDefault(x => x.AddOnId == addOnId && x.Type == type
            //           && x.SourceId == sourceId);
            //if (header != null)
            {

                PayrollDataContext.ClearNewAddOnPaySalary(addOnId, payrollPeriodId, addOnheaderId.ToString());
            }
        }
        public static List<PartialPaidDetail> GetAddOnDetail(int payrollPeriodId)
        {
            List<PartialPaidDetail> list =
                (
                from h in PayrollDataContext.PartialPaidHeaders
                join d in PayrollDataContext.PartialPaidDetails on h.PartialPaidHeaderId equals d.PartialPaidHeaderId
                where h.PayrollPeriodId == payrollPeriodId
                select d
                ).ToList();

            List<PartialTaxPaid> otherList = PayrollDataContext.PartialTaxPaids.Where(x => x.PayrollPeriodId == payrollPeriodId).ToList();

            PartialPaidHeader pfHeader = new PartialPaidHeader { Title="Income PF", Type = (int)CalculationColumnType.IncomePF, SourceId = (int)CalculationColumnType.IncomePF };
            PartialPaidHeader dedPfHeader = new PartialPaidHeader { Title = "Deduction PF", Type = (int)CalculationColumnType.DeductionPF, SourceId = (int)CalculationColumnType.DeductionPF };
           
            PartialPaidHeader citHeader = new PartialPaidHeader { Title = "CIT", Type = (int)CalculationColumnType.DeductionCIT, SourceId = (int)CalculationColumnType.DeductionCIT };

            PartialPaidHeader tdsHeader = new PartialPaidHeader { Title = "TDS", Type = (int)CalculationColumnType.TDS, SourceId = (int)CalculationColumnType.TDS };

            // add PF,CIT & TDS also
            foreach (PartialTaxPaid item in otherList)
            {
                list.Add(new PartialPaidDetail { 
                    Amount = Convert.ToDecimal( item.PFAmount),
                     EmployeeId = item.EmployeeId,
                      PartialPaidHeader = pfHeader
                });
                list.Add(new PartialPaidDetail
                {
                    Amount = Convert.ToDecimal(item.DeductionPFAmount),
                    EmployeeId = item.EmployeeId,
                    PartialPaidHeader = dedPfHeader
                });
                list.Add(new PartialPaidDetail
                {
                    Amount = Convert.ToDecimal(item.CITAmount),
                    EmployeeId = item.EmployeeId,
                    PartialPaidHeader = citHeader
                });
                list.Add(new PartialPaidDetail
                {
                    Amount = Convert.ToDecimal(item.PaidAmount),
                    EmployeeId = item.EmployeeId,
                    PartialPaidHeader = tdsHeader
                });
            }

            return list;
        }

        public static List<AddOnDetail> GetNewAddOnDetail(int addonId)
        {
            List<AddOnDetail> list =
                (
                from h in PayrollDataContext.AddOnHeaders
                join d in PayrollDataContext.AddOnDetails on h.AddOnHeaderId equals d.AddOnHeaderId
                where h.AddOnId == addonId
                select d
                ).ToList();

            //List<PartialTaxPaid> otherList = PayrollDataContext.PartialTaxPaids.Where(x => x.PayrollPeriodId == payrollPeriodId).ToList();

            //PartialPaidHeader pfHeader = new PartialPaidHeader { Title = "Income PF", Type = (int)CalculationColumnType.IncomePF, SourceId = (int)CalculationColumnType.IncomePF };
            //PartialPaidHeader dedPfHeader = new PartialPaidHeader { Title = "Deduction PF", Type = (int)CalculationColumnType.DeductionPF, SourceId = (int)CalculationColumnType.DeductionPF };

            //PartialPaidHeader citHeader = new PartialPaidHeader { Title = "CIT", Type = (int)CalculationColumnType.DeductionCIT, SourceId = (int)CalculationColumnType.DeductionCIT };

            //PartialPaidHeader tdsHeader = new PartialPaidHeader { Title = "TDS", Type = (int)CalculationColumnType.TDS, SourceId = (int)CalculationColumnType.TDS };


            return list;
        }

        public static List<CalcGetHeaderListResult> GetAddOnHeaderListForSelection(int payrollPeriodId)
        {
            List<PartialPaidHeader> headerList =
                PayrollDataContext.PartialPaidHeaders.Where(x => x.PayrollPeriodId == payrollPeriodId).OrderBy(x => x.Title).ToList();

            List<CalcGetHeaderListResult> selectedList = new List<CalcGetHeaderListResult>();
            foreach (PartialPaidHeader header in headerList)
            {
                selectedList.Add(new CalcGetHeaderListResult
                {
                    Type = header.Type,
                    SourceId = header.SourceId,
                    HeaderName = header.Title,
                    AddToRegularSalary = (header.AddToRegularSalary == null ? false : header.AddToRegularSalary.Value)
                });
            }

            return CalculationValue.SortHeaders(selectedList, new Dictionary<int, int>(), new Dictionary<int, int>());
        }
        public static List<CalcGetHeaderListResult> GetAddOnHeaderListNew(int addOnId)
        {
            List<AddOnHeader> headerList =
                PayrollDataContext.AddOnHeaders.Where(x => x.AddOnId == addOnId).OrderBy(x => x.Title).ToList();

            List<CalcGetHeaderListResult> selectedList = new List<CalcGetHeaderListResult>();
            foreach (AddOnHeader header in headerList)
            {
                selectedList.Add(new CalcGetHeaderListResult
                {
                    Type = header.Type,
                    SourceId = header.SourceId,
                    HeaderName = header.Title
                    ,
                    AddToRegularSalary = (header.AddToRegularSalary == null ? false : header.AddToRegularSalary.Value)
                     ,
                    UseIncomeSetting = (header.UseIncomeSetting == null ? false : header.UseIncomeSetting.Value)
                });
            }

            return CalculationValue.SortHeaders(selectedList, new Dictionary<int, int>(), new Dictionary<int, int>());
        }
        public static List<CalcGetHeaderListResult> GetAddOnHeaderList(int payrollPeriodId)
        {
            List<PartialPaidHeader> headerList =
                PayrollDataContext.PartialPaidHeaders.Where(x => x.PayrollPeriodId == payrollPeriodId && x.Type != (int)CalculationColumnType.Deduction).OrderBy(x => x.Title).ToList();

            List<CalcGetHeaderListResult> selectedList = new List<CalcGetHeaderListResult>();
            foreach (PartialPaidHeader header in headerList)
            {
                selectedList.Add(new CalcGetHeaderListResult
                {
                    Type = header.Type,
                    SourceId = header.SourceId, 
                    HeaderName = header.Title,
                    AddToRegularSalary = (header.AddToRegularSalary == null ? false : header.AddToRegularSalary.Value)
                });
            }

            return CalculationValue.SortHeaders(selectedList, new Dictionary<int, int>(), new Dictionary<int, int>());
        }
        public static List<CalcGetHeaderListResult> GetNewAddOnHeaderList(int addOnId)
        {
            List<AddOnHeader> headerList =
                PayrollDataContext.AddOnHeaders.Where(x => x.AddOnId == addOnId && x.Type != (int)CalculationColumnType.Deduction).OrderBy(x => x.Title).ToList();

            List<CalcGetHeaderListResult> selectedList = new List<CalcGetHeaderListResult>();
            foreach (AddOnHeader header in headerList)
            {
                selectedList.Add(new CalcGetHeaderListResult { Type = header.Type, SourceId = header.SourceId, HeaderName = header.Title
                 ,AddToRegularSalary = header.AddToRegularSalary// == null ?  false : header.AddToRegularSalary.Value
                ,
                                                               UseIncomeSetting = header.UseIncomeSetting == null ? false : header.UseIncomeSetting.Value
                });
            }

            return CalculationValue.SortHeaders(selectedList, new Dictionary<int, int>(), new Dictionary<int, int>());
        }
        public static void SetIncomeList(int payrollPeriodId, ref List<PIncome> availableList, ref List<PIncome> availableFixedList, ref List<CalcGetHeaderListResult> selectedList
            , ref List<CalcGetHeaderListResult> selectedDeductionList)
        {
            List<PIncome> totalIncomes = GetIncomeListForAddOnPay();


            List<PartialPaidHeader> headerList =
               PayrollDataContext.PartialPaidHeaders
               .Where(x => x.PayrollPeriodId == payrollPeriodId && x.Type != (int)CalculationColumnType.Deduction).OrderBy(x => x.Title).ToList();

            selectedList = GetAddOnHeaderList(payrollPeriodId);

            selectedDeductionList = 
                (
                    from h in PayrollDataContext.PartialPaidHeaders
                    where h.Type==(int)CalculationColumnType.Deduction && h.PayrollPeriodId == payrollPeriodId
                    select new CalcGetHeaderListResult
                    {
                         Type = h.Type,
                         SourceId = h.SourceId,
                         HeaderName = h.Title
                    }
                ).ToList();

            availableList = new List<PIncome>();
            //select normal incomes
            foreach (PIncome income in totalIncomes.Where(x => x.Type != x.SourceId).ToList())
            {
                if (!headerList.Any(x => x.Type == income.Type && x.SourceId == income.SourceId))
                {
                    PartialPaidHeader header = headerList.FirstOrDefault(x => x.Type == income.Type && x.SourceId == income.SourceId);

                    if (header != null && header.AddToRegularSalary != null && header.AddToRegularSalary.Value)
                        income.AddToRegularSalary = true;

                    availableList.Add(income);
                }
            }

            availableFixedList = new List<PIncome>();
            //select normal incomes
            foreach (PIncome income in totalIncomes.Where(x => x.Type == x.SourceId).ToList())
            {
                if (!headerList.Any(x => x.Type == income.Type && x.SourceId == income.SourceId))
                {
                    availableFixedList.Add(income);
                }
            }

        }


        public static void SetIncomeList(int payrollPeriod,int addOnId, ref List<PIncome> availableList, ref List<PIncome> availableFixedList, ref List<CalcGetHeaderListResult> selectedList
            , ref List<CalcGetHeaderListResult> selectedDeductionList)
        {
            List<PIncome> totalIncomes = GetIncomeListForAddOnPay();


            List<AddOnHeader> headerList =
               PayrollDataContext.AddOnHeaders
               .Where(x => x.AddOnId == addOnId && x.Type != (int)CalculationColumnType.Deduction).OrderBy(x => x.Title).ToList();

            selectedList = GetNewAddOnHeaderList(addOnId);

            selectedDeductionList =
                (
                    from h in PayrollDataContext.AddOnHeaders
                    where h.Type == (int)CalculationColumnType.Deduction && h.AddOnId == addOnId
                    select new CalcGetHeaderListResult
                    {
                        Type = h.Type,
                        SourceId = h.SourceId,
                        HeaderName = h.Title
                    }
                ).ToList();

            availableList = new List<PIncome>();
            //select normal incomes
            //foreach (PIncome income in totalIncomes.Where(x => x.Type != x.SourceId).ToList())
            //{
            //    if (!headerList.Any(x => x.Type == income.Type && x.SourceId == income.SourceId))
            //    {
            //        availableList.Add(income);
            //    }
            //}

            //select normal incomes
            foreach (PIncome income in totalIncomes.Where(x => x.Type != x.SourceId).ToList())
            {
                if (!headerList.Any(x => x.Type == income.Type && x.SourceId == income.SourceId))
                {
                    AddOnHeader header = headerList.FirstOrDefault(x => x.Type == income.Type && x.SourceId == income.SourceId);

                    if (header != null && header.AddToRegularSalary != null && header.AddToRegularSalary.Value)
                        income.AddToRegularSalary = header.AddToRegularSalary.Value;
                    else
                        income.AddToRegularSalary = null;

                    if (header != null && header.UseIncomeSetting != null && header.UseIncomeSetting.Value)
                        income.UseIncomeSettings = true;

                    availableList.Add(income);
                }
            }

            availableFixedList = new List<PIncome>();
            //select normal incomes
            foreach (PIncome income in totalIncomes.Where(x => x.Type == x.SourceId).ToList())
            {
                if (!headerList.Any(x => x.Type == income.Type && x.SourceId == income.SourceId))
                {
                    availableFixedList.Add(income);
                }
            }

        }

        public static List<PIncome> GetIncomeListForAddOnPay()
        {
            // Load Income
            List<PIncome> incomes = PayManager.GetIncomeListByCompany(SessionManager.CurrentCompanyId).OrderBy(x => x.Title).ToList();

            foreach (PIncome inc in incomes)
            {
                inc.Type = 1;
                inc.SourceId = inc.IncomeId;
            }


            List<PIncome> fixedIncomes = new List<PIncome>();
            fixedIncomes.Add(new PIncome
            {
                Type = (int)CalculationColumnType.IncomeLeaveEncasement,
                SourceId = (int)CalculationColumnType.IncomeLeaveEncasement,
                Title = "Leave Encashment"
            });


            // Load dynamic income like Leave Encashment & Allowance & other for HPL
            //switch (CommonManager.CompanySetting.WhichCompany)
            //{
            //    case WhichCompany.HPL:
            //        fixedIncomes.Add(new PIncome
            //        {
            //            Type = (int)CalculationColumnType.IncomeSiteAllowance,
            //            SourceId = (int)CalculationColumnType.IncomeSiteAllowance,
            //            Title = "Head work allowance"
            //        });
            //        fixedIncomes.Add(new PIncome
            //        {
            //            Type = (int)CalculationColumnType.IncomeSnacksAllowance,
            //            SourceId = (int)CalculationColumnType.IncomeSnacksAllowance,
            //            Title = "Snacks allowance"
            //        });
            //        fixedIncomes.Add(new PIncome
            //        {
            //            Type = (int)CalculationColumnType.IncomeCompensationAllowance,
            //            SourceId = (int)CalculationColumnType.IncomeCompensationAllowance,
            //            Title = "Comp. allowance"
            //        });
            //        fixedIncomes.Add(new PIncome
            //        {
            //            Type = (int)CalculationColumnType.IncomeMealAllowance,
            //            SourceId = (int)CalculationColumnType.IncomeMealAllowance,
            //            Title = "Meal allowance"
            //        });fixedIncomes.Add(new PIncome
            //        {
            //            Type = (int)CalculationColumnType.IncomeTADA,
            //            SourceId = (int)CalculationColumnType.IncomeTADA,
            //            Title = "TADA"
            //        });
            //        fixedIncomes.Add(new PIncome
            //        {
            //            Type = (int)CalculationColumnType.IncomeVehicleAllowance,
            //            SourceId = (int)CalculationColumnType.IncomeVehicleAllowance,
            //            Title = "Vehicle allowance"
            //        });

            //        fixedIncomes.Add(new PIncome
            //        {
            //            Type = (int)CalculationColumnType.HPLOvertimeIncomeTotal,
            //            SourceId = (int)CalculationColumnType.HPLOvertimeIncomeTotal,
            //            Title = "Total overtime"
            //        });
            //        break;
            //}

            fixedIncomes = fixedIncomes.OrderBy(x => x.Title).ToList();

            incomes.AddRange(fixedIncomes);

            return incomes;

        }



    }
}
