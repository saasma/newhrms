﻿using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using DAL;
using DAL.Entity;
using Utils.Helper;
using System.Xml;
using System;
using BLL.Entity;
using Utils.Calendar;
using BLL.BO;
using System.Linq.Expressions;
using Utils;
using System.Web;

namespace BLL.Manager
{
    public class LeaveAttendanceManager : BaseBiz
    {
        enum LeaveRequestFrom {
            LeaveRequest = 0,
            LeaveApproval

        }
        public const string Unpaid_Leave = "Unpaid Leave";
        public const string HalfDayPostFix = "/2";
        public const string HourlyLeaveSeparator = "-";
        public
        static string[][] DefaultLeaves = new string[][]
                                                  {
                                                      //Abbrevation Desc Pay Present
                                                      new string[] {"P", "Present","1","1"},
                                                      new string[] {"UPL", Unpaid_Leave,"0","0"},
                                                      new string[] {"UPL" + HalfDayPostFix, "Unpaid Half Leave","0.5","0.5"},
                                                      new string[] {"ABS", "Absent","0","0"}
                                                  };

        public static bool IsHourlyLeave(string value)
        {
            return value.Contains(HourlyLeaveSeparator);
        }

        public static AttendanceSkipLateDeductDate GetDate(int id)
        {
            return PayrollDataContext.AttendanceSkipLateDeductDates
               .FirstOrDefault(x => x.ID == id);
        }
        public static EmployeeCustomGratuityDate GetGratuityDate(int ein)
        {
            return PayrollDataContext.EmployeeCustomGratuityDates
               .FirstOrDefault(x => x.EIN == ein);
        }
        public static Status SaveUpdateSkipDate(AttendanceSkipLateDeductDate entity)
        {
            Status status = new Status();

            AttendanceSkipLateDeductDate dbEntity = PayrollDataContext.AttendanceSkipLateDeductDates
                .FirstOrDefault(x => x.DateEng == entity.DateEng && x.ID != entity.ID);

            if(dbEntity != null)
            {
                status.ErrorMessage = "Date already exists, if you want to replace, delete other one on this date.";
                return status;
            }

            dbEntity = PayrollDataContext.AttendanceSkipLateDeductDates
                .FirstOrDefault(x=>x.ID==entity.ID);

            if (dbEntity == null)
            {
                entity.HasBranch = entity.AttendanceSkipBranches.Count > 0;

                entity.Date = GetAppropriateDate(entity.DateEng);
                PayrollDataContext.AttendanceSkipLateDeductDates.InsertOnSubmit(entity);
            }
            else
            {
                dbEntity.AttendanceSkipBranches.Clear();
                dbEntity.AttendanceSkipBranches.AddRange(entity.AttendanceSkipBranches);

                dbEntity.HasBranch = dbEntity.AttendanceSkipBranches.Count > 0;

                dbEntity.DateEng = entity.DateEng;
                dbEntity.Date = GetAppropriateDate(dbEntity.DateEng);
                dbEntity.Comment = entity.Comment;
            }

            SaveChangeSet();

            return status;
        }

        public static List<Report_HR_GetRetirementLeaveDetailsResult> GetRetirementLeaveDetails(PayrollPeriod payrollPeriod, int employeeId)
        {
            int? startid = null; int? endId = null;
            int? lastYearOrYearOpeningPayrollPeriodId = null;
            DateTime? yearStart = null, yearEnd = null;
            DateTime? startDateLeaveTaken = null; DateTime? endDateLeaveTaken = null; int? currentLeavePeriodIdForAccural = null;


            // leave month staring with Jan or Baisakh
            if (SessionManager.CurrentCompany.LeaveStartMonth == 1)
            {
                CommonManager.SetPeriodForBaisakhJanuaaryYearlyLeavePeriod(payrollPeriod.Year.Value,
                        ref startid, ref endId, ref startDateLeaveTaken,
                        ref endDateLeaveTaken, ref currentLeavePeriodIdForAccural, ref lastYearOrYearOpeningPayrollPeriodId, ref yearStart, ref yearEnd, employeeId);

            }
            else
            {
                CommonManager.SetPeriodForMiddleSharwanJulyYearlyLeavePeriod(payrollPeriod.FinancialDate.FinancialDateId,
                     ref startid, ref endId, ref startDateLeaveTaken,
                     ref endDateLeaveTaken, ref currentLeavePeriodIdForAccural, ref lastYearOrYearOpeningPayrollPeriodId, ref yearStart, ref yearEnd, employeeId);

            }

            endId = payrollPeriod.PayrollPeriodId;

            List<Report_HR_GetRetirementLeaveDetailsResult> leaveList
                = BLL.BaseBiz.PayrollDataContext.Report_HR_GetRetirementLeaveDetails(startid, endId, employeeId, lastYearOrYearOpeningPayrollPeriodId,
                SessionManager.CurrentCompanyId, yearStart, yearEnd).ToList();
            return leaveList;
        }

        public static List<Report_HR_GetRetirementLeaveDetailsResult> GetRetireReHireLeaveDetails(PayrollPeriod payrollPeriod, int employeeId)
        {
            List<RetireRehireLeaveDetail> list = PayrollDataContext.RetireRehireLeaveDetails.Where(x =>
                x.EmployeeId == employeeId && x.PayrollPeriodId == payrollPeriod.PayrollPeriodId).ToList();

            List<Report_HR_GetRetirementLeaveDetailsResult> leaveList = new List<Report_HR_GetRetirementLeaveDetailsResult>();

            foreach (var item in list)
            {
                leaveList.Add(new Report_HR_GetRetirementLeaveDetailsResult
                {
                    LeaveTypeId = item.LeaveTypeId,
                    Name = LeaveAttendanceManager.GetLeaveTypeById(item.LeaveTypeId).Title,




                    LastYearOpening = item.LastYearOpening,
                    CurrentYearAccured = (decimal)item.CurrentYearAccured,

                    CurrentYearEligible = item.CurrentYearEligible,
                    Taken = item.Taken,
                    Adjusted = (decimal)item.Adjusted,
                    ReminingLeave = item.ReminingLeave,

                    EncashTotal = item.EncashTotal,
                    Encased = item.Encased,
                    RetirementEncash = item.RetirementEncash

                });
            }

            return leaveList;
        }
        public static Status SaveUpdateGratuityStartDate(EmployeeCustomGratuityDate entity)
        {
            Status status = new Status();

            EmployeeCustomGratuityDate dbEntity = PayrollDataContext.EmployeeCustomGratuityDates
                .FirstOrDefault(x => x.EIN == entity.EIN);

        
            if (dbEntity == null)
            {
               
                PayrollDataContext.EmployeeCustomGratuityDates.InsertOnSubmit(entity);
            }
            else
            {

               
                dbEntity.DateEng = entity.DateEng;
                dbEntity.Comment = entity.Comment;
            }

            SaveChangeSet();

            return status;
        }
        public static void DeleteSkipDate(AttendanceSkipLateDeductDate entity)
        {
            AttendanceSkipLateDeductDate dbEntity = PayrollDataContext.AttendanceSkipLateDeductDates
                .FirstOrDefault(x => x.ID == entity.ID);

            PayrollDataContext.AttendanceSkipLateDeductDates.DeleteOnSubmit(dbEntity);

            PayrollDataContext.SubmitChanges();
        }
        public static void DeleteGratuityDate(EmployeeCustomGratuityDate entity)
        {
            EmployeeCustomGratuityDate dbEntity = PayrollDataContext.EmployeeCustomGratuityDates
                .FirstOrDefault(x => x.EIN == entity.EIN);

            PayrollDataContext.EmployeeCustomGratuityDates.DeleteOnSubmit(dbEntity);

            PayrollDataContext.SubmitChanges();
        }
        public static List<AttendanceSkipLateDeductDate> GetSkipLateDates(DateTime? start, DateTime? end)
        {
            return
                PayrollDataContext.AttendanceSkipLateDeductDates
                .Where(x => start == null || (x.DateEng >= start && x.DateEng <= end))
                .OrderByDescending(x => x.DateEng).ToList();
        }

        public static string DaysOrHourTitle
        {
            set { }
            get
            {
                if (CommonManager.CalculationConstant.CompanyHasHourlyLeave != null 
                    && CommonManager.CalculationConstant.CompanyHasHourlyLeave.Value)
                    return "Hours";
                return "Days";
            }
        }

        public static bool IsChildLeave(int leaveTypeId,ref int parentId)
        {
            LLeaveType leave = PayrollDataContext.LLeaveTypes.FirstOrDefault(x => x.LeaveTypeId == leaveTypeId);
            if (leave.IsChildLeave != null && leave.IsChildLeave.Value)
            {
                parentId = leave.ParentLeaveTypeId.Value;
                return true;
            }
            else
            {
                parentId = 0;
                return false;
            }
        }
        public static void UpdateLeaveOrder(int leaveTypeId, int order,float minRequest,float maxRequest,int absentOrder)
        {
            LeaveAttendanceManager p = new LeaveAttendanceManager();
            LLeaveType income = p.GetLeaveType(leaveTypeId);
            if (income != null)
            {
                income.Order = order;
                income.SingleMinRequest = minRequest;
                income.SingleMaxRequest = maxRequest;
                income.AbsentMarkingOrder = absentOrder;
            }
            UpdateChangeSet();
        }

        public static bool IsLeaveUsingTeam()
        {
            return PayrollDataContext.LeaveApprovalEmployees.Any();
        }

      
        public static List<LLeaveType> GetLeaveGroupList()
        {
            return
                PayrollDataContext.LLeaveTypes.Where(x => x.IsParentGroupLeave == true)
                .OrderBy(x => x.Title).ToList();
        }

        /// <summary>
        /// Method to change the leave request status by Leave Approval Employee
        /// </summary>
        /// <param name="leaveRequest"></param>
        /// <param name="IsNewRequest"></param>
        /// <returns></returns>
        public static ResponseStatus ApproveSelectedLeaveRequests(List<int> list, ref int count)
        {
            ResponseStatus status = new ResponseStatus();

            count = 0;
            SetConnectionPwd(PayrollDataContext.Connection);
            PayrollDataContext.Transaction = PayrollDataContext.Connection.BeginTransaction();
            try
            {

                for (int i = 0; i < list.Count; i++)
                {
                    LeaveRequest leaveRequest = GetLeaveRequestById(list[i]);
                    if (leaveRequest == null || leaveRequest.Status != (int)LeaveRequestStatusEnum.Request)
                        continue;

                    EEmployee emp = EmployeeManager.GetEmployeeById(leaveRequest.EmployeeId);

                    //bool CountHolidayInBetweenLeave = CommonManager.CompanySetting.LeaveRequestCountHolidayInBetweenLeave;
                    bool onlyCountPublicHolidayAsLeaveWeeklySaturdayIsHoliday = false;
                    bool onlyCountWeeklySaturdayAsLeavePublicHolidayIsHoliday = false;
                    bool CountHolidayInBetweenLeave = LeaveAttendanceManager.GetLeaveRequestCountHolidayAsLeaveSetting(leaveRequest.LeaveTypeId, ref onlyCountPublicHolidayAsLeaveWeeklySaturdayIsHoliday,ref onlyCountWeeklySaturdayAsLeavePublicHolidayIsHoliday);

                    HolidayManager hmanager = new HolidayManager();
                    List<GetHolidaysForAttendenceResult> holiday = hmanager.GetMonthsHolidaysForAttendence(0, 0, SessionManager.CurrentCompanyId, 0, leaveRequest.FromDateEng, leaveRequest.ToDateEng,leaveRequest.EmployeeId).ToList();

                    int diff = 0;
                    // Attendance should not be saved
                    //if (LeaveRequestManager.IsAttendanceSavedForLeaveRequest(upLeaveRequest) && newLeaveStatus == LeaveRequestStatusEnum.Approved)
                    if (IsValidLeaveToChangeByApproval(leaveRequest.ToDateEng.Value, leaveRequest.FromDateEng.Value, ref diff) == false)
                    {
                        status.ErrorMessage =
                          string.Format("Past leave of " + emp.Name + " older than {0} days can not be changed.", diff);
                        status.IsSuccess = "false";
                        return status;
                    }

                    leaveRequest.ApprovedOn = BLL.BaseBiz.GetCurrentDateAndTime();
                    leaveRequest.ApprovedBy = SessionManager.CurrentLoggedInEmployeeId;
                    leaveRequest.Status = (int)LeaveRequestStatusEnum.Approved;

                    UpdateChangeSet();


                    // then we need to modify the Attendance also
                    ModifySavedAttendanceAfterLeaveChange(leaveRequest, emp, CountHolidayInBetweenLeave,onlyCountPublicHolidayAsLeaveWeeklySaturdayIsHoliday,onlyCountWeeklySaturdayAsLeavePublicHolidayIsHoliday, holiday, new List<LeaveDetail> { });

                    NotifyThroughMessageAndEmail("Approved", leaveRequest, leaveRequest.EmployeeId,
                    leaveRequest.FromDateEng.ToString(), leaveRequest.ToDateEng.ToString(), "approved");

                    count += 1;
                }

                PayrollDataContext.Transaction.Commit();
                return status;
            }
            catch (Exception exp)
            {
                
                PayrollDataContext.Transaction.Rollback();
                Log.log("Leave approval error : " + list.ToArray().ToString(), exp);
                status.ErrorMessage = "Leave(s) could not be approved.";
                status.IsSuccess = "false";
                return status;
            }
            finally
            {
                //if (PayrollDataContext.Connection != null)
                //{
                //    PayrollDataContext.Connection.Close();
                //}
            }


            //return status;
        }

        public static string GetLeaveNameForLeaveRequest(int leaveRequestId)
        {
            string leaveNames = "";
            LeaveRequest req = LeaveAttendanceManager.GetLeaveRequestById(leaveRequestId);
            if (req != null)
            {
                if (req.LeaveTypeId == 0)
                    leaveNames = "Unpaid Leave";
                else
                {
                    LLeaveType leave = LeaveAttendanceManager.GetLeaveTypeById(req.LeaveTypeId);
                    leaveNames = leave.Title;
                }

                if (req.LeaveTypeId2 != null)
                {
                    if (req.LeaveTypeId2 == 0)
                        leaveNames += ", Unpaid Leave";
                    else
                    {
                        LLeaveType leave = LeaveAttendanceManager.GetLeaveTypeById(req.LeaveTypeId2.Value);
                        leaveNames += ", " + leave.Title;
                    }
                }
            }
            return leaveNames;
        }

        public static void NotifyThroughMessageAndEmail(string pSubject, LeaveRequest leaveRequest, int employeeId, string fromDate, string toDate, string status)
        {
            int currentEmployeeId = 0; string empUserName = "";
            UserManager.SetCurrentEmployeeAndUser(ref currentEmployeeId, ref empUserName);


            string loggedInUserName = EmployeeManager.GetEmployeeById(currentEmployeeId).Name;
            // skip mail sendng from other email as sometime security issue like in case of Civil coming when sending email from other emails
            string loggedInEmpEmail = "";// EmployeeManager.GetEmployeeEmail(currentEmployeeId);

            #region Send Message To Leave request Sender
            string subject = pSubject;

            string body = string.Empty;
            if ((bool)(leaveRequest.IsHour == null ? false : leaveRequest.IsHour)
                && CommonManager.CalculationConstant.CompanyHasHourlyLeave.Value)
            {
                //Your leave on {0} for {1} Hours has been {2} by {3}.<br /> <br />
                //<strong>Comment:&nbsp;</strong>{3}.
                body = FileHelper.GetHourlyLeaveApproveContent();
                body = string.Format(body, DateTime.Parse(fromDate).ToShortDateString(), leaveRequest.DaysOrHours.ToString(), status, loggedInUserName, leaveRequest.Comment
                , (leaveRequest.IsHour.Value ? "hour(s)" : ""));
            }
            else
            {

            }
            {
                body = FileHelper.GetLeaveApproveContent();

                EmailContent content = CommonManager.GetMailContent((int)EmailContentType.DayTypeLeaveApproval);
                if (content != null)
                {
                    body = content.Body;
                    subject = content.Subject;

                    subject = subject
                        .Replace("#NewStatus#", ((LeaveRequestStatusEnum)leaveRequest.Status).ToString())
                        .Replace("#ApprovalName#", loggedInUserName);
                }

                body = body.Replace("#RequesterName#", "Your")
                            .Replace("#StartDate#", leaveRequest.FromDateEng.Value.ToShortDateString())
                            .Replace("#EndDate#", leaveRequest.ToDateEng.Value.ToShortDateString())
                            .Replace("#NewStatus#", status)
                            .Replace("#ApprovalName#", loggedInUserName)
                            .Replace("#Comment#", leaveRequest.Comment)
                            .Replace("#LeaveType#", GetLeaveNameForLeaveRequest(leaveRequest.LeaveRequestId))
                            .Replace("{Organizations Name}", SessionManager.CurrentCompany.Name);

                if (leaveRequest.IsCancelRequest != null && leaveRequest.IsCancelRequest.Value)
                {
                    subject = subject.Replace("Leave", "Leave Cancellation");
                    body = body.Replace("Leave", "Leave Cancellation");
                }

            }

            UUser recipient = UserManager.GetUserByEmployee(employeeId);

            if (recipient == null)
                return;

            recipient.Email = UserManager.GetEmployeeUserEmail(employeeId);

            string mailErrorMsg = "";

            //DAL.PayrollMessage msg = new DAL.PayrollMessage();
            //msg.Subject = subject;
            //msg.Body = body;
            //msg.SendBy = empUserName;
            //msg.ReceivedDate = BLL.BaseBiz.GetCurrentDateAndTime().ToShortDateString();
            //msg.ReceivedDateEng = BLL.BaseBiz.GetCurrentDateAndTime();
            //msg.IsRead = false;

            // send leave mail to the requesting Employee
            if (recipient != null)
            {
                List<string> employees = new List<string>();
                if (!string.IsNullOrEmpty(recipient.UserName))
                    employees.Add(recipient.UserName);

                //PayrollMessageManager.SendMessages(msg, employees);

                if (!string.IsNullOrEmpty(recipient.Email))
                {
                    // If email log is enabled then process to track email sending as we need synchronous process
                    if (CommonManager.Setting.EnableEmailLog != null && CommonManager.Setting.EnableEmailLog.Value)
                    {
                        string errorMsg = "";
                        bool emailStatus = SMTPHelper.SendMail(recipient.Email.Trim(), body, subject, "", loggedInEmpEmail, ref errorMsg);

                        PayrollDataContext.EmailLogs.InsertOnSubmit(GetEmailLog(employeeId, "", EmailLogTypeEnum.LeaveApprovalToEmployee, leaveRequest.LeaveRequestId,
                            recipient.Email.Trim() + "," + (""), subject, emailStatus, errorMsg));
                        PayrollDataContext.SubmitChanges();
                    }
                    else
                        SMTPHelper.SendAsyncMail(recipient.Email.Trim(), body, subject, loggedInEmpEmail);

                }
            }

            #region "Save Substiture message
            if (leaveRequest.SubstituteEmployeeId != null)
            {
                PayrollMessage msg1 = new PayrollMessage();
                UUser substitueUser = UserManager.GetUserByEmployee(leaveRequest.SubstituteEmployeeId.Value);
                if (substitueUser != null)
                {


                    string body1 = "You have been selected as substitute for {0} leave on {1}.";
                    body1 = string.Format(body1,
                            EmployeeManager.GetEmployeeById(leaveRequest.EmployeeId).Name,
                            leaveRequest.FromDate == leaveRequest.ToDate ? leaveRequest.FromDate :
                                leaveRequest.FromDate + " to " + leaveRequest.ToDate);
                    DAL.PayrollMessage msg = new DAL.PayrollMessage();
                    msg.Subject = body1;

                    CustomDate date = CustomDate.GetTodayDate(IsEnglish);
                    msg.Body = body1;
                    msg.SendBy = "HR";
                    msg.ReceivedDate = date.ToString();
                    msg.ReceivedDateEng = date.EnglishDate;
                    msg.ReceivedTo = substitueUser.UserName;
                    msg.IsRead = false;
                    msg.DueDateEng = null;
                    BLL.BaseBiz.PayrollDataContext.PayrollMessages.InsertOnSubmit(msg);
                    BLL.BaseBiz.PayrollDataContext.SubmitChanges();
                }
            }
            #endregion

            // send also email to other employee for informing
            if (leaveRequest.Status == (int)LeaveRequestStatusEnum.Approved && leaveRequest.ApprovedBy != null)
            {

                #region "Create Email Subject and Body"

                EEmployee emp = EmployeeManager.GetEmployeeById(leaveRequest.EmployeeId);
                EEmployee approvalEmp = EmployeeManager.GetEmployeeById(leaveRequest.ApprovedBy.Value);
                // Leave Notification: Sunil Manandhar will be on leave on {date} for {hours}
                // Sunil Maanandhar will be on leave on {date} for {hours} /or/ from {date} to {date}
                // His/ Her Leave has been approved by {approve}
                subject = "";
                body = "";

                string date = leaveRequest.FromDate == leaveRequest.ToDate ? leaveRequest.FromDateEng.Value.ToShortDateString()
                    : leaveRequest.FromDateEng.Value.ToShortDateString() + " to " + leaveRequest.ToDateEng.Value.ToShortDateString();

                subject = string.Format("Leave Notification: {0} will be on leave {2} {1}", emp.Name, date
                    , date.Contains("to") ? "from" : "on");
                body = string.Format("Leave has been approved by {0}", approvalEmp.Name + 
                                // append approvald designation for NCC
                                (CommonManager.CompanySetting.WhichCompany==WhichCompany.NCC ? ", " + CommonManager.GetDesignationNameByEmpId(approvalEmp.EmployeeId) : "")
                                );

                // set substitute in the body
                if (leaveRequest.SubstituteEmployeeId != null)
                {
                    EEmployee subtituteEmp = EmployeeManager.GetEmployeeById(leaveRequest.SubstituteEmployeeId.Value);
                    body += "<br></br>" + subtituteEmp.Title + " " + subtituteEmp.Name +
                                // append substitute designation for NCC
                                (CommonManager.CompanySetting.WhichCompany == WhichCompany.NCC ? ", " + CommonManager.GetDesignationNameByEmpId(leaveRequest.SubstituteEmployeeId.Value) : "")

                        + " has been selected as Substitute for the leave.";
                }

                if (leaveRequest.IsHour != null && leaveRequest.IsHour.Value)
                {
                    subject += " for " + leaveRequest.DaysOrHours + " hours from " + leaveRequest.StartTime + " to " + leaveRequest.EndTime;

                }
                else if (leaveRequest.IsHalfDayLeave != null && leaveRequest.IsHalfDayLeave.Value)
                {
                    if (!string.IsNullOrEmpty(leaveRequest.HalfDayType))
                        subject += " for " + (leaveRequest.HalfDayType.ToLower() == "am" ? "first" : "second") + " half";
                    else
                        subject += " for half day";
                }


                string branchDesignation = "";
                if(CommonManager.CompanySetting.WhichCompany==WhichCompany.NCC)
                {
                    branchDesignation=  CommonManager.GetDesignationNameByEmpId(leaveRequest.EmployeeId);
                    branchDesignation += " - " + EmployeeManager.GetCurrentBranchDepartment(leaveRequest.EmployeeId);
                }
                

                body = "<br><br>" + subject.Replace("Leave Notification: ", "") + "<br>" + branchDesignation + "<br><br>" + body;

                #endregion

                string from = approvalEmp.EAddresses[0].CIEmail.Trim();
                string bcc = "";

                // Case 1: Notification list
                int? recommenderEmpId = LeaveRequestManager.GetLeaveRecommenderEmpId(leaveRequest.LeaveRequestId);
                TravelAllowanceManager.SetBCCLeaveApprovalNotification(leaveRequest.EmployeeId,
                    leaveRequest.ApprovedBy.Value, recommenderEmpId, ref bcc, ref from, leaveRequest.LeaveTypeId);


                // Case 2: add additional email from setting
                List<GetToEmailResult> list = new List<GetToEmailResult>();
                // Add Team notification
                if (CommonManager.Setting != null && CommonManager.Setting.ApproveLeaveNotificationType != null && CommonManager.Setting.ApproveLeaveNotificationType != -1)
                    list = BLL.BaseBiz.PayrollDataContext.GetToEmail(leaveRequest.EmployeeId, leaveRequest.ApprovedBy, CommonManager.Setting.ApproveLeaveNotificationType,
                        emp.BranchId, emp.DepartmentId).ToList();

                // Case 3: add email for HR from leave setting
                if (CommonManager.Setting != null && !string.IsNullOrEmpty(CommonManager.Setting.ApproveLeaveNotificationMails))
                {
                    string[] emailList = CommonManager.Setting.ApproveLeaveNotificationMails.Split(new char[] { ',' });
                    foreach (string email in emailList)
                    {
                        list.Add(new GetToEmailResult { Email = email });
                    }
                }

                if (list.Count > 0)
                {
                    if (from == "" && list.Count > 0)
                    {
                        from = list[0].Email.Trim();
                        list.Remove(list[0]);
                    }

                    if (!string.IsNullOrEmpty(bcc))
                        bcc += ",";
                    bcc = bcc + string.Join(",", list.Select(x => x.Email).ToList().ToArray());
                }

                


                DateTime today = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
                // only inform for today or greater to date leave only
                if (!string.IsNullOrEmpty(from) && (leaveRequest.ToDateEng == null || leaveRequest.ToDateEng >= today))
                {
                    // If email log is enabled then process to track email sending as we need synchronous process
                    if (CommonManager.Setting.EnableEmailLog != null && CommonManager.Setting.EnableEmailLog.Value)
                    {
                        string errorMsg = "";
                        bool emailStatus = SMTPHelper.SendMail(from, body, subject, bcc, loggedInEmpEmail, ref errorMsg);

                        PayrollDataContext.EmailLogs.InsertOnSubmit(GetEmailLog(employeeId, "", EmailLogTypeEnum.LeaveApprovalToOtherEmployee, leaveRequest.LeaveRequestId,
                            from + ":to,bcc:" + (bcc == null ? "" : (bcc.Length > 3500 ? bcc.Substring(0, 3500) : bcc)), subject, emailStatus, errorMsg));
                        PayrollDataContext.SubmitChanges();
                    }
                    else
                        SMTPHelper.SendAsyncMail(from, body, subject, bcc);

                }
            }
            #endregion



        }
        public static List<LLeaveType> SetOrderUsingGroupAndChild(List<LLeaveType> list)
        {

            List<LLeaveType> newList = new List<LLeaveType>();
            List<LLeaveType> child = list.Where(x => x.IsChildLeave == true).ToList();

            list = list.Where(x => x.IsChildLeave != true).ToList();

            for (int i = 0; i < list.Count; i++)
            {
                newList.Add(list[i]);

                List<LLeaveType> childList = child.Where(x => x.ParentLeaveTypeId == list[i].LeaveTypeId).ToList();

                foreach (LLeaveType childLeave in childList)
                    childLeave.Title = "<span style='padding-left:20px'></span>" + childLeave.Title;

                newList.AddRange(childList);
            }

            return newList;
        }

        public static List<EmployeeLeaveListResult> SetOrderUsingGroupAndChild(List<EmployeeLeaveListResult> list)
        {

            List<EmployeeLeaveListResult> newList = new List<EmployeeLeaveListResult>();
            List<EmployeeLeaveListResult> child = list.Where(x => x.IsChildLeave == true).ToList();

            list = list.Where(x => x.IsChildLeave != true).ToList();

            for (int i = 0; i < list.Count; i++)
            {
                newList.Add(list[i]);

                List<EmployeeLeaveListResult> childList = child.Where(x => x.ParentLeaveTypeId == list[i].LeaveTypeId).ToList();

                foreach (EmployeeLeaveListResult childLeave in childList)
                    childLeave.Title = "<span style='padding-left:20px'></span>" + childLeave.Title;

                newList.AddRange(childList);
            }

            return newList;
        }

        public static List<TextValue> GetEmployeeStatusList()
        {
            PayrollPeriod firstPeriod = PayrollDataContext.PayrollPeriods.FirstOrDefault();

            if (firstPeriod == null)
                return new List<TextValue>();

            List<TextValue> list =
                (
                    from e in PayrollDataContext.EEmployees
                    select new TextValue
                    {
                        ID = e.EmployeeId,
                        Value2 = PayrollDataContext.GetEmployeeStatusForPayrollPeriod(firstPeriod.PayrollPeriodId, e.EmployeeId)
                    }
                ).ToList();

            return list;
        }

        public static List<LLeaveType> GetAllLeavesWithManualAlso(int companyId)
        {
            return PayrollDataContext.LLeaveTypes.Where(
                  c => c.CompanyId == companyId).OrderBy(c => c.Title).ToList();
        }
        public List<LLeaveType> GetWorkDayLeaves(int companyId, int leaveTypeId)
        {

            return PayrollDataContext.LLeaveTypes.Where(
                cp => cp.CompanyId == companyId && cp.LeaveTypeId != leaveTypeId).ToList();
        }

        #region "Dish home sunday leave"
        public static string GetDishHomeSundayHalfDayAbbr()
        {
            if (CommonManager.CompanySetting.WhichCompany == WhichCompany.DishHome)
            {
                return new LeaveAttendanceManager().GetLeaveType(GetSundayLeaveId()).Abbreviation + "/2";
            }

            return "";
        }
        public static int GetSundayLeaveId()
        {
            return 9;
        }
        public static bool IsDishHomeSundayLeave(int leaveTypeId)
        {
            if (CommonManager.CompanySetting.WhichCompany == WhichCompany.DishHome)
            {
                return leaveTypeId == GetSundayLeaveId();
            }

            return false;
        }

        public static bool IsValidSundayLeaveRequest(LeaveRequest leave)
        {
            if (IsDishHomeSundayLeave(leave.LeaveTypeId))
            {
                return true;
            }
            else
                return true;
        }

        #endregion

        public static bool TryToClearManualLeaveBalance(LEmployeeLeave empLeave, bool clearBalance)
        {
            if (empLeave.LLeaveType.FreqOfAccrual == LeaveAccrue.MANUALLY && clearBalance)
            {
                //get last payroll period
                LeaveAdjustment leaveAdj
                     = PayrollDataContext.LeaveAdjustments.Where(l => l.EmployeeId == empLeave.EmployeeId
                         && l.LeaveTypeId == empLeave.LeaveTypeId)
                         .OrderByDescending(l => l.PayrollPeriodId)
                         .Take(0).FirstOrDefault();
                if (leaveAdj != null)
                {
                    leaveAdj.NewBalance = 0;
                    return UpdateChangeSet();
                }
            }
            return false;
        }

        public static List<TextValue> GetPreDefinedApprovalList(bool? isForRecommender,int employeeId,PreDefindFlowType type)
        {
            return
                (
                from l in PayrollDataContext.GetPreDefinedApprovalList(isForRecommender, employeeId,(int)type)
                    // do not skip same employee as needed for sana kisan
                    //where l.EmployeeId != employeeId
                select new TextValue
                {
                    Text = l.NAME,
                    Value = l.EmployeeId.ToString()
                }
                ).ToList();
        }
        //public static List<TextValue> GetOneToOneApprovalList(bool? isForRecommender, int employeeId)
        //{
        //    LeaveSpecificEmployee entity = PayrollDataContext.LeaveSpecificEmployees.FirstOrDefault
        //        (x => x.EmployeeId == employeeId);

        //    if (entity == null)
        //        return new List<TextValue>();

        //    List<TextValue> list = new List<TextValue>();

        //    EEmployee e1, e2;

        //    if (isForRecommender == null || isForRecommender == true)
        //    {
        //        if (entity.Recommender1 != null)
        //        {
        //            e1 = EmployeeManager.GetEmployeeById(entity.Recommender1.Value);
        //            if (e1 != null)
        //                list.Add(new TextValue { Value = e1.EmployeeId.ToString(), Text = e1.Name });
        //        }
        //        if (entity.Recommender2 != null)
        //        {
        //            e2 = EmployeeManager.GetEmployeeById(entity.Recommender2.Value);
        //            if (e2 != null)
        //                list.Add(new TextValue { Value = e2.EmployeeId.ToString(), Text = e2.Name });
        //        }
        //    }
        //    if (isForRecommender == null || isForRecommender == false)
        //    {
        //        if (entity.Approval1 != null)
        //        {
        //            e1 = EmployeeManager.GetEmployeeById(entity.Approval1.Value);
        //            if (e1 != null)
        //                list.Add(new TextValue { Value = e1.EmployeeId.ToString(), Text = e1.Name });
        //        }
        //        if (entity.Approval2 != null)
        //        {
        //            e2 = EmployeeManager.GetEmployeeById(entity.Approval2.Value);
        //            if (e2 != null)
        //                list.Add(new TextValue { Value = e2.EmployeeId.ToString(), Text = e2.Name });
        //        }
        //    }
        //    return list;
        //}

        /// <summary>        
        /// </summary>
        /// <param name="entity"></param>
        /// <returns>-2=title exists, -1=abbrevation exists, 1=success, 0=fail</returns>
        public int Save(LLeaveType entity)
        {

            LLeaveType leave = PayrollDataContext.LLeaveTypes.SingleOrDefault(
                e => 
                    (
                    (e.Title.ToLower() == entity.Title.ToLower() || e.Abbreviation.ToLower() == entity.Abbreviation.ToLower()) && e.CompanyId==SessionManager.CurrentCompanyId
                    ));

            if (leave != null)
            {
                if (leave.Title == entity.Title)
                    return -2;
                else if (leave.Abbreviation == entity.Abbreviation)
                    return -1;
            }

            PayrollDataContext.LLeaveTypes.InsertOnSubmit(entity);

            if (SaveChangeSet())
                return 1;
            return 0;
        }

        public static bool DeleteAttendance(int payrollPeriodId,string xmlEmployees)
        {
            return
                PayrollDataContext.DeleteAttendance(payrollPeriodId, XElement.Parse(xmlEmployees)).SingleOrDefault().
                    Success.ToString() == "1";
        }

        public static bool DeleteAllAttendance(int payrollPeriodId)
        {
            List<Attendence> atteList = PayrollDataContext.Attendences.Where(x => x.PayrollPeriodId == payrollPeriodId).ToList();
            List<LeaveAdjustment> adjList = PayrollDataContext.LeaveAdjustments.Where(x => x.PayrollPeriodId == payrollPeriodId).ToList();

            PayrollDataContext.Attendences.DeleteAllOnSubmit(atteList);
            PayrollDataContext.LeaveAdjustments.DeleteAllOnSubmit(adjList);

            PayrollDataContext.SubmitChanges();

            return true;
        }

        public static float GetBeginningBalance(int employeeId, int leaveTypeId)
        {
            double? value =
                 PayrollDataContext.LeaveGenerateBeginningBalance(leaveTypeId, employeeId,
                                                                          null, SessionManager.CurrentCompanyId);
            if (value == null)
                return 0;
            return
                (float)value;
        }

        public static bool IsAttendanceSavedForEmployee(int payrollPeroidId, int employeeId)
        {
            return PayrollDataContext.Attendences.Any(a => a.EmployeeId == employeeId && a.PayrollPeriodId == payrollPeroidId);
        }

        public static bool IsAttendanceSavedByFillingEachDayForEmployee(int payrollPeroidId, int employeeId)
        {
            bool isSaved =  PayrollDataContext.Attendences.Any(a => a.EmployeeId == employeeId && a.PayrollPeriodId == payrollPeroidId);

            if(isSaved)
            {
                Attendence atte = PayrollDataContext.Attendences.FirstOrDefault(a => a.EmployeeId == employeeId && a.PayrollPeriodId == payrollPeroidId);
                if (atte.AttedenceDetails.Any(x => string.IsNullOrEmpty(x.DayValue)))
                    return false;

                return true;
            }

            return false;
        }

        public static bool IsAttendanceSavedForEmployee(int employeeId)
        {
            return PayrollDataContext.Attendences.Any(a => a.EmployeeId == employeeId);
        }

        public static bool IsAttendanceSaved(int payrollPeroidId)
        {
            return PayrollDataContext.Attendences.Any(a => a.PayrollPeriodId == payrollPeroidId);
        }

        public static bool IsAttendanceSavedForCompany()
        {
            
            var list =

                (
                    from a in PayrollDataContext.Attendences
                    join p in PayrollDataContext.PayrollPeriods on a.PayrollPeriodId equals p.PayrollPeriodId
                    where p.CompanyId == SessionManager.CurrentCompanyId
                    select a
                ).Count();


            if( list>0)
                return true;
            return false;
        }

        public static bool IsLeaveDeletable(int leaveId)
        {
            if (PayrollDataContext.LEmployeeLeaves
                .Where(i => i.LeaveTypeId == leaveId)
                .Count() <= 0)
                return true;
            return false;
        }

        public static bool DeleteLeave(int leaveId)
        {
            if (IsLeaveDeletable(leaveId))
            {

                LLeaveType dbEntity = PayrollDataContext.LLeaveTypes.SingleOrDefault(
                    c => (c.LeaveTypeId == leaveId));


                if (dbEntity == null)
                    return false;

                System.Data.Linq.ChangeSet cs = PayrollDataContext.GetChangeSet();
                PayrollDataContext.LLeaveTypes.DeleteOnSubmit(dbEntity);
                PayrollDataContext.SubmitChanges();
                return (cs.Deletes.Count == 1);
            }
            return false;
        }



        public static bool IsLeaveUsedInAttendance(int leaveId)
        {
            return
                PayrollDataContext.LeaveAdjustments.Any(l => l.LeaveTypeId == leaveId);
        }


        public static List<LLeaveType> GetLeaveListByCompany(int companyId)
        {
            return PayrollDataContext.LLeaveTypes.Where(c => c.CompanyId == companyId)
                .OrderBy(c => c.Title).ToList();
            //.GetIncomeListByCompany(companyId).ToList();
        }

        public static List<LLeaveType> GetLeaveListForImport(int companyId)
        {
            return PayrollDataContext.LLeaveTypes
                .Where(c => c.CompanyId == companyId && (c.IsParentGroupLeave == null || c.IsParentGroupLeave ==false) &&
                    ( c.FreqOfAccrual == LeaveAccrue.MONTHLY 
                     || c.FreqOfAccrual == LeaveAccrue.YEARLY
                     || c.FreqOfAccrual == LeaveAccrue.MANUALLY
                     || c.FreqOfAccrual==LeaveAccrue.COMPENSATORY) )
                .OrderBy(c => c.Title).ToList();

            //.GetIncomeListByCompany(companyId).ToList();
        }

        public string GetHTMLEmployeeLeaveList(int employeeId)
        {
            StringBuilder html = new StringBuilder();
            List<EmployeeLeaveListResult> leaves = GetEmployeeLeaveList(employeeId);

            SetOrderUsingGroupAndChild(leaves);

            List<GetEmployeeLeaveBalanceResult> leaveBalance = LeaveRequestManager.GetEmployeeCurrentLeaveBalance(employeeId);
            PayrollPeriod payroll = CommonManager.GetLastPayrollPeriod();

            //int daysDiff = 0; int remainingMonth = 0;
            //bool isAttendanceSavedForCurrentPeriod = false;
            //// Calculate current Month period is atte is not being saved
            //if (payroll != null && payroll.Month != SessionManager.CurrentCompany.LeaveStartMonth.Value)
            //{
            //    daysDiff = GetYearlyLeaveMiddleJoiningDays(payroll, ref remainingMonth);
            //}
            //if (payroll != null)
            //    isAttendanceSavedForCurrentPeriod = PayrollDataContext.Attendences.Any(x => x.EmployeeId == employeeId && x.PayrollPeriodId == payroll.PayrollPeriodId);


           


            //List<GetLeaveListAsPerEmployeeResult> empValidLeaves = new List<GetLeaveListAsPerEmployeeResult>();                            
            //if(payroll != null)
            //    empValidLeaves = LeaveAttendanceManager.GetAllLeavesForLeaveRequest(employeeId, payroll.PayrollPeriodId, true);
           


            //PayrollPeriod currentPeriod = CommonManager.GetLastPayrollPeriod();
            int leaveStartMonth = SessionManager.CurrentCompany.LeaveStartMonth.Value;



            int yearId = 0;



            if (SessionManager.CurrentCompany.LeaveStartMonth == 1)
            {
                List<YearBO> list = CommonManager.GetBaisakhJanuaryLeaveYearList();
                yearId = int.Parse(list[list.Count - 1].Name);
            }
            else
            {
                yearId = SessionManager.CurrentCompanyFinancialDate.FinancialDateId;
            }

            int? startid = null; int? endId = null;
            int? lastYearOrYearOpeningPayrollPeriodId = null;
            DateTime? yearStart = null, yearEnd = null;
            DateTime? startDateLeaveTaken = null; DateTime? endDateLeaveTaken = null; int? currentLeavePeriodIdForAccural = null;

            // for Sharwarn leave start month
            if (SessionManager.CurrentCompany.LeaveStartMonth == (int)EnglishMonthEnum.April && SessionManager.CurrentCompany.IsEnglishDate)
            {
                List<YearBO> list = CommonManager.GetAprilToMarchLeaveYearList();
                yearId = list[list.Count - 1].FinancialDateId;

                CommonManager.SetPeriodForAprilToMarchYearlyLeavePeriod(yearId, ref startid, ref endId, ref startDateLeaveTaken,
                     ref endDateLeaveTaken, ref currentLeavePeriodIdForAccural, ref lastYearOrYearOpeningPayrollPeriodId, ref yearStart, ref yearEnd, employeeId);
            }
            else if (SessionManager.CurrentCompany.LeaveStartMonth != 1)
            {
                CommonManager.SetPeriodForMiddleSharwanJulyYearlyLeavePeriod(yearId, ref startid, ref endId, ref startDateLeaveTaken,
                     ref endDateLeaveTaken, ref currentLeavePeriodIdForAccural, ref lastYearOrYearOpeningPayrollPeriodId, ref yearStart, ref yearEnd,employeeId);
            }
            else
            {
                CommonManager.SetPeriodForBaisakhJanuaaryYearlyLeavePeriod(yearId, ref startid, ref endId, ref startDateLeaveTaken,
                   ref endDateLeaveTaken, ref currentLeavePeriodIdForAccural, ref lastYearOrYearOpeningPayrollPeriodId, ref yearStart, ref yearEnd, employeeId);
            }

            int totalRecords = 0;
            List<Report_HR_YearlyLeaveBalanceDetailsResult> leaveBalanceList = EmployeeManager.GetYearlyLeaveReportAsList
                (-1, -1, -1, -1, employeeId, 0, 999999, "", ref totalRecords
                , startid, endId, currentLeavePeriodIdForAccural, startDateLeaveTaken, endDateLeaveTaken, "", lastYearOrYearOpeningPayrollPeriodId, yearStart.Value, yearEnd.Value
                 , (int)PageViewType.Admin, employeeId);



            //try
            //{
            //    foreach (EmployeeLeaveListResult item in leaves)
            //    {
            //        GetEmployeeLeaveBalanceResult result = leaveBalance.FirstOrDefault(x => x.LeaveTypeId == item.LeaveTypeId);
            //        if (result != null)
            //            item.BeginningBalance = (double)result.NewBalance;
            //    }
            //}
            //catch (Exception eee) { }


            bool isReadOnlyUser = SessionManager.IsReadOnlyUser; 
            


            html.Append("<table class='table table-bordered table-condensed table-striped table-primary'>");
            html.Append("<thead><tr> <th>Leave type</th> <th>Accrue</th> <th>Applies To</th>  <th>Unused Leave</th> <th>Opening </th> <th>Accural</th> <th>Taken</th> <th>Adjusted</th> <th>Lapsed/<br/>Encashed</th> <th>Balance </th> <th> Current<br/>Balance </th> <th  class='editDeleteTH'> </th> </tr></thead>");
            foreach (EmployeeLeaveListResult leave in leaves)
            {
                decimal adjusted = 0, accural = 0;
                double lapsedEncashed = 0;
                decimal yearOpening = 0, taken = 0;
                decimal balanceFromYearlyReport = 0;
                decimal currentBalance = 0;
              
                Report_HR_YearlyLeaveBalanceDetailsResult bal = leaveBalanceList.FirstOrDefault(x=>x.LeaveTypeId==leave.LeaveTypeId);
                GetEmployeeLeaveBalanceResult levBal = leaveBalance.FirstOrDefault(x => x.LeaveTypeId == leave.LeaveTypeId);
                LLeaveType leaveType = LeaveAttendanceManager.GetLeaveTypeById(leave.LeaveTypeId);

                yearOpening = bal == null || bal.LastYear == null ? 0 : (decimal)bal.LastYear.Value;
                accural = bal == null || bal.CurrentYear == null ? 0 : (decimal)bal.CurrentYear.Value;
                adjusted = bal == null || bal.Adjustment == null ? 0 : (decimal)bal.Adjustment.Value;
                taken = bal == null || bal.Taken == null ? 0 : (decimal)bal.Taken.Value;
                lapsedEncashed = bal == null ? 0 : Convert.ToDouble(bal.Lapsed) + Convert.ToDouble(bal.Encashed);
                balanceFromYearlyReport = bal == null || bal.Balance == null ? 0 : (decimal)bal.Balance.Value;
                currentBalance = levBal == null || levBal.NewBalance == null ? 0 : (decimal)levBal.NewBalance.Value;
                // for nibl compenstory leave is only deducted so do not show negative bal
                if (CommonManager.CompanySetting.WhichCompany == WhichCompany.NIBL && leaveType != null && leaveType.FreqOfAccrual == LeaveAccrue.COMPENSATORY)
                {
                    balanceFromYearlyReport = 0;
                    currentBalance = 0;
                }
                // hide lapse/encash and balance for Dishhome sunday leave
                else if(CommonManager.CompanySetting.WhichCompany==WhichCompany.DishHome && leaveType.LeaveTypeId==9)
                {

                    lapsedEncashed = 0;
                    if (balanceFromYearlyReport < 0)
                        balanceFromYearlyReport = 0;
                }

                if (isReadOnlyUser == false)
                {
                    html.Append(
                        String.Format(@"<tr  onmouseover='this.originalClass=this.className;this.className=""selected"";' onmouseout='this.className=this.originalClass;'><td> {1} </td><td>{2} </td> <td>{3} </td> {4} <td>{5} </td><td class='balanceTD'> {6}  </td><td class='balanceTD'> {7}  </td><td class='balanceTD'> {8}  </td><td class='balanceTD'> {9}  </td> <td class='balanceTD'> {10}  </td> <td class='balanceTD'> {11}  </td> <td class='balanceTD'> {12}  </td> <td  class='editDeleteTD'> 
                            <input type='image' style='border-width: 0px;' onclick='return {13}({0});' src='../images/edit.gif' />
                            &nbsp;&nbsp;<input type='image' style='border-width: 0px;' onclick='return deleteLeaveCall({0});' src='../images/delete.gif' />
                            </td> </tr>",

                            leave.LeaveTypeId,             
                            leave.Title,
                            new LeaveAccrue().Get(leave.FreqOfAccrual),
                             new JobStatus().Get(leave.AppliesTo.ToString()),
                             "",
                             leave.IsParentGroupLeave ? "" : new UnusedLeave().Get(leave.UnusedLeave),
                             leave.IsParentGroupLeave ? "" : GetDecimalValue(yearOpening), //yearOpening,
                             leave.IsParentGroupLeave ? "" : GetDecimalValue(accural),
                             leave.IsParentGroupLeave ? "" : GetDecimalValue(taken),
                             leave.IsParentGroupLeave ? "" : GetDecimalValue(adjusted),
                             leave.IsParentGroupLeave ? "" : GetDecimalValue((decimal)lapsedEncashed),
                             leave.IsParentGroupLeave ? "" : GetDecimalValue(balanceFromYearlyReport),
                             leave.IsParentGroupLeave ? "" : GetDecimalValue(currentBalance),
                            
                            (leave.FreqOfAccrual == LeaveAccrue.MANUALLY ? "popupUpdateMannualLeaveCall" : "popupUpdateLeaveCall")
                            
                            //leave.IsParentGroupLeave ? "" : Convert.ToDecimal(taken).ToString("N2"), //taken,
                            // leave.IsParentGroupLeave ? "" : Convert.ToDecimal(adjusted).ToString("N2")//, adjusted
                            // , leave.IsParentGroupLeave ? "" : Convert.ToDecimal(lapsed).ToString("N2")
                            // , leave.IsParentGroupLeave ? "" : Convert.ToDecimal(encashed).ToString("N2")
                        )
                        );
                }
                else
                {
                    html.Append(
                       String.Format(@"<tr   onmouseover='this.originalClass=this.className;this.className=""selected"";' onmouseout='this.className=this.originalClass;'><td> {0} </td><td>{1} </td> <td>{4} </td> <td>{5} </td> <td>{6} </td> <td> {2}  </td>  <td  class='editDeleteTD'> 
                            <span style='display:none'>{3}11 </span> &nbsp;
                            </td> </tr>",
                             (leave.IsChildLeave ? "<span style='margin-left:15px'></span>" : "") + "" + leave.Title,
                           new LeaveAccrue().Get(leave.FreqOfAccrual),
                           (leave.BeginningBalance.Value.ToString("N2")).ToString(),
                           leave.LeaveTypeId,
                           new JobStatus().Get(leave.AppliesTo.ToString()),
                           new LeaveType().Get(leave.Type.ToString()),
                           new UnusedLeave().Get(leave.UnusedLeave)
                       )
                       );
                }
            }

            html.Append("</table>");

            return html.ToString();
        }

        public string GetDecimalValue(decimal value)
        {
            if (value == 0)
                return "-";
            return Convert.ToDecimal(value).ToString("N2");
        }

        public List<EmployeeLeaveListResult> GetEmployeeLeaveList(int employeeId)
        {
            return PayrollDataContext.EmployeeLeaveList(employeeId).ToList();
        }


        public List<GetLeaveListForEmployeeResult> GetLeaveListForEmp(int empId)
        {
            return PayrollDataContext.GetLeaveListForEmployee(empId, SessionManager.CurrentCompanyId).ToList();
        }


        public bool AddLeaveToEmployee(int leaveTypeId, int employeeId, bool addToAllEmployees, int companyId,int statusId)
        {
            return PayrollDataContext
                .AddLeaveToEmployee(leaveTypeId, employeeId, addToAllEmployees, companyId,SessionManager.UserName,statusId) == 1;
        }

        public LLeaveType GetLeaveType(int leaveTypeId)
        {
            LLeaveType entity = PayrollDataContext.LLeaveTypes.SingleOrDefault(
                l => l.LeaveTypeId == leaveTypeId);
            return entity;
        }
        public static LLeaveType GetLeaveTypeById(int leaveTypeId)
        {
            LLeaveType entity = PayrollDataContext.LLeaveTypes.SingleOrDefault(
                l => l.LeaveTypeId == leaveTypeId);
            return entity;
        }
        public LEmployeeLeave GetEmployeeLeave(int leaveTypeId, int employeeId)
        {
            return PayrollDataContext.LEmployeeLeaves.Where(
                    l => (l.EmployeeId == employeeId && l.LeaveTypeId == leaveTypeId))
                    .SingleOrDefault();

        }


        public bool UpdateEmployeeLeave(LEmployeeLeave entity,bool changeManualBalance)
        {
            LEmployeeLeave dbEntity = PayrollDataContext.LEmployeeLeaves.SingleOrDefault(
                l => l.LeaveTypeId == entity.LeaveTypeId && l.EmployeeId == entity.EmployeeId);

            if (dbEntity == null)
                return false;
            dbEntity.EditSequence = entity.EditSequence;

            

            if (dbEntity.ApplyLeaveWithOutCheckingStatus != entity.ApplyLeaveWithOutCheckingStatus)
            {
                PayrollDataContext.ChangeMonetories.InsertOnSubmit(
                              GetMonetoryChangeLog(entity.EmployeeId, (byte)MonetoryChangeLogTypeEnum.Leave, entity.LLeaveType.Title + " : Apply leave without checking status",
                              GetString(dbEntity.ApplyLeaveWithOutCheckingStatus), GetString(entity.ApplyLeaveWithOutCheckingStatus), LogActionEnum.Update));

            }
            dbEntity.ApplyLeaveWithOutCheckingStatus = entity.ApplyLeaveWithOutCheckingStatus;


            //if (dbEntity.LLeaveType.FreqOfAccrual == LeaveAccrue.MANUALLY)
            //{
            //    if (changeManualBalance)
            //    {
            //        if (dbEntity.Accured != entity.Accured)
            //        {
            //            PayrollDataContext.ChangeMonetories.InsertOnSubmit(
            //                GetMonetoryChangeLog(entity.EmployeeId, (byte)MonetoryChangeLogTypeEnum.Leave, entity.LLeaveType.Title + " : Accured",
            //                GetString(dbEntity.Accured), GetString(entity.Accured), LogActionEnum.Update));



            //        }


            //        dbEntity.Accured = entity.Accured;

            //        // call to send changed balance to Attendance
            //        PayrollPeriod lastPeriod = CommonManager.GetLastPayrollPeriod();
            //        if (lastPeriod != null)
            //        {
            //            LeaveAdjustment latestBalance =
            //                PayrollDataContext.LeaveAdjustments
            //                    .OrderByDescending(x => x.PayrollPeriodId)
            //                    .Where(x => x.PayrollPeriodId == lastPeriod.PayrollPeriodId
            //                        && x.EmployeeId == dbEntity.EmployeeId && x.LeaveTypeId == dbEntity.LeaveTypeId)
            //                    .FirstOrDefault();

            //            if (latestBalance != null)// && latestBalance.NewBalance != null && latestBalance.NewBalance != 0)
            //            {


            //                latestBalance.NewBalance = (decimal)entity.Accured;
            //                latestBalance.Accured = latestBalance.NewBalance - (decimal)latestBalance.Taken - (decimal)latestBalance.BeginningBalance;

            //                dbEntity.Accured = 0;
                          
            //            }
            //            else
            //            {
            //                // total approved leave for this one

            //                double? takenThisMonth = 0;

            //                try
            //                {
            //                    takenThisMonth =
            //                    (
            //                       from lr in PayrollDataContext.LeaveRequests
            //                       join ld in PayrollDataContext.LeaveDetails on lr.LeaveRequestId equals ld.LeaveRequestId
            //                       where lr.EmployeeId == entity.EmployeeId
            //                       && (ld.LeaveTypeId != null ? ld.LeaveTypeId : lr.LeaveTypeId) == entity.LeaveTypeId
            //                       && ld.DateOnEng >= lastPeriod.StartDateEng
            //                       select (lr.IsHalfDayLeave != null && lr.IsHalfDayLeave.Value ? 0.5 : 1)
            //                    ).Sum();
            //                }
            //                catch(Exception exp1)
            //                {

            //                }

            //                if (takenThisMonth == null)
            //                    takenThisMonth = 0;

            //                PayrollPeriod prevPeriod  =
            //                    PayrollDataContext.PayrollPeriods
            //                        .OrderByDescending(x => x.PayrollPeriodId)
            //                        .Where(x => x.PayrollPeriodId < lastPeriod.PayrollPeriodId)
            //                        .FirstOrDefault();

            //                double beginning = 0;
            //                if (prevPeriod != null)
            //                {
            //                    LeaveAdjustment prevBalance =
            //                        PayrollDataContext.LeaveAdjustments
            //                            .OrderByDescending(x => x.PayrollPeriodId)
            //                            .Where(x => x.PayrollPeriodId == prevPeriod.PayrollPeriodId
            //                                && x.EmployeeId == dbEntity.EmployeeId && x.LeaveTypeId == dbEntity.LeaveTypeId)
            //                            .FirstOrDefault();

            //                    if (prevBalance != null)
            //                        beginning = Convert.ToDouble(prevBalance.NewBalance);
            //                }

            //                //dbEntity.Accured = entity.Accured + takenThisMonth;

            //                //latestBalance.NewBalance = (decimal)entity.Accured;

            //                dbEntity.Accured = (double)entity.Accured;
            //                dbEntity.Accured = dbEntity.Accured + takenThisMonth.Value - beginning;


            //                //dbEntity.Accured = entity.Accured - takenThisMonth - beginning;

            //                // else insert into LeaveAdjustment
            //            }


            //        }


            //    }
                
            //}

            Update(entity.LLeaveType,dbEntity.LLeaveType);

            UpdateChangeSet();

            //if (dbEntity.LLeaveType.FreqOfAccrual == LeaveAccrue.MANUALLY)
            //{
            //    // call to send changed balance to Attendance
            //    PayrollPeriod lastPeriod = CommonManager.GetLastPayrollPeriod();
            //    if (lastPeriod != null)
            //    {
            //        if (LeaveAttendanceManager.IsAttendanceSavedForEmployee(lastPeriod.PayrollPeriodId, dbEntity.EmployeeId))
            //            EmployeeManager.ResaveAttendanceOfEmployee(dbEntity.EmployeeId, lastPeriod);
            //        else
            //        {

            //        }
            //    }
            //}
            return true;
        }

      
            
        public bool Update(LLeaveType entity)
        {
            LLeaveType dbEntity = PayrollDataContext.LLeaveTypes
                .Where(l => l.LeaveTypeId == entity.LeaveTypeId).SingleOrDefault();

            if (dbEntity == null)
                return false;

            Update(entity, dbEntity);

            return UpdateChangeSet();

        }

        public void Update(LLeaveType entity,LLeaveType dbEntity)
        {
            //LLeaveType dbEntity = PayrollDataContext.LLeaveTypes.SingleOrDefault(
            //    l => l.LeaveTypeId == entity.LeaveTypeId);

            if (dbEntity == null)
                return ;

            #region "Change Log"

            AddToChangeSetting(dbEntity, entity, MonetoryChangeLogTypeEnum.Leave, entity.Title,
                "Title", "Abbreviation", "Order", "EditSequence", "LeaveTypeId", "CompanyId","CountHolidayAsLeave","PastDaysForLeaveRequest","FutureDaysForLeaveRequest");

            #endregion


            dbEntity.EditSequence = entity.EditSequence;
            dbEntity.Title = entity.Title;
            dbEntity.LegendColor = entity.LegendColor;
            dbEntity.AppliesToGender = entity.AppliesToGender;
            //dbEntity.Abbreviation = entity.Abbreviation;
            dbEntity.Type = entity.Type;
            dbEntity.FreqOfAccrual = entity.FreqOfAccrual;
           
            //

            dbEntity.CompensatoryOnHolidayFulldayValue = entity.CompensatoryOnHolidayFulldayValue;
            dbEntity.CompensatoryOnHolidayHalfdayValue = entity.CompensatoryOnHolidayHalfdayValue;

            dbEntity.SkipProportionateCalculation = entity.SkipProportionateCalculation;
            dbEntity.WorkDaysPerPeriod = entity.WorkDaysPerPeriod;
            dbEntity.LeavePerPeriod = entity.LeavePerPeriod;
            dbEntity.ManualLeaveShowInLeaveRequest = entity.ManualLeaveShowInLeaveRequest;
            dbEntity.MannualMaximumPeriod = entity.MannualMaximumPeriod;
            dbEntity.IsHalfDayAllowed = entity.IsHalfDayAllowed;
            
            //unused leave
            dbEntity.UnusedLeave = entity.UnusedLeave;

            dbEntity.ProcessForContractStatusWithAccuralInRenew = entity.ProcessForContractStatusWithAccuralInRenew;

            dbEntity.LLeaveEncashmentIncomes.Clear();
            dbEntity.LLeaveEncashmentIncomes.AddRange(entity.LLeaveEncashmentIncomes);


            dbEntity.LLeaveLapses.Clear();
            dbEntity.LLeaveLapses.AddRange(entity.LLeaveLapses);

            dbEntity.LLeaveAppliesTos.Clear();
            dbEntity.LLeaveAppliesTos.AddRange(entity.LLeaveAppliesTos);

            dbEntity.LLeaveWorkDays.Clear();
            dbEntity.LLeaveWorkDays.AddRange(entity.LLeaveWorkDays);

            // service period based
            dbEntity.LeavePerPeriodBasedOnServiceYear = entity.LeavePerPeriodBasedOnServiceYear;
            dbEntity.ServicePeriodTypeDeductPrevYearAccural = entity.ServicePeriodTypeDeductPrevYearAccural;
            dbEntity.CarryForwardCustomLapseMonth = entity.CarryForwardCustomLapseMonth;
            dbEntity.LLeaveServicePeriods.Clear();
            dbEntity.LLeaveServicePeriods.AddRange(entity.LLeaveServicePeriods);

            dbEntity.EncaseOn = entity.EncaseOn;
            dbEntity.MaximiumBalance = entity.MaximiumBalance;
            dbEntity.YearlyMaximumBalance = entity.YearlyMaximumBalance;
            //dbEntity.Encase = entity.Encase;
            dbEntity.EncaseDays = entity.EncaseDays;
            dbEntity.MaxEncashOnRetirement = entity.MaxEncashOnRetirement;
            dbEntity.FirstProriotyForEncashment = entity.FirstProriotyForEncashment;
            //return UpdateChangeSet();

            dbEntity.CountHolidayAsLeave = entity.CountHolidayAsLeave;
            dbEntity.DoNotCountWeeklySaturdayAsLeave = entity.DoNotCountWeeklySaturdayAsLeave;
            dbEntity.DoNotCountPublicHolidayAsLeave = entity.DoNotCountPublicHolidayAsLeave;
            dbEntity.AllowNegativeLeaveRequest = entity.AllowNegativeLeaveRequest;  
            dbEntity.PastDaysForLeaveRequest = entity.PastDaysForLeaveRequest;
            dbEntity.FutureDaysForLeaveRequest = entity.FutureDaysForLeaveRequest;
        }

        //public bool UpdateLeaveToEmployee(LEmployeeLeave empLeave, bool forAll,
        //    bool applyEarnedPerPeriodToAll, int companyId)
        //{
        //    return PayrollDataContext.UpdateLeaveToEmployee(empLeave.LeaveTypeId, empLeave.EmployeeId, empLeave.BeginningBalance,
        //        empLeave.EarnedPerPeriod, empLeave.EarnedPerPeriodHoursDays, empLeave.IsBalanceUnlimited.Value, empLeave.MaxBalance, forAll, applyEarnedPerPeriodToAll, companyId) == 1;

           
        //}

        public bool RemoveLeaveFromEmployee(LEmployeeLeave entity)
        {
            LEmployeeLeave dbEntity = PayrollDataContext.LEmployeeLeaves.SingleOrDefault(
                  c => (c.EmployeeId == entity.EmployeeId && c.LeaveTypeId == entity.LeaveTypeId));


            if (dbEntity == null)
                return false;


            int deleteLeaveTypeId = dbEntity.LeaveTypeId;

            // child can not be removed, as only parent group will be removed and child will be automatically removed
            if (dbEntity.LLeaveType.IsChildLeave != null && dbEntity.LLeaveType.IsChildLeave.Value)
                return false;

            #region "Change Log"
            PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(
                  dbEntity.EmployeeId, (byte)MonetoryChangeLogTypeEnum.Leave, dbEntity.LLeaveType.Title,null,null, LogActionEnum.Detach)
                  );
            #endregion


            //check if leave used in LeaveAdjustment
            if (PayrollDataContext.LeaveAdjustments.Any(
                l => l.EmployeeId == entity.EmployeeId && l.LeaveTypeId == entity.LeaveTypeId))
            {
                dbEntity.IsActive = false;
                UpdateChangeSet();
            }
            else
            {
                PayrollDataContext.LEmployeeLeaves.DeleteOnSubmit(dbEntity);
                DeleteChangeSet();
            }



            LLeaveType parentLeave = GetLeaveTypeById(deleteLeaveTypeId);
            if (parentLeave != null && parentLeave.IsParentGroupLeave != null && parentLeave.IsParentGroupLeave.Value)
            {
                List<LLeaveType> child = PayrollDataContext.LLeaveTypes.Where
                    (x => x.ParentLeaveTypeId == deleteLeaveTypeId).ToList();

                foreach (LLeaveType item in child)
                {
                    LEmployeeLeave dbEntity1 = PayrollDataContext.LEmployeeLeaves.SingleOrDefault(
                 c => (c.EmployeeId == entity.EmployeeId && c.LeaveTypeId == item.LeaveTypeId));


                    if (dbEntity1 == null)
                         continue;

                    PayrollDataContext.LEmployeeLeaves.DeleteOnSubmit(dbEntity1);
                }

                DeleteChangeSet();
            }

            return true;
        }

        /// <summary>
        /// Retrieves the Leave list that will be displayed in Income addition
        /// so IsSalaryNotAffected property is used
        /// </summary>
        /// <param name="companyId"></param>
        /// <returns></returns>
        //public List<LLeaveType> GetLeavesByCompanyForIncome(int companyId)
        //{
        //    return PayrollDataContext.LLeaveTypes.Where(
        //       c => (c.CompanyId == companyId && c.IsSalaryNotAffected == false)).OrderBy(c => c.Title).ToList();
        //}

        public static List<LLeaveType> GetAllLeaves(int companyId)
        {
            //if(SessionManager.CurrentLoggedInEmployeeId ==0)
                return PayrollDataContext.LLeaveTypes.Where(
                   c => c.CompanyId == companyId).OrderBy(c => c.Title).ToList();
            //else // Hide Manual leave in leave types for Employee
            //    return PayrollDataContext.LLeaveTypes.Where(
            //       c => c.CompanyId == companyId && c.FreqOfAccrual != LeaveAccrue.MANUALLY)
            //       .OrderBy(c => c.Title).ToList();
        }

        public static List<GetLeaveListAsPerEmployeeResult> GetAllLeavesForLeaveRequest(int employeeId, int perviousPayrollPeriodId
            ,bool isLeaveRequest)
        {
            int daysDiff = 0; int remainingMonth = 0; 
            PayrollPeriod payrollPeriod = CommonManager.GetPayrollPeriod(perviousPayrollPeriodId);
            if (payrollPeriod.Month != SessionManager.CurrentCompany.LeaveStartMonth.Value)
            {

                daysDiff = GetYearlyLeaveMiddleJoiningDays(payrollPeriod, ref remainingMonth);
            }


            List<GetLeaveListAsPerEmployeeResult> leaves =
                PayrollDataContext.GetLeaveListAsPerEmployee(employeeId, perviousPayrollPeriodId, SessionManager.CurrentCompanyId, daysDiff, remainingMonth,false,null)
                //.Where(x=>x.FreqOfAccrual != LeaveAccrue.MANUALLY || x.ManualLeaveShowInLeaveRequest ==true)
                .ToList();


            return leaves.Where(x => x.NewBalance != null).ToList();
            
        }
        #region "Leave Adjustment"


        public static int GetYearlyLeaveMiddleJoiningDays(PayrollPeriod payrollPeriod,ref int remainingMonths)
        {
            DateTime startDate = payrollPeriod.StartDateEng.Value;
            CustomDate startCustomDate = CustomDate.GetCustomDateFromString(payrollPeriod.StartDate,IsEnglish);

            int leaveStartMonth = SessionManager.CurrentCompany.LeaveStartMonth.Value;
            DateTime endDate;
            //if payrol period month < leave start month, then take the days diferent form pay roll start month to leave last end month
            if (payrollPeriod.Month < leaveStartMonth)
            {
                CustomDate date1 = new CustomDate(1, leaveStartMonth, startCustomDate.Year, IsEnglish);
                endDate = date1.EnglishDate; //new DateTime(startDate.Year, leaveStartMonth, 1);
            }
            else // >
            //if payroll > leave, then take diff using next year leave start month
            {
                CustomDate date1 = new CustomDate(1, leaveStartMonth, startCustomDate.Year + 1, IsEnglish);
                endDate = date1.EnglishDate;//new DateTime(startDate.Year + 1, leaveStartMonth, 1);
            }
            endDate = endDate.AddDays(-1);

            remainingMonths = (int)(endDate.Subtract(startDate).Days / (365.25 / 12));
            remainingMonths += 1;

            if (remainingMonths < 0)
                remainingMonths = 0;

            int diff =  (endDate - startDate).Days;
            if (diff < 0)
                return 0;
            return diff;
        }

        public static  List<LeaveGenerateEmployeeAccuredResult> GetBeginningBalaceAccuredForEmployee(int employeeId,
            int payrollPeriodId,int companyId,string employeeIDList)
        {
            CommonManager mgr = new CommonManager();
            int daysDiff = 0;
            int remainingMonth = 0; // needed when status changed proportionate calculation exists in leave status effect
            
            PayrollPeriod payrollPeriod = CommonManager.GetPayrollPeriod(payrollPeriodId);
            if (payrollPeriod.Month != SessionManager.CurrentCompany.LeaveStartMonth.Value)
            {

                daysDiff = GetYearlyLeaveMiddleJoiningDays(payrollPeriod, ref remainingMonth);
            }

            return
                PayrollDataContext.LeaveGenerateEmployeeAccured(employeeId, payrollPeriodId, companyId, daysDiff, remainingMonth, employeeIDList).ToList
                    ();
        }


        public static double GetTotalDaysInFiscalYearForPeriod(int payrollPeriodId)
        {
            CommonManager mgr = new CommonManager();
            


            DateTime startdate, enddate;

            PayrollPeriod payrollPeriod = CommonManager.GetPayrollPeriod(payrollPeriodId);
            FinancialDate year = payrollPeriod.FinancialDate;

            CalculationConstant constant = PayrollDataContext.CalculationConstants.FirstOrDefault();

            if (SessionManager.CurrentCompany.IsEnglishDate)
            {
                // always previous year of ending will be starting
                startdate = new DateTime(year.EndingDateEng.Value.Year-1, constant.FiscalYearStartingMonthForEng.Value, 1);

                enddate = new DateTime(year.EndingDateEng.Value.Year, constant.FiscalYearEndingMonthForEng.Value,
                    DateHelper.GetTotalDaysInTheMonth(year.EndingDateEng.Value.Year, constant.FiscalYearEndingMonthForEng.Value, IsEnglish));

                // if 1 added then it is becoming 366 so don not add in this code
                return (enddate - startdate).TotalDays;

            }
            else
            {

                startdate = year.StartingDateEng.Value;
                enddate = year.EndingDateEng.Value;
                //CustomDate yearEnd = CustomDate.GetCustomDateFromString(year.EndingDate, false);
                //CustomDate yearStart = new CustomDate(1, constant.FiscalYearStartingMonthForNep.Value, yearEnd.Year - 1, false);


                //startdate = yearStart.EnglishDate;
                //enddate = yearEnd.EnglishDate;

            }


            return (enddate - startdate).TotalDays + 1;

        }


        public static List<LeaveGetLeaveAdjustmentsResult> GetLeaveAdjustments(int payrollPeriodId,int leaveId, int currentPage, int pageSize, ref int totalRecords,string empIdName)
        {
            List<LeaveGetLeaveAdjustmentsResult> list = new List<LeaveGetLeaveAdjustmentsResult>();
            list = PayrollDataContext.LeaveGetLeaveAdjustments(payrollPeriodId, leaveId, currentPage, pageSize, empIdName).ToList();

            if (list.Count > 0)
                totalRecords = list[0].TotalRows.Value;
            else
                totalRecords = 0;
            return list;

        }

        public static List<LeaveGetFixedLeaveListResult> GetLeaveAdjustmentsForFixedLeave(int payrollPeriodId, string leaveAbbr, int currentPage, int pageSize, ref int totalRecords)
        {
            List<LeaveGetFixedLeaveListResult> list = new List<LeaveGetFixedLeaveListResult>();
            list = PayrollDataContext.LeaveGetFixedLeaveList(payrollPeriodId, leaveAbbr, currentPage, pageSize).ToList();
            if (list.Count > 0)
                totalRecords = list[0].TotalRows.Value;
            else
                totalRecords = 0;
            return list;

        }

        //public static int GetTotalDaysForEmployeeInAttedance(int employeeId, int payrollPeriodId)
        //{
        //    string[] values =
        //        PayrollDataContext.GetDisabledCellForSalaryStartAndRetRegDate(employeeId, payrollPeriodId)
        //        .Split(new char[] {':' });

        //    return int.Parse(values[1]) - int.Parse(values[0]) - 1;
        //}

        public static void CallToResaveAtte(int payrollPeriodId, string empIdList,int? leaveTypeId)
        {
            PayrollDataContext.ReSaveAttendanceOfFuturePayrollPeriod(payrollPeriodId, empIdList, LeaveAttendanceManager.GetLeaveYearStartDate(payrollPeriodId)
                , leaveTypeId);
        }


        public static bool IsMannualLeaveAssignedValidUsingTotalMaxBal(int empId, int month,int year, int leaveTypeId,double addedPeriod,ref double? total)
        {
            List<MannualLeaveAccure> list = PayrollDataContext.MannualLeaveAccures
               .Where(x => x.EmployeeId == empId
                   && x.LeaveTypeId == leaveTypeId && x.Month != month && x.Year != year).ToList();
            
            total = list.Sum(x => x.Period);

            if (total == null)
                total = 0;

            total += addedPeriod;

            LLeaveType type = GetLeaveTypeById(leaveTypeId);

            if (type.MannualMaximumPeriod == null || type.MannualMaximumPeriod == 0)
                return true;

            if (total > type.MannualMaximumPeriod)
                return false;

            return true;
        }

        public static Status SaveUpdateMannualAccural(MannualLeaveAccure leave)
        {
            Status status = new Status();
            MannualLeaveAccure dbLeave = PayrollDataContext.MannualLeaveAccures.FirstOrDefault
                (x => x.LeaveTypeId == leave.LeaveTypeId && x.EmployeeId == leave.EmployeeId
                    && x.Month == leave.Month && x.Year == leave.Year);

            if (dbLeave == null)
                PayrollDataContext.MannualLeaveAccures.InsertOnSubmit(leave);
            else
            {
                dbLeave.Notes = leave.Notes;
                dbLeave.Period = leave.Period;
                dbLeave.Date = leave.Date;
            }

            PayrollDataContext.SubmitChanges();


            // call to send changed balance to Attendance
            PayrollPeriod lastPeriod = CommonManager.GetPayrollPeriod(leave.Month, leave.Year);
            if (lastPeriod != null)
            {
                if (LeaveAttendanceManager.IsAttendanceSavedForEmployee(lastPeriod.PayrollPeriodId, leave.EmployeeId))
                    EmployeeManager.ResaveAttendanceOfEmployee(leave.EmployeeId, lastPeriod);
               
            }


            return status;
        }
        public static bool UpdateLeaveAdjustment(LeaveAdjustment entity)
        {
            LeaveAdjustment dbEntity =
                PayrollDataContext.LeaveAdjustments.Where(
                    l => (l.PayrollPeriodId == entity.PayrollPeriodId && l.LeaveTypeId == entity.LeaveTypeId
                          && l.EmployeeId == entity.EmployeeId)).SingleOrDefault();

            #region "Change Logs"
            if (dbEntity.Adjusted != entity.Adjusted)
                PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(dbEntity.EmployeeId, (byte)MonetoryChangeLogTypeEnum.Leave,
                    dbEntity.LLeaveType.Title + " : Adjustment", dbEntity.Adjusted, entity.Adjusted, LogActionEnum.Update));
            if (dbEntity.Notes != entity.Notes)
                PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(dbEntity.EmployeeId, (byte)MonetoryChangeLogTypeEnum.Leave,
                    dbEntity.LLeaveType.Title + " : Adjustment Note", dbEntity.Notes, entity.Notes, LogActionEnum.Update));
         
            #endregion


            dbEntity.Notes = entity.Notes;
            decimal orginalValue = Convert.ToDecimal(dbEntity.BeginningBalance) +
                Convert.ToDecimal(dbEntity.Accured) - Convert.ToDecimal(dbEntity.Taken) + Convert.ToDecimal(entity.Adjusted);

            dbEntity.Adjusted = entity.Adjusted;// (entity.NewBalance - dbEntity.InitialFixedNewBalance);
            dbEntity.NewBalance = orginalValue;
            dbEntity.InitialFixedNewBalance = orginalValue;
            //dbEntity.InitialFixedNewBalance = orginalValue;
            //dbEntity.NewBalance = orginalValue + dbEntity.Adjusted;

           // dbEntity.Adjusted = (entity.NewBalance - (decimal)dbEntity.InitialFixedNewBalance);

            UpdateChangeSet();

            //decimal? value = null;
            //double? dvalue = null;
            //PayrollDataContext.ProcessLeaveEncashment("Attendance", true, entity.EmployeeId, entity.PayrollPeriodId,
            //                      0, ref value, ref dvalue, ref value, ref dvalue, false);

            return true;
        }
        public static bool UpdateOTTotalHours(List<GetGeneratedOvertimeListResult> list,int overtimeId)
        {
            List<int> empIDList = list.Select(x=>x.EIN).ToList();

            OvertimePeriod overtime = PayrollDataContext.OvertimePeriods.FirstOrDefault(x=>x.OvertimeID==overtimeId);

            //AddOnHeader addOnHeader = PayrollDataContext.AddOnHeaders
            //    .FirstOrDefault(x=>x.AddOnId == overtime.AddOnId && x.Type == 1 && x.SourceId == 
            //        CommonManager.Setting.OvertimeIncomeId.Value);

            List<OvertimePeriodEmployee> dbList =
                PayrollDataContext.OvertimePeriodEmployees.Where(x => x.OvertimeID == overtimeId
                    && empIDList.Contains(x.EmployeeId)).ToList();

            //List<AddOnDetail> addOnDetail = PayrollDataContext.AddOnDetails
            //    .Where(x => x.AddOnHeaderId == addOnHeader.AddOnHeaderId && empIDList.Contains(x.EmployeeId)).ToList();


            foreach (GetGeneratedOvertimeListResult ot in list)
            {
                OvertimePeriodEmployee dbOT = dbList.FirstOrDefault(x => x.EmployeeId == ot.EIN);
                //AddOnDetail dbAddOn = addOnDetail.FirstOrDefault(x => x.EmployeeId == ot.EIN);

                if (dbOT != null)
                {
                    TimeSpan hourMin = TimeSpan.FromHours(ot.NewOTHour);
                    dbOT.TotalOTMinute = (int) hourMin.TotalMinutes;

                    dbOT.OTAmount = (decimal)ot.NewOTHour * dbOT.HourlyRate;

                    //if (dbAddOn != null)
                    //{
                    //    dbAddOn.Amount = dbOT.OTAmount;
                    //}
                }
            }


            UpdateChangeSet();

            return true;
        }
        #endregion

        #region "Opening balance"

        public LeaveOpeningBalEntityCol GetLeaveOpeningBalance(int companyId,
                int leaveTypeId,string searchText, int currentPage, int pageSize, ref int totalRows,string statusFilterList)
        {
            LeaveOpeningBalEntityCol list =new LeaveOpeningBalEntityCol();

            List<KeyValue> statusList = new JobStatus().GetMembers();

            foreach (var dbEntity in PayrollDataContext.GetLeaveOpeningBalance(companyId, leaveTypeId, searchText, currentPage, pageSize, statusFilterList).ToList())
            {
                LeaveOpeningBalEntity entity = new LeaveOpeningBalEntity();
                entity.EmployeeId = dbEntity.EmployeeId;
                entity.IsOpeningBalanceSaved = dbEntity.IsOpeningBalanceSaved;
                entity.IsPaymentStopped = dbEntity.IsPaymentStopped.Value;
                entity.LeaveTypeId = dbEntity.LeaveTypeId;
                entity.Name = dbEntity.Name;
                entity.OpeningBalance = dbEntity.OpeningBalance;
                if (dbEntity.CurrentYearBalance == null)
                    entity.CurrentYearBalance = 0;
                else
                    entity.CurrentYearBalance = dbEntity.CurrentYearBalance.Value;

                if (dbEntity.OpeningTaken == null)
                    entity.OpeningTaken = 0;
                else
                    entity.OpeningTaken = dbEntity.OpeningTaken.Value;

                entity.LeaveName = dbEntity.Title;
                entity.IsAttedanceSaved = dbEntity.IsAttedanceSaved.Value;
                entity.RowNumber = dbEntity.RowNumber.Value;
                entity.TotalRecords = dbEntity.TotalRows.Value;

                if (dbEntity.Status != null)
                {
                    KeyValue status = statusList.FirstOrDefault(x => x.Key == dbEntity.Status.ToString());
                    if (status != null)
                        entity.StatusText = status.Value;
                }
                list.Add(entity);
            }
            if (list.Count > 0)
                totalRows = list[0].TotalRecords;
            else
                totalRows = 0;
            
            return list;
        }

        public bool SaveLeaveOpeningBalance(string xml)
        {

            PayrollDataContext.SaveLeaveOpeningBalance(XElement.Parse(xml),SessionManager.CurrentCompanyId,
                SessionManager.User.UserName);
            return true;
        }

        //public int? GetEmployeeOpeningBalance(int employeeId, int leaveTypeId)
        //{
        //    LOpeningBalance bal =
        //        PayrollDataContext.LOpeningBalances.SingleOrDefault(
        //            b => (b.EmployeeId == employeeId && b.LeaveTypeId == leaveTypeId));
        //    if (bal == null)
        //        return null;
        //    return bal.OpeningBalance.Value;
        //}
        #endregion

        #region "Attedence"

        public static List<EEmployee> GetSelectedEmployeeList(List<int> einList)
        {
            return
                (
                   from e in PayrollDataContext.EEmployees
                   // where a.PayrollPeriodId == payrollPeriodId
                   where einList.Contains(e.EmployeeId)
                   select e

                ).ToList();
        }

        public static List<EEmployee> GetEmployeeListInAttendance(int payrollPeriodId)
        {
            return 
                (
                    from a in PayrollDataContext.Attendences
                    join e in PayrollDataContext.EEmployees on a.EmployeeId equals e.EmployeeId
                    where a.PayrollPeriodId == payrollPeriodId
                    select e

                ).ToList();
        }

        public static List<GetAttandenceListResult> GetAttedenceList(int companyId, int payrollPeriodId,
            string empSearchText, string type, int id, int currentPage, int pageSize, ref int? total,int minStatus,bool statusOnly
            , bool IsAtteReadonlyForAutoGeneration,int unitId)
        {
            currentPage -= 1;
            total = 0;

            string branchList = null;
            if (SessionManager.IsCustomRole)
                branchList = SessionManager.CustomRoleBranchIDList;

            List<GetAttandenceListResult> list = 
                PayrollDataContext.GetAttandenceList(companyId, payrollPeriodId, empSearchText, type, id,
                currentPage, pageSize, branchList, minStatus, statusOnly, IsAtteReadonlyForAutoGeneration, unitId).ToList();

            if (list.Count > 0)
                total = list[0].TotalRows.Value;

            return list;
        }

        public static List<KeyValue> GetAttendenceCellOptions(int companyId)
        {
            List<LLeaveType> leaves = PayrollDataContext.LLeaveTypes.
                Where(e => e.CompanyId == companyId).OrderBy(e => e.Abbreviation).ToList();

            List<KeyValue> options = new List<KeyValue>();

            foreach (LLeaveType leave in leaves)
            {
                KeyValue option = new KeyValue(leave.Title, leave.Abbreviation);
                options.Add(option);
            }

            //KeyValue p = new KeyValue("P", "P");
            //options.Insert(0, p);
            //Add default leaves
            for (int i = DefaultLeaves.Length-1; i >= 0; i--)
            {
                string leave = DefaultLeaves[i][0];
                KeyValue cellLeave = new KeyValue(leave, leave);
                options.Insert(0, cellLeave);
            }

            //add holidays also
            KeyValue h1 = new KeyValue("NH", "NH");
            KeyValue h2 = new KeyValue("FH", "FH");
            KeyValue h3 = new KeyValue("CH", "CH");
            KeyValue h4 = new KeyValue("WH", "WH");
            options.Add(h1);
            options.Add(h2);
            options.Add(h3);
            options.Add(h4);

            return options;
        }

        public static string[] SaveAttendence( int payrollPeriodId,string xmlEmployeeList,string xmlCellList,string xmlLeaveAdjList,ref int? errorStatusCode)
        {
            List<SaveAttendenceResult> list =
            PayrollDataContext.SaveAttendence(payrollPeriodId, XElement.Parse(xmlEmployeeList), XElement.Parse(xmlCellList), XElement.Parse(xmlLeaveAdjList),ref errorStatusCode
            , LeaveAttendanceManager.GetLeaveYearStartDate(payrollPeriodId),SessionManager.User.UserID).ToList();

            return list.Select(x => x.Name).ToArray();
        }

        public static void GenerateMonthlyAttendance(int payrollPeriodId, int employeeId)
        {
            int? errorStatusCode = 0;
            int daysDiff = 0;
            int remainingMonth = 0; // needed when status changed proportionate calculation exists in leave status effect


            // generate automatically prev month attendance also if current period is the last one
            if (payrollPeriodId == CommonManager.GetLastPayrollPeriod().PayrollPeriodId)
            {
                PayrollPeriod prevPeriod = PayrollDataContext.PayrollPeriods.OrderByDescending(x => x.PayrollPeriodId)
                    .Where(x => x.PayrollPeriodId != payrollPeriodId).FirstOrDefault();
                if (prevPeriod != null)
                {
                    if (prevPeriod.Month != SessionManager.CurrentCompany.LeaveStartMonth.Value)
                    {

                        daysDiff = GetYearlyLeaveMiddleJoiningDays(prevPeriod, ref remainingMonth);
                    }

                    PayrollDataContext.GenerateMonthlyAttendence(prevPeriod.PayrollPeriodId, ref errorStatusCode,
                        LeaveAttendanceManager.GetLeaveYearStartDate(prevPeriod.PayrollPeriodId),
                        SessionManager.User.UserID, daysDiff,
                        remainingMonth, employeeId);
                }
            }



            errorStatusCode = 0;
            daysDiff = 0;
            remainingMonth = 0;

            PayrollPeriod payrollPeriod = CommonManager.GetPayrollPeriod(payrollPeriodId);
            if (payrollPeriod.Month != SessionManager.CurrentCompany.LeaveStartMonth.Value)
            {

                daysDiff = GetYearlyLeaveMiddleJoiningDays(payrollPeriod, ref remainingMonth);
            }

            PayrollDataContext.GenerateMonthlyAttendence(payrollPeriodId, ref errorStatusCode,
                LeaveAttendanceManager.GetLeaveYearStartDate(payrollPeriodId), SessionManager.User.UserID, daysDiff,
                remainingMonth, employeeId);


        }

        #endregion

        public static List<LLeaveType> GetLegendsForHolidays()
        {
            List<LLeaveType> holidayLegends = new List<LLeaveType>();

            LLeaveType h1 = new LLeaveType();
            h1.Title = "National Holiday";
            h1.Abbreviation = HolidaysConstant.National_Holiday;
            h1.LegendColor = HolidaysConstant.GetColor(HolidaysConstant.National_Holiday);
            holidayLegends.Add(h1);

            LLeaveType h2 = new LLeaveType();
            h2.Title = "Female Holiday";
            h2.Abbreviation = HolidaysConstant.Female_Holiday;
            h2.LegendColor = HolidaysConstant.GetColor(HolidaysConstant.Female_Holiday);
            holidayLegends.Add(h2);

            if (CommonManager.CompanySetting.IsD2 == false)
            {
                LLeaveType h3 = new LLeaveType();
                h3.Title = "Caste Holiday";
                h3.Abbreviation = HolidaysConstant.Caste_Holiday;
                h3.LegendColor = HolidaysConstant.GetColor(HolidaysConstant.Caste_Holiday);
                holidayLegends.Add(h3);
            }

            LLeaveType h4 = new LLeaveType();
            h4.Title = "Weekly Holiday";
            h4.Abbreviation = HolidaysConstant.Weekly_Holiday;
            h4.LegendColor = HolidaysConstant.GetColor(HolidaysConstant.Weekly_Holiday);
            holidayLegends.Add(h4);
           
          
            
           
            return holidayLegends;
        }


        #region LeaveTransfer
        //Below line of code are added by ram
        //
        public static List<LLeaveType> GetYearlyMonthlyLeaves(int companyId)
        {
            return PayrollDataContext.LLeaveTypes
                .Where(c => c.CompanyId == companyId)
                .Where(c => c.FreqOfAccrual == LeaveAccrue.YEARLY || c.FreqOfAccrual.ToLower() == LeaveAccrue.MONTHLY
                 ||c.FreqOfAccrual == LeaveAccrue.HALFYEARLY)
                .OrderBy(c => c.Title).ToList();
        }
        /// <summary>
        /// Fetch Employees of nominated company, Where Employee are assigned leave type of frequency of accural are Monthly and Yearly 
        /// </summary>
        /// <param name="companyId">CompanyId of which Employees are to be fetched.</param>
        /// <returns></returns>
        public static List<BLL.Entity.LeaveTransfer> GetEmployeesForLeaveBalance(int companyId, int leaveTypeId, int payrollPeriodId)
        {
            var query = from e in PayrollDataContext.EEmployees
                        join ld in PayrollDataContext.LeaveAdjustments on e.EmployeeId equals ld.EmployeeId
                        join lt in PayrollDataContext.LLeaveTypes on ld.LeaveTypeId equals lt.LeaveTypeId
                        where lt.FreqOfAccrual == LeaveAccrue.YEARLY || lt.FreqOfAccrual == LeaveAccrue.HALFYEARLY || lt.FreqOfAccrual.ToLower() == LeaveAccrue.MONTHLY
                        where e.CompanyId == companyId
                        where ld.PayrollPeriodId == payrollPeriodId
                        where ld.LeaveTypeId == leaveTypeId
                        select new BLL.Entity.LeaveTransfer
                        {
                            EmployeeId = e.EmployeeId,
                            EmployeeName = e.Name,
                            NewBalance = ld.NewBalance
                        };
            //{ e.EmployeeId, e.Name, ld.NewBalance};
            return query.ToList();
        }

        public static Boolean SaveEmployeeLeaveTransfer(DAL.LeaveTransfer leavetransfer)
        {
            PayrollDataContext.LeaveTransfers.InsertOnSubmit(leavetransfer);
            PayrollDataContext.SubmitChanges();
            //return SaveChangeSet();
            return true;
        }

        public static List<GetLeaveTrasferEmployeeResult> GetLeaveTrasferEmployee(int CompanyId, int leaveTypeId, int payrollPeriodId)
        {
            return PayrollDataContext.GetLeaveTrasferEmployee((int)CompanyId, (int)leaveTypeId, (int)payrollPeriodId).ToList();
        }

        public static bool UpdateNewBalance(List<LeaveAdjustment> listLvAdj)
        {
            Boolean updatestatus = true;
            LeaveAdjustment lv;
            foreach (LeaveAdjustment la in listLvAdj)
            {
                lv = PayrollDataContext.LeaveAdjustments.Where(x => x.PayrollPeriodId == la.PayrollPeriodId && x.LeaveTypeId == la.LeaveTypeId && x.EmployeeId == la.EmployeeId).FirstOrDefault();
                lv.NewBalance = la.NewBalance;
                PayrollDataContext.SubmitChanges();
                //if (!SaveChangeSet())
                //    updatestatus = false;
            }
            return updatestatus;

        }

        /// <summary>
        /// Checks if the payroll period is valid for leave transfer
        /// </summary>
        /// <param name="inputPayrollPeriodId"></param>
        /// <returns></returns>
        public static bool IsPayrollPeriodValidForLeaveTransfer(int inputPayrollPeriodId)
        {
            //Get last final saved payroll period & check if atte saved then only this 
            //payroll period is editable
            var lastFinalSavedPayrollPeriodId
                 = (from p in PayrollDataContext.PayrollPeriods
                     orderby p.PayrollPeriodId descending
                    select p.PayrollPeriodId
                   )

                   .Take(1).SingleOrDefault();

            bool isAtteSavedForLastPayroll
                = PayrollDataContext.Attendences.Any(a => a.PayrollPeriodId == lastFinalSavedPayrollPeriodId);


            //if last atte saved, then that payroll only valid
            if (isAtteSavedForLastPayroll)
            {
                if (lastFinalSavedPayrollPeriodId == inputPayrollPeriodId)
                    return true;
                else
                    return false;
            }

            //else check for seccond last

            var secondLastPayrollPeriodId
                = (from p in PayrollDataContext.PayrollPeriods
                   orderby p.PayrollPeriodId descending
                   where p.PayrollPeriodId != lastFinalSavedPayrollPeriodId
                   select p.PayrollPeriodId
                  )

                  .Take(1).SingleOrDefault();

            if (secondLastPayrollPeriodId == inputPayrollPeriodId)
                return true;


            else
                return false;
        }

        #endregion


        #region LeaveRequest

        public static List<GetEmployeeLeaveBalanceResult> GetEmployeeLeaveBalance(int employeeId,bool isLeaveRequest
            , List<GetLeaveListAsPerEmployeeResult> list)
        {
            PayrollPeriod lastPayroll = CommonManager.GetLastPayrollPeriod();

            if (lastPayroll != null)
            {
                //List<GetLeaveListAsPerEmployeeResult> list = LeaveAttendanceManager.GetAllLeavesForLeaveRequest(employeeId, lastPayroll.PayrollPeriodId, isLeaveRequest);

                return (from l in list
                        select new GetEmployeeLeaveBalanceResult
                        {
                           FreqOfAccrual = l.FreqOfAccrual,
                            LeaveTypeId = l.LeaveTypeId,
                            Title = l.Title,
                            NewBalance = l.NewBalance == null ? 0 : (decimal)l.NewBalance.Value,
                            IsParentGroupLeave = l.IsParentGroupLeave
                        }).ToList();
            }
            else
                return PayrollDataContext.GetEmployeeLeaveBalance(employeeId).ToList();
        }

        public static bool GetLeaveRequestCountHolidayAsLeaveSetting(int leaveTypeId,
            ref bool onlyCountPublicHolidayAsLeaveWeeklySaturdayIsHoliday,ref bool onlyCountWeeklySaturdayAsLeavePublicHolidayIsHoliday)
        {
            onlyCountPublicHolidayAsLeaveWeeklySaturdayIsHoliday = false;
            onlyCountWeeklySaturdayAsLeavePublicHolidayIsHoliday = false;
            bool CountHolidayInBetweenLeave = CommonManager.CompanySetting.LeaveRequestCountHolidayInBetweenLeave;
            LLeaveType leaveType = new LeaveAttendanceManager().GetLeaveType(leaveTypeId);
            if (leaveType != null && leaveType.CountHolidayAsLeave != null)
            {
                CountHolidayInBetweenLeave = leaveType.CountHolidayAsLeave.Value;
                if (leaveType.DoNotCountWeeklySaturdayAsLeave != null)
                    onlyCountPublicHolidayAsLeaveWeeklySaturdayIsHoliday = leaveType.DoNotCountWeeklySaturdayAsLeave.Value;

                if (leaveType.DoNotCountPublicHolidayAsLeave != null)
                    onlyCountWeeklySaturdayAsLeavePublicHolidayIsHoliday = leaveType.DoNotCountPublicHolidayAsLeave.Value;
            }
            return CountHolidayInBetweenLeave;
        }

        /// <summary>
        /// Check if day is leave day or not, else means it is holiday so don't mark for leave
        /// </summary>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <param name="currentDate"></param>
        /// <returns></returns>
        public static bool IsLeaveDay(DateTime? start, DateTime? end, DateTime currentDate,bool isDayIsHoliday,bool countHolidayInBetweenLeave)
        {
            if (isDayIsHoliday == false)
                return true;
            
            if (start != null && start == currentDate)
                return true;
            if (end != null && end == currentDate)
                return true;

            if (isDayIsHoliday && countHolidayInBetweenLeave)
                return false;

            return true;

        }

        public static Boolean SaveUpdateParentLeaveRequest
            (LeaveRequest leave, ref bool isValueChangedForEmailInRequestUpdate)
        {
            isValueChangedForEmailInRequestUpdate = false;
            bool isInsert = leave.LeaveRequestId == 0;
            float daysCount = 0;

            //bool CountHolidayInBetweenLeave = LeaveAttendanceManager.GetLeaveRequestCountHolidayAsLeaveSetting(
            //    leave.ChildLeaveTypeId != null ? leave.ChildLeaveTypeId.Value : leave.LeaveTypeId);

            //HolidayManager hmanager = new HolidayManager();
            //List<GetHolidaysForAttendenceResult> holiday = hmanager.GetMonthsHolidaysForAttendence(0, 0, SessionManager.CurrentCompanyId, 0, leave.FromDateEng, leave.ToDateEng).ToList();
            EEmployee emp = EmployeeManager.GetEmployeeById(leave.EmployeeId);
            LeaveDetail ld = null;
            bool holidayExists = false;
            // if not created by HR then only 
            if (leave.IsRequestCreatedFromHR == null || leave.IsRequestCreatedFromHR == false)
            {
                leave.CreatedBy = leave.EmployeeId;
            }
            //
            if (isInsert)
            {
                            
                PayrollDataContext.LeaveRequests.InsertOnSubmit(leave);
                isValueChangedForEmailInRequestUpdate = true;
                return SaveChangeSet();
            }
            else
            {
                LeaveRequest uLq = PayrollDataContext.LeaveRequests.Where(x => x.LeaveRequestId == leave.LeaveRequestId).FirstOrDefault();

                if (uLq.FromDateEng != leave.FromDateEng
                    || uLq.ToDateEng != leave.ToDateEng
                    || uLq.RecommendEmployeeId != leave.RecommendEmployeeId
                    || uLq.SupervisorEmployeeIdInCaseOfSelection != leave.SupervisorEmployeeIdInCaseOfSelection
                    )
                {
                    isValueChangedForEmailInRequestUpdate = true;
                }

                uLq.IsHour = leave.IsHour;
                uLq.IsHalfDayLeave = leave.IsHalfDayLeave;
                uLq.DaysOrHours = leave.DaysOrHours;
                uLq.HalfDayType = leave.HalfDayType;
                uLq.IsSpecialCase = leave.IsSpecialCase;
                uLq.SpecialComment = leave.SpecialComment;

                uLq.SubstituteEmployeeId = leave.SubstituteEmployeeId;
                uLq.Days1 = leave.Days1;
                uLq.Days2 = leave.Days2;
                uLq.CompensatorIsAddType = leave.CompensatorIsAddType;
                uLq.FromDate = leave.FromDate;
                uLq.FromDateEng = leave.FromDateEng;
                uLq.ToDate = leave.ToDate;
                uLq.ToDateEng = leave.ToDateEng;

                uLq.IsHour = leave.IsHour;
                uLq.StartTime = leave.StartTime;
                uLq.EndTime = leave.EndTime;

                uLq.LeaveTypeId = leave.LeaveTypeId;
                uLq.LeaveTypeId2 = leave.LeaveTypeId2;
                uLq.ChildLeaveTypeId = leave.ChildLeaveTypeId;
                uLq.ChildLeaveTypeId2 = leave.ChildLeaveTypeId2;

                uLq.Reason = leave.Reason;
                uLq.ModifiedOn = GetCurrentDateAndTime();
                uLq.EditSequence = leave.EditSequence;
                uLq.Status = leave.Status;
                uLq.SubstituteEmployeeId = leave.SubstituteEmployeeId;
                uLq.SupervisorEmployeeIdInCaseOfSelection = leave.SupervisorEmployeeIdInCaseOfSelection;

                uLq.RecommendEmployeeId = leave.RecommendEmployeeId;

                uLq.LeaveDetails.Clear();

                uLq.DaysOrHours = leave.DaysOrHours;
                uLq.LeaveDetails.AddRange(leave.LeaveDetails);


                return UpdateChangeSet();
            }
        }
        public static Boolean SaveUpdateLeaveRequest(LeaveRequest leave,ref bool isValueChangedForEmailInRequestUpdate)
        {
            isValueChangedForEmailInRequestUpdate = false;
            bool isInsert = leave.LeaveRequestId == 0;
            float daysCount = 0;

            bool onlyCountPublicHolidayAsLeaveWeeklySaturdayIsHoliday = false;
            bool onlyCountWeeklySaturdayAsLeavePublicHolidayIsHoliday = false;
            bool CountHolidayInBetweenLeave = LeaveAttendanceManager.GetLeaveRequestCountHolidayAsLeaveSetting(
                leave.LeaveTypeId, ref onlyCountPublicHolidayAsLeaveWeeklySaturdayIsHoliday,ref onlyCountWeeklySaturdayAsLeavePublicHolidayIsHoliday);

            HolidayManager hmanager = new HolidayManager();
            List<GetHolidaysForAttendenceResult> holiday = hmanager.GetMonthsHolidaysForAttendence(0, 0, SessionManager.CurrentCompanyId, 0, leave.FromDateEng, leave.ToDateEng, leave.EmployeeId).ToList();
            EEmployee emp = EmployeeManager.GetEmployeeById(leave.EmployeeId);
            LeaveDetail ld = null;
            bool holidayExists = false;
            // if not created by HR then only 
            if (leave.IsRequestCreatedFromHR == null || leave.IsRequestCreatedFromHR == false)
            {
                leave.CreatedBy = leave.EmployeeId;
            }
                //
            if (isInsert)
            {
               
                if (leave.IsHour.Value || leave.IsHalfDayLeave.Value)
                {
                    ld = new LeaveDetail();
                    ld.LeaveTypeId = (leave.ChildLeaveTypeId != null ? leave.ChildLeaveTypeId.Value : leave.LeaveTypeId);
                    ld.DateOn = GetAppropriateDate(leave.FromDateEng.Value);
                    ld.DateOnEng = leave.FromDateEng.Value.Date;
                    if (leave.IsHour != null && leave.IsHour.Value && leave.CompensatorIsAddType != null && leave.CompensatorIsAddType.Value)
                        ld.IsHolidayForCompensatoryType = leave.CompensatorIsAddType.Value;
                    leave.LeaveDetails.Add(ld);
                }
                else
                {

                    int day = 1;
                    for (int i = 0; i <= (leave.ToDateEng.Value - leave.FromDateEng.Value).Days; i++)
                    {
                        
                        DateTime date = leave.FromDateEng.Value.AddDays(i);
                        holidayExists  = IsHolidayExistOnLeaveRequestDay(emp.EmployeeId, date, CountHolidayInBetweenLeave,onlyCountPublicHolidayAsLeaveWeeklySaturdayIsHoliday,onlyCountWeeklySaturdayAsLeavePublicHolidayIsHoliday, holiday);

                        int dateleaveTypeId = GetLeaveTypeIdForLeaveRequest(leave, date);
                        LLeaveType leaveType = new LeaveAttendanceManager().GetLeaveType(dateleaveTypeId);
                        if (leaveType != null && leaveType.FreqOfAccrual == LeaveAccrue.COMPENSATORY)
                        {
                            ld = new LeaveDetail();
                            ld.DateOn = GetAppropriateDate(date);
                            ld.DateOnEng = date;
                            ld.LeaveTypeId = dateleaveTypeId;
                            ld.IsHolidayForCompensatoryType = leave.CompensatorIsAddType.Value;
                            
                            leave.LeaveDetails.Add(ld);

                            daysCount += 1;
                        }
                        else if (IsDishHomeSundayLeave(leave.LeaveTypeId))
                        {
                            // sunday leave is taken for 3 days, first day half day mark but not leave counting, second will be holiday so no leave & 3rd day full day leave
                            if (day == 1 || day == 3)
                            {
                                ld = new LeaveDetail();
                                ld.DateOn = GetAppropriateDate(date);
                                ld.DateOnEng = date;
                                ld.LeaveTypeId = dateleaveTypeId;
                                ld.Day = day;
                                leave.LeaveDetails.Add(ld);                                
                            }
                            else if (day == 2)
                            {
                                daysCount += 1;
                            }
                            

                            day += 1;
                        }
                        else if (IsLeaveDay(leave.FromDateEng,leave.ToDateEng,date,holidayExists,CountHolidayInBetweenLeave))
                        {
                            ld = new LeaveDetail();
                            ld.DateOn = GetAppropriateDate(date);
                            ld.DateOnEng = date;
                            ld.LeaveTypeId = dateleaveTypeId;
                            leave.LeaveDetails.Add(ld);

                            daysCount += 1;
                        }
                    }
                    // Change leave days as could be effected by Holidays in between Leave
                    leave.DaysOrHours = daysCount;
                }
                PayrollDataContext.LeaveRequests.InsertOnSubmit(leave);
                isValueChangedForEmailInRequestUpdate = true;
                return SaveChangeSet();
            }
            else
            {
                LeaveRequest uLq = PayrollDataContext.LeaveRequests.Where(x => x.LeaveRequestId == leave.LeaveRequestId).FirstOrDefault();

                if (uLq.FromDateEng != leave.FromDateEng
                    || uLq.ToDateEng != leave.ToDateEng
                    || uLq.RecommendEmployeeId != leave.RecommendEmployeeId
                    || uLq.SupervisorEmployeeIdInCaseOfSelection != leave.SupervisorEmployeeIdInCaseOfSelection
                    )
                {
                    isValueChangedForEmailInRequestUpdate = true;
                }
                
                uLq.IsHour = leave.IsHour;
                uLq.IsHalfDayLeave = leave.IsHalfDayLeave;
                uLq.DaysOrHours = leave.DaysOrHours;
                uLq.HalfDayType = leave.HalfDayType;
                uLq.IsSpecialCase = leave.IsSpecialCase;
                uLq.SpecialComment = leave.SpecialComment;

                uLq.SubstituteEmployeeId = leave.SubstituteEmployeeId;
                uLq.Days1 = leave.Days1;
                uLq.Days2 = leave.Days2;
                uLq.CompensatorIsAddType = leave.CompensatorIsAddType;
                uLq.FromDate = leave.FromDate;
                uLq.FromDateEng = leave.FromDateEng;
                uLq.ToDate = leave.ToDate;
                uLq.ToDateEng = leave.ToDateEng;

                uLq.IsHour = leave.IsHour;
                uLq.StartTime = leave.StartTime;
                uLq.EndTime = leave.EndTime;

                uLq.LeaveTypeId = leave.LeaveTypeId;
                uLq.LeaveTypeId2 = leave.LeaveTypeId2;
                uLq.ChildLeaveTypeId = leave.ChildLeaveTypeId;
                uLq.ChildLeaveTypeId2 = leave.ChildLeaveTypeId2;

                uLq.Reason = leave.Reason;
                uLq.ModifiedOn = GetCurrentDateAndTime();
                uLq.EditSequence = leave.EditSequence;
                uLq.Status = leave.Status;
                uLq.SubstituteEmployeeId = leave.SubstituteEmployeeId;
                uLq.SupervisorEmployeeIdInCaseOfSelection = leave.SupervisorEmployeeIdInCaseOfSelection;
              
                uLq.RecommendEmployeeId = leave.RecommendEmployeeId;

                uLq.LeaveDetails.Clear();

                if (leave.IsHour.Value || leave.IsHalfDayLeave.Value)
                {
                    //if (leave.IsHalfDayLeave.Value)
                    //    uLq.DaysOrHours = 0.5;
                    ld = new LeaveDetail();
                    ld.LeaveTypeId = (leave.ChildLeaveTypeId != null ? leave.ChildLeaveTypeId.Value : leave.LeaveTypeId);
                    ld.DateOn = GetAppropriateDate(leave.FromDateEng.Value);
                    ld.DateOnEng = leave.FromDateEng.Value.Date;
                    if (leave.IsHour != null && leave.IsHour.Value && leave.CompensatorIsAddType != null && leave.CompensatorIsAddType.Value)
                        ld.IsHolidayForCompensatoryType = leave.CompensatorIsAddType.Value;
                    uLq.LeaveDetails.Add(ld);
                }
                else
                {
                    int day = 1;
                    for (int i = 0; i <= (leave.ToDateEng.Value - leave.FromDateEng.Value).Days; i++)
                    {
                        DateTime date = leave.FromDateEng.Value.AddDays(i);
                        holidayExists = IsHolidayExistOnLeaveRequestDay(emp.EmployeeId, date, CountHolidayInBetweenLeave,onlyCountPublicHolidayAsLeaveWeeklySaturdayIsHoliday,onlyCountWeeklySaturdayAsLeavePublicHolidayIsHoliday, holiday);
                        int dateleaveTypeId = GetLeaveTypeIdForLeaveRequest(leave, date);
                        LLeaveType leaveType = new LeaveAttendanceManager().GetLeaveType(dateleaveTypeId);
                        if (leaveType.FreqOfAccrual == LeaveAccrue.COMPENSATORY)
                        {
                            ld = new LeaveDetail();
                            ld.DateOn = GetAppropriateDate(date);
                            ld.DateOnEng = date;
                            ld.LeaveTypeId = dateleaveTypeId;
                            ld.IsHolidayForCompensatoryType = leave.CompensatorIsAddType.Value;

                            uLq.LeaveDetails.Add(ld);

                            daysCount += 1;
                        }
                        else if (IsDishHomeSundayLeave(leave.LeaveTypeId))
                        {
                            // sunday leave is taken for 3 days, first day half day mark but not leave counting, second will be holiday so no leave & 3rd day full day leave
                            if (day == 1 || day == 3)
                            {
                                ld = new LeaveDetail();
                                ld.DateOn = GetAppropriateDate(date);
                                ld.DateOnEng = date;
                                ld.LeaveTypeId = dateleaveTypeId;
                                ld.Day = day;
                                uLq.LeaveDetails.Add(ld);
                            }
                            else if (day == 2)
                            {
                                daysCount += 1;
                            }


                            day += 1;
                        }
                        else if (IsLeaveDay(leave.FromDateEng, leave.ToDateEng, date, holidayExists, CountHolidayInBetweenLeave))
                        {
                            ld = new LeaveDetail();
                            ld.DateOn = GetAppropriateDate(date);
                            ld.DateOnEng = leave.FromDateEng.Value.AddDays(i);
                            ld.LeaveTypeId = dateleaveTypeId;
                            uLq.LeaveDetails.Add(ld);

                            daysCount += 1;
                        }
                    }
                    // Change leave days as could be effected by Holidays in between Leave
                    uLq.DaysOrHours = daysCount;
                }

               

                return UpdateChangeSet();
            }
        }

        public static Boolean SaveUpdateCancelRequest(LeaveRequest leave, ref bool isValueChangedForEmailInRequestUpdate)
        {
            isValueChangedForEmailInRequestUpdate = false;
            bool isInsert = leave.LeaveRequestId == 0;
            float daysCount = 0;

            //bool onlyCountPublicHolidayAsLeaveWeeklySaturdayIsHoliday = false;
            //bool onlyCountWeeklySaturdayAsLeavePublicHolidayIsHoliday = false;
            //bool CountHolidayInBetweenLeave = LeaveAttendanceManager.GetLeaveRequestCountHolidayAsLeaveSetting(
            //    leave.LeaveTypeId, ref onlyCountPublicHolidayAsLeaveWeeklySaturdayIsHoliday,ref onlyCountWeeklySaturdayAsLeavePublicHolidayIsHoliday);

            HolidayManager hmanager = new HolidayManager();
            List<GetHolidaysForAttendenceResult> holiday = hmanager.GetMonthsHolidaysForAttendence(0, 0, SessionManager.CurrentCompanyId, 0, leave.FromDateEng, leave.ToDateEng, leave.EmployeeId).ToList();
            EEmployee emp = EmployeeManager.GetEmployeeById(leave.EmployeeId);
            LeaveDetail ld = null;
            bool holidayExists = false;
            // if not created by HR then only 
            if (leave.IsRequestCreatedFromHR == null || leave.IsRequestCreatedFromHR == false)
            {
                leave.CreatedBy = leave.EmployeeId;
            }
            //
            if (isInsert)
            {

                PayrollDataContext.LeaveRequests.InsertOnSubmit(leave);
                isValueChangedForEmailInRequestUpdate = true;
                return SaveChangeSet();
            }
            else
            {
                LeaveRequest uLq = PayrollDataContext.LeaveRequests.Where(x => x.LeaveRequestId == leave.LeaveRequestId).FirstOrDefault();

                if (uLq.FromDateEng != leave.FromDateEng
                    || uLq.ToDateEng != leave.ToDateEng
                    || uLq.RecommendEmployeeId != leave.RecommendEmployeeId
                    || uLq.SupervisorEmployeeIdInCaseOfSelection != leave.SupervisorEmployeeIdInCaseOfSelection
                    )
                {
                    isValueChangedForEmailInRequestUpdate = true;
                }

                uLq.IsHour = leave.IsHour;
                uLq.IsHalfDayLeave = leave.IsHalfDayLeave;
                uLq.DaysOrHours = leave.DaysOrHours;
                uLq.HalfDayType = leave.HalfDayType;
                uLq.IsSpecialCase = leave.IsSpecialCase;
                uLq.SpecialComment = leave.SpecialComment;

                uLq.SubstituteEmployeeId = leave.SubstituteEmployeeId;
                uLq.Days1 = leave.Days1;
                uLq.Days2 = leave.Days2;
                uLq.CompensatorIsAddType = leave.CompensatorIsAddType;
                uLq.FromDate = leave.FromDate;
                uLq.FromDateEng = leave.FromDateEng;
                uLq.ToDate = leave.ToDate;
                uLq.ToDateEng = leave.ToDateEng;

                uLq.IsHour = leave.IsHour;
                uLq.StartTime = leave.StartTime;
                uLq.EndTime = leave.EndTime;

                uLq.LeaveTypeId = leave.LeaveTypeId;
                uLq.LeaveTypeId2 = leave.LeaveTypeId2;
                uLq.ChildLeaveTypeId = leave.ChildLeaveTypeId;

                uLq.Reason = leave.Reason;
                uLq.ModifiedOn = GetCurrentDateAndTime();
                uLq.EditSequence = leave.EditSequence;
                uLq.Status = leave.Status;
                uLq.SubstituteEmployeeId = leave.SubstituteEmployeeId;
                uLq.SupervisorEmployeeIdInCaseOfSelection = leave.SupervisorEmployeeIdInCaseOfSelection;
                uLq.RecommendEmployeeId = leave.RecommendEmployeeId;
                

                return UpdateChangeSet();
            }
        }

        /// <summary>
        /// Checks if the the leave day is on Holiday or not
        /// </summary>
        /// <param name="empId"></param>
        /// <param name="leaveDate"></param>
        /// <param name="countHoliday"></param>
        /// <param name="onlyCountPublicHolidayAsLeaveWeeklySaturdayIsHoliday"></param>
        /// <param name="onlyCountWeeklySaturdayAsLeavePublicHolidayIsHoliday"></param>
        /// <param name="holidayList"></param>
        /// <returns></returns>
        public static Boolean IsHolidayExistOnLeaveRequestDay(
            int empId, DateTime leaveDate, bool countHoliday,
            bool onlyCountPublicHolidayAsLeaveWeeklySaturdayIsHoliday,
            bool onlyCountWeeklySaturdayAsLeavePublicHolidayIsHoliday,List<GetHolidaysForAttendenceResult> holidayList)
        {
            EEmployee employee = new EmployeeManager().GetById(empId);

            int? holidayGroupId = null;

            if (employee != null && employee.HHumanResources.Count >= 1)
                holidayGroupId = employee.HHumanResources[0].HolidayGroupId;

            bool returnType = false;

            // For this case, only weekly saturday holiday will be counted as holiday, Public holiday will be counted as leave
            if (countHoliday == true && onlyCountPublicHolidayAsLeaveWeeklySaturdayIsHoliday)
            {
                List<GetHolidaysForAttendenceResult> filterHolidayList1 = holidayList
                    .Where(x => x.DateEng == leaveDate && (bool)x.IsWeekly && x.Type == 1).ToList();

                if (filterHolidayList1.Count > 0)
                    return true;
                else
                    return false;
            }

            if (countHoliday == true && onlyCountWeeklySaturdayAsLeavePublicHolidayIsHoliday)
            {
                List<GetHolidaysForAttendenceResult> filterHolidayList1 = holidayList
                    .Where(x => x.DateEng == leaveDate && (bool)x.IsWeekly && x.Type == 1).ToList();

                // if has weekly holiday then it will be counted as leave, so return false
                if (filterHolidayList1.Count > 0)
                    return false;
            }

            List<GetHolidaysForAttendenceResult> filterHolidayList = holidayList.Where(x => x.DateEng == leaveDate).ToList();

            

            foreach (GetHolidaysForAttendenceResult holiday in filterHolidayList)
            {
                

                if (holiday == null)
                    return returnType;




                int branchId = -1;

                if (holiday.BranchIDList != "" && employee != null)
                {
                    branchId = BranchManager.GetEmployeeCurrentBranch(employee.EmployeeId, leaveDate);

                    if (!holiday.BranchList.Contains(branchId))
                        return returnType;
                }
                

                //All Holiday
                if (holiday.Type.Equals(-1))
                    returnType = true;

                // Female Holiday
                if (holiday.Type.Equals(0) && ( holiday.IsWeekly == null || holiday.IsWeekly==false ) )
                    if (employee != null && employee.Gender.Equals(0))
                        returnType = true;

                // Holiday group
                if (Convert.ToBoolean(holiday.IsWeekly) == false && holiday.Type >= 1 && holiday.Type == holidayGroupId)
                        returnType = true;
              

                // should be Full day holiday also
                if ((bool)holiday.IsWeekly && holiday.Type==1)
                    returnType = true;

            }

            return returnType;   
        }

 

        public static bool IsLeaveRequestMadeOnDay(LeaveRequest leave)
        {
            //bool? requestmade = false;
            bool? requestmade = PayrollDataContext.GetLeaveRequestMadeOnDateNew(leave.LeaveRequestId,leave.FromDateEng,leave.ToDateEng,leave.EmployeeId);

            //    .GetLeaveRequestMadeOnDateNew(leave.LeaveRequestId, (DateTime)leave.FromDateEng, (DateTime)leave.ToDateEng, (int)leave.EmployeeId);
            if (requestmade == null)
                requestmade = false;

            return requestmade.Value;
        }

        public static bool DeleteEmployeeLeaveRequest(int leaveRequestId, int employeeId)
        {
            //LeaveRequest entity = PayrollDataContext.LeaveRequests
            //    .SingleOrDefault(l => l.LeaveRequestId == leaveRequestId && l.EmployeeId == employeeId
            //     &&  l.Status != (int)LeaveRequestStatusEnum.Approved);

            // As leave reuester can only delete the leave request
            LeaveRequest entity = PayrollDataContext.LeaveRequests.Where(x => x.LeaveRequestId == leaveRequestId
                && x.EmployeeId == SessionManager.CurrentLoggedInEmployeeId).SingleOrDefault();

            if (entity != null)
            {
                PayrollDataContext.LeaveRequests.DeleteOnSubmit(entity);


            }
            //send message when leave is deleted


            return DeleteChangeSet();
        }

        public static List<LeaveRequestBO> GetApprovedLeaveRequest(int employeeId, out int count)
        {
            //Data retrieval
            var leaves =
                (
                    from l in PayrollDataContext.LeaveRequests
                    join e in PayrollDataContext.EEmployees on l.ApprovedBy equals e.EmployeeId
                    join lt in PayrollDataContext.LLeaveTypes on l.LeaveTypeId equals lt.LeaveTypeId into temp
                    from lt in temp.DefaultIfEmpty()
                    join lt2 in PayrollDataContext.LLeaveTypes on l.LeaveTypeId2 equals lt2.LeaveTypeId into temp2
                    from lt2 in temp2.DefaultIfEmpty()

                    orderby l.ToDateEng descending
                    where l.EmployeeId == employeeId && (l.Status == (int)LeaveRequestStatusEnum.Approved 
                        || l.Status == (int)LeaveRequestStatusEnum.Denied
                        || l.Status == (int)LeaveRequestStatusEnum.Cancelled)
                    select new LeaveRequestBO
                    {
                        LeaveRequestId = l.LeaveRequestId,
                        LeaveTypeId = l.LeaveTypeId,
                        FromDate = l.FromDateEng.Value.ToShortDateString(),
                        ToDate = l.ToDateEng.Value.ToShortDateString(),
                        DaysOrHours =(l.IsHalfDayLeave.Value ? 0.5 : l.DaysOrHours.Value),
                        Reason = l.Reason,
                        Comment = l.Comment,
                        Status = ((LeaveRequestStatusEnum)l.Status).ToString(),

                        ApprovedBy = ( (l.Status==(int)LeaveRequestStatusEnum.Cancelled && l.EmployeeId == l.ApprovedBy) ? "Self Cancelled" : e.Name), //if cancelled by self then set blank
                        LeaveName = (lt == null ? "Unpaid Leave" : lt.Title)
                                + (l.LeaveTypeId2 == 0 ? ", Unpaid Leave" : (lt2 != null ? ", " + lt2.Title : ""))

                    }
                );//.Skip(start).Take(limit).ToList();

            count = leaves.Count();

            return leaves.ToList();

            //Sorting code
            //if (sort != null)
            //{
            //    var param = Expression.Parameter(typeof(Role), "e");
            //    MemberExpression expression = Expression.Property(param, sort.Property);


            //    var sortExpression = Expression.Lambda<Func<Role, object>>(expression, param);
            //    if (sort.Direction == SortDirection.DESC)
            //    {
            //        result = result.OrderByDescending(sortExpression);
            //    }
            //    else
            //    {
            //        result = result.OrderBy(sortExpression);
            //    }

            //}

            //Paging code
            //if (start >= 0 && limit > 0)
            //{
            //    leaves = leaves.Skip(start).Take(limit);
            //}

            //count = leaves.Count();

           // return leaves;
        }

        //public static int GetNIBLPendingLeave(int empId)
        //{ 
        //    return PayrollDataContext
        //        .LeaveRequests.Count(x=>x.
        //}



        public static List<LeaveRequestBO> GetUnApprovedLeaveRequest(int employeeId)
        {
            //List<LeaveRequestBO> leaves = PayrollDataContext.LeaveRequests
            //    .Where(x => x.EmployeeId == employeeId && x.Status == (int) status).ToList();
            DateTime startDate = LeaveRequestManager.GetStartDateForLeaveDetails();

            var leaves =
                (
                    from l in PayrollDataContext.LeaveRequests
                    join lt in PayrollDataContext.LLeaveTypes on l.LeaveTypeId equals lt.LeaveTypeId into temp
                    from lt in temp.DefaultIfEmpty()
                    
                    join lt2 in PayrollDataContext.LLeaveTypes on l.LeaveTypeId2 equals lt2.LeaveTypeId into temp2
                    from lt2 in temp2.DefaultIfEmpty()


                    where l.EmployeeId == employeeId 
                        && (l.Status == (int)LeaveRequestStatusEnum.Request 
                         || l.Status == (int)LeaveRequestStatusEnum.ReDraft
                            || l.Status == (int)LeaveRequestStatusEnum.Recommended)
                        //&& l.FromDateEng >= startDate
                    orderby l.ToDateEng descending
                    select new LeaveRequestBO
                    {
                       LeaveRequestId = l.LeaveRequestId,
                       LeaveTypeId = l.LeaveTypeId,
                       FromDate = l.FromDateEng.Value.ToShortDateString(),
                       ToDate = l.ToDateEng.Value.ToShortDateString(),
                       DaysOrHours =(l.IsHalfDayLeave.Value ? 0.5 : l.DaysOrHours.Value),
                       Reason = l.Reason,
                       Comment = l.Comment,
                       Status =  l.Status.ToString(),
                       LeaveName = (lt==null ? "Unpaid Leave" : lt.Title)
                         + (l.LeaveTypeId2 == 0 ? ", Unpaid Leave" : (lt2 != null ? ", " + lt2.Title : "")),
                       CreatedOn = l.CreatedOn.Value
                    }
                ).ToList();

            return leaves;
        }

        public static List<GetUnApprovedLeaveRequestResult> GetUnApprovedLeaveRequest(int employeeId, DateTime startDate, DateTime endDate)
        {

            return PayrollDataContext.GetUnApprovedLeaveRequest(employeeId, startDate, endDate, SessionManager.CurrentCompanyId).ToList();
            //List<LeaveRequest> leaves = PayrollDataContext.LeaveRequests
            //    .Where(x => x.EmployeeId == employeeId && x.Status != (int) LeaveRequestStatusEnum.Denied
            //     &&
            //    (
            //        x.FromDateEng >= startDate && x.FromDateEng <= endDate
            //        ||
            //        x.ToDateEng >= startDate && x.ToDateEng <= endDate
            //    )
            //    )
            //    .Where(x=>x.EEmployee.CompanyId == SessionManager.CurrentCompanyId).ToList();
            //return leaves;
        }

        public static Boolean IsEmployeeAttendanceSaved(DateTime leaveRequestDate, int CompanyId, int EmployeeId)
        {
            return PayrollDataContext.CheckWhetherAttendanceIsSavedOrNot(leaveRequestDate, CompanyId, EmployeeId).Value;
        }

        public static MannualLeaveAccure GetMannualLeave(int empId, int leaveTypeId, int month, int year)
        {
            return PayrollDataContext.MannualLeaveAccures.FirstOrDefault(
                x => x.EmployeeId == empId && x.LeaveTypeId == leaveTypeId
                    && x.Month == month && x.Year == year);
        }
        public static List<MannualLeaveAccure> GetMannualLeaveList(int empId, int leaveTypeId)
        {
            LEmployeeLeave empLeave = PayrollDataContext.LEmployeeLeaves.FirstOrDefault(x => x.EmployeeId == empId && x.LeaveTypeId == leaveTypeId);

            double opening = empLeave.BeginningBalance + (empLeave.CurrentYearBalance == null ? 0 : empLeave.CurrentYearBalance.Value);
            double openingTaken = empLeave.OpeningTaken == null ? 0 : empLeave.OpeningTaken.Value;
            
           



            List<MannualLeaveAccure> list = PayrollDataContext.MannualLeaveAccures
                .Where(x => x.EmployeeId == empId && x.LeaveTypeId == leaveTypeId).ToList();

           

            foreach (MannualLeaveAccure item in list)
            {
                item.Type = "Assigned";
                item.DateDisplay = GetAppropriateDate(item.Date.Value);
            }

            // add opening

            if (openingTaken != 0)
                list.Insert(0, new MannualLeaveAccure { Period = openingTaken, Type = "Taken", DateDisplay = "Opening" });

            if (opening != 0)
                list.Insert(0, new MannualLeaveAccure { Period = opening, Type = "Assigned", DateDisplay = "Opening" });

            List<LeaveRequest> leaveRequests = PayrollDataContext.LeaveRequests
                .Where(x => x.EmployeeId == empId && x.LeaveTypeId == leaveTypeId && x.Status==(int)LeaveRequestStatusEnum.Approved).ToList();

            foreach (LeaveRequest item in leaveRequests)
            {
                MannualLeaveAccure newItem = new MannualLeaveAccure();
                newItem.Type = "Taken";
                newItem.Date = item.FromDateEng.Value;

                newItem.DateDisplay = GetAppropriateDate(item.FromDateEng.Value) + " - " + GetAppropriateDate(item.ToDateEng.Value);
                newItem.Period = item.DaysOrHours.Value;

                list.Add(newItem);
            }

            list = list.OrderBy(x => x.Date).ToList();

            double balance = 0;
            foreach (MannualLeaveAccure item in list)
            {
                if (item.Type == "Assigned")
                    balance += item.Period;
                else
                    balance -= item.Period;

                item.Balance = balance;
            }

            return list;
        }

        public static double GetLeaveDeductDays(int leaveType, bool isHalfDay)
        {
            // for unpaid like leave
            if ((leaveType == 0 || leaveType == -1) && isHalfDay)
                return 0.5;
            if ((leaveType == 0 || leaveType == -1) && isHalfDay==false)
                return 1;

            LLeaveType leave = new LeaveAttendanceManager().GetLeaveType(leaveType);

            if (isHalfDay == false)
            {
                //these are decrementing value so if no decrement set 0 or else value
                if (leave.Type == LeaveType.FULLYPAID)
                {
                    return 0;
                }
                else if (leave.Type == LeaveType.HALFPAID)
                {
                    return 0.5;
                }
                else if (leave.Type == LeaveType.NON_LEAVE_ABSENCE)
                {
                    return 0;
                }
                else if (leave.Type == LeaveType.UNPAID)
                {
                    return 1;

                }
            }
            else
            {


                //these are decrementing value so if no decrement set 0 or else value
                if (leave.Type == LeaveType.FULLYPAID)
                {
                    return 0;
                }
                else if (leave.Type == LeaveType.HALFPAID)
                {
                    return 0.25;
                }
                else if (leave.Type == LeaveType.NON_LEAVE_ABSENCE)
                {
                    return 0;
                }
                else if (leave.Type == LeaveType.UNPAID)
                {
                    return 0.5;

                }


            }
            return 0;
        }

        public static List<GetAllLeaveRequestDetailResult> GetUnApprovedLeaveRequestForApproval(int employeeId, DateTime? startDate, DateTime? endDate, 
            int requestFrom,int filterStatus,string branch,string employee)
        {
            int searchEmployeeId = 0;
            int.TryParse(employee, out searchEmployeeId);

            return PayrollDataContext.GetAllLeaveRequestDetail(employeeId, SessionManager.CurrentCompanyId, 
                startDate, endDate, (int)requestFrom,filterStatus,branch,searchEmployeeId).ToList();
            

        }

        public static List<GetLeaveRequestListForHRResult> GetLeaveRequestListForHR(
            DateTime? startDate, DateTime? endDate,
            int filterStatus, string branch, string employee, int start, int limit,bool isModifiedDate,int? leaveType
            ,string sort)
        {
            int searchEmployeeId = 0;
            int.TryParse(employee, out searchEmployeeId);

            if (startDate == null)
                startDate = new DateTime(2000, 1, 1);
            if (endDate == null)
                endDate = DateTime.MaxValue;

            startDate = DateManager.GetStartDate(startDate.Value);
            endDate = DateManager.GetEndDate(endDate.Value);

            return PayrollDataContext.GetLeaveRequestListForHR(
                startDate, endDate, filterStatus, branch, searchEmployeeId, start, limit, isModifiedDate, leaveType, sort.ToLower()).ToList();


        }

        public static DateTime? LastValidLeaveRequest(int employeeId)
        {
            DateTime? endDate =
            (
                from a in PayrollDataContext.Attendences
                join p in PayrollDataContext.PayrollPeriods on a.PayrollPeriodId equals p.PayrollPeriodId
                where a.EmployeeId == employeeId
                orderby a.PayrollPeriodId descending
                select p.EndDateEng
                ).Take(1).SingleOrDefault();

            return endDate;
        }

        public static bool DeleteLeaveRequest(int pLeaveRequestId)
        {
            LeaveRequest leave = PayrollDataContext.LeaveRequests.Where(x => x.LeaveRequestId == pLeaveRequestId).FirstOrDefault();
            PayrollDataContext.LeaveRequests.DeleteOnSubmit(leave);
            return DeleteChangeSet();
        }

        public static Ext.Net.Paging<GetLeaveByDateResult> GetLeaveByDate(int Status, int companyId, bool isYearly, int startPayrollPeriodId, int start, int limit)
        {

            List<GetLeaveByDateResult> list = DAL.BaseBiz.PayrollDataContext.GetLeaveByDate(
                LeaveRequestManager.GetStartDateForLeaveDetails(), Status, companyId, SessionManager.CurrentLoggedInEmployeeId, isYearly, startPayrollPeriodId, start, limit).ToList();
            return new Ext.Net.Paging<GetLeaveByDateResult>(list, list.Count);
        }

        public static List<GetLeaveByDateApprovalResult> GetLeaveByDateApproval(
            string Status, int companyId, int startPayrollPeriodId, int rowIndex, int limit, int leaveRequestEmployeeId, 
            int ExcludeLeaveRequestId, string branch, string searchEmployee,int currentLoggedEmpId)
        {

            int empId = 0;
            int.TryParse(searchEmployee, out empId);
            List<GetLeaveByDateApprovalResult> list = DAL.BaseBiz.PayrollDataContext.GetLeaveByDateApproval(
                null, Status, companyId, currentLoggedEmpId,
                startPayrollPeriodId, rowIndex, limit, leaveRequestEmployeeId, ExcludeLeaveRequestId,branch,empId).ToList();
            return list;
        }
        public static int GetLeaveByDateApprovalCount(string Status)
        {

            int? count = 0;
            DAL.BaseBiz.PayrollDataContext.GetLeaveByDateApprovalCount(
                Status, SessionManager.CurrentLoggedInEmployeeId, ref count);

            if (count == null)
                count = 0;

            return count.Value;
        }
        //public static List<GetLeaveByDateApprovalResult> GetLeaveByDateApprovalForDevice(string Status, int companyId, int startPayrollPeriodId, int rowIndex, int limit, int employeeId, int ExcludeLeaveRequestId, string branch, string searchEmployee)
        //{

        //    int empId = 0;
        //    int.TryParse(searchEmployee, out empId);
        //    List<GetLeaveByDateApprovalResult> list = DAL.BaseBiz.PayrollDataContext.GetLeaveByDateApproval(
        //        LeaveRequestManager.GetStartDateForLeaveDetails(), Status, companyId, employeeId,
        //        startPayrollPeriodId, rowIndex, limit, -1, ExcludeLeaveRequestId, branch, empId).ToList();
        //    return list;
        //}
        public static GetLeaveApprovalApplyToCCForEmployeeResult GetLeaveApprovalApplyToAndCCForEmployee(int employeeId)
        {

            bool hasLeaveProject = PayrollDataContext.LeaveProjects.Any();

            List<GetLeaveApprovalApplyToCCForEmployeeResult> list =
             PayrollDataContext.GetLeaveApprovalApplyToCCForEmployee(employeeId,hasLeaveProject).ToList();

            if (list.Count > 0)
            {

                GetLeaveApprovalApplyToCCForEmployeeResult entity = list[0];
                //EmployeeManager mgr = new EmployeeManager();

                if (entity.ApplyTo != null)
                    entity.ApplyToName = EmployeeManager.GetEmployeeName(entity.ApplyTo.Value);

                if (entity.CC1 != null)
                    entity.CC1Name = EmployeeManager.GetEmployeeName(entity.CC1.Value);

                if (entity.CC2 != null)
                    entity.CC2Name = EmployeeManager.GetEmployeeName(entity.CC2.Value);

                if (entity.CC3 != null)
                    entity.CC3Name = EmployeeManager.GetEmployeeName(entity.CC3.Value);

                if (entity.CC4 != null)
                    entity.CC4Name = EmployeeManager.GetEmployeeName(entity.CC4.Value);

                if (entity.CC5 != null)
                    entity.CC5Name = EmployeeManager.GetEmployeeName(entity.CC5.Value);

                if (entity.ApplyToOther != null)
                    entity.ApplyToOtherName = EmployeeManager.GetEmployeeName(entity.ApplyToOther.Value);

                if (entity.CC1Other != null)
                    entity.CC1OtherName = EmployeeManager.GetEmployeeName(entity.CC1Other.Value);

                return entity;
                
            }
            return null;
        }

        /// <summary>
        /// Check if the leave is valid to change by the Approval for past leave
        /// </summary>
        /// <param name="leaveEndDate"></param>
        /// <returns></returns>
        public static bool IsValidLeaveToChangeByApproval(DateTime leaveEndDate,DateTime leaveStartDate,ref int diffStart)
        {
            DateTime date = GetCurrentDateAndTime();
            DateTime now = new DateTime(date.Year, date.Month, date.Day);
            int diff = (now - leaveEndDate).Days;
            diffStart = (now - leaveStartDate).Days;

            if (diff >= 0 && diffStart >= 0)
            {
                if (diff <= CommonManager.CompanySetting.LeaveApprovalMinimumPastDays
                    && diffStart <= CommonManager.CompanySetting.LeaveApprovalMinimumPastDays)
                    return true;
                else
                    return false;
            }
            //for future date from current is always allowed
            else
            {
                return true;
            }

        }

        public static DateTime GetLeaveYearStartDate(int payrollPeriodId)
        {
            PayrollPeriod payrollPeriod = CommonManager.GetPayrollPeriod(payrollPeriodId);
            DateTime startDate = payrollPeriod.StartDateEng.Value;
            CustomDate startCustomDate = CustomDate.GetCustomDateFromString(payrollPeriod.StartDate, IsEnglish);

            int leaveStartMonth = SessionManager.CurrentCompany.LeaveStartMonth.Value;
            DateTime yearStartDate;

            //if payrol period month < leave start month, then take the days diferent form pay roll start month to leave last end month
            if (payrollPeriod.Month == leaveStartMonth)
            {
                yearStartDate = payrollPeriod.StartDateEng.Value;
            }
            else if (payrollPeriod.Month < leaveStartMonth)
            {
                CustomDate date1 = new CustomDate(1, leaveStartMonth, startCustomDate.Year - 1, IsEnglish);
                yearStartDate = date1.EnglishDate; //new DateTime(startDate.Year, leaveStartMonth, 1);
            }
            else // >
            //if payroll > leave, then take diff using next year leave start month
            {
                CustomDate date1 = new CustomDate(1, leaveStartMonth, startCustomDate.Year, IsEnglish);
                yearStartDate = date1.EnglishDate;//new DateTime(startDate.Year + 1, leaveStartMonth, 1);
            }
            return yearStartDate;
        }


        /// <summary>
        /// Method to change the leave request status by Leave Approval Employee
        /// </summary>
        /// <param name="leaveRequest"></param>
        /// <param name="IsNewRequest"></param>
        /// <returns></returns>
        public static ResponseStatus ForwardLeaveRequest(LeaveRequest leaveRequest, int nextReviewerEmployeeId,string comment)
        {
            int currentEmpId = 0; string currentEmpUserName = "";
            UserManager.SetCurrentEmployeeAndUser(ref currentEmpId, ref currentEmpUserName);

            ResponseStatus status = new ResponseStatus();
            EEmployee emp = EmployeeManager.GetEmployeeById(leaveRequest.EmployeeId);
            bool onlyCountPublicHolidayAsLeaveWeeklySaturdayIsHoliday = false;
            bool onlyCountWeeklySaturdayAsLeavePublicHolidayIsHoliday = false;
            bool CountHolidayInBetweenLeave = LeaveAttendanceManager.GetLeaveRequestCountHolidayAsLeaveSetting(leaveRequest.LeaveTypeId, ref onlyCountPublicHolidayAsLeaveWeeklySaturdayIsHoliday,ref onlyCountWeeklySaturdayAsLeavePublicHolidayIsHoliday);
            HolidayManager hmanager = new HolidayManager();
            List<GetHolidaysForAttendenceResult> holiday = hmanager.GetMonthsHolidaysForAttendence(0, 0, SessionManager.CurrentCompanyId, 0, leaveRequest.FromDateEng, leaveRequest.ToDateEng, leaveRequest.EmployeeId).ToList();



            if (SessionManager.CurrentLoggedInEmployeeId == 0
                && (SessionManager.User.UserMappedEmployeeId == null || SessionManager.User.UserMappedEmployeeId == 0 || SessionManager.User.UserMappedEmployeeId == -1))
            {
                status.ErrorMessage = "Please map the current user with Employee from User management before changing the leave.";
                status.IsSuccess = "false";
                return status;
            }


            int leaveRequestId = leaveRequest.LeaveRequestId;
            LeaveRequest upLeaveRequest = PayrollDataContext.LeaveRequests.SingleOrDefault(x => x.LeaveRequestId == leaveRequestId);


            LeaveRequestStatusEnum dbLeaveStatus = (LeaveRequestStatusEnum)upLeaveRequest.Status;
            LeaveRequestStatusEnum newLeaveStatus = (LeaveRequestStatusEnum)leaveRequest.Status;

            // Check the Current status of Leave Request if it can be Approved,Denied or Re-Draft
            if (dbLeaveStatus != LeaveRequestStatusEnum.Request && dbLeaveStatus != LeaveRequestStatusEnum.Recommended)
            {
                status.ErrorMessage =
                   string.Format("Leave is in \"{0}\" status, only \"Request\" or \"Recommended\" status leave can be changed.", dbLeaveStatus.ToString());
                status.IsSuccess = "false";
                return status;
            }

            int diff = 0;
            // Attendance should not be saved
            //if (LeaveRequestManager.IsAttendanceSavedForLeaveRequest(upLeaveRequest) && newLeaveStatus == LeaveRequestStatusEnum.Approved)
            if (IsValidLeaveToChangeByApproval(leaveRequest.ToDateEng.Value, leaveRequest.FromDateEng.Value, ref diff) == false)
            {
                status.ErrorMessage =
                  string.Format("Past leave older than {0} days can not be changed.", CommonManager.CompanySetting.LeaveApprovalMinimumPastDays);
                status.IsSuccess = "false";
                return status;
            }



            upLeaveRequest.LeaveDetails.Clear();
            upLeaveRequest.LeaveDetails.AddRange(GetLeaveDetail(leaveRequest, emp, CountHolidayInBetweenLeave,onlyCountPublicHolidayAsLeaveWeeklySaturdayIsHoliday,onlyCountWeeklySaturdayAsLeavePublicHolidayIsHoliday, holiday));


            CopyObject<LeaveRequest>(leaveRequest, ref upLeaveRequest, "EmployeeId", "CreatedOn", "CreatedBy", "IsRequestCreatedFromHR");


            //Don't change status here as it will be in Request 
            //upLeaveRequest.Status = leaveRequest.Status;
            upLeaveRequest.RecommendEmployeeId = nextReviewerEmployeeId;


            LeaveReviewRecommender newReview = new LeaveReviewRecommender();
            newReview.ID = Guid.NewGuid();
            newReview.RecommendedOn = GetCurrentDateAndTime();
            newReview.RecommenderEmployeeId = SessionManager.CurrentLoggedInEmployeeId;
            newReview.Comment = comment;
            upLeaveRequest.LeaveReviewRecommenders.Add(newReview);



            UpdateChangeSet();


            return status;
        }

        /// <summary>
        /// Method to change the leave request status by Leave Approval Employee
        /// </summary>
        /// <param name="leaveRequest"></param>
        /// <param name="IsNewRequest"></param>
        /// <returns></returns>
        public static Status ForwardAllowance(int allowanceId, int nextReviewerEmployeeId)
        {
            //int currentEmpId = 0; string currentEmpUserName = "";
            //UserManager.SetCurrentEmployeeAndUser(ref currentEmpId, ref currentEmpUserName);
            Status status = new Status();
            EveningCounterRequest allow = AllowanceManager.GetEveningCounterByID(allowanceId);

            //Don't change status here as it will be in Request 
            //upLeaveRequest.Status = leaveRequest.Status;
            allow.RecommendedBy = nextReviewerEmployeeId;


            //LeaveReviewRecommender newReview = new LeaveReviewRecommender();
            //newReview.ID = Guid.NewGuid();
            //newReview.RecommendedOn = GetCurrentDateAndTime();
            //newReview.RecommenderEmployeeId = SessionManager.CurrentLoggedInEmployeeId;
            //newReview.Comment = comment;
            //upLeaveRequest.LeaveReviewRecommenders.Add(newReview);

            allow.RecommenderForwardedBy = SessionManager.CurrentLoggedInEmployeeId;
            allow.RecommenderForwardedOn = CommonManager.GetCurrentDateAndTime();

            UpdateChangeSet();

            AllowanceManager.SendEmail(allow, true);

            return status;
        }

        public static bool IsPartiallyCancelLeaveExists(int empId, DateTime from, DateTime to)
        {
            if(PayrollDataContext.LeaveRequests
                .Any(x=>x.EmployeeId==empId && x.IsCancelRequest != null && x.IsCancelRequest.Value 
                    && x.Status != (int)LeaveRequestStatusEnum.Denied
                    && 
                    ((from >= x.FromDateEng && from <= x.FromDateEng) || (to >= x.FromDateEng && to <= x.ToDateEng))))
            {
                return true;
            }

            return false;
        }


        public static Status ProcessLeaveLikeForDeny(int leaveRequestId,LeaveRequestStatusEnum leaveStatus,string comments)
        {
          


            Status status = new Status();
            LeaveRequest dbRequest = GetLeaveRequestById(leaveRequestId);

            if (SessionManager.CurrentLoggedInEmployeeId == 0
             && (SessionManager.User.UserMappedEmployeeId == null || SessionManager.User.UserMappedEmployeeId == 0 || SessionManager.User.UserMappedEmployeeId == -1))
            {
                status.ErrorMessage = "Please map the current user with Employee from User management before changing the leave.";
                
                return status;
            }


            if (dbRequest != null)
            {
                int currentEmployeeId = 0; string currentEmployeeUserName = "";
                UserManager.SetCurrentEmployeeAndUser(ref currentEmployeeId, ref currentEmployeeUserName);

                if (dbRequest.Status == (int)LeaveRequestStatusEnum.Request || dbRequest.Status == (int)LeaveRequestStatusEnum.Recommended)
                {

                    dbRequest.Status = (int)leaveStatus;
                    dbRequest.Comment = comments;
                    dbRequest.ApprovedOn = DateTime.Now;
                    dbRequest.ApprovedBy = currentEmployeeId;
                    PayrollDataContext.SubmitChanges();
                }
                else
                {
                    status.ErrorMessage = "Only request leave can be denied.";

                }
            }

            return status;
        }

        /// <summary>
        /// Method to change the leave request status by Leave Approval Employee
        /// </summary>
        /// <param name="leaveRequest"></param>
        /// <param name="IsNewRequest"></param>
        /// <returns></returns>
        public static ResponseStatus HandleLeaveRequest(LeaveRequest leaveRequest, Boolean IsNewRequest)
        {
            int currentEmpId = 0; string currentEmpUserName = "";
            UserManager.SetCurrentEmployeeAndUser(ref currentEmpId, ref currentEmpUserName);

            ResponseStatus status = new ResponseStatus();
            EEmployee emp = EmployeeManager.GetEmployeeById(leaveRequest.EmployeeId);
            bool onlyCountPublicHolidayAsLeaveWeeklySaturdayIsHoliday = false;
            bool onlyCountWeeklySaturdayAsLeavePublicHolidayIsHoliday = false;
            bool CountHolidayInBetweenLeave = LeaveAttendanceManager.GetLeaveRequestCountHolidayAsLeaveSetting(leaveRequest.LeaveTypeId, ref onlyCountPublicHolidayAsLeaveWeeklySaturdayIsHoliday,ref onlyCountWeeklySaturdayAsLeavePublicHolidayIsHoliday);
            HolidayManager hmanager = new HolidayManager();
            List<GetHolidaysForAttendenceResult> holiday = hmanager.GetMonthsHolidaysForAttendence(0, 0, SessionManager.CurrentCompanyId, 0, leaveRequest.FromDateEng, leaveRequest.ToDateEng, leaveRequest.EmployeeId).ToList();

            List<LeaveDetail> baseLeaveRequestDetailList = new List<LeaveDetail>();


            if (SessionManager.CurrentLoggedInEmployeeId == 0
                && (SessionManager.User.UserMappedEmployeeId == null || SessionManager.User.UserMappedEmployeeId == 0 || SessionManager.User.UserMappedEmployeeId == -1))
            {
                status.ErrorMessage = "Please map the current user with Employee from User management before changing the leave.";
                status.IsSuccess = "false";
                return status;
            }


            SetConnectionPwd(PayrollDataContext.Connection);
            PayrollDataContext.Transaction = PayrollDataContext.Connection.BeginTransaction();
            try
            {


                // is for setting Approval or Denial or ReDraft
                if (!IsNewRequest)
                {
                    int leaveRequestId = leaveRequest.LeaveRequestId;
                    LeaveRequest upLeaveRequest = PayrollDataContext.LeaveRequests.SingleOrDefault(x => x.LeaveRequestId == leaveRequestId);

                    leaveRequest.ParentLeaveRequestRef_ID = upLeaveRequest.ParentLeaveRequestRef_ID;

                    LeaveRequestStatusEnum dbLeaveStatus = (LeaveRequestStatusEnum)upLeaveRequest.Status;
                    LeaveRequestStatusEnum newLeaveStatus = (LeaveRequestStatusEnum)leaveRequest.Status;

                    // Check the Current status of Leave Request if it can be Approved,Denied or Re-Draft
                    if (dbLeaveStatus != LeaveRequestStatusEnum.Request && dbLeaveStatus != LeaveRequestStatusEnum.Recommended)
                    {
                        status.ErrorMessage =
                           string.Format("Leave is in \"{0}\" status, only \"Request\" or \"Recommended\" status leave can be changed.", dbLeaveStatus.ToString());
                        status.IsSuccess = "false";
                        return status;
                    }

                    int diff = 0;
                    // Attendance should not be saved
                    //if (LeaveRequestManager.IsAttendanceSavedForLeaveRequest(upLeaveRequest) && newLeaveStatus == LeaveRequestStatusEnum.Approved)
                    if (IsValidLeaveToChangeByApproval(leaveRequest.ToDateEng.Value, leaveRequest.FromDateEng.Value, ref diff) == false)
                    {
                        status.ErrorMessage =
                          string.Format("Past leave older than {0} days can not be changed.", CommonManager.CompanySetting.LeaveApprovalMinimumPastDays);
                        status.IsSuccess = "false";
                        return status;
                    }

                    // If Approved then only change info
                    if (leaveRequest.Status == (int)LeaveRequestStatusEnum.Approved ||
                        leaveRequest.Status == (int)LeaveRequestStatusEnum.Recommended)
                    {

                        // do not update LeaveDetails from now, as it can not be changed by approval

                        if (upLeaveRequest.IsCancelRequest != null && upLeaveRequest.IsCancelRequest.Value)
                        {
                            upLeaveRequest.LeaveDetails.Clear();
                            // if approved remove LeaveDetail data for Original Leave Request
                            if (leaveRequest.Status == (int)LeaveRequestStatusEnum.Approved && upLeaveRequest.LeaveRequestRef_Id != null)
                            {
                                baseLeaveRequestDetailList =
                                    PayrollDataContext.LeaveDetails.Where(x => x.LeaveRequestId == upLeaveRequest.LeaveRequestRef_Id
                                        && x.DateOnEng >= leaveRequest.FromDateEng && x.DateOnEng <= leaveRequest.ToDateEng).ToList();

                                // change Approve DaysOrHours
                                LeaveRequest approvedLeave = PayrollDataContext.LeaveRequests.FirstOrDefault(x => x.LeaveRequestId
                                    == upLeaveRequest.LeaveRequestRef_Id);
                                if (approvedLeave != null)
                                    approvedLeave.DaysOrHours = approvedLeave.DaysOrHours - baseLeaveRequestDetailList.Count;


                                PayrollDataContext.LeaveDetails.DeleteAllOnSubmit(baseLeaveRequestDetailList);


                                leaveRequest.IsCancelRequest = upLeaveRequest.IsCancelRequest;
                                leaveRequest.LeaveRequestRef_Id = upLeaveRequest.LeaveRequestRef_Id;


                            }
                        }
                        //else
                        //{
                        //    upLeaveRequest.LeaveDetails.AddRange(GetLeaveDetail(leaveRequest, emp, CountHolidayInBetweenLeave, holiday));
                        //}

                        leaveRequest.SupervisorEmployeeIdInCaseOfSelection = upLeaveRequest.SupervisorEmployeeIdInCaseOfSelection;
                        leaveRequest.RecommendEmployeeId = upLeaveRequest.RecommendEmployeeId;

                        // this might be needed for cancel leave approval or for recommend flow,check
                        //CopyObject<LeaveRequest>(leaveRequest, ref upLeaveRequest, "EmployeeId", "CreatedOn", "CreatedBy", "IsRequestCreatedFromHR");

                    }


                    upLeaveRequest.Status = leaveRequest.Status;
                    if (leaveRequest.Status != (int)LeaveRequestStatusEnum.Recommended)
                    {
                        upLeaveRequest.Comment = leaveRequest.Comment;
                        upLeaveRequest.ApprovedOn = leaveRequest.ApprovedOn;
                        upLeaveRequest.ApprovedBy = leaveRequest.ApprovedBy;
                    }

                    if (leaveRequest.Status == (int)LeaveRequestStatusEnum.Approved)
                    {
                        upLeaveRequest.Comment = leaveRequest.Comment;
                        upLeaveRequest.ApprovedOn = leaveRequest.ApprovedOn;
                        upLeaveRequest.ApprovedBy = leaveRequest.ApprovedBy;
                    }
                    else if (leaveRequest.Status == (int)LeaveRequestStatusEnum.Recommended)
                    {
                        upLeaveRequest.RecommendedComment = leaveRequest.RecommendedComment;
                        upLeaveRequest.RecommendedOn = leaveRequest.RecommendedOn;
                        upLeaveRequest.RecommendedBy = leaveRequest.RecommendedBy;


                        LeaveReviewRecommender newReview = new LeaveReviewRecommender();
                        newReview.ID = Guid.NewGuid();
                        newReview.RecommendedOn = GetCurrentDateAndTime();
                        newReview.RecommenderEmployeeId = SessionManager.CurrentLoggedInEmployeeId;
                        newReview.Comment = leaveRequest.RecommendedComment;
                        upLeaveRequest.LeaveReviewRecommenders.Add(newReview);

                    }

                    UpdateChangeSet();



                }
                else
                {

                    // for leave cancellation due to partial leave cancellation assignment, do not check
                    if (leaveRequest.IsCancelRequest != null && leaveRequest.IsCancelRequest.Value &&
                        leaveRequest.Status == (int)LeaveRequestStatusEnum.Approved)
                    {
                    }
                    else
                    {
                        // validate if leave already exists for this date or not
                        if (LeaveAttendanceManager.IsLeaveRequestMadeOnDay(leaveRequest))
                        {
                            status.ErrorMessage = "Leave request already exists on this date.";
                            status.IsSuccess = "false";
                            return status;
                        }
                    }


                    LLeaveType leaveType = GetLeaveTypeById(leaveRequest.LeaveTypeId);
                    // for parent type leave details is added from gui function
                    if (leaveType != null && leaveType.IsParentGroupLeave != null && leaveType.IsParentGroupLeave.Value)
                    {

                    }
                    else
                    {
                        leaveRequest.LeaveDetails.AddRange(GetLeaveDetail(leaveRequest, emp, CountHolidayInBetweenLeave, onlyCountPublicHolidayAsLeaveWeeklySaturdayIsHoliday,onlyCountWeeklySaturdayAsLeavePublicHolidayIsHoliday, holiday));
                    }
                    int diff = 0;
                    // Attendance should not be saved
                    //if (LeaveRequestManager.IsAttendanceSavedForLeaveRequest(upLeaveRequest) && newLeaveStatus == LeaveRequestStatusEnum.Approved)
                    if (IsValidLeaveToChangeByApproval(leaveRequest.ToDateEng.Value, leaveRequest.FromDateEng.Value, ref diff) == false)
                    {
                        status.ErrorMessage =
                          string.Format("Leave can not be assigned in past days older than {0}.", CommonManager.CompanySetting.LeaveApprovalMinimumPastDays);
                        status.IsSuccess = "false";
                        return status;
                    }


                    if (leaveRequest.IsCancelRequest != null && leaveRequest.IsCancelRequest.Value)
                    {
                        leaveRequest.LeaveDetails.Clear();
                        // if approved remove LeaveDetail data for Original Leave Request
                        if (leaveRequest.Status == (int)LeaveRequestStatusEnum.Approved && leaveRequest.LeaveRequestRef_Id != null)
                        {
                            baseLeaveRequestDetailList =
                                PayrollDataContext.LeaveDetails.Where(x => x.LeaveRequestId == leaveRequest.LeaveRequestRef_Id
                                    && x.DateOnEng >= leaveRequest.FromDateEng && x.DateOnEng <= leaveRequest.ToDateEng).ToList();

                            // change Approve DaysOrHours
                            LeaveRequest approvedLeave = PayrollDataContext.LeaveRequests.FirstOrDefault(x => x.LeaveRequestId
                                == leaveRequest.LeaveRequestRef_Id);
                            if (approvedLeave != null)
                                approvedLeave.DaysOrHours = approvedLeave.DaysOrHours - baseLeaveRequestDetailList.Count;

                            // validate fro Min and Max days
                            string minMaxBalance = LeaveRequestManager.ValidateLeaveMinMaxBalance(approvedLeave.LeaveTypeId, approvedLeave.DaysOrHours.Value, approvedLeave);
                            if (!string.IsNullOrEmpty(minMaxBalance))
                            {
                                status.ErrorMessage = minMaxBalance;
                                status.IsSuccess = "false";
                                return status;
                            }

                            PayrollDataContext.LeaveDetails.DeleteAllOnSubmit(baseLeaveRequestDetailList);




                        }
                    }


                    PayrollDataContext.LeaveRequests.InsertOnSubmit(leaveRequest);
                    SaveChangeSet();
                }

                //AttendanceManager.GenerateAsyncAttendanceSingleEmployeeTable(leaveRequest.EmployeeId, leaveRequest.FromDateEng.Value.Date,
                //            leaveRequest.ToDateEng.Value.Date);

                //AttendanceManager.UpdateAttendanceEmployeeDate(leaveRequest.FromDateEng.Value);

                // "Process Resaving attendance if status is Approved"

                if (leaveRequest.LeaveRequestRef_Id != null)
                {
                    LeaveRequest dbOriginalLeaveRequest = GetLeaveRequestById(leaveRequest.LeaveRequestRef_Id.Value);
                    ModifySavedAttendanceAfterLeaveChange(dbOriginalLeaveRequest, emp, CountHolidayInBetweenLeave, onlyCountPublicHolidayAsLeaveWeeklySaturdayIsHoliday,onlyCountWeeklySaturdayAsLeavePublicHolidayIsHoliday, holiday, baseLeaveRequestDetailList);
                }
                else
                    // then we need to modify the Attendance also
                    ModifySavedAttendanceAfterLeaveChange(leaveRequest, emp, CountHolidayInBetweenLeave, onlyCountPublicHolidayAsLeaveWeeklySaturdayIsHoliday,onlyCountWeeklySaturdayAsLeavePublicHolidayIsHoliday, holiday, baseLeaveRequestDetailList);


                PayrollDataContext.Transaction.Commit();
            }
            catch (Exception exp)
            {
                
                PayrollDataContext.Transaction.Rollback();
                Log.log("Leave change approval error : " + leaveRequest.LeaveRequestId, exp);
                status.ErrorMessage = "Leave could not be changed.";
                status.IsSuccess = "false";
                return status;
            }
            finally
            {
                //if (PayrollDataContext.Connection != null)
                //{
                //    PayrollDataContext.Connection.Close();
                //}
            }

            return status;
        }

        private static void ModifySavedAttendanceAfterLeaveChange(LeaveRequest leaveRequest, EEmployee emp, bool CountHolidayInBetweenLeave,
            bool onlyCountPublicHolidayAsLeaveWeeklySaturdayIsHoliday,bool onlyCountWeeklySaturdayAsLeavePublicHolidayIsHoliday,
            List<GetHolidaysForAttendenceResult> holiday, List<LeaveDetail> baseLeaveRequestDetailList)
        {
            if (LeaveRequestManager.IsAttendanceSavedForLeaveRequest(leaveRequest) && leaveRequest.Status == (int)LeaveRequestStatusEnum.Approved)
            {

                #region "Change Saved Attendance From leave request"

                List<PayrollPeriod> list = LeaveRequestManager.GetSavePayrollListForLeaveRequest(leaveRequest.EmployeeId,
                    leaveRequest.FromDateEng.Value);
                decimal? value = null;
                double? dvalue = null;
                // EEmployee emp = EmployeeManager.GetEmployeeById(upLeaveRequest.EmployeeId);

                List<AttendanceImportEmployee> atteList = new List<AttendanceImportEmployee>();
                atteList.Add(new AttendanceImportEmployee { EmployeeId = leaveRequest.EmployeeId });

                // for Cancel leave approved case, only remove original LeaveDetail from saved Attendance
                if (baseLeaveRequestDetailList.Count > 0)
                {
                    foreach (LeaveDetail item in baseLeaveRequestDetailList)
                    {
                        AttendanceImportEmployeeLeave leave = new AttendanceImportEmployeeLeave();
                        leave.EmployeeId = leaveRequest.EmployeeId;
                        leave.NoLeave = true;
                        leave.TakenDate = item.DateOnEng;
                        //if (leaveRequest.LeaveTypeId == 0)
                        //{
                        //    leave.LeaveAbbr = "UPL";
                        //    leave.LeaveAbbr += "/2";
                        //    leave.IsUnpaidLeave = true;
                        //}
                        //else
                        //{
                        //    leave.LeaveTypeId = leaveRequest.LeaveTypeId;
                        //    leave.LeaveAbbr = new LeaveAttendanceManager().GetLeaveType(leave.LeaveTypeId).Abbreviation;
                        //    leave.LeaveAbbr += "/2";
                        //}
                        atteList[0].leaves.Add(leave);
                    }
                }
                else
                {
                    if (leaveRequest.IsHalfDayLeave != null && leaveRequest.IsHalfDayLeave.Value)
                    {
                        AttendanceImportEmployeeLeave leave = new AttendanceImportEmployeeLeave();
                        leave.EmployeeId = leaveRequest.EmployeeId;
                        leave.IsHalfDay = true;
                        leave.TakenDate = leaveRequest.FromDateEng.Value;
                        if (leaveRequest.LeaveTypeId == 0)
                        {
                            leave.LeaveAbbr = "UPL";
                            leave.LeaveAbbr += "/2";
                            leave.IsUnpaidLeave = true;
                        }
                        else
                        {
                            leave.LeaveTypeId = leaveRequest.LeaveTypeId;
                            leave.LeaveAbbr = new LeaveAttendanceManager().GetLeaveType(leave.LeaveTypeId).Abbreviation;
                            leave.LeaveAbbr += "/2";
                        }
                        atteList[0].leaves.Add(leave);
                    }
                    else if (leaveRequest.IsHour != null && leaveRequest.IsHour.Value)
                    {
                        AttendanceImportEmployeeLeave leave = new AttendanceImportEmployeeLeave();
                        leave.EmployeeId = leaveRequest.EmployeeId;
                        leave.TakenDate = leaveRequest.FromDateEng.Value;
                        if (leaveRequest.LeaveTypeId == 0)
                        {
                            leave.LeaveAbbr = "UPL";
                            leave.LeaveAbbr += "-" + leaveRequest.DaysOrHours;
                            leave.IsUnpaidLeave = true;
                        }
                        else
                        {
                            leave.LeaveTypeId = leaveRequest.LeaveTypeId;
                            leave.LeaveAbbr = new LeaveAttendanceManager().GetLeaveType(leave.LeaveTypeId).Abbreviation;
                            leave.LeaveAbbr += "-" + leaveRequest.DaysOrHours;
                        }

                        LLeaveType leaveType = new LeaveAttendanceManager().GetLeaveType(leave.LeaveTypeId);

                        // for compensatory leave approval
                        if (leaveType != null && leaveType.FreqOfAccrual == LeaveAccrue.COMPENSATORY)
                        {
                            if (leaveRequest.CompensatorIsAddType != null && leaveRequest.CompensatorIsAddType.Value)
                            {
                                leave.LeaveAbbr += "++";
                            }
                            else
                            {
                                leave.LeaveAbbr += "--";
                            }

                            //atteList[0].leaves.Add(leave);
                        }


                        atteList[0].leaves.Add(leave);
                    }
                    else
                    {

                        LeaveRequest parentLeaveRequest = null;
                        List<LeaveDetail> parentLeaveList = null;
                        // if there is already leave request exists case and substitue is being applied for Prabhu Annual Refreshment call back using Subtitute leave
                        if (leaveRequest.ParentLeaveRequestRef_ID != null)
                        {
                            parentLeaveRequest = GetLeaveRequestById(leaveRequest.ParentLeaveRequestRef_ID.Value);
                            if (parentLeaveRequest != null)
                            {
                                parentLeaveList = parentLeaveRequest.LeaveDetails.OrderBy(x => x.DateOnEng).ToList();
                            }
                        }

                        int day = 1;
                        AttendanceImportEmployeeLeave leave = new AttendanceImportEmployeeLeave();
                        for (int i = 0; i <= (leaveRequest.ToDateEng.Value - leaveRequest.FromDateEng.Value).Days; i++)
                        {
                            DateTime date = leaveRequest.FromDateEng.Value.AddDays(i);
                            leave = new AttendanceImportEmployeeLeave();
                            leave.TakenDate = date;
                            leave.EmployeeId = leaveRequest.EmployeeId;
                            leave.LeaveTypeId = GetLeaveTypeIdForLeaveRequest(leaveRequest, date);// leaveRequest.LeaveTypeId;
                            LLeaveType leaveType = GetLeaveTypeById(leave.LeaveTypeId);
                            leave.Day = i == 0 ? 1 : 3;
                            if (leave.LeaveTypeId == 0)
                            {
                                leave.IsUnpaidLeave = true;
                                leave.LeaveAbbr = "UPL";
                            }
                            else
                                leave.LeaveAbbr = leaveType.Abbreviation;

                            // for compensatory leave approval
                            if (leaveType != null && leaveType.FreqOfAccrual == LeaveAccrue.COMPENSATORY)
                            {
                                if (leaveRequest.CompensatorIsAddType != null && leaveRequest.CompensatorIsAddType.Value)
                                {
                                    leave.LeaveAbbr += "++";
                                }
                                else
                                {
                                    leave.LeaveAbbr += "--";
                                }

                                atteList[0].leaves.Add(leave);

                                // if first approved leave already exists like Prabhu Annual Refreshment and Substitue leave
                                // Mark as multiple to be processed later in LeaveAdjustment table
                                if (parentLeaveRequest != null && parentLeaveList != null)
                                {
                                    LeaveDetail parentLeave = parentLeaveList.FirstOrDefault(x => x.DateOnEng == leave.TakenDate);
                                    if (parentLeave != null)
                                    {
                                        leave.HasMulitpleLeave = true;
                                        leave.OtherLeaveTypeId = parentLeave.LeaveTypeId != null ? parentLeave.LeaveTypeId.Value : parentLeaveRequest.LeaveTypeId;
                                       
                                    }
                                }
                            }
                            else if (IsDishHomeSundayLeave(leaveRequest.LeaveTypeId))
                            {
                                // sunday leave is taken for 3 days, first day half day mark but not leave counting, second will be holiday so no leave & 3rd day full day leave
                                if (day == 1 || day == 3)
                                {
                                    if (day == 1 && leaveRequest.LeaveTypeId2 != null)
                                    {
                                        //leave.HasOtherLeaveOnFridayOfSundayLeave = true;
                                        leave.LeaveTypeId = leaveRequest.LeaveTypeId2.Value;
                                        // Change leave Abbr if secondary leave selected with Sunday leave
                                        if (leaveRequest.LeaveTypeId2 == 0)
                                        {
                                            leave.LeaveAbbr = "UPL";
                                        }
                                        else
                                        {
                                            leave.LeaveAbbr = new LeaveAttendanceManager().GetLeaveType(leaveRequest.LeaveTypeId2.Value).Abbreviation;

                                        }
                                        leave.IsHalfDay = true;
                                        leave.LeaveAbbr += "/2";
                                    }
                                    else if (day == 3)
                                    {
                                        leave.LeaveTypeId = leaveRequest.LeaveTypeId;
                                        leave.LeaveAbbr = new LeaveAttendanceManager().GetLeaveType(leaveRequest.LeaveTypeId).Abbreviation;
                                    }
                                    atteList[0].leaves.Add(leave);
                                }
                                else if (day == 2)
                                {
                                    day += 1;
                                    continue;
                                }


                                day += 1;
                            }
                           
                            else if (IsLeaveDay(leaveRequest.FromDateEng, leaveRequest.ToDateEng, date,
                                IsHolidayExistOnLeaveRequestDay(emp.EmployeeId, date, CountHolidayInBetweenLeave,onlyCountPublicHolidayAsLeaveWeeklySaturdayIsHoliday, onlyCountWeeklySaturdayAsLeavePublicHolidayIsHoliday, holiday), CountHolidayInBetweenLeave))
                            {


                                if (CommonManager.CalculationConstant.CompanyHasHourlyLeave.Value)
                                {
                                    leave.LeaveAbbr += "-" + CommonManager.CalculationConstant.CompanyLeaveHoursInADay;
                                }

                                atteList[0].leaves.Add(leave);
                            }
                        }

                    }
                }

                string msg = "";
                int count = 0;
                foreach (PayrollPeriod payroll in list)
                {
                      // This function may generate LINQ : Row not found error if multiple leave is being approved and multiple leaves
                    // of same employee lies in same month then due to stored procedure of belog code modifing linq data 
                    // generates the error
                    bool status = EmployeeManager.SaveImportedAttendance(atteList, payroll.PayrollPeriodId, ref msg, ref count, null);

                    if (status == false)
                        Log.log("Attendance resaved error for leave request id : " + leaveRequest.LeaveRequestId, msg);
                    
					
                    LeaveAttendanceManager.CallToResaveAtte(payroll.PayrollPeriodId, leaveRequest.EmployeeId.ToString(), null);
                }

                #endregion
            }
        }

        /// <summary>
        /// Returns the LeaveTypeId for that day
        /// Whey this function gettting from db, always creating problem as later LeaveTypeId for that
        /// day could be changed, but due to data retrieval from db, new leavetypeid will not be considered
        /// </summary>
        /// <param name="leaveRequest"></param>
        /// <param name="date"></param>
        /// <returns></returns>
        public static int GetLeaveTypeIdForLeaveRequest(LeaveRequest leaveRequest, DateTime date)
        {
            //if (leaveRequest.ChildLeaveTypeId != null)
               // return leaveRequest.ChildLeaveTypeId.Value;

            List<LeaveDetail> dbDetailList = PayrollDataContext.LeaveDetails
                .Where(x => x.LeaveRequestId == leaveRequest.LeaveRequestId).ToList();


            LeaveDetail dateDetail = dbDetailList.FirstOrDefault(x => x.DateOnEng.Date == date.Date);
            if (dateDetail != null && dateDetail.LeaveTypeId != null)
                return dateDetail.LeaveTypeId.Value;

            if (leaveRequest.LeaveTypeId2 != null)
            {
                // if multiple then check for first leave
                int daysDiff = (date - leaveRequest.FromDateEng.Value).Days + 1;
                if (daysDiff <= leaveRequest.Days1)
                    return leaveRequest.LeaveTypeId;
                else
                    return leaveRequest.LeaveTypeId2.Value;

            }
            // if single leave then return LeaveTypeId
            return leaveRequest.LeaveTypeId;
        }

        public static int GetLeaveTypeIdForLeaveRequest(DateTime date, int leaveTypeId, int? leaveTypeId2, DateTime from, int days1)
        {
            if (leaveTypeId2 != null)
            {
                // if multiple then check for first leave
                int daysDiff = (date - from).Days + 1;
                if (daysDiff <= days1)
                    return leaveTypeId;
                else
                    return leaveTypeId2.Value;

            }
            // if single leave then return LeaveTypeId
            return leaveTypeId;
        }

        public static List<LeaveDetail> GetLeaveDetail(
            LeaveRequest leaveRequest, EEmployee emp, bool CountHolidayInBetweenLeave,
            bool onlyCountPublicHolidayAsLeaveWeeklySaturdayIsHoliday,bool onlyCountWeeklySaturdayAsLeavePublicHolidayIsHoliday, List<GetHolidaysForAttendenceResult> holiday)
        {
            float daysCount = 0;
            List<LeaveDetail> list = new List<LeaveDetail>();
            

            if (leaveRequest.IsHour.Value || leaveRequest.IsHalfDayLeave.Value)
            {
                LeaveDetail ld = new LeaveDetail();
                ld.DateOn = GetAppropriateDate(leaveRequest.FromDateEng.Value);
                ld.DateOnEng = leaveRequest.FromDateEng.Value.Date;

                if (leaveRequest.IsHour != null && leaveRequest.IsHour.Value && leaveRequest.CompensatorIsAddType != null && leaveRequest.CompensatorIsAddType.Value)
                    ld.IsHolidayForCompensatoryType = leaveRequest.CompensatorIsAddType.Value;

                list.Add(ld);
            }
            else
            {
                int day = 1;
                for (int i = 0; i <= (leaveRequest.ToDateEng.Value - leaveRequest.FromDateEng.Value).Days; i++)
                {
                    DateTime date = leaveRequest.FromDateEng.Value.AddDays(i);

                    int leaveTypeId = GetLeaveTypeIdForLeaveRequest(leaveRequest, date);
                    LLeaveType leaveType = new LeaveAttendanceManager().GetLeaveType(leaveTypeId);
                    bool onlyCountPublicHolidayAsLeaveWeeklySaturdayIsHoliday1 = false;
                    bool onlyCountWeeklySaturdayAsLeavePublicHolidayIsHoliday1 = false;
                    bool countHolidayAsLeave = LeaveAttendanceManager.GetLeaveRequestCountHolidayAsLeaveSetting(leaveTypeId, ref onlyCountPublicHolidayAsLeaveWeeklySaturdayIsHoliday1,ref onlyCountWeeklySaturdayAsLeavePublicHolidayIsHoliday1);

                    if (leaveType != null && leaveType.FreqOfAccrual == LeaveAccrue.COMPENSATORY)
                    {
                        LeaveDetail ld = new LeaveDetail();
                        ld.DateOn = GetAppropriateDate(date);
                        ld.DateOnEng = date;
                        ld.LeaveTypeId = leaveTypeId;
                        ld.IsHolidayForCompensatoryType = leaveRequest.CompensatorIsAddType.Value;
                       

                        list.Add(ld);

                        daysCount += 1;
                    }
                    else if (IsDishHomeSundayLeave(leaveRequest.LeaveTypeId))
                    {
                        // sunday leave is taken for 3 days, first day half day mark but not leave counting, second will be holiday so no leave & 3rd day full day leave
                        if (day == 1 || day == 3)
                        {
                            LeaveDetail ld = new LeaveDetail();
                            ld.DateOn = GetAppropriateDate(date);
                            ld.DateOnEng = date;
                            ld.LeaveTypeId = leaveTypeId;
                            ld.Day = day;
                            list.Add(ld);
                        }
                        else if (day == 2)
                        {
                            daysCount += 1;
                        }


                        day += 1;
                    }
                    else if (IsLeaveDay(leaveRequest.FromDateEng, leaveRequest.ToDateEng, date, 
                        IsHolidayExistOnLeaveRequestDay(emp.EmployeeId, date, CountHolidayInBetweenLeave,onlyCountPublicHolidayAsLeaveWeeklySaturdayIsHoliday,onlyCountWeeklySaturdayAsLeavePublicHolidayIsHoliday, holiday),
                        countHolidayAsLeave))
                    {
                        LeaveDetail ld = new LeaveDetail();
                        ld.DateOn = GetAppropriateDate(date);
                        ld.DateOnEng = date;
                        ld.LeaveTypeId = leaveTypeId;
                        list.Add(ld);

                        daysCount += 1;
                    }
                }
                // Change leave days as could be effected by Holidays in between Leave
                leaveRequest.DaysOrHours = daysCount;
            }
            return list;
        }

        public static List<LeaveDetail> GetLeaveDetail(
            bool isHour,bool isHalfDay,DateTime from,DateTime to,int leaveTypeId1,int? leaveTypeId2,int? childLeaveTypeId,
            List<GetHolidaysForAttendenceResult> holiday, int days1, 
            bool? CompensatorIsAddType,int empId, ref float daysCount)
        {
            daysCount = 0;
            List<LeaveDetail> list = new List<LeaveDetail>();
           

            if (isHour || isHalfDay)
            {
                daysCount = (float) 0.5;
                LeaveDetail ld = new LeaveDetail();
                ld.DateOn = GetAppropriateDate(from);
                ld.DateOnEng = from.Date;
                list.Add(ld);
            }
            else
            {
                int day = 1;
                for (int i = 0; i <= (to - from).Days; i++)
                {
                    DateTime date = from.AddDays(i);

                    int leaveTypeId = childLeaveTypeId != null  ? childLeaveTypeId.Value :
                        GetLeaveTypeIdForLeaveRequest(date, leaveTypeId1, leaveTypeId2, from, days1);

                    LLeaveType leaveType = new LeaveAttendanceManager().GetLeaveType(leaveTypeId);
                    bool onlyCountPublicHolidayAsLeaveWeeklySaturdayIsHoliday = false;
                    bool onlyCountWeeklySaturdayAsLeavePublicHolidayIsHoliday = false;
                    bool CountHolidayInBetweenLeave = LeaveAttendanceManager.GetLeaveRequestCountHolidayAsLeaveSetting(leaveTypeId, ref onlyCountPublicHolidayAsLeaveWeeklySaturdayIsHoliday,ref onlyCountWeeklySaturdayAsLeavePublicHolidayIsHoliday);

                    if (leaveType != null && leaveType.FreqOfAccrual == LeaveAccrue.COMPENSATORY)
                    {
                        LeaveDetail ld = new LeaveDetail();
                        ld.DateOn = GetAppropriateDate(date);
                        ld.DateOnEng = date;
                        ld.LeaveTypeId = leaveTypeId;
                        ld.IsHolidayForCompensatoryType = CompensatorIsAddType.Value;


                        list.Add(ld);

                        daysCount += 1;
                    }
                    else if (IsDishHomeSundayLeave(leaveTypeId1))
                    {
                        // sunday leave is taken for 3 days, first day half day mark but not leave counting, second will be holiday so no leave & 3rd day full day leave
                        if (day == 1 || day == 3)
                        {
                            LeaveDetail ld = new LeaveDetail();
                            ld.DateOn = GetAppropriateDate(date);
                            ld.DateOnEng = date;
                            ld.LeaveTypeId = leaveTypeId;
                            ld.Day = day;
                            list.Add(ld);
                        }
                        else if (day == 2)
                        {
                            daysCount += 1;
                        }


                        day += 1;
                    }
                    else if (IsLeaveDay(from, to, date,
                        IsHolidayExistOnLeaveRequestDay(empId, date, CountHolidayInBetweenLeave,onlyCountPublicHolidayAsLeaveWeeklySaturdayIsHoliday,onlyCountWeeklySaturdayAsLeavePublicHolidayIsHoliday, holiday),
                        CountHolidayInBetweenLeave))
                    {
                        LeaveDetail ld = new LeaveDetail();
                        ld.DateOn = GetAppropriateDate(date);
                        ld.DateOnEng = date;
                        ld.LeaveTypeId = leaveTypeId;
                        list.Add(ld);

                        daysCount += 1;
                    }
                }
                // Change leave days as could be effected by Holidays in between Leave
                //daysOrHours = daysCount;
            }
            return list;
        }

        /// <summary>
        /// Returns the Leave Taken list for the Employee
        /// </summary>
        /// <param name="employeeId"></param>
        /// <returns></returns>
        public static List<LeaveAdjustment> GetLeaveSummary(int employeeId)
        {
            int payrollPeriodId = -1, startingPayrollPeriodId = -1, endingPayrollPeriodId = -1;
            bool readingSumForMiddleFiscalYearStartedReqd = false;

            PayrollPeriod payrollPeriod = CommonManager.GetLastPayrollPeriod();
            
            if (payrollPeriod != null)
            {
                payrollPeriodId = payrollPeriod.PayrollPeriodId;
            
                bool isAttandanceSavedForLastPayroll = LeaveAttendanceManager.IsAttendanceSaved(payrollPeriod.PayrollPeriodId);

                PayrollPeriod startingMonthParollPeriodInFiscalYear = CalculationManager.GenerateForPastIncome(
                    payrollPeriodId, ref readingSumForMiddleFiscalYearStartedReqd, ref startingPayrollPeriodId, ref endingPayrollPeriodId);

                List<LeaveAdjustment> leaveSummary = PayrollDataContext.LeaveAdjustments
                    .Where(x => x.PayrollPeriodId >= startingPayrollPeriodId && x.EmployeeId==employeeId )                    
                    .OrderByDescending(x=>x.PayrollPeriodId)
                    .ToList();

               // if( isAttandanceSavedForLastPayroll ==false)
                {

                // Add leave request whose attendance period has not been saved
                    var requestedApprovedLeaves =
                        (
                            from ld in PayrollDataContext.LeaveDetails
                            join lr in PayrollDataContext.LeaveRequests on ld.LeaveRequestId equals lr.LeaveRequestId

                            where lr.Status == (int)LeaveRequestStatusEnum.Approved
                            && lr.EmployeeId == employeeId
                            && ld.DateOnEng > payrollPeriod.EndDateEng //&& ld.DateOnEng <= payrollPeriod.EndDateEng
                            //select ld
                            select new
                            {
                                LeaveTypeId = 
                                (
                                    // in case of parent leave show balance under parent leave
                                    (lr.ChildLeaveTypeId != null || lr.ChildLeaveTypeId2 != null)? lr.LeaveTypeId :
                                    ld.LeaveTypeId != null ? ld.LeaveTypeId : lr.LeaveTypeId
                                ),
                                IsHalfDay = lr.IsHalfDayLeave,
                                ToDateEng = ld.DateOnEng,
                                Hours = (lr.IsHour != null && lr.IsHour.Value ? lr.DaysOrHours : 0),
                                IsHourlyLeave = lr.IsHour
                            }
                        ).ToList();


                    foreach (var detail in requestedApprovedLeaves)
                    {
                        LeaveAdjustment adj = new LeaveAdjustment();
                        adj.EmployeeId = employeeId;

                        // then future/advance leave
                        if (detail.ToDateEng > payrollPeriod.EndDateEng)
                            adj.PayrollPeriodId = -1;
                        else
                            adj.PayrollPeriodId = payrollPeriod.PayrollPeriodId;
                        adj.LeaveTypeId = detail.LeaveTypeId.Value;

                        if (detail.IsHourlyLeave != null && detail.IsHourlyLeave.Value)
                            adj.Taken = detail.Hours.Value;
                        else if (detail.IsHalfDay != null && detail.IsHalfDay.Value)
                            adj.Taken = 0.5;
                        else
                            adj.Taken = 1;

                        leaveSummary.Add(adj);
                    }


                }

                List<LeaveAdjustment> unpaidLeaveList = new List<LeaveAdjustment>();
                //Get Unpaid Leave Balance also
                foreach (int leavePayrollPeriodId in leaveSummary.Select(x => x.PayrollPeriodId).Distinct())
                {
                    //here for ABS also will be counted so think in future
                    Attendence attendance =
                        PayrollDataContext.Attendences.SingleOrDefault(x => x.EmployeeId == employeeId && x.PayrollPeriodId == leavePayrollPeriodId);

                    if (attendance != null)
                    {
                        unpaidLeaveList.Add(new LeaveAdjustment { EmployeeId = employeeId, PayrollPeriodId= leavePayrollPeriodId,
                         LeaveTypeId=0,Taken = attendance.PayDays.Value});
                    }
                }

                leaveSummary.AddRange(unpaidLeaveList);

                return leaveSummary;
            }

            return null;
        }

        public static PayrollPeriod GetCurrentMonthPayrollPeriod()
        {
            PayrollPeriod currentMonth = CommonManager.GetLastPayrollPeriod();

            return currentMonth;
        }

        public static List<PayrollPeriod> GetPayrollPeriodList(int EmployeeId)
        {
            int payrollPeriodId = -1, startingPayrollPeriodId = -1, endingPayrollPeriodId = -1;
            bool readingSumForMiddleFiscalYearStartedReqd = false;

            int leaveStartMonth = SessionManager.CurrentCompany.LeaveStartMonth == null ? 4 : SessionManager.CurrentCompany.LeaveStartMonth.Value ;

            PayrollPeriod payrollPeriod = CommonManager.GetLastPayrollPeriod(); //CommonManager.GetFirstPayrollPeriod(SessionManager.CurrentCompanyId);
            if (payrollPeriod != null)
            {
                payrollPeriodId = payrollPeriod.PayrollPeriodId;
                payrollPeriod = CommonManager.GetLastPayrollPeriod();

                if (IsEnglish || leaveStartMonth == 4)
                {
                    PayrollPeriod startingMonthParollPeriodInFiscalYear = CalculationManager.GenerateForPastIncome(
                        payrollPeriodId, ref readingSumForMiddleFiscalYearStartedReqd, ref startingPayrollPeriodId, ref endingPayrollPeriodId);
                }
                else
                {

                    int? startPeriodId =
                    (
                        from p in PayrollDataContext.PayrollPeriods
                        orderby p.EndDateEng descending
                        where p.Month == leaveStartMonth
                        select p.PayrollPeriodId
                    ).FirstOrDefault();

                    if (startPeriodId == null)
                    {
                        startPeriodId = PayrollDataContext.PayrollPeriods.OrderBy(x => x.StartDateEng).FirstOrDefault().PayrollPeriodId;
                    }

                    startingPayrollPeriodId = startPeriodId.Value;
                }

                List<PayrollPeriod> payrollperiods=
                    (
                        from p in PayrollDataContext.PayrollPeriods
                        //join a in PayrollDataContext.Attendences
                        //    on new { p.PayrollPeriodId, EmployeeId } equals new { a.PayrollPeriodId, a.EmployeeId }

                        // Left join with Attendances as for first time Attendance will not exists
                        join l in PayrollDataContext.Attendences
                            on new {  p.PayrollPeriodId, EmployeeId} equals new {  l.PayrollPeriodId,   l.EmployeeId}
                            into JoinedPayrollAttendance from l in JoinedPayrollAttendance.DefaultIfEmpty()       

                        where p.PayrollPeriodId >= startingPayrollPeriodId //&& p.PayrollPeriodId <= payrollPeriodId
                            && p.CompanyId == SessionManager.CurrentCompanyId
                        orderby p.PayrollPeriodId descending
                        select p
                    ).ToList();


                PayrollPeriod newCurrentPayroll = GetCurrentMonthPayrollPeriod();
                bool isCurrentMonthExists = false;
                foreach (PayrollPeriod payroll in payrollperiods)
                {
                    if (payroll.PayrollPeriodId == newCurrentPayroll.PayrollPeriodId)
                    {
                        isCurrentMonthExists = true;
                        break;
                    }
                }
                if (isCurrentMonthExists == false)
                {
                    payrollperiods.Insert(0, newCurrentPayroll);
                }

                return payrollperiods;

                //List<PayrollPeriod> payrollperiods = PayrollDataContext.PayrollPeriods
                    
                //    .Where(x => x.PayrollPeriodId >= startingPayrollPeriodId && x.PayrollPeriodId <= payrollPeriodId)
                //    .Where(x=>x.CompanyId == SessionManager.CurrentCompanyId)
                //    .OrderByDescending(x=>x.PayrollPeriodId)
                //    .ToList();
                //return payrollperiods;

            }
            return null;
        }

        public static void SaveLeaveProject(List<LeaveProject> list)
        {
            PayrollDataContext.LeaveProjects.InsertAllOnSubmit(list);
            PayrollDataContext.SubmitChanges();
        }
        //public static GetLeaveApprovalEmployeeNamesResult GetLeaveApprovalEmployeeNames(int EmployeeId)
        //{
        //    return PayrollDataContext.GetLeaveApprovalEmployeeNames(EmployeeId).SingleOrDefault();
        //}
        public static List<LeaveProject> GetAllLeaveProject()
        {
            return PayrollDataContext.LeaveProjects.OrderBy(x => x.Name).ToList();
        }
        public static LeaveApprovalEmployee GetEmployeeLeaveApprovalTeam(int empId)
        {
            return
                (
                    from p in PayrollDataContext.LeaveProjectEmployees
                    join la in PayrollDataContext.LeaveApprovalEmployees on p.LeaveProjectId equals la.LeaveProjectId
                    where p.EmployeeId == empId
                    select la
                ).FirstOrDefault();
        }

        public static bool UpdateLeaveApproveNotification(LeaveApprovalEmployee leaveApprovalEmp)
        {
            LeaveApprovalEmployee record = PayrollDataContext
                .LeaveApprovalEmployees.Where(x => //x.DepartmentID == leaveApprovalEmp.DepartmentID
                    leaveApprovalEmp.LeaveProjectId == x.LeaveProjectId).SingleOrDefault();
            if (record == null)
            {

                //if (PayrollDataContext.LeaveApprovalEmployees.Any(x => x.LeaveProjectId == leaveApprovalEmp.LeaveProjectId))
                //{
                //    return false;
                //}

                PayrollDataContext.LeaveApprovalEmployees.InsertOnSubmit(leaveApprovalEmp);
                return SaveChangeSet();
            }
            else
            {
                record.ApplyTo = leaveApprovalEmp.ApplyTo;
                record.ApplyToLevel = leaveApprovalEmp.ApplyToLevel;

                record.CC1Level = leaveApprovalEmp.CC1Level;
                record.CC2Level = leaveApprovalEmp.CC2Level;
                record.CC3Level = leaveApprovalEmp.CC3Level;
                record.CC4Level = leaveApprovalEmp.CC4Level;
                record.CC5Level = leaveApprovalEmp.CC5Level;

                record.CC1 = leaveApprovalEmp.CC1;
                record.AllowCC1ToApprove = leaveApprovalEmp.AllowCC1ToApprove;

                record.CC2 = leaveApprovalEmp.CC2;
                record.AllowCC2ToApprove = leaveApprovalEmp.AllowCC2ToApprove;

                record.TimeSheetHRReviewerCC2 = leaveApprovalEmp.TimeSheetHRReviewerCC2;

                record.CC3 = leaveApprovalEmp.CC3;
                record.AllowCC3TOApprove = leaveApprovalEmp.AllowCC3TOApprove;

                record.CC4 = leaveApprovalEmp.CC4;
                record.AllowCC4ToApprove = leaveApprovalEmp.AllowCC4ToApprove;

                record.CC5 = leaveApprovalEmp.CC5;
                record.AllowCC5ToApprove = leaveApprovalEmp.AllowCC5ToApprove;

                record.ApprovalNotificationToType = leaveApprovalEmp.ApprovalNotificationToType;
                record.CCEmailList = leaveApprovalEmp.CCEmailList;

               

                UpdateChangeSet();
                return true;
            }
        }

        public static GetLeaveApprovalEmployeeByDepartmentIdResult GetLeaveApprovalEmployeeByDepartmentId(int depratmentId, int leaveProjectId)
        {
            GetLeaveApprovalEmployeeByDepartmentIdResult record = PayrollDataContext.GetLeaveApprovalEmployeeByDepartmentId(depratmentId,leaveProjectId)
                .FirstOrDefault();

            
            return record;
        }

        public static int? GetUnreadMessageByUserName(string UserName)
        {
            try
            {
                return PayrollDataContext.GetUnreadPayrollMessage(UserName);
            }
            catch { }
            return 0;
        }

        //public static GetRecipientEmailResult GetRecipientByEmployeeId(int EmployeeId)
        //{
        //    return PayrollDataContext.GetRecipientEmail(EmployeeId).SingleOrDefault();
        //}

        public static GetRecipientEmailResult GetRecipientByEmployeeId(int EmployeeId, PayrollDataContext PayrollDataContext)
        {
            GetRecipientEmailResult entity = PayrollDataContext.GetRecipientEmail(EmployeeId).SingleOrDefault();

            //// check & remove the duplicate for Messaging & Emailing
            //if (!string.IsNullOrEmpty(entity.ApplyToUserName) && !string.IsNullOrEmpty(entity.CC1UserName)
            //    && entity.ApplyToUserName.ToLower().Trim() == entity.CC1UserName.ToLower().Trim())
            //{
            //    entity.CC1UserName = "";
            //    entity.CC1Email = "";
            //}

            //if (!string.IsNullOrEmpty(entity.ApplyToUserName) && !string.IsNullOrEmpty(entity.CC2UserName)
            //    && entity.ApplyToUserName.ToLower().Trim() == entity.CC2UserName.ToLower().Trim())
            //{
            //    entity.CC2UserName = "";
            //    entity.CC2Email = "";
            //}

            //if (!string.IsNullOrEmpty(entity.CC1UserName) && !string.IsNullOrEmpty(entity.CC2UserName)
            //   && entity.CC1UserName.ToLower().Trim() == entity.CC2UserName.ToLower().Trim())
            //{
            //    entity.CC2UserName = "";
            //    entity.CC2Email = "";
            //}

            //if (CommonManager.CompanySetting.LeaveRequestToOnlyOneSupervisor && supervisorEmployeeId != null)
            //{
            //    if (entity.ApplyToEmployeeId != supervisorEmployeeId)
            //    {
            //        entity.ApplyToEmail = "";
            //        entity.ApplyToUserName = "";
            //    }
            //    if (entity.CC1EmployeeId != supervisorEmployeeId)
            //    {
            //        entity.CC1Email = "";
            //        entity.CC1UserName = "";
            //    }
            //    if (entity.CC2EmployeeId != supervisorEmployeeId)
            //    {
            //        entity.CC2Email = "";
            //        entity.CC2UserName = "";
            //    }
            //    if (entity.CC3EmployeeId != supervisorEmployeeId)
            //    {
            //        entity.CC3Email = "";
            //        entity.CC3UserName = "";
            //    }
            //    if (entity.CC4EmployeeId != supervisorEmployeeId)
            //    {
            //        entity.CC4Email = "";
            //        entity.CC4UserName = "";
            //    }
            //    if (entity.CC5EmployeeId != supervisorEmployeeId)
            //    {
            //        entity.CC5Email = "";
            //        entity.CC5UserName = "";
            //    }
            //}

            //if (CommonManager.CompanySetting.LeaveRequestToOnlyOneSupervisor && reviewToEmployeeId != null)
            //{
            //    if (entity.ApplyToEmployeeId != reviewToEmployeeId)
            //    {
            //        entity.ApplyToEmail = "";
            //        entity.ApplyToUserName = "";
            //    }
            //    if (entity.CC1EmployeeId != reviewToEmployeeId)
            //    {
            //        entity.CC1Email = "";
            //        entity.CC1UserName = "";
            //    }
            //    if (entity.CC2EmployeeId != reviewToEmployeeId)
            //    {
            //        entity.CC2Email = "";
            //        entity.CC2UserName = "";
            //    }
            //    if (entity.CC3EmployeeId != reviewToEmployeeId)
            //    {
            //        entity.CC3Email = "";
            //        entity.CC3UserName = "";
            //    }
            //    if (entity.CC4EmployeeId != reviewToEmployeeId)
            //    {
            //        entity.CC4Email = "";
            //        entity.CC4UserName = "";
            //    }
            //    if (entity.CC5EmployeeId != reviewToEmployeeId)
            //    {
            //        entity.CC5Email = "";
            //        entity.CC5UserName = "";
            //    }
            //}
          

            return entity;
        }

        public static Boolean CheckEditSequence(int editSequence, int leaveRequestId)
        {
            return PayrollDataContext.LeaveRequests
                .Any(x => x.LeaveRequestId == leaveRequestId && x.EditSequence == editSequence);
        }

        public static bool RemoveRecipient(int employeeId, int departmentId)
        {
            LeaveApprovalEmployee leave = PayrollDataContext.LeaveApprovalEmployees.Where(x => x.DepartmentID == departmentId).SingleOrDefault();
            //leave.ApplyTo
            return false;
        }

        public static bool IsLeaveActiveForEmployee(int employeeId, int leaveTypeId)
        {
            return
                PayrollDataContext.LEmployeeLeaves.Any(x => x.EmployeeId == employeeId && x.LeaveTypeId == leaveTypeId &&
                    x.IsActive == true);
        }

        //public static bool IsAllowedToApproveLeave(int approvalEmployeeId)
        //{
        //    LeaveApprovalEmployee lae = new LeaveApprovalEmployee();
        //    bool retValue = false;
        //    if (PayrollDataContext.LeaveApprovalEmployees.Any(x => x.ApplyTo == approvalEmployeeId))
        //    {
        //        retValue = true;
        //    }
        //    else if (PayrollDataContext.LeaveApprovalEmployees.Any(x => x.CC1 == approvalEmployeeId))
        //    {
        //        lae = PayrollDataContext.LeaveApprovalEmployees.Where(x => x.CC1 == approvalEmployeeId).FirstOrDefault();
        //        if (lae == null)
        //            retValue = false;
        //        else
        //            retValue = lae.AllowCC1ToApprove.Value;
        //    }
        //    else if (PayrollDataContext.LeaveApprovalEmployees.Where(x => x.CC2 == approvalEmployeeId).Count() > 0)
        //    {
        //        lae = PayrollDataContext.LeaveApprovalEmployees.Where(x => x.CC2 == approvalEmployeeId).FirstOrDefault();
        //        if(lae == null)
        //                retValue = false;
        //        else
        //            retValue = lae.AllowCC2ToApprove.Value;
        //    }
        //    else if (PayrollDataContext.LeaveApprovalEmployees.Where(x => x.CC3 == approvalEmployeeId).Count() > 0)
        //    {
        //        lae = PayrollDataContext.LeaveApprovalEmployees.Where(x => x.CC3 == approvalEmployeeId).FirstOrDefault();
        //        if (lae == null)
        //            retValue = false;
        //        else
        //            retValue = lae.AllowCC3TOApprove.Value;
        //    }
        //    else if (PayrollDataContext.LeaveApprovalEmployees.Where(x => x.CC4 == approvalEmployeeId).Count() > 0)
        //    {
        //        lae = PayrollDataContext.LeaveApprovalEmployees.Where(x => x.CC4 == approvalEmployeeId).FirstOrDefault();
        //        if (lae == null)
        //            retValue = false;
        //        else
        //            retValue = lae.AllowCC4ToApprove.Value;
        //    }
        //    else if (PayrollDataContext.LeaveApprovalEmployees.Where(x => x.CC5 == approvalEmployeeId).Count() > 0)
        //    {
        //        lae = PayrollDataContext.LeaveApprovalEmployees.Where(x => x.CC5 == approvalEmployeeId).FirstOrDefault();
        //        if (lae == null)
        //            retValue = false;
        //        else
        //            retValue = lae.AllowCC5ToApprove.Value;
        //    }
        //    return retValue;
        //}


        public static bool IsAllowedToApproveLeave(int approvalEmployeeId,int requesterEmployeeId,PreDefindFlowType type
            ,int leaveReuestId)
        {


            if ( (CommonManager.Setting.LeaveRequestSettingUsingPreDefinedList != null &&
                CommonManager.Setting.LeaveRequestSettingUsingPreDefinedList.Value)
                 ||
                (CommonManager.Setting.LeaveRequestSettngUsingOneToOne != null &&
                CommonManager.Setting.LeaveRequestSettngUsingOneToOne.Value)
                )
            {
                return LeaveAttendanceManager.GetPreDefinedApprovalList(null, requesterEmployeeId, type)
                    .Any(x => x.Value == approvalEmployeeId.ToString());
            }
            //else if (CommonManager.Setting.LeaveRequestSettngUsingOneToOne != null &&
            //    CommonManager.Setting.LeaveRequestSettngUsingOneToOne.Value)
            //    return LeaveAttendanceManager.GetOneToOneApprovalList(null, requesterEmployeeId)
            //        .Any(x => x.Value == approvalEmployeeId.ToString());
            else
            {

                LeaveRequest request = null;

                if (leaveReuestId != 0)
                {
                    request = LeaveAttendanceManager.GetLeaveRequestById(leaveReuestId);
                    if (request != null && (request.RecommendEmployeeId == approvalEmployeeId ||
                                request.SupervisorEmployeeIdInCaseOfSelection == approvalEmployeeId))
                        return true;
                }

                LeaveProjectEmployee leaveProject
                    = PayrollDataContext.LeaveProjectEmployees.FirstOrDefault(x => x.EmployeeId == requesterEmployeeId);

                if (leaveProject != null)
                {
                    LeaveApprovalEmployee
                        approvalSetting = PayrollDataContext.LeaveApprovalEmployees.FirstOrDefault(x => x.LeaveProjectId == leaveProject.LeaveProjectId);

                    LeaveApprovalEmployee approvalSetting2 = null;

                    if (leaveProject.LeaveProjectId2 != null)
                        approvalSetting2 = PayrollDataContext.LeaveApprovalEmployees.FirstOrDefault(x => x.LeaveProjectId == leaveProject.LeaveProjectId2);

                    if (approvalSetting != null)
                    {
                        if (approvalSetting.ApplyTo == approvalEmployeeId)
                            return true;

                        if (approvalSetting.CC1 == approvalEmployeeId)
                        {
                            return true;
                        }

                        if (approvalSetting.CC2 == approvalEmployeeId)
                        {
                            return true;
                        }

                        if (approvalSetting.CC3 == approvalEmployeeId)
                        {
                            return true;
                        }

                        if (approvalSetting.CC4 == approvalEmployeeId)
                        {
                            return true;
                        }

                        if (approvalSetting.CC5 == approvalEmployeeId)
                        {
                            return true;
                        }
                    }

                    if (approvalSetting2 != null)
                    {
                        if (approvalSetting2.ApplyTo == approvalEmployeeId)
                            return true;

                        if (approvalSetting2.CC1 == approvalEmployeeId)
                        {
                            return true;
                        }
                    }
                }

            }

            return false;

        }
        #endregion


        //public static List<GetEmployeeListForTimesheetResult> GetEmployeeListForTimesheet(int payrollPeroidId,bool isApproval,bool isReview)
        //{
        //    PayrollPeriod period = CommonManager.GetPayrollPeriod(payrollPeroidId);

        //    List<GetHolidaysForAttendenceResult> holidayList = new HolidayManager().GetMonthsHolidaysForAttendence(period.Month, period.Year.Value, SessionManager.CurrentCompanyId, payrollPeroidId, null, null).ToList();
        //    float weeklyHolidayTotal =  (holidayList.Where(x => x.IsWeekly != null && x.IsWeekly.Value).Count());   


        //    return
        //        PayrollDataContext.GetEmployeeListForTimesheet(
        //        SessionManager.CurrentLoggedInEmployeeId, SessionManager.CurrentCompanyId, payrollPeroidId, isApproval, isReview, weeklyHolidayTotal).OrderBy(x => x.Name).ToList();
        //}


        /// <summary>
        /// Return the Employee List for which the current Logged in Employee can Assign leave in Leave Approval page
        /// </summary>
        /// <returns></returns>
        public static List<TextValue> GetEmployeeListForLeaveAssign()
        {

            int employeeId = SessionManager.CurrentLoggedInEmployeeId;


            return
                PayrollDataContext.GetEmployeeListForAssignLeave(employeeId, SessionManager.CurrentCompanyId)
                .OrderBy(x=>x.Name)
                .Select(x => new TextValue { Text = x.Name, Value = x.EmployeeId.ToString(), ID = x.EmployeeId }).ToList();

            
        }

        /// <summary>
        /// Return the Employee List for which the current Logged in Employee can Assign leave in Leave Approval page
        /// </summary>
        /// <returns></returns>
        public static List<TextValue> GetEmployeeListForLeaveAssignWithID()
        {

            int employeeId = SessionManager.CurrentLoggedInEmployeeId;


            return
                PayrollDataContext.GetEmployeeListForAssignLeave(employeeId, SessionManager.CurrentCompanyId)
                .OrderBy(x => x.Name)
                .Select(x => new TextValue { Text = x.Name + " - " +x.INO, Value = x.EmployeeId.ToString() }).ToList();


        }

        public static bool IsValidEmployeeForApproval()
        {
            return IsEmployeeAllowedToApproveLeave(SessionManager.CurrentLoggedInEmployeeId);

        }





        //public static Ext.Net.Paging<GetAllLeaveByDateAndEmployeeResult> GetAllLeaveByDateAndEmployee(int employeeId, bool isYearly, int payrollPeriodId, int start, int limit)
        //{
        //    List<GetAllLeaveByDateAndEmployeeResult> list = DAL.BaseBiz.PayrollDataContext.GetAllLeaveByDateAndEmployee(
        //        LeaveRequestManager.GetStartDateForLeaveDetails() , employeeId, isYearly, payrollPeriodId, start, limit).ToList();
           
        //    if( list.Count>0)
        //        return new Ext.Net.Paging<GetAllLeaveByDateAndEmployeeResult>(list, list[0].TotalRows.Value);
        //    else
        //        return new Ext.Net.Paging<GetAllLeaveByDateAndEmployeeResult>(list, 0);
        //}

        public static bool IsEmployeeAllowedToApproveLeave(int approvalEmployeeId)
        {

            if (HttpContext.Current != null && HttpContext.Current.Items != null)
            {
                string key = "ItemsSavedIsEmployeeAllowedToApproveLeave";
                if (HttpContext.Current.Items[key] != null)
                    return bool.Parse(HttpContext.Current.Items[key].ToString());

                bool state = (bool)PayrollDataContext.HasLeaveApprovalPermission(approvalEmployeeId, null, (int)PreDefindFlowType.Leave).Value;

                HttpContext.Current.Items[key] = state;

                return state;
            }
            else
                return PayrollDataContext.HasLeaveApprovalPermission(approvalEmployeeId, null, (int)PreDefindFlowType.Leave).Value;

            
        }
        public static bool IsEmployeeAllowedToApproveLeave(int approvalEmployeeId,bool? isRecommender)
        {
            if (isRecommender == null)
                return IsEmployeeAllowedToApproveLeave(approvalEmployeeId);

            if (HttpContext.Current != null && HttpContext.Current.Items != null)
            {
                string key = "ItemsSavedIsEmployeeAllowedToApproveLeaveWithRecommenderOption";
                if (HttpContext.Current.Items[key] != null)
                    return bool.Parse(HttpContext.Current.Items[key].ToString());

                bool state = (bool)PayrollDataContext.HasLeaveApprovalPermission(approvalEmployeeId, isRecommender.Value, (int)PreDefindFlowType.Leave).Value;

                HttpContext.Current.Items[key] = state;

                return state;
            }
            else
                return PayrollDataContext.HasLeaveApprovalPermission(approvalEmployeeId, isRecommender.Value, (int)PreDefindFlowType.Leave).Value;


        }
        public static bool IsEmployeeAllowedToApproveLeave()
        {
            return PayrollDataContext.HasLeaveApprovalPermission(SessionManager.CurrentLoggedInEmployeeId, false
                ,(int)PreDefindFlowType.Leave)
                .Value;

          
        }
        public static bool IsEmployeeAllowedToApproveOvertime()
        {
            return PayrollDataContext.HasLeaveApprovalPermission(SessionManager.CurrentLoggedInEmployeeId, false
                , (int)PreDefindFlowType.Overtime)
                .Value;


        }
        public static bool IsEmployeeAllowedToApproveLeaveForEmployee(int employeeId)
        {
            LeaveProjectEmployee empLeaveProject
                = PayrollDataContext.LeaveProjectEmployees.FirstOrDefault(x => x.EmployeeId == employeeId);

            if (empLeaveProject == null)
                return false;

            LeaveApprovalEmployee empTeam = PayrollDataContext.LeaveApprovalEmployees.FirstOrDefault(x => x.LeaveProjectId
                == empLeaveProject.LeaveProjectId);

            if (empTeam == null)
                return false;


            if (empTeam.ApplyTo == SessionManager.CurrentLoggedInEmployeeId
                || empTeam.CC1 == SessionManager.CurrentLoggedInEmployeeId)
                return true;

            return false;
        }
        public static bool IsEmployeeAllowedToReviewTimesheet(int empID)
        {
            return PayrollDataContext.LeaveApprovalEmployees
                //.Any(x => x.CC2 != null && x.CC2==empID && x.TimeSheetHRReviewerCC2 != null && x.TimeSheetHRReviewerCC2.Value);
                .Any(x => 
                    (x.CC2 != null && x.CC2 == empID && (x.TimeSheetHRReviewerCC2 == null || x.TimeSheetHRReviewerCC2.Value==false))
                    ||
                    (x.ApplyTo ==empID)
                    ||
                    (x.CC1 == empID)
                    ||
                    (x.CC2 == empID)
                    ||
                    (x.CC3==empID)
                    ||
                    (x.CC4==empID)
                    ||
                    (x.CC5==empID)
                    
                    );
        }

        public static bool IsEmployeeAllowedToApproveTimesheet(int empID)
        {
            return PayrollDataContext.LeaveApprovalEmployees
                //.Any(x => x.CC2 != null && x.CC2==empID && x.TimeSheetHRReviewerCC2 != null && x.TimeSheetHRReviewerCC2.Value);
                .Any(x =>
                    (x.CC2 != null && x.CC2 == empID && (x.TimeSheetHRReviewerCC2 != null && x.TimeSheetHRReviewerCC2.Value == true))
                   
                    );
        }

        public static GetLeaveListAsPerEmployeeResult GetLeaveListAsPerEmployee(int LeaveTypeId, int employeeId, int payrollPeriodId, int companyId, int daysdiff, int remMonths, 
            bool includeIncludeRequestRecommendedStatusAlso,int? skipCurrentLeaveRequestId)
        {
            return PayrollDataContext.GetLeaveListAsPerEmployee(employeeId, payrollPeriodId, companyId, daysdiff, remMonths,includeIncludeRequestRecommendedStatusAlso
                , skipCurrentLeaveRequestId)
                .Where(x => x.LeaveTypeId == LeaveTypeId).Take(1).SingleOrDefault();

        }

        public static LeaveRequestStatusEnum GetLeaveRequestStatus(int LeaveRequestId)
        {
            return (LeaveRequestStatusEnum)PayrollDataContext.LeaveRequests.Where(x => x.LeaveRequestId == LeaveRequestId).SingleOrDefault().Status;
        }

        public static LeaveRequest GetLeaveRequestById(int LeaveRequestId)
        {
            return PayrollDataContext.LeaveRequests.Where(x => x.LeaveRequestId == LeaveRequestId).SingleOrDefault();
        }

        public static List<DashboardEmployeeOnLeaveTypeCountsResult> GetEmployeeOnLeaveTypeCounts()
        {
           
            DateTime today = DateTime.Now;
            DateTime startDate = new DateTime(today.Year, today.Month, 1);
            DateTime endDate = new DateTime(today.Year, today.Month, DateHelper.GetTotalDaysInTheMonth(today.Year, today.Month, true));

            return PayrollDataContext.DashboardEmployeeOnLeaveTypeCounts(startDate, endDate).ToList();

        }

        public static Status InsertUpdateLeveBranchDepartment(int LeaveId, List<LeaveBranchDepartment> _objListBranchDepartment)
        {

            Status myStatus = new Status();
            List<LeaveBranchDepartment> _dbLeaveBranchDepartmentsList = PayrollDataContext.LeaveBranchDepartments.Where(x => x.LeaveTypeID == LeaveId).ToList();
            if (_dbLeaveBranchDepartmentsList.Any())
                PayrollDataContext.LeaveBranchDepartments.DeleteAllOnSubmit(_dbLeaveBranchDepartmentsList);
            PayrollDataContext.LeaveBranchDepartments.InsertAllOnSubmit(_objListBranchDepartment);
            PayrollDataContext.SubmitChanges();
            return myStatus;
        }

        public static List<LeaveBranchDepartment> GetLeaveBranchDepartment(int LeaveTypeID)
        {
            return PayrollDataContext.LeaveBranchDepartments.Where(x=>x.LeaveTypeID==LeaveTypeID).ToList();
        }

        public static ResponseStatus ApproveLeaveRequest(int leaveRequestId)
        {

            int count = 0;
            ResponseStatus responseStatus = ApproveSelectedLeaveRequests(new List<int> { leaveRequestId }, ref count);

          

            return responseStatus;
        }

        /// <summary>
        /// 
        /// </summary>
        public static Status CreateLeaveRequest(LeaveRequest entity)
        {
            Status status = new Status();

            if(entity.LeaveTypeId == 0)
            {
                status.ErrorMessage = "Leave type is required.";
                return status;
            }

            entity.IsSpecialCase = false;

            LeaveRequest leave = new LeaveRequest();

            ResponseStatus newstatus = LeaveRequestManager.GetLeaveRequestFromCalendarEntity(entity, leave, true, SessionManager.CurrentLoggedInEmployeeId);

            if (newstatus.IsSuccess == "true") { }
            else
            {
                status.ErrorMessage = newstatus.ErrorMessage;
                return status;
            }


            if (CommonManager.CompanySetting.LeaveRequestToOnlyOneSupervisor ||
               (CommonManager.Setting.LeaveRequestSettingUsingPreDefinedList != null && CommonManager.Setting.LeaveRequestSettingUsingPreDefinedList.Value)
               || (CommonManager.Setting.LeaveRequestSettngUsingOneToOne != null && CommonManager.Setting.LeaveRequestSettngUsingOneToOne.Value))
            {
               

                if (entity.SupervisorEmployeeIdInCaseOfSelection == 0)
                {
                    status.ErrorMessage = "Approval must be selected.";
                    return status;
                }
                if (entity.RecommendEmployeeId== 0)
                {
                    status.ErrorMessage = "Reviewer must be selected.";
                    return status;
                }

                if (entity.RecommendEmployeeId != 0)
                    leave.RecommendEmployeeId = entity.RecommendEmployeeId;

                leave.SupervisorEmployeeIdInCaseOfSelection = entity.SupervisorEmployeeIdInCaseOfSelection;

                // if no Review then set Approval emp in Review also
                if (CommonManager.Setting.HideReviewInLeaveRequest != null && CommonManager.Setting.HideReviewInLeaveRequest.Value)
                    leave.RecommendEmployeeId = leave.SupervisorEmployeeIdInCaseOfSelection;
            }

            // if has Recommender and Approval drop down and Recommender selected is the employee himself then save the leave in Recommend status
            //  instead of Request
            if (leave.RecommendEmployeeId != null && leave.RecommendEmployeeId == SessionManager.CurrentLoggedInEmployeeId
                && leave.RecommendEmployeeId != 0)
                leave.Status = (int)LeaveRequestStatusEnum.Recommended;

            bool email = true;
            LeaveAttendanceManager.SaveUpdateLeaveRequest(leave, ref email);

            if (email)
            {
                NotifyThroughMessageAndEmail(leave.LeaveRequestId, leave.IsHour,
                    leave.DaysOrHours.ToString(), leave.Reason, leave.FromDateEng.ToString(), leave.ToDateEng.ToString(),
                    ((LeaveRequestStatusEnum)leave.Status).ToString(),
                    leave.SupervisorEmployeeIdInCaseOfSelection
                    , leave.RecommendEmployeeId, null);
            }

            return status;
        }

        public static void NotifyThroughMessageAndEmail(int leaveRequestId, Boolean? IsHour, string DaysOrHour,
           string Reason, string fromDate, string toDate, string status, int? applyToEmployeeId, int? reviewToEmployeeId, int? statusForMessage)
        {

            if (HttpContext.Current.User.Identity.IsAuthenticated == false)
                return;

            DAL.LeaveRequest dbLeaveRequest = LeaveAttendanceManager.GetLeaveRequestById(leaveRequestId);

            int currentEmployeeId = 0; string empUserName = "";
            UserManager.SetCurrentEmployeeAndUser(ref currentEmployeeId, ref empUserName);

            int employeeId = dbLeaveRequest.EmployeeId;
            //string loggedInUserName = SessionManager.UserName;
            string subject = "Leave Request From";
            string body = string.Empty;
            UUser recipient = UserManager.GetUserByEmployee(employeeId);
            string employeeName = EmployeeManager.GetEmployeeName(employeeId);
            //string employeeEmail = EmployeeManager.GetEmployeeEmail(employeeId);
            // if user not created then no mail and message

            //DAL.PayrollMessage msg = new DAL.PayrollMessage();

            // Generate the Approval Url the leave request
            string url = UrlHelper.GetRootUrl();
            if (url[url.Length - 1] != '/')
                url += "/";

            url +=
               string.Format("Employee/LeaveApproval.aspx#{0}", leaveRequestId);


            #region Send Message To Notification Users

            if ((bool)(IsHour == null ? false : IsHour)
                && CommonManager.CalculationConstant.CompanyHasHourlyLeave.Value)
            {
                //New Leave Request from  {0} <br /><br />
                //<strong>On:</strong> {1} .<br /><br />
                //<strong>For:</strong> {2} Hours <br /><br />
                //<strong>Reason:</strong> {3}<br /> <br />
                //<a href='LeaveApproval.aspx'>Click here to navigate approval page</a>

                body = FileHelper.GetHourlyLeaveRequest();
                body = string.Format(body, employeeName, DateTime.Parse(fromDate).ToShortDateString(), DaysOrHour, Reason
                    , dbLeaveRequest.StartTime + " - " + dbLeaveRequest.EndTime);
                body = body.Replace("#Url#", url);
            }
            else
            {
                ////{0}'s leave<br /><br />
                ////<strong>From:</strong> {1} <strong>To</strong> {2}<br /><br />
                ////has been {3} by {4}.<br /><br />
                ////<strong>Comment:</strong>{5}
                body = FileHelper.GetLeaveApproveNotification();

                EmailContent dbMailContent = CommonManager.GetMailContent((int)EmailContentType.DayTypeLeaveRequest);

                if (dbLeaveRequest.IsCancelRequest != null && dbLeaveRequest.IsCancelRequest.Value
                    && CommonManager.GetMailContent((int)EmailContentType.CancelLeaveRequest) != null)
                    dbMailContent = CommonManager.GetMailContent((int)EmailContentType.CancelLeaveRequest);

                if (dbMailContent != null)
                {
                    subject = dbMailContent.Subject;
                    body = dbMailContent.Body;
                }

                //name is appended at the end
                subject = subject.Replace("#RequesterName#", employeeName).Replace("#LeaveType#", LeaveAttendanceManager.GetLeaveNameForLeaveRequest(leaveRequestId));

                if (dbLeaveRequest.IsHalfDayLeave != null && dbLeaveRequest.IsHalfDayLeave.Value && !string.IsNullOrEmpty(dbLeaveRequest.HalfDayType))
                {
                    DaysOrHour += ", " + dbLeaveRequest.HalfDayType;
                }

                body = body.Replace("#RequesterName#", employeeName)
                            .Replace("#StartDate#", DateTime.Parse(fromDate).ToShortDateString())
                            .Replace("#EndDate#", DateTime.Parse(toDate).ToShortDateString())
                            .Replace("#Days#", DaysOrHour)
                            .Replace("#Reason#", Reason)
                            .Replace("#LeaveType#", LeaveAttendanceManager.GetLeaveNameForLeaveRequest(leaveRequestId))
                            .Replace("#Url#", url)
                            .Replace("#Substitute#", GetSubsbituteName(dbLeaveRequest.SubstituteEmployeeId))
                            .Replace("{Organizations Name}", SessionManager.CurrentCompany.Name);

            }

            //msg = new DAL.PayrollMessage();
            //msg.Subject = subject;
            //msg.Body = body;
            //msg.SendBy = empUserName;
            //msg.ReceivedDate = BLL.BaseBiz.GetCurrentDateAndTime().ToShortDateString();
            //msg.ReceivedDateEng = BLL.BaseBiz.GetCurrentDateAndTime();
            //msg.IsRead = false;

            if (statusForMessage == null)
                statusForMessage = dbLeaveRequest.Status;

            if (statusForMessage == (int)LeaveRequestStatusEnum.Recommended)
            {
                string recommendedBy = "";
                if (dbLeaveRequest.RecommendedBy != null)
                {
                    recommendedBy = new EmployeeManager().GetById(dbLeaveRequest.RecommendedBy.Value).Name;
                }
                body = body.Replace("#RecommendedBy#", "Recommended by : " + recommendedBy);
                //msg.Body = body;

                LeaveRequestManager.SendRecommendedLeaveRelatedMessageAndMail(dbLeaveRequest.EmployeeId, body, subject, DAL.BaseBiz.PayrollDataContext,
               empUserName, "", applyToEmployeeId, leaveRequestId);
            }
            // else for Request
            else
            {
                body = body.Replace("#RecommendedBy#", "");
                //msg.Body = body;

                LeaveRequestManager.SendLeaveRelatedMessageAndMail(dbLeaveRequest.EmployeeId, body, subject, DAL.BaseBiz.PayrollDataContext,
                    empUserName, "", applyToEmployeeId, reviewToEmployeeId, leaveRequestId);
            }
            #endregion
        }

        public static string GetSubsbituteName(int? subEmpId)
        {
            if (subEmpId == null || subEmpId == -1 || subEmpId == 0)
                return "";
            return EmployeeManager.GetEmployeeName(subEmpId.Value);
        }


    }
}
