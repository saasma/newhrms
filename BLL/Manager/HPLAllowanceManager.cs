﻿using System.Collections.Generic;
using System.Linq;
using DAL;
using Utils.Calendar;
using System;
using System.Xml.Linq;
using System.Web.UI.WebControls;

namespace BLL.Manager
{
    public class HPLAllowanceManager : BaseBiz
    {

        #region "Shift Allowance Setting"

        public static bool IsShiftSettingAlreadyExists(string shiftName,int locationId,int? currentShiftAllowanceId)
        {
            return PayrollDataContext.ShiftAllowances
                .Any(o => o.CompanyId == SessionManager.CurrentCompanyId
                    && o.ShiftAllowanceName.Equals(shiftName)
                    && o.LocationId == locationId
                    && (currentShiftAllowanceId == null || o.ShiftAllowanceId != currentShiftAllowanceId));
        }

        public static bool UpdateShiftSetting(ShiftAllowance entity)
        {
            ShiftAllowance dbEntity = PayrollDataContext.ShiftAllowances
                .Where(o => o.ShiftAllowanceId == entity.ShiftAllowanceId)
                .SingleOrDefault();

            if (dbEntity == null)
                return false;


            dbEntity.ShiftAllowanceName = entity.ShiftAllowanceName;
            dbEntity.LocationId = entity.LocationId;
            dbEntity.RatePercent = entity.RatePercent;

            UpdateChangeSet();
            return true;
        }

        //public static bool IsSettingUsedForEmployee(int overtimeSettingId)
        //{

        //    OOvertimePay overtime = PayrollDataContext.OOvertimePays
        //        .Where(s => s.OvertimeSettingId == overtimeSettingId)
        //        .Take(1).SingleOrDefault();

        //    if (overtime == null)
        //        return false;


        //    return true;

        //}

        //public static bool IsSettingDeletable(int overtimeSettingId)
        //{
        //    OOvertimePay overtime = PayrollDataContext.OOvertimePays
        //        .Where(s => s.OvertimeSettingId == overtimeSettingId)
        //        .Take(1).SingleOrDefault();

        //    if (overtime == null)
        //        return true;

        //    //now check if the associated payroll is saved finally then allow to delete it
        //    //else don't allow

        //    if (CalculationManager.IsAllEmployeeSavedFinally(overtime.PayrollPeriodId.Value, false))
        //        return true;

        //    return false;
        //}

        public static bool DeleteSetting(int shiftAllowanceId)
        {
            ShiftAllowance dbEntity = PayrollDataContext.ShiftAllowances
               .Where(o => o.ShiftAllowanceId == shiftAllowanceId)
               .SingleOrDefault();


            if (dbEntity == null)
                return false;

            PayrollDataContext.ShiftAllowances.DeleteOnSubmit(dbEntity);

            return DeleteChangeSet();
        }

        public static ShiftAllowance GetSettingById(int shiftAllowanceId)
        {
            return
                PayrollDataContext.ShiftAllowances
                .Where(o => o.ShiftAllowanceId == shiftAllowanceId)
                .SingleOrDefault();
        }


        public static bool SaveSetting(ShiftAllowance setting)
        {
            setting.CompanyId = SessionManager.CurrentCompanyId;
            PayrollDataContext.ShiftAllowances.InsertOnSubmit(setting);
            return SaveChangeSet();
        }

        public static List<ShiftAllowance> GetSettings()
        {
            return PayrollDataContext.ShiftAllowances
               .Where(o => o.CompanyId == SessionManager.CurrentCompanyId)
               .OrderBy(o => o.ShiftAllowanceName)               
               .ToList();
        }

        #endregion


        public static List<HPL_GetEmployeeForAllowanceResult> HPL_GetEmployeeForAllowance(
            int payrollPeriodId)
        {
            return
                PayrollDataContext.HPL_GetEmployeeForAllowance(SessionManager.CurrentCompanyId, payrollPeriodId)
                .ToList();
        }

        public static void HPL_SaveAllowance(string xml, int payrollPeriodId)
        {
            PayrollDataContext.HPL_SaveAllowance(payrollPeriodId,
                XElement.Parse(xml));
        }



        #region "Allowance setting"
        public static ListItem[] GetLocations()
        {
            ListItem item1 = new ListItem("Palati", "Days_At_Palati_Rate");
            ListItem item2 = new ListItem("Kirne", "Days_At_Kirne_Rate");
            return new ListItem[] { item1, item2 };
        }
        public static double GetLocationRate(string location)
        {
            if (location == "Days_At_Palati_Rate")
                return
                    PayrollDataContext.HPLAllowanceRates.SingleOrDefault().Days_At_Palati_Rate.Value;
            else
                return
                    PayrollDataContext.HPLAllowanceRates.SingleOrDefault().Days_At_Kirne_Rate.Value;

        }

        public static void GenerateHPLRetrospectAllowance()
        {

            PayrollPeriod p = CommonManager.GetLastPayrollPeriod();

            PayrollDataContext.Generate_HPL_GetAllowanceIncome(p.PayrollPeriodId, SessionManager.CurrentCompanyId);
        }
        //public static List<PIncome> GetHPLAllowanceList()
        //{
        //    // for HPL add basic depedent Allowance also
        //    HPLAllowanceRate allowance = HPLAllowanceManager.GetAllowanceRate();
        //    List<PIncome> otherIncomes = new List<PIncome>();

        //    otherIncomes.Add(new PIncome
        //    {
        //        Type = (int)CalculationColumnType.IncomeSiteAllowance,
        //        IncomeId = (int)CalculationColumnType.IncomeSiteAllowance,
        //        Title = allowance.PalatiHeader
        //    });

        //    //otherIncomes.Add(new PIncome
        //    //{
        //    //    Type = (int)CalculationColumnType.IncomeOnCallAllowance,
        //    //    IncomeId = (int)CalculationColumnType.IncomeOnCallAllowance,
        //    //    Title = allowance.OnCallHeader
        //    //});

        //    otherIncomes.Add(new PIncome
        //    {
        //        Type = (int)CalculationColumnType.IncomeShiftAfternoon,
        //        IncomeId = (int)CalculationColumnType.IncomeShiftAfternoon,
        //        Title = allowance.ShiftAfternoonHeader
        //    });
        //    otherIncomes.Add(new PIncome
        //    {
        //        Type = (int)CalculationColumnType.IncomeShiftNight,
        //        IncomeId = (int)CalculationColumnType.IncomeShiftNight,
        //        Title = allowance.ShiftNightHeader
        //    });
        //    otherIncomes.Add(new PIncome
        //    {
        //        Type = (int)CalculationColumnType.IncomeShiftMorning,
        //        IncomeId = (int)CalculationColumnType.IncomeShiftMorning,
        //        Title = allowance.SplitShiftMorningHeader
        //    });

        //    otherIncomes.Add(new PIncome
        //    {
        //        Type = (int)CalculationColumnType.IncomeShiftEvening,
        //        IncomeId = (int)CalculationColumnType.IncomeShiftEvening,
        //        Title = allowance.SplitShiftEveningHeader
        //    });
        //    otherIncomes.Add(new PIncome
        //    {
        //        Type = (int)CalculationColumnType.IncomeOvertime150,
        //        IncomeId = (int)CalculationColumnType.IncomeOvertime150,
        //        Title = allowance.OT150Header
        //    });
        //    otherIncomes.Add(new PIncome
        //    {
        //        Type = (int)CalculationColumnType.IncomeOvertime200,
        //        IncomeId = (int)CalculationColumnType.IncomeOvertime200,
        //        Title = allowance.OT200Header
        //    });
        //    otherIncomes.Add(new PIncome
        //    {
        //        Type = (int)CalculationColumnType.IncomeOvertime300,
        //        IncomeId = (int)CalculationColumnType.IncomeOvertime300,
        //        Title = allowance.OT300Header
        //    });
        //    otherIncomes.Add(new PIncome
        //    {
        //        Type = (int)CalculationColumnType.IncomeOvertimePublicHoliday,
        //        IncomeId = (int)CalculationColumnType.IncomeOvertimePublicHoliday,
        //        Title = allowance.PublicHolidayHeader
        //    });

        //    return otherIncomes;
        //}

        //public static string GetAllowanceName(CalculationColumnType type)
        //{
        //    // for HPL add basic depedent Allowance also
        //    HPLAllowanceRate allowance = HPLAllowanceManager.GetAllowanceRate();


        //    if (type == CalculationColumnType.IncomeSiteAllowance)
        //        return allowance.PalatiHeader;

        //    if (type == CalculationColumnType.IncomeOnCallAllowance)
        //        return allowance.OnCallHeader;


        //    if (type == CalculationColumnType.IncomeShiftAfternoon)
        //        return allowance.ShiftAfternoonHeader;

        //     if (type == CalculationColumnType.IncomeShiftNight)
        //       return allowance.ShiftNightHeader;

        //     if (type == CalculationColumnType.IncomeShiftMorning)

        //         return allowance.SplitShiftMorningHeader;


        //     if (type == CalculationColumnType.IncomeShiftEvening)
        //         return allowance.SplitShiftEveningHeader;

        //     if (type == CalculationColumnType.IncomeOvertime150)
        //         return allowance.OT150Header;


        //   if (type == CalculationColumnType.IncomeOvertime200)
        //       return allowance.OT200Header
        //           ;
        //   if (type == CalculationColumnType.IncomeOvertime300)

        //       return allowance.OT300Header;


        //   if (type == CalculationColumnType.IncomeOvertimePublicHoliday)
        //       return allowance.PublicHolidayHeader;


        //    return "";
        //}

        public static void SaveSiteAllowanceRate(string location,double rateValue)
        {
            HPLAllowanceRate allowance = PayrollDataContext.HPLAllowanceRates.SingleOrDefault();


            if (location == "Days_At_Palati_Rate")
                allowance.Days_At_Palati_Rate = rateValue;
            else
                allowance.Days_At_Kirne_Rate = rateValue;

            UpdateChangeSet();
        }

        public static void SaveOnCallAllowanceRate(double rateValue)
        {
            HPLAllowanceRate allowance = PayrollDataContext.HPLAllowanceRates.SingleOrDefault();


            allowance.On_Call_Rate = rateValue;

            UpdateChangeSet();
        }

        public static double GetOnCallAllowance()
        {
            return PayrollDataContext.HPLAllowanceRates.SingleOrDefault().On_Call_Rate.Value;
        }

        public static HPLAllowanceRate GetAllowanceRate()
        {
            HPLAllowanceRate allowance =
               PayrollDataContext.HPLAllowanceRates.SingleOrDefault();
            return allowance;
        }

        public static void HideOtherAllowance(bool hide)
        {
            HPLAllowanceRate allowance =
               PayrollDataContext.HPLAllowanceRates.SingleOrDefault();
            allowance.HideOtherAllowance = hide;
            PayrollDataContext.SubmitChanges();
        }
        public static void UpdateAllowanceRate(string key,double value,string headerName)
        {
            HPLAllowanceRate allowance =
                PayrollDataContext.HPLAllowanceRates.SingleOrDefault();

            if (allowance != null)
            {
                switch (key)
                {
                    case "Days_At_Palati":
                        allowance.Days_At_Palati_Rate = value;
                        allowance.PalatiHeader = headerName;
                        break;
                    case "On_Call_Rate":
                        allowance.On_Call_Rate = value;
                        allowance.OnCallHeader = headerName;
                        break;
                    case "Shift_Afternoon_Rate":
                        allowance.Shift_Afternoon_Rate = value;
                        allowance.ShiftAfternoonHeader = headerName;
                        break;
                    case "Shift_Night_Rate":
                        allowance.Shift_Night_Rate = value;
                        allowance.ShiftNightHeader = headerName;
                        break;
                    case "Split_Shift_Morning_Rate":
                        allowance.Split_Shift_Morning_Rate = value;
                        allowance.SplitShiftMorningHeader = headerName;
                        break;
                    case "Split_Shift_Evening_Rate":
                        allowance.Split_Shift_Evening_Rate = value;
                        allowance.SplitShiftEveningHeader = headerName;
                        break;
                    case "OT_150_Rate":
                        allowance.OT_150_Rate = value;
                        allowance.OT150Header = headerName;
                        break;
                    case "OT_200_Rate":
                        allowance.OT_200_Rate = value;
                        allowance.OT200Header = headerName;
                        break;
                    case "OT_300_Rate":
                        allowance.OT_300_Rate = value;
                        allowance.OT300Header = headerName;
                        break;
                    case "Public_Holiday_Rate":
                        allowance.Public_Holiday_Rate = value;
                        allowance.PublicHolidayHeader = headerName;
                        break;
                }

               
              
              
               
              
               
               
                
               
               
            }

            UpdateChangeSet();
        }
       

        #endregion
    }
}
