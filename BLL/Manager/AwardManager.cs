﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Transactions;
using DAL;
using Utils.Calendar;
using System;
using System.Web.UI.WebControls;
using Utils;
using System.Web;
using Utils.Helper;
using Microsoft.SqlServer.Management.Smo;
using Microsoft.SqlServer.Management.Common;
using System.IO;
using System.IO.Compression;
using BLL.BO;
using System.Text;

namespace BLL.Manager
{
    public class AwardManager : BaseBiz
    {
        public static List<PIncome> GetAllIncomeHead()
        {
            return PayrollDataContext.PIncomes.OrderBy(x => x.Abbreviation).ToList();
        }

        public static PIncome GetPIncomeById(int incomeId)
        {
            return PayrollDataContext.PIncomes.SingleOrDefault(x => x.IncomeId == incomeId);
        }

        public static Status SaveUpdateEmployeeCashAwardType(EmployeeCashAwardType obj)
        {
            Status status = new Status();

            if (PayrollDataContext.EmployeeCashAwardTypes.Any(x => x.Name == obj.Name && x.CashAwardTypeId != obj.CashAwardTypeId))
            {
                status.ErrorMessage = "Award name already exists.";
                status.IsSuccess = false;
                return status;
            }

            if (obj.CashAwardTypeId.Equals(0))
            {
                PayrollDataContext.EmployeeCashAwardTypes.InsertOnSubmit(obj);
                PayrollDataContext.SubmitChanges();
                status.IsSuccess = true;
            }
            else
            {
                EmployeeCashAwardType dbObj = PayrollDataContext.EmployeeCashAwardTypes.SingleOrDefault(x => x.CashAwardTypeId == obj.CashAwardTypeId);
                dbObj.Name = obj.Name;
                dbObj.IncomeId = obj.IncomeId;
                PayrollDataContext.SubmitChanges();
                status.IsSuccess = true;
            }

            return status;
        }

        public static Status SaveUpdateRatingText(TrainingRatingText obj)
        {
            Status status = new Status();

            if (PayrollDataContext.TrainingRatingTexts.Any(x => x.RatingName == obj.RatingName && x.RatingID != obj.RatingID))
            {
                status.ErrorMessage = "Rating name already exists.";
                status.IsSuccess = false;
                return status;
            }

            if (obj.RatingID.Equals(0))
            {
                PayrollDataContext.TrainingRatingTexts.InsertOnSubmit(obj);
                PayrollDataContext.SubmitChanges();
                status.IsSuccess = true;
            }
            else
            {
                TrainingRatingText dbObj = PayrollDataContext.TrainingRatingTexts.SingleOrDefault(x => x.RatingID == obj.RatingID);
                dbObj.RatingName = obj.RatingName;
                PayrollDataContext.SubmitChanges();
                status.IsSuccess = true;
            }

            return status;
        }

        public static List<EmployeeCashAwardType> GetAllEmployeeCashAwardType()
        {
            List<EmployeeCashAwardType> list = PayrollDataContext.EmployeeCashAwardTypes.OrderBy(x => x.Name).ToList();

            foreach (var item in list)
            {
                item.IncomeName = AwardManager.GetPIncomeById(item.IncomeId.Value).Title;
            }

            return list;
        }

        public static EmployeeCashAwardType GetEmployeeCashAwardTypeById(int cashAwardTypeId)
        {
            return PayrollDataContext.EmployeeCashAwardTypes.SingleOrDefault(x => x.CashAwardTypeId == cashAwardTypeId);
        }


        

        public static Status DeleteEmployeeCashAwardType(int cashAwardTypeId)
        {
            Status status = new Status();

            if (PayrollDataContext.EmployeeCashAwards.Any(x => x.CashAwardTypeId == cashAwardTypeId))
            {
                status.ErrorMessage = "Award is in use, cannot be deleted.";
                status.IsSuccess = false;
                return status;
            }

            EmployeeCashAwardType obj = PayrollDataContext.EmployeeCashAwardTypes.SingleOrDefault(x => x.CashAwardTypeId == cashAwardTypeId);
            if (obj != null)
            {
                PayrollDataContext.EmployeeCashAwardTypes.DeleteOnSubmit(obj);
                PayrollDataContext.SubmitChanges();
                status.IsSuccess = true;
            }

            return status;
        }

        public static Status SaveUpdateEmployeeAward(List<EmployeeCashAward> list, ref int importCount)
        {
            Status status = new Status();
            importCount = 0;

            foreach (var item in list)
            {
                importCount++;
                if (PayrollDataContext.EmployeeCashAwards.Any(x => (x.EmployeeId == item.EmployeeId) && (x.CashAwardTypeId == item.CashAwardTypeId) && (x.Month == item.Month) && (x.Year == item.Year)))
                {
                    EmployeeCashAward dbObj = PayrollDataContext.EmployeeCashAwards.SingleOrDefault(x => (x.EmployeeId == item.EmployeeId) && (x.CashAwardTypeId == item.CashAwardTypeId) && (x.Month == item.Month) && (x.Year == item.Year));
                    dbObj.CalculationType = item.CalculationType;
                    dbObj.AmountOrRate = item.AmountOrRate;
                    dbObj.Month = item.Month;
                    dbObj.Year = item.Year;
                    dbObj.Notes = item.Notes;
                    dbObj.CreatedBy = item.CreatedBy;
                    dbObj.CreatedOn = item.CreatedOn;
                    dbObj.Date = item.Date;
                    dbObj.DateEng = item.DateEng;
                }
                else
                {
                    PayrollDataContext.EmployeeCashAwards.InsertOnSubmit(item);
                }
            }

            PayrollDataContext.SubmitChanges();
            status.IsSuccess = true;
            return status;
        }

        public static EmployeeCashAward GetEmployeeCashAwardById(int employeeId, int cashAwardTypeId, int month, int year)
        {
            return PayrollDataContext.EmployeeCashAwards.SingleOrDefault(x => (x.EmployeeId == employeeId) && (x.CashAwardTypeId == cashAwardTypeId) && (x.Month == month) && (x.Year == year));
        }

        public static Status DeleteEmployeeCashAward(int employeeId, int cashAwardTypeId, int month, int year)
        {
            Status status = new Status();

            EmployeeCashAward obj = AwardManager.GetEmployeeCashAwardById(employeeId, cashAwardTypeId, month, year);
            if (obj != null)
            {
                PayrollDataContext.EmployeeCashAwards.DeleteOnSubmit(obj);
                PayrollDataContext.SubmitChanges();
                status.IsSuccess = true;
            }

            return status;
        }

        public static List<EmployeeCashAward> GetEmployeeCashAwardList()
        {
            List<EmployeeCashAward> list = PayrollDataContext.EmployeeCashAwards.OrderBy(x => x.EmployeeId).ToList();
            int i = 0;
            foreach (var item in list)
            {
                i++;
                item.SN = i.ToString();
                item.EmployeeName = EmployeeManager.GetEmployeeById(item.EmployeeId).Name;

                EmployeeCashAwardType obj = AwardManager.GetEmployeeCashAwardTypeById(item.CashAwardTypeId);
                item.AwardName = obj.Name;
               
                if (item.CalculationType == (int)AwardCalculationEnum.MonthsBasicSalary)
                    item.Description = decimal.Parse(item.AmountOrRate.ToString()).ToString("N2") + " Months Basic Salary";
                else if (item.CalculationType == (int)AwardCalculationEnum.PercentageOfBasicSalary)
                    item.Description = decimal.Parse(item.AmountOrRate.ToString()).ToString("N2") + " % of Basic Salary";
                else if (item.CalculationType == (int)AwardCalculationEnum.FixedAmount)
                    item.Description = "Fixed Amount of " + decimal.Parse(item.AmountOrRate.ToString()).ToString("N2");
            }
            return list;
        }


    }
}
