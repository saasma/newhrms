﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using DAL;
using Utils;
using System;
using Utils.Calendar;

namespace BLL.Manager
{

    public class VouccherManager : BaseBiz
    {




        public static void SaveHead(VoucherHeadGroup entity)
        {
            PayrollDataContext.VoucherHeadGroups.InsertOnSubmit(entity);
            PayrollDataContext.SubmitChanges();
        }
        public static bool UpdateHead(VoucherHeadGroup entity)
        {
            VoucherHeadGroup dbEntity = PayrollDataContext.VoucherHeadGroups.SingleOrDefault(c => c.Type==entity.Type && c.SourceId==entity.SourceId);
            if (dbEntity == null)
            {
                PayrollDataContext.VoucherHeadGroups.InsertOnSubmit(entity);
                PayrollDataContext.SubmitChanges();
                return true;
            }

            dbEntity.Type = entity.Type;
            dbEntity.SourceId = entity.SourceId;
            dbEntity.VoucherGroupID = entity.VoucherGroupID;
            dbEntity.Name = entity.Name;

            PayrollDataContext.SubmitChanges();
            return true;

        }

        public static VoucherGroupCategory GetCategoryById(int categoryId)
        {
            return
                PayrollDataContext.VoucherGroupCategories.FirstOrDefault(x => x.CategoryID == categoryId);
        }
        public static  bool SaveVoucherGorup(VoucherGroup entity)
        {
            if (CommonManager.Setting.AccountingVoucherEnum != null && CommonManager.Setting.AccountingVoucherEnum == (int)AccountingVoucherTypeEnum.GLOBUS)
            {
            }
            else if (PayrollDataContext.VoucherGroups.Any(x => x.AccountGroupName.ToLower() == entity.AccountGroupName.ToLower()))
            {
                return false;
            }

            PayrollDataContext.VoucherGroups.InsertOnSubmit(entity);
            PayrollDataContext.SubmitChanges();

            return true;
        }
        public static bool UpdateVoucherGroup(VoucherGroup entity)
        {

            if (CommonManager.Setting.AccountingVoucherEnum != null && CommonManager.Setting.AccountingVoucherEnum == (int)AccountingVoucherTypeEnum.GLOBUS)
            {
            }
            else if (PayrollDataContext.VoucherGroups.Any(x =>x.VoucherGroupID != entity.VoucherGroupID
                 && x.AccountGroupName.ToLower() == entity.AccountGroupName.ToLower()))
            {
                return false;
            }


            VoucherGroup dbEntity = PayrollDataContext.VoucherGroups.SingleOrDefault(c => c.VoucherGroupID == entity.VoucherGroupID);
            if (dbEntity == null)
                return false;

            dbEntity.Code = entity.Code;
            dbEntity.AccountGroupName = entity.AccountGroupName;
            dbEntity.Type = entity.Type;
            dbEntity.Group = entity.Group;
            dbEntity.TransactionCode = entity.TransactionCode;
            dbEntity.DeductFromEmpAccount = entity.DeductFromEmpAccount;
            dbEntity.CategoryRef_ID = entity.CategoryRef_ID;
            dbEntity.Desc = entity.Desc;

            PayrollDataContext.SubmitChanges();
            return true;

        }

        //End Employee Fam


        public  static VoucherGroup GetVoucher(int id)
        {
            return PayrollDataContext.VoucherGroups.SingleOrDefault(c => c.VoucherGroupID == id);
        }
        public  static VoucherHeadGroup GetVoucherHead(int type,int sourceId)
        {
            return PayrollDataContext.VoucherHeadGroups.SingleOrDefault(c => c.Type==type && c.SourceId==sourceId);
        }
        public static List<VoucherGroup> GetVoucherGroupList()
        {

            //if (CommonManager.Setting.AccountingVoucherEnum != null && CommonManager.Setting.AccountingVoucherEnum == (int)AccountingVoucherTypeEnum.GLOBUS)
            //{
            //    return
            //    PayrollDataContext.VoucherGroups.OrderBy(x => x.Code).ToList();
            //}

            return
                PayrollDataContext.VoucherGroups.OrderBy(x => x.AccountGroupName).ToList();
        }
        public static List<VoucherGroupCategory> GetVoucherGroupCategoryList()
        {
            return
                PayrollDataContext.VoucherGroupCategories.ToList();
        }
        public static List<CalcGetHeaderListResult> GetHeadList()
        {
            List<VoucherHeadGroup> list =
                PayrollDataContext.VoucherHeadGroups.ToList();


             
            
            List<CalcGetHeaderListResult> incomeDeductionList = CalculationManager.GetHeaderListForVoucher();

            incomeDeductionList = CalculationValue.SortHeaders(incomeDeductionList, PayManager.GetIncomeOrderValues(), PayManager.GetDeductionOrderValues());


            foreach (var item in list)
            {
                CalcGetHeaderListResult header = incomeDeductionList.FirstOrDefault(x =>
                    x.Type == item.Type && x.SourceId == item.SourceId);
                if (header != null)
                {
                    header.VoucherGroupID = item.VoucherGroupID == null ? 0 : item.VoucherGroupID.Value;
                    header.ID = item.ID;
                }
            }

            List<CalcGetHeaderListResult> remHeaderList = new List<CalcGetHeaderListResult>();

            int sn = 0;
            foreach (CalcGetHeaderListResult header in incomeDeductionList)
            {
                header.SN = (++sn);
                //if (!list.Any(x => x.Type == header.Type && x.SourceId == header.SourceId))
                //{
                //    list.Add
                //        (
                //        new VoucherHeadGroup
                //        {
                //            ID = 0,
                //            Type = header.Type,
                //            SourceId = header.SourceId,
                //            Name = header.HeaderName
                //            ,
                //            CalculationText = header.CalculationText
                //            ,
                //            SN = ++sn
                //        }
                //        );
                //}
                //else
                //{
                //    VoucherHeadGroup head = list.FirstOrDefault(x => x.Type == header.Type && x.SourceId == header.SourceId);
                //    if (head != null)
                //    {
                //        head.SN = ++sn;
                //        head.CalculationText = header.CalculationText;
                //    }
                //}
            }
            return incomeDeductionList;

            
        }
        //End Employee Previous Employment

        public static List<CalcGetHeaderListResult> GetTaxCertificateIncomeList()
        {
            List<YearlyIncome> list =
                PayrollDataContext.YearlyIncomes.ToList();




            List<CalcGetHeaderListResult> incomeDeductionList = CalculationManager.GetHeaderListForTaxCertificate();

            incomeDeductionList = CalculationValue.SortHeaders(incomeDeductionList, PayManager.GetIncomeOrderValues(), PayManager.GetDeductionOrderValues());


            //foreach (var item in list)
            //{
            //    CalcGetHeaderListResult header = incomeDeductionList.FirstOrDefault(x =>
            //        x.Type == item.Type && x.SourceId == item.SourceID);
            //    if (header != null)
            //    {
            //        header.VoucherGroupID = item.VoucherGroupID == null ? 0 : item.VoucherGroupID.Value;
            //        header.ID = item.ID;
            //    }
            //}

            List<CalcGetHeaderListResult> remHeaderList = new List<CalcGetHeaderListResult>();

            int sn = 0;
            foreach (CalcGetHeaderListResult header in incomeDeductionList)
            {
                header.SN = (++sn);
               

                YearlyIncome entity = list.FirstOrDefault(x => x.Type == header.Type
                    && x.SourceID == header.SourceId);

                //header.SourceId = 0;

                if (entity != null)
                {
                    TaxCertificateEnum taxEnum = (TaxCertificateEnum)entity.TypeEnum;

                    header.ID = entity.TypeEnum;

                  
                }

                header.Value = header.ID.ToString();

            }
            return incomeDeductionList;


        }
    }


}
