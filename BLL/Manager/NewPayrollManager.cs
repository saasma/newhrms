﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using DAL;
using Utils;
using System.Xml.Linq;
using Utils.Calendar;
using BLL.Entity;
using BLL.BO;
using Ext.Net;
using System.IO;
using System.Web;
namespace BLL.Manager
{
    public class NewPayrollManager : BaseBiz
    {
        
        #region "Charity"
        public static Charity GetCharity(int charityId)
        {
            return PayrollDataContext.Charities.FirstOrDefault(x => x.CharityID == charityId);
        }
        public static List<Charity> GetCharityList()
        {
            List<Charity> list = PayrollDataContext.Charities.ToList();

            foreach (Charity item in list)
            {
                item.Name = new EmployeeManager().GetById(item.EmployeeId).Name;
            }

            return list;
        }
        public static Status InsertUpdateCharity(Charity entity, bool isInsert)
        {
            Status status = new Status();

            entity.ModifiedBy = SessionManager.CurrentLoggedInUserID;
            entity.ModifiedOn = GetCurrentDateAndTime();

           


            if (isInsert)
            {

                entity.CreatedBy = entity.ModifiedBy = SessionManager.CurrentLoggedInUserID;
                entity.CreatedOn = entity.ModifiedOn = GetCurrentDateAndTime();


                PayrollDataContext.Charities.InsertOnSubmit(entity);
                SaveChangeSet();
            }
            else
            {
                Charity dbEntity = PayrollDataContext.Charities.SingleOrDefault(x => x.CharityID == entity.CharityID);
                if (dbEntity != null)
                {
                    CopyObject<Charity>(entity, ref dbEntity);
                    UpdateChangeSet();
                }
                else
                {
                    status.ErrorMessage = "Charity does not exists.";
                }

            }
            return status;
        }


        #endregion

        public static BLevel GetActingLevelInTodayDate(int employeeId)
        { 

            DateTime date = new DateTime(DateTime.Now.Year,DateTime.Now.Month,DateTime.Now.Day);

            ActingEmployee acting = PayrollDataContext.ActingEmployees
                .Where(x => x.EmployeeId == employeeId &&
                    (
                        (x.ApplicableTillEng == null && date >= x.ApplicableFromEng)
                        ||
                        (x.ApplicableFromEng != null && date >= x.ApplicableFromEng && date <= x.ApplicableTillEng)
                    )
                )
                .OrderByDescending(x => x.ApplicableFromEng).FirstOrDefault();

            if (acting != null)
            {
                return GetLevelById(acting.ToLevelId.Value);
            }

            return null;
        }

        public static GradeRewardEmployee GetGradeReward(int rewardId)
        {
            return PayrollDataContext.GradeRewardEmployees.SingleOrDefault(x => x.RewardID == rewardId);
        }
        public static GradeAdditionlBasic GetAdditionalBasic(int rewardId)
        {
            return PayrollDataContext.GradeAdditionlBasics.SingleOrDefault(x => x.GradeAdditionalBasicID == rewardId);
        }
        public static string InsertUpdateGradeRewardDetail(List<GradeRewardEmployee> list, ref int rowImportedCount)
        {
            //int rowImportedCount = 0;

            int medicalTaxId = 0;
            foreach (GradeRewardEmployee entity in list)
            {
                rowImportedCount += 1;
             
                entity.CreatedBy = entity.ModifiedBy = SessionManager.CurrentLoggedInUserID;
                entity.CreatedOn = entity.ModifiedOn = GetCurrentDateAndTime();




                PayrollDataContext.GradeRewardEmployees.InsertOnSubmit(entity); 
               
            }

            PayrollDataContext.SubmitChanges();

            // errro validation like msg
            return "";
        }
        public static string InsertUpdateAdditionBasicDetail(List<GradeAdditionlBasic> list, ref int rowImportedCount)
        {
            //int rowImportedCount = 0;

            int medicalTaxId = 0;
            foreach (GradeAdditionlBasic entity in list)
            {
                rowImportedCount += 1;

                entity.CreatedBy = entity.ModifiedBy = SessionManager.CurrentLoggedInUserID;
                entity.CreatedOn = entity.ModifiedOn = GetCurrentDateAndTime();


                bool isBasicIncome = new PayManager().GetBasicIncome(SessionManager.CurrentCompanyId).IncomeId == entity.IncomeId;
                GradeAdditionlBasic dbAddtionIncome =
                    PayrollDataContext.GradeAdditionlBasics.FirstOrDefault(
                    x => x.EmployeeId == entity.EmployeeId && x.LevelId == entity.LevelId
                       && ((x.IncomeId == null && isBasicIncome) || (x.IncomeId != null && x.IncomeId == entity.IncomeId)));

                if (dbAddtionIncome != null)
                {
                    if (dbAddtionIncome.Amount != entity.Amount)
                    {
                        PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(
                                         entity.EmployeeId, (byte)MonetoryChangeLogTypeEnum.AdditionalIncome,
                                        new PayManager().GetIncomeById(entity.IncomeId.Value).Title + " for " +
                                        GetLevelById(entity.LevelId.Value).Name
                                        , dbAddtionIncome.Amount, entity.Amount, LogActionEnum.Update)
                                         );
                    }

                    dbAddtionIncome.Amount = entity.Amount;


                }
                else
                {
                    PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(
                                    entity.EmployeeId, (byte)MonetoryChangeLogTypeEnum.AdditionalIncome,
                                   new PayManager().GetIncomeById(entity.IncomeId.Value).Title + " for " +
                                        GetLevelById(entity.LevelId.Value).Name, null, entity.Amount, LogActionEnum.Add)
                                    );


                    PayrollDataContext.GradeAdditionlBasics.InsertOnSubmit(entity);

                }
            }

            PayrollDataContext.SubmitChanges();

            // errro validation like msg
            return "";
        }
        public static Status InsertUpdateGradeReward(GradeRewardEmployee entity, bool isInsert)
        {
            Status status = new Status();

            entity.ModifiedBy = SessionManager.CurrentLoggedInUserID;
            entity.ModifiedOn = GetCurrentDateAndTime();

            ECurrentStatus firstStatus = EmployeeManager.GetFirstStatus(entity.EmployeeId);
            if (firstStatus != null)
            {
                if (entity.RewardDateEng < firstStatus.FromDateEng)
                {
                    status.ErrorMessage = "Reward date can not be earlier then the joining date " + firstStatus.FromDate + ".";
                    return status;
                }
            }
            
            if (isInsert)
            {

                entity.CreatedBy = entity.ModifiedBy = SessionManager.CurrentLoggedInUserID;
                entity.CreatedOn = entity.ModifiedOn = GetCurrentDateAndTime();

                PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(
                                 entity.EmployeeId, (byte)MonetoryChangeLogTypeEnum.Reward,
                                "", null, entity.Amount, LogActionEnum.Add)
                                 );

                PayrollDataContext.GradeRewardEmployees.InsertOnSubmit(entity);
                SaveChangeSet();
            }
            else
            {
                GradeRewardEmployee dbEntity = PayrollDataContext.GradeRewardEmployees.SingleOrDefault(x => x.RewardID == entity.RewardID);
                if (dbEntity != null)
                {

                    if (dbEntity.Amount != entity.Amount)
                    {
                        PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(
                                         entity.EmployeeId, (byte)MonetoryChangeLogTypeEnum.Reward,
                                       "", dbEntity.Amount, entity.Amount, LogActionEnum.Update)
                                         );
                    }

                    CopyObject<GradeRewardEmployee>(entity, ref dbEntity, "LevelId","EmployeeId","IsActive");
                    UpdateChangeSet();
                }
                else
                {
                    status.ErrorMessage = "Reward does not exists.";
                }

            }
            return status;
        }

        public static List<GradeAdditionlBasic> GetEmployeeAdditionalBasic(int employeeId)
        {
            List<GradeAdditionlBasic> list = PayrollDataContext.GradeAdditionlBasics.Where(x => x.EmployeeId == employeeId).ToList();

            foreach (GradeAdditionlBasic item in list)
            {
                item.Level = GetLevelById(item.LevelId.Value).Name;
                if (item.IncomeId == null)
                {
                    item.Income = "Basic Salary";
                }
                else
                {
                    item.Income = new PayManager().GetIncomeById(item.IncomeId.Value).Title;
                }
            }

            return list;
        }
        public static Status InsertUpdateAdditionalBasic(GradeAdditionlBasic entity, bool isInsert)
        {
            Status status = new Status();

            entity.ModifiedBy = SessionManager.CurrentLoggedInUserID;
            entity.ModifiedOn = GetCurrentDateAndTime();

            bool isBasicIncome = new PayManager().GetBasicIncome(SessionManager.CurrentCompanyId)
                .IncomeId == entity.IncomeId;

            if (isInsert == false)
            {
                GradeAdditionlBasic dbEntity = PayrollDataContext.GradeAdditionlBasics
                    .SingleOrDefault(x => x.GradeAdditionalBasicID == entity.GradeAdditionalBasicID);
                if (dbEntity != null)
                    entity.LevelId = dbEntity.LevelId;
            }

            if (PayrollDataContext.GradeAdditionlBasics.Any(x => x.EmployeeId == entity.EmployeeId
                && x.GradeAdditionalBasicID != entity.GradeAdditionalBasicID
                && ( (x.IncomeId == null && isBasicIncome) || (x.IncomeId != null && x.IncomeId == entity.IncomeId) )
                && x.LevelId == entity.LevelId))
            {
                string levelName = NewPayrollManager.GetLevelById(entity.LevelId.Value).Name;
                status.ErrorMessage = "Multiple addition income can not be defined for same employee for same level \"" + levelName + "\", please edit the amount if you want to change.";
                return status;
            }


            if (isInsert)
            {

                entity.CreatedBy = entity.ModifiedBy = SessionManager.CurrentLoggedInUserID;
                entity.CreatedOn = entity.ModifiedOn = GetCurrentDateAndTime();

                PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(
                                    entity.EmployeeId, (byte)MonetoryChangeLogTypeEnum.AdditionalIncome,
                                   new PayManager().GetIncomeById(entity.IncomeId.Value).Title, null, entity.Amount, LogActionEnum.Add)
                                    );

                PayrollDataContext.GradeAdditionlBasics.InsertOnSubmit(entity);
                SaveChangeSet();
            }
            else
            {
                GradeAdditionlBasic dbEntity = PayrollDataContext.GradeAdditionlBasics
                    .SingleOrDefault(x => x.GradeAdditionalBasicID == entity.GradeAdditionalBasicID);
                if (dbEntity != null)
                {

                    if (dbEntity.Amount != entity.Amount)
                    {
                        PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(
                                         entity.EmployeeId, (byte)MonetoryChangeLogTypeEnum.AdditionalIncome,
                                        new PayManager().GetIncomeById(entity.IncomeId.Value).Title, dbEntity.Amount, entity.Amount, LogActionEnum.Update)
                                         );
                    }


                    CopyObject<GradeAdditionlBasic>(entity, ref dbEntity, "LevelId", "EmployeeId", "IsActive");
                    UpdateChangeSet();
                }
                else
                {
                    status.ErrorMessage = "Additional Basic does not exists.";
                }

            }
            return status;
        }
        public static Paging<GetAdditionaBasicListResult> GetAdditionBasicList(int start, int limit, int employeeId, int levelId, int branchId,int incomeId, bool showHistory)
        {


            List<GetAdditionaBasicListResult> list =
                PayrollDataContext.GetAdditionaBasicList(employeeId, start, limit, levelId, branchId,incomeId, showHistory).ToList();

            //if ((start + limit) > payrollMessages.Count)
            //{
            //    limit = payrollMessages.Count - start;
            //}



            int total = 0;
            if (list.Count > 0)
                total = list[0].TotalRows.Value;

            return new Paging<GetAdditionaBasicListResult>(list, total);


        }
        public static Paging<GetGradeRewardListResult> GetRewardList(int start, int limit, int employeeId, int levelId,int branchId)
        {


            List<GetGradeRewardListResult> list =
                PayrollDataContext.GetGradeRewardList(employeeId, start, limit, levelId,branchId).ToList();

            //if ((start + limit) > payrollMessages.Count)
            //{
            //    limit = payrollMessages.Count - start;
            //}

            foreach (GetGradeRewardListResult item in list)
            {
                if (item.IsActive != null && item.IsActive.Value)
                    item.StatusText = "Yes";
                else
                    item.StatusText = "No";
            }

            //List<GetPayrollMessageByUserNameResult> rangePlants = (start < 0 || limit < 0) ? payrollMessages : payrollMessages.GetRange(start, limit);

            int total = 0;
            if (list.Count > 0)
                total = list[0].TotalRows.Value;

            return new Paging<GetGradeRewardListResult>(list, total);


        }

        public static List<PIncomeDate> GetIncomeDateList(int incomeId)
        {
            return PayrollDataContext.PIncomeDates.Where(x => x.IncomeId == incomeId).ToList();
        }
        public static PIncomeDate GetIncomeDate(int incomeId, int dateid)
        {
            return PayrollDataContext.PIncomeDates.FirstOrDefault(x => x.IncomeId == incomeId && x.DateID==dateid);
        }
        public static int? GetLatestDateForIncome(int incomeId)
        {
            PIncomeDate date = 
             PayrollDataContext.PIncomeDates.Where(x => x.IncomeId == incomeId).OrderByDescending(
                x => x.DateID).FirstOrDefault();

            if (date == null)
                return null;

            return date.DateID;

        }
        public static DesignationHistory GetEmployeeFirstDesignation(int employeeId)
        {
            return PayrollDataContext.DesignationHistories.FirstOrDefault(x => x.EmployeeId == employeeId);
        }

        public static bool IsDesignationEditable(int employeeId)
        {
            if (PayrollDataContext.DesignationHistories.Any
                (x => x.EmployeeId == employeeId && (x.IsFirst == null || x.IsFirst == false)))
            {
                return false;
            }
            return true;
        }
        /// <summary>
        /// Check if Employee Level/Grade is editable, if first salary generated or there is promotion for Basic/Level Income
        /// then it is not editable
        /// </summary>
        public static void GetEmployeeFirstLevelGrade(int employeeId,ref bool isEditable,ref int groupId,ref int levelId,ref double gradeStep)
        {
            PIncome basic = new PayManager().GetBasicIncome(SessionManager.CurrentCompanyId);
            PEmployeeIncome empIncome = new PayManager().GetEmployeeIncome(employeeId, basic.IncomeId);

            if (empIncome == null || empIncome.LevelId == null)
            {
                isEditable = true;
                groupId = 0;
                levelId = 0;
                gradeStep = 0;
                return;
            }
            
            PEmployeeIncrement firstLevelIncrement =
                PayrollDataContext.PEmployeeIncrements.FirstOrDefault(x => x.EmployeeIncomeId == empIncome.EmployeeIncomeId);

            // enable in future, for now disable it
            bool isFirstSalaryGenerated = false;// CalculationManager.IsCalculatedSavedForEmployee(employeeId);

            if (isFirstSalaryGenerated || firstLevelIncrement != null)
                isEditable = false;
            else
            {
                isEditable = true;
            }

            // make editabl for this case, as in future correctio may required
            //if (CalculationManager.IsCalculatedSavedForEmployee(employeeId))
            //{
            //    isEditable = false;
            //}

            if (firstLevelIncrement != null && firstLevelIncrement.OldLevelId != null)
            {
                levelId = firstLevelIncrement.OldLevelId.Value;

                if (firstLevelIncrement.OldStepGrade != null)
                    gradeStep = firstLevelIncrement.OldStepGrade.Value;
                else
                    gradeStep = 0;
            }
            else
            {
                
                levelId = empIncome.LevelId.Value;
                if (empIncome.Step != null)
                    gradeStep = empIncome.Step.Value;
                else
                    gradeStep = 0;
            }


            BLevel level = GetLevelById(levelId);
            groupId = level.LevelGroupId;
        }

        public static BLevelRate GetOldLevelRateForSalaryChange(int levelId, DateTime fromDate, LevelGradeChangeType changeType)
        {
            BLevelDate lastDate = PayrollDataContext.BLevelDates.OrderByDescending(x => x.DateID).FirstOrDefault();


            bool hasSalaryChangeOccured = false;
            if (changeType == LevelGradeChangeType.SalaryStructureChange)
            {
                if (lastDate.EffectiveFromEng <= fromDate)
                    hasSalaryChangeOccured = true;
            }


            BLevelRate levelRate = PayrollDataContext.BLevelRates
                .Where(x => x.LevelID == levelId && (hasSalaryChangeOccured==false || lastDate.DateID != x.DateID))
                .OrderByDescending(x => x.BLevelDate.EffectiveFromEng)
                .FirstOrDefault(x => x.BLevelDate.EffectiveFromEng <= fromDate);

            //if (levelRate == null)
            //{
            //    levelRate = PayrollDataContext.BLevelRates
            //    .Where(x => x.LevelID == levelId)
            //    .OrderByDescending(x => x.BLevelDate.EffectiveFromEng)
            //    .FirstOrDefault(x => x.BLevelDate.EffectiveFromEng <= fromDate);
            //}

            if (levelRate == null)
            {
                levelRate = PayrollDataContext.BLevelRates
                .Where(x => x.LevelID == levelId).FirstOrDefault();
            }

            //BLevelRate levelRate = PayrollDataContext.BLevelRates
            //    .Where(x => x.LevelID == levelId)
            //    .OrderBy(x => x.BLevelDate.EffectiveFromEng)
            //    .FirstOrDefault(x => x.BLevelDate.EffectiveFromEng >= fromDate);

            //if (levelRate == null)
            //{
            //    levelRate = PayrollDataContext.BLevelRates
            //    .Where(x => x.LevelID == levelId)
            //    .OrderByDescending(x => x.BLevelDate.EffectiveFromEng)
            //    .FirstOrDefault(x => x.BLevelDate.EffectiveFromEng <= fromDate);
            //}

            //if (levelRate == null)
            //{
            //    levelRate = PayrollDataContext.BLevelRates
            //    .Where(x => x.LevelID == levelId).FirstOrDefault();
            //}

            return levelRate;
        }
        public static BLevelRate GetLevelRate(int levelId, int dateId)
        {
            return
                PayrollDataContext.BLevelRates
                .Where(x => x.LevelID == levelId && x.DateID == dateId)
                .FirstOrDefault();
        }
        public static BLevelRate GetLevelRate(int levelId, DateTime fromDate)
        {
            BLevelRate levelRate = PayrollDataContext.BLevelRates
                .Where(x => x.LevelID == levelId)
                .OrderByDescending(x => x.BLevelDate.EffectiveFromEng)
                .FirstOrDefault(x => x.BLevelDate.EffectiveFromEng <= fromDate);

            //if (levelRate == null)
            //{
            //    levelRate = PayrollDataContext.BLevelRates
            //    .Where(x => x.LevelID == levelId)
            //    .OrderByDescending(x => x.BLevelDate.EffectiveFromEng)
            //    .FirstOrDefault(x => x.BLevelDate.EffectiveFromEng <= fromDate);
            //}

            if (levelRate == null)
            {
                levelRate = PayrollDataContext.BLevelRates
                .Where(x => x.LevelID == levelId).FirstOrDefault();
            }

            //BLevelRate levelRate = PayrollDataContext.BLevelRates
            //    .Where(x => x.LevelID == levelId)
            //    .OrderBy(x => x.BLevelDate.EffectiveFromEng)
            //    .FirstOrDefault(x => x.BLevelDate.EffectiveFromEng >= fromDate);

            //if (levelRate == null)
            //{
            //    levelRate = PayrollDataContext.BLevelRates
            //    .Where(x => x.LevelID == levelId)
            //    .OrderByDescending(x => x.BLevelDate.EffectiveFromEng)
            //    .FirstOrDefault(x => x.BLevelDate.EffectiveFromEng <= fromDate);
            //}

            //if (levelRate == null)
            //{
            //    levelRate = PayrollDataContext.BLevelRates
            //    .Where(x => x.LevelID == levelId).FirstOrDefault();
            //}

            return levelRate;
        }

        #region Deputation

        /// <summary>
        /// Get new grade for Level promotion
        /// </summary>
        public static int GetNewGrade(int oldLevelId, double oldGrade, int newLevelId,DateTime fromDate,LevelGradeChangeType changeType)
        {
            if (changeType != LevelGradeChangeType.Promotion && changeType != LevelGradeChangeType.SalaryStructureChange)
                throw new Exception("Not valid for this type.");

            if (CommonManager.CompanySetting.WhichCompany != WhichCompany.RBB)
                return 0;

            BLevel oldLevel = GetLevelById(oldLevelId);
            BLevel newLevel = GetLevelById(newLevelId);

            //if(newLevel.Order >= oldLevel.Order)
            //    return 0;

            BLevelRate oldLevelRate = GetOldLevelRateForSalaryChange(oldLevelId, fromDate, changeType);
            BLevelRate newLevelRate = GetLevelRate(newLevelId,fromDate);


            decimal currentGrade = Convert.ToDecimal(oldLevelRate.StepGradeRate * (decimal)oldGrade);
            decimal currentSalary = Convert.ToDecimal(oldLevelRate.PayScale) + currentGrade;
            
            decimal quotient = 0;
            int newGrade = 0;

            // Salary Amount Change or Promotion to Atirikta Maan
            if (changeType == LevelGradeChangeType.SalaryStructureChange
                || (changeType == LevelGradeChangeType.Promotion && newLevel.IsAM != null && newLevel.IsAM.Value))
            {
                // Case 1:	Current Salary (Basic + Grade) is greater than Promoted Level's Basic
                //if (currentSalary > Convert.ToDecimal(newLevelRate.PayScale))
                //{

                    currentGrade =
                      Convert.ToDecimal
                      (
                      ((currentSalary - Convert.ToDecimal(oldLevelRate.PayScale)) / Convert.ToDecimal(newLevelRate.StepGradeRate)).ToString("N2")                      
                      );                        
                        
                    newGrade = (int)currentGrade;
                    if (Convert.ToDecimal(newGrade) != 0)
                        quotient = (currentGrade % Convert.ToDecimal(newGrade));
                    if (quotient > 0)
                        newGrade += 1;
                //}
                //else
                //{
                //    newGrade = 0;
                //}
            }
                // Normal Promotion
            else
            {
                if (currentSalary > Convert.ToDecimal(newLevelRate.PayScale))
                {
                    currentGrade =
                      Convert.ToDecimal
                      (
                      ((currentSalary - Convert.ToDecimal(newLevelRate.PayScale)) / Convert.ToDecimal(newLevelRate.StepGradeRate)).ToString("N2")
                      );

                    newGrade = (int)currentGrade;
                    if (Convert.ToDecimal(newGrade) != 0)
                        quotient = (currentGrade % Convert.ToDecimal(newGrade));
                    if (quotient > 0)
                    {
                        newGrade += 1;
                    }
                    newGrade += 1;// again 1 as reward
                }
                else if (currentSalary == Convert.ToDecimal(newLevelRate.PayScale))
                    newGrade = 1;
                else
                {
                    newGrade = 0;
                }
            }

            if (newGrade > newLevelRate.NoOfStepsGrade)
                return newLevelRate.NoOfStepsGrade.Value;

            return newGrade;
        }

        public static List<BLevelDate> GetSalaryChangeDateList()
        {
            return PayrollDataContext.BLevelDates.OrderBy(x => x.DateID).ToList();
        }

        public static Status InsertUpdateEmployeeDeputation(DeputationEmployee entity,bool isInsert)
        {
            Status status = new Status();

            entity.CreatedBy = entity.ModifiedBy = SessionManager.CurrentLoggedInUserID;
            entity.CreatedOn = entity.ModifiedOn = GetCurrentDateAndTime();

            if (isInsert)
            {
                EEmployee employee = new EmployeeManager().GetById(entity.EmployeeId.Value);
                entity.FromBranchId = employee.BranchId;
                PayrollDataContext.DeputationEmployees.InsertOnSubmit(entity);
                SaveChangeSet();
            }
            else
            {
                DeputationEmployee dbEntity = PayrollDataContext.DeputationEmployees.SingleOrDefault(x => x.DeputationEmployeeId == entity.DeputationEmployeeId);
                if (dbEntity != null)
                {
                    CopyObject<DeputationEmployee>(entity, ref dbEntity, "FromBranchId");
                    UpdateChangeSet();
                }
                else
                {
                    status.ErrorMessage = "Deputation does not exists.";
                }

            }
            return status;
        }

        public static Status InsertUpdateEmployeeReward(RewardEmployee entity, bool isInsert)
        {
            Status status = new Status();

            entity.CreatedBy = entity.ModifiedBy = SessionManager.CurrentLoggedInUserID;
            entity.CreatedOn = entity.ModifiedOn = GetCurrentDateAndTime();

            if (isInsert)
            {

                PayrollDataContext.RewardEmployees.InsertOnSubmit(entity);
                SaveChangeSet();
            }
            else
            {
                RewardEmployee dbEntity = PayrollDataContext.RewardEmployees.SingleOrDefault(x => x.RewardID == entity.RewardID);
                if (dbEntity != null)
                {
                    CopyObject<RewardEmployee>(entity, ref dbEntity, "");
                    UpdateChangeSet();
                }
                else
                {
                    status.ErrorMessage = "Reward does not exists.";
                }

            }
            return status;
        }
        public static Status InsertUpdateEmployeeActing(ActingEmployee entity, bool isInsert)
        {
            Status status = new Status();

            entity.CreatedBy = entity.ModifiedBy = SessionManager.CurrentLoggedInUserID;
            entity.CreatedOn = entity.ModifiedOn = GetCurrentDateAndTime();

            //Setting setting = PayrollDataContext.Settings.FirstOrDefault();
            //if (setting != null && setting.ActingIncomeId != null)
            //{
            //    if (PayrollDataContext.PEmployeeIncomes.Any(x => x.EmployeeId == entity.EmployeeId && x.IncomeId == setting.ActingIncomeId.Value) == false)
            //    {
            //        // prevent double insertion in this mode
            //        //if (inserted.ContainsKey(reqInstance.EmployeeID.Value + ":" + overtimeIncomeId.Value) == false)
            //        {
            //            PEmployeeIncome overtimeInc = new PEmployeeIncome();
            //            overtimeInc.EmployeeId = entity.EmployeeId;
            //            overtimeInc.IncomeId = setting.ActingIncomeId.Value;
            //            overtimeInc.IsValid = true;
            //            overtimeInc.IsEnabled = true;

            //            PayrollDataContext.PEmployeeIncomes.InsertOnSubmit(overtimeInc);
            //            //inserted.Add(reqInstance.EmployeeID + ":" + overtimeInc.IncomeId, "");
            //        }
            //    }
            //}

            //PayrollDataContext.CalcGetIncomeValue();


            if (PayrollDataContext.ActingEmployees.Any(x => x.EmployeeId == entity.EmployeeId
                && x.FromLevelId == entity.FromLevelId && x.ActingID != entity.ActingID))
            {
                status.ErrorMessage = "Acting already exists for this employee for the level " +
                    NewPayrollManager.GetLevelById(entity.FromLevelId.Value).Name + ".";
                return status;
            }

            if (isInsert)
            {
                try
                {
                    SetConnectionPwd(PayrollDataContext.Connection);
                    PayrollDataContext.Transaction = PayrollDataContext.Connection.BeginTransaction();
                    PayrollDataContext.ActingEmployees.InsertOnSubmit(entity);
                    SaveChangeSet();
                    PayrollDataContext.InsertUpdateActingEmployeeIncome(entity.ActingID);
                    //List<ActingIncome> _ActingIncomeList = PayrollDataContext.ActingIncomes.ToList();
                    //List<ActingEmployeeIncome> _listActingEmployeeIncome = new List<ActingEmployeeIncome>();
                    //if (_ActingIncomeList.Any())
                    //{
                    //    foreach (ActingIncome _itemActingIncome in _ActingIncomeList)
                    //    {
                    //        ActingEmployeeIncome _ActingEmployeeIncome = new ActingEmployeeIncome();
                    //        _ActingEmployeeIncome.ActingRef_ID = entity.ActingID;
                    //        decimal? Amount1 = PayrollDataContext.GetLevelIncomeAmount(entity.EmployeeId, entity.FromLevelId, _itemActingIncome.SourceIncomeId.Value, (entity.ApplicableTillEng.Value != null ? entity.ApplicableTillEng.Value : entity.ApplicableFromEng.Value));
                    //        decimal? Amount2 = PayrollDataContext.GetLevelIncomeAmount(entity.EmployeeId, entity.ToLevelId, _itemActingIncome.SourceIncomeId.Value, (entity.ApplicableTillEng.Value != null ? entity.ApplicableTillEng.Value : entity.ApplicableFromEng.Value));
                    //        _ActingEmployeeIncome.Amount = ((Amount2 != null) ? Amount2 : 0) - ((Amount1 != null) ? Amount1 : 0);
                    //        _ActingEmployeeIncome.EmployeeId = entity.EmployeeId;
                    //        _ActingEmployeeIncome.IncomeId = _itemActingIncome.IncomeId.Value;
                    //        _listActingEmployeeIncome.Add(_ActingEmployeeIncome);
                    //    }
                    //}

                    //PayrollDataContext.ActingEmployeeIncomes.InsertAllOnSubmit(_listActingEmployeeIncome);
                    //SaveChangeSet();
                    PayrollDataContext.Transaction.Commit();
                }
                catch (Exception exp)
                {
                    PayrollDataContext.Transaction.Rollback();
                    status.ErrorMessage = "Error while saving ";
                    return status;
                }
                finally
                {
                    if (PayrollDataContext.Connection != null)
                    {
                        PayrollDataContext.Connection.Close();
                    }
                }
            }

            else
            {
                try
                {
                    SetConnectionPwd(PayrollDataContext.Connection);
                    PayrollDataContext.Transaction = PayrollDataContext.Connection.BeginTransaction();

                    ActingEmployee dbEntity = PayrollDataContext.ActingEmployees.SingleOrDefault(x => x.ActingID == entity.ActingID);
                    if (dbEntity != null)
                    {
                        CopyObject<ActingEmployee>(entity, ref dbEntity, "");

                        dbEntity.ApplicableTill = entity.ApplicableTill;
                        dbEntity.ApplicableTillEng = entity.ApplicableTillEng;

                        //List<ActingEmployeeIncome> _listActingEmployeeIncome = new List<ActingEmployeeIncome>();

                        //List<ActingIncome> _ActingIncomeList = PayrollDataContext.ActingIncomes.ToList();
                        //if (_ActingIncomeList.Any())
                        //{
                        //    foreach (ActingIncome _itemActingIncome in _ActingIncomeList)
                        //    {
                        //        ActingEmployeeIncome dbEntityActingEmployee = PayrollDataContext.ActingEmployeeIncomes.SingleOrDefault(x => x.ActingRef_ID == entity.ActingID
                        //            && x.IncomeId == _itemActingIncome.SourceIncomeId && x.EmployeeId == entity.EmployeeId);
                        //        if (dbEntityActingEmployee != null)
                        //        {
                        //            decimal? Amount1 = PayrollDataContext.GetLevelIncomeAmount(entity.EmployeeId, entity.FromLevelId, _itemActingIncome.SourceIncomeId.Value, (entity.ApplicableTillEng.Value != null ? entity.ApplicableTillEng.Value : entity.ApplicableFromEng.Value));
                        //            decimal? Amount2 = PayrollDataContext.GetLevelIncomeAmount(entity.EmployeeId, entity.ToLevelId, _itemActingIncome.SourceIncomeId.Value, (entity.ApplicableTillEng.Value != null ? entity.ApplicableTillEng.Value : entity.ApplicableFromEng.Value));
                        //            dbEntityActingEmployee.Amount = ((Amount2 != null) ? Amount2 : 0) - ((Amount1 != null) ? Amount1 : 0);
                        //        }
                        //    }
                        //}

                        UpdateChangeSet();
                        PayrollDataContext.InsertUpdateActingEmployeeIncome(dbEntity.ActingID);
                        PayrollDataContext.Transaction.Commit();

                    }
                    else
                    {
                        status.ErrorMessage = "Acting does not exists.";
                    }
                }
                catch (Exception exp)
                {
                    PayrollDataContext.Transaction.Rollback();
                    status.ErrorMessage = "Error while saving ";
                    return status;
                }
                finally
                {
                    if (PayrollDataContext.Connection != null)
                    {
                        PayrollDataContext.Connection.Close();
                    }
                }

            }
            return status;
        }

        public static List<Deputation> GetDeputationList()
        {
            List<Deputation> list = PayrollDataContext.Deputations.OrderBy(x => x.Name).ToList();

            foreach (Deputation item in list)
            {
                item.AttendanceInText = NewMessage.GetTextValue(((DeputationAttendanceIn)item.AttendanceIn).ToString());
                item.DailyAllowanceText = NewMessage.GetTextValue(((DeputationDA)item.DailyAllowance).ToString());
                item.ApplicableSalaryAllowanceText = NewMessage.GetTextValue(((DeputationApplicableSalaryAllowance)item.ApplicableSalaryAllowance).ToString());
                item.TravellingAllowanceText = NewMessage.GetTextValue(((DeputationTA)item.TravellingAllowance).ToString());

            }


            return list;
        }

        public static List<GetyearlyIncomeListResult> GetDeputationList(int start, int pagesize, int Type)
        {
           return PayrollDataContext.GetyearlyIncomeList(start, pagesize, Type).ToList();
        }


        public static List<ActingEmployee> GetActingEmployeeList
            (int start, int limit, ref int total, string searchEmp,int empId,DateTime? startdate,DateTime? end)
        {
            searchEmp = searchEmp.ToLower();

            // if in employee acting tab then show self acting only
            if (SessionManager.CurrentLoggedInEmployeeId != 0)
                empId = SessionManager.CurrentLoggedInEmployeeId;

            start = (start * limit);

            List<ActingEmployee> list =
                (
                from t in PayrollDataContext.ActingEmployees
                join e in PayrollDataContext.EEmployees on t.EmployeeId equals e.EmployeeId
                where (searchEmp.Length == 0 || e.Name.ToLower().Contains(searchEmp))
                && (empId==0 || empId == e.EmployeeId)
                && (startdate == null || (t.ApplicableFromEng >= startdate))
                && (end == null || t.ApplicableFromEng <= end)
                orderby t.LetterDateEng descending
                select t
                ).ToList();
            //PayrollDataContext.BranchDepartmentHistories.Where(x=>x.FromBranchId != x.BranchId)
            //.OrderByDescending(x => x.FromDateEng)
            //.ToList();

            total = list.Count;
            //Paging code
            if (start >= 0 && limit > 0)
            {
                list = list.Skip(start).Take(limit).ToList();
            }

            EmployeeManager empMgr = new EmployeeManager();
            BranchManager bMgr = new BranchManager();
            DepartmentManager depMgr = new DepartmentManager();
            EEmployee emp= null;
            foreach (ActingEmployee item in list)
            {
                emp = empMgr.GetById(item.EmployeeId);
                item.EmployeeName = emp.Name;
                item.FromLevel = GetLevelById(item.FromLevelId.Value).Name;
                item.ToLevel = GetLevelById(item.ToLevelId.Value).Name;
                item.INo = emp.EHumanResources[0].IdCardNo;
            }

            return list;
        }
        public static List<RewardEmployee> GetRewardEmployeeList
            (int start, int limit, ref int total, string searchEmp,int employeeId)
        {
            searchEmp = searchEmp.ToLower();

            start = (start * limit);

            List<RewardEmployee> list =
                (
                from t in PayrollDataContext.RewardEmployees
                join e in PayrollDataContext.EEmployees on t.EmployeeId equals e.EmployeeId
                where (searchEmp.Length == 0 || e.Name.ToLower().Contains(searchEmp))
                    && (employeeId==0 || employeeId == e.EmployeeId)
                orderby t.LetterDateEng descending
                select t
                ).ToList();
            //PayrollDataContext.BranchDepartmentHistories.Where(x=>x.FromBranchId != x.BranchId)
            //.OrderByDescending(x => x.FromDateEng)
            //.ToList();

            total = list.Count;
            //Paging code
            if (start >= 0 && limit > 0)
            {
                list = list.Skip(start).Take(limit).ToList();
            }

            EmployeeManager empMgr = new EmployeeManager();
            BranchManager bMgr = new BranchManager();
            DepartmentManager depMgr = new DepartmentManager();
            foreach (RewardEmployee item in list)
            {
                item.EmployeeName = empMgr.GetById(item.EmployeeId).Name;
                item.RecommendedBy = empMgr.GetById(item.RecommendedByEmployeeId.Value).Name;
                item.RewardCashOrGrade = item.Type == 1 ? GetCurrency(item.CashAmount) : item.Grade.ToString();
            }

            return list;
        }
        public static List<DeputationEmployee> GetDeputationEmployeeList
            (int start, int limit, ref int total, string searchEmp)
        {
            searchEmp = searchEmp.ToLower();

            start = (start * limit);

            List<DeputationEmployee> list =
                (
                from t in PayrollDataContext.DeputationEmployees
                join e in PayrollDataContext.EEmployees on t.EmployeeId equals e.EmployeeId
                where (searchEmp.Length == 0 || e.Name.ToLower().Contains(searchEmp))
                orderby t.FromDateEng descending
                select t
                ).ToList();
            //PayrollDataContext.BranchDepartmentHistories.Where(x=>x.FromBranchId != x.BranchId)
            //.OrderByDescending(x => x.FromDateEng)
            //.ToList();

            total = list.Count;
            //Paging code
            if (start >= 0 && limit > 0)
            {
                list = list.Skip(start).Take(limit).ToList();
            }

            EmployeeManager empMgr = new EmployeeManager();
            BranchManager bMgr = new BranchManager();
            DepartmentManager depMgr = new DepartmentManager();
            foreach (DeputationEmployee item in list)
            {
                item.EmployeeName = empMgr.GetById(item.EmployeeId.Value).Name;
                if (item.DeputationId != -1)
                    item.DeputationText = GetDeputationById(item.DeputationId.Value).Name;
                if (item.BranchId != null)
                {
                    item.ToBranch = bMgr.GetById(item.BranchId.Value).Name;
                }

                if (item.DepeartmentId != null)
                {
                    item.ToDepartment = depMgr.GetById(item.DepeartmentId.Value).Name;
                }

                if (item.ApprovedBy != null)
                {
                    item.ApprovedByText = UserManager.GetUserByUserID(item.ApprovedBy.Value).NameIfNotEmployee;
                }
            }

            return list;
        }

        public static Deputation GetDeputationById(int deputationId)
        {
            return PayrollDataContext.Deputations.SingleOrDefault(x => x.DeputationId == deputationId);
        }

        public static YearlyIncome GetYearlyIncomeByIncomeId(int type, int sourceId)
        {
            return PayrollDataContext.YearlyIncomes.FirstOrDefault(x => x.Type == type && x.SourceID == sourceId);
        }

        public static DeputationEmployee GetDeputationEmployeeById(int deputationEmployeeId)
        {
            return PayrollDataContext.DeputationEmployees.SingleOrDefault(x => x.DeputationEmployeeId == deputationEmployeeId);
        }
        public static ActingEmployee GetActingEmployeeById(int actingEmployeeId)
        {
            return PayrollDataContext.ActingEmployees.SingleOrDefault(x => x.ActingID == actingEmployeeId);
        }
        public static RewardEmployee GetRewardEmployeeById(int actingEmployeeId)
        {
            return PayrollDataContext.RewardEmployees.SingleOrDefault(x => x.RewardID == actingEmployeeId);
        }
        public static Status DeleteDeputation(int deputationId)
        {
            Status status = new Status();
            Deputation dbRecord = PayrollDataContext.Deputations.SingleOrDefault(c => c.DeputationId == deputationId);

            PayrollDataContext.Deputations.DeleteOnSubmit(dbRecord);
            DeleteChangeSet();

            return status;
        }
        public static Status DeleteActing(int actinID)
        {
            Status status = new Status();
            ActingEmployee dbRecord = PayrollDataContext.ActingEmployees.SingleOrDefault(c => c.ActingID == actinID);
            List<ActingEmployeeIncome> dbRecordActingEmployeeIncomeList = PayrollDataContext.ActingEmployeeIncomes.Where(c => c.ActingRef_ID == actinID).ToList();
            PayrollDataContext.ActingEmployees.DeleteOnSubmit(dbRecord);
            if (dbRecordActingEmployeeIncomeList.Any())
                PayrollDataContext.ActingEmployeeIncomes.DeleteAllOnSubmit(dbRecordActingEmployeeIncomeList);
            DeleteChangeSet();
            return status;
        }
        public static Status DeleteReward(int actinID)
        {
            Status status = new Status();
            RewardEmployee dbRecord = PayrollDataContext.RewardEmployees.SingleOrDefault(c => c.RewardID == actinID);

            PayrollDataContext.RewardEmployees.DeleteOnSubmit(dbRecord);
            DeleteChangeSet();

            return status;
        }
        public static Status DeleteGradeReward(int rewardId)
        {
            Status status = new Status();
            GradeRewardEmployee dbRecord = PayrollDataContext.GradeRewardEmployees.SingleOrDefault(c => c.RewardID == rewardId);

            PayrollDataContext.GradeRewardEmployees.DeleteOnSubmit(dbRecord);
            DeleteChangeSet();

            return status;
        }
        public static Status InsertUpdateDeputation(Deputation entity, bool IsInsert)
        {
            Status myStatus = new Status();

            entity.CreatedBy = entity.ModifiedBy = SessionManager.CurrentLoggedInUserID;
            entity.CreatedOn = entity.ModifiedOn = GetCurrentDateAndTime();

            //Insert Operation
            if (IsInsert)
            {               

                PayrollDataContext.Deputations.InsertOnSubmit(entity);
                PayrollDataContext.SubmitChanges();
                return myStatus;
            }

            Deputation dbRecord = PayrollDataContext.Deputations.SingleOrDefault(c => c.DeputationId == entity.DeputationId);



            CopyObject<Deputation>(entity, ref dbRecord);
            PayrollDataContext.SubmitChanges();
            return myStatus;


        }

        public static Status InsertUpdateYearlyIncome(YearlyIncome entity)
        {
            Status myStatus = new Status();


            YearlyIncome dbentity = PayrollDataContext.YearlyIncomes.FirstOrDefault(x =>
                x.Type == entity.Type && x.SourceID == entity.SourceID);

            if (dbentity != null)
            {
               // dbentity.Type = entity.Type;
               // dbentity.SourceID = entity.SourceID;
                //dbentity.TypeEnum = entity.TypeEnum;
                PayrollDataContext.YearlyIncomes.DeleteOnSubmit(dbentity);
                PayrollDataContext.SubmitChanges();
                PayrollDataContext.YearlyIncomes.InsertOnSubmit(entity);
                PayrollDataContext.SubmitChanges();
            }
            else
            {
                PayrollDataContext.YearlyIncomes.InsertOnSubmit(entity);
            }

            PayrollDataContext.SubmitChanges();

            return myStatus;
        }

        #endregion
        #region "Defined Income Type"
        public static List<PIncome> GetDefinedTypeIncomes()
        {
            return PayrollDataContext.PIncomes.Where(x => x.IsDefinedType != null && x.IsDefinedType.Value
                ).OrderBy(x => x.Title).ToList();
        }
        public static List<PIncomeDefinedAmount> GetDefinedAmounts(int incomeId,int? dateId)
        {
            if (dateId == null)
                return new List<PIncomeDefinedAmount>();

            return PayrollDataContext.PIncomeDefinedAmounts.Where(x => x.IncomeId == incomeId && x.DateID == dateId.Value).ToList();
        }
        public static List<TextValue> GetDefinedList(int incomeId, bool isXType)
        {
            List<TextValue> list = new List<TextValue>();

            PIncome income = PayrollDataContext.PIncomes.SingleOrDefault(x => x.IncomeId == incomeId);

            if (income != null)
            {
                int value = 0;
                if (isXType)
                    value = income.XType.Value;
                else
                    value = income.YType.Value;

                IncomeDefinedTypeEnum type = (IncomeDefinedTypeEnum)value;

                switch (type)
                {
                    case IncomeDefinedTypeEnum.Branch:
                        list =
                            (
                            from b in PayrollDataContext.Branches
                            orderby b.Name
                            select new TextValue
                            {
                                Text = b.Name,
                                Value = b.BranchId.ToString()
                            }
                            ).ToList();
                        break;
                    case IncomeDefinedTypeEnum.Department:
                        list =
                            (
                            from b in PayrollDataContext.Departments
                            orderby b.Name
                            select new TextValue
                            {
                                Text = b.Name,
                                Value = b.DepartmentId.ToString()
                            }
                            ).ToList();
                        break;
                    case IncomeDefinedTypeEnum.Level:
                        list =
                            (
                            from b in PayrollDataContext.BLevels
                            orderby b.Order
                            select new TextValue
                            {
                                // replace . with # as .(dot) is not valid  in excel column header
                                Text = b.BLevelGroup.Name + " - " + b.Name.Replace(".","#"),
                                Value = b.LevelId.ToString()
                            }
                            ).ToList();
                        break;
                    case IncomeDefinedTypeEnum.Designation:
                        list =
                            (
                            from b in PayrollDataContext.EDesignations
                            orderby b.Order
                            select new TextValue
                            {
                                Text = b.BLevel.Name + " - " + b.Name,
                                Value = b.DesignationId.ToString()
                            }
                            ).ToList();
                        break;
                    case IncomeDefinedTypeEnum.NotDefined:
                        list.Add(new TextValue { Text = "", Value = "0" });
                        break;
                }

            }
            //only x type exists without y
            //else
            {

            }

            return list;
        }

        public static Status SaveUpdateDefinedIncomeAmount(List<PIncomeDefinedAmount> list, int incomeId)
        {
            int? dateId = NewPayrollManager.GetLatestDateForIncome(incomeId);
            Status status = new Status();
            if (dateId == null)
            {
                status.ErrorMessage = "From date not defined to save the values.";
                return status;
            }

            foreach (PIncomeDefinedAmount item in list)
                item.DateID = dateId.Value;
            
            PayrollDataContext.PIncomeDefinedAmounts.DeleteAllOnSubmit(
                PayrollDataContext.PIncomeDefinedAmounts.Where(x => x.IncomeId == incomeId && x.DateID==dateId).ToList());

            PayrollDataContext.PIncomeDefinedAmounts.InsertAllOnSubmit(list);

            PayrollDataContext.SubmitChanges();
            return status;
        }
        #endregion
        public static List<BLevelGroup> GetLevelGroup()
        {
            return PayrollDataContext.BLevelGroups.OrderBy(x => x.Order).ToList();
        }

        public static List<BLevel> GetParentLevels(int levelGroupId)
        {
            return PayrollDataContext.BLevels.Where(x => x.LevelGroupId == levelGroupId && x.ParentLevelId==null)
                .OrderBy(x => x.Order).ToList();
        }
        public static List<BLevel> GetAllParentLevels()
        {
            return PayrollDataContext.BLevels.Where(x=>x.ParentLevelId==null).OrderBy(x => x.Order).ToList();
        }
        public static BLevel GetLevelById(int levelId)
        {
            return PayrollDataContext.BLevels.SingleOrDefault(x => x.LevelId == levelId);
        }
        public static BLevelGroup GetGroupById(int LevelGroupId)
        {
            return PayrollDataContext.BLevelGroups.SingleOrDefault(x => x.LevelGroupId == LevelGroupId);
        }
        public static BLevelDate GetLevelDateById(int levelDateID)
        {
            return PayrollDataContext.BLevelDates.SingleOrDefault(x => x.DateID == levelDateID);
        }
        public static HFamily GetFamilyMemberById(int levelId)
        {
            return PayrollDataContext.HFamilies.SingleOrDefault(x => x.FamilyId == levelId);
        }

        public static Status GenerateSalaryScale(BLevel level)
        {
            Status status = new Status();

            //decimal amount = level.PayScale;
            //for (int i = 1; i <= level.NoOfSteps; i++)
            //{

            //}

            return status;
        }

        public static BLevelDate GetPrevLevelDate(int levelDateID)
        {
            return PayrollDataContext.BLevelDates.OrderByDescending(x => x.DateID)
                .Where(x =>levelDateID==0 || x.DateID < levelDateID).FirstOrDefault();
        }
        public static PIncomeDate GetPrevIncomeDate(int levelDateID,int incomeId)
        {
            return PayrollDataContext.PIncomeDates.OrderByDescending(x => x.DateID)
                .Where(x => (levelDateID == 0 || x.DateID < levelDateID) && x.IncomeId == incomeId).FirstOrDefault();
        }
        public static BLevelDate GetLastLevelDate()
        {
            return PayrollDataContext.BLevelDates.OrderByDescending(x => x.DateID)
                .FirstOrDefault();
        }
        public static void ChangeSalaryScale(List<SalaryScale> list, BLevelDate date,bool isInsert)
        {
            if (isInsert)
            {
                PayrollDataContext.BLevelDates.InsertOnSubmit(date);
            }
            else
            {
                List<BLevelRate> oldRates = PayrollDataContext.BLevelRates.Where(x => x.DateID == date.DateID).ToList();

                BLevelDate dbDate = PayrollDataContext.BLevelDates.SingleOrDefault(x => x.DateID == date.DateID);

                dbDate.Name = date.Name;
                dbDate.EffectiveFrom = date.EffectiveFrom;
                dbDate.EffectiveFromEng = date.EffectiveFromEng;

                PayrollDataContext.BLevelRates.DeleteAllOnSubmit(oldRates);
            }

            List<BLevelRate> newRates = new List<BLevelRate>();
            foreach (SalaryScale item in list)
            {
                BLevelRate rate = new BLevelRate();
                rate.LevelID = item.LevelId;

                rate.PayScale = item.PayScale;
                rate.NoOfStepsGrade = item.NoOfSteps;
                rate.StepGradeRate = item.StepRate;
                rate.CreatedBy = rate.ModifiedBy = SessionManager.CurrentLoggedInUserID;
                rate.CreatedOn = rate.ModifiedOn = GetCurrentDateAndTime();


                if (isInsert == false)
                    rate.DateID = date.DateID;
                
                newRates.Add(rate);
            }

            if (isInsert)
                date.BLevelRates.AddRange(newRates);
            else
            {
                PayrollDataContext.BLevelRates.InsertAllOnSubmit(newRates);
            }

            PayrollDataContext.SubmitChanges();
        }
        public static void ChangeDefinedDate( PIncomeDate date, bool isInsert)
        {
            if (isInsert)
            {
                PayrollDataContext.PIncomeDates.InsertOnSubmit(date);
            }
            //else
            //{
            //    List<BLevelRate> oldRates = PayrollDataContext.BLevelRates.Where(x => x.DateID == date.DateID).ToList();

            //    BLevelDate dbDate = PayrollDataContext.BLevelDates.SingleOrDefault(x => x.DateID == date.DateID);

            //    dbDate.Name = date.Name;
            //    dbDate.EffectiveFrom = date.EffectiveFrom;
            //    dbDate.EffectiveFromEng = date.EffectiveFromEng;

            //    PayrollDataContext.BLevelRates.DeleteAllOnSubmit(oldRates);
            //}

           
            PayrollDataContext.SubmitChanges();
        }
        public static BLevelDate GetLastSalaryScaleDate()
        {
            return PayrollDataContext.BLevelDates.OrderByDescending(x => x.DateID).FirstOrDefault();
        }
        public static int? GetMaxGradeStepForAllDateForGrid()
        {
            return PayrollDataContext.BLevelRates.Select(x => x.NoOfStepsGrade).Max();
        }
        public static List<SalaryScale> GetSalaryScaleList(int dateID)
        {
            List<BLevel> levels = GetAllParentLevelWithSubLevelList(); ;

            List<SalaryScale> list = new List<SalaryScale>();

            bool allowDecimalInGrade = AllowDecimalInGrade();

            foreach (BLevel level in levels)
            {
                BLevelRate rate = PayrollDataContext.BLevelRates.FirstOrDefault(x => x.DateID == dateID &&
                    x.LevelID == level.LevelId);

                SalaryScale scale = new SalaryScale();
                scale.LevelGroupId = level.LevelGroupId;
                scale.GroupLevel = level.BLevelGroup.Name;
                scale.LevelId = level.LevelId;
                scale.LevelCode = level.Code;
                scale.NoOfSteps = rate == null ? 0 : rate.NoOfStepsGrade.Value;
                scale.PayScale = rate == null ? 0 : rate.PayScale.Value;// level.PayScale;
                scale.StepRate = rate == null ? 0 : rate.StepGradeRate.Value;// level.StepRate;
                scale.Amounts = new Dictionary<string, decimal?>();

                for (int i = 1; i <= scale.NoOfSteps; i++)
                {
                    scale.Amounts.Add
                        (
                        scale.LevelGroupId + ":" + scale.LevelId + ":" + i,
                        scale.PayScale + (scale.StepRate * i)
                    );

                    if (allowDecimalInGrade)
                    {
                        scale.Amounts.Add(
                          scale.LevelGroupId + ":" + scale.LevelId + ":" + (i + 0.5),
                          scale.PayScale + (scale.StepRate * (decimal)(i + 0.5))
                        );
                    }
                }

                list.Add(scale);
            }


            return list;
        }


        public static List<BLevel> GetAllParentLevelList()
        {
            List<BLevel> list = PayrollDataContext.BLevels.Where(x=>x.ParentLevelId==null)
                .OrderBy(x => x.BLevelGroup.Order)
                .ThenBy(x => x.Order).ToList();

            foreach (BLevel level in list)
            {
                level.GroupName = level.BLevelGroup.Name;
            }
            return list;
        }
        public static List<EmployeeLevelGradeBO> GetEmployeeCurrentLevelAndGradeForExportPromotion()
        {
            return
                (
                    from l in PayrollDataContext.PEmployeeIncomes
                    join e in PayrollDataContext.EEmployees on l.EmployeeId equals e.EmployeeId
                    join le in PayrollDataContext.BLevels on l.LevelId equals le.LevelId
                    join gg in PayrollDataContext.BLevelGroups on le.LevelGroupId equals gg.LevelGroupId
                    join h in PayrollDataContext.EHumanResources on e.EmployeeId equals h.EmployeeId
                    join d in PayrollDataContext.EDesignations on e.DesignationId equals d.DesignationId
                    where l.LevelId != null
                    orderby e.EmployeeId
                    select new EmployeeLevelGradeBO
                    {
                        EIN = e.EmployeeId,
                        INo = h.IdCardNo==null ? "" : h.IdCardNo,
                        Name = e.Name,
                        CurrentLevel = gg.Name + " - " + le.Name,
                        CurrentGrade = (l.Step == null ? "" : l.Step.ToString()),
                        CurrentDesignation = gg.Name + " - " + le.Name + " - " + d.Name,
                        DesignationId = e.DesignationId.Value,
                        LevelId = l.LevelId.Value,
                        Grade = (l.Step==null ? 0 : l.Step.Value)
                    }

                ).ToList();
        }
        public static List<BLevel> GetAllParentLevelWithSubLevelList()
        {
            List<BLevel> list = PayrollDataContext.BLevels.Where(x => x.ParentLevelId == null)
                .OrderBy(x => x.BLevelGroup.Order)
                .ThenBy(x => x.Order).ToList();

            for (int i = list.Count - 1; i >= 0; i--)
            {
                BLevel level = list[i];
                level.GroupName = level.BLevelGroup.Name;

                if(CommonManager.CompanySetting.WhichCompany==WhichCompany.RBB)
                {
                    List<BLevel> childList =
                        PayrollDataContext.BLevels.Where(x => x.ParentLevelId == level.LevelId).OrderBy(x => x.Order).ToList();

                    int insert = i + 1;
                    for (int j = 0; j < childList.Count; j++)
                    {
                        list.Insert(insert, childList[j]);

                        insert = insert + 1;
                    }
                }
            }

            return list;
        }

        public static List<BLevel> GetAllSubLevelList()
        {
            List<BLevel> list = PayrollDataContext.BLevels.Where(x => x.ParentLevelId != null)
                .OrderBy(x => x.BLevelGroup.Order)
                .ThenBy(x => x.Order).ToList();

            foreach (BLevel level in list)
            {
                level.GroupName = level.BLevelGroup.Name;
                level.ParentName = GetLevelById(level.ParentLevelId.Value).Name;
            }
            return list;
        }
        public static List<HFamily> GetAllFamilyList(int empID)
        {
            List<HFamily> list = PayrollDataContext.HFamilies.OrderBy(x => x.Name).Where(y => y.EmployeeId == empID).ToList();

            foreach (var item in list)
            {
                if (NewPayrollManager.IsFamilyEditable(item) == 1)
                    item.IsEditable = 1;
                else
                    item.IsEditable = 0;

                if (item.OccupationId != null)
                    item.Occupation = PayrollDataContext.HrFamilyOccupations.SingleOrDefault(x => x.OccupationId == item.OccupationId).Occupation;
               
            }

            return list;
        }

        public static List<EDesignation> GetAllPositionOrDesignationsList()
        {
            List<EDesignation> list = PayrollDataContext.EDesignations.OrderBy(x => x.BLevel.Order)
                .ThenBy(x => x.Order).ToList();

            foreach (EDesignation level in list)
            {
                BLevel levelEntity =NewPayrollManager.GetLevelById(level.LevelId.Value);
                if (level.LevelId != null)
                    level.LevelName = levelEntity.BLevelGroup.Name + " -  " + levelEntity.Name;
                level.Category = ((DesignationCategory)level.CategoryId).ToString();
                level.Group = ((DesignationGroup)level.GroupId).ToString();
            }

            return list;
        }
        public static Status DeleteLevel(int levelId)
        {
            Status status = new Status();

            BLevel dbEntity = PayrollDataContext.BLevels.SingleOrDefault(x => x.LevelId == levelId);

            if (PayrollDataContext.PEmployeeIncomes.Any(x => x.LevelId == levelId))
            {
                status.ErrorMessage = "Level is assigned to the employee.";
                return status;
            }
            if (PayrollDataContext.PEmployeeIncrements.Any(x => x.OldLevelId == levelId || x.NewLevelId == levelId))
            {
                status.ErrorMessage = "Level is assigned to the employee.";
                return status;
            }
            if (PayrollDataContext.LevelGradeChangeDetails.Any(x => x.PrevLevelId == levelId || x.LevelId == levelId))
            {
                status.ErrorMessage = "Level is assigned to the employee.";
                return status;
            }
            PayrollDataContext.BLevels.DeleteOnSubmit(dbEntity);
            PayrollDataContext.SubmitChanges();
            return status;
        }
        public static Status DeleteGroup(int groupLevelId)
        {
            Status status = new Status();

            BLevelGroup dbEntity = PayrollDataContext.BLevelGroups.SingleOrDefault(x => x.LevelGroupId == groupLevelId);

            if (PayrollDataContext.BLevels.Any(x => x.LevelGroupId == groupLevelId))
            {
                status.ErrorMessage = "Group is assigned to the level.";
                return status;
            }


            PayrollDataContext.BLevelGroups.DeleteOnSubmit(dbEntity);
            PayrollDataContext.SubmitChanges();
            return status;
        }
        public static Status DeleteFamilyMember(int levelId)
        {
            Status status = new Status();

            HFamily dbEntity = PayrollDataContext.HFamilies.SingleOrDefault(x => x.FamilyId == levelId);
            PayrollDataContext.HFamilies.DeleteOnSubmit(dbEntity);
            PayrollDataContext.SubmitChanges();
            return status;
        }

        //public static int GetGradeStepForLevel(int levelID, DateTime date)
        //{
        //    //DateTime currentDate = GetCurrentDateAndTime();

        //    BLevelRate rate =
        //        (
        //         from r in PayrollDataContext.BLevelRates
        //         join d in PayrollDataContext.BLevelDates on r.DateID equals d.DateID
        //         where d.EffectiveFromEng >= date
        //         orderby d.EffectiveFromEng
        //         select r
        //        ).FirstOrDefault();

        //    if (rate == null)
        //        rate = PayrollDataContext.BLevelRates.FirstOrDefault(x => x.LevelID == levelID);

        //    return rate.NoOfStepsGrade == null ? 0 : rate.NoOfStepsGrade.Value;
        //}

        //public static void UpdateLevelScale(BLevelRate entity)
        //{
        //    BLevel dbEntity = PayrollDataContext.BLevels
        //        .SingleOrDefault(x => x.LevelId == entity.LevelID);

        //    // set basic salary id as fixed for bhanijya
        //    if (dbEntity.IncomeId == 0)
        //    {
        //        dbEntity.IncomeId = new PayManager().GetBasicIncome(SessionManager.CurrentCompanyId).IncomeId;
        //    }

        //    dbEntity.PayScale = entity.PayScale;
        //    dbEntity.NoOfSteps = entity.NoOfSteps;
        //    dbEntity.StepRate = entity.StepRate;

        //    PayrollDataContext.SubmitChanges();

        //}

        public static void ChangeDesignationOrder(int sourceId, int destId,string mode)
        {
            EDesignation dbSource = PayrollDataContext.EDesignations.SingleOrDefault(x => x.DesignationId == sourceId);
            EDesignation dbDest = PayrollDataContext.EDesignations.SingleOrDefault(x => x.DesignationId == destId);
            bool isDropBefore = mode.ToLower().Trim().Contains("before") ? true : false;
            int? order = null;
            if (isDropBefore)
            {

                // now change order of other elements after dest
                List<EDesignation> otherList = PayrollDataContext.EDesignations
                    .Where(x => x.Order > dbDest.Order).OrderBy(x => x.Order).ToList();

                order = dbDest.Order;
                dbSource.Order = dbDest.Order;
                dbDest.Order = ++order;                

                foreach (EDesignation item in otherList)
                {
                    if (item.DesignationId != dbSource.DesignationId)
                        item.Order = ++order;
                }


            }
            else
            {
                // now change order of other elements after dest
                List<EDesignation> otherList = PayrollDataContext.EDesignations
                    .Where(x => x.Order > dbDest.Order).OrderBy(x => x.Order).ToList();

                order = dbDest.Order;
                dbSource.Order = ++order;

                foreach (EDesignation item in otherList)
                {
                    if (item.DesignationId != dbSource.DesignationId)
                        item.Order = ++order;
                }
            }

            PayrollDataContext.SubmitChanges();

            order = 1;
            List<EDesignation> allList = PayrollDataContext.EDesignations.OrderBy(x => x.Order).ToList();
            foreach (EDesignation item in allList)
            {
                item.Order = order++;
            }

            PayrollDataContext.SubmitChanges();
        }


        public static bool IsLevelOrderExists(int LevelID, int Order)
        {
            List<BLevel> dbEntity = PayrollDataContext.BLevels.Where(x => x.LevelId != LevelID && x.Order == Order).ToList();
            if (dbEntity.Any())
                return true;
            else return false;
        }

        public static bool IsLevelCodeExists(int LevelID, string Code)
        {
            List<BLevel> dbEntity = PayrollDataContext.BLevels.Where(x => x.LevelId != LevelID && x.Code == Code).ToList();
            if (dbEntity.Any())
                return true;
            else return false;
        }
        public static bool IsLevelNameExists(int LevelID, int groupId,string name)
        {
            List<BLevel> dbEntity = PayrollDataContext.BLevels.Where(x => x.LevelId != LevelID && x.Name.ToLower() == name.ToLower()
                && x.LevelGroupId == groupId).ToList();
            if (dbEntity.Any())
                return true;
            else return false;
        }
        public static bool IsDesignationNameExists(int LevelID, int designationid, string name)
        {
            List<EDesignation> dbEntity = PayrollDataContext.EDesignations.Where(x => x.DesignationId != designationid 
                && x.Name.ToLower() == name.ToLower()
                && x.LevelId == LevelID).ToList();
            if (dbEntity.Any())
                return true;
            else return false;
        }
        public static bool IsPositionOrderExists(int DesignationID, int Order)
        {
            List<EDesignation> dbEntity = PayrollDataContext.EDesignations.Where(x => x.DesignationId != DesignationID && x.Order == Order).ToList();
            if (dbEntity.Any())
                return true;
            else return false;
        }

        public static bool IsPositionCodeAlreadyExists(int DesignationID, string Code)
        {
            List<EDesignation> dbEntity = PayrollDataContext.EDesignations.Where(x => x.DesignationId != DesignationID && x.Code == Code).ToList();
            if (dbEntity.Any())
                return true;
            else return false;
        }


        public static Status InsertUpdateLevel(BLevel entity,bool isInsert)
        {
            Status status = new Status();


            if (isInsert)
            {
                entity.CreatedBy = entity.ModifiedBy = SessionManager.CurrentLoggedInUserID;
                entity.CreatedOn = entity.ModifiedOn = GetCurrentDateAndTime();

                List<BLevelDate> levelDateList = PayrollDataContext.BLevelDates.ToList();
                // Insert level for each date as in each date case Amount,Step/Grade could be different
                //foreach (BLevelDate date in levelDateList)
                //{
                //    BLevel newLevel = new BLevel();
                //    CopyObject<BLevel>(entity, ref newLevel);
                //    newLevel.DateID = date.DateID;

                //    PayrollDataContext.BLevels.InsertOnSubmit(entity);
                //}

                PayrollDataContext.BLevels.InsertOnSubmit(entity);
               
            }
            else
            {
                BLevel dbEntity = PayrollDataContext.BLevels.SingleOrDefault(x => x.LevelId == entity.LevelId);
                //CopyObject<BLevel>(entity, ref dbEntity, "CreatedOn", "CreatedBy");
                dbEntity.ParentLevelId = entity.ParentLevelId;
                dbEntity.IsAM = entity.IsAM;
                dbEntity.Name = entity.Name;
                dbEntity.LevelGroupId = entity.LevelGroupId;
                dbEntity.Code = entity.Code;
                dbEntity.Order = entity.Order;
                dbEntity.ClericalType = entity.ClericalType;
                dbEntity.ModifiedBy = SessionManager.CurrentLoggedInUserID;
                dbEntity.ModifiedOn = GetCurrentDateAndTime();
            }

            PayrollDataContext.SubmitChanges();

            return status;
        }

        public static bool IsGroupOrderExists(int LevelGroupId,int Order)
        {
            List<BLevelGroup> dbEntity = PayrollDataContext.BLevelGroups.Where(x => x.LevelGroupId != LevelGroupId && x.Order == Order).ToList();
            if (dbEntity.Any())
                return true;
            else return false;
        }

        public static Status InsertUpdateGroup(BLevelGroup entity, bool isInsert)
        {
            Status status = new Status();


            if (isInsert)
            {


                PayrollDataContext.BLevelGroups.InsertOnSubmit(entity);

            }
            else
            {


                BLevelGroup dbEntity = PayrollDataContext.BLevelGroups.SingleOrDefault(x => x.LevelGroupId == entity.LevelGroupId);
                //CopyObject<BLevel>(entity, ref dbEntity, "CreatedOn", "CreatedBy");

               
                dbEntity.Name = entity.Name;
                dbEntity.LevelGroupId = entity.LevelGroupId;
                dbEntity.Order = entity.Order;


            }

            PayrollDataContext.SubmitChanges();

            return status;
        }
        public static List<EDesignation> GetDesignationByLevel(int levelId)
        {
            return PayrollDataContext.EDesignations.Where(x => x.LevelId == levelId).
                OrderBy(x => x.Order).ToList();
        }

        public static string GetValidatePFCITAndPANNo(EFundDetail fund,int employeeId,PPay pay)
        {
          
            string pfNo = fund.PFRFNo == null ? "" : fund.PFRFNo.Trim().ToLower();
            string citNo =fund.CITNo == null ? "" : fund.CITNo.Trim().ToLower();
            string panNo =fund.PANNo==null ? "" :  fund.PANNo.Trim().ToLower();
            string bankNo = pay.BankACNo == null ? "" : pay.BankACNo.Trim().ToLower();
            string loanAccount = pay.LoanAccount == null ? "" : pay.LoanAccount.Trim().ToLower();
            string advanceAccount = pay.AdvanceAccount == null ? "" : pay.AdvanceAccount.Trim().ToLower();

            if (!string.IsNullOrEmpty(pfNo))
            {
                EFundDetail otherEmp = 
                    (
                        from x in PayrollDataContext.EFundDetails
                        join h in PayrollDataContext.EHumanResources on x.EmployeeId equals h.EmployeeId
                        join e in PayrollDataContext.EEmployees on x.EmployeeId equals e.EmployeeId
                        where x.EmployeeId != employeeId && x.PFRFNo.Trim().ToLower() == pfNo
                            
                        select x
                    ).FirstOrDefault();
                    //PayrollDataContext.EFundDetails.Where(x => x.EmployeeId != employeeId && x.PFRFNo.Trim().ToLower() == pfNo)
                    //.FirstOrDefault();
                if (otherEmp != null)
                {
                    return
                        "PF No \"" + fund.PFRFNo + "\" already exists for other employee " + otherEmp.EEmployee.Name + ".";
                }
            }

            if (!string.IsNullOrEmpty(citNo))
            {
                EFundDetail otherEmp =
                     (
                        from x in PayrollDataContext.EFundDetails
                        join h in PayrollDataContext.EHumanResources on x.EmployeeId equals h.EmployeeId
                        join e in PayrollDataContext.EEmployees on x.EmployeeId equals e.EmployeeId
                        where x.EmployeeId != employeeId && x.CITNo.Trim().ToLower() == citNo
                             
                        select x
                    ).FirstOrDefault();
                    //PayrollDataContext.EFundDetails.Where(x => x.EmployeeId != employeeId && x.CITNo.Trim().ToLower() == citNo)
                    //.FirstOrDefault();
                if (otherEmp != null)
                {
                    return
                        "CIT No \"" + fund.CITNo + "\" already exists for other employee " + otherEmp.EEmployee.Name + ".";
                }
            }

            if (!string.IsNullOrEmpty(panNo))
            {
                EFundDetail otherEmp =
                     (
                        from x in PayrollDataContext.EFundDetails
                        join h in PayrollDataContext.EHumanResources on x.EmployeeId equals h.EmployeeId
                        join e in PayrollDataContext.EEmployees on x.EmployeeId equals e.EmployeeId
                        where x.EmployeeId != employeeId && x.PANNo.Trim().ToLower() == panNo
                            
                        select x
                    ).FirstOrDefault();
                if (otherEmp != null)
                {
                    return
                        "PAN No \"" + fund.PANNo + "\" already exists for other employee " + otherEmp.EEmployee.Name + ".";
                }
            }

            if (!string.IsNullOrEmpty(bankNo))
            {
                PPay otherEmp =
                     (
                        from x in PayrollDataContext.PPays
                        join h in PayrollDataContext.EHumanResources on x.EmployeeId equals h.EmployeeId
                        join e in PayrollDataContext.EEmployees on x.EmployeeId equals e.EmployeeId
                        where x.EmployeeId != employeeId && x.BankACNo.Trim().ToLower() == bankNo

                        select x
                    ).FirstOrDefault();
                if (otherEmp != null)
                {
                    return
                        "Bank Account No \"" + pay.BankACNo + "\" already exists for other employee " + otherEmp.EEmployee.Name + ".";
                }
            }

            if (!string.IsNullOrEmpty(loanAccount))
            {
                PPay otherEmp =
                     (
                        from x in PayrollDataContext.PPays
                        join h in PayrollDataContext.EHumanResources on x.EmployeeId equals h.EmployeeId
                        join e in PayrollDataContext.EEmployees on x.EmployeeId equals e.EmployeeId
                        where x.EmployeeId != employeeId && x.LoanAccount.Trim().ToLower() == loanAccount

                        select x
                    ).FirstOrDefault();
                if (otherEmp != null)
                {
                    return
                        "Loan Account No \"" + pay.LoanAccount + "\" already exists for other employee " + otherEmp.EEmployee.Name + ".";
                }
            }

            if (!string.IsNullOrEmpty(advanceAccount))
            {
                PPay otherEmp =
                     (
                        from x in PayrollDataContext.PPays
                        join h in PayrollDataContext.EHumanResources on x.EmployeeId equals h.EmployeeId
                        join e in PayrollDataContext.EEmployees on x.EmployeeId equals e.EmployeeId
                        where x.EmployeeId != employeeId && x.AdvanceAccount.Trim().ToLower() == advanceAccount

                        select x
                    ).FirstOrDefault();
                if (otherEmp != null)
                {
                    return
                        "Advance Account No \"" + pay.AdvanceAccount + "\" already exists for other employee " + otherEmp.EEmployee.Name + ".";
                }
            }
            return "";
        }

        public static Status UpdateFund(EFundDetail entity,EEmployee employee,PPay pay)
        {
            Status status = new Status();

            EFundDetail dbEntity = PayrollDataContext.EFundDetails.SingleOrDefault(x => x.EmployeeId == employee.EmployeeId);
            EEmployee dbEmp = PayrollDataContext.EEmployees.FirstOrDefault(x => x.EmployeeId == employee.EmployeeId);
            EmployeeManager mgr = new EmployeeManager();


            //Tax Status Log
            if (dbEmp.HasCoupleTaxStatus != employee.HasCoupleTaxStatus)
            {
                PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(dbEmp.EmployeeId, (byte)MonetoryChangeLogTypeEnum.HasCoupleTaxStatus,
                    "Has Couple Tax Status", dbEmp.HasCoupleTaxStatus, employee.HasCoupleTaxStatus, LogActionEnum.Update));
            }

            dbEmp.HasCoupleTaxStatus = employee.HasCoupleTaxStatus;
            dbEmp.IsLocalEmployee = employee.IsLocalEmployee;
            dbEmp.IsNonResident = employee.IsNonResident;

            if (dbEmp.ExcludeInSalary != employee.ExcludeInSalary)
                PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(dbEmp.EmployeeId, (byte)MonetoryChangeLogTypeEnum.Salary
                    , "Do Not Calculate Salary", dbEmp.ExcludeInSalary == null ? "" : dbEmp.ExcludeInSalary.ToString(), employee.ExcludeInSalary == null ? "" : employee.ExcludeInSalary.ToString(), LogActionEnum.Update));
            
            dbEmp.ExcludeInSalary = employee.ExcludeInSalary;


            string errorMsg = GetValidatePFCITAndPANNo(entity,employee.EmployeeId,pay);
            if (!string.IsNullOrEmpty(errorMsg))
            {
                status.ErrorMessage = errorMsg;
                return status;
            }

            if (dbEntity == null)
            {
                dbEntity = new EFundDetail();
                dbEntity.EmployeeId = employee.EmployeeId;
                dbEntity.NoCITContribution = false;
                PayrollDataContext.EFundDetails.InsertOnSubmit(dbEntity);
            }

            dbEntity.PFRFNo = entity.PFRFNo;
            dbEntity.CID = entity.CID;
            dbEntity.Nominee = entity.Nominee;
            dbEntity.Relation = entity.Relation;

            dbEntity.EmployeePan = entity.EmployeePan;
            dbEntity.PANNo = entity.PANNo;

            dbEntity.CITNo = entity.CITNo;
            dbEntity.CITCode = entity.CITCode;
            #region "Change Logs"            

            //No PF
            if ((dbEntity.NoPF == null || dbEntity.NoPF == false) && entity.NoPF == true)
            {
                PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(dbEntity.EmployeeId.Value, (byte)MonetoryChangeLogTypeEnum.NoPF,
                    "", "-", "Yes", LogActionEnum.Add));
            }
            else if ((dbEntity.NoPF != null && dbEntity.NoPF == true) && entity.NoPF == false)
            {
                PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(dbEntity.EmployeeId.Value, (byte)MonetoryChangeLogTypeEnum.NoPF,
                    "", "Yes", "No", LogActionEnum.Update));
            }

            //No Gratuity
            if ((dbEntity.ExcludeInGratuity == null || dbEntity.ExcludeInGratuity == false) && entity.ExcludeInGratuity == true)
            {
                PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(dbEntity.EmployeeId.Value, (byte)MonetoryChangeLogTypeEnum.NoGratuity,
                    "", "-", "Yes", LogActionEnum.Add));
            }
            else if ((dbEntity.ExcludeInGratuity != null && dbEntity.ExcludeInGratuity == true) && entity.ExcludeInGratuity == false)
            {
                PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(dbEntity.EmployeeId.Value, (byte)MonetoryChangeLogTypeEnum.NoGratuity,
                    "", "Yes", "No", LogActionEnum.Update));
            }

            //No Tax
            if ((dbEmp.NoTax == null || dbEmp.NoTax == false) && employee.NoTax == true)
            {
                PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(dbEntity.EmployeeId.Value, (byte)MonetoryChangeLogTypeEnum.NoTax,
                    "", "-", "Yes", LogActionEnum.Add));
            }
            else if ((dbEmp.NoTax != null && dbEmp.NoTax == true) && employee.NoTax == false)
            {
                PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(dbEntity.EmployeeId.Value, (byte)MonetoryChangeLogTypeEnum.NoTax,
                    "", "Yes", "No", LogActionEnum.Update));
            }

            //if (dbEntity.AdditionalPFDeduction != entity.AdditionalPFDeduction)
            //{
            //    PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(dbEntity.EmployeeId.Value, (byte)MonetoryChangeLogTypeEnum.Information,
            //        "Additional PF Deduction", dbEntity.AdditionalPFDeduction, entity.AdditionalPFDeduction, LogActionEnum.Update));
            //}

            //CIT checked
            if (dbEntity.HasCIT == false && entity.HasCIT == true)
            {
                PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(dbEntity.EmployeeId.Value, (byte)MonetoryChangeLogTypeEnum.CIT,
                    "", "-", mgr.GetCITValue(entity), LogActionEnum.Add));
            }
            else if (dbEntity.HasCIT == true && entity.HasCIT == true && mgr.GetCITValue(dbEntity) != mgr.GetCITValue(entity))
            {
                PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(dbEntity.EmployeeId.Value, (byte)MonetoryChangeLogTypeEnum.CIT,
                    "", mgr.GetCITValue(dbEntity), mgr.GetCITValue(entity), LogActionEnum.Update));
            }
            else if (dbEntity.HasCIT == true && entity.HasCIT == false)
            {

                PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(dbEntity.EmployeeId.Value, (byte)MonetoryChangeLogTypeEnum.CIT,
                    "", mgr.GetCITValue(dbEntity), "-", LogActionEnum.Delete));
            }

            //No    CIT checked
            if (dbEntity.NoCITContribution == false && entity.NoCITContribution == true)
            {
                PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(dbEntity.EmployeeId.Value, (byte)MonetoryChangeLogTypeEnum.NoCIT,
                    "", "-", EmployeeManager.GetCITDuration(entity), LogActionEnum.Add));
            }
            else if (dbEntity.NoCITContribution == true && entity.NoCITContribution == true && EmployeeManager.GetCITDuration(dbEntity) != EmployeeManager.GetCITDuration(entity))
            {
                PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(dbEntity.EmployeeId.Value, (byte)MonetoryChangeLogTypeEnum.NoCIT,
                    "", EmployeeManager.GetCITDuration(dbEntity), EmployeeManager.GetCITDuration(entity), LogActionEnum.Update));
            }
            else if (dbEntity.NoCITContribution == true && entity.NoCITContribution == false)
            {

                PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(dbEntity.EmployeeId.Value, (byte)MonetoryChangeLogTypeEnum.NoCIT,
                    "", EmployeeManager.GetCITDuration(dbEntity), "-", LogActionEnum.Delete));
            }

            
            #endregion

            dbEmp.NoTax = employee.NoTax;
            dbEntity.NoPF = entity.NoPF;
            dbEntity.HasCIT = entity.HasCIT;
            dbEntity.NoCITContribution = entity.NoCITContribution;
            dbEntity.CITEffectiveFromMonth = entity.CITEffectiveFromMonth;
            dbEntity.CITEffectiveFromYear = entity.CITEffectiveFromYear;
            dbEntity.CITAmount = entity.CITAmount;
            dbEntity.CITIsRate = entity.CITIsRate;
            dbEntity.CITRate = entity.CITRate;
            dbEntity.CITRateAdditionalAmount = entity.CITRateAdditionalAmount;
            dbEntity.NoCITFromMonth = entity.NoCITFromMonth;
            dbEntity.NoCITFromYear = entity.NoCITFromYear;
            dbEntity.NoCITToMonth = entity.NoCITToMonth;
            dbEntity.NoCITToYear = entity.NoCITToYear;
            dbEntity.CITCode = entity.CITCode;
            dbEntity.HasOtherFund = entity.HasOtherFund;
            dbEntity.HasOtherPFFund = entity.HasOtherPFFund;
            dbEntity.ExcludeInGratuity = entity.ExcludeInGratuity;
            //dbEntity.AdditionalPFDeduction = entity.AdditionalPFDeduction;
            if (dbEntity.NoCITContribution.Value)
            {
                dbEntity.NoCITFromEng = new CustomDate(1, dbEntity.NoCITFromMonth.Value, dbEntity.NoCITFromYear.Value, IsEnglish).EnglishDate;
                dbEntity.NoCITToEng = new CustomDate(DateHelper.GetTotalDaysInTheMonth(dbEntity.NoCITToYear.Value,dbEntity.NoCITToMonth.Value,IsEnglish)
                    , dbEntity.NoCITToMonth.Value, dbEntity.NoCITToYear.Value, IsEnglish).EnglishDate;
                   
            }

            PPay dbPay = PayrollDataContext.PPays.SingleOrDefault(x => x.EmployeeId == employee.EmployeeId);
            if (dbPay == null)
            {
                dbPay = new PPay();
                dbPay.EmployeeId = employee.EmployeeId;
                PayrollDataContext.PPays.InsertOnSubmit(dbPay);
            }

            #region "Change Logs"
            if (dbPay.PaymentMode != pay.PaymentMode)
            {
                PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(dbEntity.EmployeeId.Value, (byte)MonetoryChangeLogTypeEnum.Information,
                    "Payment Mode", dbPay.PaymentMode, pay.PaymentMode, LogActionEnum.Update));
            }
            if (dbEmp.IsEmployeeIncomeFromOtherSource != employee.IsEmployeeIncomeFromOtherSource)
            {
                PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(dbEntity.EmployeeId.Value, (byte)MonetoryChangeLogTypeEnum.Information,
                    "Income from Other Source", dbEmp.IsEmployeeIncomeFromOtherSource, employee.IsEmployeeIncomeFromOtherSource, LogActionEnum.Update));
            }
            //if (dbPay.BankName != pay.BankName)
            //{
            //    PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(dbEntity.EmployeeId.Value, (byte)MonetoryChangeLogTypeEnum.Information,
            //        "Bank Name", dbPay.BankName, pay.BankName, LogActionEnum.Update));
            //}
            if (dbPay.BankID != pay.BankID)
            {
                PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(dbEntity.EmployeeId.Value, (byte)MonetoryChangeLogTypeEnum.Information,
                    "Bank Name", dbPay.BankID, pay.BankID, LogActionEnum.Update));
            }
            if (dbPay.BankACNo != pay.BankACNo)
            {
                PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(dbEntity.EmployeeId.Value, (byte)MonetoryChangeLogTypeEnum.Information,
                    "Bank a/c", dbPay.BankACNo, pay.BankACNo, LogActionEnum.Update));
            }
            if (dbPay.LoanAccount != pay.LoanAccount)
            {
                PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(dbEntity.EmployeeId.Value, (byte)MonetoryChangeLogTypeEnum.Information,
                    "Loan Account", dbPay.LoanAccount, pay.LoanAccount, LogActionEnum.Update));
            }
            if (dbPay.AdvanceAccount != pay.AdvanceAccount)
            {
                PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(dbEntity.EmployeeId.Value, (byte)MonetoryChangeLogTypeEnum.Information,
                    "Advance Account", dbPay.AdvanceAccount, pay.AdvanceAccount, LogActionEnum.Update));
            }
          
            #endregion

            dbPay.PaymentMode = pay.PaymentMode;
            dbPay.BankID = pay.BankID;
            dbPay.BankName = pay.BankName;
            dbPay.BankACNo = pay.BankACNo;
            dbPay.AdvanceAccount = pay.AdvanceAccount;
            dbPay.LoanAccount = pay.LoanAccount;
            dbPay.BankBranchID = pay.BankBranchID;

            dbEmp.IsEmployeeIncomeFromOtherSource = employee.IsEmployeeIncomeFromOtherSource;

            if (pay.BankBranchID != null)
            {
                if (dbPay.BankID != PayManager.GetBankBranchList().FirstOrDefault(x => x.BankBranchID == pay.BankBranchID).BankID)
                {
                    status.ErrorMessage = "Selected bank branch does not come under the selected Bank.";
                    return status;
                }
            }

            UpdateChangeSet();

            return status;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="entity"></param>
        /// <returns>true if updated,false if not, as the record may have been deleted also</returns>
        public static bool Update(EEmployee entity)
        {
            bool isFirstStatusFromDateChanged = false;


            EEmployee dbEntity = PayrollDataContext.EEmployees.SingleOrDefault(c => c.EmployeeId == entity.EmployeeId);
            //may be deleted
            if (dbEntity == null)
                return false;


            //check for grade set in update then create in history table also
            dbEntity.BranchId = entity.BranchId;
            dbEntity.DepartmentId = entity.DepartmentId;            
            dbEntity.DesignationId = entity.DesignationId;

            if (dbEntity.EHumanResources.Count <= 0)
            {
                dbEntity.EHumanResources.Add(new EHumanResource { IdCardNo="" });
            }
            if (dbEntity.ECurrentStatus.Count <= 0)
            {
                dbEntity.ECurrentStatus.Add(new ECurrentStatus());
            }

            if (dbEntity.EHumanResources[0].SalaryCalculateFrom != entity.EHumanResources[0].SalaryCalculateFrom)
                dbEntity.EHumanResources[0].SalaryCalculateFromEng = GetEngDate(entity.EHumanResources[0].SalaryCalculateFrom, IsEnglish);

            dbEntity.EHumanResources[0].RetirementOptionType = entity.EHumanResources[0].RetirementOptionType;
            dbEntity.EHumanResources[0].SalaryCalculateFrom = entity.EHumanResources[0].SalaryCalculateFrom;
            dbEntity.EHumanResources[0].HireMethod = entity.EHumanResources[0].HireMethod;
            dbEntity.EHumanResources[0].AppointmentLetterDate = entity.EHumanResources[0].AppointmentLetterDate;
            dbEntity.EHumanResources[0].AppointmentLetterDateEng = entity.EHumanResources[0].AppointmentLetterDateEng;
            dbEntity.EHumanResources[0].AppointmentLetterNo = entity.EHumanResources[0].AppointmentLetterNo;

            dbEntity.ECurrentStatus.OrderBy(x => x.FromDateEng).First().CurrentStatus = entity.ECurrentStatus.OrderBy(x => x.FromDateEng).First().CurrentStatus;
            //check if first emp status from date is changed or not
            if (!string.IsNullOrEmpty(dbEntity.ECurrentStatus.OrderBy(x => x.FromDateEng).First().FromDate)
                && !dbEntity.ECurrentStatus.OrderBy(x => x.FromDateEng).First().FromDate.Equals(entity.ECurrentStatus.OrderBy(x => x.FromDateEng).First().FromDate))
            {
                isFirstStatusFromDateChanged = true;
                
            }

            // change from date of first branch transfer is exists
            BranchDepartmentHistory firstBranch = PayrollDataContext.BranchDepartmentHistories
                .Where(x => x.EmployeeId == entity.EmployeeId && x.IsFirst != null && x.IsFirst.Value).FirstOrDefault();
            if (firstBranch != null)
            {
                firstBranch.FromDate = entity.ECurrentStatus.OrderBy(x => x.FromDateEng).First().FromDate;
                firstBranch.FromDateEng = entity.ECurrentStatus.OrderBy(x => x.FromDateEng).First().FromDateEng;
                firstBranch.BranchId = dbEntity.BranchId;
                firstBranch.DepeartmentId = dbEntity.DepartmentId;
            }

            // change from date of first Designation if exists
            DesignationHistory firstDesignation = PayrollDataContext.DesignationHistories
                .Where(x => x.EmployeeId == entity.EmployeeId && x.IsFirst != null && x.IsFirst.Value).FirstOrDefault();
            if (firstDesignation != null)
            {
                firstDesignation.FromDate = entity.ECurrentStatus.OrderBy(x => x.FromDateEng).First().FromDate;
                firstDesignation.FromDateEng = entity.ECurrentStatus.OrderBy(x => x.FromDateEng).First().FromDateEng;
            }

            dbEntity.ECurrentStatus.OrderBy(x => x.FromDateEng).First().FromDate = entity.ECurrentStatus.OrderBy(x => x.FromDateEng).First().FromDate;
            dbEntity.ECurrentStatus.OrderBy(x => x.FromDateEng).First().FromDateEng = entity.ECurrentStatus.OrderBy(x => x.FromDateEng).First().FromDateEng;
            dbEntity.ECurrentStatus.OrderBy(x => x.FromDateEng).First().ToDate = entity.ECurrentStatus.OrderBy(x => x.FromDateEng).First().ToDate;
            dbEntity.ECurrentStatus.OrderBy(x => x.FromDateEng).First().ToDateEng = entity.ECurrentStatus.OrderBy(x => x.FromDateEng).First().ToDateEng;
            dbEntity.ECurrentStatus.OrderBy(x => x.FromDateEng).First().DefineToDate = entity.ECurrentStatus.OrderBy(x => x.FromDateEng).First().DefineToDate;

            //update for Group/Level/Step

            PIncome income = new PayManager().GetBasicIncome(SessionManager.CurrentCompanyId);
            PEmployeeIncome basicIncome = new PayManager().GetEmployeeIncome(entity.EmployeeId, income.IncomeId);

            if (basicIncome == null)
            {
                basicIncome = new PEmployeeIncome();
                basicIncome.IncomeId = income.IncomeId;
                basicIncome.IsValid = true;
                basicIncome.IsEnabled = true;
                dbEntity.PEmployeeIncomes.Add(basicIncome);
            }


            bool isLevelEditable = true;
            int groupID = 0, levelID = 0;
            double stepGrade = 0;
            NewPayrollManager.GetEmployeeFirstLevelGrade(entity.EmployeeId, ref isLevelEditable, ref groupID, ref levelID, ref stepGrade);
            /// IF level grade already changed or Employee used in payroll then Level/Grade not editable from this page       
            if (isLevelEditable)
            {
                //basicIncome.GroupId = entity.PEmployeeIncomes[0].GroupId;
                basicIncome.LevelId = entity.PEmployeeIncomes[0].LevelId;
                basicIncome.Step = entity.PEmployeeIncomes[0].Step;
            }

            if (basicIncome.Amount == null)
                basicIncome.Amount = 0;
            basicIncome.IsValid = true;

            

            dbEntity.Modified = GetCurrentDateAndTime();
            UpdateChangeSet();
            return true;
        }


        public static HDocument GetDocumentById(int levelId)
        {
            return PayrollDataContext.HDocuments.Where(x => x.DocumentId == levelId).First();
        }

        public static Status DeleteDocumentMember(int levelId)
        {
            Status status = new Status();

            HDocument dbEntity = PayrollDataContext.HDocuments.SingleOrDefault(x => x.DocumentId == levelId);

            PayrollDataContext.HDocuments.DeleteOnSubmit(dbEntity);
            PayrollDataContext.SubmitChanges();
            status.IsSuccess = true;
            return status;
        }

        public static int IsFamilyEditable(HFamily obj)
        {
            // make editable for Prabhu for some time
            if (CommonManager.CompanySetting.WhichCompany == WhichCompany.Prabhu)
                return 1;

            if (SessionManager.CurrentLoggedInEmployeeId == 0)
                return 1;

            if (obj.Status == 1)
                return 0;
            else
                return 1;

        }

        public static List<TaxDetail> GetTaxDetailList()
        {
            TaxDetail objTaxDetail = PayrollDataContext.TaxDetails.OrderByDescending(x => x.EffectiveDateEng).FirstOrDefault();
            if (objTaxDetail != null && objTaxDetail.EffectiveDateEng != null)
                return PayrollDataContext.TaxDetails.Where(x => x.EffectiveDateEng == objTaxDetail.EffectiveDateEng).OrderBy(x => x.Type).ToList();
            else
                return PayrollDataContext.TaxDetails.Where(x => x.EffectiveDateEng == null).OrderBy(x => x.Type).ToList();

            
        }

        public static Status SaveUpdateTaxDetails(List<TaxDetail> list)
        {
            Status status = new Status();

            List<TaxDetail> dbTaxDetailList = PayrollDataContext.TaxDetails.Where(x => x.EffectiveDateEng == list[0].EffectiveDateEng).ToList();
            if (dbTaxDetailList.Count > 0)
            {
                PayrollDataContext.TaxDetails.DeleteAllOnSubmit(dbTaxDetailList);
                PayrollDataContext.TaxDetails.InsertAllOnSubmit(list);

                status.ErrorMessage = "Taxt details updated successfully.";
            }
            else
            {
                status.ErrorMessage = "Taxt details saved successfully.";

               
                PayrollDataContext.TaxDetails.InsertAllOnSubmit(list);
            }

            PayrollDataContext.SubmitChanges();
            status.IsSuccess = true;

            return status;
        }


        public static bool AllowDecimalInGrade()
        {
            bool value = false;

            Setting setting = PayrollDataContext.Settings.SingleOrDefault();
            if (setting != null && setting.AllowDecimalInGrade != null && setting.AllowDecimalInGrade.Value)
                value = true;

            return value;
        }
    }
}
