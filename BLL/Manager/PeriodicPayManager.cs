﻿using System.Collections.Generic;
using System.Linq;
using DAL;
using Utils.Calendar;
using System;
using BLL.BO;

namespace BLL.Manager
{
    public class PeriodicPayManager : BaseBiz
    {

        public static PayGroupCalculation GetLastPay(int payGroupID)
        {
            return PayrollDataContext.PayGroupCalculations.Where(x=>x.PayGroupRef_ID==payGroupID) .OrderByDescending(x=>x.CalculationID)
                .FirstOrDefault();
        }

        public static PayGroup GetPayGroup(int id)
        {
            return PayrollDataContext.PayGroups.FirstOrDefault(x => x.PayGroupID == id);
        }
        public static PayGroupCalculation GetPayGroupCalculation(int id)
        {
            return PayrollDataContext.PayGroupCalculations.FirstOrDefault(x => x.CalculationID == id);
        }
        public static List<PayGroup> GetPayGroup()
        {
            return PayrollDataContext.PayGroups.OrderBy(x => x.Name).ToList();
        }

        public static PayGroupEmployee GetPeriodEmployee(int ein, int calculatinId)
        {
            return
                (
                from e in PayrollDataContext.PayGroupEmployees
                join c in PayrollDataContext.PayGroupCalculations on e.CalculationID equals c.CalculationID
                where e.EmployeeID == ein && c.CalculationID == calculatinId
                select e
                ).FirstOrDefault();
        }

        public static Status InsertUpdateGroup(PayGroup entity, bool isInsert)
        {
            Status status = new Status();


            if (isInsert)
            {


                PayrollDataContext.PayGroups.InsertOnSubmit(entity);

            }
            else
            {


                PayGroup dbEntity = PayrollDataContext.PayGroups.SingleOrDefault(x => x.PayGroupID == entity.PayGroupID);


                dbEntity.Name = entity.Name;
                dbEntity.Description = entity.Description;
                dbEntity.TDSRate = entity.TDSRate;
                dbEntity.IncomeId = entity.IncomeId;


            }

            PayrollDataContext.SubmitChanges();

            return status;
        }

        public static void DeleteSkipDate(PayGroupCalculation entity)
        {
            PayGroupCalculation dbEntity = PayrollDataContext.PayGroupCalculations
                .FirstOrDefault(x => x.CalculationID == entity.CalculationID);

            PayrollDataContext.PayGroupCalculations.DeleteOnSubmit(dbEntity);

            PayrollDataContext.SubmitChanges();
        }

        public static PayGroupCalculation GetDate(int id)
        {
            return PayrollDataContext.PayGroupCalculations
               .FirstOrDefault(x => x.CalculationID == id);
        }
        public static List<PayGroupCalculation> GetPayList(DateTime? start, DateTime? end)
        {
            List<PayGroupCalculation> list = 
                PayrollDataContext.PayGroupCalculations
                .Where(x => 
                        (start == null || x.StartDateEng >= start)
                        ||
                        (end == null || x.EndDateEng <= end)
                        )
                .OrderByDescending(x => x.StartDateEng).ToList();

            foreach (var item in list)
            {
                item.PayName = GetPayGroup(item.PayGroupRef_ID.Value).Name;
            }


            return list;
        }

        public static Status SaveUpdatePeriodicPay(PayGroupCalculation entity)
        {
            Status status = new Status();

            if (entity.EndDateEng < entity.StartDateEng)
            {
                status.ErrorMessage = "End date should be greater than start date.";
                return status;
            }

            PayGroupCalculation lastCalculation = null;

            if (entity.CalculationID == 0)
            {
                lastCalculation = PayrollDataContext.PayGroupCalculations.Where(x=>x.PayGroupRef_ID==entity.PayGroupRef_ID)
                    .OrderByDescending(x => x.CalculationID)
                    .FirstOrDefault();
            }
            else
            {
                lastCalculation = PayrollDataContext.PayGroupCalculations
                    .Where(x => x.PayGroupRef_ID == entity.PayGroupRef_ID)
                    .OrderByDescending(x => x.CalculationID)
                    .FirstOrDefault(x => x.CalculationID < entity.CalculationID);
            }

            if (lastCalculation != null)
            {
                DateTime validStartDate = lastCalculation.EndDateEng.Value.AddDays(1);

                if (entity.StartDateEng.Value.Date.Equals(validStartDate.Date) == false)
                {
                    status.ErrorMessage = "Start date must be " + validStartDate.ToString("yyyy/MMM/dd") + ".";
                    return status;
                }
            }


            PayGroupCalculation dbEntity = PayrollDataContext.PayGroupCalculations
                .FirstOrDefault(x => x.CalculationID == entity.CalculationID);

          

            if (dbEntity == null)
            {


                PayrollDataContext.PayGroupCalculations.InsertOnSubmit(entity);
            }
            else
            {

                dbEntity.StartDate = entity.StartDate;
                dbEntity.StartDateEng = entity.StartDateEng;
                dbEntity.EndDate = entity.EndDate;
                dbEntity.EndDateEng = entity.EndDateEng;

                dbEntity.PayGroupRef_ID = entity.PayGroupRef_ID;
                dbEntity.NepName = entity.NepName;
                dbEntity.EngName = entity.EngName;
            }

            SaveChangeSet();

            return status;
        }

        public static Status DeleteGroup(int groupLevelId)
        {
            Status status = new Status();

            PayGroup dbEntity = PayrollDataContext.PayGroups.SingleOrDefault(x => x.PayGroupID == groupLevelId);

            if (PayrollDataContext.EEmployees.Any(x => x.PayGroupRef_ID == groupLevelId))
            {
                status.ErrorMessage = "Group is assigned to the employee.";
                return status;
            }


            PayrollDataContext.PayGroups.DeleteOnSubmit(dbEntity);
            PayrollDataContext.SubmitChanges();
            return status;
        }

        public static Status UpdatePayDays(int ein, int calcId, double payDays)
        {
            Status status = new Status();

            PayGroupEmployee emp = GetPeriodEmployee(ein, calcId);
            if (emp != null)
            {
                PayGroupCalculation calc = PayrollDataContext.PayGroupCalculations.FirstOrDefault(x => x.CalculationID == emp.CalculationID);
                if (calc != null && calc.IsFinalSaved != null && calc.IsFinalSaved.Value)
                {
                    status.ErrorMessage = "Locked period can not be changed.";
                    return status;
                }

                int validDays = ((calc.EndDateEng.Value - calc.StartDateEng.Value).Days + 1);
                if (payDays > validDays)
                {
                    status.ErrorMessage = "Maximum days can be upto " + payDays + " days only.";
                    return status;
                }

                emp.AdjustmentDays = payDays - emp.PresentDays.Value;
                emp.PayDays = payDays;
                emp.GrossAmount = (decimal)payDays * emp.Rate.Value;

                PayGroup pay = PayrollDataContext.PayGroups.FirstOrDefault(x => x.PayGroupID == calc.PayGroupRef_ID);

                emp.TDS = (decimal)(pay.TDSRate.Value / 100.0) * emp.GrossAmount.Value;
                emp.NetPay = emp.GrossAmount - emp.TDS;

                PayrollDataContext.SubmitChanges();

            }
            else
            {
                status.ErrorMessage = "Employee does not exists.";
            }
            return status;
        }
        public static Status LockLastPay(int calculationId)
        {
            PayGroupCalculation group= PayrollDataContext.PayGroupCalculations.OrderByDescending(x => x.CalculationID)
                .FirstOrDefault(x => x.CalculationID == calculationId);

            if (group.IsFinalSaved != null && group.IsFinalSaved.Value) { }
            else
            {
                group.IsFinalSaved = true;
            }

            PayrollDataContext.SubmitChanges();

            Status status = new Status();
            return status;

        }
        public static int GetTotalPay()
        {
            return PayrollDataContext.PayGroupCalculations.Count();
        }
    }
}
