﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Transactions;
using DAL;
using Utils.Calendar;
using System;
using System.Web.UI.WebControls;
using Utils;
using System.Web;
using Utils.Helper;
using Microsoft.SqlServer.Management.Smo;
using Microsoft.SqlServer.Management.Common;
using System.IO;
using System.IO.Compression;
using BLL.BO;
using System.Text;


namespace BLL.Manager
{
    public class ListManager : BaseBiz
    {
        public static List<FixedValueFamilyRelation> GetFamilyRelations()
        {
            return PayrollDataContext.FixedValueFamilyRelations.OrderBy(x => x.Name).ToList();
        }

        public static Status SaveUpdateRelation(FixedValueFamilyRelation obj)
        {
            Status status = new Status();

            if (PayrollDataContext.FixedValueFamilyRelations.Any(x => x.Name.ToLower() == obj.Name.ToLower() && x.ID != obj.ID))
            {
                status.ErrorMessage = "Family relation already exists.";
                status.IsSuccess = false;
                return status;
            }

            if (obj.ID.Equals(0))
            {
                PayrollDataContext.FixedValueFamilyRelations.InsertOnSubmit(obj);
                PayrollDataContext.SubmitChanges();
                status.IsSuccess = true;
            }
            else
            {
                FixedValueFamilyRelation dbEntity = PayrollDataContext.FixedValueFamilyRelations.SingleOrDefault(x => x.ID == obj.ID);
                dbEntity.Name = obj.Name;
                dbEntity.ShowBirthdayInFamilyListReport = obj.ShowBirthdayInFamilyListReport;
                PayrollDataContext.SubmitChanges();
                status.IsSuccess = true;
            }

            return status;
        }

        public static FixedValueFamilyRelation GetFamilyRelationById(int Id)
        {
            return PayrollDataContext.FixedValueFamilyRelations.SingleOrDefault(x => x.ID == Id);
        }

        public static Status  DeleteFixedValueFamilyReln(int id)
        {
            Status status = new Status();

            if (PayrollDataContext.HFamilies.Any(x => x.RelationID == id))
            {
                status.ErrorMessage = "Family relation is in use, cannot be deleted.";
                status.IsSuccess = false;
                return status;
            }

            FixedValueFamilyRelation dbEntity = PayrollDataContext.FixedValueFamilyRelations.SingleOrDefault(x => x.ID == id);
            PayrollDataContext.FixedValueFamilyRelations.DeleteOnSubmit(dbEntity);
            PayrollDataContext.SubmitChanges();
            status.IsSuccess = true;

            return status;
        }

        public static List<ELocation> GetELocationList()
        {
            return PayrollDataContext.ELocations.Where(x => x.CompanyId == SessionManager.CurrentCompanyId).OrderBy(x => x.Name).ToList();
        }
        public static List<ELocation> GetELocationListWithRemoteArea()
        {
            List<ELocation> list = PayrollDataContext.ELocations.Where(x => x.CompanyId == SessionManager.CurrentCompanyId).OrderBy(x => x.Name).ToList();

            foreach (ELocation item in list)
            {
                if (item.RemoteAreaId != null)
                    item.RemoteArea = PayrollDataContext.RemoteAreas.FirstOrDefault(x => x.RemoteAreaId == item.RemoteAreaId.Value).Value;
            }

            return list;
        }
        public static List<RemoteArea> GetRemoteAreaWithNameAndAmount()
        {
            List<RemoteArea> list = PayrollDataContext.RemoteAreas.OrderBy(x => x.Value).ToList();

            foreach (RemoteArea item in list)
            {
                item.Name = item.Value + " (" + GetCurrency(item.Deduction) + ")";
            }

            return list;
        }
        public static Status SaveUpdateELocation(ELocation obj)
        {
            Status status = new Status();

            if (PayrollDataContext.ELocations.Any(x => x.Name.ToLower() == obj.Name.ToLower() && x.LocationId != obj.LocationId))
            {
                status.ErrorMessage = "ELocation name already exists.";
                status.IsSuccess = false;
                return status;
            }

            if (obj.LocationId.Equals(0))
            {
                PayrollDataContext.ELocations.InsertOnSubmit(obj);
                PayrollDataContext.SubmitChanges();
                status.IsSuccess = true;
            }
            else
            {
                ELocation dbEntity = PayrollDataContext.ELocations.SingleOrDefault(x => x.LocationId == obj.LocationId);
                dbEntity.Name = obj.Name;
                dbEntity.RemoteAreaId = obj.RemoteAreaId;
                dbEntity.CompanyId = SessionManager.CurrentCompanyId;
                PayrollDataContext.SubmitChanges();
                status.IsSuccess = true;
            }

            return status;
        }

        public static ELocation GetELocationById(int locationId)
        {
            return PayrollDataContext.ELocations.SingleOrDefault(x => x.LocationId == locationId);
        }

        public static Status DeleteELocation(int locationId)
        {
            Status status = new Status();

            var list = (from emp in PayrollDataContext.EEmployees
                        join lh in PayrollDataContext.ELocationHistories on emp.EmployeeId equals lh.EmployeeId
                        where emp.CompanyId == SessionManager.CurrentCompanyId && lh.LocationId == locationId
                        select lh).ToList();

            if(list.Count > 0)
            {
                status.ErrorMessage = "ELocation is in use, cannot be deleted.";
                status.IsSuccess = false;
                return status;
            }

            ELocation dbEntity = PayrollDataContext.ELocations.SingleOrDefault(x => x.LocationId == locationId);
            PayrollDataContext.ELocations.DeleteOnSubmit(dbEntity);
            PayrollDataContext.SubmitChanges();
            status.IsSuccess = true;            

            return status;
        }

        public static List<ExperienceCategory> GetExperienceCategory()
        {
            return PayrollDataContext.ExperienceCategories.OrderBy(x => x.Name).ToList();
        }

        public static Status SaveUpdateExperienceCategory(ExperienceCategory obj)
        {
            Status status = new Status();

            if (PayrollDataContext.ExperienceCategories.Any(x => x.Name.ToLower() == obj.Name.ToLower() && x.CategoryID != obj.CategoryID))
            {
                status.ErrorMessage = "Experience category already exists.";
                status.IsSuccess = false;
                return status;
            }

            if (obj.CategoryID.Equals(0))
            {
                PayrollDataContext.ExperienceCategories.InsertOnSubmit(obj);
                PayrollDataContext.SubmitChanges();
                status.IsSuccess = true;
            }
            else
            {
                ExperienceCategory dbEntity = PayrollDataContext.ExperienceCategories.SingleOrDefault(x => x.CategoryID == obj.CategoryID);
                dbEntity.Name = obj.Name;
                PayrollDataContext.SubmitChanges();
                status.IsSuccess = true;
            }

            return status;
        }

        public static ExperienceCategory GetExerienceCategoryById(int categoryId)
        {
            return PayrollDataContext.ExperienceCategories.SingleOrDefault(x => x.CategoryID == categoryId);
        }

        public static Status DeleteExperienceCategory(int categoryId)
        {
            Status status = new Status();

            if (PayrollDataContext.HPreviousEmployments.Any(x => x.CategoryID == categoryId))
            {
                status.ErrorMessage = "Experience category is in use, cannot be deleted.";
                status.IsSuccess = false;
                return status;
            }

            ExperienceCategory dbEntity = PayrollDataContext.ExperienceCategories.SingleOrDefault(x => x.CategoryID == categoryId);
            PayrollDataContext.ExperienceCategories.DeleteOnSubmit(dbEntity);
            PayrollDataContext.SubmitChanges();
            status.IsSuccess = true;
            return status;
        }

        public static List<HBloodGroup> GetHBloodGroups()
        {
            return PayrollDataContext.HBloodGroups.OrderBy(x => x.BloodGroupName).ToList();
        }
        
        public static Status SaveUpdateHBloodGroup(HBloodGroup obj)
        {
            Status status = new Status();

            if (PayrollDataContext.HBloodGroups.Any(x => x.BloodGroupName.ToLower() == obj.BloodGroupName.ToLower() && x.Id != obj.Id))
            {
                status.ErrorMessage = "Blood group name already exists.";
                status.IsSuccess = false;
                return status;
            }

            if (obj.Id.Equals(0))
            {
                PayrollDataContext.HBloodGroups.InsertOnSubmit(obj);
                PayrollDataContext.SubmitChanges();
                status.IsSuccess = true;
            }
            else
            {
                HBloodGroup dbEntity = PayrollDataContext.HBloodGroups.SingleOrDefault(x => x.Id == obj.Id);
                dbEntity.BloodGroupName = obj.BloodGroupName;
                PayrollDataContext.SubmitChanges();
                status.IsSuccess = true;
            }

            return status;
        }

        public static HBloodGroup GetHBloodGroupById(int id)
        {
            return PayrollDataContext.HBloodGroups.SingleOrDefault(x => x.Id == id);
        }
        
        public static Status DeleteHBloodGroupById(int id)
        {
            Status status = new Status();

            HBloodGroup dbEntity = PayrollDataContext.HBloodGroups.SingleOrDefault(x => x.Id == id);

            if (PayrollDataContext.HHumanResources.Any(x => x.BloodGroup == dbEntity.BloodGroupName))
            {
                status.ErrorMessage = "Blood group is in use, cannot be deleted.";
                status.IsSuccess = false;
                return status;
            }


            PayrollDataContext.HBloodGroups.DeleteOnSubmit(dbEntity);
            PayrollDataContext.SubmitChanges();
            status.IsSuccess = true;

            return status;
        }

        public static List<DocumentDocumentType> GetDocumentDocumentTypes()
        {
            return PayrollDataContext.DocumentDocumentTypes.OrderBy(x => x.DocumentTypeName).ToList();
        }

        public static Status SaveUpdateDocumentDocumentType(DocumentDocumentType obj)
        {
            Status status = new Status();

            if (PayrollDataContext.DocumentDocumentTypes.Any(x => x.DocumentTypeName.ToLower() == obj.DocumentTypeName.ToLower() && x.DocumentTypeId != obj.DocumentTypeId))
            {
                status.ErrorMessage = "Document type name already exists.";
                status.IsSuccess = false;
                return status;
            }

            if (obj.DocumentTypeId.Equals(0))
            {
                PayrollDataContext.DocumentDocumentTypes.InsertOnSubmit(obj);
                PayrollDataContext.SubmitChanges();
                status.IsSuccess = true;
            }
            else
            {
                DocumentDocumentType dbEntity = PayrollDataContext.DocumentDocumentTypes.SingleOrDefault(x => x.DocumentTypeId == obj.DocumentTypeId);
                dbEntity.DocumentTypeName = obj.DocumentTypeName;
                PayrollDataContext.SubmitChanges();
                status.IsSuccess = true;
            }

            return status;
        }

        public static DocumentDocumentType GetDocumentDocumentTypeById(int documentTypeId)
        {
            return PayrollDataContext.DocumentDocumentTypes.SingleOrDefault(x => x.DocumentTypeId == documentTypeId);
        }

        public static Status DeleteDocumentDocumentType(int documentTypeId)
        {
            Status status = new Status();

            if (PayrollDataContext.HDocuments.Any(x => x.DocumentType == documentTypeId))
            {
                status.ErrorMessage = "Document type is in use, cannot be deleted.";
                status.IsSuccess = false;
                return status;
            }

            DocumentDocumentType dbEntity = PayrollDataContext.DocumentDocumentTypes.SingleOrDefault(x => x.DocumentTypeId == documentTypeId);
            PayrollDataContext.DocumentDocumentTypes.DeleteOnSubmit(dbEntity);
            PayrollDataContext.SubmitChanges();
            status.IsSuccess = true;

            return status;
        }

        public static List<HealthCondition> GetHealthConditionTypes()
        {
            return PayrollDataContext.HealthConditions.OrderBy(x => x.ConditionTypeName).ToList();
        }

        public static Status SaveUpdateHealthConditionType(HealthCondition obj)
        {
            Status status = new Status();

            if (PayrollDataContext.HealthConditions.Any(x => x.ConditionTypeName.ToLower() == obj.ConditionTypeName.ToLower() && x.ConditionTypeId != obj.ConditionTypeId))
            {
                status.ErrorMessage = "Health condition type name already exists.";
                status.IsSuccess = false;
                return status;
            }

            if (obj.ConditionTypeId.Equals(0))
            {
                PayrollDataContext.HealthConditions.InsertOnSubmit(obj);
                PayrollDataContext.SubmitChanges();
                status.IsSuccess = true;
            }
            else
            {
                HealthCondition dbEntity = PayrollDataContext.HealthConditions.SingleOrDefault(x => x.ConditionTypeId == obj.ConditionTypeId);
                dbEntity.ConditionTypeName = obj.ConditionTypeName;
                PayrollDataContext.SubmitChanges();
                status.IsSuccess = true;
            }

            return status;
        }

        public static HealthCondition GetHealthConditionById(int conditionTypeId)
        {
            return PayrollDataContext.HealthConditions.SingleOrDefault(x => x.ConditionTypeId == conditionTypeId);
        }
        
        public static Status DeleteHealthConditionType(int conditionTypeId)
        {
            Status status = new Status();

            if (PayrollDataContext.HHealths.Any(x => x.ContitionTypeID == conditionTypeId))
            {
                status.ErrorMessage = "Health condition type is in use, cannot be deleted.";
                status.IsSuccess = false;
                return status;
            }

            HealthCondition dbEntity = PayrollDataContext.HealthConditions.SingleOrDefault(x => x.ConditionTypeId == conditionTypeId);
            PayrollDataContext.HealthConditions.DeleteOnSubmit(dbEntity);
            PayrollDataContext.SubmitChanges();
            status.IsSuccess = true;

            return status;
        }

        public static List<CitizenNationality> GetCitizenNationalities()
        {
            return PayrollDataContext.CitizenNationalities.OrderBy(x => x.Nationality).ToList();
        }
        
        public static Status SaveUpdateCitizenNationality(CitizenNationality obj)
        {
            Status status = new Status();

            if (PayrollDataContext.CitizenNationalities.Any(x => x.Nationality.ToLower() == obj.Nationality.ToLower() && x.NationalityId != obj.NationalityId))
            {
                status.ErrorMessage = "Nationality already exists.";
                status.IsSuccess = false;
                return status;
            }

            if (obj.NationalityId.Equals(0))
            {
                PayrollDataContext.CitizenNationalities.InsertOnSubmit(obj);
                PayrollDataContext.SubmitChanges();
                status.IsSuccess = true;
            }
            else
            {
                CitizenNationality dbEntity = PayrollDataContext.CitizenNationalities.SingleOrDefault(x => x.NationalityId == obj.NationalityId);
                dbEntity.Nationality = obj.Nationality;
                PayrollDataContext.SubmitChanges();
                status.IsSuccess = true;
            }

            return status;
        }

        public static CitizenNationality GetCitizenNationalityById(int nationalityId)
        {
            return PayrollDataContext.CitizenNationalities.SingleOrDefault(x => x.NationalityId == nationalityId);
        }
        
        public static Status DeleteCitizenNationality(int nationalityId)
        {
            Status status = new Status();
            string nationality = ListManager.GetCitizenNationalityById(nationalityId).Nationality;

            if (PayrollDataContext.HCitizenships.Any(x => x.Nationality == nationality))
            {
                status.ErrorMessage = "Nationality is in use, cannot be deleted.";
                status.IsSuccess = false;
                return status;
            }

            CitizenNationality dbEntity = PayrollDataContext.CitizenNationalities.SingleOrDefault(x => x.NationalityId == nationalityId);
            PayrollDataContext.CitizenNationalities.DeleteOnSubmit(dbEntity);
            PayrollDataContext.SubmitChanges();
            status.IsSuccess = true;

            return status;
        }

        public static List<TrainingType> GetTrainingTypes()
        {
            return PayrollDataContext.TrainingTypes.OrderBy(x => x.TrainingTypeName).ToList();
        }
        public static List<FunctionalTitle> GetFunctionalTitleType()
        {
            return PayrollDataContext.FunctionalTitles.OrderBy(x => x.Name).ToList();
        }
        public static FunctionalTitle GetFunctionalTitle(int id)
        {
            return PayrollDataContext.FunctionalTitles.FirstOrDefault(x => x.FunctionalTitleId == id);
        }
        public static Status SaveUpdateTrainingType(TrainingType obj)
        {
            Status status = new Status();

            if (PayrollDataContext.TrainingTypes.Any(x => x.TrainingTypeName.ToLower() == obj.TrainingTypeName.ToLower() && x.TrainingTypeId != obj.TrainingTypeId))
            {
                status.ErrorMessage = "Training type already exists.";
                status.IsSuccess = false;
                return status;
            }

            if (obj.TrainingTypeId.Equals(0))
            {
                PayrollDataContext.TrainingTypes.InsertOnSubmit(obj);
                PayrollDataContext.SubmitChanges();
                status.IsSuccess = true;
            }
            else
            {
                TrainingType dbEntity = PayrollDataContext.TrainingTypes.SingleOrDefault(x => x.TrainingTypeId == obj.TrainingTypeId);
                dbEntity.TrainingTypeName = obj.TrainingTypeName;
                PayrollDataContext.SubmitChanges();
                status.IsSuccess = true;
            }

            return status;
        }
        public static Status SaveUpdateFunctionTitle(FunctionalTitle obj)
        {
            Status status = new Status();

            if (PayrollDataContext.FunctionalTitles.Any(x => x.Name.ToLower() == obj.Name.ToLower() && x.FunctionalTitleId != obj.FunctionalTitleId))
            {
                status.ErrorMessage = "Type already exists.";
                status.IsSuccess = false;
                return status;
            }

            if (obj.FunctionalTitleId.Equals(0))
            {
                PayrollDataContext.FunctionalTitles.InsertOnSubmit(obj);
                PayrollDataContext.SubmitChanges();
                status.IsSuccess = true;
            }
            else
            {
                FunctionalTitle dbEntity = PayrollDataContext.FunctionalTitles.SingleOrDefault(x => x.FunctionalTitleId == obj.FunctionalTitleId);
                dbEntity.Name = obj.Name;
                PayrollDataContext.SubmitChanges();
                status.IsSuccess = true;
            }

            return status;
        }
        public static TrainingType GetTrainingTypeById(int trainingTypeId)
        {
            return PayrollDataContext.TrainingTypes.SingleOrDefault(x => x.TrainingTypeId == trainingTypeId);
        }
        public static FunctionalTitle GetTypeById(int typeid)
        {
            return PayrollDataContext.FunctionalTitles.SingleOrDefault(x => x.FunctionalTitleId == typeid);
        }
        public static Status DeleteTrainingType(int trainingTypeId)
        {
            Status status = new Status();

            if (PayrollDataContext.HTrainings.Any(x => x.TrainingTypeID == trainingTypeId))
            {
                status.ErrorMessage = "Training type is in use, cannot be deleted.";
                status.IsSuccess = false;
                return status;
            }

            TrainingType dbEntity = PayrollDataContext.TrainingTypes.SingleOrDefault(x => x.TrainingTypeId == trainingTypeId);
            PayrollDataContext.TrainingTypes.DeleteOnSubmit(dbEntity);
            PayrollDataContext.SubmitChanges();
            status.IsSuccess = true;

            return status;
        }
        public static Status DeleteFunctionTitleType(int typeid)
        {
            Status status = new Status();

            if (PayrollDataContext.EEmployees.Any(x => x.FunctionalTitleId == typeid))
            {
                status.ErrorMessage = "Type is in use, cannot be deleted.";
                status.IsSuccess = false;
                return status;
            }

            FunctionalTitle dbEntity = PayrollDataContext.FunctionalTitles.SingleOrDefault(x => x.FunctionalTitleId == typeid);
            PayrollDataContext.FunctionalTitles.DeleteOnSubmit(dbEntity);
            PayrollDataContext.SubmitChanges();
            status.IsSuccess = true;

            return status;
        }
        public static int LocationHistoryCountByEmployeeId(int employeeId)
        {
            int count = 0;

            List<ELocationHistory> list = PayrollDataContext.ELocationHistories.Where(x => x.EmployeeId == employeeId).ToList();
            if (list.Count > 0)
                count = list.Count;
            return count;
        }

        public static int GradeHistoryCountByEmployeeId(int employeeId)
        {
            int count = 0;

            List<EGradeHistory> list = PayrollDataContext.EGradeHistories.Where(x => x.EmployeeId == employeeId).ToList();
            if (list.Count > 0)
                count = list.Count;
            return count;
        }

        public static List<PublicationType> GetPublicationTypes()
        {
            return PayrollDataContext.PublicationTypes.OrderBy(x => x.PublicationTypeName).ToList();
        }


        public static List<ReportNewHR_GetTerminationListResult> GetEmployeeTerminations(int StartRow, int PageSize, string Sort, int BranchID, int DepartmentID, DateTime Date, DateTime Date1)
        {
            List<ReportNewHR_GetTerminationListResult> list = PayrollDataContext.ReportNewHR_GetTerminationList(StartRow, PageSize, Sort, SessionManager.CurrentCompanyId, BranchID, DepartmentID, Date, Date1).ToList();
            return list;
        }


        public static Status SaveUpdatePublicationType(PublicationType obj)
        {
            Status status = new Status();

            if (PayrollDataContext.PublicationTypes.Any(x => x.PublicationTypeName.ToLower() == obj.PublicationTypeName.ToLower() && x.PublicationTypeID != obj.PublicationTypeID))
            {
                status.ErrorMessage = "Publication type already exists.";
                status.IsSuccess = false;
                return status;
            }

            if (obj.PublicationTypeID.Equals(0))
            {
                PayrollDataContext.PublicationTypes.InsertOnSubmit(obj);
                PayrollDataContext.SubmitChanges();
                status.IsSuccess = true;
            }
            else
            {
                PublicationType dbEntity = PayrollDataContext.PublicationTypes.SingleOrDefault(x => x.PublicationTypeID == obj.PublicationTypeID);
                dbEntity.PublicationTypeName = obj.PublicationTypeName;
                PayrollDataContext.SubmitChanges();
                status.IsSuccess = true;
            }

            return status;
        }

        public static PublicationType GetPublicationTypeById(int publicationTypeId)
        {
            return PayrollDataContext.PublicationTypes.SingleOrDefault(x => x.PublicationTypeID == publicationTypeId);
        }

        public static Status DeletePublicationType(int publicationTypeId)
        {
            Status status = new Status();

            if (PayrollDataContext.HPublications.Any(x => x.PublicationTypeID == publicationTypeId))
            {
                status.ErrorMessage = "Publication type is in use, cannot be deleted.";
                status.IsSuccess = false;
                return status;
            }

            PublicationType dbEntity = PayrollDataContext.PublicationTypes.SingleOrDefault(x => x.PublicationTypeID == publicationTypeId);
            PayrollDataContext.PublicationTypes.DeleteOnSubmit(dbEntity);
            PayrollDataContext.SubmitChanges();
            status.IsSuccess = true;

            return status;
        }

        public static List<HireMethod> GetHireMethods()
        {
            return PayrollDataContext.HireMethods.OrderBy(x => x.HireMethodName).ToList();
        }

        public static Status SaveUpdateHireMethods(HireMethod obj)
        {
            Status status = new Status();

            if(PayrollDataContext.HireMethods.Any(x => x.HireMethodName.ToLower() == obj.HireMethodName.ToLower() && x.HireMethodId != obj.HireMethodId))
            {
                status.ErrorMessage = "Hire method name already exists.";
                status.IsSuccess = false;
                return status;
            }

            if(obj.HireMethodId.Equals(0))
            {
                PayrollDataContext.HireMethods.InsertOnSubmit(obj);
                PayrollDataContext.SubmitChanges();
                status.IsSuccess = true;
            }
            else
            {
                HireMethod dbEntity = PayrollDataContext.HireMethods.SingleOrDefault(x => x.HireMethodId == obj.HireMethodId);
                dbEntity.HireMethodName = obj.HireMethodName;
                PayrollDataContext.SubmitChanges();
                status.IsSuccess = true;
            }

            return status;
        }

        public static HireMethod GetHireMethodById(int hireMethodId)
        {
            return PayrollDataContext.HireMethods.SingleOrDefault(x => x.HireMethodId == hireMethodId);
        }

        public static Status DeleteHireMethod(int hireMethodId)
        {
            Status status = new Status();

            if (PayrollDataContext.EHumanResources.Any(x => x.HireMethod == hireMethodId))
            {
                status.ErrorMessage = "Hire method is in use, cannot be deleted.";
                status.IsSuccess = false;
                return status;
            }

            HireMethod dbEntity = PayrollDataContext.HireMethods.SingleOrDefault(x => x.HireMethodId == hireMethodId);
            PayrollDataContext.HireMethods.DeleteOnSubmit(dbEntity);
            PayrollDataContext.SubmitChanges();
            status.IsSuccess = true;

            return status;
        }

        public static List<HReligion> GetReligions()
        {
            return PayrollDataContext.HReligions.OrderBy(x => x.ReligionName).ToList();
        }

        public static Status SaveUpdateReligion(HReligion obj)
        {
            Status status = new Status();

            if (PayrollDataContext.HReligions.Any(x => x.ReligionName.ToLower() == obj.ReligionName.ToLower() && x.ReligionId != obj.ReligionId))
            {
                status.ErrorMessage = "Religion name already exists.";
                status.IsSuccess = false;
                return status;
            }

            if (obj.ReligionId.Equals(0))
            {
                PayrollDataContext.HReligions.InsertOnSubmit(obj);
                PayrollDataContext.SubmitChanges();
                status.IsSuccess = true;
            }
            else
            {
                HReligion dbEntity = PayrollDataContext.HReligions.SingleOrDefault(x => x.ReligionId == obj.ReligionId);
                dbEntity.ReligionName = obj.ReligionName;
                PayrollDataContext.SubmitChanges();
                status.IsSuccess = true;
            }

            return status;
        }

        public static HReligion GetReligionById(int religionId)
        {
            return PayrollDataContext.HReligions.SingleOrDefault(x => x.ReligionId == religionId);
        }

        public static Status DeleteReligion(int religionId)
        {
            Status status = new Status();
            
            string religionName = ListManager.GetReligionById(religionId).ReligionName;

            if (PayrollDataContext.HHumanResources.Any(x => x.Religion == religionName))
            {
                status.ErrorMessage = "Religion is in use, cannot be deleted.";
                status.IsSuccess = false;
                return status;
            }

            HReligion dbEntity = PayrollDataContext.HReligions.SingleOrDefault(x => x.ReligionId == religionId);
            PayrollDataContext.HReligions.DeleteOnSubmit(dbEntity);
            PayrollDataContext.SubmitChanges();
            status.IsSuccess = true;

            return status;
        }

        public static List<HCaste> GetCastes()
        {
            return PayrollDataContext.HCastes.OrderBy(x => x.CasteName).ToList();
        }

        public static Status SaveUpdateCasteEthnicity(HCaste obj)
        {
            Status status = new Status();

            if (PayrollDataContext.HCastes.Any(x => x.CasteName.ToLower() == obj.CasteName.ToLower() && x.CasteId != obj.CasteId))
            {
                status.ErrorMessage = "Caste/Ethnicity name already exists.";
                status.IsSuccess = false;
                return status;
            }

            if (obj.CasteId.Equals(0))
            {
                PayrollDataContext.HCastes.InsertOnSubmit(obj);
                PayrollDataContext.SubmitChanges();
                status.IsSuccess = true;
            }
            else
            {
                HCaste dbEntity = PayrollDataContext.HCastes.SingleOrDefault(x => x.CasteId == obj.CasteId);
                dbEntity.CasteName = obj.CasteName;
                PayrollDataContext.SubmitChanges();
                status.IsSuccess = true;
            }

            return status;
        }

        public static HCaste GetCasteById(int casteId)
        {
            return PayrollDataContext.HCastes.SingleOrDefault(x => x.CasteId == casteId);
        }

        public static Status DeleteCasteEthnicity(int casteId)
        {
            Status status = new Status();

            if (PayrollDataContext.HHumanResources.Any(x => x.CasteId == casteId))
            {
                status.ErrorMessage = "Caste/Ethnicity is in use, cannot be deleted.";
                status.IsSuccess = false;
                return status;
            }

            HCaste dbEntity = PayrollDataContext.HCastes.SingleOrDefault(x => x.CasteId == casteId);
            PayrollDataContext.HCastes.DeleteOnSubmit(dbEntity);
            PayrollDataContext.SubmitChanges();
            status.IsSuccess = true;

            return status;
        }
        
        public static List<EGrade> GetGrades()
        {
            return PayrollDataContext.EGrades.OrderBy(x => x.Name).ToList();
        }
        
        public static Status SaveUpdateGrade(EGrade obj)
        {
            Status status = new Status();

            if (PayrollDataContext.EGrades.Any(x => x.Name.ToLower() == obj.Name.ToLower() && x.GradeId != obj.GradeId))
            {
                status.ErrorMessage = "Grade/Step name already exists.";
                status.IsSuccess = false;
                return status;
            }

            if (obj.GradeId.Equals(0))
            {
                PayrollDataContext.EGrades.InsertOnSubmit(obj);
                PayrollDataContext.SubmitChanges();
                status.IsSuccess = true;
            }
            else
            {
                EGrade dbEntity = PayrollDataContext.EGrades.SingleOrDefault(x => x.GradeId == obj.GradeId);
                dbEntity.Name = obj.Name;
                dbEntity.CompanyId = obj.CompanyId;
                dbEntity.Order = obj.Order;
                PayrollDataContext.SubmitChanges();
                status.IsSuccess = true;
            }

            return status;
        }

        public static EGrade GetEGradeById(int gradeId)
        {
            return PayrollDataContext.EGrades.SingleOrDefault(x => x.GradeId == gradeId);
        }
        
        public static Status DeleteGrade(int gradeId)
        {
            Status status = new Status();

            if (PayrollDataContext.EEmployees.Any(x => x.GradeId == gradeId))
            {
                status.ErrorMessage = "Grade is in use, cannot be deleted.";
                status.IsSuccess = false;
                return status;
            }

            EGrade dbEntity = PayrollDataContext.EGrades.SingleOrDefault(x => x.GradeId == gradeId);
            PayrollDataContext.EGrades.DeleteOnSubmit(dbEntity);
            PayrollDataContext.SubmitChanges();
            status.IsSuccess = true;

            return status;
        }

        public static List<SubDepartment> GetSubDepartments()
        {
            return PayrollDataContext.SubDepartments.OrderBy(x => x.Name).ToList();
        }

        public static Status SaveUpdateSubDepartment(SubDepartment obj)
        {
            Status status = new Status();

            if (PayrollDataContext.SubDepartments.Any(x => x.Name.ToLower() == obj.Name.ToLower() && x.SubDepartmentId != obj.SubDepartmentId))
            {
                status.ErrorMessage = "Sub Department name already exists.";
                status.IsSuccess = false;
                return status;
            }

            if (obj.SubDepartmentId.Equals(0))
            {
                PayrollDataContext.SubDepartments.InsertOnSubmit(obj);
                PayrollDataContext.SubmitChanges();
                status.IsSuccess = true;
            }
            else
            {
                SubDepartment dbEntity = PayrollDataContext.SubDepartments.SingleOrDefault(x => x.SubDepartmentId == obj.SubDepartmentId);
                dbEntity.Name = obj.Name;
                dbEntity.Description = obj.Description;
                PayrollDataContext.SubmitChanges();
                status.IsSuccess = true;
            }

            return status;
        }

        public static SubDepartment GetSubDepartmentById(int subDepartmentId)
        {
            return PayrollDataContext.SubDepartments.SingleOrDefault(x => x.SubDepartmentId == subDepartmentId);
        }

        public static Status DeleteSubDepartment(int subDepartmentId)
        {
            Status status = new Status();

            if (PayrollDataContext.EEmployees.Any(x => x.SubDepartmentId == subDepartmentId))
            {
                status.ErrorMessage = "Sub Department is in use, cannot be deleted.";
                status.IsSuccess = false;
                return status;
            }

            SubDepartment dbEntity = PayrollDataContext.SubDepartments.SingleOrDefault(x => x.SubDepartmentId == subDepartmentId);
            PayrollDataContext.SubDepartments.DeleteOnSubmit(dbEntity);
            PayrollDataContext.SubmitChanges();
            status.IsSuccess = true;

            return status;
        }

        public static List<HrFamilyOccupation> GetAllOccuations()
        {
            return PayrollDataContext.HrFamilyOccupations.OrderBy(x => x.Occupation).ToList();
        }

        public static List<FixedValueDivision> GetAllDivisions()
        {
            return PayrollDataContext.FixedValueDivisions.OrderBy(x => x.DivisionId).ToList();
        }

        public static List<ClientList> GetAllClients()
        {
            return PayrollDataContext.ClientLists.OrderBy(x => x.ClientName).ToList();
        }

        public static Status SaveUpdateClient(ClientList obj)
        {
            Status status = new Status();

            if (PayrollDataContext.ClientLists.Any(x => x.ClientName.ToLower() == obj.ClientName.ToLower() && x.ClientId != obj.ClientId))
            {
                status.ErrorMessage = "Client name already exists.";
                status.IsSuccess = false;
                return status;
            }

            if (obj.ClientId.Equals(0))
            {
                PayrollDataContext.ClientLists.InsertOnSubmit(obj);
                PayrollDataContext.SubmitChanges();
                status.IsSuccess = true;
            }
            else
            {
                ClientList dbEntity = PayrollDataContext.ClientLists.SingleOrDefault(x => x.ClientId == obj.ClientId);
                dbEntity.ClientName = obj.ClientName;
                PayrollDataContext.SubmitChanges();
                status.IsSuccess = true;
            }

            return status;
        }

        public static ClientList GetClientById(int clientId)
        {
            return PayrollDataContext.ClientLists.SingleOrDefault(x => x.ClientId == clientId);
        }

        public static Status DeleteClient(int clientId)
        {
            Status status = new Status();
            if (PayrollDataContext.NewActivities.Any(x => x.ClientSoftwareId == clientId))
            {
                status.ErrorMessage = "Client is in use, cannot be deleted.";
                status.IsSuccess = false;
                return status;
            }

            ClientList dbEntity = PayrollDataContext.ClientLists.SingleOrDefault(x => x.ClientId == clientId);
            PayrollDataContext.ClientLists.DeleteOnSubmit(dbEntity);
            PayrollDataContext.SubmitChanges();
            status.IsSuccess = true;

            return status;
        }

        public static Status SaveUpdateSoftware(SoftwareList obj)
        {
            Status status = new Status();

            if (PayrollDataContext.SoftwareLists.Any(x => x.SoftwareName.ToLower() == obj.SoftwareName.ToLower() && x.SoftwareId != obj.SoftwareId))
            {
                status.ErrorMessage = "Software name already exists.";
                status.IsSuccess = false;
                return status;
            }

            if (obj.SoftwareId.Equals(0))
            {
                PayrollDataContext.SoftwareLists.InsertOnSubmit(obj);
                PayrollDataContext.SubmitChanges();
                status.IsSuccess = true;
            }
            else
            {
                SoftwareList dbEntity = PayrollDataContext.SoftwareLists.SingleOrDefault(x => x.SoftwareId == obj.SoftwareId);
                dbEntity.SoftwareName = obj.SoftwareName;
                PayrollDataContext.SubmitChanges();
                status.IsSuccess = true;
            }

            return status;
        }

        public static List<SoftwareList> GetAllSoftware()
        {
            return PayrollDataContext.SoftwareLists.OrderBy(x => x.SoftwareName).ToList();
        }

        public static SoftwareList GetSoftwareById(int softwareId)
        {
            return PayrollDataContext.SoftwareLists.SingleOrDefault(x => x.SoftwareId == softwareId);
        }

        public static Status DeleteSoftware(int softwareId)
        {
            Status status = new Status();
            if (PayrollDataContext.NewActivities.Any(x => x.ClientSoftwareId == softwareId))
            {
                status.ErrorMessage = "Software is in use, cannot be deleted.";
                status.IsSuccess = false;
                return status;
            }

            SoftwareList dbEntity = PayrollDataContext.SoftwareLists.SingleOrDefault(x => x.SoftwareId == softwareId);
            PayrollDataContext.SoftwareLists.DeleteOnSubmit(dbEntity);
            PayrollDataContext.SubmitChanges();
            status.IsSuccess = true;

            return status;
        }

        public static Status SaveUpdateHobbyType(HobbyType obj)
        {
            Status status = new Status();

            if (PayrollDataContext.HobbyTypes.Any(x => x.HobbyName.ToLower() == obj.HobbyName.ToLower() && x.HobbyTypeId != obj.HobbyTypeId))
            {
                status.ErrorMessage = "Hobby type already exists.";
                status.IsSuccess = false;
                return status;
            }

            if (obj.HobbyTypeId.Equals(0))
            {
                PayrollDataContext.HobbyTypes.InsertOnSubmit(obj);
                PayrollDataContext.SubmitChanges();
                status.IsSuccess = true;
            }
            else
            {
                HobbyType dbEntity = PayrollDataContext.HobbyTypes.SingleOrDefault(x => x.HobbyTypeId == obj.HobbyTypeId);
                dbEntity.HobbyName = obj.HobbyName;
                PayrollDataContext.SubmitChanges();
                status.IsSuccess = true;
            }

            return status;
        }

        public static List<HobbyType> GetHobbyTpyeList()
        {
            return PayrollDataContext.HobbyTypes.OrderBy(x => x.HobbyName).ToList();
        }

        public static HobbyType GetHobyyTypeById(int hobbyTypeId)
        {
            return PayrollDataContext.HobbyTypes.SingleOrDefault(x => x.HobbyTypeId == hobbyTypeId);
        }

        public static Status DeleteHobbyType(int hobbyTypeId)
        {
            Status status = new Status();

            if (PayrollDataContext.EHobbies.Any(x => x.HobbyTypeId == hobbyTypeId))
            {
                status.ErrorMessage = "Hobby type is in use, cannot be deleted.";
                status.IsSuccess = false;
                return status;
            }

            HobbyType dbEntity = PayrollDataContext.HobbyTypes.SingleOrDefault(x => x.HobbyTypeId == hobbyTypeId);
            PayrollDataContext.HobbyTypes.DeleteOnSubmit(dbEntity);
            PayrollDataContext.SubmitChanges();
            status.IsSuccess = true;

            return status;
        }

        public static List<Branch> GetBranchesByCompany(int companyId)
        {
            List<Branch> list = null;           

            list = PayrollDataContext.Branches.Where(
                b => b.CompanyId == companyId).OrderBy(b => b.Name).ToList();
            return list;
        }

        public static List<ActivityType> GetActivityTypes()
        {
            return PayrollDataContext.ActivityTypes.OrderBy(x => x.Name).ToList();
        }

        public static Status SaveUpdateActivityType(ActivityType obj)
        {
            Status status = new Status();

            if (obj.ActivityTypeId.Equals(0))
            {
                if (PayrollDataContext.ActivityTypes.Any(x => x.Name.ToLower() == obj.Name.ToLower()))
                {
                    status.ErrorMessage = "Name already exists.";
                    status.IsSuccess = false;
                    return status;
                }

                PayrollDataContext.ActivityTypes.InsertOnSubmit(obj);
            }
            else
            {
                ActivityType dbObj = GetActivityTypeById(obj.ActivityTypeId);
                dbObj.Name = obj.Name;
            }

            PayrollDataContext.SubmitChanges();
            status.IsSuccess = true;            

            return status;
        }

        public static ActivityType GetActivityTypeById(int activityTypeId)
        {
            return PayrollDataContext.ActivityTypes.SingleOrDefault(x => x.ActivityTypeId == activityTypeId);
        }

        public static Status DeleteActivityType(int activityTypeId)
        {
            Status status = new Status();

            ActivityType dbObj = GetActivityTypeById(activityTypeId);
            if (dbObj == null)
            {
                status.ErrorMessage = "Activity type not found.";
                status.IsSuccess = false;
                return status;
            }

            PayrollDataContext.ActivityTypes.DeleteOnSubmit(dbObj);
            PayrollDataContext.SubmitChanges();
            status.IsSuccess = true;

            return status;
        }

        public static List<KeyValue> GetRepresentativeList(string prefix)
        {
            List<NewActivityDetail> list = PayrollDataContext.NewActivityDetails.Where(x => x.Representative.ToLower().Contains(prefix.ToLower())).OrderBy(x => x.Representative).ToList();

            List<KeyValue> listKV = new List<KeyValue>();
            foreach (var item in list)
            {
                KeyValue obj = new KeyValue() { Key = item.Representative, Value = item.Representative };
                listKV.Add(obj);
            }

            return listKV.Distinct().ToList();
        }

        public static HrFamilyOccupation GetHrFamilyOccupationByName(string name)
        {
            return PayrollDataContext.HrFamilyOccupations.SingleOrDefault(x => x.Occupation.ToLower() == name.ToLower());
        }

    }
}
