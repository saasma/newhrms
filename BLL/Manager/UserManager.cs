﻿using System.Linq;
using DAL;
using Utils.Security;
using System.Web.Security;
using System.Collections.Generic;
using System.Xml.Linq;
using Utils.Helper;
using System.IO;
using Utils;
using System;
using BLL.BO;
using System.Xml.Serialization;
using System.Xml;
using System.Web;


namespace BLL.Manager
{
    public class ModuleData
    {
        public int ModuleId{get;set;}
        public string Name{get;set;}
    }
    public class UserManager : BaseBiz
    {

        public static string GetDefaultPageForRedirect(int roleId,Guid userID)
        {
            URole role1 = GetRoleById(roleId);


            if (UserManager.IsModuleAccessible(ModuleEnum.HumanResource, roleId,userID) == true)
                System.Web.HttpContext.Current.Response.Redirect("~/newhr/DashboardHR.aspx", true);

            if (UserManager.IsModuleAccessible(ModuleEnum.LeaveAndAttendance, roleId, userID) == true)
                System.Web.HttpContext.Current.Response.Redirect("~/Dashboard/DashboardLeave.aspx", true);

            if (UserManager.IsModuleAccessible(ModuleEnum.PayrollManagement, roleId, userID) == true)
                System.Web.HttpContext.Current.Response.Redirect("~/Dashboard/DashboardPR.aspx", true);


            if (UserManager.IsModuleAccessible(ModuleEnum.EmployeeAppraisal, roleId, userID) == true)
                System.Web.HttpContext.Current.Response.Redirect("~/Dashboard/DashboardAppraisal.aspx", true);


            if (UserManager.IsModuleAccessible(ModuleEnum.Retirement, roleId, userID) == true)
                System.Web.HttpContext.Current.Response.Redirect("~/Dashboard/DashboardRetirement.aspx", true);
            if (UserManager.IsModuleAccessible(ModuleEnum.ProjectAndTimesheet, roleId, userID) == true)
                System.Web.HttpContext.Current.Response.Redirect("~/Dashboard/DashboardProject.aspx", true);

            if (UserManager.IsModuleAccessible(ModuleEnum.AdministratorTools, roleId, userID) == true)
                System.Web.HttpContext.Current.Response.Redirect("~/newhr/AllSettings.aspx", true);

            if (UserManager.IsModuleAccessible(ModuleEnum.DocumentManagement, roleId, userID) == true)
                System.Web.HttpContext.Current.Response.Redirect("~/Dashboard/DashboardDocument.aspx", true);

            return "";
        }

        public static void RedirectToProperModuleDashboardIfHasOnlySingleModulePermission(int roleId, string username)
        {
            Guid userid = GetUserByUserName(username).UserID;

            string defaultPage = GetDefaultPageForRedirect(roleId,userid);
            if (!string.IsNullOrEmpty(defaultPage))
            {
                HttpContext.Current.Response.Redirect(defaultPage, true);
                return;
            }


        }

        public static List<ModuleData> GetModuleList()
        {
            List<ModuleData> list = new List<ModuleData>();

            list.Add( new ModuleData{ ModuleId = (int)ModuleEnum.AdministratorTools,Name=ModuleEnum.AdministratorTools.ToString() });
            list.Add( new ModuleData{ ModuleId = (int)ModuleEnum.EmployeeAppraisal,Name=ModuleEnum.EmployeeAppraisal.ToString() });
            list.Add( new ModuleData{ ModuleId = (int)ModuleEnum.HumanResource,Name=ModuleEnum.HumanResource.ToString() });
            list.Add( new ModuleData{ ModuleId = (int)ModuleEnum.LeaveAndAttendance,Name=ModuleEnum.LeaveAndAttendance.ToString() });
            list.Add( new ModuleData{ ModuleId = (int)ModuleEnum.PayrollManagement,Name=ModuleEnum.PayrollManagement.ToString() });
            list.Add( new ModuleData{ ModuleId = (int)ModuleEnum.ProjectAndTimesheet,Name=ModuleEnum.ProjectAndTimesheet.ToString() });
            list.Add( new ModuleData{ ModuleId = (int)ModuleEnum.Retirement,Name=ModuleEnum.Retirement.ToString() });
            list.Add(new ModuleData { ModuleId = (int)ModuleEnum.DocumentManagement, Name = ModuleEnum.DocumentManagement.ToString() });

            return list.OrderBy(x => x.Name).ToList();
        }

        public static bool IsPageAccessible(string url)
        {
            if (SessionManager.IsCustomRole == false)
                return true;

            url = url.Replace("~/", "").ToLower().Trim();

            List<UPermission> pages = GetPermissionByRole(SessionManager.User.RoleId.Value);

            foreach (string page in pages.Select(x => x.Url).ToList())
            {
                if (page.Contains(url))
                    return true;
            }

            if( SessionManager.User.URole.URoleModules.Any(x=>x.ModuleRef_ID== GetPageModuleId(
                url,SessionManager.User.RoleId.Value)))
                return true;

            //if not page then check if it is the Module url
            if (IsUrlModuleAndIsAccessible(url, SessionManager.User.RoleId.Value))
                return true;


            return false;
        }

        public static bool IsContainsPage(string url, List<UPermission> pages)
        {
            foreach (UPermission page in pages)
            {
                if (url.Contains(page.Url))
                    return true;
            }
            return false;
        }
        public static bool IsModuleAccessible(ModuleEnum module)
        {
            if (SessionManager.IsCustomRole == false)
                return true;

            if (SessionManager.User.URole.URoleModules.Any(x => x.ModuleRef_ID == (int)module))
                return true;

            List<UPermission> pages = GetPermissionByRole(SessionManager.User.RoleId.Value);

            string moduleId = ((int)module).ToString();
            XmlDocument doc = MyCache.GetMenuXml(SessionManager.User.UserID.ToString(), CommonManager.CompanySetting.HasLevelGradeSalary);

            XmlNodeList bookNodes = doc.GetElementsByTagName("siteMapNode");
            foreach (XmlNode nextNode in bookNodes)
            {
                foreach (XmlAttribute nextAttr in nextNode.Attributes)
                {
                    if (nextAttr.Name == "moduleId" && nextNode.Attributes["url"] != null)
                    {
                        if (nextAttr.Value.ToString() == moduleId
                            && IsContainsPage(nextNode.Attributes["url"].Value.ToLower(),pages))
                            return true;
                    }
                }
            }

            return false;
        }
        public static bool IsModuleAccessible(ModuleEnum module,int roleId)
        {

            URole role = GetRoleById(roleId);

            if (role.URoleModules.Any(x => x.ModuleRef_ID == (int)module))
                return true;

            List<UPermission> pages = GetPermissionByRole(roleId);

            string moduleId = ((int)module).ToString();
            XmlDocument doc = MyCache.GetMenuXml(SessionManager.User.UserID.ToString(), CommonManager.CompanySetting.HasLevelGradeSalary);

            XmlNodeList bookNodes = doc.GetElementsByTagName("siteMapNode");
            foreach (XmlNode nextNode in bookNodes)
            {
                foreach (XmlAttribute nextAttr in nextNode.Attributes)
                {
                    if (nextAttr.Name == "moduleId" && nextNode.Attributes["url"] != null)
                    {
                        if (nextAttr.Value.ToString() == moduleId
                            && IsContainsPage(nextNode.Attributes["url"].Value.ToLower(), pages))
                            return true;
                    }
                }
            }

            return false;
        }
        public static bool IsModuleAccessible(ModuleEnum module, int roleId,Guid userID)
        {

            URole role = GetRoleById(roleId);

            if (role.URoleModules.Any(x => x.ModuleRef_ID == (int)module))
                return true;

            List<UPermission> pages = GetPermissionByRole(roleId);

            string moduleId = ((int)module).ToString();
            XmlDocument doc = MyCache.GetMenuXml(userID.ToString(), CommonManager.CompanySetting.HasLevelGradeSalary);

            XmlNodeList bookNodes = doc.GetElementsByTagName("siteMapNode");
            foreach (XmlNode nextNode in bookNodes)
            {
                foreach (XmlAttribute nextAttr in nextNode.Attributes)
                {
                    if (nextAttr.Name == "moduleId" && nextNode.Attributes["url"] != null)
                    {
                        if (nextAttr.Value.ToString() == moduleId
                            && IsContainsPage(nextNode.Attributes["url"].Value.ToLower(), pages))
                            return true;
                    }
                }
            }

            return false;
        }
        public static bool IsUrlModuleAndIsAccessible(string url,int roleId)
        {



            XmlDocument doc = MyCache.GetMenuXml(SessionManager.User.UserID.ToString(), CommonManager.CompanySetting.HasLevelGradeSalary);

            XmlNodeList bookNodes = doc.GetElementsByTagName("siteMapNode");


            foreach(XmlNode module in doc.ChildNodes[1].ChildNodes)
            {
                if(module.Attributes["url"].Value.ToLower().Contains(url))
                {
                    int moduleId = int.Parse(module.Attributes["moduleId"].Value);
                    if (PayrollDataContext.UPermissions.Any(x => x.RoleId == roleId && x.ModuleId == moduleId))
                        return true;
                }
            }

      

            return false;
        }

        public static int GetPageModuleId(string url, int roleId)
        {

            XmlDocument doc = MyCache.GetMenuXml(SessionManager.User.UserID.ToString(), CommonManager.CompanySetting.HasLevelGradeSalary);
            XmlNodeList bookNodes = doc.GetElementsByTagName("siteMapNode");

            foreach (XmlNode page in bookNodes)
            {
                if (page.Attributes != null &&
                    page.Attributes["url"] != null &&
                    page.Attributes["url"].Value.ToLower().Contains(url))
                {
                    int moduleId = int.Parse(page.Attributes["moduleId"].Value);
                    return moduleId;
                }
            }

            return 0;
        }

        /// <summary>
        /// Pick UserMappedEmployeeId for the curent admin user & set EmployeeId and Employee user name
        /// </summary>
        /// <param name="currentEmployeeId"></param>
        /// <param name="currentEmployeeUserName"></param>
        public static void SetCurrentEmployeeAndUser(ref int currentEmployeeId, ref string currentEmployeeUserName)
        {

            if (SessionManager.CurrentLoggedInEmployeeId != 0)
            {
                currentEmployeeId = SessionManager.CurrentLoggedInEmployeeId;
            }
            else
            {
                if (SessionManager.User.UserMappedEmployeeId == null)
                {
                    currentEmployeeId = 0;
                    currentEmployeeUserName = "";
                    return;
                }
                currentEmployeeId = SessionManager.User.UserMappedEmployeeId.Value;
            }

            if (SessionManager.CurrentLoggedInEmployeeId != 0)
                currentEmployeeUserName = SessionManager.UserName;
            else
            {
                if (SessionManager.User.UserMappedEmployeeId != -1)
                {
                    UUser empUser = GetUserByEmployee(SessionManager.User.UserMappedEmployeeId.Value);
                    if (empUser != null)
                        currentEmployeeUserName = empUser.UserName;
                }
            }

            if (currentEmployeeId == -1)
                currentEmployeeId = 0;
        }

        /// <summary>
        /// Verify Login process
        /// User passes in a user name and password and will be redirected on if successful
        /// </summary>
        /// <param name="email">User name the customer is authenticating with</param>
        /// <param name="password">Password the customer is using</param>
        /// <returns>true if the login is successful</returns>
        public static string ProcessWindowsAuthLogin(string userName,ref string role,ref int roleId)
        {


            if (string.IsNullOrEmpty(userName))
                return "";

            UUser user = PayrollDataContext.UUsers.Where(u => u.ActiveDirectoryName.ToLower().Trim() ==
                userName.ToLower().Trim()).Take(1).SingleOrDefault();

            if (user == null)
                return "";

            role = user.URole.Name;
            roleId = user.RoleId.Value;
            return user.UserName;


        }

        public bool ValidateUser(string userName, string pwd)
        {
            

            UUser entity = PayrollDataContext.UUsers.SingleOrDefault(
                e => (e.UserName.Trim().ToLower() == userName.Trim().ToLower() ));

            if (entity == null)
                return false;

            pwd = SecurityHelper.GetHash(pwd,entity.IsNew);

            if (pwd.Equals(entity.Password))
                return true;
            else
                return false;
        }

        public bool ValidateAdministrator(string userName, string pwd)
        {
            

            UUser entity = PayrollDataContext.UUsers.SingleOrDefault(
                e => (e.UserName.Trim().ToLower() == userName.Trim().ToLower() && e.RoleId == 1));

            if (entity == null)
                return false;

            pwd = SecurityHelper.GetHash(pwd,entity.IsNew);

            if (pwd.Equals(entity.Password))
                return true;
            else
                return false;
        }

        public bool SetUserInfo(string userName, ref string role, ref int roleId)
        {
            UUser entity = PayrollDataContext.UUsers.SingleOrDefault(
              e => (e.UserName.ToLower() == userName.ToLower()));

            if (entity == null)
                return false;

            role = entity.URole.Name;
            roleId = entity.RoleId.Value;

            return true;
        }


        public static bool IsEmployeeUserName(string userName)
        {
            if( PayrollDataContext.UUsers.Any(
                   e => (e.UserName.ToLower() == userName.ToLower() && e.IsEmployeeType == true)))
                return true;
            return false;
        }

        public static bool IsNonEmployeeUserName(string userName)
        {
            if (PayrollDataContext.UUsers.Any(
                   e => (e.UserName.ToLower() == userName.ToLower() && ( e.IsEmployeeType == null || e.IsEmployeeType == false ))))
                return true;
            return false;
        }

        public static UUser GetAdminUserForEmployee(int employeeId)
        {
            return
                PayrollDataContext.UUsers.
                Where(x => x.UserMappedEmployeeId == employeeId).FirstOrDefault();
        }

        public bool ValidateUser(string userName, string pwd,ref string role,ref int roleId,ref UUser user,bool isEmployeeType,ref bool isEmpRetired
            , ref bool isEmployeePortalDisabled)
        {

            isEmpRetired = false;
            isEmployeePortalDisabled = false;

            UUser entity = null;

            //if (isEmployeeType == false)
                entity = PayrollDataContext.UUsers.SingleOrDefault(
                    e => (e.UserName.ToLower() == userName.ToLower()));
            //else
            //    entity = PayrollDataContext.UUsers.SingleOrDefault(
            //        e => (e.UserName.ToLower() == userName.ToLower() && e.IsEmployeeType == true));

            user = entity;

            if (entity == null)
                return false;


            // If active directory enabled then make the login using Active direcotory for Employee
            DateTime today = DateTime.Now.Date;
            #region "Active directory Login"
            if (entity.EmployeeId != null && LdapHelper.IsLDAPAccessible() && !string.IsNullOrEmpty(entity.ActiveDirectoryName))
            {
                GetEmployeeUserDetailsResult employee = PayrollDataContext.GetEmployeeUserDetails(entity.EmployeeId.Value).FirstOrDefault();
                if (employee != null)
                {
                   
                    if (employee.IsResigned && employee.DateOfResignationEng.Value.AddMonths(1) < today)
                    {
                        isEmpRetired = true;
                        return false;
                    }
                    if (employee.IsRetired && employee.DateOfRetirementEng.Value.AddMonths(1) < today)
                    {
                        isEmpRetired = true;
                        return false;
                    }

                   
                }

                //if (LdapHelper.IsAuthenticated(userName, pwd))
                //{
                //    role = entity.URole.Name;
                //    roleId = entity.RoleId.Value;
                //    return true;
                //}

                if (LdapHelper.IsAuthenticated(entity.ActiveDirectoryName, pwd))
                {
                    role = entity.URole.Name;
                    roleId = entity.RoleId.Value;
                    return true;
                }

                

                return false;
            }
            #endregion

            pwd = SecurityHelper.GetHash(pwd,entity.IsNew);

            if (pwd.Equals(entity.Password) && entity.IsActive.Value && entity.URole.IsActive.Value)
            {
                role = entity.URole.Name;
                roleId = entity.RoleId.Value;


                Role userRole = (Role)roleId;
                //If Employee then should not be retired/resigned emp
                if (userRole == Role.Employee)
                {
                    if (CommonManager.Setting.DisableEmployeePortal != null && CommonManager.Setting.DisableEmployeePortal.Value)
                    {
                        isEmployeePortalDisabled = true;
                        return false;
                    }

                    EEmployee employee = EmployeeManager.GetEmployeeById(entity.EmployeeId.Value);
                    if (employee != null)
                    {
                        if (employee.IsResigned.HasValue && employee.IsResigned.Value && employee.EHumanResources[0].DateOfResignationEng.Value.AddMonths(1) <= DateTime.Now)
                        {
                            isEmpRetired = true;
                            return false;
                        }
                        if (employee.IsRetired.HasValue && employee.IsRetired.Value && employee.EHumanResources[0].DateOfRetirementEng.Value.AddMonths(1) <= DateTime.Now)
                        {
                            isEmpRetired = true;
                            return false;
                        }
                    }
                }


                return true;
            }
            else
                return false;
        }


        public bool ChangePassword(string userName, string pwd)
        {
            pwd = SecurityHelper.GetHash(pwd,false);

            UUser entity =
                PayrollDataContext.UUsers.SingleOrDefault(e => e.UserName.ToLower() == userName.ToLower());

            if (entity == null)
                return false;

            entity.IsNew = false;
            entity.Password = pwd;
            entity.IsInitialPasswordChanged = true;

            SavePasswordChangedHistory(userName, pwd);

            return UpdateChangeSet();
        }

        #region Roles

        public static System.Collections.Generic.List<URole> GetRoles(int roleIdToSkip)
        {
            return PayrollDataContext.URoles
                .Where(r=>((roleIdToSkip==0 || roleIdToSkip != r.RoleId) 
                    && (r.CompanyId==SessionManager.CurrentCompanyId || r.CompanyId == null)))
                .OrderBy(r => r.IsFixed)
                .OrderBy(r => r.Name).ToList();
                
        }

        public static bool IsRoleNameAlreadyExists(string role, int roleId)
        {
            URole roleObj = PayrollDataContext.URoles.Where(
                r => ((r.Name.ToLower() == role.Trim().ToLower()) && (roleId == 0 || roleId != r.RoleId))
                )
                .SingleOrDefault();

            if (roleObj == null)
                return false;
            return true;
        }

        public static void CreateRole(URole role)
        {
            if (!IsRoleNameAlreadyExists(role.Name,0))
            {
                //URole role = new URole();
                //role.Name = roleName;
                role.IsFixed = false;
                role.IsActive = true;
                
                role.RoleId = 0;
                
                role.CompanyId = SessionManager.CurrentCompanyId;

                PayrollDataContext.URoles.InsertOnSubmit(role);
              
                SaveChangeSet();
            }
        }

        public static bool IsRoleInUse(int roleId)
        {
            return PayrollDataContext.UUsers.Where(u => u.RoleId == roleId).Count() > 0;
        }

        public static bool DeleteRole(URole role)
        {
            URole dbRole = PayrollDataContext.URoles.Where(r => r.RoleId == role.RoleId).SingleOrDefault();


            PayrollDataContext.URoles.DeleteOnSubmit(dbRole);
            return DeleteChangeSet(); 
        }

        public static URole GetRoleById(int roleId)
        {
            return PayrollDataContext.URoles.Where(r => r.RoleId == roleId).SingleOrDefault();
        }

       

        public static void UpdateRole(URole role)
        {

            if (!IsRoleNameAlreadyExists(role.Name, role.RoleId))
            {
                URole dbRole = PayrollDataContext.URoles.Where(r => r.RoleId == role.RoleId).SingleOrDefault();

                dbRole.IsActive = role.IsActive;
                dbRole.Name = role.Name;
                dbRole.Description = role.Description;
                dbRole.IsReadOnly = role.IsReadOnly;
				 dbRole.HasDepartmentAssigned = role.HasDepartmentAssigned;
                dbRole.URoleDepartments.Clear();

                dbRole.URoleDepartments.AddRange(role.URoleDepartments);

                dbRole.URoleBranches.Clear();
                dbRole.URoleBranches.AddRange(role.URoleBranches);

                dbRole.URoleModules.Clear();
                dbRole.URoleModules.AddRange(role.URoleModules);

                UpdateChangeSet();
                
            }

            
        }

        #endregion
        #region Users

        /// <summary>
        /// Get user list skipping the current administrator user
        /// </summary>
        /// <param name="name"></param>
        /// <param name="currentUserName"></param>
        /// <param name="isEmployeeType"></param>
        /// <returns></returns>
        public static System.Collections.Generic.List<UUser> GetUsers(string name, string currentUserName, bool isEmployeeType)
        {
            //skip "admin" user also
            return PayrollDataContext.UUsers.Where(u => ( u.UserName != "admin" && u.UserName!="suser" && u.UserName != currentUserName && u.UserName.Contains(name) 
                && u.IsEmployeeType == isEmployeeType
                 && (u.CompanyId == SessionManager.CurrentCompanyId || u.CompanyId==null )))
                .OrderBy(r => r.UserName).ToList();

        }

        /// <summary>
        /// Get user list skipping the current administrator user
        /// </summary>
        /// <param name="name"></param>
        /// <param name="currentUserName"></param>
        /// <param name="isEmployeeType"></param>
        /// <returns></returns>
        public static System.Collections.Generic.List<GetUserListResult> GetUserList(string name, string currentUserName, bool isEmployeeType
            , int currentPage, int pageSize, ref int totalRecords,bool isPossibleEmployeesAlso)
        {


            //skip "admin" user also
            List<GetUserListResult> list= PayrollDataContext.GetUserList(name, currentUserName, isEmployeeType, SessionManager.CurrentCompanyId,
                currentPage, pageSize, isPossibleEmployeesAlso).ToList();

            if (list.Count > 0)
                totalRecords = list[0].TotalRows.Value;
            else
                totalRecords = 0;
            return list;


        }


        public static string SaveImportedUser(List<GetUserListResult> users,ref int imported)
        {
            imported = 0;

            foreach (GetUserListResult user in users)
            {
                if (PayrollDataContext.UUsers.Any(x => x.UserName.ToLower() == user.UserName.ToLower() && x.EmployeeId != user.EmployeeId))
                {
                    return string.Format("\"{0}\" user name already defined for different Employee.", user.UserName);
                }
                if (!string.IsNullOrEmpty(user.ActiveDirectoryName))
                {
                    if (PayrollDataContext.UUsers.Any(x => x.ActiveDirectoryName.ToLower() == user.ActiveDirectoryName.ToLower() && x.EmployeeId != user.EmployeeId))
                    {
                        return string.Format("\"{0}\" AD name already defined for different Employee.", user.UserName);
                    }
                }

                UUser dbUser = PayrollDataContext.UUsers.SingleOrDefault(x => x.EmployeeId == user.EmployeeId);

                if (dbUser == null)
                {
                    dbUser = new UUser();
                    dbUser.UserID = Guid.NewGuid();
                    dbUser.CompanyId = SessionManager.CurrentCompanyId;
                    dbUser.EmployeeId = user.EmployeeId;
                    dbUser.IsActive = true;
                    dbUser.Email = "";
                    dbUser.IsEmployeeType = true;
                    dbUser.IsInitialPasswordChanged = true;
                    string pwd = "";

                    if (!string.IsNullOrEmpty(user.Password))
                        pwd = user.Password;
                    else
                        pwd = Util.CreateRandomPassword(6);

                    dbUser.Password = SecurityHelper.GetHash(pwd,false);
                    dbUser.IsNew = false;
                    dbUser.UserName = user.UserName;
                    dbUser.RoleId = (int)Role.Employee;
                    dbUser.IsEnglishPreferred = false;

                    PayrollDataContext.UUsers.InsertOnSubmit(dbUser);

                    imported += 1;
                }
                else
                {
                    if (!string.IsNullOrEmpty(user.Password))
                    {
                        dbUser.IsNew = false;
                        dbUser.Password = SecurityHelper.GetHash(user.Password, false);

                    }

                    if (dbUser.UserName.ToLower() != user.UserName.ToLower())
                    {
                        return string.Format("For Employee ID having {0}, user name has been changed to \"{1}\" from \"{2}\", user name can't be changed once saved.", 
                            user.EmployeeId,user.UserName,dbUser.UserName);
                    }

                    imported += 1;
                }

                dbUser.ActiveDirectoryName = user.ActiveDirectoryName;

                
            }

            PayrollDataContext.SubmitChanges();

            return "";
        }

        public static bool IsUserNameAlreadyExists(string userName)
        {
            string[] names = { "admin", "suser" };

            if (names.Contains(userName.ToLower()))
            {
                return true;
            }

            UUser roleObj = PayrollDataContext.UUsers.Where(
                r => ((r.UserName.ToLower() == userName.Trim().ToLower())
                ))
                .SingleOrDefault();

            if (roleObj == null)
                return false;
            return true;
        }

        public static bool DeleteUser(string userName)
        {
             //if user present in log then delete
            if (PayrollDataContext.ChangeMonetories.Any(c => c.UserName == userName))
                return false;


            UUser dbEntity = PayrollDataContext.UUsers.Where(r => r.UserName == userName).SingleOrDefault();

           
            PayrollDataContext.UUsers.DeleteOnSubmit(dbEntity);
            return DeleteChangeSet();
        }

        public static string SendNewUserNameInMail(Guid userID, string newUserName, UUser dbEntity)
        {
            //UUser dbEntity = PayrollDataContext.UUsers.Where(r => r.UserName == userName).SingleOrDefault();


           // string pwd = Util.CreateRandomPassword(6);
           // dbEntity.Password = SecurityHelper.GetHash(pwd, true);  //Membership.GeneratePassword(6, 2);
            //dbEntity.IsNew = true;


            string userOrEmployeeEmail = "";
            string userOrEmployeeName = "";

            if (dbEntity.IsEmployeeType == true)
            {
                userOrEmployeeEmail = dbEntity.EEmployee.EAddresses[0].CIEmail.Trim();
                userOrEmployeeName = dbEntity.EEmployee.Name.Trim();
            }
            else
            {
                userOrEmployeeName = string.IsNullOrEmpty(dbEntity.NameIfNotEmployee) ? dbEntity.UserName : dbEntity.NameIfNotEmployee;
                userOrEmployeeEmail = dbEntity.Email.Trim();

            }

            //send password in mail if has email
            if (!string.IsNullOrEmpty(userOrEmployeeEmail))
            {

                string body = FileHelper.GetNewUserNameMailContent();
                string subject = "Payroll UserName changed";

                EmailContent dbMailContent = CommonManager.GetMailContent((int)EmailContentType.UserNameChanged);
                if (dbMailContent != null)
                {
                    subject = dbMailContent.Subject;
                    body = dbMailContent.Body;
                }

                string employeeloginUrl = (UrlHelper.GetRootUrl());// + "/Employee");
                if (employeeloginUrl[employeeloginUrl.Length - 1] != '/')
                    employeeloginUrl += "/";

                // Append for Employee type only
                if (dbEntity.IsEmployeeType == true)
                    employeeloginUrl += "Employee";

                // Replace Constant values
                body = body.Replace("#Name#", userOrEmployeeName).Replace("#UserName#", newUserName)
                    .Replace("#Url#", employeeloginUrl)
                    .Replace("{Organizations Name}", SessionManager.CurrentCompany.Name);


                //body = string.Format(body, userOrEmployeeName, dbEntity.UserName, pwd, employeeloginUrl);

                string mailErrorMsg = "";

                if (SMTPHelper.SendMail(userOrEmployeeEmail, body, subject, ref mailErrorMsg))
                    UpdateChangeSet();
                else
                    return "Username changed email could not be sent : " + mailErrorMsg;
            }

            return "";
        }
        public static string SendNewPasswordInMail(string userName)
        {
            UUser dbEntity = PayrollDataContext.UUsers.Where(r => r.UserName == userName).SingleOrDefault();


            string pwd = Util.CreateRandomPassword(6);
            dbEntity.Password = SecurityHelper.GetHash(pwd,false);  //Membership.GeneratePassword(6, 2);
            dbEntity.IsNew = false;
           

            string userOrEmployeeEmail = "";
            string userOrEmployeeName = "";

            if (dbEntity.IsEmployeeType == true)
            {
                userOrEmployeeEmail = dbEntity.EEmployee.EAddresses[0].CIEmail.Trim();
                userOrEmployeeName = dbEntity.EEmployee.Name.Trim();
            }
            else
            {
                userOrEmployeeName = string.IsNullOrEmpty(dbEntity.NameIfNotEmployee) ? dbEntity.UserName : dbEntity.NameIfNotEmployee;
                userOrEmployeeEmail = dbEntity.Email.Trim();

            }

            //send password in mail if has email
            if (!string.IsNullOrEmpty(userOrEmployeeEmail))
            {

                string body = FileHelper.GetPasswordMailContent();
                string subject = Config.PasswordMailSubject;

                EmailContent dbMailContent = CommonManager.GetMailContent((int)EmailContentType.SendNewPasswordInMail);
                if (dbMailContent != null)
                {
                    subject = dbMailContent.Subject;
                    body = dbMailContent.Body;
                }

                string employeeloginUrl = (UrlHelper.GetRootUrl());// + "/Employee");
                if (employeeloginUrl[employeeloginUrl.Length - 1] != '/')
                    employeeloginUrl += "/";

                // Append for Employee type only
                if (dbEntity.IsEmployeeType == true)
                    employeeloginUrl += "Employee";

                // Replace Constant values
                body = body.Replace("#Name#", userOrEmployeeName).Replace("#UserName#", dbEntity.UserName)
                        .Replace("#Password#", pwd).Replace("#Url#", employeeloginUrl)
                        .Replace("{Organizations Name}", SessionManager.CurrentCompany.Name);

                
                //body = string.Format(body, userOrEmployeeName, dbEntity.UserName, pwd, employeeloginUrl);

                  string mailErrorMsg=  "";

                if (SMTPHelper.SendMail(userOrEmployeeEmail, body, subject,ref mailErrorMsg ))
                    UpdateChangeSet();
                else
                    return "Password could not be sent in the email : " + mailErrorMsg;
            }

            return "";
        }

        public static void CreateUser(UUser user1)
        {
            if (user1.UserMappedEmployeeId == 0 || user1.UserMappedEmployeeId == -1)
                user1.UserMappedEmployeeId = null;

            if (!IsUserNameAlreadyExists(user1.UserName))
            {
                UUser user = new UUser();
                user.UserID = Guid.NewGuid();
                user.UserName = user1.UserName;
                user.Email = user1.Email;
                user.RoleId = user1.RoleId;
                user.NameIfNotEmployee = user1.NameIfNotEmployee;
                user.ActiveDirectoryName = user1.ActiveDirectoryName;
                user.EnablePayrollSignOff = user1.EnablePayrollSignOff;
                user.DocumentIsViewOnly = user1.DocumentIsViewOnly;
                //set user name as default password
                string pwd = Util.CreateRandomPassword(6);
                user.Password = SecurityHelper.GetHash(pwd,false);  //Membership.GeneratePassword(6, 2);
                user.IsNew = false;
                user.IsActive = true;
                user.IsEmployeeType = user1.IsEmployeeType;
                user.EmployeeId = user1.EmployeeId;
                user.UserMappedEmployeeId = user1.UserMappedEmployeeId;
                user.CompanyId = user1.CompanyId;
                user.IsInitialPasswordChanged = false;
                user.AllowableIPList = user1.AllowableIPList;
                user.IsEnglishPreferred = false;

                PayrollDataContext.UUsers.InsertOnSubmit(user);
                SaveChangeSet();

                string mailErrorMsg = "";


                //send password in mail if has email
                if (!string.IsNullOrEmpty(user.Email))
                {
                    SendNewPasswordInMail(user.UserName);
                    //string body = FileHelper.GetPasswordMailContent();
                    //body = string.Format(body, user.UserName, user.UserName, pwd);

                    //SMTPHelper.SendMail(user.Email, body, Config.PasswordMailSubject,ref mailErrorMsg);
                }
            }
        }

        
        

        //public static bool DeleteUser(URole role)
        //{
        //    URole dbRole = PayrollDataContext.URoles.Where(r => r.RoleId == role.RoleId).SingleOrDefault();


        //    PayrollDataContext.URoles.DeleteOnSubmit(dbRole);
        //    return DeleteChangeSet();
        //}

        public static UUser GetUserByUserName(string userName)
        {
            return PayrollDataContext.UUsers.Where(r => r.UserName == userName).SingleOrDefault();
        }
        public static int GetUserEmployeeId(string username)
        {
            int? empId =
                (
                from u in PayrollDataContext.UUsers
                where u.UserName == username
                select u.EmployeeId
                ).FirstOrDefault();

            if (empId == null)
                return 0;
            return empId.Value;
        }
        public static Guid GetUserId(string username)
        {
            Guid userid =
                (
                from u in PayrollDataContext.UUsers
                where u.UserName == username
                select u.UserID
                ).FirstOrDefault();


            return userid;
        }
        public static UUser GetUserByUserID(Guid userID)
        {
            return PayrollDataContext.UUsers.Where(r => r.UserID == userID).SingleOrDefault();
        }
        public static UUser GetUserByUserName(string userName, bool isEmployeeType)
        {
            //return PayrollDataContext.UUsers.Where(r => r.UserName == userName).SingleOrDefault();
            UUser entity = null;

            if (isEmployeeType == false)
                entity = PayrollDataContext.UUsers.SingleOrDefault(
                    e => (e.UserName == userName && (e.IsEmployeeType == null || e.IsEmployeeType == false)));
            else
                entity = PayrollDataContext.UUsers.SingleOrDefault(
                    e => (e.UserName == userName && e.IsEmployeeType == true));

            return entity;

        }

        public static UUser GetEmployeeUserByEmail(string email,ref bool isUserExists)
        {
            isUserExists = true;

            EEmployee emp =
                (from e in PayrollDataContext.EEmployees
                 join a in PayrollDataContext.EAddresses on e.EmployeeId equals a.EmployeeId
                 where a.CIEmail != null && a.CIEmail.ToLower().Equals(email.ToLower())
                 select e
                ).FirstOrDefault();

            if (emp == null)
            {
                isUserExists = false;
                return null;
            }

            UUser user = 
            (
                from u in PayrollDataContext.UUsers
                //join e in PayrollDataContext.EEmployees on u.EmployeeId equals e.EmployeeId
                //join a in PayrollDataContext.EAddresses on e.EmployeeId equals a.EmployeeId
                where u.EmployeeId == emp.EmployeeId
                select u
                ).FirstOrDefault();

            return user;
        }

        public static UUser GetUserByLDAP(string userName)
        {
            return PayrollDataContext.UUsers.Where(r => r.ActiveDirectoryName == userName).SingleOrDefault();
        }
        public static UUser GetUserByEmployee(int employeeId)
        {
            return PayrollDataContext.UUsers.Where(r => r.EmployeeId==employeeId).SingleOrDefault();
        }
        public static UUser GetUserByEmployee(int employeeId, PayrollDataContext PayrollDataContext)
        {
            return PayrollDataContext.UUsers.Where(r => r.EmployeeId == employeeId).SingleOrDefault();
        }

        public static string GetEmployeeUserEmail(int employeeId)
        {
            return (
                from a in PayrollDataContext.EAddresses
                where a.EmployeeId == employeeId
                select a.CIEmail
              ).SingleOrDefault();
        }

        public static bool UpdateUser(UUser user)
        {
            if (user.UserMappedEmployeeId == 0 || user.UserMappedEmployeeId == -1)
                user.UserMappedEmployeeId = null;

            UUser dbUser =  PayrollDataContext.UUsers.Where(r => r.UserName == user.UserName).SingleOrDefault();
            // if user name has been changed then process for it
            if (dbUser.IsEmployeeType != null && dbUser.IsEmployeeType.Value &&   user.UserName.ToLower() != user.NewUserName.ToLower())
            {
                if (IsUserNameAlreadyExists(user.NewUserName))
                {
                    return false;
                }
                SendNewUserNameInMail(dbUser.UserID, user.NewUserName, dbUser);
                dbUser.UserName = user.NewUserName;
                
            }

            if (dbUser.UserName.ToLower() != "admin" && dbUser.UserName.ToLower() != "suser")
            {
                #region "Change Logs"

                PayrollDataContext.ChangeSettings.InsertOnSubmit(GetSettingChangeLog((byte)MonetoryChangeLogTypeEnum.User,
                   dbUser.UserName + " updated", "", "", LogActionEnum.Update));

                if (dbUser.NameIfNotEmployee == null)
                    dbUser.NameIfNotEmployee = "";
                if (dbUser.NameIfNotEmployee.Equals(user.NameIfNotEmployee) == false)
                PayrollDataContext.ChangeSettings.InsertOnSubmit(GetSettingChangeLog((byte)MonetoryChangeLogTypeEnum.User,
                     dbUser.UserName + " updated", "Name : "  +  dbUser.NameIfNotEmployee, "Name : " + user.NameIfNotEmployee  , LogActionEnum.Update));



                #endregion


                dbUser.NameIfNotEmployee = user.NameIfNotEmployee;
                dbUser.ActiveDirectoryName = user.ActiveDirectoryName;
                dbUser.RoleId = user.RoleId;
                dbUser.IsActive = user.IsActive;
                dbUser.Email = user.Email;
                dbUser.EnablePayrollSignOff = user.EnablePayrollSignOff;
                dbUser.DocumentIsViewOnly = user.DocumentIsViewOnly;


             

            }
            dbUser.AllowableIPList = user.AllowableIPList;
            dbUser.UserMappedEmployeeId = user.UserMappedEmployeeId;

            UpdateChangeSet();

            return true;


        }

        #endregion

        //public static URole GetRoleByUser(string userName)
        //{
        //    var query=
        //        ( from u in PayrollDataContext.UUsers
        //            join r in PayrollDataContext.URoles
        //                on u.RoleId == 
        //}

        #region "Page & Permission"

        /// <summary>
        /// Retrieve pages that are accessible for the role
        /// </summary>
        /// <param name="roleId"></param>
        /// <returns></returns>
        public static string[] GetAccessiblePagesForRole(int roleId)
        {

            var query =
               (from p in PayrollDataContext.UPermissions
                where p.RoleId == roleId
                select p.Url).ToArray();

            return query;
        }

        

        public static int GetPageIdByPageUrl(string url)
        {
            UPage page =
                PayrollDataContext.UPages
                    .Where(p => p.PageUrl.Substring(p.PageUrl.LastIndexOf("/") + 1).ToLower() == url.ToLower())
                    .SingleOrDefault();

            if (page == null)
                return 0;
            return page.PageId;
        }

        public static UPage GetPageById(int pageId)
        {
            return PayrollDataContext.UPages.Where(p => p.PageId == pageId).SingleOrDefault();
        }
        public static UPageSection GetPageSectionById(int pageSectionId)
        {
            return PayrollDataContext.UPageSections.Where(p => p.PageSectionId == pageSectionId).SingleOrDefault();
        }
        public static void SavePermission(int roleId, List<UPermission> list )// List<string> pages)
        {

            PayrollDataContext.UPermissions.DeleteAllOnSubmit(
                PayrollDataContext.UPermissions.Where(x => x.RoleId == roleId).ToList());

            PayrollDataContext.UPermissions.InsertAllOnSubmit(list);

            //foreach (string page in pages)
            //{
            //    PayrollDataContext.UPermissions.InsertOnSubmit(
            //        new UPermission { RoleId = roleId, Url = page });
            //}

            PayrollDataContext.SubmitChanges();

            MyCache.Reset();
        }
        public static void SavePermissionSection(string xml,int roleId)
        {
            PayrollDataContext.SavePermissionSection(roleId, XElement.Parse(xml));
        }

        public static List<UPermission> GetPermissionByRole(int roleId)
        {

            if (MyCache. GetFromGlobalCache("RolePermission" + roleId) != null)
                return MyCache.GetFromGlobalCache("RolePermission" + roleId) as List<UPermission>;

            List<UPermission> list = 
                            PayrollDataContext.UPermissions
                                .Where(p => p.RoleId == roleId)
                                .ToList();

            MyCache.SaveToGlobalCache("RolePermission" + roleId, list, MyCache. ExpirationType.Sliding,MyCache.veryLongTime);

            return list;
        }

        public static List<UPermissionSection> GetPermissionSectionByRole(int roleId)
        {
            return
                PayrollDataContext.UPermissionSections
                    .Where(p => p.RoleId == roleId)
                    .ToList();
        }

        public static List<UPermissionSection> GetAccessibleSectionsForPage(int roleId, int pageId)
        {
            return
                PayrollDataContext.UPermissionSections
                    .Where(p => (p.RoleId == roleId && p.PageId == pageId))
                    .ToList();
        }

        public static List<UPage> GetParentPages()
        {
            return
                PayrollDataContext.UPages
                .Where(p => p.IsParent == true)
                .ToList();
                
        }

        public static List<UPage> GetChildPages(int parentId)
        {
            return
                PayrollDataContext.UPages
                .Where(p => (p.IsParent == false && p.ParentPageId == parentId))
                .ToList();

        }

        public static List<UPageSection> GetSections(int pageId)
        {
            return
                PayrollDataContext.UPageSections
                .Where(p => p.PageId == pageId)
                .ToList();
        }
        #endregion

        public static bool SendNewPasswordInMailToEmployee(string userName)
        {


            UUser dbEntity = PayrollDataContext.UUsers.Where(r => r.UserName == userName).SingleOrDefault();


            string pwd = Util.CreateRandomPassword(6);
            dbEntity.Password = SecurityHelper.GetHash(pwd, false);  //Membership.GeneratePassword(6, 2);
            dbEntity.IsNew = false;


            string userOrEmployeeEmail = "";
            string userOrEmployeeName = "";

            if (dbEntity.IsEmployeeType == true)
            {
                userOrEmployeeEmail = dbEntity.EEmployee.EAddresses[0].CIEmail.Trim();
                userOrEmployeeName = dbEntity.EEmployee.Name.Trim();
            }
            else
            {
                userOrEmployeeName = string.IsNullOrEmpty(dbEntity.NameIfNotEmployee) ? dbEntity.UserName : dbEntity.NameIfNotEmployee;
                userOrEmployeeEmail = dbEntity.Email.Trim();

            }

            //send password in mail if has email
            if (!string.IsNullOrEmpty(userOrEmployeeEmail))
            {

                string body = FileHelper.GetPasswordMailContent();
                string subject = Config.PasswordMailSubject;

                EmailContent dbMailContent = CommonManager.GetMailContent((int)EmailContentType.SendNewPasswordInMail);
                if (dbMailContent != null)
                {
                    subject = dbMailContent.Subject;
                    body = dbMailContent.Body;
                }

                string employeeloginUrl = (UrlHelper.GetRootUrl());// + "/Employee");
                if (employeeloginUrl[employeeloginUrl.Length - 1] != '/')
                    employeeloginUrl += "/";

                // Append for Employee type only
                if (dbEntity.IsEmployeeType == true)
                    employeeloginUrl += "Employee";

                // Replace Constant values
                body = body.Replace("#Name#", userOrEmployeeName).Replace("#UserName#", dbEntity.UserName)
                        .Replace("#Password#", pwd).Replace("#Url#", employeeloginUrl)
                        .Replace("{Organizations Name}", SessionManager.CurrentCompany.Name);


                //body = string.Format(body, userOrEmployeeName, dbEntity.UserName, pwd, employeeloginUrl);

                string mailErrorMsg = "";

                if (SMTPHelper.SendAsyncMail(userOrEmployeeName, body, subject, ""))
                    UpdateChangeSet();
                else return false;


            }
            else
                return false;
            return true;
        }

        public static bool IsMatchedToLastTwoPasswords(string userName, string passWord)
        {
            passWord = SecurityHelper.GetHash(passWord, false);

            List<PasswordChangedHistory> list = PayrollDataContext.PasswordChangedHistories.Where(x => x.UserName == userName).OrderByDescending(x => x.ChangedDate).Take(2).ToList();
            if (list.Count > 0)
            {
                foreach (PasswordChangedHistory obj in list)
                {
                    if (obj.Password == passWord)
                        return true;
                }
            }

            return false;
        }

        public static void SavePasswordChangedHistory(string userName, string pwd)
        {
            List<PasswordChangedHistory> list = PayrollDataContext.PasswordChangedHistories.Where(x => x.UserName == userName).OrderByDescending(x => x.ChangedDate).ToList();
            int i = 0;
            foreach (PasswordChangedHistory dbObj in list)
            {
                i++;
                if (i > 1)
                    PayrollDataContext.PasswordChangedHistories.DeleteOnSubmit(dbObj);
            }

            PasswordChangedHistory obj = new PasswordChangedHistory()
            {
                UserName = userName,
                Password = pwd,
                ChangedDate = DateTime.Now
            };

            PayrollDataContext.PasswordChangedHistories.InsertOnSubmit(obj);
        }

        public static bool IsPwdChangedBeforePwdChangingDays(string userName, ref int days)
        {
            DateTime today = DateTime.Now;
            days = 0;

            int passwordChangingDays = 0;
            Setting objSetting = PayrollDataContext.Settings.SingleOrDefault();
            if (objSetting != null && objSetting.PasswordChangingDays != null)
                passwordChangingDays = objSetting.PasswordChangingDays.Value;

            if (passwordChangingDays != 0)
            {
                days = passwordChangingDays;
                List<PasswordChangedHistory> list = PayrollDataContext.PasswordChangedHistories.Where(x => x.UserName == userName).OrderByDescending(x => x.ChangedDate).ToList();
                if (list.Count > 0)
                {
                    TimeSpan ts = today - list[0].ChangedDate.Value;
                    if (ts.TotalDays >= passwordChangingDays)
                        return false;
                    else
                        return true;
                }
                else
                    return true;
            }
            else
                return true;
        }

        public static Status SaveUpdatePasswordChangeRule(int days, int passwordChangeType)
        {
            Status status = new Status();
            Setting dbObj = PayrollDataContext.Settings.SingleOrDefault();
            if (dbObj != null)
            {
                dbObj.PasswordChangingDays = days;
                dbObj.PasswordChangeType = passwordChangeType;
                PayrollDataContext.SubmitChanges();
                status.IsSuccess = true;
            }

            return status;
        }

        public static Setting GetSettingForPasswordChangeRule()
        {
            return PayrollDataContext.Settings.SingleOrDefault();
        }

        public static string SendForgotPasswordLinkInMail(string emailId, string body, string userName, Guid objGuid,string ip)
        {
            if (SMTPHelper.SendToRigoNepalDomainOnlyIfLocalAccess(emailId,"") == false)
            {
                return "Not Allowed to send mail to this email from localhost";
            }

            //string errorMsg = SMTPHelper.SendForgotPasswordLinkInMail(emailId, body);
            //bool status = //SMTPHelper.TestMail(emailId, body, "HR Password Reset");
            SMTPHelper.SendAsyncMail(emailId, body, "HR Password Reset", "");

            //if (status)
            {
                PwdRecovery dbObj = PayrollDataContext.PwdRecoveries.FirstOrDefault(x => x.UserName.ToLower() == userName.ToLower());
                if (dbObj != null)
                {
                    PayrollDataContext.PwdRecoveries.DeleteOnSubmit(dbObj);
                    PayrollDataContext.SubmitChanges();
                }

                PwdRecovery obj = new PwdRecovery();
                obj.UserName = userName;
                obj.RequestedDate = DateTime.Now;
                obj.RequestGuid = objGuid;
                PayrollDataContext.PwdRecoveries.InsertOnSubmit(obj);

                // place requested log
                ChangeUserActivity log = new ChangeUserActivity();
                log.CompanyId = SessionManager.CurrentCompanyId;
                log.UserName = "";
                log.DateEng = GetCurrentDateAndTime();
                log.Remarks = "Password reset requested from mail " + emailId + " from IP " + ip + ".";

                BLL.BaseBiz.PayrollDataContext.ChangeUserActivities.InsertOnSubmit(log);

                PayrollDataContext.SubmitChanges();

                return "";
            }


            //return "";
        }

        public static PwdRecovery GetPasswordRecoveryById(string Id)
        {
            return PayrollDataContext.PwdRecoveries.SingleOrDefault(x => x.RequestGuid.ToString().ToLower() == Id.ToLower());
        }

        public static XmlDocument GetEMpMenuXml()
        {    
            XmlDocument doc = new XmlDocument();
            doc.Load(HttpContext.Current.Server.MapPath("~/EmpMenus/All.xml"));

            return doc;
        }

        public static List<EPermissionPage> GetEPermissionPagesByCompanyId()
        {
            return PayrollDataContext.EPermissionPages.Where(x => x.CompanyId == SessionManager.CurrentCompanyId).ToList();
        }

        public static void SaveEmpPermission(List<EPermissionPage> list)
        {

            PayrollDataContext.EPermissionPages.DeleteAllOnSubmit(
                PayrollDataContext.EPermissionPages.Where(x => x.CompanyId == SessionManager.CurrentCompanyId ).ToList());

            PayrollDataContext.EPermissionPages.InsertAllOnSubmit(list);
          
            PayrollDataContext.SubmitChanges();
        }

        public static UUser GetUUser()
        {
            return PayrollDataContext.UUsers.SingleOrDefault(x => x.UserID == SessionManager.CurrentLoggedInUserID);
        }

        public static bool UpdateUUserForEnglishPreferred(bool val)
        {
            UUser dbObj = PayrollDataContext.UUsers.SingleOrDefault(x => x.UserID == SessionManager.CurrentLoggedInUserID);
            if (dbObj == null)
                return false;
            else
            {
                dbObj.IsEnglishPreferred = val;
                PayrollDataContext.SubmitChanges();
                return true;
            }
        }

        public static bool IsEnglishDatePreferredInNepaliDateSys()
        {
            if (IsEnglish)
                return true;
            else
            {
                UUser dbUUser = GetUUser();
                if (dbUUser.IsEnglishPreferred == true)
                    return true;
                else
                    return false;
            }
        }

        public static List<PasswordChangedHistory> GetPasswordChangedHisotryForUser()
        {
            return PayrollDataContext.PasswordChangedHistories.Where(x => x.UserName == SessionManager.UserName).ToList();
        }

    }
}
