﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;
using BLL.BO;
using Utils;
using System.Reflection;
using BLL.Entity;

namespace BLL.Manager
{
    public class TimeSheetManager : BaseBiz
    {

        public static double GetSelfFilledTimeSheetPercent(int employeeId, int payrollPeriodId)
        {
            PayrollPeriod period = CommonManager.GetPayrollPeriod(payrollPeriodId);
            float daysInTheMonth = period.TotalDays.Value;

            List<GetHolidaysForAttendenceResult> holidayList = new HolidayManager().GetMonthsHolidaysForAttendence(period.Month, period.Year.Value, SessionManager.CurrentCompanyId, payrollPeriodId, null, null).ToList();
            daysInTheMonth = daysInTheMonth - (holidayList.Where(x => x.IsWeekly != null && x.IsWeekly.Value).Count());   

            double totalFilledPercent = 0;
            Timesheet timesheet = PayrollDataContext.Timesheets.SingleOrDefault(x => x.EmployeeId == employeeId && x.PayrollPeriodId == payrollPeriodId);

            if (timesheet != null && timesheet.TotalHours != null)
            {
                return ( timesheet.TotalHours.Value * 100) / (daysInTheMonth * 8);
            }

            return totalFilledPercent;

        }

        public static ResponseStatus SaveUpdateProjectTimesheet(double totalHours, double prevTotalHours, int payrollPeriodId, int employeeId,
            List<TimesheetProject> projectHourList, List<TimesheetLeave> leaveList,int timeSheetStatus,string otherProjectName)
        {
            ResponseStatus status = new ResponseStatus();
            SetConnectionPwd(PayrollDataContext.Connection);
            PayrollDataContext.Transaction = PayrollDataContext.Connection.BeginTransaction();

            PayrollPeriod prevPayrollPeriod = PayrollDataContext.PayrollPeriods
                .Where(x => x.PayrollPeriodId < payrollPeriodId)
                .OrderByDescending(x => x.PayrollPeriodId).Take(1).SingleOrDefault();

            try
            {

                Timesheet dbTimesheet = PayrollDataContext.Timesheets
                    .SingleOrDefault(x => x.PayrollPeriodId == payrollPeriodId && x.EmployeeId == employeeId);

                Timesheet prevDbTimesheet = null;

                if (prevPayrollPeriod != null)
                {
                    prevDbTimesheet = PayrollDataContext.Timesheets
                        .SingleOrDefault(x => x.PayrollPeriodId == prevPayrollPeriod.PayrollPeriodId && x.EmployeeId == employeeId);
                }

                if (dbTimesheet != null)
                {
                    dbTimesheet.OtherProjectName = otherProjectName;
                    // first delete old project values
                    List<TimesheetProject> dbProjectList = dbTimesheet.TimesheetProjects.ToList();
                    PayrollDataContext.TimesheetProjects.DeleteAllOnSubmit(dbProjectList);
                    PayrollDataContext.TimesheetLeaves.DeleteAllOnSubmit(dbTimesheet.TimesheetLeaves);
                    PayrollDataContext.SubmitChanges();
                }
                else
                {
                    dbTimesheet = new Timesheet();
                    dbTimesheet.PayrollPeriodId = payrollPeriodId;
                    dbTimesheet.OtherProjectName = otherProjectName;
                    dbTimesheet.EmployeeId = employeeId;                    
                    PayrollDataContext.Timesheets.InsertOnSubmit(dbTimesheet);
                }

                // Previous Timesheet
                if (prevDbTimesheet != null)
                {
                    // first delete old project values
                    List<TimesheetProject> dbProjectList = prevDbTimesheet.TimesheetProjects.Where(x => x.DayNumber >= 27).ToList();
                    PayrollDataContext.TimesheetProjects.DeleteAllOnSubmit(dbProjectList);
                    PayrollDataContext.TimesheetLeaves.DeleteAllOnSubmit(prevDbTimesheet.TimesheetLeaves.Where(x => x.DayNumber >= 27).ToList());
                    PayrollDataContext.SubmitChanges();


                    prevDbTimesheet.TimesheetProjects.AddRange(projectHourList.Where(x => x.IsPreviousMonth == true));
                    prevDbTimesheet.TimesheetLeaves.AddRange(leaveList.Where(x => x.IsPreviousMonth == true));

                }
                // No insertion for Prev month only update Timesheet
                //else
                //{
                //    dbTimesheet = new Timesheet();
                //    dbTimesheet.PayrollPeriodId = payrollPeriodId;
                //    dbTimesheet.EmployeeId = employeeId;
                //    PayrollDataContext.Timesheets.InsertOnSubmit(dbTimesheet);
                //}



                if (timeSheetStatus == (int)TimeSheetStatus.Approved)
                {
                    dbTimesheet.ApprovedEmployeeId = SessionManager.CurrentLoggedInEmployeeId;
                    dbTimesheet.ApprovedOn = GetCurrentDateAndTime();
                }
                if (timeSheetStatus == (int)TimeSheetStatus.ReviewedByHR)
                {
                    dbTimesheet.ReviewedHREmployeeId = SessionManager.CurrentLoggedInEmployeeId;
                    dbTimesheet.ReviewedOn = GetCurrentDateAndTime();
                }

                dbTimesheet.Status = (byte)timeSheetStatus;
                dbTimesheet.TotalHours = totalHours;
                if (prevDbTimesheet != null)
                    prevDbTimesheet.TotalHours = prevTotalHours;
                dbTimesheet.TimesheetProjects.AddRange(projectHourList.Where(x => x.IsPreviousMonth == false));
                dbTimesheet.TimesheetLeaves.AddRange(leaveList.Where(x => x.IsPreviousMonth == false));

                PayrollDataContext.SubmitChanges();
                PayrollDataContext.Transaction.Commit();
                //return true;
            }
            catch (Exception exp)
            {
                Log.log("Error while saving new emp.", exp);
                PayrollDataContext.Transaction.Rollback();
                status.ErrorMessage = exp.Message;

            }
            return status;
        }

        public static Timesheet GetTimeSheet(int payrollPeriodId, int employeeId)
        {
            return PayrollDataContext.Timesheets.SingleOrDefault(x => x.EmployeeId == employeeId && x.PayrollPeriodId == payrollPeriodId);
        }

        public static List<TimesheetGridBO> GetProjectList(int payrollPeriodId, int employeeId)
        {
            List<TimesheetGridBO> list = new List<TimesheetGridBO>();
            Timesheet dbPrevMonthTimesheet = PayrollDataContext.Timesheets
                .Where(x => x.PayrollPeriodId < payrollPeriodId && x.EmployeeId == employeeId)
                .OrderByDescending(x => x.PayrollPeriodId).Take(1).SingleOrDefault();

            Timesheet dbTimesheet = PayrollDataContext.Timesheets.SingleOrDefault(x => x.PayrollPeriodId == payrollPeriodId && x.EmployeeId == employeeId);
            
            bool isSaved = dbTimesheet == null ? false : true;
            double total = 0;

            PayrollPeriod prevPayrollPeriod = PayrollDataContext.PayrollPeriods
                .Where(x => x.PayrollPeriodId < payrollPeriodId)
                .OrderByDescending(x => x.PayrollPeriodId).Take(1).SingleOrDefault();

            PayrollPeriod payrollPeriod = CommonManager.GetPayrollPeriod(payrollPeriodId);
           


            int totalRows = 0;
            List<GetProjectsResult> projectList = ProjectManager.GetProjectList(prevPayrollPeriod != null 
                ? prevPayrollPeriod.StartDateEng.Value : payrollPeriod.StartDateEng.Value, payrollPeriod.EndDateEng.Value,
                true,
                "", 1, 100, ref totalRows);


            // Add Other Project
            projectList.Add(new GetProjectsResult { ProjectId = 0, Name = (dbTimesheet != null ?  dbTimesheet.OtherProjectName : "Other") });

            if (isSaved == false)
            {
             
                foreach (var pro in projectList)
                {
                    list.Add(new TimesheetGridBO { Id = pro.ProjectId, Name = pro.Name, Code = pro.Code, TotalDays = payrollPeriod.TotalDays.Value });
                }
                
            }
            else
            {
                List<TimesheetProject> timesheetProjectList = PayrollDataContext.TimesheetProjects.Where(
                    x => x.TimesheetId == dbTimesheet.TimesheetId).ToList();

                List<TimesheetProject> prevTimesheetProjectList = new List<TimesheetProject>();

                if (dbPrevMonthTimesheet != null)
                    prevTimesheetProjectList = PayrollDataContext.TimesheetProjects.Where(
                        x => x.TimesheetId == dbPrevMonthTimesheet.TimesheetId).ToList();



                foreach (var pro in projectList)
                {

                    // iterate project timesheet from db or else set blank row
                    List<TimesheetProject> filtertimesheetProjectList =
                        timesheetProjectList.Where(x => x.ProjectId == pro.ProjectId).ToList();

                    List<TimesheetProject> prevFiltertimesheetProjectList = new List<TimesheetProject>();

                    if (prevTimesheetProjectList != null)
                        prevFiltertimesheetProjectList = prevTimesheetProjectList.Where(x => x.ProjectId == pro.ProjectId).ToList();


                    if (filtertimesheetProjectList.Count > 0 || prevFiltertimesheetProjectList.Count > 0)
                    {
                        TimesheetGridBO timesheetProjectRow = new TimesheetGridBO { Id = pro.ProjectId, Name = pro.Name, Code = pro.Code, };
                        timesheetProjectRow.TotalDays = payrollPeriod.TotalDays.Value;

                        // Current Month
                        total = 0;
                        for (int i = 1; i <= 31; i++)
                        {
                            PropertyInfo dayProperty = timesheetProjectRow.GetType().GetProperty("D" + i);
                            if (dayProperty != null)
                            {
                                object value = null;
                                TimesheetProject valueEntity = filtertimesheetProjectList.SingleOrDefault(x => x.DayNumber == i);
                                if (valueEntity != null)
                                {
                                    value = valueEntity.Hours;
                                    total += valueEntity.Hours.Value;
                                }
                                dayProperty.SetValue(timesheetProjectRow, value, null);

                            }
                        }
                        timesheetProjectRow.Total = total;

                        total = 0;
                        // Prev Month
                        double prev1_26Total = 0;

                        if (prevFiltertimesheetProjectList != null)
                        {
                            for (int i = 1; i <= 31; i++)
                            {
                                TimesheetProject valueEntity = prevFiltertimesheetProjectList.SingleOrDefault(x => x.DayNumber == i);
                                if (valueEntity != null)
                                {

                                    total += valueEntity.Hours.Value;
                                    if (i <= 26)
                                        prev1_26Total += valueEntity.Hours.Value;
                                }

                                PropertyInfo dayProperty = timesheetProjectRow.GetType().GetProperty("P" + i);
                                if (i >= 27)
                                {
                                    if (dayProperty != null)
                                    {
                                        object value = null;
                                        if (valueEntity != null)
                                        {
                                            value = valueEntity.Hours;

                                        }
                                        dayProperty.SetValue(timesheetProjectRow, value, null);
                                    }
                                }
                                
                            }
                        }
                        timesheetProjectRow.PreviousTotal = total;
                        timesheetProjectRow.P1_26Total = prev1_26Total;
                        list.Add(timesheetProjectRow);
                    }
                    else
                    {
                        list.Add(new TimesheetGridBO { Id = pro.ProjectId, Name = pro.Name, Code = pro.Code, Total = 0, TotalDays = payrollPeriod.TotalDays.Value });
                    }
                }

            }
            return list;
        }

        public static List<TimesheetGridBO> GetLeaveList(int payrollPeriodId, int employeeId)
        {
            List<TimesheetGridBO> list = new List<TimesheetGridBO>();
            PayrollPeriod payrollPeriod = CommonManager.GetPayrollPeriod(payrollPeriodId);
            Timesheet dbTimesheet = PayrollDataContext.Timesheets.SingleOrDefault(x => x.PayrollPeriodId == payrollPeriodId && x.EmployeeId == employeeId);

            Timesheet dbPrevMonthTimesheet = PayrollDataContext.Timesheets
                .Where(x => x.PayrollPeriodId < payrollPeriodId && x.EmployeeId == employeeId)
                .OrderByDescending(x => x.PayrollPeriodId).Take(1).SingleOrDefault();
            PayrollPeriod prevPayrollPeriod = PayrollDataContext.PayrollPeriods
               .Where(x => x.PayrollPeriodId < payrollPeriodId)
               .OrderByDescending(x => x.PayrollPeriodId).Take(1).SingleOrDefault();

            List<EmployeeLeaveListResult> leaveList = new LeaveAttendanceManager().GetEmployeeLeaveList(employeeId)
                .Where(x => x.FreqOfAccrual != "Compensatory").ToList();

            List<TimesheetGridBO> projectList = GetProjectList(payrollPeriodId, employeeId);

            List<TimesheetGridBO> prevProjectList = new List<TimesheetGridBO>();

            if (prevPayrollPeriod != null)
                prevProjectList = GetProjectList(prevPayrollPeriod.PayrollPeriodId, employeeId);

            // Add Holiday title also
            leaveList.Add(new EmployeeLeaveListResult { LeaveTypeId = -2, Title = "Holiday" });

            bool isSaved = LeaveAttendanceManager.IsAttendanceSavedForEmployee(payrollPeriodId, employeeId); //dbTimesheet == null ? false : true;
            double total = 0;

            if (isSaved == false)
            {
                // get approved leave
                List<LeaveDetail> approvedLeaves =
                    (
                    from ld in PayrollDataContext.LeaveDetails
                    join lr in PayrollDataContext.LeaveRequests on ld.LeaveRequestId equals lr.LeaveRequestId
                    where lr.EmployeeId == employeeId && lr.Status == 2 //only approved
                        && ld.DateOnEng >= payrollPeriod.StartDateEng && ld.DateOnEng <= payrollPeriod.EndDateEng
                    select ld
                    ).ToList();

                List<LeaveDetail> prevApprovedLeaves = new List<LeaveDetail>();

                if (prevPayrollPeriod != null)
                {
                    // as past atte is already saved
                    List<AttedenceDetail> prevAtteDetails = new List<AttedenceDetail>();
                                       
                    prevAtteDetails =
                    (
                    from d in PayrollDataContext.AttedenceDetails
                    join a in PayrollDataContext.Attendences on d.AttendenceId equals a.AttendenceId
                    where a.EmployeeId == employeeId && a.PayrollPeriodId == prevPayrollPeriod.PayrollPeriodId
                        && d.Type !=0 && d.Type != (byte)AttendaceColumnType.WeeklyHoliday && d.Type != (byte)AttendaceColumnType.Holiday
                    select d
                    ).ToList();

                    foreach (AttedenceDetail d in prevAtteDetails)
                    {
                        prevApprovedLeaves.Add
                            (
                                new LeaveDetail
                                {
                                    DateOnEng = d.DateEng.Value,
                                    IsHoliday = ((d.Type == (byte)AttendaceColumnType.Holiday || d.Type == (byte)AttendaceColumnType.WeeklyHoliday) ? true : false),
                                    IsWeeklyHoliday = d.Type == (byte)AttendaceColumnType.WeeklyHoliday,
                                    IsPrevMonth = true,
                                    LeaveRequest = new LeaveRequest { LeaveTypeId = d.TypeValue.Value, IsHalfDayLeave = (d.DayValue.Contains("/2") ? true : false) }
                                }
                            );
                    }

                    //prevApprovedLeaves =
                    //        (
                    //        from ld in PayrollDataContext.LeaveDetails
                    //        join lr in PayrollDataContext.LeaveRequests on ld.LeaveRequestId equals lr.LeaveRequestId
                    //        where lr.EmployeeId == employeeId && lr.Status == 2 //only approved
                    //            && ld.DateOnEng >= prevPayrollPeriod.StartDateEng && ld.DateOnEng <= prevPayrollPeriod.EndDateEng
                    //        // && ld.DateOnEng.Day >=27
                    //        select ld
                    //        ).ToList();
                }

                foreach (var item in leaveList)
                {
                    // iterate project timesheet from db or else set blank row
                    List<LeaveDetail> filtertimesheetLeaveList = approvedLeaves.Where(x => x.LeaveRequest.LeaveTypeId == item.LeaveTypeId).ToList();

                    List<LeaveDetail> prevFiltertimesheetLeaveList = null;
                    if (prevApprovedLeaves != null)
                        prevFiltertimesheetLeaveList = prevApprovedLeaves.Where(x => x.LeaveRequest.LeaveTypeId == item.LeaveTypeId).ToList();

                    // for Holiday
                    if (item.LeaveTypeId == -2)
                    {
                        filtertimesheetLeaveList = new List<LeaveDetail>();
                        List<GetHolidaysForAttendenceResult> holidayList = new HolidayManager().GetMonthsHolidaysForAttendence(0, 0, SessionManager.CurrentCompanyId, 0, payrollPeriod.StartDateEng, payrollPeriod.EndDateEng).ToList();

                        foreach (GetHolidaysForAttendenceResult holiday in holidayList)
                        {
                            LeaveDetail newHolidayDetail = new LeaveDetail();
                            newHolidayDetail.DateOnEng = holiday.DateEng.Value;
                            newHolidayDetail.LeaveRequest = new LeaveRequest();
                            newHolidayDetail.LeaveRequest.IsHalfDayLeave = false; // as no half day leave for PSI in holiday list
                            newHolidayDetail.IsHoliday = true;

                            if (holiday.IsWeekly != null && holiday.IsWeekly.Value)
                            {
                                newHolidayDetail.IsWeeklyHoliday = true;
                            }
                           

                            filtertimesheetLeaveList.Add(newHolidayDetail);
                        }

                        // Prev month holiday list
                        holidayList = new HolidayManager().GetMonthsHolidaysForAttendence(0, 0, SessionManager.CurrentCompanyId, 0, prevPayrollPeriod.StartDateEng, prevPayrollPeriod.EndDateEng).ToList();

                        foreach (GetHolidaysForAttendenceResult holiday in holidayList)
                        {
                            LeaveDetail newHolidayDetail = new LeaveDetail();
                            newHolidayDetail.IsPrevMonth = true;
                            newHolidayDetail.DateOnEng = holiday.DateEng.Value;
                            newHolidayDetail.LeaveRequest = new LeaveRequest();
                            newHolidayDetail.LeaveRequest.IsHalfDayLeave = false; // as no half day leave for PSI in holiday list
                            newHolidayDetail.IsHoliday = true;
                            if (holiday.IsWeekly != null && holiday.IsWeekly.Value)
                            {
                                newHolidayDetail.IsWeeklyHoliday = true;
                            }
                            prevFiltertimesheetLeaveList.Add(newHolidayDetail);
                        }
                    }


                    if (filtertimesheetLeaveList.Count > 0 || prevFiltertimesheetLeaveList.Count>0)
                    {
                        TimesheetGridBO timesheetLeaveRow = new TimesheetGridBO { Id = item.LeaveTypeId, Name = item.Title, Code = "" };
                        timesheetLeaveRow.TotalDays = payrollPeriod.TotalDays.Value;

                        SetLeaveDays(projectList, filtertimesheetLeaveList, timesheetLeaveRow, false);
                        SetLeaveDays(prevProjectList, prevFiltertimesheetLeaveList, timesheetLeaveRow, true);


                        list.Add(timesheetLeaveRow);
                    }
                    else
                    {
                        list.Add(new TimesheetGridBO { Id = item.LeaveTypeId, Name = item.Title, TotalDays = payrollPeriod.TotalDays.Value });
                    }
                }
                // if not saved then get the Approved leave from leave Request/Approval to fill in default                
            }
            else
            {

                List<AttedenceDetail> atteDetails =
                    (
                    from d in PayrollDataContext.AttedenceDetails
                    join a in PayrollDataContext.Attendences on d.AttendenceId equals a.AttendenceId
                    where a.EmployeeId == employeeId && a.PayrollPeriodId == payrollPeriodId
                    select d
                    ).ToList();

                List<AttedenceDetail> prevAtteDetails = new List<AttedenceDetail>();

                if( prevPayrollPeriod != null)
                    prevAtteDetails = 
                    (
                    from d in PayrollDataContext.AttedenceDetails
                    join a in PayrollDataContext.Attendences on d.AttendenceId equals a.AttendenceId
                    where a.EmployeeId == employeeId && a.PayrollPeriodId == prevPayrollPeriod.PayrollPeriodId
                    select d
                    ).ToList();

                //List<TimesheetLeave> timesheetLeaveList = PayrollDataContext.TimesheetLeaves.Where(
                //   x => x.TimesheetId == dbTimesheet.TimesheetId).ToList();

                foreach (var pro in leaveList)
                {
                    // iterate project timesheet from db or else set blank row
                    List<AttedenceDetail> filtertimesheetLeaveList =
                        atteDetails.Where(x => x.Type==1 && x.TypeValue.Value == pro.LeaveTypeId).ToList();

                    List<AttedenceDetail> prevFilterTimesheetLeaveList =
                        prevAtteDetails.Where(x => x.Type == 1 && x.TypeValue.Value == pro.LeaveTypeId).ToList();

                    // for Holiday
                    if (pro.LeaveTypeId == -2)
                    {                        
                        filtertimesheetLeaveList = atteDetails.Where(x => (x.Type == 2 || x.Type == 3)).ToList();
                        prevFilterTimesheetLeaveList = prevAtteDetails.Where(x => (x.Type == 2 || x.Type == 3)).ToList();    
                    }

                    if (filtertimesheetLeaveList.Count > 0 || prevFilterTimesheetLeaveList.Count > 0)
                    {
                        TimesheetGridBO timesheetLeaveRow = new TimesheetGridBO { Id = pro.LeaveTypeId, Name = pro.Title, Code = "" };
                        timesheetLeaveRow.TotalDays = payrollPeriod.TotalDays.Value;

                        SetLeaveDays(projectList, filtertimesheetLeaveList, timesheetLeaveRow, false);
                        SetLeaveDays(prevProjectList, prevFilterTimesheetLeaveList, timesheetLeaveRow, true);

                        list.Add(timesheetLeaveRow);
                    }
                    else
                    {
                        list.Add(new TimesheetGridBO { Id = pro.LeaveTypeId, Name = pro.Title, Code = "", Total = 0, TotalDays = payrollPeriod.TotalDays.Value });
                    }
                }
            }
            return list;
        }

        private static double SetLeaveDays(List<TimesheetGridBO> projectList, 
            List<AttedenceDetail> filtertimesheetLeaveList, TimesheetGridBO timesheetLeaveRow,bool isPreMonth)
        {

            string dayStartWith = "D";
            string holidayStartWith = "D{0}IsHoliday";

            if (isPreMonth)
            {
                dayStartWith = "P";
                holidayStartWith = "P{0}IsHoliday";
            }
            object value = null;
            double total = 0, prevTotal = 0;
            for (int i = 1; i <= 31; i++)
            {
                AttedenceDetail valueEntity = filtertimesheetLeaveList.SingleOrDefault(x => x.DayNumber == i);
                //for weekly holiday
                if (valueEntity != null && valueEntity.Type == 2)
                {
                    //valueen
                    continue;
                }

                PropertyInfo dayProperty = timesheetLeaveRow.GetType().GetProperty(dayStartWith + i);
                if (dayProperty != null)
                {
                    value = null;
                   

                    if (valueEntity != null)
                    {
                        value = valueEntity.DayValue.Contains("/2") ? 4 : 8;

                        if (valueEntity.Type == 2 || valueEntity.Type == 3)
                        {
                            PropertyInfo holidayProperty = timesheetLeaveRow.GetType().GetProperty(string.Format(holidayStartWith,i));
                            if (holidayProperty != null)
                                holidayProperty.SetValue(timesheetLeaveRow, true, null);
                            //for holiday also there could be overtime so to display holiday value we must check if there is value in proejct
                            // on that day & if it has then we should deduct that day in holiday display

                            //first find if there is value in project on this day
                            double projectTotalHours = GetProjectHourOnThisDay(i, projectList);

                            value = (object)(Convert.ToDouble(value) - projectTotalHours);
                        }


                        total += (double)Convert.ToDecimal(value);
                    }
                    if (value != null)
                        dayProperty.SetValue(timesheetLeaveRow, Convert.ToDouble(value), null);
                }

                //if (i <= 26 && valueEntity != null && isPreMonth)
                //{
                //    value = valueEntity.DayValue.Contains("/2") ? 4 : 8;//
                //    prevTotal += (double)Convert.ToDecimal(value);
                //}
            }
            if (isPreMonth == false)
                timesheetLeaveRow.Total = total;
            else
                timesheetLeaveRow.PreviousTotal = total;
            return total;
        }

        private static double SetLeaveDays(List<TimesheetGridBO> projectList, 
            List<LeaveDetail> filtertimesheetLeaveList, TimesheetGridBO timesheetLeaveRow,bool isPrevMonth)
        {
            string dayStartWith = "D";
            string holidayStartWith = "D{0}IsHoliday";

            if (isPrevMonth)
            {
                dayStartWith = "P";
                holidayStartWith = "P{0}IsHoliday";
            }

            double prevTotal = 0, p1_26Total = 0;
            double total = 0;object value = null;
                    
            for (int i = 1; i <= 31; i++)
            {
                LeaveDetail valueEntity = filtertimesheetLeaveList.FirstOrDefault(x => x.DateOnEng.Day == i);

                if (valueEntity != null && valueEntity.IsWeeklyHoliday)
                {
                    //valueen
                    continue;
                }

                PropertyInfo dayProperty = timesheetLeaveRow.GetType().GetProperty(dayStartWith + i);
                if (dayProperty != null)
                {

                    value = null;
                    if (valueEntity != null)
                    {
                        PropertyInfo holidayProperty = timesheetLeaveRow.GetType().GetProperty(string.Format(holidayStartWith,i));
                        if (holidayProperty != null)
                            holidayProperty.SetValue(timesheetLeaveRow, valueEntity.IsHoliday, null);
                        //timesheetLeaveRow.IsHoliday = valueEntity.IsHoliday;

                        value = valueEntity.LeaveRequest.IsHalfDayLeave.Value ? 4 : 8; // for half days = 4, for full days = 8

                        if (valueEntity.IsHoliday)
                        {
                            //for holiday also there could be overtime so to display holiday value we must check if there is value in proejct
                            // on that day & if it has then we should deduct that day in holiday display
                            //first find if there is value in project on this day
                            double projectTotalHours = GetProjectHourOnThisDay(i, projectList);
                            value = (object)(Convert.ToDouble(value) - projectTotalHours);
                        }
                        total += (double)Convert.ToDecimal(value);
                    }
                    if (Convert.ToDouble(value) != 0)
                        dayProperty.SetValue(timesheetLeaveRow, Convert.ToDouble(value), null);

                }


                if (isPrevMonth && valueEntity != null && i <= 26)
                {
                    value = valueEntity.LeaveRequest.IsHalfDayLeave.Value ? 4 : 8; //

            //        if ( )
                    {                        
                        p1_26Total += (double)Convert.ToDecimal(value);
                    }
                  
                    {
                        value = valueEntity.LeaveRequest.IsHalfDayLeave.Value ? 4 : 8; //
                        total += (double)Convert.ToDecimal(value);
                    }
                }
            }
            if (isPrevMonth == false)
                timesheetLeaveRow.Total = total;
            else
            {
                timesheetLeaveRow.PreviousTotal = total;
                timesheetLeaveRow.P1_26Total = p1_26Total;
            }
            return total;
        }

        public static double GetProjectHourOnThisDay(int day, List<TimesheetGridBO> projectList)
        {
            double total = 0;
            object value;
            foreach (TimesheetGridBO item in projectList)
            {
                PropertyInfo dayProperty = item.GetType().GetProperty("D" + day);
                if (dayProperty != null)
                {
                    value = dayProperty.GetValue(item, null);
                    if (value != null)
                        total += Convert.ToDouble(value);
                }
            }
            return total;
        }
    }
}
