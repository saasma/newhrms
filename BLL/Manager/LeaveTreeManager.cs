﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using DAL;
using Utils;
using System.Xml.Linq;
using Utils.Calendar;
using BLL.Entity;
using BLL.BO;

namespace BLL.Manager
{
    public class LeaveTreeManager:BaseBiz
    {

        public static void Update(int teamId, string employeeList)
        {
            string[] list = employeeList.Split(new char[] { ',' });
            foreach (string item in list)
            {
                string value = item.Replace("E", "");

                LeaveProjectEmployee db = PayrollDataContext.LeaveProjectEmployees
                    .FirstOrDefault(x => x.EmployeeId == int.Parse(value));
                if (db != null)
                    db.LeaveProjectId = teamId;
                else
                {
                    db = new LeaveProjectEmployee();
                    db.LeaveProjectId = teamId;
                    db.EmployeeId = int.Parse(value);

                    PayrollDataContext.LeaveProjectEmployees.InsertOnSubmit(db);
                }
            }

            PayrollDataContext.SubmitChanges();
        }

        public static List<Branch> GetAllBranches()
        {
            return PayrollDataContext.Branches.OrderBy(x => x.BranchId).ToList();
        }

        public static List<JoinLeaveDeptTreePanelResult> GetJoinedLeaveDepartment()
        {
            List<JoinLeaveDeptTreePanelResult> iList = new List<JoinLeaveDeptTreePanelResult>();

            iList = PayrollDataContext.JoinLeaveDeptTreePanel().ToList();

            return iList;
        }

        public static List<LeaveTreeJoinBranchDepartmentResult> GetAllJoinedDepartments()
        {
            List<LeaveTreeJoinBranchDepartmentResult> iList = new List<LeaveTreeJoinBranchDepartmentResult>();

            iList = PayrollDataContext.LeaveTreeJoinBranchDepartment().ToList();

            return iList;
        }

        public static List<EEmployee> GetAllCurrentEmployees()
        {
            List<EEmployee> lstAllEmployees = PayrollDataContext.EEmployees.OrderBy(x => x.Name).ToList();

            List<EEmployee> lstWorkingEmployees = new List<EEmployee>();

            foreach (EEmployee employee in lstAllEmployees)
            {
                if (PayrollDataContext.IsEmployeeNotRetOrResigned(employee.EmployeeId) == true)
                {
                    lstWorkingEmployees.Add(employee);
                }
            }

            return lstWorkingEmployees;
        }

        public static string GetProjectNameofEmployee(int empId)
        {
            var list = PayrollDataContext.LeaveProjectEmployees
                            .Join(PayrollDataContext.LeaveProjects,
                            e => e.LeaveProjectId, p => p.LeaveProjectId,
                            (e, p) => new
                            {
                                LeaveProjectName = p.Name,
                                e.EmployeeId,
                                p.LeaveProjectId
                            });

            string returnItem = null;

            foreach (var listItem in list)
            {
                if (listItem.EmployeeId == empId)
                {
                    returnItem = listItem.LeaveProjectName;
                    break;
                }
            }

            return returnItem;
        }

        public static List<LeaveTreeJoinProjectBranchResult> GetAllJoinedLeaveProjects()
        {
            List<LeaveTreeJoinProjectBranchResult> iList = new List<LeaveTreeJoinProjectBranchResult>();

            iList = PayrollDataContext.LeaveTreeJoinProjectBranch().ToList();

            return iList;
        }

        public static List<LeaveTreeJoinProjectEmployeeBranchResult> GetAllJoinedLeaveProjectEmployees()
        {
            List<LeaveTreeJoinProjectEmployeeBranchResult> iList = new List<LeaveTreeJoinProjectEmployeeBranchResult>();

            iList = PayrollDataContext.LeaveTreeJoinProjectEmployeeBranch().ToList();

            return iList;
        }
    }
}
