﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;
using BLL.BO;
using Utils;
using System.Reflection;
using BLL.Entity;
using System.Globalization;

using System.Web;
using Utils.Calendar;

namespace BLL.Manager
{
    public class NewTimeSheetManager : BaseBiz
    {
        public static  long[] CareFixedActivityID = { 999999999999999, 999999999999994, 999999999999995};

        public bool HideSubProject 
        {
            get { return false; }
            set { }
        }

        public static int TOTAL_WEEKS = 5;

        public static bool IsValidActivityIDForCare(long value,ref string msg,bool allowZeroAlso)
        {
            msg = "";

            bool state =  (value >= 1 && value <= 500) || value == 999999999999999;// || value == 0;

            if (state)
                return true;

            if (allowZeroAlso && value == 0)
                return true;

            foreach (var item in CareFixedActivityID)
            {
                if (item == value)
                    return true;
            }

            msg = "Activity should be 1-500 ";
            foreach (var item in CareFixedActivityID)
            {
                msg += " or " + item;
            }


            return false;
        }

        public static bool CheckInExists(DAL.AttendanceMapping map, DateTime currentDate)
        {
            bool checkInExists = BLL.BaseBiz.PayrollDataContext.AttendanceCheckInCheckOuts
                .Any(x => x.DeviceID == map.DeviceId
                    && x.DateTime.Year == currentDate.Year
                    && x.DateTime.Month == currentDate.Month
                    && x.DateTime.Day == currentDate.Day);
            return checkInExists;
        }

        public static bool DeleteDarftTimeSheet(int id)
        {
            PayrollDataContext.Timesheets.DeleteOnSubmit(
               PayrollDataContext.Timesheets.SingleOrDefault(x => x.TimesheetId == id));

            Timesheet dbEntity = PayrollDataContext.Timesheets.SingleOrDefault(c => c.TimesheetId == id);
            int empId = dbEntity.EmployeeId;

            EEmployee  _Employeee = EmployeeManager.GetEmployeeById(empId);
            string EmployeeName = _Employeee.Name;


            string MonthName = Convert.ToDateTime(dbEntity.StartDate).Year + " - " +
            DateHelper.GetMonthName(Convert.ToDateTime(dbEntity.StartDate).Month, true);

            PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(dbEntity.EmployeeId, (byte)MonetoryChangeLogTypeEnum.Timesheet,
            "","", "Timesheet of " + EmployeeName + " has been Deleted for the  month " + MonthName, LogActionEnum.Delete));

            PayrollDataContext.SubmitChanges();
            return true;
        }

        public static void PunchInForD2InEmployeeLogin(int empId)
        {
            if (CommonManager.CompanySetting.IsD2 == false)
                return;

            DAL.AttendanceMapping map = BLL.BaseBiz.PayrollDataContext.AttendanceMappings
                  .SingleOrDefault(x => x.EmployeeId == empId);

            if (map == null)
            {                
                return;
            }

            DateTime currentDate = DateTime.Now; //Utils.Helper.Util.GetCurrentDateAndTime();

            bool checkInExists = CheckInExists(map, currentDate);


            if (checkInExists == false)
            {
                AttendanceCheckInCheckOut chkInOut = new AttendanceCheckInCheckOut();
                chkInOut.ChkInOutID = Guid.NewGuid();
                chkInOut.DeviceID = map.DeviceId;
                chkInOut.AuthenticationType = (byte)TimeAttendanceAuthenticationType.SelfManualEntryFromSoftware;
                chkInOut.InOutMode = (byte)0 ;
                chkInOut.IPAddress = HttpContext.Current.Request.UserHostAddress;
                //chkInOut.ManualVisibleTime = lblTime.Text.Trim();



                DateTime chkinoutDate = currentDate;
                DateTime chkinoutTime = currentDate;

                chkInOut.DateTime = new DateTime(chkinoutDate.Year, chkinoutDate.Month, chkinoutDate.Day, chkinoutTime.Hour, chkinoutTime.Minute, chkinoutTime.Second);

                chkInOut.ModificationRemarks = "Manual Attendance";

                AttendanceManager.SaveOrUpdateAttendanceException(chkInOut);

            }
        }

        //public static Status ImportFromJira(List<WorkLogItem> list)
        //{
        //    Status status = new Status();

        //    int failedDueToEmpNotExists = 0;
            

        //    foreach (WorkLogItem item in list)
        //    {
        //        EHumanResource employee = PayrollDataContext.EHumanResources.FirstOrDefault(
        //            x => x.IdCardNo.ToLower().Trim().Equals(item.updateAuthor.name.ToLower().Trim()));

        //        if (employee == null)
        //        {
        //            failedDueToEmpNotExists += 1;
        //            continue;
        //        }

        //        // 1. Project Processing
        //        Project project = PayrollDataContext.Projects.FirstOrDefault(x => x.OtherName.ToLower().Trim()
        //            == item.Project.value.ToLower().Trim());

        //        if (project == null)
        //        {
        //            project = new Project { Name = item.Project.value.Trim(), OtherName = item.Project.value.Trim(), CompanyId = SessionManager.CurrentCompanyId };
        //            PayrollDataContext.Projects.InsertOnSubmit(project);                    

        //            //ProjectEmployee prjEmp = new ProjectEmployee{ EmployeeId = employee.EmployeeId,ProjectId = project.ProjectId};
        //            //PayrollDataContext.ProjectEmployees.InsertOnSubmit(prjEmp);

        //            PayrollDataContext.SubmitChanges();
        //        }

        //        ProjectEmployee projectEmployee = PayrollDataContext.ProjectEmployees.FirstOrDefault(x => x.EmployeeId == employee.EmployeeId
        //            && x.ProjectId == project.ProjectId);

        //        if (projectEmployee == null)
        //        {
        //            ProjectEmployee prjEmp = new ProjectEmployee { EmployeeId = employee.EmployeeId, ProjectId = project.ProjectId };
        //            PayrollDataContext.ProjectEmployees.InsertOnSubmit(prjEmp);
        //            PayrollDataContext.SubmitChanges();
        //        }

        //        // 2. Sub-Project Processing
        //        ProjectSubProject subProject = PayrollDataContext.ProjectSubProjects.FirstOrDefault(x => x.OtherName.ToLower().Trim()
        //            == item.SubProject.value.ToLower().Trim() && x.ProjectId == project.ProjectId);

        //        if (subProject == null)
        //        {
        //            subProject = new ProjectSubProject { Name = item.SubProject.value.Trim(), OtherName = item.SubProject.value.Trim(), ProjectId = project.ProjectId };
        //            PayrollDataContext.ProjectSubProjects.InsertOnSubmit(subProject);
        //            PayrollDataContext.SubmitChanges();
        //        }

        //        // 3. Task
        //        ProjectTask task = PayrollDataContext.ProjectTasks.FirstOrDefault(x => x.OtherName.ToLower().Trim()
        //            == item.Task.name.ToLower().Trim() && x.SubProjectId == subProject.SubProjectId);

        //        if (task == null)
        //        {
        //            task = new ProjectTask { Name = item.Task.name.Trim(), OtherName = item.Task.name.Trim(), SubProjectId = subProject.SubProjectId };
        //            PayrollDataContext.ProjectTasks.InsertOnSubmit(task);
        //            PayrollDataContext.SubmitChanges();
        //        }

        //        // 4. TimesheetProject
        //        TimesheetProject timesheet = new TimesheetProject();
        //        timesheet.EmployeeId = employee.EmployeeId;
        //        timesheet.ProjectId = project.ProjectId;
        //        timesheet.SubProjectId = subProject.SubProjectId;
        //        timesheet.TaskId = task.TaskId;
        //        timesheet.Hours = item.timeSpentSeconds / 60 / 60;
        //        timesheet.DayNumber = (byte)item.started.DayOfWeek;
        //        timesheet.DateEng = item.started;
        //        timesheet.IsFixed = true;
        //        timesheet.Notes = item.comment;

        //        PayrollDataContext.TimesheetProjects.InsertOnSubmit(timesheet);

                

        //    }

        //    PayrollDataContext.SubmitChanges();

        //    if (failedDueToEmpNotExists != 0)
        //    {
        //        status.ErrorMessage = "Failed for : " + failedDueToEmpNotExists;
        //    }
        //    else
        //    {
        //        status.ErrorMessage = "Sucessfull imported for : " + list.Count;
        //    }
        //    return status;
        //}

        public static int GetTimesheetCountForManager()
        {
          
            return
                 PayrollDataContext.GetTimeSheetListForApproval(0, int.MaxValue, -1, null, null, SessionManager.CurrentLoggedInEmployeeId
                , -2).ToList().Count;
        }



        public static string GetWeekRangeName(DateTime weekStartDate, DateTime weekEndDate, bool isMonthly)
        {

            if (!isMonthly)
            {
                return weekStartDate.ToString("MMM d") + ", " + weekStartDate.Year + " - " +
             weekEndDate.ToString("MMM d") + ", " + weekEndDate.Year;
            }
            else
                return weekStartDate.ToString("MMM") + ", " + weekStartDate.Year;
        }


        public static Status ReviewTimeSheet(List<Timesheet> list)
        {
            Status status = new Status();


            foreach (var item in list)
            {
                Timesheet dbEntity = PayrollDataContext.Timesheets.SingleOrDefault(x => x.TimesheetId == item.TimesheetId);

                //if (IsTimesheetViewableForManager(dbEntity.TimesheetId) == false)
                //{
                //    status.ErrorMessage = "Not enough permission to review the timesheet.";
                //    return status;
                //}

                if (SessionManager.User.UserMappedEmployeeId == null)
                {
                    status.ErrorMessage = "Current user should map with employee from \"Manage Users\" for any leave action.";
                    return status;
                }

                if (item.TimesheetId == 0)//Not Filled Status
                {
                    status.ErrorMessage = "Timesheet is in Not Submitted".ToString()
                        + " status, only  Approved status timesheet can be approved.";
                    return status;
                }

                if ((dbEntity.Status != (int)TimeSheetStatus.Approved))
                {
                    status.ErrorMessage = "Timesheet is in " + ((TimeSheetStatus)dbEntity.Status).ToString()
                        + " status, only  Approved status timesheet can be approved.";
                    return status;
                }

                dbEntity.ReviewedEmployeeId = SessionManager.CurrentLoggedInEmployeeId;
                dbEntity.ReviewedOn = BLL.BaseBiz.GetCurrentDateAndTime();
                dbEntity.Status = (int)TimeSheetStatus.Reviewed;

            }
            status.IsSuccess = true;
            PayrollDataContext.SubmitChanges();

            return status;
        }

        public static List<GetTimeSheetListForApprovalCountResult> GetTimeSheetListForApprovalCount(int employeeId,
          string startDate, string endDate, int status, bool isMonthly)
        {
            DateTime? startDateEng = null, endDateEng = null;
            if (!string.IsNullOrEmpty(startDate))
                startDateEng = Convert.ToDateTime(startDate);

            if (!string.IsNullOrEmpty(endDate))
                endDateEng = Convert.ToDateTime(endDate);

            List<GetTimeSheetListForApprovalCountResult> list =
                PayrollDataContext.GetTimeSheetListForApprovalCount(employeeId, startDateEng, endDateEng, SessionManager.CurrentLoggedInEmployeeId
                , status).ToList();


            return list;
        }

        public static List<GetTimeSheetListForApprovalResult> GetTimeSheetListForApproval(int start, int pagesize, int employeeId, 
            string startDate, string endDate,int status,bool isMonthly)
        {
            DateTime? startDateEng = null, endDateEng = null;
            if (!string.IsNullOrEmpty(startDate))
            {
                startDateEng = Convert.ToDateTime(startDate);
                startDateEng = new DateTime(startDateEng.Value.Year, startDateEng.Value.Month, 1);
            }

            if (!string.IsNullOrEmpty(endDate))
                endDateEng = Convert.ToDateTime(endDate);

            List<GetTimeSheetListForApprovalResult> list=
                PayrollDataContext.GetTimeSheetListForApproval(start, pagesize, employeeId, startDateEng, endDateEng,SessionManager.CurrentLoggedInEmployeeId
                ,status).ToList();

            foreach (GetTimeSheetListForApprovalResult item in list)
            {
                item.StatusText =
                   HttpContext.GetGlobalResourceObject("FixedValues", ((TimeSheetStatus)item.Status).ToString()) == null ?
                   ((TimeSheetStatus)item.Status).ToString() :
                   HttpContext.GetGlobalResourceObject("FixedValues", ((TimeSheetStatus)item.Status).ToString()).ToString();

                ;
                item.Month = item.StartDate.Value.Year + " - " +
                    DateHelper.GetMonthName(item.StartDate.Value.Month, true);

                item.WeekText = GetWeekRangeName(item.StartDate.Value, item.EndDate.Value, isMonthly);
            }

            //foreach (GetTimeSheetListForApprovalResult item in list)
            //{
            //    item.StatusText = ((TimeSheetStatus)item.Status).ToString();
            //    item.WeekText = GetWeekRangeName(item.StartDate.Value, item.EndDate.Value, isMonthly);
            //}


            return list;
        }

        public static List<GetTimeSheetListResult> GetTimeSheetList(int start, int pagesize, int employeeId,
            string startDate, string endDate, int status)
        {
            DateTime? startDateEng = null, endDateEng = null;
            if (!string.IsNullOrEmpty(startDate))
            {
                startDateEng = Convert.ToDateTime(startDate);
                startDateEng = new DateTime(startDateEng.Value.Year, startDateEng.Value.Month, 1);
            }

            if (!string.IsNullOrEmpty(endDate))
                endDateEng = Convert.ToDateTime(endDate);

            List<GetTimeSheetListResult> list =
                PayrollDataContext.GetTimeSheetList(start, pagesize, employeeId, startDateEng, endDateEng,status).ToList();

            foreach (GetTimeSheetListResult item in list)
            {
                item.StatusText =
                   HttpContext.GetGlobalResourceObject("FixedValues", ((TimeSheetStatus)item.Status).ToString()) == null ?
                   ((TimeSheetStatus)item.Status).ToString() :
                   HttpContext.GetGlobalResourceObject("FixedValues", ((TimeSheetStatus)item.Status).ToString()).ToString();

                ;
                item.Month = item.StartDate.Value.Year + " - " +
                    DateHelper.GetMonthName(item.StartDate.Value.Month, true);
            }

            return list;
        }




        public static void SetTimeSheetCount(ref int? pendingCount, ref int? recommendCount, ref int? approvedCount)
        {

            if (SessionManager.CurrentLoggedInEmployeeId != 0)
            {
                PayrollDataContext.GetTimesheetRequestCountForManager
              (SessionManager.CurrentLoggedInEmployeeId, (int)TimeSheetStatus.AwaitingApproval, ref pendingCount);

                PayrollDataContext.GetTimesheetRequestCountForManager
                     (SessionManager.CurrentLoggedInEmployeeId, (int)TimeSheetStatus.Approved, ref approvedCount);

            }

            else
            {
                pendingCount = PayrollDataContext.Timesheets.Where(x => x.Status == (int)TimeSheetStatus.AwaitingApproval).Count();
                recommendCount = 0;
                approvedCount = PayrollDataContext.Timesheets.Where(x => x.Status == (int)TimeSheetStatus.Approved).Count();
            }


            //List<GetTimeSheetListForApprovalResult> resultSet = NewTimeSheetManager.GetTimeSheetListForApproval(0, int.MaxValue, SessionManager.CurrentLoggedInEmployeeId, null, null,-2);

            //pendingCount = resultSet.Where(x => x.StatusText.ToLower() == Enum.GetName(typeof(TimeSheetStatus), TimeSheetStatus.AwaitingApproval).ToLower()).ToList().Count();
            //approvedCount = resultSet.Where(x => x.StatusText.ToLower() == Enum.GetName(typeof(TimeSheetStatus), TimeSheetStatus.Approved).ToLower()).ToList().Count();

        }


        public static List<GetEmployeProjectContributionListResult> GetEmployeProjectContributionList(int start, int pagesize, int employeeId,string startDate)
        {
            DateTime? startDateEng = null;
            if (!string.IsNullOrEmpty(startDate))
            {
                startDateEng = Convert.ToDateTime(startDate);
                startDateEng = new DateTime(startDateEng.Value.Year, startDateEng.Value.Month, 1);
            }

         
                DateTime weekStart = Convert.ToDateTime(startDate);//Convert.ToDateTime(hdnStartDate.Text);
                weekStart = new DateTime(weekStart.Year, weekStart.Month, 1);

                DateTime monthEndDate = new DateTime(weekStart.Year, weekStart.Month,
                DateTime.DaysInMonth(weekStart.Year, weekStart.Month));

                DateTime week1 = weekStart;
                DateTime week2 = week1.AddDays(7);
                DateTime week3 = week2.AddDays(7);
                DateTime week4 = week3.AddDays(7);
                DateTime week5 = week4.AddDays(7);

                if (week4 < monthEndDate)
                {
                    week5 = week4.AddDays(7);
                }

                List<GetEmployeProjectContributionListResult> list =
                PayrollDataContext.GetEmployeProjectContributionList(start, pagesize, employeeId, week1, week2, week3, week4, week5, monthEndDate).ToList();

            //foreach (GetTimeSheetListResult item in list)
            //{
            //    item.StatusText =
            //       HttpContext.GetGlobalResourceObject("FixedValues", ((TimeSheetStatus)item.Status).ToString()) == null ?
            //       ((TimeSheetStatus)item.Status).ToString() :
            //       HttpContext.GetGlobalResourceObject("FixedValues", ((TimeSheetStatus)item.Status).ToString()).ToString();

            //    ;
            //    item.Month = item.StartDate.Value.Year + " - " +
            //        DateHelper.GetMonthName(item.StartDate.Value.Month, true);
            //}

            return list;
        }


        public static string GetWeekRangeName(DateTime weekStartDate, DateTime weekEndDate)
        {
            return
                 weekStartDate.ToString("MMM d") + ", " + weekStartDate.Year + " - " +
                 weekEndDate.ToString("MMM d") + ", " + weekEndDate.Year;
        }
        public static List<GetTimeSheetListForEmployeeResult> GetTimeSheetListForEmployee(int start, int pagesize, int employeeId, string startDate, string endDate, int status, bool isMonthly)
        {
            DateTime? startDateEng = null, endDateEng = null;
            if (!string.IsNullOrEmpty(startDate))
            {
                startDateEng = Convert.ToDateTime(startDate);
                startDateEng = new DateTime(startDateEng.Value.Year, startDateEng.Value.Month, 1);
            }
            if (!string.IsNullOrEmpty(endDate))
                endDateEng = Convert.ToDateTime(endDate);

            List<GetTimeSheetListForEmployeeResult> list =
                PayrollDataContext.GetTimeSheetListForEmployee(start, pagesize, employeeId, startDateEng, endDateEng, status).ToList();

            foreach (GetTimeSheetListForEmployeeResult item in list)
            {
                item.StatusText =
                    HttpContext.GetGlobalResourceObject("FixedValues", ((TimeSheetStatus)item.Status).ToString()) == null?
                    ((TimeSheetStatus)item.Status).ToString() : 
                    HttpContext.GetGlobalResourceObject("FixedValues", ((TimeSheetStatus)item.Status).ToString()).ToString();
                    
                    ;
                item.Month = item.StartDate.Value.Year+ " - " +
                    DateHelper.GetMonthName(item.StartDate.Value.Month, true);

                item.WeekText = GetWeekRangeName(item.StartDate.Value, item.EndDate.Value, isMonthly);

                item.StartDateText = HttpContext.Current.Server.UrlEncode(item.StartDate.Value.ToShortDateString());
            }

            return list;
        }

        public static List<GetTimeSheetListCountResult> GetTimeSheetListCount(int employeeId,string startDate, string endDate, int status, bool isMonthly)
        {
            DateTime? startDateEng = null, endDateEng = null;
            if (!string.IsNullOrEmpty(startDate))
                startDateEng = Convert.ToDateTime(startDate);

            if (!string.IsNullOrEmpty(endDate))
                endDateEng = Convert.ToDateTime(endDate);

            List<GetTimeSheetListCountResult> list =
                PayrollDataContext.GetTimeSheetListCount(employeeId, startDateEng, endDateEng, status).ToList();

            return list;
        }
      
        public static Status RejectTimeSheet(Timesheet obj)
        {
            Status status = new Status();

            Timesheet dbEntity = PayrollDataContext.Timesheets.SingleOrDefault(x => x.TimesheetId == obj.TimesheetId);
            
            if (dbEntity != null)
            {
                if (IsTimesheetViewableForManager(dbEntity.TimesheetId)==false)
                {
                    status.ErrorMessage = "Not enough permission to reject the timesheet.";
                    return status;
                }

                if (dbEntity.Status != (int)TimeSheetStatus.AwaitingApproval)
                {
                    status.ErrorMessage = "Timesheet is in " + ((TimeSheetStatus)dbEntity.Status).ToString()
                        + " status, only Submitted timesheet can be rejected.";
                    return status;
                }

                dbEntity.ApprovedOn = DateTime.Now;
                dbEntity.ApprovedBy = SessionManager.CurrentLoggedInEmployeeId;
                dbEntity.Status = obj.Status;
                dbEntity.Notes = obj.Notes;
                PayrollDataContext.SubmitChanges();
                status.IsSuccess = true;
            }
            else
            {
                status.ErrorMessage = "Time sheet not found.";
                status.IsSuccess = false;
            }

            return status;
        }

        public static  List<Timesheet> GetAllTimeSheet()
        {
            return PayrollDataContext.Timesheets.ToList();
        }

        public static Status ApproveTimeSheet(List<Timesheet> list)
        {
            Status status = new Status();
            foreach (var item in list)
            {
                Timesheet dbEntity = PayrollDataContext.Timesheets.SingleOrDefault(x => x.TimesheetId == item.TimesheetId);

                if (IsTimesheetViewableForManager(dbEntity.TimesheetId) == false)
                {
                    status.ErrorMessage = "Not enough permission to approve the timesheet.";
                    return status;
                }

                if (dbEntity.Status != (int)TimeSheetStatus.AwaitingApproval)
                {
                    status.ErrorMessage = "Timesheet is in " + ((TimeSheetStatus)dbEntity.Status).ToString()
                        + " status, only Submitted status timesheet can be approved.";
                    return status;
                }

                dbEntity.ApprovedBy = SessionManager.CurrentLoggedInEmployeeId;
                dbEntity.ApprovedOn = BLL.BaseBiz.GetCurrentDateAndTime();
                dbEntity.Status = (int)TimeSheetStatus.Approved;

            }
            status.IsSuccess = true;
            PayrollDataContext.SubmitChanges();

            return status;
        }


    
        public static ResponseStatus SaveUpdateProjectTimesheet(DateTime weekStartDate, double totalHours, int employeeId,
            List<TimesheetProject> projectHourList, List<TimesheetLeave> leaveList,int timeSheetStatus,int approvalEmpId)
        {
            ResponseStatus status = new ResponseStatus();
            SetConnectionPwd(PayrollDataContext.Connection);
            PayrollDataContext.Transaction = PayrollDataContext.Connection.BeginTransaction();


            DateTime weekEndDate = weekStartDate.AddDays(6);

            try
            {

                Timesheet dbTimesheet = PayrollDataContext.Timesheets
                    .SingleOrDefault(x => x.StartDate == weekStartDate && x.EmployeeId == employeeId);


                // first delete old project values
                List<TimesheetProject> dbProjectList = new List<TimesheetProject>();
                
                if(dbTimesheet != null)
                    dbProjectList =  PayrollDataContext
                        .TimesheetProjects.Where(x => x.TimesheetId == dbTimesheet.TimesheetId).ToList();

                List<TimesheetLeave> dbLeaveList = new List<TimesheetLeave>();
                
                if(dbTimesheet   != null)
                    dbLeaveList =  PayrollDataContext
                        .TimesheetLeaves.Where(x => x.TimesheetId == dbTimesheet.TimesheetId).ToList();

                PayrollDataContext.TimesheetProjects.DeleteAllOnSubmit(dbProjectList);
                PayrollDataContext.TimesheetLeaves.DeleteAllOnSubmit(dbLeaveList);

                //if (dbTimesheet != null)
                //{
                //    PayrollDataContext.TimesheetLeaves.DeleteAllOnSubmit(dbTimesheet.TimesheetLeaves);
                //    PayrollDataContext.SubmitChanges();
                //}
                if (dbTimesheet == null)
                {
                    dbTimesheet = new Timesheet();
                    dbTimesheet.ApprovedEmployeeId = approvalEmpId;
                    dbTimesheet.CreatedBy = SessionManager.CurrentLoggedInEmployeeId;
                    dbTimesheet.StartDate = weekStartDate;
                    dbTimesheet.EndDate = new DateTime(weekStartDate.Year, weekStartDate.Month,
                            DateTime.DaysInMonth(weekStartDate.Year, weekStartDate.Month));
                    //dbTimesheet.WeekNo = GetIso8601WeekOfYear(weekStartDate);
                    dbTimesheet.EmployeeId = employeeId;
                    dbTimesheet.SubmittedOn = GetCurrentDateAndTime();
                    PayrollDataContext.Timesheets.InsertOnSubmit(dbTimesheet);
                }

                if(dbTimesheet != null)
                {
                    dbTimesheet.ApprovedEmployeeId = approvalEmpId;

                    if (dbTimesheet.Status != (byte)TimeSheetStatus.Draft &&
                        (int)dbTimesheet.Status != (int)TimeSheetStatus.Rejected
                        && (int)dbTimesheet.Status != (int)TimeSheetStatus.AwaitingApproval)
                    {
                        status.ErrorMessage = "Approved timesheet can not be changed.";
                        status.IsSuccessType = false;
                        return status;
                    }
                }


                if (timeSheetStatus == (int)TimeSheetStatus.Approved)
                {
                    dbTimesheet.ApprovedBy = SessionManager.CurrentLoggedInEmployeeId;
                    dbTimesheet.CreatedBy = SessionManager.CurrentLoggedInEmployeeId;
                    dbTimesheet.ApprovedOn = GetCurrentDateAndTime();
                }
                //if (timeSheetStatus == (int)TimeSheetStatus.ReviewedByHR)
                //{
                //    dbTimesheet.ReviewedHREmployeeId = SessionManager.CurrentLoggedInEmployeeId;
                //    dbTimesheet.ReviewedOn = GetCurrentDateAndTime();
                //}

                dbTimesheet.Status = (byte)timeSheetStatus;
                dbTimesheet.TotalHours = totalHours;
              
                dbTimesheet.TimesheetProjects.AddRange(projectHourList);
                dbTimesheet.TimesheetLeaves.AddRange(leaveList);

                PayrollDataContext.SubmitChanges();
                PayrollDataContext.Transaction.Commit();
                //return true;
            }
            catch (Exception exp)
            {
                Log.log("Error while saving new emp.", exp);
                PayrollDataContext.Transaction.Rollback();
                status.ErrorMessage = exp.Message;

            }
            return status;
        }



        public static ResponseStatus SaveUpdateMonthlyProjectTimesheet(DateTime weekStartDate, double totalHours, int employeeId,
         List<TimesheetProject> projectHourList, List<TimesheetLeave> leaveList, int timeSheetStatus, bool isMonthly,
         List<TimesheetProjectTemplate> templateList, double? totalProjectHours, int approvalEmpId)
        {
            ResponseStatus status = new ResponseStatus();
            SetConnectionPwd(PayrollDataContext.Connection);
            PayrollDataContext.Transaction = PayrollDataContext.Connection.BeginTransaction();
            DateTime weekEndDate;
            if (!isMonthly)
                weekEndDate = weekStartDate.AddDays(6);
            else
            {
                DateTime StartingDateofMonth = new DateTime(weekStartDate.Year, weekStartDate.Month, 1);
                weekStartDate = StartingDateofMonth;
                weekEndDate = StartingDateofMonth.AddMonths(1).AddDays(-1);
            }

            try
            {

                // resave template
                if (templateList != null)
                {
                    List<TimesheetProjectTemplate> dbTemp = PayrollDataContext.TimesheetProjectTemplates.Where(x => x.EmployeeId == employeeId).ToList();
                    PayrollDataContext.TimesheetProjectTemplates.DeleteAllOnSubmit(dbTemp);
                    foreach (var item in templateList)
                        item.EmployeeId = employeeId;
                    PayrollDataContext.TimesheetProjectTemplates.InsertAllOnSubmit(templateList);
                }

                Timesheet dbTimesheet = PayrollDataContext.Timesheets
                    .SingleOrDefault(x => x.StartDate == weekStartDate && x.EmployeeId == employeeId);


                // first delete old project values
                List<TimesheetProject> dbProjectList = PayrollDataContext
                    .TimesheetProjects.Where(x => x.DateEng >= weekStartDate && x.DateEng <= weekEndDate && x.EmployeeId == employeeId).ToList();

                List<TimesheetLeave> dbLeaveList = PayrollDataContext
                    .TimesheetLeaves.Where(x => x.DateEng >= weekStartDate && x.DateEng <= weekEndDate && x.EmployeeId == employeeId).ToList();

                //#region "Validate IsFixed"
                //// Valid IsFixed of Timesheet
                //double totalDBIsFixed = dbProjectList.Where(x => x.IsFixed == true).Sum(x => x.Hours == null ? 0 : x.Hours.Value);
                //double totalInputIsFixed = projectHourList.Where(x => x.IsFixed == true).Sum(x => x.Hours == null ? 0 : x.Hours.Value);
                //if (totalDBIsFixed > 0 && totalDBIsFixed != totalInputIsFixed)
                //{
                //    status.ErrorMessage = "Imported hours can not be changed.";
                //    return status;
                //}
                //#endregion



                PayrollDataContext.TimesheetProjects.DeleteAllOnSubmit(dbProjectList);
                PayrollDataContext.TimesheetLeaves.DeleteAllOnSubmit(dbLeaveList);


                //if (dbTimesheet != null)
                //{
                //    PayrollDataContext.TimesheetLeaves.DeleteAllOnSubmit(dbTimesheet.TimesheetLeaves);
                //    PayrollDataContext.SubmitChanges();
                //}
                if (dbTimesheet == null)
                {
                    dbTimesheet = new Timesheet();
                    dbTimesheet.StartDate = weekStartDate;
                    if (!isMonthly)
                        dbTimesheet.EndDate = weekStartDate.AddDays(6);
                    else
                    {
                        DateTime StartingDateofMonth = new DateTime(weekStartDate.Year, weekStartDate.Month, 1);
                        weekStartDate = StartingDateofMonth;
                        weekEndDate = StartingDateofMonth.AddMonths(1).AddDays(-1);
                        dbTimesheet.EndDate = weekEndDate;
                    }

                    dbTimesheet.WeekNo = GetIso8601WeekOfYear(weekStartDate);
                    dbTimesheet.EmployeeId = employeeId;
                    dbTimesheet.ApprovalEmployeeId = approvalEmpId;
                    PayrollDataContext.Timesheets.InsertOnSubmit(dbTimesheet);
                }

                if (dbTimesheet != null)
                {
                    dbTimesheet.ApprovalEmployeeId = approvalEmpId;

                    if (dbTimesheet.Status != (byte)TimeSheetStatus.Draft &&
                        (int)dbTimesheet.Status != (int)TimeSheetStatus.Rejected)
                    {
                        status.ErrorMessage = "Submitted timesheet can not be changed.";
                        status.IsSuccessType = false;
                        return status;
                    }
                }




                if (timeSheetStatus == (int)TimeSheetStatus.Approved)
                {
                    dbTimesheet.ApprovedEmployeeId = SessionManager.CurrentLoggedInEmployeeId;
                    dbTimesheet.ApprovedOn = GetCurrentDateAndTime();
                }
                if (timeSheetStatus == (int)TimeSheetStatus.ReviewedByHR)
                {
                    dbTimesheet.ReviewedHREmployeeId = SessionManager.CurrentLoggedInEmployeeId;
                    dbTimesheet.ReviewedOn = GetCurrentDateAndTime();
                }

                dbTimesheet.TotalProjectHours = totalProjectHours;
                dbTimesheet.Status = (byte)timeSheetStatus;
                dbTimesheet.TotalHours = totalHours;

                if (CommonManager.CompanySetting.IsD2)
                {
                    // add time card row
                    TimesheetGridNewBO timecard = NewTimeSheetManager.GetTimecard(employeeId, weekStartDate);
                    TimeSpan span = TimeSpan.FromMinutes(timecard.Total == null ? 0 : timecard.Total.Value);

                    var hours = (int)span.TotalHours;
                    var minutes = span.Minutes;

                    dbTimesheet.WorkHours = hours + ":" + minutes;
                }


                dbTimesheet.TimesheetProjects.AddRange(projectHourList);
                dbTimesheet.TimesheetLeaves.AddRange(leaveList);

                PayrollDataContext.SubmitChanges();
                PayrollDataContext.Transaction.Commit();
                //return true;
            }
            catch (Exception exp)
            {
                Log.log("Error while saving new emp.", exp);
                PayrollDataContext.Transaction.Rollback();
                status.ErrorMessage = exp.Message;

            }
            return status;
        }


        public static TimesheetGridNewBO GetTimecard(int employeeId, DateTime weekStartDate)
        {
            TimesheetGridNewBO time = new TimesheetGridNewBO();

            weekStartDate = GetStartDate(weekStartDate);
            DateTime weekEndDate = GetEndDate(weekStartDate.AddDays(6));

            DAL.AttendanceMapping map = BLL.BaseBiz.PayrollDataContext.AttendanceMappings
                    .SingleOrDefault(x => x.EmployeeId == employeeId);

            if (map == null)
            {
                return time;
            }

            List<AttendanceCheckInCheckOutView> list =
                BLL.BaseBiz.PayrollDataContext.AttendanceCheckInCheckOutViews
                .Where(x => x.DeviceID == map.DeviceId && x.DateTime >= weekStartDate && x.DateTime <= weekEndDate).ToList();

            weekStartDate = weekStartDate.AddDays(-1);
            double? value = null;
            double totalMinutes = 0;
            for (int i = 1; i <= 7; i++)
            {
                weekStartDate = weekStartDate.AddDays(1);

                AttendanceCheckInCheckOutView intime = list.OrderBy(x => x.DateTime)
                    .FirstOrDefault(x => x.DeviceID == map.DeviceId && x.InOutMode == (byte)AttendanceInOutMode.In
                    && x.DateTime.Year == weekStartDate.Year
                    && x.DateTime.Month == weekStartDate.Month
                    && x.DateTime.Day == weekStartDate.Day);

                AttendanceCheckInCheckOutView outTime = list.OrderByDescending(x => x.DateTime)
                   .FirstOrDefault(x => x.DeviceID == map.DeviceId && x.InOutMode == (byte)AttendanceInOutMode.Out
                   && x.DateTime.Year == weekStartDate.Year
                   && x.DateTime.Month == weekStartDate.Month
                   && x.DateTime.Day == weekStartDate.Day);


                // If Out time is null then Pick payroll login time if it is greater then In Time
                //if (outTime == null)
                //{
                //    ChangeUserActivity activityLog = PayrollDataContext.ChangeUserActivities
                //        .OrderByDescending(x => x.DateEng)
                //        .FirstOrDefault(x => x.DateEng.Value.Year == weekStartDate.Year
                //            && x.DateEng.Value.Month == weekStartDate.Month
                //            && x.DateEng.Value.Day == weekStartDate.Day
                //            && x.EmployeeId == employeeId && x.IsLogin != null && x.IsLogin.Value);

                //    if (intime != null && activityLog != null && activityLog.DateEng > intime.DateTime)
                //    {
                //        outTime = new AttendanceCheckInCheckOutView();
                //        outTime.DateTime = activityLog.DateEng;
                //    }

                //}

                if (intime == null && outTime == null)
                {
                    value = null;
                }
                else if (intime == null || outTime == null)
                {
                    value = 0;
                }
                else
                {
                    value =
                        double.Parse((outTime.DateTime - intime.DateTime).TotalMinutes.ToString("N0"));
                    if (value < 0)
                        value = 0;
                }

                PropertyInfo dayProperty = time.GetType().GetProperty("D" + i);

                if (dayProperty != null)
                {
                    dayProperty.SetValue(time, value, null);
                }

                totalMinutes += (value == null ? 0 : value.Value);
            }

            time.Name = "Timecard";
            time.Total = totalMinutes;

            return time;
        }


         public static List<TimesheetGridMonthWiseNewBO> GetRepeatedProjectListMonthWise(DateTime weekStartDate, int employeeId)
        {

            DateTime weekEndDate = weekStartDate.AddMonths(1).AddDays(-1);

            List<TimesheetGridMonthWiseNewBO> list = new List<TimesheetGridMonthWiseNewBO>();


            List<TimesheetProject> timesheetProjectList = PayrollDataContext.TimesheetProjects.Where(
                    x => x.EmployeeId == employeeId && x.DateEng >= weekStartDate && x.DateEng <= weekEndDate)
                    .OrderBy(x => x.TimesheetProjectId).ToList();


            List<GetAssginedEmployeeProjectListResult> projectList = GetEmployeeProjectListForMonth(employeeId, weekStartDate, weekEndDate)
               .OrderBy(x => x.Name).ToList();


            if (timesheetProjectList.Count <= 0)
            {
                
                foreach (var pro in projectList)
                {
                    list.Add(new TimesheetGridMonthWiseNewBO { Id = pro.ProjectId, Name = pro.Name, Code = pro.Code, TotalDays = 0 });

                }

            }
            else
            {
                Project projectname = null;
                ProjectSubProject subprojectname = null;
                ProjectTask taskname = null;


                if (timesheetProjectList[0].Timesheet.Status == (int)TimeSheetStatus.Draft)
                {
                    // if new project exists then add in the list
                    List<TimesheetProject> newProject = new List<TimesheetProject>();
                    foreach (GetAssginedEmployeeProjectListResult pro in projectList)
                    {
                        if (timesheetProjectList.Any(x => x.ProjectId == pro.ProjectId) == false)
                        {
                            timesheetProjectList.Add(new TimesheetProject { ProjectId = pro.ProjectId });
                        }
                    }
                }

                foreach (TimesheetProject item in timesheetProjectList)
                {
                    TimesheetGridMonthWiseNewBO projectRow = null;
                    // Check if the Project with Day Number is blank then get the Project row
                    foreach (TimesheetGridMonthWiseNewBO project in list.Where(x => x.Id == item.ProjectId && x.SubProjectId == item.SubProjectId && x.TaskId == item.TaskId))
                    {
                        PropertyInfo dayValueProperty = project.GetType().GetProperty("D" + item.DayNumber);
                        object objValue = dayValueProperty.GetValue(project, null);
                        double? value = null;
                        if (objValue != null)
                            value = (double)objValue;

                        if (value == null || value == 0)
                        {
                            projectRow = project;
                            break;
                        }
                    }


                    // Create new project row
                    if (projectRow == null)
                    {

                        projectname = ProjectManager.GetProjectById(item.ProjectId.Value);

                        if (item.SubProjectId != null)
                            subprojectname = ProjectManager.GetProjectSubProjectById(item.SubProjectId.Value);
                        if (item.TaskId != null)
                            taskname = ProjectManager.GetProjectTaskById(item.TaskId.Value);

                        //Project pro = ProjectManager.GetProjectById(item.ProjectId.Value);                        
                        projectRow = new TimesheetGridMonthWiseNewBO
                        {
                            Id = item.ProjectId.Value,
                            Name = projectname == null ? "" : projectname.Name,
                            SubProjectId = item.SubProjectId,
                            SubProjectName = subprojectname == null ? "" : subprojectname.Name,
                            TaskId = item.TaskId,
                            TaskName = taskname == null ? "" : taskname.Name,
                        };
                        if (item.TaskId != null)
                            projectRow.TaskName = GetTask(item.TaskId.Value).Name;
                        list.Add(projectRow);
                    }

                    PropertyInfo dayProperty = projectRow.GetType().GetProperty("D" + item.DayNumber);
                    PropertyInfo dayComment = projectRow.GetType().GetProperty("D" + item.DayNumber + "Comment");
                    PropertyInfo dayIsFixed = projectRow.GetType().GetProperty("D" + item.DayNumber + "IsFixed");

                    if (dayProperty != null)
                    {
                        dayProperty.SetValue(projectRow, item.Hours, null);
                    }
                    if (dayComment != null)
                    {
                        dayComment.SetValue(projectRow, item.Notes, null);
                    }
                    if (dayIsFixed != null)
                    {
                        dayIsFixed.SetValue(projectRow, item.IsFixed, null);
                    }

                }

            }

            //set total
            foreach (TimesheetGridMonthWiseNewBO item in list)
            {
                item.Total =
                    (item.D1 == null ? 0 : item.D1.Value) + (item.D2 == null ? 0 : item.D2.Value) + (item.D3 == null ? 0 : item.D3.Value) +
                     (item.D4 == null ? 0 : item.D4.Value) + (item.D5 == null ? 0 : item.D5.Value) + (item.D6 == null ? 0 : item.D6.Value) +
                      (item.D7 == null ? 0 : item.D7.Value) + (item.D8 == null ? 0 : item.D8.Value) + (item.D9 == null ? 0 : item.D9.Value) + (item.D10 == null ? 0 : item.D10.Value)
                      + (item.D11 == null ? 0 : item.D11.Value) + (item.D12 == null ? 0 : item.D12.Value) + (item.D13 == null ? 0 : item.D13.Value) + (item.D14 == null ? 0 : item.D14.Value)
                      + (item.D15 == null ? 0 : item.D15.Value) + (item.D16 == null ? 0 : item.D16.Value) + (item.D17 == null ? 0 : item.D17.Value) + (item.D18 == null ? 0 : item.D18.Value)
                      + (item.D19 == null ? 0 : item.D19.Value) + (item.D20 == null ? 0 : item.D20.Value) + (item.D21 == null ? 0 : item.D21.Value)
                      + (item.D22 == null ? 0 : item.D22.Value) + (item.D23 == null ? 0 : item.D23.Value) + (item.D24 == null ? 0 : item.D24.Value) + (item.D25 == null ? 0 : item.D25.Value)
                      + (item.D26 == null ? 0 : item.D26.Value) + (item.D27 == null ? 0 : item.D27.Value) + (item.D28 == null ? 0 : item.D28.Value) + (item.D29 == null ? 0 : item.D29.Value)
                      + (item.D30 == null ? 0 : item.D30.Value) + (item.D31 == null ? 0 : item.D31.Value) + (item.D32 == null ? 0 : item.D32.Value);
            }

            return list;
        }

        public static ProjectTask GetTask(int taskId)
        {
            return PayrollDataContext.ProjectTasks.FirstOrDefault(x => x.TaskId == taskId);
        }

        public static List<WeeklyHoliday> GetWeeklyHolidayList()
        {
            return PayrollDataContext.WeeklyHolidays.Where(x=>x.CompanyId == SessionManager.CurrentCompanyId).ToList();
        }

        //public static DateTime StartOfWeek( DateTime dt, DayOfWeek startOfWeek)
        //{
        //    int diff = dt.DayOfWeek - startOfWeek;
        //    if (diff < 0)
        //    {
        //        diff += 7;
        //    }

        //    return dt.AddDays(-1 * diff).Date;
        //}

        /// <summary>
        /// Check if the timesheet employee is viewable by Current Logged In Employee or not
        /// </summary>
        /// <param name="timesheetId"></param>
        /// <returns></returns>
        public static bool IsTimesheetViewableForManager(int timesheetId)
        {
            Timesheet sheet = GetTimeSheetById(timesheetId);
            if (sheet != null)
            {
                    return sheet.ApprovedEmployeeId == SessionManager.CurrentLoggedInEmployeeId || sheet.ApprovalEmployeeId == SessionManager.CurrentLoggedInEmployeeId;
             
                //return LeaveAttendanceManager.IsAllowedToApproveLeave(SessionManager.CurrentLoggedInEmployeeId,
                //    sheet.EmployeeId, PreDefindFlowType.Leave);
            }
            return false;
        }

        public static Timesheet GetTimeSheetById(int timeSheetId)
        {
            return PayrollDataContext.Timesheets.SingleOrDefault(x => x.TimesheetId == timeSheetId);
        }

      

        public static TimeSheetStatus GetTimesheetStatus(DateTime weekStartDate, int employeeId,ref string notes,ref Timesheet timesheet)
        {
            notes = "";

            timesheet = PayrollDataContext.Timesheets.FirstOrDefault(x => x.EmployeeId == employeeId 
                && x.StartDate == weekStartDate);
            
            if (timesheet == null)
                return TimeSheetStatus.NotSubmitted;

            //TimeSheetStatus status = (TimeSheetStatus)(timesheet.Status);
            //return status.ToString();

            notes = timesheet.Notes;

            return ((TimeSheetStatus)timesheet.Status);
        }

        public static DateTime GetMonthStartDate(DateTime weekDate)
        {
            return new DateTime(weekDate.Year, weekDate.Month, 1); //StartOfWeek(weekDate,DayOfWeek.Sunday);
        }

        public static List<GetAssginedEmployeeProjectListResult> GetEmployeeProjectList(int employeeId)
        {
            //int totalRows = 0;
            //List<GetProjectsResult> projectList = ProjectManager.GetProjectList(DateTime.Now, DateTime.Now,
            //    true,
            //    "", 1, 100, ref totalRows).OrderBy(x => x.Name).ToList();

            //return projectList;
           // return PayrollDataContext.GetAssginedEmployeeProjectList(employeeId).ToList();

            Setting _Setting = CalculationManager.GetSetting();

            if (_Setting.IsMonthlyTimeSheet != null && _Setting.IsMonthlyTimeSheet == true)
            {
                return
                    (
                        from p in PayrollDataContext.Projects
                        select new GetAssginedEmployeeProjectListResult
                        {
                            ProjectId = p.ProjectId,
                            Name = p.Name
                        }
                    ).ToList();

                
            }
            else
            {
                return PayrollDataContext.GetAssginedEmployeeProjectList(employeeId).ToList();
            }

        }

        public static List<GetAssginedEmployeeProjectListResult> GetEmployeeProjectListForMonth(int employeeId, DateTime monthStart, DateTime monthEnd)
        {

            Setting _Setting = CalculationManager.GetSetting();

            if (_Setting.IsMonthlyTimeSheet != null && _Setting.IsMonthlyTimeSheet == true)
            {
                return
                (
                    from x in PayrollDataContext.GetAssginedEmployeeProjectListWithDate(employeeId)
                    where
                        (x.StartDate >= monthStart && x.StartDate <= monthEnd) ||
                        (x.EndDate >= monthStart && x.EndDate <= monthEnd) ||
                        (x.StartDate <= monthStart && x.EndDate >= monthEnd)
                    select new GetAssginedEmployeeProjectListResult
                    {
                        ProjectId = x.ProjectId,
                        Name = x.Name
                    }
                ).Distinct()
                .ToList();

            }
            else
            {
                return
                (
                    from x in PayrollDataContext.GetAssginedEmployeeProjectList(employeeId)
                    where
                        (x.StartDate >= monthStart && x.StartDate <= monthEnd) ||
                        (x.EndDate >= monthStart && x.EndDate <= monthEnd) ||
                        (x.StartDate <= monthStart && x.EndDate >= monthEnd)
                    select new GetAssginedEmployeeProjectListResult
                    {
                        ProjectId = x.ProjectId,
                        Name = x.Name
                    }
                ).Distinct()
                .ToList();
            }


           
        }

        //public static List<ProjectSubProject> GetAllSubProjectList(int employeeId)
        //{
        //   return  (
        //        from s in PayrollDataContext.ProjectSubProjects
        //        join p in PayrollDataContext.Projects on s.ProjectId equals p.ProjectId
        //        join pe in PayrollDataContext.ProjectEmployees on p.ProjectId equals pe.ProjectId
        //        where pe.EmployeeId == employeeId
        //        select s
        //        ).ToList();
        //    //return PayrollDataContext.ProjectSubProjects.OrderBy(x => x.Name).ToList();
        //}
        //public static List<ProjectTask> GetAllTaskList(int employeeId)
        //{
        //    return (
        //       from t in PayrollDataContext.ProjectTasks
        //       join s in PayrollDataContext.ProjectSubProjects on t.SubProjectId equals s.SubProjectId
        //       join p in PayrollDataContext.Projects on s.ProjectId equals p.ProjectId
        //       join pe in PayrollDataContext.ProjectEmployees on p.ProjectId equals pe.ProjectId
        //       where pe.EmployeeId == employeeId
        //       select t
        //       ).ToList();
        //    //return PayrollDataContext.ProjectTasks.OrderBy(x => x.Name).ToList();
        //}
        public static List<TimesheetGridNewBO> GetProjectList(DateTime monthStartDate, int employeeId)
        {

            DateTime monthEndDate = new DateTime(monthStartDate.Year, monthStartDate.Month,
                DateTime.DaysInMonth(monthStartDate.Year, monthStartDate.Month));


            List<TimesheetGridNewBO> list = new List<TimesheetGridNewBO>();
                  double total = 0;



                  List<GetAssginedEmployeeProjectListResult> projectList = GetEmployeeProjectList(employeeId).OrderBy(x => x.Name).ToList();

            List<TimesheetProject> timesheetProjectList = PayrollDataContext.TimesheetProjects.Where(
                    x => x.EmployeeId == employeeId && x.StartDate >= monthStartDate && x.StartDate <= monthEndDate)
                    .OrderBy(x => x.StartDate).ToList();


            if (timesheetProjectList.Count<=0)
            {
             
                foreach (var pro in projectList)
                {
                    list.Add(new TimesheetGridNewBO { Id = pro.ProjectId, Name = pro.Name, Code = pro.Code, TotalDays = 0 });
                }
                
            }
            else
            {
                

                foreach (var pro in projectList)
                {

                    // iterate project timesheet from db or else set blank row
                    List<TimesheetProject> filtertimesheetProjectList =
                        timesheetProjectList.Where(x => x.ProjectId == pro.ProjectId).ToList();

                 
                    
                    if (filtertimesheetProjectList.Count > 0 )
                    {
                        TimesheetGridNewBO timesheetProjectRow = new TimesheetGridNewBO { Id = pro.ProjectId, Name = pro.Name, Code = pro.Code, };
                        timesheetProjectRow.TotalDays = 0;

                        DateTime start = monthStartDate;

                        // Current Month
                        total = 0;
                        for (int i = 1; i <= TOTAL_WEEKS; i++)
                        {
                            PropertyInfo dayProperty = timesheetProjectRow.GetType().GetProperty("D" + i);
                            PropertyInfo dayComment = timesheetProjectRow.GetType().GetProperty("D" + i + "Comment");
                            PropertyInfo dayIsFixed = timesheetProjectRow.GetType().GetProperty("D" + i + "IsFixed");

                            if (dayProperty != null)
                            {
                                object value = null;
                                TimesheetProject valueEntity = filtertimesheetProjectList.FirstOrDefault(x => x.StartDate == start);
                                if (valueEntity != null)
                                {
                                    value = valueEntity.Hours;
                                    total += valueEntity.Hours.Value;

                                    // set Comments
                                    if (dayComment != null)
                                        dayComment.SetValue(timesheetProjectRow, valueEntity.Notes, null);

                                  
                                }
                                dayProperty.SetValue(timesheetProjectRow, value, null);

                            }

                            start = start.AddDays(1);
                        }
                        timesheetProjectRow.Total = total;

                        
                        list.Add(timesheetProjectRow);
                    }
                    else
                    {
                        list.Add(new TimesheetGridNewBO { Id = pro.ProjectId, Name = pro.Name, Code = pro.Code, Total = 0, TotalDays = 0 });
                    }
                }

            }
            return list;
        }

        public static void AutoFillHourUsingDefaultPercentage(List<TimesheetGridNewBO> projectList, int empId
            , DateTime monthStartDate, DateTime monthEndDate)
        {
            const double Default_Weekly_Hours = 40;

            // Calculate Leave/Public holiday total for each week
            List<TimesheetGridNewBO> leaveList = NewTimeSheetManager.GetLeaveList(monthStartDate, empId);
            foreach (TimesheetGridNewBO item in leaveList)
            {
                item.L1 = item.D1;
                item.L2 = item.D2;
                item.L3 = item.D3;
                item.L4 = item.D4;
                item.L5 = item.D5;
            }
            TimesheetGridNewBO leavetotal = new TimesheetGridNewBO();
            TimesheetGridNewBO projectTotal = new TimesheetGridNewBO();

            leavetotal.D1 = leaveList.Sum(x => x.L1 == null ? 0 : x.L1.Value);
            leavetotal.D2 = leaveList.Sum(x => x.L2 == null ? 0 : x.L2.Value);
            leavetotal.D3 = leaveList.Sum(x => x.L3 == null ? 0 : x.L3.Value);
            leavetotal.D4 = leaveList.Sum(x => x.L4 == null ? 0 : x.L4.Value);
            leavetotal.D5 = leaveList.Sum(x => x.L5 == null ? 0 : x.L5.Value);

            // now calculate default Project hours
            List<ProjectEmployeeContribution> list =
                PayrollDataContext.ProjectEmployeeContributions.Where(x => x.EmployeeId == empId).ToList();


            foreach (TimesheetGridNewBO item in projectList)
            {
                ProjectEmployeeContribution cont = list.FirstOrDefault(x => x.ProjectId == item.Id);
                if (cont != null && cont.Rate != null)
                {
                    item.ProjectPercent = cont.Rate.Value;
                }
            }

            projectTotal.D1 = Default_Weekly_Hours - leavetotal.D1;
            projectTotal.D2 = Default_Weekly_Hours - leavetotal.D2;
            projectTotal.D3 = Default_Weekly_Hours - leavetotal.D3;
            projectTotal.D4 = Default_Weekly_Hours - leavetotal.D4;

            int lastDays = 0;
            // for last week days or hours will vary
            for (int i = 29; i <= DateTime.DaysInMonth(monthStartDate.Year, monthStartDate.Month); i++)
            {
                DateTime day = new DateTime(monthStartDate.Year,monthStartDate.Month, i);
                if (day.DayOfWeek != DayOfWeek.Saturday && day.DayOfWeek != DayOfWeek.Sunday)
                {
                    lastDays += 1;
                }
            }
            projectTotal.D5 = (lastDays* 8) - leavetotal.D5;


            foreach (TimesheetGridNewBO item in projectList)
            {
                if (item.ProjectPercent != 0)
                {
                    item.D1 = double.Parse(((projectTotal.D1 * item.ProjectPercent) / 100).Value.ToString("N0"));
                    item.D2 = double.Parse(((projectTotal.D2 * item.ProjectPercent) / 100).Value.ToString("N0"));
                    item.D3 = double.Parse(((projectTotal.D3 * item.ProjectPercent) / 100).Value.ToString("N0"));
                    item.D4 = double.Parse(((projectTotal.D4 * item.ProjectPercent) / 100).Value.ToString("N0"));
                    item.D5 = double.Parse(((projectTotal.D5 * item.ProjectPercent) / 100).Value.ToString("N0"));
                }


            }

        }

        public static List<TimesheetGridNewBO> GetRepeatedProjectList(DateTime monthStartDate, int employeeId)
        {

            monthStartDate = GetMonthStartDate(monthStartDate);
            DateTime monthEndDate = new DateTime(monthStartDate.Year, monthStartDate.Month, 
                DateTime.DaysInMonth(monthStartDate.Year,monthStartDate.Month));

            List<TimesheetGridNewBO> list = new List<TimesheetGridNewBO>();


            Timesheet dbTimesheet = PayrollDataContext.Timesheets.FirstOrDefault(
                x => x.EmployeeId == employeeId && x.StartDate == monthStartDate);



            List<TimesheetProject> timesheetProjectList = new List<TimesheetProject>();

            if (dbTimesheet != null)
            {
                timesheetProjectList = dbTimesheet.TimesheetProjects.Where(X=>X.WeekNumber>0).ToList();
            }

            // if time sheet not saved then load Project list for the Employee
            if (timesheetProjectList.Count <= 0)
            {

                List<GetAssginedEmployeeProjectListResult> projectList = GetEmployeeProjectListForMonth(employeeId,monthStartDate,monthEndDate)
                    .OrderBy(x => x.Name).ToList();


                string Category = PayrollDataContext.EHumanResources.FirstOrDefault(x => x.EmployeeId == employeeId).ProjectCategory;
                

                foreach (var pro in projectList)
                {
                    ProjectEmployeeActivity entity  = PayrollDataContext.ProjectEmployeeActivities.FirstOrDefault(x => x.EmployeeId == employeeId && x.ProjectId == pro.ProjectId);

                    double? Activity = null;

                    if (entity != null)
                    {
                        Activity = entity.Activity;
                    }

                    long act = 0;

                    if (Activity != null)
                    {
                        Activity = long.Parse(Activity.Value.ToString());
                        act = (long)Activity;
                    }
                    list.Add(new TimesheetGridNewBO { Id = pro.ProjectId, Activity = act, Category = Category, Name = pro.Name, Code = pro.Code, TotalDays = 0 });
                }

                AutoFillHourUsingDefaultPercentage(list, employeeId, monthStartDate, monthEndDate);

            }
            else
            {

                bool newProAdded = false;
                // recheck if any project has been added or delete for this month
                if (dbTimesheet != null && dbTimesheet.Status == (int)TimeSheetStatus.Draft)
                {
                    
                    List<GetAssginedEmployeeProjectListResult> projectList = GetEmployeeProjectListForMonth(employeeId, monthStartDate, monthEndDate)
                        .OrderBy(x => x.Name).ToList();

                    foreach (GetAssginedEmployeeProjectListResult pro in projectList)
                    {
                        if (timesheetProjectList.Any(x => x.ProjectId == pro.ProjectId))
                        {
                            TimesheetProject addedPro = timesheetProjectList.FirstOrDefault(x => x.ProjectId == pro.ProjectId);

                            ProjectEmployeeActivity entity = PayrollDataContext.ProjectEmployeeActivities.FirstOrDefault(x => x.EmployeeId == employeeId && x.ProjectId == pro.ProjectId);
                            TimesheetProject objTimesheetProject=null;

                            if (dbTimesheet != null)
                                objTimesheetProject = PayrollDataContext.TimesheetProjects.FirstOrDefault(x => x.TimesheetId == dbTimesheet.TimesheetId && x.ProjectId == pro.ProjectId);


                            double? Activity = null;

                            if (entity != null)
                            {
                                Activity = entity.Activity;
                            }

                            long act = 0;

                            if (objTimesheetProject == null)
                            {
                                if (Activity != null)
                                {
                                    Activity = long.Parse(Activity.Value.ToString());
                                    act = (long)Activity;
                                }
                            }
                            else
                            {
                                if (objTimesheetProject.Activity != null)
                                    act = (long)objTimesheetProject.Activity;

                            }

                            addedPro.Activity = act;
                        }
                        else
                        {
                            string cat = PayrollDataContext.EHumanResources.FirstOrDefault(x => x.EmployeeId == employeeId).ProjectCategory;
                            ProjectEmployeeActivity entity = PayrollDataContext.ProjectEmployeeActivities.FirstOrDefault(x => x.EmployeeId == employeeId && x.ProjectId == pro.ProjectId);

                            double? Activity = null;

                            if (entity != null)
                            {
                                Activity = entity.Activity;
                            }

                            long act = 0;

                            if (Activity != null)
                            {
                                Activity = long.Parse(Activity.Value.ToString());
                                act = (long)Activity;
                            }
                            timesheetProjectList.Add(new TimesheetProject { ProjectId = pro.ProjectId, Activity = act, Category = cat });

                            newProAdded = true;
                        }
                    }
                }

                foreach (TimesheetProject item in timesheetProjectList)
                {
                    TimesheetGridNewBO projectRow = null;
                    // Check if the Project with Day Number is blank then get the Project row
                    foreach (TimesheetGridNewBO project in list.Where(x => x.Id == item.ProjectId))
                    {
                        PropertyInfo dayValueProperty = project.GetType().GetProperty("D" + item.WeekNumber);
                        object objValue = dayValueProperty.GetValue(project, null);
                        double? value = null;
                        if (objValue != null)
                            value = (double)objValue;

                        if (value == null || value == 0)
                        {
                            projectRow = project;

                            break;
                        }
                    }

                    
                    // Create new project row
                    if (projectRow == null)
                    {
                        //Project pro = ProjectManager.GetProjectById(item.ProjectId.Value);

                        TimesheetProject dbTimeSheetProject = PayrollDataContext.TimesheetProjects
                       .Where(x => x.TimesheetId == dbTimesheet.TimesheetId && x.EmployeeId == employeeId && x.ProjectId == item.ProjectId).ToList().FirstOrDefault();


                        long? Activity = 0;
                        string Category = string.Empty;
                        if (dbTimeSheetProject != null)
                        {
                            Activity = (dbTimeSheetProject.Activity == null) ? 0 : dbTimeSheetProject.Activity;
                            Category = dbTimeSheetProject.Category;
                        }
                            // if project not saved then assign these field values
                        else
                        {
                            Activity = item.Activity == null ? (long)0 : (long)item.Activity;
                            Category = item.Category;
                        }

                        projectRow = new TimesheetGridNewBO
                        {
                            Id = item.ProjectId.Value,
                            Name = ProjectManager.GetProjectById(item.ProjectId.Value).Name,
                            Activity = Activity.Value,
                            Category = Category
                        };
                        list.Add(projectRow);
                    }

                    PropertyInfo dayProperty = projectRow.GetType().GetProperty("D" + item.WeekNumber);
                    PropertyInfo dayComment = projectRow.GetType().GetProperty("D" + item.WeekNumber + "Comment");
                    PropertyInfo dayIsFixed = projectRow.GetType().GetProperty("D" + item.WeekNumber + "IsFixed");

                    if (dayProperty != null)
                    {
                        dayProperty.SetValue(projectRow, item.Hours, null);
                    }
                    if (dayComment != null)
                    {
                        dayComment.SetValue(projectRow, item.Notes, null);
                    }
                   
                    
                }


                if (newProAdded)
                {
                    AutoFillHourUsingDefaultPercentage(list, employeeId, monthStartDate, monthEndDate);
                }

            }

            //set total
            foreach (TimesheetGridNewBO item in list)
            {
                item.Total =
                    (item.D1 == null ? 0 : item.D1.Value) + (item.D2 == null ? 0 : item.D2.Value) + (item.D3 == null ? 0 : item.D3.Value) +
                     (item.D4 == null ? 0 : item.D4.Value) + (item.D5 == null ? 0 : item.D5.Value) ;
                // now calculate default Project hours
                List<ProjectEmployeeContribution> ProjectEmployeeContributionlist =
                    PayrollDataContext.ProjectEmployeeContributions.Where(x => x.EmployeeId == employeeId).ToList();
                ProjectEmployeeContribution cont = ProjectEmployeeContributionlist.FirstOrDefault(x => x.ProjectId == item.Id);
                if (cont != null && cont.Rate != null)
                {
                    item.ProjectPercent = cont.Rate.Value;
                }

                
            }

            return list;
        }

        public static string GetManagerEmailForTimesheetRequestForMonthlyPSI(int employeeId)
        {
            return PayrollDataContext.EAddresses.FirstOrDefault(x => x.EmployeeId == employeeId).CIEmail;

        }

        public static DateTime GetStartDate(DateTime date)
        {
            return new DateTime(date.Year, date.Month, date.Day, 0, 0, 0);
        }

        public static DateTime GetEndDate(DateTime date)
        {
            return new DateTime(date.Year, date.Month, date.Day, 23, 59, 59);
        }


        public static List<TimesheetGridMonthWiseNewBO> GetLeaveListMonthWise(DateTime weekStartDate, int employeeId, bool IsmonthWise)
        {

            DateTime StartingDateofMonth = new DateTime(weekStartDate.Year, weekStartDate.Month, 1);
            weekStartDate = StartingDateofMonth;
            DateTime weekEndDate = StartingDateofMonth.AddMonths(1).AddDays(-1);


            List<TimesheetGridMonthWiseNewBO> list = new List<TimesheetGridMonthWiseNewBO>();

            string rejectionNotes = "";
            Timesheet dbTimesheet = null;
            TimeSheetStatus status = NewTimeSheetManager.GetTimesheetStatus(weekStartDate, employeeId, ref rejectionNotes, ref dbTimesheet);

            DateTime? retirementDate = null;
            EEmployee emp  = EmployeeManager.GetEmployeeById(employeeId);
            if (emp != null && emp.IsRetired != null && emp.IsRetired.Value)
            {
                retirementDate = emp.EHumanResources[0].DateOfRetirementEng;
                if (retirementDate != null)
                    retirementDate = retirementDate.Value.Date;
            }


            List<EmployeeLeaveListResult> leaveList = new LeaveAttendanceManager().GetEmployeeLeaveList(employeeId).ToList();

            List<TimesheetGridMonthWiseNewBO> projectList = GetProjectListMonthWise(weekStartDate, employeeId);


            // Add Holiday title also
            leaveList.Add(new EmployeeLeaveListResult { LeaveTypeId = 0, Title = "Unpaid Leave" });
            leaveList.Add(new EmployeeLeaveListResult { LeaveTypeId = -2, Title = "Holiday" });

            //bool isSaved = false;// LeaveAttendanceManager.IsAttendanceSavedForEmployee(payrollPeriodId, employeeId); //dbTimesheet == null ? false : true;
            double total = 0;

            //if (isSaved == false)
            //{
            // get approved leave
            List<LeaveDetail> approvedLeaves =
                (
                from ld in PayrollDataContext.LeaveDetails
                join lr in PayrollDataContext.LeaveRequests on ld.LeaveRequestId equals lr.LeaveRequestId
                where lr.EmployeeId == employeeId && lr.Status == 2 //only approved
                    && ld.DateOnEng >= weekStartDate && ld.DateOnEng <= weekEndDate
                select ld
                ).ToList();

            for (int i = approvedLeaves.Count - 1; i >= 0; i--)
            {
                LeaveDetail leave = approvedLeaves[i];
                if (leave.LeaveRequest.LeaveTypeId != 0)
                {
                    LLeaveType l = new LeaveAttendanceManager().GetLeaveType(leave.LeaveRequest.LeaveTypeId);

                    // for compensator leave, it should be leave deducted case
                    if (l.FreqOfAccrual == LeaveAccrue.COMPENSATORY
                           && (leave.IsHolidayForCompensatoryType != null && leave.IsHolidayForCompensatoryType.Value)
                        )
                    {
                        approvedLeaves.RemoveAt(i);
                    }
                }
            }

            //List<LeaveDetail> prevApprovedLeaves = new List<LeaveDetail>();
            // mon-fri only
            //List<GetHolidaysForAttendenceResult> holidayList = new HolidayManager()
            //    .GetMonthsHolidaysForAttendence(0, 0, SessionManager.CurrentCompanyId, 0, weekStartDate.AddDays(1), weekEndDate.AddDays(-1), employeeId).ToList();


            List<GetHolidaysForAttendenceResult> holidayList = new HolidayManager()
             .GetMonthsHolidaysForAttendence(0, 0, SessionManager.CurrentCompanyId, 0, weekStartDate, weekEndDate, employeeId).ToList();


            // remove holiday if leave already exists for that day
            for (int i = holidayList.Count - 1; i >= 0; i--)
            {
                if (holidayList[i].DateEng != null && approvedLeaves.Any(x => x.DateOnEng == holidayList[i].DateEng.Value))
                {
                    holidayList.RemoveAt(i);
                }
            }

            // remove those holiday that is set before the join date
            ECurrentStatus firstStatus = EmployeeManager.GetFirstStatus(employeeId);
            if (firstStatus != null && firstStatus.FromDateEng >= weekStartDate)
            {
                // remove holiday if leave already exists for that day
                for (int i = holidayList.Count - 1; i >= 0; i--)
                {
                    if (holidayList[i].DateEng != null && holidayList[i].DateEng < firstStatus.FromDateEng)
                    {
                        holidayList.RemoveAt(i);
                    }
                }
            }

            // remove those holiday that is set after retirement date
            if (retirementDate != null && retirementDate.Value >= weekStartDate && retirementDate.Value <= weekEndDate)
            {
                // remove holiday if leave already exists for that day
                for (int i = holidayList.Count - 1; i >= 0; i--)
                {
                    if (holidayList[i].DateEng != null && holidayList[i].DateEng.Value.Date > retirementDate.Value.Date)
                    {
                        holidayList.RemoveAt(i);
                    }
                }
            }


            foreach (var item in leaveList)
            {
                // iterate project timesheet from db or else set blank row
                List<LeaveDetail> filtertimesheetLeaveList = approvedLeaves.Where(x => x.LeaveRequest.LeaveTypeId == item.LeaveTypeId).ToList();

                // for Holiday
                if (item.LeaveTypeId == -2)
                {
                    filtertimesheetLeaveList = new List<LeaveDetail>();


                    foreach (GetHolidaysForAttendenceResult holiday in holidayList)
                    {
                        LeaveDetail newHolidayDetail = new LeaveDetail();
                        newHolidayDetail.DateOnEng = holiday.DateEng.Value;
                        newHolidayDetail.LeaveRequest = new LeaveRequest();
                        newHolidayDetail.LeaveRequest.IsHalfDayLeave = false; // as no half day leave for PSI in holiday list
                        newHolidayDetail.IsHoliday = true;

                        if (holiday.IsWeekly != null && holiday.IsWeekly.Value)
                        {
                            newHolidayDetail.IsWeeklyHoliday = true;
                        }


                        filtertimesheetLeaveList.Add(newHolidayDetail);
                    }

                }


                if (filtertimesheetLeaveList.Count > 0)
                {
                    TimesheetGridMonthWiseNewBO timesheetLeaveRow = new TimesheetGridMonthWiseNewBO { Id = item.LeaveTypeId, Name = item.Title, Code = "" };
                    timesheetLeaveRow.TotalDays = 0;// payrollPeriod.TotalDays.Value;

                    SetLeaveDaysMonthWise(projectList, filtertimesheetLeaveList, timesheetLeaveRow, weekStartDate);


                    list.Add(timesheetLeaveRow);
                }
                else
                {
                    list.Add(new TimesheetGridMonthWiseNewBO { Id = item.LeaveTypeId, Name = item.Title, TotalDays = 0 });
                }
            }

            return list;
        }
     

        public static List<TimesheetGridMonthWiseNewBO> GetProjectListMonthWise(DateTime weekStartDate, int employeeId)
        {

            DateTime StartingDateofMonth = new DateTime(weekStartDate.Year, weekStartDate.Month, 1);
            weekStartDate = StartingDateofMonth;
            DateTime weekEndDate = StartingDateofMonth.AddMonths(1).AddDays(-1);

            List<TimesheetGridMonthWiseNewBO> list = new List<TimesheetGridMonthWiseNewBO>();
            double total = 0;

            List<GetAssginedEmployeeProjectListResult> projectList = GetEmployeeProjectList(employeeId).OrderBy(x => x.Name).ToList();

            List<TimesheetProject> timesheetProjectList = PayrollDataContext.TimesheetProjects.Where(
                    x => x.EmployeeId == employeeId && x.DateEng >= weekStartDate && x.DateEng <= weekEndDate)
                    .OrderBy(x => x.DateEng).ToList();


            if (timesheetProjectList.Count <= 0)
            {

                foreach (var pro in projectList)
                {
                    list.Add(new TimesheetGridMonthWiseNewBO { Id = pro.ProjectId, Name = pro.Name, Code = pro.Code, TotalDays = 0 });
                }

            }
            else
            {


                foreach (var pro in projectList)
                {

                    // iterate project timesheet from db or else set blank row
                    List<TimesheetProject> filtertimesheetProjectList =
                        timesheetProjectList.Where(x => x.ProjectId == pro.ProjectId).ToList();



                    if (filtertimesheetProjectList.Count > 0)
                    {
                        TimesheetGridMonthWiseNewBO timesheetProjectRow = new TimesheetGridMonthWiseNewBO { Id = pro.ProjectId, Name = pro.Name, Code = pro.Code, };
                        timesheetProjectRow.TotalDays = 0;

                        DateTime start = weekStartDate;

                        // Current Month
                        total = 0;
                        for (int i = 1; i <= 32; i++)
                        {
                            PropertyInfo dayProperty = timesheetProjectRow.GetType().GetProperty("D" + i);
                            PropertyInfo dayComment = timesheetProjectRow.GetType().GetProperty("D" + i + "Comment");
                            PropertyInfo dayIsFixed = timesheetProjectRow.GetType().GetProperty("D" + i + "IsFixed");

                            if (dayProperty != null)
                            {
                                object value = null;
                                TimesheetProject valueEntity = filtertimesheetProjectList.FirstOrDefault(x => x.DateEng == start);
                                if (valueEntity != null)
                                {
                                    value = valueEntity.Hours;
                                    total += valueEntity.Hours.Value;

                                    // set Comments
                                    if (dayComment != null)
                                        dayComment.SetValue(timesheetProjectRow, valueEntity.Notes, null);

                                    if (dayIsFixed != null)
                                        dayIsFixed.SetValue(timesheetProjectRow, valueEntity.IsFixed, null);
                                }
                                dayProperty.SetValue(timesheetProjectRow, value, null);

                            }

                            start = start.AddDays(1);
                        }
                        timesheetProjectRow.Total = total;


                        list.Add(timesheetProjectRow);
                    }
                    else
                    {
                        list.Add(new TimesheetGridMonthWiseNewBO { Id = pro.ProjectId, Name = pro.Name, Code = pro.Code, Total = 0, TotalDays = 0 });
                    }
                }

            }
            return list;
        }
        private static double SetLeaveDaysMonthWise(List<TimesheetGridMonthWiseNewBO> projectList,
        List<LeaveDetail> filtertimesheetLeaveList, TimesheetGridMonthWiseNewBO timesheetLeaveRow, DateTime weekStartDate)
        {
            string dayStartWith = "D";
            string holidayStartWith = "D{0}IsHoliday";

            weekStartDate = weekStartDate.AddDays(-1);

            double total = 0; object value = null;

            for (int i = 1; i <= 32; i++)
            {
                weekStartDate = weekStartDate.AddDays(1);

                LeaveDetail valueEntity = filtertimesheetLeaveList.FirstOrDefault(x => x.DateOnEng == weekStartDate);

                if (valueEntity != null && valueEntity.IsWeeklyHoliday)
                {
                    //valueen
                    continue;
                }

                PropertyInfo dayProperty = timesheetLeaveRow.GetType().GetProperty(dayStartWith + i);
                if (dayProperty != null)
                {

                    value = null;
                    if (valueEntity != null)
                    {
                        PropertyInfo holidayProperty = timesheetLeaveRow.GetType().GetProperty(string.Format(holidayStartWith, i));
                        if (holidayProperty != null)
                            holidayProperty.SetValue(timesheetLeaveRow, valueEntity.IsHoliday, null);
                        //timesheetLeaveRow.IsHoliday = valueEntity.IsHoliday;

                        value = valueEntity.LeaveRequest.IsHalfDayLeave.Value ? 4 : 8; // for half days = 4, for full days = 8

                        if (valueEntity.IsHoliday)
                        {
                            //for holiday also there could be overtime so to display holiday value we must check if there is value in proejct
                            // on that day & if it has then we should deduct that day in holiday display
                            //first find if there is value in project on this day
                            //double projectTotalHours = GetProjectHourOnThisDayMonthWise(i, projectList);
                            value = (object)(Convert.ToDouble(value));
                        }
                        total += (double)Convert.ToDecimal(value);
                    }
                    if (Convert.ToDouble(value) != 0)
                        dayProperty.SetValue(timesheetLeaveRow, Convert.ToDouble(value), null);

                }



            }

            timesheetLeaveRow.Total = total;



            return total;
        }

        // This presumes that weeks start with Monday.
        // Week 1 is the 1st week of the year with a Thursday in it.
        public static int GetIso8601WeekOfYear(DateTime time)
        {
            // Seriously cheat.  If its Monday, Tuesday or Wednesday, then it'll 
            // be the same week# as whatever Thursday, Friday or Saturday are,
            // and we always get those right
            DayOfWeek day = CultureInfo.InvariantCulture.Calendar.GetDayOfWeek(time);
            if (day >= DayOfWeek.Monday && day <= DayOfWeek.Wednesday)
            {
                time = time.AddDays(3);
            }

            // Return the week of our adjusted day
            return CultureInfo.InvariantCulture.Calendar.GetWeekOfYear(time, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
        }

        public static List<DateTime> getSundays(int intMonth, int intYear)
        {
            List<DateTime> lstSundays = new List<DateTime>();
            int intDaysThisMonth = DateTime.DaysInMonth(intYear, intMonth);
            DateTime oBeginnngOfThisMonth = new DateTime(intYear, intMonth, 1);
            //oBeginnngOfThisMonth=oBeginnngOfThisMonth.AddDays(-1);// need to show if first day of the display calendar lies in prev month also

            lstSundays.Add(NewTimeSheetManager.GetMonthStartDate(oBeginnngOfThisMonth));

            for (int i = 1; i <= 5; i++)
            {
                oBeginnngOfThisMonth = oBeginnngOfThisMonth.AddDays(6);
                lstSundays.Add(NewTimeSheetManager.GetMonthStartDate(oBeginnngOfThisMonth));
            }
            return lstSundays;
        }


        //public static TimesheetGridNewBO GetTimecard(int employeeId, DateTime weekStartDate)
        //{
        //    TimesheetGridNewBO time = new TimesheetGridNewBO();

        //    weekStartDate = GetStartDate(weekStartDate);
        //    DateTime weekEndDate = GetEndDate( weekStartDate.AddDays(6));

        //      DAL.AttendanceMapping map = BLL.BaseBiz.PayrollDataContext.AttendanceMappings
        //            .SingleOrDefault(x => x.EmployeeId == employeeId);

        //    if (map == null)
        //    {
        //        return time;
        //    }

        //    List<AttendanceCheckInCheckOutView> list =
        //        BLL.BaseBiz.PayrollDataContext.AttendanceCheckInCheckOutViews
        //        .Where(x => x.DeviceID == map.DeviceId && x.DateTime >= weekStartDate && x.DateTime <= weekEndDate).ToList();

        //    weekStartDate = weekStartDate.AddDays(-1);
        //    double? value = null;
        //    double totalMinutes = 0;
        //    for (int i = 1; i <= 7; i++)
        //    {
        //        weekStartDate = weekStartDate.AddDays(1);

        //        AttendanceCheckInCheckOutView intime = list.OrderBy(x=>x.DateTime)
        //            .FirstOrDefault(x => x.DeviceID == map.DeviceId && x.InOutMode == (byte)AttendanceInOutMode.In
        //            && x.DateTime.Value.Year == weekStartDate.Year
        //            && x.DateTime.Value.Month == weekStartDate.Month
        //            && x.DateTime.Value.Day == weekStartDate.Day);

        //        AttendanceCheckInCheckOutView outTime = list.OrderByDescending(x => x.DateTime)
        //           .FirstOrDefault(x => x.DeviceID == map.DeviceId && x.InOutMode == (byte)AttendanceInOutMode.Out
        //           && x.DateTime.Value.Year == weekStartDate.Year
        //           && x.DateTime.Value.Month == weekStartDate.Month
        //           && x.DateTime.Value.Day == weekStartDate.Day);


        //        if (intime == null && outTime == null)
        //        {
        //            value = null;
        //        }
        //        else if (intime == null || outTime == null)
        //        {
        //            value = 0;
        //        }
        //        else
        //        {
        //            value =
        //                double.Parse((outTime.DateTime.Value - intime.DateTime.Value).TotalMinutes.ToString("N2"));
        //        }

        //        PropertyInfo dayProperty = time.GetType().GetProperty("D" + i);

        //        if (dayProperty != null)
        //        {
        //            dayProperty.SetValue(time, value, null);
        //        }

        //        totalMinutes += (value == null ? 0 : value.Value);
        //    }

        //    time.Name = "Timecard";
        //    time.Total = totalMinutes;

        //    return time;
        //}

        public static List<TimesheetGridNewBO> GetLeaveList(DateTime monthStartDate, int employeeId)
        {
            monthStartDate = GetMonthStartDate(monthStartDate);

            DateTime monthEndDate = new DateTime(monthStartDate.Year, monthStartDate.Month,
                 DateTime.DaysInMonth(monthStartDate.Year, monthStartDate.Month));


            List<TimesheetGridNewBO> list = new List<TimesheetGridNewBO>();



            double total = 0;

            //if (isSaved == false)
            //{
            // get approved leave
            List<LeaveDetail> approvedLeaves =
                (
                from ld in PayrollDataContext.LeaveDetails
                join lr in PayrollDataContext.LeaveRequests on ld.LeaveRequestId equals lr.LeaveRequestId
                where lr.EmployeeId == employeeId && lr.Status == 2 //only approved
                    && ld.DateOnEng >= monthStartDate && ld.DateOnEng <= monthEndDate
                select ld
                ).ToList();

            //List<LeaveDetail> prevApprovedLeaves = new List<LeaveDetail>();

            #region "Append Holiday"

            EEmployee emp = new EmployeeManager().GetById(employeeId);

            List<GetHolidaysForAttendenceResult> holidayList = new HolidayManager()
                      .GetMonthsHolidaysForAttendence(0, 0, SessionManager.CurrentCompanyId, 0, monthStartDate, monthEndDate,employeeId).ToList();

            foreach (GetHolidaysForAttendenceResult holiday in holidayList)
            {
                LeaveDetail newHolidayDetail = new LeaveDetail();
                if (holiday.IsWeekly != null && holiday.IsWeekly.Value)
                {
                    continue;
                    //newHolidayDetail.IsWeeklyHoliday = true;
                }

                // if holiday on sat or sunday
                if (holiday.DateEng.Value.DayOfWeek == DayOfWeek.Saturday || holiday.DateEng.Value.DayOfWeek == DayOfWeek.Sunday)
                    continue;

                if (holiday.DateEng != null && !string.IsNullOrEmpty(holiday.BranchIDList))
                {
                    int branchId = BranchManager.GetEmployeeCurrentBranch(employeeId, holiday.DateEng.Value);
                    //if (branchId != branchHoliday.BranchId)
                    if (!holiday.BranchList.Contains(branchId))
                        continue;
                }

                // remove female holiday for male-

                
                // if gender not femaly then remove femaly holidays from list
                if (emp.Gender != 0 && holiday.Type == 0 && (holiday.IsWeekly == null || holiday.IsWeekly == false))
                {
                    continue;
                }


                newHolidayDetail.DateOnEng = holiday.DateEng.Value;
                newHolidayDetail.LeaveRequest = new LeaveRequest();
                newHolidayDetail.LeaveRequest.IsHalfDayLeave = false; // as no half day leave for PSI in holiday list
                newHolidayDetail.IsHoliday = true;
                newHolidayDetail.LeaveTypeId = -2;
                newHolidayDetail.LeaveRequest.LeaveTypeId = -2;


                approvedLeaves.Add(newHolidayDetail);
            }
            #endregion



            foreach (LeaveDetail leave in approvedLeaves)
            {
             
                TimesheetGridNewBO timesheetLeaveRow = new TimesheetGridNewBO
                {
                    Id = leave.LeaveTypeId == null ?
                        leave.LeaveRequest.LeaveTypeId : leave.LeaveTypeId,
                    Code = ""
                };

                if (timesheetLeaveRow.Id == -2)
                    timesheetLeaveRow.Name = "Public Holiday";
                else if (timesheetLeaveRow.Id == 0)
                    timesheetLeaveRow.Name = "Unpaid Leave";
                else if(timesheetLeaveRow.Id != null)
                    timesheetLeaveRow.Name = LeaveAttendanceManager.GetLeaveTypeById(timesheetLeaveRow.Id.Value).Title;

                timesheetLeaveRow.TotalDays = 0;// payrollPeriod.TotalDays.Value;

                SetLeaveDays(leave, timesheetLeaveRow, monthStartDate);


                list.Add(timesheetLeaveRow);
            }

          

            // Combine same leave type for same week
            List<TimesheetGridNewBO> finalList = new List<TimesheetGridNewBO>();
            foreach (TimesheetGridNewBO item in list)
            {
                TimesheetGridNewBO leave = null;

                if (item.D1 != null && item.D1 != 0)
                {
                    leave = finalList.FirstOrDefault(x => x.Id == item.Id && x.D1 != null && x.D1 != 0);
                    if (leave != null)
                        leave.D1 = item.D1.Value + leave.D1.Value;
                }
                if (item.D2 != null && item.D2 != 0)
                {
                    leave = finalList.FirstOrDefault(x => x.Id == item.Id && x.D2 != null && x.D2 != 0);
                    if (leave != null)
                        leave.D2 = item.D2.Value + leave.D2.Value;
                }
                if (item.D3 != null && item.D3 != 0)
                {
                    leave = finalList.FirstOrDefault(x => x.Id == item.Id && x.D3 != null && x.D3 != 0);
                    if (leave != null)
                        leave.D3 = item.D3.Value + leave.D3.Value;
                }
                if (item.D4 != null && item.D4 != 0)
                {
                    leave = finalList.FirstOrDefault(x => x.Id == item.Id && x.D4 != null && x.D4 != 0);
                    if (leave != null)
                        leave.D4 = item.D4.Value + leave.D4.Value;
                }
                if (item.D5 != null && item.D5 != 0)
                {
                    leave = finalList.FirstOrDefault(x => x.Id == item.Id && x.D5 != null && x.D5 != 0);
                    if (leave != null)
                        leave.D5 = item.D5.Value + leave.D5.Value;
                }

                if (leave == null)
                {
                    finalList.Add(new TimesheetGridNewBO { Id = item.Id, Name = item.Name, D1 = item.D1, D2 = item.D2, D3 = item.D3, D4 = item.D4, D5 = item.D5, Total = 0 });
                }
            }

            // set total
            foreach (TimesheetGridNewBO item in finalList)
            {
                item.Total = Convert.ToDouble(item.D1) + Convert.ToDouble(item.D2) + Convert.ToDouble(item.D3) +
                            Convert.ToDouble(item.D4) + Convert.ToDouble(item.D5);
            }



            //foreach (var item in leaveList)
            //{
            //    // iterate project timesheet from db or else set blank row
            //    List<LeaveDetail> filtertimesheetLeaveList = approvedLeaves.Where(x => x.LeaveRequest.LeaveTypeId == item.LeaveTypeId).ToList();

            //    // for public Holiday
            //    #region "Public Holiday"
            //    if (item.LeaveTypeId == -2)
            //    {
            //        filtertimesheetLeaveList = new List<LeaveDetail>();
                  
      

            //        // Prev month holiday list
            //        //holidayList = new HolidayManager().GetMonthsHolidaysForAttendence(0, 0, SessionManager.CurrentCompanyId, 0, monthStartDate, monthEndDate).ToList();

            //        //foreach (GetHolidaysForAttendenceResult holiday in holidayList)
            //        //{
            //        //    LeaveDetail newHolidayDetail = new LeaveDetail();
            //        //    newHolidayDetail.IsPrevMonth = true;
            //        //    newHolidayDetail.DateOnEng = holiday.DateEng.Value;
            //        //    newHolidayDetail.LeaveRequest = new LeaveRequest();
            //        //    newHolidayDetail.LeaveRequest.IsHalfDayLeave = false; // as no half day leave for PSI in holiday list
            //        //    newHolidayDetail.IsHoliday = true;
            //        //    if (holiday.IsWeekly != null && holiday.IsWeekly.Value)
            //        //    {
            //        //        newHolidayDetail.IsWeeklyHoliday = true;
            //        //    }
            //        //}
            //    }

            //    #endregion


            //    if (filtertimesheetLeaveList.Count > 0)
            //    {
            //        TimesheetGridNewBO timesheetLeaveRow = new TimesheetGridNewBO { Id = item.LeaveTypeId, Name = item.Title, Code = "" };
            //        timesheetLeaveRow.TotalDays = 0;// payrollPeriod.TotalDays.Value;

            //        SetLeaveDays(filtertimesheetLeaveList, timesheetLeaveRow, monthStartDate);


            //        list.Add(timesheetLeaveRow);
            //    }
            //    else
            //    {
            //        list.Add(new TimesheetGridNewBO { Id = item.LeaveTypeId, Name = item.Title, TotalDays = 0 });
            //    }
            //}

            // skip leaves if has no hour
            finalList.RemoveAll(x =>
                (x.D1 == null || x.D1 == 0) && (x.D2 == null || x.D2 == 0) && (x.D3 == null || x.D3 == 0) && (x.D4 == null || x.D4 == 0) &&
                (x.D5 == null || x.D5 == 0));

            return finalList;
        }

       

        //private static double SetLeaveDays(//List<TimesheetGridNewBO> projectList,
        //    List<LeaveDetail> filtertimesheetLeaveList, TimesheetGridNewBO timesheetLeaveRow, DateTime weekStartDate)
        //{
        //    string dayStartWith = "D";
        //    string holidayStartWith = "D{0}IsHoliday";



        //    double total = 0; object value = null;
        //    DateTime weekEndDays = weekStartDate.AddDays(6);

        //    DateTime monthEndDate = new DateTime(weekStartDate.Year, weekStartDate.Month,
        //      DateTime.DaysInMonth(weekStartDate.Year, weekStartDate.Month));

        //    for (int i = 1; i <= TOTAL_WEEKS; i++)
        //    {


        //        List<LeaveDetail> leaveDays = filtertimesheetLeaveList.Where(x => x.DateOnEng >= weekStartDate
        //            && x.DateOnEng <= weekEndDays).ToList();

        //        foreach (LeaveDetail leaveItem in leaveDays)
        //        {

        //            if (leaveItem.IsWeeklyHoliday)
        //            {
        //                //valueen
        //                continue;
        //            }

        //            PropertyInfo dayProperty = timesheetLeaveRow.GetType().GetProperty(dayStartWith + i);
        //            if (dayProperty != null)
        //            {

        //                value = null;
        //                if (leaveDays != null)
        //                {
        //                    PropertyInfo holidayProperty = timesheetLeaveRow.GetType().GetProperty(string.Format(holidayStartWith, i));
        //                    if (holidayProperty != null)
        //                        holidayProperty.SetValue(timesheetLeaveRow, leaveItem.IsHoliday, null);
        //                    //timesheetLeaveRow.IsHoliday = valueEntity.IsHoliday;

        //                    value = leaveItem.LeaveRequest.IsHalfDayLeave.Value ? 4 : 8; // for half days = 4, for full days = 8

        //                    //if (leaveItem.IsHoliday)
        //                    //{
        //                    //    //for holiday also there could be overtime so to display holiday value we must check if there is value in proejct
        //                    //    // on that day & if it has then we should deduct that day in holiday display
        //                    //    //first find if there is value in project on this day
        //                    //    double projectTotalHours = GetProjectHourOnThisDay(i, projectList);
        //                    //    value = (object)(Convert.ToDouble(value) - projectTotalHours);
        //                    //}
        //                    total += (double)Convert.ToDecimal(value);
        //                }
        //                if (Convert.ToDouble(value) != 0)
        //                {
        //                    object weekValue = dayProperty.GetValue(timesheetLeaveRow, null);
        //                    value = (double)(Convert.ToDecimal(value) + Convert.ToDecimal(weekValue));
        //                    dayProperty.SetValue(timesheetLeaveRow, Convert.ToDouble(value), null);
        //                }
        //            }
        //        }

        //        weekStartDate = weekStartDate.AddDays(7);
        //        weekEndDays = weekStartDate.AddDays(6);

        //        if (weekStartDate > monthEndDate)
        //            break;

        //        if (weekEndDays > monthEndDate)
        //            weekEndDays = monthEndDate;

        //    }

        //    timesheetLeaveRow.Total = total;

           

        //    return total;
        //}
        private static double SetLeaveDays(//List<TimesheetGridNewBO> projectList,
           LeaveDetail leaveDetail, TimesheetGridNewBO timesheetLeaveRow, DateTime weekStartDate)
        {
            string dayStartWith = "D";
            string holidayStartWith = "D{0}IsHoliday";



            double total = 0; object value = null;
            DateTime weekEndDays = weekStartDate.AddDays(6);

            DateTime monthEndDate = new DateTime(weekStartDate.Year, weekStartDate.Month,
              DateTime.DaysInMonth(weekStartDate.Year, weekStartDate.Month));

            for (int i = 1; i <= TOTAL_WEEKS; i++)
            {

                //List<LeaveDetail> leaveDays = filtertimesheetLeaveList.Where(x => x.DateOnEng >= weekStartDate
                //    && x.DateOnEng <= weekEndDays).ToList();

                //foreach (LeaveDetail leaveItem in leaveDays)
                if(leaveDetail.DateOnEng >= weekStartDate && leaveDetail.DateOnEng <= weekEndDays)
                {

                    //if (leaveItem.IsWeeklyHoliday)
                    //{
                    //    //valueen
                    //    continue;
                    //}

                    PropertyInfo dayProperty = timesheetLeaveRow.GetType().GetProperty(dayStartWith + i);
                    if (dayProperty != null)
                    {

                        value = null;
                        //if (leaveDays != null)
                        {
                            //PropertyInfo holidayProperty = timesheetLeaveRow.GetType().GetProperty(string.Format(holidayStartWith, i));
                            //if (holidayProperty != null)
                            //    holidayProperty.SetValue(timesheetLeaveRow, leaveItem.IsHoliday, null);
                            ////timesheetLeaveRow.IsHoliday = valueEntity.IsHoliday;

                            value = leaveDetail.LeaveRequest.IsHalfDayLeave.Value ? 4 : 8; // for half days = 4, for full days = 8

                            total += (double)Convert.ToDecimal(value);
                        }
                        if (Convert.ToDouble(value) != 0)
                        {
                            object weekValue = dayProperty.GetValue(timesheetLeaveRow, null);
                            value = (double)(Convert.ToDecimal(value) + Convert.ToDecimal(weekValue));
                            dayProperty.SetValue(timesheetLeaveRow, Convert.ToDouble(value), null);
                        }
                    }
                }

                weekStartDate = weekStartDate.AddDays(7);
                weekEndDays = weekStartDate.AddDays(6);

                if (weekStartDate > monthEndDate)
                    break;

                if (weekEndDays > monthEndDate)
                    weekEndDays = monthEndDate;

            }

            timesheetLeaveRow.Total = total;



            return total;
        }
        public static double GetProjectHourOnThisDay(int day, List<TimesheetGridNewBO> projectList)
        {
            double total = 0;
            object value;
            foreach (TimesheetGridNewBO item in projectList)
            {
                PropertyInfo dayProperty = item.GetType().GetProperty("D" + day);
                if (dayProperty != null)
                {
                    value = dayProperty.GetValue(item, null);
                    if (value != null)
                        total += Convert.ToDouble(value);
                }
            }
            return total;
        }
    }
}
