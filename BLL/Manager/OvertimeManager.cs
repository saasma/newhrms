﻿using System.Collections.Generic;
using System.Collections.Generic;
using System.Linq;
using DAL;
using Utils.Calendar;
using System;
using BLL.Entity;
using Utils;
using Utils.Helper;
using System.Web;
using BLL.BO;

namespace BLL.Manager
{
    public class OvertimeManager : BaseBiz
    {

        #region "Overtime Setting"

        public static bool IsOvertimeSettingAlreadyExists(string timeSlot, int treatmentType, int gradeId,int? currentOvertimeSettingId)
        {


            return PayrollDataContext.OOvertimeSettings
                .Any(o => o.CompanyId == SessionManager.CurrentCompanyId
                    && o.TimeSlotType.ToString() == timeSlot.ToString()
                    && o.GradeId == gradeId && o.IsDeleted==false
                    && (currentOvertimeSettingId == null || o.OvertimeSettingId != currentOvertimeSettingId));
        }

        public static bool UpdateOvertimeSetting(OOvertimeSetting setting)
        {
            OOvertimeSetting dbEntity = PayrollDataContext.OOvertimeSettings
                .Where(o => o.OvertimeSettingId == setting.OvertimeSettingId)
                .SingleOrDefault();

            if (dbEntity == null)
                return false;


            dbEntity.TimeSlotType = setting.TimeSlotType;
            dbEntity.TreatmentType = setting.TreatmentType;
            dbEntity.GradeId = setting.GradeId;
            dbEntity.RatePercent = setting.RatePercent;
            dbEntity.LeaveHours = setting.LeaveHours;
            dbEntity.HoursWorked = setting.HoursWorked;


            return UpdateChangeSet();
        }

        public static void SetOvertimeCount(ref int? pendingCount,ref int? recommendCount,ref int?approvedCount)
        {

            if (SessionManager.CurrentLoggedInEmployeeId != 0)
            {
                PayrollDataContext.GetOvertimeRequestCountForManager
                    (SessionManager.CurrentLoggedInEmployeeId, -1, (int)OvertimeStatusEnum.Pending, ref pendingCount);

                PayrollDataContext.GetOvertimeRequestCountForManager
                (SessionManager.CurrentLoggedInEmployeeId, -1, (int)OvertimeStatusEnum.Recommended, ref recommendCount);
            }

            else
            {
                pendingCount = PayrollDataContext.OvertimeRequests.Where(x => x.Status == (int)OvertimeStatusEnum.Pending).Count();
                recommendCount = PayrollDataContext.OvertimeRequests.Where(x => x.Status == (int)OvertimeStatusEnum.Recommended).Count();
                approvedCount = PayrollDataContext.OvertimeRequests.Where(x => x.Status == (int)OvertimeStatusEnum.Approved).Count();
            }



        }

        public static void SetTravelRequestsCount(ref int approvedForAdvance, ref int empSettled)
        {
            int lastStatus = 0;
            lastStatus = (int)FlowStepEnum.Step15End;

            List<GetTravelRequestAdvanceResult> iList =
                PayrollDataContext.GetTravelRequestAdvance(SessionManager.CurrentLoggedInEmployeeId,
                (int)FlowTypeEnum.TravelOrder, null, null, lastStatus,-1, -1, -1, "", "").ToList();


            approvedForAdvance = iList.Count(x => x.AdvanceStatus == null);
            empSettled = iList.Count(x => x.AdvanceStatus != null && x.AdvanceStatus == (int)AdvanceStatusEnum.EmployeeSettle);


        }

        public static void SetTravelRequestsCountForSupervisor(ref int count)
        {
            int lastStatus = 0;
            lastStatus = (int)FlowStepEnum.Step15End;

            count = PayrollDataContext.TARequests.Where(x => x.CurrentApprovalSelectedEmployeeId == SessionManager.CurrentLoggedInEmployeeId
                && x.Status != lastStatus).Count();


        }

        public static void SetEveningCounterCount(ref int? pendingCount, ref int? recommendCount, ref int? approvedCount, int eveningCounterTypeId=-1)
        {

            if (SessionManager.CurrentLoggedInEmployeeId != 0)
            {
                List<GetEveningCounterRequestForManagerResult> list = PayrollDataContext.GetEveningCounterRequestForManager
                    (0, 99999, SessionManager.CurrentLoggedInEmployeeId, -1, (int)EveningCounterStatusEnum.Pending
                    ,null,null,"",-1, eveningCounterTypeId).ToList();

                pendingCount = list.Count;

                list = PayrollDataContext.GetEveningCounterRequestForManager(0, 99999, SessionManager.CurrentLoggedInEmployeeId, -1, (int)EveningCounterStatusEnum.Recommended
                    , null, null, "", -1, eveningCounterTypeId).ToList();

                recommendCount = list.Count;

            }

            else
            {
                pendingCount = PayrollDataContext.EveningCounterRequests.Where(x => x.Status == (int)OvertimeStatusEnum.Pending).Count();
                recommendCount = PayrollDataContext.EveningCounterRequests.Where(x => x.Status == (int)OvertimeStatusEnum.Recommended).Count();
                approvedCount = PayrollDataContext.EveningCounterRequests.Where(x => x.Status == (int)OvertimeStatusEnum.Approved).Count();
            }



        }
        public static void SetTADACount(ref int? pendingCount, ref int? recommendCount, ref int? approvedCount)
        {

            if (SessionManager.CurrentLoggedInEmployeeId != 0)
            {
                PayrollDataContext.GetTADARequestCountForManager
              (SessionManager.CurrentLoggedInEmployeeId, (int)TADAStatusEnum.Pending, ref pendingCount);

                PayrollDataContext.GetTADARequestCountForManager
                     (SessionManager.CurrentLoggedInEmployeeId, (int)TADAStatusEnum.Recommended, ref recommendCount);

            }

            else
            {
                pendingCount = PayrollDataContext.DHTADAClaims.Where(x => x.Status == (int)TADAStatusEnum.Pending).Count();
                recommendCount = PayrollDataContext.DHTADAClaims.Where(x => x.Status == (int)TADAStatusEnum.Recommended).Count();
                approvedCount = PayrollDataContext.DHTADAClaims.Where(x => x.Status == (int)TADAStatusEnum.Approved).Count();
            }



        }

    

        public static bool IsSettingUsedForEmployee(int overtimeSettingId)
        {

            OOvertimePay overtime = PayrollDataContext.OOvertimePays
                .Where(s => s.OvertimeSettingId == overtimeSettingId)
                .Take(1).SingleOrDefault();

            if (overtime == null)
                return false;


            return true;

        }

        public static bool IsSettingDeletable(int overtimeSettingId)
        {
            OOvertimePay overtime = PayrollDataContext.OOvertimePays
                .Where(s => s.OvertimeSettingId == overtimeSettingId)
                .Take(1).SingleOrDefault();

            if (overtime == null)
                return true;

            //now check if the associated payroll is saved finally then allow to delete it
            //else don't allow

            if (CalculationManager.IsAllEmployeeSavedFinally(overtime.PayrollPeriodId.Value, false))
                return true;

            return false;
        }

        public static bool DeleteOvertimeSetting(int overtimeSettingId)
        {
            OOvertimeSetting dbEntity = PayrollDataContext.OOvertimeSettings
                .Where(o => o.OvertimeSettingId == overtimeSettingId)
                .SingleOrDefault();

            if (dbEntity == null)
                return false;

            dbEntity.IsDeleted = true;

            return UpdateChangeSet();
        }

        public static OOvertimeSetting GetSettingById(int overtimeSettingId)
        {
            return
                PayrollDataContext.OOvertimeSettings
                .Where(o => o.OvertimeSettingId == overtimeSettingId)
                .SingleOrDefault();
        }

        /// <summary>
        /// Ref from the excel "HPL OT settting validation.xlsx"
        /// </summary>
        /// <param name="setting"></param>
        /// <returns></returns>
        public static bool IsOvertimeSettingValid(OOvertimeSetting setting,int? overtimeSettingId)
        {
            OOvertimeSetting dbSetting = PayrollDataContext.OOvertimeSettings
                .Where(o => o.GradeId == setting.GradeId && o.IsDeleted == false)
                .Take(1)
                .SingleOrDefault();

            if (dbSetting == null)
                return true;


            if (overtimeSettingId != null && OvertimeManager.IsSettingDeletable(overtimeSettingId.Value))
            {
                return true;
            }
            else
            {
                //if current grade treatment is different then not valid as a grade can have eiter overtime or leave
                if (setting.TreatmentType != dbSetting.TreatmentType)
                    return false;
            }

            return true;
        }

        public static OOvertimeSetting GetOvertimeSettingForEmployee(int gradeId, string timeslotType)
        {
            return PayrollDataContext.OOvertimeSettings
                .Where(s => s.GradeId == gradeId 
                    && s.TimeSlotType == timeslotType
                    && s.IsDeleted==false)
                .SingleOrDefault();
        }


        public static bool SaveOvertimeSetting(OOvertimeSetting setting)
        {
            setting.CompanyId = SessionManager.CurrentCompanyId;
            setting.Created = setting.Updated = DateTime.Now;
            setting.IsDeleted = false;
            PayrollDataContext.OOvertimeSettings.InsertOnSubmit(setting);
            return SaveChangeSet();
        }

        public static List<OOvertimeSetting> GetOvertimeSettings()
        {
            return PayrollDataContext.OOvertimeSettings
               .Where(o => o.CompanyId == SessionManager.CurrentCompanyId && o.IsDeleted==false)
               .OrderBy(o => o.TimeSlotType)
               .ThenBy(o => o.TreatmentType)
               .ThenBy(o => o.GradeId)
               .ToList();
        }

        #endregion

        public static bool IsRequestAutoGroupType
        {
            get
            {
                if (OvertimeManager.getTopRounding().IsOTRequestTypeAutoGroup != null && OvertimeManager.getTopRounding().IsOTRequestTypeAutoGroup.Value)
                {
                    return true;
                }
                return false;
            }
        }

        #region "Manage Overtime"

        /// <summary>
        /// Checks if the overtime emp salary has been saved for the payrollperiod or not
        /// </summary>
        /// <param name="overtimeId"></param>
        /// <returns></returns>
        public static bool IsOvertimeEditable(int overtimeId)
        {
            OOvertimePay overtime = PayrollDataContext.OOvertimePays
              .Where(o => o.OvertimeId == overtimeId)
              .SingleOrDefault();


            return !CalculationManager.IsCalculationSavedForEmployee(overtime.PayrollPeriodId.Value
                , overtime.EmployeeId.Value);
        }

        public static bool UpdateOvertime(OOvertimePay overtime)
        {
            OOvertimePay dbEntity = PayrollDataContext.OOvertimePays
                .Where(o => o.OvertimeId == overtime.OvertimeId)
                .SingleOrDefault();

            if (dbEntity == null)
                return false;


            dbEntity.Hours = overtime.Hours;
            dbEntity.Notes = overtime.Notes;

            return UpdateChangeSet();
        }

        public static bool SaveOvertime(OOvertimePay overtime)
        {
            overtime.Created = overtime.Updated = DateTime.Now;
            overtime.CreatedBy = SessionManager.UserName;

            PayrollDataContext.OOvertimePays.InsertOnSubmit(overtime);
            return SaveChangeSet();
        }

        public static bool IsOvertimeAlreadyExists(OOvertimePay overtime, int? currentOvertimeId)
        {
            return PayrollDataContext.OOvertimePays
                .Where(o => o.EmployeeId == overtime.EmployeeId && o.PayrollPeriodId == overtime.PayrollPeriodId
                    && o.TimeSlotType.Trim().Equals(overtime.TimeSlotType.Trim())
                    && (currentOvertimeId == null || o.OvertimeId != currentOvertimeId))
                    .Count() > 0;
        }

        public static OOvertimePay GetOvertimeById(int overtimeId)
        {
            return PayrollDataContext.OOvertimePays
                .Where(o => o.OvertimeId == overtimeId)
                .SingleOrDefault();
        }
        public static OvertimeType GetOvertimeType(int overtimeId)
        {
            return PayrollDataContext.OvertimeTypes
                .Where(o => o.OvertimeTypeId == overtimeId)
                .SingleOrDefault();
        }
        public static List<OOvertimePay> GetOvertimes(int payrollPeriodId)
        {
            return
                PayrollDataContext.OOvertimePays
                .Where(o => o.PayrollPeriodId == payrollPeriodId)
                .OrderBy(o => o.EEmployee.Name).ToList();
        }

        //public static decimal? GetOvertimeAmountHPL(int employeeId, int payrollPeriodId,int overtimeId)
        //{
        //    return
        //        PayrollDataContext.GetOvertimeAmountHPL(employeeId, payrollPeriodId,
        //        SessionManager.CurrentCompanyId, overtimeId);
        //}

        public static bool DeleteOvertime(int overtimeId)
        {
            OOvertimePay dbEntity = PayrollDataContext.OOvertimePays
                .Where(o => o.OvertimeId == overtimeId)
                .SingleOrDefault();

            if (dbEntity == null)
                return false;

            PayrollDataContext.OOvertimePays.DeleteOnSubmit(dbEntity);
            return DeleteChangeSet();
        }

        #endregion


        public static Status InsertUpdateOvertimeLevelDetail(List<OvertimeTypeLevel> lines)
        {
            Status status = new Status();
            OvertimeTypeLevel rate;
            foreach (var line in lines)
            {





                rate = new OvertimeTypeLevel();
                rate = PayrollDataContext.OvertimeTypeLevels.Where(x => x.LevelId == line.LevelId && x.OvertimeTypeId == line.OvertimeTypeId).FirstOrDefault();
                if (rate == null)
                {
                    PayrollDataContext.OvertimeTypeLevels.InsertOnSubmit(line);

                }
                else
                {
                    rate.CalculationType = line.CalculationType;
                    rate.Rate = line.Rate;
                }
            }


            PayrollDataContext.SubmitChanges();
            return status;
        }
        public static Status InsertUpdateAllowanceLevelDetail(List<EveningCounterTypeLevel> lines)
        {
            Status status = new Status();
            EveningCounterTypeLevel rate;
            foreach (var line in lines)
            {





                rate = new EveningCounterTypeLevel();
                rate = PayrollDataContext.EveningCounterTypeLevels.Where(x => x.LevelId == line.LevelId && x.EveningCounterTypeId == line.EveningCounterTypeId).FirstOrDefault();
                if (rate == null)
                {
                    PayrollDataContext.EveningCounterTypeLevels.InsertOnSubmit(line);

                }
                else
                {
                    //rate.CalculationType = line.CalculationType;
                    rate.Rate = line.Rate;
                }
            }


            PayrollDataContext.SubmitChanges();
            return status;
        }
        public static Status DeleteOvertimeType(int overtimeTypeId)
        {
            Status status = new Status();
            if (PayrollDataContext.OvertimeRequests.Any(x => x.OvertimeTypeId == overtimeTypeId))
            {
                status.ErrorMessage = "Used overtime type can not be deleted.";
                return status;
            }

            OvertimeType dbType = PayrollDataContext.OvertimeTypes.FirstOrDefault(x => x.OvertimeTypeId == overtimeTypeId);
            PayrollDataContext.OvertimeTypes.DeleteOnSubmit(dbType);
            PayrollDataContext.SubmitChanges();
            return status;  
        }
        public static Status InsertUpdateOvertimeType(OvertimeType overtime,bool isInsert)
        {
            Status status = new Status();


            if (PayrollDataContext.EveningCounterTypes.Any(x => x.IncomeId == overtime.IncomeId))
            {
                status.ErrorMessage = "Income set in Evening counter types cannot be set in Overtime type.";
                return status;
            }


            if (PayrollDataContext.OvertimeTypes.Any(x => x.Name.ToLower().Trim() == overtime.Name.ToLower().Trim()
                && x.OvertimeTypeId != overtime.OvertimeTypeId))
            {
                status.ErrorMessage = "Overtime name already exists.";
                return status;
            }

            if (overtime.OvertimeTypeIncomes.Count <= 0)
            {
                status.ErrorMessage = "Income(s) should be selected for % of the incomes calculation.";
                return status;
            }

            if (isInsert)
                PayrollDataContext.OvertimeTypes.InsertOnSubmit(overtime);
            else
            {
                OvertimeType dbType = PayrollDataContext.OvertimeTypes.FirstOrDefault(x => x.OvertimeTypeId == overtime.OvertimeTypeId);

                if (dbType.Name != overtime.Name)
                    PayrollDataContext.ChangeSettings.InsertOnSubmit(GetSettingChangeLog((byte)MonetoryChangeLogTypeEnum.AllowanceType, "Name",
                        dbType.Name, overtime.Name, LogActionEnum.Update));

                //if (dbType.Rate != overtime.Rate)
                //    PayrollDataContext.ChangeSettings.InsertOnSubmit(GetSettingChangeLog((byte)MonetoryChangeLogTypeEnum.AllowanceType, "Rate",
                //        GetString(dbType.Rate), GetString(overtime.Rate), LogActionEnum.Update));

                if (dbType.IncomeId != overtime.IncomeId)
                    PayrollDataContext.ChangeSettings.InsertOnSubmit(GetSettingChangeLog((byte)MonetoryChangeLogTypeEnum.AllowanceType, "Income Id",
                        GetString(dbType.IncomeId), GetString(overtime.IncomeId), LogActionEnum.Update));

                if (dbType.CalculationType != overtime.CalculationType)
                    PayrollDataContext.ChangeSettings.InsertOnSubmit(GetSettingChangeLog((byte)MonetoryChangeLogTypeEnum.AllowanceType, "Calculation Type",
                        GetString(dbType.CalculationType), GetString(overtime.CalculationType), LogActionEnum.Update));

                
                dbType.Name = overtime.Name;
                dbType.CalculationType = overtime.CalculationType;
                dbType.Rate = overtime.Rate;
                dbType.IncomeId = overtime.IncomeId;


                string oldIncome = "", newIncome = "";
                foreach (OvertimeTypeIncome inc in dbType.OvertimeTypeIncomes.OrderBy(x => x.IncomeId))
                {
                    oldIncome += "," + inc.IncomeId.ToString();
                }

                foreach (OvertimeTypeIncome inc in overtime.OvertimeTypeIncomes.OrderBy(x => x.IncomeId))
                {
                    newIncome += "," + inc.IncomeId.ToString();
                }

                if (oldIncome != newIncome)
                    PayrollDataContext.ChangeSettings.InsertOnSubmit(GetSettingChangeLog((byte)MonetoryChangeLogTypeEnum.AllowanceType, "Incomes",
                        oldIncome, newIncome, LogActionEnum.Update));


              //  dbType.OvertimeTypeIncomes.Clear();
                PayrollDataContext.OvertimeTypeIncomes.DeleteAllOnSubmit(dbType.OvertimeTypeIncomes);
                dbType.OvertimeTypeIncomes.AddRange(overtime.OvertimeTypeIncomes.ToList());

                //dbType.OvertimeNotApplicableTos.Clear();
                PayrollDataContext.OvertimeNotApplicableTos.DeleteAllOnSubmit(dbType.OvertimeNotApplicableTos);
                dbType.OvertimeNotApplicableTos.AddRange(overtime.OvertimeNotApplicableTos.ToList());
            }


            PayrollDataContext.SubmitChanges();
            return status;
        }
        public static List<OvertimeTypeLevel> GetOvertimeLevelList(int overtypeId)
        {
            List<OvertimeTypeLevel> returnList = new List<OvertimeTypeLevel>();
            List<BLevel> levels = new List<BLevel>();

            levels = PayrollDataContext.BLevels.OrderBy(x => x.Order).ToList();
            OvertimeTypeLevel rate;
            if (levels != null)
            {
                foreach (var val1 in levels)
                {
                    rate = new OvertimeTypeLevel();
                    rate.LevelId = val1.LevelId;
                    rate.LevelName = val1.Name;
                    rate.OvertimeTypeId = overtypeId;
                    returnList.Add(rate);
                }

                if (returnList != null)
                {
                    foreach (var val2 in returnList)
                    {
                      
                        rate = new OvertimeTypeLevel();
                        rate = PayrollDataContext.OvertimeTypeLevels.Where(x => x.OvertimeTypeId == overtypeId && x.LevelId == val2.LevelId).FirstOrDefault();
                        if (rate != null)
                        {
                            val2.OvertimeTypeId = overtypeId;
                            val2.CalculationType = rate.CalculationType;
                            val2.Rate = rate.Rate;

                        }


                    }
                }
            }

            return returnList;
        }
        public static List<EveningCounterTypeLevel> GetAllowanceLevelList(int allowanceId)
        {
            List<EveningCounterTypeLevel> returnList = new List<EveningCounterTypeLevel>();
            List<BLevel> levels = new List<BLevel>();

            levels = PayrollDataContext.BLevels.OrderBy(x => x.Order).ToList();
            EveningCounterTypeLevel rate;
            if (levels != null)
            {
                foreach (var val1 in levels)
                {
                    rate = new EveningCounterTypeLevel();
                    rate.LevelId = val1.LevelId;
                    rate.LevelName = val1.Name;
                    rate.EveningCounterTypeId = allowanceId;
                    returnList.Add(rate);
                }

                if (returnList != null)
                {
                    foreach (var val2 in returnList)
                    {

                        rate = new EveningCounterTypeLevel();
                        rate = PayrollDataContext.EveningCounterTypeLevels.Where(x => x.EveningCounterTypeId
                            == allowanceId && x.LevelId == val2.LevelId).FirstOrDefault();
                        if (rate != null)
                        {
                            val2.EveningCounterTypeId = allowanceId;
                            //val2.CalculationType = rate.CalculationType;
                            val2.Rate = rate.Rate;

                        }


                    }
                }
            }

            return returnList;
        }
        public static List<GetOvertimeForApprovalResult> GetOvertimelistForApproval(int start, int pagesize, int employeeId,
            int status,DateTime?StartDate,DateTime? EndDate,DateTime? date)
        {
            List<GetOvertimeForApprovalResult> list = PayrollDataContext.GetOvertimeForApproval(start, pagesize, employeeId, status, StartDate,
                EndDate,SessionManager.CurrentLoggedInEmployeeId,date).ToList();
            foreach (GetOvertimeForApprovalResult item in list)
            {
                item.StatusText =
                   HttpContext.GetGlobalResourceObject("FixedValues", ((OvertimePeriodDetailStatus)item.Status).ToString()) == null ?
                   ((OvertimePeriodDetailStatus)item.Status).ToString() :
                   HttpContext.GetGlobalResourceObject("FixedValues", ((OvertimePeriodDetailStatus)item.Status).ToString()).ToString();
                item.OTHours = MinuteToHourMinuteConverter(item.OTMinute);
                item.ApprovedHrsFormatted = MinuteToHourFormatted(item.ApprovedMin);
                item.ApprovedMinFormatted = MinuteToMinuteFormatted(item.ApprovedMin);

                try
                {
                    item.NepDate = CustomDate.ConvertEngToNep(new CustomDate(item.OTDate.Day, item.OTDate.Month, item.OTDate.Year, true))
                        .ToStringShortMonthName();
                }
                catch { }

            }
            return list;
        }
        public static List<GetMyOvertimeListResult> GetMyOvertimelist(int start, int pagesize,
            int status, DateTime? StartDate, DateTime? EndDate)
        {
            List<GetMyOvertimeListResult> list = PayrollDataContext.GetMyOvertimeList(start, pagesize, SessionManager.CurrentLoggedInEmployeeId, 
                status, StartDate,
                EndDate).ToList();
            foreach (GetMyOvertimeListResult item in list)
            {
                item.StatusText =
                   HttpContext.GetGlobalResourceObject("FixedValues", ((OvertimePeriodDetailStatus)item.Status).ToString()) == null ?
                   ((OvertimePeriodDetailStatus)item.Status).ToString() :
                   HttpContext.GetGlobalResourceObject("FixedValues", ((OvertimePeriodDetailStatus)item.Status).ToString()).ToString();
                item.OTHours = MinuteToHourMinuteConverter(item.OTMinute);
                item.ApprovedHrsFormatted = MinuteToHourFormatted(item.ApprovedMin);
                item.ApprovedMinFormatted = MinuteToMinuteFormatted(item.ApprovedMin);


            }
            return list;
        }
        public static List<OvertimePeriod> GetOverTimPeriod()
        {
            return PayrollDataContext.OvertimePeriods.OrderBy(x => x.OvertimeID).ToList(); 
        }
        public static OvertimePeriod GetOverTimPeriodById(int OvertimePeriodID)
        {
            return PayrollDataContext.OvertimePeriods.SingleOrDefault(x => x.OvertimeID == OvertimePeriodID);
        }

        public static string MinuteToHourMinuteConverter(int minute)
        {
            TimeSpan span = TimeSpan.FromMinutes(minute);
            return string.Format(" {0:D2}:{1:D2}",
                span.Hours,
                span.Minutes);
        }

        public static string MinuteToHourFormatted(int minute)
        {
            TimeSpan span = TimeSpan.FromMinutes(minute);
            return span.Hours.ToString();
            //return string.Format(" {0:D2}",span.Hours);
        }
        public static string MinuteToMinuteFormatted(int minute)
        {
            TimeSpan span = TimeSpan.FromMinutes(minute);
            return span.Minutes.ToString();
            //return string.Format(" {0:D2}", span.Minutes);
        }


        public static DateTime RoundUp(DateTime dt, TimeSpan d)
        {
            return new DateTime(((dt.Ticks + d.Ticks - 1) / d.Ticks) * d.Ticks);
        }

        public static OvertimeRounding getTopRounding()
        {
            OvertimeRounding retInstance = new OvertimeRounding();
            retInstance = PayrollDataContext.OvertimeRoundings.OrderBy(x => x.ID).FirstOrDefault();
            return retInstance;
        }


        public static bool UpdateOvertimeRounding(OvertimeRounding roundingInstance,int overtimeIncomeId)
        {
            OvertimeRounding dbEntity = new OvertimeRounding();
            dbEntity = PayrollDataContext.OvertimeRoundings.OrderBy(x => x.ID).FirstOrDefault();
            if (dbEntity == null)
            {
                dbEntity = new OvertimeRounding();
                dbEntity.ID = 1;
                PayrollDataContext.OvertimeRoundings.InsertOnSubmit(dbEntity);
            }

            #region "Logs"
            if (roundingInstance.OvertimeHoursInTheMonth != dbEntity.OvertimeHoursInTheMonth)
                PayrollDataContext.ChangeSettings.InsertOnSubmit(GetSettingChangeLog((byte)MonetoryChangeLogTypeEnum.Information, "Hours in Month",
                    dbEntity.OvertimeHoursInTheMonth == null ? "" : dbEntity.OvertimeHoursInTheMonth.ToString(), roundingInstance.OvertimeHoursInTheMonth.ToString(), LogActionEnum.Update));

            #endregion

            dbEntity.MinTime = roundingInstance.MinTime;
            dbEntity.MaxTime = roundingInstance.MaxTime;
            dbEntity.rounding = roundingInstance.rounding;
            dbEntity.OvertimeHoursInTheMonth = roundingInstance.OvertimeHoursInTheMonth;
            dbEntity.OvertimePermitedDays = roundingInstance.OvertimePermitedDays;
            dbEntity.MinBufferTimeAfterRegularShift = roundingInstance.MinBufferTimeAfterRegularShift;
            dbEntity.MaxMinInADay = roundingInstance.MaxMinInADay;
            dbEntity.MaxMinInAWeek = roundingInstance.MaxMinInAWeek;

            dbEntity.ModifiedDate = SessionManager.GetCurrentDateAndTime();
            dbEntity.ModifiedBy = SessionManager.CurrentLoggedInUserID;
            dbEntity.AllowFutureDaysRequestAlso = roundingInstance.AllowFutureDaysRequestAlso;



            Setting setting = GetSetting();
            setting.OvertimeIncomeId = overtimeIncomeId;

            dbEntity.OvertimeRuleMinTimeBufferNotApplicableTos.Clear();
            dbEntity.OvertimeRuleMinTimeBufferNotApplicableTos.AddRange(roundingInstance.OvertimeRuleMinTimeBufferNotApplicableTos.ToList());

            PayrollDataContext.SubmitChanges();
            return true;
        }

        public static bool UpdateLeaveEncashmentRounding(bool isRounding)
        {
          


            Setting setting = GetSetting();
            setting.IsLeaveEncashmentRoundingInRetirement = isRounding;

            PayrollDataContext.SubmitChanges();
            return true;
        }
        public static bool UpdateJoiningDate(bool enable)
        {



            Setting setting = GetSetting();
            setting.AllowToChangeJoiningDate = enable;

            PayrollDataContext.SubmitChanges();
            return true;
        }
        public static bool UpdateMultipleAddOn(bool enable,bool skipRetDay,bool hidePayslip,bool hideAllMonthPayslip,
            bool processOTAllowanceInAddOn,bool savingNegativeNetSalary,bool enableHealthInsurance,bool bringGratuityInRetirement)
        {
            Setting setting = GetSetting();

            #region "Log"

            if (enable != setting.EnableMultipleAddOn)
                PayrollDataContext.ChangeSettings.InsertOnSubmit(GetSettingChangeLog((byte)MonetoryChangeLogTypeEnum.Information, "Enable Multi Add-On",
                    setting.EnableMultipleAddOn == null ? "" : setting.EnableMultipleAddOn.ToString(), enable.ToString(), LogActionEnum.Update));

            if (skipRetDay != setting.SkipRetDayForCalculation)
                PayrollDataContext.ChangeSettings.InsertOnSubmit(GetSettingChangeLog((byte)MonetoryChangeLogTypeEnum.Information, "Exclude Retirement day in Calculation",
                    setting.SkipRetDayForCalculation == null ? "" : setting.SkipRetDayForCalculation.ToString(), skipRetDay.ToString(), LogActionEnum.Update));

            if (processOTAllowanceInAddOn != setting.ProcessApprovedOTAllowanceInAddOnOnly)
                PayrollDataContext.ChangeSettings.InsertOnSubmit(GetSettingChangeLog((byte)MonetoryChangeLogTypeEnum.Information, "Process Approved OT/Allowance in Add-On only",
                    setting.ProcessApprovedOTAllowanceInAddOnOnly == null ? "" : setting.ProcessApprovedOTAllowanceInAddOnOnly.ToString(), processOTAllowanceInAddOn.ToString(), LogActionEnum.Update));

            if (savingNegativeNetSalary != CommonManager.CompanySetting.PayrollAllowNegativeSalarySave)
                PayrollDataContext.ChangeSettings.InsertOnSubmit(GetSettingChangeLog((byte)MonetoryChangeLogTypeEnum.Information, "Allow Negative Net Salary Saving",
                    CommonManager.CompanySetting.PayrollAllowNegativeSalarySave.ToString(), savingNegativeNetSalary.ToString(), LogActionEnum.Update));

            if (enableHealthInsurance != setting.EnableHealthInsurance)
                PayrollDataContext.ChangeSettings.InsertOnSubmit(GetSettingChangeLog((byte)MonetoryChangeLogTypeEnum.Information, "Enable Health Insurance",
                    setting.EnableHealthInsurance == null ? "" : setting.EnableHealthInsurance.ToString(), enableHealthInsurance.ToString(), LogActionEnum.Update));

            if (bringGratuityInRetirement != setting.BringGratuityInRetirement)
                PayrollDataContext.ChangeSettings.InsertOnSubmit(GetSettingChangeLog((byte)MonetoryChangeLogTypeEnum.Information, "Bring Gratuity In Retirement",
                    setting.BringGratuityInRetirement == null ? "" : setting.BringGratuityInRetirement.ToString(), bringGratuityInRetirement.ToString(), LogActionEnum.Update));

            #endregion

            setting.EnableMultipleAddOn = enable;
            setting.SkipRetDayForCalculation = skipRetDay;
            setting.HidePaySlip = hidePayslip;
            setting.HideAllMonthPayslip = hideAllMonthPayslip;
            setting.ProcessApprovedOTAllowanceInAddOnOnly = processOTAllowanceInAddOn;
            setting.EnableHealthInsurance = enableHealthInsurance;
            setting.BringGratuityInRetirement = bringGratuityInRetirement;
            //setting.AllowNetSalaryNegative = savingNegativeNetSalary;

            CommonManager.Setting.EnableMultipleAddOn = enable;
            CommonManager.Setting.SkipRetDayForCalculation = skipRetDay;
            CommonManager.Setting.HidePaySlip = hidePayslip;
            CommonManager.Setting.HideAllMonthPayslip = hideAllMonthPayslip;
            CommonManager.Setting.ProcessApprovedOTAllowanceInAddOnOnly = processOTAllowanceInAddOn;
            CommonManager.Setting.EnableHealthInsurance = enableHealthInsurance;
            CommonManager.Setting.BringGratuityInRetirement = bringGratuityInRetirement;

            CommonManager.UpdateCompanySetting("PayrollAllowNegativeSalarySave", savingNegativeNetSalary.ToString().ToLower());
            CommonManager.CompanySetting.PayrollAllowNegativeSalarySave = savingNegativeNetSalary;

            

            PayrollDataContext.SubmitChanges();
            return true;
        }
        public static bool UpdateLeaveFareSetting(Setting setting)
        {
            Setting dbsetting = GetSetting();

            dbsetting.LeaveFareAllowProportionate = setting.LeaveFareAllowProportionate;
            dbsetting.LeaveFareFullDays = setting.LeaveFareFullDays;
            dbsetting.LeaveFareIncomeId = setting.LeaveFareIncomeId;
            dbsetting.LeaveFareLeaveTypeId = setting.LeaveFareLeaveTypeId;
            dbsetting.LeaveFareProportionateDays = setting.LeaveFareProportionateDays;

            PayrollDataContext.SubmitChanges();
            return true;

        }
        public static bool UpdateSettings(LeaveEncashmentTreatmentOnRetirement encashmentType, bool LeaveRequestSettingUsingPreDefinedList, bool LeaveRequestSettngUsingOneToOne, bool allowMultipleReview
            , LeaveEncashmentRounding rounding, bool disableAtteTimeChange, bool multipleOvertimeReqApproval, string lateEntryGraceMin,string lateDisplayGraceMin,
             string approvedLeaveType, string notificationMails,string requestNotificationMails, string statusSelectionInLeave,string disableEmpPortal,string hideUnpaidLeave,
             string lockLeaveDate,string AbsentNotificationMails,bool leaveApprovalSelection)
        {



            Setting setting = GetSetting();
            if (encashmentType == LeaveEncashmentTreatmentOnRetirement.Standard)
            {
                setting.AllLeaveOnRetirementBenefit = false;
                setting.AllLeaveOnRegularBenefit = false;
            }
            if (encashmentType == LeaveEncashmentTreatmentOnRetirement.AllFifteenPercentBenefit)
            {
                setting.AllLeaveOnRetirementBenefit = true;
                setting.AllLeaveOnRegularBenefit = false;
            }
            if (encashmentType == LeaveEncashmentTreatmentOnRetirement.AllRegular)
            {
                setting.AllLeaveOnRetirementBenefit = false;
                setting.AllLeaveOnRegularBenefit = true;
            }

            if (rounding == LeaveEncashmentRounding.NoRounding)
            {
                setting.IsLeaveEncashmentRoundingInRetirement = false;
                setting.IsLeaveBalanceRoundDown = false;
            }
            if (rounding == LeaveEncashmentRounding.RoundingUp)
            {
                setting.IsLeaveEncashmentRoundingInRetirement = true;
                setting.IsLeaveBalanceRoundDown = false;
            }
            if (rounding == LeaveEncashmentRounding.RoundingDown)
            {
                setting.IsLeaveEncashmentRoundingInRetirement = false;
                setting.IsLeaveBalanceRoundDown = true;
            }

            setting.LeaveRequestSettingUsingPreDefinedList = LeaveRequestSettingUsingPreDefinedList;
            setting.LeaveRequestSettngUsingOneToOne = LeaveRequestSettngUsingOneToOne;
            setting.LeaveAllowMultipleReview = allowMultipleReview;

            // leave approval selection or all option for Team type
            if(LeaveRequestSettingUsingPreDefinedList == false && LeaveRequestSettngUsingOneToOne == false)
            {
                CommonManager.CompanySetting.LeaveRequestToOnlyOneSupervisor = leaveApprovalSelection;

                CommonManager.UpdateCompanySetting("LeaveRequestToOnlyOneSupervisor", leaveApprovalSelection.ToString().ToLower());
            }

            setting.DisableAttendanceTimeAddEdit = disableAtteTimeChange;
            setting.MultipleOvertimeRequestApproval = multipleOvertimeReqApproval;
            setting.ApproveLeaveNotificationType = int.Parse(approvedLeaveType);
            setting.ApproveLeaveNotificationMails = notificationMails;
            setting.RequestLeaveNotificationMails = requestNotificationMails;
            setting.AbsentNotificationMails = AbsentNotificationMails;
            int? graceMin = null;
            if (!string.IsNullOrEmpty(lateEntryGraceMin))
                graceMin = int.Parse(lateEntryGraceMin);

            int? graceDisplayMin = null;
            if (!string.IsNullOrEmpty(lateDisplayGraceMin))
                graceDisplayMin = int.Parse(lateDisplayGraceMin);

            CommonManager.Setting.AtteLateDeductionGraceMin = graceMin;
            CommonManager.Setting.ApproveLeaveNotificationType = int.Parse(approvedLeaveType);
            CommonManager.Setting.ApproveLeaveNotificationMails = notificationMails;
            CommonManager.Setting.AtteLateDisplayGraceMin = graceDisplayMin;
            setting.AtteLateDeductionGraceMin = graceMin;
            setting.AtteLateDisplayGraceMin = graceDisplayMin;
            

            if (statusSelectionInLeave == "-1")
                statusSelectionInLeave = "false";


            bool statusSelection = bool.Parse(statusSelectionInLeave);
            setting.ExcludeStatusHierarchyInLeave = statusSelection;
            CommonManager.Setting.ExcludeStatusHierarchyInLeave = statusSelection;

            bool value = bool.Parse(disableEmpPortal);
            setting.DisableEmployeePortal = value;
            CommonManager.Setting.DisableEmployeePortal = value;


            value = bool.Parse(hideUnpaidLeave);
            setting.HideUnpaidLeaveInLeaveRequest = value;
            CommonManager.Setting.HideUnpaidLeaveInLeaveRequest = value;

            DateTime? date = null;
            if (!string.IsNullOrEmpty(lockLeaveDate))
                date = GetEngDate(lockLeaveDate, IsEnglish);
            setting.DateLockForLeaveChangeFromEmployee = date;
            CommonManager.Setting.DateLockForLeaveChangeFromEmployee = date;

            PayrollDataContext.SubmitChanges();
            return true;
        }

        public static bool UpdateTADAIncome(int incomeId)
        {



            Setting setting = GetSetting();
            setting.TADAIncomeId = incomeId;

            PayrollDataContext.SubmitChanges();
            return true;
        }

        public static bool IsHourMinValid(string hourMin, ref int minute,ref string message)
        {
            try
            {
                minute = 0;
                
                if (hourMin.Contains(":"))
                {
                    string[] texts = hourMin.Split(new char[] { ':' });
                    int hourValue = 0;
                    int minValue = 0;

                    string hour = texts[0];
                    string min = texts[1];

                    if (!string.IsNullOrEmpty(hour) && int.TryParse(hour, out hourValue) == false)
                    {
                        message = "Invalid hour value.";
                        return false;
                    }

                    if (!string.IsNullOrEmpty(min) && int.TryParse(min, out minValue) == false)
                    {
                        message = "Invalid minute value.";
                        return false;
                    }

                    minute = minValue + (hourValue * 60);
                }
                else
                {
                    if (int.TryParse(hourMin, out minute))
                    {
                        minute = minute * 60;
                    }
                    else
                    {
                        message = "Invalid hour value, should be integer value.";
                        return false;
                    }
                }
            }
            catch
            {
                message = "Place valid hour min in the format hh:mm.";
                return false;
            }
            return true;
        }

        public static int GetTotalRequestedMinInADay(int otRequestId,int empId,DateTime date)
        {
            List<OvertimeRequest> list = PayrollDataContext.OvertimeRequests.
                Where(x => x.OvertimeRequestID != otRequestId && x.EmployeeID == empId && x.Date.Value == date.Date
                && x.Status != (int)OvertimeStatusEnum.Rejected).ToList();

            int totalMinute = 0;

            foreach (var item in list)
            {
                if (item.RequestedTimeInMin != null)
                    totalMinute += item.RequestedTimeInMin.Value;
                else
                    totalMinute += (int)((item.EndTime.Value - item.StartTime.Value).TotalMinutes);
            }

            return totalMinute;
        }

        public static int GetTotalRequestedMinInAWeek(int otRequestId, int empId, DateTime date)
        {
            DateTime weekStart = GetWeekStartDate(date);
            DateTime weekEnd = weekStart.AddDays(6);

            List<OvertimeRequest> list = PayrollDataContext.OvertimeRequests.
                Where(x => x.OvertimeRequestID != otRequestId && x.EmployeeID == empId
                    && x.Date.Value >= weekStart && x.Date.Value <= weekEnd
                && x.Status != (int)OvertimeStatusEnum.Rejected).ToList();

            int totalMinute = 0;

            foreach (var item in list)
            {
                if (item.RequestedTimeInMin != null)
                    totalMinute += item.RequestedTimeInMin.Value;
                else
                    totalMinute += (int)((item.EndTime.Value - item.StartTime.Value).TotalMinutes);
            }

            return totalMinute;
        }

        public static DateTime GetWeekStartDate(DateTime weekDate)
        {
            return StartOfWeek(weekDate, DayOfWeek.Sunday);
        }

        public static DateTime StartOfWeek(DateTime dt, DayOfWeek startOfWeek)
        {
            int diff = dt.DayOfWeek - startOfWeek;
            if (diff < 0)
            {
                diff += 7;
            }

            return dt.AddDays(-1 * diff).Date;
        }

        public static ResponseStatus InsertUpdateRequest(OvertimeRequest requestInstance)
        {
            OvertimeRequest dbEntity = new OvertimeRequest();
            ResponseStatus status = new ResponseStatus();
            status.IsSuccessType = true;

            OvertimeRounding otSetting = PayrollDataContext.OvertimeRoundings.First();
            if (otSetting.MinTime != null && otSetting.MinTime != 0)
            {
                if (requestInstance.RequestedTimeInMin < otSetting.MinTime)
                {
                    status.IsSuccessType = false;
                    status.ErrorMessage = "OT time must be greater or equal to " + otSetting.MinTime + " minutes.";
                    return status;
                }
            }

            if (otSetting.MaxMinInADay != null && otSetting.MaxMinInADay != 0)
            {
                int totalRequested = GetTotalRequestedMinInADay(requestInstance.OvertimeRequestID, requestInstance.EmployeeID.Value, requestInstance.Date.Value);
                if (requestInstance.RequestedTimeInMin.Value + totalRequested > otSetting.MaxMinInADay.Value)
                {
                    status.IsSuccessType = false;
                    status.ErrorMessage = "Maximum OT hour in a day is " + GetHourMin(otSetting.MaxMinInADay.Value) + " only,<br/>" +
                        GetHourMin( otSetting.MaxMinInADay.Value - totalRequested) +
                         " hour of OT can only be requested for this day.";
                    return status;
                }
            }

            if (otSetting.MaxMinInAWeek != null && otSetting.MaxMinInAWeek != 0)
            {
                int totalRequested = GetTotalRequestedMinInAWeek(requestInstance.OvertimeRequestID, requestInstance.EmployeeID.Value, requestInstance.Date.Value);
                if (requestInstance.RequestedTimeInMin.Value + totalRequested > otSetting.MaxMinInAWeek.Value)
                {
                    status.IsSuccessType = false;
                    status.ErrorMessage = "Maximum OT hour in a week is " + GetHourMin(otSetting.MaxMinInAWeek.Value) + " only,<br/>" +
                        GetHourMin(otSetting.MaxMinInAWeek.Value - totalRequested ) + 
                         " hour of OT can only be requested for this day.";
                    return status;
                }
            }

            if (PayrollDataContext.OvertimeRequests.Any(x => x.OvertimeRequestID == requestInstance.OvertimeRequestID))
            {
                dbEntity = PayrollDataContext.OvertimeRequests.Where(x => x.OvertimeRequestID == requestInstance.OvertimeRequestID).SingleOrDefault();

                if (dbEntity.Status != (int)OvertimeStatusEnum.Pending)
                {
                    status.IsSuccessType = false;
                    status.ErrorMessage = "Only Requests with pending status can be updated";
                    return status;
                }

                dbEntity.NepaliDate = requestInstance.NepaliDate;
                dbEntity.Date = requestInstance.Date;
                dbEntity.StartTime = requestInstance.StartTime;
                dbEntity.EndTime = requestInstance.EndTime;
                dbEntity.Reason = requestInstance.Reason;
                dbEntity.ApprovalID = requestInstance.ApprovalID;
                dbEntity.ApprovalName = requestInstance.ApprovalName;
                dbEntity.RecommendedBy = requestInstance.RecommendedBy;
                dbEntity.OvertimeTypeId = requestInstance.OvertimeTypeId;
                dbEntity.RequestedTimeInMin = requestInstance.RequestedTimeInMin;
                dbEntity.ActualTimeInMin = requestInstance.ActualTimeInMin;

                dbEntity.ApprovedTimeInMinute = requestInstance.RequestedTimeInMin;
                dbEntity.ApprovedTime = AttendanceManager.MinuteToHourMinuteConverterWithOutLabelDisplay(dbEntity
                    .ApprovedTimeInMinute.Value);
            }
            else
            {
                requestInstance.ApprovedTimeInMinute = requestInstance.RequestedTimeInMin;
                requestInstance.ApprovedTime = AttendanceManager.MinuteToHourMinuteConverterWithOutLabelDisplay(requestInstance
                    .ApprovedTimeInMinute.Value);


                requestInstance.CreatedBy = SessionManager.User.UserID;
                requestInstance.CreatedOn = GetCurrentDateAndTime();
                PayrollDataContext.OvertimeRequests.InsertOnSubmit(requestInstance);
            }
            PayrollDataContext.SubmitChanges();

            return status;
        }

        public static ResponseStatus AssignOvertime(OvertimeRequest requestInstance)
        {
            OvertimeRequest dbEntity = new OvertimeRequest();
            ResponseStatus status = new ResponseStatus();
            status.IsSuccessType = true;

            requestInstance.CreatedBy = SessionManager.User.UserID;
            requestInstance.CreatedOn = GetCurrentDateAndTime();


            int minute = requestInstance.RequestedTimeInMin.Value;

            requestInstance.ApprovedTimeInMinute = minute;
            requestInstance.ApprovedTime = GetHourMin(minute);


            if (requestInstance.Status == (int)OvertimeStatusEnum.Recommended)
            {
                requestInstance.RecommendedBy = SessionManager.CurrentLoggedInEmployeeId;
                requestInstance.RecommendedOn = GetCurrentDateAndTime();
            }
            else
            {
                //admin assign
                if (SessionManager.CurrentLoggedInEmployeeId == 0)
                {
                    requestInstance.ForwardedBy = SessionManager.CurrentLoggedInUserID;
                    requestInstance.ForwardedOn = DateTime.Now;

                    requestInstance.ApprovalID = SessionManager.User.UserMappedEmployeeId;
                    requestInstance.ApprovedBy = SessionManager.User.UserMappedEmployeeId;
                    requestInstance.ApprovedOn = GetCurrentDateAndTime();
                }
                else
                {
                    requestInstance.ApprovedBy = SessionManager.CurrentLoggedInEmployeeId;
                    requestInstance.ApprovedOn = GetCurrentDateAndTime();
                }

                
            }

            int rounding = PayrollDataContext.OvertimeRoundings.First().rounding.Value;
            if (rounding > 0)
            {
                requestInstance.StartTime = OvertimeManager.RoundUp(requestInstance.StartTime.Value, TimeSpan.FromMinutes(rounding));
                requestInstance.EndTime = OvertimeManager.RoundUp(requestInstance.EndTime.Value, TimeSpan.FromMinutes(rounding));
            }



            PayrollDataContext.OvertimeRequests.InsertOnSubmit(requestInstance);


            PayrollDataContext.SubmitChanges();
            return status;
        }
        public static ResponseStatus isRequestedDateValid(int? empID, DateTime? reqDate, DateTime? startTime, DateTime? endTime, bool isEdit)
        {
            ResponseStatus status = new ResponseStatus();
            status.IsSuccessType = true;


            DateTime today = SessionManager.GetCurrentDateAndTime();
            today = new DateTime(today.Year, today.Month, today.Day);

            OvertimeRounding roundingInstance = OvertimeManager.getTopRounding();

            if (roundingInstance != null && roundingInstance.AllowFutureDaysRequestAlso != null && roundingInstance.AllowFutureDaysRequestAlso.Value)
            { }
            else if (reqDate.Value > today)
            {
                status.IsSuccessType = false;
                status.ErrorMessage = "Overtime Request date can not be in Future";
                return status;
            }

            if (startTime != null)
            {
                if (startTime == endTime || endTime < startTime)
                {
                    status.IsSuccessType = false;
                    status.ErrorMessage = "End Time can not be less than or Equal to Start Time";
                    return status;
                }
            }

            

            int PermitedDays = int.Parse(PayrollDataContext.OvertimeRoundings.First().OvertimePermitedDays.Value.ToString());
            DateTime PermitedDate = today.AddDays(-PermitedDays);
            if (reqDate.Value < PermitedDate)
            {
                status.IsSuccessType = false;
                status.ErrorMessage = "Can not request Overtime in the past days.";
                return status;
            }



            if (isEdit == false && startTime != null)
            {

                //if (CommonManager.CompanySetting.WhichCompany != WhichCompany.HPL)
                {

                    if (
                        PayrollDataContext.OvertimeRequests.Any(x => x.EmployeeID == empID
                            && x.Date == reqDate && x.Status != (int)OvertimeStatusEnum.Rejected
                            &&
                            (
                                (startTime > x.StartTime && startTime < x.EndTime) ||
                                (endTime > x.StartTime && endTime < x.EndTime)
                            )))
                    {

                        status.IsSuccessType = false;
                        status.ErrorMessage = "Overtime request already exists for this day and time.";
                        return status;
                    }

                }

            }

            return status;
                 
            
        }

        public static bool Between(DateTime input, DateTime date1, DateTime date2)
        {
            return (input > date1 && input < date2);
        }

        public static Status ApproveRequest(OvertimeRequest requestInstance,List<RequestEditHistory> edits)
        {
            OvertimeRequest dbEntity = new OvertimeRequest();
            Status status = new Status();
            if (PayrollDataContext.OvertimeRequests.Any(x => x.OvertimeRequestID == requestInstance.OvertimeRequestID))
            {

                dbEntity = PayrollDataContext.OvertimeRequests.Where(x => x.OvertimeRequestID == requestInstance.OvertimeRequestID).SingleOrDefault();

                if (dbEntity.ApprovalID != SessionManager.CurrentLoggedInEmployeeId && dbEntity.ApprovalID2 != SessionManager.CurrentLoggedInEmployeeId)
                {
                    status.ErrorMessage = ((OvertimeStatusEnum)dbEntity.Status).ToString() + " overtime can not be approved.";
                    return status;
                }

                dbEntity.ApprovedTime = requestInstance.ApprovedTime;
                dbEntity.ApprovedTimeInMinute = requestInstance.ApprovedTimeInMinute;
                dbEntity.Status = requestInstance.Status;
                dbEntity.ApprovedBy = SessionManager.CurrentLoggedInEmployeeId;
                dbEntity.ApprovedOn = GetCurrentDateAndTime();


                foreach (var editHist in edits)
                {
                    editHist.RequestID = dbEntity.OvertimeRequestID;

                    if (SessionManager.User.EEmployee != null && SessionManager.User.EEmployee.EmployeeId != null)
                    {
                        editHist.ModifiedBy = SessionManager.User.EEmployee.EmployeeId.ToString();
                        editHist.ModifiedByName = SessionManager.User.EEmployee.Name;
                    }
                    else if (SessionManager.User != null && SessionManager.User.UserID != null)
                    {
                        editHist.ModifiedBy = SessionManager.User.UserID.ToString();
                        editHist.ModifiedByName = SessionManager.User.UserName;
                    }
                    //editHist.ModifiedBy = SessionManager.User.UserID;

                    editHist.ModifiedOn = CommonManager.GetCurrentDateAndTime();
                    editHist.RequestType = (int)RequestHistoryTypeEnum.Overtime;
                }

                PayrollDataContext.RequestEditHistories.InsertAllOnSubmit(edits);

            }
            PayrollDataContext.SubmitChanges();
            return status;
        }
        public static Status UpdateRequestFromHR(OvertimeRequest requestInstance, List<RequestEditHistory> edits)
        {
            OvertimeRequest dbEntity = new OvertimeRequest();
            Status status = new Status();
            if (PayrollDataContext.OvertimeRequests.Any(x => x.OvertimeRequestID == requestInstance.OvertimeRequestID))
            {

                dbEntity = PayrollDataContext.OvertimeRequests.Where(x => x.OvertimeRequestID == requestInstance.OvertimeRequestID).SingleOrDefault();

                if (dbEntity.Status == (int)OvertimeStatusEnum.Forwarded && dbEntity.PayrollPeriodId != null
                    && CalculationManager.IsCalculationSavedForEmployee(dbEntity.PayrollPeriodId.Value,dbEntity.EmployeeID.Value))
                {
                    status.ErrorMessage = "Salary processed overtime can not be changed.";
                    return status;
                }

                foreach(var editHist in edits)
                {

                editHist.RequestID = dbEntity.OvertimeRequestID;

                if (SessionManager.User.EEmployee != null && SessionManager.User.EEmployee.EmployeeId!=null)
                {
                    editHist.ModifiedBy = SessionManager.User.EEmployee.EmployeeId.ToString();
                    editHist.ModifiedByName = SessionManager.User.EEmployee.Name;
                }
                else if (SessionManager.User != null && SessionManager.User.UserID != null)
                {
                    editHist.ModifiedBy = SessionManager.User.UserID.ToString();
                    editHist.ModifiedByName = SessionManager.User.UserName;
                }
                //editHist.ModifiedBy = SessionManager.User.UserID;
                
                    editHist.ModifiedOn = CommonManager.GetCurrentDateAndTime();
                    editHist.RequestType = (int)RequestHistoryTypeEnum.Overtime;

                    PayrollDataContext.RequestEditHistories.InsertOnSubmit(editHist);
                }

                dbEntity.OvertimeTypeId = requestInstance.OvertimeTypeId;
                dbEntity.ApprovedTime = requestInstance.ApprovedTime;
                dbEntity.ApprovedTimeInMinute = requestInstance.ApprovedTimeInMinute;
            }
            PayrollDataContext.SubmitChanges();
            return status;
        }
        public static Status RejectOTRequestFromHR(OvertimeRequest requestInstance)
        {
            OvertimeRequest dbEntity = new OvertimeRequest();
            Status status = new Status();
            if (PayrollDataContext.OvertimeRequests.Any(x => x.OvertimeRequestID == requestInstance.OvertimeRequestID))
            {

                dbEntity = PayrollDataContext.OvertimeRequests.Where(x => x.OvertimeRequestID == requestInstance.OvertimeRequestID).SingleOrDefault();

                if (dbEntity.Status != (int)OvertimeStatusEnum.Forwarded)
                {
                    status.ErrorMessage = "Only forwarded status can be rejected from HR.";
                    return status;
                }

                if (SessionManager.User.UserMappedEmployeeId == null || SessionManager.User.UserMappedEmployeeId == 0 || SessionManager.User.UserMappedEmployeeId==-1)
                {
                    status.ErrorMessage = "Admin user should map to Employee user for processing.";
                    return status;
                }

                if (dbEntity.Status == (int)OvertimeStatusEnum.Forwarded && dbEntity.PayrollPeriodId != null
                    && CalculationManager.IsCalculationSavedForEmployee(dbEntity.PayrollPeriodId.Value, dbEntity.EmployeeID.Value))
                {
                    status.ErrorMessage = "Salary processed overtime can not be changed.";
                    return status;
                }


                dbEntity.ApprovedBy = SessionManager.User.UserMappedEmployeeId ;
                dbEntity.ApprovedOn = BLL.BaseBiz.GetCurrentDateAndTime();
                dbEntity.Status = (int)OvertimeStatusEnum.Rejected;

            }
            PayrollDataContext.SubmitChanges();
            return status;
        }
        public static Status RejectAllowanceRequestFromHR(EveningCounterRequest requestInstance)
        {
            EveningCounterRequest dbEntity = new EveningCounterRequest();
            Status status = new Status();
            if (PayrollDataContext.EveningCounterRequests.Any(x => x.CounterRequestID == requestInstance.CounterRequestID))
            {

                dbEntity = PayrollDataContext.EveningCounterRequests.Where(x => x.CounterRequestID == requestInstance.CounterRequestID).SingleOrDefault();

                if (dbEntity.Status != (int)EveningCounterStatusEnum.Forwarded)
                {
                    status.ErrorMessage = "Only forwarded status can be rejected from HR.";
                    return status;
                }

                if (SessionManager.User.UserMappedEmployeeId == null || SessionManager.User.UserMappedEmployeeId == 0 || SessionManager.User.UserMappedEmployeeId == -1)
                {
                    status.ErrorMessage = "Admin user should map to Employee user for processing.";
                    return status;
                }

                if (dbEntity.Status == (int)EveningCounterStatusEnum.Forwarded && dbEntity.PayrollPeriodId != null
                    && CalculationManager.IsCalculationSavedForEmployee(dbEntity.PayrollPeriodId.Value, dbEntity.EmployeeID.Value))
                {
                    status.ErrorMessage = "Salary processed allowance can not be changed.";
                    return status;
                }


                dbEntity.ApprovedBy = SessionManager.User.UserMappedEmployeeId;
                dbEntity.ApprovedOn = BLL.BaseBiz.GetCurrentDateAndTime();
                dbEntity.Status = (int)EveningCounterStatusEnum.Rejected;

            }
            PayrollDataContext.SubmitChanges();
            return status;
        }
        public static Status UpdateRequestFromHR(EveningCounterRequest requestInstance, List<RequestEditHistory> edits)
        {
            Status status = new Status();

            DateTime today = SessionManager.GetCurrentDateAndTime();
            today = new DateTime(today.Year, today.Month, today.Day);

            // as can be in future also
            //if (request.EndDate > today)
            //{                
            //    status.ErrorMessage = "Request date cannot be in Future.";
            //    return status;
            //}

            EveningCounterRequest dbEntity = new EveningCounterRequest();

            dbEntity = PayrollDataContext.EveningCounterRequests.Where(x => x.CounterRequestID == requestInstance.CounterRequestID).SingleOrDefault();


            if (PayrollDataContext.EveningCounterRequests.Any(x =>
                x.EmployeeID == dbEntity.EmployeeID
                && dbEntity.Status != (int)EveningCounterStatusEnum.Rejected
                && x.EveningCounterTypeId == requestInstance.EveningCounterTypeId
                && x.CounterRequestID != requestInstance.CounterRequestID
                &&
                (
                    (x.StartDate >= requestInstance.StartDate && x.StartDate <= requestInstance.EndDate) ||
                    (x.EndDate >= requestInstance.StartDate && x.EndDate <= requestInstance.EndDate) ||
                    (requestInstance.StartDate >= x.StartDate && requestInstance.StartDate <= x.EndDate) ||
                    (requestInstance.EndDate >= x.StartDate && requestInstance.EndDate <= x.EndDate)
                ))
                )
            {
                status.ErrorMessage = "Request already exists for this date and allowance.";
                return status;
            }

            // Validate that the start/end date should lie in same nepali calendar month
            if (IsEnglish == false)
            {
                CustomDate dateStart = CustomDate.GetCustomDateFromString(GetAppropriateDate(requestInstance.StartDate.Value), false);
                CustomDate dateEnd = CustomDate.GetCustomDateFromString(GetAppropriateDate(requestInstance.EndDate.Value), false);
                if (dateStart.Month != dateEnd.Month || dateStart.Year != dateEnd.Year)
                {
                    status.ErrorMessage = "End date should also lie in the same Start date month of \"" +
                        DateHelper.GetMonthName(dateStart.Month, false) + "\".";
                    return status;
                }
                requestInstance.MonthName = DateHelper.GetMonthName(dateStart.Month, false);

                requestInstance.MonthTotalDays = DateHelper.GetTotalDaysInTheMonth(
                    dateStart.Year, dateStart.Month, false);
            }



            
           
            if (PayrollDataContext.EveningCounterRequests.Any(x => x.CounterRequestID == requestInstance.CounterRequestID))
            {

                if (dbEntity.Status == (int)OvertimeStatusEnum.Forwarded && dbEntity.PayrollPeriodId != null
                    && CalculationManager.IsCalculationSavedForEmployee(dbEntity.PayrollPeriodId.Value, dbEntity.EmployeeID.Value))
                {
                    status.ErrorMessage = "Salary processed allowance can not be changed.";
                    return status;
                }


                dbEntity.EveningCounterTypeId = requestInstance.EveningCounterTypeId;
                dbEntity.StartDate = requestInstance.StartDate;
                dbEntity.EndDate = requestInstance.EndDate;
                dbEntity.Days = requestInstance.Days;
                dbEntity.MonthName = requestInstance.MonthName;
                dbEntity.MonthTotalDays = requestInstance.MonthTotalDays;

                foreach (var editHist in edits)
                {
                    editHist.RequestID = requestInstance.CounterRequestID;

                    if (SessionManager.User.EEmployee != null && SessionManager.User.EEmployee.EmployeeId != null)
                    {
                        editHist.ModifiedBy = SessionManager.User.EEmployee.EmployeeId.ToString();
                        editHist.ModifiedByName = SessionManager.User.EEmployee.Name;
                    }
                    else if (SessionManager.User != null && SessionManager.User.UserID != null)
                    {
                        editHist.ModifiedBy = SessionManager.User.UserID.ToString();
                        editHist.ModifiedByName = SessionManager.User.UserName;
                    }
                    //editHist.ModifiedBy = SessionManager.User.UserID;

                    editHist.ModifiedOn = CommonManager.GetCurrentDateAndTime();
                    editHist.RequestType = (int)RequestHistoryTypeEnum.EveningCounter;
                }

                PayrollDataContext.RequestEditHistories.InsertAllOnSubmit(edits);

            }
            PayrollDataContext.SubmitChanges();
            return status;
        }
        public static Status RecommendRequest(OvertimeRequest requestInstance, List<RequestEditHistory> edits)
        {
            OvertimeRequest dbEntity = new OvertimeRequest();
            Status status = new Status();
            if (PayrollDataContext.OvertimeRequests.Any(x => x.OvertimeRequestID == requestInstance.OvertimeRequestID))
            {

                dbEntity = PayrollDataContext.OvertimeRequests.Where(x => x.OvertimeRequestID == requestInstance.OvertimeRequestID).SingleOrDefault();

                if (dbEntity.Status != (int)OvertimeStatusEnum.Pending)
                {
                    status.ErrorMessage = ((OvertimeStatusEnum)dbEntity.Status).ToString() + " overtime can not be recommended.";
                    return status;
                }

                dbEntity.ApprovedTime = requestInstance.ApprovedTime;
                dbEntity.ApprovedTimeInMinute = requestInstance.ApprovedTimeInMinute;
                dbEntity.Status = requestInstance.Status;
                dbEntity.RecommendedBy = SessionManager.CurrentLoggedInEmployeeId;
                dbEntity.RecommendedOn = GetCurrentDateAndTime();

                foreach (var editHist in edits)
                {
                    editHist.RequestID = dbEntity.OvertimeRequestID;

                    if (SessionManager.User.EEmployee != null && SessionManager.User.EEmployee.EmployeeId != null)
                    {
                        editHist.ModifiedBy = SessionManager.User.EEmployee.EmployeeId.ToString();
                        editHist.ModifiedByName = SessionManager.User.EEmployee.Name;
                    }
                    else if (SessionManager.User != null && SessionManager.User.UserID != null)
                    {
                        editHist.ModifiedBy = SessionManager.User.UserID.ToString();
                        editHist.ModifiedByName = SessionManager.User.UserName;
                    }
                    //editHist.ModifiedBy = SessionManager.User.UserID;

                    editHist.ModifiedOn = CommonManager.GetCurrentDateAndTime();
                    editHist.RequestType = (int)RequestHistoryTypeEnum.Overtime;
                }

                PayrollDataContext.RequestEditHistories.InsertAllOnSubmit(edits);
            }
            PayrollDataContext.SubmitChanges();
            return status;
        }
        public static bool ForwardRequest(int requestID)
        {
            OvertimeRequest dbEntity = new OvertimeRequest();
            dbEntity = PayrollDataContext.OvertimeRequests.Where(x => x.OvertimeRequestID == requestID).SingleOrDefault();
            dbEntity.Status = (int)OvertimeStatusEnum.Forwarded;
            dbEntity.ForwardedBy = SessionManager.CurrentLoggedInUserID;
            dbEntity.ForwardedOn = CommonManager.GetCurrentDateAndTime();

            PayrollDataContext.SubmitChanges();
            return true;
        }

        public static List<OvertimeType> GetOvertimeList()
        {
             List<OvertimeType> list = PayrollDataContext.OvertimeTypes.OrderBy(x => x.Name).ToList();
            PayManager mgr = new PayManager();
             foreach (OvertimeType item in list)
             {
                 if (item.IncomeId != null && mgr.GetIncomeById(item.IncomeId.Value) != null)
                     item.Income = mgr.GetIncomeById(item.IncomeId.Value).Title;
             }
             return list;
        }
        public static Setting GetSetting()
        {
            return PayrollDataContext.Settings.FirstOrDefault();
        }
        public static bool ForwardRequestInBulk(List<OvertimeRequest> requests,out int count)
        {
            Setting setting = PayrollDataContext.Settings.FirstOrDefault();
            //int? overtimeIncomeId = null;

            //if(setting != null)
            //    overtimeIncomeId = setting.OvertimeIncomeId;

            Dictionary<string, string> inserted = new System.Collections.Generic.Dictionary<string, string>();

            count = 0;
            OvertimeRequest reqInstance;
            foreach (OvertimeRequest req in requests)
            {
                reqInstance = new OvertimeRequest();
                reqInstance = PayrollDataContext.OvertimeRequests.Where(x => x.OvertimeRequestID == req.OvertimeRequestID).FirstOrDefault();
                if (reqInstance != null && reqInstance.Status == (int)OvertimeStatusEnum.Approved)
                {
                    reqInstance.Status = (int)OvertimeStatusEnum.Forwarded;
                    reqInstance.ForwardedBy = SessionManager.CurrentLoggedInUserID;
                    reqInstance.ForwardedOn = CommonManager.GetCurrentDateAndTime();
                    count += 1;

                    // set income association also
                    //if (overtimeIncomeId != null && overtimeIncomeId != -1)
                    //{
                    //    if (PayrollDataContext.PEmployeeIncomes.Any(x => x.EmployeeId == reqInstance.EmployeeID && x.IncomeId == overtimeIncomeId.Value) == false)
                    //    {
                    //        // prevent double insertion in this mode
                    //        if (inserted.ContainsKey(reqInstance.EmployeeID.Value + ":" + overtimeIncomeId.Value) == false)
                    //        {
                    //            PEmployeeIncome overtimeInc = new PEmployeeIncome();
                    //            overtimeInc.EmployeeId = reqInstance.EmployeeID.Value;
                    //            overtimeInc.IncomeId = overtimeIncomeId.Value;
                    //            overtimeInc.IsValid = true;
                    //            overtimeInc.IsEnabled = true;

                    //            PayrollDataContext.PEmployeeIncomes.InsertOnSubmit(overtimeInc);
                    //            inserted.Add(reqInstance.EmployeeID + ":" + overtimeInc.IncomeId, "");
                    //        }
                    //    }
                    //}

                    OvertimeType overtimeType = PayrollDataContext.OvertimeTypes.FirstOrDefault(x => x.OvertimeTypeId == reqInstance.OvertimeTypeId);
                    if (overtimeType != null && overtimeType.IncomeId != null)
                    {
                        if (PayrollDataContext.PEmployeeIncomes.Any(x => x.EmployeeId == reqInstance.EmployeeID && x.IncomeId == overtimeType.IncomeId.Value) == false)
                        {
                            // prevent double insertion in this mode
                            if (inserted.ContainsKey(reqInstance.EmployeeID.Value + ":" + overtimeType.IncomeId.Value) == false)
                            {
                                PEmployeeIncome overtimeInc = new PEmployeeIncome();
                                overtimeInc.EmployeeId = reqInstance.EmployeeID.Value;
                                overtimeInc.IncomeId = overtimeType.IncomeId.Value;
                                overtimeInc.IsValid = true;
                                overtimeInc.IsEnabled = true;

                                PayrollDataContext.PEmployeeIncomes.InsertOnSubmit(overtimeInc);
                                inserted.Add(reqInstance.EmployeeID + ":" + overtimeType.IncomeId.Value, "");
                            }
                        }
                    }
                }
            }

            PayrollDataContext.SubmitChanges();
            return true;
        }


        public static bool RejectRequestInBulk(List<OvertimeRequest> requests, out int count)
        {
            

            Dictionary<string, string> inserted = new System.Collections.Generic.Dictionary<string, string>();

            count = 0;
            OvertimeRequest reqInstance;
            foreach (OvertimeRequest req in requests)
            {
                reqInstance = new OvertimeRequest();
                reqInstance = PayrollDataContext.OvertimeRequests.Where(x => x.OvertimeRequestID == req.OvertimeRequestID).FirstOrDefault();
                if (reqInstance != null && reqInstance.Status == (int)OvertimeStatusEnum.Approved)
                {
                    reqInstance.Status = (int)OvertimeStatusEnum.Rejected;
                    reqInstance.ForwardedBy = SessionManager.CurrentLoggedInUserID;
                    reqInstance.ForwardedOn = CommonManager.GetCurrentDateAndTime();
                    count += 1;

                   
                }
            }

            PayrollDataContext.SubmitChanges();
            return true;
        }



        public static int GetMinute(DateTime start, DateTime end)
        { 
            return (int)
                (end.Subtract(start).TotalMinutes);
        }
        public static string GetHourMin(int minute)
        {
            return new AttendanceManager().MinuteToHourMinuteConverter(minute);
        }

        public static bool RecommendOrApprove(List<OvertimeRequest> requests, out int count)
        {


            Dictionary<string, string> inserted = new System.Collections.Generic.Dictionary<string, string>();

            count = 0;
            OvertimeRequest dbEntity;
            foreach (OvertimeRequest req in requests)
            {

                dbEntity = PayrollDataContext.OvertimeRequests.Where(x => x.OvertimeRequestID == req.OvertimeRequestID).FirstOrDefault();
                if (dbEntity != null && dbEntity.Status == (int)OvertimeStatusEnum.Pending)
                {

                    if (dbEntity.RequestedTimeInMin != null)
                        dbEntity.ApprovedTimeInMinute = dbEntity.RequestedTimeInMin.Value;
                    else
                        dbEntity.ApprovedTimeInMinute = GetMinute(dbEntity.StartTime.Value, dbEntity.EndTime.Value);
                    dbEntity.ApprovedTime = GetHourMin(dbEntity.ApprovedTimeInMinute.Value);
                    dbEntity.Status = (int)OvertimeStatusEnum.Recommended;
                    dbEntity.RecommendedBy = SessionManager.CurrentLoggedInEmployeeId;
                    dbEntity.RecommendedOn = GetCurrentDateAndTime();

                    if (LeaveRequestManager.CanApprove(dbEntity.EmployeeID.Value, SessionManager.CurrentLoggedInEmployeeId,PreDefindFlowType.Overtime))
                    {
                        dbEntity.ApprovedBy = SessionManager.CurrentLoggedInEmployeeId;
                        dbEntity.ApprovedOn = GetCurrentDateAndTime();
                        dbEntity.Status = (int)OvertimeStatusEnum.Approved;
                    }

                    count += 1;



                }
                else if (dbEntity != null && dbEntity.Status == (int)OvertimeStatusEnum.Recommended
                    && LeaveRequestManager.CanApprove(dbEntity.EmployeeID.Value, SessionManager.CurrentLoggedInEmployeeId,PreDefindFlowType.Overtime))
                {
                    if (dbEntity.RequestedTimeInMin != null)
                        dbEntity.ApprovedTimeInMinute = dbEntity.RequestedTimeInMin.Value;
                    else
                        dbEntity.ApprovedTimeInMinute = GetMinute(dbEntity.StartTime.Value, dbEntity.EndTime.Value);
                    dbEntity.ApprovedTime = GetHourMin(dbEntity.ApprovedTimeInMinute.Value);
                    dbEntity.Status = (int)OvertimeStatusEnum.Approved;
                    dbEntity.ApprovedBy = SessionManager.CurrentLoggedInEmployeeId;
                    dbEntity.ApprovedOn = GetCurrentDateAndTime();

                    count += 1;
                }
            }

            PayrollDataContext.SubmitChanges();
            return true;
        }
        public static bool RejectRequest(int requestID)
        {
            OvertimeRequest dbEntity = new OvertimeRequest();
            dbEntity = PayrollDataContext.OvertimeRequests.Where(x => x.OvertimeRequestID == requestID).SingleOrDefault();
            dbEntity.Status = (int)OvertimeStatusEnum.Rejected;
            
            dbEntity.RejectedBy = SessionManager.CurrentLoggedInEmployeeId;
            dbEntity.RejectedOn = GetCurrentDateAndTime();

            PayrollDataContext.SubmitChanges();
            return true;
        }

        //public static List<EEmployee> GetSupervisorList(int employeeID)
        //{


        //    List<EEmployee> dbInstance = (from p in PayrollDataContext.LeaveProjects
        //                                  join q in PayrollDataContext.LeaveProjectEmployees
        //                                  on p.LeaveProjectId equals q.LeaveProjectId
        //                                  join e in PayrollDataContext.EEmployees on p.ProjectManagerEmployeeId equals e.EmployeeId
        //                                  where q.EmployeeId == employeeID
        //                                  select e).ToList();

        //    return dbInstance;
        //}


        public static List<GetOvertimeRequestByIDResult> GetLeaveRequestsByEmpID(int p, int type, int currentPage, int pageSize, int status, int overtimeType, ref int total)
        {
            //List<GetOvertimeRequestByIDResult> dbEntity = new System.Collections.Generic.List<GetOvertimeRequestByIDResult>();
            
            //dbEntity= PayrollDataContext.GetOvertimeRequestByID(p, type).ToList();
            //bool isEnglish;
            //isEnglish = SessionManager.IsEnglish;

            //foreach (var val1 in dbEntity)
            //{
                
            //    val1.DurationModified = new Manager.AttendanceManager().MinuteToHourMinuteConverter(Math.Abs(val1.Duration.Value));
            //    val1.StatusModified = ((OvertimeStatusEnum)val1.Status.Value).ToString();
                
            //}
            //return dbEntity;

            


            string depIDList = SessionManager.CustomRoleDeparmentIDList;

            currentPage -= 1;

            List<GetOvertimeRequestByIDResult> list =
                PayrollDataContext.GetOvertimeRequestByID(currentPage, pageSize, SessionManager.CurrentLoggedInEmployeeId, type, status, overtimeType).ToList();

            foreach (var val1 in list)
            {
                //int minute = Math.Abs(val1.Duration.Value);
                //AttendanceManager atteManager = new AttendanceManager();
                //val1.DurationModified = atteManager.MinuteToHourMinuteConverter(minute);

                //val1.StatusModified = ((OvertimeStatusEnum)val1.Status.Value).ToString();

                val1.DurationModified = new Manager.AttendanceManager().MinuteToHourMinuteConverterWithOutLabel(Math.Abs(val1.Duration.Value));
                val1.StatusModified = ((OvertimeStatusEnum)val1.Status.Value).ToString();
            }

            total = 0;
            if (list.Count > 0)
                total = list[0].TotalRows.Value;

            return list;


        }



        public static List<GetOvertimeRequestForAdminResult> GetOvertimeListForAdmin(int type, int status
            , DateTime? start, DateTime? end, string searchText, int currentPage, int pageSize, ref int? total, 
            int BranchID,int otType,string sort,int? postedPeriodId)
        {

            if (searchText.ToLower() == "employee name")
                searchText = "";

            currentPage -= 1;
            total = 0;

            List<GetOvertimeRequestForAdminResult> dbEntity = new List<GetOvertimeRequestForAdminResult>();

            dbEntity = PayrollDataContext.GetOvertimeRequestForAdmin(type, status, start, end, searchText, currentPage, pageSize, BranchID, otType, sort, postedPeriodId).ToList();

            bool isSortingByName = sort == "EmployeeName ASC" || sort == "EmployeeName DESC" ||
                sort == "EmployeeId ASC" || sort == "EmployeeId DESC";

            int prevEIN = 0;
            //if (dbEntity.Count > 0)
            //    prevEIN = dbEntity[0].EmployeeID.Value;

            foreach (var val1 in dbEntity)
            {
                int minute = Math.Abs(val1.Duration.Value);
                AttendanceManager atteManager = new AttendanceManager();
                val1.DurationModified = atteManager.MinuteToHourMinuteConverterWithOutLabel(minute);

               
                val1.NepDate = GetAppropriateDate(val1.Date.Value);

                //if (isSortingByName)
                //{
                //    if (val1.EmployeeID.Value == prevEIN && prevEIN != 0)
                //        val1.EmployeeName = "";
                //    else
                //    {
                //        prevEIN = val1.EmployeeID.Value;
                //    }
                //}
            }

            if (dbEntity.Count > 0)
                total = dbEntity[0].TotalRows.Value;

            return dbEntity;
        }


        public static List<GetEveningCounterRequestForAdminResult> GetEveningCounterListForAdmin(int type, int status
            , DateTime? start, DateTime? end, string searchText, int currentPage, int pageSize,
            ref int? total, int BranchID, int otType, string sort, int? postedPeriodId)
        {

            if (searchText.ToLower() == "employee name")
                searchText = "";

            currentPage -= 1;
            total = 0;

            List<GetEveningCounterRequestForAdminResult> dbEntity = new List<GetEveningCounterRequestForAdminResult>();

            dbEntity = PayrollDataContext.GetEveningCounterRequestForAdmin(type, status, start, end, searchText, currentPage, pageSize, BranchID
                ,otType,sort,postedPeriodId).ToList();

            foreach (var val1 in dbEntity)
            {
                /*
                int minute = Math.Abs(val1.Duration.Value);
                AttendanceManager atteManager = new AttendanceManager();
                val1.DurationModified = atteManager.MinuteToHourMinuteConverter(minute);
                */
                //if (val1.Status != null)
                //    val1.StatusModified = ((EveningCounterStatusEnum)val1.Status.Value).ToString();
                if (val1.StartTime != null)
                    val1.StartDateNep = GetAppropriateDate(val1.StartTime.Value);
                if (val1.EndTime != null)
                    val1.EndDateNep = GetAppropriateDate(val1.EndTime.Value);
            }

            if (dbEntity.Count > 0)
                total = dbEntity[0].TotalRows.Value;

            return dbEntity;
        }


        public static List<GetOvertimeRequestForManagerResult> GetLeaveRequestsForManager(int type, int status, int currentPage, int pageSize, int overtimeTypeId, int branchId, DateTime? fromDate, DateTime? toDate, ref int total)
        {
            /*
            List<GetOvertimeRequestForManagerResult> dbEntity = new List<GetOvertimeRequestForManagerResult>();

            dbEntity = PayrollDataContext.GetOvertimeRequestForManager(SessionManager.CurrentLoggedInEmployeeId ,type, status).ToList();

            foreach (var val1 in dbEntity)
            {
                int minute = Math.Abs(val1.Duration.Value);
                AttendanceManager atteManager = new AttendanceManager();
                val1.DurationModified = atteManager.MinuteToHourMinuteConverter(minute);

                val1.StatusModified = ((OvertimeStatusEnum)val1.Status.Value).ToString();
            }
            return dbEntity;*/

            string depIDList = SessionManager.CustomRoleDeparmentIDList;

            currentPage -= 1;

            List<GetOvertimeRequestForManagerResult> list =
                PayrollDataContext.GetOvertimeRequestForManager(currentPage, pageSize, SessionManager.CurrentLoggedInEmployeeId, type, status, overtimeTypeId, branchId, fromDate, toDate).ToList();

            foreach (var val1 in list)
            {
                int minute = Math.Abs(val1.Duration.Value);
                AttendanceManager atteManager = new AttendanceManager();
                val1.DurationModified = atteManager.MinuteToHourMinuteConverterWithOutLabel(minute);

                val1.StatusModified = ((OvertimeStatusEnum)val1.Status.Value).ToString();
            }

            total = 0;
            if (list.Count > 0)
                total = list[0].TotalRows.Value;

            return list;

        }

        public static List<GetEveningCounterRequestForManagerResult> GetEveningCounterForManager(int type, int status, int currentPage, int pageSize,
            ref int total, DateTime? start, DateTime? end, string searchText,int branchId, int eveningCounterTypeId)
        {
          
            //List<GetEveningCounterRequestForManagerResult> dbEntity = new List<GetEveningCounterRequestForManagerResult>();
            //
            //dbEntity = PayrollDataContext.GetEveningCounterRequestForManager(SessionManager.CurrentLoggedInEmployeeId, type, status).ToList();

            //foreach (var val1 in dbEntity)
            //{
            //    if (val1.Status != null)
            //    {
            //        val1.StatusModified = ((EveningCounterStatusEnum)val1.Status.Value).ToString();
            //    }
            //}
            //return dbEntity;


            string depIDList = SessionManager.CustomRoleDeparmentIDList;

            currentPage -= 1;

            List<GetEveningCounterRequestForManagerResult> list =
                PayrollDataContext.GetEveningCounterRequestForManager(currentPage,
                pageSize, SessionManager.CurrentLoggedInEmployeeId, type, status, start, end, searchText, branchId, eveningCounterTypeId).ToList();            

            total = 0;
            if (list.Count > 0)
                total = list[0].TotalRows.Value;

            return list;




        }



        public static OvertimeRequest getRequestByID(string p)
        {
            int requestId = int.Parse(p);
            return PayrollDataContext.OvertimeRequests.Where(x => x.OvertimeRequestID == requestId).FirstOrDefault();
        }

        

        public static TimeSpan getOfficeEndTime(int employeeID)
        {
            GetEmployeeTimeResult time = 
                PayrollDataContext.GetEmployeeTime(employeeID, DateTime.Now.Date, 1, "", "").FirstOrDefault();

            if (time != null && time.OfficeOutTime != null)
                return time.OfficeOutTime.Value;


           return PayrollDataContext.AttendanceInOutTimes.FirstOrDefault().OfficeOutTime.Value;
        }

        public static Status InsertUpdateOvertimeExclusion(OvertimeExclusion exclusion)
        {
            Status status = new Status();
            if (PayrollDataContext.OvertimeExclusions.Any(x => x.LineID == exclusion.LineID))
            {
                OvertimeExclusion dbEntity = new OvertimeExclusion();

                dbEntity = PayrollDataContext.OvertimeExclusions.FirstOrDefault(x => x.LineID == exclusion.LineID);
                dbEntity.GroupID = exclusion.GroupID;
                dbEntity.GroupName = exclusion.GroupName;
                dbEntity.LevelID = exclusion.LevelID;
                dbEntity.LevelName = exclusion.LevelName;
                dbEntity.DesignationID = exclusion.DesignationID;
                dbEntity.DesignationName = exclusion.DesignationName;

                PayrollDataContext.SubmitChanges();
            }
            else
            {
                PayrollDataContext.OvertimeExclusions.InsertOnSubmit(exclusion);
                PayrollDataContext.SubmitChanges();
            }

            status.IsSuccess = true;
            return status;
        }

        public static List<OvertimeExclusion> getAllOvertimeExclusions()
        {
            return PayrollDataContext.OvertimeExclusions.ToList();
        }

        public static OvertimePeriodDetail GetOvertimePeriodDetailsForEmployeOfDate(int EmployeeID,DateTime Date)
        {
            return PayrollDataContext.OvertimePeriodDetails.SingleOrDefault(x => x.EmployeeId == EmployeeID && x.OTDate == Date);
        }


        public static OvertimeExclusion getExclusionByID(int LineID)
        {
            OvertimeExclusion exc  = new OvertimeExclusion();

            if (PayrollDataContext.OvertimeExclusions.Any(x => x.LineID == LineID))
            {
                exc = PayrollDataContext.OvertimeExclusions.FirstOrDefault(x => x.LineID == LineID);
            }

            return exc;
        }

        public static Status IsEmployeeEligibleToOvertime(EEmployee employee)
        {
            Status status = new Status();
            status.IsSuccess = false;
           // if(PayrollDataContext.OvertimeExclusions.Any(x=

            return status;
        }

        public static int GetEmployeeCurrentDesignation(int employeeId, DateTime dt)
        {
            return (int)PayrollDataContext.GetEmployeeCurrentPositionForDate(dt, employeeId);
        }

        public static bool CanEmployeeRequestForOverTime(int employeeId, int overtimeTypeId, DateTime dt)
        {

            List<OvertimeNotApplicableTo> list = PayrollDataContext.OvertimeNotApplicableTos.Where(x => x.OvertimeTypeId == overtimeTypeId).ToList();

            int designationId = GetEmployeeCurrentDesignation(employeeId, dt);

            int levelId = PayrollDataContext.EDesignations.SingleOrDefault(x => x.DesignationId == designationId).LevelId.Value;

            if (list.Count > 0 && list.Any(x => x.LevelId == levelId))
                return false;
            else
                return true;
        }

        public static int GetOvertimeMinimumBufferTime(int employeeId, DateTime dt)
        {
            int minimumTime = 0;

            int designationId = GetEmployeeCurrentDesignation(employeeId, dt);

            int levelId = PayrollDataContext.EDesignations.SingleOrDefault(x => x.DesignationId == designationId).LevelId.Value;

            OvertimeRounding objOR = getTopRounding();
            List<OvertimeRuleMinTimeBufferNotApplicableTo> list = objOR.OvertimeRuleMinTimeBufferNotApplicableTos.ToList();

            if (list.Count > 0 )
            {
                if(!list.Any(x => x.LevelId == levelId))
                    minimumTime = objOR.MinBufferTimeAfterRegularShift.Value;
            }

            return minimumTime;
        }

        public static Status SaveOvertimeRequestList(List<OvertimeRequest> list)
        {
            Status status = new Status();

            SetConnectionPwd(PayrollDataContext.Connection);
            PayrollDataContext.Transaction = PayrollDataContext.Connection.BeginTransaction();

            try
            {
                foreach (var item in list)
                {
                    OvertimeRequest dbObj = PayrollDataContext.OvertimeRequests.SingleOrDefault(x => x.EmployeeID == item.EmployeeID && x.Date.Value.Date == item.Date.Value.Date);
                    if (dbObj != null)
                    {
                        PayrollDataContext.Transaction.Rollback();
                        status.IsSuccess = false;
                        status.ErrorMessage = "Overtime request for date : " + item.Date.Value.ToShortDateString() + " is already saved.";
                        return status;
                    }


                    int rounding = PayrollDataContext.OvertimeRoundings.First().rounding.Value;
                    if (rounding > 0)
                    {
                        item.StartTime = OvertimeManager.RoundUp(item.StartTime.Value, TimeSpan.FromMinutes(rounding));
                        item.EndTime = OvertimeManager.RoundUp(item.EndTime.Value, TimeSpan.FromMinutes(rounding));
                    }

                    item.CreatedBy = SessionManager.User.UserID;
                    item.CreatedOn = GetCurrentDateAndTime();
                    PayrollDataContext.OvertimeRequests.InsertOnSubmit(item);

                    PayrollDataContext.SubmitChanges();

                }
                

                PayrollDataContext.Transaction.Commit();
                status.IsSuccess = true;
            }
            catch (Exception exp)
            {
                Log.log("Error while saving overtime requests", exp);
                PayrollDataContext.Transaction.Rollback();
                status.IsSuccess = false;
            }
            finally
            {

            }

            return status;
        }
        public static Status ApproveOvertimePeriodDetails(List<OvertimePeriodDetail> list)
        {
            Status status = new Status();
            try
            {
                foreach (var item in list)
                {
                    OvertimePeriodDetail dbEntity = PayrollDataContext.OvertimePeriodDetails.SingleOrDefault(x => x.EmployeeId == item.EmployeeId && x.OTDate == item.OTDate);

                    //if (dbEntity.Status != (int)TimeSheetStatus.AwaitingApproval)
                    //{
                    //    status.ErrorMessage = "Timesheet is in " + ((TimeSheetStatus)dbEntity.Status).ToString()
                    //        + " status, only Submitted status timesheet can be approved.";
                    //    return status;
                    //}

                    dbEntity.ApprovedMin = item.ApprovedMin;
                    dbEntity.ApprovedBy = SessionManager.CurrentLoggedInUserID;
                    dbEntity.ApprovedOn = BLL.BaseBiz.GetCurrentDateAndTime();
                    dbEntity.Status = (int)OvertimePeriodDetailStatus.Approved;
                }

                status.IsSuccess = true;
                PayrollDataContext.SubmitChanges();
                return status;
            }
            catch
            {
                status.ErrorMessage = "Error while Approve Overtime.";
                status.IsSuccess = false;
                return status;
            }
        }

        public static Status RevertOvertime(List<OvertimePeriodDetail> list)
        {
            Status status = new Status();
            try
            {
                foreach (var item in list)
                {
                    OvertimePeriodDetail dbEntity = PayrollDataContext.OvertimePeriodDetails.SingleOrDefault(x => x.EmployeeId == item.EmployeeId && x.OTDate == item.OTDate);

                    //if (dbEntity.Status != (int)TimeSheetStatus.AwaitingApproval)
                    //{
                    //    status.ErrorMessage = "Timesheet is in " + ((TimeSheetStatus)dbEntity.Status).ToString()
                    //        + " status, only Submitted status timesheet can be approved.";
                    //    return status;
                    //}

                   
                    dbEntity.ApprovedBy = SessionManager.CurrentLoggedInUserID;
                    dbEntity.ApprovedOn = BLL.BaseBiz.GetCurrentDateAndTime();
                    dbEntity.Status = (int)OvertimePeriodDetailStatus.Saved;
                }

                status.IsSuccess = true;
                PayrollDataContext.SubmitChanges();
                return status;
            }
            catch
            {
                status.ErrorMessage = "Error while Approve Overtime.";
                status.IsSuccess = false;
                return status;
            }
        }
        public static Status SaveUpdateLeaveFareDetails(List<LeaveFareBO> list, int period)
        {
            Status status = new Status();
            PayrollPeriod lastPeriod = CommonManager.GetLastPayrollPeriod();

            if (lastPeriod.PayrollPeriodId != period)
            {
                status.ErrorMessage = "Leave fare can be posted for current period only.";
                return status;
            }

            Setting setting = OvertimeManager.GetSetting();
            AddOn addOn = PayrollDataContext.AddOns.FirstOrDefault(x => x.PayrollPeriodId == period && x.AddOnType == (int)AddOnTypeEnum.LeaveFare);
            if (addOn == null)
            {
                status.ErrorMessage = "Leave fare add-on not defined in multi add-on for the period " + CommonManager.GetPayrollPeriod(period).Name + ".";
                return status;
            }
            if (setting.LeaveFareIncomeId == null || setting.LeaveFareIncomeId == -1)
            {
                status.ErrorMessage = "Leave fare income type not defined in Leave Fare setting.";
                return status;
            }

            if (PayrollDataContext.AddOnHeaders.Any(x => x.AddOnId == addOn.AddOnId && x.Type == 1 && x.SourceId == setting.LeaveFareIncomeId.Value) == false)
            {
                status.ErrorMessage = "Leave fare income not defined in Add-On.";
                return status;
            }

            CCalculation calculation = PayrollDataContext.CCalculations.Where(x => x.PayrollPeriodId == period).FirstOrDefault();
            List<CCalculationEmployee> salarySavedList = new System.Collections.Generic.List<CCalculationEmployee>();
            if (calculation != null)
                salarySavedList = PayrollDataContext.CCalculationEmployees.Where(x => x.CalculationId == calculation.CalculationId).ToList();

            List<LeaveFareEmployee> leaveFareList = new System.Collections.Generic.List<LeaveFareEmployee>();
            List<LeaveFareEmployee> dbLeaveFareList = PayrollDataContext.LeaveFareEmployees
                .Where(x => x.PayrollPeriodId == period).ToList();

            foreach (LeaveFareBO item in list)
            {
                if (item.PayrollPeriodId != lastPeriod.PayrollPeriodId)
                {
                    status.ErrorMessage = "Leave fare can be posted for current period only.";
                    return status;
                }

                if (salarySavedList.Any(x => x.EmployeeId == item.EmployeeId))
                {
                    status.ErrorMessage = CommonManager.GetPayrollPeriod(period).Name + " salary already saved for \"" + 
                        EmployeeManager.GetEmployeeName(item.EmployeeId) + "\".";
                    return status;
                }

                if (dbLeaveFareList.Any(x => x.LeaveRequestId == item.LeaveRequestId))
                    continue;
                // multiple entry in add on can not be done for same employee due to multiple leave reqquest
                if (leaveFareList.Any(x => x.EmployeeId == item.EmployeeId))
                    continue;
                if (item.Amount == null || item.Amount == 0)
                    continue;

                LeaveFareEmployee entity = new LeaveFareEmployee();
                

                leaveFareList.Add(entity);

                entity.EmployeeId = item.EmployeeId;
                entity.LeaveRequestId = item.LeaveRequestId;
                entity.BranchId = item.BranchId;
                entity.DepartmentId = item.DepartmentId;
                entity.DesignationId = item.DesignationId;
                entity.ServiceStatusId = item.ServiceStatusId;
                entity.TakenDays = item.TakenDays;
                entity.IsFullPayment = item.IsFull.ToLower().Trim() == "full";
                entity.StartDate = Convert.ToDateTime(item.StartDate);
                entity.EndDate = Convert.ToDateTime(item.EndDate);
                entity.PaidAmount = item.Amount == null ? 0 : item.Amount.Value;
                entity.FullAmount = item.FullAmount == null ? 0 : item.FullAmount.Value;
                entity.PayrollPeriodId = period;
                entity.CreatedBy = SessionManager.CurrentLoggedInUserID;
                entity.CreatedOn = GetCurrentDateAndTime();
            }

            DateTime addOnCreatedDate = GetCurrentDateAndTime();

            List<AddOnDetail> dbAddOnList =
                    (
                    from h in PayrollDataContext.AddOnHeaders
                    join d in PayrollDataContext.AddOnDetails on h.AddOnHeaderId equals d.AddOnHeaderId
                    where h.AddOnId == addOn.AddOnId && h.SourceId == setting.LeaveFareIncomeId.Value
                    select d
                    ).ToList();

            List<AddOnHeader> dbHeaderList = PayrollDataContext.AddOnHeaders.Where(x => x.AddOnId == addOn.AddOnId).ToList();

            List<ChangeMonetory> dbLogList = new List<ChangeMonetory>();

            foreach (LeaveFareEmployee tax in leaveFareList)
            {


                AddOnDetail dbDetail =
                    dbAddOnList.FirstOrDefault(x =>
                        x.AddOnHeader.Type == 1
                        && x.AddOnHeader.SourceId == setting.LeaveFareIncomeId.Value
                        && x.EmployeeId == tax.EmployeeId);


                if (dbDetail == null)
                {
                    AddOnHeader header = dbHeaderList.FirstOrDefault(x => x.AddOnId == addOn.AddOnId
                        && x.Type == 1 && x.SourceId == setting.LeaveFareIncomeId.Value);
                    if (header != null)
                    {
                        PayrollDataContext.AddOnDetails.InsertOnSubmit(
                            new AddOnDetail
                            {
                                Amount = tax.PaidAmount,
                                EmployeeId = tax.EmployeeId,
                                AddOnHeaderId = header.AddOnHeaderId,
                                CreatedOn = addOnCreatedDate
                            }
                            );

                        // Log                        
                        dbLogList.Add(GetMonetoryChangeLog(
                            tax.EmployeeId, (byte)MonetoryChangeLogTypeEnum.AddOn, lastPeriod.Name + " Leave Fare ", "", tax.PaidAmount, LogActionEnum.Add)
                                        );

                    }
                }
                else
                {
                    if (dbDetail.Amount != tax.PaidAmount)
                    {
                        dbLogList.Add(GetMonetoryChangeLog(
                            tax.EmployeeId, (byte)MonetoryChangeLogTypeEnum.AddOn, lastPeriod.Name + "  Leave Fare ", dbDetail.Amount, tax.PaidAmount, LogActionEnum.Update)
                                        );
                    }

                    if (dbDetail.Amount == 0)
                        dbDetail.CreatedOn = addOnCreatedDate;

                    dbDetail.Amount = tax.PaidAmount;
                }

            }

            PayrollDataContext.LeaveFareEmployees.InsertAllOnSubmit(leaveFareList);
            PayrollDataContext.ChangeMonetories.InsertAllOnSubmit(dbLogList);
            status.IsSuccess = true;
            PayrollDataContext.SubmitChanges();
            return status;

        }

        public static Status SaveUpdateHolidayAdditionalPay(List<GetHolidayAdditionalPayListResult> list, int period)
        {
            Status status = new Status();
            PayrollPeriod lastPeriod = CommonManager.GetLastPayrollPeriod();

            if (lastPeriod.PayrollPeriodId != period)
            {
                status.ErrorMessage = "Pay can be posted for current period only.";
                return status;
            }

           

            CCalculation calculation = PayrollDataContext.CCalculations.Where(x => x.PayrollPeriodId == period).FirstOrDefault();
            List<CCalculationEmployee> salarySavedList = new System.Collections.Generic.List<CCalculationEmployee>();
            if (calculation != null)
                salarySavedList = PayrollDataContext.CCalculationEmployees.Where(x => x.CalculationId == calculation.CalculationId).ToList();

            List<HolidayAdditionalPay> leaveFareList = new System.Collections.Generic.List<HolidayAdditionalPay>();
            List<HolidayAdditionalPay> dbLeaveFareList = PayrollDataContext.HolidayAdditionalPays
                .Where(x => x.PayrollPeriodId == period).ToList();

            foreach (GetHolidayAdditionalPayListResult item in list)
            {
                
                if (salarySavedList.Any(x => x.EmployeeId == item.EmployeeId))
                {
                    status.ErrorMessage = CommonManager.GetPayrollPeriod(period).Name + " salary already saved for \"" +
                        EmployeeManager.GetEmployeeName(item.EmployeeId) + "\".";
                    return status;
                }




                HolidayAdditionalPay entity = new HolidayAdditionalPay();


                leaveFareList.Add(entity);

                entity.EmployeeId = item.EmployeeId;
                entity.DateEng = item.DateEng;
                entity.DayValueText = item.DayValueText;
                entity.BranchId = item.BranchId;
                entity.DepartmentId = item.DepartmentId;
                entity.DesignationId = item.DesignationId;
                entity.ServiceStatusId = (int)item.ServiceStatusId;
                entity.ClockIn = item.ClockIn.Value;
                entity.ClockOut = item.ClockOut.Value;
                entity.AdditionalSalaryType = item.SalaryType;
                entity.PayrollPeriodId = period;
                entity.CreatedBy = SessionManager.CurrentLoggedInUserID;
                entity.CreatedOn = GetCurrentDateAndTime();
                entity.BasicAmount = item.BasicAmount;

                //recalculate amount as full/half day may have been changed
                entity.Amount = item.BasicAmount.Value * (decimal)(12.0 / 365.0) * 
                            (decimal)(item.SalaryType == "Full Pay" ? 1 : 0.5);
            }

            PayrollDataContext.HolidayAdditionalPays.InsertAllOnSubmit(leaveFareList);

            status.IsSuccess = true;
            PayrollDataContext.SubmitChanges();
            return status;

        }

        public static Status PostHolidayAdditionalPayToSalary(int period)
        {
            Status status = new Status();
            PayrollPeriod lastPeriod = CommonManager.GetLastPayrollPeriod();

            if (lastPeriod.PayrollPeriodId != period)
            {
                status.ErrorMessage = "Pay can be posted for current period only.";
                return status;
            }


            Setting setting = PayrollDataContext.Settings.FirstOrDefault();
            int? overtimeIncomeId = null;

            if (setting != null)
                overtimeIncomeId = setting.OvertimeIncomeId;

            if (overtimeIncomeId == null || overtimeIncomeId == 0 || overtimeIncomeId == -1)
            {
                status.ErrorMessage = "Overtime income not selected in Adminstration tools -> Income / Pay setting.";
                return status;
            }

            List<HolidayAdditionalPay> dbList = PayrollDataContext.HolidayAdditionalPays
                .Where(x => x.PayrollPeriodId == period).ToList();


            List<int> einList = dbList.Select(x => x.EmployeeId).Distinct().ToList();

            foreach (var ein in einList)
            {

                if (PayrollDataContext.PEmployeeIncomes.Any(x => x.EmployeeId == ein && x.IncomeId == overtimeIncomeId.Value) == false)
                {
                    // prevent double insertion in this mode
                    PEmployeeIncome overtimeInc = new PEmployeeIncome();
                    overtimeInc.EmployeeId = ein;
                    overtimeInc.IncomeId = overtimeIncomeId.Value;
                    overtimeInc.IsValid = true;
                    overtimeInc.IsEnabled = true;

                    PayrollDataContext.PEmployeeIncomes.InsertOnSubmit(overtimeInc);
                    PayrollDataContext.SubmitChanges();

                }

                // process to add
                decimal totalAmount = dbList.Where(x=>x.EmployeeId == ein).Sum(x => Convert.ToDecimal(x.Amount));

                PEmployeeIncome dbIncome = PayrollDataContext.PEmployeeIncomes.FirstOrDefault(x =>
                    x.EmployeeId == ein && x.IncomeId == overtimeIncomeId.Value);

                dbIncome.Amount = totalAmount;
            }

            status.IsSuccess = true;
            PayrollDataContext.SubmitChanges();
            return status;

        }

        public static bool IsOvertimePeriodEditable(OvertimePeriod period)
        {
            if (period.Status == -1 && CommonManager.GetLastPayrollPeriod().PayrollPeriodId == period.PaidPayrollPeriodId)
                return true;

            return false;
        }

        public static Status RejectAllSavedOvertimePeriodDetails()
        {
            Status status = new Status();

            OvertimePeriod period = PayrollDataContext.OvertimePeriods.OrderByDescending(x => x.EndDate).FirstOrDefault();

            if (IsOvertimePeriodEditable(period) == false || period == null )
                return status;

            int empId= SessionManager.CurrentLoggedInEmployeeId;

            List<OvertimePeriodDetail> list =
                (
                    from o in PayrollDataContext.OvertimePeriodDetails
                    join lpe in PayrollDataContext.LeaveProjectEmployees on o.EmployeeId equals lpe.EmployeeId
                    join la in PayrollDataContext.LeaveApprovalEmployees on lpe.LeaveProjectId equals la.LeaveProjectId
                    where (la.ApplyTo == empId || la.CC1 == empId || la.CC2 == empId
                            || la.CC3 == empId || la.CC4 == empId || la.CC5 == empId)
                        && o.Status == (int)OvertimePeriodDetailStatus.Saved
                        && o.OTDate >= period.StartDate && o.OTDate <= period.EndDate
                    select o
                ).ToList();

                //PayrollDataContext.OvertimePeriodDetails.Where(x => x.Status == (int)OvertimePeriodDetailStatus.Saved).ToList();
            
            foreach (var dbEntity in list)
            {
                //OvertimePeriodDetail dbEntity = PayrollDataContext.OvertimePeriodDetails.SingleOrDefault(x => x.EmployeeId == item.EmployeeId && x.OTDate == item.OTDate);
                dbEntity.Status = (int)OvertimePeriodDetailStatus.Rejected;
            }
            status.IsSuccess = true;
            PayrollDataContext.SubmitChanges();
            return status;

        }

        public static Status RejectSelectedOvertimePeriodDetails(List<OvertimePeriodDetail> list)
        {
            Status status = new Status();
            try
            {
                foreach (var item in list)
                {
                    OvertimePeriodDetail dbEntity = PayrollDataContext.OvertimePeriodDetails.SingleOrDefault(x => x.EmployeeId == item.EmployeeId && x.OTDate == item.OTDate);
                    dbEntity.Status = (int)OvertimePeriodDetailStatus.Rejected;
                }
                status.IsSuccess = true;
                PayrollDataContext.SubmitChanges();
                return status;
            }
            catch
            {
                status.ErrorMessage = "Error while Approve Overtime.";
                status.IsSuccess = false;
                return status;
            }
        }

        public static Status ApproveOvertimePeriodByLine(OvertimePeriodDetail _dbOvertimePeriodDetails)
        {
            Status status = new Status();
            try
            {
               
                _dbOvertimePeriodDetails.ApprovedBy = SessionManager.CurrentLoggedInUserID;
                _dbOvertimePeriodDetails.ApprovedOn = BLL.BaseBiz.GetCurrentDateAndTime();
                _dbOvertimePeriodDetails.Status = (int)OvertimePeriodDetailStatus.Approved;
                status.IsSuccess = true;
                PayrollDataContext.SubmitChanges();
                return status;
            }
            catch
            {
                status.ErrorMessage = "Error while Approve Overtime.";
                status.IsSuccess = false;
                return status;
            }

        }

        public static Status RejectOvertimePeriodByLine(OvertimePeriodDetail _dbOvertimePeriodDetails)
        {
            Status status = new Status();
            try
            {

                //_dbOvertimePeriodDetails.ApprovedBy = SessionManager.CurrentLoggedInUserID;
                //_dbOvertimePeriodDetails.ApprovedOn = BLL.BaseBiz.GetCurrentDateAndTime();
                _dbOvertimePeriodDetails.Status = (int)OvertimePeriodDetailStatus.Rejected;
                status.IsSuccess = true;
                PayrollDataContext.SubmitChanges();
                return status;
            }
            catch
            {
                status.ErrorMessage = "Error while Reject Overtime.";
                status.IsSuccess = false;
                return status;
            }

        }
        public static Status RevertToSaved(OvertimePeriodDetail _dbOvertimePeriodDetails)
        {
            Status status = new Status();
            try
            {

                _dbOvertimePeriodDetails.ApprovedBy = SessionManager.CurrentLoggedInUserID;
                _dbOvertimePeriodDetails.ApprovedOn = BLL.BaseBiz.GetCurrentDateAndTime();
                _dbOvertimePeriodDetails.Status = (int)OvertimePeriodDetailStatus.Saved;
                status.IsSuccess = true;
                PayrollDataContext.SubmitChanges();
                return status;
            }
            catch
            {
                status.ErrorMessage = "Error while Reject Overtime.";
                status.IsSuccess = false;
                return status;
            }

        }
        public static Status AssignOvertimes(List<OvertimeRequest> list)
        {
            Status status = new Status();

            SetConnectionPwd(PayrollDataContext.Connection);
            PayrollDataContext.Transaction = PayrollDataContext.Connection.BeginTransaction();

            try
            {
                foreach (var item in list)
                {
                    OvertimeRequest dbObj = PayrollDataContext.OvertimeRequests.SingleOrDefault(x => x.EmployeeID == item.EmployeeID && x.Date.Value.Date == item.Date.Value.Date);
                    if (dbObj != null)
                    {
                        PayrollDataContext.Transaction.Rollback();
                        status.IsSuccess = false;
                        status.ErrorMessage = "Overtime request for date : " + item.Date.Value.ToShortDateString() + " is already saved.";
                        return status;
                    }

                    int rounding = PayrollDataContext.OvertimeRoundings.First().rounding.Value;
                    if (rounding > 0)
                    {
                        item.StartTime = OvertimeManager.RoundUp(item.StartTime.Value, TimeSpan.FromMinutes(rounding));
                        item.EndTime = OvertimeManager.RoundUp(item.EndTime.Value, TimeSpan.FromMinutes(rounding));
                    }

                    if (item.Status == (int)OvertimeStatusEnum.Recommended)
                    {
                        item.RecommendedBy = SessionManager.CurrentLoggedInEmployeeId;
                        item.RecommendedOn = GetCurrentDateAndTime();
                    }
                    else
                    {
                        item.ApprovedBy = SessionManager.CurrentLoggedInEmployeeId;
                        item.ApprovedOn = GetCurrentDateAndTime();

                        string min = (item.StartTime.Value.Subtract(item.EndTime.Value).TotalMinutes).ToString();
                        int minute = Math.Abs(int.Parse(min));
                        string hrmin = new AttendanceManager().MinuteToHourMinuteConverter(minute);
                        string hr;
                        //txtRequested.Text = hrmin;
                        hr = hrmin.Split(':').First();
                        min = hrmin.Split(':').Last();

                        item.ApprovedTimeInMinute = minute;
                        item.ApprovedTime = hr + min.Trim();
                    }


                    item.CreatedBy = SessionManager.User.UserID;
                    item.CreatedOn = GetCurrentDateAndTime();
                    
                    PayrollDataContext.OvertimeRequests.InsertOnSubmit(item);

                    PayrollDataContext.SubmitChanges();

                }


                PayrollDataContext.Transaction.Commit();
                status.IsSuccess = true;
            }
            catch (Exception exp)
            {
                Log.log("Error while assigning overtime requests", exp);
                PayrollDataContext.Transaction.Rollback();
                status.IsSuccess = false;
            }
            finally
            {

            }

            return status;
        }

        public static List<GetOvertimeRequestForManagerNewResult> GetLeaveRequestsForManagerNew(int type, int status, int currentPage, int pageSize, ref int total)
        {
            string depIDList = SessionManager.CustomRoleDeparmentIDList;

            currentPage -= 1;

            List<GetOvertimeRequestForManagerNewResult> list =
                PayrollDataContext.GetOvertimeRequestForManagerNew(currentPage, pageSize, SessionManager.CurrentLoggedInEmployeeId, type, status).ToList();

            foreach (var val1 in list)
            {
                int minute = Math.Abs(val1.Duration.Value);
                AttendanceManager atteManager = new AttendanceManager();
                val1.DurationModified = atteManager.MinuteToHourMinuteConverter(minute);

                val1.StatusModified = ((OvertimeStatusEnum)val1.Status.Value).ToString();
            }

            total = 0;
            if (list.Count > 0)
                total = list[0].TotalRows.Value;

            return list;

        }

        public static bool UpdateHideTeamHRDetails(bool enable,bool joiningDateEditable,bool einEditable
            ,bool showAppraisalDashboard)
        {
            Setting setting = GetSetting();
            setting.HideTeamHRDetails = enable;
            setting.AllowToChangeJoiningDate = joiningDateEditable; ;
            setting.IsEINEditable = einEditable;
            setting.ShowAppraisalDashboard = showAppraisalDashboard;

            CommonManager.Setting.HideTeamHRDetails = enable;
            CommonManager.Setting.AllowToChangeJoiningDate = joiningDateEditable;
            CommonManager.Setting.IsEINEditable = einEditable;
            CommonManager.Setting.ShowAppraisalDashboard = showAppraisalDashboard;

            PayrollDataContext.SubmitChanges();
            return true;
        }

        public static Status DeleteOvertimeRequest(int overtimeRequestId)
        {
            Status status = new Status();

            OvertimeRequest dbObj = PayrollDataContext.OvertimeRequests.SingleOrDefault(x => x.OvertimeRequestID == overtimeRequestId);
            if (dbObj != null)
            {
                PayrollDataContext.OvertimeRequests.DeleteOnSubmit(dbObj);
                PayrollDataContext.SubmitChanges();
                status.IsSuccess = true;
            }
            else
            {
                status.ErrorMessage = "Error while deleting.";
                status.IsSuccess = false;
            }

            return status;
        }
    }


    public class OverTimeClsWithEmp
    {
        public int SN { get; set; }
        public string Text { get; set; }
        public string Value { get; set; }
        public DateTime DateEng { get; set; }
        public DateTime InTime { get; set; }
        public DateTime OutTime { get; set; }
        public string WorkHours { get; set; }
        public string OutNote { get; set; }
        public DateTime DATETIMEIN { get; set; }
        public DateTime DATETIMEOUT { get; set; }
        public int EmployeeId { get; set; }
        public string Name { get; set; }
    }
}
