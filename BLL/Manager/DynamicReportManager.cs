﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BLL.BO;
//using System.Transactions;
using DAL;
using Utils;
using Ext.Net;
using System.Text.RegularExpressions;
using Utils.Calendar;
using System.Reflection;
using System.Web;

namespace BLL.Manager
{
    public class DynamicReportManager : BaseBiz
    {
        private static DynamicReportManager m_DynamicReportManager = null;
        public static DynamicReportManager Instance
        {
            get
            {
                if (m_DynamicReportManager == null)
                    m_DynamicReportManager = new DynamicReportManager();

                return m_DynamicReportManager;

            }
        }





        public static List<DynamicReportFieldsBO> GetDynamicFieldsProperty()
        {
           // List<testdynamicTable> _testdynamicTable = DynamicReportManager.GetDynamicColumns();

            List<GetEmployeeMasterOtherHeadersResult> _testdynamicTable = EmployeeManager.GetEmployeeMasterHeader();

            List<DynamicReportFieldsBO> _listFields = new List<DynamicReportFieldsBO>();
            foreach (GetEmployeeMasterOtherHeadersResult _item in _testdynamicTable)
            {
                DynamicReportFieldsBO _DynamicReportFieldsBO = new DynamicReportFieldsBO();
                _DynamicReportFieldsBO.FieldName = _item.HeaderName;
                _DynamicReportFieldsBO.ServerMappingFields = _item.HeaderName;//_item.HeaderName.Replace(" ", "");
                _DynamicReportFieldsBO.DataType = "string";

                _listFields.Add(_DynamicReportFieldsBO);
            }
            return _listFields;
        }

        public static List<DynamicReportFieldsBO> GetDynamicReportAvilableFields(object obj, List<string> ColumnsToHide)
        {

            List<DynamicReportFieldsBO> _listFields = new List<DynamicReportFieldsBO>();
            PropertyInfo[] properties = obj.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance);

            foreach (PropertyInfo p in properties)
            {
                bool HideColumn = false;
                DynamicReportFieldsBO _DynamicReportFields = new DynamicReportFieldsBO();

                foreach (object _hiddenCols in ColumnsToHide)
                {
                    if (p.Name == NewMessage.GetTextValue(_hiddenCols.ToString()))
                        HideColumn = true;
                }
                if (HideColumn == false)
                {
                    _DynamicReportFields.ServerMappingFields = p.Name;
                    _DynamicReportFields.FieldName = NewMessage.GetTextValue(p.Name);//HttpContext.GetGlobalResourceObject("ResourceEnums", p.Name).ToString();//BLL.BaseBiz.GetEnumRes(p.Name);
                    string Type = string.Empty;
                    if (p.PropertyType.IsGenericType && p.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>))
                    {

                        Type = p.PropertyType.GetGenericArguments()[0].UnderlyingSystemType.Name.ToLower();

                    }
                    else
                        Type = p.PropertyType.Name.ToLower();

                    if (Type == "int32")
                        _DynamicReportFields.DataType = "float";
                    else if (Type == "decimal")
                        _DynamicReportFields.DataType = "float";

                    else if (Type == "datetime")
                        _DynamicReportFields.DataType = "date";

                    else
                        _DynamicReportFields.DataType = "string";

                    _listFields.Add(_DynamicReportFields);
                }
                
            }

            return _listFields;

        }
      
    }
}
