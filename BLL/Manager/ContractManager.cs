﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using DAL;
using Utils;
using System.Xml.Linq;
using Utils.Calendar;
using BLL.Entity;
using System.Web;
using System.IO;
using System.Collections;
using BLL.BO;
using Utils.Security;

namespace BLL.Manager
{
    public class DocumentManager : BaseBiz
    {
        public static Status SaveUpdateDocCategory(DocCategory obj, bool isSave)
        {
            Status status = new Status();

            if (PayrollDataContext.DocCategories.Any(x => x.Name.ToLower() == obj.Name.ToLower() && x.CategoryID != obj.CategoryID))
            {
                status.ErrorMessage = "Category already exists.";
                status.IsSuccess = false;
                return status;
            }

            if (isSave)
            {
                PayrollDataContext.DocCategories.InsertOnSubmit(obj);
                PayrollDataContext.SubmitChanges();
                status.IsSuccess = true;
            }
            else
            {
                DocCategory dbEntity = PayrollDataContext.DocCategories.SingleOrDefault(x => x.CategoryID == obj.CategoryID);
                dbEntity.Name = obj.Name;
                dbEntity.Code = obj.Code;
                PayrollDataContext.SubmitChanges();
                status.IsSuccess = true;
            }

            return status;
        }

        public static List<DocCategory> GetDocCategories()
        {
            return PayrollDataContext.DocCategories.OrderBy(x => x.Name).ToList();
        }

        public static Status DeleteDocCategories(Guid categoryId)
        {
            Status status = new Status();

            if (PayrollDataContext.DocDocuments.Any(x => x.CategoryID == categoryId) || PayrollDataContext.DocSubCategories.Any(x => x.CategoryID == categoryId))
            {
                status.ErrorMessage = "Category is in use, cannot be deleted.";
                status.IsSuccess = false;
                return status;
            }

            DocCategory dbObjDocCategory = PayrollDataContext.DocCategories.SingleOrDefault(x => x.CategoryID == categoryId);
            if (dbObjDocCategory == null)
            {
                status.ErrorMessage = "Category is not found.";
                status.IsSuccess = false;
                return status;
            }

            PayrollDataContext.DocCategories.DeleteOnSubmit(dbObjDocCategory);
            PayrollDataContext.SubmitChanges();
            status.IsSuccess = true;

            return status;
        }

        public static DocCategory GetDocCategoryById(Guid categoryId)
        {
            return PayrollDataContext.DocCategories.SingleOrDefault(x => x.CategoryID == categoryId);
        }

        public static Status SaveUpdateDocSubCategory(DocSubCategory obj, bool isSave)
        {
            Status status = new Status();

            if (PayrollDataContext.DocSubCategories.Any(x => x.Name.ToLower() == obj.Name.ToLower() && x.SubCategoryID != obj.SubCategoryID))
            {
                status.ErrorMessage = "Sub-Category already exists.";
                status.IsSuccess = false;
                return status;
            }

            if (isSave)
            {
                PayrollDataContext.DocSubCategories.InsertOnSubmit(obj);
                PayrollDataContext.SubmitChanges();
                status.IsSuccess = true;
            }
            else
            {
                DocSubCategory dbEntity = PayrollDataContext.DocSubCategories.SingleOrDefault(x => x.SubCategoryID == obj.SubCategoryID);
                dbEntity.Name = obj.Name;
                dbEntity.Code = obj.Code;
                dbEntity.CategoryID = obj.CategoryID;
                PayrollDataContext.SubmitChanges();
                status.IsSuccess = true;
            }

            return status;
        }

        public static List<DocSubCategory> GetDocSubCategories()
        {
            List<DocSubCategory> list = PayrollDataContext.DocSubCategories.OrderBy(x => x.Name).ToList();
            foreach (DocSubCategory item in list)
                item.CategoryName = GetDocCategoryById(item.CategoryID.Value).Name;

            return list;
        }

        public static Status DeleteDocSubCategories(Guid subCategoryId)
        {
            Status status = new Status();

            if (PayrollDataContext.DocDocuments.Any(x => x.SubCategoryID == subCategoryId))
            {
                status.ErrorMessage = "Sub-Category is in use, cannot be deleted.";
                status.IsSuccess = false;
                return status;
            }

            DocSubCategory dbObjDocSubCategory = PayrollDataContext.DocSubCategories.SingleOrDefault(x => x.SubCategoryID == subCategoryId);
            if (dbObjDocSubCategory == null)
            {
                status.ErrorMessage = "Sub-Category is not found.";
                status.IsSuccess = false;
                return status;
            }

            PayrollDataContext.DocSubCategories.DeleteOnSubmit(dbObjDocSubCategory);
            PayrollDataContext.SubmitChanges();
            status.IsSuccess = true;

            return status;
        }

        public static DocSubCategory GetDocSubCategoryById(Guid subCategoryId)
        {
            return PayrollDataContext.DocSubCategories.SingleOrDefault(x => x.SubCategoryID == subCategoryId);
        }

        public static List<DocParty> GetDocPartySearch(string prefix)
        {
            return  PayrollDataContext.DocParties.Where(x => x.Name.ToLower().Contains(prefix.ToLower())).OrderBy(x => x.Name).ToList();
        }

        public static List<DocSubCategory> GetDocSubCategoryByCategoryId(Guid categoryId)
        {
            return PayrollDataContext.DocSubCategories.Where(x => x.CategoryID == categoryId).OrderBy(x => x.Name).ToList();
        }

        public static List<TextValue> GetUUserSearch(string prefix)
        {
            List<TextValue> list = (from u in PayrollDataContext.UUsers
                                    where u.UserName.ToLower().Contains(prefix.ToLower())
                                    select new TextValue()
                                    {
                                        Text = u.UserName,
                                        Value = u.UserID.ToString()
                                    }).ToList();

            return list;
        }

        public static DocAttachment GetDocAttacmentById(Guid FileID)
        {
            return PayrollDataContext.DocAttachments.SingleOrDefault(x => x.FileID == FileID);
        }

        public static Status DeleteDocAttachment(Guid fileID)
        {
            Status status = new Status();

            DocAttachment dbObjDocAttachment = PayrollDataContext.DocAttachments.SingleOrDefault(x => x.FileID == fileID);
            if (dbObjDocAttachment == null)
            {
                status.IsSuccess = false;
                status.ErrorMessage = "Attachment not found.";
                return status;
            }

            PayrollDataContext.DocAttachments.DeleteOnSubmit(dbObjDocAttachment);
            PayrollDataContext.SubmitChanges();
            status.IsSuccess = true;
            return status;
        }



        public static Status InsertUpdateDealer(DocPartyContact _dataDocPartyContact, List<DocContactAttachment> _dataDocContactAttachment,
           List<DocContactAgreementLine> _dataDocContactAgreementLine,bool IsInsert)
        {
            Status myStatus = new Status();

            //DataContext.Connection.Open();
            //DataContext.Transaction = DataContext.Connection.BeginTransaction();
            //try
            //{
                _dataDocPartyContact.ModifiedBy = SessionManager.CurrentLoggedInUserID;
                _dataDocPartyContact.ModifiedOn = GetCurrentDateAndTime();

                if (IsInsert)
                {

                    _dataDocPartyContact.CreatedBy = _dataDocPartyContact.ModifiedBy.Value;
                    _dataDocPartyContact.CreatedOn = _dataDocPartyContact.ModifiedOn.Value;

                    if (_dataDocContactAttachment != null)
                        _dataDocPartyContact.DocContactAttachments.AddRange(_dataDocContactAttachment);

                    if (_dataDocContactAgreementLine != null)
                        _dataDocPartyContact.DocContactAgreementLines.AddRange(_dataDocContactAgreementLine);

                    PayrollDataContext.DocPartyContacts.InsertOnSubmit(_dataDocPartyContact);
                }
                else
                {
                    DocPartyContact dbRecord = PayrollDataContext.DocPartyContacts.SingleOrDefault(c => c.ContactId == _dataDocPartyContact.ContactId);

                    if (dbRecord == null)
                    {
                        myStatus.ErrorMessage = "Data not found";
                        myStatus.IsSuccess = false;
                        return myStatus;
                    }
                    else
                    {

                        CopyObject<DocPartyContact>(_dataDocPartyContact, ref dbRecord, "ContactId", "CreatedOn", "CreatedBy");
                    }

                    List<DocContactAttachment> DocContactAttachmentList = PayrollDataContext.DocContactAttachments.Where(c => c.ContactRef_ID == _dataDocPartyContact.ContactId).ToList();

                    List<DocContactAgreementLine> DocContactAgreementLinesList = PayrollDataContext.DocContactAgreementLines.Where(c => c.ContactRef_Id == _dataDocPartyContact.ContactId).ToList();


                    PayrollDataContext.DocContactAttachments.DeleteAllOnSubmit(DocContactAttachmentList);
                    PayrollDataContext.DocContactAgreementLines.DeleteAllOnSubmit(DocContactAgreementLinesList);
                   

                    PayrollDataContext.DocContactAttachments.InsertAllOnSubmit(_dataDocContactAttachment);
                    PayrollDataContext.DocContactAgreementLines.InsertAllOnSubmit(_dataDocContactAgreementLine);
                }

                PayrollDataContext.SubmitChanges();
               // PayrollDataContext.Transaction.Commit();
                return myStatus;
            

            //catch (Exception exp)
            //{
            //    Log.log("error while saving record", exp);
            //    myStatus.ErrorMessage = exp.Message;
            //    DataContext.Transaction.Rollback();
            //    myStatus.IsSuccess = false;
            //}
            //finally
            //{
            //    DataContext.Connection.Close();
            //}
            //return myStatus;

        }

        public static Status SaveUpdateDocument(DocDocument objDocDocument, DocParty objDocParty, List<DocAssign> listDocAssign, List<DocAttachment> listDocAttachment, List<DocAdditionalInformationDetail> listAdditionalInformationDetl, bool isSave)
        {
            Status status = new Status();

            int i = 0;
            string categoryCode = GetDocCategoryById(objDocDocument.CategoryID).Code;
            string subCategoryCode = GetDocSubCategoryById(objDocDocument.SubCategoryID).Code;
            Setting dbObjSetting = PayrollDataContext.Settings.FirstOrDefault();
            string documentDateYear = objDocDocument.DocumentDate.Substring(2, 2);

            if (dbObjSetting.DocDocumentId != null)
                i = dbObjSetting.DocDocumentId.Value;

            if (isSave)
            {
                objDocDocument.CreatedBy = SessionManager.CurrentLoggedInUserID;
                objDocDocument.CreatedOn = GetCurrentDateAndTime();
                objDocDocument.CreatedOnDate = CustomDate.GetTodayDate(false).ToString();

                i++;
                dbObjSetting.DocDocumentId = i;
                objDocDocument.ID = string.Format("{0}{1}{2}-{3}", (string.IsNullOrEmpty(categoryCode) ? "" : categoryCode + "-"), (string.IsNullOrEmpty(subCategoryCode) ? "" : subCategoryCode + "-"), documentDateYear, i.ToString().PadLeft(3, '0'));

                PayrollDataContext.DocDocuments.InsertOnSubmit(objDocDocument);

                if (!PayrollDataContext.DocParties.Any(x => x.PartyID == objDocParty.PartyID))
                {
                    PayrollDataContext.DocParties.InsertOnSubmit(objDocParty);
                }

                PayrollDataContext.DocAssigns.InsertAllOnSubmit(listDocAssign);

                if(listDocAttachment.Count >0)
                    PayrollDataContext.DocAttachments.InsertAllOnSubmit(listDocAttachment);

                DocDocumentHistory objDocDocumentHistory = new DocDocumentHistory();
                objDocDocumentHistory.HistoryID = Guid.NewGuid();
                objDocDocumentHistory.UserID = objDocDocument.CreatedBy;
                objDocDocumentHistory.Action = "Created";
                objDocDocumentHistory.ActionDate = objDocDocument.CreatedOn;
                objDocDocumentHistory.DocumentRef_ID = objDocDocument.DocumentID;
                PayrollDataContext.DocDocumentHistories.InsertOnSubmit(objDocDocumentHistory);

                if (listAdditionalInformationDetl.Count > 0)
                {
                    listAdditionalInformationDetl.ForEach(x => x.DocumentRef_ID = objDocDocument.DocumentID);
                    PayrollDataContext.DocAdditionalInformationDetails.InsertAllOnSubmit(listAdditionalInformationDetl);
                }

                PayrollDataContext.SubmitChanges();
                status.IsSuccess = true;
            }
            else
            {
                DocDocument dbObj = PayrollDataContext.DocDocuments.SingleOrDefault(x => x.DocumentID == objDocDocument.DocumentID);
                status.IsSuccess = false;

                if (dbObj != null)
                {
                    dbObj.Title = objDocDocument.Title;
                    dbObj.PartyRef_ID = objDocDocument.PartyRef_ID;
                    objDocDocument.ID = string.Format("{0}-{1}-{2}-{3}", (string.IsNullOrEmpty(categoryCode) ? "" : categoryCode + "-"), (string.IsNullOrEmpty(subCategoryCode) ? "" : subCategoryCode + "-"), documentDateYear, i.ToString().PadLeft(3, '0'));

                    dbObj.CategoryID = objDocDocument.CategoryID;
                    dbObj.SubCategoryID = objDocDocument.SubCategoryID;
                    dbObj.DocumentDate = objDocDocument.DocumentDate;
                    dbObj.DocumentDateEng = objDocDocument.DocumentDateEng;
                    dbObj.HasExpiry = objDocDocument.HasExpiry;

                    dbObj.ExpiryDate = objDocDocument.ExpiryDate;
                    dbObj.ExpiryDateEng = objDocDocument.ExpiryDateEng;
                    dbObj.Description = objDocDocument.Description;
                    dbObj.IsPrivate = objDocDocument.IsPrivate;
                    dbObj.ModifiedBy = SessionManager.CurrentLoggedInUserID;
                    dbObj.ModifiedOn = GetCurrentDateAndTime();

                    List<DocAssign> dbListDocAssign = PayrollDataContext.DocAssigns.Where(x => x.DocumentRef_ID == objDocDocument.DocumentID).ToList();
                    PayrollDataContext.DocAssigns.DeleteAllOnSubmit(dbListDocAssign);
                    PayrollDataContext.DocAssigns.InsertAllOnSubmit(listDocAssign);

                    List<DocAttachment> dbListDocAttachment = PayrollDataContext.DocAttachments.Where(x => x.DocumentRef_ID == objDocDocument.DocumentID).ToList();
                    if (dbListDocAttachment.Count > 0)
                    {
                        PayrollDataContext.DocAttachments.DeleteAllOnSubmit(dbListDocAttachment);

                        string path = "";
                        foreach (var item in dbListDocAttachment)
                        {
                            path = HttpContext.Current.Server.MapPath(@"~/uploads/Document" + item.ServerFileName);

                            if (File.Exists(path))
                                File.Delete(path);
                        }
                    }

                    DocDocumentHistory objDocDocumentHistory = new DocDocumentHistory();
                    objDocDocumentHistory.HistoryID = Guid.NewGuid();
                    objDocDocumentHistory.UserID = dbObj.ModifiedBy;
                    objDocDocumentHistory.Action = "Updated";
                    objDocDocumentHistory.ActionDate = dbObj.ModifiedOn;
                    objDocDocumentHistory.DocumentRef_ID = objDocDocument.DocumentID;
                    PayrollDataContext.DocDocumentHistories.InsertOnSubmit(objDocDocumentHistory);

                    if (listDocAttachment.Count > 0)
                        PayrollDataContext.DocAttachments.InsertAllOnSubmit(listDocAttachment);

                    List<DocAdditionalInformationDetail> dbListDocAdditionalInformationDetail = PayrollDataContext.DocAdditionalInformationDetails.Where(x => x.DocumentRef_ID == objDocDocument.DocumentID).ToList();
                    if (dbListDocAdditionalInformationDetail.Count > 0)
                        PayrollDataContext.DocAdditionalInformationDetails.DeleteAllOnSubmit(dbListDocAdditionalInformationDetail);

                    if (dbListDocAdditionalInformationDetail.Count > 0)
                    {
                        listAdditionalInformationDetl.ForEach(x => x.DocumentRef_ID = objDocDocument.DocumentID);
                        PayrollDataContext.DocAdditionalInformationDetails.InsertAllOnSubmit(listAdditionalInformationDetl);
                    }                   
                
                }

                PayrollDataContext.SubmitChanges();
                status.IsSuccess = true;
            }

            return status;
        }

        public static DocDocument GetDocDocumentById(Guid documentId)
        {
            return PayrollDataContext.DocDocuments.SingleOrDefault(x => x.DocumentID == documentId);
        }


        public static DocPartyContact GetDocPartyContact(Guid ContactId)
        {
            return PayrollDataContext.DocPartyContacts.SingleOrDefault(x => x.ContactId == ContactId);
        }

        public static List<DocContactAgreementLine> GetDocContactAgreementLine(Guid ContactId)
        {
            return PayrollDataContext.DocContactAgreementLines.Where(x => x.ContactRef_Id == ContactId).ToList();
        }

        public static List<DocContactAttachment> GetDocContactAttachment(Guid ContactId)
        {
            return PayrollDataContext.DocContactAttachments.Where(x => x.ContactRef_ID == ContactId).ToList();
        }

        public static DocContactAttachment DocContactAttachmentById(Guid LineId)
        {
            return PayrollDataContext.DocContactAttachments.SingleOrDefault(x => x.DocumentID == LineId);
        }


        public static DocParty GetDocPartyById(Guid partyId)
        {
            return PayrollDataContext.DocParties.SingleOrDefault(x => x.PartyID == partyId);
        }

        public static List<DocAssign> GetDocAssignByDocumentId(Guid documentId)
        {
            return PayrollDataContext.DocAssigns.Where(x => x.DocumentRef_ID == documentId).OrderBy(x => x.UserID).ToList();
        }

        public static List<DocAttachment> GetDocAttachmentByDocumentId(Guid documentId)
        {
            return PayrollDataContext.DocAttachments.Where(x => x.DocumentRef_ID == documentId).ToList();
        }

        public static List<GetDocumentListResult> GetDocumentList(int currentPage, int pageSize, Guid? partyID, Guid? categoryID, Guid? subCategoryID, Guid? documentID, string documentNameSearch,
            DateTime? dateAddedFrom, DateTime? dateAddedTo, DateTime? expiryDateFrom, DateTime? expiryDateTo)
        {
            return PayrollDataContext.GetDocumentList(currentPage, pageSize, partyID, categoryID, subCategoryID, documentID, documentNameSearch, dateAddedFrom, dateAddedTo, expiryDateFrom, expiryDateTo, SessionManager.CurrentLoggedInUserID).ToList();
        }


        public static List<GetDocDealerListResult> GetGetDocDealerListResult(int currentPage, int pageSize, string DealerName, int? ZoneID, int? DistrictId,
        DateTime? expiryDateFrom, DateTime? expiryDateTo, int branchId)
        {
            return PayrollDataContext.GetDocDealerList(currentPage, pageSize, DealerName, ZoneID, DistrictId, expiryDateFrom, expiryDateTo, branchId).ToList();
        }


        public static List<GetDocPainterListResult> GetGetDocPainterListResult(int currentPage, int pageSize, string DealerName, int? ZoneID, int? DistrictId,
            int branchId, Guid? dealerId)
        {
            return PayrollDataContext.GetDocPainterList(currentPage, pageSize, DealerName, ZoneID,DistrictId, branchId, dealerId).ToList();
        }

        public static Status SaveDocumentComment(DocComment obj)
        {
            Status status = new Status();

            PayrollDataContext.DocComments.InsertOnSubmit(obj);
            PayrollDataContext.SubmitChanges();
            status.IsSuccess = true;

            return status;
        }


        public static List<TextValue> GetDocumentNameSearch(string prefix)
        {
            List<TextValue> list = (from r in PayrollDataContext.DocDocuments
                                    where (r.Title.ToLower().Contains(prefix.ToLower()) || r.ID.ToLower().Contains(prefix.ToLower()) || r.Description.ToLower().Contains(prefix.ToLower()))
                                    select new TextValue()
                                    {
                                        Text = r.Title,
                                        Value = r.DocumentID.ToString()
                                    }).ToList();
            return list;
        }


        public static List<TextValue> GetDealerNameSearch(string prefix,int Type)
        {
            List<TextValue> list = (from r in PayrollDataContext.DocPartyContacts
                                    where (r.Name.ToLower().Contains(prefix.ToLower()) && r.Type == Type)
                                    select new TextValue()
                                    {
                                        Text = r.Name,
                                        Value = r.ContactId.ToString()
                                    }).ToList();
            return list;
        }


        public static List<BODocumentClass> GetDocDocumentComments(string searchText)
        {
            List<BODocumentClass> list = (from dc in PayrollDataContext.DocComments
                                          join dd in PayrollDataContext.DocDocuments on dc.DocumentRef_ID equals dd.DocumentID
                                          join p in PayrollDataContext.DocParties on dd.PartyRef_ID equals p.PartyID
                                          join uu in PayrollDataContext.UUsers on dc.CommentedBy equals uu.UserID
                                          where (dd.Title.ToLower().Contains(searchText.ToLower()) || p.Name.ToLower().Contains(searchText.ToLower()))
                                          && (dd.IsDeleted == null || dd.IsDeleted.Value == false)
                                          orderby dc.CommentedDate descending
                                          select new BODocumentClass()
                                          {
                                              Comment = dc.Comments,
                                              Title = dd.Title,
                                              DateValue = dc.CommentedDate,
                                              DocumentID = dd.DocumentID.ToString(),
                                              CommentedBy = uu.UserName
                                          }).Take(10).ToList();
            return list;
        }

        public static Status DeleteDocDocument(Guid documentID)
        {
            Status status = new Status();

            DocDocument dbDocDocument = PayrollDataContext.DocDocuments.SingleOrDefault(x => x.DocumentID == documentID);
            if (dbDocDocument != null)
            {
                dbDocDocument.IsDeleted = true;
                PayrollDataContext.SubmitChanges();
                status.IsSuccess = true;
            }
            else
            {
                status.ErrorMessage = "Document not found.";
                status.IsSuccess =false;
            }

            return status;
        }

        public static Status DeleteDocDealerPainter(Guid documentID)
        {
            Status status = new Status();

            DocPartyContact dbDocDocument = PayrollDataContext.DocPartyContacts.SingleOrDefault(x => x.ContactId == documentID);
            if (dbDocDocument != null)
            {

                PayrollDataContext.DocPartyContacts.DeleteOnSubmit(dbDocDocument);
                status.IsSuccess = true;
            }
            else
            {
                status.ErrorMessage = "Document not found.";
                status.IsSuccess = false;
            }
            PayrollDataContext.SubmitChanges();
            return status;
        }


        public static List<DocumentHistoryBO> GetDocumentHistoryByDocumentID(Guid documentID)
        {
            List<DocumentHistoryBO> list = (from dh in PayrollDataContext.DocDocumentHistories
                                            join doc in PayrollDataContext.DocDocuments on dh.DocumentRef_ID equals doc.DocumentID
                                            join uu in PayrollDataContext.UUsers on dh.UserID equals uu.UserID
                                            where dh.DocumentRef_ID == documentID
                                            orderby dh.ActionDate descending
                                            select new DocumentHistoryBO()
                                            {
                                                Action = dh.Action,
                                                ActionDate = dh.ActionDate.Value,
                                                UserName = uu.UserName,
                                                Details = string.Format("{0} {1}", doc.Title, dh.Action)
                                            }).ToList();

            return list;
        }

        public static Status SaveUpdateDocAdditionalInformation(DocAdditionalInformation obj)
        {
            Status status = new Status();

            DocAdditionalInformation dbObj = PayrollDataContext.DocAdditionalInformations.SingleOrDefault(x => x.AdditionalInformationID == obj.AdditionalInformationID);
            if (dbObj != null)
                dbObj.Name = obj.Name;
            else
                PayrollDataContext.DocAdditionalInformations.InsertOnSubmit(obj);

            PayrollDataContext.SubmitChanges();
            status.IsSuccess = true;
            return status;
        }

        public static DocAdditionalInformation GetDocAdditionalInformationById(Guid additionalInformationID)
        {
            return PayrollDataContext.DocAdditionalInformations.SingleOrDefault(x => x.AdditionalInformationID == additionalInformationID);
        }

        public static List<DocAdditionalInformation> GetDocAdditionalInformations()
        {
            return PayrollDataContext.DocAdditionalInformations.OrderBy(x => x.Name).ToList();
        }

        public static Status DeleteDocAdditionalInformation(Guid additionalInformationID)
        {
            Status status = new Status();

            DocAdditionalInformation dbObj = GetDocAdditionalInformationById(additionalInformationID);
            if (dbObj == null)
            {
                status.ErrorMessage = "Record not found.";
                status.IsSuccess = false;
                return status;               
            }

            if (PayrollDataContext.DocAdditionalInformationDetails.Any(x => x.AdditionalInformationRef_ID == additionalInformationID))
            {
                status.ErrorMessage = "Additional Information is in use, cannot be deleted.";
                status.IsSuccess = false;
                return status;   
            }

            PayrollDataContext.DocAdditionalInformations.DeleteOnSubmit(dbObj);
            PayrollDataContext.SubmitChanges();
            status.IsSuccess = true;

            return status;
        }

        public static List<DocAdditionalInformationBO> GetAdditionalInformationBOByDocumentID(Guid documentID)
        {
            List<DocAdditionalInformationBO> list = (from aid in PayrollDataContext.DocAdditionalInformationDetails
                                                     join dd in PayrollDataContext.DocDocuments on aid.DocumentRef_ID equals dd.DocumentID
                                                     join ai in PayrollDataContext.DocAdditionalInformations on aid.AdditionalInformationRef_ID equals ai.AdditionalInformationID
                                                     where aid.DocumentRef_ID == documentID
                                                     select new DocAdditionalInformationBO()
                                                     {
                                                         AdditionalInformationID = aid.AdditionalInformationRef_ID.Value.ToString(),
                                                         Name = ai.Name,
                                                         Value = aid.Value
                                                     }).OrderBy(x => x.Name).ToList();

            return list;
        }

        public static List<TextValue> GetDocumentSearchByTitlePartyNameID(string prefix)
        {
            List<TextValue> list = (from r in PayrollDataContext.DocDocuments
                                    join dp in PayrollDataContext.DocParties on r.PartyRef_ID equals dp.PartyID
                                    where (r.Title.ToLower().Contains(prefix.ToLower()) 
                                        || r.ID.ToLower().Contains(prefix.ToLower()) 
                                        || dp.Name.ToLower().Contains(prefix.ToLower())
                                        )
                                    select new TextValue()
                                    {
                                        Text = dp.Name + " : " + r.Title + " : " + r.ID,
                                        Value = r.DocumentID.ToString()
                                    }).ToList();
            return list;
        }

        public static bool CanUserDeleteDocument()
        {
            if (SessionManager.IsCustomRole == true)
            {
                return false;
            }

            return true;
        }

        public static bool CanUserAddEditDocument()
        {
            if (SessionManager.IsCustomRole == true)
            {
                UUser objUUser = UserManager.GetUserByUserID(SessionManager.CurrentLoggedInUserID);
                if (objUUser != null && objUUser.DocumentIsViewOnly != null && !objUUser.DocumentIsViewOnly.Value)
                    return true;
                else
                    return false;
            }

            return true;
        }

        public static Status SaveUpdateDocParty(DocParty objDocParty, bool IsSave)
        {
            Status status = new Status();

            if (IsSave)
            {
                if (PayrollDataContext.DocParties.Any(x => x.Name.ToLower() == objDocParty.Name.ToLower()))
                {
                    status.ErrorMessage = "Party name already exists.";
                    status.IsSuccess = false;
                    return status;
                }

                PayrollDataContext.DocParties.InsertOnSubmit(objDocParty);
            }
            else
            {
                DocParty dbDocParty = PayrollDataContext.DocParties.SingleOrDefault(x => x.PartyID == objDocParty.PartyID);
                dbDocParty.Name = objDocParty.Name;
            }

            PayrollDataContext.SubmitChanges();
            status.IsSuccess = true;

            return status;
        }

        public static Status DeleteDocParty(Guid partyID)
        {
            Status status = new Status();

            DocParty dbDocParty = GetDocPartyById(partyID);
            if (dbDocParty != null)
            {
                if (PayrollDataContext.DocDocuments.Any(x => x.PartyRef_ID == partyID))
                {
                    status.ErrorMessage = "Party is in use, cannot be deleted.";
                    status.IsSuccess = false;
                    return status;
                }

                PayrollDataContext.DocParties.DeleteOnSubmit(dbDocParty);
                PayrollDataContext.SubmitChanges();
                status.IsSuccess = true;
            }
            else
            {
                status.ErrorMessage = "Party not found.";
                status.IsSuccess = false;
            }

            return status;
        }

        public static DocContactAttachment GetDocContactAttachmentsByID(Guid documentID)
        {
            return PayrollDataContext.DocContactAttachments.SingleOrDefault(x => x.DocumentID == documentID);
        }

        public static List<DocPartyContact> GetDealers()
        {
            return PayrollDataContext.DocPartyContacts.Where(x => x.Type == 1).OrderBy(x => x.Name).ToList();
        }

    }


    public class BODocumentClass
    {
        public string Title { get; set; }
        public string TimeDetails { get; set; }
        public DateTime DateValue { get; set; }
        public string DocumentID { get; set; }
        public string Comment { get; set; }
        public string CommentedBy { get; set; }
    }

    public class DocumentHistoryBO
    {
        public string Action { get; set; }
        public DateTime ActionDate { get; set; }
        public string UserName { get; set; }
        public string Details { get; set; }
    }

    public class DocAdditionalInformationBO
    {
        public string AdditionalInformationID { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
    }
}
