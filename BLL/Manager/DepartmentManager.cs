﻿using System.Collections.Generic;
using System.Linq;
using DAL;
using System;

namespace BLL.Manager
{
    public class DepartmentManager : BaseBiz
    {
        public bool Save(Department entity)
        {

            if (PayrollDataContext.Departments.Any(x => x.Name.Trim().ToLower() == entity.Name.Trim().ToLower()
               ))
                return false;

            PayrollDataContext.Departments.InsertOnSubmit(entity);
            PayrollDataContext.SubmitChanges();
            return true;
        }

        public bool Save(SubDepartment entity)
        {
            if (PayrollDataContext.SubDepartments.Any(x => x.Name.Trim().ToLower() == entity.Name.Trim().ToLower()
              && x.DepartmentId == entity.DepartmentId))
                return false;

            PayrollDataContext.SubDepartments.InsertOnSubmit(entity);
            PayrollDataContext.SubmitChanges();
            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="entity"></param>
        /// <returns>true if updated,false if not, as the record may have been deleted also</returns>
        public bool Update(Department entity)
        {
 
            if (PayrollDataContext.Departments.Any(x => x.Name.Trim().ToLower() == entity.Name.Trim().ToLower()
                && x.DepartmentId != entity.DepartmentId))
                return false;

            Department dbEntity = PayrollDataContext.Departments.SingleOrDefault(c => c.DepartmentId == entity.DepartmentId);
            //may be deleted
            if (dbEntity == null)
                return false;

            dbEntity.Name = entity.Name;
            dbEntity.HeadEmployeeId = entity.HeadEmployeeId;
            dbEntity.Description = entity.Description;
            dbEntity.Phone = entity.Phone;
            dbEntity.Fax = entity.Fax;
            dbEntity.Email = entity.Email;
            dbEntity.Code = entity.Code;
            dbEntity.LocationBranchId = entity.LocationBranchId;
            //BizHelper.CopyObject<Department>(entity, ref dbEntity);
            UpdateChangeSet();
            return true;
            //return true;
        }

        public bool Update(SubDepartment entity)
        {
            if (PayrollDataContext.SubDepartments.Any(x => x.Name.Trim().ToLower() == entity.Name.Trim().ToLower()
                && x.SubDepartmentId != entity.SubDepartmentId && x.DepartmentId == entity.DepartmentId))
                return false;



            SubDepartment dbEntity = PayrollDataContext.SubDepartments.SingleOrDefault(c => c.SubDepartmentId == entity.SubDepartmentId);
            //may be deleted
            if (dbEntity == null)
                return false;

            dbEntity.Name = entity.Name;
            
            dbEntity.Description = entity.Description;
           

            //BizHelper.CopyObject<Department>(entity, ref dbEntity);
            return UpdateChangeSet();
            //return true;
        }


        public int Delete(Department entity)
        {

            return PayrollDataContext.DeleteDepartment(entity.DepartmentId);
        }

        public int Delete(SubDepartment entity)
        {
            if (PayrollDataContext.EEmployees.Any(x => x.SubDepartmentId == entity.SubDepartmentId))
                return 0;

            SubDepartment dbenitty = PayrollDataContext.SubDepartments.SingleOrDefault(x => x.SubDepartmentId == entity.SubDepartmentId);
            if (dbenitty != null)
                PayrollDataContext.SubDepartments.DeleteOnSubmit(dbenitty);

            DeleteChangeSet();
            return 1;
        }

        public Department GetById(int departmentId)
        {
            return PayrollDataContext.Departments.SingleOrDefault(c => c.DepartmentId == departmentId);
        }
        public static Department GetDepartmentById(int departmentId)
        {
            return PayrollDataContext.Departments.SingleOrDefault(c => c.DepartmentId == departmentId);
        }
        public SubDepartment GetSubDepartmentById(int subDepartmentId)
        {
            return PayrollDataContext.SubDepartments.SingleOrDefault(c => c.SubDepartmentId == subDepartmentId);
        }

        public static List<Department> GetAllDepartments()
        {
            return PayrollDataContext.Departments.OrderBy(x => x.Name).ToList();
            //List<GetDepartmentListResult> list = null;

            //string departmentIDList = SessionManager.CustomRoleDeparmentIDList;

            //if (departmentIDList == null)
            //{
            //    list = GetFromCache<List<GetDepartmentListResult>>("GetAllDepartment" );
            //    if (list != null)
            //        return list;
            //}

            //list = PayrollDataContext.GetDepartmentList(0, departmentIDList).ToList();


            //if (departmentIDList == null)
            //{
            //    SaveToCache<List<GetDepartmentListResult>>("GetAllDepartment", list);
            //}

            //return list;


        }

        public static List<GetDepartmentListResult> GetAllDepartmentsByBranch(int branchId)
        {

            List<GetDepartmentListResult> list = null;

            string departmentIDList = SessionManager.CustomRoleDeparmentIDList;

            if (departmentIDList == null)
            {
                list = GetFromCache<List<GetDepartmentListResult>>("GetAllDepartmentsByBranch" + branchId);
                if (list != null)
                    return list;
            }

            list = PayrollDataContext.GetDepartmentList(branchId, departmentIDList).ToList();


            if (departmentIDList == null)
            {
                SaveToCache<List<GetDepartmentListResult>>("GetAllDepartmentsByBranch" + branchId, list);
            }

            return list;


        }

        public static List<SubDepartment> GetAllSubDepartmentsByDepartment(int departmentId)
        {

            return PayrollDataContext.SubDepartments.Where(x => x.DepartmentId == departmentId).OrderBy(x=>x.Name).ToList();
        }


        public static List<SubDepartment> GetAllSubDepartmentsByDepartment(List<int> depList)
        {

            return PayrollDataContext.SubDepartments.Where(x => depList.Contains(x.DepartmentId.Value)).ToList();
        }
        //public List<Department> GetSubDepartmentsByCompany(int companyId)
        //{

        //    List<int> customRoleDepartmentIDList = SessionManager.CustomRoleDeparmentList;

        //    return PayrollDataContext.Departments
        //        // Custom Role Dep Restriction case
        //        .Where(x => customRoleDepartmentIDList.Count == 0 || customRoleDepartmentIDList.Contains(x.DepartmentId))
        //        .Where(e => e.Branch.CompanyId == companyId)
        //        .OrderBy(e => e.Name).ToList();
        //}

        public List<Department> GetDepartmentsByCompany(int companyId)
        {

            List<int> customRoleDepartmentIDList = SessionManager.CustomRoleDeparmentList;

            return PayrollDataContext.Departments
                // Custom Role Dep Restriction case
                .Where(x => customRoleDepartmentIDList.Count == 0 || customRoleDepartmentIDList.Contains(x.DepartmentId))
                //.Where(e => e.Branch.CompanyId == companyId)
                .OrderBy(e => e.Name).ToList();
        }

        public List<Department> GetDepartmentsWithBranchByCompany(int companyId)
        {

            bool hasMultipleBranch = PayrollDataContext.Branches.Where(x => x.CompanyId == companyId).Count() > 1;

            List<int> customRoleDepartmentIDList = SessionManager.CustomRoleDeparmentList;

            List<Department> list = PayrollDataContext.Departments
                // Custom Role Dep Restriction case
               .Where(x => customRoleDepartmentIDList.Count == 0 || customRoleDepartmentIDList.Contains(x.DepartmentId))
               //.Where(e => e.Branch.CompanyId == companyId)
               .OrderBy(e => e.Name).ToList();

            //if (hasMultipleBranch)
            //{
            //    foreach (Department dep in list)
            //    {
            //        dep.Name +=" (" +  (dep.Branch.Name) + ")";
            //    }
            //}

            return list;
        }

        public static bool IsDepartmentUnderThisBranch(int branchid, int departmentid)
        {
            if (PayrollDataContext.BranchDepartments.Any(x => x.BranchID == branchid && x.DepartmentID == departmentid))
                return true;
            return false;
        }

        public List<SubDepartment> GetSubDepartmentsByCompany(int companyId)
        {

            List<int> customRoleDepartmentIDList = SessionManager.CustomRoleDeparmentList;

            return PayrollDataContext.SubDepartments
                
                .OrderBy(e => e.Name).ToList();
        }
        public static List<Department> GetCommonDepartmentList(int branchId)
        {
            //if (branchId == -1 && CommonManager.CalculationConstant.CompanyIsHPL.Value)
            //{
            //    //get common departments that exists in diff branch
            //    List<GetCommonDepartmentInDiffBranchResult> list = PayrollDataContext
            //        .GetCommonDepartmentInDiffBranch(SessionManager.CurrentCompanyId).ToList();

            //    List<Department> depList = new List<Department>();
            //    foreach (GetCommonDepartmentInDiffBranchResult item in list)
            //    {
            //        Department dep = new Department();
            //        dep.DepartmentId = item.DepartmentId;
            //        dep.Name = item.Name;

            //        depList.Add(dep);
            //    }

            //    return depList;
            //}
            //else
            {
                return PayrollDataContext.Departments
                   
                    .OrderBy(d => d.Name).ToList();

            }
        }


        public static List<BranchDepartment> getBranchDepartmentForHead()
        {
            List<BranchDepartment> newList = new List<BranchDepartment>();
            // newList = PayrollDataContext.BranchDepartments.ToList();

            // list head office only as in case of branch we can not track department wise
            newList =
                (
                   from bd in PayrollDataContext.BranchDepartments
                   join b in PayrollDataContext.Branches on bd.BranchID equals b.BranchId
                   where b.IsHeadOffice != null && b.IsHeadOffice == true
                   select bd
                ).ToList();

            EEmployee emp = null;
            foreach (var item in newList)
            {
                item.BranchName = item.Branch.Name;
                item.DepartmentName = item.Department.Name;
                if (item.HeadEmployeeId != null)
                {
                    emp =  EmployeeManager.GetEmployeeById(item.HeadEmployeeId.Value);
                    item.HeadName = emp.Name + " - " +
                        item.HeadEmployeeId.Value;


                    if (emp.IsRetired != null && emp.IsRetired.Value && emp.EHumanResources[0].DateOfRetirementEng <= DateTime.Now.Date)
                    {
                        item.HeadName += " <span style='color:red'> (Already retired) </span>";
                    }
                    if (emp.IsResigned != null && emp.IsResigned.Value && emp.EHumanResources[0].DateOfResignationEng <= DateTime.Now.Date)
                    {
                        item.HeadName += " <span style='color:red'> (Already retired) </span>";
                    }
                }
                //
            }
            return newList;
        }

        public static BranchDepartment getBranchDepartmentByIDs(int BranchID, int DepartmentID)
        {
            return PayrollDataContext.BranchDepartments.Where(x => x.BranchID == BranchID && x.DepartmentID == DepartmentID).FirstOrDefault();
        }

        public static Status UpdateDepartmentHeads(BranchDepartment instance)
        {
            Status status = new Status();
            BranchDepartment dbEntity = new BranchDepartment();
            dbEntity = PayrollDataContext.BranchDepartments.Where(x => x.BranchID == instance.BranchID && x.DepartmentID == instance.DepartmentID).FirstOrDefault();
            dbEntity.HeadEmployeeId = instance.HeadEmployeeId;
            PayrollDataContext.SubmitChanges();
            status.IsSuccess = true;
            return status;

        }

        internal static Department getDepartmentByName(string DepartmentName)
        {
            if (PayrollDataContext.Departments.Any(x => x.Name == DepartmentName))
            {
                return PayrollDataContext.Departments.FirstOrDefault(x => x.Name == DepartmentName);
            }
            return new Department();
        }
    }
}

