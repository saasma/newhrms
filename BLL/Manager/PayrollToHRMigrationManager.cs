﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Transactions;
using DAL;
using Utils.Calendar;
using System;
using System.Web.UI.WebControls;
using Utils;
using System.Web;
using Utils.Helper;
using Microsoft.SqlServer.Management.Smo;
using Microsoft.SqlServer.Management.Common;
using System.IO;
using System.IO.Compression;
using BLL.BO;
using System.Text;

namespace BLL.Manager
{
    public class PayrollToHRMigrationManager : BaseBiz
    {

        public static Status MigratePayrollToHR()
        {
            Status status = new Status();
            //importCount = 0;

            //foreach (var item in list)
            //{
            //    importCount++;
            //    if (PayrollDataContext.EmployeeCashAwards.Any(x => (x.EmployeeId == item.EmployeeId) && (x.CashAwardTypeId == item.CashAwardTypeId) && (x.Month == item.Month) && (x.Year == item.Year)))
            //    {
            //        EmployeeCashAward dbObj = PayrollDataContext.EmployeeCashAwards.SingleOrDefault(x => (x.EmployeeId == item.EmployeeId) && (x.CashAwardTypeId == item.CashAwardTypeId) && (x.Month == item.Month) && (x.Year == item.Year));
            //        dbObj.CalculationType = item.CalculationType;
            //        dbObj.AmountOrRate = item.AmountOrRate;
            //        dbObj.Month = item.Month;
            //        dbObj.Year = item.Year;
            //        dbObj.Notes = item.Notes;
            //        dbObj.CreatedBy = item.CreatedBy;
            //        dbObj.CreatedOn = item.CreatedOn;
            //        dbObj.Date = item.Date;
            //        dbObj.DateEng = item.DateEng;
            //    }
            //    else
            //    {
            //        PayrollDataContext.EmployeeCashAwards.InsertOnSubmit(item);
            //    }
            //}

            // Nep DOB
            //if (IsEnglish == false)
            {
                List<EEmployee> emp = PayrollDataContext.EEmployees.ToList();
                CustomDate cDate = null;
                foreach (EEmployee item in emp)
                {
                    string dob = item.DateOfBirth;


                    string[] values = dob.Split(new char[] { '/' });

                   

                    // if last portion is year then only convert or else already converted
                    if (int.Parse(values[2]) >= 1000)
                    {
                        //if (Config.DateFormat_YYYY_MM_DD)
                        cDate = new CustomDate(int.Parse(values[0]), int.Parse(values[1]), int.Parse(values[2]), false);

                        item.DateOfBirth = cDate.ToString();
                        item.DateOfBirthEng = cDate.EnglishDate;
                    }

                   

                }

            }

            List<ECurrentStatus> _listECurrentStatus = PayrollDataContext.ECurrentStatus.ToList();
            foreach (ECurrentStatus _Item in _listECurrentStatus)
            {
                
                _Item.FromDate = GetAppropriateDate(_Item.FromDateEng);
                //string EngDate = _Item.FromDateEng.Value.ToString("yyyy/MM/dd");
               // _Item.FromDateEng = DateTime.Parse(EngDate);
                
            }

            List<EWorkShiftHistory> _listWorkshift = PayrollDataContext.EWorkShiftHistories.ToList();
            foreach (EWorkShiftHistory _Item in _listWorkshift)
            {
                if (_Item.FromDateEng != null)
                    _Item.FromDate = GetAppropriateDate(_Item.FromDateEng.Value);

            }

            List<ELocationHistory> _listLocation = PayrollDataContext.ELocationHistories.ToList();
            foreach (ELocationHistory _Item in _listLocation)
            {
                if (_Item.FromDateEng != null)
                    _Item.FromDate = GetAppropriateDate(_Item.FromDateEng.Value);

            }

            List<EGradeHistory> _listGrae = PayrollDataContext.EGradeHistories.ToList();
            foreach (EGradeHistory _Item in _listGrae)
            {
                if (_Item.FromDateEng != null)
                    _Item.FromDate = GetAppropriateDate(_Item.FromDateEng.Value);

            }


            List<EHumanResource> _listEHumanResource = PayrollDataContext.EHumanResources.ToList();
            foreach (EHumanResource _Item in _listEHumanResource)
            {
                _Item.SalaryCalculateFrom = GetAppropriateDate(_Item.SalaryCalculateFromEng);
                if(_Item.DateOfRetirementEng!=null)
                _Item.DateOfRetirement = GetAppropriateDate(_Item.DateOfRetirementEng.Value);

                if (_Item.DateOfResignationEng != null)
                    _Item.DateOfResignation = GetAppropriateDate(_Item.DateOfResignationEng.Value);
            }

            List<FinancialDate> _listFinancialDate = PayrollDataContext.FinancialDates.ToList();
            foreach (FinancialDate _Item in _listFinancialDate)
            {
                if (_Item.StartingDateEng != null)
                _Item.StartingDate = GetAppropriateDate(_Item.StartingDateEng.Value);
                if (_Item.EndingDateEng != null)
                _Item.EndingDate = GetAppropriateDate(_Item.EndingDateEng.Value);
            }

            List<LeaveDetail> _LeaveDetail = PayrollDataContext.LeaveDetails.ToList();
            foreach (LeaveDetail _Item in _LeaveDetail)
            {
                _Item.DateOn = GetAppropriateDate(_Item.DateOnEng);
            }


            List<LeaveRequest> _LeaveRequestList = PayrollDataContext.LeaveRequests.ToList();
            foreach (LeaveRequest _Item in _LeaveRequestList)
            {
                if (_Item.FromDateEng != null)
                _Item.FromDate = GetAppropriateDate(_Item.FromDateEng.Value);
                if (_Item.ToDateEng != null)
                _Item.ToDate = GetAppropriateDate(_Item.ToDateEng.Value);
            }

            List<PayrollMessage> _listPayrollMessage = PayrollDataContext.PayrollMessages.ToList();
            foreach (PayrollMessage _Item in _listPayrollMessage)
            {
                if (_Item.ReceivedDateEng != null)
                _Item.ReceivedDate = GetAppropriateDate(_Item.ReceivedDateEng.Value);
            }


            List<PayrollPeriod> _listPayrollPeriod = PayrollDataContext.PayrollPeriods.ToList();
            foreach (PayrollPeriod _Item in _listPayrollPeriod)
            {
                if (_Item.StartDateEng != null)
                _Item.StartDate = GetAppropriateDate(_Item.StartDateEng.Value);
                if (_Item.EndDateEng != null)
                _Item.EndDate = GetAppropriateDate(_Item.EndDateEng.Value);
            }

            List<PFRFFund> _PFRFFundList = PayrollDataContext.PFRFFunds.ToList();
            foreach (PFRFFund _Item in _PFRFFundList)
            {
                if (_Item.ApprovalDateEng != null)
                _Item.ApprovalDate = GetAppropriateDate(_Item.ApprovalDateEng.Value);
            }


            List<PIncome> _listPIncome = PayrollDataContext.PIncomes.ToList();
            foreach (PIncome _Item in _listPIncome)
            {
                if(_Item.OFEligibilityDateEng!=null)
                _Item.OFEligibilityDate = GetAppropriateDate(_Item.OFEligibilityDateEng.Value);
                if (_Item.OFWorkdaysFromDateEng != null)
                _Item.OFWorkdaysFromDate = GetAppropriateDate(_Item.OFWorkdaysFromDateEng.Value);
            }

            List<Project> _listProject = PayrollDataContext.Projects.ToList();
            foreach (Project _Item in _listProject)
            {
                if (_Item.StartDateEng != null)
                _Item.StartDate = GetAppropriateDate(_Item.StartDateEng.Value);
                if (_Item.EndDateEng != null)
                _Item.EndDate = GetAppropriateDate(_Item.EndDateEng.Value);
            }

            List<Setting> _listSetting = PayrollDataContext.Settings.ToList();
            foreach (Setting _Item in _listSetting)
            {
                if(_Item.ContractUptoDateEng!=null)
                _Item.ContractUptoDate = GetAppropriateDate(_Item.ContractUptoDateEng.Value);
                if (_Item.SalaryProcessUptoDateEng != null)
                _Item.SalaryProcessUptoDate = GetAppropriateDate(_Item.SalaryProcessUptoDateEng.Value);
            }


            PayrollDataContext.SubmitChanges();
            status.IsSuccess = true;
            return status;
        }
    }
}
