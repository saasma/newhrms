﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;
using System.Globalization;
using Utils;
using Ext.Net;
using BLL.Entity;
using System.Web;
using Utils.Calendar;
using Utils.Helper;
using BLL.BO;
using System.Configuration;
using System.Net.Configuration;

namespace BLL.Manager
{





    public class AttendanceManager : BaseBiz
    {
        public delegate void AttendanceEmployeeGenerate(int empId, DateTime start, DateTime end, bool saveEndDate);
        public delegate void AutoAttendanceEmployeeGenerate(DateTime start, DateTime end);

        //public static void UpdateAttendanceEmployeeDate(DateTime date)
        //{
        //    AttendanceEmployeeDate lastDate = PayrollDataContext.AttendanceEmployeeDates.FirstOrDefault();
        //    DateTime today = GetCurrentDateAndTime().Date;
        //    if (lastDate != null && date < today && date < lastDate.LastUptoDate.Value)
        //    {
        //        lastDate.LastUptoDate = date;
        //        PayrollDataContext.SubmitChanges();
        //    }
        //}

        public static List<AttendanceEmployeeDate> GetAttendanceProcessedLastDate()
        {
            return PayrollDataContext.AttendanceEmployeeDates.OrderByDescending(x => x.ID)
                .Take(20).ToList();
        }

        public static AttendanceEmployeeDate GetAttendanceGeneratedLastDate()
        {
            return PayrollDataContext.AttendanceEmployeeDates.OrderByDescending(x => x.ID).FirstOrDefault();
        }

        // Regenerate atte report for the Employee for the selected date range
        public static void GenerateAsyncAttendanceSingleEmployeeTable(int empId, DateTime start, DateTime end)
        {
            AttendanceEmployeeGenerate method = new AttendanceEmployeeGenerate(GenerateAttendanceEmployeeTable);
            method.BeginInvoke(empId, start, end, false, null, null);
        }

        //public static void GenerateAsyncAttendanceEmployeeTable()
        //{

        //    if (PayrollDataContext.AttendanceInOutTimes.Any() == false)
        //        return;

        //    DateTime? start = null, end = null;
        //    if (PayrollDataContext.AttendanceEmployees.Any() == false
        //        || PayrollDataContext.AttendanceEmployeeDates.Any() == false)
        //    {
        //        AttendanceCheckInCheckOut atte = PayrollDataContext.AttendanceCheckInCheckOuts
        //            .OrderBy(x => x.DateTime).FirstOrDefault();

        //        if (atte != null && atte.DateTime != null)
        //            start = atte.DateTime.Date;
        //    }
        //    else
        //    {
        //        AttendanceEmployeeDate lastDate = PayrollDataContext.AttendanceEmployeeDates.FirstOrDefault();
        //        start = lastDate.LastUptoDate.Value;

        //        // if already processed for this date
        //        if (start.Value.Date == GetCurrentDateAndTime().Date.AddDays(-1).Date)
        //            return;

        //        start = start.Value.AddDays(1).Date;
        //    }

        //    end = GetCurrentDateAndTime().Date.AddDays(-1).Date;

        //    if (start == null)
        //        start = end;

        //    if (start > end)
        //        start = end;

        //    AutoAttendanceEmployeeGenerate method = new AutoAttendanceEmployeeGenerate(AutoGenerateAttendanceEmployeeTable);
        //    method.BeginInvoke(start.Value, end.Value, null, null);



        //}

        public static List<GetAttendanceListForOvernightShiftMarkingResult> GetListForOvernightShiftMarking(
           int branchid, int depId, int empId, DateTime? start, DateTime? end, int currentPage, int pageSize, string sortBy, ref int total
            , int levelId, int designationId, TimeSpan toTime)
        {

            TimeSpan span = new TimeSpan(12, 0, 0);

            List<GetAttendanceListForOvernightShiftMarkingResult> list =
                PayrollDataContext.GetAttendanceListForOvernightShiftMarking
                (branchid, depId, empId, start, end, currentPage, pageSize, sortBy, levelId, designationId, span, toTime)
                .ToList();

            if (list.Count > 0)
                total = list[0].TotalRows.Value;

            return list;
        }

        public static List<GetAttendanceListForOvernightMarkingNewResult> GetListForOvernightShiftMarkingNew
            (
         int branchid, int depId, int empId, DateTime? start, DateTime? end, int currentPage, int pageSize, string sortBy, ref int total
          , int levelId, int designationId)
        {



            List<GetAttendanceListForOvernightMarkingNewResult> list =
                PayrollDataContext.GetAttendanceListForOvernightMarkingNew
                (branchid, depId, empId, start, end, currentPage, pageSize, sortBy, levelId, designationId)
                .ToList();


            foreach (GetAttendanceListForOvernightMarkingNewResult item in list)
            {
                if (item.InTime != null)
                    item.InTimeText = item.InTime.Value.ToShortTimeString() + " " + item.InTime.Value.ToString("MMM-dd");
                if (item.OutTime != null)
                    item.OutTimeText = item.OutTime.Value.ToShortTimeString() + " " + item.OutTime.Value.ToString("MMM-dd");
                if (item.WorkHour != null)
                {
                    item.WorkHourText = MinuteWith24Hour(item.WorkHour.Value);
                }

                //if (item.DateEng != null)
                //    item.ActualDate = GetAppropriateDate(item.DateEng.Value);
            }

            if (list.Count > 0)
                total = list[0].TotalRows.Value;

            return list;
        }
        public static void AutoGenerateAttendanceEmployeeTable(DateTime start, DateTime end)
        {

            try
            {

                using (PayrollDataContext _payrollDataContext = new PayrollDataContext(Config.ConnectionString))
                {

                    _payrollDataContext.GenerateAttendanceEmployee(0, start, end, true);
                }

                Log.log(
                    string.Format("Auto Attendance generating for From:{0}, To:{1}",
                        start.ToShortDateString(), end.ToShortDateString()), "");

            }
            catch (Exception exp)
            {
                Log.log(
                    string.Format("Auto Attendance error generating for From:{0}, To:{1}",
                        start.ToShortDateString(), end.ToShortDateString()), exp.ToString());
            }
        }

        public static void GenerateAttendanceEmployeeTable(int empId, DateTime start, DateTime end, bool saveEndDate)
        {

            try
            {

                using (PayrollDataContext _payrollDataContext = new PayrollDataContext(Config.ConnectionString))
                {

                    _payrollDataContext.GenerateAttendanceEmployee(empId, start, end, saveEndDate);
                }

                Log.log(
                    string.Format("Attendance generating for EIN:{0}, From:{1}, To:{2}",
                    empId, start.ToShortDateString(), end.ToShortDateString()), "");

            }
            catch (Exception exp)
            {
                Log.log(
                    string.Format("Attendance error generating for EIN:{0}, From:{1}, To:{2}",
                    empId, start.ToShortDateString(), end.ToShortDateString()), exp.ToString());
            }
        }

        public static List<GetAttendanceEmployeeReportSummaryResult> GetAttendanceSummary(
            int branchid, int depId, int levelid, string designation, int empId, int currentPage, DateTime start, DateTime end,
            int pageSize, string sortBy, ref int total, bool isGroup, bool branchWise, bool depWise,
            bool levelPosWise, bool desigWise, bool isExport, int payGroupID)
        {

            List<GetAttendanceEmployeeReportSummaryResult> list =
                PayrollDataContext.GetAttendanceEmployeeReportSummary
                (empId, start.Date, end.Date, branchid, depId, levelid, designation, isGroup, branchWise, depWise, levelPosWise, desigWise,
                currentPage, pageSize, sortBy, payGroupID)
                .ToList();

            if (isExport == false)
            {
                foreach (GetAttendanceEmployeeReportSummaryResult item in list)
                {
                    item.QueryString =
                        string.Format("from={0}&to={1}&ein={2}&isGroup={3}&gb={4}&gd={5}&gp={6}&gdd={7}",
                            start.ToShortDateString(), end.ToShortDateString(), item.EmployeeId, isGroup,
                            item.BranchID == null ? 0 : item.BranchID.Value,
                            item.DepartmentID == null ? 0 : item.DepartmentID.Value,
                            item.LevelPositionID == null ? 0 : item.LevelPositionID.Value,
                            item.DesignationID == null ? 0 : item.DesignationID.Value);
                }
            }

            if (list.Count > 0)
                total = list[0].TotalRows.Value;

            return list;
        }

        public static List<GetAttendanceEmployeeReportDetailsResult> GetAttendanceDetails(
           int branchid, int depId, int levelid, string designationName, int empId, int currentPage, DateTime start, DateTime end,
           int pageSize, string sortBy, ref int total, bool isGroup, bool islateOnly, bool isAbsentOnly, bool isLeaveOnly, bool skipTotal
            , bool showLeaveMarkedForAbsent
            )
        {

            List<GetAttendanceEmployeeReportDetailsResult> list =
                PayrollDataContext.GetAttendanceEmployeeReportDetails
                (empId, start.Date, end.Date, branchid, depId, levelid, designationName,
                currentPage, pageSize, sortBy, islateOnly, isAbsentOnly, isLeaveOnly, showLeaveMarkedForAbsent)
                .ToList();



            foreach (GetAttendanceEmployeeReportDetailsResult item in list)
            {

                try
                {
                    item.NepDate = CustomDate.ConvertEngToNep(new CustomDate(item.Date.Day, item.Date.Month, item.Date.Year, true))
                        .ToStringShortMonthName();
                }
                catch { }
            }



            if (list.Count > 0)
            {

                total = list[0].TotalRows.Value;

            }

            if (list.Count > 0 && skipTotal == false)
            {
                if ((pageSize * currentPage) + pageSize >= list[0].TotalRows.Value)
                {
                    GetAttendanceEmployeeReportDetailsResult totalrow = new GetAttendanceEmployeeReportDetailsResult();
                    totalrow.Name = "Total";
                    totalrow.StandardMin = list[0].TotalStandardMin == null ? 0 : list[0].TotalStandardMin.Value;
                    totalrow.WorkedMin = list[0].TotalWorkedMin == null ? 0 : list[0].TotalWorkedMin.Value;
                    totalrow.MissingOrExtra = list[0].TotalMissingOrExtra == null ? 0 : list[0].TotalMissingOrExtra.Value;

                    totalrow.LateMin = list[0].TotalLateMin == null ? 0 : list[0].TotalLateMin.Value;

                    totalrow.IsActualLate = list[0].TotalActualLate == null ? "" : list[0].TotalActualLate.Value.ToString();
                    totalrow.IsAbsent = list[0].TotalAbsent == null ? "" : list[0].TotalAbsent.Value.ToString();

                    totalrow.OTMinute = list[0].TotalOTMinute == null ? 0 : list[0].TotalOTMinute.Value;

                    list.Add(totalrow);

                    total += 1;

                }
            }

            return list;
        }

        public static List<GetAttendanceUploadStatusResult> GetAttendanceUploadStatus(int currentPage, int pageSize, string sortBy)
        {
            return PayrollDataContext.GetAttendanceUploadStatus(currentPage, pageSize, sortBy).ToList();
        }

        public static Status SetUpdateLateDeduction(List<GetListForLateProcessingResult> list)
        {
            Status status = new Status();

            List<AttendanceAbsent> insertList = new List<AttendanceAbsent>();

            foreach (GetListForLateProcessingResult item in list)
            {
                AttendanceAbsent dbEntity = PayrollDataContext.AttendanceAbsents.FirstOrDefault(
                    x => x.EmployeeId == item.EmployeeId && x.DateEng == item.Date);

                if (item.IsLateMarked == false)
                {
                    if (dbEntity != null)
                        PayrollDataContext.AttendanceAbsents.DeleteOnSubmit(dbEntity);
                }
                else
                {
                    if (dbEntity == null)
                    {
                        dbEntity = new AttendanceAbsent();
                        dbEntity.EmployeeId = item.EmployeeId;
                        dbEntity.DateEng = item.Date;
                        dbEntity.Date = GetAppropriateDate(item.Date);
                        dbEntity.CreatedBy = SessionManager.User.UserID;
                        dbEntity.CreatedOn = GetCurrentDateAndTime();
                        dbEntity.IsLate = true;

                        insertList.Add(dbEntity);
                    }
                    else
                        dbEntity.IsLate = true;
                }
            }

            if (insertList.Count > 0)
                PayrollDataContext.AttendanceAbsents.InsertAllOnSubmit(insertList);

            PayrollDataContext.SubmitChanges();

            return status;

        }
        public static Status SetUpdateSkipCumLateDeduction(List<GetListForCumulativeLateProcessingResult> list)
        {
            Status status = new Status();

            List<CumulativeLateEntry> insertList = new List<CumulativeLateEntry>();

            foreach (GetListForCumulativeLateProcessingResult item in list)
            {
                CumulativeLateEntry dbEntity = PayrollDataContext.CumulativeLateEntries.FirstOrDefault(
                    x => x.EmployeeId == item.EmployeeId && x.DateEng == item.Date);

                if (item.IsSkipEntry == false)
                {
                    if (dbEntity != null)
                        dbEntity.IsSkipEntry = false;
                }
                else
                {
                    if (dbEntity == null)
                    {
                        dbEntity = new CumulativeLateEntry();
                        dbEntity.EmployeeId = item.EmployeeId;
                        dbEntity.DateEng = item.Date;
                        dbEntity.Date = GetAppropriateDate(item.Date);
                        dbEntity.CreatedBy = SessionManager.User.UserID;
                        dbEntity.CreatedOn = GetCurrentDateAndTime();
                        dbEntity.IsSkipEntry = item.IsSkipEntry;

                        insertList.Add(dbEntity);
                    }
                    else
                    {
                        dbEntity.IsLateEntry = false;
                        dbEntity.IsSkipEntry = true;
                    }
                }
            }

            if (insertList.Count > 0)
                PayrollDataContext.CumulativeLateEntries.InsertAllOnSubmit(insertList);

            PayrollDataContext.SubmitChanges();

            return status;

        }
        public static Status SetSaveMarkLateDeduction(List<GetListForCumulativeLateProcessingResult> list)
        {
            Status status = new Status();

            List<CumulativeLateEntry> insertList = new List<CumulativeLateEntry>();

            foreach (GetListForCumulativeLateProcessingResult item in list)
            {
                CumulativeLateEntry dbEntity = PayrollDataContext.CumulativeLateEntries.FirstOrDefault(
                    x => x.EmployeeId == item.EmployeeId && x.DateEng == item.Date);

                if (item.IsLateMarked == false)
                {
                    if (dbEntity != null && dbEntity.IsLateEntry)
                        dbEntity.IsLateEntry = false;
                }
                else
                {
                    if (dbEntity == null)
                    {
                        dbEntity = new CumulativeLateEntry();
                        dbEntity.EmployeeId = item.EmployeeId;
                        dbEntity.DateEng = item.Date;
                        dbEntity.Date = GetAppropriateDate(item.Date);
                        dbEntity.CreatedBy = SessionManager.User.UserID;
                        dbEntity.CreatedOn = GetCurrentDateAndTime();
                        dbEntity.IsLateEntry = item.IsLateMarked; ;

                        insertList.Add(dbEntity);
                    }
                    else
                    {
                        dbEntity.IsLateEntry = true;
                        dbEntity.IsSkipEntry = false;
                    }
                }
            }

            if (insertList.Count > 0)
                PayrollDataContext.CumulativeLateEntries.InsertAllOnSubmit(insertList);

            PayrollDataContext.SubmitChanges();

            return status;

        }
        public static List<GetListForLateProcessingResult> GetLateProcessing(
          int branchid, int depId, int levelid, int desigId, int empId, int currentPage, DateTime start, DateTime end,
          int pageSize, string sortBy, ref int total, bool isLateMarkedOnly)
        {

            List<GetListForLateProcessingResult> list =
                PayrollDataContext.GetListForLateProcessing
                (empId, start.Date, end.Date, branchid, depId, levelid, desigId,
                currentPage, pageSize, sortBy, isLateMarkedOnly)
                .ToList();



            foreach (GetListForLateProcessingResult item in list)
            {

                try
                {
                    item.NepDate = CustomDate.ConvertEngToNep(new CustomDate(item.Date.Day, item.Date.Month, item.Date.Year, true))
                        .ToStringShortMonthName();
                }
                catch { }

                item.RecordID = item.EmployeeId + "," + item.Date.ToShortDateString();
            }



            if (list.Count > 0)
            {

                total = list[0].TotalRows.Value;

            }



            return list;
        }

        public static List<GetListForCumulativeLateProcessingResult> GetCumulativeLateProcessing(
         int branchid, int depId, int levelid, int desigId, int empId, int currentPage, DateTime start, DateTime end,
         int pageSize, string sortBy, ref int total, bool isLateMarkedOnly)
        {

            List<GetListForCumulativeLateProcessingResult> list =
                PayrollDataContext.GetListForCumulativeLateProcessing
                (empId, start.Date, end.Date, branchid, depId, levelid, desigId,
                currentPage, pageSize, sortBy, isLateMarkedOnly)
                .ToList();



            foreach (GetListForCumulativeLateProcessingResult item in list)
            {

                try
                {
                    item.NepDate = CustomDate.ConvertEngToNep(new CustomDate(item.Date.Day, item.Date.Month, item.Date.Year, true))
                        .ToStringShortMonthName();
                }
                catch { }

                item.RecordID = item.EmployeeId + "," + item.Date.ToShortDateString();
            }



            if (list.Count > 0)
            {

                total = list[0].TotalRows.Value;

            }



            return list;
        }
        public static DateTime? GetEmployeeCheckInOutTime(int employeeId, DateTime givenDate, string inoutType)
        {

            IQueryable<AttendanceGetInAndOutTimeResult> list =
                 PayrollDataContext.AttendanceGetInAndOutTime(employeeId,
                  new DateTime(givenDate.Year, givenDate.Month, givenDate.Day));

            if (list.ToList().Count > 0)
            {
                if (inoutType == "CheckIn")
                    return list.ToList()[0].InDate;
                else
                    return list.ToList()[0].OutDate;
            }

            return null;
        }


        public static void SetTimeRequestCount(ref int? pendingCount, ref int? recommendCount)
        {

            if (SessionManager.CurrentLoggedInEmployeeId != 0)
            {
                pendingCount = PayrollDataContext.TimeRequests.Where(x => x.Status == (int)AttendanceRequestStatusEnum.Saved
                    && x.RecommenderEmployeeId == SessionManager.CurrentLoggedInEmployeeId).Count();
                recommendCount = PayrollDataContext.TimeRequests.Where(x => x.Status == (int)AttendanceRequestStatusEnum.Recommended
                    && x.ApprovalEmployeeId == SessionManager.CurrentLoggedInEmployeeId).Count();
            }
            else
            {
                pendingCount = PayrollDataContext.TimeRequests.Where(x => x.Status == (int)AttendanceRequestStatusEnum.Saved).Count();
                recommendCount = PayrollDataContext.TimeRequests.Where(x => x.Status == (int)AttendanceRequestStatusEnum.Recommended).Count();
            }



        }

        //static List<AttendanceAbsent> absList = new List<AttendanceAbsent>();
        //static List<AttendanceAbsent> deleteList = new List<AttendanceAbsent>();

        //public static List<AttendanceAbsent> ABSLIST
        //{
        //    get { return absList; }
        //    set { absList = value; }
        //}
        //public static List<AttendanceAbsent> DELETELIST
        //{
        //    get { return deleteList; }
        //    set { deleteList = value; }
        //}




        public static bool IsAttendanceSavedForEmployee(int employeeId, int payrollPeriodId)
        {
            return PayrollDataContext.Attendences.Any(e => e.EmployeeId == employeeId && e.PayrollPeriodId == payrollPeriodId);
        }

        //public static GetEmployeeTimeResult GetEmployeeTime(int employeeId)
        //{
        //    return
        //         PayrollDataContext.GetEmployeeTime(employeeId, null, 0, "","").SingleOrDefault();
        //}
        public string CheckInOutTimeCheck(int cincout, int minute)
        {
            if (cincout == 1)
                return CheckInTimeCheck(minute);
            else if (cincout == 2)
                return CheckOutTimeCheck(minute);

            return "";
        }


        public string MinuteToHourMinuteConverter(int minute)
        {
            TimeSpan span = TimeSpan.FromMinutes(minute);
            return string.Format(" {0:D2}:{1:D2}",
                span.Hours,
                span.Minutes);
        }

        public string MinuteToHourMinuteConverterWithOutLabel(int minute)
        {
            TimeSpan span = TimeSpan.FromMinutes(minute);
            return string.Format(" {0:D2}:{1:D2}",
                span.Hours,
                span.Minutes);
        }
        public static string MinuteToHourMinuteConverterWithOutLabelDisplay(int minute)
        {
            TimeSpan span = TimeSpan.FromMinutes(minute);
            return string.Format(" {0:D2}:{1:D2}",
                span.Hours,
                span.Minutes);
        }

        /// <summary>
        /// This function return for more than 24 hour also, confirm above function does not return but being used in 24 ot
        /// </summary>
        /// <param name="minute"></param>
        /// <returns></returns>
        public static string MinuteWith24Hour(int minute)
        {
            TimeSpan span = TimeSpan.FromMinutes(minute);
            return string.Format(" {0:D2}:{1:D2}",
                (span.Days * 24) + span.Hours,
                span.Minutes);


        }

        public string CheckInTimeCheck(int minute)
        {
            string HourMinute = MinuteToHourMinuteConverter(Math.Abs(minute));
            string returnString = "";

            if (minute < 0)
            {
                returnString = "Early In" + HourMinute;
            }
            else if (minute > 0)
            {
                returnString = "Late In" + HourMinute;
            }
            else
            {
                returnString = "Right on time";
            }


            return returnString;
        }

        public string CheckOutTimeCheck(int minute)
        {
            string HourMinute = MinuteToHourMinuteConverter(Math.Abs(minute));
            string returnString = "";

            if (minute < 0)
            {
                returnString = "Early Out" + HourMinute;
            }
            else if (minute > 0)
            {
                returnString = "Late Out" + HourMinute;
            }
            else
            {
                returnString = "Right on time";
            }


            return returnString;
        }




        public static List<AttendanceExceptionResult> GetAttendanceExceptionList(int EID, int filterType, DateTime startdate, DateTime enddate, int BranchID, int CurrentPage, int PageSize, bool excudeHoliday
           )
        {
            string branchList = null;
            if (SessionManager.IsCustomRole)
                branchList = SessionManager.CustomRoleBranchIDList;
            else if (SessionManager.CurrentLoggedInEmployeeId != 0)
            {
                List<Branch> branches =
                        BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId)
                        .Where(x => x.BranchHeadId == SessionManager.CurrentLoggedInEmployeeId)
                        .Select(x => new Branch { BranchId = x.BranchId, Name = x.Name }).ToList();

                List<string> list2 = new List<string>();
                foreach (Branch dep in branches)
                {
                    list2.Add(dep.BranchId.ToString());
                }
                branchList = string.Join(",", list2.ToArray());
            }

            List<AttendanceExceptionResult> list = new List<AttendanceExceptionResult>();
            List<AttendanceExceptionResult> list1 = PayrollDataContext.AttendanceException(EID, filterType,
                startdate, enddate, BranchID, CurrentPage, PageSize, excudeHoliday, branchList).ToList();
            list = list1;
            DateTime tempDateTime = new DateTime();
            int count = list1.Count;
            for (int i = 0; i < count; i++)
            {
                AttendanceExceptionResult tempResult = new AttendanceExceptionResult();
                tempResult = list1[i];

                if (tempResult.DateEng != null)
                    tempResult.ActualDate = GetAppropriateDate(tempResult.DateEng.Value);

            }

            return list;
        }


        public static EEmployee GetEmployeeByID(int employeeId)
        {
            return PayrollDataContext.EEmployees.Where(x => x.EmployeeId == employeeId).FirstOrDefault();
        }

        public static void SaveOrUpdateAttendanceException(AttendanceCheckInCheckOut chkInOut)
        {
            PayrollDataContext.AttendanceCheckInCheckOuts.InsertOnSubmit(chkInOut);
            PayrollDataContext.SubmitChanges();
        }


        public static void sendDailyMailAbsent(List<AttendanceAllEmployeeReportResult> ListAbsentEmployes)
        {

            try
            {

                EmailContent dbMailContent = BLL.Manager.CommonManager.GetMailContent((int)EmailContentType.Absent);
                if (dbMailContent == null)
                {
                    CommonManager.WriteToLogFile("\n Absent Template not defined" + DateTime.Now.ToString() + "  : \n");
                    return;
                }

                string todayDate = GetCurrentDateAndTime().ToString("yyyy/MMM/d");
                string subject = dbMailContent.Subject.Replace("#TodayDate#", todayDate);
                int count = 0;
                string SenderEmail = Config.GetEmailSenderName;
                List<GetForDeviceMappingResult> _GetForDeviceMappingResult = PayrollDataContext.GetForDeviceMapping(
              -1, "", 0, 9999)
              .ToList();

                foreach (AttendanceAllEmployeeReportResult _item in ListAbsentEmployes)
                {

                    if (_item.Type == 2 && _item.DayValue != "WH/2")//weekly holiday dot not send mail
                    {

                    }
                    else
                    {
                        //skip for employee who is marked in setting device mapping
                        GetForDeviceMappingResult _DeviceMappingResult = _GetForDeviceMappingResult.SingleOrDefault(x => x.EmployeeId == _item.EmployeeId);
                        if (_DeviceMappingResult != null && _DeviceMappingResult.IsEnabledSkipLateEntry != null)
                        {
                            if (!_DeviceMappingResult.IsEnabledSkipLateEntry.Value)
                            {

                                string body = dbMailContent.Body.Replace("#TodayDate#", todayDate);
                                body = body.Replace("#Employee#", _item.EmplooyeeName);
                                string EmailTo = EmployeeManager.GetEmployeeEmailForLateEntry(_item.EmployeeId.Value);
                                if (string.IsNullOrEmpty(EmailTo))
                                {

                                    CommonManager.WriteToLogFile("\n Email address not found for Employee" + DateTime.Now.ToString() + "  : \n");
                                    // return;
                                }
                                else
                                {

                                    bool isSendSuccess = SMTPHelper.SendMailAsync(EmailTo, body, subject, "", SenderEmail);
                                    if (isSendSuccess)
                                        count++;
                                }
                            }
                        }
                    }
                }


                if (count > 0)
                {
                    CommonManager.WriteToLogFile("\n Email Send successfully for " + count + " Absent Employees" + DateTime.Now.ToString() + "  : \n");
                    return;
                }
                else
                {
                    CommonManager.WriteToLogFile("\n error while sending email" + DateTime.Now.ToString() + "  : \n");
                    return;
                }
            }

            catch (Exception exp)
            {
                CommonManager.WriteToLogFile("\n Error for : " + GetCurrentDateAndTime().Date.ToString() + "  : \n" + exp.ToString());
            }
        }
        public static void sendDailyMailLateEntry(List<GetLateListForEmailResult> ListLateEmployes)
        {
            string logMessage = "";
            try
            {
                EmailContent dbMailContent = BLL.Manager.CommonManager.GetMailContent((int)EmailContentType.LateIn);
                if (dbMailContent == null)
                {
                    logMessage = "\n Late Entry Template not defined" + DateTime.Now.ToString() + "  : \n";
                    Log.SaveAdditionalLog(logMessage, LogType.Schedular);
                    return;
                }
                string todayDate = GetCurrentDateAndTime().ToString("yyyy/MMM/d");
                string subject = dbMailContent.Subject.Replace("#TodayDate#", todayDate);
                int count = 0;
                string SenderEmail = Config.GetEmailSenderName;

                List<GetForDeviceMappingResult> _GetForDeviceMappingResult = PayrollDataContext.GetForDeviceMapping(-1, "", 0, 9999).ToList();
                foreach (GetLateListForEmailResult _item in ListLateEmployes)
                {
                    //if (_item.Type == 2 && _item.DayValue != "WH/2")//weekly holiday dot not send mail
                    //{

                    //}
                    //else
                    //{
                        //skip for LateEntry from the setting in device mapping
                          GetForDeviceMappingResult _DeviceMappingResult = _GetForDeviceMappingResult.SingleOrDefault(x => x.EmployeeId == _item.EmployeeId);
                          if (_DeviceMappingResult != null && _DeviceMappingResult.IsEnabledSkipLateEntry != null)
                          {
                              if (!_DeviceMappingResult.IsEnabledSkipLateEntry.Value)
                              {
                                  //if (_item.InRemarksText == null)
                                  //    _item.InRemarksText = "";

                                  string body = dbMailContent.Body.Replace("#LateHour#", MinuteToHourMinuteConverterWithOutLabelDisplay(_item.LateMin.Value));
                                  body = body.Replace("#TodayDate#", todayDate);
                                  body = body.Replace("#Employee#", _item.NAME);
                                  string EmailTo = EmployeeManager.GetEmployeeEmailForLateEntry(_item.EmployeeId);
                                  if (string.IsNullOrEmpty(EmailTo))
                                  {
                                      
                                      logMessage = "\n Email address not found for Employee " + _item.NAME + " " + DateTime.Now.ToString() + "  : \n";
                                      Log.SaveAdditionalLog(logMessage, LogType.Schedular);
                                  }
                                  else
                                  {
                                      bool isSendSuccess = SMTPHelper.SendMailAsyncWebServiceOnly(EmailTo, body, subject, "", SenderEmail);
                                      if (isSendSuccess)
                                          count++;
                                  }
                              }
                          }
                 //   }
                           
                    
                }
                if (count > 0)
                {
                    logMessage = "\n Email Send successfully for " + count + "Late Entry Employees " + DateTime.Now.ToString() + "  : \n";
                    Log.SaveAdditionalLog(logMessage, LogType.Schedular);
                    return;
                }
                else
                {
                    logMessage = "\n error while sending email " + DateTime.Now.ToString() + "  : \n";
                    Log.SaveAdditionalLog(logMessage, LogType.Schedular);
                    return;
                }
            }
            catch (Exception exp)
            {
                logMessage = "\n Error for : " + GetCurrentDateAndTime().Date.ToString() + "  : \n" + exp.ToString();
                Log.SaveAdditionalLog(logMessage, LogType.Schedular);
            }
        }

        public static Status InsertUpdateCheckIN(AttendanceCheckInCheckOut chkInOut)
        {
            Status status = new Status();

            PayrollDataContext.AttendanceCheckInCheckOuts.InsertOnSubmit(chkInOut);
            PayrollDataContext.SubmitChanges();


            status.IsSuccess = true;
            return status;
        }


        public static Status InsertUpdateCheckOut(AttendanceCheckInCheckOut chkInOut)
        {
            Status status = new Status();
            // AttendanceGetInAndOutTimeResult tempInst = new AttendanceGetInAndOutTimeResult();
            //  tempInst = AttendanceManager.getAttendanceofGivenDate(SessionManager.CurrentLoggedInEmployeeId, CommonManager.GetCurrentDateAndTime());

            //if (tempInst != null && tempInst.OutID != null && tempInst.OutID != Guid.Empty && PayrollDataContext.AttendanceCheckInCheckOuts.Any(x => x.ChkInOutID == tempInst.OutID.Value))
            //{
            //    AttendanceCheckInCheckOut dbEntity = new AttendanceCheckInCheckOut();
            //    dbEntity = PayrollDataContext.AttendanceCheckInCheckOuts.FirstOrDefault(x => x.ChkInOutID == tempInst.OutID.Value);
            //    dbEntity.DateTime = chkInOut.DateTime;
            //    dbEntity.Date = dbEntity.DateTime.Date;
            //    chkInOut.ChkInOutID = dbEntity.ChkInOutID;
            //    PayrollDataContext.SubmitChanges();
            //}
            //else
            //{
            PayrollDataContext.AttendanceCheckInCheckOuts.InsertOnSubmit(chkInOut);
            PayrollDataContext.SubmitChanges();
            // }

            status.IsSuccess = true;
            return status;
        }

        public static string GetCheckInOutTimeToModify(int employeeId, DateTime chkinoutTime, int io)
        {
            string InOutTime = "";


            if (io == 1)
            {

                var tempValue1 = (from v1 in PayrollDataContext.AttendanceCheckInCheckOuts
                                  join v2 in PayrollDataContext.AttendanceMappings on v1.DeviceID equals v2.DeviceId
                                  where v2.EmployeeId == employeeId && v1.Date == chkinoutTime.Date
                                     &&
                                     (
                                         (CommonManager.CompanySetting.AttendanceInOut && v1.InOutMode == 0)
                                         ||
                                         CommonManager.CompanySetting.AttendanceInOut == false
                                     )
                                  orderby v1.DateTime.TimeOfDay ascending
                                  select v1);

                if (tempValue1.Count() > 0)
                    InOutTime = (tempValue1.FirstOrDefault().DateTime.TimeOfDay).ToString() + "#" + (tempValue1.FirstOrDefault().ChkInOutID.ToString() + "#" + tempValue1.FirstOrDefault().ModificationRemarks);

            }
            else if (io == 2)
            {
                var tempValue2 = (from v1 in PayrollDataContext.AttendanceCheckInCheckOuts
                                  join v2 in PayrollDataContext.AttendanceMappings on v1.DeviceID equals v2.DeviceId
                                  where v2.EmployeeId == employeeId && v1.Date == chkinoutTime.Date
                                    &&
                                     (
                                         (CommonManager.CompanySetting.AttendanceInOut && v1.InOutMode == 1)
                                         ||
                                         CommonManager.CompanySetting.AttendanceInOut == false
                                     )
                                  orderby v1.DateTime.TimeOfDay descending
                                  select v1);
                if (tempValue2.Count() > 0)
                    InOutTime = (tempValue2.FirstOrDefault().DateTime.TimeOfDay).ToString() + "#" + (tempValue2.FirstOrDefault().ChkInOutID.ToString() + "#" + tempValue2.FirstOrDefault().ModificationRemarks);

            }



            return InOutTime;
        }




        public static AttendanceCheckInCheckOut GetCheckInOutTimeForComment(string deviceID, DateTime IODate, int io)
        {
            AttendanceCheckInCheckOut thisAttendanceCheckInCheckOut = new AttendanceCheckInCheckOut();
            if (io == 0)
            {
                return PayrollDataContext.AttendanceCheckInCheckOuts
                    //.OrderBy(x => x.DateTime)
                        .FirstOrDefault(x => x.DeviceID == deviceID && x.Date == IODate.Date && x.InOutMode == io && x.AuthenticationType == (byte)TimeAttendanceAuthenticationType.SelfManualEntryFromSoftware);
            }
            else if (io == 1)
            {
                return PayrollDataContext.AttendanceCheckInCheckOuts
                    //.OrderByDescending(x => x.DateTime)
                    .FirstOrDefault(x => x.DeviceID == deviceID && x.Date == IODate.Date && x.InOutMode == io && x.AuthenticationType == (byte)TimeAttendanceAuthenticationType.SelfManualEntryFromSoftware);
            }
            return thisAttendanceCheckInCheckOut;
        }




        public static string DeleteModifiedTime(int employeeId, DateTime chkinoutTime)
        {
            var tempValue = (from v1 in PayrollDataContext.AttendanceCheckInCheckOuts
                             where v1.DeviceID == employeeId.ToString() && v1.ModifiedBy != null
                                   && v1.Date == chkinoutTime.Date
                             select v1);
            DAL.AttendanceCheckInCheckOut attecincout = new AttendanceCheckInCheckOut();
            foreach (var v2 in tempValue)
            {
                attecincout = (AttendanceCheckInCheckOut)v2;
                PayrollDataContext.AttendanceCheckInCheckOuts.DeleteOnSubmit(attecincout);
            }


            try
            {



                PayrollDataContext.SubmitChanges();

            }
            catch (Exception ex)
            {
                Log.log("Error while deleting Modified Time: ", ex);
                X.Msg.Alert("Warning", "Error while deleting Modified Time").Show();
                return "false";

            }

            return "true";
        }

        public static List<AttendanceInOutTime> GetEmployeeTimeForExport()
        {
            List<AttendanceInOutTime> list = new List<AttendanceInOutTime>();

            if (PayrollDataContext.AttendanceInOutTimes.Any(x => x.DateEng != null))
            {
                list =
                PayrollDataContext.AttendanceInOutTimes.Where(x => x.DateEng != null)
                .OrderByDescending(x => x.DateEng).ToList();
            }
            else
            {
                list =
                    PayrollDataContext.AttendanceInOutTimes//.Where(x => x.EmployeeId != null)
                    .OrderBy(x => x.EmployeeId).ToList();
            }

            List<AttendanceInOutTime> times = new List<AttendanceInOutTime>();

            foreach (AttendanceInOutTime time in list)
            {
                if (times.Any(x => x.ShiftID == time.ShiftID) == false)
                    times.Add(time);
            }

            return times;
        }

        public static Status DeleteTime(int employeeId, DateTime chkinoutTime, bool isInTime)
        {
            Status status = new Status();

            DAL.AttendanceMapping map = BLL.BaseBiz.PayrollDataContext.AttendanceMappings
                 .SingleOrDefault(x => x.EmployeeId == employeeId);

            if (map == null)
            {
                status.ErrorMessage = "Mapping not defined.";
                return status;
            }

            AttendanceCheckInCheckOut dbEntity = null;

            if (isInTime)
            {
                dbEntity = (from v1 in PayrollDataContext.AttendanceCheckInCheckOuts
                            where v1.DeviceID == map.DeviceId && v1.Date == chkinoutTime.Date
                            orderby v1.DateTime
                            select v1).FirstOrDefault();
            }
            else
            {
                dbEntity = (from v1 in PayrollDataContext.AttendanceCheckInCheckOuts
                            where v1.DeviceID == map.DeviceId && v1.Date == chkinoutTime.Date
                            orderby v1.DateTime descending
                            select v1).FirstOrDefault();
            }

            if (dbEntity != null)
            {
                string authType = "";
                if (dbEntity.AuthenticationType != null)
                    authType = ((TimeAttendanceAuthenticationType)dbEntity.AuthenticationType.Value).ToString();

                // place log before deleting
                PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(employeeId, (byte)MonetoryChangeLogTypeEnum.AttendanceTimeDeleted
                    , authType + " : " + dbEntity.IPAddress, dbEntity.DateTime.ToString(), "", LogActionEnum.Delete));

                PayrollDataContext.AttendanceCheckInCheckOuts.DeleteOnSubmit(dbEntity);
                PayrollDataContext.SubmitChanges();
            }
            else
            {
                status.ErrorMessage = "No time found.";
                return status;
            }



            return status;
        }
        public static bool IsAttendanceDeviceIntegrated()
        {
            return PayrollDataContext.AttendanceInOutTimes.Any();
        }

        public static AttendanceInOutTime GetDefaultOfficeTime(int ShiftId)
        {
            return PayrollDataContext.AttendanceInOutTimes.Where(x => x.ShiftID == ShiftId)
                .OrderByDescending(x => x.DateEng).FirstOrDefault();
        }
        


        public static void SaveEmployeeTime(List<AttendanceInOutTime> list, DateTime datetime)
        {


            foreach (AttendanceInOutTime time in list)
            {
                time.DateEng = datetime;

                AttendanceInOutTime dbTime = PayrollDataContext.AttendanceInOutTimes
                    .FirstOrDefault(x => x.ShiftID == time.ShiftID && x.DateEng == datetime);
                if (dbTime == null)
                    PayrollDataContext.AttendanceInOutTimes.InsertOnSubmit(time);
                else
                    PayrollDataContext.AttendanceInOutTimes.DeleteOnSubmit(dbTime);

                //PayrollDataContext.AttendanceInOutTimes.DeleteAllOnSubmit
                //    (PayrollDataContext.AttendanceInOutTimes.Where(x => x.EmployeeId == time.EmployeeId || (x.EmployeeId == null && time.EmployeeId == null)));
            }

            PayrollDataContext.AttendanceInOutTimes.InsertAllOnSubmit(list);

            PayrollDataContext.SubmitChanges();
        }

        public static List<GetAbsentListForEmailResult> GetAbsentListForEmail(DateTime StartDate, DateTime EndDate)
        {
            List<GetAbsentListForEmailResult> _list = new List<GetAbsentListForEmailResult>();
            _list = PayrollDataContext.GetAbsentListForEmail(StartDate, EndDate).ToList();
            if (_list.Any())
                return _list.OrderByDescending(x => x.SupervisorEmail).ToList();
            else
                return _list;
        }


        public static List<AbsentEmpIDBO> GetAbsentEmpIDListNew(DateTime StartDate, DateTime EndDate, List<GetAbsentListForEmailResult> _listAbsEmployee)
        {
            List<AbsentEmpIDBO> _list = new List<AbsentEmpIDBO>();
            //  List<GetAbsentListForEmailResult> _listAbsEmployee = PayrollDataContext.GetAbsentListForEmail(StartDate, EndDate).ToList();
            if (_listAbsEmployee.Any())
            {
                int OldEmployeeID = 0;
                foreach (GetAbsentListForEmailResult item in _listAbsEmployee)
                {
                    if (OldEmployeeID != item.EmployeeId)
                    {
                        AbsentEmpIDBO _AbsentEmpIDBO = new AbsentEmpIDBO();
                        _AbsentEmpIDBO.EmpID = item.EmployeeId;
                        _AbsentEmpIDBO.Name = item.NAME;
                        _AbsentEmpIDBO.Email = item.Email;
                        _list.Add(_AbsentEmpIDBO);
                    }
                    OldEmployeeID = item.EmployeeId;
                }
            }

            _list.Select(x => x.EmpID).Distinct();

            return _list;
        }

        public static List<AbsentEmpIDBO> GetAbsentEmpIDList(DateTime StartDate, DateTime EndDate)
        {
            List<AbsentEmpIDBO> _list = new List<AbsentEmpIDBO>();
            List<GetAbsentListForEmailResult> _listAbsEmployee = PayrollDataContext.GetAbsentListForEmail(StartDate, EndDate).ToList();
            if (_listAbsEmployee.Any())
            {
                int OldEmployeeID = 0;
                foreach (GetAbsentListForEmailResult item in _listAbsEmployee)
                {
                    if (OldEmployeeID != item.EmployeeId)
                    {
                        AbsentEmpIDBO _AbsentEmpIDBO = new AbsentEmpIDBO();
                        _AbsentEmpIDBO.EmpID = item.EmployeeId;
                        _AbsentEmpIDBO.Name = item.NAME;
                        _AbsentEmpIDBO.Email = item.Email;
                        _list.Add(_AbsentEmpIDBO);
                    }
                    OldEmployeeID = item.EmployeeId;
                }
            }

            _list.Select(x => x.EmpID).Distinct();

            return _list;
        }



        public static List<SupervisorEmails> GetSupervisorEmailListForMailNew(DateTime StartDate, DateTime EndDate, List<GetAbsentListForEmailResult> _listAbsEmployee)
        {
            List<SupervisorEmails> _list = new List<SupervisorEmails>();
            //  List<GetAbsentListForEmailResult> _listAbsEmployee = PayrollDataContext.GetAbsentListForEmail(StartDate, EndDate).ToList();
            string OldEmail = string.Empty;
            if (_listAbsEmployee.Any())
            {
                foreach (GetAbsentListForEmailResult item in _listAbsEmployee)
                {
                    if (OldEmail != item.SupervisorEmail)
                    {
                        SupervisorEmails _SupervisorEmails = new SupervisorEmails();
                        _SupervisorEmails.SupervisorEmail = item.SupervisorEmail;
                        _list.Add(_SupervisorEmails);
                    }
                    OldEmail = item.SupervisorEmail;
                }
            }
            return _list;
        }


        public static List<SupervisorEmails> GetSupervisorEmailListForLateEmpNew(DateTime StartDate, DateTime EndDate, List<GetLateListForEmailResult> _listLateEmployee)
        {
            List<SupervisorEmails> _list = new List<SupervisorEmails>();
            string OldEmail = string.Empty;
            if (_listLateEmployee.Any())
            {
                foreach (GetLateListForEmailResult item in _listLateEmployee)
                {
                    if (OldEmail != item.SupervisorEmail)
                    {
                        SupervisorEmails _SupervisorEmails = new SupervisorEmails();
                        _SupervisorEmails.SupervisorEmail = item.SupervisorEmail;
                        _list.Add(_SupervisorEmails);
                    }
                    OldEmail = item.SupervisorEmail;
                }
            }
            return _list;
        }

        public static List<SupervisorEmails> GetSupervisorEmailListForMail(DateTime StartDate, DateTime EndDate)
        {
            List<SupervisorEmails> _list = new List<SupervisorEmails>();
            List<GetAbsentListForEmailResult> _listAbsEmployee = PayrollDataContext.GetAbsentListForEmail(StartDate, EndDate).ToList();
            string OldEmail = string.Empty;
            if (_listAbsEmployee.Any())
            {
                foreach (GetAbsentListForEmailResult item in _listAbsEmployee)
                {
                    if (OldEmail != item.SupervisorEmail)
                    {
                        SupervisorEmails _SupervisorEmails = new SupervisorEmails();
                        _SupervisorEmails.SupervisorEmail = item.SupervisorEmail;
                        _list.Add(_SupervisorEmails);
                    }
                    OldEmail = item.SupervisorEmail;
                }
            }
            return _list;
        }




        public static List<AttendanceAllEmployeeReportResult> GetAttendanceDailyReportWebServiceCalling(DateTime filterDate, int Status, string BranchID)
        {
            //AttendanceManager attendanceManager = new AttendanceManager();
            // PayrollDataContext payrollDataContext = new PayrollDataContext();

            //  List<AttendanceAllEmployeeReportResult> returnList = new List<AttendanceAllEmployeeReportResult>();
            List<AttendanceAllEmployeeReportResult> attendanceList = new List<AttendanceAllEmployeeReportResult>();

            string branchList = null;
            //if (SessionManager.IsCustomRole)
            //    branchList = SessionManager.CustomRoleBranchIDList;

            AttendanceManager mgr = new AttendanceManager();

            attendanceList = PayrollDataContext.AttendanceAllEmployeeReport(filterDate, BranchID, branchList).ToList();

            for (int i = 0; i < attendanceList.Count; i++)
            {
                //attendanceList[i].Ou
                if (string.IsNullOrEmpty(attendanceList[i].InRemarks.ToString()) != true || string.IsNullOrEmpty(attendanceList[i].RefinedOutRemarks.ToString()) != true)
                {
                    if (attendanceList[i].WorkHour != null)
                        attendanceList[i].RefinedWorkHour = new AttendanceManager().MinuteToHourMinuteConverter(Math.Abs(int.Parse(attendanceList[i].WorkHour.ToString())));
                }

                if (attendanceList[i].InRemarks != null)
                    attendanceList[i].InRemarksText = mgr.CheckInOutTimeCheck(1, attendanceList[i].InRemarks.Value);
                if (attendanceList[i].OutRemarks != null)
                    attendanceList[i].OutRemarksText = mgr.CheckInOutTimeCheck(2, attendanceList[i].OutRemarks.Value);
            }

            if (Status != 0 && Status != 4)
            {

                return attendanceList.Where(x => x.AtteState == Status).ToList();

            }
            else if (Status == 4)
            {
                return attendanceList.Where(x => x.AtteState == Status || x.AtteState == 2).ToList();
            }
            else// if (Status == 0)
                return attendanceList;

            // return returnList;
        }
        public static List<AttendanceAllEmployeeReportResult> GetAttendanceDailyReport(DateTime filterDate, int Status, string BranchID)
        {
            //AttendanceManager attendanceManager = new AttendanceManager();
            // PayrollDataContext payrollDataContext = new PayrollDataContext();

            //  List<AttendanceAllEmployeeReportResult> returnList = new List<AttendanceAllEmployeeReportResult>();
            List<AttendanceAllEmployeeReportResult> attendanceList = new List<AttendanceAllEmployeeReportResult>();

            string branchList = null;
            if (SessionManager.IsCustomRole)
                branchList = SessionManager.CustomRoleBranchIDList;

            AttendanceManager mgr = new AttendanceManager();

            attendanceList = PayrollDataContext.AttendanceAllEmployeeReport(filterDate, BranchID, branchList).ToList();

            for (int i = 0; i < attendanceList.Count; i++)
            {
                //attendanceList[i].Ou
                if (string.IsNullOrEmpty(attendanceList[i].InRemarks.ToString()) != true || string.IsNullOrEmpty(attendanceList[i].RefinedOutRemarks.ToString()) != true)
                {
                    if (attendanceList[i].WorkHour != null)
                        attendanceList[i].RefinedWorkHour = new AttendanceManager().MinuteToHourMinuteConverter(Math.Abs(int.Parse(attendanceList[i].WorkHour.ToString())));
                }

                if (attendanceList[i].InRemarks != null)
                    attendanceList[i].InRemarksText = mgr.CheckInOutTimeCheck(1, attendanceList[i].InRemarks.Value);
                if (attendanceList[i].OutRemarks != null)
                    attendanceList[i].OutRemarksText = mgr.CheckInOutTimeCheck(2, attendanceList[i].OutRemarks.Value);

                //attendanceList[i].InIPAddress = PayrollDataContext.AttendanceGetInAndOutTime(attendanceList[i].EmployeeId, filterDate).ToList()[0].InIPAddress;
                //attendanceList[i].OutIPAddress = PayrollDataContext.AttendanceGetInAndOutTime(attendanceList[i].EmployeeId, filterDate).ToList()[0].OutIPAddress;

                
            }

            if (Status != 0 && Status != 4)
            {

                return attendanceList.Where(x => x.AtteState == Status).ToList();

            }
            else if (Status == 4)
            {
                return attendanceList.Where(x => x.AtteState == Status || x.AtteState == 2).ToList();
            }
            else// if (Status == 0)
                return attendanceList;

            // return returnList;
        }

        public static bool UpdateCheckInCheckOutTime(string checkInID, string checkOutID, string employeeID, DateTime CheckInDateTime, DateTime CheckOutDateTime, string EditNote1, string EditNote2)
        {

            Guid CheckInID = new Guid();

            DAL.AttendanceMapping map = BLL.BaseBiz.PayrollDataContext.AttendanceMappings
                   .SingleOrDefault(x => x.EmployeeId == int.Parse(employeeID));


            if (string.IsNullOrEmpty(checkInID))
                CheckInID = Guid.NewGuid();
            else
                CheckInID = new Guid(checkInID);

            Guid CheckOutID = new Guid();

            if (string.IsNullOrEmpty(checkOutID))
                CheckOutID = Guid.NewGuid();
            else
                CheckOutID = new Guid(checkOutID);

            // validate if DeviceId,DateTime,InOutMode already exists


            var tempValue1 = (from v1 in PayrollDataContext.AttendanceCheckInCheckOuts
                              where v1.ChkInOutID == CheckInID
                              orderby v1.DateTime.TimeOfDay ascending
                              select v1).FirstOrDefault();


            var tempValue2 = (from v1 in PayrollDataContext.AttendanceCheckInCheckOuts
                              where v1.ChkInOutID == CheckOutID
                              orderby v1.DateTime.TimeOfDay descending
                              select v1).FirstOrDefault();

            //insert check in
            if (tempValue1 == null)
            {
                AttendanceCheckInCheckOut checkIn = new AttendanceCheckInCheckOut();
                checkIn.ChkInOutID = CheckInID;
                checkIn.DeviceID = map.DeviceId; ;
                checkIn.AuthenticationType = (byte)TimeAttendanceAuthenticationType.SupervisorAdminModified;
                checkIn.InOutMode = 0;


                checkIn.DateTime = new DateTime(CheckInDateTime.Year, CheckInDateTime.Month, CheckInDateTime.Day, CheckInDateTime.Hour, CheckInDateTime.Minute, 0);
                checkIn.Date = checkIn.DateTime.Date;
                checkIn.ModifiedBy = SessionManager.User.UserID;
                checkIn.ModifiedOn = BLL.BaseBiz.GetCurrentDateAndTime();
                checkIn.ModificationRemarks = EditNote1;

                PayrollDataContext.AttendanceCheckInCheckOuts.InsertOnSubmit(checkIn);
            }

            if (tempValue2 == null)
            {
                AttendanceCheckInCheckOut checkOut = new AttendanceCheckInCheckOut();
                checkOut.ChkInOutID = CheckOutID;
                checkOut.DeviceID = map.DeviceId; ;
                checkOut.AuthenticationType = (byte)TimeAttendanceAuthenticationType.SupervisorAdminModified;
                checkOut.InOutMode = 1;


                checkOut.DateTime = new DateTime(CheckOutDateTime.Year, CheckOutDateTime.Month, CheckOutDateTime.Day, CheckOutDateTime.Hour, CheckOutDateTime.Minute, 0);
                checkOut.Date = checkOut.DateTime.Date;
                checkOut.ModifiedBy = SessionManager.User.UserID;
                checkOut.ModifiedOn = BLL.BaseBiz.GetCurrentDateAndTime();
                checkOut.ModificationRemarks = EditNote2;

                PayrollDataContext.AttendanceCheckInCheckOuts.InsertOnSubmit(checkOut);
            }

            if (checkOutID == checkInID)
            {
                if (tempValue1 != null)
                {
                    tempValue1.DateTime = CheckInDateTime;
                    tempValue1.Date = tempValue1.DateTime.Date;
                    tempValue1.ModifiedBy = SessionManager.User.UserID;
                    tempValue1.ModifiedOn = BLL.BaseBiz.GetCurrentDateAndTime();
                    if (!string.IsNullOrEmpty(EditNote1))
                        tempValue1.ModificationRemarks = EditNote1;
                }
            }
            else
            {

                if (tempValue1 != null)
                {
                    tempValue1.DateTime = CheckInDateTime;
                    tempValue1.Date = tempValue1.DateTime.Date;
                    tempValue1.ModifiedBy = SessionManager.User.UserID;
                    tempValue1.ModifiedOn = BLL.BaseBiz.GetCurrentDateAndTime();
                    if (!string.IsNullOrEmpty(EditNote1))
                        tempValue1.ModificationRemarks = EditNote1;

                }

                if (tempValue2 != null)
                {
                    tempValue2.DateTime = CheckOutDateTime;
                    tempValue2.Date = tempValue2.DateTime.Date;
                    tempValue2.ModifiedBy = SessionManager.User.UserID;
                    tempValue2.ModifiedOn = BLL.BaseBiz.GetCurrentDateAndTime();
                    if (!string.IsNullOrEmpty(EditNote2))
                        tempValue2.ModificationRemarks = EditNote2;
                }
            }

            PayrollDataContext.SubmitChanges();

            return true;
        }




        public static List<AttendanceAbsent> GetAbsenteesAddList(List<AttendanceExceptionResult> entryLineObj)
        {
            List<AttendanceAbsent> retList = new List<AttendanceAbsent>();
            AttendanceAbsent attendanceAbsentsBO;
            foreach (var val1 in entryLineObj)
            {
                attendanceAbsentsBO = new AttendanceAbsent();
                if (string.IsNullOrEmpty(val1.InTime.ToString()) && string.IsNullOrEmpty(val1.OutTime.ToString()))
                {
                    var value1 = (from val2 in PayrollDataContext.AttendanceAbsents
                                  where val2.EmployeeId == val1.EmployeeId && val2.DateEng.Value.Date == val1.DateEng.Value.Date
                                  select new { val2 }).FirstOrDefault();

                    if (value1 == null)
                    {
                        if (val1.EmployeeId != null)
                            attendanceAbsentsBO.EmployeeId = val1.EmployeeId.Value;
                        if (val1.DateEng != null)
                        {
                            attendanceAbsentsBO.DateEng = val1.DateEng.Value;
                            attendanceAbsentsBO.Date = GetAppropriateDate(attendanceAbsentsBO.DateEng.Value);
                        }

                        retList.Add(attendanceAbsentsBO);
                    }

                }
            }

            return retList;
        }





        public static List<AttendanceAbsent> AbsenteesDeleteList(List<AttendanceExceptionResult> entryLineObj)
        {
            List<AttendanceAbsent> retList = new List<AttendanceAbsent>();
            AttendanceAbsent attendanceAbsentsBO;
            foreach (var val1 in entryLineObj)
            {
                attendanceAbsentsBO = new AttendanceAbsent();
                if (
                    (string.IsNullOrEmpty(val1.InTime.ToString()) && string.IsNullOrEmpty(val1.OutTime.ToString()))
                    ||
                    val1.DayValue == "ABS"

                    )
                {
                    var value1 = (from val2 in PayrollDataContext.AttendanceAbsents
                                  where val2.EmployeeId == val1.EmployeeId && val2.DateEng.Value.Date == val1.DateEng.Value.Date
                                  select new { val2 }).FirstOrDefault();

                    if (value1 != null)
                    {
                        if (val1.EmployeeId != null)
                            attendanceAbsentsBO.EmployeeId = value1.val2.EmployeeId;
                        if (val1.DateEng != null)
                            attendanceAbsentsBO.DateEng = value1.val2.DateEng;

                        retList.Add(attendanceAbsentsBO);
                    }

                }
            }

            return retList;

        }

        public static void AddAbsentToDB(List<AttendanceAbsent> absList)
        {
            //Remaining : exception saving abs restrict one month saving only & should be the last period only



            // 1 . Save in Attendance table
            foreach (var val1 in absList)
            {
                val1.CreatedBy = SessionManager.User.UserID;
                val1.CreatedOn = GetCurrentDateAndTime();
                PayrollDataContext.AttendanceAbsents.InsertOnSubmit(val1);
            }
            PayrollDataContext.SubmitChanges();


            DateTime? fromDate = null;
            int? savePayrollPeriodId = 0;
            List<int> empIDList = absList.Select(x => x.EmployeeId.Value).Distinct().ToList();

            // 2 . recalculte atte for saved 
            foreach (int employeeId in empIDList)
            {

                fromDate = absList.Where(x => x.EmployeeId == employeeId).OrderBy(x => x.DateEng).Select(x => x.DateEng).FirstOrDefault();
                List<AttendanceAbsent> absentList = new List<AttendanceAbsent>();

                if (fromDate != null &&
                    PayrollDataContext.CheckWhetherAttendanceIsSavedOrNot(fromDate, SessionManager.CurrentCompanyId, employeeId).Value)
                {
                    // get saved PayrollPeriodId
                    savePayrollPeriodId = PayrollDataContext.PayrollPeriods.Where(x => fromDate >= x.StartDateEng && fromDate <= x.EndDateEng)
                        .OrderByDescending(x => x.PayrollPeriodId).Select(x => x.PayrollPeriodId).FirstOrDefault();


                    if (savePayrollPeriodId != null && savePayrollPeriodId != 0)
                    {
                        List<AttendanceImportEmployee> atteList = new List<AttendanceImportEmployee>();
                        atteList.Add(new AttendanceImportEmployee { EmployeeId = employeeId });
                        absentList = absList.Where(x => x.EmployeeId == employeeId).ToList();

                        AttendanceImportEmployeeLeave leave = new AttendanceImportEmployeeLeave();
                        foreach (AttendanceAbsent absentDay in absentList)
                        {
                            DateTime date = absentDay.DateEng.Value;
                            leave = new AttendanceImportEmployeeLeave();

                            leave.TakenDate = date;
                            leave.EmployeeId = employeeId;


                            leave.LeaveTypeId = 0;
                            leave.IsAbsentLeave = true;
                            leave.LeaveAbbr = "ABS";
                            atteList[0].leaves.Add(leave);

                        }


                        string msg = "";
                        int count = 0;

                        //recalculate leave balance
                        EmployeeManager.SaveImportedAttendance(atteList, savePayrollPeriodId.Value, ref msg, ref count, null);
                        // Change lapse/encash                       
                        LeaveAttendanceManager.CallToResaveAtte(savePayrollPeriodId.Value, employeeId.ToString(), null);

                    }
                }
            }

        }

        public static void DeleteAbsentFromDB(List<AttendanceAbsent> deleteList)
        {

            List<AttendanceAbsent> dbList = new List<AttendanceAbsent>();

            foreach (AttendanceAbsent item in deleteList)
            {
                AttendanceAbsent db = PayrollDataContext.AttendanceAbsents
                    .FirstOrDefault(x => x.EmployeeId == item.EmployeeId && x.DateEng == item.DateEng);

                if (db != null)
                    dbList.Add(db);
            }

            PayrollDataContext.AttendanceAbsents.DeleteAllOnSubmit(dbList);
            PayrollDataContext.SubmitChanges();



            DateTime? fromDate = null;
            int? savePayrollPeriodId = 0;
            List<int> empIDList = deleteList.Select(x => x.EmployeeId.Value).Distinct().ToList();

            // 2 . recalculte atte for saved 
            foreach (int employeeId in empIDList)
            {

                fromDate = deleteList.Where(x => x.EmployeeId == employeeId).OrderBy(x => x.DateEng).Select(x => x.DateEng).FirstOrDefault();
                List<AttendanceAbsent> absentList = new List<AttendanceAbsent>();

                if (fromDate != null &&
                    PayrollDataContext.CheckWhetherAttendanceIsSavedOrNot(fromDate, SessionManager.CurrentCompanyId, employeeId).Value)
                {
                    // get saved PayrollPeriodId
                    savePayrollPeriodId = PayrollDataContext.PayrollPeriods.Where(x => fromDate >= x.StartDateEng && fromDate <= x.EndDateEng)
                        .OrderByDescending(x => x.PayrollPeriodId).Select(x => x.PayrollPeriodId).FirstOrDefault();


                    if (savePayrollPeriodId != null && savePayrollPeriodId != 0)
                    {
                        List<AttendanceImportEmployee> atteList = new List<AttendanceImportEmployee>();
                        atteList.Add(new AttendanceImportEmployee { EmployeeId = employeeId });
                        absentList = deleteList.Where(x => x.EmployeeId == employeeId).ToList();

                        AttendanceImportEmployeeLeave leave = new AttendanceImportEmployeeLeave();
                        foreach (AttendanceAbsent absentDay in absentList)
                        {
                            DateTime date = absentDay.DateEng.Value;
                            leave = new AttendanceImportEmployeeLeave();

                            leave.TakenDate = date;
                            leave.EmployeeId = employeeId;

                            leave.NoLeave = true;
                            //leave.LeaveTypeId = 0;
                            //leave.IsAbsentLeave = true;
                            //leave.LeaveAbbr = "ABS";

                            atteList[0].leaves.Add(leave);

                        }


                        string msg = "";
                        int count = 0;

                        //recalculate leave balance
                        EmployeeManager.SaveImportedAttendance(atteList, savePayrollPeriodId.Value, ref msg, ref count, null);
                        // Change lapse/encash                       
                        LeaveAttendanceManager.CallToResaveAtte(savePayrollPeriodId.Value, employeeId.ToString(), null);

                    }
                }
            }
        }

        public static int getShiftIDByItsName(string ShiftName)
        {
            if (PayrollDataContext.EWorkShifts.Any(x => x.Name.Trim() == ShiftName))
            {
                return PayrollDataContext.EWorkShifts.Where(x => x.Name.Trim() == ShiftName).FirstOrDefault().WorkShiftId;
            }
            return 0;
        }

        internal static EWorkShift GetShiftByID(int ShiftID)
        {
            return PayrollDataContext.EWorkShifts.Where(x => x.WorkShiftId == ShiftID).FirstOrDefault();
        }


        public static EWorkShift GetDefaultShift()
        {
            return PayrollDataContext.EWorkShifts.SingleOrDefault(x => x.IsDefault == true);
        }

        public static AttendanceCheckInCheckOut getCheckINOutByID(string ID)
        {
            return PayrollDataContext.AttendanceCheckInCheckOuts.FirstOrDefault(x => x.ChkInOutID == new Guid(ID));
        }


        public static DateTime GetAttendanceCommentThreshold()
        {
            DateTime thisDateTime = CommonManager.GetCurrentDateAndTime().Date;
            try
            {
                if (CommonManager.Setting.AttendanceCommentByEmpThreshold == (int)AttendanceCommentThresholdEnum.CurrentDay)
                {
                    return thisDateTime.AddDays(-1);
                }
                else if (CommonManager.Setting.AttendanceCommentByEmpThreshold == (int)AttendanceCommentThresholdEnum.ThisWeek)
                {
                    return thisDateTime.AddDays(-8);
                }
                else if (CommonManager.Setting.AttendanceCommentByEmpThreshold == (int)AttendanceCommentThresholdEnum.ThisMonth)
                {
                    return new DateTime(thisDateTime.Year, thisDateTime.Month, 1).AddDays(-1);
                }
                else if (CommonManager.Setting.AttendanceCommentByEmpThreshold == (int)AttendanceCommentThresholdEnum.LastMonth)
                {
                    return
                       thisDateTime.AddMonths(-1).AddDays(-1);
                    //    new DateTime(thisDateTime.Year,
                    //thisDateTime.AddMonths(-1).Month,
                    //1).AddDays(-1);
                }
                else if (CommonManager.Setting.AttendanceCommentByEmpThreshold == (int)AttendanceCommentThresholdEnum.Past3Days)
                {
                    return thisDateTime.AddDays(-3);
                }
                else if (CommonManager.Setting.AttendanceCommentByEmpThreshold == (int)AttendanceCommentThresholdEnum.AnyDate)
                {
                    return thisDateTime.AddYears(-10);
                }

                return thisDateTime.AddDays(-1);
            }
            catch (Exception ex)
            {
                return thisDateTime.AddDays(-1);
            }
        }


        public static Status InsertUpdateAttendanceComment(AttendanceCheckInCheckOutComment comment)
        {



            bool isEdit = false;
            if (PayrollDataContext.AttendanceCheckInCheckOutComments.Any(x => x.EmpID == comment.EmpID
                && x.AttendanceDate.Value.Date == comment.AttendanceDate.Value.Date
                && x.InOutMode == comment.InOutMode))
            {
                isEdit = true;
            }

            Status status = new Status();

            if (comment.AttendanceDate.Value.Date < GetAttendanceCommentThreshold())
            {
                status.IsSuccess = false;
                status.ErrorMessage = "Sorry comment threshold has exceeded.";
                return status;
            }

            if (isEdit)
            {
                AttendanceCheckInCheckOutComment dbEntity = new AttendanceCheckInCheckOutComment();
                dbEntity = PayrollDataContext.AttendanceCheckInCheckOutComments.FirstOrDefault(x => x.EmpID == comment.EmpID
                && x.AttendanceDate.Value.Date == comment.AttendanceDate.Value.Date
                && x.InOutMode == comment.InOutMode);

                if (dbEntity != null)
                {
                    dbEntity.Note = comment.Note;
                    PayrollDataContext.SubmitChanges();
                }

            }
            else
            {
                comment.LineID = Guid.NewGuid();
                PayrollDataContext.AttendanceCheckInCheckOutComments.InsertOnSubmit(comment);
                PayrollDataContext.SubmitChanges();
                status.IsSuccess = true;

            }
            return status;
        }

        public static List<AttendanceCheckInCheckOutComment> getAllComments(Guid ChkInOutID)
        {
            return PayrollDataContext.AttendanceCheckInCheckOutComments.Where(x => x.ChkInOutID == ChkInOutID).ToList();
        }

        public static List<AttendanceCheckInCheckOutComment> getAllCommentsNew(DateTime datetime, int InOutMode, int EmpID)
        {
            List<AttendanceCheckInCheckOutComment> comments = new List<AttendanceCheckInCheckOutComment>();
            AttendanceCheckInCheckOutComment comment = new AttendanceCheckInCheckOutComment();

            AttendanceGetInAndOutTimeResult IO = PayrollDataContext.AttendanceGetInAndOutTime(EmpID, datetime).FirstOrDefault();

            comment = PayrollDataContext
                     .AttendanceCheckInCheckOutComments
                     .FirstOrDefault(x => x.AttendanceDate.Value.Date == datetime.Date && x.InOutMode == InOutMode && x.EmpID == EmpID);

            if (IO != null && comment != null)
            {
                if (InOutMode == (int)ChkINOUTEnum.ChkIn)
                {

                    if (string.IsNullOrEmpty(comment.Note))
                    {
                        comment.Note = IO.InNote;
                    }

                }
                else
                {
                    if (string.IsNullOrEmpty(comment.Note))
                    {
                        comment.Note = IO.OutNote;
                    }
                }
            }

            if (comment != null)
                comments.Add(comment);

            //return PayrollDataContext.AttendanceCheckInCheckOutComments.Where(x => x.AttendanceDate.Value.Date == datetime.Date && x.InOutMode==InOutMode &&  x.EmpID==EmpID).ToList();
            return comments;
        }


        public static List<AttendanceReportResult> GetAttendanceOfDateRange(int EmpID, int PayrollPeriodID, DateTime? startDate,
            DateTime? endDate, int SuperVisorID, int currentPage, int pageSize)
        {
            List<AttendanceReportResult> data = PayrollDataContext.AttendanceReport(EmpID, PayrollPeriodID, startDate, endDate, SuperVisorID, currentPage, pageSize).ToList();

            foreach (AttendanceReportResult item in data)
            {
                if (item.InTime != null)
                    item.RefinedInRemarks = item.InTime.Value.ToShortTimeString();
                if (item.OutTime != null)
                    item.RefinedOutRemarks = item.OutTime.Value.ToShortTimeString();
                if (item.WorkHour != null)
                {
                    item.RefinedWorkHour = MinuteWith24Hour(item.WorkHour.Value);
                }

                if (item.DateEng != null)
                    item.ActualDate = GetAppropriateDate(item.DateEng.Value);
            }

            return data;
        }


        public static AttendanceGetInAndOutTimeResult getAttendanceofGivenDate(int employeeId, DateTime date)
        {
            //return PayrollDataContext.GetEmployeeTime(employeeId, date, attendanceType, attendanceValue).SingleOrDefault();
            return PayrollDataContext.AttendanceGetInAndOutTime(employeeId, date).FirstOrDefault();
        }

        public int GetTotalDaysInMonth(int payrollperiodid)
        {
            PayrollPeriod pp = new PayrollPeriod();
            pp = PayrollDataContext.PayrollPeriods.FirstOrDefault(x => x.PayrollPeriodId == payrollperiodid);
            if (pp != null)
            {
                if (pp.TotalDays != null)
                {
                    return pp.TotalDays.Value;
                }
            }
            return 0;
        }

        public bool? IsManualAttendancePossible(int EmpID)
        {
            bool? isBranchEnabled = EmployeeManager.GetEmployeeById(EmpID).Branch.IsManualAttendance;
            bool? isEmployeeEnabled = false;

            if (PayrollDataContext.AttendanceMappings.Any(x => x.EmployeeId == EmpID && x.IsEnabledManual != null))
            {
                isEmployeeEnabled = PayrollDataContext.AttendanceMappings.FirstOrDefault(x => x.EmployeeId == EmpID).IsEnabledManual;
            }

            if (isBranchEnabled == true || isEmployeeEnabled == true)
            {
                return true;
            }
            else
                return false;

        }

        public static List<TimeRequestLine> GetTimeRequestLineForDateRange(DateTime startDate, DateTime endDate, string inOutNote, int employeeId)
        {
            List<TimeRequestLine> list = new List<TimeRequestLine>();
            //HolidayManager hmanager = new HolidayManager();
            //List<GetHolidaysForAttendenceResult> holiday = hmanager.GetMonthsHolidaysForAttendence(0, 0, SessionManager.CurrentCompanyId, 0, startDate, endDate).ToList();


            TimeSpan ts = endDate - startDate;
            int totalDays = ts.Days;
            int sn = 1;
            for (int i = 0; i <= totalDays; i++)
            {
                TimeRequestLine obj = new TimeRequestLine();
                obj.SN = sn;
                sn = sn + 1;
                obj.DateEng = startDate.AddDays(i);
                obj.DayName = obj.DateEng.Value.DayOfWeek.ToString();

                //GetHolidaysForAttendenceResult objHolidayResult = holiday.SingleOrDefault(x => x.DateEng == obj.DateEng);
                //if (objHolidayResult != null)
                //    obj.Description = objHolidayResult.Name;

                //GetEmployeeTimeResult time = new GetEmployeeTimeResult();

                //time = PayrollDataContext.GetEmployeeTime(employeeId, obj.DateEng, 1, "", "").FirstOrDefault();

                // show blank for all
                //if (obj.DateEng.Value.Date.DayOfWeek==DayOfWeek.Friday)
                ////if (obj.DayName.Trim().ToLower() == "friday")
                //{
                //    time = PayrollDataContext.GetEmployeeTime(employeeId, obj.DateEng, 2, "WH/2", "").FirstOrDefault();

                //    if (time != null)
                //    {
                //        if (time.OfficeInTime != null)
                //        {
                //            if (CommonManager.CompanySetting.WhichCompany == WhichCompany.Rigo)
                //                obj.InTimeDT = null; 
                //            else
                //                obj.InTimeDT = new DateTime(obj.DateEng.Value.Year, obj.DateEng.Value.Month, obj.DateEng.Value.Day, time.OfficeInTime.Value.Hours, time.OfficeInTime.Value.Minutes, time.OfficeInTime.Value.Seconds);

                //        }

                //        if (time.OfficeOutTime != null)
                //        {
                //            if (CommonManager.CompanySetting.WhichCompany == WhichCompany.Rigo)
                //                obj.OutTimeDT = null;
                //            else
                //                obj.OutTimeDT = new DateTime(obj.DateEng.Value.Year, obj.DateEng.Value.Month, obj.DateEng.Value.Day, time.OfficeOutTime.Value.Hours, time.OfficeOutTime.Value.Minutes, time.OfficeOutTime.Value.Seconds);

                //        }
                //    }
                //}
                //else
                //{
                //    time = PayrollDataContext.GetEmployeeTime(employeeId, obj.DateEng, 1, "", "").FirstOrDefault();

                //    if (time != null)
                //    {
                //        if (time.OfficeInTime != null)
                //        {
                //            if (CommonManager.CompanySetting.WhichCompany == WhichCompany.Rigo)
                //                obj.InTimeDT = null;
                //            else
                //                obj.InTimeDT = new DateTime(obj.DateEng.Value.Year, obj.DateEng.Value.Month, obj.DateEng.Value.Day, time.OfficeInTime.Value.Hours, time.OfficeInTime.Value.Minutes, time.OfficeInTime.Value.Seconds);
                //        }

                //        if (time.OfficeOutTime != null)
                //        {
                //            if (CommonManager.CompanySetting.WhichCompany == WhichCompany.Rigo)
                //                obj.OutTimeDT = null;
                //            else
                //                obj.OutTimeDT = new DateTime(obj.DateEng.Value.Year, obj.DateEng.Value.Month, obj.DateEng.Value.Day, time.OfficeOutTime.Value.Hours, time.OfficeOutTime.Value.Minutes, time.OfficeOutTime.Value.Seconds);                                

                //        }
                //    }
                //}

                if (!string.IsNullOrEmpty(inOutNote))
                {
                    obj.InNote = inOutNote;
                    obj.OutNote = inOutNote;
                }

                list.Add(obj);
            }

            return list;
        }

        public static AttendanceEmpComment GetTimeCommentRequestByRequestId(Guid requestID)
        {
            return PayrollDataContext.AttendanceEmpComments.SingleOrDefault(x => x.LineID == requestID);
        }

        public static List<TimeRequestLine> GetTimeRequestLinesByRequestId(int requestID)
        {
            List<TimeRequestLine> list = PayrollDataContext.TimeRequestLines.Where(x => x.RequestID == requestID).OrderBy(x => x.DateEng).ToList();

            int sn = 1;
            foreach (TimeRequestLine item in list)
            {
                item.SN = sn;
                sn = sn + 1;
                item.DayName = item.DateEng.Value.DayOfWeek.ToString();

                if (item.InTime != null)
                {
                    DateTime inTimeDateTime = new DateTime(item.DateEng.Value.Year, item.DateEng.Value.Month, item.DateEng.Value.Day, item.InTime.Value.Hours, item.InTime.Value.Minutes, item.InTime.Value.Seconds);
                    item.InTimeString = inTimeDateTime.ToString();
                    item.InTimeDT = inTimeDateTime;
                }

                if (item.OutTime != null)
                {
                    DateTime outTimeDateTime = new DateTime(item.DateEng.Value.Year, item.DateEng.Value.Month, item.DateEng.Value.Day, item.OutTime.Value.Hours, item.OutTime.Value.Minutes, item.OutTime.Value.Seconds);
                    item.OutTimeString = outTimeDateTime.ToString();
                    item.OutTimeDT = outTimeDateTime;
                }
            }

            return list;
        }

        public static bool CheckTimeRequestIsSaved(DateTime startDate, DateTime endDate, int employeeid)
        {
            if (PayrollDataContext.TimeRequests.Any(x =>
                (
                (startDate.Date >= x.StartDateEng.Value.Date && startDate.Date <= x.EndDateEng.Value.Date) ||
                (endDate.Date >= x.StartDateEng.Value.Date && endDate.Date <= x.EndDateEng.Value.Date)
                )
                && x.EmployeeId == employeeid && x.Status != (int)AttendanceRequestStatusEnum.Rejected))
                return true;
            else
                return false;
        }


        public static bool CheckTimeCommentRequestIsSaved(DateTime startDate, int employeeid)
        {
            if (PayrollDataContext.AttendanceEmpComments.Any(x => startDate.Date == x.AttendanceDate.Value.Date && x.EmpID == employeeid))
                return true;
            else
                return false;
        }


        public static Status SaveTimeCommentRequest(AttendanceEmpComment obj, bool isSave)
        {
            Status status = new Status();
            if (!isSave)
            {
                AttendanceEmpComment dbObjTimeRequest = PayrollDataContext.AttendanceEmpComments.SingleOrDefault(x => x.LineID == obj.LineID);
                PayrollDataContext.AttendanceEmpComments.DeleteOnSubmit(dbObjTimeRequest);
                PayrollDataContext.AttendanceEmpComments.InsertOnSubmit(obj);
                PayrollDataContext.SubmitChanges();
            }
            else
            {
                PayrollDataContext.AttendanceEmpComments.InsertOnSubmit(obj);
                PayrollDataContext.SubmitChanges();
                status.IsSuccess = true;
            }

            SendEmailTimeCommentRequset(obj);

            return status;
        }
        public static Status SaveUpdateCumulativeSummary(List<CumulativeSummary> list, int peirodId)
        {
            Status status = new Status();

            List<CumulativeSummary> dbList = PayrollDataContext.CumulativeSummaries.Where(x => x.PayrollPeriodId == peirodId).ToList();
            CCalculation calc = CalculationManager.GetCalculation(peirodId);
            List<CCalculationEmployee> calcEmpList = new List<CCalculationEmployee>();
            if (calc != null)
            {
                calcEmpList = PayrollDataContext.CCalculationEmployees
                    .Where(x => x.CalculationId == calc.CalculationId).ToList();
            }

            PIncome lateIncome = PayrollDataContext.PIncomes.FirstOrDefault(x => x.IsLateIncomeType != null && x.IsLateIncomeType.Value);
            List<PEmployeeIncome> dbEmployeeIncomes = new List<PEmployeeIncome>();

            if (lateIncome != null)
                dbEmployeeIncomes = PayrollDataContext.PEmployeeIncomes.Where(x => x.IncomeId == lateIncome.IncomeId).ToList();

            foreach (CumulativeSummary item in list)
            {
                if (calcEmpList.Any(x => x.EmployeeId == item.EmployeeId))
                {
                    status.ErrorMessage = "Salary already saved for EIN " + item.EmployeeId + ".";
                    return status;
                }

                if (lateIncome != null && dbEmployeeIncomes.Any(x => x.EmployeeId == item.EmployeeId) == false)
                {
                    PEmployeeIncome empIncome = new PEmployeeIncome();
                    empIncome.EmployeeId = item.EmployeeId;
                    empIncome.IncomeId = lateIncome.IncomeId;
                    empIncome.IsValid = true;
                    empIncome.IsEnabled = true;
                    empIncome.Amount = 0;
                    PayrollDataContext.PEmployeeIncomes.InsertOnSubmit(empIncome);
                }

                CumulativeSummary dbEntity = dbList.FirstOrDefault(x => x.EmployeeId == item.EmployeeId);


                if (dbEntity != null)
                {
                    dbEntity.LateDays = item.LateDays;
                    dbEntity.LeaveDays = item.LeaveDays;
                    dbEntity.HalfDayLeaveDaysCount = item.HalfDayLeaveDaysCount;
                    dbEntity.ModifiedOn = DateTime.Now;
                    dbEntity.ModifiedBy = SessionManager.CurrentLoggedInUserID;
                }
                else
                {
                    PayrollDataContext.CumulativeSummaries.InsertOnSubmit(item);
                }
            }

            PayrollDataContext.SubmitChanges();

            return status;
        }
        public static Status SaveTimeRequest(TimeRequest obj, bool timeRequestTeamUsingRecommendApproval)
        {
            Status status = new Status();

            bool sendToRecommender = true;

            // if requester and recommender is same then mail goes to Approval
            if (obj.Status == (int)AttendanceRequestStatusEnum.Recommended)
            {
                sendToRecommender = false;

                obj.RecommendedBy = (SessionManager.CurrentLoggedInEmployeeId != 0 ?
                    SessionManager.CurrentLoggedInEmployeeId : SessionManager.User.UserMappedEmployeeId);
                obj.RecommendedOn = DateTime.Now;
            }


            if (obj.RequestID != 0)
            {
                TimeRequest dbObjTimeRequest = PayrollDataContext.TimeRequests.SingleOrDefault(x => x.RequestID == obj.RequestID);
                List<TimeRequestLine> newTimeRequestLines = obj.TimeRequestLines.ToList();

                List<TimeRequestLine> listTimeRequestLines = PayrollDataContext.TimeRequestLines.Where(x => x.RequestID == obj.RequestID).ToList();
                PayrollDataContext.TimeRequestLines.DeleteAllOnSubmit(listTimeRequestLines);

                dbObjTimeRequest.TimeRequestLines.AddRange(newTimeRequestLines);
                dbObjTimeRequest.IPAddress = obj.IPAddress;
                dbObjTimeRequest.ApprovalId1 = obj.ApprovalId1;
                dbObjTimeRequest.ApprovalId2 = obj.ApprovalId2;
                dbObjTimeRequest.RecommenderEmployeeId = obj.RecommenderEmployeeId;
                dbObjTimeRequest.ApprovalEmployeeId = obj.ApprovalEmployeeId;

                dbObjTimeRequest.StartDate = obj.StartDate;
                dbObjTimeRequest.StartDateEng = obj.StartDateEng;
                dbObjTimeRequest.EndDate = obj.EndDate;
                dbObjTimeRequest.EndDateEng = obj.EndDateEng;
                dbObjTimeRequest.WorkDays = obj.WorkDays;

                PayrollDataContext.SubmitChanges();
            }
            else
            {

                List<TimeRequestLine> dbLines =
                    (
                    from t in PayrollDataContext.TimeRequests
                    join l in PayrollDataContext.TimeRequestLines on t.RequestID equals l.RequestID

                    where t.EmployeeId == obj.EmployeeId && l.DateEng >= obj.StartDateEng && l.DateEng <= obj.EndDateEng
                        && t.Status != (int)AttendanceRequestStatusEnum.Rejected
                    select l
                    ).ToList();

                // verify each day if already time request exists or not
                foreach(var item in obj.TimeRequestLines)
                {
                    if (dbLines.Any(x => x.DateEng == item.DateEng)) 
                    {
                        status.ErrorMessage = "Time request already exists for " + item.DateEng.Value.ToString("yyyy-MMM-dd") + ", please remove this line for saving.";
                        return status;
                    }   
                }

                PayrollDataContext.TimeRequests.InsertOnSubmit(obj);
                PayrollDataContext.SubmitChanges();
                status.IsSuccess = true;
            }




            SendEmail(obj, timeRequestTeamUsingRecommendApproval, sendToRecommender);


            return status;
        }

        public static List<AttendanceEmpComment> GetTimeCommentRequestsByEmployeeId(int employeeId)
        {
            List<AttendanceEmpComment> list = PayrollDataContext.AttendanceEmpComments.Where(x => x.EmpID == employeeId).OrderBy(x => x.LineID).ToList();

            string empName = EmployeeManager.GetEmployeeName(employeeId);

            foreach (var item in list)
            {
                item.Name = empName;

                if (item.Status == (int)AttendanceRequestStatusEnum.Approved)
                {
                    UUser objUUser = PayrollDataContext.UUsers.SingleOrDefault(x => x.UserID == item.ApprovedBy);

                    if (objUUser.EmployeeId != null)
                        item.StatusName = "Approved By : " + EmployeeManager.GetEmployeeName(objUUser.EmployeeId.Value);
                    else
                        item.StatusName = "Approved By : " + objUUser.UserName;


                }
                else if (item.Status == (int)AttendanceRequestStatusEnum.Rejected)
                {
                    UUser objUUser = PayrollDataContext.UUsers.SingleOrDefault(x => x.UserID == item.ApprovedBy);
                    if (objUUser.EmployeeId != null)
                        item.StatusName = "Rejected By : " + EmployeeManager.GetEmployeeName(objUUser.EmployeeId.Value);
                    else
                        item.StatusName = "Rejected By : " + objUUser.UserName;
                }
                else
                    item.StatusName = "Request";
            }

            return list;
        }

        public static List<TimeRequest> GetTimeRequestsByEmployeeId(int employeeId)
        {
            List<TimeRequest> list = PayrollDataContext.TimeRequests.Where(x => x.EmployeeId == employeeId).OrderBy(x => x.RequestID).ToList();

            foreach (var item in list)
            {
                if (item.Status == (int)AttendanceRequestStatusEnum.Approved)
                {
                    UUser objUUser = PayrollDataContext.UUsers.SingleOrDefault(x => x.UserID == item.ApprovedBy);

                    if (objUUser.EmployeeId != null)
                        item.StatusName = "Approved By : " + EmployeeManager.GetEmployeeById(objUUser.EmployeeId.Value).Name;
                    else
                        item.StatusName = "Approved By : " + objUUser.UserName;
                }
                else if (item.Status == (int)AttendanceRequestStatusEnum.Rejected)
                {
                    if (item.ApprovedBy == null)
                    {
                        item.StatusName = "Rejected";
                    }
                    else
                    {
                        UUser objUUser = PayrollDataContext.UUsers.SingleOrDefault(x => x.UserID == item.ApprovedBy);
                        if (objUUser.EmployeeId != null)
                            item.StatusName = "Rejected By : " + EmployeeManager.GetEmployeeById(objUUser.EmployeeId.Value).Name;
                        else
                            item.StatusName = "Rejected By : " + objUUser.UserName;
                    }
                }
                else
                    item.StatusName = "Awaiting Approval";
            }

            return list;
        }

        public static int GetTimeRequestsCount()
        {
            return PayrollDataContext.TimeRequests.Where(x => x.Status == (int)AttendanceRequestStatusEnum.Saved)
                .Count();
        }

        public static int GetAttendanceCommentRequestsCount()
        {
            return PayrollDataContext.AttendanceEmpComments.Where(x => x.Status == (int)AttendanceRequestStatusEnum.Saved)
                .Count();
        }

        public static List<TimeRequestLine> GetTimeRequestLinesByRequestID(int requestId)
        {
            List<TimeRequestLine> list = PayrollDataContext.TimeRequestLines.Where(x => x.RequestID == requestId).OrderBy(x => x.RequestLineID).ToList();
            TimeRequest objTimeRequest = GetTimeRequestByRequestID(requestId);
            string submittedOnString = "";
            if (objTimeRequest.SubmittedOnEng != null)
                submittedOnString = objTimeRequest.SubmittedOnEng.Value.ToString("MMM d, h:mm tt");

            foreach (var item in list)
            {
                item.DayName = item.DateEng.Value.DayOfWeek.ToString();

                if (item.InTime != null)
                    item.InTimeString = AttendanceManager.CalculateManualVisibleTime(item.InTime.Value);

                if (item.OutTime != null)
                    item.OutTimeString = AttendanceManager.CalculateManualVisibleTime(item.OutTime.Value);

                if (item.WorkHours != null)
                    item.WorkHoursString = item.WorkHours.Value.ToString().Replace('.', ':');

                item.SubmittedOnString = submittedOnString;

            }

            return list;
        }


        public static Status RevertAttCommentRequest(Guid requestID)
        {
            Status status = new Status();

            AttendanceEmpComment dbObj = PayrollDataContext.AttendanceEmpComments.SingleOrDefault(x => x.LineID == requestID);
            if (dbObj != null)
            {
                dbObj.Status = (int)AttendanceRequestStatusEnum.Rejected;
                dbObj.ApprovedBy = SessionManager.CurrentLoggedInUserID;
                dbObj.ApprovedOn = System.DateTime.Now;
                PayrollDataContext.SubmitChanges();
                status.IsSuccess = true;
            }

            return status;
        }

        public static Status RejectAttRequestTime(int requestID, string comment)
        {
            Status status = new Status();

            TimeRequest dbObj = PayrollDataContext.TimeRequests.SingleOrDefault(x => x.RequestID == requestID);
            if (dbObj != null)
            {
                DAL.AttendanceMapping map = BLL.BaseBiz.PayrollDataContext.AttendanceMappings
                                        .SingleOrDefault(x => x.EmployeeId == dbObj.EmployeeId.Value);

                // if already approved then delete from AttendanceCheckInCheckOut table
                if (dbObj.Status == (int)AttendanceRequestStatusEnum.Approved)
                {
                    dbObj.IsRejectedAfterApproval = true;

                    foreach(var line in dbObj.TimeRequestLines)
                    {
                        DateTime? inDateTime = null, outDateTime = null; ;

                        if (line.InTime != null)
                            inDateTime = new DateTime(line.DateEng.Value.Year, line.DateEng.Value.Month, line.DateEng.Value.Day,
                                line.InTime.Value.Hours, line.InTime.Value.Minutes, line.InTime.Value.Seconds);

                        if (line.OutTime != null)
                            outDateTime = new DateTime(line.DateEng.Value.Year, line.DateEng.Value.Month, line.DateEng.Value.Day,
                                line.OutTime.Value.Hours, line.OutTime.Value.Minutes, line.OutTime.Value.Seconds);

                        List<AttendanceCheckInCheckOut> dbAtteList =
                            PayrollDataContext.AttendanceCheckInCheckOuts
                            .Where(x => x.DeviceID == map.DeviceId 
                                &&
                                (
                                    (x.AuthenticationType == (int)TimeAttendanceAuthenticationType.TimeRequest && (inDateTime != null && x.DateTime == inDateTime))
                                    ||
                                    (x.AuthenticationType == (int)TimeAttendanceAuthenticationType.TimeRequest && (outDateTime != null && x.DateTime == outDateTime ))
                                )
                                )
                            .ToList();

                        PayrollDataContext.AttendanceCheckInCheckOuts.DeleteAllOnSubmit(dbAtteList);
                    }
                }

                dbObj.Status = (int)AttendanceRequestStatusEnum.Rejected;
                if (!string.IsNullOrEmpty(comment))
                    dbObj.RejectComment = comment;

                

                dbObj.ApprovedBy = SessionManager.CurrentLoggedInUserID;
                dbObj.ApprovedOn = System.DateTime.Now;
                PayrollDataContext.SubmitChanges();
                status.IsSuccess = true;
            }

            return status;
        }

        public static TimeRequest GetTimeRequestByRequestID(int requestID)
        {
            return PayrollDataContext.TimeRequests.SingleOrDefault(x => x.RequestID == requestID);
        }
        public static AttendanceMapping GetAttendanceMappingOfEmployee(int EmpID)
        {
            return PayrollDataContext.AttendanceMappings.SingleOrDefault(x => x.EmployeeId == EmpID);
        }

        public static string CalculateManualVisibleTime(TimeSpan ts)
        {
            string manualVisualTime = "";

            if (ts.Hours > 12)
            {
                manualVisualTime = (ts.Hours - 12).ToString() + ":" + ts.Minutes.ToString().PadLeft(2, '0') + " PM";
            }
            else if (ts.Hours == 12)
            {
                manualVisualTime = ts.Hours.ToString() + ":" + ts.Minutes.ToString().PadLeft(2, '0') + " PM";
            }
            else
            {
                manualVisualTime = ts.Hours.ToString() + ":" + ts.Minutes.ToString().PadLeft(2, '0') + " AM";
            }

            return manualVisualTime;
        }

        public static AttendanceReportResult GetEmployeeAttendanceTimeOFDay(DateTime Date)
        {
            List<AttendanceReportResult> employeeCheckInOutTimeList = PayrollDataContext.AttendanceReport(SessionManager.CurrentLoggedInEmployeeId, null,
                       Date, Date, 0, 0, 99999).ToList();
            return employeeCheckInOutTimeList.FirstOrDefault();
        }


        public static Status ApproveAttCommentRequest(List<AttendanceEmpComment> list)
        {
            Status status = new Status();

            SetConnectionPwd(PayrollDataContext.Connection);
            PayrollDataContext.Transaction = PayrollDataContext.Connection.BeginTransaction();

            try
            {

                foreach (AttendanceEmpComment item in list)
                {
                    AttendanceEmpComment dbObjTimeRequest = PayrollDataContext.AttendanceEmpComments.SingleOrDefault(x => x.LineID == item.LineID && x.EmpID == item.EmpID);
                    if (dbObjTimeRequest != null)
                    {
                        dbObjTimeRequest.Status = (int)AttendanceRequestStatusEnum.Approved;
                        dbObjTimeRequest.ApprovedBy = SessionManager.CurrentLoggedInUserID;
                        dbObjTimeRequest.ApprovedOn = DateTime.Now;
                    }
                    //while approving if in or out comment has non empty text then replace in the table AttendanceCheckInCheckOutComment table, if AttendanceCheckInCheckOutComment already contains in/out comment then replace them

                    List<AttendanceCheckInCheckOutComment> dbObjAttendanceCheckInCheckOutComment = PayrollDataContext.AttendanceCheckInCheckOutComments.Where(x => x.AttendanceDate == item.AttendanceDate && x.EmpID == item.EmpID).ToList();

                    //----------------insert mode--------------------------
                    if (!string.IsNullOrEmpty(item.InComment))
                    {
                        if ((dbObjAttendanceCheckInCheckOutComment.SingleOrDefault(x => x.InOutMode == 0)) == null)//InMode
                        {
                            AttendanceCheckInCheckOutComment _AttendanceCheckInCheckOutComment = new AttendanceCheckInCheckOutComment();
                            _AttendanceCheckInCheckOutComment.LineID = Guid.NewGuid();
                            _AttendanceCheckInCheckOutComment.Note = item.InComment;
                            _AttendanceCheckInCheckOutComment.AttendanceDate = item.AttendanceDate;
                            _AttendanceCheckInCheckOutComment.InOutMode = 0;
                            _AttendanceCheckInCheckOutComment.InComment = item.InComment;
                            _AttendanceCheckInCheckOutComment.EmpID = item.EmpID;
                            PayrollDataContext.AttendanceCheckInCheckOutComments.InsertOnSubmit(_AttendanceCheckInCheckOutComment);
                        }
                    }

                    if (!string.IsNullOrEmpty(item.OutComment))
                    {
                        if ((dbObjAttendanceCheckInCheckOutComment.SingleOrDefault(x => x.InOutMode == 1)) == null)//OutMode
                        {
                            AttendanceCheckInCheckOutComment _AttendanceCheckInCheckOutComment = new AttendanceCheckInCheckOutComment();
                            _AttendanceCheckInCheckOutComment.LineID = Guid.NewGuid();
                            _AttendanceCheckInCheckOutComment.Note = item.OutComment;
                            _AttendanceCheckInCheckOutComment.AttendanceDate = item.AttendanceDate;
                            _AttendanceCheckInCheckOutComment.InOutMode = 1;
                            _AttendanceCheckInCheckOutComment.OutComment = item.OutComment;
                            _AttendanceCheckInCheckOutComment.EmpID = item.EmpID;
                            PayrollDataContext.AttendanceCheckInCheckOutComments.InsertOnSubmit(_AttendanceCheckInCheckOutComment);
                        }
                    }
                    //-----------------------------------------------------

                    //-------------------------update mode-----------------
                    if (dbObjAttendanceCheckInCheckOutComment.Any())
                    {
                        if (!string.IsNullOrEmpty(item.InComment))//InMode
                        {
                            AttendanceCheckInCheckOutComment dbAttendanceCheckInCheckOutCommentInMode = dbObjAttendanceCheckInCheckOutComment.SingleOrDefault(x => x.InOutMode == 0);

                            if (dbAttendanceCheckInCheckOutCommentInMode != null)
                            {
                                dbAttendanceCheckInCheckOutCommentInMode.InComment = item.InComment;
                                dbAttendanceCheckInCheckOutCommentInMode.Note = item.InComment;
                            }
                        }

                        if (!string.IsNullOrEmpty(item.OutComment))//OutMode
                        {
                            AttendanceCheckInCheckOutComment dbAttendanceCheckInCheckOutCommentOutMode = dbObjAttendanceCheckInCheckOutComment.SingleOrDefault(x => x.InOutMode == 1);
                            if (dbAttendanceCheckInCheckOutCommentOutMode != null)
                            {
                                dbAttendanceCheckInCheckOutCommentOutMode.OutComment = item.OutComment;
                                dbAttendanceCheckInCheckOutCommentOutMode.Note = item.OutComment;
                            }
                        }
                    }
                    //-----------------------------------------------------------

                }

                PayrollDataContext.SubmitChanges();
                PayrollDataContext.Transaction.Commit();
                status.IsSuccess = true;
            }
            catch (Exception exp)
            {
                Log.log("Error while approving attendance Comment", exp);
                PayrollDataContext.Transaction.Rollback();
                status.IsSuccess = false;
            }
            finally
            {

            }


            return status;
        }

        public static Status ApproveAttRequestTime(List<TimeRequest> list, bool isAssign, bool skipEntryInTimeAttendanceTable)
        {
            Status status = new Status();

            Setting setting = OvertimeManager.GetSetting();
            bool timeRequestTeamUsingRecommendApproval = false;
            if (setting.TimeRequestTeamUsingRecommendApproval != null && setting.TimeRequestTeamUsingRecommendApproval.Value)
                timeRequestTeamUsingRecommendApproval = true;

            SetConnectionPwd(PayrollDataContext.Connection);
            PayrollDataContext.Transaction = PayrollDataContext.Connection.BeginTransaction();

            try
            {

                foreach (TimeRequest item in list)
                {

                    TimeSpan defaultBreanInTime = new TimeSpan(13, 0, 0);

                    if (item.TimeRequestLines.Count > 0)
                    {
                        bool IsApproval = false;

                        if (SessionManager.CurrentLoggedInEmployeeId != 0 && timeRequestTeamUsingRecommendApproval && isAssign)
                        {
                            List<TextValue> listApproval = LeaveRequestManager.GetApplyToListForEmployee(item.EmployeeId.Value, true, PreDefindFlowType.Overtime);
                            if (listApproval.Any(x => x.Value == SessionManager.CurrentLoggedInEmployeeId.ToString()))
                                IsApproval = true;
                        }


                        if ((SessionManager.CurrentLoggedInEmployeeId == 0) || (!timeRequestTeamUsingRecommendApproval) || (!isAssign && item.ApprovalEmployeeId == SessionManager.CurrentLoggedInEmployeeId) || (isAssign && IsApproval))
                        {
                            GetEmployeeTimeResult employeeDefaultTime = PayrollDataContext.GetEmployeeTime(item.EmployeeId.Value, item.TimeRequestLines
                                [0].DateEng.Value.Date, null, "", "").FirstOrDefault();

                            List<AttendanceReportResult> employeeCheckInOutTimeList = PayrollDataContext.AttendanceReport(item.EmployeeId.Value, null,
                                item.StartDateEng.Value, item.EndDateEng.Value, 0, 0, 99999).ToList();

                            foreach (TimeRequestLine itemTimeReqLine in item.TimeRequestLines)
                            {
                                //Guid objCheckInGuid = new Guid();
                                //Guid objCheckOutGuid = new Guid();

                                DAL.AttendanceMapping map = BLL.BaseBiz.PayrollDataContext.AttendanceMappings
                                          .SingleOrDefault(x => x.EmployeeId == item.EmployeeId.Value);

                                if (map == null)
                                {
                                    status.ErrorMessage = "Employee mapping with the device does not exists.";
                                    status.IsSuccess = false;
                                    PayrollDataContext.Transaction.Rollback();
                                    return status;
                                }

                                AttendanceReportResult timeForThisDay = employeeCheckInOutTimeList.FirstOrDefault(x => x.DateEng == itemTimeReqLine.DateEng);

                                // if in time exists before Brean In Time then do not proceed
                                if (timeForThisDay != null && timeForThisDay.InTime != null && timeForThisDay.InTime.Value
                                    .TimeOfDay < defaultBreanInTime)
                                {
                                }
                                else if (itemTimeReqLine.InTime != null)
                                {

                                    AttendanceCheckInCheckOut chkInOut = new AttendanceCheckInCheckOut();
                                    chkInOut.ChkInOutID = Guid.NewGuid();
                                    chkInOut.DeviceID = map.DeviceId;
                                    chkInOut.AuthenticationType = (int)TimeAttendanceAuthenticationType.TimeRequest;
                                    chkInOut.InOutMode = (int)ChkINOUTEnum.ChkIn;
                                    chkInOut.ModifiedBy = SessionManager.CurrentLoggedInUserID;
                                    chkInOut.ModifiedOn = DateTime.Now;
                                    //chkInOut.IPAddress = HttpContext.Current.Request.UserHostAddress;
                                    chkInOut.ManualVisibleTime = AttendanceManager.CalculateManualVisibleTime(itemTimeReqLine.InTime.Value);

                                    chkInOut.DateTime = new DateTime(itemTimeReqLine.DateEng.Value.Date.Year, itemTimeReqLine.DateEng.Value.Date.Month, itemTimeReqLine.DateEng.Value.Date.Day, itemTimeReqLine.InTime.Value.Hours, itemTimeReqLine.InTime.Value.Minutes, itemTimeReqLine.InTime.Value.Seconds);
                                    chkInOut.Date = chkInOut.DateTime.Date;
                                    chkInOut.ModificationRemarks = "Time Request Attendance";
                                    chkInOut.IPAddress = item.IPAddress;

                                    PayrollDataContext.AttendanceCheckInCheckOuts.InsertOnSubmit(chkInOut);
                                    // PayrollDataContext.SubmitChanges();
                                    //objCheckInGuid = chkInOut.ChkInOutID;

                                }


                                // if in time exists before Brean In Time then do not proceed
                                if (timeForThisDay != null &&
                                    (timeForThisDay.OutTime != null && timeForThisDay.OutTime.Value.TimeOfDay > defaultBreanInTime)
                                    ||
                                    // if in time exists after break in
                                    (timeForThisDay.OutTime == null && timeForThisDay.InTime != null && timeForThisDay.InTime.Value
                                        .TimeOfDay > defaultBreanInTime)
                                    )
                                {
                                }
                                else if (itemTimeReqLine.OutTime != null)
                                {
                                    DateTime outDateTime = new DateTime();
                                    if (itemTimeReqLine.OvernightShift != null && itemTimeReqLine.OvernightShift.Value)
                                        outDateTime = itemTimeReqLine.DateEng.Value.AddDays(1);
                                    else
                                        outDateTime = itemTimeReqLine.DateEng.Value;



                                    AttendanceCheckInCheckOut chkInOut = new AttendanceCheckInCheckOut();
                                    chkInOut.ChkInOutID = Guid.NewGuid();
                                    chkInOut.DeviceID = map.DeviceId;
                                    chkInOut.AuthenticationType = (int)TimeAttendanceAuthenticationType.TimeRequest;
                                    chkInOut.InOutMode = (int)ChkINOUTEnum.ChkOut;
                                    //chkInOut.IPAddress = HttpContext.Current.Request.UserHostAddress;
                                    chkInOut.ManualVisibleTime = AttendanceManager.CalculateManualVisibleTime(itemTimeReqLine.OutTime.Value);

                                    chkInOut.DateTime = new DateTime(outDateTime.Date.Year, outDateTime.Date.Month, outDateTime.Date.Day, itemTimeReqLine.OutTime.Value.Hours, itemTimeReqLine.OutTime.Value.Minutes, itemTimeReqLine.OutTime.Value.Seconds);
                                    chkInOut.Date = itemTimeReqLine.DateEng.Value.Date;
                                    chkInOut.ModificationRemarks = "Time Request Attendance";
                                    chkInOut.IPAddress = item.IPAddress;
                                    chkInOut.ModifiedBy = SessionManager.CurrentLoggedInUserID;
                                    chkInOut.ModifiedOn = DateTime.Now;
                                    PayrollDataContext.AttendanceCheckInCheckOuts.InsertOnSubmit(chkInOut);
                                    //PayrollDataContext.SubmitChanges();
                                    //objCheckOutGuid = chkInOut.ChkInOutID;

                                }

                                if (itemTimeReqLine.InNote != null)
                                {

                                    AttendanceCheckInCheckOutComment dbObjAttendanceCheckInCheckOutComment = PayrollDataContext.AttendanceCheckInCheckOutComments.FirstOrDefault(x => x.EmpID == item.EmployeeId.Value
                                                                                    && x.AttendanceDate.Value.Date == itemTimeReqLine.DateEng.Value.Date
                                                                                    && x.InOutMode == (int)ChkINOUTEnum.ChkIn);

                                    if (dbObjAttendanceCheckInCheckOutComment != null)
                                    {
                                        dbObjAttendanceCheckInCheckOutComment.Note = itemTimeReqLine.InNote;
                                        //PayrollDataContext.SubmitChanges();
                                    }
                                    else
                                    {
                                        AttendanceCheckInCheckOutComment comment = new AttendanceCheckInCheckOutComment();
                                        //comment.ChkInOutID = objCheckInGuid;
                                        comment.AttendanceDate = itemTimeReqLine.DateEng;
                                        comment.InOutMode = (int)ChkINOUTEnum.ChkIn;
                                        comment.Note = itemTimeReqLine.InNote;
                                        comment.EmpID = item.EmployeeId;
                                        comment.LineID = Guid.NewGuid();
                                        PayrollDataContext.AttendanceCheckInCheckOutComments.InsertOnSubmit(comment);
                                        //PayrollDataContext.SubmitChanges();
                                    }

                                }


                                if (itemTimeReqLine.OutNote != null)
                                {

                                    AttendanceCheckInCheckOutComment dbObjAttendanceCheckInCheckOutComment = PayrollDataContext.AttendanceCheckInCheckOutComments.FirstOrDefault(x => x.EmpID == item.EmployeeId.Value
                                                                                    && x.AttendanceDate.Value.Date == itemTimeReqLine.DateEng.Value.Date
                                                                                    && x.InOutMode == (int)ChkINOUTEnum.ChkOut);

                                    if (dbObjAttendanceCheckInCheckOutComment != null)
                                    {
                                        dbObjAttendanceCheckInCheckOutComment.Note = itemTimeReqLine.OutNote;
                                        //PayrollDataContext.SubmitChanges();
                                    }
                                    else
                                    {
                                        AttendanceCheckInCheckOutComment comment = new AttendanceCheckInCheckOutComment();
                                        //comment.ChkInOutID = objCheckOutGuid;
                                        comment.AttendanceDate = itemTimeReqLine.DateEng;
                                        comment.InOutMode = (int)ChkINOUTEnum.ChkOut;
                                        comment.Note = itemTimeReqLine.OutNote;
                                        comment.EmpID = item.EmployeeId;
                                        comment.LineID = Guid.NewGuid();
                                        PayrollDataContext.AttendanceCheckInCheckOutComments.InsertOnSubmit(comment);
                                        // PayrollDataContext.SubmitChanges();
                                    }

                                }

                            }

                        }

                        TimeRequest dbObjTimeRequest = PayrollDataContext.TimeRequests.SingleOrDefault(x => x.RequestID == item.RequestID && x.EmployeeId == item.EmployeeId);
                        // in hr Assign not saved in TimeRequest



                        //if (dbObjTimeRequest != null)
                        //{
                        //    dbObjTimeRequest.Status = (int)AttendanceRequestStatusEnum.Approved;
                        //    dbObjTimeRequest.ApprovedBy = SessionManager.CurrentLoggedInUserID;
                        //    dbObjTimeRequest.ApprovedOn = DateTime.Now;
                        //}
                        //else
                        //{
                        //    item.Status = (int)AttendanceRequestStatusEnum.Approved;
                        //    item.ApprovedBy = SessionManager.CurrentLoggedInUserID;
                        //    item.ApprovedOn = DateTime.Now;
                        //    PayrollDataContext.TimeRequests.InsertOnSubmit(item);
                        //}

                        if (dbObjTimeRequest != null)
                        {
                            if (SessionManager.CurrentLoggedInEmployeeId == 0 || !timeRequestTeamUsingRecommendApproval)
                            {
                                dbObjTimeRequest.Status = (int)AttendanceRequestStatusEnum.Approved;
                                dbObjTimeRequest.ApprovedBy = SessionManager.CurrentLoggedInUserID;
                                dbObjTimeRequest.ApprovedOn = DateTime.Now;
                                dbObjTimeRequest.RecommendedBy = (SessionManager.CurrentLoggedInEmployeeId != 0 ?
                                    SessionManager.CurrentLoggedInEmployeeId : SessionManager.User.UserMappedEmployeeId);
                                dbObjTimeRequest.RecommendedOn = DateTime.Now;
                            }
                            else
                            {

                                if ((!isAssign && dbObjTimeRequest.ApprovalEmployeeId == SessionManager.CurrentLoggedInEmployeeId) || (isAssign && IsApproval))
                                {
                                    dbObjTimeRequest.Status = (int)AttendanceRequestStatusEnum.Approved;
                                    dbObjTimeRequest.ApprovedBy = SessionManager.CurrentLoggedInUserID;
                                    dbObjTimeRequest.ApprovedOn = DateTime.Now;
                                    dbObjTimeRequest.RecommendedBy = (SessionManager.CurrentLoggedInEmployeeId != 0 ?
                                        SessionManager.CurrentLoggedInEmployeeId : SessionManager.User.UserMappedEmployeeId);
                                    dbObjTimeRequest.RecommendedOn = DateTime.Now;
                                }
                                else
                                {
                                    dbObjTimeRequest.Status = (int)AttendanceRequestStatusEnum.Recommended;
                                    dbObjTimeRequest.RecommendedBy = (SessionManager.CurrentLoggedInEmployeeId != 0 ?
                                        SessionManager.CurrentLoggedInEmployeeId : SessionManager.User.UserMappedEmployeeId);
                                    dbObjTimeRequest.RecommendedOn = DateTime.Now;

                                    SendEmail(dbObjTimeRequest, timeRequestTeamUsingRecommendApproval, false);
                                }
                            }

                        }
                        else
                        {
                            if (SessionManager.CurrentLoggedInEmployeeId == 0 || !timeRequestTeamUsingRecommendApproval)
                            {
                                item.Status = (int)AttendanceRequestStatusEnum.Approved;
                                item.ApprovedBy = SessionManager.CurrentLoggedInUserID;
                                item.ApprovedOn = DateTime.Now;
                                item.RecommendedBy = (SessionManager.CurrentLoggedInEmployeeId != 0 ?
                                    SessionManager.CurrentLoggedInEmployeeId : SessionManager.User.UserMappedEmployeeId); ;
                                item.RecommendedOn = DateTime.Now;
                            }
                            else
                            {
                                if ((!isAssign && item.ApprovalEmployeeId == SessionManager.CurrentLoggedInEmployeeId) || (isAssign && IsApproval))
                                {
                                    item.Status = (int)AttendanceRequestStatusEnum.Approved;
                                    item.ApprovedBy = SessionManager.CurrentLoggedInUserID;
                                    item.ApprovedOn = DateTime.Now;
                                    item.RecommendedBy = (SessionManager.CurrentLoggedInEmployeeId != 0 ?
                                        SessionManager.CurrentLoggedInEmployeeId : SessionManager.User.UserMappedEmployeeId); ;
                                    item.RecommendedOn = DateTime.Now;
                                }
                                else
                                {
                                    item.Status = (int)AttendanceRequestStatusEnum.Recommended;
                                    item.RecommendedBy = (SessionManager.CurrentLoggedInEmployeeId != 0 ?
                                        SessionManager.CurrentLoggedInEmployeeId : SessionManager.User.UserMappedEmployeeId); ;
                                    item.RecommendedOn = DateTime.Now;

                                    SendEmail(item, timeRequestTeamUsingRecommendApproval, false);
                                }
                            }

                            // for assignment from HR skip to save in this table as later when deletion from Exception entry case
                            if (skipEntryInTimeAttendanceTable == false)
                                PayrollDataContext.TimeRequests.InsertOnSubmit(item);
                        }
                    }
                }

                PayrollDataContext.SubmitChanges();
                PayrollDataContext.Transaction.Commit();
                status.IsSuccess = true;
            }
            catch (Exception exp)
            {
                Log.log("Error while approving attendance time", exp);
                PayrollDataContext.Transaction.Rollback();
                status.ErrorMessage = exp.Message;
                status.IsSuccess = false;

            }
            finally
            {

            }


            return status;
        }

        public static Status ImportOTTime(List<TimeRequestLine> list)
        {
            Status status = new Status();

            SetConnectionPwd(PayrollDataContext.Connection);
            PayrollDataContext.Transaction = PayrollDataContext.Connection.BeginTransaction();

            try
            {

                foreach (TimeRequestLine itemTimeReqLine in list)
                {


                    //if (item.TimeRequestLines.Count > 0)
                    {

                        //foreach (TimeRequestLine itemTimeReqLine in item.TimeRequestLines)
                        {
                            //Guid objCheckInGuid = new Guid();
                            //Guid objCheckOutGuid = new Guid();

                            DAL.AttendanceMapping map = BLL.BaseBiz.PayrollDataContext.AttendanceMappings
                                      .SingleOrDefault(x => x.EmployeeId == itemTimeReqLine.EmpId);

                            if (map == null)
                            {
                                status.ErrorMessage = "Employee mapping with the device does not exists for EIN " + itemTimeReqLine.EmpId + ".";
                                status.IsSuccess = false;
                                PayrollDataContext.Transaction.Rollback();
                                return status;
                            }

                            // AttendanceReportResult timeForThisDay = employeeCheckInOutTimeList.FirstOrDefault(x => x.DateEng == itemTimeReqLine.DateEng);

                            // if in time exists before Brean In Time then do not proceed
                            //if (timeForThisDay != null && timeForThisDay.InTime != null && timeForThisDay.InTime.Value
                            //    .TimeOfDay < defaultBreanInTime)
                            //{
                            //}
                            //else if (itemTimeReqLine.InTime != null)
                            {

                                AttendanceCheckInCheckOut chkInOut = new AttendanceCheckInCheckOut();
                                chkInOut.ChkInOutID = Guid.NewGuid();
                                chkInOut.DeviceID = map.DeviceId;
                                chkInOut.AuthenticationType = (int)TimeAttendanceAuthenticationType.OTTimeImport;
                                chkInOut.InOutMode = (int)ChkINOUTEnum.ChkIn;
                                chkInOut.ModifiedBy = SessionManager.CurrentLoggedInUserID;
                                chkInOut.ModifiedOn = DateTime.Now;
                                //chkInOut.IPAddress = HttpContext.Current.Request.UserHostAddress;
                                chkInOut.ManualVisibleTime = "";

                                chkInOut.DateTime = new DateTime(itemTimeReqLine.DateEng.Value.Date.Year, itemTimeReqLine.DateEng.Value.Date.Month, itemTimeReqLine.DateEng.Value.Date.Day, itemTimeReqLine.InTime.Value.Hours, itemTimeReqLine.InTime.Value.Minutes, itemTimeReqLine.InTime.Value.Seconds);
                                chkInOut.Date = chkInOut.DateTime.Date;
                                chkInOut.ModificationRemarks = "OT Attendance";
                                chkInOut.IPAddress = "";

                                PayrollDataContext.AttendanceCheckInCheckOuts.InsertOnSubmit(chkInOut);
                                // PayrollDataContext.SubmitChanges();
                                //objCheckInGuid = chkInOut.ChkInOutID;

                            }



                            {
                                DateTime outDateTime = new DateTime();
                                if (itemTimeReqLine.OvernightShift != null && itemTimeReqLine.OvernightShift.Value)
                                    outDateTime = itemTimeReqLine.DateEng.Value.AddDays(1);
                                else
                                    outDateTime = itemTimeReqLine.DateEng.Value;



                                AttendanceCheckInCheckOut chkInOut = new AttendanceCheckInCheckOut();
                                chkInOut.ChkInOutID = Guid.NewGuid();
                                chkInOut.DeviceID = map.DeviceId;
                                chkInOut.AuthenticationType = (int)TimeAttendanceAuthenticationType.OTTimeImport;
                                chkInOut.InOutMode = (int)ChkINOUTEnum.ChkOut;
                                //chkInOut.IPAddress = HttpContext.Current.Request.UserHostAddress;
                                chkInOut.ManualVisibleTime = AttendanceManager.CalculateManualVisibleTime(itemTimeReqLine.OutTime.Value);

                                chkInOut.DateTime = new DateTime(outDateTime.Date.Year, outDateTime.Date.Month, outDateTime.Date.Day, itemTimeReqLine.OutTime.Value.Hours, itemTimeReqLine.OutTime.Value.Minutes, itemTimeReqLine.OutTime.Value.Seconds);
                                chkInOut.Date = itemTimeReqLine.DateEng.Value.Date;
                                chkInOut.ModificationRemarks = "OT Attendance";
                                chkInOut.IPAddress = "";
                                chkInOut.ModifiedBy = SessionManager.CurrentLoggedInUserID;
                                chkInOut.ModifiedOn = DateTime.Now;
                                PayrollDataContext.AttendanceCheckInCheckOuts.InsertOnSubmit(chkInOut);
                                //PayrollDataContext.SubmitChanges();
                                //objCheckOutGuid = chkInOut.ChkInOutID;

                            }

                            if (itemTimeReqLine.InNote != null)
                            {

                                AttendanceCheckInCheckOutComment dbObjAttendanceCheckInCheckOutComment = PayrollDataContext.AttendanceCheckInCheckOutComments.FirstOrDefault(x => x.EmpID == itemTimeReqLine.EmpId
                                                                                && x.AttendanceDate.Value.Date == itemTimeReqLine.DateEng.Value.Date
                                                                                && x.InOutMode == (int)ChkINOUTEnum.ChkIn);

                                if (dbObjAttendanceCheckInCheckOutComment != null)
                                {
                                    dbObjAttendanceCheckInCheckOutComment.Note += ", OT : " + itemTimeReqLine.InNote;
                                    //PayrollDataContext.SubmitChanges();
                                }
                                else
                                {
                                    AttendanceCheckInCheckOutComment comment = new AttendanceCheckInCheckOutComment();
                                    //comment.ChkInOutID = objCheckInGuid;
                                    comment.AttendanceDate = itemTimeReqLine.DateEng;
                                    comment.InOutMode = (int)ChkINOUTEnum.ChkIn;
                                    comment.Note = itemTimeReqLine.InNote;
                                    comment.EmpID = itemTimeReqLine.EmpId;
                                    comment.LineID = Guid.NewGuid();
                                    PayrollDataContext.AttendanceCheckInCheckOutComments.InsertOnSubmit(comment);
                                    //PayrollDataContext.SubmitChanges();
                                }

                            }

                        }


                    }
                }

                PayrollDataContext.SubmitChanges();
                PayrollDataContext.Transaction.Commit();
                status.IsSuccess = true;
            }
            catch (Exception exp)
            {
                Log.log("Error while approving attendance time", exp);
                PayrollDataContext.Transaction.Rollback();
                status.ErrorMessage = exp.ToString();
                status.IsSuccess = false;
            }
            finally
            {

            }


            return status;
        }


        public static Status DeleteTimeCommentRequest(Guid requestID)
        {
            Status status = new Status();
            try
            {
                AttendanceEmpComment dbObj = PayrollDataContext.AttendanceEmpComments.SingleOrDefault(x => x.LineID == requestID);
                if (dbObj != null)
                {
                    PayrollDataContext.AttendanceEmpComments.DeleteOnSubmit(dbObj);
                    PayrollDataContext.SubmitChanges();
                    status.IsSuccess = true;
                }
                else
                {
                    status.ErrorMessage = "Record not found.";
                    status.IsSuccess = false;
                }
            }
            catch
            {
                status.ErrorMessage = "Error while deleting Record";
                status.IsSuccess = false;
            }

            return status;
        }

        public static Status DeleteTimeRequest(int requestID)
        {
            Status status = new Status();

            TimeRequest dbObj = PayrollDataContext.TimeRequests.SingleOrDefault(x => x.RequestID == requestID);
            if (dbObj != null)
            {
                PayrollDataContext.TimeRequests.DeleteOnSubmit(dbObj);
                PayrollDataContext.SubmitChanges();
                status.IsSuccess = true;
            }
            else
            {
                status.ErrorMessage = "Record not found.";
                status.IsSuccess = false;
            }

            return status;
        }

        public static List<TimeRequest> GetTimeRequestListByDateRange(DateTime startDate, DateTime endDate)
        {
            // hide rejected in calendar so that new request could be made easily
            return PayrollDataContext.TimeRequests
                .Where(x => (
                    (x.StartDateEng.Value.Date >= startDate.Date && x.StartDateEng.Value.Date <= endDate.Date) ||
                    (x.EndDateEng.Value.Date >= startDate.Date && x.EndDateEng.Value.Date <= endDate.Date))
                    && (x.EmployeeId == SessionManager.CurrentLoggedInEmployeeId)
                    && x.Status != (int)(AttendanceRequestStatusEnum.Rejected)).ToList();
        }




        //public static List<AttendanceCheckInCheckOut> GetAttendanceCheckInCheckOutByDeviceIdAndDate(string deviceId, DateTime dt)
        //{
        //    return PayrollDataContext.AttendanceCheckInCheckOuts.Where(x => x.DeviceID == deviceId && x.DateTime.Date == dt.Date).ToList();
        //}

        //public static List<AttendanceCheckInCheckOut> GetAttendanceCheckInCheckOutByIPAddressAndDate(string IPAddress, DateTime dt)
        //{
        //    return PayrollDataContext.AttendanceCheckInCheckOuts.Where(x => x.IPAddress == IPAddress && x.DateTime.Date == dt.Date).ToList();
        //}

        public static string CheckEmpTimeAttRequestThreshold(DateTime startDate, DateTime endDate)
        {
            int EmpTimeAttRequestThreshold = -1;
            string msg = "";

            if (CommonManager.Setting.EmpTimeAttRequestThreshold != null)
                EmpTimeAttRequestThreshold = CommonManager.Setting.EmpTimeAttRequestThreshold.Value;

            DateTime today = DateTime.Today;

            if (EmpTimeAttRequestThreshold != -1)
            {
                if (EmpTimeAttRequestThreshold == 0)
                {
                    if (startDate.Date != today || endDate.Date != today)
                        msg = "Time Attendance can be requested for today only.";
                }
                else if (EmpTimeAttRequestThreshold == 1)
                {
                    if ((GetWeekNumber(today) != GetWeekNumber(startDate)) || (GetWeekNumber(today) != GetWeekNumber(endDate)))
                        msg = "Time Attendance can be requested for this week only.";
                }
                else
                {
                    if ((today.Date.Month != startDate.Date.Month) || (today.Date.Month != endDate.Date.Month))
                        msg = "Time Attendance can be requested for this month only.";
                }
            }

            return msg;
        }

        public static int GetWeekNumber(DateTime dtPassed)
        {
            CultureInfo ciCurr = CultureInfo.CurrentCulture;
            int weekNum = ciCurr.Calendar.GetWeekOfYear(dtPassed, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
            return weekNum;
        }

        public static void SendEmailTimeCommentRequset(AttendanceEmpComment obj)
        {
            string to = "", bcc = "", body = "", subject = "";

            if (obj.ApprovalId1 != null && obj.ApprovalId2 != null)
            {
                to = UserManager.GetEmployeeUserEmail(obj.ApprovalId1.Value).Trim();
                bcc = UserManager.GetEmployeeUserEmail(obj.ApprovalId2.Value).Trim();
            }
            else if (obj.ApprovalId1 != null)
                to = UserManager.GetEmployeeUserEmail(obj.ApprovalId1.Value).Trim();
            else
                to = UserManager.GetEmployeeUserEmail(obj.ApprovalId2.Value).Trim();

            if (to == "")
                return;

            subject = "Late Attendance Approval of " + EmployeeManager.GetEmployeeById(SessionManager.CurrentLoggedInEmployeeId).Name;
            body = "You are requested to approve the late attendance of " + EmployeeManager.GetEmployeeById(SessionManager.CurrentLoggedInEmployeeId).Name + "  for " +
                obj.AttendanceDate.Value.ToString("dd-MMM-yyyy").ToString();

            SMTPHelper.SendAsyncMail(to, body, subject, bcc);
        }


        public static void SendEmail(TimeRequest obj, bool timeRequestTeamUsingRecommendApproval, bool sendToRecommender)
        {
            string to = "", bcc = "", body = "", subject = "";

            subject = "Time Attendance Approval Request of " + EmployeeManager.GetEmployeeById(obj.EmployeeId.Value).Name;

            body = "You are requested to approve the time attendance of " + EmployeeManager.GetEmployeeById(obj.EmployeeId.Value).Name + " starting from " +
                obj.StartDateEng.Value.ToString("dd-MMM-yyyy") + " to " + obj.EndDateEng.Value.ToString("dd-MMM-yyyy");

            if (timeRequestTeamUsingRecommendApproval)
            {
                if (sendToRecommender)
                {
                    to = UserManager.GetEmployeeUserEmail(obj.RecommenderEmployeeId.Value).Trim();
                    subject = "Time Attendance Recommendation Request of " + EmployeeManager.GetEmployeeById(obj.EmployeeId.Value).Name;
                    body = "You are requested to recommend the time attendance of " + EmployeeManager.GetEmployeeById(obj.EmployeeId.Value).Name + " starting from " +
                            obj.StartDateEng.Value.ToString("dd-MMM-yyyy") + " to " + obj.EndDateEng.Value.ToString("dd-MMM-yyyy");
                }
                else
                    to = UserManager.GetEmployeeUserEmail(obj.ApprovalEmployeeId.Value).Trim();
            }
            else
            {
                if (obj.ApprovalId1 != null && obj.ApprovalId2 != null)
                {
                    to = UserManager.GetEmployeeUserEmail(obj.ApprovalId1.Value).Trim();
                    bcc = UserManager.GetEmployeeUserEmail(obj.ApprovalId2.Value).Trim();
                }
                else if (obj.ApprovalId1 != null)
                    to = UserManager.GetEmployeeUserEmail(obj.ApprovalId1.Value).Trim();
                else
                    to = UserManager.GetEmployeeUserEmail(obj.ApprovalId2.Value).Trim();
            }

            if (to == "")
                return;

            SMTPHelper.SendAsyncMail(to, body, subject, bcc);
        }

        public static List<TimeRequest> GetTimeRequestsForApprovalById()
        {
            List<TimeRequest> list = new List<TimeRequest>();

            if (SessionManager.CurrentLoggedInEmployeeId == 0)
                list = PayrollDataContext.TimeRequests.Where(x => x.Status == (int)AttendanceRequestStatusEnum.Saved).OrderBy(x => x.EmployeeId).ToList();
            else
                list = PayrollDataContext.TimeRequests.Where(x =>
                    (x.Status == (int)AttendanceRequestStatusEnum.Saved
                    && (x.EmployeeId != SessionManager.CurrentLoggedInEmployeeId)
                    &&
                    (
                        x.ApprovalId1 == SessionManager.CurrentLoggedInEmployeeId ||
                        x.ApprovalId2 == SessionManager.CurrentLoggedInEmployeeId
                    ))).OrderBy(x => x.EmployeeId).ToList();

            foreach (var item in list)
            {
                item.EmployeeName = EmployeeManager.GetEmployeeById(item.EmployeeId.Value).Name;
            }

            return list;
        }
        public static List<AttendanceEmpComment> GetCommentRequest()
        {
            List<AttendanceEmpComment> list = new List<AttendanceEmpComment>();

            if (SessionManager.CurrentLoggedInEmployeeId == 0)
                list = PayrollDataContext.AttendanceEmpComments.Where(x => x.Status == (int)AttendanceRequestStatusEnum.Saved).OrderBy(x => x.EmpID).ToList();
            else
                list = PayrollDataContext.AttendanceEmpComments.Where(x =>
                    (x.Status == (int)AttendanceRequestStatusEnum.Saved
                    && (x.EmpID != SessionManager.CurrentLoggedInEmployeeId)
                    &&
                    (
                        x.ApprovalId1 == SessionManager.CurrentLoggedInEmployeeId ||
                        x.ApprovalId2 == SessionManager.CurrentLoggedInEmployeeId
                    ))).OrderBy(x => x.EmpID).ToList();

            foreach (var item in list)
            {
                item.EmployeeName = EmployeeManager.GetEmployeeById(item.EmpID.Value).Name;
            }

            return list;
        }
        // do not this type of code, will make process slow
        //public static string GetIPAddressByEmployeeId(int employeeId)
        //{
        //    string ipAddress = "";

        //    AttendanceMapping objAttendanceMapping = PayrollDataContext.AttendanceMappings.SingleOrDefault(x => x.EmployeeId == employeeId);
        //    if (objAttendanceMapping != null && objAttendanceMapping.DeviceId != null)
        //    {
        //        AttendanceCheckInCheckOut objAttendanceCheckInCheckOut = PayrollDataContext.AttendanceCheckInCheckOuts.FirstOrDefault(x => x.DeviceID == objAttendanceMapping.DeviceId);
        //        if (objAttendanceCheckInCheckOut != null && objAttendanceCheckInCheckOut.IPAddress != null)
        //            ipAddress = objAttendanceCheckInCheckOut.IPAddress;
        //    }

        //    return ipAddress;
        //}

        //public static Status AssignTimeAttendance(TimeRequest objTimeRequest)
        //{
        //    Status status = new Status();

        //    SetConnectionPwd(PayrollDataContext.Connection);
        //    PayrollDataContext.Transaction = PayrollDataContext.Connection.BeginTransaction();

        //    try
        //    {

        //        foreach (TimeRequestLine itemTimeReqLine in objTimeRequest.TimeRequestLines)
        //        {
        //            Guid objCheckInGuid = new Guid();
        //            Guid objCheckOutGuid = new Guid();

        //            DAL.AttendanceMapping map = BLL.BaseBiz.PayrollDataContext.AttendanceMappings
        //                      .SingleOrDefault(x => x.EmployeeId == objTimeRequest.EmployeeId.Value);

        //            if (map == null)
        //            {
        //                status.ErrorMessage = "Employee mapping with the device does not exists.";
        //                status.IsSuccess = false;
        //                return status;
        //            }

        //            if (itemTimeReqLine.InTime != null)
        //            {
        //                //AttendanceCheckInCheckOut dbObjAttendanceCheckInCheckOut = PayrollDataContext.AttendanceCheckInCheckOuts.SingleOrDefault(x => x.DeviceID == map.DeviceId && x.DateTime.Date == itemTimeReqLine.DateEng.Value.Date && x.InOutMode == (int)ChkINOUTEnum.ChkIn);
        //                //if (dbObjAttendanceCheckInCheckOut != null)
        //                //{
        //                //    dbObjAttendanceCheckInCheckOut.AuthenticationType = (int)TimeAttendanceAuthenticationType.TimeRequest;
        //                //    dbObjAttendanceCheckInCheckOut.ModificationRemarks = "Time Request Attendance";
        //                //    dbObjAttendanceCheckInCheckOut.ManualVisibleTime = AttendanceManager.CalculateManualVisibleTime(itemTimeReqLine.InTime.Value);
        //                //    dbObjAttendanceCheckInCheckOut.DateTime = new DateTime(itemTimeReqLine.DateEng.Value.Date.Year, itemTimeReqLine.DateEng.Value.Date.Month, itemTimeReqLine.DateEng.Value.Date.Day, itemTimeReqLine.InTime.Value.Hours, itemTimeReqLine.InTime.Value.Minutes, itemTimeReqLine.InTime.Value.Seconds);
        //                //    dbObjAttendanceCheckInCheckOut.Date = dbObjAttendanceCheckInCheckOut.DateTime.Date;
        //                //    dbObjAttendanceCheckInCheckOut.ModifiedBy = SessionManager.CurrentLoggedInUserID;
        //                //    dbObjAttendanceCheckInCheckOut.ModifiedOn = DateTime.Now;
        //                //    objCheckInGuid = dbObjAttendanceCheckInCheckOut.ChkInOutID;
        //                //    PayrollDataContext.SubmitChanges();
        //                //}
        //                //else
        //                {

        //                    AttendanceCheckInCheckOut chkInOut = new AttendanceCheckInCheckOut();
        //                    chkInOut.ChkInOutID = Guid.NewGuid();
        //                    chkInOut.DeviceID = map.DeviceId;
        //                    chkInOut.AuthenticationType = (int)TimeAttendanceAuthenticationType.TimeRequest;
        //                    chkInOut.InOutMode = (int)ChkINOUTEnum.ChkIn;
        //                    chkInOut.ModifiedBy = SessionManager.CurrentLoggedInUserID;
        //                    chkInOut.ModifiedOn = DateTime.Now;
        //                    //chkInOut.IPAddress = HttpContext.Current.Request.UserHostAddress;
        //                    chkInOut.ManualVisibleTime = AttendanceManager.CalculateManualVisibleTime(itemTimeReqLine.InTime.Value);

        //                    chkInOut.DateTime = new DateTime(itemTimeReqLine.DateEng.Value.Date.Year, itemTimeReqLine.DateEng.Value.Date.Month, itemTimeReqLine.DateEng.Value.Date.Day, itemTimeReqLine.InTime.Value.Hours, itemTimeReqLine.InTime.Value.Minutes, itemTimeReqLine.InTime.Value.Seconds);
        //                    chkInOut.Date = chkInOut.DateTime.Date;
        //                    chkInOut.ModificationRemarks = "Time Request Attendance";
        //                    chkInOut.IPAddress = objTimeRequest.IPAddress;

        //                    PayrollDataContext.AttendanceCheckInCheckOuts.InsertOnSubmit(chkInOut);
        //                    PayrollDataContext.SubmitChanges();
        //                    objCheckInGuid = chkInOut.ChkInOutID;
        //                }
        //            }


        //            if (itemTimeReqLine.OutTime != null)
        //            {
        //                DateTime outDateTime = new DateTime();
        //                if (itemTimeReqLine.OvernightShift != null && itemTimeReqLine.OvernightShift.Value)
        //                    outDateTime = itemTimeReqLine.DateEng.Value.AddDays(1);
        //                else
        //                    outDateTime = itemTimeReqLine.DateEng.Value;

        //                //AttendanceCheckInCheckOut dbObjAttendanceCheckInCheckOut = PayrollDataContext.AttendanceCheckInCheckOuts.SingleOrDefault(x => x.DeviceID == map.DeviceId && x.DateTime.Date == itemTimeReqLine.DateEng.Value.Date && x.InOutMode == (int)ChkINOUTEnum.ChkOut);
        //                //if (dbObjAttendanceCheckInCheckOut != null)
        //                //{
        //                //    dbObjAttendanceCheckInCheckOut.AuthenticationType = (int)TimeAttendanceAuthenticationType.TimeRequest;
        //                //    dbObjAttendanceCheckInCheckOut.ModificationRemarks = "Time Request Attendance";
        //                //    dbObjAttendanceCheckInCheckOut.ManualVisibleTime = AttendanceManager.CalculateManualVisibleTime(itemTimeReqLine.OutTime.Value);

        //                //    dbObjAttendanceCheckInCheckOut.DateTime = new DateTime(outDateTime.Date.Year, outDateTime.Date.Month, outDateTime.Date.Day, itemTimeReqLine.OutTime.Value.Hours, itemTimeReqLine.OutTime.Value.Minutes, itemTimeReqLine.OutTime.Value.Seconds);

        //                //    dbObjAttendanceCheckInCheckOut.Date = itemTimeReqLine.DateEng.Value.Date;
        //                //    dbObjAttendanceCheckInCheckOut.ModifiedBy = SessionManager.CurrentLoggedInUserID;
        //                //    dbObjAttendanceCheckInCheckOut.ModifiedOn = DateTime.Now;
        //                //    PayrollDataContext.SubmitChanges();
        //                //    objCheckOutGuid = dbObjAttendanceCheckInCheckOut.ChkInOutID;
        //                //}
        //                //else
        //                {

        //                    AttendanceCheckInCheckOut chkInOut = new AttendanceCheckInCheckOut();
        //                    chkInOut.ChkInOutID = Guid.NewGuid();
        //                    chkInOut.DeviceID = map.DeviceId;
        //                    chkInOut.AuthenticationType = (int)TimeAttendanceAuthenticationType.TimeRequest;
        //                    chkInOut.InOutMode = (int)ChkINOUTEnum.ChkOut;
        //                    //chkInOut.IPAddress = HttpContext.Current.Request.UserHostAddress;
        //                    chkInOut.ManualVisibleTime = AttendanceManager.CalculateManualVisibleTime(itemTimeReqLine.OutTime.Value);

        //                    chkInOut.DateTime = new DateTime(outDateTime.Date.Year, outDateTime.Date.Month, outDateTime.Date.Day, itemTimeReqLine.OutTime.Value.Hours, itemTimeReqLine.OutTime.Value.Minutes, itemTimeReqLine.OutTime.Value.Seconds);

        //                    chkInOut.Date = itemTimeReqLine.DateEng.Value.Date;
        //                    chkInOut.ModificationRemarks = "Time Request Attendance";
        //                    chkInOut.IPAddress = objTimeRequest.IPAddress;
        //                    chkInOut.ModifiedBy = SessionManager.CurrentLoggedInUserID;
        //                    chkInOut.ModifiedOn = DateTime.Now;
        //                    PayrollDataContext.AttendanceCheckInCheckOuts.InsertOnSubmit(chkInOut);
        //                    PayrollDataContext.SubmitChanges();
        //                    objCheckOutGuid = chkInOut.ChkInOutID;
        //                }
        //            }

        //            if (itemTimeReqLine.InNote != null)
        //            {
        //                if (itemTimeReqLine.DateEng.Value.Date < GetAttendanceCommentThreshold())
        //                { }
        //                else
        //                {
        //                    AttendanceCheckInCheckOutComment dbObjAttendanceCheckInCheckOutComment = PayrollDataContext.AttendanceCheckInCheckOutComments.SingleOrDefault(x => x.EmpID == objTimeRequest.EmployeeId.Value
        //                                                                    && x.AttendanceDate.Value.Date == itemTimeReqLine.DateEng.Value.Date
        //                                                                    && x.InOutMode == (int)ChkINOUTEnum.ChkIn);

        //                    if (dbObjAttendanceCheckInCheckOutComment != null)
        //                    {
        //                        dbObjAttendanceCheckInCheckOutComment.Note = itemTimeReqLine.InNote;
        //                        PayrollDataContext.SubmitChanges();
        //                    }
        //                    else
        //                    {
        //                        AttendanceCheckInCheckOutComment comment = new AttendanceCheckInCheckOutComment();
        //                        comment.ChkInOutID = objCheckInGuid;
        //                        comment.AttendanceDate = itemTimeReqLine.DateEng;
        //                        comment.InOutMode = (int)ChkINOUTEnum.ChkIn;
        //                        comment.Note = itemTimeReqLine.InNote;
        //                        comment.EmpID = objTimeRequest.EmployeeId;
        //                        comment.LineID = Guid.NewGuid();
        //                        PayrollDataContext.AttendanceCheckInCheckOutComments.InsertOnSubmit(comment);
        //                        PayrollDataContext.SubmitChanges();
        //                    }
        //                }
        //            }


        //            if (itemTimeReqLine.OutNote != null)
        //            {
        //                if (itemTimeReqLine.DateEng.Value.Date < GetAttendanceCommentThreshold())
        //                { }
        //                else
        //                {
        //                    AttendanceCheckInCheckOutComment dbObjAttendanceCheckInCheckOutComment = PayrollDataContext.AttendanceCheckInCheckOutComments.SingleOrDefault(x => x.EmpID == objTimeRequest.EmployeeId.Value
        //                                                                    && x.AttendanceDate.Value.Date == itemTimeReqLine.DateEng.Value.Date
        //                                                                    && x.InOutMode == (int)ChkINOUTEnum.ChkOut);

        //                    if (dbObjAttendanceCheckInCheckOutComment != null)
        //                    {
        //                        dbObjAttendanceCheckInCheckOutComment.Note = itemTimeReqLine.OutNote;
        //                        PayrollDataContext.SubmitChanges();
        //                    }
        //                    else
        //                    {
        //                        AttendanceCheckInCheckOutComment comment = new AttendanceCheckInCheckOutComment();
        //                        comment.ChkInOutID = objCheckOutGuid;
        //                        comment.AttendanceDate = itemTimeReqLine.DateEng;
        //                        comment.InOutMode = (int)ChkINOUTEnum.ChkOut;
        //                        comment.Note = itemTimeReqLine.OutNote;
        //                        comment.EmpID = objTimeRequest.EmployeeId;
        //                        comment.LineID = Guid.NewGuid();
        //                        PayrollDataContext.AttendanceCheckInCheckOutComments.InsertOnSubmit(comment);
        //                        PayrollDataContext.SubmitChanges();
        //                    }
        //                }
        //            }                    

        //        }

        //        objTimeRequest.ApprovedBy = SessionManager.CurrentLoggedInUserID;
        //        objTimeRequest.ApprovedOn = DateTime.Now;
        //        PayrollDataContext.TimeRequests.InsertOnSubmit(objTimeRequest);
        //        PayrollDataContext.SubmitChanges();

        //        PayrollDataContext.Transaction.Commit();
        //        status.IsSuccess = true;
        //    }
        //    catch (Exception exp)
        //    {
        //        Log.log("Error while assigning attendance time", exp);
        //        PayrollDataContext.Transaction.Rollback();
        //        status.IsSuccess = false;
        //    }
        //    finally
        //    {

        //    }

        //    return status;
        //}


        //public static Status AssignTimeAttendanceHR(TimeRequest objTimeRequest)
        //{
        //    Status status = new Status();

        //    SetConnectionPwd(PayrollDataContext.Connection);
        //    PayrollDataContext.Transaction = PayrollDataContext.Connection.BeginTransaction();

        //    try
        //    {

        //        foreach (TimeRequestLine itemTimeReqLine in objTimeRequest.TimeRequestLines)
        //        {
        //            Guid objCheckInGuid = new Guid();
        //            Guid objCheckOutGuid = new Guid();

        //            DAL.AttendanceMapping map = BLL.BaseBiz.PayrollDataContext.AttendanceMappings
        //                      .SingleOrDefault(x => x.EmployeeId == objTimeRequest.EmployeeId.Value);

        //            if (map == null)
        //            {
        //                status.ErrorMessage = "Employee mapping with the device does not exists.";
        //                status.IsSuccess = false;
        //                return status;
        //            }

        //            if (itemTimeReqLine.InTime != null)
        //            {
        //                //AttendanceCheckInCheckOut dbObjAttendanceCheckInCheckOut = PayrollDataContext.AttendanceCheckInCheckOuts.SingleOrDefault(x => x.DeviceID == map.DeviceId && x.DateTime.Date == itemTimeReqLine.DateEng.Value.Date && x.InOutMode == (int)ChkINOUTEnum.ChkIn);
        //                //if (dbObjAttendanceCheckInCheckOut != null)
        //                //{
        //                //    dbObjAttendanceCheckInCheckOut.AuthenticationType = (int)TimeAttendanceAuthenticationType.TimeRequest;
        //                //    dbObjAttendanceCheckInCheckOut.ModificationRemarks = "Time Request Attendance";
        //                //    dbObjAttendanceCheckInCheckOut.ManualVisibleTime = AttendanceManager.CalculateManualVisibleTime(itemTimeReqLine.InTime.Value);
        //                //    dbObjAttendanceCheckInCheckOut.DateTime = new DateTime(itemTimeReqLine.DateEng.Value.Date.Year, itemTimeReqLine.DateEng.Value.Date.Month, itemTimeReqLine.DateEng.Value.Date.Day, itemTimeReqLine.InTime.Value.Hours, itemTimeReqLine.InTime.Value.Minutes, itemTimeReqLine.InTime.Value.Seconds);
        //                //    dbObjAttendanceCheckInCheckOut.Date = dbObjAttendanceCheckInCheckOut.DateTime.Date;
        //                //    dbObjAttendanceCheckInCheckOut.ModifiedBy = SessionManager.CurrentLoggedInUserID;
        //                //    dbObjAttendanceCheckInCheckOut.ModifiedOn = DateTime.Now;
        //                //    objCheckInGuid = dbObjAttendanceCheckInCheckOut.ChkInOutID;
        //                //    PayrollDataContext.SubmitChanges();
        //                //}
        //                //else
        //                {

        //                    AttendanceCheckInCheckOut chkInOut = new AttendanceCheckInCheckOut();
        //                    chkInOut.ChkInOutID = Guid.NewGuid();
        //                    chkInOut.DeviceID = map.DeviceId;
        //                    chkInOut.AuthenticationType = (int)TimeAttendanceAuthenticationType.TimeRequest;
        //                    chkInOut.InOutMode = (int)ChkINOUTEnum.ChkIn;
        //                    chkInOut.ModifiedBy = SessionManager.CurrentLoggedInUserID;
        //                    chkInOut.ModifiedOn = DateTime.Now;
        //                    //chkInOut.IPAddress = HttpContext.Current.Request.UserHostAddress;
        //                    chkInOut.ManualVisibleTime = AttendanceManager.CalculateManualVisibleTime(itemTimeReqLine.InTime.Value);

        //                    chkInOut.DateTime = new DateTime(itemTimeReqLine.DateEng.Value.Date.Year, itemTimeReqLine.DateEng.Value.Date.Month, itemTimeReqLine.DateEng.Value.Date.Day, itemTimeReqLine.InTime.Value.Hours, itemTimeReqLine.InTime.Value.Minutes, itemTimeReqLine.InTime.Value.Seconds);
        //                    chkInOut.Date = chkInOut.DateTime.Date;
        //                    chkInOut.ModificationRemarks = "Time Request Attendance";
        //                    chkInOut.IPAddress = objTimeRequest.IPAddress;

        //                    PayrollDataContext.AttendanceCheckInCheckOuts.InsertOnSubmit(chkInOut);
        //                    PayrollDataContext.SubmitChanges();
        //                    objCheckInGuid = chkInOut.ChkInOutID;
        //                }
        //            }


        //            if (itemTimeReqLine.OutTime != null)
        //            {
        //                DateTime outDateTime = new DateTime();
        //                if (itemTimeReqLine.OvernightShift != null && itemTimeReqLine.OvernightShift.Value)
        //                    outDateTime = itemTimeReqLine.DateEng.Value.AddDays(1);
        //                else
        //                    outDateTime = itemTimeReqLine.DateEng.Value;

        //                //AttendanceCheckInCheckOut dbObjAttendanceCheckInCheckOut = PayrollDataContext.AttendanceCheckInCheckOuts.SingleOrDefault(x => x.DeviceID == map.DeviceId && x.DateTime.Date == itemTimeReqLine.DateEng.Value.Date && x.InOutMode == (int)ChkINOUTEnum.ChkOut);
        //                //if (dbObjAttendanceCheckInCheckOut != null)
        //                //{
        //                //    dbObjAttendanceCheckInCheckOut.AuthenticationType = (int)TimeAttendanceAuthenticationType.TimeRequest;
        //                //    dbObjAttendanceCheckInCheckOut.ModificationRemarks = "Time Request Attendance";
        //                //    dbObjAttendanceCheckInCheckOut.ManualVisibleTime = AttendanceManager.CalculateManualVisibleTime(itemTimeReqLine.OutTime.Value);
        //                //    dbObjAttendanceCheckInCheckOut.DateTime = new DateTime(outDateTime.Date.Year, outDateTime.Date.Month, outDateTime.Date.Day, itemTimeReqLine.OutTime.Value.Hours, itemTimeReqLine.OutTime.Value.Minutes, itemTimeReqLine.OutTime.Value.Seconds);
        //                //    dbObjAttendanceCheckInCheckOut.Date = itemTimeReqLine.DateEng.Value.Date;
        //                //    dbObjAttendanceCheckInCheckOut.ModifiedBy = SessionManager.CurrentLoggedInUserID;
        //                //    dbObjAttendanceCheckInCheckOut.ModifiedOn = DateTime.Now;
        //                //    PayrollDataContext.SubmitChanges();
        //                //    objCheckOutGuid = dbObjAttendanceCheckInCheckOut.ChkInOutID;
        //                //}
        //                //else
        //                {

        //                    AttendanceCheckInCheckOut chkInOut = new AttendanceCheckInCheckOut();
        //                    chkInOut.ChkInOutID = Guid.NewGuid();
        //                    chkInOut.DeviceID = map.DeviceId;
        //                    chkInOut.AuthenticationType = (int)TimeAttendanceAuthenticationType.TimeRequest;
        //                    chkInOut.InOutMode = (int)ChkINOUTEnum.ChkOut;
        //                    //chkInOut.IPAddress = HttpContext.Current.Request.UserHostAddress;
        //                    chkInOut.ManualVisibleTime = AttendanceManager.CalculateManualVisibleTime(itemTimeReqLine.OutTime.Value);

        //                    chkInOut.DateTime = new DateTime(outDateTime.Date.Year, outDateTime.Date.Month, outDateTime.Date.Day, itemTimeReqLine.OutTime.Value.Hours, itemTimeReqLine.OutTime.Value.Minutes, itemTimeReqLine.OutTime.Value.Seconds);
        //                    chkInOut.Date = itemTimeReqLine.DateEng.Value.Date;
        //                    chkInOut.ModificationRemarks = "Time Request Attendance";
        //                    chkInOut.IPAddress = objTimeRequest.IPAddress;
        //                    chkInOut.ModifiedBy = SessionManager.CurrentLoggedInUserID;
        //                    chkInOut.ModifiedOn = DateTime.Now;
        //                    PayrollDataContext.AttendanceCheckInCheckOuts.InsertOnSubmit(chkInOut);
        //                    PayrollDataContext.SubmitChanges();
        //                    objCheckOutGuid = chkInOut.ChkInOutID;
        //                }
        //            }

        //            if (itemTimeReqLine.InNote != null)
        //            {
        //                if (itemTimeReqLine.DateEng.Value.Date < GetAttendanceCommentThreshold())
        //                { }
        //                else
        //                {
        //                    AttendanceCheckInCheckOutComment dbObjAttendanceCheckInCheckOutComment = PayrollDataContext.AttendanceCheckInCheckOutComments.SingleOrDefault(x => x.EmpID == objTimeRequest.EmployeeId.Value
        //                                                                    && x.AttendanceDate.Value.Date == itemTimeReqLine.DateEng.Value.Date
        //                                                                    && x.InOutMode == (int)ChkINOUTEnum.ChkIn);

        //                    if (dbObjAttendanceCheckInCheckOutComment != null)
        //                    {
        //                        dbObjAttendanceCheckInCheckOutComment.Note = itemTimeReqLine.InNote;
        //                        PayrollDataContext.SubmitChanges();
        //                    }
        //                    else
        //                    {
        //                        AttendanceCheckInCheckOutComment comment = new AttendanceCheckInCheckOutComment();
        //                        comment.ChkInOutID = objCheckInGuid;
        //                        comment.AttendanceDate = itemTimeReqLine.DateEng;
        //                        comment.InOutMode = (int)ChkINOUTEnum.ChkIn;
        //                        comment.Note = itemTimeReqLine.InNote;
        //                        comment.EmpID = objTimeRequest.EmployeeId;
        //                        comment.LineID = Guid.NewGuid();
        //                        PayrollDataContext.AttendanceCheckInCheckOutComments.InsertOnSubmit(comment);
        //                        PayrollDataContext.SubmitChanges();
        //                    }
        //                }
        //            }


        //            if (itemTimeReqLine.OutNote != null)
        //            {
        //                if (itemTimeReqLine.DateEng.Value.Date < GetAttendanceCommentThreshold())
        //                { }
        //                else
        //                {
        //                    AttendanceCheckInCheckOutComment dbObjAttendanceCheckInCheckOutComment = PayrollDataContext.AttendanceCheckInCheckOutComments.SingleOrDefault(x => x.EmpID == objTimeRequest.EmployeeId.Value
        //                                                                    && x.AttendanceDate.Value.Date == itemTimeReqLine.DateEng.Value.Date
        //                                                                    && x.InOutMode == (int)ChkINOUTEnum.ChkOut);

        //                    if (dbObjAttendanceCheckInCheckOutComment != null)
        //                    {
        //                        dbObjAttendanceCheckInCheckOutComment.Note = itemTimeReqLine.OutNote;
        //                        PayrollDataContext.SubmitChanges();
        //                    }
        //                    else
        //                    {
        //                        AttendanceCheckInCheckOutComment comment = new AttendanceCheckInCheckOutComment();
        //                        comment.ChkInOutID = objCheckOutGuid;
        //                        comment.AttendanceDate = itemTimeReqLine.DateEng;
        //                        comment.InOutMode = (int)ChkINOUTEnum.ChkOut;
        //                        comment.Note = itemTimeReqLine.OutNote;
        //                        comment.EmpID = objTimeRequest.EmployeeId;
        //                        comment.LineID = Guid.NewGuid();
        //                        PayrollDataContext.AttendanceCheckInCheckOutComments.InsertOnSubmit(comment);
        //                        PayrollDataContext.SubmitChanges();
        //                    }
        //                }
        //            }

        //        }            

        //        PayrollDataContext.Transaction.Commit();
        //        status.IsSuccess = true;
        //    }
        //    catch (Exception exp)
        //    {
        //        Log.log("Error while assigning attendance time", exp);
        //        PayrollDataContext.Transaction.Rollback();
        //        status.IsSuccess = false;
        //    }
        //    finally
        //    {

        //    }

        //    return status;
        //}

        public static List<GetTimeRequestsForApprovalResult> GetTimeRequestForApproval(int employeeId, int managerId, DateTime? fromDate, DateTime? toDate, int currentPage, int pageSize, out int total, string status)
        {
            total = 0;
            Setting setting = OvertimeManager.GetSetting();
            bool timeRequestTeamUsingRecommendApproval = false;
            if (setting.TimeRequestTeamUsingRecommendApproval != null && setting.TimeRequestTeamUsingRecommendApproval.Value)
                timeRequestTeamUsingRecommendApproval = true;

            List<GetTimeRequestsForApprovalResult> list = PayrollDataContext.GetTimeRequestsForApproval(fromDate,
                toDate, managerId, employeeId, currentPage, pageSize, status, timeRequestTeamUsingRecommendApproval).ToList();
            if (list.Count > 0)
                total = list[0].TotalRows.Value;
            return list;

        }

        public static List<GetAttendanceCommentRequestsForApprovalResult> GetAttendanceCommentRequestForApproval(int employeeId, int managerId, DateTime? fromDate, DateTime? toDate, int currentPage, int pageSize, out int total, int status
            , string sort)
        {
            total = 0;
            List<GetAttendanceCommentRequestsForApprovalResult> list = PayrollDataContext.GetAttendanceCommentRequestsForApproval(fromDate,
                toDate, managerId, employeeId, currentPage, pageSize, status, sort).ToList();
            if (list.Count > 0)
                total = list[0].TotalRows.Value;
            return list;
        }

        public static bool IsCheckedInOrCheckedOut(int checkInCheckOut)
        {
            string deviceId = PayrollDataContext.AttendanceMappings.SingleOrDefault(x => x.EmployeeId == SessionManager.CurrentLoggedInEmployeeId).DeviceId;
            if (deviceId != null)
            {
                if (PayrollDataContext.AttendanceCheckInCheckOuts.Any(x => x.DeviceID == deviceId && x.DateTime.Date == DateTime.Now.Date && x.InOutMode == checkInCheckOut))
                    return true;
            }

            return false;
        }

        public static ChangeUserActivity GetLastChangeUserActivityForEmployee()
        {
            return PayrollDataContext.ChangeUserActivities.Where(x => x.UserName == SessionManager.UserName && x.Remarks.ToLower().Contains("user logged in")).OrderByDescending(x => x.DateEng).FirstOrDefault();
        }

        public static int GetLoggedInCountForUser()
        {
            int count = 0;
            List<ChangeUserActivity> list = PayrollDataContext.ChangeUserActivities.Where(x => x.UserName == SessionManager.UserName && x.DateEng.Value.Date == DateTime.Now.Date && x.Remarks.ToLower().Contains("user logged in")).ToList();
            count = list.Count;
            return count;
        }

        public static Status InsertCheckInAndComments(AttendanceCheckInCheckOut chkInOut, AttendanceCheckInCheckOutComment comment)
        {
            Status status = new Status();

            PayrollDataContext.AttendanceCheckInCheckOuts.InsertOnSubmit(chkInOut);

            if (comment.Note != null)
            {
                comment.LineID = Guid.NewGuid();
                PayrollDataContext.AttendanceCheckInCheckOutComments.InsertOnSubmit(comment);
            }

            PayrollDataContext.SubmitChanges();
            status.IsSuccess = true;

            PayrollDataContext.SubmitChanges();


            status.IsSuccess = true;
            return status;
        }

        public static TimeRequest GetRejectedTimeRequestForEmployee()
        {
            return PayrollDataContext.TimeRequests.Where(x => x.EmployeeId == SessionManager.CurrentLoggedInEmployeeId && x.Status == (int)AttendanceRequestStatusEnum.Rejected && (x.StartDateEng.Value.Date >= DateTime.Now.Date.AddDays(-7) && x.StartDateEng.Value <= DateTime.Now.Date)).OrderByDescending(x => x.StartDateEng).FirstOrDefault();
        }

        public static CumulativeSummary GetCumulativeSummaryById(int employeeId, int payrollPeriodId)
        {
            return PayrollDataContext.CumulativeSummaries.SingleOrDefault(x => x.EmployeeId == employeeId && x.PayrollPeriodId == payrollPeriodId);
        }

        public static List<AttendanceAllEmployeeReportNewResult> GetAttendanceDailyReportNew(DateTime filterDate, int Status, string BranchID, int departmentId = -1)
        {
            List<AttendanceAllEmployeeReportNewResult> attendanceList = new List<AttendanceAllEmployeeReportNewResult>();

            string branchList = null;
            if (SessionManager.IsCustomRole)
                branchList = SessionManager.CustomRoleBranchIDList;

            AttendanceManager mgr = new AttendanceManager();

            attendanceList = PayrollDataContext.AttendanceAllEmployeeReportNew(filterDate, BranchID, branchList, departmentId).ToList();

            for (int i = 0; i < attendanceList.Count; i++)
            {
                if (string.IsNullOrEmpty(attendanceList[i].InRemarks.ToString()) != true || string.IsNullOrEmpty(attendanceList[i].RefinedOutRemarks.ToString()) != true)
                {
                    if (attendanceList[i].WorkHour != null)
                        attendanceList[i].RefinedWorkHour = new AttendanceManager().MinuteToHourMinuteConverter(Math.Abs(int.Parse(attendanceList[i].WorkHour.ToString()))).TrimStart(' ');
                }

                if (attendanceList[i].InRemarks != null)
                    attendanceList[i].InRemarksText = mgr.CheckInOutTimeCheck(1, attendanceList[i].InRemarks.Value);
                if (attendanceList[i].OutRemarks != null)
                    attendanceList[i].OutRemarksText = mgr.CheckInOutTimeCheck(2, attendanceList[i].OutRemarks.Value);
            }

            if (Status != 0 && Status != 4)
            {

                return attendanceList.Where(x => x.AtteState == Status).ToList();

            }
            else if (Status == 4)
            {
                return attendanceList.Where(x => x.AtteState == Status || x.AtteState == 2).ToList();
            }
            else// if (Status == 0)
                return attendanceList;

            // return returnList;
        }

        public static void SaveUpdateEmpAttendanceReportColumns(List<EmpAttendanceReportColumn> list)
        {
            List<EmpAttendanceReportColumn> dbList = PayrollDataContext.EmpAttendanceReportColumns.ToList();
            PayrollDataContext.EmpAttendanceReportColumns.DeleteAllOnSubmit(dbList);
            PayrollDataContext.EmpAttendanceReportColumns.InsertAllOnSubmit(list);
            PayrollDataContext.SubmitChanges();
        }

        public static List<EmpAttendanceReportColumn> GetEmpAttendanceReportColumn()
        {
            return PayrollDataContext.EmpAttendanceReportColumns.Where(x => x.ReportType == (int)ReportTypeEnum.EmpDailyAttendendance).ToList();
        }


        public static void AbsentScheduler_DailyNew()
        {
            string logMessage = "";
            try
            {
                
                DateTime CurrentDate = GetCurrentDateAndTime().Date;
                // AttendanceManager.GenerateAttendanceEmployeeTable(-1, CurrentDate, CurrentDate, false);
                List<GetAbsentListForEmailResult> ListAbsentEmployes = GetAbsentListForEmail(CurrentDate, CurrentDate).ToList();
                List<AbsentEmpIDBO> _AbsentEmpIDList = GetAbsentEmpIDListNew(CurrentDate, CurrentDate, ListAbsentEmployes).ToList();
                List<SupervisorEmails> _SupervisorEmails = GetSupervisorEmailListForMailNew(CurrentDate, CurrentDate, ListAbsentEmployes).ToList();
                if (ListAbsentEmployes.Count == 0)
                {
                    logMessage = "\n Absent employee not found." + DateTime.Now.ToString() + "  : \n";
                    Log.SaveAdditionalLog(logMessage, LogType.Schedular);
                    return;
                }

                if (ListAbsentEmployes.Count > 0)
                {
                    SendAbsentMailForEmployee(_AbsentEmpIDList);
                    sendMailAbsentsForSupervisor(ListAbsentEmployes, _SupervisorEmails);
                }
            }
            catch (Exception exp)
            {
                logMessage = "\n Error for : " + GetCurrentDateAndTime().Date.ToString() + "  : \n" + exp.ToString();
                Log.SaveAdditionalLog(logMessage, LogType.Schedular);
            }
        }

        public static void SendAbsentMailForEmployee(List<AbsentEmpIDBO> AbsentEmpIDList)
        {
            string logMessage = "";
            try
            {
                if (!AbsentEmpIDList.Any())
                    return;


                EmailContent dbMailContentForEmployee = BLL.Manager.CommonManager.GetMailContent((int)EmailContentType.Absent);
                if (dbMailContentForEmployee == null)
                {
                    logMessage = "\n Absent Template not defined for employee " + DateTime.Now.ToString() + "  : \n";
                    Log.SaveAdditionalLog(logMessage, LogType.Schedular);
                    return;
                }

                string todayDate = GetCurrentDateAndTime().ToString("yyyy/MMM/d");
                string BodyEmployee = dbMailContentForEmployee.Body;
                string body = string.Empty;
                string subject = string.Empty;
                int count = 0;
                string SendEmail = Config.GetEmailSenderName;
                List<GetForDeviceMappingResult> _GetForDeviceMappingResult = PayrollDataContext.GetForDeviceMapping(-1, "", 0, 9999).ToList();

                foreach (AbsentEmpIDBO EmpIDInfo in AbsentEmpIDList)
                {
                    GetForDeviceMappingResult _DeviceMappingResult = _GetForDeviceMappingResult.SingleOrDefault(x => x.EmployeeId == EmpIDInfo.EmpID);
                    if (_DeviceMappingResult != null && _DeviceMappingResult.IsEnabledSkipLateEntry != null)
                    {
                        if (!_DeviceMappingResult.IsEnabledSkipLateEntry.Value)
                        {
                            subject = dbMailContentForEmployee.Subject.Replace("#TodayDate#", todayDate);
                            subject = subject.Replace("#Employee#", EmpIDInfo.Name);
                            body = BodyEmployee.Replace("#TodayDate#", todayDate);
                            body = body.Replace("#Employee#", EmpIDInfo.Name);

                            if (string.IsNullOrEmpty(EmpIDInfo.Email))
                            {
                                logMessage = "\n Email address not found for Employee " + EmpIDInfo.Name + " " + DateTime.Now.ToString() + "  : \n";
                                Log.SaveAdditionalLog(logMessage, LogType.Schedular);
                            }
                            else
                            {
                                //bool isSendSuccess = SMTPHelper.SendMailAsync(EmpIDInfo.Email, body, subject, "", SendEmail);
                                bool isSendSuccess = SMTPHelper.SendMailWebServiceOnly(EmpIDInfo.Email, body, subject, "", SendEmail);
                                if (isSendSuccess)
                                    count++;
                            }
                        }
                    }
                }

                if (count > 0)
                {
                    logMessage = "\n Email Send successfully for " + count + " Absent Employees " + DateTime.Now.ToString() + "  : \n";
                    Log.SaveAdditionalLog(logMessage, LogType.Schedular);
                    return;
                }

                else
                {
                    logMessage = "\n error while sending email " + DateTime.Now.ToString() + "  : \n";
                    Log.SaveAdditionalLog(logMessage, LogType.Schedular);
                    return;
                }


            }

            catch (Exception exp)
            {
                logMessage = "\n Error for : " + GetCurrentDateAndTime().Date.ToString() + "  : \n" + exp.ToString();
                Log.SaveAdditionalLog(logMessage, LogType.Schedular);
            }

        }


        public static void sendMailLateForSupervisor(List<GetLateListForEmailResult> ListLateEmployes, List<SupervisorEmails> ListSupervisorEmails)
        {
            string logMessage = "";

            try
            {

                if (!ListLateEmployes.Any())
                    return;
                if (!ListSupervisorEmails.Any())
                    return;

                //EmailContent dbMailContentForSupervisor = BLL.Manager.CommonManager.GetMailContent((int)EmailContentType.AttendanceAbsentdailyforSuperviser);
                //if (dbMailContentForSupervisor == null)
                //{
                //    logMessage = "\n Weekly Absent Template not defined for supervisor" + DateTime.Now.ToString() + "  : \n";
                //    Log.SaveAdditionalLog(logMessage, LogType.Schedular);
                //    return;
                //}

                string todayDate = GetCurrentDateAndTime().ToString("yyyy/MMM/d");
                int count = 0;
                int countEmailToHr = 0;
                string SenderEmail = Config.GetEmailSenderName;
                List<GetForDeviceMappingResult> _GetForDeviceMappingResult = PayrollDataContext.GetForDeviceMapping(-1, "", 0, 9999).ToList();
                string EmailsforHR = PayrollDataContext.Settings.SingleOrDefault().AbsentNotificationMails;
                if (!string.IsNullOrEmpty(EmailsforHR))
                {
                    //send absent mail for HR Email Notifier
                    bool IsDataOnAbsListForHR = true;
                    string strHTMLTblAllAbs = "<table>";

                    foreach (GetLateListForEmailResult ListAbs in ListLateEmployes)
                    {
                        GetForDeviceMappingResult _DeviceMappingResult = _GetForDeviceMappingResult.SingleOrDefault(x => x.EmployeeId == ListAbs.EmployeeId);
                        if (_DeviceMappingResult != null && _DeviceMappingResult.IsEnabledSkipLateEntry != null)
                        {
                            if (!_DeviceMappingResult.IsEnabledSkipLateEntry.Value)
                            {
                                IsDataOnAbsListForHR = true;

                                TimeSpan t = TimeSpan.FromMinutes(ListAbs.LateMin == null ? 0 : ListAbs.LateMin.Value);
                                string lateBy =  (t.Days * 24) + t.Hours + ":" + t.Minutes.ToString("00");

                                strHTMLTblAllAbs += "<tr>" + "<td> " + " (" + ListAbs.EmployeeId + ") " + ListAbs.NAME + " Late by : " + lateBy + "</td>" + "</tr>";
                            }
                        }
                    }

                    strHTMLTblAllAbs += "</table>";

                    string[] HrEmails = EmailsforHR.Split(',');
                    string body = "Following Employee are late in office today <br>" + strHTMLTblAllAbs; //dbMailContentForSupervisor.Body.Replace("#List#", strHTMLTblAllAbs);
                   // dbMailContentForSupervisor.Subject = dbMailContentForSupervisor.Subject.Replace("#TodayDate#", todayDate);
                    //dbMailContentForSupervisor
                    string Subject = "Late In Employee Notification"; 
                    foreach (string ToEmail in HrEmails)
                    {
                        if (!string.IsNullOrEmpty(ToEmail) && IsDataOnAbsListForHR == true)
                        {
                           // bool isSendSuccess = SMTPHelper.SendMailAsync(ToEmail, body, Subject, "", SenderEmail);
                            bool isSendSuccess = SMTPHelper.SendMailWebServiceOnly(ToEmail, body, Subject, "", SenderEmail);
                            if (isSendSuccess)
                                countEmailToHr++;
                        }
                    }

                }

                //-------------------------------
                //send mail for Supervisor---------------------------------------
                string strHTMLTbl = string.Empty;
                ListLateEmployes = ListLateEmployes.Where(x => !string.IsNullOrEmpty(x.SupervisorEmail)).ToList();

                foreach (SupervisorEmails _SupervisorEmails in ListSupervisorEmails)
                {
                    List<GetLateListForEmailResult> _ListOfSelectedEmployee = ListLateEmployes.Where(x => x.SupervisorEmail == _SupervisorEmails.SupervisorEmail).ToList();

                    string strDates = string.Empty;
                    bool IsDataOnList = false;
                    strHTMLTbl = "<table>";
                    foreach (GetLateListForEmailResult ListAbs in _ListOfSelectedEmployee)
                    {
                        GetForDeviceMappingResult _DeviceMappingResult = _GetForDeviceMappingResult.SingleOrDefault(x => x.EmployeeId == ListAbs.EmployeeId);
                        if (_DeviceMappingResult != null && _DeviceMappingResult.IsEnabledSkipLateEntry != null)
                        {
                            if (string.IsNullOrEmpty(strHTMLTbl))
                                strHTMLTbl = "<tr>" + "<td> " + ListAbs.NAME + "    </td>" + "<td></td>" + "</tr>";
                            else
                                strHTMLTbl += "<tr>" + "<td> " + ListAbs.NAME + "    </td>" + "<td></td>" + "</tr>";
                            IsDataOnList = true;
                        }
                    }
                    strHTMLTbl += "</table>";


                    string SVbodycontents = "Following Employee are late in office today <br>" + strHTMLTbl; //dbMailContentForSupervisor.Body.Replace("#List#", strHTMLTblAllAbs);
                   // dbMailContentForSupervisor.Subject = dbMailContentForSupervisor.Subject.Replace("#TodayDate#", todayDate);
                    //dbMailContentForSupervisor
                    string SVSubject = "Late In Employee Notification"; 

                    //dbMailContentForSupervisor.Body.Replace("#List#", strHTMLTbl);
                   // dbMailContentForSupervisor.Subject = dbMailContentForSupervisor.Subject.Replace("#TodayDate#", todayDate);
                    
                    string[] SupervisorEmails = _SupervisorEmails.SupervisorEmail.Split(',');

                    foreach (string ToEmail in SupervisorEmails)
                    {
                        if (!string.IsNullOrEmpty(ToEmail) && IsDataOnList == true)
                        {
                            bool isSendSuccess = SMTPHelper.SendMailAsyncWebServiceOnly(ToEmail, SVbodycontents, SVSubject, "", SenderEmail);
                            if (isSendSuccess)
                                count++;
                        }
                    }
                }

                if (count > 0)
                {
                    logMessage = "\n Email Send successfully for " + count + " Supervisor " + DateTime.Now.ToString() + "  : \n ";
                    Log.SaveAdditionalLog(logMessage, LogType.Schedular);

                }
                //Email send for HR Manager
                if (countEmailToHr > 0)
                {
                    logMessage = "\n Email Send successfully for " + countEmailToHr + " HR Manager " + DateTime.Now.ToString() + "  : \n ";
                    Log.SaveAdditionalLog(logMessage, LogType.Schedular);
                }

                if (countEmailToHr == 0 && count == 0)
                {
                    logMessage = "\n error while sending email" + DateTime.Now.ToString() + "  : \n";
                    Log.SaveAdditionalLog(logMessage, LogType.Schedular);

                }
            }
            catch (Exception exp)
            {
                logMessage = "\n Error for : " + GetCurrentDateAndTime().Date.ToString() + "  : \n" + exp.ToString();
                Log.SaveAdditionalLog(logMessage, LogType.Schedular);
                return;
            }
        }

        public static void sendMailAbsentsForSupervisor(List<GetAbsentListForEmailResult> ListAbsentEmployes, List<SupervisorEmails> ListSupervisorEmails)
        {
            string logMessage = "";

            try
            {
                if (!ListAbsentEmployes.Any())
                    return;
                if (!ListSupervisorEmails.Any())
                    return;

                EmailContent dbMailContentForSupervisor = BLL.Manager.CommonManager.GetMailContent((int)EmailContentType.AttendanceAbsentdailyforSuperviser);
                if (dbMailContentForSupervisor == null)
                {
                    logMessage = "\n Weekly Absent Template not defined for supervisor" + DateTime.Now.ToString() + "  : \n";
                    Log.SaveAdditionalLog(logMessage, LogType.Schedular);
                    return;
                }

                string todayDate = GetCurrentDateAndTime().ToString("yyyy/MMM/d");
                int count = 0;
                int countEmailToHr = 0;
                string SenderEmail = Config.GetEmailSenderName;
                List<GetForDeviceMappingResult> _GetForDeviceMappingResult = PayrollDataContext.GetForDeviceMapping(-1, "", 0, 9999).ToList();
                string EmailsforHR = PayrollDataContext.Settings.SingleOrDefault().AbsentNotificationMails;
                if (!string.IsNullOrEmpty(EmailsforHR))
                {
                    //send absent mail for HR Email Notifier
                    bool IsDataOnAbsListForHR = true;
                    string strHTMLTblAllAbs = "<table>";
                    foreach (GetAbsentListForEmailResult ListAbs in ListAbsentEmployes)
                    {
                        GetForDeviceMappingResult _DeviceMappingResult = _GetForDeviceMappingResult.SingleOrDefault(x => x.EmployeeId == ListAbs.EmployeeId);
                        if (_DeviceMappingResult != null && _DeviceMappingResult.IsEnabledSkipLateEntry != null)
                        {
                            if (!_DeviceMappingResult.IsEnabledSkipLateEntry.Value)
                            {
                                IsDataOnAbsListForHR = true;
                                strHTMLTblAllAbs += "<tr>" + "<td> " + " (" + ListAbs.EmployeeId + ") " + ListAbs.NAME + "</td>" + "</tr>";
                            }
                        }
                    }

                    strHTMLTblAllAbs += "</table>";

                    string[] HrEmails = EmailsforHR.Split(',');
                    string body = dbMailContentForSupervisor.Body.Replace("#List#", strHTMLTblAllAbs);
                    dbMailContentForSupervisor.Subject = dbMailContentForSupervisor.Subject.Replace("#TodayDate#", todayDate);

                    foreach (string ToEmail in HrEmails)
                    {
                        if (!string.IsNullOrEmpty(ToEmail) && IsDataOnAbsListForHR==true)
                        {
                           // bool isSendSuccess = SMTPHelper.SendMailAsync(ToEmail, body, dbMailContentForSupervisor.Subject, "", SenderEmail);
                            bool isSendSuccess = SMTPHelper.SendMailWebServiceOnly(ToEmail, body, dbMailContentForSupervisor.Subject, "", SenderEmail);
                            
                            if (isSendSuccess)
                                countEmailToHr++;
                        }
                    }

                }

                 //-------------------------------
               //send mail for Supervisor---------------------------------------
                string strHTMLTbl = string.Empty;
                ListAbsentEmployes =  ListAbsentEmployes.Where(x => !string.IsNullOrEmpty(x.SupervisorEmail)).ToList();

                foreach (SupervisorEmails _SupervisorEmails in ListSupervisorEmails)
                {
                    List<GetAbsentListForEmailResult> _ListOfSelectedEmployee = ListAbsentEmployes.Where(x => x.SupervisorEmail == _SupervisorEmails.SupervisorEmail).ToList();

                    string strDates = string.Empty;
                    bool IsDataOnList = false;

                    strHTMLTbl = "<table>";
                    foreach (GetAbsentListForEmailResult ListAbs in _ListOfSelectedEmployee)
                    {
                        GetForDeviceMappingResult _DeviceMappingResult = _GetForDeviceMappingResult.SingleOrDefault(x => x.EmployeeId == ListAbs.EmployeeId);
                        if (_DeviceMappingResult != null && _DeviceMappingResult.IsEnabledSkipLateEntry != null)
                        {
                            if (string.IsNullOrEmpty(strHTMLTbl))
                                strHTMLTbl = "<tr>" + "<td> " + ListAbs.NAME + "    </td>" + "<td></td>" + "</tr>";
                            else
                                strHTMLTbl += "<tr>" + "<td> " + ListAbs.NAME + "    </td>" + "<td></td>" + "</tr>";
                            IsDataOnList = true;
                        }
                    }
                    strHTMLTbl += "</table>";

                    string bodycontents = dbMailContentForSupervisor.Body.Replace("#List#", strHTMLTbl);
                    dbMailContentForSupervisor.Subject = dbMailContentForSupervisor.Subject.Replace("#TodayDate#", todayDate);
                    string[] SupervisorEmails = _SupervisorEmails.SupervisorEmail.Split(',');

                    foreach (string ToEmail in SupervisorEmails)
                    {
                        if (!string.IsNullOrEmpty(ToEmail) && IsDataOnList==true)
                        {
                            bool isSendSuccess = SMTPHelper.SendMailAsyncWebServiceOnly(ToEmail, bodycontents, dbMailContentForSupervisor.Subject, "", SenderEmail);
                            if (isSendSuccess)
                                count++;
                        }
                    }
                }
               
                if (count > 0)
                {
                    logMessage = "\n Email Send successfully for " + count + " Supervisor " + DateTime.Now.ToString() + "  : \n ";
                    Log.SaveAdditionalLog(logMessage, LogType.Schedular);
                   
                }
                //Email send for HR Manager
                if (countEmailToHr > 0)
                {
                    logMessage = "\n Email Send successfully for " + countEmailToHr + " HR Manager " + DateTime.Now.ToString() + "  : \n ";
                    Log.SaveAdditionalLog(logMessage, LogType.Schedular);
                }

                if (countEmailToHr == 0 && count==0)
                {
                    logMessage = "\n error while sending email" + DateTime.Now.ToString() + "  : \n";
                    Log.SaveAdditionalLog(logMessage, LogType.Schedular);
                  
                }
            }
            catch (Exception exp)
            {
                logMessage = "\n Error for : " + GetCurrentDateAndTime().Date.ToString() + "  : \n" + exp.ToString();
                Log.SaveAdditionalLog(logMessage, LogType.Schedular);
                return;
            }
        }

        public static List<AttendanceDashBoardAllEmployeeReportResult> getDailyAttedanceForDashBoard()
        {
            DateTime CurrentDate = GetCurrentDateAndTime();
            return PayrollDataContext.AttendanceDashBoardAllEmployeeReport(CurrentDate, "", null).ToList();
        }


        public static void LateEntryScheduler_DailyNew()
        {
            string logMessage = "";
            try
            {
                DateTime CurrentDate = GetCurrentDateAndTime();
               // List<AttendanceAllEmployeeReportResult> ListDailyAttendance = PayrollDataContext.AttendanceAllEmployeeReport(CurrentDate, "", null).ToList();
                //List<AttendanceAllEmployeeReportResult> ListLateEmployes = ListDailyAttendance.Where(x => (x.InState == 3) && x.AtteState == 11 && x.Type != 3).ToList();
                PayrollDataContext.GenerateAttendanceEmployee(0, CurrentDate.Date, CurrentDate.Date, true);
                List<GetLateListForEmailResult> ListLateEmployes = PayrollDataContext.GetLateListForEmail(CurrentDate.Date, CurrentDate.Date).ToList();
                List<SupervisorEmails> _SupervisorEmails = GetSupervisorEmailListForLateEmpNew(CurrentDate, CurrentDate, ListLateEmployes).ToList();

                if (ListLateEmployes.Count == 0)
                {
                    logMessage = "\n Late In employee not found." + DateTime.Now.ToString() + "  : \n";
                    Log.SaveAdditionalLog(logMessage, LogType.Schedular);
                    return;
                }
                if (ListLateEmployes.Count > 0)
                {
                    sendDailyMailLateEntry(ListLateEmployes);
                   // List<GetLateListForEmailResult> _listLateEmployee = PayrollDataContext.GetLateListForEmail(CurrentDate.Date, CurrentDate.Date).ToList();
                    sendMailLateForSupervisor(ListLateEmployes, _SupervisorEmails);
                }
            }

            catch (Exception exp)
            {
                logMessage = "\n Error for : " + GetCurrentDateAndTime().Date.ToString() + "  : \n" + exp.ToString();
                Log.SaveAdditionalLog(logMessage, LogType.Schedular);
            }
        }

        public static List<AttendanceReportForTimeRequestResult> GetAttendanceReportForTimeRequest(int employeeId, DateTime startDate, DateTime endDate,
            int currentPage, int pageSize)
        {
            List<AttendanceReportForTimeRequestResult> list = PayrollDataContext.AttendanceReportForTimeRequest(employeeId, startDate, endDate, currentPage, pageSize).ToList();

            foreach (var item in list)
            {

                if (item.DeviceIn != null)
                    item.DeviceInString = item.DeviceIn.Value.ToShortTimeString();
                if (item.DeviceOut != null)
                    item.DeviceOutString = item.DeviceOut.Value.ToShortTimeString();
                if (item.RequestIn != null)
                    item.RequestInString = item.RequestIn.Value.ToShortTimeString();
                if (item.RequestOut != null)
                    item.RequestOutString = item.RequestOut.Value.ToShortTimeString();
                if (item.DateEng != null)
                    item.ActualDate = GetAppropriateDate(item.DateEng.Value);
                if (item.DateEng != null)
                    item.EngDateFormatted = item.DateEng.Value.ToString("dd-MMM-yyyy");
            }

            return list;
        }


    }

    public class GenericComboBoxList
    {
        private  string name;
        private  int id;

        public  string Name
        {
            get { return name; }
            set { name = value; }
        }

        public  int ID
        {
            get { return id; }
            set { id = value; }
        }
    }


    public class AttendanceAbsentsBO
    {
        private string employeeId;
        private string date;
        private string dateEng;
        private string createdBy;
        private string createdOn;
        private string note;
        
        public string EmployeeId
        {
            get { return employeeId; }
            set { employeeId = value; }
        }

        public string Date
        {
            get { return date; }
            set { date = value; }
        }

        public string DateEng
        {
            get { return dateEng; }
            set { dateEng = value; }
        }

        public string CreatedBy
        {
            get { return createdBy; }
            set { createdBy = value; }
        }

        public string CreatedOn
        {
            get { return createdOn; }
            set { createdOn = value; }
        }

        public string Note
        {
            get { return note; }
            set { note = value; }
        }


    }

}



