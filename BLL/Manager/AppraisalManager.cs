﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using DAL;
using Utils;
using System.Xml.Linq;
using Utils.Calendar;
using BLL.Entity;
using BLL.BO;
using Utils.Helper;

namespace BLL.Manager
{
    public class AppraisalManager : BaseBiz
    {
        //public static void GetMegaGradeForPercent(decimal percent,out string grade,out string gradeName)
        //{
        //    if (percent <= 25)
        //    {
        //        grade =  "F";
        //        gradeName = "Satisfactory";
        //    }
        //    else if (percent <= 40)
        //    {
        //        grade = "E";
        //        gradeName = "Average";
        //    }
        //    else if (percent <= 55)
        //    {
        //        grade = "D";
        //        gradeName = "Good";
        //    }
        //    else if (percent <= 70)
        //    {
        //        grade = "C";
        //        gradeName = "Very Good";
        //    }
        //    else if (percent <= 85)
        //    {
        //        grade = "B";
        //        gradeName = "Excellent";
        //    }
        //    else
        //    {
        //        grade = "A";
        //        gradeName = "Outstanding";
        //    }

        //}

        //public static void GetMegaGradeForPercent(string grade, out string gradeName)
        //{
        //    if (grade == "F")
        //    {
        //        gradeName = "Satisfactory";
        //    }
        //    else if (grade == "E"){
        //        gradeName = "Average";
        //    }
        //    else if (grade == "D"){
        //        gradeName = "Good";
        //    }
        //    else if (grade == "C"){
        //        gradeName = "Very Good";
        //    }
        //    else if (grade == "B"){
        //        gradeName = "Excellent";
        //    }
        //    else
        //    {
        //         gradeName = "Outstanding";
        //    }

        //}

        //public static List<AppraisalFormPoint> GetPoints(int formId)
        //{
        //    return PayrollDataContext.AppraisalFormPoints.Where(x => x.AppraisalFormRef_ID == formId).ToList();
        //}
        public static List<AppraisalRatingBO> GetTargetRating(int formid)
        {
            return
                (
                from r in PayrollDataContext.AppraisalRatingScales
                join rl in PayrollDataContext.AppraisalRatingScaleLines on r.RatingScaleID equals rl.RatingScaleRefID
                join f in PayrollDataContext.AppraisalForms on r.RatingScaleID equals f.TargetRatingScaleRef_ID
                where f.AppraisalFormID == formid
                orderby rl.Score descending
                select new AppraisalRatingBO
                {
                    Label = rl.Label,
                    Score = rl.Score.Value
                }

                ).ToList();
        }

        public static List<AppraisalRatingBO> GetRating(int ratingId)
        {
            return
                (
                from r in PayrollDataContext.AppraisalRatingScales
                join rl in PayrollDataContext.AppraisalRatingScaleLines on r.RatingScaleID equals rl.RatingScaleRefID
                where r.RatingScaleID == ratingId
                orderby rl.Score descending
                select new AppraisalRatingBO
                {
                    Label = rl.Label,
                    Score = rl.Score.Value
                }

                ).ToList();
        }

        /// <summary>
        /// For mega, overall score doesn't have A,B,C like grade, grade exists for Mega Values only
        /// </summary>
        /// <param name="percent"></param>
        /// <param name="grade"></param>
        //public static void GetMegaCompetenciesGradeForPercent(decimal percent, out string grade)
        //{
        //    if (percent <= 29)
        //    {

        //        grade = "D";
        //    }
        //    else if (percent <= 59)
        //    {
        //        grade = "C";
        //    }
        //    else if (percent <= 89)
        //    {
        //        grade = "B";
        //    }
        //    else
        //    {
        //        grade = "A";
        //    }

        //}

        public static bool HasTargetForForm(int formId)
        {
            if (PayrollDataContext.AppraisalForms.Any(x => x.AppraisalFormID == formId
                && x.HideTarget != null && x.HideTarget.Value))
                return false;

            return true;
        }
        public static bool HasSupervisorReviewForForm(int formId)
        {
            if (PayrollDataContext.AppraisalForms.Any(x => x.AppraisalFormID == formId
                && x.HideSupervisorReview != null && x.HideSupervisorReview.Value))
                return false;

            return true;
        }
        public static bool HasActivitySummaryForForm(int formId)
        {
            if (PayrollDataContext.AppraisalForms.Any(x => x.AppraisalFormID == formId
                && x.HideActivitySummary != null && x.HideActivitySummary.Value))
                return false;

            return true;
        }
        public static Status InsertUpdateRatingScale(AppraisalRatingScale ratingScale, List<AppraisalRatingScaleLine> lines)
        {
            Status status = new Status();
            AppraisalRatingScale dbEntity = new AppraisalRatingScale();

            if (PayrollDataContext.AppraisalRatingScales.Any(x => x.RatingScaleID == ratingScale.RatingScaleID))
            {

                //if(PayrollDataContext.AppraisalForms.Any(
                //    x=>x.ActivityRatingScaleRef_ID ==ratingScale.RatingScaleID ||
                //        x.ReviewRatingScaleRef_ID == ratingScale.RatingScaleID ||
                //    x.CompentencyRatingScaleRef_ID == ratingScale.RatingScaleID ||
                //    x.QuestionnareRatingScaleRef_ID == ratingScale.RatingScaleID
                //    ))
                //{
                //        status.IsSuccess = false;
                //        status.ErrorMessage = "Rating Scale in use, can not be edited.";
                //        return status;
                //}

                dbEntity = PayrollDataContext.AppraisalRatingScales.Where(x => x.RatingScaleID == ratingScale.RatingScaleID).First();
                List<AppraisalRatingScaleLine> items = PayrollDataContext.AppraisalRatingScaleLines.Where(x => x.RatingScaleRefID == ratingScale.RatingScaleID).ToList();

                if (items != null)
                    PayrollDataContext.AppraisalRatingScaleLines.DeleteAllOnSubmit(items);

                foreach (var val1 in lines)
                {
                    val1.RatingScaleRefID = ratingScale.RatingScaleID;
                }

                PayrollDataContext.AppraisalRatingScaleLines.InsertAllOnSubmit(lines);
                dbEntity.Name = ratingScale.Name;
                dbEntity.Description = ratingScale.Description;
            }
            else
            {

                ratingScale.AppraisalRatingScaleLines.AddRange(lines);

                PayrollDataContext.AppraisalRatingScales.InsertOnSubmit(ratingScale);

            }

            PayrollDataContext.SubmitChanges();
            status.IsSuccess = true;
            return status;

        }
        public static List<AppraisalRatingScale> GetAllRatingScale()
        {
            List<AppraisalRatingScale> list = PayrollDataContext.AppraisalRatingScales.OrderBy(x => x.Name).ToList();

            return list;
        }
        public static AppraisalRatingScale GetRatingScaleById(int? levelId)
        {
            if (levelId == null)
                return null;
            return PayrollDataContext.AppraisalRatingScales.Where(x => x.RatingScaleID == levelId).SingleOrDefault();
        }
        public static Status DeleteRatingScale(int ratingScale)
        {
            Status status = new Status();
            if (PayrollDataContext.AppraisalForms.Any(
                   x => x.ActivityRatingScaleRef_ID == ratingScale ||
                       x.ReviewRatingScaleRef_ID == ratingScale ||
                   x.CompentencyRatingScaleRef_ID == ratingScale ||
                   x.QuestionnareRatingScaleRef_ID == ratingScale
                   ))
            {
                status.IsSuccess = false;
                status.ErrorMessage = "Rating Scale in use, can not be deleted.";
                return status;
            }

            AppraisalRatingScale dbEntity = PayrollDataContext.AppraisalRatingScales.SingleOrDefault(x => x.RatingScaleID == ratingScale);
            foreach (var val1 in dbEntity.AppraisalRatingScaleLines)
            {
                PayrollDataContext.AppraisalRatingScaleLines.DeleteOnSubmit(val1);
            }

            PayrollDataContext.AppraisalRatingScales.DeleteOnSubmit(dbEntity);
            PayrollDataContext.SubmitChanges();
            return status;
        }

        public static List<AppraisalCompetency> GetAllCompetencies()
        {
            //return PayrollDataContext.AppraisalCompetencies.ToList();

            List<AppraisalCompetency> list = PayrollDataContext.AppraisalCompetencies.OrderBy(x => x.CompetencyID).ToList();

            foreach (AppraisalCompetency competency in list)
            {
                competency.CategoryName = competency.AppraisalCategory.Name;
            }

            return list;
        }
        public static List<AppraisalTarget> GetAllTargets()
        {
            //return PayrollDataContext.AppraisalCompetencies.ToList();

            List<AppraisalTarget> list = PayrollDataContext.AppraisalTargets.OrderBy(x => x.TargetID).ToList();



            return list;
        }

        public static List<AppraisalRolloutGroup> GetAllRolloutGroup()
        {
            List<AppraisalRolloutGroup> list = PayrollDataContext.AppraisalRolloutGroups.OrderBy(x => x.RolloutGroupID).ToList();
            return list;
        }

        public static List<GetEmployeeAppraisalRolloutGroupListResult> GetEmployeeAppraisalRolloutGroup(int currentPage, int pageSize, int employeeId, string employeeName, int groupId)
        {
            return PayrollDataContext.GetEmployeeAppraisalRolloutGroupList(currentPage, pageSize, employeeId, employeeName, groupId).ToList();
        }

        public static List<GetEmployeeSeniorityListResult> GetEmployeeSeniorityList(int currentPage, int pageSize, int employeeId, string employeeName)
        {
            return PayrollDataContext.GetEmployeeSeniorityList(currentPage, pageSize, employeeId, employeeName).ToList();
        }

        public static List<AppraisalRecommendFor> GetAllRecommendFor()
        {
            //return PayrollDataContext.AppraisalCompetencies.ToList();

            List<AppraisalRecommendFor> list = PayrollDataContext.AppraisalRecommendFors.OrderBy(x => x.RecommendForId).ToList();



            return list;
        }
        public static List<AppraisalPeriod> GetAllPeriods()
        {
            //return PayrollDataContext.AppraisalCompetencies.ToList();

            List<AppraisalPeriod> list = PayrollDataContext.AppraisalPeriods.OrderByDescending(x => x.StartDate).ToList();



            return list;
        }
        public static List<AppraisalRolloutGroup> GetAllRolloutGroups()
        {
            //return PayrollDataContext.AppraisalCompetencies.ToList();

            List<AppraisalRolloutGroup> list = PayrollDataContext.AppraisalRolloutGroups.OrderByDescending(x => x.Name).ToList();



            return list;
        }
        public static List<AppraisalCategory> GetCategories()
        {
            return PayrollDataContext.AppraisalCategories.OrderBy(x => x.CategoryID).ToList();
        }

        public static List<string> GetAllCompetencyNames()
        {
            return PayrollDataContext.AppraisalCompetencies.Select(x => x.Name).ToList();
        }
        public static List<string> GetAllTargetNames()
        {
            return PayrollDataContext.AppraisalTargets.Select(x => x.Name).ToList();
        }

        public static List<string> GetAllRolloutGroupNames()
        {
            return PayrollDataContext.AppraisalRolloutGroups.Select(x => x.Name).ToList();
        }
        public static List<string> GetAllRecommendNames()
        {
            return PayrollDataContext.AppraisalRecommendFors.Select(x => x.Name).ToList();
        }
        public static List<EDesignation> GetPositions()
        {
            return PayrollDataContext.EDesignations.OrderBy(x => x.DesignationId).ToList();
        }


        public static ResponseStatus SaveVariableAppraisalTarget(List<AppraisalEmployeeTarget> projectPays,
          bool isDeduction, ref int count, int month, int year)
        {
            count = 0;
            ResponseStatus status = new ResponseStatus();

            CustomDate start = new CustomDate(1, month, year, IsEnglish);
            CustomDate end = new CustomDate(DateHelper.GetTotalDaysInTheMonth(year, month, IsEnglish), month, year, IsEnglish);

            DateTime startDate = start.EnglishDate;
            DateTime endDate = end.EnglishDate;

            List<AppraisalEmployeeTarget> _dbAppraisalEmployeeTargetsList = PayrollDataContext.AppraisalEmployeeTargets
                .Where(x => x.Month == month && x.Year == year).ToList();
            // .SingleOrDefault(x => x.TargetID == item.TargetID && x.EmployeeId == item.EmployeeId);

            foreach (AppraisalEmployeeTarget item in projectPays)
            {

                AppraisalEmployeeTarget _AppraisalEmployeeTarget = new AppraisalEmployeeTarget();
                _AppraisalEmployeeTarget.EmployeeId = item.EmployeeId;
                _AppraisalEmployeeTarget.TargetID = item.TargetID;
                _AppraisalEmployeeTarget.Value = item.Value;
                _AppraisalEmployeeTarget.Month = month;
                _AppraisalEmployeeTarget.Year = year;
                _AppraisalEmployeeTarget.StartDate = startDate;
                _AppraisalEmployeeTarget.EndDate = endDate;

                //if (EmployeeManager.IsEmployeeIDExists(item.EmployeeId) == false)
                //{
                //    status.ErrorMessage = string.Format("Employee ID \"{0}\" does not exists.", item.EmployeeId);
                //    return status;
                //}

                AppraisalEmployeeTarget _dbAppraisalEmployeeTargets = _dbAppraisalEmployeeTargetsList
                    .SingleOrDefault(x => x.TargetID == item.TargetID && x.EmployeeId == item.EmployeeId);
                if (_dbAppraisalEmployeeTargets != null) //update
                {
                    _dbAppraisalEmployeeTargets.Value = item.Value;
                }
                else
                    PayrollDataContext.AppraisalEmployeeTargets.InsertOnSubmit(_AppraisalEmployeeTarget);

            }

            UpdateChangeSet();

            return status;

        }



        public static List<AppraisalEmployeeTarget> GetAppraisalEmployeeTargets(int month, int year)
        {
            return PayrollDataContext.AppraisalEmployeeTargets.Where(x => x.Month == month && x.Year == year).ToList();

        }

        public static List<GetAppraisalEducationLocationScoreResult> GetAppraisalEducationLocationScore(int start, int limit, int EmployeeID, int PeriodId)
        {
            List<GetAppraisalEducationLocationScoreResult> list = PayrollDataContext.GetAppraisalEducationLocationScore(start, limit, EmployeeID, PeriodId).ToList();
            return list;
        }


        public static AppraisalEmployeeEducationLocationScore GetAppraisalEducationLocationScore(int EmployeeID, int PeriodId)
        {
            return PayrollDataContext.AppraisalEmployeeEducationLocationScores.SingleOrDefault(x => x.EmployeeId == EmployeeID && x.AppraisalPeriodRef_ID == PeriodId);

        }

        public static List<AppraisalEmployeeEducationLocationScore> GetAppraisalEmployeeEducationLocation(int AppraisalPeriodID)
        {
            return PayrollDataContext.AppraisalEmployeeEducationLocationScores.Where(x => x.AppraisalPeriodRef_ID == AppraisalPeriodID).ToList();
        }

        public static List<TargetOtherDetail> GetTargetOtherDetails(int empId, int formId, int targetId)
        {
            return
                (
                from t in PayrollDataContext.AppraisalEmployeeTargetOtherDetails
                join e in PayrollDataContext.EEmployees on t.EmployeeId equals e.EmployeeId
                join f in PayrollDataContext.AppraisalForms on t.FormRef_ID equals f.AppraisalFormID
                join tt in PayrollDataContext.AppraisalTargets on t.TargetRef_ID equals tt.TargetID
                orderby e.Name
                select new TargetOtherDetail
                {
                    EmployeeId = e.EmployeeId,
                    TargetId = t.TargetRef_ID,
                    FormId = t.FormRef_ID,

                    Name = e.Name,
                    TargetName = tt.Name,
                    FormName = f.Name,

                    Weight = t.Weight,
                    TargetTextValue = t.AssignedTargetTextValue,
                    AchievementTextValue = t.AchievementTextValue,
                    AssignedTargetTextValue = t.AssignedTargetTextValue

                }

                ).ToList();
        }

        public static List<AppraisalTarget> GetAllAppraisalTargets()
        {
            return PayrollDataContext.AppraisalTargets.ToList();
        }

        public static Status InsertUpdateCompetency(AppraisalCompetency entity, List<AppraisalCompetencyTeaser> TeaserLines, List<AppraisalCompetencyPosition> PositionLines, bool isInsert)
        {
            Status status = new Status();

            if (isInsert)
            {
                // setting the order
                int i = 1;
                foreach (var line in TeaserLines)
                {
                    line.OrderSequence = i;
                    i++;
                }

                i = 1;
                foreach (var line in PositionLines)
                {
                    line.OrderSequence = i;
                    i++;
                }


                entity.AppraisalCompetencyTeasers.AddRange(TeaserLines);
                entity.AppraisalCompetencyPositions.AddRange(PositionLines);

                PayrollDataContext.AppraisalCompetencies.InsertOnSubmit(entity);
            }
            else
            {
                AppraisalCompetency dbEntity = PayrollDataContext.AppraisalCompetencies.SingleOrDefault(x => x.CompetencyID == entity.CompetencyID);

                dbEntity.Name = entity.Name;
                dbEntity.Description = entity.Description;
                dbEntity.CategoryID = entity.CategoryID;
                dbEntity.IsCore = entity.IsCore;


                // First of all delete all the positions and teasers with selected competencyID

                // Deleting teasers
                List<AppraisalCompetencyTeaser> teasersToDelete = PayrollDataContext.AppraisalCompetencyTeasers.Where(x => x.CompetencyID == entity.CompetencyID).ToList();
                foreach (AppraisalCompetencyTeaser teaerToDelete in teasersToDelete)
                {
                    PayrollDataContext.AppraisalCompetencyTeasers.DeleteOnSubmit(teaerToDelete);
                }

                // Deleting positions
                List<AppraisalCompetencyPosition> positionsToDelete = PayrollDataContext.AppraisalCompetencyPositions.Where(x => x.CompetencyID == entity.CompetencyID).ToList();
                foreach (AppraisalCompetencyPosition positionToDelete in positionsToDelete)
                {
                    PayrollDataContext.AppraisalCompetencyPositions.DeleteOnSubmit(positionToDelete);
                }

                PayrollDataContext.SubmitChanges();

                //Now Adding the data
                // setting the order
                int i = 1;
                foreach (var line in TeaserLines)
                {
                    line.OrderSequence = i;
                    i++;
                }

                i = 1;
                foreach (var line in PositionLines)
                {
                    line.OrderSequence = i;
                    i++;
                }


                dbEntity.AppraisalCompetencyTeasers.AddRange(TeaserLines);
                dbEntity.AppraisalCompetencyPositions.AddRange(PositionLines);
            }

            PayrollDataContext.SubmitChanges();

            return status;
        }

        public static Status InsertUpdateTarget(AppraisalTarget entity, bool isInsert)
        {
            Status status = new Status();

            if (isInsert)
            {

                PayrollDataContext.AppraisalTargets.InsertOnSubmit(entity);
            }
            else
            {
                AppraisalTarget dbEntity = PayrollDataContext.AppraisalTargets.SingleOrDefault(x => x.TargetID == entity.TargetID);

                dbEntity.Name = entity.Name;
            }

            PayrollDataContext.SubmitChanges();

            return status;
        }

        public static Status InsertUpdateRolloutGroup(AppraisalRolloutGroup entity, bool isInsert)
        {
            Status status = new Status();
            if (isInsert)
            {
                PayrollDataContext.AppraisalRolloutGroups.InsertOnSubmit(entity);
            }
            else
            {
                AppraisalRolloutGroup dbEntity = PayrollDataContext.AppraisalRolloutGroups.SingleOrDefault(x => x.RolloutGroupID == entity.RolloutGroupID);
                dbEntity.Name = entity.Name;
            }
            PayrollDataContext.SubmitChanges();
            return status;
        }


        public static Status InsertUpdateRecommend(AppraisalRecommendFor entity, bool isInsert)
        {
            Status status = new Status();

            if (isInsert)
            {

                PayrollDataContext.AppraisalRecommendFors.InsertOnSubmit(entity);
            }
            else
            {
                AppraisalRecommendFor dbEntity = PayrollDataContext.AppraisalRecommendFors.SingleOrDefault(x => x.RecommendForId == entity.RecommendForId);

                dbEntity.Name = entity.Name;
            }

            PayrollDataContext.SubmitChanges();

            return status;
        }

        public static Status InsertUpdateEducationLocationScore(List<AppraisalEmployeeEducationLocationScore> ListEntity)
        {
            Status status = new Status();

            foreach (AppraisalEmployeeEducationLocationScore _item in ListEntity)
            {
                AppraisalEmployeeEducationLocationScore dbEntity = PayrollDataContext.AppraisalEmployeeEducationLocationScores.SingleOrDefault(x => x.AppraisalPeriodRef_ID == _item.AppraisalPeriodRef_ID && x.EmployeeId == _item.EmployeeId);
                if (dbEntity != null)
                {
                    dbEntity.EducationScore = _item.EducationScore;
                    dbEntity.SeniorityScore = _item.SeniorityScore;
                    dbEntity.LocationScore = _item.LocationScore;
                }
                else
                    PayrollDataContext.AppraisalEmployeeEducationLocationScores.InsertOnSubmit(_item);
            }
            PayrollDataContext.SubmitChanges();
            return status;
        }
        public static Status InsertUpdatePeroid(AppraisalPeriod entity, bool isInsert)
        {
            Status status = new Status();

            if (isInsert)
            {

                PayrollDataContext.AppraisalPeriods.InsertOnSubmit(entity);
            }
            else
            {
                AppraisalPeriod dbEntity = PayrollDataContext.AppraisalPeriods.SingleOrDefault(x => x.PeriodId == entity.PeriodId);

                dbEntity.Name = entity.Name;
                dbEntity.StartDate = entity.StartDate;
                dbEntity.EndDate = entity.EndDate;
                dbEntity.DueDate = entity.DueDate;
                dbEntity.Closed = entity.Closed;
            }

            PayrollDataContext.SubmitChanges();

            return status;
        }
        public static Status DeleteCompetency(int competencyID)
        {
            Status status = new Status();

            if (PayrollDataContext.AppraisalEmployeeFormCompetencies.Any(x => x.CompetencyRef_ID == competencyID))
            {
                status.IsSuccess = false;
                status.ErrorMessage = "Competancy in use.";
                return status;
            }

            AppraisalCompetency competenyEntity = PayrollDataContext.AppraisalCompetencies.SingleOrDefault(x => x.CompetencyID == competencyID);
            var teasersToDelete = PayrollDataContext.AppraisalCompetencyTeasers.Where(x => x.CompetencyID == competencyID);
            var positionsToDelete = PayrollDataContext.AppraisalCompetencyPositions.Where(x => x.CompetencyID == competencyID);

            PayrollDataContext.AppraisalCompetencies.DeleteOnSubmit(competenyEntity);
            PayrollDataContext.AppraisalCompetencyTeasers.DeleteAllOnSubmit(teasersToDelete);
            PayrollDataContext.AppraisalCompetencyPositions.DeleteAllOnSubmit(positionsToDelete);

            PayrollDataContext.SubmitChanges();

            return status;
        }
        public static Status DeleteTarget(int TargetID)
        {
            Status status = new Status();
            if (PayrollDataContext.AppraisalFormTargets.Any(x => x.TargetRef_ID == TargetID))
            {
                status.IsSuccess = false;
                status.ErrorMessage = "Target in use.";
                return status;
            }

            AppraisalTarget competenyEntity = PayrollDataContext.AppraisalTargets.SingleOrDefault(x => x.TargetID == TargetID);


            PayrollDataContext.AppraisalTargets.DeleteOnSubmit(competenyEntity);

            PayrollDataContext.SubmitChanges();

            return status;
        }

        public static Status DeleteRolloutGroup(int GroupID)
        {
            Status status = new Status();
            if (PayrollDataContext.AppraisalRolloutGroupEmployees.Any(x => x.RolloutGroupRef_ID == GroupID))
            {
                status.IsSuccess = false;
                status.ErrorMessage = "Rollout Group in use.";
                return status;
            }
            AppraisalRolloutGroup Entity = PayrollDataContext.AppraisalRolloutGroups.SingleOrDefault(x => x.RolloutGroupID == GroupID);
            PayrollDataContext.AppraisalRolloutGroups.DeleteOnSubmit(Entity);
            PayrollDataContext.SubmitChanges();
            return status;
        }

        public static Status DeleteRecommendFor(int TargetID)
        {
            Status status = new Status();

            if (PayrollDataContext.AppraisalEmployeeForms.Any(x => x.RecommendForId == TargetID))
            {
                status.IsSuccess = false;
                status.ErrorMessage = "Recommend in use.";
                return status;
            }

            AppraisalRecommendFor competenyEntity = PayrollDataContext.AppraisalRecommendFors.SingleOrDefault(x => x.RecommendForId == TargetID);


            PayrollDataContext.AppraisalRecommendFors.DeleteOnSubmit(competenyEntity);

            PayrollDataContext.SubmitChanges();

            return status;
        }
        public static AppraisalCompetency GetCompetencyByID(int competencyID)
        {
            return PayrollDataContext.AppraisalCompetencies.SingleOrDefault(x => x.CompetencyID == competencyID);
        }
        public static AppraisalTarget GetTargetByID(int TargetID)
        {
            return PayrollDataContext.AppraisalTargets.SingleOrDefault(x => x.TargetID == TargetID);
        }

        public static AppraisalRolloutGroup GetGroupByID(int GroupID)
        {
            return PayrollDataContext.AppraisalRolloutGroups.SingleOrDefault(x => x.RolloutGroupID == GroupID);
        }

        public static AppraisalRecommendFor GetRecommendForByID(int TargetID)
        {
            return PayrollDataContext.AppraisalRecommendFors.SingleOrDefault(x => x.RecommendForId == TargetID);
        }
        public static AppraisalPeriod GetPeriodByID(int periodId)
        {
            return PayrollDataContext.AppraisalPeriods.SingleOrDefault(x => x.PeriodId == periodId);
        }
        public static List<int> GetTeaserIDsByCompetencyID(int competencyID)
        {
            return PayrollDataContext.AppraisalCompetencyTeasers.Where(x => x.CompetencyID == competencyID).Select(x => x.TeaserID).ToList();
        }

        public static string GetTeaserNameByID(int teaserID)
        {
            AppraisalCompetencyTeaser entity = PayrollDataContext.AppraisalCompetencyTeasers.Where(x => x.TeaserID == teaserID).FirstOrDefault();

            return entity.Name;
        }

        public static List<int> GetDesignationIDsByCompetencyID(int competencyID)
        {
            return PayrollDataContext.AppraisalCompetencyPositions.Where(x => x.CompetencyID == competencyID).Select(x => x.DesignationID).ToList();
        }

        public static Status UpdateTeaserName(int teaserID, string teaserName)
        {
            Status status = new Status();

            AppraisalCompetencyTeaser entity = PayrollDataContext.AppraisalCompetencyTeasers.SingleOrDefault(x => x.TeaserID == teaserID);
            entity.Name = teaserName;

            PayrollDataContext.SubmitChanges();

            return status;
        }

        public static int GetCompetencyPositionID(int competencyID, int designationID)
        {
            AppraisalCompetencyPosition entity = PayrollDataContext.AppraisalCompetencyPositions.FirstOrDefault(x => x.CompetencyID == competencyID && x.DesignationID == designationID);
            return entity.CompetencyPositionID;
        }

        public static List<AppraisalGoalsCategory> getallGoalsCategory()
        {
            return PayrollDataContext.AppraisalGoalsCategories.OrderBy(x => x.Name).ToList();
        }

        public static List<AppraisalCategory> getallAppraisalCategory()
        {
            return PayrollDataContext.AppraisalCategories.OrderBy(x => x.Name).ToList();
        }

        public static Status InsertUpdateGoal(AppraisalGoal goalInstance, bool isInsert)
        {
            Status status = new Status();
            AppraisalGoal goalInstance1 = new AppraisalGoal();
            if (isInsert)
            {
                PayrollDataContext.AppraisalGoals.InsertOnSubmit(goalInstance);
            }
            else
            {
                goalInstance1 = PayrollDataContext.AppraisalGoals.Where(x => x.GoalID == goalInstance.GoalID).First();
                goalInstance1.VisibilityText = goalInstance.VisibilityText;
                goalInstance1.CategoryText = goalInstance.CategoryText;
                goalInstance1.CategoryID = goalInstance.CategoryID;
                goalInstance1.GoalName = goalInstance.GoalName;
                goalInstance1.Metric = goalInstance.Metric;
                goalInstance1.Weight = goalInstance.Weight;
                goalInstance1.StartDate = goalInstance.StartDate;
                goalInstance1.DueDate = goalInstance.DueDate;
                goalInstance1.Complete = goalInstance.Complete;
                goalInstance1.Status = goalInstance.Status;
            }

            PayrollDataContext.SubmitChanges();
            status.IsSuccess = true;
            return status;
        }

        public static AppraisalGoalsCategory GetCategoryGoalById(int levelId)
        {
            return PayrollDataContext.AppraisalGoalsCategories.Where(x => x.CategoryID == levelId).SingleOrDefault();
        }

        public static AppraisalCategory GetAppraisalCategoryById(int levelId)
        {
            return PayrollDataContext.AppraisalCategories.Where(x => x.CategoryID == levelId).SingleOrDefault();
        }


        public static Status DeleteGoalCategory(int goalCategory)
        {
            Status status = new Status();
            AppraisalGoalsCategory dbEntity = PayrollDataContext.AppraisalGoalsCategories.SingleOrDefault(x => x.CategoryID == goalCategory);

            PayrollDataContext.AppraisalGoalsCategories.DeleteOnSubmit(dbEntity);
            PayrollDataContext.SubmitChanges();
            status.IsSuccess = true;
            return status;
        }

        public static Status DeleteAppraisalCategory(int goalCategory)
        {
            Status status = new Status();
            AppraisalCategory dbEntity = PayrollDataContext.AppraisalCategories.SingleOrDefault(x => x.CategoryID == goalCategory);

            PayrollDataContext.AppraisalCategories.DeleteOnSubmit(dbEntity);
            PayrollDataContext.SubmitChanges();
            status.IsSuccess = true;
            return status;
        }


        public static Status DeleteCategory(int categoryId)
        {
            Status status = new Status();
            if (PayrollDataContext.AppraisalFormQuestionnaires.Any(x => x.CategoryID == categoryId))
            {
                status.ErrorMessage = "Please delete the question first to delete the category.";
                return status;
            }

            AppraisalFormQuestionnaireCategory dbCat =
                PayrollDataContext.AppraisalFormQuestionnaireCategories.FirstOrDefault(x => x.CategoryID == categoryId);

            PayrollDataContext.AppraisalFormQuestionnaireCategories.DeleteOnSubmit(dbCat);

            PayrollDataContext.SubmitChanges();

            return status;
        }

        public static bool DeleteQuestionByID(int QuestionID, int FormID, out Boolean isInUse)
        {

            isInUse = false;
            if (NewHRManager.IsAppraisalQuestionnariesUsedInEmployee(FormID))
                return false;

            Status status = new Status();
            AppraisalFormQuestionnaire dbEntity = PayrollDataContext.AppraisalFormQuestionnaires.SingleOrDefault(x => x.QuestionnareID == QuestionID);

            PayrollDataContext.AppraisalFormQuestionnaires.DeleteOnSubmit(dbEntity);
            return DeleteChangeSet();


        }

        public static int GetFormIDByQuestionnaireCategoryID(int categoryID)
        {
            AppraisalFormQuestionnaireCategory cat = PayrollDataContext.AppraisalFormQuestionnaireCategories.FirstOrDefault(x => x.CategoryID == categoryID);
            if (cat != null)
                return cat.AppraisalFormRef_ID;

            return 0;
        }

        public static Status DeleteRolloutByID(int RolloutID)
        {
            Status status = new Status();

            List<AppraisalEmployeeForm> empForm = PayrollDataContext.AppraisalEmployeeForms
                .Where(x => x.RolloutID == RolloutID).ToList();

            foreach (AppraisalEmployeeForm item in empForm)
            {
                //if (empForm != null)
                {
                    if (item.Status != (int)AppraisalStatus.Saved)
                    {
                        status.IsSuccess = false;
                        status.ErrorMessage = "Forwarded rollout form can not be deleted.";
                        return status;
                    }
                }
            }

            PayrollDataContext.AppraisalEmployeeForms.DeleteAllOnSubmit(empForm);

            AppraisalRollout dbEntity = PayrollDataContext.AppraisalRollouts.SingleOrDefault(x => x.RolloutID == RolloutID);

            PayrollDataContext.AppraisalRollouts.DeleteOnSubmit(dbEntity);
            PayrollDataContext.SubmitChanges();
            status.IsSuccess = true;
            return status;
        }

        public static bool DeleteReviewQuestionByID(int QuestionID, int FormID, out bool isInUse)
        {

            isInUse = false;
            if (NewHRManager.IsAppraisalReviewUsedInEmployee(FormID))
                return false;

            AppraisalFormReview dbEntity = PayrollDataContext.AppraisalFormReviews.SingleOrDefault(x => x.QuestionnareID == QuestionID);
            PayrollDataContext.AppraisalFormReviews.DeleteOnSubmit(dbEntity);
            return DeleteChangeSet();
        }

        public static Status UpdatePosition(int comPosID, int newDesignationID)
        {
            Status status = new Status();

            AppraisalCompetencyPosition entity = PayrollDataContext.AppraisalCompetencyPositions.SingleOrDefault(x => x.CompetencyPositionID == comPosID);

            entity.DesignationID = newDesignationID;

            PayrollDataContext.SubmitChanges();

            return status;
        }

        public static List<AppraisalFormQuestionnaire> GetAllQuestionByFormID(int FormID)
        {
            List<AppraisalFormQuestionnaire> iList = new List<AppraisalFormQuestionnaire>();
            iList = PayrollDataContext.AppraisalFormQuestionnaires.Where(x => x.AppraisalFormRef_ID == FormID)
                .OrderBy(x => x.AppraisalFormQuestionnaireCategory.Sequence).ThenBy(x => x.Sequence).ToList();

            List<AppraisalFormQuestionnaire> newList = new List<AppraisalFormQuestionnaire>();
            List<AppraisalFormQuestionnaireCategory> catList =
                PayrollDataContext.AppraisalFormQuestionnaireCategories
                .Where(x => x.AppraisalFormRef_ID == FormID).OrderBy(x => x.Sequence).ToList();


            foreach (AppraisalFormQuestionnaireCategory cat in catList)
            {
                AppraisalFormQuestionnaire catQ = new AppraisalFormQuestionnaire();
                catQ.CategoryID = cat.CategoryID;
                catQ.CategoryName = cat.Name;
                catQ.IsCategory = true;
                catQ.QuestionnareID = cat.CategoryID;
                catQ.Sequence = cat.Sequence;
                catQ.Weight = cat.Weight;

                newList.Add(catQ);

                newList.AddRange(
                    iList.Where(x => x.CategoryID == cat.CategoryID).OrderBy(x => x.Sequence)
                    );
            }


            int? categoryId = 0;
            if (iList.Count > 0)
                categoryId = newList[0].CategoryID;

            int catCount = 1;
            int qCount = 1;


            foreach (AppraisalFormQuestionnaire item in newList)
            {
                //if (item.AppraisalFormQuestionnaireCategory != null)
                //    item.CategoryName = item.AppraisalFormQuestionnaireCategory.Name;

                if (item.IsCategory)
                {


                    if (categoryId == item.CategoryID)
                    {

                    }
                    else
                    {
                        catCount += 1;
                        qCount = 1;
                    }


                    item.SN = catCount.ToString();
                }
                else
                {
                    //if (item.CategoryID == categoryId)
                    //{
                    //    item.SN = catCount + "." + qCount;

                    //}
                    //else
                    {
                        //categoryId = item.CategoryID;
                        //qCount = 1;
                        //catCount += 1;

                        item.SN = catCount + "." + qCount;
                    }

                    qCount += 1;

                    item.Sequence = 0;
                }
            }
            // add SN for question





            return newList;
        }

        public static void UpdateImportedQuestion(int catId, List<string> list)
        {
            PayrollDataContext.AppraisalFormQuestionnaires.DeleteAllOnSubmit
                (PayrollDataContext.AppraisalFormQuestionnaires.Where(x => x.CategoryID == catId).ToList());

            AppraisalFormQuestionnaireCategory cat = PayrollDataContext.AppraisalFormQuestionnaireCategories
                .FirstOrDefault(x => x.CategoryID == catId);


            int seq = 1;
            foreach (string item in list)
            {
                PayrollDataContext.AppraisalFormQuestionnaires.InsertOnSubmit(
                    new AppraisalFormQuestionnaire
                    {
                        CategoryID = catId,
                        AppraisalFormRef_ID = cat.AppraisalFormRef_ID,
                        Question = item,
                        Weight = 1,
                        Sequence = seq++
                    });
            }

            PayrollDataContext.SubmitChanges();

        }

        public static List<AppraisalFormReview> GetAllReviewQuestionByFormID(int FormID)
        {
            return PayrollDataContext.AppraisalFormReviews.Where(x => x.AppraisalFormRef_ID == FormID).ToList();
        }

        public static List<AppraisalFormQuestionnaire> GetCategoryQuestionList(int categoryId)
        {
            return
                PayrollDataContext.AppraisalFormQuestionnaires
                .Where(x => x.CategoryID == categoryId).OrderBy(x => x.Sequence).ToList();
        }

        public static Status InsertUpdateFormQuetion(AppraisalFormQuestionnaire inst, bool isEdit)
        {
            Status status = new Status();

            AppraisalFormQuestionnaire dbEntity = new AppraisalFormQuestionnaire();
            int sequenceID = 0;
            if (isEdit)
            {
                dbEntity = PayrollDataContext.AppraisalFormQuestionnaires.Where(x => x.QuestionnareID == inst.QuestionnareID).FirstOrDefault();
                if (dbEntity != null)
                {
                    sequenceID = dbEntity.Sequence;
                    PayrollDataContext.AppraisalFormQuestionnaires.DeleteOnSubmit(dbEntity);
                }
                inst.Sequence = sequenceID;
                PayrollDataContext.AppraisalFormQuestionnaires.InsertOnSubmit(inst);

            }
            else
            {
                PayrollDataContext.AppraisalFormQuestionnaires.InsertOnSubmit(inst);
            }

            PayrollDataContext.SubmitChanges();
            status.IsSuccess = true;
            return status;
        }


        public static Status SendAppraisalMailAndMessage(EmailContentType mailType, int toEmployeeId
            , AppraisalRollout rollout, AppraisalEmployeeForm empForm)
        {
            Status status = new Status();

            EmailContent content = CommonManager.GetMailContent((int)mailType);
            if (content == null)
            {
                status.ErrorMessage = "Email content not defined.";
                return status;
            }

            EEmployee toEmployee = new EmployeeManager().GetById(toEmployeeId);
            UUser user = UserManager.GetUserByEmployee(toEmployeeId);
            EEmployee forEmployee = null;


            string subject = content.Subject;
            string message = content.Body;

            string toUser = user == null ? "" : user.UserName;
            string toEmail = toEmployee.EAddresses[0].CIEmail;

            // Generate the Approval Url the leave request
            string url = UrlHelper.GetRootUrl();
            if (url[url.Length - 1] != '/')
                url += "/";



            switch (mailType)
            {
                case EmailContentType.AppraisalEmployeeRollout:

                    if (empForm != null)
                        url += string.Format("Employee/Appraisal/EmployeeIntroduction.aspx?fid={0}&efid={1}",
                            empForm.AppraisalFormRef_ID, empForm.AppraisalEmployeeFormID);
                    else
                        url = "javascript:void(0)";


                    subject = subject.Replace("{From Date}", rollout.FromDate.Value.ToShortDateString())
                               .Replace("{To Date}", rollout.ToDate.Value.ToShortDateString());

                    message = message.Replace("{Employee Name}", toEmployee.Name)
                                    .Replace("{Organizations Name}", SessionManager.CurrentCompany.Name)
                                    .Replace("{From Date}", rollout.FromDate.Value.ToShortDateString())
                                    .Replace("{To Date}", rollout.ToDate.Value.ToShortDateString())
                                    .Replace("{Due Date}", rollout.SubmissionDate.Value.ToShortDateString())
                                    .Replace("#Url#", url);

                    break;
                case EmailContentType.AppraisalSupervisor:

                    if (empForm != null)
                        url += string.Format("Employee/Appraisal/EmployeeIntroduction.aspx?fid={0}&efid={1}",
                           empForm.AppraisalFormRef_ID, empForm.AppraisalEmployeeFormID);
                    else
                        url = "javascript:void(0)";

                    forEmployee = new EmployeeManager().GetById(empForm.EmployeeId);


                    subject = subject.Replace("{Title Employee Name}", forEmployee.Title + " " + forEmployee.Name)
                                    .Replace("{his/her}", forEmployee.Gender == 1 ? "his" : "her");

                    message = message.Replace("{Supervisor Name}", toEmployee.Name)
                                    .Replace("{Title Employee Name}", forEmployee.Title + " " + forEmployee.Name)
                                    .Replace("{his/her}", forEmployee.Gender == 1 ? "his" : "her")
                                    .Replace("{Organizations Name}", SessionManager.CurrentCompany.Name)
                                     .Replace("#Url#", url);

                    break;
                case EmailContentType.AppraisalEmployeeReview:

                    if (empForm != null)
                        url += string.Format("Employee/Appraisal/EmployeeIntroduction.aspx?fid={0}&efid={1}",
                          empForm.AppraisalFormRef_ID, empForm.AppraisalEmployeeFormID);
                    else
                        url = "javascript:void(0)";

                    forEmployee = new EmployeeManager().GetById(empForm.EmployeeId);


                    //subject = subject;

                    message = message.Replace("{Employee Name}", forEmployee.Name)
                                    .Replace("{Organizations Name}", SessionManager.CurrentCompany.Name)
                                     .Replace("#Url#", url);

                    break;
                case EmailContentType.AppraisalHigherLevel:

                    if (empForm != null)
                        url += string.Format("Employee/Appraisal/EmployeeAppraisalReviewPage.aspx?fid={0}&efid={1}",
                          empForm.AppraisalFormRef_ID, empForm.AppraisalEmployeeFormID);
                    else
                        url = "javascript:void(0)";

                    forEmployee = new EmployeeManager().GetById(empForm.EmployeeId);


                    subject = subject.Replace("{Employee Name}", forEmployee.Name);

                    message = message.Replace("{Authority Name}", toEmployee.Name)
                                    .Replace("{Employee Name}", forEmployee.Name)
                                    .Replace("{Organizations Name}", SessionManager.CurrentCompany.Name)
                                     .Replace("#Url#", url);

                    break;
            }



            bool mailSent = false;
            if (!string.IsNullOrEmpty(toEmail))
            {
                mailSent = SMTPHelper.SendAsyncMail(toEmail, message, subject, "");
            }

            if (!string.IsNullOrEmpty(toUser))
            {
                PayrollMessage msg = new PayrollMessage();
                msg.Subject = subject;
                msg.Body = message;
                //message.DueDateEng = inst.SubmissionDate;
                msg.IsRead = false;
                DateTime date = SessionManager.GetCurrentDateAndTime();
                msg.ReceivedDate = date.ToShortDateString();
                msg.ReceivedDateEng = date;
                msg.ReceivedTo = toUser;
                msg.SendBy = SessionManager.UserName;
                PayrollDataContext.PayrollMessages.InsertOnSubmit(msg);
                PayrollDataContext.SubmitChanges();
            }



            return status;
        }



        public static Status InsertUpdateRollout(AppraisalRollout inst, bool isEdit)
        {
            Status status = new Status();
            int rolloutid;
            AppraisalRollout dbEntity = new AppraisalRollout();
            dbEntity.ModifiedDate = SessionManager.GetCurrentDateAndTime();
            dbEntity.ModifiedBy = SessionManager.CurrentLoggedInUserID;
            dbEntity.PeriodId = inst.PeriodId;

            AppraisalPeriod period = GetPeriodByID(inst.PeriodId.Value);
            inst.FromDate = period.StartDate;
            inst.ToDate = period.EndDate;

            int empOfThatLevel;


            EmailContent content = CommonManager.GetMailContent((int)EmailContentType.AppraisalEmployeeRollout);
            if (content == null)
            {
                status.ErrorMessage = "Email content not defined.";
                return status;
            }


            List<AppraisalRolloutEmployeeListResult> empList = new List<AppraisalRolloutEmployeeListResult>();
            empList = PayrollDataContext.AppraisalRolloutEmployeeList(inst.LevelID, inst.DepartmentID, inst.PeriodId, inst.RolloutGroupRef_ID).ToList();



            empOfThatLevel = empList.Count();








            List<GetEmployeeTargetForDateResult> empTargetList = PayrollDataContext.GetEmployeeTargetForDate(inst.FromDate, inst.ToDate, null, -1).ToList();

            if (isEdit)
            {
                dbEntity = PayrollDataContext.AppraisalRollouts.Where(x => x.RolloutID == inst.RolloutID).FirstOrDefault();
                //  dbEntity.NumEmployees = empOfThatLevel;
                dbEntity.FormID = inst.FormID;
                dbEntity.LevelID = inst.LevelID;
                dbEntity.DepartmentID = inst.DepartmentID;
                dbEntity.RolloutDate = inst.RolloutDate;
                dbEntity.SubmissionDate = inst.SubmissionDate;
                dbEntity.RolloutMessage = inst.RolloutMessage;
                rolloutid = dbEntity.RolloutID;

                dbEntity.AppraisalRolloutFlows.Clear();
                dbEntity.AppraisalRolloutFlows.AddRange(inst.AppraisalRolloutFlows);
            }
            else
            {
                if (empOfThatLevel == 0)
                {
                    status.ErrorMessage = "No employee left under this level for rollout.";
                    return status;
                }

                inst.NumEmployees = empOfThatLevel;
                inst.ModifiedBy = dbEntity.ModifiedBy;
                inst.ModifiedDate = dbEntity.ModifiedDate;
                inst.CreatedBy = dbEntity.ModifiedBy;
                inst.CreatedDate = dbEntity.ModifiedDate;
                PayrollDataContext.AppraisalRollouts.InsertOnSubmit(inst);

                PayrollDataContext.SubmitChanges();

                rolloutid = inst.RolloutID;
            }

            AppraisalEmployeeForm empForm;
            //PayrollMessage message;

            List<AppraisalEmployeeForm> empAppraisalList = new List<AppraisalEmployeeForm>();

            List<AppraisalFormTarget> formTargetList = PayrollDataContext.AppraisalFormTargets.Where(x => x.AppraisalFormRef_ID == inst.FormID)
                .OrderBy(x => x.TargetRef_ID).ToList();

            if (isEdit == false)
            {

                foreach (var empInst in empList)
                {

                    empForm = new AppraisalEmployeeForm();

                    //--update score in Appraisal Employee Form
                    AppraisalEmployeeEducationLocationScore _dbLocationEductaionScore = AppraisalManager.GetAppraisalEducationLocationScore(empInst.EmployeeId, inst.PeriodId.Value);
                    if (_dbLocationEductaionScore != null)
                    {
                        empForm.EducationScore = _dbLocationEductaionScore.EducationScore;
                        empForm.LocationScore = _dbLocationEductaionScore.LocationScore;
                        empForm.SeniorityScore = _dbLocationEductaionScore.SeniorityScore;

                        // for civil score and percent is same if differnt for other compnay place different logic
                        empForm.EducationPercent = _dbLocationEductaionScore.EducationScore;
                        empForm.LocationTransferPercent = _dbLocationEductaionScore.LocationScore;
                        empForm.WorkHistorySeniorityPercent = _dbLocationEductaionScore.SeniorityScore;
                    }
                    //------------------------

                    empForm.AppraisalFormRef_ID = inst.FormID.Value;
                    empForm.PeriodId = inst.PeriodId.Value;
                    empForm.EmployeeId = empInst.EmployeeId;
                    empForm.RolloutDate = inst.RolloutDate;
                    empForm.RolloutID = rolloutid;
                    empForm.Status = 0;
                    empForm.CreatedByUserID = SessionManager.CurrentLoggedInUserID;
                    empForm.CreatedOn = GetCurrentDateAndTime();
                    empAppraisalList.Add(empForm);

                    empInst.EmployeeForm = empForm;

                    List<AppraisalEmployeeFormTarget> targets = new List<AppraisalEmployeeFormTarget>();

                    foreach (AppraisalFormTarget target in formTargetList)
                    {
                        AppraisalEmployeeFormTarget item = new AppraisalEmployeeFormTarget();
                        item.TargetRef_ID = target.TargetRef_ID;

                        GetEmployeeTargetForDateResult assignedTarget = empTargetList.FirstOrDefault(x =>
                            x.EmployeeId == empInst.EmployeeId && x.TargetId == target.TargetRef_ID);
                        if (assignedTarget != null && assignedTarget.AssignedTarget != null && assignedTarget.AssignedTarget != 0)
                        {
                            item.AssignedTargetValue = assignedTarget.AssignedTarget == null ? 0 : assignedTarget.AssignedTarget.Value;

                            item.SelfAchievementValue = assignedTarget.Achievement == null ? 0 : assignedTarget.Achievement.Value;

                            empForm.AppraisalEmployeeFormTargets.Add(item);
                        }

                    }

                }


                PayrollDataContext.AppraisalEmployeeForms.InsertAllOnSubmit(empAppraisalList);
            }

            PayrollDataContext.SubmitChanges();



            if (isEdit == false)
            {
                foreach (var empInst in empList)
                {
                    SendAppraisalMailAndMessage(EmailContentType.AppraisalEmployeeRollout, empInst.EmployeeId,
                        inst, empInst.EmployeeForm);
                }
            }

            status.IsSuccess = true;
            return status;
        }

        public static void UpdateTargetAchievement(int year, List<TargetAchievmentBO> list)
        {
            List<AppraisalEmployeeTarget> dbtargets = PayrollDataContext.AppraisalEmployeeTargets
                .Where(x => x.Year == year).ToList();

            List<AppraisalEmployeeTarget> newInsert = new List<AppraisalEmployeeTarget>();

            CustomDate date1Start = new CustomDate(1, 1, year, IsEnglish);
            CustomDate date1End = new CustomDate(DateHelper.GetTotalDaysInTheMonth(year, 1, IsEnglish), 1, year, IsEnglish);
            DateTime start1 = date1Start.EnglishDate;
            DateTime end1 = date1End.EnglishDate;

            CustomDate date2Start = new CustomDate(1, 2, year, IsEnglish);
            CustomDate date2End = new CustomDate(DateHelper.GetTotalDaysInTheMonth(year, 2, IsEnglish), 2, year, IsEnglish);
            DateTime start2 = date2Start.EnglishDate;
            DateTime end2 = date2End.EnglishDate;

            CustomDate date3Start = new CustomDate(1, 3, year, IsEnglish);
            CustomDate date3End = new CustomDate(DateHelper.GetTotalDaysInTheMonth(year, 3, IsEnglish), 3, year, IsEnglish);
            DateTime start3 = date3Start.EnglishDate;
            DateTime end3 = date3End.EnglishDate;

            CustomDate date4Start = new CustomDate(1, 4, year, IsEnglish);
            CustomDate date4End = new CustomDate(DateHelper.GetTotalDaysInTheMonth(year, 4, IsEnglish), 4, year, IsEnglish);
            DateTime start4 = date4Start.EnglishDate;
            DateTime end4 = date4End.EnglishDate;

            CustomDate date5Start = new CustomDate(1, 5, year, IsEnglish);
            CustomDate date5End = new CustomDate(DateHelper.GetTotalDaysInTheMonth(year, 5, IsEnglish), 5, year, IsEnglish);
            DateTime start5 = date5Start.EnglishDate;
            DateTime end5 = date5End.EnglishDate;

            CustomDate date6Start = new CustomDate(1, 6, year, IsEnglish);
            CustomDate date6End = new CustomDate(DateHelper.GetTotalDaysInTheMonth(year, 6, IsEnglish), 6, year, IsEnglish);
            DateTime start6 = date6Start.EnglishDate;
            DateTime end6 = date6End.EnglishDate;

            CustomDate date7Start = new CustomDate(1, 7, year, IsEnglish);
            CustomDate date7End = new CustomDate(DateHelper.GetTotalDaysInTheMonth(year, 7, IsEnglish), 7, year, IsEnglish);
            DateTime start7 = date7Start.EnglishDate;
            DateTime end7 = date7End.EnglishDate;

            CustomDate date8Start = new CustomDate(1, 8, year, IsEnglish);
            CustomDate date8End = new CustomDate(DateHelper.GetTotalDaysInTheMonth(year, 8, IsEnglish), 8, year, IsEnglish);
            DateTime start8 = date8Start.EnglishDate;
            DateTime end8 = date8End.EnglishDate;

            CustomDate date9Start = new CustomDate(1, 9, year, IsEnglish);
            CustomDate date9End = new CustomDate(DateHelper.GetTotalDaysInTheMonth(year, 9, IsEnglish), 9, year, IsEnglish);
            DateTime start9 = date9Start.EnglishDate;
            DateTime end9 = date9End.EnglishDate;

            CustomDate date10Start = new CustomDate(1, 10, year, IsEnglish);
            CustomDate date10End = new CustomDate(DateHelper.GetTotalDaysInTheMonth(year, 10, IsEnglish), 10, year, IsEnglish);
            DateTime start10 = date10Start.EnglishDate;
            DateTime end10 = date10End.EnglishDate;

            CustomDate date11Start = new CustomDate(1, 11, year, IsEnglish);
            CustomDate date11End = new CustomDate(DateHelper.GetTotalDaysInTheMonth(year, 11, IsEnglish), 11, year, IsEnglish);
            DateTime start11 = date11Start.EnglishDate;
            DateTime end11 = date11End.EnglishDate;

            CustomDate date12Start = new CustomDate(1, 12, year, IsEnglish);
            CustomDate date12End = new CustomDate(DateHelper.GetTotalDaysInTheMonth(year, 12, IsEnglish), 12, year, IsEnglish);
            DateTime start12 = date12Start.EnglishDate;
            DateTime end12 = date12End.EnglishDate;


            foreach (TargetAchievmentBO item in list)
            {
                AppraisalEmployeeTarget t1 = dbtargets.FirstOrDefault(x => x.EmployeeId == item.EIN && x.TargetID == item.TargetID && x.Month == 1);
                AppraisalEmployeeTarget t2 = dbtargets.FirstOrDefault(x => x.EmployeeId == item.EIN && x.TargetID == item.TargetID && x.Month == 2);
                AppraisalEmployeeTarget t3 = dbtargets.FirstOrDefault(x => x.EmployeeId == item.EIN && x.TargetID == item.TargetID && x.Month == 3);
                AppraisalEmployeeTarget t4 = dbtargets.FirstOrDefault(x => x.EmployeeId == item.EIN && x.TargetID == item.TargetID && x.Month == 4);
                AppraisalEmployeeTarget t5 = dbtargets.FirstOrDefault(x => x.EmployeeId == item.EIN && x.TargetID == item.TargetID && x.Month == 5);
                AppraisalEmployeeTarget t6 = dbtargets.FirstOrDefault(x => x.EmployeeId == item.EIN && x.TargetID == item.TargetID && x.Month == 6);
                AppraisalEmployeeTarget t7 = dbtargets.FirstOrDefault(x => x.EmployeeId == item.EIN && x.TargetID == item.TargetID && x.Month == 7);
                AppraisalEmployeeTarget t8 = dbtargets.FirstOrDefault(x => x.EmployeeId == item.EIN && x.TargetID == item.TargetID && x.Month == 8);
                AppraisalEmployeeTarget t9 = dbtargets.FirstOrDefault(x => x.EmployeeId == item.EIN && x.TargetID == item.TargetID && x.Month == 9);
                AppraisalEmployeeTarget t10 = dbtargets.FirstOrDefault(x => x.EmployeeId == item.EIN && x.TargetID == item.TargetID && x.Month == 10);
                AppraisalEmployeeTarget t11 = dbtargets.FirstOrDefault(x => x.EmployeeId == item.EIN && x.TargetID == item.TargetID && x.Month == 11);
                AppraisalEmployeeTarget t12 = dbtargets.FirstOrDefault(x => x.EmployeeId == item.EIN && x.TargetID == item.TargetID && x.Month == 12);

                if (t1 != null)
                {
                    t1.Value = item.Target1;
                    t1.AchievementValue = item.Achievment1;
                }
                else if (item.Target1 != 0 || item.Achievment1 != 0)
                {
                    t1 = new AppraisalEmployeeTarget { Value = item.Target1, AchievementValue = item.Achievment1, EmployeeId = item.EIN, TargetID = item.TargetID, Year = year, Month = 1, StartDate = start1, EndDate = end1 };
                    newInsert.Add(t1);
                }

                if (t2 != null)
                {
                    t2.Value = item.Target2;
                    t2.AchievementValue = item.Achievment2;
                }
                else if (item.Target2 != 0 || item.Achievment2 != 0)
                {
                    t2 = new AppraisalEmployeeTarget { Value = item.Target2, AchievementValue = item.Achievment2, EmployeeId = item.EIN, TargetID = item.TargetID, Year = year, Month = 2, StartDate = start2, EndDate = end2 };
                    newInsert.Add(t2);
                }
                if (t3 != null)
                {
                    t3.Value = item.Target3;
                    t3.AchievementValue = item.Achievment3;
                }
                else if (item.Target3 != 0 || item.Achievment3 != 0)
                {
                    t3 = new AppraisalEmployeeTarget { Value = item.Target3, AchievementValue = item.Achievment3, EmployeeId = item.EIN, TargetID = item.TargetID, Year = year, Month = 3, StartDate = start3, EndDate = end3 };
                    newInsert.Add(t3);
                }
                if (t4 != null)
                {
                    t4.Value = item.Target4;
                    t4.AchievementValue = item.Achievment4;
                }
                else if (item.Target4 != 0 || item.Achievment4 != 0)
                {
                    t4 = new AppraisalEmployeeTarget { Value = item.Target4, AchievementValue = item.Achievment4, EmployeeId = item.EIN, TargetID = item.TargetID, Year = year, Month = 4, StartDate = start4, EndDate = end4 };
                    newInsert.Add(t4);
                }
                if (t5 != null)
                {
                    t5.Value = item.Target5;
                    t5.AchievementValue = item.Achievment5;
                }
                else if (item.Target5 != 0 || item.Achievment5 != 0)
                {
                    t5 = new AppraisalEmployeeTarget { Value = item.Target5, AchievementValue = item.Achievment5, EmployeeId = item.EIN, TargetID = item.TargetID, Year = year, Month = 5, StartDate = start5, EndDate = end5 };
                    newInsert.Add(t5);
                }
                if (t6 != null)
                {
                    t6.Value = item.Target6;
                    t6.AchievementValue = item.Achievment6;
                }
                else if (item.Target6 != 0 || item.Achievment6 != 0)
                {
                    t6 = new AppraisalEmployeeTarget { Value = item.Target6, AchievementValue = item.Achievment6, EmployeeId = item.EIN, TargetID = item.TargetID, Year = year, Month = 6, StartDate = start6, EndDate = end6 };
                    newInsert.Add(t6);
                }

                if (t7 != null)
                {
                    t7.Value = item.Target7;
                    t7.AchievementValue = item.Achievment7;
                }
                else if (item.Target7 != 0 || item.Achievment7 != 0)
                {
                    t7 = new AppraisalEmployeeTarget { Value = item.Target7, AchievementValue = item.Achievment7, EmployeeId = item.EIN, TargetID = item.TargetID, Year = year, Month = 7, StartDate = start7, EndDate = end7 };
                    newInsert.Add(t7);
                }
                if (t8 != null)
                {
                    t8.Value = item.Target8;
                    t8.AchievementValue = item.Achievment8;
                }
                else if (item.Target8 != 0 || item.Achievment8 != 0)
                {
                    t8 = new AppraisalEmployeeTarget { Value = item.Target8, AchievementValue = item.Achievment8, EmployeeId = item.EIN, TargetID = item.TargetID, Year = year, Month = 8, StartDate = start8, EndDate = end8 };
                    newInsert.Add(t8);
                }
                if (t9 != null)
                {
                    t9.Value = item.Target9;
                    t9.AchievementValue = item.Achievment9;
                }
                else if (item.Target9 != 0 || item.Achievment9 != 0)
                {
                    t9 = new AppraisalEmployeeTarget { Value = item.Target9, AchievementValue = item.Achievment9, EmployeeId = item.EIN, TargetID = item.TargetID, Year = year, Month = 9, StartDate = start9, EndDate = end9 };
                    newInsert.Add(t9);
                }
                if (t10 != null)
                {
                    t10.Value = item.Target10;
                    t10.AchievementValue = item.Achievment10;
                }
                else if (item.Target10 != 0 || item.Achievment10 != 0)
                {
                    t10 = new AppraisalEmployeeTarget { Value = item.Target10, AchievementValue = item.Achievment10, EmployeeId = item.EIN, TargetID = item.TargetID, Year = year, Month = 10, StartDate = start10, EndDate = end10 };
                    newInsert.Add(t10);
                }
                if (t11 != null)
                {
                    t11.Value = item.Target11;
                    t11.AchievementValue = item.Achievment11;
                }
                else if (item.Target11 != 0 || item.Achievment11 != 0)
                {
                    t11 = new AppraisalEmployeeTarget { Value = item.Target11, AchievementValue = item.Achievment11, EmployeeId = item.EIN, TargetID = item.TargetID, Year = year, Month = 11, StartDate = start11, EndDate = end11 };
                    newInsert.Add(t11);
                }
                if (t12 != null)
                {
                    t12.Value = item.Target12;
                    t12.AchievementValue = item.Achievment12;
                }
                else if (item.Target12 != 0 || item.Achievment12 != 0)
                {
                    t12 = new AppraisalEmployeeTarget { Value = item.Target12, AchievementValue = item.Achievment12, EmployeeId = item.EIN, TargetID = item.TargetID, Year = year, Month = 12, StartDate = start12, EndDate = end12 };
                    newInsert.Add(t12);
                }

            }

            PayrollDataContext.AppraisalEmployeeTargets.InsertAllOnSubmit(newInsert);
            PayrollDataContext.SubmitChanges();
        }

        public static List<TargetAchievmentBO> GetEmpTargetAchievments(int page, int size, int empId, int targetId, int year)
        {
            List<GetTargetAndAchievmentsResult> targetList = BLL.BaseBiz.PayrollDataContext.
                GetTargetAndAchievments(page, size, empId, targetId).ToList();

            List<int> empIDList = targetList.Select(x => x.EmployeeId).ToList();

            List<TargetAchievmentBO> list = new List<TargetAchievmentBO>();

            List<AppraisalEmployeeTarget> empTargets = PayrollDataContext
                .AppraisalEmployeeTargets.Where(x => empIDList.Contains(x.EmployeeId) && x.Year == year).ToList();

            foreach (GetTargetAndAchievmentsResult item in targetList)
            {
                TargetAchievmentBO emp = new TargetAchievmentBO();
                list.Add(emp);
                emp.EIN = item.EmployeeId;
                emp.Name = item.NAME;
                emp.TargetID = item.TargetID;
                emp.TargetName = item.TargetName;

                AppraisalEmployeeTarget t1 = empTargets.FirstOrDefault(x => x.EmployeeId == item.EmployeeId && x.TargetID == item.TargetID && x.Month == 1);
                AppraisalEmployeeTarget t2 = empTargets.FirstOrDefault(x => x.EmployeeId == item.EmployeeId && x.TargetID == item.TargetID && x.Month == 2);
                AppraisalEmployeeTarget t3 = empTargets.FirstOrDefault(x => x.EmployeeId == item.EmployeeId && x.TargetID == item.TargetID && x.Month == 3);
                AppraisalEmployeeTarget t4 = empTargets.FirstOrDefault(x => x.EmployeeId == item.EmployeeId && x.TargetID == item.TargetID && x.Month == 4);
                AppraisalEmployeeTarget t5 = empTargets.FirstOrDefault(x => x.EmployeeId == item.EmployeeId && x.TargetID == item.TargetID && x.Month == 5);
                AppraisalEmployeeTarget t6 = empTargets.FirstOrDefault(x => x.EmployeeId == item.EmployeeId && x.TargetID == item.TargetID && x.Month == 6);
                AppraisalEmployeeTarget t7 = empTargets.FirstOrDefault(x => x.EmployeeId == item.EmployeeId && x.TargetID == item.TargetID && x.Month == 7);
                AppraisalEmployeeTarget t8 = empTargets.FirstOrDefault(x => x.EmployeeId == item.EmployeeId && x.TargetID == item.TargetID && x.Month == 8);
                AppraisalEmployeeTarget t9 = empTargets.FirstOrDefault(x => x.EmployeeId == item.EmployeeId && x.TargetID == item.TargetID && x.Month == 9);
                AppraisalEmployeeTarget t10 = empTargets.FirstOrDefault(x => x.EmployeeId == item.EmployeeId && x.TargetID == item.TargetID && x.Month == 10);
                AppraisalEmployeeTarget t11 = empTargets.FirstOrDefault(x => x.EmployeeId == item.EmployeeId && x.TargetID == item.TargetID && x.Month == 11);
                AppraisalEmployeeTarget t12 = empTargets.FirstOrDefault(x => x.EmployeeId == item.EmployeeId && x.TargetID == item.TargetID && x.Month == 12);

                if (t1 != null)
                {
                    emp.Target1 = t1.Value;
                    emp.Achievment1 = t1.AchievementValue;
                }
                if (t2 != null)
                {
                    emp.Target2 = t2.Value;
                    emp.Achievment2 = t2.AchievementValue;
                }
                if (t3 != null)
                {
                    emp.Target3 = t3.Value;
                    emp.Achievment3 = t3.AchievementValue;
                }
                if (t4 != null)
                {
                    emp.Target4 = t4.Value;
                    emp.Achievment4 = t4.AchievementValue;
                }
                if (t5 != null)
                {
                    emp.Target5 = t5.Value;
                    emp.Achievment5 = t5.AchievementValue;
                }
                if (t6 != null)
                {
                    emp.Target6 = t6.Value;
                    emp.Achievment6 = t6.AchievementValue;
                }
                if (t7 != null)
                {
                    emp.Target7 = t7.Value;
                    emp.Achievment7 = t7.AchievementValue;
                }
                if (t8 != null)
                {
                    emp.Target8 = t8.Value;
                    emp.Achievment8 = t8.AchievementValue;
                }
                if (t9 != null)
                {
                    emp.Target9 = t9.Value;
                    emp.Achievment9 = t9.AchievementValue;
                }
                if (t10 != null)
                {
                    emp.Target10 = t10.Value;
                    emp.Achievment10 = t10.AchievementValue;
                }
                if (t11 != null)
                {
                    emp.Target11 = t11.Value;
                    emp.Achievment11 = t11.AchievementValue;
                }
                if (t12 != null)
                {
                    emp.Target12 = t12.Value;
                    emp.Achievment12 = t12.AchievementValue;
                }

                emp.TotalRows = item.TotalRows.Value;
            }

            return list;

        }
        public static bool IsAppraisalRolloutAlreadyExistsForThisPeriod(int employeeId, int periodId, int formId)
        {
            if (PayrollDataContext.AppraisalRollouts.Any(
                x => x.EmployeeId == employeeId && x.PeriodId == periodId && x.FormID == formId
                //    &&
                //(
                //    (
                //        x.FromDate >= fromDate && x.FromDate <= toDate
                //    )
                //    ||
                //    (
                //        x.ToDate >= fromDate && x.ToDate <= toDate
                //    )
                //)
                ))
            {
                return true;
            }

            if (PayrollDataContext.AppraisalEmployeeForms.Any(
                x => x.EmployeeId == employeeId && x.PeriodId == periodId && x.AppraisalFormRef_ID == formId
                //    &&
                //(
                //    (
                //        fromDate >= x.FromDate && fromDate <= x.ToDate
                //    )
                //    ||
                //    (
                //       toDate >= x.FromDate && toDate <= x.ToDate
                //    )
                //)
                ))
            {
                return true;
            }

            return false;
        }

        public static Status UpdateTargetValue(int appraisalEmpFormId)
        {
            AppraisalEmployeeForm dbForm = PayrollDataContext.AppraisalEmployeeForms.FirstOrDefault(x => x.AppraisalEmployeeFormID == appraisalEmpFormId);
            Status status = new Status();
            if (dbForm.Status == null || dbForm.Status == 0)
            {
            }
            else
            {
                status.ErrorMessage = "Only not submitted forms can be updated.";
                return status;
            }


            AppraisalPeriod period = GetPeriodByID(dbForm.PeriodId.Value);
            List<GetEmployeeTargetForDateResult> empTargetList = PayrollDataContext.GetEmployeeTargetForDate
                (period.StartDate.Value, period.EndDate.Value, null, dbForm.EmployeeId).ToList();


            List<AppraisalEmployeeFormTarget> targets = dbForm.AppraisalEmployeeFormTargets.ToList();
            foreach (AppraisalEmployeeFormTarget target in targets)
            {
                GetEmployeeTargetForDateResult newTarget = empTargetList.FirstOrDefault(x => x.TargetId == target.TargetRef_ID);
                if (newTarget != null && newTarget.AssignedTarget != null && newTarget.AssignedTarget != 0)
                {
                    target.AssignedTargetValue = newTarget.AssignedTarget;
                    target.SelfAchievementValue = newTarget.Achievement;
                }
                //else
                //{

                //}
            }

            PayrollDataContext.SubmitChanges();

            return status;

        }

        public static Status InsertUpdateEmpTarget(AppraisalEmployeeFormTarget target)
        {
            AppraisalEmployeeFormTarget dbTarget = PayrollDataContext.AppraisalEmployeeFormTargets
                .FirstOrDefault(x => x.TargetRef_ID == target.TargetRef_ID && x.AppraisalEmployeeFormRef_ID == target.AppraisalEmployeeFormRef_ID);

            if (dbTarget != null)
            {
                dbTarget.Weight = target.Weight;
                dbTarget.AssignedTargetTextValue = target.AssignedTargetTextValue;
                dbTarget.AchievementTextValue = target.AchievementTextValue;
            }
            else
                PayrollDataContext.AppraisalEmployeeFormTargets.InsertOnSubmit(target);

            PayrollDataContext.SubmitChanges();

            return new Status();
        }
        public static Status DeleteEmpTarget(AppraisalEmployeeFormTarget target)
        {
            AppraisalEmployeeFormTarget dbTarget = PayrollDataContext.AppraisalEmployeeFormTargets
                .FirstOrDefault(x => x.TargetRef_ID == target.TargetRef_ID && x.AppraisalEmployeeFormRef_ID == target.AppraisalEmployeeFormRef_ID);

            if (dbTarget != null)
            {
                PayrollDataContext.AppraisalEmployeeFormTargets.DeleteOnSubmit(dbTarget);

                dbTarget.AppraisalEmployeeForm.IsTargetSaved = false;
            }

            PayrollDataContext.SubmitChanges();

            return new Status();
        }
        public static Status InsertMegaRollout(AppraisalRollout inst, bool isEdit, List<AppraisalEmployeeFormTarget> targetList, List<ApprovalFlow> flowList)
        {
            Status status = new Status();
            int rolloutid;
            AppraisalRollout dbEntity = new AppraisalRollout();
            dbEntity.ModifiedDate = SessionManager.GetCurrentDateAndTime();
            dbEntity.ModifiedBy = SessionManager.CurrentLoggedInUserID;

            EmailContent content = CommonManager.GetMailContent((int)EmailContentType.AppraisalEmployeeRollout);
            if (content == null)
            {
                status.ErrorMessage = "Email content not defined.";
                return status;
            }

            if (IsAppraisalRolloutAlreadyExistsForThisPeriod(inst.EmployeeId.Value, inst.PeriodId.Value, inst.FormID.Value))
            {
                status.ErrorMessage = "Appraisal rollout already exists for this period and form.";
                return status;
            }

            //int empOfThatLevel;

            //List<AppraisalRolloutEmployeeListResult> empList = new List<AppraisalRolloutEmployeeListResult>();

            UUser empUser = UserManager.GetUserByEmployee(inst.EmployeeId.Value);

            //empList.Add(new AppraisalRolloutEmployeeListResult
            //{
            //    EmployeeId = inst.EmployeeId.Value,
            //    UserName =
            //        (empUser == null ? "" : empUser.UserName)
            //});


            //empOfThatLevel = empList.Count();




            if (isEdit)
            {
                dbEntity = PayrollDataContext.AppraisalRollouts.Where(x => x.RolloutID == inst.RolloutID).FirstOrDefault();
                CopyObject<AppraisalRollout>(inst, ref dbEntity, "ModifiedDate", "ModifiedBy");
                rolloutid = dbEntity.RolloutID;
            }
            else
            {


                inst.NumEmployees = 1;
                inst.ModifiedBy = dbEntity.ModifiedBy;
                inst.ModifiedDate = dbEntity.ModifiedDate;
                inst.CreatedBy = dbEntity.ModifiedBy;
                inst.CreatedDate = dbEntity.ModifiedDate;
                PayrollDataContext.AppraisalRollouts.InsertOnSubmit(inst);
                PayrollDataContext.SubmitChanges();

                rolloutid = inst.RolloutID;
            }



            AppraisalEmployeeForm empForm = null;
            PayrollMessage message;


            AppraisalEmployeeForm dbForm = PayrollDataContext.AppraisalEmployeeForms.FirstOrDefault(x => x.EmployeeId == inst.EmployeeId
                && x.PeriodId == inst.PeriodId
                && x.AppraisalFormRef_ID == inst.FormID);

            AppraisalEmployeeEducationLocationScore _dbLocationEductaionScore = AppraisalManager.GetAppraisalEducationLocationScore(inst.EmployeeId.Value, inst.PeriodId.Value);

            if (dbForm != null)
            {

                //--update score in Appraisal Employee Form

                if (_dbLocationEductaionScore != null)
                {
                    dbForm.EducationScore = _dbLocationEductaionScore.EducationScore;
                    dbForm.LocationScore = _dbLocationEductaionScore.LocationScore;
                    dbForm.SeniorityScore = _dbLocationEductaionScore.SeniorityScore;

                    // in case of Civil score and perent is same, in future if diff for other company set percent conversion here
                    dbForm.EducationPercent = dbForm.EducationScore;
                    dbForm.LocationTransferPercent = dbForm.LocationScore;
                    dbForm.WorkHistorySeniorityPercent = dbForm.SeniorityScore;

                }
                //------------------------

                List<AppraisalEmployeeFormTarget> targets = dbForm.AppraisalEmployeeFormTargets.ToList();
                foreach (AppraisalEmployeeFormTarget target in targets)
                {
                    AppraisalEmployeeFormTarget newTarget = targetList.FirstOrDefault(x => x.TargetRef_ID == target.TargetRef_ID);
                    if (newTarget != null)
                    {
                        target.AssignedTargetValue = newTarget.AssignedTargetValue;
                        target.SelfAchievementValue = newTarget.Achievement;
                        target.Weight = newTarget.Weight;
                        target.AssignedTargetTextValue = newTarget.AssignedTargetTextValue;
                        target.AchievementTextValue = newTarget.AchievementTextValue;
                    }
                }

                // add flow
                List<AppraisalEmployeeFlow> actualflowList = new List<AppraisalEmployeeFlow>();
                foreach (ApprovalFlow item in flowList)
                {
                    AppraisalEmployeeFlow flow = new AppraisalEmployeeFlow();
                    flow.StepRef_ID = item.StepID;
                    flow.EmployeeID = item.EmployeeId;
                    flow.ShowAdditionalStep = item.ShowAdditionalStep;
                    flow.StepName = item.StepName;
                    flow.AuthorityTypeDisplayName = item.AuthorityTypeDisplayName;
                    actualflowList.Add(flow);
                }
                dbForm.AppraisalEmployeeFlows.Clear();
                dbForm.AppraisalEmployeeFlows.AddRange(actualflowList);

            }
            else
            {

                empForm = new AppraisalEmployeeForm();

                if (_dbLocationEductaionScore != null)
                {
                    empForm.EducationScore = _dbLocationEductaionScore.EducationScore;
                    empForm.LocationScore = _dbLocationEductaionScore.LocationScore;
                    empForm.SeniorityScore = _dbLocationEductaionScore.SeniorityScore;
                }

                // in case of Civil score and perent is same, in future if diff for other company set percent conversion here
                empForm.EducationPercent = empForm.EducationScore;
                empForm.LocationTransferPercent = empForm.LocationScore;
                empForm.WorkHistorySeniorityPercent = empForm.SeniorityScore;

                empForm.AppraisalFormRef_ID = inst.FormID.Value;
                empForm.PeriodId = inst.PeriodId.Value;
                empForm.EmployeeId = inst.EmployeeId.Value;
                empForm.RolloutDate = inst.RolloutDate;
                empForm.RolloutID = rolloutid;
                empForm.Status = 0;
                empForm.CreatedByUserID = SessionManager.CurrentLoggedInUserID;
                empForm.CreatedOn = GetCurrentDateAndTime();

                // add flow
                List<AppraisalEmployeeFlow> actualflowList = new List<AppraisalEmployeeFlow>();
                foreach (ApprovalFlow item in flowList)
                {
                    AppraisalEmployeeFlow flow = new AppraisalEmployeeFlow();
                    flow.StepRef_ID = item.StepID;
                    flow.EmployeeID = item.EmployeeId;
                    flow.ShowAdditionalStep = item.ShowAdditionalStep;
                    flow.StepName = item.StepName;
                    flow.AuthorityTypeDisplayName = item.AuthorityTypeDisplayName;
                    actualflowList.Add(flow);
                }
                empForm.AppraisalEmployeeFlows.AddRange(actualflowList);

                PayrollDataContext.AppraisalEmployeeForms.InsertOnSubmit(empForm);

                // set targets
                empForm.AppraisalEmployeeFormTargets.AddRange(targetList);
            }



            PayrollDataContext.SubmitChanges();




            if (isEdit == false)
            {
                SendAppraisalMailAndMessage(EmailContentType.AppraisalEmployeeRollout, inst.EmployeeId.Value,
                    inst, dbForm != null ? dbForm : empForm);
            }

            status.IsSuccess = true;
            return status;
        }

        public static List<EEmployee> GetRolloutEmployeeList(int rolloutId)
        {

            var list =
                (
                   from e in PayrollDataContext.AppraisalEmployeeForms
                   join ee in PayrollDataContext.EEmployees on e.EmployeeId equals ee.EmployeeId
                   where e.RolloutID == rolloutId// && e.EmployeeId == null
                   orderby ee.Name
                   select new
                   {
                       EmployeeId = ee.EmployeeId,
                       Name = ee.Name + " - " + ee.EmployeeId
                   }
                ).ToList();

            List<EEmployee> newList = new List<EEmployee>();

            foreach (var item in list)
            {
                newList.Add(new EEmployee { EmployeeId = item.EmployeeId, Name = item.Name });
            }

            return newList;
        }

        public static Status UpdateMegaRollout(AppraisalRollout inst, DateTime duedate, List<AppraisalEmployeeFormTarget> targetList, List<ApprovalFlow> flowList)
        {
            Status status = new Status();

            AppraisalRollout dbEntity = PayrollDataContext.AppraisalRollouts.
                FirstOrDefault(x => x.RolloutID == inst.RolloutID);

            //dbEntity.ModifiedDate = SessionManager.GetCurrentDateAndTime();
            //dbEntity.ModifiedBy = SessionManager.CurrentLoggedInUserID;

            //dbEntity.Notes = inst.Notes;

            AppraisalEmployeeForm dbForm = PayrollDataContext.AppraisalEmployeeForms
                .FirstOrDefault(x => x.RolloutID == inst.RolloutID && x.EmployeeId == inst.EmployeeId);


            if (dbEntity != null && inst.Step2SupervisorEmployeeId != null && dbEntity.Step2SupervisorEmployeeId != null)
            {
                dbEntity.Step2SupervisorEmployeeId = inst.Step2SupervisorEmployeeId;
            }

            if (dbForm != null)
            {
                //--update score in Appraisal Employee Form
                AppraisalEmployeeEducationLocationScore _dbLocationEductaionScore = AppraisalManager.GetAppraisalEducationLocationScore(inst.EmployeeId.Value, inst.PeriodId.Value);
                if (_dbLocationEductaionScore != null)
                {
                    dbForm.EducationScore = _dbLocationEductaionScore.EducationScore;
                    dbForm.LocationScore = _dbLocationEductaionScore.LocationScore;
                    dbForm.SeniorityScore = _dbLocationEductaionScore.SeniorityScore;
                }
                //------------------------

                dbForm.DueDate = inst.SubmissionDate;
            }




            // add flow
            List<AppraisalEmployeeFlow> actualflowList = new List<AppraisalEmployeeFlow>();
            foreach (ApprovalFlow item in flowList)
            {
                AppraisalEmployeeFlow flow = new AppraisalEmployeeFlow();
                flow.StepRef_ID = item.StepID;
                flow.EmployeeID = item.EmployeeId;
                flow.ShowAdditionalStep = item.ShowAdditionalStep;
                flow.StepName = item.StepName;
                flow.AuthorityTypeDisplayName = item.AuthorityTypeDisplayName;
                actualflowList.Add(flow);
            }
            dbForm.AppraisalEmployeeFlows.Clear();
            dbForm.AppraisalEmployeeFlows.AddRange(actualflowList);

            if (dbForm != null)
            {

                List<AppraisalEmployeeFormTarget> targets = dbForm.AppraisalEmployeeFormTargets.ToList();
                foreach (AppraisalEmployeeFormTarget target in targets)
                {
                    AppraisalEmployeeFormTarget newTarget = targetList.FirstOrDefault(x => x.TargetRef_ID == target.TargetRef_ID);
                    if (newTarget != null)
                    {
                        target.AssignedTargetValue = newTarget.AssignedTargetValue;
                        target.SelfAchievementValue = newTarget.Achievement;

                        target.Weight = newTarget.Weight;
                        target.AssignedTargetTextValue = newTarget.AssignedTargetTextValue;
                        target.AchievementTextValue = newTarget.AchievementTextValue;
                    }
                    else
                        PayrollDataContext.AppraisalEmployeeFormTargets.DeleteOnSubmit(target);
                }

            }



            PayrollDataContext.SubmitChanges();






            status.IsSuccess = true;
            return status;
        }

        public static List<AppraisalEmployeeFlow> GetFlowList(int appraisalEmpFormId)
        {
            return PayrollDataContext.AppraisalEmployeeFlows.Where(x => x.AppraisalEmployeeFormRef_ID == appraisalEmpFormId).OrderBy(x => x.StepRef_ID).ToList();
        }
        public static Status InsertUpdateFormCategory(AppraisalFormQuestionnaireCategory inst, bool isEdit)
        {
            Status status = new Status();
            AppraisalFormQuestionnaireCategory dbEntity = new AppraisalFormQuestionnaireCategory();

            if (isEdit)
            {
                dbEntity = PayrollDataContext.AppraisalFormQuestionnaireCategories.Where(x => x.CategoryID == inst.CategoryID).FirstOrDefault();
                dbEntity.Name = inst.Name;
                dbEntity.AppraisalFormRef_ID = inst.AppraisalFormRef_ID;
                dbEntity.Sequence = inst.Sequence;
            }
            else
            {
                PayrollDataContext.AppraisalFormQuestionnaireCategories.InsertOnSubmit(inst);
            }

            PayrollDataContext.SubmitChanges();
            status.IsSuccess = true;
            return status;
        }




        public static Status InsertUpdateReviewFormQuetion(AppraisalFormReview inst, bool isEdit)
        {
            Status status = new Status();
            AppraisalFormReview dbEntity = new AppraisalFormReview();

            if (isEdit)
            {
                dbEntity = PayrollDataContext.AppraisalFormReviews.Where(x => x.QuestionnareID == inst.QuestionnareID).FirstOrDefault();
                dbEntity.Question = inst.Question;
                dbEntity.Weight = inst.Weight;
            }
            else
            {
                PayrollDataContext.AppraisalFormReviews.InsertOnSubmit(inst);
            }

            PayrollDataContext.SubmitChanges();
            status.IsSuccess = true;
            return status;
        }

        public static AppraisalFormQuestionnaire GetQuestionByFormID(int RowID)
        {
            return PayrollDataContext.AppraisalFormQuestionnaires.Where(x => x.QuestionnareID == RowID).FirstOrDefault();
        }

        public static AppraisalRollout GetRolloutByFormID(int RowID)
        {
            return PayrollDataContext.AppraisalRollouts.Where(x => x.RolloutID == RowID).FirstOrDefault();
        }

        public static AppraisalFormReview GetReviewQuestionByFormID(int RowID)
        {
            return PayrollDataContext.AppraisalFormReviews.Where(x => x.QuestionnareID == RowID).FirstOrDefault();
        }

        public static Status UpdateQuestionnaireFormData(AppraisalForm inst, List<AppraisalFormQuestionnaire> catQuestionList)
        {
            Status status = new Status();
            AppraisalForm dbEntity = new AppraisalForm();
            dbEntity = PayrollDataContext.AppraisalForms.Where(x => x.AppraisalFormID == inst.AppraisalFormID).FirstOrDefault();
            dbEntity.QuestionnareDescription = inst.QuestionnareDescription;
            dbEntity.QuestionnareName = inst.QuestionnareName;
            dbEntity.SummaryName = inst.SummaryName;
            dbEntity.SummaryDescription = inst.SummaryDescription;
            dbEntity.AllowQuestionSelfRating = inst.AllowQuestionSelfRating;
            dbEntity.QuestionnareRatingScaleRef_ID = inst.QuestionnareRatingScaleRef_ID;
            dbEntity.AllowQuestionSelfComment = inst.AllowQuestionSelfComment;

            dbEntity.AllowQuestionSupRating = inst.AllowQuestionSupRating;
            dbEntity.AllowQuestionSupComment = inst.AllowQuestionSupComment;

            dbEntity.QuestionaireMarkingType = inst.QuestionaireMarkingType;

            List<AppraisalFormQuestionnaire> dbQuestionList = dbEntity.AppraisalFormQuestionnaires.ToList();
            List<AppraisalFormQuestionnaireCategory> dbCatList = dbEntity.AppraisalFormQuestionnaireCategories.ToList();

            foreach (AppraisalFormQuestionnaireCategory dbCat in dbCatList)
            {
                AppraisalFormQuestionnaire catQuest = catQuestionList.FirstOrDefault(x => x.CategoryID == dbCat.CategoryID);
                if (catQuest != null)
                    dbCat.Weight = catQuest.Weight;

                double weight = dbCat.Weight == null ? 0 : dbCat.Weight.Value;
                int count = (int)dbQuestionList.Where(x => x.CategoryID == dbCat.CategoryID).Count();

                if (count > 0)
                {
                    weight = weight / count;
                    weight = double.Parse(weight.ToString("n2"));
                }

                foreach (AppraisalFormQuestionnaire dbQuestion in dbQuestionList)
                {
                    // if has weightage set from UI set that value
                    AppraisalFormQuestionnaire question = 
                        catQuestionList.FirstOrDefault(x => x.IsCategory == false && x.QuestionnareID == dbQuestion.QuestionnareID);
                    if (question.Weight != null && question.Weight != 0)
                        dbQuestion.Weight = question.Weight;
                    else
                        dbQuestion.Weight = weight;
                }
            }



            status.IsSuccess = true;
            PayrollDataContext.SubmitChanges();
            return status;
        }

        public static Status UpdateReviewFormData(AppraisalForm inst)
        {
            Status status = new Status();
            AppraisalForm dbEntity = new AppraisalForm();
            dbEntity = PayrollDataContext.AppraisalForms.Where(x => x.AppraisalFormID == inst.AppraisalFormID).FirstOrDefault();
            dbEntity.ReviewDescription = inst.ReviewDescription;
            dbEntity.ReviewName = inst.ReviewName;
            dbEntity.HideSupervisorReview = inst.HideSupervisorReview;
            status.IsSuccess = true;
            PayrollDataContext.SubmitChanges();
            return status;
        }


        public static AppraisalForm getFormInstanceByID(int formID)
        {
            return PayrollDataContext.AppraisalForms.Where(x => x.AppraisalFormID == formID).FirstOrDefault();
        }
        #region "Form Creation"
        public static List<AppraisalDynamicFormCompetency> GetFormComptency(int formId)
        {
            return
                (
                    from fc in PayrollDataContext.AppraisalFormCompetencies
                    join c in PayrollDataContext.AppraisalCompetencies on fc.CompetencyRef_ID equals c.CompetencyID
                    where fc.AppraisalFormRef_ID == formId
                    orderby fc.Sequence
                    select new AppraisalDynamicFormCompetency
                    {
                        CompetencyID = c.CompetencyID,
                        CategoryID = fc.CompetencyCatRef_ID.Value,
                        Name = c.Name,
                        Order = fc.Sequence,
                        Description = c.Description
                    }
                )
                .ToList();
        }
        public static List<AppraisalDynamicFormTarget> GetFormTarget(int formId)
        {
            return
                (
                    from fc in PayrollDataContext.AppraisalFormTargets
                    join c in PayrollDataContext.AppraisalTargets on fc.TargetRef_ID equals c.TargetID
                    where fc.AppraisalFormRef_ID == formId
                    orderby fc.Sequence
                    select new AppraisalDynamicFormTarget
                    {
                        TargetD = c.TargetID,
                        Name = c.Name,
                        Order = fc.Sequence,
                        Weight = fc.Weight == null ? 0 : fc.Weight.Value
                    }
                )
                .ToList();
        }

        public static List<GetYourAppraisalSPResult> GetYourAppraisalList(int start, int pagesize, int EmoloyeeID, string periodIds)
        {
            List<GetYourAppraisalSPResult> list = PayrollDataContext.GetYourAppraisalSP(start, pagesize, EmoloyeeID, periodIds).ToList();
            DateTime date = GetCurrentDateAndTime();
            date = new DateTime(date.Year, date.Month, date.Day);
            foreach (var item in list)
            {

                if (item.PeriodId != null)
                {
                    item.PeriodName = PayrollDataContext.AppraisalPeriods.FirstOrDefault(x => x.PeriodId == item.PeriodId).Name;
                }

                if (PayrollDataContext.AppraisalForms.Any(x => x.AppraisalFormID == item.AppraisalFormRef_ID))
                {
                    item.AppraisalFormName = PayrollDataContext.AppraisalForms.FirstOrDefault(x => x.AppraisalFormID == item.AppraisalFormRef_ID).Name;
                }

                if (item.Status == (int)AppraisalStatus.Saved)
                    item.StatusText = "Pending";

                AppraisalRollout rollout = PayrollDataContext.AppraisalRollouts.FirstOrDefault(x =>
                 x.RolloutID == item.RolloutID);

                item.FromDate = rollout.FromDate;
                item.ToDate = rollout.ToDate;
                item.DueDate = rollout.SubmissionDate;

                if (item.DueDate == null)
                    item.DueDate = rollout.ToDate;


                if (item.Status != (int)AppraisalStatus.Saved)
                    item.DaysRemaining = "Completed";
                else
                {
                    if (item.DueDate != null)
                    {
                        int days = (item.DueDate.Value - date).Days;
                        if (days > 0)
                            item.DaysRemaining = days + " days";
                        else if (days == 0)
                            item.DaysRemaining = "0 days";
                        else
                            item.DaysRemaining = Math.Abs(days) + " days ago";
                    }

                }
            }

            return list;
        }

        public static List<GetAppraisalListForReviewResult> GetAppraisalReviewList(int start, int pagesize, int EmployeeID, string periodIds)
        {
            //List<AppraisalEmployeeForm> list = PayrollDataContext
            //    .AppraisalEmployeeForms
            //    // skip Saved only status
            //    .Where(x => x.Status != 0 && x.EmployeeId != SessionManager.CurrentLoggedInEmployeeId)
            //    .Where(x => PayrollDataContext.CanViewDocumentUsingApprovalFlow((int)FlowTypeEnum.Appraisal,
            //            x.Status, x.EmployeeId, SessionManager.CurrentLoggedInEmployeeId,x.RolloutID.Value) == true)
            //    .ToList();

            List<GetAppraisalListForReviewResult> list = PayrollDataContext
                .GetAppraisalListForReview(start, pagesize, EmployeeID, periodIds).ToList();

            foreach (GetAppraisalListForReviewResult item in list)
            {

                if (item.PeriodId != null)
                {
                    item.PeriodName = PayrollDataContext.AppraisalPeriods.FirstOrDefault(x => x.PeriodId == item.PeriodId).Name;
                }

                //item.AppraisalFormName = item.AppraisalForm.Name;
                if (PayrollDataContext.AppraisalForms.Any(x => x.AppraisalFormID == item.AppraisalFormRef_ID))
                {
                    item.AppraisalFormName = PayrollDataContext.AppraisalForms.FirstOrDefault(x => x.AppraisalFormID == item.AppraisalFormRef_ID).Name;
                }

                item.EmployeeName = new EmployeeManager().GetById(item.EmployeeId).Name;

                AppraisalRollout rollout = PayrollDataContext.AppraisalRollouts.FirstOrDefault(x =>
              x.RolloutID == item.RolloutID);

                item.FromDate = rollout.FromDate;
                item.ToDate = rollout.ToDate;
            }

            return list;
        }

        public static bool IsSelfAppraisalFormReadonly(int employeeAppraisalFormId)
        {
            AppraisalEmployeeForm empForm = AppraisalManager.GetEmployeeAppraisaleForm(employeeAppraisalFormId);
            // self appraisal form is editable in Saved & Supervisor Commented status only
            if (empForm.Status == (int)AppraisalStatus.Saved ||
                empForm.Status == (int)AppraisalStatus.SupervisorManagerCommented)
                return false;

            return true;
        }

        public static List<AppraisalFormQuestionnaireCategory> GetQuestionCategoryList(int formId)
        {
            return PayrollDataContext.AppraisalFormQuestionnaireCategories
                .Where(x => x.AppraisalFormRef_ID == formId).OrderBy(x => x.Sequence).ToList();
        }
        public static List<AppraisalDynamicFormQuestionnaire> GetFormQuestionnaire(int formId)
        {
            return
                (
                    from fc in PayrollDataContext.AppraisalFormQuestionnaires
                    where fc.AppraisalFormRef_ID == formId
                    orderby fc.Sequence
                    select new AppraisalDynamicFormQuestionnaire
                    {
                        QuestionnareID = fc.QuestionnareID,
                        Name = fc.Question,
                        Order = fc.Sequence,
                        CategoryID = fc.CategoryID,
                        Description = fc.Description
                    }
                )
                .ToList();
        }

        public static List<AppraisalDynamicFormReview> GetFormReview(int formId)
        {
            return
                (
                    from fc in PayrollDataContext.AppraisalFormReviews
                    where fc.AppraisalFormRef_ID == formId
                    orderby fc.Sequence
                    select new AppraisalDynamicFormReview
                    {
                        ReviewID = fc.QuestionnareID,
                        Name = fc.Question,
                        Order = fc.Sequence.Value
                    }
                )
                .ToList();
        }

        public static List<AppraisalEmployeeFormCompetency> GetEmployeeCompetenchList(int employeeFormId)
        {
            return
                PayrollDataContext.AppraisalEmployeeFormCompetencies.Where(x => x.AppraisalEmployeeFormRef_ID
                            == employeeFormId).ToList();
        }

        public static int GetEmpoyeeAppraisalFormIDUsingRolloutID(int rollOutId)
        {
            return PayrollDataContext.AppraisalEmployeeForms
                .FirstOrDefault(x => x.RolloutID == rollOutId).AppraisalEmployeeFormID;
        }
        public static List<AppraisalEmployeeFormTarget> GetEmployeeTargetList(int employeeFormId)
        {
            return
                PayrollDataContext.AppraisalEmployeeFormTargets.Where(x => x.AppraisalEmployeeFormRef_ID
                            == employeeFormId).ToList();
        }
        public static List<AppraisalEmployeeFormReview> GetEmployeeReviewList(int employeeFormId)
        {
            return
                PayrollDataContext.AppraisalEmployeeFormReviews.Where(x => x.AppraisalEmployeeFormRef_ID
                            == employeeFormId).ToList();
        }
        public static AppraisalRatingScale GetFormQuestionRating(int formId)
        {
            int? rating = PayrollDataContext.AppraisalForms.FirstOrDefault(x => x.AppraisalFormID
               == formId).QuestionnareRatingScaleRef_ID;
            if (rating != null)
                return PayrollDataContext.AppraisalRatingScales.FirstOrDefault(x => x.RatingScaleID == rating);

            return null;
        }
        public static AppraisalRatingScale GetFormCompetencyRating(int formId)
        {
            int? rating = PayrollDataContext.AppraisalForms.FirstOrDefault(x => x.AppraisalFormID
                == formId).CompentencyRatingScaleRef_ID;

            if (rating != null)
                return PayrollDataContext.AppraisalRatingScales.FirstOrDefault(x => x.RatingScaleID == rating);

            return null;
        }
        public static AppraisalRatingScale GetFormTargetRating(int formId)
        {
            int? rating = PayrollDataContext.AppraisalForms.FirstOrDefault(x => x.AppraisalFormID
                == formId).TargetRatingScaleRef_ID;

            if (rating != null)
                return PayrollDataContext.AppraisalRatingScales.FirstOrDefault(x => x.RatingScaleID == rating);

            return null;
        }
        public static AppraisalRatingScale GetFormActivityRating(int formId)
        {
            int? rating = PayrollDataContext.AppraisalForms.FirstOrDefault(x => x.AppraisalFormID
                == formId).ActivityRatingScaleRef_ID;

            if (rating != null)
                return PayrollDataContext.AppraisalRatingScales.FirstOrDefault(x => x.RatingScaleID == rating);

            return null;
        }
        public static AppraisalRatingScale GetFormReviewRating(int formId)
        {
            if (formId == 0)
                return null;

            int? rating = PayrollDataContext.AppraisalForms.FirstOrDefault(x => x.AppraisalFormID
                == formId).ReviewRatingScaleRef_ID;

            if (rating != null)
                return PayrollDataContext.AppraisalRatingScales.FirstOrDefault(x => x.RatingScaleID == rating);

            return null;
        }
        public static List<AppraisalEmployeeFormQuestionnaire> GetEmployeeQuestionList(int employeeFormId)
        {
            return
                PayrollDataContext.AppraisalEmployeeFormQuestionnaires.Where(x => x.AppraisalEmployeeFormRef_ID
                            == employeeFormId).ToList();
        }

        public static double? GetRatingScore(AppraisalRatingScale ratingScale, double? ratingIndex)
        {
            return ratingIndex;
            //if (ratingScale == null)
            //    return ratingIndex;

            //List<AppraisalRatingScaleLine> list = ratingScale.AppraisalRatingScaleLines.OrderBy(x => x.Score)
            //    .ToList();


            //if (ratingIndex - 1 < 0)
            //    return 0;

            //return list[ratingIndex.Value - 1].Score.Value;
        }

        public static int GetRatingIndex(AppraisalRatingScale ratingScale, double? score)
        {
            if (score == null || ratingScale == null)
                return 0;



            List<AppraisalRatingScaleLine> list = ratingScale.AppraisalRatingScaleLines.OrderBy(x => x.Score)
               .ToList();

            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].Score == score)
                    return i + 1;
            }

            return 0;
        }

        public static decimal GetFormSectionDistributionPercent(AppraisalForm form, AppraisalBlockType blockType)
        {
            List<AppraisalFormDistribution> dist = PayrollDataContext.AppraisalFormDistributions.Where(x => x.AppraisalFormRef_ID == form.AppraisalFormID).ToList();
            // for new case when distributed is saved in the table
            if (dist.Any())
            {
                AppraisalFormDistribution block = dist.FirstOrDefault(x => x.BlockType == (int)blockType);
                if (block != null)
                    return block.Percent;

                return 0;
            }
            // for old case when distribution exists in ApppraisalForm table itself, in old case percent will exists for 4 block only
            else
            {
                if (blockType == AppraisalBlockType.Activity)
                    return form.PerformanceActivityValue == null ? 0 : form.PerformanceActivityValue.Value;
                if (blockType == AppraisalBlockType.Competency)
                    return form.PerformanceCoreValue == null ? 0 : form.PerformanceCoreValue.Value;
                if (blockType == AppraisalBlockType.Question)
                    return form.PerformanceQuestionnaireValue == null ? 0 : form.PerformanceQuestionnaireValue.Value;
                if (blockType == AppraisalBlockType.Target)
                    return form.PerformanceTargetValue == null ? 0 : form.PerformanceTargetValue.Value;

            }

            return 0;
        }

        public static Status SaveUpdateEmployeeCompetency(int employeeFormId,
            List<AppraisalValue> competencyList)
        {
            Status status = new Status();
            bool recordExists = PayrollDataContext.AppraisalEmployeeFormCompetencies.Any(x => x.AppraisalEmployeeFormRef_ID
                    == employeeFormId);


            AppraisalEmployeeForm empForm = GetEmployeeAppraisaleForm(employeeFormId);
            AppraisalForm form = empForm.AppraisalForm;
            //AppraisalRatingScale ratingScale = GetRatingScaleById(empForm.AppraisalForm.CompentencyRatingScaleRef_ID.Value);
            AppraisalRatingScaleLine smallestRatingScore =
                (
                    from l in PayrollDataContext.AppraisalRatingScaleLines
                    where l.RatingScaleRefID == empForm.AppraisalForm.CompentencyRatingScaleRef_ID.Value
                    orderby l.Score
                    select l
                ).FirstOrDefault();
            AppraisalRatingScaleLine highestRatingScore =
               (
                   from l in PayrollDataContext.AppraisalRatingScaleLines
                   where l.RatingScaleRefID == empForm.AppraisalForm.CompentencyRatingScaleRef_ID.Value
                   orderby l.Score descending
                   select l
               ).FirstOrDefault();

            if (recordExists == false)
            {
                List<AppraisalEmployeeFormCompetency> dbCompetencies = new List<AppraisalEmployeeFormCompetency>();

                foreach (AppraisalValue value in competencyList)
                {
                    dbCompetencies.Add
                        (
                            new AppraisalEmployeeFormCompetency
                            {
                                CompetencyRef_ID = value.ComptencyId,
                                AppraisalEmployeeFormRef_ID = employeeFormId,
                                SelfRating = value.SelfRating == null ? 0 : value.SelfRating.Value,
                                SelfComment = value.SelfComment
                            }
                        );
                }

                PayrollDataContext.AppraisalEmployeeFormCompetencies.InsertAllOnSubmit(dbCompetencies);

            }
            else
            {

                List<AppraisalEmployeeFormCompetency> dbCompetencies
                    = PayrollDataContext.AppraisalEmployeeFormCompetencies.Where(x => x.AppraisalEmployeeFormRef_ID
                        == employeeFormId).ToList();

                foreach (AppraisalEmployeeFormCompetency dbCompetency in dbCompetencies)
                {
                    AppraisalValue value = competencyList.FirstOrDefault(x => x.ComptencyId == dbCompetency.CompetencyRef_ID);
                    if (value != null)
                    {
                        if (empForm.Status == null || empForm.Status == 0)
                        {
                            dbCompetency.SelfComment = value.SelfComment;
                            dbCompetency.SelfRating = value.SelfRating == null ? 0 : value.SelfRating.Value;
                        }
                        else if (empForm.Status == (int)AppraisalStatus.SaveAndSend)
                        {
                            dbCompetency.ManagerComment = value.SupervisorComment;
                            dbCompetency.ManagerRating = value.SupervisorRating == null ? 0 : value.SupervisorRating.Value;
                        }
                    }
                }
            }

            double totalNewWeight = 0;

            decimal? PerformanceCoreValue = GetFormSectionDistributionPercent(form, AppraisalBlockType.Competency);

            // Calculate Total Points
            if (empForm.Status == null || empForm.Status == 0)
            {
                empForm.EmployeeCompetencyScore = competencyList.Sum(x => x.SelfRating);

                if (smallestRatingScore != null && smallestRatingScore.Score == 0 && smallestRatingScore.SkipInPointCalculation != null && smallestRatingScore.SkipInPointCalculation.Value)
                {
                    totalNewWeight = highestRatingScore.Score.Value * competencyList.Count(x => x.SelfRating != smallestRatingScore.Score);
                }
                else if (highestRatingScore != null)
                {
                    totalNewWeight = highestRatingScore.Score.Value * competencyList.Count;
                }

                empForm.EmployeeCompetencyTotalScore = totalNewWeight;

                if (empForm.EmployeeCompetencyTotalScore != 0)
                {

                    // form.PerformanceCoreValue == null ? 0 : form.PerformanceCoreValue.Value;

                    empForm.FinalEmpCompetencyScore = (empForm.EmployeeCompetencyScore / empForm.EmployeeCompetencyTotalScore) *
                        (double)(PerformanceCoreValue / 100) * highestRatingScore.Score;

                    empForm.EmployeeCompetencyTotalScore = (empForm.EmployeeCompetencyScore / empForm.EmployeeCompetencyTotalScore) *
                                            highestRatingScore.Score;
                }
                else
                    empForm.FinalEmpCompetencyScore = 0;

                empForm.FinalEmpCompetencyScore = double.Parse(empForm.FinalEmpCompetencyScore.Value.ToString("n2"));
            }
            else if (empForm.Status == (int)AppraisalStatus.SaveAndSend && smallestRatingScore != null && highestRatingScore != null)
            {
                empForm.SupervisorCompetencyScore = competencyList.Sum(x => x.SupervisorRating);

                if (smallestRatingScore != null && smallestRatingScore.Score == 0 && smallestRatingScore.SkipInPointCalculation != null && smallestRatingScore.SkipInPointCalculation.Value)
                {
                    totalNewWeight = highestRatingScore.Score.Value * competencyList.Count(x => x.SupervisorRating != smallestRatingScore.Score);
                }
                else if (highestRatingScore != null)
                {
                    totalNewWeight = highestRatingScore.Score.Value * competencyList.Count;
                }

                empForm.SupervisorCompetencyTotalScore = totalNewWeight;

                if (empForm.SupervisorCompetencyTotalScore != 0)
                {
                    empForm.FinalSupCompetencyScore = (empForm.SupervisorCompetencyScore / empForm.SupervisorCompetencyTotalScore) *
                       (double)(PerformanceCoreValue / 100) * highestRatingScore.Score;

                    empForm.SupervisorCompetencyTotalScore = (empForm.SupervisorCompetencyScore / empForm.SupervisorCompetencyTotalScore) *
                            highestRatingScore.Score;
                }
                else
                {
                    empForm.FinalSupCompetencyScore = 0;
                }
                empForm.FinalSupCompetencyScore = double.Parse(empForm.FinalSupCompetencyScore.Value.ToString("n2"));
            }

            empForm.IsCompetencySaved = true;
            PayrollDataContext.SubmitChanges();

            double? empScore = 0, supScore = 0;
            PayrollDataContext.AppraisalCalculateRating(employeeFormId, form.AppraisalFormID, ref empScore, ref supScore);
            PayrollDataContext.SubmitChanges();


            return status;
        }

        public static List<GetEmployeeIncomeListForAppraisalResult> GetEmployeeIncomesForAppraisals(int employeeId)
        {
            if (PayrollDataContext.AppraisalIncomes.Any() == false)
                return null;

            List<GetEmployeeIncomeListForAppraisalResult> list = PayrollDataContext.GetEmployeeIncomeListForAppraisal
                (employeeId).ToList();

            return list;
        }

        public static Status SaveUpdateEmployeeTarget(int employeeFormId,
            List<AppraisalValue> competencyList)
        {
            Status status = new Status();
            //bool recordExists = PayrollDataContext.AppraisalEmployeeFormTargets.Any(x => x.AppraisalEmployeeFormRef_ID
            //        == employeeFormId);

            AppraisalEmployeeForm empForm = GetEmployeeAppraisaleForm(employeeFormId);
            AppraisalForm form = empForm.AppraisalForm;
            //AppraisalRatingScale ratingScale = GetRatingScaleById(empForm.AppraisalForm.CompentencyRatingScaleRef_ID.Value);



            int ratingscoreId = 0;

            if (empForm.AppraisalForm.CompentencyRatingScaleRef_ID != null)
                ratingscoreId = empForm.AppraisalForm.CompentencyRatingScaleRef_ID.Value;
            else if (empForm.AppraisalForm.QuestionnareRatingScaleRef_ID != null)
                ratingscoreId = empForm.AppraisalForm.QuestionnareRatingScaleRef_ID.Value;
            else if (empForm.AppraisalForm.TargetRatingScaleRef_ID != null)
                ratingscoreId = empForm.AppraisalForm.TargetRatingScaleRef_ID.Value;

            AppraisalRatingScale ratingScale = GetRatingScaleById(ratingscoreId);

            // get competency or questionnaire rating scale as Target do not have rating scale association
            AppraisalRatingScaleLine highestRatingScore = new AppraisalRatingScaleLine();
            highestRatingScore.Score = 1;

            if (ratingscoreId != 0)
            {
                highestRatingScore =
                    (
                        from l in PayrollDataContext.AppraisalRatingScaleLines
                        where l.RatingScaleRefID == ratingscoreId
                        orderby l.Score descending
                        select l
                    ).FirstOrDefault();
            }



            List<AppraisalEmployeeFormTarget> dbTargets
                = PayrollDataContext.AppraisalEmployeeFormTargets.Where(x => x.AppraisalEmployeeFormRef_ID
                    == employeeFormId).ToList();

            foreach (AppraisalEmployeeFormTarget dbCompetency in dbTargets)
            {
                AppraisalValue value = competencyList.FirstOrDefault(x => x.TargetID == dbCompetency.TargetRef_ID);
                if (value != null)
                {
                    if (empForm.Status == null || empForm.Status == 0)
                    {
                        dbCompetency.SelfComment = value.SelfComment;
                        dbCompetency.SelfRating = GetRatingScore(ratingScale, value.SelfRating);
                        //dbCompetency.SelfAchievementValue = value.TargetAchievement;
                    }
                    else if (empForm.Status == (int)AppraisalStatus.SaveAndSend)
                    {
                        dbCompetency.ManagerComment = value.SupervisorComment;
                        dbCompetency.ManagerRating = GetRatingScore(ratingScale, value.SupervisorRating);
                    }
                }
            }

            decimal? PerformanceTargetValue = GetFormSectionDistributionPercent(form, AppraisalBlockType.Target);

            // if rating based score
            if (form.TargetHideAssignedShowRating != null && form.TargetHideAssignedShowRating.Value)
            {
                if (empForm.Status == null || empForm.Status == 0)
                {
                    empForm.TargetScore = dbTargets.Sum(x => (x.SelfRating == null ? 0 : x.SelfRating.Value) * (x.Weight == null ? 0 : x.Weight.Value));
                    empForm.TargetScore = empForm.TargetScore / 100;
                    empForm.FinalTargetScore = empForm.TargetScore * ((double)(PerformanceTargetValue / 100));
                }
                else
                {
                    empForm.SupervisorTargetScore = dbTargets.Sum(x => (x.ManagerRating == null ? 0 : x.ManagerRating.Value) * (x.Weight == null ? 0 : x.Weight.Value));
                    empForm.SupervisorTargetScore = empForm.SupervisorTargetScore / 100;
                    empForm.FinalSupervisorTargetScore = empForm.SupervisorTargetScore * ((double)(PerformanceTargetValue / 100));
                }
            }
            // Calculate Total Points
            else if (empForm.Status == null || empForm.Status == 0)
            {
                double? totalAssignedTarget = dbTargets.Sum(x => x.AssignedTargetValue); ;
                double? totalAchievement = dbTargets.Sum(x => x.SelfAchievementValue);

                empForm.TargetScore = GetDishhomeTargetScore(totalAssignedTarget, totalAchievement);
                empForm.FinalTargetScore = empForm.TargetScore * ((double)(PerformanceTargetValue / 100));
                empForm.FinalTargetScore = double.Parse(empForm.FinalTargetScore.Value.ToString("n2"));

                empForm.SupervisorTargetScore = empForm.TargetScore;
                empForm.FinalSupervisorTargetScore = empForm.FinalTargetScore;
            }

            empForm.IsTargetSaved = true;
            PayrollDataContext.SubmitChanges();

            double? empScore = 0, supScore = 0;
            PayrollDataContext.AppraisalCalculateRating(employeeFormId, form.AppraisalFormID, ref empScore, ref supScore);
            PayrollDataContext.SubmitChanges();

            return status;
        }

        /// <summary>
        /// Target update after form rollout and form submission, so function created to calculated
        /// target points and overall points, works for DH like apprisal where not rating used,
        /// calculation will be done by assinged target and achievement import
        /// </summary>
        /// <param name="empid"></param>
        public static void DishHomeTempFunctionToUpdateTargetAndReCalculationPoints(int appraisanEmpFormID)
        {

            List<AppraisalEmployeeForm> list = PayrollDataContext.AppraisalEmployeeForms
                .Where(x => appraisanEmpFormID == 0 || appraisanEmpFormID == x.AppraisalEmployeeFormID).ToList();

            //List<AppraisalEmployeeForm> list = PayrollDataContext.AppraisalEmployeeForms
            //    .Where(x => x.Status != (int)AppraisalStatus.Saved).ToList();




            foreach (var item in list)
            {

                AppraisalEmployeeForm empForm = item;



                AppraisalForm form = empForm.AppraisalForm;

                decimal? PerformanceTargetValue = GetFormSectionDistributionPercent(form, AppraisalBlockType.Target);

                List<AppraisalEmployeeFormTarget> dbTargets
                    = PayrollDataContext.AppraisalEmployeeFormTargets.Where(x => x.AppraisalEmployeeFormRef_ID
                        == empForm.AppraisalEmployeeFormID).ToList();


                // Calculate Total Points
                //if (empForm.Status == null || empForm.Status == 0)
                {

                    double? totalAssignedTarget = dbTargets.Sum(x => x.AssignedTargetValue); ;
                    double? totalAchievement = dbTargets.Sum(x => x.SelfAchievementValue);

                    //empForm.TargetScore = 



                    //if (totalAssignedTarget != 0)
                    //{

                    empForm.TargetScore = GetDishhomeTargetScore(totalAssignedTarget, totalAchievement);
                    empForm.SupervisorTargetScore = empForm.TargetScore;

                    empForm.FinalTargetScore = empForm.TargetScore * ((double)(PerformanceTargetValue / 100));
                    empForm.FinalSupervisorTargetScore = empForm.FinalTargetScore;
                    //(target / totalTarget) *
                    //(double)(form.PerformanceTargetValue / 100) * highestRatingScore.Score;
                    //}
                    //else
                    //    empForm.FinalTargetScore = 0;

                    empForm.FinalTargetScore = double.Parse(empForm.FinalTargetScore.Value.ToString("n2"));
                    empForm.FinalSupervisorTargetScore = empForm.FinalTargetScore;
                }


                PayrollDataContext.SubmitChanges();

                double? empScore = 0, supScore = 0;
                PayrollDataContext.AppraisalCalculateRating(empForm.AppraisalEmployeeFormID, form.AppraisalFormID, ref empScore, ref supScore);
                //PayrollDataContext.SubmitChanges();
            }

        }

        /// <summary>
        /// Target update after form rollout and form submission, so function created to calculated
        /// target points and overall points, works for DH like apprisal where not rating used,
        /// calculation will be done by assinged target and achievement import
        /// </summary>
        /// <param name="empid"></param>
        public static void TempFunctionToUpdateTargetAndReCalculationPoints(int appraisanEmpFormID)
        {

            List<AppraisalEmployeeForm> list = PayrollDataContext.AppraisalEmployeeForms
                .Where(x => appraisanEmpFormID == 0 || appraisanEmpFormID == x.AppraisalEmployeeFormID).ToList();

            //List<AppraisalEmployeeForm> list = PayrollDataContext.AppraisalEmployeeForms
            //    .Where(x => x.Status != (int)AppraisalStatus.Saved).ToList();

            foreach (var item in list)
            {

                AppraisalEmployeeForm empForm = item;


                AppraisalForm form = empForm.AppraisalForm;

                //List<AppraisalEmployeeFormTarget> dbTargets
                //    = PayrollDataContext.AppraisalEmployeeFormTargets.Where(x => x.AppraisalEmployeeFormRef_ID
                //        == empForm.AppraisalEmployeeFormID).ToList();


                //// Calculate Total Points
                ////if (empForm.Status == null || empForm.Status == 0)
                //{

                //    double? totalAssignedTarget = dbTargets.Sum(x => x.AssignedTargetValue); ;
                //    double? totalAchievement = dbTargets.Sum(x => x.SelfAchievementValue);

                //    //empForm.TargetScore = 



                //    //if (totalAssignedTarget != 0)
                //    //{

                //    empForm.TargetScore = GetDishhomeTargetScore(totalAssignedTarget, totalAchievement);
                //    empForm.SupervisorTargetScore = empForm.TargetScore;

                //    empForm.FinalTargetScore = empForm.TargetScore * ((double)(form.PerformanceTargetValue / 100));
                //    empForm.FinalSupervisorTargetScore = empForm.FinalTargetScore;
                //    //(target / totalTarget) *
                //    //(double)(form.PerformanceTargetValue / 100) * highestRatingScore.Score;
                //    //}
                //    //else
                //    //    empForm.FinalTargetScore = 0;

                //    empForm.FinalTargetScore = double.Parse(empForm.FinalTargetScore.Value.ToString("n2"));
                //    empForm.FinalSupervisorTargetScore = empForm.FinalTargetScore;
                //}


                //PayrollDataContext.SubmitChanges();

                double? empScore = 0, supScore = 0;
                PayrollDataContext.AppraisalCalculateRating(empForm.AppraisalEmployeeFormID, form.AppraisalFormID, ref empScore, ref supScore);
                //PayrollDataContext.SubmitChanges();
            }

        }
        /*
         * 
         * The rating for the sales target and achievement should be as per the following.
 
 
Target Achievement
Rating
Below 70 %
1
70% -79%
2
80%-89%
2.5
90%-99%
3
100%-109%
3.5
110%-119%
4
120%-129%
4.5
130% Above
5

         */
        public static double GetDishhomeTargetScore(double? assignedTarget, double? achievement)
        {

            if (assignedTarget == null)
                assignedTarget = 0;

            if (achievement == null)
                achievement = 0;

            if (assignedTarget == 0)
                return 0;

            double score = (achievement.Value / assignedTarget.Value) * 100;

            score = double.Parse(score.ToString("n2"));

            if (score < 70)
                return 1;
            if (score < 80)
                return 2;
            if (score < 90)
                return 2.5;
            if (score < 100)
                return 3;
            if (score < 110)
                return 3.5;
            if (score < 120)
                return 4;
            if (score < 130)
                return 4.5;
            // else greater than 130% or above 
            //if (score < 79)
            return 5;

            //return score;
        }

        public static void CreateNewForm(string name, int sourceFormId)
        {
            AppraisalForm form = new AppraisalForm();
            form.Name = name;
            form.Description = "";

            NewHRManager.InsertUpdateAppraisalForm_GeneralSetting(form, true);

            CopyForm(sourceFormId, form.AppraisalFormID, true, true, true, true, true, true, true, true, true, true);
        }

        public static Status CopyForm(int sourceFormId, int destFormId, bool copyIntroduction,
            bool copyObjective, bool copyperformance, bool copyActivitySummary, bool copyCompetencies, bool copyTargets,
            bool copyQuestionnaire, bool copySupervisorReview, bool copyComments, bool copySignature)
        {
            Status status = new Status();

            if (PayrollDataContext.AppraisalEmployeeForms.Any(x => x.AppraisalFormRef_ID == destFormId))
            {
                status.ErrorMessage = "Rollout form can not be changed.";
                return status;
            }

            AppraisalForm sourceForm = getFormInstanceByID(sourceFormId);
            AppraisalForm destForm = getFormInstanceByID(destFormId);

            destForm.HideRecommendBlock = sourceForm.HideRecommendBlock;

            if (copyIntroduction)
            {
                destForm.IntroductionName = sourceForm.IntroductionName;
                destForm.IntroductionDescription = sourceForm.IntroductionDescription;
            }
            if (copyObjective)
            {
                destForm.ObjectiveName = sourceForm.ObjectiveName;
                destForm.ObjectiveDescription = sourceForm.ObjectiveDescription;
                destForm.HideObjectiveBlock = sourceForm.HideObjectiveBlock;
            }
            if (copyperformance)
            {
                destForm.PerformanceSummaryName = sourceForm.PerformanceSummaryName;
                destForm.PerformanceSummaryDescription = sourceForm.PerformanceSummaryDescription;

                destForm.PerformanceActivityValue = sourceForm.PerformanceActivityValue;
                destForm.PerformanceCoreValue = sourceForm.PerformanceCoreValue;
                destForm.PerformanceTargetValue = sourceForm.PerformanceTargetValue;
                destForm.PerformanceQuestionnaireValue = sourceForm.PerformanceQuestionnaireValue;

                PayrollDataContext.AppraisalFormGradings.DeleteAllOnSubmit(
                    PayrollDataContext.AppraisalFormGradings.Where(x => x.AppraisalFormRef_ID == destForm.AppraisalFormID));

                foreach (AppraisalFormGrading item in PayrollDataContext.AppraisalFormGradings
                    .Where(x => x.AppraisalFormRef_ID == sourceForm.AppraisalFormID))
                {
                    AppraisalFormGrading newItem = new AppraisalFormGrading();
                    CopyObject<AppraisalFormGrading>(item, ref newItem, "");
                    newItem.AppraisalFormRef_ID = destFormId;

                    PayrollDataContext.AppraisalFormGradings.InsertOnSubmit(newItem);
                }
            }

            if (copyActivitySummary)
            {
                destForm.ActivityName = sourceForm.ActivityName;
                destForm.ActivityDescription = sourceForm.ActivityDescription;

                destForm.ActivityRatingScaleRef_ID = sourceForm.ActivityRatingScaleRef_ID;
                destForm.AllowActivitySelfRating = sourceForm.AllowActivitySelfRating;

                destForm.ActivityMarkingType = sourceForm.ActivityMarkingType;
                destForm.ActivityMaximumMarks = sourceForm.ActivityMaximumMarks;

                destForm.HideActivitySummary = sourceForm.HideActivitySummary;

            }

            if (copyCompetencies)
            {
                destForm.CompentencyName = sourceForm.CompentencyName;
                destForm.CompentencyDescription = sourceForm.CompentencyDescription;

                destForm.CompentencyRatingScaleRef_ID = sourceForm.CompentencyRatingScaleRef_ID;
                destForm.AllowCompetencySelfRating = sourceForm.AllowCompetencySelfRating;
                destForm.AllowCompetencySelfComment = sourceForm.AllowCompetencySelfComment;

                destForm.AppraisalFormCompetencies.Clear();

                foreach (AppraisalFormCompetency item in sourceForm.AppraisalFormCompetencies.OrderBy(x => x.Sequence))
                {
                    AppraisalFormCompetency newItem = new AppraisalFormCompetency();
                    CopyObject<AppraisalFormCompetency>(item, ref newItem, "");

                    destForm.AppraisalFormCompetencies.Add(newItem);
                }

                destForm.AppraisalFormCompetencyCategories.Clear();

                foreach (AppraisalFormCompetencyCategory item in sourceForm.AppraisalFormCompetencyCategories.OrderBy(x => x.Sequence))
                {
                    AppraisalFormCompetencyCategory newItem = new AppraisalFormCompetencyCategory();
                    CopyObject<AppraisalFormCompetencyCategory>(item, ref newItem, "");

                    destForm.AppraisalFormCompetencyCategories.Add(newItem);
                }
            }


            if (copyTargets)
            {
                destForm.TargetName = sourceForm.TargetName;
                destForm.TargetDescription = sourceForm.TargetDescription;
                destForm.HideTarget = sourceForm.HideTarget;

                destForm.TargetRatingScaleRef_ID = sourceForm.TargetRatingScaleRef_ID;
                destForm.AllowTargetSelfRating = sourceForm.AllowTargetSelfRating;
                destForm.AllowTargetSelfComment = sourceForm.AllowTargetSelfComment;
                destForm.TargetHideAssignedShowRating = sourceForm.TargetHideAssignedShowRating;

                destForm.AppraisalFormTargets.Clear();

                foreach (AppraisalFormTarget item in sourceForm.AppraisalFormTargets.OrderBy(x => x.Sequence))
                {
                    AppraisalFormTarget newItem = new AppraisalFormTarget();
                    CopyObject<AppraisalFormTarget>(item, ref newItem, "");

                    destForm.AppraisalFormTargets.Add(newItem);
                }


            }

            if (copyQuestionnaire)
            {
                destForm.QuestionnareName = sourceForm.QuestionnareName;
                destForm.QuestionnareDescription = sourceForm.QuestionnareDescription;

                destForm.QuestionnareRatingScaleRef_ID = sourceForm.QuestionnareRatingScaleRef_ID;
                destForm.AllowQuestionSelfRating = sourceForm.AllowQuestionSelfRating;
                destForm.AllowQuestionSelfComment = sourceForm.AllowQuestionSelfComment;

                destForm.AppraisalFormQuestionnaireCategories.Clear();
                destForm.AppraisalFormQuestionnaires.Clear();

                foreach (AppraisalFormQuestionnaireCategory itemCategory in sourceForm.AppraisalFormQuestionnaireCategories)
                {
                    AppraisalFormQuestionnaireCategory newCategory = new AppraisalFormQuestionnaireCategory();
                    CopyObject<AppraisalFormQuestionnaireCategory>(itemCategory, ref newCategory, "AppraisalFormRef_ID", "CategoryID");

                    newCategory.AppraisalFormRef_ID = destFormId;
                    destForm.AppraisalFormQuestionnaireCategories.Add(newCategory);


                    foreach (AppraisalFormQuestionnaire itemQuestion in sourceForm.AppraisalFormQuestionnaires
                        .Where(x => x.CategoryID == itemCategory.CategoryID))
                    {
                        AppraisalFormQuestionnaire newItem = new AppraisalFormQuestionnaire();
                        CopyObject<AppraisalFormQuestionnaire>(itemQuestion, ref newItem, "QuestionnareID", "AppraisalFormRef_ID", "CategoryID");

                        newItem.AppraisalFormRef_ID = destFormId;

                        newCategory.AppraisalFormQuestionnaires.Add(newItem);



                    }
                }




            }

            if (copySupervisorReview)
            {
                destForm.ReviewName = sourceForm.ReviewName;
                destForm.ReviewDescription = sourceForm.ReviewDescription;


                destForm.HideSupervisorReview = sourceForm.HideSupervisorReview;

                destForm.AppraisalFormReviews.Clear();

                foreach (AppraisalFormReview item in sourceForm.AppraisalFormReviews)
                {
                    AppraisalFormReview newItem = new AppraisalFormReview();
                    CopyObject<AppraisalFormReview>(item, ref newItem, "");

                    destForm.AppraisalFormReviews.Add(newItem);
                }

            }

            if (copyComments)
            {
                destForm.SummaryName = sourceForm.SummaryName;
                destForm.SummaryDescription = sourceForm.SummaryDescription;
            }
            if (copySignature)
            {
                destForm.SignationName = sourceForm.SignationName;
                destForm.SignationDescription = sourceForm.SignationDescription;
            }

            PayrollDataContext.SubmitChanges();
            return status;
        }

        #region "Appraisal saving/updating by self employee & other employee

        public static Status SaveEmployeeAppraisalForm(int employeeFormId, int recommendForId, string comments, bool skipCommentSaving)
        {
            Status status = new Status();



            AppraisalEmployeeForm empForm = GetEmployeeAppraisaleForm(employeeFormId);
            AppraisalForm form = getFormInstanceByID(empForm.AppraisalFormRef_ID);

            int currentStatus = empForm.Status.Value;

            AppraisalRollout rollout = GetRolloutByFormID(empForm.RolloutID.Value);
            GetDocumentNextStepUsingApprovalFlowResult nextStep = CommonManager.GetDocumentNextStep(
                (int)empForm.Status, FlowTypeEnum.Appraisal, empForm.EmployeeId, rollout.RolloutID, false);


            if (skipCommentSaving == false)
            {
                AppraisalEmployeeSummaryComment dbComment = new AppraisalEmployeeSummaryComment();
                dbComment.AppraisalEmployeeFormRef_ID = employeeFormId;
                dbComment.CommentedEmployeeId = SessionManager.CurrentLoggedInEmployeeId;

                if (empForm.EmployeeId == SessionManager.CurrentLoggedInEmployeeId && empForm.Status == (int)AppraisalStatus.Saved)
                    dbComment.CommenterDisplayTitle = EmployeeManager.GetEmployeeName(empForm.EmployeeId);
                else
                    dbComment.CommenterDisplayTitle = nextStep.AuthorityName;
                dbComment.CommentedOn = GetCurrentDateAndTime();
                dbComment.StatusStepID = nextStep.NextStepID;
                dbComment.Comment = comments;
                if (recommendForId != -1 && recommendForId != 0)
                {
                    dbComment.RecommendForId = recommendForId;
                    dbComment.Type = (int)AppraisalSummaryCommentTypeEnum.HasRecommendation;
                }
                PayrollDataContext.AppraisalEmployeeSummaryComments.InsertOnSubmit(dbComment);
            }

            if (empForm.EmployeeId == SessionManager.CurrentLoggedInEmployeeId)
            {
                // process for first forward from the Employee to Supervisor
                if (empForm.Status == (int)AppraisalStatus.Saved)
                {
                    if (form.HideActivitySummary != null && form.HideActivitySummary.Value) { }
                    else
                    {
                        if (empForm.IsActivitySaved == null || empForm.IsActivitySaved == false)
                        {
                            status.ErrorMessage = "\"" + form.ActivityName + "\" must be saved before forwarding the appraisal.";
                            return status;
                        }
                    }

                    if (empForm.AppraisalEmployeeFormCompetencies.Count < form.AppraisalFormCompetencies.Count)
                    {
                        status.ErrorMessage = "\"" + form.CompentencyName + "\" must be saved before forwarding the appraisal.";
                        return status;
                    }

                    if (empForm.AppraisalEmployeeFormQuestionnaires.Count < form.AppraisalFormQuestionnaires.Count)
                    {
                        status.ErrorMessage = "\"" + form.QuestionnareName + "\" must be saved before forwarding the appraisal.";
                        return status;
                    }

                    empForm.SelfSubmittedOn = GetCurrentDateAndTime();
                    empForm.Status = (int)AppraisalStatus.SaveAndSend;
                    empForm.StatusText = nextStep.StepStatusName;



                }
                // process for third i agree/dis-agree forward from the Employee to Supervisor
                else if (empForm.Status == (int)AppraisalStatus.SupervisorManagerCommented)
                {
                    AppraisalEmployeeSummaryComment comment = AppraisalManager.GetAppraisalSelfEmployeeSummaryComment(employeeFormId);
                    if (comment == null)
                    {
                        status.ErrorMessage = "Please save the \"" + form.SummaryName + "\" before forwarding the appraisal";
                        return status;
                    }

                    empForm.Status = nextStep.NextStepID;
                    empForm.StatusText = nextStep.StepStatusName;
                    empForm.SelfCommentedOn = GetCurrentDateAndTime();
                }
                else
                {
                    status.ErrorMessage = "Employee can not submit the form in this status.";
                    return status;
                }
            }
            // process for second step submission from Supervisor
            else if (empForm.Status == (int)AppraisalStatus.SaveAndSend)
            {

                if (form.HideActivitySummary != null && form.HideActivitySummary.Value) { }
                else
                {
                    if (empForm.ActivitySupervisorRating == null && form.ActivityMarkingType != 0)
                    {
                        status.ErrorMessage = "\"" + form.ActivityName + "\" marking must be saved before forwarding the appraisal.";
                        return status;
                    }
                }

                if (empForm.AppraisalEmployeeFormCompetencies.Where(x => x.ManagerRating != null).Count()
                    < form.AppraisalFormCompetencies.Count)
                {
                    status.ErrorMessage = "All \"" + form.CompentencyName + "\" must be saved before forwarding the appraisal.";
                    return status;
                }

                if (empForm.AppraisalEmployeeFormQuestionnaires.Where(x => x.ManagerRating != null).Count()
                    < form.AppraisalFormQuestionnaires.Count)
                {
                    status.ErrorMessage = "All \"" + form.QuestionnareName + "\" must be saved before forwarding the appraisal.";
                    return status;
                }

                if (empForm.AppraisalEmployeeFormReviews.Where(x => string.IsNullOrEmpty(x.ManagerComment) == false)
                    .ToList().Count < form.AppraisalFormReviews.Count)
                {
                    status.ErrorMessage = "All \"" + form.ReviewName + "\" with comment must be saved before forwarding the appraisal.";
                    return status;
                }

                ///----bocked as this section is optional
                //if(form.HideHistorySection == false && empForm.HistoryManagerQualificationRating == null)
                //{
                //    status.ErrorMessage = "Rating must be saved for \"" + form.HistoryName + "\" before forwarding the appraisal.";
                //    return status;
                //}

                empForm.SupervisorEmployeeId = SessionManager.CurrentLoggedInEmployeeId;
                empForm.SupervisorSubmittedOn = GetCurrentDateAndTime();
                empForm.Status = nextStep.NextStepID;
                empForm.StatusText = nextStep.StepStatusName;


                // calcuate Score in this stage

                //empForm.SelfEmployeeRatingScore = employeeScore;
                //empForm.SupervisorRatingScore = supervisorScore;

            }


            // new employee 
            if (nextStep != null)
            {
                empForm.CurrentApprovalSelectedEmployeeId = nextStep.ApprovalEmployee1;
            }

            if (nextStep != null)
                empForm.NextStepOrStatusId = nextStep.NextNextStepID;

            PayrollDataContext.SubmitChanges();


            #region Send Message and Email
            if (empForm.Status == (int)AppraisalStatus.SaveAndSend && nextStep != null)
            {
                if (nextStep.ApprovalEmployee1 != null)
                    SendAppraisalMailAndMessage(EmailContentType.AppraisalSupervisor, nextStep.ApprovalEmployee1.Value, null, empForm);



            }
            else if (empForm.Status == (int)AppraisalStatus.SupervisorManagerCommented && nextStep != null)
            {

                SendAppraisalMailAndMessage(EmailContentType.AppraisalEmployeeReview, empForm.EmployeeId, null, empForm);

                //if (nextStep.ApprovalEmployee2 != null)
                //    SendAppraisalMailAndMessage(EmailContentType.AppraisalSupervisor, nextStep.ApprovalEmployee2.Value, null, empForm);

            }
            else if (empForm.Status == (int)AppraisalStatus.EmployeeReviewed && nextStep != null)
            {
                if (nextStep.ApprovalEmployee1 != null)
                    SendAppraisalMailAndMessage(EmailContentType.AppraisalHigherLevel, nextStep.ApprovalEmployee1.Value, null, empForm);



            }
            #endregion

            //if (currentStatus == (int)AppraisalStatus.Saved || currentStatus == (int)AppraisalStatus.SaveAndSend)
            //{
            //    double? employeeScore = 0, supervisorScore = 0;

            //    PayrollDataContext.AppraisalCalculateRatingForMega(
            //        empForm.AppraisalEmployeeFormID, empForm.AppraisalFormRef_ID,
            //        ref employeeScore, ref supervisorScore);
            //}

            BLL.BaseBiz.Dispose();

            return status;
        }

        //public static bool HasAdditionalSteps(int stepid,FlowTypeEnum type,int appraisalEmployeeFormId, AppraisalAdditionalStep block)
        //{
        //    if (PayrollDataContext.AppraisalEmployeeFlows.Any(x => x.AppraisalEmployeeFormRef_ID == appraisalEmployeeFormId))
        //    {
        //        AppraisalEmployeeFlow flow =
        //            PayrollDataContext.AppraisalEmployeeFlows.FirstOrDefault
        //            (x => x.AppraisalEmployeeFormRef_ID == appraisalEmployeeFormId && x.StepRef_ID == stepid);

        //        if (flow != null && flow.ShowAdditionalStep != null && flow.ShowAdditionalStep.Value == (int)block)
        //            return true;

        //    }

        //    ApprovalFlow flow1 =  PayrollDataContext.ApprovalFlows.FirstOrDefault(x => x.FlowType == (int)type
        //        && x.StepID == stepid);
        //    if (flow1 != null && flow1.ShowAdditionalStep != null && flow1.ShowAdditionalStep.Value == (int)block)
        //        return true;

        //    return false;
        //}
        public static int? GetAdditionalSteps(int stepid, FlowTypeEnum type, int appraisalEmployeeFormId)
        {
            if (PayrollDataContext.AppraisalEmployeeFlows.Any(x => x.AppraisalEmployeeFormRef_ID == appraisalEmployeeFormId))
            {
                AppraisalEmployeeFlow flow =
                    PayrollDataContext.AppraisalEmployeeFlows.FirstOrDefault
                    (x => x.AppraisalEmployeeFormRef_ID == appraisalEmployeeFormId && x.StepRef_ID == stepid);

                if (flow != null && flow.ShowAdditionalStep != null)// && flow.ShowAdditionalStep.Value == (int)block)
                    return flow.ShowAdditionalStep.Value;

            }

            ApprovalFlow flow1 = PayrollDataContext.ApprovalFlows.FirstOrDefault(x => x.FlowType == (int)type
               && x.StepID == stepid);
            if (flow1 != null && flow1.ShowAdditionalStep != null)// && flow1.ShowAdditionalStep.Value == (int)block)
                return flow1.ShowAdditionalStep.Value;

            return null;
        }

        //public static bool HasAddMarksBlock(int stepid, FlowTypeEnum type, int appraisalEmployeeFormId)
        //{
        //    if (PayrollDataContext.AppraisalEmployeeFlows.Any(x => x.AppraisalEmployeeFormRef_ID == appraisalEmployeeFormId))
        //    {
        //        AppraisalEmployeeFlow flow =
        //            PayrollDataContext.AppraisalEmployeeFlows.FirstOrDefault
        //            (x => x.AppraisalEmployeeFormRef_ID == appraisalEmployeeFormId && x.StepRef_ID == stepid);

        //        if (flow != null && flow.ShowAdditionalStep != null && flow.ShowAdditionalStep.Value == (int)AppraisalAdditionalStep.AddAdditionPoints)
        //            return true;

        //    }

        //    ApprovalFlow flow1 = PayrollDataContext.ApprovalFlows.FirstOrDefault(x => x.FlowType == (int)type
        //       && x.StepID == stepid);
        //    if (flow1 != null && flow1.ShowAdditionalStep != null && flow1.ShowAdditionalStep.Value == (int)AppraisalAdditionalStep.AddAdditionPoints)
        //        return true;

        //    return false;
        //}

        //public static void CalculateMarks(int employeeFormId)
        //{
        //    double? employeeScore = 0, supervisorScore = 0;
        //    AppraisalEmployeeForm empForm = GetEmployeeAppraisaleForm(employeeFormId);

        //    PayrollDataContext.AppraisalCalculateRatingForMega(
        //        empForm.AppraisalEmployeeFormID, empForm.AppraisalFormRef_ID,
        //        ref employeeScore, ref supervisorScore);

        //    BLL.BaseBiz.Dispose();

        //}

        public static void UpdateEmployeeActivitySummary(int employeeFormId, string html,
      double? selfRating, double? supervisorRating, string supervisorComment, AppraisalEmployeeForm objUploadFileInfo)
        {
            AppraisalEmployeeForm empForm = GetEmployeeAppraisaleForm(employeeFormId);


            AppraisalRatingScale ratingScale = GetRatingScaleById(empForm.AppraisalForm.ActivityRatingScaleRef_ID);


            if (empForm.Status == null || empForm.Status == 0)
            {
                empForm.ActivitySummary = html;
                empForm.ActivitySelfRating = selfRating;//GetRatingScore(ratingScale, selfRating);
            }
            else if (empForm.Status == (int)AppraisalStatus.SaveAndSend)
            {
                empForm.ActivitySupervisorRating = supervisorRating;// GetRatingScore(ratingScale, supervisorRating);
                empForm.ActivitySupervisorComment = supervisorComment;
            }

            if (objUploadFileInfo.ActivitySummaryFileName != null)
                empForm.ActivitySummaryFileName = objUploadFileInfo.ActivitySummaryFileName;

            if (objUploadFileInfo.ActivitySummaryFileUrl != null)
                empForm.ActivitySummaryFileUrl = objUploadFileInfo.ActivitySummaryFileUrl;

            if (objUploadFileInfo.FileFormat != null)
                empForm.FileFormat = objUploadFileInfo.FileFormat;

            if (objUploadFileInfo.FileType != null)
                empForm.FileType = objUploadFileInfo.FileType;

            empForm.ActivitySummaryServerFileName = objUploadFileInfo.ActivitySummaryServerFileName;
            empForm.IsActivitySaved = true;
            PayrollDataContext.SubmitChanges();
        }

        //public static void UpdateEmployeeQualificationEducationRating(int employeeFormId, double educationRating,double transferRating, double workHistoryRating)
        //{
        //    AppraisalEmployeeForm empForm = GetEmployeeAppraisaleForm(employeeFormId);
        //    AppraisalForm form = empForm.AppraisalForm;

        //    decimal? educationDistributionPercent = GetFormSectionDistributionPercent(form, AppraisalBlockType.QualificationAndEducation);

        //    decimal? transferDistPercent = GetFormSectionDistributionPercent(form, AppraisalBlockType.TransferHistory);

        //    decimal? workHistoryDistPercent = GetFormSectionDistributionPercent(form, AppraisalBlockType.LevelDesignationWorkHistory);

        //    AppraisalRatingScaleLine educationHighestRatingScore =
        //       (
        //           from l in PayrollDataContext.AppraisalRatingScaleLines
        //           where l.RatingScaleRefID == form.HistoryEducationRatingScaleRef_ID
        //           orderby l.Score descending
        //           select l
        //       ).FirstOrDefault();

        //    AppraisalRatingScaleLine transferHighestRatingScore =
        //       (
        //           from l in PayrollDataContext.AppraisalRatingScaleLines
        //           where l.RatingScaleRefID == form.TransferRatingScaleRef_ID
        //           orderby l.Score descending
        //           select l
        //       ).FirstOrDefault();

        //    AppraisalRatingScaleLine workHistoryHighestRatingScore =
        //       (
        //           from l in PayrollDataContext.AppraisalRatingScaleLines
        //           where l.RatingScaleRefID == form.WorkHistoryRatingScaleRef_ID
        //           orderby l.Score descending
        //           select l
        //       ).FirstOrDefault();


        //    empForm.HistoryManagerQualificationRating = educationRating;
        //    empForm.HistoryManagerTransferRating = transferRating;
        //    empForm.HistoryManagerWorkHistoryRating = workHistoryRating;

        //   // empForm.SupervisorHistoryQualificationScore = empForm.SupervisorHistoryQualificationScore;

        //     empForm.EducationScore = empForm.EducationScore;

        //    // confirm below formulate for civil bank cases

        //    //if (educationHighestRatingScore != null)
        //    //    empForm.SupervisorHistoryQualificationScore = (educationRating / educationHighestRatingScore.Score.Value) * (double) educationDistributionPercent.Value;

        //    //if (transferHighestRatingScore != null)
        //    //    empForm.SupervisorHistoryTransferScore = (transferRating / transferHighestRatingScore.Score.Value) * (double)transferDistPercent.Value;

        //    //if(workHistoryHighestRatingScore != null)
        //    //    empForm.SupervisorHistoryWorkHistoryScore = (workHistoryRating / workHistoryHighestRatingScore.Score.Value) * (double)workHistoryDistPercent.Value;


        //    PayrollDataContext.SubmitChanges();

        //    double? empScore = 0, supScore = 0;
        //    PayrollDataContext.AppraisalCalculateRating(employeeFormId, form.AppraisalFormID, ref empScore, ref supScore);
        //    PayrollDataContext.SubmitChanges();

        //}

        public static Status ReverseAppraisalStatus(int empAppraisalFormId, int newStepStatusId, string notes)
        {
            Status statusReturn = new Status();
            AppraisalEmployeeForm empForm = GetEmployeeAppraisaleForm(empAppraisalFormId);


            AppraisalStatus status = (AppraisalStatus)empForm.Status;



            if (newStepStatusId >= empForm.Status)
            {
                statusReturn.ErrorMessage = "Revert can be done to previous status only.";
                return statusReturn;
            }


            //if (status == AppraisalStatus.SaveAndSend)
            //{
            //    empForm.Status = (int)AppraisalStatus.Saved;
            //    empForm.RevertedBy = SessionManager.CurrentLoggedInUserID;
            //    empForm.RevertedOn = GetCurrentDateAndTime();
            //    PayrollDataContext.SubmitChanges();
            //    return statusReturn;
            //}

            GetDocumentNextStepUsingApprovalFlowResult nextStep = CommonManager.GetDocumentNextStep(
                (int)newStepStatusId, FlowTypeEnum.Appraisal, empForm.EmployeeId, empForm.RolloutID.Value, true);


            AppraisalRolloutFlow rolloutStep = PayrollDataContext.AppraisalRolloutFlows.FirstOrDefault
                (x => x.StepRef_ID == newStepStatusId && x.RolloutRef_ID == empForm.RolloutID);

            ApprovalFlow step = //CommonManager.GetDocumentPrevStep(empForm.Status.Value,FlowTypeEnum.Appraisal);
                PayrollDataContext.ApprovalFlows.FirstOrDefault(x => x.FlowType == (int)FlowTypeEnum.Appraisal && x.StepID == newStepStatusId);

            if (newStepStatusId == (int)AppraisalStatus.Saved)
            {
                empForm.Status = (int)AppraisalStatus.Saved;
                empForm.StatusText = "";
                empForm.RevertedBy = SessionManager.CurrentLoggedInUserID;
                empForm.RevertedOn = GetCurrentDateAndTime();
                empForm.RevertNotes = notes;
                empForm.CurrentApprovalSelectedEmployeeId = null;
                empForm.NextStepOrStatusId = null;

                // delete comments done after this status as commenting will be re-entered
                PayrollDataContext.AppraisalEmployeeSummaryComments.DeleteAllOnSubmit(
                    PayrollDataContext.AppraisalEmployeeSummaryComments.Where(x =>
                        x.AppraisalEmployeeFormRef_ID == empAppraisalFormId && x.StatusStepID > empForm.Status));

                PayrollDataContext.SubmitChanges();
            }
            // for rollout flow case
            else if (rolloutStep != null)
            {
                empForm.Status = rolloutStep.StepRef_ID;
                empForm.StatusText = rolloutStep.StepName;
                empForm.RevertedBy = SessionManager.CurrentLoggedInUserID;
                empForm.RevertedOn = GetCurrentDateAndTime();
                empForm.RevertNotes = notes;

                AppraisalStatus defaultStatus = (AppraisalStatus)newStepStatusId;
                if (defaultStatus == AppraisalStatus.Saved || nextStep == null)
                {
                    empForm.CurrentApprovalSelectedEmployeeId = null;
                    empForm.NextStepOrStatusId = null;
                }
                else
                {
                    empForm.CurrentApprovalSelectedEmployeeId = nextStep.ApprovalEmployee1;
                    empForm.NextStepOrStatusId = nextStep.NextNextStepID;
                }

                // delete comments done after this status as commenting will be re-entered
                PayrollDataContext.AppraisalEmployeeSummaryComments.DeleteAllOnSubmit(
                    PayrollDataContext.AppraisalEmployeeSummaryComments.Where(x =>
                        x.AppraisalEmployeeFormRef_ID == empAppraisalFormId && x.StatusStepID > empForm.Status));

                PayrollDataContext.SubmitChanges();
            }

            else if (step != null)
            {
                empForm.Status = step.StepID;
                empForm.StatusText = step.StepName;
                empForm.RevertedBy = SessionManager.CurrentLoggedInUserID;
                empForm.RevertedOn = GetCurrentDateAndTime();
                empForm.RevertNotes = notes;

                AppraisalStatus defaultStatus = (AppraisalStatus)newStepStatusId;
                if (defaultStatus == AppraisalStatus.Saved || nextStep == null)
                {
                    empForm.CurrentApprovalSelectedEmployeeId = null;
                    empForm.NextStepOrStatusId = null;
                }
                else
                {
                    empForm.CurrentApprovalSelectedEmployeeId = nextStep.ApprovalEmployee1;
                    empForm.NextStepOrStatusId = nextStep.NextNextStepID;
                }

                // delete comments done after this status as commenting will be re-entered
                PayrollDataContext.AppraisalEmployeeSummaryComments.DeleteAllOnSubmit(
                    PayrollDataContext.AppraisalEmployeeSummaryComments.Where(x =>
                        x.AppraisalEmployeeFormRef_ID == empAppraisalFormId && x.StatusStepID > empForm.Status));

                PayrollDataContext.SubmitChanges();
            }
            else
            {
                statusReturn.ErrorMessage = empForm.StatusText + " appraisal can not be reverted.";
            }

            return statusReturn;
        }

        ///// <summary>
        ///// Revert by department head for dish home, as for this case if reverted and submitted from reversion
        ///// then it should come back directory to department head
        ///// </summary>
        ///// <param name="empAppraisalFormId"></param>
        ///// <param name="newStepStatusId"></param>
        ///// <param name="notes"></param>
        ///// <returns></returns>
        //public static Status DHDepartmentHeadReverseAppraisalStatus(int empAppraisalFormId, int newStepStatusId, string notes)
        //{
        //    Status statusReturn = new Status();
        //    AppraisalEmployeeForm empForm = GetEmployeeAppraisaleForm(empAppraisalFormId);


        //    AppraisalStatus status = (AppraisalStatus)empForm.Status;



        //    if (newStepStatusId >= empForm.Status)
        //    {
        //        statusReturn.ErrorMessage = "Revert can be done to previous status only.";
        //        return statusReturn;
        //    }


        //    //if (status == AppraisalStatus.SaveAndSend)
        //    //{
        //    //    empForm.Status = (int)AppraisalStatus.Saved;
        //    //    empForm.RevertedBy = SessionManager.CurrentLoggedInUserID;
        //    //    empForm.RevertedOn = GetCurrentDateAndTime();
        //    //    PayrollDataContext.SubmitChanges();
        //    //    return statusReturn;
        //    //}

        //    GetDocumentNextStepUsingApprovalFlowResult nextStep = CommonManager.GetDocumentNextStep(
        //        (int)newStepStatusId, FlowTypeEnum.Appraisal, empForm.EmployeeId, empForm.RolloutID.Value, true);

        //    ApprovalFlow step = //CommonManager.GetDocumentPrevStep(empForm.Status.Value,FlowTypeEnum.Appraisal);
        //        PayrollDataContext.ApprovalFlows.FirstOrDefault(x => x.FlowType == (int)FlowTypeEnum.Appraisal && x.StepID == newStepStatusId);


        //    //empForm.CurrentApprovalSelectedEmployeeId = SessionManager.CurrentLoggedInEmployeeId;



        //    if (newStepStatusId == (int)AppraisalStatus.Saved)
        //    {
        //        empForm.Status = (int)AppraisalStatus.Saved;
        //        empForm.StatusText = "";
        //        empForm.RevertedBy = SessionManager.CurrentLoggedInUserID;
        //        empForm.RevertedOn = GetCurrentDateAndTime();
        //        empForm.RevertNotes = notes;


        //        // delete comments done after this status as commenting will be re-entered
        //        PayrollDataContext.AppraisalEmployeeSummaryComments.DeleteAllOnSubmit(
        //            PayrollDataContext.AppraisalEmployeeSummaryComments.Where(x =>
        //                x.AppraisalEmployeeFormRef_ID == empAppraisalFormId && x.StatusStepID > empForm.Status));

        //        PayrollDataContext.SubmitChanges();
        //    }
        //    else if (step != null)
        //    {
        //        empForm.Status = step.StepID;
        //        empForm.StatusText = step.StepName;
        //        empForm.RevertedBy = SessionManager.CurrentLoggedInUserID;
        //        empForm.RevertedOn = GetCurrentDateAndTime();
        //        empForm.RevertNotes = notes;

        //        //AppraisalStatus defaultStatus = (AppraisalStatus)newStepStatusId;
        //        //if (defaultStatus == AppraisalStatus.Saved || nextStep == null)
        //        //{
        //        //    empForm.CurrentApprovalSelectedEmployeeId = null;
        //        //    empForm.NextStepOrStatusId = null;
        //        //}
        //        //else
        //        //{
        //        //    empForm.CurrentApprovalSelectedEmployeeId = nextStep.ApprovalEmployee1;
        //        //    empForm.NextStepOrStatusId = nextStep.NextStepID;
        //        //}

        //        // delete comments done after this status as commenting will be re-entered
        //        PayrollDataContext.AppraisalEmployeeSummaryComments.DeleteAllOnSubmit(
        //            PayrollDataContext.AppraisalEmployeeSummaryComments.Where(x =>
        //                x.AppraisalEmployeeFormRef_ID == empAppraisalFormId && x.StatusStepID > empForm.Status));

        //        PayrollDataContext.SubmitChanges();
        //    }
        //    else
        //    {
        //        statusReturn.ErrorMessage = empForm.StatusText + " appraisal can not be reverted.";
        //    }

        //    return statusReturn;
        //}

        public static string GetRecommendationName(int? recommendId)
        {
            AppraisalRecommendFor item =
                PayrollDataContext.AppraisalRecommendFors.FirstOrDefault(x => x.RecommendForId == recommendId);
            if (item == null)
                return "";

            return item.Name;
        }

        /// <summary>
        /// Review other appraisal form
        /// </summary>
        /// <param name="employeeAppraisalFormId"></param>
        /// <param name="comment"></param>
        /// <returns></returns>
        public static Status UpdateAppraisalSummaryComment(int employeeAppraisalFormId, string comment, int? recommendForId, double? additionPoints)
        {
            Status status = new Status();

            AppraisalEmployeeForm empForm = GetEmployeeAppraisaleForm(employeeAppraisalFormId);

            if (empForm.EmployeeId == SessionManager.CurrentLoggedInEmployeeId)
            {
                status.ErrorMessage = "Self employee can not review the form.";
                return status;
            }

            int currentEmployeeId = 0; string currentEmployeeUserName = "";
            UserManager.SetCurrentEmployeeAndUser(ref currentEmployeeId, ref currentEmployeeUserName);
            if (SessionManager.CurrentLoggedInEmployeeId == 0)
            {

                if (currentEmployeeId == 0)
                {
                    status.ErrorMessage = "Please assign the employee from Manage user before changing appraisal.";
                    return status;
                }

            }

            AppraisalRollout rollout = GetRolloutByFormID(empForm.RolloutID.Value);
            GetDocumentNextStepUsingApprovalFlowResult nextStep = CommonManager.GetDocumentNextStep(
                (int)empForm.Status, FlowTypeEnum.Appraisal, empForm.EmployeeId, rollout.RolloutID, false);


            AppraisalEmployeeSummaryComment dbComment = new AppraisalEmployeeSummaryComment();
            dbComment.AppraisalEmployeeFormRef_ID = employeeAppraisalFormId;
            dbComment.CommentedEmployeeId = SessionManager.CurrentLoggedInEmployeeId;
            if (dbComment.CommentedEmployeeId == 0)
                dbComment.CommentedEmployeeId = currentEmployeeId;
            dbComment.CommentedOn = GetCurrentDateAndTime();
            dbComment.Comment = comment;
            dbComment.CommenterDisplayTitle = nextStep.AuthorityName;
            dbComment.StatusStepID = nextStep.NextStepID;

            if (recommendForId != null && recommendForId != -1 && recommendForId != 0)
            {
                empForm.RecommendForId = recommendForId;
                empForm.RecommendedByEmpId = SessionManager.CurrentLoggedInEmployeeId;
                if (empForm.RecommendedByEmpId == 0)
                    empForm.RecommendedByEmpId = currentEmployeeId;
                dbComment.RecommendForId = recommendForId;
                dbComment.Type = (int)AppraisalSummaryCommentTypeEnum.HasRecommendation;
            }
            if (additionPoints != null)
            {
                empForm.AdditionalQuestionnaireScore = additionPoints;
                dbComment.Type = (int)AppraisalSummaryCommentTypeEnum.HasAdditionalMarks;

                // recalculate Questionnaire marks, currently this option is being used for Civil to add Questionnare to make upto 80%
                decimal? PerformanceQuestionnaireValue = GetFormSectionDistributionPercent(empForm.AppraisalForm, AppraisalBlockType.Question);

                if (empForm.SupervisorQuestionTotalScore != 0)// && form.PerformanceQuestionnaireValue != null)
                {
                    empForm.FinalSupQuestionScore = (empForm.SupervisorQuestionScore / empForm.SupervisorQuestionTotalScore) *
                     (double)PerformanceQuestionnaireValue;
                }
                else
                    empForm.FinalSupQuestionScore = 0;

                empForm.FinalSupQuestionScoreWithoutAddition = empForm.FinalSupQuestionScore;
                empForm.FinalSupQuestionScore += additionPoints.Value;
                empForm.FinalSupQuestionScore = double.Parse(empForm.FinalSupQuestionScore.Value.ToString("n2"));

                // if greater than set in Performance summary then set as maximum
                if (empForm.FinalSupQuestionScore.Value > (double)PerformanceQuestionnaireValue)
                    empForm.FinalSupQuestionScore = (double)PerformanceQuestionnaireValue;


            }

            PayrollDataContext.AppraisalEmployeeSummaryComments.InsertOnSubmit(dbComment);

            empForm.Status = nextStep.NextStepID;
            empForm.StatusText = nextStep.StepStatusName;

            if (nextStep != null)
            {
                empForm.CurrentApprovalSelectedEmployeeId = nextStep.ApprovalEmployee1;

                empForm.NextStepOrStatusId = nextStep.NextNextStepID;
            }



            PayrollDataContext.SubmitChanges();

            if (additionPoints != null)
            {
                double? empScore = 0, supScore = 0;
                PayrollDataContext.AppraisalCalculateRating(empForm.AppraisalEmployeeFormID, empForm.AppraisalForm.AppraisalFormID, ref empScore, ref supScore);
            }

            if (nextStep != null)
            {
                if (nextStep.ApprovalEmployee1 != null)
                    SendAppraisalMailAndMessage(EmailContentType.AppraisalHigherLevel, nextStep.ApprovalEmployee1.Value, null, empForm);

                //if (nextStep.ApprovalEmployee2 != null)
                //    SendAppraisalMailAndMessage(EmailContentType.AppraisalHigherLevel, nextStep.ApprovalEmployee2.Value, null, empForm);
            }

            return status;
        }

        public static void UpdateEmployeeSelfSummaryComment(int employeeAppraisalFormId, string comment, bool? iAgree)
        {
            AppraisalEmployeeForm empForm = GetEmployeeAppraisaleForm(employeeAppraisalFormId);
            AppraisalForm form = empForm.AppraisalForm;

            if (empForm.EmployeeId != SessionManager.CurrentLoggedInEmployeeId)
                return;

            //AppraisalEmployeeSummaryComment dbComment =
            //    PayrollDataContext.AppraisalEmployeeSummaryComments.FirstOrDefault
            //        (x => x.AppraisalEmployeeFormRef_ID == employeeAppraisalFormId && x.CommentedEmployeeId
            //            == SessionManager.CurrentLoggedInEmployeeId);

            //if (dbComment != null)
            //{
            //    dbComment.Comment = comment;
            //    if (form.HideIAgreeDisAgreeButtons != null && form.HideIAgreeDisAgreeButtons.Value) { }
            //    else
            //        empForm.SupervisorReviewAgreed = iAgree;
            //}
            //else
            {
                AppraisalEmployeeSummaryComment dbComment = new AppraisalEmployeeSummaryComment();
                dbComment.AppraisalEmployeeFormRef_ID = employeeAppraisalFormId;
                dbComment.CommentedEmployeeId = SessionManager.CurrentLoggedInEmployeeId;
                dbComment.CommentedOn = GetCurrentDateAndTime();
                dbComment.Comment = comment;
                dbComment.CommenterDisplayTitle = new EmployeeManager().GetById(empForm.EmployeeId).Name;
                dbComment.StatusStepID = (int)AppraisalStatus.EmployeeReviewed;

                if (form.ShowAgreeDisAgreeButtons != null && form.ShowAgreeDisAgreeButtons.Value)
                {
                    empForm.SupervisorReviewAgreed = iAgree;
                    dbComment.Type = (int)AppraisalSummaryCommentTypeEnum.HasIAgreeOrDisagree;
                }
                PayrollDataContext.AppraisalEmployeeSummaryComments.InsertOnSubmit(dbComment);
            }

            PayrollDataContext.SubmitChanges();
        }
        public static void UpdateStep2SupervisorComment(int employeeAppraisalFormId, string comment, int recommendForId)
        {
            AppraisalEmployeeForm empForm = GetEmployeeAppraisaleForm(employeeAppraisalFormId);

            empForm.SupervisorStep2Comment = comment;

            if (recommendForId != -1)
            {
                empForm.RecommendForId = recommendForId;
                empForm.RecommendedByEmpId = SessionManager.CurrentLoggedInEmployeeId;
                //dbComment.RecommendForId = recommendForId;
            }



            PayrollDataContext.SubmitChanges();
        }
        #endregion   

        //public static bool HasFormSelfRating(int formId)
        //{
        //    AppraisalForm form = getFormInstanceByID(formId);
        //    bool hasSelfRating = false;

        //    if (form.AllowActivitySelfRating != null && form.AllowActivitySelfRating.Value)
        //        hasSelfRating = true;

        //    if (form.AllowCompetencySelfRating != null && form.AllowCompetencySelfRating.Value)
        //        hasSelfRating = true;

        //    if (form.AllowQuestionSelfRating != null && form.AllowQuestionSelfRating.Value)
        //        hasSelfRating = true;

        //    return hasSelfRating;
        //}

        public static Status SaveUpdateEmployeeReview(int employeeFormId,
            List<AppraisalValue> rewviewList)
        {
            Status status = new Status();
            bool recordExists = PayrollDataContext.AppraisalEmployeeFormReviews.Any(x => x.AppraisalEmployeeFormRef_ID
                    == employeeFormId);

            AppraisalEmployeeForm empForm = GetEmployeeAppraisaleForm(employeeFormId);
            AppraisalRatingScale ratingScale = null;

            if (empForm.AppraisalForm.ReviewRatingScaleRef_ID != null)
                ratingScale = GetRatingScaleById(empForm.AppraisalForm.ReviewRatingScaleRef_ID.Value);

            if (recordExists == false)
            {
                List<AppraisalEmployeeFormReview> dbReviews = new List<AppraisalEmployeeFormReview>();

                foreach (AppraisalValue value in rewviewList)
                {
                    dbReviews.Add
                        (
                        new AppraisalEmployeeFormReview
                        {
                            ReviewRef_ID = value.ReviewId,
                            AppraisalEmployeeFormRef_ID = employeeFormId,
                            ManagerRating = GetRatingScore(ratingScale, value.SupervisorRating),
                            ManagerComment = value.SupervisorComment
                        }
                        );
                }

                PayrollDataContext.AppraisalEmployeeFormReviews.InsertAllOnSubmit(dbReviews);

            }
            else
            {

                //List<AppraisalEmployeeFormReview> dbReviews
                //    = PayrollDataContext.AppraisalEmployeeFormReviews.Where(x => x.AppraisalEmployeeFormRef_ID
                //        == employeeFormId).ToList();

                foreach (AppraisalValue value in rewviewList)
                {
                    AppraisalEmployeeFormReview dbReview = PayrollDataContext.AppraisalEmployeeFormReviews.FirstOrDefault
                        (x => x.AppraisalEmployeeFormRef_ID == employeeFormId && x.ReviewRef_ID == value.ReviewId);

                    if (dbReview != null)
                    {
                        if (empForm.Status == (int)AppraisalStatus.SaveAndSend)
                        {
                            dbReview.ManagerComment = value.SupervisorComment;
                            dbReview.ManagerRating = GetRatingScore(ratingScale, value.SupervisorRating);
                        }
                    }
                    else
                    {
                        AppraisalEmployeeFormReview review =
                        new AppraisalEmployeeFormReview
                        {
                            ReviewRef_ID = value.ReviewId,
                            AppraisalEmployeeFormRef_ID = employeeFormId,
                            ManagerRating = GetRatingScore(ratingScale, value.SupervisorRating),
                            ManagerComment = value.SupervisorComment
                        };

                        PayrollDataContext.AppraisalEmployeeFormReviews.InsertOnSubmit(review);

                    }
                }
            }


            PayrollDataContext.SubmitChanges();
            return status;
        }
        public static Status SaveUpdateEmployeeQuestion(int employeeFormId,
            List<AppraisalValue> questionList)
        {
            Status status = new Status();
            bool recordExists = PayrollDataContext.AppraisalEmployeeFormQuestionnaires.Any(x => x.AppraisalEmployeeFormRef_ID
                    == employeeFormId);

            AppraisalEmployeeForm empForm = GetEmployeeAppraisaleForm(employeeFormId);
            List<AppraisalFormQuestionnaire> dbFormQuestionnaires =
                (
                    from a in PayrollDataContext.AppraisalFormQuestionnaires
                    where a.AppraisalFormRef_ID == empForm.AppraisalFormRef_ID
                    select a
                ).ToList();

            AppraisalForm form = empForm.AppraisalForm;
            AppraisalRatingScaleLine smallestRatingScore =
                (
                    from l in PayrollDataContext.AppraisalRatingScaleLines
                    where l.RatingScaleRefID == empForm.AppraisalForm.QuestionnareRatingScaleRef_ID.Value
                    orderby l.Score
                    select l
                ).FirstOrDefault();
            AppraisalRatingScaleLine highestRatingScore =
               (
                   from l in PayrollDataContext.AppraisalRatingScaleLines
                   where l.RatingScaleRefID == empForm.AppraisalForm.QuestionnareRatingScaleRef_ID.Value
                   orderby l.Score descending
                   select l
               ).FirstOrDefault();

            List<AppraisalEmployeeFormQuestionnaire> dbQuestionnaires = new List<AppraisalEmployeeFormQuestionnaire>();

            if (recordExists == false)
            {

                foreach (AppraisalValue value in questionList)
                {
                    dbQuestionnaires.Add
                        (
                        new AppraisalEmployeeFormQuestionnaire
                        {
                            QuestionnaireRef_ID = value.QuestionnaireId,
                            AppraisalEmployeeFormRef_ID = employeeFormId,
                            SelfRating = value.SelfRating == null ? 0 : value.SelfRating.Value,
                            SelfComment = value.SelfComment
                        }
                        );
                }

                PayrollDataContext.AppraisalEmployeeFormQuestionnaires.InsertAllOnSubmit(dbQuestionnaires);

            }
            else
            {

                dbQuestionnaires = PayrollDataContext.AppraisalEmployeeFormQuestionnaires.Where(x => x.AppraisalEmployeeFormRef_ID
                                    == employeeFormId).ToList();

                foreach (AppraisalValue question in questionList)
                {
                    AppraisalEmployeeFormQuestionnaire dbQuestion = dbQuestionnaires.Where(x => x.QuestionnaireRef_ID == question.QuestionnaireId).FirstOrDefault();

                    if (dbQuestion == null)
                    {
                        PayrollDataContext.AppraisalEmployeeFormQuestionnaires.InsertOnSubmit
                        (
                        new AppraisalEmployeeFormQuestionnaire
                        {
                            QuestionnaireRef_ID = question.QuestionnaireId,
                            AppraisalEmployeeFormRef_ID = employeeFormId,
                            SelfRating = question.SelfRating == null ? 0 : question.SelfRating.Value,
                            SelfComment = question.SelfComment
                        }
                        );
                    }
                    else
                    {
                        if (empForm.Status == null || empForm.Status == 0)
                        {
                            dbQuestion.SelfComment = question.SelfComment;
                            dbQuestion.SelfRating = question.SelfRating == null ? 0 : question.SelfRating.Value;
                        }
                        else if (empForm.Status == (int)AppraisalStatus.SaveAndSend)
                        {
                            dbQuestion.ManagerComment = question.SupervisorComment;
                            dbQuestion.ManagerRating = question.SupervisorRating == null ? 0 : question.SupervisorRating.Value;
                        }
                    }

                }


            }


            double totalNewWeight = 0;
            double totalScore = 0;

            decimal? PerformanceQuestionnaireValue = GetFormSectionDistributionPercent(form, AppraisalBlockType.Question);

            // Calculate Total Points
            if (empForm.Status == null || empForm.Status == 0)
            {
                empForm.EmployeeQuestionScore = dbQuestionnaires.Sum(x => x.SelfRating);

                if (smallestRatingScore.Score == 0 && smallestRatingScore.SkipInPointCalculation != null && smallestRatingScore.SkipInPointCalculation.Value)
                {
                    totalNewWeight = highestRatingScore.Score.Value * dbQuestionnaires.Count(x => x.SelfRating != smallestRatingScore.Score);
                }
                else
                {
                    totalNewWeight = highestRatingScore.Score.Value * dbQuestionnaires.Count;
                }

                empForm.EmployeeQuestionTotalScore = totalNewWeight;

                // prabhu like Questionnaire points calculation
                if (form.QuestionaireMarkingType == (int)(int)AppraisalMarkingTypeEnum.PercentWithEachQuestionWeightage)
                {
                    // https://rigotech.atlassian.net/browse/HR-82
                    foreach (var item in dbQuestionnaires)
                    {
                        AppraisalFormQuestionnaire dbFormQuestion = dbFormQuestionnaires.FirstOrDefault(x => x.QuestionnareID == item.QuestionnaireRef_ID);
                        if(dbFormQuestion != null)
                        {
                            totalScore += ((item.SelfRating == null ? 0 : item.SelfRating.Value) / 100.0) * (dbFormQuestion.Weight == null ? 0 : dbFormQuestion.Weight.Value);
                        }
                    }
                    
                    empForm.EmployeeQuestionScore = totalScore;

                    // convert using Performance score percentage
                    empForm.FinalEmpQuestionScore = ((double)PerformanceQuestionnaireValue / 100.0) * totalScore;
                }
                else
                {
                    if (empForm.EmployeeQuestionTotalScore != 0)
                    {
                        empForm.FinalEmpQuestionScore = (empForm.EmployeeQuestionScore / empForm.EmployeeQuestionTotalScore) *
                         (double)PerformanceQuestionnaireValue;
                    }
                    else
                        empForm.FinalEmpQuestionScore = 0;
                }

                empForm.FinalEmpQuestionScore = double.Parse(empForm.FinalEmpQuestionScore.Value.ToString("n2"));
            }
            else if (empForm.Status == (int)AppraisalStatus.SaveAndSend)
            {
                empForm.SupervisorQuestionScore = dbQuestionnaires.Sum(x => x.ManagerRating);

                if (smallestRatingScore.Score == 0 && smallestRatingScore.SkipInPointCalculation != null && smallestRatingScore.SkipInPointCalculation.Value)
                {
                    totalNewWeight = highestRatingScore.Score.Value * dbQuestionnaires.Count(x => x.ManagerRating != smallestRatingScore.Score);
                }
                else
                {
                    totalNewWeight = highestRatingScore.Score.Value * dbQuestionnaires.Count;
                }

                empForm.SupervisorQuestionTotalScore = totalNewWeight;

                // prabhu like Questionnaire points calculation
                if (form.QuestionaireMarkingType == (int)(int)AppraisalMarkingTypeEnum.PercentWithEachQuestionWeightage)
                {
                    // https://rigotech.atlassian.net/browse/HR-82
                    foreach (var item in dbQuestionnaires)
                    {
                        AppraisalFormQuestionnaire dbFormQuestion = dbFormQuestionnaires.FirstOrDefault(x => x.QuestionnareID == item.QuestionnaireRef_ID);
                        if (dbFormQuestion != null)
                        {
                            totalScore += ((item.ManagerRating == null ? 0 : item.ManagerRating.Value) / 100.0) * (dbFormQuestion.Weight == null ? 0 : dbFormQuestion.Weight.Value);
                        }
                    }
                    empForm.SupervisorQuestionScore = totalScore;

                    // convert using Performance score percentage
                    empForm.FinalSupQuestionScore = ((double)PerformanceQuestionnaireValue / 100.0) * totalScore;
                }
                else
                {
                    if (empForm.SupervisorQuestionTotalScore != 0)// && form.PerformanceQuestionnaireValue != null)
                    {
                        empForm.FinalSupQuestionScore = (empForm.SupervisorQuestionScore / empForm.SupervisorQuestionTotalScore) *
                         (double)PerformanceQuestionnaireValue;
                    }
                    else
                        empForm.FinalSupQuestionScore = 0;
                }

                empForm.FinalSupQuestionScore = double.Parse(empForm.FinalSupQuestionScore.Value.ToString("n2"));
            }

            empForm.IsQuestionSaved = true;
            PayrollDataContext.SubmitChanges();

            double? empScore = 0, supScore = 0;
            PayrollDataContext.AppraisalCalculateRating(employeeFormId, form.AppraisalFormID, ref empScore, ref supScore);
            PayrollDataContext.SubmitChanges();

            return status;
        }


        public static AppraisalEmployeeSummaryComment GetAppraisalSelfEmployeeSummaryComment(int employeeAppraisalFormId)
        {
            AppraisalEmployeeForm empForm = GetEmployeeAppraisaleForm(employeeAppraisalFormId);

            AppraisalEmployeeSummaryComment dbComment =
             PayrollDataContext.AppraisalEmployeeSummaryComments
             .Where(x => x.AppraisalEmployeeFormRef_ID == employeeAppraisalFormId && x.CommentedEmployeeId
                     == empForm.EmployeeId).FirstOrDefault();
            ;

            if (dbComment != null)
                return dbComment;

            return null;
        }
        public static List<AppraisalEmployeeSummaryComment> GetAppraisalCommentList(int employeeAppraisalFormId)
        {
            AppraisalEmployeeForm empForm = GetEmployeeAppraisaleForm(employeeAppraisalFormId);

            return PayrollDataContext.AppraisalEmployeeSummaryComments.Where
                  (x => x.AppraisalEmployeeFormRef_ID == employeeAppraisalFormId)
                     .OrderBy(x => x.CommentedOn).ToList();


        }

        public static AppraisalEmployeeForm GetEmployeeAppraisaleForm(int employeeAppraisaleFormId)
        {
            return PayrollDataContext.AppraisalEmployeeForms.FirstOrDefault(
                x => x.AppraisalEmployeeFormID == employeeAppraisaleFormId);
        }
        public static AppraisalEmployeeForm GetEmployeeAppraisaleForm(int empId, int formId)
        {
            return PayrollDataContext.AppraisalEmployeeForms.FirstOrDefault(
                x => x.EmployeeId == empId && x.AppraisalFormRef_ID == formId);
        }
        #endregion

        public static List<AppraisalFormQuestionnaireCategory> getallCategoriesByFormID(int formID)
        {
            return PayrollDataContext.AppraisalFormQuestionnaireCategories.Where(x => x.AppraisalFormRef_ID == formID).ToList();
        }

        public static List<AppraisalForm> GetAllForms()
        {
            return PayrollDataContext.AppraisalForms.ToList();
        }

        public static List<GetAppraisalRolloutSPResult> GetAllRollouts(int start, int pagesize, int empId, int formId, string period)
        {
            EmployeeManager mgr = new EmployeeManager();
            //List<AppraisalRollout> rollout = new List<AppraisalRollout>();

            //rollout = PayrollDataContext.AppraisalRollouts.Where(x => (empId == -1 || x.EmployeeId == empId) && (formId == -1 || x.FormID == formId)).ToList();

            List<GetAppraisalRolloutSPResult> rollout = new List<GetAppraisalRolloutSPResult>();

            rollout = PayrollDataContext.GetAppraisalRolloutSP(start, pagesize, empId, formId, period).ToList();



            return rollout;
        }

        public static Status DivieQuestionnariesWeightEqually(AppraisalFormQuestionnaire inst)
        {
            Status status = new Status();
            List<AppraisalFormQuestionnaire> _listDbEnity = new List<AppraisalFormQuestionnaire>();

            _listDbEnity = PayrollDataContext.AppraisalFormQuestionnaires.Where(x => x.AppraisalFormRef_ID == inst.AppraisalFormRef_ID).ToList();
            foreach (AppraisalFormQuestionnaire _list in _listDbEnity)
            {
                _list.Weight = inst.Weight;
            }

            PayrollDataContext.SubmitChanges();
            status.IsSuccess = true;
            return status;
        }


        public static Status DivieCompetencyWeightEqually(AppraisalFormCompetency inst)
        {
            Status status = new Status();
            List<AppraisalFormCompetency> _listDbEnity = new List<AppraisalFormCompetency>();

            _listDbEnity = PayrollDataContext.AppraisalFormCompetencies.Where(x => x.AppraisalFormRef_ID == inst.AppraisalFormRef_ID).ToList();
            foreach (AppraisalFormCompetency _list in _listDbEnity)
            {
                _list.Weight = inst.Weight;
            }

            PayrollDataContext.SubmitChanges();
            status.IsSuccess = true;
            return status;
        }

        public static Status DivieReviewWeightEqually(AppraisalFormReview inst)
        {
            Status status = new Status();
            List<AppraisalFormReview> _listDbEnity = new List<AppraisalFormReview>();

            _listDbEnity = PayrollDataContext.AppraisalFormReviews.Where(x => x.AppraisalFormRef_ID == inst.AppraisalFormRef_ID).ToList();
            foreach (AppraisalFormReview _list in _listDbEnity)
            {
                _list.Weight = inst.Weight;
            }

            PayrollDataContext.SubmitChanges();
            status.IsSuccess = true;
            return status;
        }


        public static Status DeleteForms(List<int> formList)
        {
            Status status = new Status();

            foreach (var item in formList)
            {
                AppraisalEmployeeForm dbForm = PayrollDataContext.AppraisalEmployeeForms
                    .FirstOrDefault(x => x.AppraisalEmployeeFormID == item);
                if (dbForm != null)
                {
                    AppraisalRollout rollout = PayrollDataContext.AppraisalRollouts
                        .FirstOrDefault(x => x.RolloutID == dbForm.RolloutID);

                    if (rollout != null && rollout.EmployeeId != null)
                    {
                        // if individual roll out then delete rollout also
                        PayrollDataContext.AppraisalRollouts.DeleteOnSubmit(rollout);
                    }

                    AppraisalPeriod period = GetPeriodByID(dbForm.PeriodId.Value);

                    ChangeMonetory log = BLL.BaseBiz.GetMonetoryChangeLog(
                       dbForm.EmployeeId, (byte)MonetoryChangeLogTypeEnum.Appraisal, "Appraisal deleted of " + period.Name, "",
                       "", LogActionEnum.Delete)
                                    ;

                    PayrollDataContext.AppraisalEmployeeForms.DeleteOnSubmit(dbForm);

                }
            }

            PayrollDataContext.SubmitChanges();

            return status;
        }

        public static List<GetRolloutEmployeesListResult> GetEmployeeRolloutList(int start, int pagesize,
            int EmoloyeeID, int levelId, int statusId, int branchId, int formId, string statusFilter, string periodIds, int appraisalGroup)
        {
            List<GetRolloutEmployeesListResult> resultSet = PayrollDataContext.GetRolloutEmployeesList
                (start, pagesize, EmoloyeeID, statusId, levelId, branchId, formId, statusFilter, periodIds, appraisalGroup).ToList();

            DateTime date = GetCurrentDateAndTime();
            date = new DateTime(date.Year, date.Month, date.Day);

            string grade, gradeName;


            foreach (GetRolloutEmployeesListResult item in resultSet)
            {
                if (item.Status == (int)AppraisalStatus.Saved)
                    item.StatusText = "Not submitted";





            }

            return resultSet;
        }

        public static List<Appraisal_GetDetailReportResult> GetDetailReport(int start, int pagesize,
            int EmoloyeeID, int levelId, int statusId, int branchId, int formId, string periodIds)
        {
            List<Appraisal_GetDetailReportResult> resultSet = PayrollDataContext.Appraisal_GetDetailReport
                (start, pagesize, EmoloyeeID, statusId, levelId, branchId, formId, periodIds).ToList();

            DateTime date = GetCurrentDateAndTime();
            date = new DateTime(date.Year, date.Month, date.Day);

            string grade, gradeName, summaryName;


            foreach (Appraisal_GetDetailReportResult item in resultSet)
            {
                item.SetComment();


                //if (item.EmployeeCompetencyTotalScore != null)
                //{
                //    GetMegaCompetenciesGradeForPercent((decimal)item.EmployeeCompetencyTotalScore.Value, out gradeName);
                //    item.EmployeeCompetencyGrade = gradeName;
                //}

                //if (item.SupervisorCompetencyTotalScore!= null)
                //{
                //    GetMegaCompetenciesGradeForPercent((decimal)item.SupervisorCompetencyTotalScore.Value, out gradeName);
                //    item.SupervisorCompetencyGrade = gradeName;
                //}

                //if (!string.IsNullOrEmpty(item.EmployeeGrade))
                //{
                //    GetMegaGradeForPercent(item.EmployeeGrade, out summaryName);
                //    item.EmployeeSummary = summaryName;
                //}

                //if (!string.IsNullOrEmpty(item.SupervisorGrade))
                //{
                //    GetMegaGradeForPercent(item.SupervisorGrade, out summaryName);
                //    item.SupervisorSummary = summaryName;
                //}

            }

            return resultSet;
        }

        public static Status SaveUpdateWeightsTargetsAchievements(List<AppraisalEmployeeTargetOtherDetail> list)
        {
            Status status = new Status();

            SetConnectionPwd(PayrollDataContext.Connection);
            PayrollDataContext.Transaction = PayrollDataContext.Connection.BeginTransaction();
            try
            {
                foreach (var item in list)
                {
                    AppraisalEmployeeTargetOtherDetail dbObj = PayrollDataContext.AppraisalEmployeeTargetOtherDetails.SingleOrDefault(x => x.EmployeeId == item.EmployeeId && x.FormRef_ID == item.FormRef_ID && x.TargetRef_ID == item.TargetRef_ID);
                    if (dbObj != null)
                    {
                        dbObj.Weight = item.Weight;
                        dbObj.AssignedTargetTextValue = item.AssignedTargetTextValue;
                        dbObj.AchievementTextValue = item.AchievementTextValue;
                    }
                    else
                        PayrollDataContext.AppraisalEmployeeTargetOtherDetails.InsertOnSubmit(item);
                }

                PayrollDataContext.SubmitChanges();
                PayrollDataContext.Transaction.Commit();
                status.IsSuccess = true;

            }
            catch (Exception exp)
            {

                PayrollDataContext.Transaction.Rollback();
                Log.log("Error while importing : " + list.ToArray().ToString(), exp);
                status.ErrorMessage = "Error while importing.";
                status.IsSuccess = false;
            }
            finally
            {
            }

            return status;
        }

        public static Status SaveExcelSheetEmpRolloutGroup(List<AppraisalRolloutGroupEmployee> list)
        {
            Status status = new Status();

            SetConnectionPwd(PayrollDataContext.Connection);
            PayrollDataContext.Transaction = PayrollDataContext.Connection.BeginTransaction();
            try
            {
                foreach (var item in list)
                {
                    AppraisalRolloutGroupEmployee dbObj = PayrollDataContext.AppraisalRolloutGroupEmployees.SingleOrDefault(x => x.EmployeeID == item.EmployeeID);
                    if (dbObj != null)
                    {
                        PayrollDataContext.AppraisalRolloutGroupEmployees.DeleteOnSubmit(dbObj);
                        PayrollDataContext.SubmitChanges();
                    }
                    PayrollDataContext.AppraisalRolloutGroupEmployees.InsertOnSubmit(item);
                }
                PayrollDataContext.SubmitChanges();
                PayrollDataContext.Transaction.Commit();
                status.IsSuccess = true;
            }
            catch (Exception exp)
            {

                PayrollDataContext.Transaction.Rollback();
                Log.log("Error while importing : " + list.ToArray().ToString(), exp);
                status.ErrorMessage = "Error while importing.";
                status.IsSuccess = false;
            }
            finally
            {
            }

            return status;
        }

        public static Status UpdateEmpRolloutGroup(AppraisalRolloutGroupEmployee _dbobj)
        {
            Status status = new Status();
            if (_dbobj != null)
            {
                AppraisalRolloutGroupEmployee ent = PayrollDataContext.AppraisalRolloutGroupEmployees.SingleOrDefault(x => x.EmployeeID == _dbobj.EmployeeID);
                if (ent != null)
                {
                    PayrollDataContext.AppraisalRolloutGroupEmployees.DeleteOnSubmit(ent);
                    PayrollDataContext.SubmitChanges();
                }
                else
                {
                    status.IsSuccess = false;
                    return status;
                }
                PayrollDataContext.AppraisalRolloutGroupEmployees.InsertOnSubmit(_dbobj);
                PayrollDataContext.SubmitChanges();
                status.IsSuccess = true;
            }
            return status;
        }

        public static Status UpdateEmpSeniority(EEmployee _dbobj)
        {
            Status status = new Status();
            SetConnectionPwd(PayrollDataContext.Connection);
            PayrollDataContext.Transaction = PayrollDataContext.Connection.BeginTransaction();
            if (_dbobj != null)
            {
                EEmployee ent = PayrollDataContext.EEmployees.SingleOrDefault(x => x.EmployeeId == _dbobj.EmployeeId);
                if (ent != null)
                {
                    ent.Seniority = _dbobj.Seniority;
                    ent.Modified = DateTime.Now.Date;
                    ent.ModifiedBy = SessionManager.CurrentLoggedInUserID;
                    status.IsSuccess = true;
                    PayrollDataContext.SubmitChanges();
                    PayrollDataContext.Transaction.Commit();
                }

            }
            return status;
        }


        public static Status SaveExcelSheetEmpSeniority(List<EEmployee> list)
        {
            Status status = new Status();

            SetConnectionPwd(PayrollDataContext.Connection);
            PayrollDataContext.Transaction = PayrollDataContext.Connection.BeginTransaction();
            try
            {
                foreach (var item in list)
                {
                    EEmployee dbObj = PayrollDataContext.EEmployees.SingleOrDefault(x => x.EmployeeId == item.EmployeeId);
                    if (dbObj != null)
                    {
                        dbObj.Seniority = item.Seniority;
                        dbObj.Modified = DateTime.Now.Date;
                        dbObj.ModifiedBy = SessionManager.CurrentLoggedInUserID;
                    }
                }
                PayrollDataContext.SubmitChanges();
                PayrollDataContext.Transaction.Commit();
                status.IsSuccess = true;
            }
            catch (Exception exp)
            {

                PayrollDataContext.Transaction.Rollback();
                Log.log("Error while importing : " + list.ToArray().ToString(), exp);
                status.ErrorMessage = "Error while importing.";
                status.IsSuccess = false;
            }
            finally
            {
            }

            return status;
        }

    }

}
