﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using DAL;
using Utils;
using System;
using Utils.Calendar;
using BLL.BO;

namespace BLL.Manager
{




    public enum UploadType
    {
        Photo = 1,
        Signature = 2,
        ThumbPrint = 3,
        Document = 4
    }

    public class HRManager : BaseBiz
    {

        public bool SaveEmployeeHRDetails(EHumanResource entity)
        {
            PayrollDataContext.EHumanResources.InsertOnSubmit(entity);
            return SaveChangeSet();
        }

        public bool SaveOrUpdateImage(UploadType type, string fileLocation, int employeeId)
        {
            HHumanResource dbEntity = GetHumanResource(employeeId);
            bool isSaved = false;
            string oldFileUrl = "";
            if (dbEntity == null)
            {
                isSaved = true;
                dbEntity = new HHumanResource();
                dbEntity.EmployeeId = employeeId;
            }


            switch (type)
            {
                case UploadType.Photo:
                    oldFileUrl = dbEntity.UrlPhoto;
                    dbEntity.UrlPhoto = fileLocation;
                    break;
                case UploadType.Signature:
                    oldFileUrl = dbEntity.UrlSignature;
                    dbEntity.UrlSignature = fileLocation;
                    break;
                case UploadType.ThumbPrint:
                    oldFileUrl = dbEntity.UrlThumbPrint;
                    dbEntity.UrlThumbPrint = fileLocation;
                    break;
            }

            if (isSaved)
            {
                PayrollDataContext.HHumanResources.InsertOnSubmit(dbEntity);
                return SaveChangeSet();
            }
            else
            {
                //delete old file if has
                try
                {
                    if (!string.IsNullOrEmpty(oldFileUrl))
                    {
                        string url = Path.Combine(HttpContext.Current.Server.MapPath(Config.UploadLocation), oldFileUrl);
                        File.Delete(url);
                    }
                }
                catch { }

                UpdateChangeSet();
                return true;
            }
        }

        public bool SaveOrUpdateEmployeeHRDetails(HHumanResource entity, EAddress empAddress, bool IsEnglishDOB, string DateOfBirth)
        {
            HHumanResource dbEntity = GetHumanResource(entity.EmployeeId.Value);
            if (dbEntity == null)
            {
                PayrollDataContext.HHumanResources.InsertOnSubmit(entity);
                return SaveChangeSet();
            }

            if (string.IsNullOrEmpty(DateOfBirth) == false)
            {
                dbEntity.EEmployee.IsEnglishDOB = IsEnglishDOB;
                dbEntity.EEmployee.DateOfBirth = DateOfBirth;
                try
                {
                    if (!string.IsNullOrEmpty(dbEntity.EEmployee.DateOfBirth))
                        dbEntity.EEmployee.DateOfBirthEng = GetEngDate(dbEntity.EEmployee.DateOfBirth, dbEntity.EEmployee.IsEnglishDOB.Value);
                    else
                        dbEntity.EEmployee.DateOfBirthEng = null;
                }
                catch
                {
                }
            }

            dbEntity.BloodGroup = entity.BloodGroup;
            dbEntity.CasteId = entity.CasteId;
            dbEntity.Qualification = entity.Qualification;
            dbEntity.MajorSubjects = entity.MajorSubjects;
            dbEntity.Languages = entity.Languages;

            dbEntity.CitizenshipCertNo = entity.CitizenshipCertNo;
            dbEntity.CitizenshipIssuedAt = entity.CitizenshipIssuedAt;
            dbEntity.DrivingLicenseNo = entity.DrivingLicenseNo;

            dbEntity.PassportValidUpto = entity.PassportValidUpto;
            dbEntity.PassportNo = entity.PassportNo;
            dbEntity.PassportIssuedDate = entity.PassportIssuedDate;
            dbEntity.PassportValidUpto = entity.PassportValidUpto;
            dbEntity.PassportIssuedAt = entity.PassportIssuedAt;

            dbEntity.Notes = entity.Notes;

            // called from Employee HR page so update Address/Contact info also
            if (empAddress != null)
            {
                new EmployeeManager().SetEAddress(dbEntity.EEmployee.EAddresses[0], empAddress, false);
            }



            return UpdateChangeSet();
        }

        public HHumanResource GetHumanResource(int employeeId)
        {
            return PayrollDataContext.HHumanResources.SingleOrDefault
                (e => e.EmployeeId == employeeId);
        }


        private int startIndex = 0;
        private void Reset()
        {
            startIndex = 0;
        }
        private string GetCss()
        {
            if ((startIndex++) % 2 == 0)
                return "odd";
            return "even";
        }

        public string GetHTMLEmployeeEduction(int employeeId)
        {
            List<HEducation> educations = GetEmployeeEducation(employeeId);

            StringBuilder html = new StringBuilder();

            html.Append("<table border='0' class='tableLightColor'><tbody>");
            html.Append("<tr> <th>Course name</th><th>University</th><th>College</th><th>Place</th><th>% or Grade</th><th>Passed year</th><th>Expires</th>  <th> </th> </tr>");
            Reset();
            foreach (HEducation eduction in educations)
            {
                string changeHTML = string.Format(@"
<input type='image' style='border-width: 0px;' onclick='return popupUpdateEducationCall({0});' src='../images/edit.gif' />
                            <input type='image' style='border-width: 0px;' onclick='return deleteEducationCall({0});' src='../images/delete.gif' />",
                                                                                                                                                    eduction.EductionId);

                html.Append(
                    string.Format(@"
                            <tr class='{7}'> 
                                <td>{0}</td><td>{1}</td><td>{8}</td><td>{9}</td><td>{2}</td><td>{3}</td><td>{4}</td>   
                            <td> 
                              {10}
                            </td> 
                            </tr>",
                        eduction.Course,
                        eduction.University,
                        eduction.Percentage.ToString(),
                        eduction.PassedYear.ToString(),
                        eduction.Expires,
                        eduction.Remarks,
                        eduction.EductionId,
                        GetCss(),
                        eduction.College,
                        eduction.Place,
                        (SessionManager.IsReadOnlyUser ? "" : changeHTML)
                    )
                    );
            }

            html.Append("</tbody></table>");

            return html.ToString();

        }

        public List<HEducation> GetEmployeeEducation(int employeeId)
        {
            return PayrollDataContext.HEducations.Where(
                e => e.EmployeeId == employeeId).ToList();
        }
        public HEducation GetEmployeeEducationById(int educationId)
        {
            return PayrollDataContext.HEducations.SingleOrDefault(c => c.EductionId == educationId);
        }

        public bool DeleteEducation(HEducation education)
        {
            HEducation edu = PayrollDataContext.HEducations.SingleOrDefault(c => c.EductionId == education.EductionId);
            if (edu == null)
                return false;

            System.Data.Linq.ChangeSet cs = PayrollDataContext.GetChangeSet();
            PayrollDataContext.HEducations.DeleteOnSubmit(edu);
            PayrollDataContext.SubmitChanges();
            return (cs.Deletes.Count == 1);
        }
        public void SaveEmployeeEducation(HEducation entity)
        {
            PayrollDataContext.HEducations.InsertOnSubmit(entity);
            PayrollDataContext.SubmitChanges();
        }
        public List<HCourseList> GetAllCourses()
        {
            return PayrollDataContext.HCourseLists.ToList();
        }
        public bool UpdateEmployeeEducation(HEducation entity)
        {
            HEducation dbEntity = PayrollDataContext.HEducations.SingleOrDefault(c => c.EductionId == entity.EductionId);
            if (dbEntity == null)
                return false;

            dbEntity.Course = entity.Course;
            dbEntity.University = entity.University;
            dbEntity.College = entity.College;
            dbEntity.Place = entity.Place;
            dbEntity.Expires = entity.Expires;
            dbEntity.PassedYear = entity.PassedYear;
            dbEntity.Percentage = entity.Percentage;
            dbEntity.Remarks = entity.Remarks;
            PayrollDataContext.SubmitChanges();
            return true;

        }


        public bool SaveDocument(HDocument document)
        {
            if (document.DocumentId == 0)
            {
                PayrollDataContext.HDocuments.InsertOnSubmit(document);
            }
            else
            {
                HDocument editDocument = new HDocument();
                editDocument = PayrollDataContext.HDocuments.Where(h => h.DocumentId == document.DocumentId).First();
                editDocument.Description = document.Description;
            }
            return SaveChangeSet();
        }
        public static DepositDocument GetDepositDoc(int id)
        {
            return PayrollDataContext.DepositDocuments.SingleOrDefault(x => x.DepositDocumentId == id);
        }
        public static bool SaveDocument(DepositDocumentAttachment document)
        {
            PayrollDataContext.DepositDocumentAttachments.InsertOnSubmit(document);
            return SaveChangeSet();
        }
        public static bool SaveDocument(DepositDocument document)
        {

            if (PayrollDataContext.DepositDocuments.Any(x => x.Type == document.Type && x.PayrollPeriodId == document.PayrollPeriodId
                && x.DepositDocumentId != document.DepositDocumentId))
                return false;

            document.CreatedBy = SessionManager.User.UserID;
            document.CreatedOn = DateTime.Now;
            document.ModifedBy = SessionManager.User.UserID;
            document.ModifiedOn = DateTime.Now;

            PayrollDataContext.DepositDocuments.InsertOnSubmit(document);
            return SaveChangeSet();
        }
        public static List<DepositDocument> GetDepoistDocumentList(int yearId)
        {
            List<DepositDocument> list =

                (
                from d in PayrollDataContext.DepositDocuments
                join p in PayrollDataContext.PayrollPeriods on d.PayrollPeriodId equals p.PayrollPeriodId
                where p.FinancialDateId == yearId
                orderby d.PayrollPeriodId, d.Type
                select d
                ).ToList();



            return list;
        }
        public static bool UpdateDocument(DepositDocument document)
        {
            if (PayrollDataContext.DepositDocuments.Any(x => x.Type == document.Type && x.PayrollPeriodId == document.PayrollPeriodId
               && x.DepositDocumentId != document.DepositDocumentId))
                return false;

            DepositDocument dbDoc = PayrollDataContext.DepositDocuments.SingleOrDefault(x => x.DepositDocumentId == document.DepositDocumentId);

            dbDoc.ModifedBy = SessionManager.User.UserID;
            dbDoc.ModifiedOn = DateTime.Now;

            CopyObject<DepositDocument>(document, ref dbDoc, "DepositDocumentId", "CreatedBy", "CreatedOn");

            UpdateChangeSet();
            return true;
        }
        public List<HDocument> GetDocumentList(int employeeId)
        {
            return PayrollDataContext.HDocuments.Where
                (e => e.EmployeeId == employeeId).ToList();
        }

        public HDocument GetDocument(string docUrl)
        {
            return PayrollDataContext.HDocuments.SingleOrDefault(
                e => e.Url == docUrl);
        }

        public static DepositDocumentAttachment GetDepositDocument(string docUrl)
        {
            return PayrollDataContext.DepositDocumentAttachments.SingleOrDefault(
                e => e.Url == docUrl);
        }

        public bool DeleteDocument(HDocument document)
        {
            try
            {
                HDocument doc = PayrollDataContext.HDocuments.SingleOrDefault(e => e.DocumentId == document.DocumentId);

                if (doc == null)
                    return false;

                System.Data.Linq.ChangeSet cs = PayrollDataContext.GetChangeSet();
                PayrollDataContext.HDocuments.DeleteOnSubmit(doc);
                PayrollDataContext.SubmitChanges();
                string url = Path.Combine(HttpContext.Current.Server.MapPath(Config.UploadLocation), doc.Url);
                File.Delete(url);
            }
            catch (Exception exp)
            {
                Log.log("Error while deleting document", exp);
            }
            return true;

        }
        public static bool DeleteDepositDocument(int depositDocId)
        {
            try
            {
                DepositDocument doc = PayrollDataContext.DepositDocuments.SingleOrDefault(e => e.DepositDocumentId == depositDocId);

                if (doc == null)
                    return false;


                PayrollDataContext.DepositDocuments.DeleteOnSubmit(doc);
                PayrollDataContext.SubmitChanges();

            }
            catch (Exception exp)
            {
                Log.log("Error while deleting document", exp);
            }
            return true;

        }
        public bool DeleteDepositDocument(DepositDocumentAttachment document)
        {
            try
            {
                DepositDocumentAttachment doc = PayrollDataContext.DepositDocumentAttachments.SingleOrDefault(e => e.DocumentId == document.DocumentId);

                if (doc == null)
                    return false;

                System.Data.Linq.ChangeSet cs = PayrollDataContext.GetChangeSet();
                PayrollDataContext.DepositDocumentAttachments.DeleteOnSubmit(doc);
                PayrollDataContext.SubmitChanges();
                string url = Path.Combine(HttpContext.Current.Server.MapPath(Config.UploadLocation), doc.Url);
                File.Delete(url);
            }
            catch (Exception exp)
            {
                Log.log("Error while deleting document", exp);
            }
            return true;

        }
        public string GetHTMLDocuments(int employeeId)
        {
            StringBuilder str = new StringBuilder();

            List<HDocument> docs = GetDocumentList(employeeId);
            string code =
                @"<div class='documentSeparatorDiv' > <a href='../DocumentHandler.ashx?id={0}'>{1}</a> <input type='image' onclick='confirmDelDoc({2});return false;' src='../images/delete.gif'></image> </div>";
            foreach (HDocument doc in docs)
            {
                str.Append(string.Format(code, Path.GetFileNameWithoutExtension(doc.Url), doc.Name, doc.DocumentId));
            }
            return str.ToString();
        }
        public static string GetHTMLDepositDocuments(int depositDocumentId)
        {
            StringBuilder str = new StringBuilder();

            List<DepositDocumentAttachment> docs = PayrollDataContext.DepositDocumentAttachments.Where(x => x.DepositDocumentId == depositDocumentId).ToList();
            string code =
                @"<div class='documentSeparatorDiv' > <a href='../DocumentHandler.ashx?id={0}'>{1}</a> <input type='image' onclick='confirmDelDoc({2});return false;' src='../images/delete.gif'></image> </div>";
            foreach (DepositDocumentAttachment doc in docs)
            {
                str.Append(string.Format(code, Path.GetFileNameWithoutExtension(doc.Url), doc.Name, doc.DocumentId));
            }
            return str.ToString();
        }
        public static List<HDocument> GetDocuments(int employeeId)
        {
            List<HDocument> list =
                PayrollDataContext.HDocuments.Where(e => e.EmployeeId == employeeId).ToList();

            foreach (HDocument item in list)
            {
                if (item.DocumentType != null)
                    item.TypeName = ((EmployeeDocumentType)item.DocumentType).ToString();
            }

            return list;
        }

        public string GetHTMLEmployeeTraining(int employeeId)
        {
            List<HTraining> trainings = GetEmployeeTraining(employeeId);

            StringBuilder html = new StringBuilder();

            html.Append("<table class='tableLightColor' border='0'><tbody>");
            html.Append("<tr> <th>Training name</th><th>Institution</th><th>From</th><th>To</th><th>Note</th>   <th> </th></tr>");
            Reset();
            foreach (HTraining training in trainings)
            {
                string changeHTML = string.Format(@"
   <input type='image' style='border-width: 0px;' onclick='return popupUpdateTrainingCall({0});' src='../images/edit.gif' />
                            <input type='image' style='border-width: 0px;' onclick='return deleteTrainingCall({0});' src='../images/delete.gif' />"
                    , training.TrainingId);

                html.Append(
                    string.Format(@"
                            <tr class='{6}'> 
                                <td>{0}</td><td>{1}</td><td>{2}</td><td>{3}</td><td>{4}</td>  
                            <td> 
                         {7}
                            </td> 
                            </tr>",
                        training.TrainingName,
                        training.InstitutionName,
                        training.TrainingFrom,
                        training.TrainingTo,
                        training.Remarks,
                        training.TrainingId,
                        GetCss(),
                        (SessionManager.IsReadOnlyUser ? "" : changeHTML)
                    )
                    );
            }

            html.Append("</tbody></table>");

            return html.ToString();

        }
        public List<HTraining> GetEmployeeTraining(int employeeId)
        {
            return PayrollDataContext.HTrainings.Where(
                e => e.EmployeeId == employeeId).ToList();
        }
        public HTraining GetEmployeeTrainingById(int trainingId)
        {
            return PayrollDataContext.HTrainings.SingleOrDefault(c => c.TrainingId == trainingId);
        }

        public bool DeleteTraining(HTraining training)
        {
            HTraining train = PayrollDataContext.HTrainings.SingleOrDefault(c => c.TrainingId == training.TrainingId);
            if (train == null)
                return false;

            System.Data.Linq.ChangeSet cs = PayrollDataContext.GetChangeSet();
            PayrollDataContext.HTrainings.DeleteOnSubmit(train);
            PayrollDataContext.SubmitChanges();
            return (cs.Deletes.Count == 1);
        }
        public void SaveEmployeeTraining(HTraining entity)
        {
            PayrollDataContext.HTrainings.InsertOnSubmit(entity);
            PayrollDataContext.SubmitChanges();
        }
        public bool UpdateEmployeeTraining(HTraining entity)
        {
            HTraining dbEntity = PayrollDataContext.HTrainings.SingleOrDefault(c => c.TrainingId == entity.TrainingId);
            if (dbEntity == null)
                return false;

            dbEntity.TrainingName = entity.TrainingName;
            dbEntity.InstitutionName = entity.InstitutionName;
            dbEntity.TrainingFrom = entity.TrainingFrom;
            dbEntity.TrainingTo = entity.TrainingTo;
            dbEntity.Remarks = entity.Remarks;
            PayrollDataContext.SubmitChanges();
            return true;

        }

        //End Employee Training


        //Employee Curriculum

        public string GetHTMLEmployeeCurriculum(int employeeId)
        {
            List<HExtraActivity> activities = GetEmployeeCurriculum(employeeId);

            StringBuilder html = new StringBuilder();

            html.Append("<table class='tableLightColor'><tbody>");
            html.Append("<tr> <th>Game/Activity</th><th>Event</th><th>Award</th><th>From</th><th>To</th><th>Note</th>   <th> </th> </tr>");
            Reset();
            foreach (HExtraActivity activity in activities)
            {
                string changeHTML = string.Format(@"
<input type='image' style='border-width: 0px;' onclick='return popupUpdateActivityCall({0});' src='../images/edit.gif' />
                            <input type='image' style='border-width: 0px;' onclick='return deleteActivityCall({0});' src='../images/delete.gif' />
", activity.CurricularId);

                html.Append(
                    string.Format(@"
                            <tr class='{6}'> 
                                <td>{0}</td><td>{1}</td><td>{2}</td><td>{3}</td><td>{4}</td><td>{5}</td>   
                            <td> 
                            {7}
                            </td> 
                            </tr>",
                        "",//activity.Game_Activity,
                         "",//activity.Event,
                        activity.Award,
                        "",//activity.DateFrom,
                        "",//activity.DateTo,
                        activity.Remarks,
                        GetCss(),
                        (SessionManager.IsReadOnlyUser ? "" : changeHTML)


                    )
                    );
            }

            html.Append("</tbody></table>");

            return html.ToString();

        }
        public List<HExtraActivity> GetEmployeeCurriculum(int employeeId)
        {
            return PayrollDataContext.HExtraActivities.Where(
                e => e.EmployeeId == employeeId).ToList();
        }
        public HExtraActivity GetEmployeeCurriculumById(int curriculumId)
        {
            return PayrollDataContext.HExtraActivities.SingleOrDefault(c => c.CurricularId == curriculumId);
        }

        public bool DeleteCurriculum(HExtraActivity activity)
        {
            HExtraActivity act = PayrollDataContext.HExtraActivities.SingleOrDefault(c => c.CurricularId == activity.CurricularId);
            if (act == null)
                return false;

            System.Data.Linq.ChangeSet cs = PayrollDataContext.GetChangeSet();
            PayrollDataContext.HExtraActivities.DeleteOnSubmit(act);
            PayrollDataContext.SubmitChanges();
            return (cs.Deletes.Count == 1);
        }
        public void SaveEmployeeCurriculum(HExtraActivity entity)
        {
            PayrollDataContext.HExtraActivities.InsertOnSubmit(entity);
            PayrollDataContext.SubmitChanges();
        }
        public bool UpdateEmployeeCurriculum(HExtraActivity entity)
        {
            HExtraActivity dbEntity = PayrollDataContext.HExtraActivities.SingleOrDefault(c => c.CurricularId == entity.CurricularId);
            if (dbEntity == null)
                return false;

            //dbEntity.Game_Activity = entity.Game_Activity;
            //dbEntity.Event = entity.Event;
            //dbEntity.Award = entity.Award;
            //dbEntity.DateFrom = entity.DateFrom;
            //dbEntity.DateTo = entity.DateTo;
            dbEntity.Remarks = entity.Remarks;
            PayrollDataContext.SubmitChanges();
            return true;

        }
        //End Employee Curriculum


        //Employee Family

        public string GetHTMLEmployeeFamily(int employeeId)
        {
            List<HFamily> families = GetEmployeeFamily(employeeId);

            StringBuilder html = new StringBuilder();

            html.Append("<table class='tableLightColor'><tbody>");
            html.Append("<tr> <th> Name</th><th>Relation</th><th>Remarks</th><th>Date of Birth</th><th>Dependent</th>   <th> </th></tr>");
            Reset();
            foreach (HFamily family in families)
            {
                string changeHTML = string.Format(@"
 <input type='image' style='border-width: 0px;' onclick='return popupUpdateFamilyCall({0});' src='../images/edit.gif' />
                            <input type='image' style='border-width: 0px;' onclick='return deleteFamilyCall({0});' src='../images/delete.gif' />
", family.FamilyId);

                html.Append(
                    string.Format(@"
                            <tr class='{6}'> 
                                <td>{0}</td><td>{1}</td><td>{2}</td><td>{3}</td><td>{4}</td>  
                            <td> 
                           {7}
                            </td> 
                            </tr>",
                        family.Name,
                        family.Relation,
                        family.Remarks,
                        family.DateOfBirth,
                        family.HasDependent,
                        family.FamilyId,
                        GetCss(),
                        (SessionManager.IsReadOnlyUser ? "" : changeHTML)
                      )
                    );
            }

            html.Append("</tbody></table>");

            return html.ToString();

        }
        public List<HFamily> GetEmployeeFamily(int employeeId)
        {
            return PayrollDataContext.HFamilies.Where(
                e => e.EmployeeId == employeeId).ToList();
        }
        public HFamily GetEmployeeFamilyById(int familyId)
        {
            return PayrollDataContext.HFamilies.SingleOrDefault(c => c.FamilyId == familyId);
        }

        public bool DeleteFamily(HFamily family)
        {
            HFamily fam = PayrollDataContext.HFamilies.SingleOrDefault(c => c.FamilyId == family.FamilyId);
            if (fam == null)
                return false;

            System.Data.Linq.ChangeSet cs = PayrollDataContext.GetChangeSet();
            PayrollDataContext.HFamilies.DeleteOnSubmit(fam);
            PayrollDataContext.SubmitChanges();
            return (cs.Deletes.Count == 1);
        }
        public void SaveEmployeeFamily(HFamily entity)
        {
            PayrollDataContext.HFamilies.InsertOnSubmit(entity);
            PayrollDataContext.SubmitChanges();
        }
        public bool UpdateEmployeeFamily(HFamily entity)
        {
            HFamily dbEntity = PayrollDataContext.HFamilies.SingleOrDefault(c => c.FamilyId == entity.FamilyId);
            if (dbEntity == null)
                return false;

            dbEntity.Name = entity.Name;
            dbEntity.Relation = entity.Relation;
            dbEntity.Remarks = entity.Remarks;
            dbEntity.DateOfBirth = entity.DateOfBirth;
            dbEntity.HasDependent = entity.HasDependent;

            PayrollDataContext.SubmitChanges();
            return true;

        }

        //End Employee Family


        //Employee Disciplinary Action

        public string GetHTMLEmployeeDiscipline(int employeeId)
        {
            List<HDisciplinaryAction> disciplines = GetEmployeeDiscipline(employeeId);

            StringBuilder html = new StringBuilder();

            html.Append("<table  class='tableLightColor'><tbody>");
            html.Append("<tr> <th>Memo </th><th>Issued date</th><th>Issued by</th><th>Comments</th>   <th> </th></tr>");
            Reset();
            foreach (HDisciplinaryAction discipline in disciplines)
            {
                string changeHTML = string.Format(@"
 <input type='image' style='border-width: 0px;' onclick='return popupUpdateDisciplineCall({0});' src='../images/edit.gif' />
                            <input type='image' style='border-width: 0px;' onclick='return deleteDisciplineCall({0});' src='../images/delete.gif' />"
                    , discipline.DisciplinaryId);

                html.Append(
                    string.Format(@"
                            <tr class='{4}'> 
                                <td>{0}</td><td>{1}</td><td>{2}</td><td>{3}</td>  
                            <td> 
                           {5}
                            </td> 
                            </tr>",
                                  discipline.Memo,
                                  discipline.Issued__Date,
                                  discipline.Issued_By,
                                  discipline.Comments,
                                  GetCss(),
                                  (SessionManager.IsReadOnlyUser ? "" : changeHTML)

                    )
                    );
            }

            html.Append("</tbody></table>");

            return html.ToString();

        }
        public List<HDisciplinaryAction> GetEmployeeDiscipline(int employeeId)
        {
            return PayrollDataContext.HDisciplinaryActions.Where(
                e => e.EmployeeId == employeeId).ToList();
        }
        public HDisciplinaryAction GetEmployeeDisciplineById(int disciplineId)
        {
            return PayrollDataContext.HDisciplinaryActions.SingleOrDefault(c => c.DisciplinaryId == disciplineId);
        }

        public bool DeleteDiscipline(HDisciplinaryAction Discipline)
        {
            HDisciplinaryAction discp = PayrollDataContext.HDisciplinaryActions.SingleOrDefault(c => c.DisciplinaryId == Discipline.DisciplinaryId);
            if (discp == null)
                return false;

            System.Data.Linq.ChangeSet cs = PayrollDataContext.GetChangeSet();
            PayrollDataContext.HDisciplinaryActions.DeleteOnSubmit(discp);
            PayrollDataContext.SubmitChanges();
            return (cs.Deletes.Count == 1);
        }
        public void SaveEmployeeDiscipline(HDisciplinaryAction entity)
        {
            PayrollDataContext.HDisciplinaryActions.InsertOnSubmit(entity);
            PayrollDataContext.SubmitChanges();
        }
        public bool UpdateEmployeeDiscipline(HDisciplinaryAction entity)
        {
            HDisciplinaryAction dbEntity = PayrollDataContext.HDisciplinaryActions.SingleOrDefault(c => c.DisciplinaryId == entity.DisciplinaryId);
            if (dbEntity == null)
                return false;
            dbEntity.Memo = entity.Memo;
            dbEntity.Issued__Date = entity.Issued__Date;
            dbEntity.Issued_By = entity.Issued_By;
            dbEntity.Comments = entity.Comments;
            PayrollDataContext.SubmitChanges();
            return true;

        }

        //End Employee Disciplinary Action




        //Employee Evaluation 

        public string GetHTMLEmployeeEvaluation(int employeeId)
        {
            List<HEvaluation> evaluations = GetEmployeeEvaluation(employeeId);

            StringBuilder html = new StringBuilder();

            html.Append("<table   class='tableLightColor'><tbody>");
            html.Append("<tr> <th>Evaluation date </th><th>Done by</th><th>Comments</th><th>Next Evaluation date</th>   <th> </th></tr>");
            Reset();
            foreach (HEvaluation evaluation in evaluations)
            {
                html.Append(
                    string.Format(@"
                            <tr class='{5}'> 
                                <td>{0}</td><td>{1}</td><td>{2}</td><td>{3}</td>  
                            <td> 
                            <input type='image' style='border-width: 0px;' onclick='return popupUpdateEvaluationCall({4});' src='../images/edit.gif' />
                            <input type='image' style='border-width: 0px;' onclick='return deleteEvaluationCall({4});' src='../images/delete.gif' />
                            </td> 
                            </tr>",
                                  evaluation.EvaluationDate,
                                  evaluation.DoneBy,
                                  evaluation.Comments,
                                  evaluation.NextEvaluationDate,
                                  evaluation.EvaluationId,
                                  GetCss()

                    )
                    );
            }

            html.Append("</tbody></table>");

            return html.ToString();

        }
        public List<HEvaluation> GetEmployeeEvaluation(int employeeId)
        {
            return PayrollDataContext.HEvaluations.Where(
                e => e.EmployeeId == employeeId).ToList();
        }
        public HEvaluation GetEmployeeEvaluationById(int evaluationId)
        {
            return PayrollDataContext.HEvaluations.SingleOrDefault(c => c.EvaluationId == evaluationId);
        }

        public bool DeleteEvaluation(HEvaluation Eval)
        {
            HEvaluation eval = PayrollDataContext.HEvaluations.SingleOrDefault(c => c.EvaluationId == Eval.EvaluationId);
            if (eval == null)
                return false;

            System.Data.Linq.ChangeSet cs = PayrollDataContext.GetChangeSet();
            PayrollDataContext.HEvaluations.DeleteOnSubmit(eval);
            PayrollDataContext.SubmitChanges();
            return (cs.Deletes.Count == 1);
        }
        public void SaveEmployeeEvaluation(HEvaluation entity)
        {
            PayrollDataContext.HEvaluations.InsertOnSubmit(entity);
            PayrollDataContext.SubmitChanges();
        }
        public bool UpdateEmployeeEvaluation(HEvaluation entity)
        {
            HEvaluation dbEntity = PayrollDataContext.HEvaluations.SingleOrDefault(c => c.EvaluationId == entity.EvaluationId);
            if (dbEntity == null)
                return false;
            dbEntity.DoneBy = entity.DoneBy;
            dbEntity.EvaluationDate = entity.EvaluationDate;
            dbEntity.Comments = entity.Comments;
            dbEntity.NextEvaluationDate = entity.NextEvaluationDate;
            PayrollDataContext.SubmitChanges();
            return true;

        }

        //End Employee Evaluation 


        //Employee Accident 

        public string GetHTMLEmployeeAccident(int employeeId)
        {
            List<HAccident> accidents = GetEmployeeAccident(employeeId);

            StringBuilder html = new StringBuilder();

            html.Append("<table    class='tableLightColor'><tbody>");
            html.Append("<tr> <th>Accident </th><th>Location</th><th>Date</th><th>Compensation</th><th>Comments</th>   <th> </th></tr>");
            Reset();
            foreach (HAccident accident in accidents)
            {
                string changeHTML =
                            string.Format(@"<input type='image' style='border-width: 0px;' onclick='return popupUpdateAccidentCall({0});' src='../images/edit.gif' />
                            <input type='image' style='border-width: 0px;' onclick='return deleteAccidentCall({0});' src='../images/delete.gif' />", accident.AccidentId);

                html.Append(
                    string.Format(@"
                            <tr class='{5}'> 
                                <td>{0}</td><td>{1}</td><td>{2}</td><td>{3}</td><td>{4}</td>  
                            <td> 
                            {6}
                            </td> 
                            </tr>",
                                  accident.Accident,
                                  accident.Location,
                                  accident.Date,
                                  accident.Compensation,
                                  accident.Comments,
                                  GetCss(),
                                  (SessionManager.IsReadOnlyUser ? "" : changeHTML)
                    )
                    );
            }

            html.Append("</tbody></table>");

            return html.ToString();

        }
        public List<HAccident> GetEmployeeAccident(int employeeId)
        {
            return PayrollDataContext.HAccidents.Where(
                e => e.EmployeeId == employeeId).ToList();
        }
        public HAccident GetEmployeeAccidentById(int accidentId)
        {
            return PayrollDataContext.HAccidents.SingleOrDefault(c => c.AccidentId == accidentId);
        }

        public bool DeleteAccident(HAccident Acci)
        {
            HAccident accident = PayrollDataContext.HAccidents.SingleOrDefault(c => c.AccidentId == Acci.AccidentId);
            if (accident == null)
                return false;

            System.Data.Linq.ChangeSet cs = PayrollDataContext.GetChangeSet();
            PayrollDataContext.HAccidents.DeleteOnSubmit(accident);
            PayrollDataContext.SubmitChanges();
            return (cs.Deletes.Count == 1);
        }
        public void SaveEmployeeAccident(HAccident entity)
        {
            PayrollDataContext.HAccidents.InsertOnSubmit(entity);
            PayrollDataContext.SubmitChanges();
        }
        public bool UpdateEmployeeAccident(HAccident entity)
        {
            HAccident dbEntity = PayrollDataContext.HAccidents.SingleOrDefault(c => c.AccidentId == entity.AccidentId);
            if (dbEntity == null)
                return false;
            dbEntity.Accident = entity.Accident;
            dbEntity.Location = entity.Location;
            dbEntity.Date = entity.Date;
            dbEntity.Comments = entity.Comments;
            dbEntity.Compensation = entity.Compensation;
            PayrollDataContext.SubmitChanges();
            return true;

        }

        //End Employee Accident 

        //Special Event 

        public string GetHTMLSpecialEvent(int employeeId)
        {
            List<HSpecialEvent> events = GetSpecialEvent(employeeId);

            StringBuilder html = new StringBuilder();

            html.Append("<table   class='tableLightColor'><tbody>");
            html.Append("<tr> <th style='width:160px'>Description </th><th>Date</th><th style='width:160px'>Note</th>   <th> </th></tr>");
            Reset();
            foreach (HSpecialEvent eventt in events)
            {
                string editDeleteHTML = string.Format(
                    @"<input type='image' style='border-width: 0px;' onclick='return popupUpdateEventCall({0});' src='../images/edit.gif' />
                      <input type='image' style='border-width: 0px;' onclick='return deleteEventCall({0});' src='../images/delete.gif' />
                    ",
                    eventt.EventId);

                //If included in calculation clear
                //if (IsSpecialEventIncludedInCalculation(eventt.EventId))
                //{
                //    editDeleteHTML = string.Empty;    
                //}
                //CustomDate date = CustomDate.GetCustomDateFromString(eventt.Date, IsEnglish);
                html.Append(
                    string.Format(
                        @"
                            <tr class='{4}'> 
                                <td>{0}</td><td>{1}</td><td>{2}</td>  
                            <td> 
                                {3}
                            </td> 
                            </tr>",
                        eventt.Description,
                        eventt.Date,
                        // GetCurrency( eventt.PaySlip),
                        eventt.Comments,
                        (SessionManager.IsReadOnlyUser ? "" : editDeleteHTML),
                        GetCss()
                        )
                    );
            }

            html.Append("</tbody></table>");

            return html.ToString();

        }
        public List<HSpecialEvent> GetSpecialEvent(int employeeId)
        {
            return PayrollDataContext.HSpecialEvents.Where(
                e => e.EmployeeId == employeeId).ToList();
        }
        //public static bool IsSpecialEventIncludedInCalculation(int eventId)
        //{
        //    return PayrollDataContext.CCalculationIncludeds.Where(
        //        i => i.Type == ((int)CalculationColumnType.IncomeSpecialEvent)
        //             && i.SourceId == eventId
        //               ).Count() > 0;
        //}

        public HSpecialEvent GetSpecialEventById(int eventId)
        {
            return PayrollDataContext.HSpecialEvents.SingleOrDefault(c => c.EventId == eventId);
        }

        public bool DeleteSpecialEvent(HSpecialEvent Event)
        {
            HSpecialEvent splEvent = PayrollDataContext.HSpecialEvents.SingleOrDefault(c => c.EventId == Event.EventId);
            if (splEvent == null)
                return false;

            System.Data.Linq.ChangeSet cs = PayrollDataContext.GetChangeSet();
            PayrollDataContext.HSpecialEvents.DeleteOnSubmit(splEvent);
            PayrollDataContext.SubmitChanges();
            return (cs.Deletes.Count == 1);
        }
        public void SaveSpecialEvent(HSpecialEvent entity)
        {
            entity.Created = entity.Modified = DateTime.Now;
            entity.DateEng = GetEngDate(entity.Date, null);

            PayrollDataContext.HSpecialEvents.InsertOnSubmit(entity);
            PayrollDataContext.SubmitChanges();
        }
        public bool UpdateSpecialEvent(HSpecialEvent entity)
        {
            HSpecialEvent dbEntity = PayrollDataContext.HSpecialEvents.SingleOrDefault(c => c.EventId == entity.EventId);
            if (dbEntity == null)
                return false;
            dbEntity.Description = entity.Description;
            dbEntity.Date = entity.Date;
            dbEntity.Month = entity.Month;
            dbEntity.DateEng = GetEngDate(entity.Date, true);
            dbEntity.PaySlip = entity.PaySlip;
            dbEntity.Comments = entity.Comments;
            dbEntity.Modified = DateTime.Now;
            PayrollDataContext.SubmitChanges();
            return true;

        }
        //End Special Event 

        //Employee Previous Employment

        public string GetHTMLPreviousEmployment(int employeeId)
        {
            List<HPreviousEmployment> prevEmployments = GetEmployeePreviousEmployment(employeeId);

            StringBuilder html = new StringBuilder();

            html.Append("<table  class='tableLightColor'><tbody>");
            html.Append("<tr> <th>Organization</th><th>Place</th><th>Job responsibility</th><th>Employment date</th><th>Resignation date</th><th>Reason for leaving</th><th>Notes</th>   <th> </th> </tr>");
            foreach (HPreviousEmployment prevEmployment in prevEmployments)
            {
                string changeHTML = string.Format(@"  
<input type='image' style='border-width: 0px;' onclick='return popupUpdateEmploymentCall({0});' src='../images/edit.gif' />
                            <input type='image' style='border-width: 0px;' onclick='return deleteEmploymentCall({0});' src='../images/delete.gif' />", prevEmployment.PrevEmploymentId);

                html.Append(
                    string.Format(@"
                            <tr class='{8}'> 
                                <td>{0}</td><td>{1}</td><td>{2}</td><td>{3}</td><td>{4}</td><td>{5}</td><td>{6}</td>   
                            <td> 
                          {9}

                            </td> 
                            </tr>",
                                  prevEmployment.Organization,
                                  prevEmployment.Place,
                                  prevEmployment.JobResponsibility,
                                  prevEmployment.JoinDate,
                                  prevEmployment.ResignDate,
                                  prevEmployment.ReasonForLeaving,
                                  prevEmployment.Notes,
                                  prevEmployment.PrevEmploymentId,
                                  GetCss(),
                                  (SessionManager.IsReadOnlyUser ? "" : changeHTML)

                    )
                    );
            }

            html.Append("</tbody></table>");

            return html.ToString();

        }
        public List<HPreviousEmployment> GetEmployeePreviousEmployment(int employeeId)
        {
            return PayrollDataContext.HPreviousEmployments.Where(
                e => e.EmployeeId == employeeId).ToList();
        }
        public HPreviousEmployment GetEmployeePrevEmploymentById(int prevEmpId)
        {
            return PayrollDataContext.HPreviousEmployments.SingleOrDefault(c => c.PrevEmploymentId == prevEmpId);
        }

        public bool DeletePreviousEmployment(HPreviousEmployment employment)
        {
            HPreviousEmployment prevEmp = PayrollDataContext.HPreviousEmployments.SingleOrDefault(c => c.PrevEmploymentId == employment.PrevEmploymentId);
            if (prevEmp == null)
                return false;

            System.Data.Linq.ChangeSet cs = PayrollDataContext.GetChangeSet();
            PayrollDataContext.HPreviousEmployments.DeleteOnSubmit(prevEmp);
            PayrollDataContext.SubmitChanges();
            return (cs.Deletes.Count == 1);
        }
        public void SavePreviousEmployment(HPreviousEmployment entity)
        {
            PayrollDataContext.HPreviousEmployments.InsertOnSubmit(entity);
            PayrollDataContext.SubmitChanges();
        }
        public bool UpdatePreviousEmployment(HPreviousEmployment entity)
        {
            HPreviousEmployment dbEntity = PayrollDataContext.HPreviousEmployments.SingleOrDefault(c => c.PrevEmploymentId == entity.PrevEmploymentId);
            if (dbEntity == null)
                return false;

            dbEntity.Organization = entity.Organization;
            dbEntity.Place = entity.Place;
            dbEntity.JobResponsibility = entity.JobResponsibility;
            dbEntity.JoinDate = entity.JoinDate;
            dbEntity.ResignDate = entity.ResignDate;
            dbEntity.ReasonForLeaving = entity.ReasonForLeaving;
            dbEntity.Notes = entity.Notes;
            PayrollDataContext.SubmitChanges();
            return true;

        }
        //End Employee Previous Employment


        public static Status InsertUpdateClaimRequest(SCClaim obj, bool isInsert)
        {
            Status status = new Status();
            status.IsSuccess = false;
            try
            {

               

                if (isInsert)
                {

                    SCClaim _dbobj = PayrollDataContext.SCClaims.Where(x => x.EmployeeId == obj.EmployeeId && x.ClaimDate == obj.ClaimDate).SingleOrDefault();
                    if (_dbobj != null)
                    {
                        status.IsSuccess = false;
                        status.ErrorMessage = "Already exist insurance claim on provided date!";
                        return status;
                    }
                    obj.CreatedOn = DateTime.Now;
                    obj.CreatedBy = SessionManager.User.UserID;
                    obj.Status = (int)ClaimStatus.Uploaded;
                    PayrollDataContext.SCClaims.InsertOnSubmit(obj);
                    PayrollDataContext.SubmitChanges();
                    status.IsSuccess = true;

                }
                else
                {
                    SCClaim _dobj = PayrollDataContext.SCClaims.Where(x => x.EmployeeId == obj.EmployeeId && x.ClaimDate == obj.ClaimDate && x.ClaimID!=obj.ClaimID).SingleOrDefault();
                    if (_dobj != null)
                    {
                        status.IsSuccess = false;
                        status.ErrorMessage = "Already exist insurance claim on provided date!";
                        return status;
                    }


                    SCClaim dbobj = PayrollDataContext.SCClaims.Where(x => x.ClaimID == obj.ClaimID).SingleOrDefault();
                    if (dbobj != null && dbobj.Status != (int)ClaimStatus.Uploaded)
                    {
                        status.IsSuccess = false;
                        status.ErrorMessage = "Unable to update. Status have been already changed.";
                        return status;
                    }
                    if (dbobj != null)
                    {
                        dbobj.EmployeeId = obj.EmployeeId;
                        dbobj.ClaimDate = obj.ClaimDate;
                        dbobj.ClaimType = obj.ClaimType;
                        dbobj.ModifiedBy = SessionManager.User.UserID;
                        dbobj.ModifiedOn = DateTime.Now;
                        dbobj.ClaimOfFamilyId = obj.ClaimOfFamilyId;
                        dbobj.RelationId = obj.RelationId;
                        dbobj.DiagnosisDate = obj.DiagnosisDate;
                        dbobj.AccidentDateTime = obj.AccidentDateTime;
                        dbobj.DoctorName = obj.DoctorName;
                        dbobj.DoctorAddress = obj.DoctorAddress;
                        dbobj.AccidentLocation = obj.AccidentLocation;
                        dbobj.HospitalNameAddress = obj.HospitalNameAddress;
                        dbobj.ConsultantName = obj.ConsultantName;
                        dbobj.Details = obj.Details;
                        dbobj.TotalClaimAmount = obj.TotalClaimAmount;
                        dbobj.Status = (int)ClaimStatus.Uploaded;
                        dbobj.DetailsOfTreatment = obj.DetailsOfTreatment;
                        PayrollDataContext.SubmitChanges();
                        status.IsSuccess = true;
                    }
                }

            }
            catch (Exception ex)
            {
                status.IsSuccess = false;
                status.ErrorMessage = ex.Message.ToString();
            }
            return status;
        }

        public static Status DeleteClaim(int claimID)
        {
            Status status = new Status();
            status.IsSuccess = false;
            SCClaim dbobj = PayrollDataContext.SCClaims.Where(x => x.ClaimID == claimID).SingleOrDefault();
            if (dbobj != null && dbobj.Status != (int)ClaimStatus.Uploaded)
            {
                status.IsSuccess = false;
                status.ErrorMessage = "Unable to delete record ! Status have been already changed.";
                return status;
            }
            else if (dbobj != null)
            {
                PayrollDataContext.SCClaims.DeleteOnSubmit(dbobj);
                PayrollDataContext.SubmitChanges();
                status.IsSuccess = true;
            }
            return status;
        }

        public static SCClaim getClaim(int claimId)
        {
            return (PayrollDataContext.SCClaims.Where(x => x.ClaimID == claimId).SingleOrDefault());
        }

        public static List<GetEmployeeInsuranceClaimListResult> GetEmployeeInsuranceClaimList(int type, int status, int ein, DateTime? fromdate, DateTime? todate, int currentPage, int pageSize, ref int? total)
        {
            total = 0;
            List<GetEmployeeInsuranceClaimListResult> dbEntity = new List<GetEmployeeInsuranceClaimListResult>();
            dbEntity = PayrollDataContext.GetEmployeeInsuranceClaimList(ein, status, type, fromdate, todate, currentPage, pageSize).ToList();
            if (dbEntity.Count > 0)
                total = dbEntity[0].TotalRows.Value;

            return dbEntity;
        }

        public static List<GetEmployeeInsuranceClaimListByLotResult> GetEmployeeInsuranceClaimListByLot(int type, int status, int ein, int lotno, int havingLotNO, int currentPage, int pageSize, ref int? total)
        {
            total = 0;
            List<GetEmployeeInsuranceClaimListByLotResult> dbEntity = new List<GetEmployeeInsuranceClaimListByLotResult>();
            dbEntity = PayrollDataContext.GetEmployeeInsuranceClaimListByLot(ein, status, lotno, type, havingLotNO, currentPage, pageSize).ToList();
            if (dbEntity.Count > 0)
                total = dbEntity[0].TotalRows.Value;

            return dbEntity;
        }

        public static List<GetLotDetailsForInsuranceResult> GetLotDetailsForInsurance(int currentPage, int pageSize, ref int? total)
        {
            total = 0;
            List<GetLotDetailsForInsuranceResult> dbEntity = new List<GetLotDetailsForInsuranceResult>();
            dbEntity = PayrollDataContext.GetLotDetailsForInsurance(currentPage, pageSize).ToList();
            if (dbEntity.Count > 0)
                total = dbEntity[0].TotalRows.Value;
            return dbEntity;
        }


        public static Status SaveLot(List<SCClaimLotDetail> Lotdetails, out string ltNo)
        {
            ltNo = "";
            Status status = new Status();
            status.IsSuccess = false;

            SetConnectionPwd(PayrollDataContext.Connection);
            PayrollDataContext.Transaction = PayrollDataContext.Connection.BeginTransaction();
            try
            {
                //lot
                SCClaimLot ls = new SCClaimLot();
                ls.CreatedOn = DateTime.Now;
                ls.CreatedBy = SessionManager.User.UserID;

                PayrollDataContext.SCClaimLots.InsertOnSubmit(ls);
                PayrollDataContext.SubmitChanges();


                //lot details
                int lotno = ls.LotID;
                ltNo = ls.LotID.ToString();
                foreach (var item in Lotdetails)
                {
                    item.LotRef_ID = lotno;
                }
                PayrollDataContext.SCClaimLotDetails.InsertAllOnSubmit(Lotdetails);
                PayrollDataContext.SubmitChanges();

                //claims
                foreach (var itm in Lotdetails)
                {
                    SCClaim obj = PayrollDataContext.SCClaims.Where(x => x.ClaimID == itm.ClaimRef_ID).SingleOrDefault();
                    if (obj != null)
                    {
                        obj.LotRef_ID = lotno;
                    }
                }
                PayrollDataContext.SubmitChanges();

                status.IsSuccess = true;
                PayrollDataContext.Transaction.Commit();

            }
            catch (Exception exp)
            {
                Log.log("Error while assinging lot to insurance claims!", exp);
                PayrollDataContext.Transaction.Rollback();
                status.IsSuccess = false;
                status.ErrorMessage = "Unable to assign lot no.";
            }
            finally
            {

            }

            return status;


        }

        public static List<GetEmployeeInsuranceClaimListForAdminResult> GetEmployeeInsuranceClaimListForAdmin(int status, int ein, DateTime? fromdate, DateTime? todate, int currentPage, int pageSize, ref int? total)
        {
            total = 0;
            List<GetEmployeeInsuranceClaimListForAdminResult> dbEntity = new List<GetEmployeeInsuranceClaimListForAdminResult>();
            dbEntity = PayrollDataContext.GetEmployeeInsuranceClaimListForAdmin(ein, status, fromdate, todate, currentPage, pageSize).ToList();
            if (dbEntity.Count > 0)
                total = dbEntity[0].TotalRows.Value;

            return dbEntity;
        }

        public static Status UpdateInsuranceClaimStatus(int claimID, int newStatus, DateTime date)
        {
            Status status = new Status();
            status.IsSuccess = false;
            DAL.SCClaimStatusHistory dbobj = new SCClaimStatusHistory();
            DAL.SCClaim obj = getClaim(claimID);
            if (obj != null)
            {
                //history
                dbobj.Status = obj.Status;
                dbobj.ClaimRef_ID = claimID;
                dbobj.CreatedBy = SessionManager.User.UserID;
                dbobj.CreatedOn = DateTime.Now;
                dbobj.StatusDate = date;

                //main table
                obj.Status = newStatus;

                PayrollDataContext.SCClaimStatusHistories.InsertOnSubmit(dbobj);
                PayrollDataContext.SubmitChanges();


                status.IsSuccess = true;
            }
            return status;
        }

        public static List<TextValue> GetTextValuesForInsuranceStatus(Type enumType)
        {
            List<TextValue> optionList = new List<TextValue>();

            foreach (object optionEnum in Enum.GetValues(enumType))
            {
                TextValue obj = new TextValue();

                string option = optionEnum.ToString();
                object optionName = HttpContext.GetGlobalResourceObject("ResourceEnums", option);//.ToString();

                if ((int)optionEnum == (int)ClaimStatus.Pending)
                    obj.Text = "Hardcopy Pending";
                else
                    obj.Text = optionName == null ? option : optionName.ToString();
                obj.Value = ((int)optionEnum).ToString();
                optionList.Add(obj);
            }

            return optionList;
        }

    }

}
