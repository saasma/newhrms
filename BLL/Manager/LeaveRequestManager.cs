﻿using System.Collections.Generic;
using System.Linq;
using DAL;
using BLL.Entity;
using Ext.Net;
using System;
using Utils.Calendar;
using Utils.Helper;
using Utils;
using BLL.BO;

namespace BLL.Manager
{
    public class LeaveRequestManager : BaseBiz
    {

        /// <summary>
        /// Set To date & others when Half or Hourly leave request is made
        /// </summary>
        /// <param name="request"></param>
        //public static void ProcessForHourlyOrHalfDaySet(LeaveRequest request)
        //{
        //    bool HasCompanyHasHourlyLeave = CommonManager.CalculationConstant.CompanyHasHourlyLeave.Value;

        //    // If hourly or half day then set To Date = From Date
        //    if ((HasCompanyHasHourlyLeave && request.IsHour.Value) ||
        //        (!HasCompanyHasHourlyLeave && request.IsHalfDayLeave.Value))
        //    {

        //        request.ToDate = request.FromDate;
        //        request.ToDateEng = request.FromDateEng;

        //    }
        //    else
        //    {

        //        request.DaysOrHours = (request.ToDateEng.Value - request.FromDateEng.Value).Days + 1;

        //    }
        //}

        public static bool IsCancelRequestNotCreatedForLeaveRequest(int leaveRequestId)
        {
            if (PayrollDataContext.LeaveRequests.Any(x => x.LeaveRequestRef_Id == leaveRequestId))
                return false;
            return true;
        }
        public static ResponseStatus ValidateLeaveRequestDelete(int leaveRequestId)
        {
            LeaveRequest dbEntity = null;
            dbEntity = LeaveAttendanceManager.GetLeaveRequestById(leaveRequestId);
            PayrollPeriod payrollPeriod = CommonManager.GetLastPayrollPeriod();
            // Set if attendance is saved for this Period for this Employee 
            bool isAttendanceSaveForThisPeriod = LeaveAttendanceManager.IsAttendanceSavedForEmployee(payrollPeriod.PayrollPeriodId, dbEntity.EmployeeId);

            return ValidateCommon(leaveRequestId, payrollPeriod, isAttendanceSaveForThisPeriod,true);
        }

        public static ResponseStatus ValidateCommon(int LeaveRequestId,PayrollPeriod payrollPeriod, bool isAttendanceSaveForThisPeriod
            ,bool isDelete)
        {
           
            ResponseStatus status = new ResponseStatus();        

            LeaveRequest dbEntity = null;
            dbEntity = LeaveAttendanceManager.GetLeaveRequestById(LeaveRequestId);
            if (dbEntity != null)
            {
                LeaveRequestStatusEnum leaveStatus = (LeaveRequestStatusEnum)dbEntity.Status.Value;
               
                // For delete, Request/ReDraft leave can be deleted at any time
                if (!isDelete)
                {
                    // Check if leave is past one then also not editable
                    if ((isAttendanceSaveForThisPeriod && dbEntity.FromDateEng <= payrollPeriod.EndDateEng) ||
                        (!isAttendanceSaveForThisPeriod && dbEntity.FromDateEng < payrollPeriod.StartDateEng))
                    {
                        status.ErrorMessage = "Past leaves cannot be edited.";
                        status.IsSuccess = "false";
                        return status;
                    }
                }
            }

            return status;
        }

        public static bool IsAttendanceSavedForLeaveRequest(LeaveRequest leave)
        {
            return PayrollDataContext.CheckWhetherAttendanceIsSavedOrNot(leave.FromDateEng,
                SessionManager.CurrentCompanyId, leave.EmployeeId).Value;
        }
        public static bool IsTimesheetSavedForLeaveRequest(LeaveRequest leave)
        {
            return PayrollDataContext.CheckWhetherTimesheetIsSavedOrNot(leave.FromDateEng,
                SessionManager.CurrentCompanyId, leave.EmployeeId).Value;
        }
        public static List<PayrollPeriod> GetSavePayrollListForLeaveRequest(int employeeId,DateTime leaveStartDate)
        {
            return

                    (
                    from p in PayrollDataContext.PayrollPeriods
                    join a in PayrollDataContext.Attendences on p.PayrollPeriodId equals a.PayrollPeriodId
                    where a.EmployeeId == employeeId && 
                    (
                         p.EndDateEng >= leaveStartDate// && p.EndDateEng <=  
                    )
                    orderby p.PayrollPeriodId
                    select p
                    
                    ).Distinct().ToList();
        }

        public static Status AssignValidateParentLeave(LeaveRequest sourceLeave,
            LeaveRequest leave, int parentLeaveTypeId, PayrollPeriod payrollPeriod, int daysDiff, int remainingMonth)
        {

            Status status = new Status();


            // Note : Holiday will always be counted as leave for this group type
            //HolidayManager hmanager = new HolidayManager();
            //List<GetHolidaysForAttendenceResult> holiday = hmanager.GetMonthsHolidaysForAttendence(0, 0, SessionManager.CurrentCompanyId, 0, leave.FromDateEng, leave.ToDateEng).ToList();

            int forcedLeaveId=0, carryForwardId=0;
            double forcedRemBalance, carryForwardRemBalance;

           
           

            List<LLeaveType> childLeaves = PayrollDataContext.LLeaveTypes.Where(x => x.ParentLeaveTypeId == parentLeaveTypeId).ToList();

            // 1. Set Forced leavetype id
            if (childLeaves.Any(x => x.IsForceLeaveIncluded != null && x.IsForceLeaveIncluded.Value))
            {
                forcedLeaveId = childLeaves.FirstOrDefault(
                    x => x.IsForceLeaveIncluded != null && x.IsForceLeaveIncluded.Value).LeaveTypeId;
            }
            else if(childLeaves.Any(x=>x.UnusedLeave==UnusedLeave.LAPSE))
            {
                forcedLeaveId = childLeaves.FirstOrDefault(x => x.UnusedLeave == UnusedLeave.LAPSE).LeaveTypeId;
            }

            // 2. Carry forward leave
            carryForwardId =
                childLeaves.FirstOrDefault(
                    x => (x.IsForceLeaveIncluded == null || x.IsForceLeaveIncluded == false)
                        && x.UnusedLeave != UnusedLeave.LAPSE).LeaveTypeId;

            // 3. check for leave balance.
            GetLeaveListAsPerEmployeeResult forcedleaveBalance = LeaveAttendanceManager.
                GetLeaveListAsPerEmployee(forcedLeaveId, leave.EmployeeId, payrollPeriod.PayrollPeriodId, SessionManager.CurrentCompanyId, daysDiff, remainingMonth, true,leave.LeaveRequestId);
            //check for leave balance.
            GetLeaveListAsPerEmployeeResult carryleaveBalance = LeaveAttendanceManager.
                GetLeaveListAsPerEmployee(carryForwardId, leave.EmployeeId, payrollPeriod.PayrollPeriodId, SessionManager.CurrentCompanyId, daysDiff, remainingMonth, true, leave.LeaveRequestId);

            if (forcedleaveBalance.NewBalance == null)
                forcedleaveBalance.NewBalance = 0;

            if (carryleaveBalance.NewBalance == null)
                carryleaveBalance.NewBalance = 0;



            //List<LeaveDetail> leaveEachDayList = LeaveAttendanceManager.
            //    GetLeaveDetail(leave, new EmployeeManager().GetById(empId),
            //    LeaveAttendanceManager.GetLeaveRequestCountHolidayAsLeaveSetting(actualLeaveTypeId), holiday);


            leave.DaysOrHours = (double)(leave.ToDateEng.Value - leave.FromDateEng.Value).Days + 1;
            
            // don't validate for Sunday leave here as it will be validated for one per month



            if ((forcedleaveBalance.NewBalance + carryleaveBalance.NewBalance - leave.DaysOrHours) < 0)
            {
                status.ErrorMessage = "You do not have any leaves of this type left, remaining balance is " +
                    (forcedleaveBalance.NewBalance + carryleaveBalance.NewBalance)
                    + " after deducting pending leave request also.";
               
                return status;
            }

            List<LeaveDetail> details = new List<LeaveDetail>();

            // if force balance is enough
            if (leave.DaysOrHours <= forcedleaveBalance.NewBalance)
            {
                leave.ChildLeaveTypeId = forcedLeaveId;
                leave.ChildLeaveTypeId2 = null;

                // all forced leave
                for (int i = 0; i <= (leave.ToDateEng.Value - leave.FromDateEng.Value).Days; i++)
                {
                    DateTime date = leave.FromDateEng.Value.AddDays(i);
                    LeaveDetail ld = new LeaveDetail();
                    ld.DateOn = GetAppropriateDate(date);
                    ld.DateOnEng = date;
                    ld.LeaveTypeId = leave.ChildLeaveTypeId;
                    leave.LeaveDetails.Add(ld);
                }


            }
            else
            {
                leave.ChildLeaveTypeId = forcedLeaveId;
                leave.ChildLeaveTypeId2 = carryForwardId;

                // all forced leave
                int forceDay = 0;
                

                // 1. Forced day
                for (int i = 0; i <= (leave.ToDateEng.Value - leave.FromDateEng.Value).Days; i++)
                {
                    DateTime date = leave.FromDateEng.Value.AddDays(i);
                    LeaveDetail ld = new LeaveDetail();
                    ld.DateOn = GetAppropriateDate(date);
                    ld.DateOnEng = date;
                    ld.LeaveTypeId = leave.ChildLeaveTypeId;
                    leave.LeaveDetails.Add(ld);

                    forceDay += 1;

                    if (forceDay <= forcedleaveBalance.NewBalance)
                        continue;
                    else
                        ld.LeaveTypeId = leave.ChildLeaveTypeId2;
                }

                //// 2. Carry forward day
                //for (; i <= (leave.ToDateEng.Value - leave.FromDateEng.Value).Days; i++)
                //{
                //    DateTime date = leave.FromDateEng.Value.AddDays(i);
                //    LeaveDetail ld = new LeaveDetail();
                //    ld.DateOn = GetAppropriateDate(date);
                //    ld.DateOnEng = date;
                //    ld.LeaveTypeId = leave.ChildLeaveTypeId2;
                //    leave.LeaveDetails.Add(ld);

                    
                //}
            }



            return status;
        }

        /// <summary>
        /// Converts the Calendar Event into LeaveRequest
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="leave"></param>
        /// <param name="isInsert"></param>
        /// <returns></returns>
        public static ResponseStatus GetLeaveRequestFromCalendarEntity(DAL.LeaveRequest entity, LeaveRequest leave, bool isInsert, int employeeId)
        {
            
            //int employeeId = SessionManager.CurrentLoggedInEmployeeId;
            leave.EmployeeId = employeeId;
            entity.EmployeeId = employeeId;

            ResponseStatus status = new ResponseStatus();
            LeaveAttendanceManager leaveMgr = new LeaveAttendanceManager();
            EEmployee employee = EmployeeManager.GetEmployeeById(employeeId);
            PayrollPeriod payrollPeriod = CommonManager.GetLastPayrollPeriod();
            DateTime date;

            LLeaveType leaveType = leaveMgr.GetLeaveType(entity.LeaveTypeId);

            int daysDiff = 0; int remainingMonth = 0;
            if (payrollPeriod != null && payrollPeriod.Month != SessionManager.CurrentCompany.LeaveStartMonth.Value)
            {
                daysDiff = LeaveAttendanceManager.GetYearlyLeaveMiddleJoiningDays(payrollPeriod, ref remainingMonth);
            }


            if (string.IsNullOrEmpty(entity.FromDate) || DateTime.TryParse(entity.FromDate, out date) == false)
            {
                status.ErrorMessage = "Start date is not correct.";
                status.IsSuccess = "false";
                return status;
            }

            // 2. Half Day
            leave.IsHalfDayLeave = entity.IsHalfDayLeave;

            // 3. Set Dates
            leave.FromDateEng = new DateTime(date.Year, date.Month, date.Day);
            if (entity.IsHalfDayLeave == true)
            {
                leave.ToDateEng = leave.FromDateEng;
            }
            else
            {
                if (string.IsNullOrEmpty(entity.ToDate) || DateTime.TryParse(entity.ToDate, out date) == false)
                {
                    status.ErrorMessage = "End date is not correct.";
                    status.IsSuccess = "false";
                    return status;
                }
                leave.ToDateEng = new DateTime(date.Year, date.Month, date.Day);
            }

            // validate date lock exceeding case
            if (CommonManager.Setting.DateLockForLeaveChangeFromEmployee != null && leave.FromDateEng != null && SessionManager.CurrentLoggedInEmployeeId != 0)
            {
                if (leave.FromDateEng.Value.Date <= CommonManager.Setting.DateLockForLeaveChangeFromEmployee.Value.Date)
                {
                    string lockDate = GetAppropriateDate(CommonManager.Setting.DateLockForLeaveChangeFromEmployee.Value);
                    if(IsEnglish==false)
                    {
                        lockDate += " (" + CommonManager.Setting.DateLockForLeaveChangeFromEmployee.Value.ToString("yyyy-MMM-dd") + ")";
                    }
                    status.ErrorMessage = "Requesting leave start date should be after " + lockDate + ", please contact HR for details.";
                    status.IsSuccess = "false";
                    return status;
                }
            }

            // For non-group leave, child leave could not be selected
            if (leaveType != null && leaveType.IsParentGroupLeave != null && leaveType.IsParentGroupLeave.Value)
            {
                //// auto calculation of Child leave in case of Group Type
                // Assign and validate Group leave

                Status balanceFail = 
                    AssignValidateParentLeave(entity, leave, leaveType.LeaveTypeId, payrollPeriod, daysDiff, remainingMonth);

                if (balanceFail.IsSuccess == false)
                {
                    status.ErrorMessage = balanceFail.ErrorMessage;
                    status.IsSuccess = "false";
                    return status;
                }

             

            }
            else
            {
                leave.ChildLeaveTypeId = null;
                entity.ChildLeaveTypeId = null;
                leave.ChildLeaveTypeId2 = null;
                entity.ChildLeaveTypeId2 = null;
            }


            // 1. Leave Type
            // If Unpaid Leave selected then LeaveTypeId will be 0
            if (entity.LeaveTypeId == -1)
                leave.LeaveTypeId = 0;
            else
                leave.LeaveTypeId = entity.LeaveTypeId;

            if (entity.LeaveTypeId2 != null && (entity.IsHalfDayLeave == null || entity.IsHalfDayLeave == false))
            {
                if (entity.LeaveTypeId2 == -1)
                    leave.LeaveTypeId2 = 0;
                else
                    leave.LeaveTypeId2 = entity.LeaveTypeId2;

                leave.Days1 = entity.Days1;
                leave.Days2 = entity.Days2;

                if (leave.LeaveTypeId == leave.LeaveTypeId2)
                {
                    status.ErrorMessage = "Please select diferent leaves in the leave type boxes.";
                    status.IsSuccess = "false";
                    return status;
                }
            }

            // Validate Days should exists if multiple leave selected
            if (entity.LeaveTypeId2 != null)
            {
                if (entity.Days1 == null || entity.Days1 == 0)
                {
                    status.ErrorMessage = "Please enter days for both the leaves or un-select second leave.";
                    status.IsSuccess = "false";
                    return status;
                }
                if (entity.Days2 == null || entity.Days2 == 0)
                {
                    status.ErrorMessage = "Please enter days for both the leaves or un-select second leave.";
                    status.IsSuccess = "false";
                    return status;
                }
            }

            


            // 4. Set Hour or Half Day 
            leave.IsHour = false;
            if (CommonManager.CalculationConstant.CompanyHasHourlyLeave.Value == true)
                leave.IsHour = true;           

            if (CommonManager.CalculationConstant.CompanyHasHourlyLeave.Value)
            {
                //leave.DaysOrHours = 0;// double.Parse(entity.Url.Split(';')[0].ToString() == "" ? "0" : entity.Url.Split(';')[0].ToString());
                if (leave.DaysOrHours == null)
                    leave.DaysOrHours = entity.DaysOrHours;

                //if (!string.IsNullOrEmpty(leave.StartTime) && !string.IsNullOrEmpty(leave.EndTime))
                //{
                //     TimeSpan startTime = new TimeSpan(DateTime.Parse(leave.StartTime).Hour, DateTime.Parse(leave.StartTime).Minute, 0);
                //    TimeSpan endTime = new TimeSpan(DateTime.Parse(leave.EndTime).Hour, DateTime.Parse(leave.EndTime).Minute, 0);

                //    DateTime startDateTime = new DateTime(leave.FromDateEng.Value.Year, leave.FromDateEng.Value.Month, leave.FromDateEng.Value.Day,
                //        startTime.Hours, startTime.Minutes, 0);
                //    DateTime endDateTime = new DateTime(leave.ToDateEng.Value.Year, leave.ToDateEng.Value.Month, leave.ToDateEng.Value.Day,
                //      endTime.Hours, endTime.Minutes, 0);


                //    if (endDateTime <= startDateTime)
                //    {
                //        status.ErrorMessage = "End date and time should be greater than the start date and time.";
                //        status.IsSuccess = "false";
                //        return status;
                //    }

                //    //TimeSpan time1 = new TimeSpan((endDateTime - startDateTime).Hours, (endDateTime - startDateTime).Minutes, 0);
                //    //TimeSpan time2 = TimeSpan.FromHours(leave.DaysOrHours.Value);

                //   // double t1 = double.Parse(string.Format("{0:0.#}", (endDateTime - startDateTime).TotalHours));
                //   // double t2 = double.Parse(string.Format("{0:0.#}", leave.DaysOrHours.Value));

                //   //if (t1!=t2)
                //   // {
                //   //     status.ErrorMessage = "Hours typed and start/end time does not match,"
                //   //         + " hour/min generated from the date and time difference is " + t1;
                //   //     status.IsSuccess = "false";
                //   //     return status;
                //   // }
                //}
                
            }
            else if (leave.IsHalfDayLeave.Value)
            {
                leave.ToDateEng = leave.FromDateEng;
                leave.DaysOrHours = (double)0.5;
                leave.LeaveTypeId2 = null;
                leave.Days2 = 0;
                leave.HalfDayType = entity.HalfDayType;
            }
            // If not Hour or Half day then it must be more than one day leave
            else
            {
                leave.DaysOrHours = (double)(leave.ToDateEng.Value - leave.FromDateEng.Value).Days + 1;
            }

            // Validate total days = day1 + day2
            if (leave.LeaveTypeId2 != null)
            {
                if (leave.DaysOrHours != (leave.Days1 + leave.Days2))
                {
                    status.ErrorMessage = "The total leave days is not equal to the sum of two leave days.";
                    status.IsSuccess = "false";
                    return status;
                }
            }

            if (leave.FromDateEng == DateTime.MinValue)
            {
                status.ErrorMessage = "Invalid Start date.";
                status.IsSuccess = "false";
                return status;
            }
            if (leave.ToDateEng == DateTime.MinValue)
            {
                status.ErrorMessage = "Invalid End date.";
                status.IsSuccess = "false";
                return status;
            }

            leave.FromDate = GetAppropriateDate(leave.FromDateEng.Value);
            leave.ToDate = GetAppropriateDate(leave.ToDateEng.Value);

            leave.Reason = entity.Reason.Trim();
            if (leave.Reason.Length > 500)
            {
                status.ErrorMessage = "Pleae write a shorter reason.";
                status.IsSuccess = "false";
                return status;
            }

            if (isInsert)
                leave.CreatedOn = GetCurrentDateAndTime();
            leave.ModifiedOn = GetCurrentDateAndTime();
            leave.Status = (int)LeaveRequestStatusEnum.Request;

         
            if (isInsert)
            {
                leave.EditSequence = 1;
            }
            else
            {
                //leave.EditSequence = int.Parse(entity.Url.Split(';')[1].ToString()) + 1;

                LeaveRequest dbLeaveRequest = LeaveAttendanceManager.GetLeaveRequestById(leave.LeaveRequestId);
                if (dbLeaveRequest != null)
                {
                    LeaveRequestStatusEnum dbLeaveStatus = (LeaveRequestStatusEnum)dbLeaveRequest.Status.Value;
                    // In update mode, as Request & Re-Draft leave can only be changed
                    if (dbLeaveStatus != LeaveRequestStatusEnum.ReDraft && dbLeaveStatus != LeaveRequestStatusEnum.Request)
                    {
                        status.ErrorMessage =
                            string.Format("Leave is in \"{0}\" status, only \"Request\" status leave can be changed.", dbLeaveStatus.ToString());
                        status.IsSuccess = "false";
                        return status;
                    }

                    // Check if the LeaveRequest is associated with the CurrentEmployee
                    if (dbLeaveRequest.EmployeeId != employeeId)
                    {
                        string empName = new EmployeeManager().GetById(employeeId).Name;
                        status.ErrorMessage =
                          string.Format("This Leave is not applicable to \"{0}\".", empName);
                        status.IsSuccess = "false";
                        return status;
                    }
                }
            }
            //#region "Leave Request Validation"

            //bool isLeaveValid = false;

            // If has balance then allow it
            //LeaveAttendanceManager lm = new LeaveAttendanceManager();
            //List<EmployeeLeaveListResult> list = lm.GetEmployeeLeaveList(employeeId);
            //foreach (EmployeeLeaveListResult leavelist in list)
            //{
            //    if (leavelist.LeaveTypeId == leave.LeaveTypeId && leavelist.IsActive==false)
            //    {
            //        status.ErrorMessage = "Leave has been disabled, please refresh the page.";
            //        status.IsSuccess = "false";
            //        return status;
            //    }

            //    if (leavelist.LeaveTypeId == leave.LeaveTypeId)
            //    {
            //        if (leavelist.IsHalfDayAllowed == false)
            //            leave.IsHalfDayLeave =false;
            //        isLeaveValid = true;
            //        break;
            //    }
            //}
            // Unpaid Leave will have Id = 0
            //if (leave.LeaveTypeId == 0)
            //    isLeaveValid = true;

            //if (!isLeaveValid)
            //{
            //    status.ErrorMessage = "Leave is not associated with you or has been disabled.";
            //    status.IsSuccess = "false";
            //    return status;
            //}

            if ((leave.ToDateEng.Value - leave.FromDateEng.Value).Days < 0)
            {
                status.ErrorMessage = "To date must be same or later then the start date.";
                status.IsSuccess = "false";
                return status;
            }

           
           


            // new leave request start date can not be < DateTime.Now

            int pastLeaveRequestDays = CommonManager.CompanySetting.LeaveRequestMinimumPastDays;
           
            if (leaveType != null && leaveType.PastDaysForLeaveRequest != null)
                pastLeaveRequestDays = leaveType.PastDaysForLeaveRequest.Value;


            date = GetCurrentDateAndTime();
            DateTime currentDateOnly = (new DateTime(date.Year, date.Month, date.Day))
                .AddDays(pastLeaveRequestDays * -1);

            // for HR assigning leave we do not need to validate as hr can assign in any day
            if (SessionManager.CurrentLoggedInEmployeeId != 0)
            {

                if (isInsert && leave.FromDateEng < currentDateOnly)
                {
                    if (pastLeaveRequestDays > 0)
                        status.ErrorMessage =
                               string.Format("You cannot request leave for a date earlier than " + pastLeaveRequestDays + " days.");
                    else
                        status.ErrorMessage = "You cannot apply for the leave on past date.";
                    status.IsSuccess = "false";
                    return status;
                }

                else if (!isInsert && leave.FromDateEng < currentDateOnly)
                {
                    status.ErrorMessage =
                           string.Format("Past Leave Request can not be changed.");
                    status.IsSuccess = "false";
                    return status;

                }


                int futureLeaveRequestDays = CommonManager.CompanySetting.LeaveRequestValidMonths * 30; // convert month to days

                if (leaveType != null && leaveType.FutureDaysForLeaveRequest != null)
                    futureLeaveRequestDays = leaveType.FutureDaysForLeaveRequest.Value;
                

                // Check the valid months for the month range, default is for 2 month, 0 or -1 means no restriction
                if (futureLeaveRequestDays >= 0)
                {

                    //DateTime startDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
                    DateTime endDate = GetCurrentDateAndTime().AddDays(futureLeaveRequestDays); //GetMonthsFromStartDate(startDate, CommonManager.CompanySetting.LeaveRequestValidMonths);

                    // If leave start & end date must lie between payroll start date & months end date
                    if ((leave.FromDateEng >= currentDateOnly && leave.FromDateEng <= endDate)
                        &&
                        (leave.ToDateEng >= currentDateOnly && leave.ToDateEng <= endDate)) { }
                    else
                    {
                        if(futureLeaveRequestDays==0)
                            status.ErrorMessage = string.Format("Leave can be requested only within today.");
                        else
                            status.ErrorMessage = string.Format("Leave can be requested only within {0} days.", futureLeaveRequestDays);
                        status.IsSuccess = "false";
                        return status;
                    }
                }

            }


            if (leave.Reason.Equals(string.Empty))
            {
                status.ErrorMessage = "Please write a reason for the leave.";
                status.IsSuccess = "false";
                return status;
            }

            // Compensatory leave validation
            if (leaveType != null && leave.LeaveTypeId2 != null &&
                ( leaveType.FreqOfAccrual == LeaveAccrue.COMPENSATORY ||
                    (                        
                        leave.LeaveTypeId2 != 0 &&
                        LeaveAttendanceManager.GetLeaveTypeById(leave.LeaveTypeId2.Value).FreqOfAccrual == LeaveAccrue.COMPENSATORY
                    )
                )
            )
            {
                status.ErrorMessage = "Compensatory leave can not be mixed with other leave on leave requests.";
                status.IsSuccess = "false";
                return status;
            }

            // Don't allow half day with Compensatory leave
            //if (leaveType != null && leaveType.LeaveTypeId != 0 && leaveType.FreqOfAccrual == LeaveAccrue.COMPENSATORY
            //    && leave.IsHalfDayLeave != null && leave.IsHalfDayLeave.Value)
            //{

            //}


            // Compensatory leave validation
            if (leaveType != null && leave.LeaveTypeId2 != null &&
                (leaveType.FreqOfAccrual == LeaveAccrue.MANUALLY ||
                    (
                        leave.LeaveTypeId2 != 0 &&
                        LeaveAttendanceManager.GetLeaveTypeById(leave.LeaveTypeId2.Value).FreqOfAccrual == LeaveAccrue.MANUALLY
                    )
                )
            )
            {
                status.ErrorMessage = "Mannual leave can not be mixed with other leave on leave requests.";
                status.IsSuccess = "false";
                return status;
            }

            if (leaveType != null && leaveType.FreqOfAccrual == LeaveAccrue.COMPENSATORY)
            {
                if (leave.CompensatorIsAddType == null)
                {
                    status.ErrorMessage = "For \"" + leaveType.Title + "\", Add or Deduct type must be selected.";
                    status.IsSuccess = "false";
                    return status;
                }
            }
            
            double firstLeaveTaken;
            //TODO: remaining

            GetLeaveListAsPerEmployeeResult leaveBalance = null;
            


            #region "Dish home Sunday Leave"

            if (CommonManager.CompanySetting.WhichCompany == WhichCompany.DishHome)
            {
                // if Sunday leave is selected as Seconday the show validation message
                if (leave.LeaveTypeId2 != null && LeaveAttendanceManager.IsDishHomeSundayLeave(leave.LeaveTypeId2.Value))
                {
                    status.ErrorMessage = "\"Sunday leave\" should be selected as primary leave.";
                    status.IsSuccess = "false";
                    return status;
                }

                // if Sunday leave is mixed with other leave then that other leave must be half day enabled
                if (LeaveAttendanceManager.IsDishHomeSundayLeave(leave.LeaveTypeId) && leave.LeaveTypeId2 != null)
                {
                    if (leave.LeaveTypeId2 != 0 && LeaveAttendanceManager.GetLeaveTypeById(leave.LeaveTypeId2.Value).IsHalfDayAllowed == false)
                    {
                        status.ErrorMessage = "\"" + LeaveAttendanceManager.GetLeaveTypeById(leave.LeaveTypeId2.Value).Title 
                            + "\" is not allowed to take as half day.";
                        status.IsSuccess = "false";
                        return status;
                    }
                }

                //if second leave is not UPL then check if has balance
                if (LeaveAttendanceManager.IsDishHomeSundayLeave(leave.LeaveTypeId) && leave.LeaveTypeId2 != null)
                {
                    if (leave.LeaveTypeId2 != 0)
                    {
                        //check for leave balance.
                        leaveBalance =
                            LeaveAttendanceManager.GetLeaveListAsPerEmployee(leave.LeaveTypeId2.Value, leave.EmployeeId, payrollPeriod.PayrollPeriodId, SessionManager.CurrentCompanyId, daysDiff, remainingMonth, true, leave.LeaveRequestId);
                        if ((leaveBalance.NewBalance - (double)leave.Days2) < 0)
                        {
                            status.ErrorMessage = "You do not have any balance left for " + LeaveAttendanceManager.GetLeaveTypeById(leave.LeaveTypeId2.Value).Title + ".";
                            status.IsSuccess = "false";
                            return status;
                        }
                    }
               } 


                if (LeaveAttendanceManager.IsDishHomeSundayLeave(leave.LeaveTypeId))
                {

                    if (leave.DaysOrHours != 3)
                    {
                        status.ErrorMessage = "\"Sunday leave\" can be taken for consecutive 3 days only.";
                        status.IsSuccess = "false";
                        return status;
                    }
                   

                    DateTime sundayLeaveDate = leave.ToDateEng.Value;
                    CustomDate sundayNepDate = new CustomDate(sundayLeaveDate.Day, sundayLeaveDate.Month, sundayLeaveDate.Year, true);
                    sundayNepDate = CustomDate.ConvertEngToNep(sundayNepDate);
                    CustomDate sundayStart = new CustomDate(1, sundayNepDate.Month, sundayNepDate.Year, false);
                    CustomDate sundayEnd = new CustomDate(DateHelper.GetTotalDaysInTheMonth(sundayNepDate.Year, sundayNepDate.Month, false)
                        , sundayNepDate.Month, sundayNepDate.Year, false);

                    DateTime sundayStartEng = sundayStart.EnglishDate;
                    DateTime sundayEndEng = sundayEnd.EnglishDate;

                    if (
                      (from lr in PayrollDataContext.LeaveRequests
                       join ld in PayrollDataContext.LeaveDetails on lr.LeaveRequestId equals ld.LeaveRequestId
                       where lr.EmployeeId == leave.EmployeeId && lr.LeaveTypeId == LeaveAttendanceManager.GetSundayLeaveId()
                       && ld.DateOnEng >= sundayStartEng && ld.DateOnEng <= sundayEndEng
                       && lr.Status != (int)LeaveRequestStatusEnum.Cancelled
                       && lr.Status != (int)LeaveRequestStatusEnum.Denied
                       select ld.LeaveDetailId
                      ).Any())
                    {
                        status.ErrorMessage =
                            string.Format("\"Sunday leave\" request already exists for the month \"{0}({1} - {2})\".",
                               DateHelper.GetMonthName(sundayStart.Month, false),
                               sundayStartEng.ToString("MMMM d"),
                               sundayEndEng.ToString("MMMM d")
                               );
                        status.IsSuccess = "false";
                        return status;
                    }
                    
                }
            }
            #endregion


            if (CommonManager.CompanySetting.LeaveRequestNegativeLeaveReqBalanceNotAllowed &&
                (leaveType != null && (leaveType.IsParentGroupLeave == null || leaveType.IsParentGroupLeave==false)))
            {



           // #endregion


                firstLeaveTaken = 0;

                status = ValidateNegativeLeaveBalance(entity, leave, payrollPeriod, 
                        ref firstLeaveTaken, ref leaveBalance, daysDiff, remainingMonth);

                if (status.IsSuccessType == false)
                    return status;

            }


            if ((leaveType != null && (leaveType.IsParentGroupLeave == null || leaveType.IsParentGroupLeave == false)))
            {
                // Check Min/Max for each Leave Balance
                firstLeaveTaken = leave.LeaveTypeId2 != null ? leave.Days1.Value : leave.DaysOrHours.Value;
                string minMaxBalance = ValidateLeaveMinMaxBalance(leave.ChildLeaveTypeId != null ? leave.ChildLeaveTypeId.Value : leave.LeaveTypeId, firstLeaveTaken, leave);
                if (!string.IsNullOrEmpty(minMaxBalance))
                {
                    status.ErrorMessage = minMaxBalance;
                    status.IsSuccess = "false";
                    return status;
                }
                if (leave.LeaveTypeId2 != null)
                {
                    minMaxBalance = ValidateLeaveMinMaxBalance(leave.LeaveTypeId2.Value, leave.Days2.Value, leave);
                    if (!string.IsNullOrEmpty(minMaxBalance))
                    {
                        status.ErrorMessage = minMaxBalance;
                        status.IsSuccess = "false";
                        return status;
                    }
                }

                string orderValidation = ValidateForLeaveOrder(leave);
                if (!string.IsNullOrEmpty(orderValidation))
                {
                    status.ErrorMessage = orderValidation;
                    status.IsSuccess = "false";
                    return status;
                }
            }

            //#endregion

            if (LeaveAttendanceManager.IsLeaveRequestMadeOnDay(leave))
            {

                status.ErrorMessage = "You have already applied for leave on this date.";
                status.IsSuccess = "false";
            }


            return status;



        }

        private static ResponseStatus ValidateNegativeLeaveBalance(DAL.LeaveRequest entity, LeaveRequest leave, PayrollPeriod payrollPeriod, 
            ref double firstLeaveTaken, ref GetLeaveListAsPerEmployeeResult leaveBalance, int daysDiff, int remainingMonth)
        {

            LLeaveType leaveType = LeaveAttendanceManager.GetLeaveTypeById(leave.LeaveTypeId);

            ResponseStatus status = new ResponseStatus();

            int actualLeaveTypeId = entity.ChildLeaveTypeId != null ? entity.ChildLeaveTypeId.Value : entity.LeaveTypeId;

            // Allow negative balance for Compensatory leave for NIBL as there is no add feature
            if (CommonManager.CompanySetting.WhichCompany == WhichCompany.NIBL 
                && leaveType != null && leaveType.FreqOfAccrual == LeaveAccrue.COMPENSATORY)
            {
                return status;
            }

            // Allow for Compensatory leave if Add type
            if (leaveType != null && leaveType.FreqOfAccrual == LeaveAccrue.COMPENSATORY &&
                leave.CompensatorIsAddType != null && leave.CompensatorIsAddType.Value)
            {
                return status;
            }

            if (leaveType != null && leave.LeaveTypeId != 0
                && leave.LeaveTypeId2 == null //&& leaveType.FreqOfAccrual != LeaveAccrue.COMPENSATORY
                && (leaveType.AllowNegativeLeaveRequest == null || leaveType.AllowNegativeLeaveRequest == false)
                && LeaveAttendanceManager.IsDishHomeSundayLeave(leave.LeaveTypeId) == false
                )
            {
               
                HolidayManager hmanager = new HolidayManager();
                List<GetHolidaysForAttendenceResult> holiday = hmanager.GetMonthsHolidaysForAttendence(0, 0, SessionManager.CurrentCompanyId, 0, leave.FromDateEng, leave.ToDateEng, leave.EmployeeId).ToList();
                bool onlyCountPublicHolidayAsLeaveWeeklySaturdayIsHoliday = false;
                bool onlyCountWeeklySaturdayAsLeavePublicHolidayIsHoliday = false;
                bool countHolidayAsLeave = LeaveAttendanceManager.GetLeaveRequestCountHolidayAsLeaveSetting(actualLeaveTypeId, ref onlyCountPublicHolidayAsLeaveWeeklySaturdayIsHoliday,ref onlyCountWeeklySaturdayAsLeavePublicHolidayIsHoliday);
                List<LeaveDetail> leaveEachDayList = LeaveAttendanceManager.
                    GetLeaveDetail(leave, new EmployeeManager().GetById(entity.EmployeeId),
                    countHolidayAsLeave,onlyCountPublicHolidayAsLeaveWeeklySaturdayIsHoliday,onlyCountWeeklySaturdayAsLeavePublicHolidayIsHoliday, holiday);

                //check for leave balance.
                leaveBalance = LeaveAttendanceManager.
                    GetLeaveListAsPerEmployee(actualLeaveTypeId, leave.EmployeeId, payrollPeriod.PayrollPeriodId, SessionManager.CurrentCompanyId, daysDiff, remainingMonth, true, leave.LeaveRequestId);

                firstLeaveTaken = entity.IsHalfDayLeave != null && entity.IsHalfDayLeave.Value ? 0.5 : leaveEachDayList.Count;

                // don't validate for Sunday leave here as it will be validated for one per month



                if ((leaveBalance.NewBalance - firstLeaveTaken) < 0)
                {
                    status.ErrorMessage = "You do not have any leaves of this type left, remaining balance is " +
                        leaveBalance.NewBalance + " only after deducting pending leave request.";
                    status.IsSuccess = "false";
                    return status;
                }


            }
            //#endregion                        
            else if (leaveType != null && leaveType.AllowNegativeLeaveRequest != null && leaveType.AllowNegativeLeaveRequest.Value)
            {
            }
            else if (leaveType != null)// && leaveType.FreqOfAccrual != LeaveAccrue.COMPENSATORY)
            {
                //check for leave balance.
                leaveBalance =
                    LeaveAttendanceManager.GetLeaveListAsPerEmployee(actualLeaveTypeId, leave.EmployeeId, payrollPeriod.PayrollPeriodId, SessionManager.CurrentCompanyId, daysDiff, remainingMonth, true, leave.LeaveRequestId);

                if (leave.LeaveTypeId != 0)
                {
                    firstLeaveTaken = leave.LeaveTypeId2 != null ? leave.Days1.Value : leave.DaysOrHours.Value;

                    // don't validate for Sunday leave here as it will be validated for one per month
                    if (LeaveAttendanceManager.IsDishHomeSundayLeave(leave.LeaveTypeId) == false)
                    {

                        if ((leaveBalance.NewBalance - firstLeaveTaken) < 0)
                        {
                            status.ErrorMessage = "You do not have any leaves of this type left.";
                            status.IsSuccess = "false";
                            return status;
                        }
                    }
                }
            }


            if (leave.LeaveTypeId2 != null && leave.LeaveTypeId2 != 0)
            {
                LLeaveType otherLeave = LeaveAttendanceManager.GetLeaveTypeById(leave.LeaveTypeId2.Value);

                if (otherLeave.AllowNegativeLeaveRequest != null && otherLeave.AllowNegativeLeaveRequest.Value)
                {
                }
                else// if (leaveType.FreqOfAccrual != LeaveAccrue.COMPENSATORY)
                {
                    //check for leave balance.
                    leaveBalance =
                        LeaveAttendanceManager.GetLeaveListAsPerEmployee(leave.LeaveTypeId2.Value, leave.EmployeeId, payrollPeriod.PayrollPeriodId, SessionManager.CurrentCompanyId, daysDiff, remainingMonth, true, leave.LeaveRequestId);
                    if ((leaveBalance.NewBalance - (double)leave.Days2) < 0)
                    {
                        status.ErrorMessage = "You do not have any leaves of this type left.";
                        status.IsSuccess = "false";
                        return status;
                    }
                }
            }
            return status;
        }

        public static int? GetLeaveRecommenderEmpId(int leaverequestId)
        {
            return (
                from a in PayrollDataContext.LeaveRequests
                where a.LeaveRequestId == leaverequestId
                select a.RecommendedBy
                ).FirstOrDefault();
        }


        /// <summary>
        /// Validate for Min & Max leave can be taken for each Leave Type
        /// </summary>
        public static string ValidateLeaveMinMaxBalance(int leaveTypeId, double leaveTaken,LeaveRequest request)
        {
            // no checking for unpaid leave
            if (leaveTypeId == 0)
                return "";

            if (request != null && request.IsHalfDayLeave != null && request.IsHalfDayLeave.Value)
            {
                return "";
            }

            LLeaveType leave = LeaveAttendanceManager.GetLeaveTypeById(leaveTypeId);

            if (request.IsSpecialCase == null || request.IsSpecialCase == false)
            {
                if (leave.SingleMinRequest != null && leave.SingleMinRequest != 0)
                {
                    if (leaveTaken < leave.SingleMinRequest)
                    {
                        return leave.Title + " minimum allowable days is " + leave.SingleMinRequest + ".";
                    }
                }
            }

            if (leave.SingleMaxRequest != null && leave.SingleMaxRequest != 0)
            {
                if (leaveTaken > leave.SingleMaxRequest)
                {
                    return leave.Title + " maximum allowable days is " + leave.SingleMaxRequest + ".";
                }
            }

            return "";
        }

        
        public static String ValidateForLeaveOrder(LeaveRequest request)
        {
            if (CommonManager.CompanySetting.LeaveHasRequestOrder == false)
                return "";


            bool hasMultipleLeave = request.LeaveTypeId2 != null;
            List<GetEmployeeLeaveBalanceResult> balanceList;
            List<LLeaveType> leaveList =  LeaveAttendanceManager.GetLeaveListByCompany(SessionManager.CurrentCompanyId)
                .OrderBy(x => x.Order).ToList();

            PayrollPeriod lastPayroll = CommonManager.GetLastPayrollPeriod();
            
                int daysDiff = 0; int remainingMonth = 0;
                if (lastPayroll.Month != SessionManager.CurrentCompany.LeaveStartMonth.Value)
                {
                    daysDiff = LeaveAttendanceManager.
                        GetYearlyLeaveMiddleJoiningDays(lastPayroll, ref remainingMonth);
                }
                

                balanceList = PayrollDataContext.GetLeaveListAsPerEmployeeForOrderProcessing
                    (request.EmployeeId, lastPayroll.PayrollPeriodId, SessionManager.CurrentCompanyId, daysDiff, remainingMonth)
                    .Select(x => new GetEmployeeLeaveBalanceResult
                    {
                        LeaveTypeId = x.LeaveTypeId,
                        NewBalance = x.NewBalance == null ? 0 : (decimal)x.NewBalance
                    }).ToList();
           

            // Set Employee remaining balance & leave taken in this request in each Leave Type
            foreach (LLeaveType leave in leaveList)
            {
                GetEmployeeLeaveBalanceResult balance = balanceList.FirstOrDefault(x => x.LeaveTypeId == leave.LeaveTypeId);
                if (balance != null)
                {
                    leave.EmployeeBalanceRemaining = balance.NewBalance == null ? 0 : (double)balance.NewBalance;
                }

                if (request.LeaveTypeId == leave.LeaveTypeId)
                {
                    if (request.LeaveTypeId2 != null)
                        leave.TakenBalanceInThisRequest = request.Days1 == null ? 0 : request.Days1.Value;
                    else
                        leave.TakenBalanceInThisRequest = request.DaysOrHours == null ? 0 : request.DaysOrHours.Value;
                }

                if (request.LeaveTypeId2 != null && request.LeaveTypeId2 == leave.LeaveTypeId)
                    leave.TakenBalanceInThisRequest = request.Days2 == null ? 0 : request.Days2.Value;

               
            }


            for (int i = leaveList.Count - 1; i >= 0; i--)
            {
                LLeaveType leave = leaveList[i];
                double upperLeaveBalance = i <= 0 ? 0 : (leaveList[i - 1].EmployeeBalanceRemaining - leaveList[i - 1].TakenBalanceInThisRequest);
                // Now check/validate if Order above leave has balance & not taken then don't allow to save the current Leave Request
                if (
                    leave.TakenBalanceInThisRequest > 0
                    &&
                    i > 0
                    &&
                    upperLeaveBalance > 0
                   )
                {
                    return leaveList[i - 1].Title + " balance " + upperLeaveBalance + " has to be finished before taking another leave.";
                }

            }


            return "";
        }

    

        /// <summary>
        /// Check if the current employee can change the leave request
        /// </summary>
        public static bool CanCurrentEmployeeChangeLeaveRequestStatus(int currentEmployeeId,LeaveRequest request)
        {

            if (
                (CommonManager.Setting.LeaveRequestSettingUsingPreDefinedList != null &&
                CommonManager.Setting.LeaveRequestSettingUsingPreDefinedList.Value)
                 ||
                (CommonManager.Setting.LeaveRequestSettngUsingOneToOne != null &&
                CommonManager.Setting.LeaveRequestSettngUsingOneToOne.Value)
                )
            {
                if (PayrollDataContext.GetPreDefinedApprovalList(
                    null, request.EmployeeId,(int)PreDefindFlowType.Leave).Any(x => x.EmployeeId == currentEmployeeId))
                    return true;

                // When the Employee Supervisor or Branch is changed after leave request is being sent, then Reviewer/Approval
                // will be changed, so now the old can view the leave as it has his name then check if exists the name
                if (request.RecommendEmployeeId == currentEmployeeId ||
                    request.SupervisorEmployeeIdInCaseOfSelection == currentEmployeeId)
                {
                    return true;
                }

            }
            else
            {
                if (request.RecommendEmployeeId == currentEmployeeId ||
                   request.SupervisorEmployeeIdInCaseOfSelection == currentEmployeeId)
                {
                    return true;
                }

                LeaveApprovalEmployee approvalEmp = GetEmployeeTeam(request.EmployeeId);

                if (approvalEmp != null)
                {
                    if (approvalEmp.ApplyTo == currentEmployeeId)
                        return true;

                    if (approvalEmp.CC1 != null && approvalEmp.CC1 == currentEmployeeId)
                        return true;

                    if (approvalEmp.CC2 != null && approvalEmp.CC2 == currentEmployeeId)
                        return true;

                    if (approvalEmp.CC3 != null && approvalEmp.CC3 == currentEmployeeId)
                        return true;

                    if (approvalEmp.CC4 != null && approvalEmp.CC4 == currentEmployeeId)
                        return true;

                    if (approvalEmp.CC5 != null && approvalEmp.CC5 == currentEmployeeId)
                        return true;
                }

                LeaveApprovalEmployee approvalEmp2 = GetEmployeeOtherSecondTeam(request.EmployeeId);

                if (approvalEmp2 != null)
                {
                    if (approvalEmp2.ApplyTo == currentEmployeeId)
                        return true;

                    if (approvalEmp2.CC1 != null && approvalEmp.CC1 == currentEmployeeId)
                        return true;

                    
                }

                //bool hasLeaveProject = PayrollDataContext.LeaveProjects.Any();
                //List<GetLeaveApprovalApplyToCCForEmployeeResult> list =
                //    PayrollDataContext.GetLeaveApprovalApplyToCCForEmployee(request.EmployeeId, hasLeaveProject).ToList();

                //foreach (GetLeaveApprovalApplyToCCForEmployeeResult approvalEmp in list)
                //{
                //    if (approvalEmp.ApplyTo == currentEmployeeId)
                //        return true;

                //    if (approvalEmp.CC1 != null && approvalEmp.CC1 == currentEmployeeId)
                //        return true;

                //    if (approvalEmp.CC2 != null && approvalEmp.CC2 == currentEmployeeId)
                //        return true;

                //    if (approvalEmp.CC3 != null && approvalEmp.CC3 == currentEmployeeId)
                //        return true;

                //    if (approvalEmp.CC4 != null && approvalEmp.CC4 == currentEmployeeId)
                //        return true;

                //    if (approvalEmp.CC5 != null && approvalEmp.CC5 == currentEmployeeId)
                //        return true;
                //}

            }
           
            return false;
        }

        //SessionManager.UserName   SessionManager.User.Email
        public static bool SendLeaveRelatedMessageAndMail(int employeeId, string body, string subject, PayrollDataContext PayrollDataContext
            , string UserName, string UserEmail, 
            int? supervisorEmployeeId, int? reviewToEmployeeId,int leaveRequestId)
        {

            List<string> employees = new List<string>();
            string to = "", bcc = "";


            if (
                (CommonManager.Setting.LeaveRequestSettingUsingPreDefinedList != null &&
                CommonManager.Setting.LeaveRequestSettingUsingPreDefinedList.Value)
                 ||
                (CommonManager.Setting.LeaveRequestSettngUsingOneToOne != null &&
                CommonManager.Setting.LeaveRequestSettngUsingOneToOne.Value)
                )
            {


                GetPreDefinedApprovalListResult result = null;

                // if no Review then direct mail to Approval for INGO leave request and for SANA kisan like no approval needed company
                if (CommonManager.Setting.HideReviewInLeaveRequest != null && CommonManager.Setting.HideReviewInLeaveRequest.Value)
                {
                    result = PayrollDataContext.GetPreDefinedApprovalList(null, employeeId, (int)PreDefindFlowType.Leave).
                         Where(x => x.EmployeeId == supervisorEmployeeId).FirstOrDefault();
                }
                else
                {
                    result = PayrollDataContext.GetPreDefinedApprovalList(null, employeeId, (int)PreDefindFlowType.Leave).
                         Where(x => x.EmployeeId == reviewToEmployeeId).FirstOrDefault();
                }

                if (result != null && !string.IsNullOrEmpty(result.UserName))
                {
                    employees.Add(result.UserName);
                }
                if (result != null && !string.IsNullOrEmpty(result.Email))
                {
                    to = result.Email;
                }

            }
            else
            {

                GetRecipientEmailResult ApprovalRecipient = 
                    LeaveAttendanceManager.GetRecipientByEmployeeId(employeeId, PayrollDataContext);
                bool isRecommendationNeededTeam = false;

                if (ApprovalRecipient != null && (ApprovalRecipient.CC4EmployeeId != null || ApprovalRecipient.CC5EmployeeId != null))
                {
                    isRecommendationNeededTeam = true;
                }

              

                if (ApprovalRecipient != null)
                {

                    // if direct approval then 
                    if (isRecommendationNeededTeam == false)
                    {
                        if (!string.IsNullOrEmpty(ApprovalRecipient.ApplyToUserName)
                            && (supervisorEmployeeId == null || supervisorEmployeeId == ApprovalRecipient.ApplyToEmployeeId))
                            employees.Add(ApprovalRecipient.ApplyToUserName);

                        if (!string.IsNullOrEmpty(ApprovalRecipient.CC1UserName) && ApprovalRecipient.CC1UserName != UserName
                            && (supervisorEmployeeId == null || supervisorEmployeeId == ApprovalRecipient.CC1EmployeeId))
                            employees.Add(ApprovalRecipient.CC1UserName);

                        if (!string.IsNullOrEmpty(ApprovalRecipient.CC2UserName) && ApprovalRecipient.CC2UserName != UserName
                            && (supervisorEmployeeId == null || supervisorEmployeeId == ApprovalRecipient.CC2EmployeeId))
                            employees.Add(ApprovalRecipient.CC2UserName);

                        if (!string.IsNullOrEmpty(ApprovalRecipient.CC3UserName) && ApprovalRecipient.CC3UserName != UserName
                            && (supervisorEmployeeId == null || supervisorEmployeeId == ApprovalRecipient.CC3EmployeeId))
                            employees.Add(ApprovalRecipient.CC3UserName);
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(ApprovalRecipient.CC4UserName)
                            && ApprovalRecipient.CC4UserName != UserName
                            && (reviewToEmployeeId == null || reviewToEmployeeId == ApprovalRecipient.CC4EmployeeId))
                            employees.Add(ApprovalRecipient.CC4UserName);

                        if (!string.IsNullOrEmpty(ApprovalRecipient.CC5UserName)
                            && ApprovalRecipient.CC5UserName != UserName
                            && (reviewToEmployeeId == null || reviewToEmployeeId == ApprovalRecipient.CC5EmployeeId))
                            employees.Add(ApprovalRecipient.CC5UserName);
                    }






                    if (isRecommendationNeededTeam == false)
                    {
                        if (!string.IsNullOrEmpty(ApprovalRecipient.ApplyToEmail)
                            && (supervisorEmployeeId == null || supervisorEmployeeId == ApprovalRecipient.ApplyToEmployeeId))// && ApprovalRecipient.ApplyToEmail != UserEmail)
                            to = ApprovalRecipient.ApplyToEmail.Trim();

                        if (!string.IsNullOrEmpty(ApprovalRecipient.CC1Email) 
                            && (supervisorEmployeeId == null || supervisorEmployeeId == ApprovalRecipient.CC1EmployeeId))
                        {
                            if (to == "")
                                to = ApprovalRecipient.CC1Email.Trim();
                            else
                                bcc = ApprovalRecipient.CC1Email.Trim();
                        }

                        if (!string.IsNullOrEmpty(ApprovalRecipient.CC2Email) 
                            && (supervisorEmployeeId == null || supervisorEmployeeId == ApprovalRecipient.CC2EmployeeId))
                        {
                            if (to == "")
                                to = ApprovalRecipient.CC2Email.Trim();
                            else if (bcc == "")
                                bcc = ApprovalRecipient.CC2Email.Trim();
                            else
                                bcc += "," + ApprovalRecipient.CC2Email.Trim();
                        }

                        if (!string.IsNullOrEmpty(ApprovalRecipient.CC3Email) 
                             && (supervisorEmployeeId == null || supervisorEmployeeId == ApprovalRecipient.CC3EmployeeId))
                        {
                            if (to == "")
                                to = ApprovalRecipient.CC3Email.Trim();
                            else if (bcc == "")
                                bcc = ApprovalRecipient.CC3Email.Trim();
                            else
                                bcc += "," + ApprovalRecipient.CC3Email.Trim();
                        }
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(ApprovalRecipient.CC4Email) 
                            && (reviewToEmployeeId == null || reviewToEmployeeId == ApprovalRecipient.CC4EmployeeId))
                        {
                            if (to == "")
                                to = ApprovalRecipient.CC4Email.Trim();
                            else if (bcc == "")
                                bcc = ApprovalRecipient.CC4Email.Trim();
                            else
                                bcc += "," + ApprovalRecipient.CC4Email.Trim();
                        }

                        if (!string.IsNullOrEmpty(ApprovalRecipient.CC5Email) 
                            && (reviewToEmployeeId == null || reviewToEmployeeId == ApprovalRecipient.CC5EmployeeId))
                        {
                            if (to == "")
                                to = ApprovalRecipient.CC5Email.Trim();
                            else if (bcc == "")
                                bcc = ApprovalRecipient.CC5Email.Trim();
                            else
                                bcc += "," + ApprovalRecipient.CC5Email.Trim();
                        }
                    }

                }
            }

            //if (employees.Count > 0)
            //    PayrollMessageManager.SendMessages(msg, employees, PayrollDataContext);

            // Case 3: add email for HR from leave setting
            if (CommonManager.Setting != null && !string.IsNullOrEmpty(CommonManager.Setting.RequestLeaveNotificationMails))
            {
                string[] emailList = CommonManager.Setting.RequestLeaveNotificationMails.Split(new char[] { ',' });
                foreach (string email in emailList)
                {
                    if (!string.IsNullOrEmpty(bcc))
                        bcc += ",";

                    bcc += email;
                    //list.Add(new GetToEmailResult { Email = email });
                }
            }


            if (to != "")
            {
                

                // If email log is enabled then process to track email sending as we need synchronous process
                if (CommonManager.Setting.EnableEmailLog != null && CommonManager.Setting.EnableEmailLog.Value)
                {
                    string errorMsg = "";
                    bool emailStatus = SMTPHelper.SendMail(to, body, subject, bcc, UserEmail,ref errorMsg);

                    PayrollDataContext.EmailLogs.InsertOnSubmit(GetEmailLog(employeeId, "", EmailLogTypeEnum.LeaveRequest, leaveRequestId,
                        to + ":to,bcc:" + (bcc == null ? "" : bcc), subject, emailStatus, errorMsg));
                    PayrollDataContext.SubmitChanges();

                    return emailStatus;
                }

                return SMTPHelper.SendAsyncMail(to, body, subject, bcc,UserEmail);
            }


            return true;
        }

        /// <summary>
        /// Return the list of Reviewer Employee name checking Multiple Review case
        /// </summary>
        /// <param name="leaveRequestId"></param>
        /// <param name="lastRecommenderEmployeeId"></param>
        /// <returns></returns>
        public static string GetLeaveRequestReviewerList(int leaveRequestId, int lastRecommenderEmployeeId)
        {
            List<LeaveApprovalEmployeeBO> list =
                (
                    from l in PayrollDataContext.LeaveReviewRecommenders
                    join e in PayrollDataContext.EEmployees on l.RecommenderEmployeeId equals e.EmployeeId
                    where l.LeaveRequestId == leaveRequestId
                    orderby l.RecommendedOn
                    select new LeaveApprovalEmployeeBO
                    {
                        EmployeeId = e.EmployeeId,
                        Name = e.Name
                    }
                ).Distinct().ToList();

            if (list.Any(x => x.EmployeeId == lastRecommenderEmployeeId) == false)
            {
                list.Add(new LeaveApprovalEmployeeBO
                {
                    EmployeeId = lastRecommenderEmployeeId,
                    Name
                        = EmployeeManager.GetEmployeeById(lastRecommenderEmployeeId).Name
                });
            }

            string names = "";

            foreach (LeaveApprovalEmployeeBO item in list)
            {
                if (names == "")
                    names = item.Name;
                else
                    names += ", " + item.Name;
            }

            return names;
        }

        public static bool SendRecommendedLeaveRelatedMessageAndMail(int employeeId,  string body, string subject, PayrollDataContext PayrollDataContext
            , string UserName, string UserEmail, int? supervisorEmployeeId,int leaveRequestId)
        {

            List<string> employees = new List<string>();
            string to = "", bcc = "";


            if (
                (CommonManager.Setting.LeaveRequestSettingUsingPreDefinedList != null &&
                CommonManager.Setting.LeaveRequestSettingUsingPreDefinedList.Value)
                ||
                (CommonManager.Setting.LeaveRequestSettngUsingOneToOne != null &&
                CommonManager.Setting.LeaveRequestSettngUsingOneToOne.Value)
                )
            {
                GetPreDefinedApprovalListResult result = PayrollDataContext.GetPreDefinedApprovalList(
                    null, employeeId,(int)PreDefindFlowType.Leave).Where(x => x.EmployeeId == supervisorEmployeeId).FirstOrDefault();
                if (result != null && !string.IsNullOrEmpty(result.UserName))
                {
                    employees.Add(result.UserName);
                }
                if (result != null && !string.IsNullOrEmpty(result.Email))
                {
                    to = result.Email;
                }
            }
            else
            {

                GetRecipientEmailResult ApprovalRecipient = LeaveAttendanceManager.GetRecipientByEmployeeId(employeeId, PayrollDataContext);


                if (ApprovalRecipient != null)
                {

                    //TODO: remove this one as same emp request can not go to same emp for aproval,uncomment in future
                    if (!string.IsNullOrEmpty(ApprovalRecipient.ApplyToUserName)
                        && (supervisorEmployeeId == null || supervisorEmployeeId == ApprovalRecipient.ApplyToEmployeeId))// && ApprovalRecipient.ApplyToUserName != UserName)
                        employees.Add(ApprovalRecipient.ApplyToUserName);
                    if (!string.IsNullOrEmpty(ApprovalRecipient.CC1UserName) && ApprovalRecipient.CC1UserName != UserName
                        && (supervisorEmployeeId == null || supervisorEmployeeId == ApprovalRecipient.CC1EmployeeId))
                        employees.Add(ApprovalRecipient.CC1UserName);
                    if (!string.IsNullOrEmpty(ApprovalRecipient.CC2UserName) && ApprovalRecipient.CC2UserName != UserName
                        && (supervisorEmployeeId == null || supervisorEmployeeId == ApprovalRecipient.CC2EmployeeId))
                        employees.Add(ApprovalRecipient.CC2UserName);
                    if (!string.IsNullOrEmpty(ApprovalRecipient.CC3UserName) && ApprovalRecipient.CC3UserName != UserName
                        && (supervisorEmployeeId == null || supervisorEmployeeId == ApprovalRecipient.CC3EmployeeId))
                        employees.Add(ApprovalRecipient.CC3UserName);
                    //if (!string.IsNullOrEmpty(ApprovalRecipient.CC4UserName) && ApprovalRecipient.CC4UserName != UserName)
                    //    employees.Add(ApprovalRecipient.CC4UserName);
                    //if (!string.IsNullOrEmpty(ApprovalRecipient.CC5UserName) && ApprovalRecipient.CC5UserName != UserName)
                    //    employees.Add(ApprovalRecipient.CC5UserName);







                    if (!string.IsNullOrEmpty(ApprovalRecipient.ApplyToEmail)
                        && (supervisorEmployeeId == null || supervisorEmployeeId == ApprovalRecipient.ApplyToEmployeeId))// && ApprovalRecipient.ApplyToEmail != UserEmail)
                        to = ApprovalRecipient.ApplyToEmail.Trim();

                    if (!string.IsNullOrEmpty(ApprovalRecipient.CC1Email) 
                        && (supervisorEmployeeId == null || supervisorEmployeeId == ApprovalRecipient.CC1EmployeeId))
                    {
                        if (to == "")
                            to = ApprovalRecipient.CC1Email.Trim();
                        else
                            bcc = ApprovalRecipient.CC1Email.Trim();
                    }

                    if (!string.IsNullOrEmpty(ApprovalRecipient.CC2Email) 
                        && (supervisorEmployeeId == null || supervisorEmployeeId == ApprovalRecipient.CC2EmployeeId))
                    {
                        if (to == "")
                            to = ApprovalRecipient.CC2Email.Trim();
                        else if (bcc == "")
                            bcc = ApprovalRecipient.CC2Email.Trim();
                        else
                            bcc += "," + ApprovalRecipient.CC2Email.Trim();
                    }

                    if (!string.IsNullOrEmpty(ApprovalRecipient.CC3Email) 
                        && (supervisorEmployeeId == null || supervisorEmployeeId == ApprovalRecipient.CC3EmployeeId))
                    {
                        if (to == "")
                            to = ApprovalRecipient.CC3Email.Trim();
                        else if (bcc == "")
                            bcc = ApprovalRecipient.CC3Email.Trim();
                        else
                            bcc += "," + ApprovalRecipient.CC3Email.Trim();
                    }

                    //if (isRecommendationNeededTeam == false && !string.IsNullOrEmpty(ApprovalRecipient.CC4Email) && ApprovalRecipient.CC4Email != UserEmail)
                    //{
                    //    if (to == "")
                    //        to = ApprovalRecipient.CC4Email.Trim();
                    //    else if (bcc == "")
                    //        bcc = ApprovalRecipient.CC4Email.Trim();
                    //    else
                    //        bcc += "," + ApprovalRecipient.CC4Email.Trim();
                    //}

                    //if (isRecommendationNeededTeam == false && !string.IsNullOrEmpty(ApprovalRecipient.CC5Email) && ApprovalRecipient.CC5Email != UserEmail)
                    //{
                    //    if (to == "")
                    //        to = ApprovalRecipient.CC5Email.Trim();
                    //    else if (bcc == "")
                    //        bcc = ApprovalRecipient.CC5Email.Trim();
                    //    else
                    //        bcc += "," + ApprovalRecipient.CC5Email.Trim();
                    //}
                }
            }

            //if (employees.Count > 0)
            //    PayrollMessageManager.SendMessages(msg, employees, PayrollDataContext);

            if (to != "")
            {
                // If email log is enabled then process to track email sending as we need synchronous process
                if (CommonManager.Setting.EnableEmailLog != null && CommonManager.Setting.EnableEmailLog.Value)
                {
                    string errorMsg = "";
                    bool emailStatus = SMTPHelper.SendMail(to, body, subject, bcc, UserEmail, ref errorMsg);

                    PayrollDataContext.EmailLogs.InsertOnSubmit(GetEmailLog(employeeId, "", EmailLogTypeEnum.LeaveRecommended, leaveRequestId,
                        to + ":to,bcc:" + (bcc == null ? "" : bcc), subject, emailStatus, errorMsg));
                    PayrollDataContext.SubmitChanges();

                    return emailStatus;
                }

                return SMTPHelper.SendAsyncMail(to, body, subject, bcc,UserEmail);
            }


            return true;
        }
        
        public static LeaveApprovalEmployee GetEmployeeTeam(int employeeId)
        {
            LeaveApprovalEmployee approval
                = (
                    from lae in PayrollDataContext.LeaveApprovalEmployees
                    join ls in PayrollDataContext.LeaveProjectEmployees on lae.LeaveProjectId equals ls.LeaveProjectId
                    where ls.EmployeeId == employeeId
                    select lae
                ).FirstOrDefault();
            return approval;
        }
        public static LeaveApprovalEmployee GetEmployeeOtherSecondTeam(int employeeId)
        {
            LeaveApprovalEmployee approval
                = (
                    from lae in PayrollDataContext.LeaveApprovalEmployees
                    join ls in PayrollDataContext.LeaveProjectEmployees on lae.LeaveProjectId equals ls.LeaveProjectId2
                    where ls.EmployeeId == employeeId
                    select lae
                ).FirstOrDefault();
            return approval;
        }
        public static bool IsEmployeeLeaveRequiredRecommendation(int employeeId)
        {// for this setting it will always have Recommender
            if (
                (CommonManager.Setting.LeaveRequestSettingUsingPreDefinedList != null &&
                  CommonManager.Setting.LeaveRequestSettingUsingPreDefinedList.Value)
                 ||
                (CommonManager.Setting.LeaveRequestSettngUsingOneToOne != null &&
                CommonManager.Setting.LeaveRequestSettngUsingOneToOne.Value)
                )
            {
                return true;
            }
            else
            {
                LeaveApprovalEmployee approval
                    = (
                        from lae in PayrollDataContext.LeaveApprovalEmployees
                        join ls in PayrollDataContext.LeaveProjectEmployees on lae.LeaveProjectId equals ls.LeaveProjectId
                        where ls.EmployeeId == employeeId
                        select lae
                    ).FirstOrDefault();

                if (approval != null)
                {
                    if (approval.CC4 != null || approval.CC5 != null)
                        return true;
                }
            }
            return false;
        }
        public static bool CanRecommend(int employeeId, int approvalEmployeeId,PreDefindFlowType type)
        {
            if ( 
                (CommonManager.Setting.LeaveRequestSettingUsingPreDefinedList != null &&
              CommonManager.Setting.LeaveRequestSettingUsingPreDefinedList.Value)
                 ||
                (CommonManager.Setting.LeaveRequestSettngUsingOneToOne != null &&
                CommonManager.Setting.LeaveRequestSettngUsingOneToOne.Value)
                )
            {

                return LeaveAttendanceManager.GetPreDefinedApprovalList(true, employeeId, type)
                    .Any(x => x.Value == approvalEmployeeId.ToString());
            }
            else
            {
                LeaveApprovalEmployee approval
                    = (
                        from lae in PayrollDataContext.LeaveApprovalEmployees
                        join ls in PayrollDataContext.LeaveProjectEmployees on lae.LeaveProjectId equals ls.LeaveProjectId
                        where ls.EmployeeId == employeeId
                        select lae
                    ).FirstOrDefault();

                if (approval != null)
                {
                    if (approvalEmployeeId == approval.CC4 || approvalEmployeeId == approval.CC5)
                        return true;
                }
            }
            return false;
        }
        public static bool CanApprove(int employeeId, int approvalEmployeeId, PreDefindFlowType type)
        {
            if (
                (CommonManager.Setting.LeaveRequestSettingUsingPreDefinedList != null &&
              CommonManager.Setting.LeaveRequestSettingUsingPreDefinedList.Value)
                 ||
                (CommonManager.Setting.LeaveRequestSettngUsingOneToOne != null &&
                CommonManager.Setting.LeaveRequestSettngUsingOneToOne.Value)
                )
            {

                return LeaveAttendanceManager.GetPreDefinedApprovalList(false, employeeId,type)
                    .Any(x => x.Value == approvalEmployeeId.ToString());
            }
            else
            {
                LeaveApprovalEmployee approval
                   = (
                       from lae in PayrollDataContext.LeaveApprovalEmployees
                       join ls in PayrollDataContext.LeaveProjectEmployees on lae.LeaveProjectId equals ls.LeaveProjectId
                       where ls.EmployeeId == employeeId
                       select lae
                   ).FirstOrDefault();

                if (approval != null)
                {
                    if (approvalEmployeeId == approval.ApplyTo || approvalEmployeeId == approval.CC1
                         || approvalEmployeeId == approval.CC2 || approvalEmployeeId == approval.CC3)
                        return true;
                }

                LeaveApprovalEmployee otherApproval = GetEmployeeOtherSecondTeam(employeeId);
                if (otherApproval != null)
                {
                    if (otherApproval.ApplyTo == approvalEmployeeId || otherApproval.CC1 == approvalEmployeeId)
                        return true;
                }

            }

            return false;
        }
        
        public static void GetApplyToForEmployee(int employeeId, ref string review, ref string apply)
        {

            GetLeaveApprovalApplyToCCForEmployeeResult
                entity = LeaveAttendanceManager.GetLeaveApprovalApplyToAndCCForEmployee(employeeId);

            List<int> applyToList = new List<int>();
            List<int> reviewList = new List<int>();

            if (entity != null)
            {
                string value = "";
                EmployeeManager empMgr = new EmployeeManager();
                if (entity.ApplyTo != null && entity.ApplyTo != employeeId)
                {
                    applyToList.Add(entity.ApplyTo.Value);
                    value = entity.ApplyToName;
                }

                if (entity.CC1 != null && entity.CC1 != employeeId && applyToList.Contains(entity.CC1.Value) == false)
                {
                    applyToList.Add(entity.CC1.Value);
                    if (value == "") value = entity.CC1Name;
                    else value += ", " + entity.CC1Name;
                }

                if (entity.CC2 != null && entity.CC2 != employeeId && applyToList.Contains(entity.CC2.Value) == false)
                {
                    applyToList.Add(entity.CC2.Value);
                    if (value == "") value = entity.CC2Name;
                    else value += ", " + entity.CC2Name;
                }

                if (entity.CC3 != null && entity.CC3 != employeeId && applyToList.Contains(entity.CC3.Value) == false)
                {
                    applyToList.Add(entity.CC3.Value);
                    if (value == "") value = entity.CC3Name;
                    else value += ", " + entity.CC3Name;
                }

                if (entity.ApplyToOther != null && entity.ApplyToOther != employeeId && applyToList.Contains(entity.ApplyToOther.Value) == false)
                {
                    applyToList.Add(entity.ApplyToOther.Value);
                    if (value == "") value = entity.ApplyToOtherName;
                    else value += ", " + entity.ApplyToOtherName;
                }

                if (entity.CC1Other != null && entity.CC1Other != employeeId && applyToList.Contains(entity.CC1Other.Value) == false)
                {
                    applyToList.Add(entity.CC1Other.Value);
                    if (value == "") value = entity.CC1OtherName;
                    else value += ", " + entity.CC1OtherName;
                }

                apply = value;

                string recommendFrom = "";

                if (entity.CC4 != null && entity.CC4 != employeeId)
                {
                    reviewList.Add(entity.CC4.Value);
                    recommendFrom = entity.CC4Name;

                }

                if (entity.CC5 != null && entity.CC5 != employeeId && reviewList.Contains(entity.CC5.Value) == false)
                {
                    if (recommendFrom == "")
                        recommendFrom = entity.CC5Name;
                    else
                        recommendFrom += " , " + entity.CC5Name;

                }

                review = recommendFrom;

                //return value + recommendFrom; ;
            }
            // return "";
        }

        public static List<TextValue> GetFilterRecommenderForForward(List<TextValue> list,int leaveRequestId)
        {
            List<TextValue> newList = new List<TextValue>();

            List<DAL.LeaveReviewRecommender> reviewedList =
                PayrollDataContext.LeaveReviewRecommenders.Where(x => x.LeaveRequestId == leaveRequestId).ToList();

            foreach (TextValue item in list)
            {
                if (!string.IsNullOrEmpty(item.Value))
                {
                    int empId = int.Parse(item.Value);

                    if (reviewedList.Any(x => x.RecommenderEmployeeId == empId) == false
                        && SessionManager.CurrentLoggedInEmployeeId != empId)
                        newList.Add(item);
                }
            }


            return newList;
        }

        public static List<TextValue> GetApplyToListForEmployee(int employeeId,bool isApproval,PreDefindFlowType type)
        {

            GetLeaveApprovalApplyToCCForEmployeeResult
                entity = LeaveAttendanceManager.GetLeaveApprovalApplyToAndCCForEmployee(employeeId);

            List<TextValue> list = new List<TextValue>();

            if (
                (CommonManager.Setting.LeaveRequestSettingUsingPreDefinedList != null &&
                CommonManager.Setting.LeaveRequestSettingUsingPreDefinedList.Value)
                 ||
                (CommonManager.Setting.LeaveRequestSettngUsingOneToOne != null &&
                CommonManager.Setting.LeaveRequestSettngUsingOneToOne.Value)
                )
            {
                return LeaveAttendanceManager.GetPreDefinedApprovalList(!isApproval, employeeId,type);                
            }
            else
            {
                if (entity != null)
                {

                    EmployeeManager empMgr = new EmployeeManager();
                    if (isApproval)
                    {
                        if (entity.ApplyTo != null)
                        {
                            list.Add(new TextValue { Text = entity.ApplyToName, Value = entity.ApplyTo.ToString() });
                        }

                        if (entity.CC1 != null)
                        {
                            list.Add(new TextValue { Text = entity.CC1Name, Value = entity.CC1.ToString() });

                        }

                        if (entity.CC2 != null)
                        {
                            list.Add(new TextValue { Text = entity.CC2Name, Value = entity.CC2.ToString() });
                        }

                        if (entity.CC3 != null)
                        {
                            list.Add(new TextValue { Text = entity.CC3Name, Value = entity.CC3.ToString() });
                        }
                    }
                    else
                    {
                        if (entity.CC4 != null)
                        {
                            list.Add(new TextValue { Text = entity.CC4Name, Value = entity.CC4.ToString() });
                        }

                        if (entity.CC5 != null)
                        {
                            list.Add(new TextValue { Text = entity.CC5Name, Value = entity.CC5.ToString() });
                        }
                    }

                }
            }


            return list.OrderBy(x => x.Text).ToList();

        }

        public static LeaveApprovalEmployee GetLeaveApprovalEmployee( int leaveProjectId)
        {
            return 
                PayrollDataContext.LeaveApprovalEmployees.
                FirstOrDefault(x=>(leaveProjectId == x.LeaveProjectId));
        }

        public static bool DeleteLeaveApprovel(int id)
        {
            PayrollDataContext.LeaveApprovalEmployees.DeleteOnSubmit(
               PayrollDataContext.LeaveApprovalEmployees.SingleOrDefault(x => x.ID == id));
            PayrollDataContext.SubmitChanges();
            return true;
        }


        public static void SetLeaveCount(ref int? pendingCount, ref int? recommendCount, ref int? approvedCount)
        {

            List<LeaveRequestBO> list = LeaveAttendanceManager.GetUnApprovedLeaveRequest(
              SessionManager.CurrentLoggedInEmployeeId);

            

            recommendCount = list.Where(x => x.Status == Enum.GetName(typeof(LeaveRequestStatusEnum), LeaveRequestStatusEnum.Recommended)).ToList().Count();
            pendingCount = list.Where(x => x.Status == Enum.GetName(typeof(LeaveRequestStatusEnum), LeaveRequestStatusEnum.Request)).ToList().Count();

            int count;
            List<LeaveRequestBO> listApproved = LeaveAttendanceManager.GetApprovedLeaveRequest(SessionManager.CurrentLoggedInEmployeeId, out count);
            approvedCount = count;

        }




        public static ResponseStatus CancelledApprovedLeave(int leaveRequestId, string comment, bool IsHRView)
        {
            int currentEmpId = 0; string currentEmpUserName = "";
            UserManager.SetCurrentEmployeeAndUser(ref currentEmpId, ref currentEmpUserName);

            LeaveRequest upLeaveRequest = LeaveAttendanceManager.GetLeaveRequestById(leaveRequestId);
            ResponseStatus status = new ResponseStatus();

            if (SessionManager.CurrentLoggedInEmployeeId == 0
                && (SessionManager.User.UserMappedEmployeeId == null || SessionManager.User.UserMappedEmployeeId == 0 || SessionManager.User.UserMappedEmployeeId == -1))
            {
                status.ErrorMessage = "Please map the current user with Employee from User management before changing the leave.";
                status.IsSuccess = "false";
                return status;
            }

            if (upLeaveRequest != null)
            {

                if (IsHRView == false)
                {
                    if (LeaveRequestManager.CanCurrentEmployeeChangeLeaveRequestStatus(currentEmpId, upLeaveRequest) == false)
                    {
                        status.ErrorMessage = "You do not enough permission to cancel the leave.";
                        status.IsSuccess = "false";
                        return status;
                    }
                }

                int diff = 0;
                // Attendance should not be saved
                //if (LeaveRequestManager.IsAttendanceSavedForLeaveRequest(upLeaveRequest) && newLeaveStatus == LeaveRequestStatusEnum.Approved)
                if (LeaveAttendanceManager.IsValidLeaveToChangeByApproval(upLeaveRequest.ToDateEng.Value, upLeaveRequest.FromDateEng.Value, ref diff) == false)
                {
                    status.ErrorMessage =
                      string.Format("Past leave earlier than {0} days can not be cancelled.", CommonManager.CompanySetting.LeaveApprovalMinimumPastDays);
                    status.IsSuccess = "false";
                    return status;
                }


                // Only Approved & Attendance not saved leave can be cancelled
                SetConnectionPwd(PayrollDataContext.Connection);
                PayrollDataContext.Transaction = PayrollDataContext.Connection.BeginTransaction();
                try
                {

                    if ((LeaveRequestStatusEnum)upLeaveRequest.Status == LeaveRequestStatusEnum.Approved)
                    {
                        if (!string.IsNullOrEmpty(comment))
                            upLeaveRequest.Comment = comment;
                        upLeaveRequest.Status = (int)LeaveRequestStatusEnum.Cancelled;
                        upLeaveRequest.ApprovedOn = GetCurrentDateAndTime();
                        upLeaveRequest.ApprovedBy = currentEmpId;
                        UpdateChangeSet();

                        //AttendanceManager.GenerateAsyncAttendanceSingleEmployeeTable(upLeaveRequest.EmployeeId, upLeaveRequest.FromDateEng.Value.Date,
                        //    upLeaveRequest.ToDateEng.Value.Date);


                        // Company like PSI having timesheet, to remove approved leave from timesheet after Leave Cancellation
                        if (CommonManager.CompanySetting.WhichCompany == WhichCompany.PSI
                            && LeaveRequestManager.IsTimesheetSavedForLeaveRequest(upLeaveRequest))
                        {
                            foreach (LeaveDetail detail in upLeaveRequest.LeaveDetails.OrderBy(x => x.DateOnEng))
                            {
                                // Get saved timesheet leave details for the cancelled leave day
                                TimesheetLeave dbTimesheetLeave =
                                    (
                                    from l in PayrollDataContext.TimesheetLeaves
                                    join t in PayrollDataContext.Timesheets on l.TimesheetId equals t.TimesheetId
                                    where t.EmployeeId == upLeaveRequest.EmployeeId
                                        && l.StartDate.Equals(detail.DateOnEng)
                                    select l
                                    ).SingleOrDefault();

                                if (dbTimesheetLeave != null)
                                    PayrollDataContext.TimesheetLeaves.DeleteOnSubmit(dbTimesheetLeave);
                            }
                        }

                        // then we need to modify the Attendance also
                        if (LeaveRequestManager.IsAttendanceSavedForLeaveRequest(upLeaveRequest))
                        {
                            #region "Change Saved Attendance From leave request"

                            bool CountHolidayInBetweenLeave = CommonManager.CompanySetting.LeaveRequestCountHolidayInBetweenLeave;
                            HolidayManager hmanager = new HolidayManager();
                            List<GetHolidaysForAttendenceResult> holiday = hmanager.GetMonthsHolidaysForAttendence(0, 0, SessionManager.CurrentCompanyId, 0, upLeaveRequest.FromDateEng, upLeaveRequest.ToDateEng, upLeaveRequest.EmployeeId).ToList();

                            List<PayrollPeriod> list = LeaveRequestManager.GetSavePayrollListForLeaveRequest(upLeaveRequest.EmployeeId, upLeaveRequest.FromDateEng.Value);
                            decimal? value = null;
                            double? dvalue = null;
                            EEmployee emp = EmployeeManager.GetEmployeeById(upLeaveRequest.EmployeeId);

                            List<AttendanceImportEmployee> atteList = new List<AttendanceImportEmployee>();
                            atteList.Add(new AttendanceImportEmployee { EmployeeId = upLeaveRequest.EmployeeId });


                            AttendanceImportEmployeeLeave leave = new AttendanceImportEmployeeLeave();
                            // Iterate for LeaveDetails so that when Holiday lies in between then it is not removed from Attendance
                            foreach (LeaveDetail detail in upLeaveRequest.LeaveDetails.OrderBy(x => x.DateOnEng))
                            {
                                leave = new AttendanceImportEmployeeLeave();
                                leave.TakenDate = detail.DateOnEng;
                                leave.EmployeeId = upLeaveRequest.EmployeeId;
                                leave.NoLeave = true;
                                leave.Day = detail.Day;
                                atteList[0].leaves.Add(leave);
                            }


                            string msg = "";
                            int count = 0;
                            foreach (PayrollPeriod payroll in list)
                            {
                                //recalculate leave balance
                                EmployeeManager.SaveImportedAttendance(atteList, payroll.PayrollPeriodId, ref msg, ref count, null);
                                // Change lapse/encash
                                //PayrollDataContext.ProcessLeaveEncashment("Attendance", true, upLeaveRequest.EmployeeId, payroll.PayrollPeriodId,
                                //    0, ref value, ref dvalue, ref value, ref dvalue, false);
                                LeaveAttendanceManager.CallToResaveAtte(payroll.PayrollPeriodId, upLeaveRequest.EmployeeId.ToString(), null);
                            }

                            #endregion
                        }

                        //catch (Exception exp1)
                        //{
                        //    Log.log("Error while changing attendance for attendance saved leave cancel leave.", exp1);
                        //}

                        PayrollDataContext.Transaction.Commit();
                    }


                }
                catch (Exception exp)
                {

                    PayrollDataContext.Transaction.Rollback();
                    Log.log("Leave change cancel error : " + leaveRequestId, exp);
                    status.ErrorMessage = "Leave could not be changed.";
                    status.IsSuccess = "false";
                    return status;
                }
                finally
                {
                    //if (PayrollDataContext.Connection != null)
                    //{
                    //    PayrollDataContext.Connection.Close();
                    //}
                }
                //AttendanceManager.UpdateAttendanceEmployeeDate(upLeaveRequest.FromDateEng.Value);
            }
            return status;
        }

        public static bool DeleteSelfLeave(int leaveRequestId)
        {
            LeaveRequest leave = LeaveAttendanceManager.GetLeaveRequestById(leaveRequestId);

            if (leave != null)
            {
                //no need to check date as any leave under this status can be cancelled as won't 
                //effect Attendance
                if(  (LeaveRequestStatusEnum) leave.Status  == LeaveRequestStatusEnum.ReDraft 
                    ||  (LeaveRequestStatusEnum) leave.Status  == LeaveRequestStatusEnum.Request
                    || (LeaveRequestStatusEnum)leave.Status == LeaveRequestStatusEnum.Recommended)
                {

                    //PayrollDataContext.LeaveRequests.DeleteOnSubmit(leave);
                    //DeleteChangeSet();
                    // do not delete, only cancel leaves
                    leave.Status = (int)LeaveRequestStatusEnum.Cancelled;
                    leave.ApprovedOn = GetCurrentDateAndTime();
                    leave.ApprovedBy = SessionManager.CurrentLoggedInEmployeeId;
                    UpdateChangeSet();
                    return true;
                }
            }
            return false;
        }

        public static bool CancelRecommendedSelfLeave(int leaveRequestId)
        {
            LeaveRequest leave = LeaveAttendanceManager.GetLeaveRequestById(leaveRequestId);

            if (leave != null)
            {
                //no need to check date as any leave under this status can be cancelled as won't 
                //effect Attendance
                if ((LeaveRequestStatusEnum)leave.Status == LeaveRequestStatusEnum.ReDraft
                    || (LeaveRequestStatusEnum)leave.Status == LeaveRequestStatusEnum.Request
                    || (LeaveRequestStatusEnum)leave.Status == LeaveRequestStatusEnum.Recommended)
                {

                    //PayrollDataContext.LeaveRequests.DeleteOnSubmit(leave);
                    //DeleteChangeSet();
                    leave.Status = (int)LeaveRequestStatusEnum.Cancelled;
                    leave.ApprovedOn = GetCurrentDateAndTime();
                    leave.ApprovedBy = SessionManager.CurrentLoggedInEmployeeId;
                    UpdateChangeSet();
                    return true;
                }
            }
            return false;
        }
        #region "Asynchronous Method to send Message and Email"

        //public delegate void DelegateNotifyThroughMessageAndEmail(LeaveRequest leaveRequest, int CurrentLoggedInEmployeeId, bool CompanyHasHourlyLeave
        //    ,string UserName,string Email);

        //public static void NotifyThroughMessageAndEmail(LeaveRequest leaveRequest)
        //{
        //    //DelegateNotifyThroughMessageAndEmail method = new DelegateNotifyThroughMessageAndEmail(NotifyThroughMessageAndEmailMethod);
        //    //method.BeginInvoke(leaveRequest, SessionManager.CurrentLoggedInEmployeeId,CommonManager.CalculationConstant.CompanyHasHourlyLeave.Value,

        //    NotifyThroughMessageAndEmailMethod(leaveRequest, SessionManager.CurrentLoggedInEmployeeId, CommonManager.CalculationConstant.CompanyHasHourlyLeave.Value,
        //      SessionManager.UserName,SessionManager.User.Email );
        //}
        //public static void NotifyThroughMessageAndEmailMethod(LeaveRequest leaveRequest, int CurrentLoggedInEmployeeId, bool CompanyHasHourlyLeave
        //    ,string UserName,string Email)
        //{
        //    // Create DataContext due to Asynchronous case
        //    using (PayrollDataContext _payrollDataContext = new PayrollDataContext(Config.ConnectionString))
        //    {

        //        string loggedInUserName = EmployeeManager.GetEmployeeById(CurrentLoggedInEmployeeId,_payrollDataContext).Name;
        //        string status = ((LeaveRequestStatusEnum)leaveRequest.Status).ToString();

        //        #region Send Message To Leave request Sender
        //        string subject = "Leave " + status + " by ";

        //        UUser recipient = UserManager.GetUserByEmployee(leaveRequest.EmployeeId,_payrollDataContext);

        //        string body = string.Empty;
        //        if (leaveRequest.IsHour.HasValue && leaveRequest.IsHour.Value)
        //        {
        //            //Your leave on {0} for {1} has been {2} by{3}.<br /> <br />
        //            //<strong>Comment:&nbsp;</strong>{3}.
        //            body = FileHelper.GetHourlyLeaveApproveContent();
        //            body = string.Format(body, leaveRequest.FromDateEng.Value.ToShortDateString(), leaveRequest.DaysOrHours.ToString(),
        //                status, loggedInUserName, leaveRequest.Comment
        //                , (leaveRequest.IsHour.Value ? "hour(s)" : ""));
        //        }
        //        else
        //        {
        //            //Your leave from {0} to {1} has been {2} by {3}.<br /> <br />
        //            //<strong>Comment:&nbsp;</strong>{4}.
        //            body = FileHelper.GetLeaveApproveContent();

        //            EmailContent content = CommonManager.GetMailContent((int)EmailContentType.DayTypeLeaveApproval);
        //            if (content != null)
        //            {
        //                body = content.Body;
        //                subject = content.Subject;

        //                string approvalName = "";
        //                EEmployee approvalEmployee = EmployeeManager.GetEmployeeById(leaveRequest.EmployeeId);
        //                if (approvalEmployee != null)
        //                    approvalName = approvalEmployee.Name;

        //                subject = subject
        //                    .Replace("#NewStatus#", status)
        //                    .Replace("#ApprovalName#", approvalName);
        //            }

        //            body = body.Replace("#RequesterName#", "Your")
        //                        .Replace("#StartDate#", leaveRequest.FromDateEng.Value.ToShortDateString())
        //                        .Replace("#EndDate#", leaveRequest.ToDateEng.Value.ToShortDateString())
        //                        .Replace("#NewStatus#", status)
        //                        .Replace("#ApprovalName#", loggedInUserName)
        //                        .Replace("#Comment#", leaveRequest.Comment);
        //            //body = string.Format(body, leaveRequest.FromDateEng.Value.ToShortDateString(), leaveRequest.ToDateEng.Value.ToShortDateString(), status, loggedInUserName, leaveRequest.Comment);
        //        }
                

        //        DAL.PayrollMessage msg = new DAL.PayrollMessage();
        //        msg.Subject = subject;
        //        msg.Body = body;
        //        msg.SendBy = UserName;
        //        msg.ReceivedDate = GetCurrentDateAndTime().ToShortDateString();
        //        msg.ReceivedDateEng = GetCurrentDateAndTime();
        //        msg.IsRead = false;
        //        string mailErrorMsg=  "";
        //        if (recipient != null)
        //        {
        //            List<string> employees = new List<string>();
        //            if (!string.IsNullOrEmpty(recipient.UserName))
        //                employees.Add(recipient.UserName);

        //            PayrollMessageManager.SendMessages(msg, employees,_payrollDataContext);

        //            if (!string.IsNullOrEmpty(recipient.Email))
        //                SMTPHelper.SendMail(recipient.Email.Trim(), body, subject,ref mailErrorMsg);
        //        }
        //        #endregion

        //        //#region Send Message To Notification Users
        //        //if (recipient != null)
        //        //{
        //        //    if (
        //        //         ((bool)(leaveRequest.IsHour == null ? false : leaveRequest.IsHour)) ||
        //        //         ((bool)(leaveRequest.IsHalfDayLeave == null ? false : leaveRequest.IsHalfDayLeave))
        //        //        )
        //        //    {

        //        //        string hourOrDay = "";
        //        //        if (CompanyHasHourlyLeave)
        //        //            hourOrDay = "hour";
        //        //        else
        //        //            hourOrDay = "day";

        //        //        body = FileHelper.GetHoulyLeaveApproveNotification();
        //        //        body = string.Format(body,
        //        //            recipient.EEmployee.Name, leaveRequest.FromDateEng.Value.ToShortDateString(), leaveRequest.DaysOrHours.ToString(),
        //        //           status, loggedInUserName, leaveRequest.Comment, hourOrDay);
        //        //    }
        //        //    else
        //        //    {
                    
        //        //         body = FileHelper.GetLeaveApproveContent();

        //        //        EmailContent content = CommonManager.GetMailContent((int)EmailContentType.DayTypeLeaveApproval);
        //        //        if (content != null)
        //        //        {
        //        //            body = content.Body;
        //        //        }

        //        //        body = body.Replace("#RequesterName#", recipient.EEmployee.Name)
        //        //                    .Replace("#StartDate#", leaveRequest.FromDateEng.Value.ToShortDateString())
        //        //                    .Replace("#EndDate#", leaveRequest.ToDateEng.Value.ToShortDateString())
        //        //                    .Replace("#NewStatus#", status)
        //        //                    .Replace("#ApprovalName#", loggedInUserName)
        //        //                    .Replace("#Comment#", leaveRequest.Comment);
        //        //    }
        //        //}


        //        //msg = new DAL.PayrollMessage();
        //        //msg.Subject = subject;
        //        //msg.Body = body;
        //        //msg.SendBy = UserName;
        //        //msg.ReceivedDate = System.DateTime.Now.ToShortDateString();
        //        //msg.ReceivedDateEng = System.DateTime.Now;
        //        //msg.IsRead = false;


        //        //LeaveRequestManager.SendLeaveRelatedMessageAndMail(leaveRequest.EmployeeId, msg, body, subject,
        //        //    _payrollDataContext, UserName, Email);

        //        //#endregion

        //        _payrollDataContext.SubmitChanges();
        //    }
        //}


        #endregion

        public static List<GetEmployeeLeaveBalanceResult> GetEmployeeCurrentLeaveBalance(int employeeId)
        {
            PayrollPeriod lastPayroll = CommonManager.GetLastPayrollPeriod();

            
                List<GetLeaveListAsPerEmployeeResult> list = LeaveAttendanceManager.GetAllLeavesForLeaveRequest(employeeId, lastPayroll.PayrollPeriodId, true);
                
                return list.Select
                    (
                        x =>
                        new GetEmployeeLeaveBalanceResult
                        {
                            LeaveTypeId = x.LeaveTypeId,
                            NewBalance = x.NewBalance == null ? ((decimal)0) : (decimal)x.NewBalance,
                            Title = x.Title,
                            EmployeeId = employeeId,
                            CurrentBalance = x.NewBalance == null ? ((double)0) : (double)x.NewBalance,
                        }
                    ).ToList();


          
        }

        /// <summary>
        /// Get Leave Balance
        /// </summary>
        /// <param name="employeeId"></param>
        /// <returns></returns>
        public static string GetLeaveBalanceAsHTML(int employeeId, bool isLeaveRequest, List<GetLeaveListAsPerEmployeeResult> list)
        {
            PayrollPeriod lastPayroll = CommonManager.GetLastPayrollPeriod();
            string html = "<table cellspacing='0' class='gridtbl leavetype'> <tr> <th class='leaveTitle'> Leave Type </th> <th> Balance </th> </tr>";


            // combine child in to parent
            //foreach (GetLeaveListAsPerEmployeeResult item in list)
            for (int i = 0; i < list.Count; i++)
            {
                GetLeaveListAsPerEmployeeResult item = list[i];
                if (item.IsParentGroupLeave)
                {
                    for (int j = 0; j < list.Count; j++)
                    {
                        if (list[j].ParentLeaveTypeId != null && list[j].ParentLeaveTypeId == item.LeaveTypeId)
                        {
                            if (item.NewBalance == null)
                                item.NewBalance = 0;
                            if (list[j].NewBalance != null)
                                item.NewBalance += list[j].NewBalance;
                        }
                    }
                }
            }

            list = list.Where(x => x.IsChildLeave == false).OrderBy(x => x.Title).ToList();


            //  List<GetEmployeeLeaveBalanceResult> balanceList = LeaveAttendanceManager.GetEmployeeLeaveBalance(employeeId);
            foreach (GetLeaveListAsPerEmployeeResult balance in list)
            {
                //if (balance.FreqOfAccrual != LeaveAccrue.COMPENSATORY)'

                if (CommonManager.CompanySetting.WhichCompany == WhichCompany.NIBL && balance.FreqOfAccrual == LeaveAccrue.COMPENSATORY)
                { }
                else
                {
                    html += string.Format("<tr> <td> {0} </td> <td style='text-align:right'> {1} </td> </tr>", balance.Title, (balance.NewBalance == null ? "0" : balance.NewBalance.ToString())
                       );
                }
            }


            html += "</table>";
            return html;
        }

        /// <summary>
        /// Add the months for "LeaveRequestValidMonths"
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="months"></param>
        /// <returns></returns>
        public static DateTime GetMonthsFromStartDate(DateTime startDate, int months)
        {
            if (IsEnglish)
            {
                return startDate.AddMonths(months);
            }
            else
            {
                CustomDate d = new CustomDate(startDate.Day, startDate.Month, startDate.Year, true);
                CustomDate nepDate = CustomDate.ConvertEngToNep(d);
                string[] values = nepDate.ToString().Split('/');
                int[] dates = { int.Parse(values[0]), int.Parse(values[1]), int.Parse(values[2]) };
                for (int i = 1; i <= months; i++)
                {
                    if (dates[1] == 12)
                    {
                        dates[1] = 1;
                        dates[2] += 1;
                    }
                    else
                        dates[1] += 1;
                }

                int maxDay = DateHelper.GetTotalDaysInTheMonth(dates[2], dates[1], false);




                return CustomDate.GetCustomDateFromString(dates[0] + "/" + dates[1] + "/" + dates[2], false)
                    .EnglishDate.AddDays(-1);
            }


        }


        /// <summary>
        /// Returns the Start Date for leave listing, by picking last payroll period & going before the six months if exists
        /// </summary>
        /// <returns></returns>
        public static DateTime GetStartDateForLeaveDetails()
        {
            List<PayrollPeriod> periods
                = (
                from p in PayrollDataContext.PayrollPeriods
                where p.CompanyId == SessionManager.CurrentCompanyId
                orderby p.PayrollPeriodId descending
                select p
                ).Take(6).ToList();


            if (periods.Count > 0)
                return periods[periods.Count - 1].StartDateEng.Value;

            return GetCurrentDateAndTime();
        }

        #region "Leave Projects"

        public static LeaveProjectEmployee GetLeaveProjectForEmployee(int employeeId)
        {
            return PayrollDataContext.LeaveProjectEmployees.FirstOrDefault(x => x.EmployeeId == employeeId);
        }

        public static EHumanResource GetGratuityClassForEmployee(int employeeId)
        {
            return PayrollDataContext.EHumanResources.FirstOrDefault(x => x.EmployeeId == employeeId);
        }
        public static List<GetLeaveProjectsResult> GetProjectList(string searchText, int currentPage, int pageSize, ref int total)
        {

            string depIDList = SessionManager.CustomRoleDeparmentIDList;

            currentPage -= 1;

            List<GetLeaveProjectsResult> list =
                PayrollDataContext.GetLeaveProjects(currentPage, pageSize, searchText, depIDList).ToList();

            total = 0;
            if (list.Count > 0)
                total = list[0].TotalRows.Value;

            return list;



        }
        public static List<GetEmpSpecificLeaveDefListResult> GetEmployeeSupervisorList
            (int currentPage, int pageSize,int empId,int branchId,int depId,int rid,int aid,int type, ref int total)
        {


            List<GetEmpSpecificLeaveDefListResult> list =
                PayrollDataContext.GetEmpSpecificLeaveDefList(currentPage, pageSize,empId,branchId,depId,rid,aid).ToList();
           list =  list.Where(x => x.Type == type).ToList();
            
            total = 0;
            if (list.Count > 0)
                total = list.Count;

            return list;



        }
        public static List<LeaveProject> GetLeaveProjectListByDepartment(int departmentId)
        {
            return

                PayrollDataContext.LeaveProjects.Where(x => departmentId==-1 || x.DepartmentId == departmentId)
                .OrderBy(x => x.Name).ToList();
        }

        public static List<LeaveProject> GetAllLeaveProjectList()
        {
            return

                PayrollDataContext.LeaveProjects
                .OrderBy(x => x.Name).ToList();
        }


        public static bool SaveUpdateProject(LeaveProject project)
        {
            project.ModifedOn = DateTime.Now;
            project.ModifiedBy = SessionManager.User.UserName;
           
            if (project.LeaveProjectId == 0)
            {
                project.CreatedOn = DateTime.Now;
                project.CreatedBy = SessionManager.User.UserName;
                PayrollDataContext.LeaveProjects.InsertOnSubmit(project);
                return SaveChangeSet();
            }
            else
            {
                LeaveProject dbEntity = PayrollDataContext.LeaveProjects.SingleOrDefault(p => p.LeaveProjectId == project.LeaveProjectId);
                if (dbEntity != null)
                {
                    CopyObject<LeaveProject>(project, ref dbEntity, "");
                    UpdateChangeSet();
                    return true;
                }
            }
            return false;
        }

        public static bool IsEmployeeExistsForProject(int leaveProjectId)
        {
            bool exists =
                PayrollDataContext.LeaveProjectEmployees.Any(x => x.LeaveProjectId == leaveProjectId);

            if (exists)
                return true;

            exists =
                PayrollDataContext.LeaveApprovalEmployees.Any(x => x.LeaveProjectId == leaveProjectId);

            return exists;
        }

        public static bool DeleteProject(int leaveprojectId)
        {

            if (IsEmployeeExistsForProject(leaveprojectId))
                return false;

            LeaveProject entity = PayrollDataContext.LeaveProjects.SingleOrDefault(x => x.LeaveProjectId == leaveprojectId);
            if (entity != null)
            {
                PayrollDataContext.LeaveProjects.DeleteOnSubmit(entity);
                return DeleteChangeSet();
            }
            return false;
        }
        public static LeaveProject GetLeaveProject(int leaveProjectId)
        {
            return PayrollDataContext.LeaveProjects.SingleOrDefault(x => x.LeaveProjectId == leaveProjectId);
        }
        public static bool IsDepartmentAlreadyExists(LeaveProject entity)
        {
            return

                PayrollDataContext.LeaveProjects.Any(x => x.DepartmentId == entity.DepartmentId &&

                    (entity.LeaveProjectId == 0 || entity.LeaveProjectId != x.LeaveProjectId));
        }

        public static bool IsProjectNameAlreadyExists(LeaveProject entity)
        {
            return

                PayrollDataContext.LeaveProjects.Any(x => x.Name.ToLower() == entity.Name.ToLower() &&

                    (entity.LeaveProjectId == 0 || entity.LeaveProjectId != x.LeaveProjectId));
        }

        #endregion

        #region "Employee Leave Projects"

        public static List<GetLeaveProjectEmployeesResult> GetEmployeeProjectList(string searchText, int currentPage, int pageSize, ref int total
            ,int departmentId,int leaveProjectId,int desigId)
        {

            currentPage -= 1;

            string depListID = SessionManager.CustomRoleDeparmentIDList;

            List<GetLeaveProjectEmployeesResult> list =
                PayrollDataContext.GetLeaveProjectEmployees(currentPage, pageSize,departmentId, searchText
                , SessionManager.CurrentCompanyId, depListID, leaveProjectId,desigId).ToList();

            total = 0;
            if (list.Count > 0)
                total = list[0].TotalRows.Value;

            return list;



        }

        public static List<GetEmployeesManagersListResult> GetEmployeesManagersList1(string searchText, int currentPage, int pageSize, ref int total
            , int departmentId)
        {

            currentPage -= 1;


            List<GetEmployeesManagersListResult> list =
                PayrollDataContext.GetEmployeesManagersList(currentPage, pageSize, departmentId, searchText
                , SessionManager.CurrentCompanyId).ToList();

            total = 0;
            if (list.Count > 0)
                total = list[0].TotalRows.Value;

            return list;



        }


        public static void SaveUpdateEmployeeProject(List<GetLeaveProjectEmployeesResult> projectEmployeeList)
        {
            foreach (GetLeaveProjectEmployeesResult entity in projectEmployeeList)
            {
                LeaveProjectEmployee project = PayrollDataContext.LeaveProjectEmployees.SingleOrDefault(x => x.EmployeeId == entity.EmployeeId);

                if (project == null)
                {

                    project = new LeaveProjectEmployee();
                    project.EmployeeId = entity.EmployeeId;
                    project.LeaveProjectId = entity.LeaveProjectId;

                    PayrollDataContext.LeaveProjectEmployees.InsertOnSubmit(project);
                
                }
                else
                {
                    project.LeaveProjectId = entity.LeaveProjectId;
                    

                }
            }

            PayrollDataContext.SubmitChanges();
        }

        public static void SaveUpdateEmployeeSupervisor(List<GetEmpSpecificLeaveDefListResult> list)
        {
            foreach (GetEmpSpecificLeaveDefListResult entity in list)
            {
                LeaveSpecificEmployee project = PayrollDataContext.LeaveSpecificEmployees
                    .SingleOrDefault(x => x.EmployeeId == entity.EmployeeId && x.Type == entity.Type);

                if (project == null)
                {

                    project = new LeaveSpecificEmployee();
                    project.EmployeeId = entity.EmployeeId;
                    project.Recommender1 = entity.R1EIN;
                    project.Recommender2 = entity.R2EIN;
                    project.Approval1 = entity.A1EIN;
                    project.Approval2 = entity.A2EIN;
                    project.Type = entity.Type;

                    PayrollDataContext.LeaveSpecificEmployees.InsertOnSubmit(project);

                }
                else
                {

                    project.EmployeeId = entity.EmployeeId;
                    project.Recommender1 = entity.R1EIN;
                    project.Recommender2 = entity.R2EIN;
                    project.Approval1 = entity.A1EIN;
                    project.Approval2 = entity.A2EIN;

                }
            }

            PayrollDataContext.SubmitChanges();
        }
        public static void SaveUpdateLeaveRequestSettings(List<GetLeaveApprovalSettingListResult> leaveRequestList)
        {
            foreach (GetLeaveApprovalSettingListResult entity in leaveRequestList)
            {
                LeaveApprovalEmployee project = PayrollDataContext.LeaveApprovalEmployees.FirstOrDefault(x => x.LeaveProjectId == entity.LeaveProjectId);

                if (project == null)
                {

                    project = new LeaveApprovalEmployee();
                    project.LeaveProjectId = entity.LeaveProjectId;
                    project.ApplyTo = int.Parse(entity.ApplyTo);
                    if (!string.IsNullOrEmpty(entity.CC1))
                        project.CC1 = int.Parse(entity.CC1);
                    else
                        project.CC1 = null;

                    if (!string.IsNullOrEmpty(entity.CC4))
                        project.CC4 = int.Parse(entity.CC4);
                    else
                        project.CC4 = null;

                    if (!string.IsNullOrEmpty(entity.CC5))
                        project.CC5 = int.Parse(entity.CC5);
                    else
                        project.CC5 = null;

                    PayrollDataContext.LeaveApprovalEmployees.InsertOnSubmit(project);

                }
                else
                {
                    project.ApplyTo = int.Parse(entity.ApplyTo);
                    if (!string.IsNullOrEmpty(entity.CC1))
                        project.CC1 = int.Parse(entity.CC1);
                    else
                        project.CC1 = null;

                    if (!string.IsNullOrEmpty(entity.CC4))
                        project.CC4 = int.Parse(entity.CC4);
                    else
                        project.CC4 = null;

                    if (!string.IsNullOrEmpty(entity.CC5))
                        project.CC5 = int.Parse(entity.CC5);
                    else
                        project.CC5 = null;


                }
            }

            PayrollDataContext.SubmitChanges();
        }
        public static bool SaveUpdateEmployeeProject(LeaveProjectEmployee project)
        {
            //project.ModifedOn = DateTime.Now;
            //project.ModifiedBy = SessionManager.User.UserName;
            LeaveProjectEmployee dbEntity = PayrollDataContext.LeaveProjectEmployees.SingleOrDefault(p => p.EmployeeId == project.EmployeeId);

            //Delete if called from AEEmployee page
            if (project.LeaveProjectId == -1)
            {
                if (dbEntity != null)
                {
                    PayrollDataContext.LeaveProjectEmployees.DeleteOnSubmit(dbEntity);
                }
                return true;
            }

           //if (project.LeaveProjectEmployeeId == 0)
            if (dbEntity == null)
            {
                //project.CreatedOn = DateTime.Now;
                //project.CreatedBy = SessionManager.User.UserName;
                PayrollDataContext.LeaveProjectEmployees.InsertOnSubmit(project);
                return SaveChangeSet();
            }
            else
            {
                dbEntity.LeaveProjectId = project.LeaveProjectId;
                dbEntity.LeaveProjectId2 = project.LeaveProjectId2;
                //CopyObject<LeaveProjectEmployee>(project, ref dbEntity, "");
                UpdateChangeSet();
                return true;

            }
            //return false;
        }


        public static LeaveProjectEmployee GetEmployeeLeaveProject(int leaveProjectId)
        {
            return PayrollDataContext.LeaveProjectEmployees.SingleOrDefault(x => x.LeaveProjectEmployeeId == leaveProjectId);
        }

        //public static bool IsEmployeeAlreadyExists(LeaveProjectEmployee entity)
        //{
        //    return

        //        PayrollDataContext.LeaveProjectEmployees.Any(x => x.EmployeeId == entity.EmployeeId &&

        //            (entity.LeaveProjectEmployeeId == 0 || entity.LeaveProjectEmployeeId != x.LeaveProjectEmployeeId));
        //}

        //public static bool IsProjectNameAlreadyExists(LeaveProject entity)
        //{
        //    return

        //        PayrollDataContext.LeaveProjects.Any(x => x.Name.ToLower() == entity.Name.ToLower() &&

        //            (entity.LeaveProjectId == 0 || entity.LeaveProjectId != x.LeaveProjectId));
        //}

        #endregion


        public static List<GetLeaveApprovalSettingListResult> GetLeaveApprovalList(int departmentId, int start, int limit, int branchId, int teamId)
        {
            string depIDList = SessionManager.CustomRoleDeparmentIDList;

            List<GetLeaveApprovalSettingListResult> list =
                PayrollDataContext.GetLeaveApprovalSettingList(SessionManager.CurrentCompanyId, departmentId, branchId, start, limit, depIDList, teamId).ToList();
            return list;
        }

        //public static int GetBranchIDByDepartment(int departmentID)
        //{
        //    return (int)PayrollDataContext.Departments.Where(r => r.DepartmentId == departmentID).Select(s => s.BranchId).SingleOrDefault();
        //}
    }
}


