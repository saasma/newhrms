﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using DAL;
using Utils;
using System.Xml.Linq;
using Utils.Calendar;
using BLL.Entity;
using BLL.BO;
using System.Web.UI.HtmlControls;
namespace BLL.Manager
{

    public class ActivityManager : BaseBiz
    {

        public static List<darGetNotSubmittedDatesResult> GetNotSubmittedList(int empId, DateTime start, DateTime end)
        {
            List<darGetNotSubmittedDatesResult> list = BLL.BaseBiz.PayrollDataContext.darGetNotSubmittedDates(
                 empId, start, end).ToList();

            int sn = 0;

            foreach (darGetNotSubmittedDatesResult item in list)
                item.SN = ++sn;


            return list;
        }


        public static Status DeleteActivity(int activityId)
        {
            Status status = new Status();
            Activiti dbEntity = PayrollDataContext.Activitis.SingleOrDefault(x => x.ActivityID == activityId);

            if (dbEntity.Status != (int)DARStatusEnum.Verified)
            {
                PayrollDataContext.Activitis.DeleteOnSubmit(dbEntity);
                PayrollDataContext.SubmitChanges();
            }
            else
                status.ErrorMessage = "Verified activity can not be deleted.";

            return status;
        }

        public static Status InsertUpdateActivity(Activiti entity, List<ActivityLearning> learningLines, List<ActivityAchvement> achievementLines, bool isInsert)
        {
            Status status = new Status();

            if (isInsert)
            {
                entity.ActivityLearnings.AddRange(learningLines);
                entity.ActivityAchvements.AddRange(achievementLines);

                PayrollDataContext.Activitis.InsertOnSubmit(entity);
            }
            else
            {
                Activiti dbEntity = PayrollDataContext.Activitis.SingleOrDefault(x => x.ActivityID == entity.ActivityID);
                dbEntity.ActivityDateEng = entity.ActivityDateEng;
                dbEntity.Details = entity.Details;
                dbEntity.EmployeeId = entity.EmployeeId;
                dbEntity.AttachmentName = entity.AttachmentName;
                dbEntity.AttachmentUrl = entity.AttachmentUrl;
                dbEntity.Status = entity.Status;
                dbEntity.ApplyToVerifyEmployeeId = entity.ApplyToVerifyEmployeeId;
                //dbEntity.SubmissionDate = DateTime.Today;

                List<ActivityLearning> lstLearningsToDelete = PayrollDataContext.ActivityLearnings.Where(x => x.ActivityID == entity.ActivityID).ToList();
                foreach (ActivityLearning learningToDelete in lstLearningsToDelete)
                {
                    PayrollDataContext.ActivityLearnings.DeleteOnSubmit(learningToDelete);
                }

                List<ActivityAchvement> lstAchievementsToDelete = PayrollDataContext.ActivityAchvements.Where(x => x.ActivityId == entity.ActivityID).ToList();
                foreach (ActivityAchvement achievementToDelete in lstAchievementsToDelete)
                {
                    PayrollDataContext.ActivityAchvements.DeleteOnSubmit(achievementToDelete);
                }

                PayrollDataContext.SubmitChanges();

                dbEntity.ActivityLearnings.AddRange(learningLines);
                dbEntity.ActivityAchvements.AddRange(achievementLines);
            }

            PayrollDataContext.SubmitChanges();

            return status;
        }

        public static List<Activiti> GetAllUnreadActivities(int empId)
        {
            List<Activiti> lstActivities = PayrollDataContext.Activitis
                .Where(x => (x.EmployeeId == empId) && (x.Status == 1 || x.Status == 2 || x.Status == 3))
                .OrderByDescending(x => x.ActivityDateEng).ToList();

            foreach (Activiti activity in lstActivities)
            {
                if (activity.Status == 1)
                {
                    activity.Statuss = "Draft";
                }
                else if (activity.Status == 2)
                {
                    activity.Statuss = "Pending";
                }
                else
                    activity.Statuss = "Verified";
            }

            return lstActivities;
        }

        //public static string GetActivityDateFromID(int ActivityID)
        //{
        //    Activiti entity = PayrollDataContext.Activitis.Where(x => x.ActivityID == ActivityID).FirstOrDefault();
        //    return entity.ActivityDateEng.Value.ToShortDateString();
        //}

        public static string GetDetailsFromID(int ActivityID)
        {
            Activiti entity = PayrollDataContext.Activitis.Where(x => x.ActivityID == ActivityID).FirstOrDefault();
            return entity.Details;
        }

        public static string GetAttachmentFromID(int ActivityID)
        {
            Activiti entity = PayrollDataContext.Activitis.Where(x => x.ActivityID == ActivityID).FirstOrDefault();
            return entity.AttachmentName;
        }

        public static string GetAttachmentURLFromID(int ActivityID)
        {
            Activiti entity = PayrollDataContext.Activitis.Where(x => x.ActivityID == ActivityID).FirstOrDefault();
            return entity.AttachmentUrl;
        }

        public static List<ActivityLearning> GetLearningsFromID(int ActivityID)
        {
            List<ActivityLearning> learnings = PayrollDataContext.ActivityLearnings.Where(x => x.ActivityID == ActivityID).OrderBy(x => x.ActivityLearningId).ToList();

            return learnings;
        }

        public static List<ActivityAchvement> GetAchievementsFromID(int ActivityID)
        {
            var achievements = PayrollDataContext.ActivityAchvements.Where(x => x.ActivityId == ActivityID).OrderBy(x => x.ActivityAchivementId).ToList();
            return achievements;
        }

        public static Status VerifyActivity(int ActivityID, int statusEnum)
        {
            Status status = new Status();

            Activiti entity = PayrollDataContext.Activitis.Where(x => x.ActivityID == ActivityID).FirstOrDefault();
            entity.Status = statusEnum;
            PayrollDataContext.SubmitChanges();

            return status;
        }
        public static Status VerifyActivity(List<int> list, int statusEnum)
        {
            Status status = new Status();

            foreach (int item in list)
            {

                Activiti entity = PayrollDataContext.Activitis.Where(x => x.ActivityID == item).FirstOrDefault();
                entity.Status = statusEnum;
                entity.VerifiedBy = SessionManager.User.UserID;
                entity.VerifiedOn = GetCurrentDateAndTime();
            }
            PayrollDataContext.SubmitChanges();

            return status;
        }
        public static List<Activiti> GetActivitiesToVerify()
        {
            List<Activiti> lstActivities = PayrollDataContext.Activitis.OrderBy(x => x.ActivityID).Where(x => x.Status == 2).ToList();

            return lstActivities;
        }

        public static Activiti GetActivityFromID(int ActivityID)
        {
            return PayrollDataContext.Activitis.SingleOrDefault(x => x.ActivityID == ActivityID);
        }

        public static List<DARGetEmployeesResult> GetEmployeesDailyReport(string empName, int? branchid, int? levelId, DateTime startDate, DateTime endDate, int orderBy,bool isadmin)
        {
            //return PayrollDataContext.DARGetEmployees(empName, branchid).ToList();
            try
            {
                List<DARGetEmployeesResult> iList = new List<DARGetEmployeesResult>();
                iList = PayrollDataContext.DARGetEmployees(empName, branchid, levelId, startDate, endDate, orderBy,isadmin,SessionManager.CurrentLoggedInEmployeeId).ToList();
                return iList;
            }
            catch (Exception)
            {
                return new List<DARGetEmployeesResult>();
            }
        }

        public static List<darNotVerifiedActivityListResult> GetNotVerifiedActivities(string empName, int? branchId, int? levelId, int groupBy, DateTime startDate, DateTime endDate, bool isAdmin)
        {
            List<darNotVerifiedActivityListResult> iList = new List<darNotVerifiedActivityListResult>();
            iList = PayrollDataContext.darNotVerifiedActivityList(empName, branchId, levelId, groupBy, startDate, endDate, isAdmin, SessionManager.CurrentLoggedInEmployeeId).ToList();
            return iList;
        }

        public static List<darVerifiedActivityListResult> GetVerifiedActivities(string empName, int? branchId, int? levelId, int groupBy, DateTime startDate, DateTime endDate,bool isAdmin)
        {
            List<darVerifiedActivityListResult> iList = new List<darVerifiedActivityListResult>();
            iList = PayrollDataContext.darVerifiedActivityList(empName, branchId, levelId, groupBy, startDate, endDate,isAdmin,SessionManager.CurrentLoggedInEmployeeId).ToList();
            return iList;
        }

        public static List<Branch> GetAllBranches()
        {
            return PayrollDataContext.Branches.OrderBy(x => x.BranchId).ToList();
        }

        public static List<BLevel> GetLevels()
        {
            return PayrollDataContext.BLevels.OrderBy(x => x.LevelId).ToList();
        }
        public static EEmployee GetEmployee(int empId)
        {
            EEmployee entity = PayrollDataContext.EEmployees.SingleOrDefault(x => x.EmployeeId == empId);
            return entity;
        }
        public static Branch GetBranch(int branchId)
        {
            return PayrollDataContext.Branches.SingleOrDefault(x => x.BranchId == branchId);
        }
        public static EDesignation GetDesignation(int designationId)
        {
            return PayrollDataContext.EDesignations.SingleOrDefault(x => x.DesignationId == designationId);
        }

        public static bool CheckIfAlreadyExists(int empId, DateTime activityDate)
        {
            Activiti entity = PayrollDataContext.Activitis.FirstOrDefault(x => x.EmployeeId == empId && x.ActivityDateEng == activityDate);
            if (entity == null)
                return false;
            else
                return true;
        }
        public static int GetActivityIdFromDate(DateTime activityDate)
        {
            Activiti entity = PayrollDataContext.Activitis.FirstOrDefault(x => x.ActivityDateEng == activityDate);

            return entity.ActivityID;
        }

      
    }
}
    