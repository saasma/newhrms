﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using DAL;
using Utils;
using System.Xml.Linq;
using Utils.Calendar;
using BLL.Entity;
using BLL.BO;
namespace BLL.Manager
{

    public class NoticeManager : BaseBiz
    {

        public static Status AddUpdateNoticeCategory(NoticeCategory noticeCate)
        {
            Status status = new Status();
                
               

                if (PayrollDataContext.NoticeCategories.Any(x => x.CategoryID == noticeCate.CategoryID))
                {
                    NoticeCategory dbEnity = PayrollDataContext.NoticeCategories.FirstOrDefault(x => x.CategoryID == noticeCate.CategoryID);
                    if (dbEnity != null)
                    {
                        dbEnity.Name = noticeCate.Name;
                    }
                    PayrollDataContext.SubmitChanges();
                }
                else
                {
                    if (PayrollDataContext.NoticeCategories.Any(x => x.Name == noticeCate.Name))
                    {
                        status.IsSuccess = false;
                        status.ErrorMessage = "Category with the same Name already exist";
                        return status;
                    }

                    PayrollDataContext.NoticeCategories.InsertOnSubmit(noticeCate);
                    PayrollDataContext.SubmitChanges();
                }
                status.IsSuccess = true;

            return status;
        }

        public static List<NoticeCategory> getAllNoticeCategories()
        {
            return PayrollDataContext.NoticeCategories.OrderBy(x => x.Name).ToList();
        }

        public static Status InsertUpdateNotice(NoticeBoard noticeboard)
        {
            Status status = new Status();


            if (PayrollDataContext.NoticeBoards.Any(x => x.NoticeID == noticeboard.NoticeID))
            {
                NoticeBoard dbEnity = PayrollDataContext.NoticeBoards.FirstOrDefault(x => x.NoticeID == noticeboard.NoticeID);
                if (dbEnity != null)
                {
                    dbEnity.Body = noticeboard.Body;
                    dbEnity.ExpiryDate = noticeboard.ExpiryDate;
                    dbEnity.FileExtention = noticeboard.FileExtention;

                    if (!string.IsNullOrEmpty(noticeboard.FileName) && !string.IsNullOrEmpty(noticeboard.URL))
                    {
                        dbEnity.FileName = noticeboard.FileName;
                        dbEnity.FileType = noticeboard.FileType;
                        dbEnity.Description = noticeboard.Description;
                        dbEnity.URL = noticeboard.URL;
                        dbEnity.Size = noticeboard.Size;
                    }

                    if(dbEnity.Status!= (int)NoticeBoardStatusEnum.Unpublished)
                        dbEnity.Status = noticeboard.Status;

                    dbEnity.Title = noticeboard.Title;

                    if (noticeboard.CategoryID != null)
                    {
                        dbEnity.CategoryID = noticeboard.CategoryID;
                    }
                    

                }
                PayrollDataContext.SubmitChanges();
            }
            else
            {

                PayrollDataContext.NoticeBoards.InsertOnSubmit(noticeboard);
                PayrollDataContext.SubmitChanges();
            }

            status.IsSuccess = true;
            return status;
        }

        public static List<NoticeBoard> getAllNotice(int status)
        {
            List<NoticeBoard> notices = new List<NoticeBoard>();
            
            notices = PayrollDataContext.NoticeBoards.Where(x => x.Status == status).OrderByDescending(x=>x.PublishDate).ToList();
            

            foreach (var val1 in notices)
            {
                val1.StatusStr = ((NoticeBoardStatusEnum)val1.Status).ToString();
                if(!string.IsNullOrEmpty(val1.URL))
                val1.URL = System.IO.Path.GetFileNameWithoutExtension(val1.URL).ToString();

                if(val1.PublishDate!=null)
                {
                    if (val1.PublishDate.Value.Date == CommonManager.GetCurrentDateAndTime().Date)
                    {
                        val1.PublishedDateStr = "Today";
                    }
                    else if (val1.PublishDate.Value.Date == CommonManager.GetCurrentDateAndTime().AddDays(-1).Date)
                    {
                        val1.PublishedDateStr = "Yesterday";
                    }
                    else
                    {
                        val1.PublishedDateStr = val1.PublishDate.Value.ToShortDateString();
                    }
                }
            }
            PayrollDataContext.SubmitChanges();
            return notices;
        }

        public static Status ChangeNoticeStatus(int NoticeID)
        {
            Status status = new Status();
            NoticeBoard notice = new NoticeBoard();
            if (PayrollDataContext.NoticeBoards.Any(x => x.NoticeID == NoticeID))
            {
                notice = PayrollDataContext.NoticeBoards.FirstOrDefault(x => x.NoticeID == NoticeID);
                if (notice.Status == (int)NoticeBoardStatusEnum.Published)
                {
                    notice.Status = (int)NoticeBoardStatusEnum.Unpublished;
                }
                else
                {
                    notice.Status = (int)NoticeBoardStatusEnum.Published;
                }
            }

            status.IsSuccess = true;
            return status;
        }

        public static NoticeBoard getNoticeByID(int NoticeID)
        {
            NoticeBoard notice = new NoticeBoard();
            if (PayrollDataContext.NoticeBoards.Any(x => x.NoticeID == NoticeID))
            {
                notice = PayrollDataContext.NoticeBoards.FirstOrDefault(x => x.NoticeID == NoticeID);
            }
            return notice;
        }

        public static NoticeCategory GetNoticeCategoryByID(int? CategoryID)
        {
            if (PayrollDataContext.NoticeCategories.Any(x => x.CategoryID == CategoryID))
            {
                return PayrollDataContext.NoticeCategories.FirstOrDefault(x => x.CategoryID == CategoryID);
            }

            return new NoticeCategory();
        }

        public static NoticeBoard getNoticeByAttachment(string p)
        {
            return PayrollDataContext.NoticeBoards.SingleOrDefault(
                e => e.URL == p);
        }

        public static Status DeleteNotice(int noticeId)
        {
            Status status = new Status();

            NoticeBoard dbObj = PayrollDataContext.NoticeBoards.SingleOrDefault(x => x.NoticeID == noticeId);
            if (dbObj == null)
            {
                status.ErrorMessage = "Notice not found.";
                status.IsSuccess = false;
                return status;
            }

            PayrollDataContext.NoticeBoards.DeleteOnSubmit(dbObj);
            PayrollDataContext.SubmitChanges();
            status.IsSuccess = true;

            return status;
        }

    }
}
    