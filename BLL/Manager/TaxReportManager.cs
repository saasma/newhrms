﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BLL;
using DAL;
namespace BLL
{
    public class OtherIncome
    {
        public string Name { get; set; }
        public decimal Amount { get; set; }
    }
    public class TaxReportManager : BaseBiz
    {
        
        public decimal Salary = 0;
        public decimal Allowances = 0;
        public decimal ProvidentFund = 0;
        public decimal VehicleFacility = 0;
        public decimal TelephoneFacility =0;
       public decimal  HousingAllowance = 0;
       public decimal  Bonus = 0;


      public decimal   HealthExpenses = 0;
      public decimal   LeaveEncashment =0; // company protion leave encashment also
      public decimal   DashainAllowance =0;
      public decimal   LifeInsuranceIncome = 0;
      public decimal   Reward =0;
      public decimal   ConcessessionalInterest =0;

      public decimal TotalIncomes = 0;
      //public decimal   Other = 14,


       public decimal  PFDeductionAndCITSum = 0;
       public decimal  InsuranceDeduction = 0;
        public decimal RemoteAllowance  =0;
       public decimal  TotalDeduction = 0;
       public decimal  SingleMarriedDeductionOROnePercentAmount = 0;
       public decimal FifteenPercentAmount = 0;

       public decimal  TotalTaxableIncome = 0;

       public decimal  OnePercent = 0;
       public decimal  FifteenPercent =0;
       public decimal  TwentyFivePercent = 0;
       public decimal  ThirtyFivePercent = 0;
       public decimal  TotalTax =0;

       public decimal  MedicalTax = 0;
       public decimal  FemaleRebate = 0;
       public decimal TotalFinalTax = 0;

       public decimal AnnualTaxableAmount = 0;


       public Dictionary<int, decimal> list = new Dictionary<int, decimal>();


       public List<OtherIncome> otherIncomeList = new List<OtherIncome>();

       public void SetValue(string TaxDetails)
       {
            //happens due to new employee with no basic salary like 
            if (TaxDetails == null)
                return;


            List<YearlyIncome> incomeGroups = PayrollDataContext.YearlyIncomes.ToList();


            string value = "", description,  amount;
            int type, sourceId;
            decimal amountValue, amount1, amount2;
            //if (list == null)
            {
                list = new Dictionary<int, decimal>();
                
                string[] values = TaxDetails.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);

                foreach (string valueEach in values)
                {
                    string[] items = valueEach.Split(new char[] { ':' });

                    value = "";

                    // if has more than 5 then only the line is valid as other has not Type/SourceId info
                    if (items.Length >= 5)
                    {
                        type = int.Parse(items[4]);
                        sourceId = int.Parse(items[5]);
                        amount = items[1];
                        description = items[2];

                        amountValue = 0;
                        decimal.TryParse(amount, out amountValue);

                        PIncome income = BaseBiz.PayrollDataContext.PIncomes.SingleOrDefault(x => x.IncomeId == sourceId);

                        if (type == (int)CalculationColumnType.IncomePF)
                            this.ProvidentFund += amountValue;
                        else if (type == (int)CalculationColumnType.IncomeLeaveEncasement)
                            this.LeaveEncashment += amountValue;
                        else if (income != null && (type == 1 || type == 25))
                        {

                            YearlyIncome setting = incomeGroups.FirstOrDefault(x=>x.SourceID == income.IncomeId && x.Type == 1);

                            if(setting != null)
                            {
                                BLL.TaxCertificateEnum taxEnum = (BLL.TaxCertificateEnum)setting.TypeEnum;

                                switch (taxEnum)
                                {
                                    case BLL.TaxCertificateEnum.Salary:
                                        this.Salary += amountValue;
                                        break;
                                    case BLL.TaxCertificateEnum.Allowances:
                                        this.Allowances += amountValue; break;
                                    //case BLL.TaxCertificateEnum.ProvidentFund:
                                       // this.ProvidentFund += amountValue; break;
                                    case BLL.TaxCertificateEnum.VehicleFacility:
                                        this.VehicleFacility += amountValue; break;
                                    case BLL.TaxCertificateEnum.TelephoneFacility:
                                        this.TelephoneFacility += amountValue; break;
                                    case BLL.TaxCertificateEnum.HousingAllowance:
                                        this.HousingAllowance += amountValue; break;
                                    case BLL.TaxCertificateEnum.Bonus:
                                        this.Bonus += amountValue; break;
                                    case BLL.TaxCertificateEnum.HealthExpenses:
                                        this.HealthExpenses += amountValue; break;
                                    case BLL.TaxCertificateEnum.LeaveEncashment:
                                        this.LeaveEncashment += amountValue; break;
                                    case BLL.TaxCertificateEnum.DashainAllowance:
                                        this.DashainAllowance += amountValue; break;
                                    case BLL.TaxCertificateEnum.LifeInsuranceIncome:
                                        this.LifeInsuranceIncome += amountValue; break;
                                    case BLL.TaxCertificateEnum.Reward:
                                        this.Reward += amountValue; break;
                                    case BLL.TaxCertificateEnum.ConcessessionalInterest:
                                        this.ConcessessionalInterest += amountValue; break;
                                    case TaxCertificateEnum.Other :

                                        OtherIncome inc = new OtherIncome();
                                        inc.Name = income.Title;
                                        inc.Amount = amountValue;
                                        otherIncomeList.Add(inc);

                                        break;
                                }

                            }
                           

                        }
                        else if (valueEach.ToLower().Contains("insurance income") &&
                           valueEach.ToLower().Contains("none cash yearly"))
                        {
                            if (decimal.TryParse(amount, out amountValue))
                            {
                                this.LifeInsuranceIncome += amountValue;
                            }
                        }
                        else if (valueEach.ToLower().Contains("insurance premium"))
                        {
                            if (decimal.TryParse(amount, out amountValue))
                            {
                                this.InsuranceDeduction += amountValue;
                            }
                        }
                        
                        else if (valueEach.ToLower().Contains("Annual Taxable Income".ToLower()))
                        {
                            if (decimal.TryParse(amount, out amountValue))
                            {
                                this.TotalIncomes += amountValue;
                            }
                        }
                            else if (valueEach.ToLower().Contains("Annual Taxable Amount".ToLower()))
                        {
                            if (decimal.TryParse(amount, out amountValue))
                            {
                                this.AnnualTaxableAmount += amountValue;
                            }
                        }
                        else if (valueEach.ToLower().Equals("health insurance"))
                        {
                            if (decimal.TryParse(amount, out amountValue))
                            {
                                this.InsuranceDeduction += amountValue;
                            }
                        }
                        // place opening or some case past in bhatta
                        else if (valueEach.ToLower().Contains("past regular income amount"))
                        {
                            if (decimal.TryParse(amount, out amountValue))
                            {
                                this.Allowances += amountValue;
                            }
                        }
                        // place opening or some case past in bhatta
                        else if (valueEach.ToLower().Contains("deduction total (if any)"))
                        {
                            if (decimal.TryParse(amount, out amountValue))
                            {
                                this.Allowances += amountValue;
                            }
                        }
                       
                        else if (valueEach.ToLower().Equals("remote area"))
                        {
                            if (decimal.TryParse(amount, out amountValue))
                            {
                                this.RemoteAllowance += amountValue;
                            }
                        }
                        //else if (list.ContainsKey(type + ":" + sourceId) == false)
                        //{
                        //    list.Add(type + ":" + sourceId, amount);
                        //}
                       
                        //else
                        //{   //if already added like due to some logic dashain can come twice but only one can have value
                        //    decimal newAmount, oldAmount;
                        //    decimal.TryParse(amount, out newAmount);
                        //    string oldValue = list[type + ":" + sourceId];
                        //    decimal.TryParse(oldValue, out oldAmount);

                        //    list[type + ":" + sourceId] = (oldAmount + newAmount).ToString();
                        //}


                    }
                    else
                    {

                        if (decimal.TryParse(items[1], out amountValue))
                        {
                            // Calculation for other fixed also
                            if (valueEach.ToLower().Contains("1% tax"))
                            {

                                value = items[2].Replace("on", "");
                                if (decimal.TryParse(value, out amount1))
                                    this.SingleMarriedDeductionOROnePercentAmount = amount1;


                               this.OnePercent = amountValue;

                                this.TotalTax = Convert.ToDecimal(this.OnePercent) + Convert.ToDecimal(this.FifteenPercent) 
                                    + Convert.ToDecimal(this.TwentyFivePercent) + Convert.ToDecimal(this.ThirtyFivePercent)
                                    + Convert.ToDecimal(this.MedicalTax);
                            }
                            else if (valueEach.ToLower().Contains("Annual Taxable Income".ToLower()))
                            {
                                //if (decimal.TryParse(amount, out amountValue))
                                {
                                    this.TotalIncomes = amountValue;
                                }
                            }
                                  else if (valueEach.ToLower().Contains("Annual Taxable Amount".ToLower()))
                        {
                           // if (decimal.TryParse(amount, out amountValue))
                            {
                                this.AnnualTaxableAmount += amountValue;
                            }
                                  }
                            else if (valueEach.ToLower().Contains("15% tax"))
                            {
                                value = items[2].Replace("on", "");
                                if (decimal.TryParse(value, out amount1))
                                    this.FifteenPercentAmount = amount1;


                                this.FifteenPercent = amountValue;

                                this.TotalTax = Convert.ToDecimal(this.OnePercent) + Convert.ToDecimal(this.FifteenPercent)
                                    + Convert.ToDecimal(this.TwentyFivePercent) + Convert.ToDecimal(this.ThirtyFivePercent)
                                    + Convert.ToDecimal(this.MedicalTax);
                            }
                            else if (valueEach.ToLower().Contains("25% tax"))
                            {
                                
                                this.TwentyFivePercent = amountValue;

                                this.TotalTax = Convert.ToDecimal(this.OnePercent) + Convert.ToDecimal(this.FifteenPercent)
                                    + Convert.ToDecimal(this.TwentyFivePercent) + Convert.ToDecimal(this.ThirtyFivePercent)
                                    + Convert.ToDecimal(this.MedicalTax);
                            }
                            else if (valueEach.ToLower().Contains("surcharge"))
                            {
                                //value = items[2].Replace("on", "");
                                //if (decimal.TryParse(value, out amount1))
                                //    this.FourtyPercentAmount = amount1;
                               // this.ThirtyFivePercent = ((decimal)2.5 * amountValue) * 4;

                                this.ThirtyFivePercent = amountValue;

                                // change surchange as per in the excel

                                if (this.ThirtyFivePercent > 0)
                                {
                                    this.TwentyFivePercent =( ( 2500000 - this.SingleMarriedDeductionOROnePercentAmount -
                                        this.FifteenPercentAmount) * (decimal)(25.0 / 100.0)) ;
                                    this.ThirtyFivePercent = ((this.AnnualTaxableAmount - 2500000) * (decimal)(35.0 / 100.0))
                                        ;
                                }

                                this.TotalTax = Convert.ToDecimal(this.OnePercent) + Convert.ToDecimal(this.FifteenPercent)
                                     + Convert.ToDecimal(this.TwentyFivePercent) + Convert.ToDecimal(this.ThirtyFivePercent)
                                     + Convert.ToDecimal(this.MedicalTax);
                            }
                            else if (valueEach.ToLower().Contains("medical tax"))
                            {
                                this.MedicalTax = Math.Abs(amountValue);

                                //this.TotalTax = Convert.ToDecimal(this.OnePercent) + Convert.ToDecimal(this.FifteenPercent)
                                //      + Convert.ToDecimal(this.TwentyFivePercent) + Convert.ToDecimal(this.ThirtyFivePercent)
                                //      + Convert.ToDecimal(this.MedicalTax);
                       
                            }
                            
                                // old add on
                            //else if (valueEach.ToLower().Contains("add-on tax") || valueEach.ToLower().Contains("add-on paid"))
                            //{
                            //    if (valueEach.ToLower().Contains("sst"))
                            //        this.AddOnSSTPaid += amountValue;
                            //    else
                            //        this.AddOnTDSPaid += amountValue;
                            //}
                            //// sst/tds paid from regular salary
                            //else if (valueEach.ToLower().Contains("sst paid in past"))
                            //{
                            //    this.SSTPaid += amountValue;
                            //}
                            //else if (valueEach.ToLower().Contains("tds paid in past"))
                            //{
                            //    this.TDSPaid += amountValue;
                            //}
                            //else if (valueEach.ToLower().Contains("sst this month"))
                            //{
                            //    this.SSTPaid += amountValue;
                            //}
                            //else if (valueEach.ToLower().Contains("tds this month"))
                            //{
                            //    this.TDSPaid += amountValue;
                            //}  
                            else if (valueEach.ToLower().Contains("Min of a, b or c".ToLower()))
                            {
                                //if (decimal.TryParse(amount, out amountValue))
                                    this.PFDeductionAndCITSum = amountValue;
                            }
                            else if (valueEach.ToLower().Contains("insurance premium"))
                            {
                                this.InsuranceDeduction += amountValue;
                            }
                            else if (valueEach.ToLower().Contains("female rebate"))
                            {
                                this.FemaleRebate += Math.Abs( amountValue);
                            }
                            else if (valueEach.ToLower().Contains("health insurance"))
                            {
                                 this.InsuranceDeduction += amountValue;
                            }
                            else if (valueEach.ToLower().Contains("insurance income") && 
                                valueEach.ToLower().Contains("none cash yearly"))
                            {
                                this.LifeInsuranceIncome += amountValue;
                            }
                           
                        }
                    }
                }

              
             
            }

           
        }
    }
}
