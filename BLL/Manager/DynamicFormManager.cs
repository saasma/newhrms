﻿using System.Collections.Generic;
using System.Linq;
using DAL;
using System;
using Utils;
using System.Text;

namespace BLL.Manager
{
    public class DynamicFormManager : BaseBiz
    {

        public static Status InsertUpdateDynamicForm(DynamicForm item, List<DynamicFormControl> list)
        {
            Status status = new Status();

            if (item.FormID.Equals(0))
            {
                if (PayrollDataContext.DynamicForms.Any(x => x.Name == item.Name))
                {
                    status.ErrorMessage = "Form Name Already Exists.";
                    status.IsSuccess = false;
                    return status;
                }
                PayrollDataContext.DynamicForms.InsertOnSubmit(item);
                PayrollDataContext.SubmitChanges();

                foreach (var child in list)
                {
                    child.FormID = item.FormID;
                    PayrollDataContext.DynamicFormControls.InsertOnSubmit(child);
                    PayrollDataContext.SubmitChanges();
                }

                status.ErrorMessage = item.FormID.ToString();
            }
            else
            {
                DynamicForm entity = PayrollDataContext.DynamicForms.SingleOrDefault(x => x.FormID == item.FormID);
                entity.Name = item.Name;
                foreach (var child in entity.DynamicFormControls)
                    child.DynamicFormControlValues.Clear();
                entity.DynamicFormControls.Clear();

                foreach (var child in list)
                {
                    child.FormID = item.FormID;
                    PayrollDataContext.DynamicFormControls.InsertOnSubmit(child);
                }
                PayrollDataContext.SubmitChanges();
                status.ErrorMessage = item.FormID.ToString();
            }

            return status;
        }

        public static List<DynamicForm> GetDynamicForms()
        {
            return PayrollDataContext.DynamicForms.OrderBy(x => x.FormID).ToList();
        }

        public static List<DynamicForm> GetDynamicFormsForEmp()
        {         
            return PayrollDataContext.DynamicForms.OrderBy(x => x.FormID).ToList();
        }

        public static Status DeleteDynamicForm(int formId)
        {
            Status status = new Status();
            DynamicForm dbEntity = PayrollDataContext.DynamicForms.SingleOrDefault(x => x.FormID == formId);
            if (dbEntity == null)
            {
                status.IsSuccess = false;
                return status;
            }

            if (PayrollDataContext.DynamicFormEmployees.Any(x => x.FormID == formId))
            {
                status.IsSuccess = false;
                status.ErrorMessage = "Form is used so cannot be deleted.";
                return status;
            }

            PayrollDataContext.DynamicForms.DeleteOnSubmit(dbEntity);
            PayrollDataContext.SubmitChanges();
            status.IsSuccess = true;
            return status;
        }

        public static string GetDynamicFormNameUsingFormId(int formId)
        {
            return (string)(from c in PayrollDataContext.DynamicForms
                            where c.FormID == formId
                            select c.Name).SingleOrDefault();

        }

        public static List<DynamicFormControl> GetDynamicFormControlList(int FormId)
        {
            List<DynamicFormControl> list = PayrollDataContext.DynamicFormControls.Where(x => x.FormID == FormId).OrderBy(x => x.Sequence).ToList();

            foreach (var item in list)
            {
                if (item.ControlType == Convert.ToInt32(ControlType.DropDownList) || item.ControlType == Convert.ToInt32(ControlType.CheckBoxList) || item.ControlType == Convert.ToInt32(ControlType.RadioButtonList))
                {
                    List<DynamicFormControlValue> dynamicFormControlValueList = PayrollDataContext.DynamicFormControlValues.Where(x => x.ControlID == item.ControlID).ToList();
                    if (dynamicFormControlValueList.Count != 0)
                    {
                        int i = 0;
                        StringBuilder sb = new StringBuilder();
                        foreach (var obj in dynamicFormControlValueList)
                        {
                            if (i < dynamicFormControlValueList.Count - 1)
                            {
                                sb.Append(obj.ControlValue);
                                sb.Append(",,");
                            }
                            else
                            {
                                sb.Append(obj.ControlValue);
                            }
                            i++;
                        }
                        item.ControlValues = sb.ToString();
                    }
                }
                item.ControlTypeName = ((ControlType)item.ControlType).ToString();
                if (item.IsRequired.Value)
                    item.IsRequiredOrNot = "Yes";
                else item.IsRequiredOrNot = "No";
            }

            return list;
        }

        public static List<DynamicFormControlValue> GetDynamicFormControlValueList(int controlId)
        {
            return PayrollDataContext.DynamicFormControlValues.Where(x => x.ControlID == controlId).ToList();
        }

        public static Status InsertUpdateDynamicRequestForm(DynamicFormEmployee obj, DynamicFormEmployeeStatusHistory hist)
        {
            Status status = new Status();

            hist.ApprovedOn = CommonManager.GetCurrentDateAndTime();
            hist.ApprovedBy = SessionManager.CurrentLoggedInUserID;

            if (obj.DFEID.Equals(0))
            {
                GetDocumentNextStepUsingDynamicApprovalFlowResult nextStep = PayrollDataContext.GetDocumentNextStepUsingDynamicApprovalFlow(obj.Status.Value - 1, obj.EmployeeID.Value, obj.FormID.Value).FirstOrDefault();


                if (obj.Status == (int)FlowStepEnum.Saved)
                    obj.StatusName = "Saved";
                else
                {
                    obj.StatusName = nextStep.StepStatusName;

                    // send message
                    // send message
                    //if (nextStep != null)
                    //{
                    //    if (nextStep.ApprovalEmployee1 != null)
                    //        SendTravelRequestMailAndMessage(EmailContentType.TravelRequestReview, nextStep.ApprovalEmployee1.Value, request);

                    //    if (nextStep.ApprovalEmployee2 != null)
                    //        SendTravelRequestMailAndMessage(EmailContentType.TravelRequestReview, nextStep.ApprovalEmployee2.Value, request);

                    //}
                }

                PayrollDataContext.DynamicFormEmployees.InsertOnSubmit(obj);
                PayrollDataContext.SubmitChanges();
                status.IsSuccess = true;
            }
            else
            {
                DynamicFormEmployee entity = PayrollDataContext.DynamicFormEmployees.SingleOrDefault(x => x.DFEID == obj.DFEID);


                GetDocumentNextStepUsingDynamicApprovalFlowResult nextStep = PayrollDataContext.GetDocumentNextStepUsingDynamicApprovalFlow(entity.Status.Value, entity.EmployeeID.Value, obj.FormID.Value).FirstOrDefault();

                entity.Status = obj.Status;
                entity.StatusName = obj.StatusName;

                if (entity.Status != (int)FlowStepEnum.Saved)
                {
                    if (nextStep == null)
                    {
                        status.ErrorMessage = "Dynamic Form Employee can not be changed as already ended.";
                        return status;
                    }


                    entity.Status = obj.Status;
                    entity.StatusName = nextStep.StepStatusName;

                    if (entity.Status != (int)FlowStepEnum.Saved && entity.Status != (int)FlowStepEnum.Step1SaveAndSend)
                    {
                        hist.Status = entity.Status.Value;
                        hist.ApprovalDisplayTitle = nextStep.AuthorityName;
                        entity.DynamicFormEmployeeStatusHistories.Add(hist);
                    }

                    // send message
                    //if (nextStep != null)
                    //{
                    //    if (nextStep.ApprovalEmployee1 != null)
                    //        SendTravelRequestMailAndMessage(EmailContentType.TravelRequestReview, nextStep.ApprovalEmployee1.Value, dbEntity);

                    //    if (nextStep.ApprovalEmployee2 != null)
                    //        SendTravelRequestMailAndMessage(EmailContentType.TravelRequestReview, nextStep.ApprovalEmployee2.Value, dbEntity);

                    //}
                }


                entity.DynamicFormEmployeeValues.Clear();
                entity.DynamicFormEmployeeValues.AddRange(obj.DynamicFormEmployeeValues);
                entity.FormID = obj.FormID;
                
                PayrollDataContext.SubmitChanges();
                status.IsSuccess = true;
            }

            return status;
        }

        public static string GetDynamicFormEmpValues(int dynamicFormEmpId, int controlId)
        {
            return (string)(from c in PayrollDataContext.DynamicFormEmployeeValues
                            where c.DFEID == dynamicFormEmpId && c.ControlID == controlId
                            select c.Value).SingleOrDefault();
        }

        public static List<DynamicFormEmployee> GetDynamicFormEmpoyeeList(int formId, int employeeId)
        {
            return PayrollDataContext.DynamicFormEmployees.Where(x => (x.FormID == formId && x.EmployeeID == employeeId)).OrderBy(x => x.DFEID).ToList();
        }

        public static List<DynamicFormEmployee> GetDynamicFormEmpList(int employeeId)
        {
            return PayrollDataContext.DynamicFormEmployees.Where(x => x.EmployeeID == employeeId).ToList();
        }

        public static Status DeleteDynamicFormEmployee(int dynamicFormEmpId)
        {
            Status status = new Status();
            DynamicFormEmployee entity = PayrollDataContext.DynamicFormEmployees.Single(x => x.DFEID == dynamicFormEmpId);
            if (entity == null)
            {
                status.IsSuccess = false;
                return status;
            }
            PayrollDataContext.DynamicFormEmployees.DeleteOnSubmit(entity);
            PayrollDataContext.SubmitChanges();
            status.IsSuccess = true;
            return status;
        }

        public static bool CheckDynamicFormUsedInEmployee(int formId)
        {
            bool status = false;
            if (PayrollDataContext.DynamicFormEmployees.Any(x => x.FormID == formId))
                status = true;
            return status;
        }

        public static DynamicForm GetDynamicFormUsingFormId(int formId)
        {
            return PayrollDataContext.DynamicForms.SingleOrDefault(x => x.FormID == formId);
        }

        public static List<DynamicApprovalFlow> GetDynamicApprovalFlow(int formId)
        {
            return PayrollDataContext.DynamicApprovalFlows.Where(x => x.FormID == formId).OrderBy(x => x.FormID).ToList();
        }

        public static Status UpdateDynamicApprovalFlow(List<DynamicApprovalFlow> lines, int formId)
        {
            Status status = new Status();
            foreach (var item in PayrollDataContext.DynamicApprovalFlows.Where(x => x.FormID == formId).ToList())
            {
                PayrollDataContext.DynamicApprovalFlows.DeleteOnSubmit(item);
            }

            PayrollDataContext.DynamicApprovalFlows.InsertAllOnSubmit(lines);
            status.IsSuccess = true;
            PayrollDataContext.SubmitChanges();
            return status;
        }

        public static List<Branch> GetBranches()
        {
            return PayrollDataContext.Branches.Where(x => x.CompanyId == SessionManager.CurrentCompanyId).OrderBy(x => x.Name).ToList();
        }

        public static List<Department> GetDepartments()
        {
            return PayrollDataContext.Departments.OrderBy(x => x.Name).ToList();
        }

        public static List<BLevel> GetAllLevels()
        {
            return PayrollDataContext.BLevels.OrderBy(x => x.Order).ToList();
        }

        public static List<EEmployee> GetAllEmployees()
        {
            return PayrollDataContext.EEmployees.OrderBy(x => x.EmployeeId).ToList();
        }

        public static List<DynamicFormPublish> GetAllDynamicFormPublish()
        {
            List<DynamicFormPublish> list = PayrollDataContext.DynamicFormPublishes.OrderBy(x => x.PublishID).ToList();

            foreach (var item in list)
            {
                item.FormName = GetDynamicFormNameUsingFormId(item.FormID);

                Branch objBranch = PayrollDataContext.Branches.SingleOrDefault(x => x.BranchId == item.BranchId);
                if (objBranch != null)
                    item.BranchName = objBranch.Name;
                else
                    item.BranchName = "";

                Department objDepartment = PayrollDataContext.Departments.SingleOrDefault(x => x.DepartmentId == item.DepartmentId);
                if (objDepartment != null)
                    item.DepartmentName = objDepartment.Name;
                else
                    item.DepartmentName = "";

                BLevel objBLevel = PayrollDataContext.BLevels.SingleOrDefault(x => x.LevelId == item.LevelId);
                if (objBLevel != null)
                    item.LevelName = objBLevel.Name;
                else
                    item.LevelName = "";

                EEmployee objEEmployee = PayrollDataContext.EEmployees.SingleOrDefault(x => x.EmployeeId == item.EmployeeId);
                if (objEEmployee != null)
                    item.EmployeeName = objEEmployee.Name;
                else
                    item.EmployeeName = "";


            }
            return list;
        }

        public static Status SaveUpdateDynamicFormPublish(DynamicFormPublish obj)
        {
            Status status = new Status();

            if (obj.PublishID.Equals(0))
            {
                if (PayrollDataContext.DynamicFormPublishes.Any(x => (x.FormID == obj.FormID) && (x.BranchId == obj.BranchId || x.DepartmentId == obj.DepartmentId || x.LevelId == obj.LevelId || x.EmployeeId == obj.EmployeeId)))
                {
                    status.ErrorMessage = "Dynamic Form is already published.";
                    status.IsSuccess = false;
                    return status;
                }
                else
                {
                    PayrollDataContext.DynamicFormPublishes.InsertOnSubmit(obj);
                    PayrollDataContext.SubmitChanges();
                    status.IsSuccess = true;
                }
            }
            else
            {
                DynamicFormPublish entity = PayrollDataContext.DynamicFormPublishes.SingleOrDefault(x => x.PublishID == obj.PublishID);
                entity.FormID = obj.FormID;
                entity.BranchId = obj.BranchId;
                entity.DepartmentId = obj.DepartmentId;
                entity.LevelId = obj.LevelId;
                entity.EmployeeId = obj.EmployeeId;
                entity.CreatedBy = obj.CreatedBy;
                entity.CreatedOn = obj.CreatedOn;
                PayrollDataContext.SubmitChanges();
                status.IsSuccess = true;
            }

            return status;
        }

        public static DynamicFormPublish GetDynamicFormPublishUsingID(int publishID)
        {
            return PayrollDataContext.DynamicFormPublishes.SingleOrDefault(x => x.PublishID == publishID);
        }

        public static Status DeleteDynamicFormPublish(DynamicFormPublish obj)
        {
            Status status = new Status();

            DynamicFormPublish entity = PayrollDataContext.DynamicFormPublishes.Single(x => x.PublishID == obj.PublishID);
            if (entity == null)
            {
                status.IsSuccess = false;
                status.ErrorMessage = "Dynamic form publish is not found.";
                return status;
            }

            if(PayrollDataContext.DynamicFormEmployees.Any(x => x.FormID == obj.FormID))
            {
                status.IsSuccess = false;
                status.ErrorMessage = "Dynamic form is used so cannot be deleted.";
                return status;
            }

            PayrollDataContext.DynamicFormPublishes.DeleteOnSubmit(entity);
            PayrollDataContext.SubmitChanges();
            status.IsSuccess = true;
            return status;
        }

        public static List<GetDynamicFormPublishForEmployeeResult> GetDynamicFormPublishList(int EmplpoyeeId)
        {
            return PayrollDataContext.GetDynamicFormPublishForEmployee(EmplpoyeeId).ToList();
        }

        public static DynamicFormEmployee GetDynamicFormEmpoyeeUsingID(int DFEID)
        {
            return PayrollDataContext.DynamicFormEmployees.SingleOrDefault(x => x.DFEID == DFEID);
        }

        public static List<GetDynamicFormEmployeeForHRResult> GetAllDynamicFormForHR(int FormID)
        {
            return PayrollDataContext.GetDynamicFormEmployeeForHR(SessionManager.CurrentLoggedInEmployeeId, FormID).ToList();
        }

        public static bool CanEmpChangeDocument(int status, int employeeId, int currentEmployeeId, int FormID)
        {
            return PayrollDataContext.CanChangeDocumentUsingDynamicFormApprovalFlow(status, employeeId, currentEmployeeId, FormID).Value;
        }

        public static GetDocumentNextStepUsingDynamicApprovalFlowResult GetDocumentNextStepDynamicApproval(int currentStepID, int employeeId, int FormID)
        {
            return PayrollDataContext.GetDocumentNextStepUsingDynamicApprovalFlow(currentStepID, employeeId, FormID).FirstOrDefault();
        }

        public static List<DynamicApprovalFlow> GetDynamicApprovalFlowList(int FormID)
        {
            return PayrollDataContext.DynamicApprovalFlows.Where(x => x.FormID == FormID).OrderBy(x => x.StepID).ToList();

        }

        public static List<GetDynamicFormEmployeeForAdminResult> GetAllDynamicFormEmployeeForAdmin(int status, int FormID)
        {
            return PayrollDataContext.GetDynamicFormEmployeeForAdmin(status, FormID).ToList();
        }

        public static Status UpdateAdvanceDynamicFormEmployeeReq(DynamicFormEmployee entity)
        {
            Status status = new Status();
            DynamicFormEmployee dbEntity = new DynamicFormEmployee();
            if (PayrollDataContext.DynamicFormEmployees.Any(x => x.DFEID == entity.DFEID))
            {
                dbEntity = PayrollDataContext.DynamicFormEmployees.Where(x => x.DFEID == entity.DFEID).First();
                dbEntity.Status = (int)AdvanceStatusEnum.HRAdvance;
                dbEntity.StatusName = "HRAdvance";

                dbEntity.DynamicFormEmployeeValues.Clear();
                entity.DynamicFormEmployeeValues.AddRange(entity.DynamicFormEmployeeValues);
                entity.FormID = entity.FormID;
                //entity.EmployeeID = entity.EmployeeID;
                //entity.Date = entity.Date;
                //entity.DateEng = entity.DateEng;
            }
            PayrollDataContext.SubmitChanges();
            status.IsSuccess = true;
            return status;
        }

        public static List<GetDynamicFormEmployeeAdvanceResult> GetAllDynamicFormEmpForAdvance(int? advanceStatus, int FormID)
        {
            int lastStatus = 0;
            lastStatus = (int)FlowStepEnum.Step15End;

            List<GetDynamicFormEmployeeAdvanceResult> list = new List<GetDynamicFormEmployeeAdvanceResult>();
            list = PayrollDataContext.GetDynamicFormEmployeeAdvance(SessionManager.CurrentLoggedInEmployeeId, lastStatus, advanceStatus, FormID).ToList();

            foreach (var item in list)
            {
                if (item.Status != null)
                {
                    AdvanceStatusEnum stat = (AdvanceStatusEnum)item.Status.Value;
                    if (stat == AdvanceStatusEnum.HRAdvance)
                        item.StatusName = "Advance Set";
                    else if (stat == AdvanceStatusEnum.EmployeeSettle)
                        item.StatusName = "Employee Settled";
                    else
                        item.StatusName = "HR Settled";
                }
                else
                {
                    item.StatusName = "Advance Not Set";
                }
            }

            return list;
        }

        public static List<DynamicFormEmployeeStatusHistory> GetDynamicFormEmpStatusHist(int dfeid)
        {
            List<DynamicFormEmployeeStatusHistory> list = new List<DynamicFormEmployeeStatusHistory>();
            list = PayrollDataContext.DynamicFormEmployeeStatusHistories.Where(x => x.DFEID == dfeid && x.Status > (int)FlowStepEnum.Step1SaveAndSend).OrderBy(y => y.ApprovedOn).ToList();
            return list;
        }

        public static Status SettleDynamicFormEmployee(DynamicFormEmployee obj, DynamicFormEmployeeStatusHistory hist)
        {
            Status status = new Status();
            DynamicFormEmployee dbEntity = new DynamicFormEmployee();

            hist.ApprovedOn = CommonManager.GetCurrentDateAndTime();
            hist.ApprovedBy = SessionManager.CurrentLoggedInUserID;
            status.IsSuccess = true;

            if (PayrollDataContext.DynamicFormEmployees.Any(x => x.DFEID == obj.DFEID))
            {
                dbEntity = PayrollDataContext.DynamicFormEmployees.Where(x => x.DFEID == obj.DFEID).SingleOrDefault();

                dbEntity.DynamicFormEmployeeValues.Clear();
                dbEntity.DynamicFormEmployeeValues.AddRange(obj.DynamicFormEmployeeValues);
                dbEntity.FormID = obj.FormID;
                dbEntity.EmployeeID = obj.EmployeeID;
                dbEntity.Date = obj.Date;
                dbEntity.DateEng = obj.DateEng;
                dbEntity.Status = obj.Status;
            }
            else
            {
                status.IsSuccess = false;
                status.ErrorMessage = "Dynamic Form Employee Not found!";
            }

            return status;
        }

        public static List<GetDynamicFormEmployeeByUserResult> GetAllDynamicFormEmployeeByUser(int EmployeeId, int FormID)
        {
            return PayrollDataContext.GetDynamicFormEmployeeByUser(EmployeeId, FormID).ToList();
        }

        public static List<DynamicFormPublish> GetAllPublishedDynamicForms()
        {

            List<DynamicFormPublish> list = PayrollDataContext.DynamicFormPublishes.OrderBy(x => x.FormID).GroupBy(x => x.FormID).Select(x => x.First()).ToList();

            foreach (var item in list)
            {
                item.FormName = GetDynamicFormNameUsingFormId(item.FormID);
            }
            return list;
        }


    }
}


