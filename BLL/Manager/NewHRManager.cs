﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using DAL;
using Utils;
using System.Xml.Linq;
using Utils.Calendar;
using BLL.Entity;
using System.Web;
using System.IO;
using System.Collections;
using BLL.BO;
using Utils.Security;
using System.Diagnostics;

namespace BLL.Manager
{
    public class NewHRManager : BaseBiz
    {

        private static NewHRManager m_NewHRManager = null;
        public static NewHRManager Instance
        {
            get
            {
                if (m_NewHRManager == null)
                    m_NewHRManager = new NewHRManager();

                return m_NewHRManager;

            }
        }




        #region Education
        public static List<HEducation> GetEducationByEmployeeID(int EmployeeId)
        {
            List<HEducation> list = PayrollDataContext.HEducations.Where(x => x.EmployeeId == EmployeeId).OrderBy(x => x.Order == null ? 0 : x.Order.Value).ToList();

            foreach (var item in list)
            {
                item.IsEditable = NewHRManager.IsHrEditable(item.Status);
                if (item.ServerFileName == null || item.ServerFileName == "")
                    item.ContainsFile = 0;
                else
                    item.ContainsFile = 1;
            }

            return list;
        }

        public static HEducation GetEducationDetailsById(int EducationID)
        {
            return PayrollDataContext.HEducations.SingleOrDefault(
                e => e.EductionId == EducationID);
        }

        public static bool DeleteEducationByID(int EducationID)
        {
            HEducation _HData = PayrollDataContext.HEducations.SingleOrDefault(t => t.EductionId == EducationID);
            PayrollDataContext.HEducations.DeleteOnSubmit(_HData);
            PayrollDataContext.SubmitChanges();
            return true;
        }


        public Status InsertUpdateEducation(HEducation _HEducation, bool IsInsert)
        {
            Status myStatus = new Status();

            int order = 1;
            //Insert Operation
            if (IsInsert)
            {
                _HEducation.Modifiedby = SessionManager.CurrentLoggedInUserID;
                _HEducation.ModifiedOn = BLL.BaseBiz.GetCurrentDateAndTime();

                HEducation lastRecord = PayrollDataContext.HEducations.Where(x => x.EmployeeId == _HEducation.EmployeeId).OrderByDescending(x => x.Order).FirstOrDefault();
                if (lastRecord != null && lastRecord.Order != null)
                    order = lastRecord.Order.Value + 1;

                _HEducation.Order = order;

                if (SessionManager.CurrentLoggedInEmployeeId == 0)
                {
                    _HEducation.ApprovedBy = SessionManager.CurrentLoggedInUserID;
                    _HEducation.ApprovedOn = BLL.BaseBiz.GetCurrentDateAndTime();
                    _HEducation.Status = (int)HRStatusEnum.Approved;
                }
                else
                {
                    _HEducation.Status = (int)HRStatusEnum.Saved;
                }
                PayrollDataContext.HEducations.InsertOnSubmit(_HEducation);
                PayrollDataContext.SubmitChanges();
                return myStatus;
            }

            HEducation dbRecord = PayrollDataContext.HEducations.SingleOrDefault(c => c.EductionId == _HEducation.EductionId);

            if (dbRecord == null)
            {
                myStatus.ErrorMessage = "error while saving.";//GetConcurrencyErrorMsg();//need to implement this on later.
                myStatus.IsSuccess = false;
                return myStatus;
            }
            else
            {
                _HEducation.Modifiedby = SessionManager.CurrentLoggedInUserID;
                _HEducation.ModifiedOn = BLL.BaseBiz.GetCurrentDateAndTime();
                if (SessionManager.CurrentLoggedInEmployeeId == 0)
                {
                    _HEducation.ApprovedBy = SessionManager.CurrentLoggedInUserID;
                    _HEducation.ApprovedOn = BLL.BaseBiz.GetCurrentDateAndTime();
                    _HEducation.Status = (int)HRStatusEnum.Approved;
                }
                else
                {
                    _HEducation.Status = (int)HRStatusEnum.Saved;
                }

                CopyObject<HEducation>(_HEducation, ref dbRecord, "EducationID", "EmployeeID");
                PayrollDataContext.SubmitChanges();
                return myStatus;
            }

        }

        public Status DeleteEducationFile(int EducationID)
        {
            Status myStatus = new Status();
            HEducation dbRecord = PayrollDataContext.HEducations.SingleOrDefault(c => c.EductionId == EducationID);

            if (dbRecord == null)
            {
                myStatus.ErrorMessage = "error while saving.";//GetConcurrencyErrorMsg();//need to implement this on later.
                myStatus.IsSuccess = false;
                return myStatus;
            }
            else
            {
                dbRecord.FileFormat = "";
                dbRecord.FileLocation = "";
                dbRecord.ServerFileName = "";
                dbRecord.Size = "";
                dbRecord.UserFileName = "";
                dbRecord.FileType = "";
                PayrollDataContext.SubmitChanges();
                return myStatus;
            }
        }
        #endregion
        #region Training
        public static List<HTraining> GetTrainingByEmployeeID(int EmployeeId)
        {
            List<HTraining> list = PayrollDataContext.HTrainings.Where(x => x.EmployeeId == EmployeeId).ToList();

            foreach (var item in list)
            {
                item.IsEditable = NewHRManager.IsHrEditable(item.Status);
                if (item.ServerFileName == null || item.ServerFileName == "")
                    item.ContainsFile = 0;
                else
                    item.ContainsFile = 1;
            }

            return list;
        }


        public static List<ReportNewHR_TrainingReportResult> GetTrainingList(int start, int pagesize, int employeeId,
          string startDate, string endDate, int branchId = -1, int levelId = -1, int trainingTypeId = -1, string name = "")
        {
            DateTime? startDateEng = null, endDateEng = null;
            if (!string.IsNullOrEmpty(startDate))
            {
                startDateEng = Convert.ToDateTime(startDate);
                //startDateEng = new DateTime(startDateEng.Value.Year, startDateEng.Value.Month, 1);
            }

            if (!string.IsNullOrEmpty(endDate))
                endDateEng = Convert.ToDateTime(endDate);


            List<ReportNewHR_TrainingReportResult> list = PayrollDataContext.ReportNewHR_TrainingReport(start, pagesize, "", employeeId, startDateEng, endDateEng,
                branchId, levelId, trainingTypeId, name).ToList();

            return list;
        }


        public static List<ReportNewHR_GenderWiseStatusResult> GetGenderWiseStatusReport(int start, int pagesize, string StatusName)
        {
            return PayrollDataContext.ReportNewHR_GenderWiseStatus(start, pagesize, "", StatusName).ToList();
        }

        public static HTraining GetTrainingDetailsById(int TrainingID)
        {
            return PayrollDataContext.HTrainings.SingleOrDefault(
                e => e.TrainingId == TrainingID);
        }

        public static bool DeleteTrainingByID(int TrainingID)
        {
            HTraining AssetDocument = PayrollDataContext.HTrainings.SingleOrDefault(t => t.TrainingId == TrainingID);
            PayrollDataContext.HTrainings.DeleteOnSubmit(AssetDocument);
            PayrollDataContext.SubmitChanges();
            return true;
        }

        public HDocument GetDocument(int docID)
        {
            return PayrollDataContext.HDocuments.SingleOrDefault(
                e => e.DocumentId == docID);
        }

        public static List<LevelGradeChangeDetail> GetLevelGradeHistory(int employeeId)
        {
            //if (CommonManager.CompanySetting.WhichCompany==WhichCompany.RBB)
            {
                List<LevelGradeChangeDetail> list =
                    PayrollDataContext.LevelGradeChangeDetails.
                        Where(x => x.EmployeeId == employeeId && x.Status == (int)LevelGradeChangeStatusType.Approved)
                        .OrderBy(x => x.FromDateEng)
                        .ToList();

                foreach (LevelGradeChangeDetail item in list)
                {
                    if (item.PrevLevelId != null)
                    {
                        BLevel level = NewPayrollManager.GetLevelById(item.PrevLevelId.Value);
                        item.OldLevel = level.BLevelGroup.Name + " - " + level.Name;
                    }
                    else
                        item.OldLevel = item.PrevLevelName;

                    if (item.LevelId != null)
                    {
                        BLevel level = NewPayrollManager.GetLevelById(item.LevelId.Value);
                        item.NewLevel = level.BLevelGroup.Name + " - " + level.Name;
                    }
                    else
                        item.NewLevel = item.LevelName;


                    // if prev and new level matches then it is "Grade Change"
                    if ((item.LevelId != null && item.LevelId == item.PrevLevelId)
                            || (item.NewLevel != null && item.PrevLevelName != null && item.NewLevel.Equals(item.PrevLevelName)))
                        item.ChangeType = "Grade Change";
                    else if (item.LevelGradeChange.Type != null)
                        item.ChangeType = ((LevelGradeChangeType)item.LevelGradeChange.Type.Value).ToString();


                    // do not append designation name in listing for citizn as designation name and level is same so text is being repeated
                    if (CommonManager.CompanySetting.WhichCompany != WhichCompany.Citizen
                        && CommonManager.CompanySetting.WhichCompany != WhichCompany.Civil)
                    {
                        // append designation name in level also
                        DesignationHistory desig = PayrollDataContext.DesignationHistories.
                            FirstOrDefault(x => x.EmployeeId == item.EmployeeId && x.FromDateEng == item.FromDateEng);
                        if (desig != null && desig.FromDesignationId != null)
                        {
                            EDesignation designation = PayrollDataContext.EDesignations.FirstOrDefault(x => x.DesignationId == desig.FromDesignationId.Value);
                            item.OldLevel += " - " + designation.Name;
                        }


                        if (desig != null && desig.DesignationId != null)
                        {
                            EDesignation designation = PayrollDataContext.EDesignations.FirstOrDefault(x => x.DesignationId == desig.DesignationId.Value);
                            item.NewLevel += " - " + designation.Name;
                        }
                    }
                }

                return list;
            }
            //else
            //{
            //    PEmployeeIncome basicIncome =new PayManager().GetEmployeeIncome(
            //        employeeId,new PayManager().GetBasicIncome(SessionManager.CurrentCompanyId).IncomeId);

            //    List<PEmployeeIncrement> levelChange =
            //       PayrollDataContext.PEmployeeIncrements.Where(x => x.EmployeeIncomeId == basicIncome.EmployeeIncomeId)
            //       .OrderBy(x => x.Id).ToList();

            //    List<LevelGradeChangeDetail> list = new List<LevelGradeChangeDetail>();
            //    foreach (PEmployeeIncrement item in levelChange)
            //    {
            //        if(item.OldLevelId != null && item.NewLevelId != null)
            //        {
            //            LevelGradeChangeDetail detail = new LevelGradeChangeDetail();
            //            detail.FromDate = item.EffectiveFromDate;
            //            detail.FromDateEng = item.EffectiveFromDateEng;
            //            detail.OldLevel = NewPayrollManager.GetLevelById(item.OldLevelId.Value).Name;
            //            detail.PrevStepGrade = item.OldStepGrade.Value;
            //            detail.NewLevel = NewPayrollManager.GetLevelById(item.NewLevelId.Value).Name;
            //            detail.StepGrade = item.NewStepGrade.Value;
            //            detail.ChangeType = "";

            //            list.Add(detail);
            //        }
            //    }

            //    return list;
            //}
        }

        public static List<GetLevelGradeChangeDetailsSPResult> GetLevelGradeReport(int currentPage, int pageSize, DateTime? startDate, DateTime? endDate, int? employeeId
            , bool? type, int? levelGradeId)
        {
            //if (CommonManager.CompanySetting.WhichCompany==WhichCompany.RBB)
            EmployeeManager mgr = new EmployeeManager();
            {
                //List<LevelGradeChangeDetail> list =
                //    PayrollDataContext.LevelGradeChangeDetails.
                //        Where(x => x.Status == (int)LevelGradeChangeStatusType.Approved &&
                //            (employeeId == null || employeeId == x.EmployeeId) &&
                //            (startDate==null || x.FromDateEng >= startDate) &&
                //            (endDate == null || x.FromDateEng <= endDate)
                //            )
                //        .OrderByDescending(x => x.DetailId)
                //        .ToList();


                List<GetLevelGradeChangeDetailsSPResult> list = new List<GetLevelGradeChangeDetailsSPResult>();
                list = PayrollDataContext.GetLevelGradeChangeDetailsSP(currentPage, pageSize, employeeId, startDate, endDate, type, levelGradeId).ToList();

                //foreach (GetLevelGradeChangeDetailsSPResult item in list)
                //{

                //    if (item.PrevLevelId != null)
                //    {
                //        BLevel level = NewPayrollManager.GetLevelById(item.PrevLevelId.Value);

                //        if (level != null)
                //        {
                //            item.OldLevel = level.BLevelGroup.Name + " - " + level.Name;
                //        }
                //    }
                //    else
                //        item.OldLevel = item.PrevLevelName;


                //    if (item.LevelId != null)
                //    {
                //        BLevel level = NewPayrollManager.GetLevelById(item.LevelId.Value);

                //        if (level != null)
                //            item.NewLevel = level.BLevelGroup.Name + " - " + level.Name;
                //    }
                //    else
                //        item.NewLevel = item.LevelName;

                //    EEmployee emp = mgr.GetById(item.EmployeeId.Value);
                //    if (emp != null)
                //        item.Name = emp.Name;
                //    else
                //        continue;

                //    PEmployeeIncrement increment = PayrollDataContext.PEmployeeIncrements
                //        .OrderBy(x => x.Id)
                //        .FirstOrDefault(x => x.SourceId == item.DetailId);
                //    if (increment != null)
                //    {
                //        if (PayrollDataContext.CCalculationIncludeds.Any(x => x.Type == 16 && x.SourceId == increment.Id))
                //        {
                //            item.IsUsedInSalary = true;
                //        }
                //        else
                //            item.IsUsedInSalary = false;

                //        item.IsBackdatedPromotion = increment.IsReterospect;
                //    }
                //    else
                //    {
                //        item.IsUsedInSalary = false;
                //    }

                //}

                return list;
            }
            //else
            //{
            //    PEmployeeIncome basicIncome =new PayManager().GetEmployeeIncome(
            //        employeeId,new PayManager().GetBasicIncome(SessionManager.CurrentCompanyId).IncomeId);

            //    List<PEmployeeIncrement> levelChange =
            //       PayrollDataContext.PEmployeeIncrements.Where(x => x.EmployeeIncomeId == basicIncome.EmployeeIncomeId)
            //       .OrderBy(x => x.Id).ToList();

            //    List<LevelGradeChangeDetail> list = new List<LevelGradeChangeDetail>();
            //    foreach (PEmployeeIncrement item in levelChange)
            //    {
            //        if(item.OldLevelId != null && item.NewLevelId != null)
            //        {
            //            LevelGradeChangeDetail detail = new LevelGradeChangeDetail();
            //            detail.FromDate = item.EffectiveFromDate;
            //            detail.FromDateEng = item.EffectiveFromDateEng;
            //            detail.OldLevel = NewPayrollManager.GetLevelById(item.OldLevelId.Value).Name;
            //            detail.PrevStepGrade = item.OldStepGrade.Value;
            //            detail.NewLevel = NewPayrollManager.GetLevelById(item.NewLevelId.Value).Name;
            //            detail.StepGrade = item.NewStepGrade.Value;
            //            detail.ChangeType = "";

            //            list.Add(detail);
            //        }
            //    }

            //    return list;
            //}
        }
        public static bool IsBranchEditable(int employeeId)
        {
            if (PayrollDataContext.BranchDepartmentHistories.Any
                (x => x.EmployeeId == employeeId && (x.IsFirst == null || x.IsFirst == false)))
            //&& x.FromBranchId != x.BranchId))
            {
                return false;
            }
            return true;
        }

        public static bool HasEmployeeLevelGrade(int employeeId)
        {
            if (CommonManager.CompanySetting.HasLevelGradeSalary == false)
                return false;

            if (PayrollDataContext.PEmployeeIncomes.Any(x => x.EmployeeId == employeeId &&
                x.LevelId != null))
                return true;

            return false;
        }

        public static bool IsEmployeeLastStatusForLevelGradeEligible(int employeeId)
        {
            if (CommonManager.CompanySetting.HasLevelGradeSalary == false)
                return false;

            ECurrentStatus lastStatus = PayrollDataContext
                .ECurrentStatus.Where(x => x.EmployeeId == employeeId)
                .OrderByDescending(x => x.FromDateEng)
                .FirstOrDefault();


            //first check in the company if it has Level/Grade system from setting or not
            if (EmployeeManager.IsContractStatus(lastStatus.CurrentStatus))
            {
                return false;
            }
            return true;

        }

        public static Status UpdateRetirement(EEmployee entity)
        {
            Status status = new Status();

            #region "Retirement"

            //if reg is set
            bool isRetired, dbIsRetired;

            EEmployee dbEntity = EmployeeManager.GetEmployeeById(entity.EmployeeId);


            isRetired = entity.IsRetired != null ? entity.IsRetired.Value : false;
            dbIsRetired = dbEntity.IsRetired.Value;

            dbEntity.IsRetired = entity.IsRetired == null ? false : true;

            //if ret is set
            if (dbIsRetired == false && isRetired == true)
            {
                PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(dbEntity.EmployeeId, (byte)MonetoryChangeLogTypeEnum.Information
                    , "Retirement", "-", entity.EHumanResources[0].DateOfRetirement, LogActionEnum.Add));

                PayrollPeriod period = PayrollDataContext.PayrollPeriods.FirstOrDefault(x => dbEntity.EHumanResources[0].DateOfRetirementEng >= x.StartDateEng && dbEntity.EHumanResources[0].DateOfRetirementEng <= x.EndDateEng);
                if (period != null)
                {
                    CCalculationEmployee calcEmployee
                        =
                        (
                            from ce in PayrollDataContext.CCalculationEmployees
                            join c in PayrollDataContext.CCalculations on ce.CalculationId equals c.CalculationId
                            where c.PayrollPeriodId == period.PayrollPeriodId
                            && ce.EmployeeId == dbEntity.EmployeeId
                            select ce
                        ).FirstOrDefault();

                    if (calcEmployee != null)
                        calcEmployee.IsRetiredOrResigned = true;

                }

            }
            //if ret set is changed
            else if (dbIsRetired == true && isRetired == true && dbEntity.EHumanResources[0].DateOfResignation != entity.EHumanResources[0].DateOfResignation)
            {
                if (dbEntity.EHumanResources[0].DateOfResignation != entity.EHumanResources[0].DateOfResignation)
                    PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(dbEntity.EmployeeId, (byte)MonetoryChangeLogTypeEnum.Information
                        , "Retirement", dbEntity.EHumanResources[0].DateOfRetirement, entity.EHumanResources[0].DateOfRetirement, LogActionEnum.Update));
            }
            //if ret date is removed
            else if (dbIsRetired == true && isRetired == false)
            {
                PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(dbEntity.EmployeeId, (byte)MonetoryChangeLogTypeEnum.Information
                    , "Retirement", dbEntity.EHumanResources[0].DateOfRetirement, "-", LogActionEnum.Delete));


            }



            if (dbEntity.IsRetired.Value)
            {
                dbEntity.IsRetired = entity.IsRetired;
                dbEntity.EHumanResources[0].DateOfRetirementEng = GetEngDate(entity.EHumanResources[0].DateOfRetirement, null);
                dbEntity.EHumanResources[0].DateOfRetirement = entity.EHumanResources[0].DateOfRetirement;
                dbEntity.EHumanResources[0].RetirementType = entity.EHumanResources[0].RetirementType;
            }

            #endregion

            PayrollDataContext.SubmitChanges();

            return status;
        }

        public static int GetTotalActiveEmp()
        {
            return (

               from x in PayrollDataContext.EEmployees
               join h in PayrollDataContext.EHumanResources on x.EmployeeId equals h.EmployeeId

               where x.CompanyId == SessionManager.CurrentCompanyId
               && (
                    (
                        x.IsResigned == false ||
                        h.DateOfResignationEng >= DateTime.Now.Date
                        )
                    &&
                     (
                        x.IsRetired == false ||
                        h.DateOfRetirementEng >= DateTime.Now.Date
                        )
                )
               select x
                ).Count();
        }
        public static Status IsEmployeeCountValidForNewEmp()
        {
            Status status = new Status();

            string encrypted = CommonManager.Setting.Value;

            if (string.IsNullOrEmpty(encrypted))
                return status;

            EncryptorDecryptor enc = new EncryptorDecryptor();

            try
            {

                string value = enc.Decrypt(encrypted);
                string[] strs = value.Split(new char[] { ':' });

                int vaildCount = 0;

                if (int.TryParse(strs[1], out vaildCount) == false)
                    return status;

                int currentCount = GetTotalActiveEmp();

                if (currentCount + 1 > vaildCount)
                {
                    status.ErrorMessage = "Current HR licensed enabled for total " + vaildCount + " employee(s) only, current active employee count is " +
                        currentCount + ".";
                    return status;
                }


            }
            catch
            {
                return status;
            }
            return status;
        }

        public static Status InsertUpdateNewEmployee(EEmployee entity, bool isInsert)
        {
            bool isFirstStatusFromDateChanged = false;
            Status status = new Status();
            EEmployee dbEntity = new EEmployee();
            dbEntity.Modified = CustomDate.GetTodayDate(true).EnglishDate;

            if (isInsert)
            {

                status = IsEmployeeCountValidForNewEmp();
                if (status.IsSuccess == false)
                    return status;


                #region "Process for custom EIN"

                if (CommonManager.Setting.IsEINEditable != null && CommonManager.Setting.IsEINEditable.Value)
                {


                    EEmployee einEmp = PayrollDataContext.EEmployees.FirstOrDefault(x => x.EmployeeId == entity.EmployeeId);
                    if (einEmp != null)
                    {
                        status.ErrorMessage = "EIN number already exists for the employee " + einEmp.Name + ", please use different EIN number.";
                        return status;
                    }

                    PayrollDataContext.CreateBlankEmployee(entity.EmployeeId);
                    EEmployee dbEmployee = PayrollDataContext.EEmployees.SingleOrDefault(x => x.EmployeeId == entity.EmployeeId);
                    CopyObject<EEmployee>(entity, ref dbEmployee, "EmployeeId", "Created");

                    dbEmployee.CreatedBy = SessionManager.CurrentLoggedInUserID;
                    dbEmployee.ModifiedBy = SessionManager.CurrentLoggedInUserID;

                    dbEmployee.IsHandicapped = false;
                    dbEmployee.IsLocalEmployee = false;
                    dbEmployee.IsNonResident = false;
                    dbEmployee.IsResigned = false;
                    dbEmployee.IsRetired = false;
                    dbEmployee.HasCoupleTaxStatus = false;
                    entity.CompanyId = SessionManager.CurrentCompanyId;

                    if (CommonManager.Setting.EnableWeeklyLikePay != null && CommonManager.Setting.EnableWeeklyLikePay.Value)
                    {
                        if (dbEmployee.PayGroupRef_ID != null)
                            dbEmployee.ExcludeInSalary = true;
                    }

                    dbEmployee.EHumanResources.AddRange(entity.EHumanResources);
                    dbEmployee.EFundDetails.AddRange(entity.EFundDetails);
                    dbEmployee.PEmployeeIncomes.AddRange(entity.PEmployeeIncomes);
                    dbEmployee.PEmployeeDeductions.AddRange(entity.PEmployeeDeductions);
                    entity.ECurrentStatus[0].EventID = (int)ServiceEventType.Appointment;
                    dbEmployee.ECurrentStatus.AddRange(entity.ECurrentStatus);
                    dbEmployee.EWorkShiftHistories.AddRange(entity.EWorkShiftHistories);
                    dbEmployee.HHumanResources.AddRange(entity.HHumanResources);

                    dbEmployee.PEmployeeIncomes[0].IncomeId = new PayManager().GetBasicIncome(SessionManager.CurrentCompanyId).IncomeId;
                    // If has level/grade then only mark as Valid for salary generation
                    if (EmployeeManager.HasLevelGradeForStatus(dbEmployee.ECurrentStatus.OrderBy(x => x.FromDateEng).First().CurrentStatus, entity.SkipLevelGradeMatrix))
                    {
                        dbEmployee.PEmployeeIncomes[0].IsValid = true;
                    }
                    dbEmployee.PEmployeeIncomes[0].IsEnabled = true;

                    dbEmployee.Created = dbEntity.Modified;
                    dbEmployee.Modified = dbEntity.Modified;

                    if (dbEmployee.EHumanResources.Count <= 0)
                    {
                        dbEmployee.EHumanResources.Add(new EHumanResource { IdCardNo = "" });
                    }
                    if (dbEmployee.EFundDetails.Count <= 0)
                    {
                        dbEmployee.EFundDetails.Add(new EFundDetail { NoCITContribution = false });
                    }
                    if (dbEmployee.PPays.Count <= 0)
                    {
                        dbEmployee.PPays.Add(new PPay { });
                    }
                    if (dbEmployee.EAddresses.Count <= 0)
                    {
                        dbEmployee.EAddresses.Add(new EAddress { CIEmail = "" });
                    }

                    dbEmployee.CompanyId = SessionManager.CurrentCompanyId;
                    dbEmployee.EditSequence = 1;


                    #region "Save into service history also"
                    if (CommonManager.IsServiceHistoryEnabled)
                    {
                        // insert into Service event also
                        EmployeeServiceHistory history = new EmployeeServiceHistory();
                        history.BranchId = entity.BranchId;
                        history.CreatedBy = SessionManager.CurrentLoggedInUserID;
                        history.CreatedOn = GetCurrentDateAndTime();
                        history.Date = dbEmployee.ECurrentStatus[0].FromDate;
                        history.DateEng = dbEmployee.ECurrentStatus[0].FromDateEng;
                        history.DepartmentId = entity.DepartmentId;
                        history.SubDepartmentId = entity.SubDepartmentId;
                        history.DesignationId = entity.DesignationId;
                        history.EmployeeId = entity.EmployeeId;
                        history.EventID = (short)ServiceEventType.Appointment;
                        history.LetterNo = dbEmployee.EHumanResources[0].AppointmentLetterNo;
                        history.LetterDate = dbEmployee.EHumanResources[0].AppointmentLetterDate;
                        history.LetterDateEng = dbEmployee.EHumanResources[0].AppointmentLetterDateEng;
                        history.LevelId = CommonManager.GetLevelIDForDesignation(history.DesignationId.Value);
                        history.ModifiedBy = SessionManager.CurrentLoggedInUserID;
                        history.ModifiedOn = history.CreatedOn;
                        history.ServiceSerialNo = 1;
                        history.StatusId = dbEmployee.ECurrentStatus[0].CurrentStatus;
                        history.ToDate = dbEmployee.ECurrentStatus[0].ToDate;
                        history.ToDateEng = dbEmployee.ECurrentStatus[0].ToDateEng;
                        history.Notes = "";
                        history.PayrollPeriodId = CommonManager.GetLastPayrollPeriod().PayrollPeriodId;
                        PayrollDataContext.EmployeeServiceHistories.InsertOnSubmit(history);

                        // save to get EventSourceID
                        PayrollDataContext.SubmitChanges();

                        BranchDepartmentHistory first = new BranchDepartmentHistory();
                        first.EmployeeId = entity.EmployeeId;
                        first.FromBranchId = entity.BranchId;
                        first.FromDepartmentId = entity.DepartmentId;
                        first.BranchId = entity.BranchId;
                        first.DepeartmentId = entity.DepartmentId;
                        first.SubDepartmentId = entity.SubDepartmentId;
                        first.FromDate = dbEmployee.ECurrentStatus[0].FromDate;
                        first.FromDateEng = dbEmployee.ECurrentStatus[0].FromDateEng;
                        first.IsFirst = true;
                        first.EventID = (int)ServiceEventType.Appointment;
                        first.EventSourceID = history.EventSourceID;
                        PayrollDataContext.BranchDepartmentHistories.InsertOnSubmit(first);

                        DesignationHistory firstDesig = new DesignationHistory();
                        firstDesig.EmployeeId = entity.EmployeeId;
                        firstDesig.DesignationId = entity.DesignationId;
                        firstDesig.FromDate = dbEmployee.ECurrentStatus[0].FromDate;
                        firstDesig.FromDateEng = dbEmployee.ECurrentStatus[0].FromDateEng;
                        firstDesig.IsFirst = true;
                        firstDesig.EventID = (int)ServiceEventType.Appointment;
                        firstDesig.EventSourceID = history.EventSourceID;
                        PayrollDataContext.DesignationHistories.InsertOnSubmit(firstDesig);

                        dbEmployee.ECurrentStatus[0].EventSourceID = history.EventSourceID;
                    }

                    #endregion

                    PayrollDataContext.SubmitChanges();

                    // for NIBL set EIN to Ino
                    if (isInsert && CommonManager.CompanySetting.WhichCompany == WhichCompany.NIBL)
                    {
                        dbEmployee.EHumanResources[0].IdCardNo = entity.EmployeeId.ToString();
                        if (PayrollDataContext.AttendanceMappings.Any(x => x.EmployeeId == entity.EmployeeId) == false)
                        {
                            PayrollDataContext.AttendanceMappings.InsertOnSubmit(
                                new AttendanceMapping { EmployeeId = entity.EmployeeId, DeviceId = entity.EmployeeId.ToString() });
                        }


                        PayrollDataContext.SubmitChanges();
                    }

                    return status;

                }

                #endregion
            }

            if (isInsert)
            {

                entity.CreatedBy = SessionManager.CurrentLoggedInUserID;
                entity.ModifiedBy = SessionManager.CurrentLoggedInUserID;


                entity.IsHandicapped = false;
                entity.IsLocalEmployee = false;
                entity.IsNonResident = false;
                entity.IsResigned = false;
                entity.IsRetired = false;
                entity.HasCoupleTaxStatus = false;
                entity.CompanyId = SessionManager.CurrentCompanyId;

                if (CommonManager.Setting.EnableWeeklyLikePay != null && CommonManager.Setting.EnableWeeklyLikePay.Value)
                {
                    if (entity.PayGroupRef_ID != null)
                        entity.ExcludeInSalary = true;
                }

                entity.PEmployeeIncomes[0].IncomeId = new PayManager().GetBasicIncome(SessionManager.CurrentCompanyId).IncomeId;
                // If has level/grade then only mark as Valid for salary generation
                if (EmployeeManager.HasLevelGradeForStatus(entity.ECurrentStatus.OrderBy(x => x.FromDateEng).First().CurrentStatus, entity.SkipLevelGradeMatrix))
                {
                    entity.PEmployeeIncomes[0].IsValid = true;
                }
                entity.PEmployeeIncomes[0].IsEnabled = true;

                PayrollDataContext.EEmployees.InsertOnSubmit(entity);

                entity.Created = dbEntity.Modified;
                entity.Modified = dbEntity.Modified;

                if (entity.EHumanResources.Count <= 0)
                {
                    entity.EHumanResources.Add(new EHumanResource { IdCardNo = "" });
                }



                if (entity.EFundDetails.Count <= 0)
                {
                    entity.EFundDetails.Add(new EFundDetail { NoCITContribution = false });
                }
                if (entity.PPays.Count <= 0)
                {
                    entity.PPays.Add(new PPay { });
                }
                if (entity.EAddresses.Count <= 0)
                {
                    entity.EAddresses.Add(new EAddress { CIEmail = "" });
                }
            }
            else
            {
                dbEntity = PayrollDataContext.EEmployees.Where(x => x.EmployeeId == entity.EmployeeId).FirstOrDefault();


                #region "Change Logs"
                if (dbEntity.Name != entity.Name)
                    PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(entity.EmployeeId, (byte)MonetoryChangeLogTypeEnum.Name
                        , "Name", dbEntity.Name, entity.Name, LogActionEnum.Update));

                if (dbEntity.Gender != entity.Gender)
                    PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(entity.EmployeeId, (byte)MonetoryChangeLogTypeEnum.Gender
                        , "Gender", EmployeeManager.GetGenderValue(dbEntity.Gender), EmployeeManager.GetGenderValue(entity.Gender), LogActionEnum.Update));


                if (CommonManager.IsUnitEnabled)
                {
                    if (dbEntity.UnitId != entity.UnitId)
                        PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(entity.EmployeeId, (byte)MonetoryChangeLogTypeEnum.Information
                            , "Unit", dbEntity.UnitId == null ? "" : NewHRManager.GetUnitById(dbEntity.UnitId.Value).Name, entity.UnitId == null ? "" : NewHRManager.GetUnitById(entity.UnitId.Value).Name, LogActionEnum.Update));
                }

                if (dbEntity.MaritalStatus != entity.MaritalStatus)
                    PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(entity.EmployeeId, (byte)MonetoryChangeLogTypeEnum.TaxStatus
                        , "Marital Status", dbEntity.MaritalStatus, entity.MaritalStatus, LogActionEnum.Update));


                if (dbEntity.DateOfBirthEng != entity.DateOfBirthEng)
                    PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(entity.EmployeeId, (byte)MonetoryChangeLogTypeEnum.Information
                        , "DOB", (dbEntity.DateOfBirthEng == null ? "" : dbEntity.DateOfBirthEng.Value.ToShortDateString()),
                        (entity.DateOfBirthEng == null ? "" : entity.DateOfBirthEng.Value.ToShortDateString()), LogActionEnum.Update));

                if (dbEntity.SkipLevelGradeMatrix != entity.SkipLevelGradeMatrix)
                    PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(entity.EmployeeId, (byte)MonetoryChangeLogTypeEnum.Information
                        , "Exclude Salary Matrix", dbEntity.SkipLevelGradeMatrix, entity.SkipLevelGradeMatrix, LogActionEnum.Update));

                //if (dbEntity.HasDependent != entity.HasDependent)
                //    PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(entity.EmployeeId, (byte)MonetoryChangeLogTypeEnum.TaxStatus
                //        , "Dependent", dbEntity.HasDependent, entity.HasDependent, LogActionEnum.Update));

                //if (dbEntity.IsHandicapped != entity.IsHandicapped)
                //    PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(entity.EmployeeId, (byte)MonetoryChangeLogTypeEnum.TaxStatus
                //        , "Handicapped", dbEntity.IsHandicapped, entity.IsHandicapped, LogActionEnum.Update));



                //if (dbEntity.IsLocalEmployee != entity.IsLocalEmployee)
                //    PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(entity.EmployeeId, (byte)MonetoryChangeLogTypeEnum.Information
                //        , "Local Employee", dbEntity.IsLocalEmployee, entity.IsLocalEmployee, LogActionEnum.Update));

                // if pay group field changed
                if (CommonManager.Setting.EnableWeeklyLikePay != null && CommonManager.Setting.EnableWeeklyLikePay.Value)
                {
                    if (dbEntity.PayGroupRef_ID != entity.PayGroupRef_ID)
                    {
                        if (entity.PayGroupRef_ID != null)
                        {
                            dbEntity.ExcludeInSalary = true;
                        }
                        else
                        {
                            dbEntity.ExcludeInSalary = false;
                        }
                    }
                }


                if (dbEntity.PayGroupRef_ID != entity.PayGroupRef_ID)
                    PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(entity.EmployeeId, (byte)MonetoryChangeLogTypeEnum.PayGroup
                        , "Pay Group", dbEntity.PayGroupRef_ID == null ? "" : PeriodicPayManager.GetPayGroup(dbEntity.PayGroupRef_ID.Value).Name,
                        entity.PayGroupRef_ID == null ? "" : PeriodicPayManager.GetPayGroup(entity.PayGroupRef_ID.Value).Name, LogActionEnum.Update));


                if (dbEntity.BranchId != entity.BranchId)
                    PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(entity.EmployeeId, (byte)MonetoryChangeLogTypeEnum.Branch
                        , "Branch", new BranchManager().GetById(dbEntity.BranchId.Value).Name, new BranchManager().GetById(entity.BranchId.Value).Name, LogActionEnum.Update));

                if (dbEntity.DepartmentId != entity.DepartmentId)
                    PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(entity.EmployeeId, (byte)MonetoryChangeLogTypeEnum.Department
                        , "Department", new DepartmentManager().GetById(dbEntity.DepartmentId.Value).Name, new DepartmentManager().GetById(entity.DepartmentId.Value).Name, LogActionEnum.Update));

                if (dbEntity.DesignationId != entity.DesignationId)
                    PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(entity.EmployeeId, (byte)MonetoryChangeLogTypeEnum.Designation
                        , "Designation", new CommonManager().GetDesignationById(dbEntity.DesignationId.Value).Name, new CommonManager().GetDesignationById(entity.DesignationId.Value).Name, LogActionEnum.Update));

                #endregion

                dbEntity.Title = entity.Title;

                dbEntity.ModifiedBy = SessionManager.CurrentLoggedInUserID;
                dbEntity.Modified = GetCurrentDateAndTime();

                dbEntity.Gender = entity.Gender;

                dbEntity.UnitId = entity.UnitId;
                dbEntity.MotherTongue = entity.MotherTongue;
                dbEntity.Children = entity.Children;
                dbEntity.FirstName = entity.FirstName;
                dbEntity.MiddleName = entity.MiddleName;
                dbEntity.LastName = entity.LastName;
                dbEntity.Name = entity.Name;
                dbEntity.NameNepali = entity.NameNepali;
                dbEntity.FatherName = entity.FatherName;
                dbEntity.MotherName = entity.MotherName;
                dbEntity.GradeId = entity.GradeId;
                dbEntity.SubDepartmentId = entity.SubDepartmentId;
                dbEntity.FunctionalTitleId = entity.FunctionalTitleId;
                dbEntity.PayGroupRef_ID = entity.PayGroupRef_ID;


                dbEntity.MaritalStatus = entity.MaritalStatus;
                if (dbEntity.MaritalStatus != MaritalStatus.MARRIED)
                    dbEntity.HasCoupleTaxStatus = false;

                dbEntity.DateOfBirth = entity.DateOfBirth;
                dbEntity.IsEnglishDOB = entity.IsEnglishDOB;
                dbEntity.DateOfBirthEng = entity.DateOfBirthEng;

                dbEntity.PlaceOfBirth = entity.PlaceOfBirth;
                dbEntity.SkipLevelGradeMatrix = entity.SkipLevelGradeMatrix;
                dbEntity.Hobby = entity.Hobby;
                dbEntity.MarriageAniversary = entity.MarriageAniversary;
                dbEntity.MarriageAniversaryEng = entity.MarriageAniversaryEng;

                if (dbEntity.HHumanResources.Count() > 0)
                {
                    dbEntity.HHumanResources.First().Religion = entity.HHumanResources.First().Religion;
                    dbEntity.HHumanResources.First().CasteId = entity.HHumanResources.First().CasteId;
                    dbEntity.HHumanResources.First().HolidayGroupId = entity.HHumanResources.First().HolidayGroupId;
                }
                else
                {
                    HHumanResource resourceEntity = new HHumanResource();
                    if (entity.HHumanResources.Count > 0)
                    {
                        resourceEntity.Religion = entity.HHumanResources.First().Religion;
                        resourceEntity.CasteId = entity.HHumanResources.First().CasteId;
                        resourceEntity.HolidayGroupId = entity.HHumanResources.First().HolidayGroupId;
                    }
                    dbEntity.HHumanResources.Add(resourceEntity);
                }
                #region "Employeement details"

                //check for grade set in update then create in history table also
                if (IsBranchEditable(entity.EmployeeId))
                {
                    dbEntity.BranchId = entity.BranchId;
                    dbEntity.DepartmentId = entity.DepartmentId;
                }
                if (NewPayrollManager.IsDesignationEditable(entity.EmployeeId))
                    dbEntity.DesignationId = entity.DesignationId;




                if (dbEntity.EHumanResources[0].SalaryCalculateFrom != entity.EHumanResources[0].SalaryCalculateFrom)
                {
                    if (dbEntity.EHumanResources[0].SalaryCalculateFrom != entity.EHumanResources[0].SalaryCalculateFrom)

                        PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(entity.EmployeeId, (byte)MonetoryChangeLogTypeEnum.Information
                            , "Salary Calculation Date", dbEntity.EHumanResources[0].SalaryCalculateFrom, entity.EHumanResources[0].SalaryCalculateFrom, LogActionEnum.Update));

                    dbEntity.EHumanResources[0].SalaryCalculateFromEng = GetEngDate(entity.EHumanResources[0].SalaryCalculateFrom, IsEnglish);

                }
                dbEntity.EHumanResources[0].RetirementOptionType = entity.EHumanResources[0].RetirementOptionType;
                dbEntity.EHumanResources[0].SalaryCalculateFrom = entity.EHumanResources[0].SalaryCalculateFrom;
                dbEntity.EHumanResources[0].HireMethod = entity.EHumanResources[0].HireMethod;
                dbEntity.EHumanResources[0].AppointmentLetterDate = entity.EHumanResources[0].AppointmentLetterDate;
                dbEntity.EHumanResources[0].AppointmentLetterDateEng = entity.EHumanResources[0].AppointmentLetterDateEng;
                dbEntity.EHumanResources[0].AppointmentLetterNo = entity.EHumanResources[0].AppointmentLetterNo;

                if (dbEntity.EHumanResources[0].LocationId != entity.EHumanResources[0].LocationId
                    && dbEntity.EHumanResources[0].LocationId != null && entity.EHumanResources[0].LocationId != null)
                    PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(entity.EmployeeId, (byte)MonetoryChangeLogTypeEnum.Information
                        , "Location", new CommonManager().GetLocationById(dbEntity.EHumanResources[0].LocationId.Value).Name, new CommonManager().GetLocationById(entity.EHumanResources[0].LocationId.Value).Name, LogActionEnum.Update));

                dbEntity.EHumanResources[0].LocationId = entity.EHumanResources[0].LocationId;
                dbEntity.EHumanResources[0].HireReference = entity.EHumanResources[0].HireReference;

                DateTime? prevHireDate = null;

                //check if first emp status from date is changed or not
                if (!string.IsNullOrEmpty(dbEntity.ECurrentStatus.OrderBy(x => x.FromDateEng).First().FromDate)
                    && !dbEntity.ECurrentStatus.OrderBy(x => x.FromDateEng).First().FromDate.Equals(
                        entity.ECurrentStatus.OrderBy(x => x.FromDateEng).First().FromDate))
                {
                    isFirstStatusFromDateChanged = true;
                    prevHireDate = dbEntity.ECurrentStatus.OrderBy(x => x.FromDateEng).First().FromDateEng;
                }



                // change from date of first branch transfer is exists
                BranchDepartmentHistory firstBranch = PayrollDataContext.BranchDepartmentHistories
                    .Where(x => x.EmployeeId == entity.EmployeeId && x.FromDateEng == dbEntity.ECurrentStatus.OrderBy(y => y.FromDateEng).First().FromDateEng)
                    .OrderBy(x => x.FromDateEng).FirstOrDefault();
                if (firstBranch != null)
                {
                    if (isFirstStatusFromDateChanged)
                    {
                        firstBranch.FromDate = entity.ECurrentStatus.OrderBy(x => x.FromDateEng).First().FromDate;
                        firstBranch.FromDateEng = entity.ECurrentStatus.OrderBy(x => x.FromDateEng).First().FromDateEng;


                    }

                    if (IsBranchEditable(entity.EmployeeId))
                    {
                        firstBranch.FromBranchId = entity.BranchId;
                        firstBranch.BranchId = entity.BranchId;

                        firstBranch.DepeartmentId = entity.DepartmentId;
                        firstBranch.FromDepartmentId = entity.DepartmentId;
                    }

                }

                // Probation period
                #region "Care Case Only"
                // Case 1 : if probation is being added in update
                if (dbEntity.ProbationHistories.Count <= 0 && entity.ProbationHistories.Count >= 1)
                {
                    dbEntity.ProbationHistories.AddRange(entity.ProbationHistories);
                }
                // Case 2 : if probation is being updated
                else if (dbEntity.ProbationHistories.Count == 1 && entity.ProbationHistories.Count == 1)
                {
                    ProbationHistory firstProbation = dbEntity.ProbationHistories
                        .OrderBy(x => x.ProbationId).FirstOrDefault();
                    if (firstProbation != null)
                    {
                        firstProbation.FromDate = entity.ProbationHistories[0].FromDate;
                        firstProbation.FromDateEng = entity.ProbationHistories[0].FromDateEng;
                        firstProbation.ToDate = entity.ProbationHistories[0].ToDate;
                        firstProbation.ToDateEng = entity.ProbationHistories[0].ToDateEng;
                    }
                }
                // Case 3 : if probation is being removed
                else if (dbEntity.ProbationHistories.Count == 1 && entity.ProbationHistories.Count == 0)
                {
                    dbEntity.ProbationHistories.Clear();
                }
                #endregion 


                // change from date of first Designation if exists
                DesignationHistory firstDesignation = PayrollDataContext.DesignationHistories
                    .Where(x => x.EmployeeId == entity.EmployeeId && x.FromDateEng == dbEntity.ECurrentStatus.OrderBy(y => y.FromDateEng).First().FromDateEng)
                    .OrderBy(x => x.FromDateEng).FirstOrDefault();
                if (firstDesignation != null)
                {
                    if (isFirstStatusFromDateChanged)
                    {
                        firstDesignation.FromDate = entity.ECurrentStatus.OrderBy(x => x.FromDateEng).First().FromDate;
                        firstDesignation.FromDateEng = entity.ECurrentStatus.OrderBy(x => x.FromDateEng).First().FromDateEng;


                    }

                    if (NewPayrollManager.IsDesignationEditable(entity.EmployeeId))
                    {
                        firstDesignation.DesignationId = entity.DesignationId;
                    }

                }

                if (dbEntity.ECurrentStatus[0].CurrentStatus != entity.ECurrentStatus[0].CurrentStatus)
                    PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(entity.EmployeeId, (byte)MonetoryChangeLogTypeEnum.HireStatus
                        , "Hire Job Status", JobStatus.GetStatusForDisplay(dbEntity.ECurrentStatus[0].CurrentStatus), JobStatus.GetStatusForDisplay(entity.ECurrentStatus[0].CurrentStatus), LogActionEnum.Update));

                if (dbEntity.ECurrentStatus[0].FromDate != entity.ECurrentStatus[0].FromDate)
                    PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(entity.EmployeeId, (byte)MonetoryChangeLogTypeEnum.HireDate
                        , "Hire Date", dbEntity.ECurrentStatus[0].FromDate, entity.ECurrentStatus[0].FromDate, LogActionEnum.Update));




                dbEntity.ECurrentStatus.OrderBy(x => x.FromDateEng).First().CurrentStatus = entity.ECurrentStatus.OrderBy(x => x.FromDateEng).First().CurrentStatus;
                dbEntity.ECurrentStatus.OrderBy(x => x.FromDateEng).First().FromDate = entity.ECurrentStatus.OrderBy(x => x.FromDateEng).First().FromDate;
                dbEntity.ECurrentStatus.OrderBy(x => x.FromDateEng).First().FromDateEng = entity.ECurrentStatus.OrderBy(x => x.FromDateEng).First().FromDateEng;
                dbEntity.ECurrentStatus.OrderBy(x => x.FromDateEng).First().ToDate = entity.ECurrentStatus.OrderBy(x => x.FromDateEng).First().ToDate;
                dbEntity.ECurrentStatus.OrderBy(x => x.FromDateEng).First().ToDateEng = entity.ECurrentStatus.OrderBy(x => x.FromDateEng).First().ToDateEng;
                dbEntity.ECurrentStatus.OrderBy(x => x.FromDateEng).First().DefineToDate = entity.ECurrentStatus.OrderBy(x => x.FromDateEng).First().DefineToDate;

                //update for Group/Level/Step

                PIncome income = new PayManager().GetBasicIncome(SessionManager.CurrentCompanyId);
                PEmployeeIncome basicIncome = new PayManager().GetEmployeeIncome(entity.EmployeeId, income.IncomeId);


                // Change service history if only Appointment row exists
                if (!PayrollDataContext.EmployeeServiceHistories.Any(x => x.EmployeeId == entity.EmployeeId && x.EventID != (int)ServiceEventType.Appointment))
                {
                    EmployeeServiceHistory serviceHistory = PayrollDataContext.EmployeeServiceHistories.FirstOrDefault(
                        x => x.EmployeeId == entity.EmployeeId && x.EventID == (int)ServiceEventType.Appointment);
                    if (serviceHistory != null)
                    {
                        serviceHistory.BranchId = entity.BranchId;
                        serviceHistory.DepartmentId = entity.DepartmentId;
                        serviceHistory.SubDepartmentId = entity.SubDepartmentId;
                        serviceHistory.DesignationId = entity.DesignationId;
                        serviceHistory.LevelId = CommonManager.GetLevelIDForDesignation(entity.DesignationId.Value);
                        serviceHistory.StatusId = entity.ECurrentStatus[0].CurrentStatus;

                        serviceHistory.Date = entity.ECurrentStatus[0].FromDate;
                        serviceHistory.DateEng = entity.ECurrentStatus[0].FromDateEng;
                        serviceHistory.ToDate = entity.ECurrentStatus[0].ToDate;
                        serviceHistory.ToDateEng = entity.ECurrentStatus[0].ToDateEng;

                        serviceHistory.LetterNo = entity.EHumanResources[0].AppointmentLetterNo;
                        serviceHistory.LetterDate = entity.EHumanResources[0].AppointmentLetterDate;
                        serviceHistory.LetterDateEng = entity.EHumanResources[0].AppointmentLetterDateEng;

                    }
                }


                if (EmployeeManager.HasLevelGradeForStatus(dbEntity.ECurrentStatus.OrderByDescending(x => x.FromDateEng).First().CurrentStatus, entity.SkipLevelGradeMatrix))
                {
                    bool isLevelEditable = true;
                    int groupID = 0, levelID = 0;
                    double stepGrade = 0;
                    NewPayrollManager.GetEmployeeFirstLevelGrade(entity.EmployeeId, ref isLevelEditable, ref groupID, ref levelID, ref stepGrade);
                    /// IF level grade already changed or Employee used in payroll then Level/Grade not editable from this page       
                    if (isLevelEditable)
                    {
                        //basicIncome.GroupId = entity.PEmployeeIncomes[0].GroupId;
                        basicIncome.LevelId = entity.PEmployeeIncomes[0].LevelId;
                        basicIncome.Step = entity.PEmployeeIncomes[0].Step;
                    }
                    basicIncome.IsValid = true;
                    basicIncome.IsNotSourceSalaryMatrix = false;
                    if (basicIncome.Amount == null)
                        basicIncome.Amount = 0;
                }
                else if (basicIncome != null)
                {
                    basicIncome.LevelId = null;
                    basicIncome.Step = null;
                    //basicIncome.GroupId = null;
                }
                #endregion

                // if increment or salary history exists for then change that date also as Hire date has been changed
                if (isFirstStatusFromDateChanged && prevHireDate != null)
                {
                    List<PEmployeeIncome> incomes =
                        (
                            from ei in PayrollDataContext.PEmployeeIncomes
                            join i in PayrollDataContext.PIncomes on ei.IncomeId equals i.IncomeId
                            where ei.EmployeeId == entity.EmployeeId
                            && i.Calculation == IncomeCalculation.FIXED_AMOUNT
                            select ei
                        ).ToList();
                    foreach (PEmployeeIncome empInc in incomes)
                    {
                        PEmployeeIncrement firstIncrement = PayrollDataContext.PEmployeeIncrements
                            .FirstOrDefault(x => x.EmployeeIncomeId == empInc.EmployeeIncomeId && x.EffectiveFromDateEng == prevHireDate);
                        if (firstIncrement != null)
                        {
                            firstIncrement.EffectiveFromDate = entity.ECurrentStatus.OrderBy(x => x.FromDateEng).First().FromDate;
                            firstIncrement.EffectiveFromDateEng = entity.ECurrentStatus.OrderBy(x => x.FromDateEng).First().FromDateEng;
                        }
                    }
                }

                // if Join date is changed and increment exists for income then change from date also
                //if (CommonManager.CompanySetting.HasLevelGradeSalary == false
                //    && CalculationManager.IsCalculatedSavedForEmployee(dbEntity.EmployeeId) == false
                //    && isFirstStatusFromDateChanged)
                //{
                //    List<PEmployeeIncome> incomes =
                //        (
                //            from ei in PayrollDataContext.PEmployeeIncomes
                //            join i in PayrollDataContext.PIncomes on ei.IncomeId equals i.IncomeId
                //            where ei.EmployeeId == entity.EmployeeId
                //            && i.Calculation == IncomeCalculation.FIXED_AMOUNT
                //            select ei
                //        ).ToList();
                //    foreach (PEmployeeIncome empInc in incomes)
                //    {
                //        PEmployeeIncrement firstIncrement = PayrollDataContext.PEmployeeIncrements
                //            .FirstOrDefault(x => x.EmployeeIncomeId == empInc.EmployeeIncomeId);
                //        if (firstIncrement != null)
                //        {
                //            firstIncrement.EffectiveFromDate = entity.ECurrentStatus.OrderBy(x => x.FromDateEng).First().FromDate;
                //            firstIncrement.EffectiveFromDateEng = entity.ECurrentStatus.OrderBy(x => x.FromDateEng).First().FromDateEng;
                //        }
                //    }
                //}




                //#region "Retirement"

                ////if reg is set
                //bool isRetired, dbIsRetired;

                //isRetired = entity.IsRetired != null ? entity.IsRetired.Value : false;
                //dbIsRetired = dbEntity.IsRetired.Value;

                //dbEntity.IsRetired = entity.IsRetired == null ? false : true;

                ////if ret is set
                //if (dbIsRetired == false && isRetired == true)
                //{
                //    PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(dbEntity.EmployeeId, (byte)MonetoryChangeLogTypeEnum.Information
                //        , "Retirement", "-", entity.EHumanResources[0].DateOfRetirement, LogActionEnum.Add));

                //    PayrollPeriod period = PayrollDataContext.PayrollPeriods.FirstOrDefault(x => dbEntity.EHumanResources[0].DateOfRetirementEng >= x.StartDateEng && dbEntity.EHumanResources[0].DateOfRetirementEng <= x.EndDateEng);
                //    if (period != null)
                //    {
                //        CCalculationEmployee calcEmployee
                //            =
                //            (
                //                from ce in PayrollDataContext.CCalculationEmployees
                //                join c in PayrollDataContext.CCalculations on ce.CalculationId equals c.CalculationId
                //                where c.PayrollPeriodId == period.PayrollPeriodId
                //                && ce.EmployeeId == dbEntity.EmployeeId
                //                select ce
                //            ).FirstOrDefault();

                //        if (calcEmployee != null)
                //            calcEmployee.IsRetiredOrResigned = true;

                //    }

                //}
                ////if ret set is changed
                //else if (dbIsRetired == true && isRetired == true && dbEntity.EHumanResources[0].DateOfResignation != entity.EHumanResources[0].DateOfResignation)
                //{
                //    if (dbEntity.EHumanResources[0].DateOfResignation != entity.EHumanResources[0].DateOfResignation)
                //        PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(dbEntity.EmployeeId, (byte)MonetoryChangeLogTypeEnum.Information
                //            , "Retirement", dbEntity.EHumanResources[0].DateOfRetirement, entity.EHumanResources[0].DateOfRetirement, LogActionEnum.Update));
                //}
                ////if ret date is removed
                //else if (dbIsRetired == true && isRetired == false)
                //{
                //    PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(dbEntity.EmployeeId, (byte)MonetoryChangeLogTypeEnum.Information
                //        , "Retirement", dbEntity.EHumanResources[0].DateOfRetirement, "-", LogActionEnum.Delete));


                //}



                //if (dbEntity.IsRetired.Value)
                //{
                //    dbEntity.IsRetired = entity.IsRetired;
                //    dbEntity.EHumanResources[0].DateOfRetirementEng = GetEngDate(entity.EHumanResources[0].DateOfRetirement, null);
                //    dbEntity.EHumanResources[0].DateOfRetirement = entity.EHumanResources[0].DateOfRetirement;
                //    dbEntity.EHumanResources[0].RetirementType = entity.EHumanResources[0].RetirementType;
                //}

                //#endregion


            }



            PayrollDataContext.SubmitChanges();

            // for NIBL set EIN to Ino
            if (isInsert && CommonManager.CompanySetting.WhichCompany == WhichCompany.NIBL)
            {
                entity.EHumanResources[0].IdCardNo = entity.EmployeeId.ToString();
                if (PayrollDataContext.AttendanceMappings.Any(x => x.EmployeeId == entity.EmployeeId) == false)
                {
                    PayrollDataContext.AttendanceMappings.InsertOnSubmit(
                        new AttendanceMapping { EmployeeId = entity.EmployeeId, DeviceId = entity.EmployeeId.ToString() });
                }
                PayrollDataContext.SubmitChanges();
            }

            status.IsSuccess = true;

            return status;
        }

        public static BLevel GetEmployeeNextLevelForActing(int employeeId)
        {
            BLevel level = GetEmployeeCurrentLevel(employeeId);
            if (level == null)
            {
                return null;
            }

            level = PayrollDataContext.BLevels
                .OrderByDescending(x => x.Order)
                .FirstOrDefault(x => //x.LevelGroupId == level.LevelGroupId && 
                    x.Order < level.Order && x.LevelId != level.LevelId);

            return level;
        }

        public static BLevel GetEmployeeCurrentLevel(int employeeId)
        {

            DateTime fromDate = GetCurrentDateAndTime();
            return GetEmployeeLevelUsingDate(employeeId, fromDate);
        }

        public static BLevel GetEmployeeLevelForNoLevelGrade(int employeeId)
        {
            EEmployee emp = GetEmployeeByID(employeeId);
            if (emp.DesignationId != null)
                return new CommonManager().GetDesignationById(emp.DesignationId.Value).BLevel;

            return null;
        }

        public static BLevel GetEmployeeLastestLevel(int employeeId)
        {
            PIncome basicIncome = new PayManager().GetBasicIncome(SessionManager.CurrentCompanyId);
            PEmployeeIncome empIncome = new PayManager().GetEmployeeIncome(employeeId, basicIncome.IncomeId);

            if (empIncome.LevelId == null)
                return null;

            return NewPayrollManager.GetLevelById(empIncome.LevelId.Value);
        }
        /// <summary>
        /// Set Joining Level/Grade of the Employee
        /// </summary>
        public static void SetEmployeeJoiningLevelGrade(ref int? levelId, ref double grade, int employeeId)
        {
            PIncome basicIncome = new PayManager().GetBasicIncome(SessionManager.CurrentCompanyId);
            PEmployeeIncome empIncome = new PayManager().GetEmployeeIncome(employeeId, basicIncome.IncomeId);

            if (empIncome == null)
                return;

            PEmployeeIncrement increment = PayrollDataContext.PEmployeeIncrements.FirstOrDefault(x => x.EmployeeIncomeId == empIncome.EmployeeId);
            if (increment != null)
            {
                levelId = increment.OldLevelId;
                if (increment.OldStepGrade != null)
                    grade = increment.OldStepGrade.Value;
            }

            levelId = empIncome.LevelId;
            if (empIncome.Step != null)
                grade = empIncome.Step.Value;

        }
        public static BLevel GetEmployeeLevelUsingDate(int employeeId, DateTime fromDate)
        {
            PIncome basicIncome = new PayManager().GetBasicIncome(SessionManager.CurrentCompanyId);
            PEmployeeIncome empIncome = new PayManager().GetEmployeeIncome(employeeId, basicIncome.IncomeId);

            if (empIncome == null)
                return null;


            PEmployeeIncrement history = PayrollDataContext.PEmployeeIncrements
                .Where(x => x.EmployeeIncomeId == empIncome.EmployeeIncomeId
                    && x.EffectiveFromDateEng <= fromDate)
                .OrderByDescending(x => x.EffectiveFromDateEng).ThenByDescending(x => x.Id).FirstOrDefault();

            if (history != null && history.NewLevelId != null)
                return NewPayrollManager.GetLevelById(history.NewLevelId.Value);

            // order doesn't reqd for OldLevelId as it will always be same
            history = PayrollDataContext.PEmployeeIncrements.Where(x => x.EmployeeIncomeId == empIncome.EmployeeIncomeId)
                .OrderBy(x => x.EffectiveFromDateEng).FirstOrDefault();

            if (history != null && history.OldLevelId != null)
                return NewPayrollManager.GetLevelById(history.OldLevelId.Value);

            // for Level not assinged emp
            if (empIncome.LevelId == null)
                return null;

            return NewPayrollManager.GetLevelById(empIncome.LevelId.Value);

        }
        public static double? GetEmployeeCurrentGradeStep(int employeeId)
        {

            DateTime fromDate = GetCurrentDateAndTime();
            return GetEmployeeGradeStepUsingDate(employeeId, fromDate);
        }
        public static double? GetEmployeeGradeStepUsingDate(int employeeId, DateTime fromDate)
        {
            PIncome basicIncome = new PayManager().GetBasicIncome(SessionManager.CurrentCompanyId);
            PEmployeeIncome empIncome = new PayManager().GetEmployeeIncome(employeeId, basicIncome.IncomeId);

            if (empIncome == null)
                return null;


            PEmployeeIncrement history = PayrollDataContext.PEmployeeIncrements
                .Where(x => x.EmployeeIncomeId == empIncome.EmployeeIncomeId
                    && x.EffectiveFromDateEng <= fromDate)
                .OrderByDescending(x => x.EffectiveFromDateEng).ThenByDescending(x => x.Id).FirstOrDefault();

            if (history != null)
                return history.NewStepGrade == null ? 0 : history.NewStepGrade;

            history = PayrollDataContext.PEmployeeIncrements.Where(x => x.EmployeeIncomeId == empIncome.EmployeeIncomeId)
                .OrderBy(x => x.EffectiveFromDateEng).FirstOrDefault();

            if (history != null)
                return history.OldStepGrade == null ? 0 : history.OldStepGrade;


            return empIncome.Step;
        }
        public static EDesignation GetEmployeeCurrentPostDesitionPosition(int employeeId)
        {
            DateTime fromDate = GetCurrentDateAndTime();
            DesignationHistory history = PayrollDataContext.DesignationHistories
                .Where(x => x.EmployeeId == employeeId && x.FromDateEng <= fromDate)
                .OrderByDescending(x => x.FromDateEng).FirstOrDefault();

            if (history == null)
                history = PayrollDataContext.DesignationHistories
                    .FirstOrDefault(x => x.EmployeeId == employeeId);

            if (history == null)
            {
                EEmployee emp = EmployeeManager.GetEmployeeById(employeeId);
                if (emp.DesignationId != null)
                {
                    return new CommonManager().GetDesignationById(emp.DesignationId.Value);
                }
                else
                    return null;
            }
            return new CommonManager().GetDesignationById(history.DesignationId.Value);
        }
        public static Status InsertUpdateAddress(EAddress address, int EmpID)
        {
            Status status = new Status();

            EEmployee updateInstance = new EEmployee();
            updateInstance = PayrollDataContext.EEmployees.Where(x => x.EmployeeId == EmpID).FirstOrDefault();

            bool addressExists = PayrollDataContext.EAddresses.Any(x => x.EmployeeId == EmpID);


            updateInstance.Modified = CustomDate.GetTodayDate(true).EnglishDate;


            if (addressExists == false)
            {
                address.ModifiedBy = SessionManager.CurrentLoggedInUserID;
                address.ModifiedOn = BLL.BaseBiz.GetCurrentDateAndTime();
                if (SessionManager.CurrentLoggedInEmployeeId == 0)
                {
                    address.ApprovedBy = SessionManager.CurrentLoggedInUserID;
                    address.ApprovedOn = BLL.BaseBiz.GetCurrentDateAndTime();
                    address.Status = (int)HRStatusEnum.Approved;
                }
                else
                {
                    address.Status = (int)HRStatusEnum.Saved;
                }

                address.CreatedBy = SessionManager.CurrentLoggedInUserID;
                address.CreatedOn = GetCurrentDateAndTime();

                updateInstance.EAddresses.Add(address);
            }
            else
            {
                if (SessionManager.CurrentLoggedInEmployeeId == 0)
                {
                    updateInstance.EAddresses.First().ApprovedBy = SessionManager.CurrentLoggedInUserID;
                    updateInstance.EAddresses.First().ApprovedOn = BLL.BaseBiz.GetCurrentDateAndTime();
                    updateInstance.EAddresses.First().Status = (int)HRStatusEnum.Approved;
                }
                else
                {
                    updateInstance.EAddresses.First().Status = (int)HRStatusEnum.Saved;
                }

                updateInstance.EAddresses.First().ModifiedBy = SessionManager.CurrentLoggedInUserID;
                updateInstance.EAddresses.First().ModifiedOn = GetCurrentDateAndTime();

                updateInstance.EAddresses.First().PECountryId = address.PECountryId;

                updateInstance.EAddresses.First().PEZoneId = address.PEZoneId;

                updateInstance.EAddresses.First().PEDistrictId = address.PEDistrictId;

                updateInstance.EAddresses.First().PEVDCMuncipality = address.PEVDCMuncipality;
                updateInstance.EAddresses.First().PEStreet = address.PEStreet;
                updateInstance.EAddresses.First().PEWardNo = address.PEWardNo;
                updateInstance.EAddresses.First().PEHouseNo = address.PEHouseNo;
                updateInstance.EAddresses.First().PEState = address.PEState;
                updateInstance.EAddresses.First().PEZipCode = address.PEZipCode;
                updateInstance.EAddresses.First().PELocality = address.PELocality;


                updateInstance.EAddresses.First().PSCountryId = address.PSCountryId;

                updateInstance.EAddresses.First().PSZoneId = address.PSZoneId;

                updateInstance.EAddresses.First().PSDistrictId = address.PSDistrictId;

                updateInstance.EAddresses.First().PSVDCMuncipality = address.PSVDCMuncipality;
                updateInstance.EAddresses.First().PSStreet = address.PSStreet;
                updateInstance.EAddresses.First().PSWardNo = address.PSWardNo;
                updateInstance.EAddresses.First().PSHouseNo = address.PSHouseNo;
                updateInstance.EAddresses.First().PSState = address.PSState;
                updateInstance.EAddresses.First().PSZipCode = address.PSZipCode;
                updateInstance.EAddresses.First().PSLocality = address.PSLocality;
                updateInstance.EAddresses.First().PSCitIssDis = address.PSCitIssDis;


                // Prevent accidental update from Emplooyee
                if (SessionManager.CurrentLoggedInEmployeeId == 0)
                {
                    updateInstance.EAddresses.First().CIEmail = address.CIEmail;
                }

                updateInstance.EAddresses.First().CIPhoneNo = address.CIPhoneNo;
                updateInstance.EAddresses.First().Extension = address.Extension;
                updateInstance.EAddresses.First().CIMobileNo = address.CIMobileNo;
                updateInstance.EAddresses.First().PersonalEmail = address.PersonalEmail;
                updateInstance.EAddresses.First().PersonalMobile = address.PersonalMobile;
                updateInstance.EAddresses.First().PersonalPhone = address.PersonalPhone;
                updateInstance.EAddresses.First().EmergencyRelation = address.EmergencyRelation;
                updateInstance.EAddresses.First().EmergencyName = address.EmergencyName;
                updateInstance.EAddresses.First().EmergencyPhone = address.EmergencyPhone;
                updateInstance.EAddresses.First().EmergencyMobile = address.EmergencyMobile;
            }

            PayrollDataContext.SubmitChanges();
            status.IsSuccess = true;
            // DeleteFromCache("GetEmployeeAddressReportResult");
            return status;
        }
        public Status InsertUpdateTraining(HTraining _HTraining, bool IsInsert)
        {
            Status myStatus = new Status();
            //Insert Operation
            if (IsInsert)
            {
                _HTraining.Modifiedby = SessionManager.CurrentLoggedInUserID;
                _HTraining.ModifiedOn = BLL.BaseBiz.GetCurrentDateAndTime();
                if (SessionManager.CurrentLoggedInEmployeeId == 0)
                {
                    _HTraining.ApprovedBy = SessionManager.CurrentLoggedInUserID;
                    _HTraining.ApprovedOn = BLL.BaseBiz.GetCurrentDateAndTime();
                    _HTraining.Status = (int)HRStatusEnum.Approved;
                }
                else
                {
                    _HTraining.Status = (int)HRStatusEnum.Saved;
                }

                PayrollDataContext.HTrainings.InsertOnSubmit(_HTraining);
                PayrollDataContext.SubmitChanges();
                return myStatus;
            }

            HTraining dbRecord = PayrollDataContext.HTrainings.SingleOrDefault(c => c.TrainingId == _HTraining.TrainingId);

            if (dbRecord == null)
            {
                myStatus.ErrorMessage = "error while saving.";//GetConcurrencyErrorMsg();//need to implement this on later.
                myStatus.IsSuccess = false;
                return myStatus;
            }
            else
            {
                _HTraining.Modifiedby = SessionManager.CurrentLoggedInUserID;
                _HTraining.ModifiedOn = BLL.BaseBiz.GetCurrentDateAndTime();
                if (SessionManager.CurrentLoggedInEmployeeId == 0)
                {
                    _HTraining.ApprovedBy = SessionManager.CurrentLoggedInUserID;
                    _HTraining.ApprovedOn = BLL.BaseBiz.GetCurrentDateAndTime();
                    _HTraining.Status = (int)HRStatusEnum.Approved;
                }
                else
                {
                    _HTraining.Status = (int)HRStatusEnum.Saved;
                }

                CopyObject<HTraining>(_HTraining, ref dbRecord, "TrainingID", "EmployeeID", "TrainingRef_ID");
                PayrollDataContext.SubmitChanges();
                return myStatus;
            }

        }

        public Status DeleteTrainingFile(int TrainingID)
        {
            Status myStatus = new Status();
            HTraining dbRecord = PayrollDataContext.HTrainings.SingleOrDefault(c => c.TrainingId == TrainingID);

            if (dbRecord == null)
            {
                myStatus.ErrorMessage = "error while saving.";//GetConcurrencyErrorMsg();//need to implement this on later.
                myStatus.IsSuccess = false;
                return myStatus;
            }
            else
            {
                dbRecord.FileFormat = "";
                dbRecord.FileLocation = "";
                dbRecord.ServerFileName = "";
                dbRecord.Size = "";
                dbRecord.UserFileName = "";
                dbRecord.FileType = "";
                PayrollDataContext.SubmitChanges();
                return myStatus;
            }
        }
        #endregion
        #region Seminar
        public static List<HSeminar> GetSeminarByEmployeeID(int EmployeeId)
        {
            List<HSeminar> list = PayrollDataContext.HSeminars.Where(x => x.EmployeeId == EmployeeId).ToList();

            foreach (var item in list)
            {
                item.IsEditable = NewHRManager.IsHrEditable(item.Status);
            }

            return list;
        }

        public static HSeminar GetSeminarDetailsById(int SeminarID)
        {
            return PayrollDataContext.HSeminars.SingleOrDefault(
                e => e.SeminarId == SeminarID);
        }


        public static EEmployee GetEmployeeByID(int p)
        {
            EEmployee retEmp = new EEmployee();
            if (PayrollDataContext.EEmployees.Any(x => x.EmployeeId == p))
                retEmp = PayrollDataContext.EEmployees.Where(x => x.EmployeeId == p).First();
            return retEmp;
        }

        public static EAddress GetAddressByID(int p)
        {
            EAddress retAdd = new EAddress();
            if (PayrollDataContext.EAddresses.Any(x => x.EmployeeId == p))
                retAdd = PayrollDataContext.EAddresses.Where(x => x.EmployeeId == p).First();
            return retAdd;
        }
        public static bool DeleteSeminarByID(int SeminarID)
        {
            HSeminar _HData = PayrollDataContext.HSeminars.SingleOrDefault(t => t.SeminarId == SeminarID);
            PayrollDataContext.HSeminars.DeleteOnSubmit(_HData);
            PayrollDataContext.SubmitChanges();
            return true;
        }


        public Status InsertUpdateSeminar(HSeminar _HSeminar, bool IsInsert)
        {
            Status myStatus = new Status();
            //Insert Operation
            if (IsInsert)
            {
                _HSeminar.Modifiedby = SessionManager.CurrentLoggedInUserID;
                _HSeminar.ModifiedOn = BLL.BaseBiz.GetCurrentDateAndTime();
                if (SessionManager.CurrentLoggedInEmployeeId == 0)
                {
                    _HSeminar.ApprovedBy = SessionManager.CurrentLoggedInUserID;
                    _HSeminar.ApprovedOn = BLL.BaseBiz.GetCurrentDateAndTime();
                    _HSeminar.Status = (int)HRStatusEnum.Approved;
                }
                else
                {
                    _HSeminar.Status = (int)HRStatusEnum.Saved;
                }

                PayrollDataContext.HSeminars.InsertOnSubmit(_HSeminar);
                PayrollDataContext.SubmitChanges();
                return myStatus;
            }

            HSeminar dbRecord = PayrollDataContext.HSeminars.SingleOrDefault(c => c.SeminarId == _HSeminar.SeminarId);

            if (dbRecord == null)
            {
                myStatus.ErrorMessage = "error while saving.";//GetConcurrencyErrorMsg();//need to implement this on later.
                myStatus.IsSuccess = false;
                return myStatus;
            }
            else
            {
                _HSeminar.Modifiedby = SessionManager.CurrentLoggedInUserID;
                _HSeminar.ModifiedOn = BLL.BaseBiz.GetCurrentDateAndTime();
                if (SessionManager.CurrentLoggedInEmployeeId == 0)
                {
                    _HSeminar.ApprovedBy = SessionManager.CurrentLoggedInUserID;
                    _HSeminar.ApprovedOn = BLL.BaseBiz.GetCurrentDateAndTime();
                    _HSeminar.Status = (int)HRStatusEnum.Approved;
                }
                else
                {
                    _HSeminar.Status = (int)HRStatusEnum.Saved;
                }

                CopyObject<HSeminar>(_HSeminar, ref dbRecord, "SeminarID", "EmployeeID");
                PayrollDataContext.SubmitChanges();
                return myStatus;
            }

        }

        #endregion
        #region SkillSets
        public static List<SkillSetEmployee> GetSkillSetsByEmployeeID(int EmployeeId)
        {
            List<SkillSetEmployee> list = PayrollDataContext.SkillSetEmployees.Where(x => x.EmployeeId == EmployeeId).ToList();
            return list;
        }

        public static SkillSetEmployee GetSkillSetsDetailsById(int SkillSetsID, int EmployeeID)
        {
            return PayrollDataContext.SkillSetEmployees.SingleOrDefault(
                e => e.SkillSetId == SkillSetsID && e.EmployeeId == EmployeeID);
        }

        public static bool DeleteSkillSetsByID(int SkillSetsID, int EmployeeID)
        {
            SkillSetEmployee _HData = PayrollDataContext.SkillSetEmployees.SingleOrDefault(t => t.SkillSetId == SkillSetsID && t.EmployeeId == EmployeeID);
            PayrollDataContext.SkillSetEmployees.DeleteOnSubmit(_HData);
            PayrollDataContext.SubmitChanges();
            return true;
        }

        public static bool IsDuplicateSkillByEmpID(int SkillSetsID, int EmployeeID)
        {
            SkillSetEmployee _HData = PayrollDataContext.SkillSetEmployees.SingleOrDefault(t => t.SkillSetId == SkillSetsID && t.EmployeeId == EmployeeID);
            if (_HData != null)
                return true;
            else
                return false;

        }

        public static bool IsDuplicateSkillInPool(string SkillName)
        {
            SkillSet _HData = PayrollDataContext.SkillSets.SingleOrDefault(t => t.Name == SkillName);
            if (_HData != null)
                return true;
            else
                return false;

        }


        public static List<NewHREmployeeSkillSetBO> GetEmployeesSkillSet(int employeeId)
        {
            List<NewHREmployeeSkillSetBO> list = new List<NewHREmployeeSkillSetBO>();

            if (SessionManager.CurrentLoggedInEmployeeId == 0 || SessionManager.CurrentLoggedInEmployeeId != employeeId)
            {
                list =
                   (
                   from se in PayrollDataContext.SkillSetEmployees
                   join s in PayrollDataContext.SkillSets on se.SkillSetId equals s.SkillSetId

                   join l in PayrollDataContext.SkillSetCompetencyLevels on se.LevelID equals l.LevelID into dd
                   from ddd in dd.DefaultIfEmpty()

                   orderby s.Name
                   where se.EmployeeId == employeeId && se.Status == (int)HRStatusEnum.Approved
                   select new NewHREmployeeSkillSetBO
                   {
                       EmployeeId = employeeId,
                       SkillSetId = s.SkillSetId,
                       SkillSetName = s.Name,
                       LevelOfExpertise = ddd.Name,
                       IsEditable = NewHRManager.IsHrEditable(se.Status)

                   }
                   ).ToList();
            }
            else
            {
                list =
                   (
                   from se in PayrollDataContext.SkillSetEmployees
                   join s in PayrollDataContext.SkillSets on se.SkillSetId equals s.SkillSetId

                   join l in PayrollDataContext.SkillSetCompetencyLevels on se.LevelID equals l.LevelID into dd
                   from ddd in dd.DefaultIfEmpty()

                   orderby s.Name
                   where se.EmployeeId == employeeId
                   select new NewHREmployeeSkillSetBO
                   {
                       EmployeeId = employeeId,
                       SkillSetId = s.SkillSetId,
                       SkillSetName = s.Name,
                       LevelOfExpertise = ddd.Name,
                       IsEditable = NewHRManager.IsHrEditable(se.Status)

                   }
                   ).ToList();
            }


            return list;
        }



        public Status InsertUpdateSkillSets(SkillSetEmployee _SkillSetEmployee, bool IsInsert, bool SaveToPool, string NewSkillPool, int EmployeeID)
        {
            Status myStatus = new Status();

            //Insert Operation
            if (IsInsert)
            {
                _SkillSetEmployee.Modifiedby = SessionManager.CurrentLoggedInUserID;
                _SkillSetEmployee.ModifiedOn = BLL.BaseBiz.GetCurrentDateAndTime();
                if (SessionManager.CurrentLoggedInEmployeeId == 0)
                {
                    _SkillSetEmployee.ApprovedBy = SessionManager.CurrentLoggedInUserID;
                    _SkillSetEmployee.ApprovedOn = BLL.BaseBiz.GetCurrentDateAndTime();
                    _SkillSetEmployee.Status = (int)HRStatusEnum.Approved;
                }
                else
                {
                    _SkillSetEmployee.Status = (int)HRStatusEnum.Saved;
                }

                if (SaveToPool)//first save skill to pool and getID
                {
                    SkillSet _SkillSet = new SkillSet();
                    _SkillSet.Name = NewSkillPool;
                    PayrollDataContext.SkillSets.InsertOnSubmit(_SkillSet);
                    PayrollDataContext.SubmitChanges();

                    SkillSet _SkillSetRecord = PayrollDataContext.SkillSets.SingleOrDefault(c => c.Name == NewSkillPool);
                    if (_SkillSetRecord != null)
                    {
                        _SkillSetEmployee.SkillSetId = _SkillSetRecord.SkillSetId;
                    }

                    PayrollDataContext.SkillSetEmployees.InsertOnSubmit(_SkillSetEmployee);
                }
                else
                {
                    PayrollDataContext.SkillSetEmployees.InsertOnSubmit(_SkillSetEmployee);
                }

                PayrollDataContext.SubmitChanges();
                return myStatus;
            }

            SkillSetEmployee dbRecord = PayrollDataContext.SkillSetEmployees.SingleOrDefault(c => c.SkillSetId == _SkillSetEmployee.SkillSetId && c.EmployeeId == EmployeeID);

            if (dbRecord == null)
            {
                myStatus.ErrorMessage = "error while saving.";//GetConcurrencyErrorMsg();//need to implement this on later.
                myStatus.IsSuccess = false;
                return myStatus;
            }
            else
            {
                _SkillSetEmployee.Modifiedby = SessionManager.CurrentLoggedInUserID;
                _SkillSetEmployee.ModifiedOn = BLL.BaseBiz.GetCurrentDateAndTime();
                if (SessionManager.CurrentLoggedInEmployeeId == 0)
                {
                    _SkillSetEmployee.ApprovedBy = SessionManager.CurrentLoggedInUserID;
                    _SkillSetEmployee.ApprovedOn = BLL.BaseBiz.GetCurrentDateAndTime();
                    _SkillSetEmployee.Status = (int)HRStatusEnum.Approved;
                }
                else
                {
                    _SkillSetEmployee.Status = (int)HRStatusEnum.Saved;
                }

                CopyObject<SkillSetEmployee>(_SkillSetEmployee, ref dbRecord, "SkillSetsID", "EmployeeID");
                PayrollDataContext.SubmitChanges();
                return myStatus;
            }

        }

        #endregion
        #region Publication
        public static List<HPublication> GetPublicationByEmployeeID(int EmployeeId)
        {
            List<HPublication> list = PayrollDataContext.HPublications.Where(x => x.EmployeeId == EmployeeId).ToList();

            foreach (var item in list)
            {
                item.IsEditable = NewHRManager.IsHrEditable(item.Status);
            }

            return list;
        }

        public static HPublication GetPublicationDetailsById(int PublicationID)
        {
            return PayrollDataContext.HPublications.SingleOrDefault(
                e => e.PublicationId == PublicationID);
        }

        public static bool DeletePublicationByID(int PublicationID)
        {
            HPublication _HData = PayrollDataContext.HPublications.SingleOrDefault(t => t.PublicationId == PublicationID);
            PayrollDataContext.HPublications.DeleteOnSubmit(_HData);
            PayrollDataContext.SubmitChanges();
            return true;
        }


        public Status InsertUpdatePublication(HPublication _HPublication, bool IsInsert)
        {
            Status myStatus = new Status();
            //Insert Operation
            if (IsInsert)
            {
                _HPublication.Modifiedby = SessionManager.CurrentLoggedInUserID;
                _HPublication.ModifiedOn = BLL.BaseBiz.GetCurrentDateAndTime();
                if (SessionManager.CurrentLoggedInEmployeeId == 0)
                {
                    _HPublication.ApprovedBy = SessionManager.CurrentLoggedInUserID;
                    _HPublication.ApprovedOn = BLL.BaseBiz.GetCurrentDateAndTime();
                    _HPublication.Status = (int)HRStatusEnum.Approved;
                }
                else
                {
                    _HPublication.Status = (int)HRStatusEnum.Saved;
                }

                PayrollDataContext.HPublications.InsertOnSubmit(_HPublication);
                PayrollDataContext.SubmitChanges();
                return myStatus;
            }

            HPublication dbRecord = PayrollDataContext.HPublications.SingleOrDefault(c => c.PublicationId == _HPublication.PublicationId);

            if (dbRecord == null)
            {
                myStatus.ErrorMessage = "error while saving.";//GetConcurrencyErrorMsg();//need to implement this on later.
                myStatus.IsSuccess = false;
                return myStatus;
            }
            else
            {
                _HPublication.Modifiedby = SessionManager.CurrentLoggedInUserID;
                _HPublication.ModifiedOn = BLL.BaseBiz.GetCurrentDateAndTime();
                if (SessionManager.CurrentLoggedInEmployeeId == 0)
                {
                    _HPublication.ApprovedBy = SessionManager.CurrentLoggedInUserID;
                    _HPublication.ApprovedOn = BLL.BaseBiz.GetCurrentDateAndTime();
                    _HPublication.Status = (int)HRStatusEnum.Approved;
                }
                else
                {
                    _HPublication.Status = (int)HRStatusEnum.Saved;
                }

                CopyObject<HPublication>(_HPublication, ref dbRecord, "PublicationID", "EmployeeID");
                PayrollDataContext.SubmitChanges();
                return myStatus;
            }

        }

        #endregion
        #region LanguageSets
        public static List<LanguageSetEmployee> GetLanguageSetsByEmployeeID(int EmployeeId)
        {
            List<LanguageSetEmployee> list = PayrollDataContext.LanguageSetEmployees.Where(x => x.EmployeeId == EmployeeId).ToList();
            return list;
        }

        public List<LanguageSet> GetAllLanguageSet()
        {
            List<LanguageSet> list = null;
            list = PayrollDataContext.LanguageSets
                .OrderBy(e => e.Name).ToList();
            return list;
        }


        public static LanguageSetEmployee GetLanguageSetsDetailsById(int LanguageSetsID, int EmployeeID)
        {
            return PayrollDataContext.LanguageSetEmployees.SingleOrDefault(
                e => e.LanguageSetId == LanguageSetsID && e.EmployeeId == EmployeeID);
        }

        public static bool DeleteLanguageSetsByID(int LanguageSetsID, int EmployeeID)
        {
            LanguageSetEmployee _HData = PayrollDataContext.LanguageSetEmployees.SingleOrDefault(t => t.LanguageSetId == LanguageSetsID && t.EmployeeId == EmployeeID);
            PayrollDataContext.LanguageSetEmployees.DeleteOnSubmit(_HData);
            PayrollDataContext.SubmitChanges();
            return true;
        }

        public static bool IsDuplicateLanguageByEmpID(int LanguageSetsID, int EmployeeID)
        {
            LanguageSetEmployee _HData = PayrollDataContext.LanguageSetEmployees.SingleOrDefault(t => t.LanguageSetId == LanguageSetsID && t.EmployeeId == EmployeeID);
            if (_HData != null)
                return true;
            else
                return false;

        }

        public static bool IsDuplicateLanguageInPool(string LanguageName)
        {
            LanguageSet _HData = PayrollDataContext.LanguageSets.SingleOrDefault(t => t.Name == LanguageName);
            if (_HData != null)
                return true;
            else
                return false;

        }

        //public static List<LanguageSetEmployee> GetEmployeesLanguageSet(int EmployeeId)
        //{
        //    List<LanguageSetEmployee> list = PayrollDataContext.LanguageSetEmployees.Where(x => x.EmployeeId == EmployeeId).ToList();
        //    return list;
        //}




        public static List<NewHREmployeeLanguageSetBO> GetEmployeesLanguageSet(int employeeId)
        {
            List<NewHREmployeeLanguageSetBO> list = new List<NewHREmployeeLanguageSetBO>();

            if (SessionManager.CurrentLoggedInEmployeeId == 0 || SessionManager.CurrentLoggedInEmployeeId != employeeId)
            {
                list =
                    (
                    from se in PayrollDataContext.LanguageSetEmployees
                    join s in PayrollDataContext.LanguageSets on se.LanguageSetId equals s.LanguageSetId

                    //join l in PayrollDataContext.LanguageSetCompetencyLevels on se.LevelID equals l.LevelID into dd
                    //from ddd in dd.DefaultIfEmpty()

                    orderby s.Name
                    where se.EmployeeId == employeeId && se.Status == (int)HRStatusEnum.Approved
                    select new NewHREmployeeLanguageSetBO
                    {
                        EmployeeId = employeeId,
                        LanguageSetId = s.LanguageSetId,
                        LanguageSetName = s.Name,
                        FluencyWriteName = se.FluencyWriteName,
                        FluencySpeakName = se.FluencySpeakName,
                        IsEditable = NewHRManager.IsHrEditable(se.Status)

                    }
                    ).ToList();

            }
            else
            {
                list =
                    (
                    from se in PayrollDataContext.LanguageSetEmployees
                    join s in PayrollDataContext.LanguageSets on se.LanguageSetId equals s.LanguageSetId

                    //join l in PayrollDataContext.LanguageSetCompetencyLevels on se.LevelID equals l.LevelID into dd
                    //from ddd in dd.DefaultIfEmpty()

                    orderby s.Name
                    where se.EmployeeId == employeeId
                    select new NewHREmployeeLanguageSetBO
                    {
                        EmployeeId = employeeId,
                        LanguageSetId = s.LanguageSetId,
                        LanguageSetName = s.Name,
                        FluencyWriteName = se.FluencyWriteName,
                        FluencySpeakName = se.FluencySpeakName,
                        IsEditable = NewHRManager.IsHrEditable(se.Status)

                    }
                    ).ToList();
            }
            return list;
        }



        public Status InsertUpdateLanguageSets(LanguageSetEmployee _LanguageSetEmployee, bool IsInsert, bool SaveToPool, string NewLanguagePool, int EmployeeID)
        {
            Status myStatus = new Status();

            //Insert Operation
            if (IsInsert)
            {
                _LanguageSetEmployee.Modifiedby = SessionManager.CurrentLoggedInUserID;
                _LanguageSetEmployee.ModifiedOn = BLL.BaseBiz.GetCurrentDateAndTime();
                if (SessionManager.CurrentLoggedInEmployeeId == 0)
                {
                    _LanguageSetEmployee.ApprovedBy = SessionManager.CurrentLoggedInUserID;
                    _LanguageSetEmployee.ApprovedOn = BLL.BaseBiz.GetCurrentDateAndTime();
                    _LanguageSetEmployee.Status = (int)HRStatusEnum.Approved;
                }
                else
                {
                    _LanguageSetEmployee.Status = (int)HRStatusEnum.Saved;
                }

                if (SaveToPool)//first save Language to pool and getID
                {
                    LanguageSet _LanguageSet = new LanguageSet();
                    _LanguageSet.Name = NewLanguagePool;
                    PayrollDataContext.LanguageSets.InsertOnSubmit(_LanguageSet);
                    PayrollDataContext.SubmitChanges();

                    LanguageSet _LanguageSetRecord = PayrollDataContext.LanguageSets.SingleOrDefault(c => c.Name == NewLanguagePool);
                    if (_LanguageSetRecord != null)
                    {
                        _LanguageSetEmployee.LanguageSetId = _LanguageSetRecord.LanguageSetId;
                    }

                    PayrollDataContext.LanguageSetEmployees.InsertOnSubmit(_LanguageSetEmployee);
                }
                else
                {
                    PayrollDataContext.LanguageSetEmployees.InsertOnSubmit(_LanguageSetEmployee);
                }

                PayrollDataContext.SubmitChanges();
                return myStatus;
            }

            LanguageSetEmployee dbRecord = PayrollDataContext.LanguageSetEmployees.SingleOrDefault(c => c.LanguageSetId == _LanguageSetEmployee.LanguageSetId && c.EmployeeId == EmployeeID);

            if (dbRecord == null)
            {
                myStatus.ErrorMessage = "error while saving.";//GetConcurrencyErrorMsg();//need to implement this on later.
                myStatus.IsSuccess = false;
                return myStatus;
            }
            else
            {
                _LanguageSetEmployee.Modifiedby = SessionManager.CurrentLoggedInUserID;
                _LanguageSetEmployee.ModifiedOn = BLL.BaseBiz.GetCurrentDateAndTime();
                if (SessionManager.CurrentLoggedInEmployeeId == 0)
                {
                    _LanguageSetEmployee.ApprovedBy = SessionManager.CurrentLoggedInUserID;
                    _LanguageSetEmployee.ApprovedOn = BLL.BaseBiz.GetCurrentDateAndTime();
                    _LanguageSetEmployee.Status = (int)HRStatusEnum.Approved;
                }
                else
                {
                    _LanguageSetEmployee.Status = (int)HRStatusEnum.Saved;
                }

                CopyObject<LanguageSetEmployee>(_LanguageSetEmployee, ref dbRecord, "LanguageSetsID", "EmployeeID");
                PayrollDataContext.SubmitChanges();
                return myStatus;
            }

        }

        #endregion
        #region Health
        public static List<HHealth> GetHealthByEmployeeID(int EmployeeId)
        {
            List<HHealth> list = PayrollDataContext.HHealths.Where(x => x.EmployeeId == EmployeeId).ToList();
            return list;
        }

        public static HHealth GetHealthDetailsById(int HealthID)
        {
            return PayrollDataContext.HHealths.SingleOrDefault(
                e => e.HealthId == HealthID);
        }

        public static bool DeleteHealthByID(int HealthID)
        {
            HHealth _HData = PayrollDataContext.HHealths.SingleOrDefault(t => t.HealthId == HealthID);
            PayrollDataContext.HHealths.DeleteOnSubmit(_HData);
            PayrollDataContext.SubmitChanges();
            return true;
        }


        public Status UpdateHandicappedDetail(EEmployee _EEmployee)
        {
            Status myStatus = new Status();

            EEmployee dbRecord = PayrollDataContext.EEmployees.SingleOrDefault(c => c.EmployeeId == _EEmployee.EmployeeId);
            if (dbRecord == null)
            {
                myStatus.ErrorMessage = "error while saving.";//GetConcurrencyErrorMsg();//need to implement this on later.
                myStatus.IsSuccess = false;
                return myStatus;
            }
            else
            {
                dbRecord.IsHandicapped = _EEmployee.IsHandicapped;
                dbRecord.HandicappedDetails = _EEmployee.HandicappedDetails;
                PayrollDataContext.SubmitChanges();
                return myStatus;
            }

        }

        public Status UpdateBrithMark(EEmployee _EEmployee)
        {
            Status myStatus = new Status();

            EEmployee dbRecord = PayrollDataContext.EEmployees.SingleOrDefault(c => c.EmployeeId == _EEmployee.EmployeeId);
            if (dbRecord == null)
            {
                myStatus.ErrorMessage = "error while saving.";//GetConcurrencyErrorMsg();//need to implement this on later.
                myStatus.IsSuccess = false;
                return myStatus;
            }
            else
            {

                dbRecord.Birthmark = _EEmployee.Birthmark;
                PayrollDataContext.SubmitChanges();
                return myStatus;
            }

        }

        public Status UpdateBloodGroup(HHumanResource _HHumanResource)
        {
            Status myStatus = new Status();

            HHumanResource dbRecord = PayrollDataContext.HHumanResources.SingleOrDefault(c => c.EmployeeId == _HHumanResource.EmployeeId);

            if (dbRecord == null)
            {
                myStatus.ErrorMessage = "error while saving.";//GetConcurrencyErrorMsg();//need to implement this on later.
                myStatus.IsSuccess = false;
                return myStatus;
            }
            else
            {

                dbRecord.BloodGroup = _HHumanResource.BloodGroup;
                PayrollDataContext.SubmitChanges();
                return myStatus;
            }

        }


        public Status InsertUpdateHealth(HHealth _HHealth, bool IsInsert)
        {
            Status myStatus = new Status();
            //Insert Operation
            if (IsInsert)
            {
                PayrollDataContext.HHealths.InsertOnSubmit(_HHealth);
                PayrollDataContext.SubmitChanges();
                return myStatus;
            }

            HHealth dbRecord = PayrollDataContext.HHealths.SingleOrDefault(c => c.HealthId == _HHealth.HealthId);

            if (dbRecord == null)
            {
                myStatus.ErrorMessage = "error while saving.";//GetConcurrencyErrorMsg();//need to implement this on later.
                myStatus.IsSuccess = false;
                return myStatus;
            }
            else
            {
                CopyObject<HHealth>(_HHealth, ref dbRecord, "HealthID", "EmployeeID");
                PayrollDataContext.SubmitChanges();
                return myStatus;
            }

        }

        #endregion
        #region Citizenship
        public static List<HCitizenship> GetCitizenshipByEmployeeID(int EmployeeId)
        {
            List<HCitizenship> list = PayrollDataContext.HCitizenships.Where(x => x.EmployeeId == EmployeeId).ToList();
            return list;
        }

        public static HCitizenship GetCitizenshipDetailsById(int CitizenshipID)
        {
            return PayrollDataContext.HCitizenships.SingleOrDefault(
                e => e.CitizenshipId == CitizenshipID);
        }

        public static bool DeleteCitizenshipByID(int CitizenshipID)
        {
            HCitizenship AssetDocument = PayrollDataContext.HCitizenships.SingleOrDefault(t => t.CitizenshipId == CitizenshipID);
            PayrollDataContext.HCitizenships.DeleteOnSubmit(AssetDocument);
            PayrollDataContext.SubmitChanges();
            return true;
        }


        public Status InsertUpdateCitizenship(HCitizenship _HCitizenship, bool IsInsert)
        {
            Status myStatus = new Status();

            _HCitizenship.ModifiedBy = SessionManager.CurrentLoggedInUserID;
            _HCitizenship.ModifiedOn = DateTime.Now;

            if (SessionManager.CurrentLoggedInEmployeeId == 0)
            {
                _HCitizenship.ApprovedBy = SessionManager.CurrentLoggedInUserID;
                _HCitizenship.ApprovedOn = DateTime.Now;
            }

            //Insert Operation
            if (IsInsert)
            {
                PayrollDataContext.HCitizenships.InsertOnSubmit(_HCitizenship);
                PayrollDataContext.SubmitChanges();
                return myStatus;
            }

            HCitizenship dbRecord = PayrollDataContext.HCitizenships.SingleOrDefault(c => c.CitizenshipId == _HCitizenship.CitizenshipId);

            if (dbRecord == null)
            {
                myStatus.ErrorMessage = "error while saving.";//GetConcurrencyErrorMsg();//need to implement this on later.
                myStatus.IsSuccess = false;
                return myStatus;
            }
            else
            {
                CopyObject<HCitizenship>(_HCitizenship, ref dbRecord, "CitizenshipID", "EmployeeID");
                PayrollDataContext.SubmitChanges();
                return myStatus;
            }

        }

        public Status DeleteCitizenshipFile(int CitizenshipID)
        {
            Status myStatus = new Status();
            HCitizenship dbRecord = PayrollDataContext.HCitizenships.SingleOrDefault(c => c.CitizenshipId == CitizenshipID);

            if (dbRecord == null)
            {
                myStatus.ErrorMessage = "error while saving.";//GetConcurrencyErrorMsg();//need to implement this on later.
                myStatus.IsSuccess = false;
                return myStatus;
            }
            else
            {
                dbRecord.FileFormat = "";
                dbRecord.FileLocation = "";
                dbRecord.ServerFileName = "";
                dbRecord.Size = "";
                dbRecord.UserFileName = "";
                dbRecord.FileType = "";
                PayrollDataContext.SubmitChanges();
                return myStatus;
            }
        }
        #endregion
        #region DrivingLicence
        public static List<HDrivingLicence> GetDrivingLicenceByEmployeeID(int EmployeeId)
        {
            List<HDrivingLicence> list = PayrollDataContext.HDrivingLicences.Where(x => x.EmployeeId == EmployeeId).ToList();
            return list;
        }

        public static HDrivingLicence GetDrivingLicenceDetailsById(int DrivingLicenceID)
        {
            return PayrollDataContext.HDrivingLicences.SingleOrDefault(
                e => e.DrivingLicenceId == DrivingLicenceID);
        }

        public static bool DeleteDrivingLicenceByID(int DrivingLicenceID)
        {
            HDrivingLicence AssetDocument = PayrollDataContext.HDrivingLicences.SingleOrDefault(t => t.DrivingLicenceId == DrivingLicenceID);
            PayrollDataContext.HDrivingLicences.DeleteOnSubmit(AssetDocument);
            PayrollDataContext.SubmitChanges();
            return true;
        }


        public Status InsertUpdateDrivingLicence(HDrivingLicence _HDrivingLicence, bool IsInsert)
        {
            Status myStatus = new Status();

            _HDrivingLicence.ModifiedBy = SessionManager.CurrentLoggedInUserID;
            _HDrivingLicence.ModifiedOn = DateTime.Now;

            if (SessionManager.CurrentLoggedInEmployeeId == 0)
            {
                _HDrivingLicence.ApprovedBy = SessionManager.CurrentLoggedInUserID;
                _HDrivingLicence.ApprovedOn = DateTime.Now;
            }

            //Insert Operation
            if (IsInsert)
            {
                PayrollDataContext.HDrivingLicences.InsertOnSubmit(_HDrivingLicence);
                PayrollDataContext.SubmitChanges();
                return myStatus;
            }

            HDrivingLicence dbRecord = PayrollDataContext.HDrivingLicences.SingleOrDefault(c => c.DrivingLicenceId == _HDrivingLicence.DrivingLicenceId);

            if (dbRecord == null)
            {
                myStatus.ErrorMessage = "error while saving.";//GetConcurrencyErrorMsg();//need to implement this on later.
                myStatus.IsSuccess = false;
                return myStatus;
            }
            else
            {
                CopyObject<HDrivingLicence>(_HDrivingLicence, ref dbRecord, "DrivingLicenceID", "EmployeeID");
                PayrollDataContext.SubmitChanges();
                return myStatus;
            }

        }

        public Status DeleteDrivingLicenceFile(int DrivingLicenceID)
        {
            Status myStatus = new Status();
            HDrivingLicence dbRecord = PayrollDataContext.HDrivingLicences.SingleOrDefault(c => c.DrivingLicenceId == DrivingLicenceID);

            if (dbRecord == null)
            {
                myStatus.ErrorMessage = "error while saving.";//GetConcurrencyErrorMsg();//need to implement this on later.
                myStatus.IsSuccess = false;
                return myStatus;
            }
            else
            {
                dbRecord.FileFormat = "";
                dbRecord.FileLocation = "";
                dbRecord.ServerFileName = "";
                dbRecord.Size = "";
                dbRecord.UserFileName = "";
                dbRecord.FileType = "";
                PayrollDataContext.SubmitChanges();
                return myStatus;
            }
        }
        #endregion
        #region Passport
        public static List<HPassport> GetPassportByEmployeeID(int EmployeeId)
        {
            List<HPassport> list = PayrollDataContext.HPassports.Where(x => x.EmployeeId == EmployeeId).ToList();
            return list;
        }

        public static HPassport GetPassportDetailsById(int PassportID)
        {
            return PayrollDataContext.HPassports.SingleOrDefault(
                e => e.PassportId == PassportID);
        }

        public static bool DeletePassportByID(int PassportID)
        {
            HPassport AssetDocument = PayrollDataContext.HPassports.SingleOrDefault(t => t.PassportId == PassportID);
            PayrollDataContext.HPassports.DeleteOnSubmit(AssetDocument);
            PayrollDataContext.SubmitChanges();
            return true;
        }


        public Status InsertUpdatePassport(HPassport _HPassport, bool IsInsert)
        {
            Status myStatus = new Status();
            //Insert Operation

            _HPassport.ModifiedBy = SessionManager.CurrentLoggedInUserID;
            _HPassport.ModifiedOn = DateTime.Now;

            if (SessionManager.CurrentLoggedInEmployeeId == 0)
            {
                _HPassport.ApprovedBy = SessionManager.CurrentLoggedInUserID;
                _HPassport.ApprovedOn = DateTime.Now;
            }

            if (IsInsert)
            {
                PayrollDataContext.HPassports.InsertOnSubmit(_HPassport);
                PayrollDataContext.SubmitChanges();
                return myStatus;
            }

            HPassport dbRecord = PayrollDataContext.HPassports.SingleOrDefault(c => c.PassportId == _HPassport.PassportId);

            if (dbRecord == null)
            {
                myStatus.ErrorMessage = "error while saving.";//GetConcurrencyErrorMsg();//need to implement this on later.
                myStatus.IsSuccess = false;
                return myStatus;
            }
            else
            {
                CopyObject<HPassport>(_HPassport, ref dbRecord, "PassportID", "EmployeeID");
                PayrollDataContext.SubmitChanges();
                return myStatus;
            }

        }

        public Status DeletePassportFile(int PassportID)
        {
            Status myStatus = new Status();
            HPassport dbRecord = PayrollDataContext.HPassports.SingleOrDefault(c => c.PassportId == PassportID);

            if (dbRecord == null)
            {
                myStatus.ErrorMessage = "error while saving.";//GetConcurrencyErrorMsg();//need to implement this on later.
                myStatus.IsSuccess = false;
                return myStatus;
            }
            else
            {
                dbRecord.FileFormat = "";
                dbRecord.FileLocation = "";
                dbRecord.ServerFileName = "";
                dbRecord.Size = "";
                dbRecord.UserFileName = "";
                dbRecord.FileType = "";
                PayrollDataContext.SubmitChanges();
                return myStatus;
            }
        }
        #endregion
        #region PreviousEmployment
        public static List<HPreviousEmployment> GetPreviousEmploymentByEmployeeID(int EmployeeId)
        {
            List<HPreviousEmployment> list = PayrollDataContext.HPreviousEmployments.Where(x => x.EmployeeId == EmployeeId).ToList();

            foreach (var item in list)
            {
                item.IsEditable = NewHRManager.IsHrEditable(item.Status);
                if (item.CategoryID != null)
                    item.Category = GetExperienceCategory(item.CategoryID.Value);
            }

            return list;
        }

        public static string GetExperienceCategory(int categoryId)
        {
            if (PayrollDataContext.ExperienceCategories.Any(x => x.CategoryID == categoryId))
                return PayrollDataContext.ExperienceCategories.SingleOrDefault(x => x.CategoryID == categoryId).Name;
            else
                return "";
        }

        public static HPreviousEmployment GetPreviousEmploymentDetailsById(int PreviousEmploymentID)
        {
            return PayrollDataContext.HPreviousEmployments.SingleOrDefault(
                e => e.PrevEmploymentId == PreviousEmploymentID);
        }

        public static bool DeletePreviousEmploymentByID(int PreviousEmploymentID)
        {
            HPreviousEmployment AssetDocument = PayrollDataContext.HPreviousEmployments.SingleOrDefault(t => t.PrevEmploymentId == PreviousEmploymentID);
            PayrollDataContext.HPreviousEmployments.DeleteOnSubmit(AssetDocument);
            PayrollDataContext.SubmitChanges();
            return true;
        }


        public Status InsertUpdatePreviousEmployment(HPreviousEmployment _HPreviousEmployment, bool IsInsert)
        {
            Status myStatus = new Status();

            //Insert Operation
            if (IsInsert)
            {
                _HPreviousEmployment.Modifiedby = SessionManager.CurrentLoggedInUserID;
                _HPreviousEmployment.ModifiedOn = BLL.BaseBiz.GetCurrentDateAndTime();
                if (SessionManager.CurrentLoggedInEmployeeId == 0)
                {
                    _HPreviousEmployment.ApprovedBy = SessionManager.CurrentLoggedInUserID;
                    _HPreviousEmployment.ApprovedOn = BLL.BaseBiz.GetCurrentDateAndTime();
                    _HPreviousEmployment.Status = (int)HRStatusEnum.Approved;
                }
                else
                {
                    _HPreviousEmployment.Status = (int)HRStatusEnum.Saved;

                }

                PayrollDataContext.HPreviousEmployments.InsertOnSubmit(_HPreviousEmployment);
                PayrollDataContext.SubmitChanges();
                return myStatus;
            }

            HPreviousEmployment dbRecord = PayrollDataContext.HPreviousEmployments.SingleOrDefault(c => c.PrevEmploymentId == _HPreviousEmployment.PrevEmploymentId);

            if (dbRecord == null)
            {
                myStatus.ErrorMessage = "error while saving.";//GetConcurrencyErrorMsg();//need to implement this on later.
                myStatus.IsSuccess = false;
                return myStatus;
            }
            else
            {
                _HPreviousEmployment.Modifiedby = SessionManager.CurrentLoggedInUserID;
                _HPreviousEmployment.ModifiedOn = BLL.BaseBiz.GetCurrentDateAndTime();
                if (SessionManager.CurrentLoggedInEmployeeId == 0)
                {
                    _HPreviousEmployment.ApprovedBy = SessionManager.CurrentLoggedInUserID;
                    _HPreviousEmployment.ApprovedOn = BLL.BaseBiz.GetCurrentDateAndTime();
                    _HPreviousEmployment.Status = (int)HRStatusEnum.Approved;
                }
                else
                {
                    _HPreviousEmployment.Status = (int)HRStatusEnum.Saved;
                }


                CopyObject<HPreviousEmployment>(_HPreviousEmployment, ref dbRecord, "PreviousEmploymentID", "EmployeeID");
                PayrollDataContext.SubmitChanges();
                return myStatus;
            }

        }


        #endregion
        #region photograph
        public bool SaveOrUpdateImage(UploadType type, string fileLocation, int employeeId, string thumbnailLocation)
        {
            HHumanResource dbEntity = GetHumanResource(employeeId);
            bool isSaved = false;
            string oldFileUrl = "";
            if (dbEntity == null)
            {
                isSaved = true;
                dbEntity = new HHumanResource();
                dbEntity.EmployeeId = employeeId;
            }


            switch (type)
            {
                case UploadType.Photo:
                    oldFileUrl = dbEntity.UrlPhoto;
                    dbEntity.UrlPhoto = fileLocation;
                    dbEntity.UrlPhotoThumbnail = thumbnailLocation;
                    break;
                case UploadType.Signature:
                    oldFileUrl = dbEntity.UrlSignature;
                    dbEntity.UrlSignature = fileLocation;
                    dbEntity.UrlSignatureThumbnail = thumbnailLocation;
                    break;
                case UploadType.ThumbPrint:
                    oldFileUrl = dbEntity.UrlThumbPrint;
                    dbEntity.UrlThumbPrint = fileLocation;
                    break;
            }

            if (isSaved)
            {
                PayrollDataContext.HHumanResources.InsertOnSubmit(dbEntity);
                return SaveChangeSet();
            }
            else
            {
                //delete old file if has
                try
                {
                    if (!string.IsNullOrEmpty(oldFileUrl))
                    {
                        string url = Path.Combine(HttpContext.Current.Server.MapPath(Config.UploadLocation), oldFileUrl);
                        File.Delete(url);
                    }
                }
                catch { }

                return UpdateChangeSet();
            }
        }
        public HHumanResource GetHumanResource(int employeeId)
        {
            return PayrollDataContext.HHumanResources.SingleOrDefault
                (e => e.EmployeeId == employeeId);
        }
        public EEmployee GetHandicappedDetailsByEmployee(int employeeId)
        {
            return PayrollDataContext.EEmployees.SingleOrDefault
                (e => e.EmployeeId == employeeId);
        }

        #endregion

        public bool SaveDocument(HDocument document)
        {
            PayrollDataContext.HDocuments.InsertOnSubmit(document);
            return SaveChangeSet();
        }

        public string GetHTMLDocuments(int employeeId)
        {
            StringBuilder str = new StringBuilder();

            List<HDocument> docs = GetDocumentList(employeeId);
            string code =
                @"<div class='documentSeparatorDiv' > <a href='../DocumentHandler.ashx?id={0}'>{1}</a> <input type='image' onclick='confirmDelDoc({2});return false;' src='../images/delete.gif'></image> </div>";
            foreach (HDocument doc in docs)
            {
                str.Append(string.Format(code, Path.GetFileNameWithoutExtension(doc.Url), doc.Name, doc.DocumentId));
            }
            return str.ToString();
        }

        public List<HDocument> GetDocumentList(int employeeId)
        {
            return PayrollDataContext.HDocuments.Where
                (e => e.EmployeeId == employeeId).ToList();
        }


        public static List<JobDescription> GetCurrentJobDescripton(int employeeId)
        {
            return PayrollDataContext.JobDescriptions.Where(x => x.EmpId == employeeId).OrderByDescending(x => x.EffectiveDateEng).Take(1).ToList();
        }

        public static List<JobDescription> GetJobDescriptonHistory(int employeeId)
        {
            List<JobDescription> _listJobDesc = PayrollDataContext.JobDescriptions.Where(x => x.EmpId == employeeId).OrderByDescending(x => x.EffectiveDateEng).Take(1).ToList();
            if (_listJobDesc.Any())
            {
                DateTime DateEng = _listJobDesc[0].EffectiveDateEng;
                _listJobDesc = PayrollDataContext.JobDescriptions.Where(x => x.EmpId == employeeId && x.EffectiveDateEng != DateEng.Date).OrderByDescending(x => x.EffectiveDateEng).ToList();
            }
            return _listJobDesc;
        }

        public static JobDescription GetEmpJobDescriptionByEffectiveDate(int EmpId, DateTime EffectiveDate)
        {
            return PayrollDataContext.JobDescriptions.SingleOrDefault(x => x.EffectiveDateEng == EffectiveDate.Date && x.EmpId == EmpId);
        }


        public static Status InsertUpdateJD(JobDescription _objJobDescription)
        {
            Status status = new Status();
            JobDescription dbJobDescription = PayrollDataContext.JobDescriptions.SingleOrDefault(x => x.EffectiveDateEng == _objJobDescription.EffectiveDateEng && x.EmpId == _objJobDescription.EmpId);
            if (dbJobDescription == null)
            {
                _objJobDescription.CreatedBy = SessionManager.CurrentLoggedInUserID;
                _objJobDescription.CreatedDate = BLL.BaseBiz.GetCurrentDateAndTime();
                _objJobDescription.ModifiedDate = _objJobDescription.CreatedDate;
                _objJobDescription.ModifiedBy = _objJobDescription.CreatedBy;
                PayrollDataContext.JobDescriptions.InsertOnSubmit(_objJobDescription);
                PayrollDataContext.SubmitChanges();
                status.IsSuccess = true;
            }
            else
                status.ErrorMessage = "Information already saved for this employee";
            return status;
        }

        public static Status UpdateJDEditor(JobDescription _objJobDescription)
        {
            Status status = new Status();
            JobDescription dbJobDescription = PayrollDataContext.JobDescriptions.SingleOrDefault(x => x.EffectiveDateEng == _objJobDescription.EffectiveDateEng && x.EmpId == _objJobDescription.EmpId);
            if (dbJobDescription != null)
            {

                dbJobDescription.ModifiedDate = _objJobDescription.CreatedDate;
                dbJobDescription.ModifiedBy = _objJobDescription.CreatedBy;
                dbJobDescription.JobDesc = _objJobDescription.JobDesc;
                PayrollDataContext.SubmitChanges();
                status.IsSuccess = true;
            }
            else
                status.ErrorMessage = "Job Information is not saved for this employee";
            return status;
        }

        public static Status InsertUpdateFamily(HFamily hFamily, bool isInsert)
        {
            Status status = new Status();
            string oldFileUrl = "";

            HFamily familyInstance = new HFamily();
            if (isInsert)
            {
                hFamily.Modifiedby = SessionManager.CurrentLoggedInUserID;
                hFamily.ModifiedOn = BLL.BaseBiz.GetCurrentDateAndTime();
                if (SessionManager.CurrentLoggedInEmployeeId == 0)
                {
                    hFamily.ApprovedBy = SessionManager.CurrentLoggedInUserID;
                    hFamily.ApprovedOn = BLL.BaseBiz.GetCurrentDateAndTime();
                    hFamily.Status = (int)HRStatusEnum.Approved;
                }
                else
                {
                    hFamily.Status = (int)HRStatusEnum.Saved;
                }
                PayrollDataContext.HFamilies.InsertOnSubmit(hFamily);
            }
            else
            {
                familyInstance = PayrollDataContext.HFamilies.Where(x => x.FamilyId == hFamily.FamilyId).First();
                familyInstance.Name = hFamily.Name;
                familyInstance.Relation = hFamily.Relation;
                familyInstance.RelationID = hFamily.RelationID;
                familyInstance.DateOfBirth = hFamily.DateOfBirth;
                familyInstance.HasDependent = hFamily.HasDependent;
                familyInstance.Remarks = hFamily.Remarks;
                familyInstance.SpecifiedDate = hFamily.SpecifiedDate;
                familyInstance.AgeOnSpecifiedSPDate = hFamily.AgeOnSpecifiedSPDate;
                familyInstance.OccupationId = hFamily.OccupationId;
                familyInstance.ContactNumber = hFamily.ContactNumber;

                familyInstance.Title = hFamily.Title;
                familyInstance.Gender = hFamily.Gender;
                familyInstance.BloodGroup = hFamily.BloodGroup;
                if (hFamily.Nationality != null)
                    familyInstance.Nationality = hFamily.Nationality;
                familyInstance.DocumentIdType = hFamily.DocumentIdType;
                familyInstance.DocumentIssueDate = hFamily.DocumentIssueDate;
                familyInstance.DocumentIssueDateEng = hFamily.DocumentIssueDateEng;
                familyInstance.DocumentIssuePlace = hFamily.DocumentIssuePlace;
                familyInstance.ForInsurance = hFamily.ForInsurance;
                familyInstance.Modifiedby = SessionManager.CurrentLoggedInUserID;
                familyInstance.ModifiedOn = BLL.BaseBiz.GetCurrentDateAndTime();
                if (SessionManager.CurrentLoggedInEmployeeId == 0)
                {
                    familyInstance.ApprovedBy = SessionManager.CurrentLoggedInUserID;
                    familyInstance.ApprovedOn = BLL.BaseBiz.GetCurrentDateAndTime();
                    familyInstance.Status = (int)HRStatusEnum.Approved;
                }
                else
                {
                    familyInstance.Status = (int)HRStatusEnum.Saved;
                }

            }

            PayrollDataContext.SubmitChanges();
            status.IsSuccess = true;
            return status;
        }

        public static Status InsertUpdateGoalCategory(AppraisalGoalsCategory categoryInstance)
        {
            Status status = new Status();
            AppraisalGoalsCategory dbEntity = new AppraisalGoalsCategory();

            if (PayrollDataContext.AppraisalGoalsCategories.Any(x => x.CategoryID == categoryInstance.CategoryID))
            {
                dbEntity = PayrollDataContext.AppraisalGoalsCategories.Where(x => x.CategoryID == categoryInstance.CategoryID).First();
                dbEntity.Name = categoryInstance.Name;
            }
            else
            {
                PayrollDataContext.AppraisalGoalsCategories.InsertOnSubmit(categoryInstance);
            }

            PayrollDataContext.SubmitChanges();
            status.IsSuccess = true;
            return status;
        }

        public static Status InsertUpdateAppraisalCategory(AppraisalCategory categoryInstance)
        {
            Status status = new Status();
            AppraisalCategory dbEntity = new AppraisalCategory();

            if (PayrollDataContext.AppraisalCategories.Any(x => x.CategoryID == categoryInstance.CategoryID))
            {
                dbEntity = PayrollDataContext.AppraisalCategories.Where(x => x.CategoryID == categoryInstance.CategoryID).First();
                dbEntity.Name = categoryInstance.Name;
            }
            else
            {
                PayrollDataContext.AppraisalCategories.InsertOnSubmit(categoryInstance);
            }

            PayrollDataContext.SubmitChanges();
            status.IsSuccess = true;
            return status;
        }

        #region Appraisal From
        public static Status InsertUpdateAppraisalForm_GeneralSetting(AppraisalForm _AppraisalForm, bool isInsert)
        {
            Status status = new Status();
            _AppraisalForm.ModifiedBy = SessionManager.CurrentLoggedInUserID;
            _AppraisalForm.ModifiedDate = GetCurrentDateAndTime();
            if (isInsert)
            {
                _AppraisalForm.CreatedBy = SessionManager.CurrentLoggedInUserID;
                _AppraisalForm.CreatedDate = GetCurrentDateAndTime();
                _AppraisalForm.AllowActivitySelfRating = true;
                _AppraisalForm.AllowCompetencySelfRating = true;
                _AppraisalForm.AllowQuestionSelfRating = true;
                PayrollDataContext.AppraisalForms.InsertOnSubmit(_AppraisalForm);
            }
            else
            {
                AppraisalForm dbEntity = PayrollDataContext.AppraisalForms.SingleOrDefault(x => x.AppraisalFormID == _AppraisalForm.AppraisalFormID);
                if (dbEntity != null)
                {
                    dbEntity.ModifiedBy = _AppraisalForm.ModifiedBy;
                    dbEntity.ModifiedDate = _AppraisalForm.ModifiedDate;
                    dbEntity.Name = _AppraisalForm.Name;
                    dbEntity.Description = _AppraisalForm.Description;
                    //dbEntity.HideRecommendBlock = _AppraisalForm.HideRecommendBlock;
                }
            }


            PayrollDataContext.SubmitChanges();
            status.IsSuccess = true;
            return status;
        }
        public static Status UpdateAppraisalForm_Objectives(AppraisalForm _AppraisalForm)
        {
            Status status = new Status();
            _AppraisalForm.ModifiedBy = SessionManager.CurrentLoggedInUserID;
            _AppraisalForm.ModifiedDate = GetCurrentDateAndTime();

            AppraisalForm dbEntity = PayrollDataContext.AppraisalForms.SingleOrDefault(x => x.AppraisalFormID == _AppraisalForm.AppraisalFormID);
            if (dbEntity != null)
            {
                dbEntity.ObjectiveName = _AppraisalForm.ObjectiveName;
                dbEntity.ObjectiveDescription = _AppraisalForm.ObjectiveDescription;
                dbEntity.HideObjectiveBlock = _AppraisalForm.HideObjectiveBlock;
            }


            PayrollDataContext.SubmitChanges();
            status.IsSuccess = true;
            return status;
        }

        public static Status UpdateAppraisalForm_PerformanceSummary(AppraisalForm _AppraisalForm
            , List<AppraisalFormGrading> gradeList, List<AppraisalFormDistribution> distributionList)
        {
            Status status = new Status();
            _AppraisalForm.ModifiedBy = SessionManager.CurrentLoggedInUserID;
            _AppraisalForm.ModifiedDate = GetCurrentDateAndTime();

            AppraisalForm dbEntity = PayrollDataContext.AppraisalForms.SingleOrDefault(x => x.AppraisalFormID == _AppraisalForm.AppraisalFormID);
            if (dbEntity != null)
            {
                dbEntity.PerformanceSummaryName = _AppraisalForm.PerformanceSummaryName;
                dbEntity.PerformanceSummaryDescription = _AppraisalForm.PerformanceSummaryDescription;

                //dbEntity.PerformanceCoreValue = _AppraisalForm.PerformanceCoreValue;
                //dbEntity.PerformanceActivityValue = _AppraisalForm.PerformanceActivityValue;
                //dbEntity.PerformanceQuestionnaireValue = _AppraisalForm.PerformanceQuestionnaireValue;
                //dbEntity.PerformanceTargetValue = _AppraisalForm.PerformanceTargetValue;

                dbEntity.HideRecommendBlock = _AppraisalForm.HideRecommendBlock;
            }



            int order = 1;
            foreach (AppraisalFormGrading item in gradeList)
            {
                item.Order = (order++);
                item.AppraisalFormRef_ID = _AppraisalForm.AppraisalFormID;
            }

            PayrollDataContext.AppraisalFormGradings.DeleteAllOnSubmit(
                PayrollDataContext.AppraisalFormGradings.Where(x => x.AppraisalFormRef_ID == _AppraisalForm.AppraisalFormID));
            PayrollDataContext.AppraisalFormGradings.InsertAllOnSubmit(gradeList);

            PayrollDataContext.AppraisalFormDistributions.DeleteAllOnSubmit(
                PayrollDataContext.AppraisalFormDistributions.Where(x => x.AppraisalFormRef_ID == _AppraisalForm.AppraisalFormID));
            PayrollDataContext.AppraisalFormDistributions.InsertAllOnSubmit(distributionList);

            PayrollDataContext.SubmitChanges();
            status.IsSuccess = true;
            return status;
        }


        public static Status UpdateAppraisalForm_Summary(AppraisalForm _AppraisalForm)
        {
            Status status = new Status();
            _AppraisalForm.ModifiedBy = SessionManager.CurrentLoggedInUserID;
            _AppraisalForm.ModifiedDate = GetCurrentDateAndTime();

            AppraisalForm dbEntity = PayrollDataContext.AppraisalForms.SingleOrDefault(x => x.AppraisalFormID == _AppraisalForm.AppraisalFormID);
            if (dbEntity != null)
            {
                dbEntity.SummaryName = _AppraisalForm.SummaryName;
                dbEntity.SummaryDescription = _AppraisalForm.SummaryDescription;
                dbEntity.ShowAgreeDisAgreeButtons = _AppraisalForm.ShowAgreeDisAgreeButtons;
            }


            PayrollDataContext.SubmitChanges();
            status.IsSuccess = true;
            return status;
        }


        public static Status UpdateAppraisalForm_Signature(AppraisalForm _AppraisalForm)
        {
            Status status = new Status();
            _AppraisalForm.ModifiedBy = SessionManager.CurrentLoggedInUserID;
            _AppraisalForm.ModifiedDate = GetCurrentDateAndTime();

            AppraisalForm dbEntity = PayrollDataContext.AppraisalForms.SingleOrDefault(x => x.AppraisalFormID == _AppraisalForm.AppraisalFormID);
            if (dbEntity != null)
            {
                dbEntity.SignationName = _AppraisalForm.SignationName;
                dbEntity.SignationDescription = _AppraisalForm.SignationDescription;
            }

            PayrollDataContext.SubmitChanges();
            status.IsSuccess = true;
            return status;
        }

        public static Status UpdateAppraisalForm_Activities(AppraisalForm _AppraisalForm)
        {
            Status status = new Status();
            _AppraisalForm.ModifiedBy = SessionManager.CurrentLoggedInUserID;
            _AppraisalForm.ModifiedDate = GetCurrentDateAndTime();

            AppraisalForm dbEntity = PayrollDataContext.AppraisalForms.SingleOrDefault(x => x.AppraisalFormID == _AppraisalForm.AppraisalFormID);
            if (dbEntity != null)
            {
                dbEntity.ActivityName = _AppraisalForm.ActivityName;
                dbEntity.ActivityDescription = _AppraisalForm.ActivityDescription;
                dbEntity.ActivityRatingScaleRef_ID = _AppraisalForm.ActivityRatingScaleRef_ID;
                dbEntity.AllowActivitySelfRating = _AppraisalForm.AllowActivitySelfRating;

                dbEntity.ActivityMarkingType = _AppraisalForm.ActivityMarkingType;
                dbEntity.ActivityMaximumMarks = _AppraisalForm.ActivityMaximumMarks;

                dbEntity.HideActivitySummary = _AppraisalForm.HideActivitySummary;
            }


            PayrollDataContext.SubmitChanges();
            status.IsSuccess = true;
            return status;
        }


        public static Status UpdateAppraisalForm_QualificationExpirence(AppraisalForm _AppraisalForm)
        {
            Status status = new Status();
            _AppraisalForm.ModifiedBy = SessionManager.CurrentLoggedInUserID;
            _AppraisalForm.ModifiedDate = GetCurrentDateAndTime();

            AppraisalForm dbEntity = PayrollDataContext.AppraisalForms.SingleOrDefault(x => x.AppraisalFormID == _AppraisalForm.AppraisalFormID);
            if (dbEntity != null)
            {
                dbEntity.HistoryName = _AppraisalForm.HistoryName;
                dbEntity.HistoryDescription = _AppraisalForm.HistoryDescription;

                dbEntity.HistoryEducationRatingScaleRef_ID = _AppraisalForm.HistoryEducationRatingScaleRef_ID;
                dbEntity.TransferRatingScaleRef_ID = _AppraisalForm.TransferRatingScaleRef_ID;
                dbEntity.WorkHistoryRatingScaleRef_ID = _AppraisalForm.WorkHistoryRatingScaleRef_ID;
                dbEntity.HideHistorySection = _AppraisalForm.HideHistorySection;
            }

            PayrollDataContext.SubmitChanges();
            status.IsSuccess = true;
            return status;
        }


        public static Status UpdateAppraisalForm_Intruduction(AppraisalForm _AppraisalForm)
        {
            Status status = new Status();
            _AppraisalForm.ModifiedBy = SessionManager.CurrentLoggedInUserID;
            _AppraisalForm.ModifiedDate = GetCurrentDateAndTime();

            AppraisalForm dbEntity = PayrollDataContext.AppraisalForms.SingleOrDefault(x => x.AppraisalFormID == _AppraisalForm.AppraisalFormID);
            if (dbEntity != null)
            {
                dbEntity.IntroductionName = _AppraisalForm.IntroductionName;
                dbEntity.IntroductionDescription = _AppraisalForm.IntroductionDescription;
            }


            PayrollDataContext.SubmitChanges();
            status.IsSuccess = true;
            return status;
        }

        public static List<AppraisalFormCompetency> SetFormCompentencyFromCategory(List<AppraisalFormCompetency> _listAppraisalFormCompetency, int formId
            , List<AppraisalFormCompetencyCategory> catList)
        {
            List<AppraisalFormCompetency> list = new List<AppraisalFormCompetency>();

            int seq = 0;


            // iterate category list
            foreach (AppraisalFormCompetency item in _listAppraisalFormCompetency)
            {
                List<AppraisalCompetency> compentencyList =
                    PayrollDataContext.AppraisalCompetencies.Where(x => x.CategoryID == item.CompetencyCatRef_ID)
                    .OrderBy(x => x.CompetencyID).ToList();

                double weight = 0;

                if (compentencyList.Count > 0)
                {
                    weight = catList.FirstOrDefault(x => x.CompetencyCategoryID == item.CompetencyCatRef_ID).Weight /
                             compentencyList.Count;
                    weight = double.Parse(weight.ToString("n2"));
                }

                foreach (AppraisalCompetency comp in compentencyList)
                {
                    list.Add(new AppraisalFormCompetency
                    {
                        CompetencyCatRef_ID = item.CompetencyCatRef_ID,
                        CompetencyRef_ID = comp.CompetencyID
                        ,
                        Sequence = seq++
                        ,
                        Weight = weight
                        ,
                        AppraisalFormRef_ID = formId
                    });

                }
            }

            return list;
        }

        public static Status UpdateAppraisalForm_Competencies(AppraisalForm _AppraisalForm,
            List<AppraisalFormCompetency> _listAppraisalFormCompetency, List<AppraisalFormCompetencyCategory> catList)
        {
            Status status = new Status();
            _AppraisalForm.ModifiedBy = SessionManager.CurrentLoggedInUserID;
            _AppraisalForm.ModifiedDate = GetCurrentDateAndTime();


            _listAppraisalFormCompetency = SetFormCompentencyFromCategory(_listAppraisalFormCompetency, _AppraisalForm.AppraisalFormID, catList);

            AppraisalForm dbEntity = PayrollDataContext.AppraisalForms.SingleOrDefault(x => x.AppraisalFormID == _AppraisalForm.AppraisalFormID);
            if (dbEntity != null)
            {
                dbEntity.ModifiedBy = _AppraisalForm.ModifiedBy;
                dbEntity.ModifiedDate = _AppraisalForm.ModifiedDate;
                dbEntity.CompentencyName = _AppraisalForm.CompentencyName;
                dbEntity.CompentencyDescription = _AppraisalForm.CompentencyDescription;
                dbEntity.CompentencyRatingScaleRef_ID = _AppraisalForm.CompentencyRatingScaleRef_ID;
                dbEntity.AllowCompetencySelfRating = _AppraisalForm.AllowCompetencySelfRating;
                dbEntity.AllowCompetencySelfComment = _AppraisalForm.AllowCompetencySelfComment;
                dbEntity.HideCompetencyCoreValues = _AppraisalForm.HideCompetencyCoreValues;
            }


            //update for AppraisalFormCompetency
            List<AppraisalFormCompetency> dbEntityAppraisalFormCompetency = PayrollDataContext.AppraisalFormCompetencies.Where(x => x.AppraisalFormRef_ID == _AppraisalForm.AppraisalFormID).ToList();
            if (dbEntity != null)
            {
                PayrollDataContext.AppraisalFormCompetencies.DeleteAllOnSubmit(dbEntityAppraisalFormCompetency);
            }

            if (_listAppraisalFormCompetency.Any())
                PayrollDataContext.AppraisalFormCompetencies.InsertAllOnSubmit(_listAppraisalFormCompetency);


            dbEntity.AppraisalFormCompetencyCategories.Clear();
            dbEntity.AppraisalFormCompetencyCategories.AddRange(catList);


            PayrollDataContext.SubmitChanges();
            status.IsSuccess = true;
            return status;
        }

        public static Status UpdateAppraisalForm_Targets(AppraisalForm _AppraisalForm,
           List<AppraisalFormTarget> _listAppraisalFormCompetency)
        {
            Status status = new Status();
            _AppraisalForm.ModifiedBy = SessionManager.CurrentLoggedInUserID;
            _AppraisalForm.ModifiedDate = GetCurrentDateAndTime();


            AppraisalForm dbEntity = PayrollDataContext.AppraisalForms.SingleOrDefault(x => x.AppraisalFormID == _AppraisalForm.AppraisalFormID);
            if (dbEntity != null)
            {
                dbEntity.ModifiedBy = _AppraisalForm.ModifiedBy;
                dbEntity.ModifiedDate = _AppraisalForm.ModifiedDate;
                dbEntity.TargetName = _AppraisalForm.TargetName;
                dbEntity.TargetDescription = _AppraisalForm.TargetDescription;
                dbEntity.HideTarget = _AppraisalForm.HideTarget;
                dbEntity.TargetRatingScaleRef_ID = _AppraisalForm.TargetRatingScaleRef_ID;
                dbEntity.AllowTargetSelfRating = _AppraisalForm.AllowTargetSelfRating;
                dbEntity.AllowTargetSelfComment = _AppraisalForm.AllowTargetSelfComment;
                dbEntity.TargetHideAssignedShowRating = _AppraisalForm.TargetHideAssignedShowRating;
            }


            //update for AppraisalFormCompetency
            List<AppraisalFormTarget> dbEntityAppraisalFormCompetency = PayrollDataContext.AppraisalFormTargets.Where(x => x.AppraisalFormRef_ID == _AppraisalForm.AppraisalFormID).ToList();
            if (dbEntity != null)
            {
                PayrollDataContext.AppraisalFormTargets.DeleteAllOnSubmit(dbEntityAppraisalFormCompetency);
                //PayrollDataContext.SubmitChanges();
            }

            if (_listAppraisalFormCompetency.Any())
            {
                foreach (AppraisalFormTarget item in _listAppraisalFormCompetency)
                {
                    item.AppraisalFormRef_ID = _AppraisalForm.AppraisalFormID;
                }
                PayrollDataContext.AppraisalFormTargets.InsertAllOnSubmit(_listAppraisalFormCompetency);
            }

            PayrollDataContext.SubmitChanges();
            status.IsSuccess = true;
            return status;
        }

        public static AppraisalForm GetAppraisal_GeneralInfoByID(int ID)
        {
            return PayrollDataContext.AppraisalForms.SingleOrDefault(x => x.AppraisalFormID == ID);
        }


        public static List<AppraisalFormCompetency> GetAppraisalFromCompetency_ByID(int ID)
        {
            return PayrollDataContext.AppraisalFormCompetencies.Where(x => x.AppraisalFormRef_ID == ID).ToList();
        }

        public static List<AppraisalFormTarget> GetAppraisalFromTargets(int ID)
        {
            return PayrollDataContext.AppraisalFormTargets.Where(x => x.AppraisalFormRef_ID == ID).OrderBy(x => x.Sequence).ToList();
        }

        public static List<AppraisalFormTarget> GetAppraisalFromTargetsForRollOut(int formId, int empId, DateTime start, DateTime end)
        {
            AppraisalForm form = AppraisalManager.getFormInstanceByID(formId);

            List<AppraisalFormTarget> list =
                PayrollDataContext.AppraisalFormTargets.Where(x => x.AppraisalFormRef_ID == formId).OrderBy(x => x.Sequence).ToList();

            if (form.TargetHideAssignedShowRating != null && form.TargetHideAssignedShowRating.Value)
            {
                List<AppraisalEmployeeTargetOtherDetail> values =
                    PayrollDataContext.AppraisalEmployeeTargetOtherDetails.Where(x => x.FormRef_ID == formId && x.EmployeeId == empId).ToList();

                foreach (var item in list)
                {
                    item.NewWeight = item.Weight == null ? 0 : item.Weight.Value;
                    AppraisalEmployeeTargetOtherDetail target = values.FirstOrDefault(x => x.TargetRef_ID == item.TargetRef_ID);
                    if (target != null)
                    {
                        if (target.Weight != 0)
                            item.NewWeight = target.Weight;
                        item.TargetTextValue = target.AssignedTargetTextValue;
                        item.AchievementTextValue = target.AssignedTargetTextValue;
                    }
                }
                return list.ToList();
            }
            else
            {
                List<GetEmployeeTargetForDateResult> targets = PayrollDataContext.GetEmployeeTargetForDate
                    (start, end, null, empId).Where(x => x.AssignedTarget != null && x.AssignedTarget != 0).ToList();
                //.Where(x => x.EmployeeId == empId).ToList();

                foreach (AppraisalFormTarget item in list)
                {
                    GetEmployeeTargetForDateResult target = targets.FirstOrDefault(x => x.TargetId == item.TargetRef_ID);
                    if (target != null)
                        item.DefaultTargetValue = target.AssignedTarget == null ? 0 : target.AssignedTarget.Value;
                    if (target != null)
                        item.Achievement = target.Achievement == null ? 0 : target.Achievement.Value;
                }
                return list.Where(x => x.DefaultTargetValue != 0).ToList();
            }
        }

        public static string GetTargetName(int target)
        {
            return PayrollDataContext.AppraisalTargets.FirstOrDefault(x => x.TargetID == target).Name;
        }


        public static List<AppraisalCategory> GetAppraisalFromCompetencyCategoryFormatted(int ID)
        {

            List<AppraisalFormCompetencyCategory> dbList = PayrollDataContext
                .AppraisalFormCompetencyCategories.Where(x => x.AppraisalFormRef_ID == ID).OrderBy(x => x.Sequence).ToList();

            List<AppraisalCategory> list = new List<AppraisalCategory>();
            List<AppraisalCompetency> _AppraisalCompetency = NewHRManager.GetAllAppraisalCompetency();
            int j = 0;
            foreach (AppraisalFormCompetencyCategory dbItem in dbList)
            {
                string divid = "'" + "a" + j.ToString() + "'";
                string TextWithHTML = "<div onclick=toggle_visibility(" + divid + ") unselectable='on'" +
                "style='text-align:left;'><div id=" + "arrow" + j + " style='width:0px;float:left;color:#2B652A'>►</div><img src='data:image/gif;base64,R0lGODlhAQABAID/AMDAwAAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==' class=' x-tree-elbow-img x-tree-elbow-plus x-tree-expander'><img src='data:image/gif;base64,R0lGODlhAQABAID/AMDAwAAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==' class=' x-tree-icon x-tree-icon-parent '><span class='x-tree-node-text '>" + dbItem.Name + "</span></div>";
                string ChildHtmlItem = "<div style='display:none' id=" + "a" + j + "><ul style='list-style-type: none;'>";
                List<AppraisalCompetency> _AppraisalCompetencyList = _AppraisalCompetency.Where(x => x.CategoryID == dbItem.CompetencyCategoryID).ToList();
                foreach (var item in _AppraisalCompetencyList)
                {
                    ChildHtmlItem += "<li><img src='data:image/gif;base64,R0lGODlhAQABAID/AMDAwAAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==' class='x-tree-icon x-tree-icon-leaf '><span class='x-tree-node-text ' style='line-height:20px'>" + item.Name + "</span></li>";
                }
                ChildHtmlItem += "</ul></div>";
                TextWithHTML = TextWithHTML + ChildHtmlItem;
                list.Add(new AppraisalCategory { CategoryID = dbItem.CompetencyCategoryID, Name = dbItem.Name, FormattedName = TextWithHTML, Weight = dbItem.Weight });
                j++;
            }
            return list;
        }

        public static List<AppraisalCategory> GetAppraisalFromCompetencyCategory(int ID)
        {

            List<AppraisalFormCompetencyCategory> dbList = PayrollDataContext
                .AppraisalFormCompetencyCategories.Where(x => x.AppraisalFormRef_ID == ID).OrderBy(x => x.Sequence).ToList();

            List<AppraisalCategory> list = new List<AppraisalCategory>();

            foreach (AppraisalFormCompetencyCategory dbItem in dbList)
            {
                list.Add(new AppraisalCategory { CategoryID = dbItem.CompetencyCategoryID, Name = dbItem.Name, Weight = dbItem.Weight });
            }

            //string text = lines[i].Label + " (" + lines[i].Score.ToString() + ")";
            //ddl.Items.Add(new System.Web.UI.WebControls.ListItem { Text = text, Value = lines[i].Score.ToString() });


            //var list =
            //    (
            //    from c in PayrollDataContext.AppraisalCategories
            //    join fc in PayrollDataContext.AppraisalFormCompetencies on c.CategoryID equals fc.CompetencyCatRef_ID
            //    orderby fc.Sequence
            //    select new
            //    {
            //        CategoryID = c.CategoryID,
            //        Name = c.Name
            //    }

            //    ).ToList();



            //var finalList = list.Distinct().ToList();


            //List<AppraisalCategory> newlist = new List<AppraisalCategory>();

            //foreach (var item in finalList)
            //{
            //    newlist.Add(new AppraisalCategory { CategoryID = item.CategoryID ,Name = item.Name }); ;
            //}

            return list;
            //return PayrollDataContext.AppraisalFormCompetencies.Where(x => x.AppraisalFormRef_ID == ID).ToList();
        }
        public static List<AppraisalCompetency> GetAllAppraisalCompetency()
        {
            return PayrollDataContext.AppraisalCompetencies.ToList();

        }
        public static List<AppraisalCategory> GetAllAppraisalCompetencyCategory()
        {
            return PayrollDataContext.AppraisalCategories.ToList();

        }

        public static List<AppraisalTarget> GetAllAppraisalTargets()
        {
            return PayrollDataContext.AppraisalTargets.ToList();

        }
        public static List<AppraisalRatingScale> GetAllRatingScale()
        {
            return PayrollDataContext.AppraisalRatingScales.ToList();

        }

        public static List<AppraisalRatingScaleLine> GetAppraisalRatingScaleLinesByScaleID(int RatingScaleRef_ID)
        {
            return PayrollDataContext.AppraisalRatingScaleLines.Where(x => x.RatingScaleRefID == RatingScaleRef_ID).ToList();
        }



        public static List<GetAppraisalListAdminSPResult> GetALLAppraisalList(int start, int pagesize)
        {
            List<GetAppraisalListAdminSPResult> list = PayrollDataContext.GetAppraisalListAdminSP(start, pagesize).ToList();

            foreach (GetAppraisalListAdminSPResult item in list)
            {
                int times = PayrollDataContext.AppraisalRollouts.Count(x => x.FormID == item.AppraisalFormID);

                if (times != 0)
                {
                    item.RollOutTimes = times + " times";
                }

                item.TotalReview = PayrollDataContext.AppraisalFormReviews.Count(x => x.AppraisalFormRef_ID == item.AppraisalFormID);
                item.TotalCompetency = PayrollDataContext.AppraisalFormCompetencies.Count(x => x.AppraisalFormRef_ID == item.AppraisalFormID);
                item.TotalQuestion = PayrollDataContext.AppraisalFormQuestionnaires.Count(x => x.AppraisalFormRef_ID == item.AppraisalFormID);
            }

            return list;
        }


        public static bool DeleteAppraisalByFormID(int FormID, out Boolean isInUse)
        {

            isInUse = false;
            if (PayrollDataContext.AppraisalEmployeeForms.Any(c => c.AppraisalFormRef_ID == FormID))
                return false;

            AppraisalForm _AppraisalForm = PayrollDataContext.AppraisalForms.SingleOrDefault(x => x.AppraisalFormID == FormID);
            PayrollDataContext.AppraisalForms.DeleteOnSubmit(_AppraisalForm);
            return DeleteChangeSet();


        }

        public static bool IsAppraisalCompetencyUsedInEmployee(int FormID)
        {
            var list =
            (
            from se in PayrollDataContext.AppraisalEmployeeForms
            join s in PayrollDataContext.AppraisalEmployeeFormCompetencies on se.AppraisalEmployeeFormID equals s.AppraisalEmployeeFormRef_ID
            where se.AppraisalFormRef_ID == FormID
            select new
            {
                AppraisalFormRef_ID = se.AppraisalFormRef_ID,
            }
            ).ToList();

            if (list.Any())
                return true;
            else
                return false;

        }
        public static bool IsAppraisalTargetUsedInEmployee(int FormID)
        {
            var list =
            (
            from se in PayrollDataContext.AppraisalEmployeeForms
            join s in PayrollDataContext.AppraisalEmployeeFormTargets on se.AppraisalEmployeeFormID equals s.AppraisalEmployeeFormRef_ID
            where se.AppraisalFormRef_ID == FormID
            select new
            {
                AppraisalFormRef_ID = se.AppraisalFormRef_ID,
            }
            ).ToList();

            if (list.Any())
                return true;
            else
                return false;

        }
        public static bool IsAppraisalReviewUsedInEmployee(int FormID)
        {
            var list =
            (
            from se in PayrollDataContext.AppraisalEmployeeForms
            join s in PayrollDataContext.AppraisalEmployeeFormReviews on se.AppraisalEmployeeFormID equals s.AppraisalEmployeeFormRef_ID
            where se.AppraisalFormRef_ID == FormID
            select new
            {
                AppraisalFormRef_ID = se.AppraisalFormRef_ID,
            }
            ).ToList();

            if (list.Any())
                return true;
            else
                return false;

        }

        public static bool IsAppraisalQuestionnariesUsedInEmployee(int FormID)
        {
            var list =
            (
            from se in PayrollDataContext.AppraisalEmployeeForms
            join s in PayrollDataContext.AppraisalEmployeeFormQuestionnaires on se.AppraisalEmployeeFormID equals s.AppraisalEmployeeFormRef_ID
            where se.AppraisalFormRef_ID == FormID
            select new
            {
                AppraisalFormRef_ID = se.AppraisalFormRef_ID,
            }
            ).ToList();

            if (list.Any())
                return true;
            else
                return false;

        }



        public static void ChangeReviewQuestionOrder(int sourceId, int destId, string mode, int formId)
        {
            AppraisalFormReview dbSource = PayrollDataContext.AppraisalFormReviews.SingleOrDefault(x => x.QuestionnareID == sourceId);
            AppraisalFormReview dbDest = PayrollDataContext.AppraisalFormReviews.SingleOrDefault(x => x.QuestionnareID == destId);
            bool isDropBefore = mode.ToLower().Trim().Contains("before") ? true : false;
            int? order = null;
            if (isDropBefore)
            {

                // now change order of other elements after dest
                List<AppraisalFormReview> otherList = PayrollDataContext.AppraisalFormReviews
                    .Where(x => x.AppraisalFormRef_ID == formId &&
                        x.Sequence > dbDest.Sequence).OrderBy(x => x.Sequence).ToList();

                order = dbDest.Sequence;
                dbSource.Sequence = dbDest.Sequence;
                dbDest.Sequence = ++order;

                foreach (AppraisalFormReview item in otherList)
                {
                    if (item.QuestionnareID != dbSource.QuestionnareID)
                        item.Sequence = ++order;
                }


            }
            else
            {
                // now change order of other elements after dest
                List<AppraisalFormReview> otherList = PayrollDataContext.AppraisalFormReviews
                    .Where(x => x.AppraisalFormRef_ID == formId &&
                        x.Sequence > dbDest.Sequence).OrderBy(x => x.Sequence).ToList();

                order = dbDest.Sequence;
                dbSource.Sequence = ++order;

                foreach (AppraisalFormReview item in otherList)
                {
                    if (item.QuestionnareID != dbSource.QuestionnareID)
                        item.Sequence = ++order;
                }
            }

            PayrollDataContext.SubmitChanges();

            order = 1;
            List<AppraisalFormReview> allList = PayrollDataContext.AppraisalFormReviews
                .Where(x => x.AppraisalFormRef_ID == formId).OrderBy(x => x.Sequence).ToList();
            foreach (AppraisalFormReview item in allList)
            {
                item.Sequence = order++;
            }

            PayrollDataContext.SubmitChanges();
        }


        public static void ChangeQuestionnareOrder(int sourceId, int destId, string mode, int CategoryID)
        {
            AppraisalFormQuestionnaire dbSource = PayrollDataContext.AppraisalFormQuestionnaires.SingleOrDefault(x => x.QuestionnareID == sourceId && x.CategoryID == CategoryID);
            AppraisalFormQuestionnaire dbDest = PayrollDataContext.AppraisalFormQuestionnaires.SingleOrDefault(x => x.QuestionnareID == destId && x.CategoryID == CategoryID);
            bool isDropBefore = mode.ToLower().Trim().Contains("before") ? true : false;
            int order = 0;
            if (isDropBefore)
            {
                if (dbSource != null && dbDest != null)
                {
                    // now change order of other elements after dest
                    List<AppraisalFormQuestionnaire> otherList = PayrollDataContext.AppraisalFormQuestionnaires
                        .Where(x => x.Sequence > dbDest.Sequence && x.CategoryID == CategoryID).OrderBy(x => x.Sequence).ToList();

                    order = dbDest.Sequence;
                    dbSource.Sequence = dbDest.Sequence;
                    dbDest.Sequence = ++order;

                    foreach (AppraisalFormQuestionnaire item in otherList)
                    {
                        if (item.QuestionnareID != dbSource.QuestionnareID)
                            item.Sequence = ++order;
                    }

                }
            }
            else
            {
                if (dbSource != null && dbDest != null)
                {
                    // now change order of other elements after dest
                    List<AppraisalFormQuestionnaire> otherList = PayrollDataContext.AppraisalFormQuestionnaires
                        .Where(x => x.Sequence > dbDest.Sequence && x.CategoryID == CategoryID).OrderBy(x => x.Sequence).ToList();

                    order = dbDest.Sequence;
                    dbSource.Sequence = ++order;

                    foreach (AppraisalFormQuestionnaire item in otherList)
                    {
                        if (item.QuestionnareID != dbSource.QuestionnareID)
                            item.Sequence = ++order;
                    }
                }
            }

            PayrollDataContext.SubmitChanges();

            order = 1;
            List<AppraisalFormQuestionnaire> allList = PayrollDataContext.AppraisalFormQuestionnaires.Where(x => x.CategoryID == CategoryID).OrderBy(t => t.Sequence).ToList();
            foreach (AppraisalFormQuestionnaire item in allList)
            {
                item.Sequence = order++;
            }

            PayrollDataContext.SubmitChanges();
        }


        #endregion

        public static int IsHrEditable(int Status)
        {
            if (SessionManager.CurrentLoggedInEmployeeId == 0)
                return 1;

            if (Status == 1)
                return 0;
            else
                return 1;
        }

        public static List<SelectAll_EmployeeProfileChangeResult> GetAllEmpProfileChangeList()
        {
            return PayrollDataContext.SelectAll_EmployeeProfileChange().ToList();
        }

        public static Status ApproveEmployeeProfileChange(List<SelectAll_EmployeeProfileChangeResult> list)
        {
            Status status = new Status();

            SetConnectionPwd(PayrollDataContext.Connection);
            PayrollDataContext.Transaction = PayrollDataContext.Connection.BeginTransaction();

            try
            {
                foreach (var item in list)
                {
                    if (item.Tbl == "EAddress")
                    {
                        TempEAddress objTempEAddress = PayrollDataContext.TempEAddresses.SingleOrDefault(x => x.AddressId == item.Id && x.EmployeeId == item.EmployeeId);
                        objTempEAddress.Status = 1;

                        EEmployee updateInstance = new EEmployee();
                        updateInstance = PayrollDataContext.EEmployees.Where(x => x.EmployeeId == item.EmployeeId).FirstOrDefault();

                        updateInstance.Modified = CustomDate.GetTodayDate(true).EnglishDate;


                        EAddress dbObjEAddress = PayrollDataContext.EAddresses.SingleOrDefault(x => x.EmployeeId == item.EmployeeId);
                        if (dbObjEAddress != null)
                        {
                            dbObjEAddress.PECountryId = objTempEAddress.PECountryId;

                            dbObjEAddress.PEZoneId = objTempEAddress.PEZoneId;

                            dbObjEAddress.PEDistrictId = objTempEAddress.PEDistrictId;

                            dbObjEAddress.PEVDCMuncipality = objTempEAddress.PEVDCMuncipality;
                            dbObjEAddress.PEStreet = objTempEAddress.PEStreet;
                            dbObjEAddress.PEWardNo = objTempEAddress.PEWardNo;
                            dbObjEAddress.PEHouseNo = objTempEAddress.PEHouseNo;
                            dbObjEAddress.PEState = objTempEAddress.PEState;
                            dbObjEAddress.PEZipCode = objTempEAddress.PEZipCode;
                            dbObjEAddress.PELocality = objTempEAddress.PELocality;

                            dbObjEAddress.PSCountryId = objTempEAddress.PSCountryId;

                            dbObjEAddress.PSZoneId = objTempEAddress.PSZoneId;

                            dbObjEAddress.PSDistrictId = objTempEAddress.PSDistrictId;

                            dbObjEAddress.PSVDCMuncipality = objTempEAddress.PSVDCMuncipality;
                            dbObjEAddress.PSStreet = objTempEAddress.PSStreet;
                            dbObjEAddress.PSWardNo = objTempEAddress.PSWardNo;
                            dbObjEAddress.PSHouseNo = objTempEAddress.PSHouseNo;
                            dbObjEAddress.PSState = objTempEAddress.PSState;
                            dbObjEAddress.PSZipCode = objTempEAddress.PSZipCode;
                            dbObjEAddress.PSLocality = objTempEAddress.PSLocality;
                            dbObjEAddress.PSCitIssDis = objTempEAddress.PSCitIssDis;

                            dbObjEAddress.CIPhoneNo = objTempEAddress.CIPhoneNo;
                            dbObjEAddress.Extension = objTempEAddress.Extension;
                            dbObjEAddress.CIMobileNo = objTempEAddress.CIMobileNo;
                            dbObjEAddress.PersonalEmail = objTempEAddress.PersonalEmail;
                            dbObjEAddress.PersonalMobile = objTempEAddress.PersonalMobile;
                            dbObjEAddress.PersonalPhone = objTempEAddress.PersonalPhone;
                            dbObjEAddress.EmergencyRelation = objTempEAddress.EmergencyRelation;
                            dbObjEAddress.EmergencyName = objTempEAddress.EmergencyName;
                            dbObjEAddress.EmergencyPhone = objTempEAddress.EmergencyPhone;
                            dbObjEAddress.EmergencyMobile = objTempEAddress.EmergencyMobile;
                            dbObjEAddress.ModifiedBy = SessionManager.CurrentLoggedInUserID;
                            dbObjEAddress.ModifiedOn = DateTime.Now;

                            dbObjEAddress.ApprovedBy = SessionManager.CurrentLoggedInUserID;
                            dbObjEAddress.ApprovedOn = BLL.BaseBiz.GetCurrentDateAndTime();
                            dbObjEAddress.Status = (int)HRStatusEnum.Approved;
                        }
                        else
                        {
                            EAddress objEAddress = new EAddress();

                            objEAddress.PECountryId = objTempEAddress.PECountryId;

                            objEAddress.PEZoneId = objTempEAddress.PEZoneId;

                            objEAddress.PEDistrictId = objTempEAddress.PEDistrictId;

                            objEAddress.PEVDCMuncipality = objTempEAddress.PEVDCMuncipality;
                            objEAddress.PEStreet = objTempEAddress.PEStreet;
                            objEAddress.PEWardNo = objTempEAddress.PEWardNo;
                            objEAddress.PEHouseNo = objTempEAddress.PEHouseNo;
                            objEAddress.PEState = objTempEAddress.PEState;
                            objEAddress.PEZipCode = objTempEAddress.PEZipCode;
                            objEAddress.PELocality = objTempEAddress.PELocality;

                            objEAddress.PSCountryId = objTempEAddress.PSCountryId;

                            objEAddress.PSZoneId = objTempEAddress.PSZoneId;

                            objEAddress.PSDistrictId = objTempEAddress.PSDistrictId;

                            objEAddress.PSVDCMuncipality = objTempEAddress.PSVDCMuncipality;
                            objEAddress.PSStreet = objTempEAddress.PSStreet;
                            objEAddress.PSWardNo = objTempEAddress.PSWardNo;
                            objEAddress.PSHouseNo = objTempEAddress.PSHouseNo;
                            objEAddress.PSState = objTempEAddress.PSState;
                            objEAddress.PSZipCode = objTempEAddress.PSZipCode;
                            objEAddress.PSLocality = objTempEAddress.PSLocality;
                            objEAddress.PSCitIssDis = objTempEAddress.PSCitIssDis;

                            objEAddress.CIPhoneNo = objTempEAddress.CIPhoneNo;
                            objEAddress.Extension = objTempEAddress.Extension;
                            objEAddress.CIMobileNo = objTempEAddress.CIMobileNo;
                            objEAddress.PersonalEmail = objTempEAddress.PersonalEmail;
                            objEAddress.PersonalMobile = objTempEAddress.PersonalMobile;
                            objEAddress.PersonalPhone = objTempEAddress.PersonalPhone;
                            objEAddress.EmergencyRelation = objTempEAddress.EmergencyRelation;
                            objEAddress.EmergencyName = objTempEAddress.EmergencyName;
                            objEAddress.EmergencyPhone = objTempEAddress.EmergencyPhone;
                            objEAddress.EmergencyMobile = objTempEAddress.EmergencyMobile;
                            objEAddress.ModifiedBy = SessionManager.CurrentLoggedInUserID;
                            objEAddress.ModifiedOn = DateTime.Now;

                            objEAddress.ApprovedBy = SessionManager.CurrentLoggedInUserID;
                            objEAddress.ApprovedOn = BLL.BaseBiz.GetCurrentDateAndTime();
                            objEAddress.Status = (int)HRStatusEnum.Approved;

                            PayrollDataContext.EAddresses.InsertOnSubmit(objEAddress);
                        }



                        PayrollDataContext.SubmitChanges();
                    }
                    else if (item.Tbl == "HFamily")
                    {
                        HFamily objHFamily = PayrollDataContext.HFamilies.SingleOrDefault(x => x.FamilyId == item.Id && x.EmployeeId == item.EmployeeId);
                        objHFamily.ApprovedBy = SessionManager.CurrentLoggedInUserID;
                        objHFamily.ApprovedOn = BLL.BaseBiz.GetCurrentDateAndTime();
                        objHFamily.Status = (int)HRStatusEnum.Approved;
                        PayrollDataContext.SubmitChanges();
                    }
                    else if (item.Tbl == "HPreviousEmployment")
                    {
                        HPreviousEmployment objHPreviousEmployment = PayrollDataContext.HPreviousEmployments.SingleOrDefault(x => x.PrevEmploymentId == item.Id && x.EmployeeId == item.EmployeeId);
                        objHPreviousEmployment.ApprovedBy = SessionManager.CurrentLoggedInUserID;
                        objHPreviousEmployment.ApprovedOn = BLL.BaseBiz.GetCurrentDateAndTime();
                        objHPreviousEmployment.Status = (int)HRStatusEnum.Approved;
                        PayrollDataContext.SubmitChanges();
                    }
                    else if (item.Tbl == "HEducation")
                    {
                        HEducation objHEducation = PayrollDataContext.HEducations.SingleOrDefault(x => x.EductionId == item.Id && x.EmployeeId == item.EmployeeId);
                        objHEducation.ApprovedBy = SessionManager.CurrentLoggedInUserID;
                        objHEducation.ApprovedOn = BLL.BaseBiz.GetCurrentDateAndTime();
                        objHEducation.Status = (int)HRStatusEnum.Approved;
                        PayrollDataContext.SubmitChanges();
                    }
                    else if (item.Tbl == "HTraining")
                    {
                        HTraining objHTraining = PayrollDataContext.HTrainings.SingleOrDefault(x => x.TrainingId == item.Id && x.EmployeeId == item.EmployeeId);
                        objHTraining.ApprovedBy = SessionManager.CurrentLoggedInUserID;
                        objHTraining.ApprovedOn = BLL.BaseBiz.GetCurrentDateAndTime();
                        objHTraining.Status = (int)HRStatusEnum.Approved;
                        PayrollDataContext.SubmitChanges();
                    }
                    else if (item.Tbl == "HSeminar")
                    {
                        HSeminar objHSeminar = PayrollDataContext.HSeminars.SingleOrDefault(x => x.SeminarId == item.Id && x.EmployeeId == item.EmployeeId);
                        objHSeminar.ApprovedBy = SessionManager.CurrentLoggedInUserID;
                        objHSeminar.ApprovedOn = BLL.BaseBiz.GetCurrentDateAndTime();
                        objHSeminar.Status = (int)HRStatusEnum.Approved;
                        PayrollDataContext.SubmitChanges();
                    }
                    else if (item.Tbl == "SkillSetEmployee")
                    {
                        SkillSetEmployee objSkillSetEmployee = PayrollDataContext.SkillSetEmployees.SingleOrDefault(x => x.SkillSetId == item.Id && x.EmployeeId == item.EmployeeId); ;
                        objSkillSetEmployee.ApprovedBy = SessionManager.CurrentLoggedInUserID;
                        objSkillSetEmployee.ApprovedOn = BLL.BaseBiz.GetCurrentDateAndTime();
                        objSkillSetEmployee.Status = (int)HRStatusEnum.Approved;
                        PayrollDataContext.SubmitChanges();
                    }
                    else if (item.Tbl == "LanguageSetEmployee")
                    {
                        LanguageSetEmployee objLanguageSetEmployee = PayrollDataContext.LanguageSetEmployees.SingleOrDefault(x => x.LanguageSetId == item.Id && x.EmployeeId == item.EmployeeId);
                        objLanguageSetEmployee.ApprovedBy = SessionManager.CurrentLoggedInUserID;
                        objLanguageSetEmployee.ApprovedOn = BLL.BaseBiz.GetCurrentDateAndTime();
                        objLanguageSetEmployee.Status = (int)HRStatusEnum.Approved;
                        PayrollDataContext.SubmitChanges();
                    }
                    else if (item.Tbl == "HPublication")
                    {
                        HPublication objHPublication = PayrollDataContext.HPublications.SingleOrDefault(x => x.PublicationId == item.Id && x.EmployeeId == item.EmployeeId);
                        objHPublication.ApprovedBy = SessionManager.CurrentLoggedInUserID;
                        objHPublication.ApprovedOn = BLL.BaseBiz.GetCurrentDateAndTime();
                        objHPublication.Status = (int)HRStatusEnum.Approved;
                        PayrollDataContext.SubmitChanges();
                    }
                    else if (item.Tbl == "HCitizenship")
                    {
                        HCitizenship objHCitizenship = PayrollDataContext.HCitizenships.SingleOrDefault(x => x.CitizenshipId == item.Id && x.EmployeeId == item.EmployeeId);
                        objHCitizenship.ApprovedBy = SessionManager.CurrentLoggedInUserID;
                        objHCitizenship.ApprovedOn = BLL.BaseBiz.GetCurrentDateAndTime();
                        objHCitizenship.Status = (int)HRStatusEnum.Approved;
                        PayrollDataContext.SubmitChanges();
                    }
                    else if (item.Tbl == "HDrivingLicence")
                    {
                        HDrivingLicence objHDrivingLicence = PayrollDataContext.HDrivingLicences.SingleOrDefault(x => x.DrivingLicenceId == item.Id && x.EmployeeId == item.EmployeeId);
                        objHDrivingLicence.ApprovedBy = SessionManager.CurrentLoggedInUserID;
                        objHDrivingLicence.ApprovedOn = BLL.BaseBiz.GetCurrentDateAndTime();
                        objHDrivingLicence.Status = (int)HRStatusEnum.Approved;
                        PayrollDataContext.SubmitChanges();
                    }
                    else if (item.Tbl == "HPassport")
                    {
                        HPassport objHPassport = PayrollDataContext.HPassports.SingleOrDefault(x => x.PassportId == item.Id && x.EmployeeId == item.EmployeeId);
                        objHPassport.ApprovedBy = SessionManager.CurrentLoggedInUserID;
                        objHPassport.ApprovedOn = BLL.BaseBiz.GetCurrentDateAndTime();
                        objHPassport.Status = (int)HRStatusEnum.Approved;
                        PayrollDataContext.SubmitChanges();
                    }
                    else if (item.Tbl == "HDocument")
                    {
                        HDocument objHDocument = PayrollDataContext.HDocuments.SingleOrDefault(x => x.DocumentId == item.Id && x.EmployeeId == item.EmployeeId);
                        objHDocument.ApprovedBy = SessionManager.CurrentLoggedInUserID;
                        objHDocument.ApprovedOn = BLL.BaseBiz.GetCurrentDateAndTime();
                        objHDocument.Status = (int)HRStatusEnum.Approved;
                        PayrollDataContext.SubmitChanges();
                    }
                }

                status.IsSuccess = true;
                PayrollDataContext.Transaction.Commit();
            }
            catch (Exception exp)
            {
                Log.log("Error while approving employee profile changes", exp);
                PayrollDataContext.Transaction.Rollback();
                status.IsSuccess = false;
            }
            finally
            {

            }

            return status;
        }


        public static Status ApproveEmpProfileChange(List<int> IdList)
        {
            Status status = new Status();
            string tblName = "";
            int tblId = 0;


            SetConnectionPwd(PayrollDataContext.Connection);
            PayrollDataContext.Transaction = PayrollDataContext.Connection.BeginTransaction();

            try
            {
                List<SelectAll_EmployeeProfileChangeResult> list = PayrollDataContext.SelectAll_EmployeeProfileChange().ToList();

                foreach (var item in list)
                {
                    if (IdList.Contains(int.Parse(item.SN.ToString())))
                    {
                        foreach (var listId in IdList)
                        {
                            if (listId == int.Parse(item.SN.ToString()))
                            {
                                tblName = item.Tbl;
                                tblId = item.Id;

                                if (tblName == "EAddress")
                                {
                                    EAddress objEAddress = PayrollDataContext.EAddresses.SingleOrDefault(x => x.AddressId == tblId);
                                    objEAddress.ApprovedBy = SessionManager.CurrentLoggedInUserID;
                                    objEAddress.ApprovedOn = BLL.BaseBiz.GetCurrentDateAndTime();
                                    objEAddress.Status = (int)HRStatusEnum.Approved;
                                    PayrollDataContext.SubmitChanges();
                                }
                                else if (tblName == "HFamily")
                                {
                                    HFamily objHFamily = PayrollDataContext.HFamilies.SingleOrDefault(x => x.FamilyId == tblId);
                                    objHFamily.ApprovedBy = SessionManager.CurrentLoggedInUserID;
                                    objHFamily.ApprovedOn = BLL.BaseBiz.GetCurrentDateAndTime();
                                    objHFamily.Status = (int)HRStatusEnum.Approved;
                                    PayrollDataContext.SubmitChanges();
                                }
                                else if (tblName == "HPreviousEmployment")
                                {
                                    HPreviousEmployment objHPreviousEmployment = PayrollDataContext.HPreviousEmployments.SingleOrDefault(x => x.PrevEmploymentId == tblId);
                                    objHPreviousEmployment.ApprovedBy = SessionManager.CurrentLoggedInUserID;
                                    objHPreviousEmployment.ApprovedOn = BLL.BaseBiz.GetCurrentDateAndTime();
                                    objHPreviousEmployment.Status = (int)HRStatusEnum.Approved;
                                    PayrollDataContext.SubmitChanges();
                                }
                                else if (tblName == "HEducation")
                                {
                                    HEducation objHEducation = PayrollDataContext.HEducations.SingleOrDefault(x => x.EductionId == tblId);
                                    objHEducation.ApprovedBy = SessionManager.CurrentLoggedInUserID;
                                    objHEducation.ApprovedOn = BLL.BaseBiz.GetCurrentDateAndTime();
                                    objHEducation.Status = (int)HRStatusEnum.Approved;
                                    PayrollDataContext.SubmitChanges();
                                }
                                else if (tblName == "HTraining")
                                {
                                    HTraining objHTraining = PayrollDataContext.HTrainings.SingleOrDefault(x => x.TrainingId == tblId);
                                    objHTraining.ApprovedBy = SessionManager.CurrentLoggedInUserID;
                                    objHTraining.ApprovedOn = BLL.BaseBiz.GetCurrentDateAndTime();
                                    objHTraining.Status = (int)HRStatusEnum.Approved;
                                    PayrollDataContext.SubmitChanges();
                                }
                                else if (tblName == "HSeminar")
                                {
                                    HSeminar objHSeminar = PayrollDataContext.HSeminars.SingleOrDefault(x => x.SeminarId == tblId);
                                    objHSeminar.ApprovedBy = SessionManager.CurrentLoggedInUserID;
                                    objHSeminar.ApprovedOn = BLL.BaseBiz.GetCurrentDateAndTime();
                                    objHSeminar.Status = (int)HRStatusEnum.Approved;
                                    PayrollDataContext.SubmitChanges();
                                }
                                else if (tblName == "SkillSetEmployee")
                                {
                                    SkillSetEmployee objSkillSetEmployee = PayrollDataContext.SkillSetEmployees.SingleOrDefault(x => x.SkillSetId == tblId && x.EmployeeId == item.EmployeeId);
                                    objSkillSetEmployee.ApprovedBy = SessionManager.CurrentLoggedInUserID;
                                    objSkillSetEmployee.ApprovedOn = BLL.BaseBiz.GetCurrentDateAndTime();
                                    objSkillSetEmployee.Status = (int)HRStatusEnum.Approved;
                                    PayrollDataContext.SubmitChanges();
                                }
                                else if (tblName == "LanguageSetEmployee")
                                {
                                    LanguageSetEmployee objLanguageSetEmployee = PayrollDataContext.LanguageSetEmployees.SingleOrDefault(x => x.LanguageSetId == tblId && x.EmployeeId == item.EmployeeId);
                                    objLanguageSetEmployee.ApprovedBy = SessionManager.CurrentLoggedInUserID;
                                    objLanguageSetEmployee.ApprovedOn = BLL.BaseBiz.GetCurrentDateAndTime();
                                    objLanguageSetEmployee.Status = (int)HRStatusEnum.Approved;
                                    PayrollDataContext.SubmitChanges();
                                }
                                else if (tblName == "HPublication")
                                {
                                    HPublication objHPublication = PayrollDataContext.HPublications.SingleOrDefault(x => x.PublicationId == tblId);
                                    objHPublication.ApprovedBy = SessionManager.CurrentLoggedInUserID;
                                    objHPublication.ApprovedOn = BLL.BaseBiz.GetCurrentDateAndTime();
                                    objHPublication.Status = (int)HRStatusEnum.Approved;
                                    PayrollDataContext.SubmitChanges();
                                }
                            }
                        }
                    }
                }

                status.IsSuccess = true;
                PayrollDataContext.Transaction.Commit();
            }
            catch (Exception exp)
            {
                Log.log("Error while approving employee profile changes", exp);
                PayrollDataContext.Transaction.Rollback();
                status.IsSuccess = false;
            }
            finally
            {

            }
            return status;
        }

        public static Status SaveUpdateEmployeePhotograph(int employeeId, string urlPhoto, string urlPhotoThumbnail)
        {
            Status status = new Status();

            HHumanResource obj = PayrollDataContext.HHumanResources.SingleOrDefault(x => x.EmployeeId == employeeId);
            string oldFileUrl = "";
            oldFileUrl = obj.tempUrlPhoto;

            obj.tempUrlPhoto = urlPhoto;
            obj.tempUrlPhotoThumbnail = urlPhotoThumbnail;

            try
            {
                if (!string.IsNullOrEmpty(oldFileUrl))
                {
                    string url = Path.Combine(HttpContext.Current.Server.MapPath(Config.UploadLocation), oldFileUrl);
                    File.Delete(url);
                }
            }
            catch { }

            PayrollDataContext.SubmitChanges();
            status.IsSuccess = true;

            return status;
        }

        public static List<HHumanResource> GetHHumanResourceListForPhotoApproval()
        {
            List<HHumanResource> list = PayrollDataContext.HHumanResources.Where(x => x.tempUrlPhoto != null).ToList();

            foreach (var item in list)
            {
                item.Name = NewHRManager.GetEmployeeByID(item.EmployeeId.Value).Name;
                item.imageUrl = "../Uploads/" + item.tempUrlPhoto;
            }

            return list;
        }

        public static Status ApproveEmployeePhoto(List<int> employeeIdList)
        {
            Status status = new Status();

            SetConnectionPwd(PayrollDataContext.Connection);
            PayrollDataContext.Transaction = PayrollDataContext.Connection.BeginTransaction();

            try
            {
                foreach (var id in employeeIdList)
                {
                    HHumanResource obj = PayrollDataContext.HHumanResources.SingleOrDefault(x => x.EmployeeId == id);
                    obj.UrlPhoto = obj.tempUrlPhoto;
                    obj.UrlPhotoThumbnail = obj.tempUrlPhotoThumbnail;
                    obj.tempUrlPhoto = null;
                    obj.tempUrlPhotoThumbnail = null;
                    PayrollDataContext.SubmitChanges();
                }
                status.IsSuccess = true;
                PayrollDataContext.Transaction.Commit();
            }
            catch (Exception exp)
            {
                Log.log("Error while approving employee photographs", exp);
                PayrollDataContext.Transaction.Rollback();
                status.IsSuccess = false;
            }
            finally
            {

            }
            return status;
        }

        public static List<GetOtherEmployeeListForBranchDepartmentHeadResult> GetOtherEmployeeListByEmpId(int employeeId)
        {
            return PayrollDataContext.GetOtherEmployeeListForBranchDepartmentHead(employeeId).ToList();
        }


        public static List<FixedValueEducationFaculty> GetAllEducationFaculty()
        {
            return PayrollDataContext.FixedValueEducationFaculties.OrderBy(x => x.Name).ToList();
        }
        //public static List<RetirementType> GetRetirementTypes()
        //{
        //    return PayrollDataContext.RetirementTypes.OrderBy(x => x.Name).ToList();
        //}
        public static Status SaveUpdateEducationFaculty(FixedValueEducationFaculty obj)
        {
            Status status = new Status();

            if (PayrollDataContext.FixedValueEducationFaculties.Any(x => x.Name.ToLower() == obj.Name.ToLower() && x.ID != obj.ID))
            {
                status.ErrorMessage = "Education faculty already exists.";
                status.IsSuccess = false;
                return status;
            }

            if (obj.ID.Equals(0))
            {
                PayrollDataContext.FixedValueEducationFaculties.InsertOnSubmit(obj);
                PayrollDataContext.SubmitChanges();
                status.IsSuccess = true;
            }
            else
            {
                FixedValueEducationFaculty dbEntity = PayrollDataContext.FixedValueEducationFaculties.SingleOrDefault(x => x.ID == obj.ID);
                dbEntity.Name = obj.Name;
                PayrollDataContext.SubmitChanges();
                status.IsSuccess = true;
            }

            return status;
        }

        public static Status SaveUpdateHolidayGroup(HolidayGroup obj)
        {
            Status status = new Status();

            if (PayrollDataContext.HolidayGroups.Any(x => x.Name.ToLower() == obj.Name.ToLower() && x.GroupId != obj.GroupId))
            {
                status.ErrorMessage = "Name already exists.";
                status.IsSuccess = false;
                return status;
            }

            if (obj.GroupId.Equals(0))
            {
                PayrollDataContext.HolidayGroups.InsertOnSubmit(obj);
                PayrollDataContext.SubmitChanges();
                status.IsSuccess = true;
            }
            else
            {
                HolidayGroup dbEntity = PayrollDataContext.HolidayGroups.SingleOrDefault(x => x.GroupId == obj.GroupId);
                dbEntity.Name = obj.Name;
                PayrollDataContext.SubmitChanges();
                status.IsSuccess = true;
            }

            return status;
        }
        public static Status SaveUpdateUnit(UnitList obj)
        {
            Status status = new Status();

            if (PayrollDataContext.UnitLists.Any(x => x.Name.ToLower() == obj.Name.ToLower() && x.UnitId != obj.UnitId))
            {
                status.ErrorMessage = "Name already exists.";
                status.IsSuccess = false;
                return status;
            }

            if (obj.UnitId.Equals(0))
            {
                PayrollDataContext.UnitLists.InsertOnSubmit(obj);
                PayrollDataContext.SubmitChanges();
                status.IsSuccess = true;
            }
            else
            {
                UnitList dbEntity = PayrollDataContext.UnitLists.SingleOrDefault(x => x.UnitId == obj.UnitId);
                dbEntity.Name = obj.Name;
                PayrollDataContext.SubmitChanges();
                status.IsSuccess = true;
            }

            return status;
        }

        public static Status SaveUpdateRetirementType(HR_Service_Event obj)
        {
            Status status = new Status();

            if (PayrollDataContext.HR_Service_Events.Any(x => x.Name.ToLower() == obj.Name.ToLower() && x.EventID != obj.EventID))
            {
                status.ErrorMessage = "Event type already exists.";
                status.IsSuccess = false;
                return status;
            }

            if (obj.EventID.Equals(0))
            {
                PayrollDataContext.HR_Service_Events.InsertOnSubmit(obj);
                obj.CreatedBy = SessionManager.User.UserID;
                obj.CreatedOn = GetCurrentDateAndTime();
                PayrollDataContext.SubmitChanges();
                status.IsSuccess = true;
            }
            else
            {
                HR_Service_Event dbEntity = PayrollDataContext.HR_Service_Events.SingleOrDefault(x => x.EventID == obj.EventID);

                if (dbEntity.GroupID != obj.GroupID)
                {
                    status.ErrorMessage = "Default type group can not be changed.";
                    status.IsSuccess = false;
                    return status;
                }

                if (dbEntity.Name != obj.Name)
                {
                    PayrollDataContext.ChangeSettings.InsertOnSubmit(GetSettingChangeLog((byte)MonetoryChangeLogTypeEnum.Information, "Service Event Type Name",
                        dbEntity.Name, obj.Name, LogActionEnum.Update));
                }

                dbEntity.Name = obj.Name;
                dbEntity.GroupID = obj.GroupID;
                PayrollDataContext.SubmitChanges();
                status.IsSuccess = true;
            }

            return status;
        }
        public static List<ExperienceCategory> GetAllExperienceCategory()
        {
            return PayrollDataContext.ExperienceCategories.OrderBy(x => x.Name).ToList();
        }

        public static List<GetEmployeeSearchListResult> GetEmployeeSearchList(int start, int limit, string employeeName, int EmployeeId, ref int totalRecords)
        {
            start = (start * limit);
            List<GetEmployeeSearchListResult> list = PayrollDataContext.GetEmployeeSearchList(start, limit, EmployeeId, SessionManager.CurrentCompanyId).ToList();

            totalRecords = list.Count;
            return list;
        }

        public static List<GetEmployeeSearchListResult> GetEmployeeSearchById(int employeeId)
        {
            List<GetEmployeeSearchListResult> list = new List<GetEmployeeSearchListResult>();
            EEmployee emp = PayrollDataContext.EEmployees.SingleOrDefault(x => x.EmployeeId == employeeId);
            GetEmployeeSearchListResult obj = new GetEmployeeSearchListResult();
            obj.Name = emp.Name;
            obj.Branch = PayrollDataContext.Branches.SingleOrDefault(x => x.BranchId == emp.BranchId).Name;
            obj.Department = PayrollDataContext.Departments.SingleOrDefault(x => x.DepartmentId == emp.DepartmentId).Name;
            obj.Position = PayrollDataContext.EDesignations.SingleOrDefault(x => x.DesignationId == emp.DesignationId).Name;
            obj.EmployeeId = emp.EmployeeId;
            list.Add(obj);
            return list;
        }

        public static FixedValueEducationFaculty GetFixedValueEducationFacultyById(int id)
        {
            return PayrollDataContext.FixedValueEducationFaculties.SingleOrDefault(x => x.ID == id);
        }
        public static HolidayGroup GetHolidayGroupById(int id)
        {
            return PayrollDataContext.HolidayGroups.SingleOrDefault(x => x.GroupId == id);
        }
        public static UnitList GetUnitById(int id)
        {
            return PayrollDataContext.UnitLists.SingleOrDefault(x => x.UnitId == id);
        }
        public static HR_Service_Event GetRetirementType(int id)
        {
            return PayrollDataContext.HR_Service_Events.SingleOrDefault(x => x.EventID == id);
        }
        public static Status DeleteFixedValueEducationFaculty(int id)
        {
            Status status = new Status();
            if (PayrollDataContext.HEducations.Any(x => x.FacultyID == id))
            {
                status.ErrorMessage = "Faculty is in use, cannot be deleted.";
                status.IsSuccess = false;
                return status;
            }

            FixedValueEducationFaculty dbEntity = PayrollDataContext.FixedValueEducationFaculties.SingleOrDefault(x => x.ID == id);
            PayrollDataContext.FixedValueEducationFaculties.DeleteOnSubmit(dbEntity);
            PayrollDataContext.SubmitChanges();
            status.IsSuccess = true;

            return status;
        }
        public static Status DeleteHolidayGroup(int id)
        {
            Status status = new Status();
            if (PayrollDataContext.HHumanResources.Any(x => x.HolidayGroupId == id))
            {
                status.ErrorMessage = "Group is in use, cannot be deleted.";
                status.IsSuccess = false;
                return status;
            }
            if (PayrollDataContext.HolidayLists.Any(x => x.AppliesTo == id))
            {
                status.ErrorMessage = "Group is in use, cannot be deleted.";
                status.IsSuccess = false;
                return status;
            }
            HolidayGroup dbEntity = PayrollDataContext.HolidayGroups.SingleOrDefault(x => x.GroupId == id);
            PayrollDataContext.HolidayGroups.DeleteOnSubmit(dbEntity);
            PayrollDataContext.SubmitChanges();
            status.IsSuccess = true;

            return status;
        }
        public static Status DeleteUnit(int id)
        {
            Status status = new Status();
            if (PayrollDataContext.EEmployees.Any(x => x.UnitId == id))
            {
                status.ErrorMessage = "Unit is in use, cannot be deleted.";
                status.IsSuccess = false;
                return status;
            }


            UnitList dbEntity = PayrollDataContext.UnitLists.SingleOrDefault(x => x.UnitId == id);
            PayrollDataContext.UnitLists.DeleteOnSubmit(dbEntity);
            PayrollDataContext.SubmitChanges();
            status.IsSuccess = true;

            return status;
        }
        public static Status DeleteRetirementType(int id)
        {
            Status status = new Status();
            if (PayrollDataContext.EHumanResources.Any(x => x.RetirementType == id)
                || PayrollDataContext.EmployeeServiceHistories.Any(x => x.EventID == id)
                || PayrollDataContext.PEmployeeIncrements.Any(x => x.EventID == id)
                || PayrollDataContext.BranchDepartmentHistories.Any(x => x.EventID == id)

                )
            {
                status.ErrorMessage = "Event type is in use, cannot be deleted.";
                status.IsSuccess = false;
                return status;
            }



            HR_Service_Event dbEntity = PayrollDataContext.HR_Service_Events.SingleOrDefault(x => x.EventID == id);

            if (dbEntity.CreatedBy == null)
            {
                status.ErrorMessage = "Default type cannot be deleted.";
                status.IsSuccess = false;
                return status;
            }

            PayrollDataContext.HR_Service_Events.DeleteOnSubmit(dbEntity);
            PayrollDataContext.SubmitChanges();
            status.IsSuccess = true;

            return status;
        }
        public static List<FixedValueEducationLevel> GetAddEducationLevel()
        {
            return PayrollDataContext.FixedValueEducationLevels.OrderBy(x => x.Name).ToList();
        }

        public static Status SaveUpdateFixedValueEducationLevel(FixedValueEducationLevel obj)
        {
            Status status = new Status();

            if (PayrollDataContext.FixedValueEducationLevels.Any(x => x.Name.ToLower() == obj.Name.ToLower() && x.ID != obj.ID))
            {
                status.ErrorMessage = "Education level already exists.";
                status.IsSuccess = false;
                return status;
            }

            if (obj.ID.Equals(0))
            {
                PayrollDataContext.FixedValueEducationLevels.InsertOnSubmit(obj);
                PayrollDataContext.SubmitChanges();
                status.IsSuccess = true;
            }
            else
            {
                FixedValueEducationLevel dbEntity = PayrollDataContext.FixedValueEducationLevels.SingleOrDefault(x => x.ID == obj.ID);
                dbEntity.Name = obj.Name;
                PayrollDataContext.SubmitChanges();
                status.IsSuccess = true;
            }

            return status;
        }

        public static FixedValueEducationLevel GetFixedValueEducationLevelyById(int id)
        {
            return PayrollDataContext.FixedValueEducationLevels.SingleOrDefault(x => x.ID == id);
        }

        public static Status DeleteFixedValueEducationLevel(int id)
        {
            Status status = new Status();
            if (PayrollDataContext.HEducations.Any(x => x.LevelTypeID == id))
            {
                status.ErrorMessage = "Level is in use, cannot be deleted.";
                status.IsSuccess = false;
                return status;
            }

            FixedValueEducationLevel dbEntity = PayrollDataContext.FixedValueEducationLevels.SingleOrDefault(x => x.ID == id);
            PayrollDataContext.FixedValueEducationLevels.DeleteOnSubmit(dbEntity);
            PayrollDataContext.SubmitChanges();
            status.IsSuccess = true;

            return status;
        }


        public static List<GetLocationTransfersResult> GetLocationTransfers(int currentPage, int pageSize, string searchText, int employeeId, ref int totalRecords)
        {
            List<GetLocationTransfersResult> list = PayrollDataContext.GetLocationTransfers(currentPage, pageSize, SessionManager.CurrentCompanyId, searchText, employeeId).ToList();

            totalRecords = list.Count;
            return list;
        }

        public static Status SaveUpdateELocationHistory(ELocationHistory obj)
        {
            Status status = new Status();

            EHumanResource objEHumanResource = PayrollDataContext.EHumanResources.SingleOrDefault(x => x.EmployeeId == obj.EmployeeId);

            if (objEHumanResource.LocationId == null)
            {
                status.ErrorMessage = "Save Location in Personal Details page for location transfer of the employee.";
                status.IsSuccess = false;
                return status;
            }

            ELocationHistory dbELocationHistory = PayrollDataContext.ELocationHistories.Where(x => x.EmployeeId == obj.EmployeeId).OrderByDescending(x => x.FromDateEng).FirstOrDefault();
            if (dbELocationHistory != null)
            {
                obj.FromLocationId = dbELocationHistory.LocationId;
            }
            else
            {
                obj.FromLocationId = objEHumanResource.LocationId;
            }

            if (obj.LocationHistoryId.Equals(0))
            {
                PayrollDataContext.ELocationHistories.InsertOnSubmit(obj);
                objEHumanResource.LocationId = obj.LocationId;
                PayrollDataContext.SubmitChanges();
                status.IsSuccess = true;
            }
            else
            {
                ELocationHistory dbEntity = PayrollDataContext.ELocationHistories.SingleOrDefault(x => x.LocationHistoryId == obj.LocationHistoryId);
                dbEntity.LocationId = obj.LocationId;
                dbEntity.EmployeeId = obj.EmployeeId;
                dbEntity.FromDate = obj.FromDate;
                dbEntity.FromDateEng = obj.FromDateEng;
                dbEntity.Notes = obj.Notes;


                ELocationHistory latestELocationHistory = PayrollDataContext.ELocationHistories.Where(x => x.EmployeeId == obj.EmployeeId).OrderByDescending(x => x.FromDateEng).FirstOrDefault();

                if (latestELocationHistory.LocationHistoryId == obj.LocationHistoryId)
                {
                    objEHumanResource.LocationId = obj.LocationId;
                }

                ELocationHistory nextELocationHistory = PayrollDataContext.ELocationHistories.Where(x => (x.EmployeeId == obj.EmployeeId) && (x.FromDateEng > dbEntity.FromDateEng)).OrderBy(x => x.FromDateEng).FirstOrDefault();

                if (nextELocationHistory != null)
                {
                    nextELocationHistory.FromLocationId = dbEntity.LocationId;
                }

                PayrollDataContext.SubmitChanges();
                status.IsSuccess = true;
            }

            return status;
        }

        public static ELocationHistory GetELocationHistoryById(int locationHistoryId)
        {
            return PayrollDataContext.ELocationHistories.SingleOrDefault(x => x.LocationHistoryId == locationHistoryId);
        }

        public static List<GetGradeTransfersResult> GetGradeTransfers(int currentPage, int pageSize, string searchText, ref int totalRecords)
        {
            List<GetGradeTransfersResult> list = PayrollDataContext.GetGradeTransfers(currentPage, pageSize, SessionManager.CurrentCompanyId, searchText).ToList();

            totalRecords = list.Count;
            return list;
        }

        public static List<EGrade> GetAllGrades()
        {
            return PayrollDataContext.EGrades.Where(x => x.CompanyId == SessionManager.CurrentCompanyId).OrderBy(x => x.Name).ToList();
        }

        public static EGradeHistory GetGradeHistoryById(int gradeHistoryId)
        {
            return PayrollDataContext.EGradeHistories.SingleOrDefault(x => x.GradeHistoryId == gradeHistoryId);
        }

        public static Status SaveUpdateGradeTransferHistory(EGradeHistory obj)
        {
            Status status = new Status();

            EEmployee emp = PayrollDataContext.EEmployees.SingleOrDefault(x => x.EmployeeId == obj.EmployeeId);

            if (emp.GradeId == null)
            {
                status.ErrorMessage = "Save Grade in Personal Details page for grade transfer of the employee.";
                status.IsSuccess = false;
                return status;
            }
            else
                obj.FromGradeId = emp.GradeId;

            if (obj.GradeHistoryId.Equals(0))
            {
                PayrollDataContext.EGradeHistories.InsertOnSubmit(obj);
                emp.GradeId = obj.GradeId;
                PayrollDataContext.SubmitChanges();
                status.IsSuccess = true;
            }
            else
            {
                EGradeHistory dbEntity = PayrollDataContext.EGradeHistories.SingleOrDefault(x => x.GradeHistoryId == obj.GradeHistoryId);
                dbEntity.EmployeeId = obj.EmployeeId;
                dbEntity.GradeId = obj.GradeId;
                dbEntity.FromDate = obj.FromDate;
                dbEntity.FromDateEng = obj.FromDateEng;
                dbEntity.Notes = obj.Notes;

                emp.GradeId = obj.GradeId;
                PayrollDataContext.SubmitChanges();
                status.IsSuccess = true;
            }

            return status;
        }

        public static Status InsertImportedDesignation(List<EDesignation> list)
        {

            Status status = new Status();

            PayrollDataContext.EDesignations.InsertAllOnSubmit(list);
            PayrollDataContext.SubmitChanges();


            return status;
        }

        public static List<EDesignation> GetDesignations()
        {
            List<EDesignation> list = PayrollDataContext.EDesignations.Where(x => x.CompanyId == SessionManager.CurrentCompanyId).OrderBy(x => x.Name).ToList();

            foreach (EDesignation item in list)
            {
                item.LevelName = NewPayrollManager.GetLevelById(item.LevelId.Value).Name + " - " + item.Name;
            }

            return list;
        }
        public static List<EDesignation> GetDesignationsByLevel(int levelId)
        {
            return PayrollDataContext.EDesignations
                .Where(x => x.CompanyId == SessionManager.CurrentCompanyId && x.LevelId == levelId).OrderBy(x => x.Name).ToList();
        }
        public static DesignationHistory GetDesignationHistoryById(int employeeDesignationId)
        {
            return PayrollDataContext.DesignationHistories.SingleOrDefault(x => x.EmployeeDesignationId == employeeDesignationId);
        }

        public static void SaveFirstDesignHistoryIfNotExists(int employeeId)
        {
            //// for service history dont place addition row in the middle, could save at the time of insertion as
            //// it shoud be associated with EmployeeServiceHistory
            //if (CommonManager.IsServiceHistoryEnabled == false)
            {
                if (!PayrollDataContext.DesignationHistories.Any(x => x.EmployeeId == employeeId))
                {
                    EEmployee employee = PayrollDataContext.EEmployees.SingleOrDefault(x => x.EmployeeId == employeeId);

                    DesignationHistory first = new DesignationHistory();
                    first.EmployeeId = employeeId;
                    first.DesignationId = employee.DesignationId;
                    first.FromDesignationId = employee.DesignationId;
                    first.IsFirst = true;

                    ECurrentStatus firstStatus = EmployeeManager.GetFirstStatus(employeeId);

                    first.FromDate = firstStatus.FromDate;
                    first.FromDateEng = firstStatus.FromDateEng;
                    first.IsFirst = true;

                    PayrollDataContext.DesignationHistories.InsertOnSubmit(first);
                    PayrollDataContext.SubmitChanges();
                }
            }
        }

        public static Status SaveDesignationHistory(DesignationHistory history)
        {
            Status status = new Status();

            // if employee is of level/grade type and new level does not matches with PEmployeeIncome level then
            // do not allow to save as it will make current level/designation in mis-match in EEmployee and PEmployeeIncome table
            if (EmployeeManager.HasLevelGrade(history.EmployeeId.Value))
            {
                BLevel level = NewHRManager.GetEmployeeCurrentLevel(history.EmployeeId.Value);
                EDesignation designation = CommonManager.GetDesigId(history.DesignationId.Value);

                if (level != null && level.LevelId != designation.LevelId)
                {
                    status.ErrorMessage = "Current level is " + level.Name + ", new level can not be changed from this page,"
                        + " please use promotion page to change the level.";
                    return status;
                }
            }

            EEmployee employee = PayrollDataContext.EEmployees.SingleOrDefault(x => x.EmployeeId == history.EmployeeId);

            history.CreatedBy = SessionManager.CurrentLoggedInUserID;
            history.ModifiedBy = history.CreatedBy;
            history.CreatedOn = DateTime.Now;
            history.ModifiedOn = history.CreatedOn;
            history.IsFirst = false;

            DesignationHistory prevHistory = PayrollDataContext.DesignationHistories
                .Where(x => x.EmployeeId == history.EmployeeId && x.FromDateEng < history.FromDateEng)
                .OrderByDescending(x => x.FromDateEng).FirstOrDefault();

            if (prevHistory != null)
            {
                history.FromDesignationId = prevHistory.DesignationId;
            }
            else
            {
                //EEmployee emp = EmployeeManager.GetEmployeeById(history.EmployeeId.Value);

                // same designation
                history.FromDesignationId = null; //history.DesignationId;
            }

            PayrollDataContext.DesignationHistories.InsertOnSubmit(history);


            SaveChangeSet();


            // set latest Designation
            DesignationHistory latestDesig = PayrollDataContext.DesignationHistories.Where(x => x.EmployeeId == history.EmployeeId)
                .OrderByDescending(x => x.FromDateEng).FirstOrDefault();
            if (latestDesig != null)
            {
                employee.DesignationId = latestDesig.DesignationId;
            }

            SaveChangeSet();

            return status;
        }

        public static void UpdateDesignationHistory(DesignationHistory obj)
        {
            DesignationHistory dbEntity = PayrollDataContext.DesignationHistories.SingleOrDefault(x => x.EmployeeDesignationId == obj.EmployeeDesignationId);

            dbEntity.DesignationId = obj.DesignationId;
            dbEntity.FromDate = obj.FromDate;
            dbEntity.FromDateEng = obj.FromDateEng;
            dbEntity.Note = obj.Note;
            dbEntity.ModifiedOn = DateTime.Now;
            dbEntity.ModifiedBy = SessionManager.CurrentLoggedInUserID;
            dbEntity.EventID = obj.EventID;


            DesignationHistory prevHistory = PayrollDataContext.DesignationHistories
               .Where(x => x.EmployeeId == dbEntity.EmployeeId && x.FromDateEng < dbEntity.FromDateEng)
               .OrderByDescending(x => x.FromDateEng).FirstOrDefault();

            if (prevHistory != null)
            {
                dbEntity.FromDesignationId = prevHistory.DesignationId;
            }
            else
            {
                //EEmployee emp = EmployeeManager.GetEmployeeById(history.EmployeeId.Value);

                // same designation
                dbEntity.FromDesignationId = null;// dbEntity.DesignationId;
            }

            UpdateChangeSet();


            // set latest Designation
            EEmployee emp = EmployeeManager.GetEmployeeById(dbEntity.EmployeeId.Value);

            DesignationHistory latestDesig = PayrollDataContext.DesignationHistories.Where(x => x.EmployeeId == dbEntity.EmployeeId)
                .OrderByDescending(x => x.FromDateEng).FirstOrDefault();
            if (latestDesig != null)
            {
                emp.DesignationId = latestDesig.DesignationId;
            }


            UpdateChangeSet();

        }

        public static List<GetDesignationTransfersResult> GetDesignationTransfers(int currentPage, int pageSize, string searchText, ref int totalRecords, int empId, DateTime? from, DateTime? to)
        {
            List<GetDesignationTransfersResult> list = PayrollDataContext.GetDesignationTransfers(currentPage, pageSize, SessionManager.CurrentCompanyId, searchText, empId, from, to).ToList();
            if (list.Count > 0)
                totalRecords = list[0].TotalRows.Value;
            return list;
        }
        public static List<Report_SalaryChangeResult> GetSalaryChange(int empId)
        {
            List<Report_SalaryChangeResult> list = PayrollDataContext.Report_SalaryChange(null, null, -1, IncomeCalculation.FIXED_AMOUNT, empId).ToList();

            return list;
        }


        #region TimeSheet

        //public static List<GetTimeSheetListForApprovalResult> GetTimeSheetListForApproval(int start, int pagesize, int employeeId, string startDate, string endDate)
        //{
        //    DateTime? startDateEng = null, endDateEng = null;
        //    if (!string.IsNullOrEmpty(startDate))
        //        startDateEng = Convert.ToDateTime(startDate);

        //    if (!string.IsNullOrEmpty(endDate))
        //        endDateEng = Convert.ToDateTime(endDate);

        //    return PayrollDataContext.GetTimeSheetListForApproval(start, pagesize, employeeId, startDateEng, endDateEng).ToList();
        //}

        //public static List<GetTimeSheetListForEmployeeResult> GetTimeSheetListForEmployee(int start, int pagesize, int employeeId, string startDate, string endDate)
        //{
        //    DateTime? startDateEng = null, endDateEng = null;
        //    if (!string.IsNullOrEmpty(startDate))
        //        startDateEng = Convert.ToDateTime(startDate);

        //    if (!string.IsNullOrEmpty(endDate))
        //        endDateEng = Convert.ToDateTime(endDate);

        //    return PayrollDataContext.GetTimeSheetListForEmployee(start, pagesize, employeeId, startDateEng, endDateEng).ToList();
        //}

        public static Status ApproveTimeSheet(List<Timesheet> list)
        {
            Status status = new Status();

            SetConnectionPwd(PayrollDataContext.Connection);
            PayrollDataContext.Transaction = PayrollDataContext.Connection.BeginTransaction();

            try
            {
                foreach (var item in list)
                {
                    Timesheet dbEntity = PayrollDataContext.Timesheets.SingleOrDefault(x => x.TimesheetId == item.TimesheetId);
                    dbEntity.ApprovedBy = SessionManager.CurrentLoggedInEmployeeId;
                    dbEntity.ApprovedOn = BLL.BaseBiz.GetCurrentDateAndTime();
                    dbEntity.Status = (int)TimeSheetStatusEnum.Approved;
                    PayrollDataContext.SubmitChanges();
                }
                status.IsSuccess = true;
                PayrollDataContext.Transaction.Commit();
            }
            catch (Exception exp)
            {
                Log.log("Error while approving time sheet", exp);
                PayrollDataContext.Transaction.Rollback();
                status.ErrorMessage = "Error while approving time sheets.";
                status.IsSuccess = false;
            }
            finally
            {
            }

            return status;
        }

        public static Timesheet GetTimeSheetById(int timeSheetId)
        {
            return PayrollDataContext.Timesheets.SingleOrDefault(x => x.TimesheetId == timeSheetId);
        }

        public static Status RejectTimeSheet(Timesheet obj)
        {
            Status status = new Status();

            Timesheet dbEntity = PayrollDataContext.Timesheets.SingleOrDefault(x => x.TimesheetId == obj.TimesheetId);
            if (dbEntity != null)
            {
                dbEntity.Status = obj.Status;
                //dbEntity.Notes = obj.Notes;
                PayrollDataContext.SubmitChanges();
                status.IsSuccess = true;
            }
            else
            {
                status.ErrorMessage = "Time sheet not found.";
                status.IsSuccess = false;
            }

            return status;
        }

        #endregion


        public static Status DeletePromotionID(int RowID)
        {
            Status status = new Status();

            bool? isSuccess = false;
            string Message = "  ";

            LevelGradeChangeDetail promotion =
                PayrollDataContext.LevelGradeChangeDetails.FirstOrDefault(x => x.DetailId == RowID);

            // Imported promotion history so allow to delete
            if (promotion != null && promotion.LevelId == null)// && promotion.LevelGradeChangeId == 0)
            {
                PayrollDataContext.LevelGradeChangeDetails.DeleteOnSubmit(promotion);
                DeleteChangeSet();

                return status;
            }

            PayrollDataContext.DeleteLevelGradePromotion(RowID, ref isSuccess, ref Message);

            if (isSuccess != null)
            {
                if (isSuccess.Value)
                {
                    status.IsSuccess = isSuccess.Value;
                    return status;
                }
                else
                {
                    status.IsSuccess = isSuccess.Value;
                    status.ErrorMessage = Message;
                }
            }

            return status;
        }


        public static void GetHRInfoCounts(ref int? branches, ref int? departments, ref int? locations, ref int? totalEmployees, ref int? maleEmp, ref int? femEmp, ref int? newJoin, ref int? retired)
        {
            int? a = 0, b = 0, c = 0, d = 0, e = 0, f = 0, g = 0, h = 0;

            DateTime today = DateTime.Now;
            //DateTime startDate = new DateTime(today.Year, today.Month, 1);
            //DateTime endDate = new DateTime(today.Year, today.Month, DateHelper.GetTotalDaysInTheMonth(today.Year, today.Month, true));

            CustomDate todayCustomDate = CustomDate.GetTodayDate(IsEnglish);
            string startDateCD = todayCustomDate.Year.ToString() + "/" + todayCustomDate.Month.ToString() + "/" + "1";
            string endDateCD = todayCustomDate.Year.ToString() + "/" + todayCustomDate.Month.ToString() + "/" + DateHelper.GetTotalDaysInTheMonth(todayCustomDate.Year, todayCustomDate.Month, IsEnglish);

            DateTime startDate = BLL.BaseBiz.GetEngDate(startDateCD, IsEnglish);
            DateTime endDate = BLL.BaseBiz.GetEngDate(endDateCD, IsEnglish);

            int i = PayrollDataContext.GetHRInfoDetails(today, startDate, endDate, SessionManager.CurrentCompanyId, ref a, ref b, ref c, ref d, ref e, ref f, ref g, ref h);

            CustomDate cd = CustomDate.GetTodayDate(IsEnglish);
            string nepDate = cd.Year.ToString() + "/" + cd.Month.ToString() + "/" + DateHelper.GetTotalDaysInTheMonth(cd.Year, cd.Month, IsEnglish);
            DateTime endEngDate = BLL.BaseBiz.GetEngDate(nepDate, false);


            branches = a;
            departments = b;
            locations = c;
            totalEmployees = d;
            maleEmp = e;
            femEmp = f;
            newJoin = g;
            retired = h;
        }

        public static Status SaveUpdateExtraActivity(HExtraActivity obj)
        {
            Status status = new Status();

            if (SessionManager.CurrentLoggedInEmployeeId == 0)
            {
                obj.ApprovedBy = SessionManager.CurrentLoggedInUserID;
                obj.ApprovedOn = DateTime.Now;
                obj.Status = (int)HRStatusEnum.Approved;
            }
            else
            {
                obj.Status = (int)HRStatusEnum.Saved;
            }

            if (obj.CurricularId.Equals(0))
            {
                PayrollDataContext.HExtraActivities.InsertOnSubmit(obj);
                PayrollDataContext.SubmitChanges();
                status.IsSuccess = true;
            }
            else
            {
                HExtraActivity dbObj = PayrollDataContext.HExtraActivities.SingleOrDefault(x => x.CurricularId == obj.CurricularId);
                dbObj.EmployeeId = obj.EmployeeId;
                dbObj.AcitivityName = obj.AcitivityName;
                dbObj.Proficiency = obj.Proficiency;
                dbObj.Award = obj.Award;
                dbObj.Year = obj.Year;
                dbObj.Remarks = obj.Remarks;

                if (SessionManager.CurrentLoggedInEmployeeId == 0)
                {
                    dbObj.ApprovedBy = SessionManager.CurrentLoggedInUserID;
                    dbObj.ApprovedOn = DateTime.Now;
                    dbObj.Status = (int)HRStatusEnum.Approved;
                }

                PayrollDataContext.SubmitChanges();
                status.IsSuccess = true;
            }

            return status;
        }

        public static List<HExtraActivity> GetExtraActivityByEmployeeId(int employeeId)
        {
            List<HExtraActivity> list = PayrollDataContext.HExtraActivities.Where(x => x.EmployeeId == employeeId).OrderBy(x => x.AcitivityName).ToList();

            foreach (var item in list)
            {
                item.IsEditable = NewHRManager.IsHrEditable(item.Status);
            }

            return list;
        }

        public static HExtraActivity GetExtraActivityById(int curricularId)
        {
            return PayrollDataContext.HExtraActivities.SingleOrDefault(x => x.CurricularId == curricularId);
        }

        public static Status DeleteExtraActivity(int curricularId)
        {
            Status status = new Status();

            HExtraActivity dbObj = PayrollDataContext.HExtraActivities.SingleOrDefault(x => x.CurricularId == curricularId);
            if (dbObj != null)
            {
                PayrollDataContext.HExtraActivities.DeleteOnSubmit(dbObj);
                PayrollDataContext.SubmitChanges();
                status.IsSuccess = true;
            }

            return status;
        }

        public static Status SaveUpdateHrNominee(HrNominee obj)
        {
            Status status = new Status();

            if (SessionManager.CurrentLoggedInEmployeeId == 0)
            {
                obj.ApprovedBy = SessionManager.CurrentLoggedInUserID;
                obj.ApprovedOn = DateTime.Now;
                obj.Status = (int)HRStatusEnum.Approved;
            }
            else
            {
                obj.Status = (int)HRStatusEnum.Saved;
            }

            if (obj.NomineeId.Equals(0))
            {
                List<HrNominee> list = PayrollDataContext.HrNominees.Where(x => x.EmployeeId == obj.EmployeeId).OrderByDescending(x => x.EffectiveDateEng).ToList();
                if (list.Count > 0)
                {
                    if ((list[0].EffectiveDateEng >= obj.EffectiveDateEng) && (list[0].NomineeId != obj.NomineeId))
                    {
                        status.ErrorMessage = "Date should be greater than last nominee effective date.";
                        status.IsSuccess = false;
                        return status;
                    }
                }


                PayrollDataContext.HrNominees.InsertOnSubmit(obj);
                PayrollDataContext.SubmitChanges();
                status.IsSuccess = true;
            }
            else
            {
                HrNominee dbObj = PayrollDataContext.HrNominees.SingleOrDefault(x => x.NomineeId == obj.NomineeId);
                dbObj.NomineeName = obj.NomineeName;
                dbObj.RelationId = obj.RelationId;
                dbObj.EffectiveDate = obj.EffectiveDate;
                dbObj.EffectiveDateEng = obj.EffectiveDateEng;
                dbObj.Remarks = obj.Remarks;

                if (SessionManager.CurrentLoggedInEmployeeId == 0)
                {
                    dbObj.ApprovedBy = SessionManager.CurrentLoggedInUserID;
                    dbObj.ApprovedOn = DateTime.Now;
                    dbObj.Status = (int)HRStatusEnum.Approved;
                }

                PayrollDataContext.SubmitChanges();
                status.IsSuccess = true;
            }

            return status;
        }

        public static List<HrNominee> GetHrNomineeByEmployeeId(int employeeId)
        {
            List<HrNominee> list = PayrollDataContext.HrNominees.Where(x => x.EmployeeId == employeeId).OrderBy(x => x.EffectiveDateEng).ToList();

            int i = 0;
            foreach (var item in list)
            {
                i++;
                item.SN = i.ToString();
                item.Relation = ListManager.GetFamilyRelationById(item.RelationId.Value).Name;
                item.IsEditable = NewHRManager.IsHrEditable(item.Status.Value);
            }

            return list;
        }

        public static HrNominee GetHrNomineeByNomineeId(int nomineeId)
        {
            return PayrollDataContext.HrNominees.SingleOrDefault(x => x.NomineeId == nomineeId);
        }

        public static Status DeleteHrNominee(int nomineeId)
        {
            Status status = new Status();

            HrNominee obj = PayrollDataContext.HrNominees.SingleOrDefault(x => x.NomineeId == nomineeId);
            if (obj != null)
            {
                PayrollDataContext.HrNominees.DeleteOnSubmit(obj);
                PayrollDataContext.SubmitChanges();
                status.IsSuccess = true;
            }

            return status;
        }

        public static void ChangeHEducationOrder(int sourceId, int destId, string mode, int employeeId)
        {
            HEducation dbSource = PayrollDataContext.HEducations.SingleOrDefault(x => x.EductionId == sourceId);
            HEducation dbDest = PayrollDataContext.HEducations.SingleOrDefault(x => x.EductionId == destId);
            bool isDropBefore = mode.ToLower().Trim().Contains("before") ? true : false;
            int? order = null;
            if (isDropBefore)
            {

                // now change order of other elements after dest
                List<HEducation> otherList = PayrollDataContext.HEducations
                    .Where(x => (x.EmployeeId == employeeId) && (x.Order > dbDest.Order)).OrderBy(x => x.Order).ToList();

                order = dbDest.Order;
                dbSource.Order = dbDest.Order;
                dbDest.Order = ++order;

                foreach (HEducation item in otherList)
                {
                    if (item.EductionId != dbSource.EductionId)
                        item.Order = ++order;
                }
            }
            else
            {
                // now change order of other elements after dest
                List<HEducation> otherList = PayrollDataContext.HEducations
                    .Where(x => (x.EductionId == employeeId) && (x.Order > dbDest.Order)).OrderBy(x => x.Order).ToList();

                order = dbDest.Order;
                dbSource.Order = ++order;

                foreach (HEducation item in otherList)
                {
                    if (item.EductionId != dbSource.EductionId)
                        item.Order = ++order;
                }
            }
            PayrollDataContext.SubmitChanges();

            order = 1;
            List<HEducation> allList = PayrollDataContext.HEducations.Where(x => x.EmployeeId == employeeId).OrderBy(x => x.Order).ToList();
            foreach (HEducation item in allList)
            {
                item.Order = order++;
            }

            PayrollDataContext.SubmitChanges();
        }

        public static List<Report_HR_EmpListResult> GetHrEmpList(string EmpName, int branchId, int departmentId, int subDepartmentId, int companyId, string departmentName)
        {
            List<Report_HR_EmpListResult> list = PayrollDataContext.Report_HR_EmpList(EmpName, branchId, departmentId, subDepartmentId, companyId, departmentName).ToList();
            foreach (var item in list)
            {

            }
            return list;
        }


        public static Status InsertUpdateEmployeeAddress(TempEAddress obj)
        {
            Status status = new Status();

            if (PayrollDataContext.TempEAddresses.Any(x => x.EmployeeId == SessionManager.CurrentLoggedInEmployeeId))
            {
                TempEAddress dbObj = PayrollDataContext.TempEAddresses.SingleOrDefault(x => x.EmployeeId == SessionManager.CurrentLoggedInEmployeeId);
                dbObj.PECountryId = obj.PECountryId;

                dbObj.PEZoneId = obj.PEZoneId;

                dbObj.PEDistrictId = obj.PEDistrictId;

                dbObj.PEVDCMuncipality = obj.PEVDCMuncipality;
                dbObj.PEStreet = obj.PEStreet;
                dbObj.PEWardNo = obj.PEWardNo;
                dbObj.PEHouseNo = obj.PEHouseNo;
                dbObj.PEState = obj.PEState;
                dbObj.PEZipCode = obj.PEZipCode;
                dbObj.PELocality = obj.PELocality;


                dbObj.PSCountryId = obj.PSCountryId;

                dbObj.PSZoneId = obj.PSZoneId;

                dbObj.PSDistrictId = obj.PSDistrictId;

                dbObj.PSVDCMuncipality = obj.PSVDCMuncipality;
                dbObj.PSStreet = obj.PSStreet;
                dbObj.PSWardNo = obj.PSWardNo;
                dbObj.PSHouseNo = obj.PSHouseNo;
                dbObj.PSState = obj.PSState;
                dbObj.PSZipCode = obj.PSZipCode;
                dbObj.PSLocality = obj.PSLocality;
                dbObj.PSCitIssDis = obj.PSCitIssDis;


                dbObj.CIPhoneNo = obj.CIPhoneNo;
                dbObj.Extension = obj.Extension;
                dbObj.CIMobileNo = obj.CIMobileNo;
                dbObj.PersonalEmail = obj.PersonalEmail;
                dbObj.PersonalMobile = obj.PersonalMobile;
                dbObj.PersonalPhone = obj.PersonalPhone;
                dbObj.EmergencyRelation = obj.EmergencyRelation;
                dbObj.EmergencyName = obj.EmergencyName;
                dbObj.EmergencyPhone = obj.EmergencyPhone;
                dbObj.EmergencyMobile = obj.EmergencyMobile;
                dbObj.Status = 0;
                dbObj.ModifiedBy = SessionManager.CurrentLoggedInUserID;
                dbObj.ModifiedOn = DateTime.Now;
                PayrollDataContext.SubmitChanges();
            }
            else
            {
                obj.Status = 0;
                obj.CreatedBy = SessionManager.CurrentLoggedInUserID;
                obj.CreatedOn = DateTime.Now;
                obj.ModifiedOn = DateTime.Now;
                obj.ModifiedBy = SessionManager.CurrentLoggedInUserID;
                PayrollDataContext.TempEAddresses.InsertOnSubmit(obj);
                PayrollDataContext.SubmitChanges();
                status.IsSuccess = true;
            }

            return status;
        }

        public static TempEAddress GetTempEAddressByEmployeeId(int employeeId)
        {
            return PayrollDataContext.TempEAddresses.SingleOrDefault(x => x.EmployeeId == employeeId && x.Status == 0);
        }

        public static Status DeleteEmployeeProfileChange(List<SelectAll_EmployeeProfileChangeResult> list)
        {
            Status status = new Status();

            SetConnectionPwd(PayrollDataContext.Connection);
            PayrollDataContext.Transaction = PayrollDataContext.Connection.BeginTransaction();

            string filePath = "";

            try
            {
                foreach (var item in list)
                {
                    filePath = "";

                    if (item.Tbl == "EAddress")
                    {
                        TempEAddress objTempEAddress = PayrollDataContext.TempEAddresses.SingleOrDefault(x => x.AddressId == item.Id && x.EmployeeId == item.EmployeeId);
                        PayrollDataContext.TempEAddresses.DeleteOnSubmit(objTempEAddress);
                        PayrollDataContext.SubmitChanges();
                    }
                    else if (item.Tbl == "HFamily")
                    {
                        HFamily objHFamily = PayrollDataContext.HFamilies.SingleOrDefault(x => x.FamilyId == item.Id && x.EmployeeId == item.EmployeeId);
                        PayrollDataContext.HFamilies.DeleteOnSubmit(objHFamily);
                        PayrollDataContext.SubmitChanges();
                    }
                    else if (item.Tbl == "HPreviousEmployment")
                    {
                        HPreviousEmployment objHPreviousEmployment = PayrollDataContext.HPreviousEmployments.SingleOrDefault(x => x.PrevEmploymentId == item.Id && x.EmployeeId == item.EmployeeId);
                        PayrollDataContext.HPreviousEmployments.DeleteOnSubmit(objHPreviousEmployment);
                        PayrollDataContext.SubmitChanges();
                    }
                    else if (item.Tbl == "HEducation")
                    {
                        HEducation objHEducation = PayrollDataContext.HEducations.SingleOrDefault(x => x.EductionId == item.Id && x.EmployeeId == item.EmployeeId);

                        if (!string.IsNullOrEmpty(objHEducation.ServerFileName))
                            filePath = HttpContext.Current.Server.MapPath(objHEducation.FileLocation) + objHEducation.ServerFileName;
                        PayrollDataContext.HEducations.DeleteOnSubmit(objHEducation);
                        PayrollDataContext.SubmitChanges();
                    }
                    else if (item.Tbl == "HTraining")
                    {
                        HTraining objHTraining = PayrollDataContext.HTrainings.SingleOrDefault(x => x.TrainingId == item.Id && x.EmployeeId == item.EmployeeId);
                        if (!string.IsNullOrEmpty(objHTraining.ServerFileName))
                            filePath = HttpContext.Current.Server.MapPath(objHTraining.FileLocation) + objHTraining.ServerFileName;
                        PayrollDataContext.HTrainings.DeleteOnSubmit(objHTraining);
                        PayrollDataContext.SubmitChanges();
                    }
                    else if (item.Tbl == "HSeminar")
                    {
                        HSeminar objHSeminar = PayrollDataContext.HSeminars.SingleOrDefault(x => x.SeminarId == item.Id && x.EmployeeId == item.EmployeeId);
                        PayrollDataContext.HSeminars.DeleteOnSubmit(objHSeminar);
                        PayrollDataContext.SubmitChanges();
                    }
                    else if (item.Tbl == "SkillSetEmployee")
                    {
                        SkillSetEmployee objSkillSetEmployee = PayrollDataContext.SkillSetEmployees.SingleOrDefault(x => x.SkillSetId == item.Id && x.EmployeeId == item.EmployeeId); ;
                        PayrollDataContext.SkillSetEmployees.DeleteOnSubmit(objSkillSetEmployee);
                        PayrollDataContext.SubmitChanges();
                    }
                    else if (item.Tbl == "LanguageSetEmployee")
                    {
                        LanguageSetEmployee objLanguageSetEmployee = PayrollDataContext.LanguageSetEmployees.SingleOrDefault(x => x.LanguageSetId == item.Id && x.EmployeeId == item.EmployeeId);
                        PayrollDataContext.LanguageSetEmployees.DeleteOnSubmit(objLanguageSetEmployee);
                        PayrollDataContext.SubmitChanges();
                    }
                    else if (item.Tbl == "HPublication")
                    {
                        HPublication objHPublication = PayrollDataContext.HPublications.SingleOrDefault(x => x.PublicationId == item.Id && x.EmployeeId == item.EmployeeId);
                        PayrollDataContext.HPublications.DeleteOnSubmit(objHPublication);
                        PayrollDataContext.SubmitChanges();
                    }
                    else if (item.Tbl == "HCitizenship")
                    {
                        HCitizenship objHCitizenship = PayrollDataContext.HCitizenships.SingleOrDefault(x => x.CitizenshipId == item.Id && x.EmployeeId == item.EmployeeId);
                        if (!string.IsNullOrEmpty(objHCitizenship.ServerFileName))
                            filePath = HttpContext.Current.Server.MapPath(objHCitizenship.FileLocation) + objHCitizenship.ServerFileName;
                        PayrollDataContext.HCitizenships.DeleteOnSubmit(objHCitizenship);
                        PayrollDataContext.SubmitChanges();
                    }
                    else if (item.Tbl == "HDrivingLicence")
                    {
                        HDrivingLicence objHDrivingLicence = PayrollDataContext.HDrivingLicences.SingleOrDefault(x => x.DrivingLicenceId == item.Id && x.EmployeeId == item.EmployeeId);
                        if (!string.IsNullOrEmpty(objHDrivingLicence.ServerFileName))
                            filePath = HttpContext.Current.Server.MapPath(objHDrivingLicence.FileLocation) + objHDrivingLicence.ServerFileName;
                        PayrollDataContext.HDrivingLicences.DeleteOnSubmit(objHDrivingLicence);
                        PayrollDataContext.SubmitChanges();
                    }
                    else if (item.Tbl == "HPassport")
                    {
                        HPassport objHPassport = PayrollDataContext.HPassports.SingleOrDefault(x => x.PassportId == item.Id && x.EmployeeId == item.EmployeeId);
                        if (!string.IsNullOrEmpty(objHPassport.ServerFileName))
                            filePath = HttpContext.Current.Server.MapPath(objHPassport.FileLocation) + objHPassport.ServerFileName;
                        PayrollDataContext.HPassports.DeleteOnSubmit(objHPassport);
                        PayrollDataContext.SubmitChanges();
                    }
                    else if (item.Tbl == "HDocument")
                    {
                        HDocument objHDocument = PayrollDataContext.HDocuments.SingleOrDefault(x => x.DocumentId == item.Id && x.EmployeeId == item.EmployeeId);
                        if (!string.IsNullOrEmpty(objHDocument.Url))
                            filePath = Path.Combine(HttpContext.Current.Server.MapPath(Config.UploadLocation), objHDocument.Url);
                        PayrollDataContext.HDocuments.DeleteOnSubmit(objHDocument);
                        PayrollDataContext.SubmitChanges();
                    }

                    if (filePath != "")
                    {
                        if (File.Exists(filePath))
                            File.Delete(filePath);
                    }
                }

                status.IsSuccess = true;
                PayrollDataContext.Transaction.Commit();
            }
            catch (Exception exp)
            {
                Log.log("Error while deleting employee profile changes", exp);
                PayrollDataContext.Transaction.Rollback();
                status.IsSuccess = false;
            }
            finally
            {

            }

            return status;
        }

        public static Status DeleteEmployeePhoto(List<int> employeeIdList)
        {
            Status status = new Status();

            SetConnectionPwd(PayrollDataContext.Connection);
            PayrollDataContext.Transaction = PayrollDataContext.Connection.BeginTransaction();

            try
            {
                foreach (var id in employeeIdList)
                {
                    HHumanResource obj = PayrollDataContext.HHumanResources.SingleOrDefault(x => x.EmployeeId == id);
                    obj.tempUrlPhoto = null;
                    obj.tempUrlPhotoThumbnail = null;
                    PayrollDataContext.SubmitChanges();
                }
                status.IsSuccess = true;
                PayrollDataContext.Transaction.Commit();
            }
            catch (Exception exp)
            {
                Log.log("Error while deleting employee photographs", exp);
                PayrollDataContext.Transaction.Rollback();
                status.IsSuccess = false;
            }
            finally
            {

            }
            return status;
        }

        public static Status SaveUpdateClientVisit(NewActivity obj)
        {
            Status status = new Status();

            if (obj.ActivityId.Equals(0))
            {
                PayrollDataContext.NewActivities.InsertOnSubmit(obj);
            }
            else
            {
                NewActivity dbNewActivity = PayrollDataContext.NewActivities.SingleOrDefault(x => x.ActivityId == obj.ActivityId);
                dbNewActivity.ClientSoftwareId = obj.ClientSoftwareId;
                dbNewActivity.Name = obj.Name;
                dbNewActivity.Date = obj.Date;
                dbNewActivity.DateEng = obj.DateEng;
                dbNewActivity.StartTime = obj.StartTime;
                dbNewActivity.EndTime = obj.EndTime;
                dbNewActivity.Duration = obj.Duration;
                dbNewActivity.Representative = obj.Representative;
                dbNewActivity.Issue = obj.Issue;
                dbNewActivity.Result = obj.Result;
                dbNewActivity.NextPlannedVisit = obj.NextPlannedVisit;
                dbNewActivity.NextPlannedVisitEng = obj.NextPlannedVisitEng;
            }

            PayrollDataContext.SubmitChanges();
            status.IsSuccess = true;
            return status;
        }

        public static Status SaveUpdateSoftwareTesting(NewActivity obj)
        {
            Status status = new Status();

            if (obj.ActivityId.Equals(0))
            {
                PayrollDataContext.NewActivities.InsertOnSubmit(obj);
            }
            else
            {
                NewActivity dbNewActivity = PayrollDataContext.NewActivities.SingleOrDefault(x => x.ActivityId == obj.ActivityId);
                dbNewActivity.ClientSoftwareId = obj.ClientSoftwareId;
                dbNewActivity.Name = obj.Name;
                dbNewActivity.Date = obj.Date;
                dbNewActivity.DateEng = obj.DateEng;
                dbNewActivity.StartTime = obj.StartTime;
                dbNewActivity.EndTime = obj.EndTime;
                dbNewActivity.Duration = obj.Duration;
                dbNewActivity.Description = obj.Description;
                dbNewActivity.Result = obj.Result;
                dbNewActivity.Issue = obj.Issue;

                if (!string.IsNullOrEmpty(obj.ServerFileName))
                {
                    dbNewActivity.FileFormat = obj.FileFormat;
                    dbNewActivity.FileType = obj.FileType;
                    dbNewActivity.FileLocation = obj.FileLocation;
                    dbNewActivity.ServerFileName = obj.ServerFileName;
                    dbNewActivity.UserFileName = obj.UserFileName;
                }

            }

            PayrollDataContext.SubmitChanges();
            status.IsSuccess = true;
            return status;
        }

        public static Status SaveUpdateDocumentation(NewActivity obj)
        {
            Status status = new Status();

            if (obj.ActivityId.Equals(0))
            {
                PayrollDataContext.NewActivities.InsertOnSubmit(obj);
            }
            else
            {
                NewActivity dbNewActivity = PayrollDataContext.NewActivities.SingleOrDefault(x => x.ActivityId == obj.ActivityId);
                dbNewActivity.ClientSoftwareId = obj.ClientSoftwareId;
                dbNewActivity.Name = obj.Name;
                dbNewActivity.Date = obj.Date;
                dbNewActivity.DateEng = obj.DateEng;
                dbNewActivity.StartTime = obj.StartTime;
                dbNewActivity.EndTime = obj.EndTime;
                dbNewActivity.Duration = obj.Duration;
                dbNewActivity.Issue = obj.Issue;
                dbNewActivity.Description = obj.Description;
            }

            PayrollDataContext.SubmitChanges();
            status.IsSuccess = true;
            return status;
        }

        public static Status SaveUpdateRemoteSupport(NewActivity obj)
        {
            Status status = new Status();

            if (obj.ActivityId.Equals(0))
            {
                PayrollDataContext.NewActivities.InsertOnSubmit(obj);
            }
            else
            {
                NewActivity dbNewActivity = PayrollDataContext.NewActivities.SingleOrDefault(x => x.ActivityId == obj.ActivityId);
                dbNewActivity.ClientSoftwareId = obj.ClientSoftwareId;
                dbNewActivity.Name = obj.Name;
                dbNewActivity.Date = obj.Date;
                dbNewActivity.DateEng = obj.DateEng;
                dbNewActivity.StartTime = obj.StartTime;
                dbNewActivity.EndTime = obj.EndTime;
                dbNewActivity.Duration = obj.Duration;
                dbNewActivity.Description = obj.Description;
                dbNewActivity.Issue = obj.Issue;
                dbNewActivity.Result = obj.Result;
                dbNewActivity.NextPlannedVisit = obj.NextPlannedVisit;
                dbNewActivity.NextPlannedVisitEng = obj.NextPlannedVisitEng;
            }

            PayrollDataContext.SubmitChanges();
            status.IsSuccess = true;
            return status;
        }

        public static Status SaveUpdateOther(NewActivity obj)
        {
            Status status = new Status();

            if (obj.ActivityId.Equals(0))
            {
                PayrollDataContext.NewActivities.InsertOnSubmit(obj);
            }
            else
            {
                NewActivity dbNewActivity = PayrollDataContext.NewActivities.SingleOrDefault(x => x.ActivityId == obj.ActivityId);
                dbNewActivity.Name = obj.Name;
                dbNewActivity.Date = obj.Date;
                dbNewActivity.DateEng = obj.DateEng;
                dbNewActivity.StartTime = obj.StartTime;
                dbNewActivity.EndTime = obj.EndTime;
                dbNewActivity.Duration = obj.Duration;
                dbNewActivity.Description = obj.Description;
            }

            PayrollDataContext.SubmitChanges();
            status.IsSuccess = true;
            return status;
        }

        public static List<NewActivity> GetNewActivityList(int activityType)
        {
            List<NewActivity> list = PayrollDataContext.NewActivities.Where(x => x.ActivityType == activityType && x.EmployeeId == SessionManager.CurrentLoggedInEmployeeId).OrderByDescending(x => x.DateEng).ToList();

            foreach (var item in list)
            {
                if (item.ActivityType == 1 || item.ActivityType == 4)
                    item.ClientName = ListManager.GetClientById(item.ClientSoftwareId.Value).ClientName;
                else if (item.ActivityType == 2 || item.ActivityType == 3)
                    item.SoftwareName = ListManager.GetSoftwareById(item.ClientSoftwareId.Value).SoftwareName;

                if (item.Status == (int)AtivityStatusEnum.Draft)
                    item.IsEditable = 1;
                else
                    item.IsEditable = 0;

                item.DurationTime = item.Duration.Value.ToString("N2");

                if (!string.IsNullOrEmpty(item.ServerFileName))
                    item.ContainsFile = 1;
                else
                    item.ContainsFile = 0;
            }

            return list;
        }

        public static NewActivity GetNewActivityById(int activityId)
        {
            return PayrollDataContext.NewActivities.SingleOrDefault(x => x.ActivityId == activityId);
        }

        public static Status DeleteSoftwareTestingFile(int activityId)
        {
            Status status = new Status();

            NewActivity dbRecord = PayrollDataContext.NewActivities.SingleOrDefault(x => x.ActivityId == activityId);

            if (dbRecord == null)
            {
                status.ErrorMessage = "error while deleting file.";//GetConcurrencyErrorMsg();//need to implement this on later.
                status.IsSuccess = false;
                return status;
            }
            else
            {
                dbRecord.FileFormat = "";
                dbRecord.FileLocation = "";
                dbRecord.ServerFileName = "";
                dbRecord.UserFileName = "";
                dbRecord.FileType = "";
                PayrollDataContext.SubmitChanges();
                status.IsSuccess = true;
                return status;
            }
        }

        public static Status DeleteNewActivityDetail(int detailId)
        {
            Status status = new Status();

            NewActivityDetail dbObj = PayrollDataContext.NewActivityDetails.SingleOrDefault(x => x.DetailId == detailId);
            if (dbObj != null)
            {
                PayrollDataContext.NewActivityDetails.DeleteOnSubmit(dbObj);
                PayrollDataContext.SubmitChanges();
                status.IsSuccess = true;
            }
            else
            {
                status.ErrorMessage = "Record not found.";
                status.IsSuccess = false;
            }

            return status;
        }

        public static List<GetNewActivityListResult> GetNewActivityListRes(DateTime? startDate, DateTime? endDate, string employeeName, int activityType, int clientSoftwareId)
        {
            List<GetNewActivityListResult> list = PayrollDataContext.GetNewActivityList(startDate, endDate, employeeName, activityType, clientSoftwareId).ToList();

            foreach (var item in list)
                item.DurationTime = item.Duration.Value.ToString("N2");

            return list;
        }

        public static Status MarkNewActivityAsApproved(int detailId)
        {
            Status status = new Status();

            NewActivityDetail dbObj = PayrollDataContext.NewActivityDetails.SingleOrDefault(x => x.DetailId == detailId);
            if (dbObj != null)
            {
                dbObj.Status = (int)AtivityStatusEnum.Approved;
                dbObj.ModifiedBy = SessionManager.CurrentLoggedInUserID;
                dbObj.ModifiedOn = System.DateTime.Now;
                PayrollDataContext.SubmitChanges();
                status.IsSuccess = true;
            }
            else
            {
                status.ErrorMessage = "Error in marking activity as Read.";
                status.IsSuccess = false;
            }

            return status;
        }

        public static Status SaveAndSendNewActivities(List<GetNewActivityListResult> list)
        {
            Status status = new Status();

            SetConnectionPwd(PayrollDataContext.Connection);
            PayrollDataContext.Transaction = PayrollDataContext.Connection.BeginTransaction();

            try
            {
                foreach (var item in list)
                {
                    NewActivity dbObj = PayrollDataContext.NewActivities.SingleOrDefault(x => x.ActivityId == item.ActivityId);
                    dbObj.Status = (int)AtivityStatusEnum.SaveAndSend;
                    dbObj.Modifiedby = SessionManager.CurrentLoggedInUserID;
                    dbObj.ModifiedOn = System.DateTime.Now;
                    PayrollDataContext.SubmitChanges();
                }

                status.IsSuccess = true;
                PayrollDataContext.Transaction.Commit();
            }
            catch (Exception exp)
            {
                Log.log("Error while saving activities as Save and Send", exp);
                PayrollDataContext.Transaction.Rollback();
                status.IsSuccess = false;
            }
            finally
            {

            }
            return status;
        }


        public static Status SaveNewActivitiesAsApproved(List<GetNewActivityListResult> list)
        {
            Status status = new Status();

            SetConnectionPwd(PayrollDataContext.Connection);
            PayrollDataContext.Transaction = PayrollDataContext.Connection.BeginTransaction();

            try
            {
                foreach (var item in list)
                {
                    NewActivity dbObj = PayrollDataContext.NewActivities.SingleOrDefault(x => x.ActivityId == item.ActivityId);
                    dbObj.Status = (int)AtivityStatusEnum.Approved;
                    dbObj.Modifiedby = SessionManager.CurrentLoggedInUserID;
                    dbObj.ModifiedOn = System.DateTime.Now;
                    PayrollDataContext.SubmitChanges();
                }

                status.IsSuccess = true;
                PayrollDataContext.Transaction.Commit();
            }
            catch (Exception exp)
            {
                Log.log("Error while marking activities as Read", exp);
                PayrollDataContext.Transaction.Rollback();
                status.IsSuccess = false;
            }
            finally
            {

            }
            return status;
        }

        public static Status SaveUpdateNMAward(NonMonetaryAward obj)
        {
            Status status = new Status();

            if (SessionManager.CurrentLoggedInEmployeeId == 0)
            {
                obj.ApprovedBy = SessionManager.CurrentLoggedInUserID;
                obj.ApprovedOn = DateTime.Now;
                obj.Status = (int)HRStatusEnum.Approved;
            }
            else
            {
                obj.Status = (int)HRStatusEnum.Saved;
            }

            if (obj.AwardId.Equals(0))
            {
                PayrollDataContext.NonMonetaryAwards.InsertOnSubmit(obj);
                PayrollDataContext.SubmitChanges();
                status.IsSuccess = true;
            }
            else
            {
                NonMonetaryAward dbObj = PayrollDataContext.NonMonetaryAwards.SingleOrDefault(x => x.AwardId == obj.AwardId);
                dbObj.AwardName = obj.AwardName;
                dbObj.AwardedBy = obj.AwardedBy;
                dbObj.AwardDate = obj.AwardDate;
                dbObj.AwardDateEng = obj.AwardDateEng;
                dbObj.Description = obj.Description;
                dbObj.Modifiedby = SessionManager.CurrentLoggedInUserID;
                dbObj.ModifiedOn = DateTime.Now;

                if (SessionManager.CurrentLoggedInEmployeeId == 0)
                {
                    dbObj.ApprovedBy = SessionManager.CurrentLoggedInUserID;
                    dbObj.ApprovedOn = DateTime.Now;
                    dbObj.Status = (int)HRStatusEnum.Approved;
                }

                PayrollDataContext.SubmitChanges();
                status.IsSuccess = true;
            }

            return status;
        }

        public static Status DeleteNMAward(int awardId)
        {
            Status status = new Status();

            NonMonetaryAward obj = PayrollDataContext.NonMonetaryAwards.SingleOrDefault(x => x.AwardId == awardId);
            if (obj != null)
            {
                PayrollDataContext.NonMonetaryAwards.DeleteOnSubmit(obj);
                PayrollDataContext.SubmitChanges();
                status.IsSuccess = true;
            }

            return status;
        }

        public static NonMonetaryAward GetNonMonetaryAwardById(int awardId)
        {
            return PayrollDataContext.NonMonetaryAwards.SingleOrDefault(x => x.AwardId == awardId);
        }

        public static List<NonMonetaryAward> GetNonMonetaryAwardByEmployeeId(int employeeId)
        {
            List<NonMonetaryAward> list = PayrollDataContext.NonMonetaryAwards.Where(x => x.EmployeeId == employeeId).OrderBy(x => x.AwardDateEng).ToList();

            foreach (var item in list)
                item.IsEditable = NewHRManager.IsHrEditable(item.Status.Value);

            return list;
        }

        public static Status SaveUpdateEHobby(EHobby obj)
        {
            Status status = new Status();

            if (SessionManager.CurrentLoggedInEmployeeId == 0)
            {
                obj.ApprovedBy = SessionManager.CurrentLoggedInUserID;
                obj.ApprovedOn = DateTime.Now;
                obj.Status = (int)HRStatusEnum.Approved;
            }
            else
            {
                obj.Status = (int)HRStatusEnum.Saved;
            }

            if (obj.HobbyId.Equals(0))
            {
                PayrollDataContext.EHobbies.InsertOnSubmit(obj);
                PayrollDataContext.SubmitChanges();
                status.IsSuccess = true;
            }
            else
            {
                EHobby dbObj = PayrollDataContext.EHobbies.SingleOrDefault(x => x.HobbyId == obj.HobbyId);
                dbObj.HobbyTypeId = obj.HobbyTypeId;
                dbObj.InvolvedFrom = obj.InvolvedFrom;
                dbObj.InvolvedFromEng = obj.InvolvedFromEng;
                dbObj.Description = obj.Description;
                dbObj.Modifiedby = SessionManager.CurrentLoggedInUserID;
                dbObj.ModifiedOn = DateTime.Now;

                if (SessionManager.CurrentLoggedInEmployeeId == 0)
                {
                    dbObj.ApprovedBy = SessionManager.CurrentLoggedInUserID;
                    dbObj.ApprovedOn = DateTime.Now;
                    dbObj.Status = (int)HRStatusEnum.Approved;
                }

                PayrollDataContext.SubmitChanges();
                status.IsSuccess = true;
            }

            return status;
        }

        public static List<EHobby> GetEHobbyByEmployeeId(int employeeId)
        {
            List<EHobby> list = PayrollDataContext.EHobbies.Where(x => x.EmployeeId == employeeId).OrderBy(x => x.InvolvedFromEng).ToList();

            foreach (var item in list)
            {
                item.IsEditable = NewHRManager.IsHrEditable(item.Status.Value);
                item.HobbyName = ListManager.GetHobyyTypeById(item.HobbyTypeId.Value).HobbyName;
            }

            return list;
        }

        public static Status DeleteEHobby(int hobbyId)
        {
            Status status = new Status();

            EHobby obj = PayrollDataContext.EHobbies.SingleOrDefault(x => x.HobbyId == hobbyId);
            if (obj != null)
            {
                PayrollDataContext.EHobbies.DeleteOnSubmit(obj);
                PayrollDataContext.SubmitChanges();
                status.IsSuccess = true;
            }

            return status;
        }

        public static EHobby GetEHobbyById(int hobbyId)
        {
            return PayrollDataContext.EHobbies.SingleOrDefault(x => x.HobbyId == hobbyId);
        }

        public static List<GetEmployeeHierarchyResult> GetEmployeeHierarchyList(int branchId, int departmentId, string sortBy)
        {
            return PayrollDataContext.GetEmployeeHierarchy(branchId, departmentId, sortBy).ToList();
        }

        public static BLevelGroup GetBLevelGroupById(int levelGroupId)
        {
            return PayrollDataContext.BLevelGroups.SingleOrDefault(x => x.LevelGroupId == levelGroupId);
        }

        public static BLevel GetBLevelById(int levelId)
        {
            return PayrollDataContext.BLevels.SingleOrDefault(x => x.LevelId == levelId);
        }

        public static EDesignation GetDesignationById(int designationId)
        {
            return PayrollDataContext.EDesignations.SingleOrDefault(x => x.DesignationId == designationId);
        }

        public static List<GetEmployeePlanResult> GetEmployeePlanList(int branchId, int departmentId, string sortBy)
        {
            return PayrollDataContext.GetEmployeePlan(branchId, departmentId, sortBy).ToList();
        }

        public static Status SaveUpdateEmployeePlan(List<EmployeePlanning> list)
        {
            Status status = new Status();


            SetConnectionPwd(PayrollDataContext.Connection);
            PayrollDataContext.Transaction = PayrollDataContext.Connection.BeginTransaction();

            try
            {

                foreach (var item in list)
                {
                    EmployeePlanning dbObj = PayrollDataContext.EmployeePlannings.SingleOrDefault(x => x.LevelGroupId == item.LevelGroupId && x.LevelId == item.LevelId
                        && x.DesignationId == item.DesignationId && x.BranchId == item.BranchId && x.DepartmentId == item.DepartmentId);
                    if (dbObj == null)
                    {
                        PayrollDataContext.EmployeePlannings.InsertOnSubmit(item);
                    }
                    else
                    {
                        dbObj.PlannedEmployees = item.PlannedEmployees;
                    }
                    PayrollDataContext.SubmitChanges();
                }
                status.IsSuccess = true;
                PayrollDataContext.Transaction.Commit();
            }
            catch (Exception exp)
            {
                Log.log("Error while saving employee planning", exp);
                PayrollDataContext.Transaction.Rollback();
                status.IsSuccess = false;
            }
            finally
            {

            }

            return status;
        }

        public static int GetNoOfPlannedEmployeesForDesignationInBrDept(int levelGroupId, int levelId, int designationId, int branchId, int departmentId)
        {
            int no = 0;
            EmployeePlanning obj = PayrollDataContext.EmployeePlannings.SingleOrDefault(x => x.LevelGroupId == levelGroupId && x.LevelId == levelId && x.DesignationId == designationId && x.BranchId == branchId && x.DepartmentId == departmentId);
            if (obj != null)
                no = obj.PlannedEmployees.Value;

            return no;
        }

        public static int GetPlannedEmployeesNoForDesignationByIds(int levelGroupId, int levelId, int designationId)
        {
            int no = 0;

            EmployeePlanning obj = PayrollDataContext.EmployeePlannings.SingleOrDefault(x => x.LevelGroupId == levelGroupId && x.LevelId == levelId && x.DesignationId == designationId);
            if (obj != null)
                no = obj.PlannedEmployees.Value;

            return no;
        }

        public static List<EmployeePlanning> GetEmployeePlanningListByIds(int levelGroupId, int levelId, int designationId)
        {
            List<EmployeePlanning> list = PayrollDataContext.EmployeePlannings.Where(x => x.LevelGroupId == levelGroupId && x.LevelId == levelId && x.DesignationId == designationId).ToList();

            foreach (var item in list)
            {
                item.br = BranchManager.GetBranchById(item.BranchId.Value).Name;
                item.dep = DepartmentManager.GetDepartmentById(item.DepartmentId.Value).Name;
            }

            return list;
        }

        public static EmployeePlanning GetEmployeePlanningByIds(int levelGroupId, int levelId, int designationId, int branchId, int departmentId)
        {
            return PayrollDataContext.EmployeePlannings.SingleOrDefault(x => x.LevelGroupId == levelGroupId && x.LevelId == levelId && x.DesignationId == designationId && x.BranchId == branchId && x.DepartmentId == departmentId);
        }

        public static List<LLeaveType> GetLeaveType()
        {
            return PayrollDataContext.LLeaveTypes.Where(x => x.CompanyId == SessionManager.CurrentCompanyId).ToList();
        }

        public static List<AttendanceGetMainReportNewWithBranchFilterResult> GetAttendanceMainReportList(int employeeId, int payrollPeriodId, int branchId)
        {
            return PayrollDataContext.AttendanceGetMainReportNewWithBranchFilter(employeeId, payrollPeriodId, branchId).ToList();
        }

        public static List<GetEmployeeLeaveDaysForPeriodResult> GetEmployeeLeaveList(DateTime startDate, DateTime endDate, int payrollPeriodId)
        {


            int totalDays = 0;
            TimeSpan ts = endDate - startDate;
            totalDays = ts.Days + 1;

            List<LLeaveType> listLLeaveType = NewHRManager.GetLeaveType();
            int? LT1 = null, LT2 = null, LT3 = null, LT4 = null, LT5 = null, LT6 = null, LT7 = null, LT8 = null, LT9 = null, LT10 = null, LT11 = null, LT12 = null;

            for (int i = 0; i < listLLeaveType.Count; i++)
            {
                if (i == 0)
                    LT1 = listLLeaveType[i].LeaveTypeId;

                if (i == 1)
                    LT2 = listLLeaveType[i].LeaveTypeId;

                if (i == 2)
                    LT3 = listLLeaveType[i].LeaveTypeId;

                if (i == 3)
                    LT4 = listLLeaveType[i].LeaveTypeId;

                if (i == 4)
                    LT5 = listLLeaveType[i].LeaveTypeId;

                if (i == 5)
                    LT6 = listLLeaveType[i].LeaveTypeId;

                if (i == 6)
                    LT7 = listLLeaveType[i].LeaveTypeId;

                if (i == 7)
                    LT8 = listLLeaveType[i].LeaveTypeId;

                if (i == 8)
                    LT9 = listLLeaveType[i].LeaveTypeId;

                if (i == 9)
                    LT10 = listLLeaveType[i].LeaveTypeId;

                if (i == 10)
                    LT11 = listLLeaveType[i].LeaveTypeId;

                if (i == 11)
                    LT12 = listLLeaveType[i].LeaveTypeId;
            }

            double sumTotalDays = 0, sumWH = 0, sumPH = 0, SumstdDays = 0, sumLT1 = 0, sumLT2 = 0, sumLT3 = 0, sumLT4 = 0, sumLT5 = 0, sumLT6 = 0, sumLT7 = 0, sumLT8 = 0, sumLT9 = 0, sumLT10 = 0, sumLT11 = 0, sumLT12 = 0, sumUPL = 0, sumAbs = 0, sumLeaveDays = 0, sumWorkDays = 0, sumPercentage = 0;
            List<GetEmployeeLeaveDaysForPeriodResult> list = PayrollDataContext.GetEmployeeLeaveDaysForPeriod(startDate, payrollPeriodId, totalDays, endDate).ToList();

            //List<LeaveSummaryBO> newList = new List<LeaveSummaryBO>();

            string einList = string.Join(",", list.Select(x => x.EmployeeId.ToString()).ToList().ToArray());

            List<GetEmployeeLeaveDaysForPeriodDetailsResult> details = PayrollDataContext.GetEmployeeLeaveDaysForPeriodDetails(startDate, payrollPeriodId, totalDays, endDate, einList).ToList();

            if (CommonManager.CalculationConstant.CompanyHasHourlyLeave.Value)
                totalDays = totalDays * CommonManager.CalculationConstant.CompanyLeaveHoursInADay.Value;

            foreach (var item in list)
            {

                // set sum value
                item.WH = (double)details.Where(x => x.EmployeeId == item.EmployeeId && x.Type == (int)TimeAttendenceDayType.WeeklyHoliday).Sum(x => x.Total).Value;
                item.PH = (double)details.Where(x => x.EmployeeId == item.EmployeeId && x.Type == (int)TimeAttendenceDayType.Holiday).Sum(x => x.Total).Value;
                item.UPL = (double)details.Where(x => x.EmployeeId == item.EmployeeId && x.Type == -1).Sum(x => x.Total);
                item.AbsDays = (double)details.Where(x => x.EmployeeId == item.EmployeeId && x.Type == (int)TimeAttendenceDayType.Absent).Sum(x => x.Total);

                if (LT1 != null)
                    item.LT1 = details.Where(x => x.EmployeeId == item.EmployeeId && x.Type == (int)TimeAttendenceDayType.Leave && x.TypeValue == LT1).Sum(x => x.Total).Value;
                if (LT2 != null)
                    item.LT2 = details.Where(x => x.EmployeeId == item.EmployeeId && x.Type == (int)TimeAttendenceDayType.Leave && x.TypeValue == LT2).Sum(x => x.Total).Value;
                if (LT3 != null)
                    item.LT3 = details.Where(x => x.EmployeeId == item.EmployeeId && x.Type == (int)TimeAttendenceDayType.Leave && x.TypeValue == LT3).Sum(x => x.Total).Value;
                if (LT4 != null)
                    item.LT4 = details.Where(x => x.EmployeeId == item.EmployeeId && x.Type == (int)TimeAttendenceDayType.Leave && x.TypeValue == LT4).Sum(x => x.Total).Value;
                if (LT5 != null)
                    item.LT5 = details.Where(x => x.EmployeeId == item.EmployeeId && x.Type == (int)TimeAttendenceDayType.Leave && x.TypeValue == LT5).Sum(x => x.Total).Value;
                if (LT6 != null)
                    item.LT6 = details.Where(x => x.EmployeeId == item.EmployeeId && x.Type == (int)TimeAttendenceDayType.Leave && x.TypeValue == LT6).Sum(x => x.Total).Value;
                if (LT7 != null)
                    item.LT7 = details.Where(x => x.EmployeeId == item.EmployeeId && x.Type == (int)TimeAttendenceDayType.Leave && x.TypeValue == LT7).Sum(x => x.Total).Value;
                if (LT8 != null)
                    item.LT8 = details.Where(x => x.EmployeeId == item.EmployeeId && x.Type == (int)TimeAttendenceDayType.Leave && x.TypeValue == LT8).Sum(x => x.Total).Value;
                if (LT9 != null)
                    item.LT9 = details.Where(x => x.EmployeeId == item.EmployeeId && x.Type == (int)TimeAttendenceDayType.Leave && x.TypeValue == LT9).Sum(x => x.Total).Value;
                if (LT10 != null)
                    item.LT10 = details.Where(x => x.EmployeeId == item.EmployeeId && x.Type == (int)TimeAttendenceDayType.Leave && x.TypeValue == LT10).Sum(x => x.Total).Value;
                if (LT11 != null)
                    item.LT11 = details.Where(x => x.EmployeeId == item.EmployeeId && x.Type == (int)TimeAttendenceDayType.Leave && x.TypeValue == LT11).Sum(x => x.Total).Value;
                if (LT12 != null)
                    item.LT12 = details.Where(x => x.EmployeeId == item.EmployeeId && x.Type == (int)TimeAttendenceDayType.Leave && x.TypeValue == LT12).Sum(x => x.Total).Value;


                item.TotalDays = totalDays;
                item.StdDays = item.TotalDays
                    - item.WH
                    - item.PH;
                //(item.WH.Value + item.PH.Value);

                item.LeaveDays =
                        (double)details.Where(x => x.EmployeeId == item.EmployeeId && x.Type == (int)TimeAttendenceDayType.Leave).Sum(x => x.Total)
                        + item.UPL // UPL
                        + item.AbsDays; // Absent

                item.WorkDays = item.StdDays - item.LeaveDays;
                item.Percentage = Math.Round((item.WorkDays / item.StdDays) * 100, 2);

                sumTotalDays = sumTotalDays + totalDays;
                sumWH = sumWH + item.WH;
                sumPH = sumPH + item.PH;
                SumstdDays = SumstdDays + item.StdDays;

                if (LT1 != null)
                    sumLT1 = sumLT1 + item.LT1;
                if (LT2 != null)
                    sumLT2 = sumLT2 + item.LT2;
                if (LT3 != null)
                    sumLT3 = sumLT3 + item.LT3;
                if (LT4 != null)
                    sumLT4 = sumLT4 + item.LT4;
                if (LT5 != null)
                    sumLT5 = sumLT5 + item.LT5;
                if (LT6 != null)
                    sumLT6 = sumLT6 + item.LT6;
                if (LT7 != null)
                    sumLT7 = sumLT7 + item.LT7;
                if (LT8 != null)
                    sumLT8 = sumLT8 + item.LT8;
                if (LT9 != null)
                    sumLT9 = sumLT9 + item.LT9;
                if (LT10 != null)
                    sumLT10 = sumLT10 + item.LT10;
                if (LT11 != null)
                    sumLT11 = sumLT11 + item.LT11;
                if (LT12 != null)
                    sumLT12 = sumLT12 + item.LT12;


                sumUPL = sumUPL + item.UPL;
                sumAbs = sumAbs + item.AbsDays;
                sumLeaveDays = sumLeaveDays + item.LeaveDays;
                sumWorkDays = sumWorkDays + item.WorkDays;
                sumPercentage = sumPercentage + item.Percentage;
            }

            GetEmployeeLeaveDaysForPeriodResult obj = new GetEmployeeLeaveDaysForPeriodResult()
            {
                EmployeeId = 0,
                Name = "Total",
                TotalDays = sumTotalDays,
                WH = sumWH,
                PH = sumPH,
                StdDays = SumstdDays,
                LT1 = sumLT1,
                LT2 = sumLT2,
                LT3 = sumLT3,
                LT4 = sumLT4,
                LT5 = sumLT5,
                LT6 = sumLT6,
                LT7 = sumLT7,
                LT8 = sumLT8,
                LT9 = sumLT9,
                LT10 = sumLT10,
                LT11 = sumLT11,
                LT12 = sumLT12,
                UPL = sumUPL,
                AbsDays = sumAbs,
                LeaveDays = sumLeaveDays,
                WorkDays = sumWorkDays,
                Percentage = Math.Round(sumPercentage / list.Count, 2)
            };
            list.Add(obj);

            return list;
        }

        public static List<GetDesignationTransfersResult> GetDesignationTransfersNew(int start, int limit, string searchText, ref int totalRecords, int empId
            , DateTime? from, DateTime? to)
        {
            totalRecords = 0;
            List<GetDesignationTransfersResult> list = PayrollDataContext.GetDesignationTransfers(start, limit, SessionManager.CurrentCompanyId, searchText, empId, from, to).ToList();
            if (list.Count > 0)
                totalRecords = list[0].TotalRows.Value;



            return list;
        }

        public static List<GetLocationTransfersResult> GetLocationTransfersNew(int start, int limit, string searchText, int employeeId, ref int totalRecords)
        {
            List<GetLocationTransfersResult> list = PayrollDataContext.GetLocationTransfers(0, 9999999, SessionManager.CurrentCompanyId, searchText, employeeId).ToList();
            totalRecords = list[0].TotalRows.Value;

            if (start >= 0 && limit > 0)
            {
                list = list.Skip(start).Take(limit).ToList();
            }

            return list;
        }

        public static List<GetEmpSpecificLeaveDefListResult> GetEmpSpecLeaveDefList(int start, int limit, int type, int employeeId, int branchId, int departmentId, int recommenderId, int approvalId, ref int totalRecords)
        {
            List<GetEmpSpecificLeaveDefListResult> list = PayrollDataContext.GetEmpSpecificLeaveDefList(0, 999999, employeeId, branchId, departmentId, recommenderId, approvalId).ToList();

            list = list.Where(x => x.Type == type).ToList();

            totalRecords = list.Count;

            if (start >= 0 && limit > 0)
            {
                list = list.Skip(start).Take(limit).ToList();
            }

            return list;
        }

        public static Status SaveUpdateLeaveSpecificEmployee(LeaveSpecificEmployee obj)
        {
            Status status = new Status();

            LeaveSpecificEmployee dbObj = PayrollDataContext.LeaveSpecificEmployees.SingleOrDefault(x => x.EmployeeId == obj.EmployeeId && x.Type == obj.Type);
            if (dbObj != null)
            {
                dbObj.Type = obj.Type;
                dbObj.Recommender1 = obj.Recommender1;
                dbObj.Recommender2 = obj.Recommender2;
                dbObj.Approval1 = obj.Approval1;
                dbObj.Approval2 = obj.Approval2;
                PayrollDataContext.SubmitChanges();
                status.IsSuccess = true;
            }
            else
            {
                PayrollDataContext.LeaveSpecificEmployees.InsertOnSubmit(obj);
                PayrollDataContext.SubmitChanges();
                status.IsSuccess = true;
            }

            return status;
        }

        public static LeaveSpecificEmployee GetLeaveSpecificEmpById(int employeeId, int type)
        {
            return PayrollDataContext.LeaveSpecificEmployees.SingleOrDefault(x => x.EmployeeId == employeeId && x.Type == type);
        }

        public static Status DeleteLeaveSpecificEmployee(int employeeId, int type)
        {
            Status status = new Status();

            LeaveSpecificEmployee dbObj = PayrollDataContext.LeaveSpecificEmployees.SingleOrDefault(x => x.EmployeeId == employeeId && x.Type == type);
            if (dbObj == null)
            {
                status.ErrorMessage = "Leave specific definition is not found.";
                status.IsSuccess = false;
            }
            else
            {
                PayrollDataContext.LeaveSpecificEmployees.DeleteOnSubmit(dbObj);
                PayrollDataContext.SubmitChanges();
                status.IsSuccess = true;
            }

            return status;
        }

        public static List<GetMultiLevelEmpGroupingResult> GetMultiLevelEmpGrouping(int branchId, int departmentId)
        {
            List<GetMultiLevelEmpGroupingResult> list = PayrollDataContext.GetMultiLevelEmpGrouping(branchId, departmentId).ToList();
            return list;
        }

        public bool SavePhotograph(string fileLocation, int employeeId, string thumbnailLocation)
        {
            HHumanResource dbEntity = GetHumanResource(employeeId);
            bool isSaved = false;
            string oldFileUrl = "";

            oldFileUrl = dbEntity.tempUrlPhoto;
            dbEntity.UrlPhoto = fileLocation;
            dbEntity.UrlPhotoThumbnail = thumbnailLocation;
            dbEntity.tempUrlPhoto = null;
            dbEntity.tempUrlPhotoThumbnail = null;

            try
            {
                if (!string.IsNullOrEmpty(oldFileUrl))
                {
                    string url = Path.Combine(HttpContext.Current.Server.MapPath(Config.UploadLocation), oldFileUrl);
                    File.Delete(url);
                }
            }
            catch { }

            return UpdateChangeSet();

        }

        public static Status DeleteLocationTransfer(int locationHistoryId)
        {
            Status status = new Status();

            ELocationHistory dbELocationHistory = PayrollDataContext.ELocationHistories.SingleOrDefault(x => x.LocationHistoryId == locationHistoryId);

            if (dbELocationHistory != null)
            {
                int employeeId = dbELocationHistory.EmployeeId.Value;

                ELocationHistory nextELocationHistory = PayrollDataContext.ELocationHistories.Where(x => (x.EmployeeId == employeeId) && (x.FromDateEng > dbELocationHistory.FromDateEng)).OrderBy(x => x.FromDateEng).FirstOrDefault();

                if (nextELocationHistory != null)
                {
                    nextELocationHistory.FromLocationId = dbELocationHistory.FromLocationId;
                }

                ELocationHistory latestELocationHistory = PayrollDataContext.ELocationHistories.Where(x => x.EmployeeId == employeeId).OrderByDescending(x => x.FromDateEng).FirstOrDefault();

                if (latestELocationHistory.LocationHistoryId == locationHistoryId)
                {
                    EHumanResource dbEHumanResource = PayrollDataContext.EHumanResources.SingleOrDefault(x => x.EmployeeId == employeeId);
                    dbEHumanResource.LocationId = dbELocationHistory.FromLocationId;
                }

                PayrollDataContext.ELocationHistories.DeleteOnSubmit(dbELocationHistory);
                PayrollDataContext.SubmitChanges();
                status.IsSuccess = true;
            }
            else
            {
                status.ErrorMessage = "Data not found.";
                status.IsSuccess = false;
            }

            return status;
        }

        public static Status SaveDesignationHistoryImport(List<DesignationHistory> list)
        {
            Status status = new Status();

            SetConnectionPwd(PayrollDataContext.Connection);
            PayrollDataContext.Transaction = PayrollDataContext.Connection.BeginTransaction();

            try
            {
                PayrollDataContext.DesignationHistories.InsertAllOnSubmit(list);
                PayrollDataContext.SubmitChanges();

                foreach (var item in list.Select(x => x.EmployeeId).Distinct())
                {
                    EEmployee dbEEmployee = PayrollDataContext.EEmployees.SingleOrDefault(x => x.EmployeeId == item);
                    int designationId = PayrollDataContext.GetEmployeeCurrentPositionForDate(DateTime.Now.Date, item).Value;

                    if (dbEEmployee.DesignationId != designationId)
                        dbEEmployee.DesignationId = designationId;
                }

                PayrollDataContext.SubmitChanges();
                status.IsSuccess = true;
                PayrollDataContext.Transaction.Commit();
            }
            catch (Exception exp)
            {
                Log.log("Error while importing designation history", exp);
                PayrollDataContext.Transaction.Rollback();
                status.IsSuccess = false;
            }
            finally
            {

            }
            return status;
        }

        public static Status SaveUpdateEmpActivity(NewActivity obj)
        {
            Status status = new Status();

            if (obj.ActivityId.Equals(0))
            {
                if (PayrollDataContext.NewActivities.Any(x => x.DateEng.Value.Date == obj.DateEng.Value.Date && x.EmployeeId == obj.EmployeeId))
                {
                    status.ErrorMessage = "Activity already exists for the day.";
                    status.IsSuccess = false;
                    return status;
                }

                PayrollDataContext.NewActivities.InsertOnSubmit(obj);
            }
            else
            {
                NewActivity dbNewActivity = PayrollDataContext.NewActivities.SingleOrDefault(x => x.ActivityId == obj.ActivityId);

                if (dbNewActivity == null)
                {
                    status.ErrorMessage = "Error while updating activities.";
                    status.IsSuccess = false;
                    return status;
                }

                NewActivity dbObjNewActivity = PayrollDataContext.NewActivities.SingleOrDefault(x => x.EmployeeId == obj.EmployeeId && x.DateEng.Value.Date == obj.DateEng.Value.Date);
                if (dbObjNewActivity != null && dbObjNewActivity.ActivityId != obj.ActivityId)
                {
                    status.ErrorMessage = "Activity is already saved for the date.";
                    status.IsSuccess = false;
                    return status;
                }


                dbNewActivity.Date = obj.Date;
                dbNewActivity.DateEng = obj.DateEng;
                //PayrollDataContext.NewActivityDetails.DeleteAllOnSubmit(dbNewActivity.NewActivityDetails);
                //dbNewActivity.NewActivityDetails.AddRange(obj.NewActivityDetails);
            }

            PayrollDataContext.SubmitChanges();
            status.IsSuccess = true;
            return status;
        }

        public static List<GetActivityListForEmployeeResult> GetActivityForEmployee(int currentPage, int pageSize, int employeeId, int clientId, int activityTypeId, int type, DateTime? startDate, DateTime? endDate, bool all)
        {
            return PayrollDataContext.GetActivityListForEmployee(currentPage, pageSize, employeeId, clientId, activityTypeId, startDate, endDate, type, all).ToList();
        }

        public static List<GetActivityListForAdminResult> GetActivityForAdmin(int currentPage, int pageSize, int employeeId, int clientId, int activityTypeId, int type, DateTime? startDate, DateTime? endDate, bool all, int status)
        {
            return PayrollDataContext.GetActivityListForAdmin(currentPage, pageSize, employeeId, clientId, activityTypeId, startDate, endDate, type, all, status).ToList();
        }

        public static Status SaveNewActivitiesListAsApproved(List<GetActivityListForAdminResult> list)
        {
            Status status = new Status();

            SetConnectionPwd(PayrollDataContext.Connection);
            PayrollDataContext.Transaction = PayrollDataContext.Connection.BeginTransaction();

            try
            {
                foreach (var item in list)
                {
                    NewActivityDetail dbObj = PayrollDataContext.NewActivityDetails.SingleOrDefault(x => x.DetailId == item.DetailId);
                    dbObj.Status = (int)AtivityStatusEnum.Approved;
                    dbObj.ModifiedBy = SessionManager.CurrentLoggedInUserID;
                    dbObj.ModifiedOn = System.DateTime.Now;
                    PayrollDataContext.SubmitChanges();
                }

                status.IsSuccess = true;
                PayrollDataContext.Transaction.Commit();
            }
            catch (Exception exp)
            {
                Log.log("Error while marking activities as Read", exp);
                PayrollDataContext.Transaction.Rollback();
                status.IsSuccess = false;
            }
            finally
            {

            }
            return status;
        }

        public static Status SaveAndSendNewActivitiesList(List<GetActivityListForAdminResult> list)
        {
            Status status = new Status();

            SetConnectionPwd(PayrollDataContext.Connection);
            PayrollDataContext.Transaction = PayrollDataContext.Connection.BeginTransaction();

            try
            {
                foreach (var item in list)
                {
                    NewActivityDetail dbObj = PayrollDataContext.NewActivityDetails.SingleOrDefault(x => x.DetailId == item.DetailId);
                    dbObj.Status = (int)AtivityStatusEnum.SaveAndSend;
                    dbObj.ModifiedBy = SessionManager.CurrentLoggedInUserID;
                    dbObj.ModifiedOn = System.DateTime.Now;
                    PayrollDataContext.SubmitChanges();
                }

                status.IsSuccess = true;
                PayrollDataContext.Transaction.Commit();
            }
            catch (Exception exp)
            {
                Log.log("Error while saving activities as Save and Send", exp);
                PayrollDataContext.Transaction.Rollback();
                status.IsSuccess = false;
            }
            finally
            {

            }
            return status;
        }

        public static bool IsActivitySaved(DateTime date)
        {
            if (PayrollDataContext.NewActivityDetails.Any(x => x.EmployeeId == SessionManager.CurrentLoggedInEmployeeId && x.DateEng.Value.Date == date.Date))
                return true;

            return false;
        }

        public static DateTime GetFirstLoggedInDateTimeForEmployeeForToday()
        {
            return PayrollDataContext.ChangeUserActivities.Where(x => x.UserName == SessionManager.UserName).OrderByDescending(x => x.DateEng).FirstOrDefault().DateEng.Value;
        }

        public static Status SaveUpdateEmployeePlan(EmployeePlan obj)
        {
            Status status = new Status();

            if (obj.PlanId.Equals(0))
            {
                if (PayrollDataContext.EmployeePlans.Any(x => x.DateEng.Value.Date == obj.DateEng.Value.Date && x.EmployeeId == obj.EmployeeId))
                {
                    status.ErrorMessage = "Plan already exists for the day.";
                    status.IsSuccess = false;
                    return status;
                }

                PayrollDataContext.EmployeePlans.InsertOnSubmit(obj);
            }
            else
            {
                EmployeePlan dbEmployeePlan = PayrollDataContext.EmployeePlans.SingleOrDefault(x => x.PlanId == obj.PlanId);
                if (dbEmployeePlan == null)
                {
                    status.IsSuccess = false;
                    status.ErrorMessage = "Error while updating.";
                    return status;
                }

                PayrollDataContext.EmployeePlanDetails.DeleteAllOnSubmit(dbEmployeePlan.EmployeePlanDetails);

                dbEmployeePlan.EmployeePlanDetails.AddRange(obj.EmployeePlanDetails);
            }

            PayrollDataContext.SubmitChanges();
            status.IsSuccess = true;

            return status;
        }

        public static EmployeePlan GetEmployeePlanById(int planId)
        {
            return PayrollDataContext.EmployeePlans.SingleOrDefault(x => x.PlanId == planId);
        }

        public static Status DeleteEmployeePlan(int planId)
        {
            Status status = new Status();

            EmployeePlan dbObj = PayrollDataContext.EmployeePlans.SingleOrDefault(x => x.PlanId == planId);
            if (dbObj != null)
            {
                PayrollDataContext.EmployeePlans.DeleteOnSubmit(dbObj);
                PayrollDataContext.SubmitChanges();
                status.IsSuccess = true;
            }
            else
            {
                status.ErrorMessage = "Record not found.";
                status.IsSuccess = false;
            }

            return status;
        }

        public static List<GetEmployeePlanListForEmployeeResult> GetEmployeePlanListForEmp(int currentPage, int pageSize, int employeeId, int type, DateTime? startDate, DateTime? endDate, bool all)
        {
            return PayrollDataContext.GetEmployeePlanListForEmployee(currentPage, pageSize, employeeId, startDate, endDate, type, all).ToList();
        }

        public static EmployeePlan GetEmployeePlanByDate(DateTime date, int employeeId)
        {
            return PayrollDataContext.EmployeePlans.SingleOrDefault(x => x.DateEng.Value.Date == date.Date && x.EmployeeId == employeeId);
        }

        public static bool IsEmployeePlanUsedInActivities(int planId)
        {
            if (PayrollDataContext.NewActivities.Any(x => x.PlanId == planId))
                return true;
            else
                return false;
        }

        public static bool RedirectToEmployeePlan()
        {
            Setting setting = PayrollDataContext.Settings.FirstOrDefault();
            if (setting != null && setting.DailyActivityPlan != null && setting.DailyActivityPlan.Value)
                return true;
            else
                return false;
        }

        public static List<GetBranchResult> GetBranchEmployees(int branchId, int empId)
        {
            return PayrollDataContext.GetBranch(SessionManager.CurrentLoggedInEmployeeId, branchId, empId).ToList();
        }

        public static List<GetEmpPlanListForAdminResult> GetEmpPlanForAdmin(int currentPage, int pageSize, int employeeId, int type, DateTime? startDate, DateTime? endDate, bool all)
        {
            return PayrollDataContext.GetEmpPlanListForAdmin(currentPage, pageSize, employeeId, startDate, endDate, type, all).ToList();
        }

        public static Status SaveActivityComment(ActivityComment obj)
        {
            Status status = new Status();

            if (!string.IsNullOrEmpty(obj.Comment))
            {
                PayrollDataContext.ActivityComments.InsertOnSubmit(obj);
            }

            NewActivityDetail dbObjNewActivityDetail = PayrollDataContext.NewActivityDetails.SingleOrDefault(x => x.DetailId == obj.ActivityId);
            if (dbObjNewActivityDetail != null)
            {
                dbObjNewActivityDetail.ActivityId = dbObjNewActivityDetail.DetailId;
                dbObjNewActivityDetail.Status = (int)AtivityStatusEnum.SaveAndSend;
                dbObjNewActivityDetail.ModifiedBy = SessionManager.CurrentLoggedInUserID;
                dbObjNewActivityDetail.ModifiedOn = System.DateTime.Now;
            }

            PayrollDataContext.SubmitChanges();
            status.IsSuccess = true;

            return status;
        }
        public static Status ImportEmployeeInfo(List<EEmployee> list, List<HCitizenship> citizenList, List<HDrivingLicence> drivingList)
        {
            Status status = new Status();

            SetConnectionPwd(PayrollDataContext.Connection);
            PayrollDataContext.Transaction = PayrollDataContext.Connection.BeginTransaction();
            try
            {
                List<EEmployee> e = PayrollDataContext.EEmployees.ToList();
                List<HHumanResource> h = PayrollDataContext.HHumanResources.ToList();
                List<EHumanResource> eh = PayrollDataContext.EHumanResources.ToList();
                List<HPassport> p = PayrollDataContext.HPassports.ToList();

                foreach (var emp in list)
                {
                    EEmployee _EEmployee = e.FirstOrDefault(x => x.EmployeeId == emp.EmployeeId);
                    if (_EEmployee != null)
                    {
                        _EEmployee.Birthmark = emp.Birthmark;
                        _EEmployee.MarriageAniversaryEng = emp.MarriageAniversaryEng;
                        _EEmployee.MarriageAniversary = emp.MarriageAniversary;
                        _EEmployee.MotherTongue = emp.MotherTongue;
                    }

                    HHumanResource _HHumanResource = h.FirstOrDefault(x => x.EmployeeId == emp.EmployeeId);
                    if (_HHumanResource != null)
                    {
                        HHumanResource dbHHumanResource = emp.HHumanResources.FirstOrDefault(x => x.EmployeeId == emp.EmployeeId); //PayrollDataContext.HHumanResources.FirstOrDefault(x => x.EmployeeId == emp.EmployeeId);
                        if (dbHHumanResource != null)
                        {
                            _HHumanResource.BloodGroup = dbHHumanResource.BloodGroup;
                            _HHumanResource.Religion = dbHHumanResource.Religion;
                        }
                    }

                    EHumanResource _EHumanResource = eh.FirstOrDefault(x => x.EmployeeId == emp.EmployeeId);
                    if (_EHumanResource != null)
                    {
                        EHumanResource dbHumanResource = emp.EHumanResources.FirstOrDefault(x => x.EmployeeId == emp.EmployeeId); //PayrollDataContext.EHumanResources.FirstOrDefault(x => x.EmployeeId == emp.EmployeeId);
                        if (dbHumanResource != null)
                            _EHumanResource.IdCardNo = dbHumanResource.IdCardNo;
                    }

                    #region Passport
                    HPassport _HPassport = p.FirstOrDefault(x => x.EmployeeId == emp.EmployeeId);
                    HPassport dbPassport = emp.HPassports.FirstOrDefault(x => x.EmployeeId == emp.EmployeeId);
                    if (_HPassport != null && dbPassport != null)
                    {
                        _HPassport.PassportNo = dbPassport.PassportNo;
                        if (dbPassport.IssuingDateEng.ToString() != null && dbPassport.IssuingDateEng.ToString() != "")
                            _HPassport.IssuingDateEng = dbPassport.IssuingDateEng;
                        _HPassport.IssuingDate = dbPassport.IssuingDate;
                        if (dbPassport.ValidUptoEng.ToString() != null && dbPassport.ValidUptoEng.ToString() != "")
                            _HPassport.ValidUptoEng = dbPassport.ValidUptoEng;
                        _HPassport.ValidUpto = dbPassport.ValidUpto;
                    }
                    else
                    {
                        if (dbPassport != null)
                        {
                            HPassport _hpass = new HPassport();
                            if (dbPassport.PassportNo != null && dbPassport.PassportNo != "")
                            {
                                _hpass.EmployeeId = emp.EmployeeId;
                                _hpass.PassportNo = dbPassport.PassportNo;
                                if (dbPassport.IssuingDateEng.ToString() == null || dbPassport.IssuingDateEng.ToString() == "")
                                    _hpass.IssuingDateEng = null;
                                else
                                {
                                    _hpass.IssuingDateEng = dbPassport.IssuingDateEng;
                                    _hpass.IssuingDate = dbPassport.IssuingDate;
                                }
                                if (dbPassport.ValidUptoEng.ToString() == null || dbPassport.ValidUptoEng.ToString() == "")
                                    _hpass.ValidUptoEng = null;
                                else
                                {
                                    _hpass.ValidUptoEng = dbPassport.ValidUptoEng;
                                    _hpass.ValidUpto = dbPassport.ValidUpto;
                                }

                                _hpass.ApprovedOn = _hpass.ModifiedOn = DateTime.Now;
                                _hpass.ApprovedBy = _hpass.ModifiedBy = SessionManager.CurrentLoggedInUserID;
                                PayrollDataContext.HPassports.InsertOnSubmit(_hpass);
                            }
                        }
                    }

                    #endregion

                    #region citizen
                    HCitizenship citizenShip = citizenList.FirstOrDefault(x => x.EmployeeId == emp.EmployeeId);
                    if (citizenShip != null)
                    {
                        HCitizenship dbCitizenShip = PayrollDataContext.HCitizenships.FirstOrDefault(x => x.EmployeeId == emp.EmployeeId);
                        if (dbCitizenShip != null)
                        {
                            dbCitizenShip.Nationality = citizenShip.Nationality;
                            dbCitizenShip.CitizenshipNo = citizenShip.CitizenshipNo;
                            if (citizenShip.IssueDateEng.ToString() != null && citizenShip.IssueDateEng.ToString() != "")
                                dbCitizenShip.IssueDateEng = citizenShip.IssueDateEng;
                            dbCitizenShip.IssueDate = citizenShip.IssueDate;
                            dbCitizenShip.Place = citizenShip.Place;
                            dbCitizenShip.ModifiedOn = DateTime.Now;

                        }
                        if (!String.IsNullOrEmpty(citizenShip.CitizenshipNo) && citizenShip.CitizenshipNo != "" && dbCitizenShip == null)
                        {
                            if (citizenShip.IssueDateEng.ToString() != null && citizenShip.IssueDateEng.ToString() != "")
                                citizenShip.IssueDateEng = null;
                            citizenShip.ApprovedOn = citizenShip.ModifiedOn = DateTime.Now;
                            citizenShip.ApprovedBy = citizenShip.ModifiedBy = SessionManager.CurrentLoggedInUserID;
                            PayrollDataContext.HCitizenships.InsertOnSubmit(citizenShip);
                        }

                    }
                    #endregion

                    #region driving
                    HDrivingLicence DrivingShip = drivingList.FirstOrDefault(x => x.EmployeeId == emp.EmployeeId);
                    if (DrivingShip != null)
                    {

                        HDrivingLicence dbDrivningShip = PayrollDataContext.HDrivingLicences.FirstOrDefault(x => x.EmployeeId == emp.EmployeeId);
                        if (dbDrivningShip != null)
                        {
                            dbDrivningShip.LiscenceTypeName = DrivingShip.LiscenceTypeName;
                            dbDrivningShip.DrivingLicenceNo = DrivingShip.DrivingLicenceNo;
                            if (DrivingShip.DrivingLicenceIssueDate.ToString() != null && DrivingShip.DrivingLicenceIssueDate.ToString() != "")
                                dbDrivningShip.DrivingLicenceIssueDate = DrivingShip.DrivingLicenceIssueDate;
                            dbDrivningShip.IssuingCountry = DrivingShip.IssuingCountry;
                            dbDrivningShip.ModifiedOn = DateTime.Now;
                        }
                        if (!String.IsNullOrEmpty(DrivingShip.DrivingLicenceNo) && DrivingShip.DrivingLicenceNo != "" && dbDrivningShip == null)
                        {
                            if (DrivingShip.DrivingLicenceIssueDate.ToString() == null && DrivingShip.DrivingLicenceIssueDate.ToString() == "")
                                DrivingShip.DrivingLicenceIssueDate = null;
                            else
                                DrivingShip.DrivingLicenceIssueDate = DrivingShip.DrivingLicenceIssueDate.ToString();
                            DrivingShip.ApprovedOn = DrivingShip.ModifiedOn = DateTime.Now;
                            DrivingShip.ApprovedBy = DrivingShip.ModifiedBy = SessionManager.CurrentLoggedInUserID;
                            PayrollDataContext.HDrivingLicences.InsertOnSubmit(DrivingShip);
                        }
                    }

                    #endregion
                }

                PayrollDataContext.SubmitChanges();
                status.IsSuccess = true;
                PayrollDataContext.Transaction.Commit();
            }

            catch (Exception exp)
            {
                Log.log("Error while importing employee education.", exp);
                PayrollDataContext.Transaction.Rollback();
                status.IsSuccess = false;
            }
            finally
            {

            }
            return status;
        }

        public static Status ImportEmployeeEducation(List<HEducation> list)
        {
            Status status = new Status();

            SetConnectionPwd(PayrollDataContext.Connection);
            PayrollDataContext.Transaction = PayrollDataContext.Connection.BeginTransaction();
            int order = 1, employeeId = 0;

            list = list.OrderBy(x => x.EmployeeId).ToList();

            try
            {
                foreach (var item in list)
                {
                    if (employeeId == item.EmployeeId)
                        order++;
                    else
                        order = 1;

                    item.Order = order;
                    employeeId = item.EmployeeId.Value;
                }

                PayrollDataContext.HEducations.InsertAllOnSubmit(list);

                PayrollDataContext.SubmitChanges();
                status.IsSuccess = true;
                PayrollDataContext.Transaction.Commit();
            }
            catch (Exception exp)
            {
                Log.log("Error while importing employee education.", exp);
                PayrollDataContext.Transaction.Rollback();
                status.IsSuccess = false;
            }
            finally
            {

            }
            return status;
        }

        public static Status ImportEmpFamilyMembers(List<HFamily> list)
        {
            Status status = new Status();

            SetConnectionPwd(PayrollDataContext.Connection);
            PayrollDataContext.Transaction = PayrollDataContext.Connection.BeginTransaction();

            try
            {
                PayrollDataContext.HFamilies.InsertAllOnSubmit(list);
                PayrollDataContext.SubmitChanges();
                status.IsSuccess = true;
                PayrollDataContext.Transaction.Commit();
            }
            catch (Exception exp)
            {
                Log.log("Error while importing employee family members", exp);
                PayrollDataContext.Transaction.Rollback();
                status.IsSuccess = false;
            }
            finally
            {

            }
            return status;
        }

        public static Status SaveUpdateEmployeeActivityComment(ActivityEmpComment obj)
        {
            Status status = new Status();

            obj.CreatedOn = DateTime.Now;
            PayrollDataContext.ActivityEmpComments.InsertOnSubmit(obj);

            PayrollDataContext.SubmitChanges();
            return status;
        }

        public static ActivityEmpComment GetActivityEmpComment(int activityId, int employeeId)
        {
            return PayrollDataContext.ActivityEmpComments.SingleOrDefault(x => x.ActivityId == activityId && x.EmployeeId == employeeId);
        }

        public static ActivityComment GetLatestUnapprovedEmpActivityComment()
        {
            ActivityComment obj = (from ac in PayrollDataContext.ActivityComments
                                   join na in PayrollDataContext.NewActivityDetails
                                   on ac.ActivityId.Value equals na.ActivityId
                                   where na.Status.Value == (int)AtivityStatusEnum.SaveAndSend && na.EmployeeId == SessionManager.CurrentLoggedInEmployeeId
                                   orderby na.DateEng descending
                                   select ac).ToList().FirstOrDefault();

            return obj;
        }

        public static Status SaveHPLDutyPeriod(List<HPLDutyPeriod> list)
        {
            Status status = new Status();

            if (PayrollDataContext.HPLDutyPeriods.Any(x => x.Year == list[0].Year))
            {
                status.IsSuccess = false;
                status.ErrorMessage = "Duty period already generated.";
                return status;
            }

            PayrollDataContext.HPLDutyPeriods.InsertAllOnSubmit(list);
            PayrollDataContext.SubmitChanges();

            return status;
        }

        public static List<HPLDutyPeriod> GetHPLDutyPeriods(int year)
        {
            return PayrollDataContext.HPLDutyPeriods.Where(x => x.Year == year).OrderBy(x => x.StartDate).ToList();
        }

        public static List<HPLDutyPeriod> GetHPLDutyPeriodsForDateRange(DateTime startDate, DateTime endEndDate)
        {
            List<HPLDutyPeriod> list = PayrollDataContext.HPLDutyPeriods.Where(x => (x.StartDate >= startDate && x.EndDate <= endEndDate)).OrderBy(x => x.StartDate).ToList();

            if (PayrollDataContext.HPLDutyPeriods.Any(x => (startDate >= x.StartDate && startDate <= x.EndDate)))
            {
                HPLDutyPeriod objFirst = PayrollDataContext.HPLDutyPeriods.SingleOrDefault(x => (startDate >= x.StartDate && startDate <= x.EndDate));

                if (!list.Any(x => x.PeriodId == objFirst.PeriodId))
                    list.Insert(0, objFirst);
            }

            if (PayrollDataContext.HPLDutyPeriods.Any(x => (endEndDate >= x.StartDate && endEndDate <= x.EndDate)))
            {
                HPLDutyPeriod objLast = PayrollDataContext.HPLDutyPeriods.SingleOrDefault(x => (endEndDate >= x.StartDate && endEndDate <= x.EndDate));

                if (!list.Any(x => x.PeriodId == objLast.PeriodId))
                    list.Add(objLast);
            }

            return list;
        }

        public static HPLDutySchedule GetEmployeeDutyScheduleForPeriod(int employeeId, int periodId)
        {
            return PayrollDataContext.HPLDutySchedules.SingleOrDefault(x => x.EmployeeId == employeeId && x.PeriodId == periodId);
        }

        public static List<HPLDutyPeriod> GetHPLDutyPeriodsForYear(int year)
        {
            return PayrollDataContext.HPLDutyPeriods.Where(x => x.Year == year).OrderBy(x => x.StartDate).ToList();
        }

        public static Status SaveUpdateEmpDutySchedule(List<HPLDutySchedule> list)
        {
            Status status = new Status();

            if (PayrollDataContext.HPLDutySchedules.Any(x => x.EmployeeId == list[0].EmployeeId && x.Year == list[0].Year))
            {
                List<HPLDutySchedule> dbList = PayrollDataContext.HPLDutySchedules.Where(x => x.EmployeeId == list[0].EmployeeId && x.Year == list[0].Year).ToList();
                PayrollDataContext.HPLDutySchedules.DeleteAllOnSubmit(dbList);
                PayrollDataContext.HPLDutySchedules.InsertAllOnSubmit(list);
            }
            else
            {
                PayrollDataContext.HPLDutySchedules.InsertAllOnSubmit(list);
            }

            PayrollDataContext.SubmitChanges();
            status.IsSuccess = true;

            return status;
        }

        public static List<HPLDutySchedule> GetTopThreeHPLDutySchedules(int employeeId, int year)
        {
            return PayrollDataContext.HPLDutySchedules.Where(x => x.EmployeeId == employeeId && x.Year == year).OrderBy(x => x.ScheduleId).Take(3).ToList();
        }

        public static Status SaveUpdateScheduleEmployee(HPLScheduleEmployee obj)
        {
            Status status = new Status();

            if (PayrollDataContext.HPLScheduleEmployees.Any(x => x.EmployeeId == obj.EmployeeId && x.Year == obj.Year))
            {
                status.ErrorMessage = "Employee is already added.";
                status.IsSuccess = false;
            }
            else
            {
                PayrollDataContext.HPLScheduleEmployees.InsertOnSubmit(obj);
                PayrollDataContext.SubmitChanges();
                status.IsSuccess = true;
            }

            return status;
        }

        public static List<int> GetScheduleEmployeesByYear(int year)
        {
            return PayrollDataContext.HPLScheduleEmployees.Where(x => x.Year == year).Select(x => x.EmployeeId.Value).ToList();
        }

        public static List<int> GetScheduleEmployeesByYearForDashboard(int year)
        {
            List<int> list = (from hse in PayrollDataContext.HPLScheduleEmployees
                              join hds in PayrollDataContext.HPLDutySchedules
                              on hse.EmployeeId equals hds.EmployeeId
                              where hse.Year == hds.Year
                              select hse.EmployeeId.Value).Distinct().ToList();

            return list;
        }

        public static List<HPLDutyPeriod> GetHPLDutyPeriodsForDashboard()
        {
            List<HPLDutyPeriod> list = PayrollDataContext.HPLDutyPeriods.Where(x => x.StartDate.Value.Date >= DateTime.Today.Date && x.Year == DateTime.Today.Year).OrderBy(x => x.StartDate).ToList();

            if (PayrollDataContext.HPLDutyPeriods.Any(x => (DateTime.Today.Date >= x.StartDate && DateTime.Today.Date <= x.EndDate)))
            {
                HPLDutyPeriod objFirst = PayrollDataContext.HPLDutyPeriods.SingleOrDefault(x => (DateTime.Today.Date >= x.StartDate && DateTime.Today.Date <= x.EndDate));

                if (!list.Any(x => x.PeriodId == objFirst.PeriodId))
                    list.Insert(0, objFirst);
            }

            return list.Take(3).ToList();
        }

        public static List<HPLDutyPeriod> GetHPLDutyPeriodsForEmployeeDashboardOfSandip()
        {
            List<HPLDutyPeriod> list = PayrollDataContext.HPLDutyPeriods.Where(x => x.StartDate.Value.Date >= DateTime.Today.Date.AddDays(-7) && x.Year == DateTime.Today.Year).OrderBy(x => x.StartDate).ToList();

            if (PayrollDataContext.HPLDutyPeriods.Any(x => (DateTime.Today.Date >= x.StartDate && DateTime.Today.Date <= x.EndDate)))
            {
                HPLDutyPeriod objFirst = PayrollDataContext.HPLDutyPeriods.SingleOrDefault(x => (DateTime.Today.Date >= x.StartDate && DateTime.Today.Date <= x.EndDate));

                if (!list.Any(x => x.PeriodId == objFirst.PeriodId))
                    list.Insert(0, objFirst);
            }

            return list.Take(10).ToList();
        }

        public static Status DeleteHPLEmpSchedule(int employeeId, int year)
        {
            Status status = new Status();

            List<HPLDutySchedule> list = PayrollDataContext.HPLDutySchedules.Where(x => x.EmployeeId == employeeId && x.Year == year).ToList();
            if (list.Count > 0)
                PayrollDataContext.HPLDutySchedules.DeleteAllOnSubmit(list);

            HPLScheduleEmployee dbObj = PayrollDataContext.HPLScheduleEmployees.SingleOrDefault(x => x.EmployeeId == employeeId && x.Year == year);
            PayrollDataContext.HPLScheduleEmployees.DeleteOnSubmit(dbObj);

            PayrollDataContext.SubmitChanges();
            status.IsSuccess = true;
            return status;
        }

        public static Status SaveUpdateEmpActivityNew(NewActivityDetail obj)
        {
            Status status = new Status();

            if (obj.DetailId.Equals(0))
            {
                PayrollDataContext.NewActivityDetails.InsertOnSubmit(obj);
            }
            else
            {
                NewActivityDetail dbObj = PayrollDataContext.NewActivityDetails.SingleOrDefault(x => x.DetailId == obj.DetailId);
                if (dbObj == null)
                {
                    status.IsSuccess = false;
                    status.ErrorMessage = "Error while updating activity.";
                    return status;
                }

                dbObj.ModifiedBy = SessionManager.CurrentLoggedInUserID;
                dbObj.ModifiedOn = DateTime.Now;

                dbObj.ActivityType = obj.ActivityType;
                dbObj.ClientSoftwareIds = obj.ClientSoftwareIds;
                dbObj.Description = obj.Description;
                dbObj.ActivityDuration = obj.ActivityDuration;
            }

            PayrollDataContext.SubmitChanges();

            return status;
        }

        public static NewActivityDetail GetNewActivityDetailById(int detailId)
        {
            return PayrollDataContext.NewActivityDetails.SingleOrDefault(x => x.DetailId == detailId);
        }

        public static List<ActivityComment> GetHRActivityComments(int activityId)
        {
            return PayrollDataContext.ActivityComments.Where(x => x.ActivityId == activityId).OrderByDescending(x => x.CreatedOn).ToList();
        }

        public static ActivityEmpComment GetActivityEmpCommentById(int activityId)
        {
            return PayrollDataContext.ActivityEmpComments.SingleOrDefault(x => x.ActivityId == activityId);
        }

        public static DateTime GetEmpActivityThreshold()
        {
            DateTime thisDateTime = CommonManager.GetCurrentDateAndTime().Date;
            try
            {
                if (CommonManager.Setting.EmployeeActvityThreshold == (int)EmployeeActivityThreshold.CurrentDay)
                {
                    return thisDateTime.AddDays(-1);
                }
                else if (CommonManager.Setting.EmployeeActvityThreshold == (int)EmployeeActivityThreshold.ThisWeek)
                {
                    return thisDateTime.AddDays(-8);
                }
                else if (CommonManager.Setting.EmployeeActvityThreshold == (int)EmployeeActivityThreshold.ThisMonth)
                {
                    return new DateTime(thisDateTime.Year, thisDateTime.Month, 1).AddDays(-1);
                }
                else if (CommonManager.Setting.EmployeeActvityThreshold == (int)EmployeeActivityThreshold.LastMonth)
                {
                    return
                       thisDateTime.AddMonths(-1).AddDays(-1);
                }
                else if (CommonManager.Setting.EmployeeActvityThreshold == (int)EmployeeActivityThreshold.Past2Days)
                {
                    return thisDateTime.AddDays(-2);
                }
                return thisDateTime.AddDays(-1);
            }
            catch (Exception ex)
            {
                return thisDateTime.AddDays(-1);
            }
        }

        public static NewActivityDetail GetNewActivityDetailByActivityId(int activityId)
        {
            return PayrollDataContext.NewActivityDetails.Where(x => x.ActivityId == activityId).OrderByDescending(x => x.DateEng).FirstOrDefault();
        }

        public static Status ImportEmployeeTrainings(List<HTraining> list)
        {
            Status status = new Status();

            SetConnectionPwd(PayrollDataContext.Connection);
            PayrollDataContext.Transaction = PayrollDataContext.Connection.BeginTransaction();
            list = list.OrderBy(x => x.EmployeeId).ToList();

            try
            {
                PayrollDataContext.HTrainings.InsertAllOnSubmit(list);
                PayrollDataContext.SubmitChanges();
                status.IsSuccess = true;
                PayrollDataContext.Transaction.Commit();
            }
            catch (Exception exp)
            {
                Log.log("Error while importing employee training.", exp);
                PayrollDataContext.Transaction.Rollback();
                status.IsSuccess = false;
            }
            finally
            {

            }
            return status;
        }

        public static Status ImportEmpPrevWorkExperience(List<HPreviousEmployment> list)
        {
            Status status = new Status();

            SetConnectionPwd(PayrollDataContext.Connection);
            PayrollDataContext.Transaction = PayrollDataContext.Connection.BeginTransaction();
            list = list.OrderBy(x => x.EmployeeId).ToList();

            try
            {
                PayrollDataContext.HPreviousEmployments.InsertAllOnSubmit(list);
                PayrollDataContext.SubmitChanges();
                status.IsSuccess = true;
                PayrollDataContext.Transaction.Commit();
            }
            catch (Exception exp)
            {
                Log.log("Error while importing employee previous work experience.", exp);
                PayrollDataContext.Transaction.Rollback();
                status.ErrorMessage = exp.ToString();
                status.IsSuccess = false;
            }
            finally
            {

            }
            return status;
        }

        public static List<GetNewActivitySummaryReportResult> GetNewActivitySummaryReport(int payrollPeriodId, int employeeId)
        {
            PayrollPeriod objPayrollPeriod = PayrollDataContext.PayrollPeriods.SingleOrDefault(x => x.PayrollPeriodId == payrollPeriodId);
            DateTime startDate = objPayrollPeriod.StartDateEng.Value;
            DateTime endDate = objPayrollPeriod.EndDateEng.Value;

            int totalDays = (int)(endDate - startDate).TotalDays + 1;
            return PayrollDataContext.GetNewActivitySummaryReport(startDate, endDate, payrollPeriodId, totalDays, employeeId).ToList();
        }

        public static List<GetNewActivityDetailsReportResult> GetNewActivityDetailsReport(int payrollPeriodId, int employeeId)
        {
            PayrollPeriod objPayrollPeriod = PayrollDataContext.PayrollPeriods.SingleOrDefault(x => x.PayrollPeriodId == payrollPeriodId);
            DateTime startDate = objPayrollPeriod.StartDateEng.Value;
            DateTime endDate = objPayrollPeriod.EndDateEng.Value;

            int totalDays = (int)(endDate - startDate).TotalDays + 1;
            return PayrollDataContext.GetNewActivityDetailsReport(startDate, endDate, payrollPeriodId, totalDays, employeeId).ToList();
        }

        public static List<GetEmployeeEducationListResult> GetEmployeeEducations(int currentPage, int pageSize, int employeeId, string employeeName, int branchId,
            int levelId, int educationLevelId, int facultyId)
        {
            return PayrollDataContext.GetEmployeeEducationList(currentPage, pageSize, employeeId, employeeName, branchId, levelId, educationLevelId, facultyId).ToList();
        }

        public static List<GetEmployeeFamilyMemberListResult> GetEmployeeFamilyMembers(int currentPage, int pageSize, int employeeId, string employeeName,
            int branchId, int levelId, int relationId, string relations)
        {
            return PayrollDataContext.GetEmployeeFamilyMemberList(currentPage, pageSize, employeeId, employeeName, branchId, levelId, relationId, relations).ToList();
        }

        public static List<GetEmployeePrevExperienceListResult> GetEmployeePreviousExpiriences(int currentPage, int pageSize, int employeeId, string employeeName,
            int branchId, int levelId, int experienceCategoryId)
        {
            return PayrollDataContext.GetEmployeePrevExperienceList(currentPage, pageSize, employeeId, employeeName, branchId, levelId, experienceCategoryId).ToList();
        }

        public static List<GetEmployeeAddressResult> GetEmployeeAddressResult(int currentPage, int pageSize, int employeeId, string employeeName, int branchId, int levelId)
        {
            return PayrollDataContext.GetEmployeeAddress(currentPage, pageSize, employeeId, employeeName, branchId, levelId).ToList();
        }

        public static List<GetEmployeeInfoResult> GetEmployeeInfoResult(int currentPage, int pageSize, int employeeId, string employeeName, int branchId)
        {
            return PayrollDataContext.GetEmployeeInfo(currentPage, pageSize, employeeId, employeeName, branchId).ToList();
        }


        public static Status ImportHolidayList(List<HolidayListImport> list)
        {
            Status status = new Status();
            List<HolidayList> lst = new List<HolidayList>();
            SetConnectionPwd(PayrollDataContext.Connection);
            PayrollDataContext.Transaction = PayrollDataContext.Connection.BeginTransaction();
            try
            {
                foreach (var item in list)
                {

                    if (item.ToDate == null)
                        item.ToDate = item.FromDate;
                    int totalDays = 1;
                    DateTime startDateEng = item.FromDate;
                    DateTime curDate;
                    if (item.FromDate != item.ToDate)
                    {
                        TimeSpan ts = item.ToDate - item.FromDate;
                        totalDays += ts.Days;
                    }
                    for (int i = 0; i < totalDays; i++)
                    {
                        HolidayList _dbobj = new HolidayList();
                        curDate = startDateEng.AddDays(i);
                        var v = lst.FirstOrDefault(x => x.EngDate == curDate);
                        if (v != null)
                        {
                            status.ErrorMessage = "Duplicate holiday date:" + curDate.ToString("MM/dd/yyyy");
                            status.IsSuccess = false;
                            return status;
                        }

                        v = null;
                        v = PayrollDataContext.HolidayLists.FirstOrDefault(x => x.EngDate == curDate);
                        if (v != null)
                        {
                            // continue;
                            status.ErrorMessage = "Date: " + curDate.ToString("MM/dd/yyyy") + " is already exist on holiday list.";
                            status.IsSuccess = false;
                            return status;
                        }

                        _dbobj.CompanyId = item.CompanyId;
                        _dbobj.EngDate = curDate;
                        _dbobj.Date = BLL.BaseBiz.GetAppropriateDate(curDate);
                        _dbobj.Description = item.Description;
                        _dbobj.IsNationalHoliday = item.IsNationalHoliday;
                        if (item.IsNationalHoliday == true)
                            _dbobj.AppliesTo = -1;
                        else
                            _dbobj.AppliesTo = item.AppliesTo;
                        _dbobj.Created = DateTime.Now;

                        lst.Add(_dbobj);
                    }
                }
                PayrollDataContext.HolidayLists.InsertAllOnSubmit(lst);
                PayrollDataContext.SubmitChanges();
                status.IsSuccess = true;
                //status.ErrorMessage= "Import successfully of " + lst.Count + " records.";
                PayrollDataContext.Transaction.Commit();
            }
            catch (Exception exp)
            {
                Log.log("Error while importing holiday list.", exp);
                PayrollDataContext.Transaction.Rollback();
                status.IsSuccess = false;
            }
            finally
            {

            }
            return status;
        }


        public static Status SaveEmployeeAddressImport(List<EAddress> list)
        {
            Status status = new Status();
            SetConnectionPwd(PayrollDataContext.Connection);
            PayrollDataContext.Transaction = PayrollDataContext.Connection.BeginTransaction();

            try
            {
                foreach (var item in list)
                {
                    EAddress _dbobj = PayrollDataContext.EAddresses.SingleOrDefault(x => x.EmployeeId == item.EmployeeId);
                    if (_dbobj != null)
                    {
                        _dbobj.PECountryId = item.PECountryId;
                        _dbobj.PEZoneId = item.PEZoneId;
                        _dbobj.PEDistrictId = item.PEDistrictId;
                        _dbobj.PEVDCMuncipality = item.PEVDCMuncipality;
                        _dbobj.PEStreet = item.PEStreet;
                        _dbobj.PEWardNo = item.PEWardNo;
                        _dbobj.PEHouseNo = item.PEHouseNo;
                        _dbobj.PELocality = item.PELocality;
                        _dbobj.PEState = item.PEState;
                        _dbobj.PEZipCode = item.PEZipCode;


                        _dbobj.PSCountryId = item.PSCountryId;
                        _dbobj.PSZoneId = item.PSZoneId;
                        _dbobj.PSDistrictId = item.PSDistrictId;
                        _dbobj.PSVDCMuncipality = item.PSVDCMuncipality;
                        _dbobj.PSStreet = item.PSStreet;
                        _dbobj.PSWardNo = item.PSWardNo;
                        _dbobj.PSHouseNo = item.PSHouseNo;
                        _dbobj.PSLocality = item.PSLocality;
                        _dbobj.PSState = item.PSState;
                        _dbobj.PSZipCode = item.PSZipCode;
                        _dbobj.PSCitIssDis = item.PSCitIssDis;

                        _dbobj.CIEmail = item.CIEmail;
                        _dbobj.CIPhoneNo = item.CIPhoneNo;
                        _dbobj.Extension = item.Extension;
                        _dbobj.CIMobileNo = item.CIMobileNo;

                        _dbobj.PersonalEmail = item.PersonalEmail;
                        _dbobj.PersonalPhone = item.PersonalPhone;
                        _dbobj.PersonalMobile = item.PersonalMobile;


                        _dbobj.EmergencyName = item.EmergencyName;
                        _dbobj.EmergencyRelation = item.EmergencyRelation;
                        _dbobj.EmergencyPhone = item.EmergencyPhone;
                        _dbobj.EmergencyMobile = item.EmergencyMobile;
                    }
                    else
                    {
                        PayrollDataContext.EAddresses.InsertOnSubmit(item);
                    }
                }

                PayrollDataContext.SubmitChanges();
                status.IsSuccess = true;
                PayrollDataContext.Transaction.Commit();

            }
            catch (Exception exp)
            {
                Log.log("Error while importing employee address", exp);
                PayrollDataContext.Transaction.Rollback();
                status.IsSuccess = false;
                status.ErrorMessage = exp.Message.ToString();
            }
            finally
            {

            }
            return status;
        }

        private static bool IsEmployeeIncomeDeleteValid(GetEmployeeIncomeDetailsListResult income, int employeeId)
        {
            // as for quest, basic salary can also be overridden
            if (CommonManager.CompanySetting.WhichCompany != WhichCompany.Quest &&
                CommonManager.CompanySetting.WhichCompany != WhichCompany.Triveni)
            {
                if (income.IsBasicIncome)
                    return false;

            }
            PEmployeeIncome empIncome = new PayManager().GetEmployeeIncome(employeeId, income.IncomeId);

            if (empIncome == null)
                return false;

            if (empIncome.IsEnabled == false)
                return false;

            return true;
        }

        public static List<GetEmployeeIncomeDetailsListResult> GetEmployeeIncomeDetailsList(string employeeIdList, int incomeId, int branchId, int departmentId,
           int designationId, int levelId, string statusList, int applyStatus)
        {
            List<GetEmployeeIncomeDetailsListResult> list = PayrollDataContext.GetEmployeeIncomeDetailsList(employeeIdList, incomeId, branchId, departmentId,
                designationId, levelId, statusList, applyStatus).ToList();

            int employeeId = -1;
            //bool isReadOnlyUser = SessionManager.IsReadOnlyUser;

            foreach (var income in list)
            {

                employeeId = income.EmployeeId;

                string rowBackColor = !(income.IsIncomeEnabled.Value && income.IsEmployeeIncomeEnabled.Value)
                    ? "disabledIncomeDeduction" : "";
                bool isUnApplied = false;

                if (income.Source.ToLower() == "not applied")
                {
                    rowBackColor = "unappliedIncomeDeduction";
                    isUnApplied = true;
                }


                string unApplyOrSourceMatrixOrSourceAmount = "Un-Apply";
                if (income.IsMatrixTypeIncome != null && income.IsMatrixTypeIncome.Value)
                {
                    if (IsEmployeeIncomeDeleteValid(income, employeeId) && income.Source == "Salary Matrix")
                        unApplyOrSourceMatrixOrSourceAmount = "";
                    else if (IsEmployeeIncomeDeleteValid(income, employeeId))// if(IsDeleteValid(income, employeeId) && income.Source == "Salary Amount")
                        unApplyOrSourceMatrixOrSourceAmount = "Restore Matrix";
                }
                else if (IsEmployeeIncomeDeleteValid(income, employeeId))
                    unApplyOrSourceMatrixOrSourceAmount = "Un-Apply";

                string applyOrEditText = "";
                // If salary matrix & income is not associated then ..
                if (income.Source == "Salary Matrix" && income.EmployeeIncomeId == 0)
                    applyOrEditText = "Change";
                else
                    applyOrEditText = "Apply";

                bool showEditElseApply;
                // if Income is not applyed then Apply or Employee Income is disabled then "Apply" or else "Edit"
                if (income.EmployeeIncomeId == 0)
                {
                    showEditElseApply = true;
                    income.CurrentStatus = "Unapplied";
                }
                else
                {
                    showEditElseApply = false;
                    income.CurrentStatus = "Applied";
                }

                //rowBackColor,
                income.CalculationString = Utils.Helper.Util.GetConstantResource(income.Calculation);

                income.RateString = income.GetRate(SessionManager.DecimalPlaces);
                income.IsDeleteValid = IsEmployeeIncomeDeleteValid(income, income.EmployeeId);

                if (unApplyOrSourceMatrixOrSourceAmount != "Un-Apply")
                    income.IsDeleteValid = false;

                income.AmountString = income.GetAmount(SessionManager.DecimalPlaces);
                income.RowBackColor = rowBackColor;

                if (income.EmployeeIncomeId.Equals(0))
                    income.IsEmployeeIncomeEnabledIfApplied = "";
                else
                {
                    if (income.IsEmployeeIncomeEnabled != null && income.IsEmployeeIncomeEnabled.Value)
                        income.IsEmployeeIncomeEnabledIfApplied = "Yes";
                    else
                        income.IsEmployeeIncomeEnabledIfApplied = "No";
                }
            }

            return list;
        }

        public static Status AddIncomeToEmployees(List<int> employeeIds, int incomeId)
        {
            Status status = new Status();

            try
            {
                foreach (int employeeId in employeeIds)
                {
                    PayManager payMgr = new PayManager();

                    PayManager.EnableIncomeIFAlreadyAssociated(employeeId, incomeId);

                    payMgr.AddIncomeToEmployee(incomeId,
                            employeeId, false, SessionManager.CurrentCompanyId, -1, -1, -1);

                }
                status.IsSuccess = true;
            }
            catch (Exception ex)
            {
                status.ErrorMessage = "Error while adding income to employees.";
                status.IsSuccess = false;
            }


            return status;
        }

        public static Status DeleteIncomeFromEmployees(List<int> empIncomeIds, int incomeId, string sourceType)
        {
            Status status = new Status();

            int count = 0;

            try
            {
                foreach (int empIncomeId in empIncomeIds)
                {
                    PayManager pay = new PayManager();

                    if (sourceType.ToLower() == "restore matrix")
                    {
                        PayManager.ReverseSourceMatrixOrAmount(empIncomeId);
                        count++;
                    }
                    else
                    {
                        if (empIncomeId != 0)
                        {
                            bool isSuccess = pay.Delete(empIncomeId);
                            if (isSuccess)
                                count++;
                        }
                    }
                }

                status.ErrorMessage = string.Format("Income unapplied to {0} employees successfully.", count);
                status.IsSuccess = true;
            }
            catch (Exception ex)
            {
                status.ErrorMessage = "Error while unapplying income from employees.";
                status.IsSuccess = false;
            }

            return status;
        }


        /// <summary>
        /// Checks if deduction can be un-attached or not
        /// </summary>
        private static bool IsEmployeeDeductionDeleteValid(GetEmployeeDeductionDetailsListResult deduction, int employeeId)
        {
            return !EmployeeManager.HasEmployeeAndDeductionIncludedInCalculation(deduction.DeductionId, employeeId);
        }

        public static List<GetEmployeeDeductionDetailsListResult> GetEmployeeDeductionDetailsList(string employeeIdList, int deductionId, int branchId, int departmentId,
            int designationId, int levelId, string statusList, int applyStatus)
        {
            List<GetEmployeeDeductionDetailsListResult> list = PayrollDataContext.GetEmployeeDeductionDetailsList(employeeIdList, deductionId, branchId,
                departmentId, designationId, levelId, statusList, applyStatus).ToList();

            PayrollPeriod lastPeriod = CommonManager.GetLastPayrollPeriod();

            int periodId = 0;
            if (lastPeriod != null)
                periodId = lastPeriod.PayrollPeriodId;

            foreach (var deduction in list)
            {
                deduction.RateAmountString = deduction.RateAmount(SessionManager.DecimalPlaces, periodId);
                deduction.CalculationString = Utils.Helper.Util.GetConstantResource(deduction.Calculation);
                deduction.IsDeletable = IsEmployeeDeductionDeleteValid(deduction, deduction.EmployeeId);
                deduction.RowBackColor = (deduction.IsEnabled != null && deduction.IsEnabled == false) ? "#E6E6E6" : "";
            }

            return list;
        }

        public static Status DeleteDeductionFromEmployees(List<int> empDeductionIds, int deductionId)
        {
            Status status = new Status();

            int count = 0;

            try
            {
                foreach (int empDeductionId in empDeductionIds)
                {
                    PayManager pay = new PayManager();

                    PEmployeeDeduction entity = new PEmployeeDeduction();
                    entity.EmployeeDeductionId = empDeductionId;

                    pay.RemoveDeductionFromEmployee(entity);
                    count++;
                }

                status.ErrorMessage = string.Format("Deduction unapplied to {0} employees successfully.", count);
                status.IsSuccess = true;
            }
            catch (Exception ex)
            {
                status.ErrorMessage = "Error while unapplying deduction from employees.";
                status.IsSuccess = false;
            }

            return status;
        }

        public static Status AddDeductionToEmployees(List<int> employeeIds, int deductionId)
        {
            Status status = new Status();

            try
            {
                foreach (int employeeId in employeeIds)
                {
                    PayManager payMgr = new PayManager();
                    payMgr.AddDeductionToEmployee(deductionId, employeeId, false, SessionManager.CurrentCompanyId);

                }
                status.IsSuccess = true;
            }
            catch (Exception ex)
            {
                status.ErrorMessage = "Error while applying deduction to employees.";
                status.IsSuccess = false;
            }


            return status;
        }

        public static List<GetEmployeeLeaveDetailsListResult> GetGetEmployeeLeaveDetailsList(string employeeIdList, int leaveTypeId, int branchId, int departmentId,
            int designationId, int levelId, string statusList, int applyStatus)
        {
            return PayrollDataContext.GetEmployeeLeaveDetailsList(employeeIdList, leaveTypeId, branchId, departmentId, designationId, levelId,
                statusList, applyStatus, SessionManager.CurrentCompanyId).ToList();
        }

        public static Status AddLeaveToEmployees(List<GetEmployeeLeaveDetailsListResult> list, int leaveTypeId, out int count)
        {
            Status status = new Status();
            count = 0;
            LeaveAttendanceManager leaveMgr = new LeaveAttendanceManager();
            bool result = false;

            try
            {
                foreach (var item in list)
                {
                    result = leaveMgr.AddLeaveToEmployee(leaveTypeId,
                    item.EmployeeId, false, SessionManager.CurrentCompanyId, item.CurrentStatusValue.Value);

                    if (result)
                        count++;
                }
                status.IsSuccess = true;
            }
            catch (Exception ex)
            {
                status.ErrorMessage = "Error while applying leave to employees.";
                status.IsSuccess = false;
            }

            return status;
        }

        public static Status DeleteLeaveFromEmployees(List<GetEmployeeLeaveDetailsListResult> list, int leaveTypeId)
        {
            Status status = new Status();
            LeaveAttendanceManager mgr = new LeaveAttendanceManager();

            try
            {
                foreach (var item in list)
                {
                    LEmployeeLeave entity = new LEmployeeLeave();
                    entity.EmployeeId = item.EmployeeId;
                    entity.LeaveTypeId = leaveTypeId;

                    mgr.RemoveLeaveFromEmployee(entity);
                }
                status.IsSuccess = true;
            }
            catch (Exception ex)
            {
                status.ErrorMessage = "Error while unapplying leave to employees.";
                status.IsSuccess = false;
            }

            return status;
        }

        public static List<GetEmployeeMaritalAndTaxStatusListResult> GetEmployeeMaritalAndTaxStatusList(string employeeIdList, int branchId, int departmentId,
            int designationId, int levelId, string statusList, int subDepartmentId, int unitId, string maritalStatus, bool? hasCoupleTaxStatus)
        {
            return PayrollDataContext.GetEmployeeMaritalAndTaxStatusList(employeeIdList, branchId, departmentId, designationId, levelId,
                statusList, subDepartmentId, unitId, maritalStatus, hasCoupleTaxStatus).ToList();
        }

        public static Status UpdateEmployeeTaxMaritalStatus(int employeeId, string maritalStatus, bool? hasCoupleTaxStatus)
        {
            Status status = new Status();
            EEmployee emp = PayrollDataContext.EEmployees.SingleOrDefault(x => x.EmployeeId == employeeId);
            if (emp == null)
            {
                status.ErrorMessage = "Employee not found.";
                status.IsSuccess = false;
                return status;
            }

            if (emp.MaritalStatus != maritalStatus)
                PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(employeeId, (byte)MonetoryChangeLogTypeEnum.TaxStatus
                    , "Marital Status", emp.MaritalStatus, maritalStatus, LogActionEnum.Update));


            emp.MaritalStatus = maritalStatus;

            //Tax Status Log
            if (emp.HasCoupleTaxStatus != hasCoupleTaxStatus)
            {
                PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(emp.EmployeeId, (byte)MonetoryChangeLogTypeEnum.HasCoupleTaxStatus,
                    "Has Couple Tax Status", emp.HasCoupleTaxStatus, hasCoupleTaxStatus, LogActionEnum.Update));
            }


            emp.HasCoupleTaxStatus = hasCoupleTaxStatus;
            PayrollDataContext.SubmitChanges();
            status.IsSuccess = true;

            return status;
        }

        public static Status UpdateEmployeeMaritalAndTaxStatus(List<GetEmployeeMaritalAndTaxStatusListResult> list)
        {
            Status status = new Status();

            foreach (var item in list)
            {
                EEmployee emp = PayrollDataContext.EEmployees.SingleOrDefault(x => x.EmployeeId == item.EmployeeId);
                if (emp != null)
                {

                    if (emp.MaritalStatus != item.MaritalStatus)
                        PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(emp.EmployeeId, (byte)MonetoryChangeLogTypeEnum.TaxStatus
                            , "Marital Status", emp.MaritalStatus, item.MaritalStatus, LogActionEnum.Update));

                    emp.MaritalStatus = item.MaritalStatus;

                    //Tax Status Log
                    if (emp.HasCoupleTaxStatus != item.HasCoupleTaxStatus)
                    {
                        PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(emp.EmployeeId, (byte)MonetoryChangeLogTypeEnum.HasCoupleTaxStatus,
                            "Has Couple Tax Status", emp.HasCoupleTaxStatus, item.HasCoupleTaxStatus, LogActionEnum.Update));
                    }

                    emp.HasCoupleTaxStatus = item.HasCoupleTaxStatus;
                }

            }


            PayrollDataContext.SubmitChanges();

            status.IsSuccess = true;

            return status;
        }

        public static List<GetEmployeeChangeStatusResult> GetEmployeeChangeStatusList(int employeeId, int branchId, int departmentId,
             int designationId, int levelId, string statusList, int subDepartmentId, int unitId, int currentPage, int pageSize)
        {
            return PayrollDataContext.GetEmployeeChangeStatus(employeeId, branchId, departmentId, designationId, levelId,
                statusList, subDepartmentId, unitId, currentPage, pageSize).ToList();
        }

        public static bool IsFirstEmployeeCurrentStatus(int employeeId, int eCurrentStatusId)
        {
            ECurrentStatus dbFirstECurrentStatus = PayrollDataContext.ECurrentStatus.Where(x => x.EmployeeId == employeeId).OrderBy(x => x.FromDateEng).FirstOrDefault();
            if (dbFirstECurrentStatus != null && dbFirstECurrentStatus.ECurrentStatusId == eCurrentStatusId)
                return true;

            return false;
        }

        public static Status ImportBranchOrDepartmentTransfer(List<BranchDepartmentHistory> list)
        {

            Status sta = new Status();
            sta.IsSuccess = false;

            list = list.OrderBy(x => x.FromDateEng).ToList();

            SetConnectionPwd(PayrollDataContext.Connection);

            try
            {

                PayrollDataContext.Transaction = PayrollDataContext.Connection.BeginTransaction();

                foreach (BranchDepartmentHistory lst in list)
                {

                    if (PayrollDataContext.BranchDepartmentHistories.Any(x => x.EmployeeId == lst.EmployeeId && x.BranchId == lst.BranchId
                         && x.FromDateEng == lst.FromDateEng))
                    {
                        sta.ErrorMessage = string.Format("Transfer already exists for employee {0} - {1} to the branch \"{2}\" on {3}.", lst.EmployeeId,
                            EmployeeManager.GetEmployeeName(lst.EmployeeId.Value), BranchManager.GetBranchById(lst.BranchId.Value).Name
                            , lst.FromDate);
                        return sta;
                    }

                    EEmployee emp = EmployeeManager.GetEmployeeById(lst.EmployeeId.Value);

                    // if import has appointment date then do not auto insert IsFirst=true line
                    DateTime? appointmentDate = PayrollDataContext.GetAppointmentEngDate(lst.EmployeeId.Value);
                    if (appointmentDate != null && appointmentDate == lst.FromDateEng)
                    {

                    }
                    else
                    {
                        CommonManager.SaveFirstIBranchfNotExists(lst.EmployeeId.Value);
                    }


                    lst.CreatedBy = SessionManager.CurrentLoggedInUserID;
                    lst.ModifiedBy = SessionManager.CurrentLoggedInUserID;
                    lst.CreatedOn = DateTime.Now;
                    lst.ModifiedOn = lst.CreatedOn;
                    lst.IsFirst = false;





                    //set prev branch as FromBranchId
                    BranchDepartmentHistory prevHistory = PayrollDataContext.BranchDepartmentHistories
                        .Where(x => x.EmployeeId == lst.EmployeeId && x.FromDateEng < lst.FromDateEng)
                        .OrderByDescending(x => x.FromDateEng).FirstOrDefault();

                    if (prevHistory == null)
                    {
                        prevHistory = list
                        .Where(x => x.EmployeeId == lst.EmployeeId && x.FromDateEng < lst.FromDateEng)
                        .OrderByDescending(x => x.FromDateEng).FirstOrDefault();
                    }

                    if (prevHistory != null)
                    {
                        lst.IsFirst = false;
                        lst.FromBranchId = prevHistory.BranchId;
                        lst.FromDepartmentId = prevHistory.DepeartmentId;
                    }
                    else
                    {
                        lst.IsFirst = true;

                        if (appointmentDate != null && appointmentDate == lst.FromDateEng) { }
                        else
                        {
                            lst.FromBranchId = emp.BranchId;
                            lst.FromDepartmentId = emp.DepartmentId;
                        }
                    }

                    if (lst.IsDepartmentTransfer.Value)
                        lst.BranchId = lst.FromBranchId;

                    PayrollDataContext.BranchDepartmentHistories.InsertOnSubmit(lst);



                    emp.BranchId = lst.BranchId;
                    emp.DepartmentId = lst.DepeartmentId;

                    PayrollDataContext.SubmitChanges();
                }






                sta.IsSuccess = true;
                PayrollDataContext.Transaction.Commit();
            }
            catch (Exception exp)
            {
                Log.log("Error while importing.", exp);
                PayrollDataContext.Transaction.Rollback();
                sta.IsSuccess = false;
            }
            finally
            {

            }
            return sta;
        }

        public static Status UpdateEmployeeInsuranceClaimOnImport(List<SCClaim> list)
        {
            Status sta = new Status();
            sta.IsSuccess = false;
            foreach (var lst in list)
            {
                SCClaim obj = PayrollDataContext.SCClaims.Where(x => x.ClaimID == lst.ClaimID && x.LotRef_ID == lst.LotRef_ID && x.EmployeeId == lst.EmployeeId).SingleOrDefault();
                if (obj != null)
                {
                    obj.ModifiedBy = SessionManager.User.UserID;
                    obj.ModifiedOn = DateTime.Now;
                    if (lst.ReceivedAmount != null)
                        obj.Status = (int)ClaimStatus.Completed;
                    obj.Remarks = lst.Remarks;
                    obj.ReceivedDate = lst.ReceivedDate;
                    obj.ReceivedAmount = lst.ReceivedAmount;
                }
            }
            PayrollDataContext.SubmitChanges();
            sta.IsSuccess = true;
            return sta;
        }


        public static List<ActivityEmpComment> GetEmpActivityCommentsByActivityId(int activityId)
        {
            return PayrollDataContext.ActivityEmpComments.Where(x => x.ActivityId == activityId).OrderBy(x => x.Id).ToList();
        }


    }

}
