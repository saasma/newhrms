﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using DAL;
using Utils;
using System.Xml.Linq;
using Utils.Calendar;
using BLL.Entity;
using BLL.BO;
namespace BLL.Manager
{

    public class TADAManager : BaseBiz
    {

        public static DHTADAClaim GetTADAClaim(int requestID)
        {
            return PayrollDataContext.DHTADAClaims.FirstOrDefault(x => x.RequestID == requestID);
        }
        public static DHTADAClaim GetTADAClaim(int empId,int month,int year)
        {
            return PayrollDataContext.DHTADAClaims.FirstOrDefault(x=>x.EmployeeID==empId && x.Month==month && x.Year==year
                && x.Status != (int)TADAStatusEnum.Rejected);
        }
        public static List<DHTADAClaim> GetSelfClaimList()
        {
            List<DHTADAClaim> list = PayrollDataContext.DHTADAClaims.Where(x => x.EmployeeID == SessionManager.CurrentLoggedInEmployeeId)
                .OrderByDescending(x => x.Date).ToList();

            foreach (DHTADAClaim item in list)
            {
                item.StatusText = ((TADAStatusEnum)item.Status).ToString();
                item.MonthText = item.Year + " - " + DateHelper.GetMonthName(item.Month, IsEnglish);

                if (item.Status <= (int)TADAStatusEnum.Pending)
                {
                    item.RequestedTotal = item.Total;
                    item.Total = 0;
                }
            }

            return list;
        }

        public static List<GetTADARequestForManagerResult> GetTADAListForReview(int status,bool hasDate,int month,int year)
        {
            List<GetTADARequestForManagerResult> dbEntity = new List<GetTADARequestForManagerResult>();
            dbEntity = PayrollDataContext.GetTADARequestForManager(SessionManager.CurrentLoggedInEmployeeId, status,hasDate,month,year).ToList();

            foreach (var val1 in dbEntity)
            {
                val1.StatusText = ((TADAStatusEnum)val1.Status).ToString();
                val1.MonthText = val1.YEAR + " / " +
                    DateHelper.GetMonthName(val1.MONTH, IsEnglish);
            }
            return dbEntity;
        }
        public static List<GetTADARequestForAdminResult> GetTADAListForForward(int status, bool hasDate, int month, int year
            ,string search)
        {
            List<GetTADARequestForAdminResult> dbEntity = new List<GetTADARequestForAdminResult>();

            dbEntity = PayrollDataContext.GetTADARequestForAdmin(SessionManager.CurrentLoggedInEmployeeId, status, hasDate, month, year,search).ToList();

            foreach (var val1 in dbEntity)
            {
                val1.StatusText = ((TADAStatusEnum)val1.Status).ToString();
                val1.MonthText = val1.YEAR + " / " +
                    DateHelper.GetMonthName(val1.MONTH, IsEnglish);
            }
            return dbEntity;
        }
        public static Status RecommendApproveRequest(DHTADAClaim entity)
        {
            Status status = new Status();
            DHTADAClaim dbEntity = new DHTADAClaim();

            if (PayrollDataContext.DHTADAClaims.Any(x => x.RequestID == entity.RequestID))
            {

                dbEntity = PayrollDataContext.DHTADAClaims.Where(x => x.RequestID == entity.RequestID).SingleOrDefault();

                dbEntity.RequestedTotal = entity.DHTADAClaimLines.Sum(x =>
                       Convert.ToDecimal(x.RequestedAllowance1) + Convert.ToDecimal(x.RequestedAllowance2)
                       + Convert.ToDecimal(x.RequestedAllowance3) + Convert.ToDecimal(x.RequestedAllowance4)
                       + Convert.ToDecimal(x.RequestedAllowance5) + Convert.ToDecimal(x.RequestedAllowance6)
                       + Convert.ToDecimal(x.RequestedAllowance7) + Convert.ToDecimal(x.RequestedAllowance8));

                dbEntity.Status = entity.Status;
                dbEntity.DHTADAClaimLines.Clear();
                dbEntity.DHTADAClaimLines.AddRange(entity.DHTADAClaimLines);
                dbEntity.Total = entity.Total;

                

                if (dbEntity.Status == (int)TADAStatusEnum.Recommended)
                {
                   

                    dbEntity.RecommendedBy = SessionManager.CurrentLoggedInEmployeeId;
                    dbEntity.RecommendedOn = GetCurrentDateAndTime();
                }
                else
                {
                    dbEntity.ApprovedBy = SessionManager.CurrentLoggedInEmployeeId;
                    dbEntity.ApprovedOn = GetCurrentDateAndTime();
                }
            }
            PayrollDataContext.SubmitChanges();
            return status;
        }


        public static Status RecommendApproveRequestByHR(DHTADAClaim entity)
        {
            Status status = new Status();
            DHTADAClaim dbEntity = new DHTADAClaim();


            if (SessionManager.User.UserMappedEmployeeId == null)
            {
                status.IsSuccess = false;
                status.ErrorMessage = "Employee not matched with the admin.";
                return status;
            }

            if (PayrollDataContext.DHTADAClaims.Any(x => x.RequestID == entity.RequestID))
            {

                dbEntity = PayrollDataContext.DHTADAClaims.Where(x => x.RequestID == entity.RequestID).SingleOrDefault();

                dbEntity.RequestedTotal = entity.DHTADAClaimLines.Sum(x =>
                       Convert.ToDecimal(x.RequestedAllowance1) + Convert.ToDecimal(x.RequestedAllowance2)
                       + Convert.ToDecimal(x.RequestedAllowance3) + Convert.ToDecimal(x.RequestedAllowance4)
                       + Convert.ToDecimal(x.RequestedAllowance5) + Convert.ToDecimal(x.RequestedAllowance6)
                       + Convert.ToDecimal(x.RequestedAllowance7) + Convert.ToDecimal(x.RequestedAllowance8));

                dbEntity.Status = entity.Status;
                dbEntity.DHTADAClaimLines.Clear();
                dbEntity.DHTADAClaimLines.AddRange(entity.DHTADAClaimLines);
                dbEntity.Total = entity.Total;






                dbEntity.RecommendedBy = SessionManager.User.UserMappedEmployeeId;
                    dbEntity.RecommendedOn = GetCurrentDateAndTime();
                
                    dbEntity.ApprovedBy = SessionManager.User.UserMappedEmployeeId;
                    dbEntity.ApprovedOn = GetCurrentDateAndTime();
                
            }
            PayrollDataContext.SubmitChanges();
            return status;
        }

        public static Status HRRejectRequest(int requestId)
        {
            Status status = new Status();
            DHTADAClaim dbEntity = PayrollDataContext.DHTADAClaims.FirstOrDefault(x => x.RequestID == requestId);

            if (dbEntity == null)
            {
                status.ErrorMessage = "Request does not exists.";
                return status;
            }

            if (dbEntity.Status != (int)TADAStatusEnum.Approved)
            {
                status.ErrorMessage = "Only approved request can be rejected.";
                return status;
            }

            dbEntity.Status = (int)TADAStatusEnum.Rejected;
            dbEntity.ForwardedBy = SessionManager.CurrentLoggedInUserID;
            dbEntity.ForwardedOn = CommonManager.GetCurrentDateAndTime();
           
            PayrollDataContext.SubmitChanges();

            return status; 
        }


        public static Status ManagerRejectRequest(int requestId)
        {
            Status status = new Status();
            DHTADAClaim dbEntity = PayrollDataContext.DHTADAClaims.FirstOrDefault(x => x.RequestID == requestId);

            if (dbEntity == null)
            {
                status.ErrorMessage = "Request does not exists.";
                return status;
            }


            dbEntity.Status = (int)TADAStatusEnum.Rejected;
            dbEntity.ForwardedBy = SessionManager.CurrentLoggedInUserID;
            dbEntity.ForwardedOn = CommonManager.GetCurrentDateAndTime();

            PayrollDataContext.SubmitChanges();

            return status;
        }

        public static Status ForwardRequest(DHTADAClaim entity)
        {
            Status status = new Status();
            DHTADAClaim dbEntity = new DHTADAClaim();

            Setting setting = PayrollDataContext.Settings.FirstOrDefault();
            int? tadaIncomeId = null;
            if (setting != null)
                tadaIncomeId = setting.TADAIncomeId;

            if (PayrollDataContext.DHTADAClaims.Any(x => x.RequestID == entity.RequestID))
            {

                dbEntity = PayrollDataContext.DHTADAClaims.Where(x => x.RequestID == entity.RequestID).SingleOrDefault();

                dbEntity.RequestedTotal = entity.DHTADAClaimLines.Sum(x =>
                       Convert.ToDecimal(x.RequestedAllowance1) + Convert.ToDecimal(x.RequestedAllowance2)
                       + Convert.ToDecimal(x.RequestedAllowance3) + Convert.ToDecimal(x.RequestedAllowance4)
                       + Convert.ToDecimal(x.RequestedAllowance5) + Convert.ToDecimal(x.RequestedAllowance6)
                       + Convert.ToDecimal(x.RequestedAllowance7) + Convert.ToDecimal(x.RequestedAllowance8));

                dbEntity.Status = entity.Status;
                dbEntity.DHTADAClaimLines.Clear();
                dbEntity.DHTADAClaimLines.AddRange(entity.DHTADAClaimLines);
                dbEntity.Total = entity.Total;                
                dbEntity.Status = (int)TADAStatusEnum.Forwarded;
                dbEntity.ForwardedBy = SessionManager.CurrentLoggedInUserID;
                dbEntity.ForwardedOn = CommonManager.GetCurrentDateAndTime();

                // Assocaite TADA income is not associated with the employee
                // set income association also
                if (tadaIncomeId != null)
                {
                    if (PayrollDataContext.PEmployeeIncomes.Any(x => x.EmployeeId == dbEntity.EmployeeID && x.IncomeId == tadaIncomeId.Value) == false)
                    {


                        PEmployeeIncome overtimeInc = new PEmployeeIncome();
                        overtimeInc.EmployeeId = dbEntity.EmployeeID;
                        overtimeInc.IncomeId = tadaIncomeId.Value;
                        overtimeInc.IsValid = true;
                        overtimeInc.IsEnabled = true;

                        PayrollDataContext.PEmployeeIncomes.InsertOnSubmit(overtimeInc);


                    }
                }
            }
            PayrollDataContext.SubmitChanges();
            return status;
        }

        public static Status InsertUpdateTADA(DHTADAClaim request, List<DHTADAClaimLine> lines)
        {
            DHTADAClaim dbEntity = new DHTADAClaim();
            Status status = new Status();

            request.DHTADAClaimLines.AddRange(lines);

            if (lines != null)
            {
                if (lines.Sum(x => x.Total) !=null && lines.Sum(x=>x.Total)<=0)
                {
                    status.IsSuccess = false;
                    status.ErrorMessage = "Grand Total must be greater than 0";
                    return status;

                }
            }

            if (PayrollDataContext.DHTADAClaims.Any(x => x.RequestID == request.RequestID))
            {
                dbEntity = PayrollDataContext.DHTADAClaims.Where(x => x.RequestID == request.RequestID).SingleOrDefault();

                if (dbEntity.Status != (int)TADAStatusEnum.Saved)
                {
                   
                    status.ErrorMessage = "Forwarded TADA can not be updated.";
                    return status;
                }


                dbEntity.Date = request.Date;
                dbEntity.Month = request.Month;
                dbEntity.Year = request.Year;
                dbEntity.Total = request.Total;
                dbEntity.ApprovalID = request.ApprovalID;
                dbEntity.ApprovalName = request.ApprovalName;
                dbEntity.RecommendedBy = request.RecommendedBy;
                dbEntity.Status = request.Status;

                dbEntity.DHTADAClaimLines.Clear();
                dbEntity.DHTADAClaimLines.AddRange(request.DHTADAClaimLines);

            }
            else
            {

                if (PayrollDataContext.DHTADAClaims.Any(x => x.EmployeeID == request.EmployeeID
                    && x.Month == request.Month && x.Year == request.Year && x.Status!= (int)TADAStatusEnum.Rejected))
                {
                    status.ErrorMessage = "TADA for " + request.Year + " / " +
                        DateHelper.GetMonthName(request.Month,IsEnglish) + " already exists.";
                    return status;
                }

                request.CreatedBy = SessionManager.User.UserID;
                request.CreatedOn = GetCurrentDateAndTime();
                PayrollDataContext.DHTADAClaims.InsertOnSubmit(request);
            }
            PayrollDataContext.SubmitChanges();
            return status;
        }

        public static Status DeleteTADA(int requstId)
        {
            DHTADAClaim claim =
                PayrollDataContext.DHTADAClaims.FirstOrDefault(x => x.RequestID == requstId);
            Status status = new Status();
            if (claim.Status != (int)TADAStatusEnum.Saved)
            {
                status.ErrorMessage = "Only saved status can be deleted.";
                return status;
            }

            PayrollDataContext.DHTADAClaims.DeleteOnSubmit(claim);
            PayrollDataContext.SubmitChanges();

            return status;
        }
        public static Status InsertTADAFromExcel(DHTADAClaim claim)
        {
            Status status = new Status();
            if (PayrollDataContext.DHTADAClaims.Any(x => x.EmployeeID == claim.EmployeeID && x.Month == claim.Month && x.Year == claim.Year
                && x.Status!= (int)TADAStatusEnum.Rejected))
            {
                DHTADAClaim dbEntity = PayrollDataContext.DHTADAClaims.FirstOrDefault(x => x.EmployeeID == claim.EmployeeID && x.Month == claim.Month && x.Year == claim.Year
                    && x.Status!= (int)TADAStatusEnum.Rejected);
                if (dbEntity.Status != (int)TADAStatusEnum.Saved)
                {
                    status.IsSuccess = false;
                    status.ErrorMessage = "TADA status has already been changed.";
                    return status;
                }

                CopyObject<DHTADAClaim>(claim, ref dbEntity, "RequestID");

                dbEntity.DHTADAClaimLines.Clear();

                dbEntity.DHTADAClaimLines.AddRange(claim.DHTADAClaimLines);

            }
            else
            {
                PayrollDataContext.DHTADAClaims.InsertOnSubmit(claim);
            }
            PayrollDataContext.SubmitChanges();
            status.IsSuccess = true;
            return status;
        }

        public static DHTADAClaimLine FillClaimDataForExcel(DHTADAClaimLine claimInst, int month, int year, int p)
        {
            DHTADAClaimLine retLine = new DHTADAClaimLine();
            retLine = claimInst;
            if (PayrollDataContext.DHTADAClaims.Any(x => x.EmployeeID == p && x.Year == year && x.Month == month && x.Status != (int)TADAStatusEnum.Rejected))
            {
                int rowID = PayrollDataContext.DHTADAClaims.FirstOrDefault(x => x.EmployeeID == p && x.Year == year && x.Month == month
                    && x.Status != (int)TADAStatusEnum.Rejected).RequestID;
                if (PayrollDataContext.DHTADAClaimLines.Any(x => x.RequestID == rowID && x.LineDate == claimInst.LineDate))
                {
                    retLine = PayrollDataContext.DHTADAClaimLines.FirstOrDefault(x => x.RequestID == rowID && x.LineDate == claimInst.LineDate);
                }
                else
                {
                    retLine.Description = " ";
                    retLine.LocationFrom = " ";
                    retLine.LocationTo = " ";
                    retLine.RequestedAllowance1 = 0;
                    retLine.RequestedAllowance2 = 0;
                    retLine.RequestedAllowance3 = 0;
                    retLine.RequestedAllowance4 = 0;
                    retLine.RequestedAllowance5 = 0;
                    retLine.RequestedAllowance6 = 0;
                    retLine.RequestedAllowance7 = 0;
                    retLine.RequestedAllowance8 = 0;
                    retLine.Allowance1 = 0;
                    retLine.Allowance2 = 0;
                    retLine.Allowance3 = 0;
                    retLine.Allowance4 = 0;
                    retLine.Allowance5 = 0;
                    retLine.Allowance6 = 0;
                    retLine.Allowance7 = 0;
                    retLine.Allowance8 = 0;
                    retLine.Total = 0;
                }
            }
            else
            {
                retLine.Description = " ";
                retLine.LocationFrom = " ";
                retLine.LocationTo = " ";
                retLine.RequestedAllowance1 = 0;
                retLine.RequestedAllowance2 = 0;
                retLine.RequestedAllowance3 = 0;
                retLine.RequestedAllowance4 = 0;
                retLine.RequestedAllowance5 = 0;
                retLine.RequestedAllowance6 = 0;
                retLine.RequestedAllowance7 = 0;
                retLine.RequestedAllowance8 = 0;
                retLine.Allowance1 = 0;
                retLine.Allowance2 = 0;
                retLine.Allowance3 = 0;
                retLine.Allowance4 = 0;
                retLine.Allowance5 = 0;
                retLine.Allowance6 = 0;
                retLine.Allowance7 = 0;
                retLine.Allowance8 = 0;
                retLine.Total = 0;
            }


            return retLine;

        }
    }
}
