﻿using System.Collections.Generic;
using System.Linq;
using System.Web;
using DAL;
using System;
using Utils;

namespace BLL.Manager
{
    public class CompanyManager : BaseBiz
    {

       

        public int GetTotalCompanies()
        {
            var query = from e in PayrollDataContext.Companies
                        where e.Status == true
                        select new { Id = e.CompanyId };
            return query.Count();
        }

        public static void SaveAbsentToDate(string date)
        {
            Setting setting = PayrollDataContext.Settings.FirstOrDefault();
            setting.AttendanceUpto = date;
            if (!string.IsNullOrEmpty(date))
                setting.AttendanceUptoDate = GetEngDate(date, IsEnglish);
            else
                setting.AttendanceUptoDate = null;

            CommonManager.Setting.AttendanceUpto = setting.AttendanceUpto;
            CommonManager.Setting.AttendanceUptoDate = setting.AttendanceUptoDate;

            PayrollDataContext.SubmitChanges();
        }
        private PayrollPeriod GetFirstDefaultPayrollPeriod(string monthName,int month,int year,string name,int companyId,int financialYearId)
        {
          


            PayrollPeriod entity = new PayrollPeriod();
            //entity.PayrollPeriodId = GetIdFromGrid();
            entity.CompanyId = companyId;
            entity.Name = monthName + "/" + year.ToString();
            entity.Month = month;
            entity.Year = year;
            //entity.StartDate = calStartDate.SelectedDate.ToString();
            //entity.EndDate = calEndDate.SelectedDate.ToString();
            entity.FinancialDateId = financialYearId;
            return entity;
        }
        

        public bool Save(Company entity)
        {
            DeleteFromCache(CacheKey.DecimalPlaces.ToString());

            //Set Encash & LapseEncash pays on month just before one month of leave start month
            if (entity.LeaveStartMonth == 1)
            {
                entity.LeaveEncashTypePaysOn = 12;
                entity.LeaveLapseEncashTypePaysOn = 12;
            }
            else
            {
                entity.LeaveEncashTypePaysOn = entity.LeaveStartMonth - 1;
                entity.LeaveLapseEncashTypePaysOn = entity.LeaveStartMonth - 1;
            }
                

            entity.Created = entity.Modified = DateTime.Now;
            entity.AdjustmentColumn = HttpContext.GetGlobalResourceObject("Messages", "AdjustmentColumnName").ToString();
            // Set eng dates
            FinancialDate date = entity.FinancialDates[0];
            date.StartingDateEng = GetEngDate(date.StartingDate,entity.IsEnglishDate);
            date.EndingDateEng = GetEngDate(date.EndingDate, entity.IsEnglishDate);




            entity.ID = Guid.NewGuid();
            entity.Status = true;

            SetConnectionPwd(PayrollDataContext.Connection);          
            PayrollDataContext.Transaction = PayrollDataContext.Connection.BeginTransaction();


            try
            {
                PayrollDataContext.Companies.InsertOnSubmit(entity);
                PayrollDataContext.SubmitChanges();
              

                //CommonManager mgr = new CommonManager();
                //mgr.SavePayrollPeriod(GetFirstDefaultPayrollPeriod(date.))

                

                //add "Basic salary/income for the company to be added to all employees created later            
                PIncome pay = new PIncome();
                pay.Title = HttpContext.GetGlobalResourceObject("FixedValues", "BasicSalary").ToString();
                pay.Abbreviation = HttpContext.GetGlobalResourceObject("FixedValues", "BasicSalaryAbbr").ToString();
                pay.IsBasicIncome = true;
                pay.IsPFCalculated = true;
                pay.IsCountForLeaveEncasement = true;
                //pay.IsCountAsReimursement = false;
                pay.IsUpdatedWithLeave = false;
                pay.IsApplyOnceFor = false;
                pay.IsTaxable = true;
                pay.Calculation = DAL.IncomeCalculation.FIXED_AMOUNT;
                pay.CompanyId = entity.CompanyId;
                pay.IsEnabled = true;
             
                PayrollDataContext.PIncomes.InsertOnSubmit(pay);

                //add head office as branch
                Branch headBranch = new Branch();
                headBranch.IsHeadOffice = true;
                headBranch.CompanyId = entity.CompanyId;
                headBranch.Address = "";
                headBranch.Phone = "";
                headBranch.Fax = "";
                headBranch.Email = "";
                headBranch.HasSeperateRetirementFund = false;
                headBranch.RetirementFundACNo = "";
                headBranch.HasSeperateEmpInsurance = false;
                headBranch.EmpInsuranceACNo = "";
                headBranch.IsInRemoteArea = false;
                headBranch.BankAccountNo = "";
                headBranch.BankName = "";

                headBranch.Name = Utils.Helper.Util.GetTextResource("BranchHeadOfficeName");
                PayrollDataContext.Branches.InsertOnSubmit(headBranch);


                if (PayrollDataContext.Settings.Any() == false)
                {
                    Setting setting = new Setting();
                    setting.EnableMultipleAddOn = true;
                    // disalbe web service access without key for any new company
                    setting.DisabeOldAPIWithoutKey = true;
                    PayrollDataContext.Settings.InsertOnSubmit(setting);
                }

                //if (CommonManager.CalculationConstant.CompanyIsHPL.HasValue && CommonManager.CalculationConstant.CompanyIsHPL.Value)
                //{
                //    //if HPL company then create Other leave at the time of company creation time
                //    LLeaveType leave = new LLeaveType();
                //    leave.CompanyId = entity.CompanyId;
                //    leave.Title = "Other leave";
                //    leave.Abbreviation = Config.HPLOtherLeaveAbbr;
                //    leave.LegendColor = Config.HPLOtherLeaveColor;
                //    leave.Type = LeaveType.FULLYPAID;
                //    leave.FreqOfAccrual = LeaveAccrue.MANUALLY;
                //    leave.IsHalfDayAllowed = false;
                //    leave.IsHPLOtherLeave = true;
                //    leave.LeavePerPeriod = 0;

                //    LLeaveAppliesTo leaveAppliesTo = new LLeaveAppliesTo();
                //    leaveAppliesTo.AppliesTo =(int) JobStatusEnum.Trainee;//all emps
                //    leave.LLeaveAppliesTos.Add(leaveAppliesTo);

                //    PayrollDataContext.LLeaveTypes.InsertOnSubmit(leave);
                //}

                //make the company as default selected
                PayrollDataContext.ChangeIsDefaultCompany(entity.CompanyId);

                //create first PayrollPeriod as Default


                PayrollDataContext.SubmitChanges();
                PayrollDataContext.Transaction.Commit();
            }
            catch(Exception exp)
            {

                Utils.Log.log("Error while creating new company", exp);
                PayrollDataContext.Transaction.Rollback();
                return false;
            }
            finally
            {
                //if (PayrollDataContext.Connection != null)
                //{
                //    PayrollDataContext.Connection.Close();
                //}
            }
            return true;
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="entity"></param>
        /// <returns>true if updated,false if not, as the record may have been deleted also</returns>
        public bool Update(Company entity)
        {
            DeleteFromCache(CacheKey.DecimalPlaces.ToString());

            Company dbEntity = PayrollDataContext.Companies.SingleOrDefault(c => c.CompanyId == entity.CompanyId);
            //may be deleted
            if (dbEntity == null)
                return false;
            
            SetCompany(dbEntity, entity);
            dbEntity.EditSequence = entity.EditSequence;

            PFRFFund dbEntityFund = PayrollDataContext.PFRFFunds.SingleOrDefault(c => c.CompanyId == entity.CompanyId);

            /// as PF fund is optional
            if (entity.PFRFFunds.Count > 0)
            {

                if (dbEntity.PFRFFunds.Count > 0)
                    SetPFRF(dbEntityFund, entity.PFRFFunds[0]);
                else
                    dbEntity.PFRFFunds.Add(entity.PFRFFunds[0]);
            }


            dbEntity.Modified = DateTime.Now;
            UpdateChangeSet();
            return true;
        }


        private void SetCompany(Company dbEntity, Company entity)
        {
            dbEntity.Name = entity.Name;

            dbEntity.LeaveStartMonth = entity.LeaveStartMonth;


            dbEntity.ResponsiblePerson = entity.ResponsiblePerson;
            dbEntity.Address = entity.Address;
            dbEntity.PhoneNo = entity.PhoneNo;
            dbEntity.FaxNo = entity.FaxNo;
            dbEntity.Email = entity.Email;
            dbEntity.Website = entity.Website;
            dbEntity.RegistrationNo = entity.RegistrationNo;

            if (dbEntity.NetSalaryRoundOff != entity.NetSalaryRoundOff)
                PayrollDataContext.ChangeSettings.InsertOnSubmit(GetSettingChangeLog((byte)MonetoryChangeLogTypeEnum.Company, "Net Salary Round Off",
                    GetString(dbEntity.NetSalaryRoundOff), GetString(entity.NetSalaryRoundOff), LogActionEnum.Update));


            dbEntity.NetSalaryRoundOff = entity.NetSalaryRoundOff;


            dbEntity.HasPFRFFund = entity.HasPFRFFund;
            //dbEntity.HasEmployeeInsurance = entity.HasEmployeeInsurance;
            dbEntity.PFRFName = entity.PFRFName;
            dbEntity.PFRFAbbreviation = entity.PFRFAbbreviation;
            //dbEntity.HasCIT = entity.HasCIT;

            dbEntity.CompanyType = entity.CompanyType;
            dbEntity.PANNo = entity.PANNo;
            dbEntity.IROId = entity.IROId;
            dbEntity.TSOId = entity.TSOId;
            dbEntity.CITCompanyCode = entity.CITCompanyCode;
            dbEntity.CITBankName = entity.CITBankName;
            dbEntity.OtherFundFullName = entity.OtherFundFullName;
            dbEntity.OtherFundAbbreviation = entity.OtherFundAbbreviation;
            dbEntity.OtherFundCode = entity.OtherFundCode;
            dbEntity.HasOtherFundLikeCIT = entity.HasOtherFundLikeCIT;
            dbEntity.EmployeeSelfPaidOtherFund = entity.EmployeeSelfPaidOtherFund;
        }


       

        public void SetPFRF(PFRFFund dbEntity, PFRFFund entity)
        {

            #region "Change Logs"
            if (dbEntity.IsApproved != entity.IsApproved)
                PayrollDataContext.ChangeSettings.InsertOnSubmit(GetSettingChangeLog((byte)MonetoryChangeLogTypeEnum.PF, "Is Approved",
                    GetString(dbEntity.IsApproved), GetString(entity.IsApproved), LogActionEnum.Update));

            if (dbEntity.ApprovalDate != entity.ApprovalDate)
                PayrollDataContext.ChangeSettings.InsertOnSubmit(GetSettingChangeLog((byte)MonetoryChangeLogTypeEnum.PF, "Approval Date",
                    GetString(dbEntity.ApprovalDate), GetString(entity.ApprovalDate),  LogActionEnum.Update));

            if (dbEntity.EmployeeContribution != entity.EmployeeContribution)
                PayrollDataContext.ChangeSettings.InsertOnSubmit(GetSettingChangeLog((byte)MonetoryChangeLogTypeEnum.PF, "Employee Contribution",
                    GetString(dbEntity.EmployeeContribution), GetString(entity.EmployeeContribution),  LogActionEnum.Update));

            if (dbEntity.CompanyContribution != entity.CompanyContribution)
                PayrollDataContext.ChangeSettings.InsertOnSubmit(GetSettingChangeLog((byte)MonetoryChangeLogTypeEnum.PF, "Company Contribution",
                    GetString(dbEntity.CompanyContribution), GetString(entity.CompanyContribution), LogActionEnum.Update));

            if (dbEntity.EffectiveFrom != entity.EffectiveFrom)
                PayrollDataContext.ChangeSettings.InsertOnSubmit(GetSettingChangeLog((byte)MonetoryChangeLogTypeEnum.PF, "Effective From",
                    JobStatus.GetValueForDisplay(dbEntity.EffectiveFrom),// ((JobStatusEnum)dbEntity.EffectiveFrom).ToString(),
                    JobStatus.GetValueForDisplay(entity.EffectiveFrom),//((JobStatusEnum)entity.EffectiveFrom).ToString(), 
                    LogActionEnum.Update));
            #endregion

            dbEntity.PFRFNo = entity.PFRFNo;
            // dbEntity.IsMultipleGroup = entity.IsMultipleGroup;
            dbEntity.IsApproved = entity.IsApproved;
            dbEntity.ApprovalDate = entity.ApprovalDate;
            dbEntity.ApprovalDateEng = entity.ApprovalDateEng;
            dbEntity.EmployeeContribution = entity.EmployeeContribution;
            dbEntity.CompanyContribution = entity.CompanyContribution;
            dbEntity.EffectiveFrom = entity.EffectiveFrom;
            dbEntity.CITReportDescription = entity.CITReportDescription;
            dbEntity.HasOtherPFFund = entity.HasOtherPFFund;
            dbEntity.OtherPFFundName = entity.OtherPFFundName;
        }

        public static bool IsPFUsedInSalary(int companyId)
        {
            int pfType = (int)CalculationColumnType.IncomePF;

            var query = from p in PayrollDataContext.CCalculationHeaders
                        join c in PayrollDataContext.CCalculations on p.CalculationId equals c.CalculationId
                        join pr in PayrollDataContext.PayrollPeriods on c.PayrollPeriodId equals pr.PayrollPeriodId
                        where pr.CompanyId == companyId &&
                         p.SourceId == pfType && p.Type == pfType && PayrollDataContext.CCalculationHeaders.Any()

                        select p;


            return query.Count() >= 1;

        }

        public FinancialDate GetCurrentFinancialDate(int companyId)
        {
            return PayrollDataContext.FinancialDates.SingleOrDefault
                (e => (e.CompanyId == companyId && e.IsCurrent == true));

        }

        public bool Delete(Company entity)
        {
            return PayrollDataContext.DeleteCompany(entity.CompanyId) == 1;            
        }

        public Company GetById(int companyId)
        {
            return PayrollDataContext.Companies.SingleOrDefault(c => c.CompanyId == companyId);
        }


        public List<Company> GetAllCompanies()
        {
            return PayrollDataContext.Companies.Where(
                e => (e.Status == true)).OrderBy(e => e.Name).ToList();
        }


        public string ChangeIsDefault(int companyId)
        {
            return PayrollDataContext.ChangeIsDefaultCompany(companyId).SingleOrDefault().Name;

        }

        public int GetDefault()
        {
            return (int) PayrollDataContext.GetDefaultCompanyId1().Value;
        }
    }
}
