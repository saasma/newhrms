﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using DAL;
using Utils;
using Utils.Calendar;
using Utils.Helper;
using System.Xml.Linq;
using System.Data.Linq;
using BLL.BO;
using BLL.Entity;
using System.Transactions;
using System.Web.UI;


namespace BLL.Manager
{
    public class TrainingManager : BaseBiz
    {
        public static GetMaxValueResult GetMax()
        {
            return PayrollDataContext.GetMaxValue().FirstOrDefault();
           
        }

        //public static SaveAttendenceResult SaveTraningAssesment(string FromName, string Subject, string Body, int CreatedBy, DateTime CreatedOn, string DueDate, DateTime DueDateEng)
        //{
        //    return PayrollDataContext.SaveAttendence(FromName, Subject, Body, CreatedBy, CreatedOn, DueDate, DueDateEng).FirstOrDefault();
        //}

        public static GetcallForRecommendResult GetCallForRecommend(int EmployeeId)
        {
            return PayrollDataContext.GetcallForRecommend(EmployeeId).FirstOrDefault(); 
        }


        public static GetSubmissionReportResult GetSubmissionReport(int BranchId, string Name, DateTime @StartDate, DateTime @EndDate, int DegisnationId)
        {
            return PayrollDataContext.GetSubmissionReport(BranchId, Name, StartDate, EndDate, DegisnationId).FirstOrDefault();
        }

        public static GetTrainingAssesmentEmployeeResult GetTraningAssesmentEmployee(int EmployeeId)
        {
            //List<int> customRoleDepartmentIDList = SessionManager.CustomRoleDeparmentList;

           // return PayrollDataContext.TrainingAssessmentEmployees.Where(x => x.EmployeeId == EmployeeId).FirstOrDefault();
            return PayrollDataContext.GetTrainingAssesmentEmployee(EmployeeId).FirstOrDefault();

        }

        public static bool Save(TrainingAssessment entity)
        {
            //PayrollDataContext.SaveTraningAssesment(entity);

            PayrollDataContext db = new PayrollDataContext();
            db.TrainingAssessments.InsertOnSubmit(entity);
            db.SubmitChanges();

            return true;
        }

        public static bool SaveRequest(TrainingAssesmentRequest entity)
        {
            //PayrollDataContext.SaveTraningAssesment(entity);

            PayrollDataContext db = new PayrollDataContext();
            db.TrainingAssesmentRequests.InsertOnSubmit(entity);
            db.SubmitChanges();

            return true;
        }

       public static UpdateTrainingAssesmentRequestResult Update(int Id, string Comment, bool Status, DateTime RecommendationDateEng)
       {
           //PayrollDataContext db = new PayrollDataContext();
           //db.UpdateTrainingAssesmentRequest(Id,Comment,Status,RecommendationDateEng);
           //db.SubmitChanges();

           return PayrollDataContext.UpdateTrainingAssesmentRequest(Id,Comment,Status,RecommendationDateEng).FirstOrDefault();

           
       
            //PayrollDataContext.SaveTraningAssesment(entity);

            //PayrollDataContext db = new PayrollDataContext();
            //db.TrainingAssessments.InsertOnSubmit(entity);
            //db.SubmitChanges();

            //return true;
        }

        public static bool SaveTraning(List<TrainingAssessmentEmployee> lstEntity, TrainingAssessmentEmployee entity)
        {
            foreach (var val1 in lstEntity)
            {
                TrainingAssessmentEmployee dbInstance = new TrainingAssessmentEmployee();
                dbInstance.EmployeeId = val1.EmployeeId;
                dbInstance.AssessmentId = entity.AssessmentId;
                PayrollDataContext.TrainingAssessmentEmployees.InsertOnSubmit(dbInstance);
               // return true;
            }
            PayrollDataContext.SubmitChanges();
            return true;
        }

        public static List<TrainingResoucrePerson> GetAllTrainingResoucrePerson()
        {
            return PayrollDataContext.TrainingResoucrePersons.ToList();
        }
        public static Status SaveUpdateTrainingRP(TrainingResoucrePerson obj)
        {
            Status status = new Status();
            PayrollDataContext.TrainingResoucrePersons.InsertOnSubmit(obj);
            PayrollDataContext.SubmitChanges();
            status.IsSuccess = true;
            return status;
        }

        public static int GetMaxResoucrePersonID()
        {
            if (!PayrollDataContext.TrainingResoucrePersons.ToList().Any())
                return PayrollDataContext.EEmployees.OrderByDescending(x => x.EmployeeId).ToList().Take(1).SingleOrDefault().EmployeeId;
            else
                return PayrollDataContext.TrainingResoucrePersons.OrderByDescending(x => x.RPID).ToList().Take(1).SingleOrDefault().RPID;
        }

        public static Status SaveUpdateTraining(Training obj,List<TrainingPlanningLine>listobjTrainingLine, bool sendNotification)//, bool sendEmail)
        {
            Status status = new Status();

            if (obj.TrainingID.Equals(0))
            {
                obj.CreatedBy = SessionManager.CurrentLoggedInUserID;
                obj.CreatedOn = System.DateTime.Now;
                obj.ModifiedBy = SessionManager.CurrentLoggedInUserID;
                obj.ModifiedOn = System.DateTime.Now;
                PayrollDataContext.Trainings.InsertOnSubmit(obj);
                obj.TrainingPlanningLines.AddRange(listobjTrainingLine);
                //  status.IsSuccess = true;
            }
            else
            {
                if (sendNotification)
                    SendNotification(obj, false);

                //if (sendEmail)
                //    SendEmail(obj, false);

                Training dbEntity = PayrollDataContext.Trainings.SingleOrDefault(x => x.TrainingID == obj.TrainingID);

                dbEntity.Name = obj.Name;
                dbEntity.ModifiedOn = System.DateTime.Now;
                dbEntity.ModifiedBy = SessionManager.CurrentLoggedInUserID;
                dbEntity.FromDate = obj.FromDate;
                dbEntity.ToDate = obj.ToDate;
                dbEntity.Days = obj.Days;
                dbEntity.StartTime = obj.StartTime;
                dbEntity.EndTime = obj.EndTime;
                dbEntity.Hours = obj.Hours;
                dbEntity.Type = obj.Type;
                dbEntity.AvailableSeats = obj.AvailableSeats;
                dbEntity.LevelIds = obj.LevelIds;
                dbEntity.Venue = obj.Venue;
                dbEntity.Host = obj.Host;
                dbEntity.Country = obj.Country;
                dbEntity.City = obj.City;
                dbEntity.ResourcePerson = obj.ResourcePerson;
                dbEntity.Notes = obj.Notes;
                dbEntity.TrainingPlanningLines.AddRange(listobjTrainingLine);
                // PayrollDataContext.SubmitChanges();
                //status.IsSuccess = true;
            }

            List<TrainingPlanningLine> DbTrainingPlanningLine = PayrollDataContext.TrainingPlanningLines.Where(x => x.TrainingRef_ID == obj.TrainingID).ToList();
            if (DbTrainingPlanningLine.Any())
            {
                PayrollDataContext.TrainingPlanningLines.DeleteAllOnSubmit(DbTrainingPlanningLine);
            }
           
            PayrollDataContext.SubmitChanges();
            status.IsSuccess = true;
            return status;
        }

        public static List<GetTrainingRatingCountResult> GetTrainingRatingCount(int TrainingID)
        {
            return PayrollDataContext.GetTrainingRatingCount(TrainingID).ToList();
        }

        public static List<Training> GetTrainingEvents()
        {
            //List<Training> list = list = PayrollDataContext.Trainings.Where(x => x.FromDate >= System.DateTime.Now && x.Status == (int)TrainingStatusEnum.Publish).OrderBy(x => x.Name).ToList();
            List<Training> list = list = PayrollDataContext.Trainings.Where(x => (x.FromDate.Value.Month >= System.DateTime.Now.Date.Month && x.FromDate.Value.Year >= System.DateTime.Now.Year) && x.Status == (int)TrainingStatusEnum.Publish).OrderBy(x => x.Name).ToList();

            foreach (var item in list)
            {
                item.BookedSeats = PayrollDataContext.TrainingRequests.Where(x => x.TrainingID == item.TrainingID && x.Status == (int)TrainingRequestStatusEnum.Approved).ToList().Count;
                item.LeftSeats = item.AvailableSeats.Value - item.BookedSeats;
                item.TrainingType = ((TrainingTypeEnum)item.Type).ToString();
            }

            return list;
        }

        public static Training GetTrainingById(int trainingId)
        {
            return PayrollDataContext.Trainings.SingleOrDefault(x => x.TrainingID == trainingId);
        }

        public static bool IsTrainingAccessibleByEmployee(int trainingId,int empId)
        {
            return PayrollDataContext.TrainingRequests.Any(x => x.TrainingID == trainingId && x.EmployeeId == empId);
        }

        public static TrainingRating GetTrainingRatingOfEmployeeByTraining(int trainingId, int EmployeeID)
        {
            return PayrollDataContext.TrainingRatings.SingleOrDefault(x => x.TrainingID == trainingId && x.EmployeeId==EmployeeID);

        }

        public static TrainingPlanningLine GetTrainingLineByIdAndDate(int trainingId, DateTime DateEng)
        {
            return PayrollDataContext.TrainingPlanningLines.SingleOrDefault(x => x.TrainingRef_ID == trainingId && x.DateEng == DateEng && x.IsTraining==true);
        }

        public static TrainingPlanningLine GetTrainingLineByLineId(Guid LineID)
        {
            return PayrollDataContext.TrainingPlanningLines.SingleOrDefault(x => x.LineID == LineID);
        }

        public static Status DeleteTraining(int trainingId)
        {
            Status status = new Status();

            if (PayrollDataContext.TrainingRequests.Any(x => x.TrainingID == trainingId))
            {
                status.ErrorMessage = "Training is in use, cannot be deleted.";
                status.IsSuccess = false;
                return status;
            }

            Training dbEntity = PayrollDataContext.Trainings.SingleOrDefault(x => x.TrainingID == trainingId);
            PayrollDataContext.Trainings.DeleteOnSubmit(dbEntity);
            PayrollDataContext.SubmitChanges();
            status.IsSuccess = true;            

            return status;
        }

        public static Status PublishTraining(int trainingId)
        {
            Status status = new Status();

            Training dbEntity = PayrollDataContext.Trainings.SingleOrDefault(x => x.TrainingID == trainingId);
            dbEntity.Status = (int)TrainingStatusEnum.Publish;
            dbEntity.ModifiedBy = SessionManager.CurrentLoggedInUserID;
            dbEntity.ModifiedOn = System.DateTime.Now;
            PayrollDataContext.SubmitChanges();
            status.IsSuccess = true;

            return status;
        }

        public static Status UnpublishTraining(int trainingId, bool sendNotifice)
        {
            Status status = new Status();

            Training dbTraining = PayrollDataContext.Trainings.SingleOrDefault(x => x.TrainingID == trainingId);            

            //List<TrainingRequest> listTr = PayrollDataContext.TrainingRequests.Where(x => x.TrainingID == trainingId).ToList();
            //if (listTr.Count > 0)
            //    PayrollDataContext.TrainingRequests.DeleteAllOnSubmit(listTr);

            if (sendNotifice)
                SendNotification(dbTraining, true);

            //if (sendMail)
            //    SendEmail(dbTraining, true);

            Training dbEntity = PayrollDataContext.Trainings.SingleOrDefault(x => x.TrainingID == trainingId);
            dbEntity.Status = (int)TrainingStatusEnum.Unpublish;
            dbEntity.ModifiedBy = SessionManager.CurrentLoggedInUserID;
            dbEntity.ModifiedOn = System.DateTime.Now;
            PayrollDataContext.SubmitChanges();
            status.IsSuccess = true;

            return status;
        }

        public static List<Training> GetTrainingEventsHistory(DateTime? dateFilterFrom, DateTime? dateFilterTo)
        {
            List<Training> list = new List<Training>();

            if ((dateFilterFrom == null || dateFilterFrom == DateTime.Parse("0001-01-01 12:00:00 AM")) && (dateFilterTo == null || dateFilterTo == DateTime.Parse("0001-01-01 12:00:00 AM")))
                list = PayrollDataContext.Trainings.Where(x => (x.FromDate.Value.Month < System.DateTime.Now.Month && x.FromDate.Value.Date.Year <= System.DateTime.Now.Year) && x.Status == (int)TrainingStatusEnum.Publish).OrderBy(x => x.Name).ToList();
            else
            {
                list = PayrollDataContext.Trainings.Where(x => (x.FromDate.Value.Month < System.DateTime.Now.Month && x.FromDate.Value.Date.Year <= System.DateTime.Now.Year) && x.Status == (int)TrainingStatusEnum.Publish).OrderBy(x => x.Name).ToList();

                if ((dateFilterFrom > DateTime.Parse("0001-01-01 12:00:00 AM")) && (dateFilterTo > DateTime.Parse("0001-01-01 12:00:00 AM")))
                    list = list.Where(x => x.FromDate.Value.Date >= dateFilterFrom.Value.Date && x.FromDate.Value.Date <= dateFilterTo.Value.Date).ToList();
                else if (dateFilterFrom > DateTime.Parse("0001-01-01 12:00:00 AM"))
                    list = list.Where(x => x.FromDate.Value.Date >= dateFilterFrom.Value.Date).ToList();
                else if(dateFilterTo > DateTime.Parse("0001-01-01 12:00:00 AM"))
                    list = list.Where(x => x.FromDate.Value.Date <= dateFilterTo.Value.Date).ToList();

            }
            
            foreach (var item in list)
            {
                item.BookedSeats = PayrollDataContext.TrainingRequests.Where(x => x.TrainingID == item.TrainingID && x.Status == (int)TrainingRequestStatusEnum.Approved).ToList().Count;
                item.LeftSeats = item.AvailableSeats.Value - item.BookedSeats;
                item.TrainingType = ((TrainingTypeEnum)item.Type).ToString();
            }

            return list;
        }
        
        public static List<Training> GetTrainingsUnpublished()
        {
            List<Training> list = PayrollDataContext.Trainings.Where(x => x.Status != (int)TrainingStatusEnum.Publish).OrderBy(x => x.Name).ToList();
            foreach (var item in list)
            {
                item.BookedSeats = PayrollDataContext.TrainingRequests.Where(x => x.TrainingID == item.TrainingID && x.Status == (int)TrainingRequestStatusEnum.Approved).ToList().Count;
                item.LeftSeats = item.AvailableSeats.Value - item.BookedSeats;
                if(item.Type!=null)
                item.TrainingType = ((TrainingTypeEnum)item.Type).ToString();
            }
            return list;
        }

        public static List<ResourcePersongBO> GetResourePersons()
        {
            List<EEmployee> listInteralRP = EmployeeManager.GetAllEmployees();
            List<TrainingResoucrePerson> _listExternalRP = TrainingManager.GetAllTrainingResoucrePerson();

            List<ResourcePersongBO> _ListResourcePersongBO = new List<ResourcePersongBO>();
            foreach (var item in listInteralRP)
            {
                _ListResourcePersongBO.Add(new ResourcePersongBO() { ResourcePersonId = item.EmployeeId, Name = item.Name });
            }

            foreach (var item in _listExternalRP)
            {
                _ListResourcePersongBO.Add(new ResourcePersongBO() { ResourcePersonId = item.RPID, Name = item.Name });
            }
            return _ListResourcePersongBO;
        }
        public static List<Training> GetTrainingsByMonth(int month, int year)
        {
            List<Training> list = new List<Training>();

            list = PayrollDataContext.Trainings.Where(x => (x.FromDate.Value.Month == month && x.FromDate.Value.Year == year)).OrderBy(x => x.Name).ToList();
            foreach (var item in list)
            {
                int bookedSeats = PayrollDataContext.TrainingRequests.Where(x => x.TrainingID == item.TrainingID).ToList().Count;
                item.LeftSeats = item.AvailableSeats.Value - bookedSeats;
            }

            return list;
        }

        public static List<Training> GetPublishedTrainingByMonth(int month, int year)
        {
            List<Training> list = new List<Training>();

            list = PayrollDataContext.Trainings.Where(x => (x.FromDate.Value.Month == month && x.FromDate.Value.Year == year && x.Status == (int)TrainingStatusEnum.Publish)).OrderBy(x => x.Name).ToList();
            foreach (var item in list)
            {
                
                int bookedSeats = PayrollDataContext.TrainingRequests.Where(x => x.TrainingID == item.TrainingID).ToList().Count;
                item.BookedSeats = bookedSeats;
                item.LeftSeats = item.AvailableSeats.Value - bookedSeats;
            }

            return list;
        }

        public static List<Training> GetTrainingsByStartEndDate(DateTime startDate, DateTime endDate)
        {
            return PayrollDataContext.Trainings.Where(x => x.FromDate >= startDate && x.FromDate <= endDate).ToList();
        }

        public static List<Training> GetLatestTrainingList()
        {
            DateTime todayDate = System.DateTime.Now;
            int year = todayDate.Year;
            int month = todayDate.Month;
            int daysInMonth = 0;

            if (month == 12)
            {
                month = 1;
                year = year + 1;
            }
            else
                month = month + 1;


            daysInMonth = DateHelper.GetTotalDaysInTheMonth(year, month, true);

            DateTime checkDate = new DateTime(year, month, daysInMonth);



            List<Training> list = PayrollDataContext.Trainings.Where(x => x.Status == (int)TrainingStatusEnum.Publish && x.FromDate > checkDate).OrderBy(x => x.FromDate).ToList();
            foreach (var item in list)
            {
                int bookedSeats = PayrollDataContext.TrainingRequests.Where(x => x.TrainingID == item.TrainingID).ToList().Count;
                item.LeftSeats = item.AvailableSeats.Value - bookedSeats;
            }
            return list;
        }

        public static bool IsTrainingSeatRemaining(int trainingId)
        {

            int totalSeats = PayrollDataContext.Trainings.SingleOrDefault(x => x.TrainingID == trainingId).AvailableSeats.Value;
            int allocatedSeats = PayrollDataContext.TrainingRequests.Where(x => x.TrainingID == trainingId && x.Status == (int)TrainingRequestStatusEnum.Approved).ToList().Count;
            
            if (allocatedSeats < totalSeats)
                return true;
            else
                return false;
        }


        public static Status AssignEmployeeTrainingRequest(List<TrainingRequest> objAssignList,int TrainingId,bool sendEmailNotification)
        {
            Status status = new Status();

            Training training = GetTrainingById(TrainingId);

            List<TrainingRequest> _listdb = PayrollDataContext.TrainingRequests.Where(x => x.TrainingID == TrainingId 
            && x.Status ==(int)TrainingRequestStatusEnum.Approved).ToList();
            if (_listdb.Any())
                PayrollDataContext.TrainingRequests.DeleteAllOnSubmit(_listdb);

            PayrollDataContext.TrainingRequests.InsertAllOnSubmit(objAssignList);
            status.IsSuccess = true;

            //foreach (TrainingRequest obj in objAssignList)
            //{

            //    if (PayrollDataContext.TrainingRequests.Any(x => x.TrainingID == obj.TrainingID && x.EmployeeId == obj.EmployeeId && x.TrainingRequestId != obj.TrainingRequestId))
            //    {
            //        status.ErrorMessage = "Training Request is already created.";
            //        status.IsSuccess = false;
            //        return status;
            //    }
            //    PayrollDataContext.TrainingRequests.InsertOnSubmit(obj);
            //}

           

            // insert/update HTraining table also
            List<HTraining> dbList = PayrollDataContext.HTrainings.Where(x => x.TrainingRef_ID == TrainingId).ToList();
            PayrollDataContext.HTrainings.DeleteAllOnSubmit(dbList);
            List<HTraining> newTrainingList = new List<HTraining>();

            TrainingType tranType = PayrollDataContext.TrainingTypes.FirstOrDefault(x => x.TrainingTypeId == training.Type);
            CountryList country = PayrollDataContext.CountryLists.FirstOrDefault(x => x.CountryId == training.Country);

            foreach(var item in objAssignList)
            {
                HTraining tran = new HTraining();
                tran.TrainingRef_ID = TrainingId;
                tran.EmployeeId = item.EmployeeId.Value;
                tran.TrainingName = training.Name;
                tran.InstitutionName = training.Host;
                tran.TrainingFrom = GetAppropriateDate(training.FromDate.Value);
                tran.TrainingTo = GetAppropriateDate(training.ToDate.Value);
                tran.TrainingTypeID = training.Type; ;
                tran.TrainingTypeName = (tranType == null ? "" : tranType.TrainingTypeName);
                tran.Country = country == null ? "" : country.CountryName;

                // duration as days
                tran.Duration = (int)training.Days.Value;
                tran.DurationTypeID = 4;// 4 as days
                tran.DurationTypeName = "Day";

                tran.TrainingFromEngDate = training.FromDate.Value;
                tran.TrainingToEngDate = training.ToDate.Value;
                tran.ResourcePerson = training.ResourcePerson;

                tran.Modifiedby = SessionManager.CurrentLoggedInUserID;
                tran.ModifiedOn = GetCurrentDateAndTime();

                tran.ApprovedBy = SessionManager.CurrentLoggedInUserID;
                tran.ApprovedOn = GetCurrentDateAndTime();

                tran.Venue = training.Venue;
                tran.Status = 1;// as approved

                newTrainingList.Add(tran);
            }

            PayrollDataContext.HTrainings.InsertAllOnSubmit(newTrainingList);
            PayrollDataContext.SubmitChanges();

            if (sendEmailNotification)
            {
                Training obj = GetTrainingById(TrainingId);
                status = SendEmail(obj);
            }

            return status;
        }

        public static Status SaveUpdateTrainingRequest(TrainingRequest obj)
        {
            Status status = new Status();

            if (PayrollDataContext.TrainingRequests.Any(x => x.TrainingID == obj.TrainingID && x.EmployeeId == obj.EmployeeId && x.TrainingRequestId != obj.TrainingRequestId))
            {
                status.ErrorMessage = "Training Request is already created.";
                status.IsSuccess = false;
                return status;
            }

            if (obj.TrainingRequestId.Equals(0))
            {
                PayrollDataContext.TrainingRequests.InsertOnSubmit(obj);
                PayrollDataContext.SubmitChanges();
                status.IsSuccess = true;
            }
            else
            {
                TrainingRequest dbEntity = PayrollDataContext.TrainingRequests.SingleOrDefault(x => x.TrainingRequestId == obj.TrainingRequestId);
                dbEntity.TrainingID = obj.TrainingID;
                dbEntity.Notes = obj.Notes;
                PayrollDataContext.SubmitChanges();
                status.IsSuccess = true;
            }

            return status;
        }

        public static Status SaveUpdateTrainingFeedback(TrainingRating obj)
        {
            Status status = new Status();

            TrainingRating dbEntity = PayrollDataContext.TrainingRatings.SingleOrDefault(x => x.TrainingID == obj.TrainingID && x.EmployeeId == obj.EmployeeId);
            if (dbEntity != null)
                PayrollDataContext.TrainingRatings.DeleteOnSubmit(dbEntity);

            PayrollDataContext.TrainingRatings.InsertOnSubmit(obj);
            PayrollDataContext.SubmitChanges();
            status.IsSuccess = true;
            return status;
        }

        public static List<TrainingRequest> GetTrainingRequestsByEmpId(int employeeId)
        {
            List<TrainingRequest> list = PayrollDataContext.TrainingRequests.Where(x => x.EmployeeId == employeeId && x.RequestedDate >= System.DateTime.Now).ToList();

            foreach (var item in list)
            {
                Training obj = TrainingManager.GetTrainingById(item.TrainingID.Value);
                item.Name = obj.Name;
                item.FromDate = obj.FromDate.Value;
                item.ToDate = obj.ToDate.Value;
                item.Days = obj.Days.Value;
            }

            return list.OrderBy(x => x.Name).ToList();
        }


        public static List<TrainingRequest> GetTrainingAssigedEmpByTrainingId(int TrainingID)
        {
            return PayrollDataContext.TrainingRequests.Where(x => x.TrainingID == TrainingID && x.Status == (int)TrainingRequestStatusEnum.Approved).ToList();
        }

        public static List<TrainingRequest> GetTrainingRequestHistoryByEmpId(int employeeId, DateTime? dateFilter)
        {
            List<TrainingRequest> list = new List<TrainingRequest>();

            if (dateFilter == null || dateFilter == DateTime.Parse("0001-01-01 12:00:00 AM"))
                list = PayrollDataContext.TrainingRequests.Where(x => x.EmployeeId == employeeId && x.RequestedDate < System.DateTime.Now).ToList();
            else
                list = PayrollDataContext.TrainingRequests.Where(x => x.EmployeeId == employeeId && x.RequestedDate >= dateFilter && x.RequestedDate < System.DateTime.Now).ToList();

            foreach (var item in list)
            {
                Training obj = TrainingManager.GetTrainingById(item.TrainingID.Value);
                item.Name = obj.Name;
                item.FromDate = obj.FromDate.Value;
                item.ToDate = obj.ToDate.Value;
                item.Days = obj.Days.Value;
            }

            return list.OrderBy(x => x.Name).ToList();
        }

        public static TrainingRequest GetTrainingRequestById(int trainingRequestId)
        {
            return PayrollDataContext.TrainingRequests.SingleOrDefault(x => x.TrainingRequestId == trainingRequestId);
        }

        public static Status DeleteTrainingRequest(int trainingRequestId)
        {
            Status status = new Status();

            if (PayrollDataContext.TrainingRequests.Any(x => x.TrainingRequestId == trainingRequestId && x.Status != (int)TrainingRequestStatusEnum.Pending))
            {
                status.ErrorMessage = "Training Request is in use, cannot be deleted.";
                status.IsSuccess = false;
                return status;
            }

            TrainingRequest dbEntity = TrainingManager.GetTrainingRequestById(trainingRequestId);
            PayrollDataContext.TrainingRequests.DeleteOnSubmit(dbEntity);
            PayrollDataContext.SubmitChanges();
            status.IsSuccess = true;

            return status;
        }        

        public static List<Training> GetTrainingRequestForEmployee(int employeeId)
        {
            List<Training> list = (from tr in PayrollDataContext.Trainings
                                   join trReq in PayrollDataContext.TrainingRequests on tr.TrainingID equals trReq.TrainingID
                                   where trReq.EmployeeId == employeeId
                                   select tr).ToList();
            return list;
        }


        public static List<TrainingRequest> GetPendingTRByTrainingId(int trainingId)
        {
            List<TrainingRequest> list = PayrollDataContext.TrainingRequests.Where(x => x.TrainingID == trainingId && x.Status == (int)TrainingRequestStatusEnum.Pending).ToList();

            foreach (var item in list)
            {
                EEmployee emp = new EmployeeManager().GetById(item.EmployeeId.Value);
                item.Name = emp.Name;
                item.Branch = new BranchManager().GetById(emp.BranchId.Value).Name;
                item.Department = new DepartmentManager().GetById(emp.DepartmentId.Value).Name;
            }

            return list;
        }

        public static List<TrainingRequest> GetApprovedTRByTrainingId(int trainingId)
        {
            List<TrainingRequest> list = PayrollDataContext.TrainingRequests.Where(x => x.TrainingID == trainingId && x.Status == (int)TrainingRequestStatusEnum.Approved).ToList();

            foreach (var item in list)
            {
                EEmployee emp = new EmployeeManager().GetById(item.EmployeeId.Value);
                item.Name = emp.Name;
                item.Branch = new BranchManager().GetById(emp.BranchId.Value).Name;
                item.Department = new DepartmentManager().GetById(emp.DepartmentId.Value).Name;
            }

            return list;
        }

        public static Status ApproveTrainingRequest(int trainingRequestId)
        {
            Status status = new Status();

            TrainingRequest dbEntity = TrainingManager.GetTrainingRequestById(trainingRequestId);
            dbEntity.Status = (int)TrainingRequestStatusEnum.Approved;
            dbEntity.ApprovedBy = SessionManager.CurrentLoggedInUserID;
            dbEntity.ApprovedOn = System.DateTime.Now;
            PayrollDataContext.SubmitChanges();
            status.IsSuccess = true;

            return status;
        }

        public static Status HousefullTrainingRequest(int trainingRequestId)
        {
            Status status = new Status();

            TrainingRequest dbEntity = TrainingManager.GetTrainingRequestById(trainingRequestId);
            dbEntity.Status = (int)TrainingRequestStatusEnum.HouseFull;
            dbEntity.ApprovedBy = SessionManager.CurrentLoggedInUserID;
            dbEntity.ApprovedOn = System.DateTime.Now;
            PayrollDataContext.SubmitChanges();
            status.IsSuccess = true;

            return status;
        }

        public static Status WaitingTrainingRequest(int trainingRequestId)
        {
            Status status = new Status();

            TrainingRequest dbEntity = TrainingManager.GetTrainingRequestById(trainingRequestId);
            dbEntity.Status = (int)TrainingRequestStatusEnum.WaitingList;
            dbEntity.ApprovedBy = SessionManager.CurrentLoggedInUserID;
            dbEntity.ApprovedOn = System.DateTime.Now;
            PayrollDataContext.SubmitChanges();
            status.IsSuccess = true;

            return status;
        }

        public static UUser GetUUserByUserId(Guid uuserId)
        {
            return PayrollDataContext.UUsers.SingleOrDefault(x => x.UserID == uuserId);
        }


        public static List<TrainingRequest> GetTrainingRequestsByStatus(int status)
        {
            List<TrainingRequest> list = PayrollDataContext.TrainingRequests.Where(x => x.Status == status).ToList();

            foreach (var item in list)
            {
                EEmployee emp = new EmployeeManager().GetById(item.EmployeeId.Value);
                item.Name = TrainingManager.GetTrainingById(item.TrainingID.Value).Name;
                item.EmployeeName = emp.Name;
                if(item.ApprovedBy != null)
                    item.ApprovedByName = TrainingManager.GetUUserByUserId(item.ApprovedBy.Value).UserName;
                item.Branch = new BranchManager().GetById(emp.BranchId.Value).Name;
                item.Department = new DepartmentManager().GetById(emp.DepartmentId.Value).Name;
            }
            return list.OrderBy(x => x.Name).ToList();
        }

        public static List<TrainingRequest> GetAllTrainingRequestHistory(DateTime? dateFilter)
        {
            List<TrainingRequest> list = new List<TrainingRequest>();

            if (dateFilter == null || dateFilter == DateTime.Parse("0001-01-01 12:00:00 AM"))
                list = PayrollDataContext.TrainingRequests.Where(x => x.RequestedDate < System.DateTime.Now && x.Status != (int)TrainingStatusEnum.Saved).ToList();
            else
                list = PayrollDataContext.TrainingRequests.Where(x => ((x.RequestedDate > dateFilter && x.RequestedDate < System.DateTime.Now) && x.Status != (int)TrainingStatusEnum.Saved)).ToList();

            foreach (var item in list)
            {
                EEmployee emp = new EmployeeManager().GetById(item.EmployeeId.Value);
                item.Name = TrainingManager.GetTrainingById(item.TrainingID.Value).Name;
                item.EmployeeName = emp.Name;
                if (item.ApprovedBy != null)
                    item.ApprovedByName = TrainingManager.GetUUserByUserId(item.ApprovedBy.Value).UserName;
                item.Branch = new BranchManager().GetById(emp.BranchId.Value).Name;
                item.Department = new DepartmentManager().GetById(emp.DepartmentId.Value).Name;
            }
            return list.OrderBy(x => x.Name).ToList();
        }        

        public static List<Training> GetTrainingForSeatsByTrainingId(int trainingId)
        {
            List<Training> list = new List<Training>();
            Training obj = PayrollDataContext.Trainings.SingleOrDefault(x => x.TrainingID == trainingId);
            int occupiedSeats = 0;
            occupiedSeats = PayrollDataContext.TrainingRequests.Where(x => x.TrainingID == trainingId && x.Status == (int)TrainingRequestStatusEnum.Approved).ToList().Count;
            obj.BookedSeats = occupiedSeats;
            obj.LeftSeats = obj.AvailableSeats.Value - obj.BookedSeats;

            list.Add(obj);
            return list;
        }

        public static List<TrainingRequest> GetTrainingRequestListWithEmpName()
        {
            List<TrainingRequest> list = PayrollDataContext.TrainingRequests.ToList();
            foreach (var item in list)
            {
                
                Training obj = PayrollDataContext.Trainings.SingleOrDefault(x => x.TrainingID == item.TrainingID);
                item.EmployeeName = EmployeeManager.GetEmployeeName(item.EmployeeId.Value);
                item.Name = obj.Name;
                item.FromDate = obj.FromDate.Value;
                item.ToDate = obj.ToDate.Value;
                item.Days = obj.Days.Value;
                item.TrainingType = ((TrainingTypeEnum)obj.Type).ToString();
            }

            return list.OrderBy(x => x.EmployeeName).ToList();
        }

        public static List<BLevel> GetBLevels()
        {
            return PayrollDataContext.BLevels.OrderBy(x => x.Name).ToList();
        }

        public static List<TrainingRequest> GetTrainingReqByEmployeeId(int employeeId)
        {
            List<TrainingRequest> list = PayrollDataContext.TrainingRequests.Where(x => x.EmployeeId == employeeId).ToList();

            foreach (var item in list)
            {
                Training obj = PayrollDataContext.Trainings.SingleOrDefault(x => x.TrainingID == item.TrainingID);

                item.Name = obj.Name;
                item.Branch = obj.FromDate.Value.Day.ToString() + "-" + obj.ToDate.Value.Day.ToString();
                item.Department = obj.Days.ToString() + " days";

                item.TrainingType = ((TrainingTypeEnum)obj.Type).ToString();

                if (item.Status == 3)
                    item.ApprovedByName = "House Full";
                else if (item.Status == 4)
                    item.ApprovedByName = "Waiting List";
                else
                    item.ApprovedByName = ((TrainingRequestStatusEnum)item.Status).ToString();
            }

            return list.OrderBy(x => x.Name).ToList();
        }
        
        public static List<TrainingRequest> GetLatestTrainingRequestsAllEmp()
        {
            List<TrainingRequest> list = PayrollDataContext.TrainingRequests.OrderByDescending(x => x.RequestedDate).ToList();

            foreach (var item in list)
            {
                Training obj = PayrollDataContext.Trainings.SingleOrDefault(x => x.TrainingID == item.TrainingID);
                item.EmployeeName = new EmployeeManager().GetById(item.EmployeeId.Value).Name;
                item.Name = obj.Name;
                GetEmployeeCurrentBranchDepartmentResult currentBranchDep = EmployeeManager.GetEmployeeCurrentBranchDepartment(item.EmployeeId.Value);
                if (currentBranchDep != null)
                {
                    item.Branch = currentBranchDep.Branch;
                    item.Department = currentBranchDep.Department;
                }

                //item.Branch = obj.FromDate.Value.Day.ToString() + "-" + obj.ToDate.Value.Day.ToString();
                //item.Department = obj.Days.ToString() + " days";

                item.TrainingType = ((TrainingTypeEnum)obj.Type).ToString();

                if (item.Status == 3)
                    item.ApprovedByName = "House Full";
                else if (item.Status == 4)
                    item.ApprovedByName = "Waiting List";
                else
                    item.ApprovedByName = ((TrainingRequestStatusEnum)item.Status).ToString();
            }

            return list.OrderBy(x => x.Name).ToList();
        }

        public static TrainingRatingText GetTrainingRatingTextByID(int Id)
        {
            return PayrollDataContext.TrainingRatingTexts.SingleOrDefault(x => x.RatingID == Id);
        }



        public static Status DeleteTrainingRatingText(int RatingID)
        {
            Status status = new Status();

            if (PayrollDataContext.TrainingRatings.Any(x => x.MyRatingRef_Id == RatingID))
            {
                status.ErrorMessage = "Rating is in use, cannot be deleted.";
                status.IsSuccess = false;
                return status;
            }

            TrainingRatingText obj = PayrollDataContext.TrainingRatingTexts.SingleOrDefault(x => x.RatingID == RatingID);
            if (obj != null)
            {
                PayrollDataContext.TrainingRatingTexts.DeleteOnSubmit(obj);
                PayrollDataContext.SubmitChanges();
                status.IsSuccess = true;
            }

            return status;
        }


        public static List<TrainingRequest> GetTrainingRequestByStatusAndTrainingId(int trainingId, int status)
        {
            return PayrollDataContext.TrainingRequests.Where(x => (x.TrainingID == trainingId) && (x.Status == status)).ToList();
        }

       

        public static int GetTrainingParticipantCount(int TrainingID)
        {
            return PayrollDataContext.TrainingRequests.Where(x => x.TrainingID == TrainingID && x.Status == (int)TrainingRequestStatusEnum.Approved).Count();
        }

        public static string GetCurrentDepartmentOfEmployee(int empId)
        {
            int? Deptid = PayrollDataContext.GetEmployeeCurrentDepartmentForDate(DateTime.Now, empId);
            string Department = "";
            if (Deptid != null)
            {
                Department _dbDepartment = DepartmentManager.GetDepartmentById(Deptid.Value);
                if (_dbDepartment != null)
                    Department = _dbDepartment.Name;
            }
            else
            {
                Department = "";
            }
            return Department;
        }


        public static List<TrainingRatingText> GetTrainingRatingText()
        {
            return PayrollDataContext.TrainingRatingTexts.OrderBy(x => x.Sequence).ToList();
        }

        public static List<Training> GetAllTrainings()
        {
            return PayrollDataContext.Trainings.OrderBy(x => x.FromDate).ToList();
        }

        public static StringBuilder GetMessageBody(Training obj)
        {
            Training dbObj = PayrollDataContext.Trainings.SingleOrDefault(x => x.TrainingID == obj.TrainingID);

            StringBuilder msgBody = new StringBuilder();

            if (obj.FromDate != dbObj.FromDate || obj.ToDate != dbObj.ToDate)
            {
                msgBody.Append("Date of " + dbObj.Name + " has been changed. Training starts on " + obj.FromDate.Value.ToShortDateString() + " and ends on "
                        + obj.ToDate.Value.ToShortDateString() + ".");
            }

            if (obj.StartTime != dbObj.StartTime || obj.EndTime != dbObj.EndTime)
            {
                msgBody.Append("Time of " + dbObj.Name + " has been changed. Training starts at " + obj.StartTime.Value.ToString() +
                        " and ends at " + obj.EndTime.Value.ToString() + ".");
            }

            if (obj.Venue != dbObj.Venue)
                msgBody.Append("Venue of " + dbObj.Name + " has been changed. New venue is " + obj.Venue + ".");

            return msgBody;
        }

        public static List<TrainingPlanningLine> GetTrainingLines(int TrainingId)
        {
            return PayrollDataContext.TrainingPlanningLines.Where(x => x.TrainingRef_ID == TrainingId).OrderBy(t=>t.DateEng).ToList();
        }
        
        public static List<TrainingPlanningLine> GetTrainingLineForDateRange(DateTime startDate, DateTime endDate, int? TrainingId,List<TrainingPlanningLine> listGrid)
        {
           // TrainingPlanningLine dbObj = PayrollDataContext.TrainingPlanningLines.SingleOrDefault(x => x.TrainingRef_ID == TrainingId);
            List<TrainingPlanningLine> list = new List<TrainingPlanningLine>();
            if (TrainingId == null)
            {
              
                TimeSpan ts = endDate - startDate;
                int totalDays = ts.Days;
                // int sn = 1;
                for (int i = 0; i <= totalDays; i++)
                {
                    TrainingPlanningLine obj = new TrainingPlanningLine();
                    //obj.SN = sn;
                    //sn = sn + 1;
                    obj.DateEng = startDate.AddDays(i);
                    obj.Day = obj.DateEng.Value.DayOfWeek.ToString();
                    obj.IsTraining = true;

                     
                    TrainingPlanningLine dbLine = listGrid.SingleOrDefault(x => x.DateEng == obj.DateEng);
                    if (dbLine != null)
                    {
                        obj.IsTraining = dbLine.IsTraining;
                        obj.StartTime = dbLine.StartTime;
                        obj.InTimeDT = dbLine.InTimeDT;
                        obj.OutTimeDT = dbLine.OutTimeDT;
                        obj.Hours = dbLine.Hours;
                        obj.ResourcePerson = dbLine.ResourcePerson;
                    }

                    list.Add(obj);
                }
                
            }
            else
            {
                TimeSpan ts = endDate - startDate;
                int totalDays = ts.Days;

                // int sn = 1;
                for (int i = 0; i <= totalDays; i++)
                {
                    
                    TrainingPlanningLine obj = new TrainingPlanningLine();
                    //obj.SN = sn;
                    //sn = sn + 1;
                    obj.DateEng = startDate.AddDays(i);
                    obj.IsTraining = true;
                    TrainingPlanningLine dbLine = PayrollDataContext.TrainingPlanningLines.SingleOrDefault(x => x.TrainingRef_ID == TrainingId && x.DateEng == obj.DateEng);
                    if (dbLine != null)
                    {
                        obj.IsTraining = dbLine.IsTraining;
                        obj.StartTime = dbLine.StartTime;
                        obj.EndTime = dbLine.EndTime;
                        obj.EndTime = dbLine.EndTime;

                        if (dbLine.StartTime != null)
                        {
                            DateTime inTimeDateTime = new DateTime(dbLine.DateEng.Value.Year, dbLine.DateEng.Value.Month, dbLine.DateEng.Value.Day, dbLine.StartTime.Value.Hours, dbLine.StartTime.Value.Minutes, dbLine.StartTime.Value.Seconds);
                            //  item.InTimeString = inTimeDateTime.ToString();
                            obj.InTimeDT = inTimeDateTime;
                        }

                        if (dbLine.EndTime != null)
                        {
                            DateTime outTimeDateTime = new DateTime(dbLine.DateEng.Value.Year, dbLine.DateEng.Value.Month, dbLine.DateEng.Value.Day, dbLine.EndTime.Value.Hours, dbLine.EndTime.Value.Minutes, dbLine.EndTime.Value.Seconds);
                            //  item.OutTimeString = outTimeDateTime.ToString();
                            obj.OutTimeDT = outTimeDateTime;
                        }
                        obj.Hours = dbLine.Hours;
                        obj.ResourcePerson = dbLine.ResourcePerson;
                        obj.TrainingRef_ID = dbLine.TrainingRef_ID;
                    }

                    obj.Day = obj.DateEng.Value.DayOfWeek.ToString();
                    list.Add(obj);
                }
            }
            return list;
        }

        public static void SendNotification(Training obj, bool unpublish)
        {
            Training dbObj = PayrollDataContext.Trainings.SingleOrDefault(x => x.TrainingID == obj.TrainingID);

            // if lies in future or today then only send mail
            if (obj.FromDate.Value.Date >= DateTime.Now.Date)
                return;

            List<string> userNameList = new List<string>();

            List<GetEmployeeListForTrainingResult> list = PayrollDataContext.GetEmployeeListForTraining(obj.TrainingID).ToList();
            if (list.Count == 0)
                return;

            foreach (var item in list)
            {
                if(!string.IsNullOrEmpty(item.UserName))
                    userNameList.Add(item.UserName);
            }

            if (userNameList.Count == 0)
                return;

            PayrollMessage msg = new PayrollMessage();

            if (unpublish)
            {
                msg.Subject = dbObj.Name + " training postponed notification.";
                msg.Body = dbObj.Name + " training scheduled from " + dbObj.FromDate.Value.ToShortDateString() + " to " + dbObj.ToDate.Value.ToShortDateString() + " from " + dbObj.StartTime.Value.ToString()
                    + " to " + dbObj.EndTime.Value.ToString() + " at venue " + dbObj.Venue + " has been postponed.";
            }
            else
            {
                msg.Subject = dbObj.Name + " training change notification.";

                StringBuilder msgBody = GetMessageBody(obj);

                if (msgBody.ToString() == "")
                    return;

                msg.Body = msgBody.ToString();
            }

            msg.SendBy = SessionManager.UserName;
            msg.ReceivedDate = BLL.BaseBiz.GetCurrentDateAndTime().ToShortDateString();
            msg.ReceivedDateEng = BLL.BaseBiz.GetCurrentDateAndTime();
            msg.IsRead = false;


            PayrollMessageManager.SendMessages(msg, userNameList);
            
        }

        /// <summary>
        /// Send the training email to the participants
        /// </summary>
        /// <param name="obj"></param>
        public static Status SendEmail(Training dbObj)///, bool Unpublish)
        {
            //Training dbObj = PayrollDataContext.Trainings.SingleOrDefault(x => x.TrainingID == obj.TrainingID);

            // if lies in future or today then only send mail
            //if (obj.FromDate.Value.Date >= DateTime.Now.Date)
            //    return;

            Status status = new Status();

            string to = "", bcc = "";

            List<string> userNameList = new List<string>();

            List<GetEmployeeListForTrainingResult> list = PayrollDataContext.GetEmployeeListForTraining(dbObj.TrainingID).ToList();
            if (list.Count == 0)
                return status;


            EmailContent dbMailContent = CommonManager.GetMailContent((int)EmailContentType.TrainingEmail);
            if (dbMailContent == null)
            {
                status.ErrorMessage = "Training template not defined.";
                return status;
            }

           

            // create period
            string period = dbObj.FromDate.Value.ToString("yyyy-MMM-dd") + " to " +
                dbObj.ToDate.Value.ToString("yyyy-MMM-dd");
            if (dbObj.FromDate.Value == dbObj.ToDate.Value)
                period = dbObj.FromDate.Value.ToString("yyyy-MMM-dd");

            // create time, if multiple rows then pick first date time
            string time = "<table>";
            foreach (var item in dbObj.TrainingPlanningLines.OrderBy(x => x.DateEng.Value))
            {

                DateTime startTime = DateTime.Today.Add(item.StartTime.Value);
                DateTime endTime = DateTime.Today.Add(item.EndTime.Value);
                //string displayTime = time.ToString("hh:mm tt");

                time += string.Format("<tr> <td> {0},  </td> <td> {1} </td> </tr>",
                    item.DateEng.Value.ToString("yyyy-MMM-dd"),
                    startTime.ToString("hh:mm tt") + " to " + endTime.ToString("hh:mm tt"));

            }
            time += "</table>";

            string subject = dbMailContent.Subject
                .Replace("#Name#", dbObj.Name).Replace("#Period#", period)
                .Replace("#Time#", time).Replace("#Venue#", dbObj.Venue)
                .Replace("#ResourcePerson#", dbObj.ResourcePerson).Replace("#Notes#", dbObj.Notes);

            string body = dbMailContent.Body
                .Replace("#Name#", dbObj.Name).Replace("#Period#", period)
                .Replace("#Time#", time).Replace("#Venue#", dbObj.Venue)
                .Replace("#ResourcePerson#", dbObj.ResourcePerson).Replace("#Notes#", dbObj.Notes);
            string errorMsg = string.Empty;


            foreach (var item in list)
            {
                if (!string.IsNullOrEmpty(item.Email))
                {
                    if (to == "")
                        to = item.Email.Trim();
                   // else
                     //   to += "," + item.Email.Trim();

                    else if (bcc == "")
                        bcc = item.Email.Trim();
                    else
                        bcc += "," + item.Email.Trim();

                    
                }
            }

            if (to == "")
            {
                status.ErrorMessage = "Employee email not defined.";
                return status;
            }

            SMTPHelper.SendMail(to, body, subject, bcc, Config.GetEmailSenderName.ToString(), ref errorMsg);

            //SMTPHelper.SendAsyncMail(to, body, subject, bcc);
            return status;
        }

        public static Status InviteEmployee(int trainingId, ref int totalCount)
        {
            Status status = new Status();
            totalCount = 0;

            Training dbObj = PayrollDataContext.Trainings.SingleOrDefault(x => x.TrainingID == trainingId);
            string to = "", bcc = "", body = "", subject = "";

            List<string> emailIds = new List<string>();
            if (!string.IsNullOrEmpty(dbObj.LevelIds))
            {
                List<GetEmployeeListForTrainingByLevelIdsResult> list = PayrollDataContext.GetEmployeeListForTrainingByLevelIds(dbObj.LevelIds).ToList();
                if (list.Count > 0)
                {
                    foreach (var item in list)
                    {                        
                        if (!string.IsNullOrEmpty(item.CIEmail))
                        {
                            totalCount++;
                            if (to == "")
                                to = item.CIEmail.Trim();
                            else
                                to += "," + item.CIEmail.Trim();
                            //else if (bcc == "")
                            //    bcc = item.CIEmail.Trim();
                            //else
                            //    bcc += "," + item.CIEmail.Trim();
                        }
                    }
                }
            }

            if (to == "")
            {
                status.IsSuccess = false;
                status.ErrorMessage = "No employee to invite.";
                return status;
            }

            subject = "Invitation for participation in " + dbObj.Name + " training";
            body = dbObj.Name + " training is being organised from " + dbObj.FromDate.Value.ToShortDateString() + " to "  + dbObj.ToDate.Value.ToShortDateString() +
            " from " + dbObj.StartTime.Value.ToString() + " to "  + dbObj.EndTime.Value.ToString() + " at "  + dbObj.Venue + ".";

            SMTPHelper.SendAsyncMail(to, body, subject, bcc);
            status.IsSuccess = true;

            return status;
        }


        public static List<GetTrainingEmpAttendnaceResult> GetTrainingEmpAttendnace(int currentPage, int pageSize, int employeeId, int TrainingID, DateTime? Date)
        {
            return PayrollDataContext.GetTrainingEmpAttendnace(currentPage, pageSize, employeeId, TrainingID, Date).ToList();
        }

        public static List<GetTrainingFeedbackOfEmployeeResult> GetTrainingFeedbackOfEmployee(int currentPage, int pageSize, int employeeId, int TrainingID)
        {
            return PayrollDataContext.GetTrainingFeedbackOfEmployee(currentPage, pageSize, employeeId, TrainingID).ToList();
        }

        public static List<TrainingRequest> GetTrainingEmployeeList(DateTime startDate, DateTime endDate)
        {
            List<TrainingRequest> listTrainingRequest = new List<TrainingRequest>();

            List<Training> list = PayrollDataContext.Trainings.Where(x => 
            (x.FromDate.Value.Date.Month >= startDate.Date.Month && x.FromDate.Value.Year >= startDate.Date.Year 
            && x.ToDate.Value.Date.Month <= endDate.Date.Month && x.FromDate.Value.Year <= endDate.Date.Year) 
            && (x.Status == (int)TrainingStatusEnum.Publish))
            .OrderBy(x => x.TrainingID).ToList();

            int SN = 0;

            foreach (var item in list)
            {
                List<TrainingRequest> trainingRequests = PayrollDataContext.TrainingRequests.Where(x => x.TrainingID == item.TrainingID && x.Status == (int)TrainingRequestStatusEnum.Approved ).OrderBy(x => x.EmployeeId).ToList();

                foreach (var trainingReq in trainingRequests)
                {
                    SN++;
                    TrainingRequest obj = new TrainingRequest();
                    obj.SN = SN;
                    obj.EmployeeId = trainingReq.EmployeeId;
                    obj.Name = item.Name + ":" + trainingReq.TrainingID.ToString();
                    EEmployee objEEmployee = EmployeeManager.GetEmployeeById(trainingReq.EmployeeId.Value);
                    obj.EmployeeName = objEEmployee.Name;
                    obj.Branch = BranchManager.GetBranchById(objEEmployee.BranchId.Value).Name;
                    obj.FromDate = item.FromDate.Value;
                    obj.ToDate = item.ToDate.Value;
                    //obj.StartTime = AttendanceManager.CalculateManualVisibleTime(item.StartTime.Value);
                    //obj.EndTime = AttendanceManager.CalculateManualVisibleTime(item.EndTime.Value);
                    listTrainingRequest.Add(obj);
                }

            }

            return listTrainingRequest;
        }

        public static Status SaveTrainingAttendanceByLine(List<AttendanceCheckInCheckOut> list)
        {
            Status status = new Status();
            PayrollDataContext.AttendanceCheckInCheckOuts.InsertAllOnSubmit(list);
            PayrollDataContext.SubmitChanges();
            status.IsSuccess = true;
            return status;
        }

        public static Status DeleteTrainingAttendanceByDate(DateTime Date,string DeviceID)
        {
            Status status = new Status();
            List<AttendanceCheckInCheckOut> _listTrainingAtt = PayrollDataContext.AttendanceCheckInCheckOuts.Where(X => X.DeviceID == DeviceID && X.Date == Date && X.AuthenticationType == (byte)TimeAttendanceAuthenticationType.Training).ToList();
           if (_listTrainingAtt.Any())
           PayrollDataContext.AttendanceCheckInCheckOuts.DeleteAllOnSubmit(_listTrainingAtt);
            PayrollDataContext.SubmitChanges();
            status.IsSuccess = true;
            return status;
        }

        public static Status SaveTrainingAttendance(List<TimeRequestLine> list)
        {
            Status status = new Status();

            SetConnectionPwd(PayrollDataContext.Connection);
            PayrollDataContext.Transaction = PayrollDataContext.Connection.BeginTransaction();
            
            try
            {
                foreach (var item in list)
                {


                    if (item.DayName == "")
                    {
                        DAL.AttendanceMapping map = BLL.BaseBiz.PayrollDataContext.AttendanceMappings
                                      .SingleOrDefault(x => x.EmployeeId == item.EmpId);

                        if (map == null)
                            continue;

                        if (item.InTime != null)
                        {
                            //AttendanceCheckInCheckOut dbObjAttendanceCheckInCheckOut = PayrollDataContext.AttendanceCheckInCheckOuts.SingleOrDefault(x => x.DeviceID == map.DeviceId && x.DateTime.Date == item.DateEng.Value.Date && x.InOutMode == (int)ChkINOUTEnum.ChkIn);
                            //if (dbObjAttendanceCheckInCheckOut != null)
                            //{
                            //    dbObjAttendanceCheckInCheckOut.AuthenticationType = (int)TimeAttendanceAuthenticationType.Training;
                            //    dbObjAttendanceCheckInCheckOut.ModificationRemarks = "Training Attendance";
                            //    dbObjAttendanceCheckInCheckOut.ManualVisibleTime = AttendanceManager.CalculateManualVisibleTime(item.InTime.Value);
                            //    dbObjAttendanceCheckInCheckOut.DateTime = new DateTime(item.DateEng.Value.Date.Year, item.DateEng.Value.Date.Month, item.DateEng.Value.Date.Day, item.InTime.Value.Hours, item.InTime.Value.Minutes, item.InTime.Value.Seconds);
                            //    dbObjAttendanceCheckInCheckOut.Date = dbObjAttendanceCheckInCheckOut.DateTime.Date;
                            //    dbObjAttendanceCheckInCheckOut.ModifiedBy = SessionManager.CurrentLoggedInUserID;
                            //    dbObjAttendanceCheckInCheckOut.ModifiedOn = DateTime.Now;
                            //    PayrollDataContext.SubmitChanges();
                            //}
                            //else
                            {

                                AttendanceCheckInCheckOut chkInOut = new AttendanceCheckInCheckOut();
                                chkInOut.ChkInOutID = Guid.NewGuid();
                                chkInOut.DeviceID = map.DeviceId;
                                chkInOut.AuthenticationType = (int)TimeAttendanceAuthenticationType.Training;
                                chkInOut.InOutMode = (int)ChkINOUTEnum.ChkIn;
                                chkInOut.ModifiedBy = SessionManager.CurrentLoggedInUserID;
                                chkInOut.ModifiedOn = DateTime.Now;
                                chkInOut.ManualVisibleTime = AttendanceManager.CalculateManualVisibleTime(item.InTime.Value);

                                chkInOut.DateTime = new DateTime(item.DateEng.Value.Date.Year, item.DateEng.Value.Date.Month, item.DateEng.Value.Date.Day, item.InTime.Value.Hours, item.InTime.Value.Minutes, item.InTime.Value.Seconds);
                                chkInOut.Date = chkInOut.DateTime.Date;
                                chkInOut.ModificationRemarks = "Training Attendance";

                                PayrollDataContext.AttendanceCheckInCheckOuts.InsertOnSubmit(chkInOut);
                                PayrollDataContext.SubmitChanges();
                            }
                        }


                        if (item.OutTime != null)
                        {
                            //AttendanceCheckInCheckOut dbObjAttendanceCheckInCheckOut = PayrollDataContext.AttendanceCheckInCheckOuts.SingleOrDefault(x => x.DeviceID == map.DeviceId && x.DateTime.Date == item.DateEng.Value.Date && x.InOutMode == (int)ChkINOUTEnum.ChkOut);
                            //if (dbObjAttendanceCheckInCheckOut != null)
                            //{
                            //    dbObjAttendanceCheckInCheckOut.AuthenticationType = (int)TimeAttendanceAuthenticationType.Training;
                            //    dbObjAttendanceCheckInCheckOut.ManualVisibleTime = AttendanceManager.CalculateManualVisibleTime(item.OutTime.Value);
                            //    dbObjAttendanceCheckInCheckOut.DateTime = new DateTime(item.DateEng.Value.Date.Year, item.DateEng.Value.Date.Month, item.DateEng.Value.Date.Day, item.OutTime.Value.Hours, item.OutTime.Value.Minutes, item.OutTime.Value.Seconds);
                            //    dbObjAttendanceCheckInCheckOut.Date = dbObjAttendanceCheckInCheckOut.DateTime.Date;
                            //    dbObjAttendanceCheckInCheckOut.ModifiedBy = SessionManager.CurrentLoggedInUserID;
                            //    dbObjAttendanceCheckInCheckOut.ModifiedOn = DateTime.Now;
                            //    dbObjAttendanceCheckInCheckOut.ModificationRemarks = "Training Attendance";
                            //    PayrollDataContext.SubmitChanges();
                            //}
                            //else
                            {
                                AttendanceCheckInCheckOut chkInOut = new AttendanceCheckInCheckOut();
                                chkInOut.ChkInOutID = Guid.NewGuid();
                                chkInOut.DeviceID = map.DeviceId;
                                chkInOut.AuthenticationType = (int)TimeAttendanceAuthenticationType.Training;
                                chkInOut.InOutMode = (int)ChkINOUTEnum.ChkOut;
                                chkInOut.ModifiedBy = SessionManager.CurrentLoggedInUserID;
                                chkInOut.ModifiedOn = DateTime.Now;
                                chkInOut.ManualVisibleTime = AttendanceManager.CalculateManualVisibleTime(item.OutTime.Value);

                                chkInOut.DateTime = new DateTime(item.DateEng.Value.Date.Year, item.DateEng.Value.Date.Month, item.DateEng.Value.Date.Day, item.OutTime.Value.Hours, item.OutTime.Value.Minutes, item.OutTime.Value.Seconds);
                                chkInOut.Date = chkInOut.DateTime.Date;
                                chkInOut.ModificationRemarks = "Training Attendance";

                                PayrollDataContext.AttendanceCheckInCheckOuts.InsertOnSubmit(chkInOut);
                                PayrollDataContext.SubmitChanges();
                            }
                        }

                    }
                }

                PayrollDataContext.Transaction.Commit();
                status.IsSuccess = true;
            }
            catch (Exception exp)
            {
                Log.log("Error while saving training attendance time", exp);
                PayrollDataContext.Transaction.Rollback();
                status.ErrorMessage = "Error while saving training attendance time"; 
                status.IsSuccess = false;
            }
            finally
            {

            }

            return status;
        }

        
    }
}
