﻿using System.Collections.Generic;
using System.Linq;
using DAL;
using System;

namespace BLL.Manager
{
    public class InsuranceManager : BaseBiz
    {
        public List<IInsuranceName> GetInsuranceName()
        {
            return PayrollDataContext.IInsuranceNames.ToList();
        }

        public List<IIndividualInsurance> GetInsuranceListingByEmployee(int employeeID, int yearId)
        {
            // return PayrollDataContext.GetInsuranceDetailsForEmployee(employeeID).ToList();
            List<IIndividualInsurance> list = PayrollDataContext.IIndividualInsurances.Where(
                i => i.EmployeeId == employeeID && i.FinancialYearId==yearId).ToList();

            EEmployee emp = new EmployeeManager().GetById(employeeID);

            foreach (IIndividualInsurance item in list)
            {
                
                item.InsuranceCompany = item.InstitutionId == -1 ? "" :
                    PayrollDataContext.IInsuranceInstitutions.FirstOrDefault(x => x.Id == item.InstitutionId).InstitutionName;
                item.Name = emp.Name;

                 //indiv.EmployerPaysPercent = double.Parse(txtEmployerPays.Text.Trim());
                //if (ddlPaidBy.SelectedIndex == 0)
                //{
                //    indiv.EmployeePaysPercent = 100;
                //    indiv.EmployerPaysPercent = 0;
                //}
                //else
                //{
                //    indiv.EmployeePaysPercent = 0;
                //    indiv.EmployerPaysPercent = 100;
                //}


                item.PaidBy = item.EmployeePaysPercent == 100 ? "Employee" : "Company";
            }

            return list;

        }

        public static IIndividualInsurance GetInsuranceDetailsByEmployee(int insuranceId)
        {
            // return PayrollDataContext.GetInsuranceDetailsForEmployee(employeeID).ToList();
            return PayrollDataContext.IIndividualInsurances.Where(
                i => i.Id == insuranceId).FirstOrDefault();

        }


        public Status  UpdateIndividualInsurance(IIndividualInsurance entity)
        {
            Status status = new Status();

            IIndividualInsurance dbEntity = PayrollDataContext.IIndividualInsurances.SingleOrDefault(c => c.Id == entity.Id);
            if (dbEntity == null)
                return status;

            if (dbEntity.FinancialYearId != SessionManager.CurrentCompanyFinancialDate.FinancialDateId)
            {
                status.ErrorMessage = "Past year insurance can not be edited.";
                return status;
            }

            AddToMonetoryChange(dbEntity, entity, MonetoryChangeLogTypeEnum.Insurance, entity.EmployeeId,
               "IsPersonalInsurance", "IsHealthInsurance","StartDate", "EndDate", "PolicyAmount", "Premium", "EmployeePaysPercent", "EmployerPaysPercent", "IsDeductFromSalary", "IsMonthlyPayment");


            dbEntity.EmployeeId = entity.EmployeeId;
            //dbEntity.IsPersonalInsurance = entity.IsPersonalInsurance;
            dbEntity.InsuranceNameId = entity.InsuranceNameId;
            dbEntity.InstitutionId = entity.InstitutionId;
            dbEntity.PolicyNo = entity.PolicyNo;
            dbEntity.FinancialYearId = entity.FinancialYearId;
            //dbEntity.StartDate = entity.StartDate;
            dbEntity.StartDateEng = entity.StartDateEng;
            //dbEntity.EndDate = entity.EndDate;
            dbEntity.EndDateEng = entity.EndDateEng;//GetEngDate(dbEntity.EndDate, IsEnglish);
            dbEntity.PolicyAmount = entity.PolicyAmount;
            dbEntity.Premium = entity.Premium;
            dbEntity.PremiumPaymentFrequency = entity.PremiumPaymentFrequency;
            dbEntity.EmployeePaysPercent = entity.EmployeePaysPercent;
            dbEntity.EmployerPaysPercent = entity.EmployerPaysPercent;
            dbEntity.IsDeductFromSalary = entity.IsDeductFromSalary;
            dbEntity.IsMonthlyPayment = entity.IsMonthlyPayment;
            dbEntity.Note = entity.Note;
            dbEntity.IsHealthInsurance = entity.IsHealthInsurance;

            PayrollDataContext.SubmitChanges();
            return status;

        }

        public List<IInsuranceInstitution> GetInsuranceInstitution()
        {
            return PayrollDataContext.IInsuranceInstitutions.ToList();
        }

        public bool Save(IInsuranceName entity)
        {
            PayrollDataContext.IInsuranceNames.InsertOnSubmit(entity);
            return SaveChangeSet();
        }
        public bool Delete(IInsuranceName entity)
        {
            return PayrollDataContext.DeleteInsuranceName(entity.Id) == 1;
        }
        public bool Update(IInsuranceName entity)
        {
            IInsuranceName dbEntity = PayrollDataContext.IInsuranceNames.SingleOrDefault
                (d => d.Id == entity.Id);
            if (dbEntity == null)
                return false;

            dbEntity.InsuranceName = entity.InsuranceName;

            UpdateChangeSet();
            return true;
        }
        public bool Save(IInsuranceInstitution entity)
        {
            PayrollDataContext.IInsuranceInstitutions.InsertOnSubmit(entity);
            return SaveChangeSet();
        }
        public bool Delete(IInsuranceInstitution entity)
        {
            return PayrollDataContext.DeleteInsuranceInstitution(entity.Id) == 1;
        }
        public bool Update(IInsuranceInstitution entity)
        {
            IInsuranceInstitution dbEntity = PayrollDataContext.IInsuranceInstitutions.SingleOrDefault
                (d => d.Id == entity.Id);
            if (dbEntity == null)
                return false;

            dbEntity.InstitutionName = entity.InstitutionName;

            return UpdateChangeSet();
        }

        //Start Individual Insurance 
        public void Save(IIndividualInsurance entity)
        {
            //entity.StartDateEng = GetEngDate(entity.StartDate, IsEnglish);
            //entity.EndDateEng = GetEngDate(entity.EndDate, IsEnglish);

            PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(entity.EmployeeId,
                (byte)MonetoryChangeLogTypeEnum.Insurance, "", "-", entity.Premium, LogActionEnum.Add));

            PayrollDataContext.IIndividualInsurances.InsertOnSubmit(entity);
            PayrollDataContext.SubmitChanges();
        }
        //public List<GetInsuranceDetailsForEmployeeResult> GetInsuranceDetailsByEmployee(int employeeID)
        //{
        //    return PayrollDataContext.GetInsuranceDetailsForEmployee(employeeID).ToList();
        //}
        public IIndividualInsurance GetInsuranceDetailsByEmployee(int employeeID, int insuranceId)
        {
           // return PayrollDataContext.GetInsuranceDetailsForEmployee(employeeID).ToList();
            return PayrollDataContext.IIndividualInsurances.Where(
                i => i.EmployeeId == employeeID && i.FinancialYearId
                ==SessionManager.CurrentCompanyFinancialDate.FinancialDateId
                && (insuranceId == 0 || insuranceId == i.Id)).FirstOrDefault();
        
        }

        public static List<IIndividualInsurance> GetCurrentYearInsuranceList(
           int yearId, int start, int limit, ref int total, string searchEmp, int employeeId)
        {
            searchEmp = searchEmp.ToLower();

            start = (start * limit);

            List<IIndividualInsurance> list =
                (
                from t in PayrollDataContext.IIndividualInsurances
                join e in PayrollDataContext.EEmployees on t.EmployeeId equals e.EmployeeId
                join h in PayrollDataContext.EHumanResources on e.EmployeeId equals h.EmployeeId
                where
                t.FinancialYearId == yearId
                &&
                 (searchEmp.Length == 0 || e.Name.ToLower().Contains(searchEmp))
                 && (employeeId == 0 || employeeId == -1 || employeeId == e.EmployeeId)
                 // show all for the year for now
                 //&&
                 //(
                 //    ( (e.IsRetired == null || e.IsRetired==false) || h.DateOfRetirementEng >= DateTime.Now)
                 //    &&
                 //    ((e.IsResigned == null || e.IsResigned == false) || h.DateOfResignationEng >= DateTime.Now)
                 //)
                orderby t.EmployeeId descending
                select t
                ).ToList();
            //PayrollDataContext.BranchDepartmentHistories.Where(x=>x.FromBranchId != x.BranchId)
            //.OrderByDescending(x => x.FromDateEng)
            //.ToList();

            total = list.Count;
            //Paging code
            if (start >= 0 && limit > 0)
            {
                list = list.Skip(start).Take(limit).ToList();
            }

            EmployeeManager empMgr = new EmployeeManager();
            BranchManager bMgr = new BranchManager();
            foreach (IIndividualInsurance item in list)
            {
                item.InsuranceCompany = item.InstitutionId == -1 ? "" :
                     PayrollDataContext.IInsuranceInstitutions.FirstOrDefault(x => x.Id == item.InstitutionId).InstitutionName;
                item.Name = empMgr.GetById(item.EmployeeId).Name;
                //item.InsuranceType = item.IsPersonalInsurance == null || item.IsPersonalInsurance == true ? "Personal Insurance" : "Family Insurance";
                item.PaidBy = item.EmployerPaysPercent == 0 ? "Employee" : "Company";
            }

            return list;
        }
        public static IInsuranceInstitution GetInsuranceCompanyByName(string name)
        {
            return PayrollDataContext.IInsuranceInstitutions.FirstOrDefault(x => x.InstitutionName.ToLower() == name);
        }

        public static Status SaveImportedInsurance(List<IIndividualInsurance> list)
        {
            Status status = new Status();
            int row =1;
            foreach (IIndividualInsurance item in list)
            {
                row +=1;
                IIndividualInsurance db = PayrollDataContext.IIndividualInsurances
                    .FirstOrDefault(x => x.Id == item.Id);
                        //x.EmployeeId == item.EmployeeId && x.FinancialYearId == SessionManager.CurrentCompanyFinancialDate.FinancialDateId);

                if (db == null)
                    PayrollDataContext.IIndividualInsurances.InsertOnSubmit(item);
                else
                {
                    //IIndividualInsurance db = PayrollDataContext.IIndividualInsurances.FirstOrDefault(x => x.Id == item.Id);
                    if (db != null)
                    {

                        if (db.FinancialYearId != item.FinancialYearId)
                        {
                            status.ErrorMessage = "Previous year insurance for the row " + row + " can not be imported for current year, to import for current year clear Insurance ID column value.";
                            return status;
                        }

                        //db.IsPersonalInsurance = item.IsPersonalInsurance;
                        db.InstitutionId = item.InstitutionId;
                        db.StartDate = item.StartDate;
                        db.StartDateEng = item.StartDateEng;
                        db.EndDate = item.EndDate;
                        db.EndDateEng = item.EndDateEng;
                        db.EmployeePaysPercent = item.EmployeePaysPercent;
                        db.EmployerPaysPercent = item.EmployerPaysPercent;
                        db.FinancialYearId = item.FinancialYearId;
                        db.PolicyNo = item.PolicyNo;
                        db.PolicyAmount = item.PolicyAmount;
                        db.Premium = item.Premium;
                        db.Note = item.Note;
                        db.IsHealthInsurance = item.IsHealthInsurance;
                        //if ((db.IsExpired == null || db.IsExpired == false) && item.IsExpired.Value)
                        //{
                        //    db.ExpiredDate = GetCurrentDateAndTime();
                        //}
                        //db.IsExpired = item.IsExpired;
                    }
                }
            }
            PayrollDataContext.SubmitChanges();
            return status;
        }


        public static IIndividualInsurance GetDefaultInsuranceEntityForImport(int employeeId,decimal premiumAmount)
        {
            IIndividualInsurance indiv = new IIndividualInsurance();
            if (employeeId > 0)
                indiv.EmployeeId = employeeId;

            //indiv.InsuranceNameId = int.Parse(ddlInsuranceName.SelectedValue);
            indiv.InstitutionId = -1;
            indiv.PolicyNo = "";

            FinancialDate year = CommonManager.GetCurrentFinancialYear();

            indiv.FinancialYearId = year.FinancialDateId;
            indiv.StartDate = year.StartingDate;
            indiv.EndDate = year.EndingDate;

            indiv.PolicyAmount = 0;

            indiv.Premium = premiumAmount;
            indiv.PremiumPaymentFrequency = PremiumPaymentFrequency.YEARLY;

            indiv.EmployeePaysPercent = 100;
            indiv.EmployerPaysPercent = 0;


            indiv.IsDeductFromSalary = false;
            if (indiv.IsDeductFromSalary.Value)
                indiv.IsMonthlyPayment = false;

            indiv.Note = "";

            return indiv;
        }

        public static bool DeleteInsurance(int empId)
        {
            IIndividualInsurance dbEntity = PayrollDataContext.IIndividualInsurances.SingleOrDefault(c => c.EmployeeId == empId);
            if (dbEntity == null)
                return false;

            PayrollDataContext.IIndividualInsurances.DeleteOnSubmit(dbEntity);
            return DeleteChangeSet();

        }

        public bool InsertUpdateIndividualInsurance(IIndividualInsurance entity)
        {
            IIndividualInsurance dbEntity = PayrollDataContext.IIndividualInsurances
                .SingleOrDefault(c => c.EmployeeId == entity.EmployeeId && c.FinancialYearId
                == SessionManager.CurrentCompanyFinancialDate.FinancialDateId
                && (entity.Id == 0 || c.Id == entity.Id));

            if (dbEntity == null)
            {
                //entity.StartDateEng = GetEngDate(entity.StartDate, IsEnglish);
                //entity.EndDateEng = GetEngDate(entity.EndDate, IsEnglish);

                PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(entity.EmployeeId,
                    (byte)MonetoryChangeLogTypeEnum.Insurance, "", "-", entity.Premium, LogActionEnum.Add));

                PayrollDataContext.IIndividualInsurances.InsertOnSubmit(entity);
                PayrollDataContext.SubmitChanges();
                return true;
            }

            AddToMonetoryChange(dbEntity, entity, MonetoryChangeLogTypeEnum.Insurance, entity.EmployeeId,
               "StartDate", "EndDate", "PolicyAmount", "Premium", "EmployeePaysPercent", "EmployerPaysPercent", "IsDeductFromSalary", "IsMonthlyPayment");


            if (dbEntity.Premium != entity.Premium)
            {
                PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(entity.EmployeeId,
                      (byte)MonetoryChangeLogTypeEnum.Insurance, "", dbEntity.Premium, entity.Premium, LogActionEnum.Update));
            }

            dbEntity.EmployeeId = entity.EmployeeId;
            dbEntity.InsuranceNameId = entity.InsuranceNameId;
            dbEntity.InstitutionId = entity.InstitutionId;
            dbEntity.PolicyNo = entity.PolicyNo;
            dbEntity.FinancialYearId = entity.FinancialYearId;
            dbEntity.StartDate = entity.StartDate;
            dbEntity.StartDateEng = GetEngDate(dbEntity.StartDate,IsEnglish);
            dbEntity.EndDate = entity.EndDate;
            dbEntity.EndDateEng = GetEngDate(dbEntity.EndDate, IsEnglish);
            dbEntity.PolicyAmount = entity.PolicyAmount;
            dbEntity.Premium = entity.Premium;
            dbEntity.PremiumPaymentFrequency = entity.PremiumPaymentFrequency;
            dbEntity.EmployeePaysPercent = entity.EmployeePaysPercent;
            dbEntity.EmployerPaysPercent = entity.EmployerPaysPercent;
            dbEntity.IsDeductFromSalary = entity.IsDeductFromSalary;
            dbEntity.IsMonthlyPayment = entity.IsMonthlyPayment;
            dbEntity.Note = entity.Note;

           

            PayrollDataContext.SubmitChanges();
            return true;

        }
        public IIndividualInsurance GetInsuranceById(int insuranceId)
        {
            return PayrollDataContext.IIndividualInsurances.SingleOrDefault(c => c.Id == insuranceId);
        }
        public List<GetEmployeesByFilterResult> GetEmployeesByFilterSelection(int companyId, string type, int id)
        {
            return PayrollDataContext.GetEmployeesByFilter(companyId, type, id).ToList();
        }

        public bool DeleteIndividualInsurance(IIndividualInsurance insurance)
        {
            IIndividualInsurance ind = PayrollDataContext.IIndividualInsurances.SingleOrDefault(c => c.Id == insurance.Id);
            if (ind == null)
                return false;

            System.Data.Linq.ChangeSet cs = PayrollDataContext.GetChangeSet();
            PayrollDataContext.IIndividualInsurances.DeleteOnSubmit(ind);
            PayrollDataContext.SubmitChanges();
            return true;
        }
        public void AddInsuranceDetailsToMultiple(int? institutionId, string empids, double? employerPays, double? employeePays, decimal? policyAmount, decimal? Premium, string PremiumFrequency
            , bool? isDeductFromSalary, bool? isMonthlyPayment)
        {
            PayrollDataContext.AddEmpInsuranceToMultiple(institutionId, empids,
                                                         employeePays, employeePays,
                                                         policyAmount, Premium, PremiumFrequency, isDeductFromSalary,
                                                         isMonthlyPayment);
        }

        //End Individual Insurance 


        //Start Group Insurance


        public void SaveGroupInsurance(EmpGroupInsurance entity)
        {
            PayrollDataContext.EmpGroupInsurances.InsertOnSubmit(entity);
            PayrollDataContext.SubmitChanges();
        }
        public List<GetGroupInsuranceDetailsByBranchIdResult> GetGroupDetailsByBranchId(int branchId)
        {
            return PayrollDataContext.GetGroupInsuranceDetailsByBranchId(branchId).ToList();
        }
        public bool UpdateGroupInsurance(EmpGroupInsurance entity)
        {
            EmpGroupInsurance dbEntity = PayrollDataContext.EmpGroupInsurances.SingleOrDefault(c => c.Id == entity.Id);
            if (dbEntity == null)
                return false;
            dbEntity.BranchId = entity.BranchId;
            dbEntity.CompanyId = entity.CompanyId;

            dbEntity.InsuranceNameId = entity.InsuranceNameId;
            dbEntity.InstitutionId = entity.InstitutionId;
            dbEntity.PolicyNo = entity.PolicyNo;
            dbEntity.StartDate = entity.StartDate;
            dbEntity.EndDate = entity.EndDate;
            dbEntity.PolicyAmount = entity.PolicyAmount;
            dbEntity.PremiumAmount = entity.PremiumAmount;
            dbEntity.PremiumPaymentFrequency = entity.PremiumPaymentFrequency;
            dbEntity.Note = entity.Note;
            PayrollDataContext.SubmitChanges();
            return true;

        }
        public EmpGroupInsurance GetGroupInsuranceById(int insuranceId)
        {
            return PayrollDataContext.EmpGroupInsurances.SingleOrDefault(c => c.Id == insuranceId);
        }

        public bool DeleteGroupInsurance(EmpGroupInsurance insurance)
        {
            EmpGroupInsurance grp = PayrollDataContext.EmpGroupInsurances.SingleOrDefault(c => c.Id == insurance.Id);
            if (grp == null)
                return false;

            System.Data.Linq.ChangeSet cs = PayrollDataContext.GetChangeSet();
            PayrollDataContext.EmpGroupInsurances.DeleteOnSubmit(grp);
            PayrollDataContext.SubmitChanges();
            return true;
        }

        //End Group Insurance
    }
}
