﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using DAL;
using Utils;
using System;
using Utils.Calendar;

namespace BLL.Manager
{

    public class VouccherManagerNew : BaseBiz
    {

        public static void DeleteReportHeaderFooter(ReportHeaderFooter entity)
        {
            ReportHeaderFooter dbEntity = PayrollDataContext.ReportHeaderFooters.FirstOrDefault(c =>
                c.ReportId == entity.ReportId);
            if (dbEntity != null)
            {
                PayrollDataContext.ReportHeaderFooters.DeleteOnSubmit(dbEntity);
                PayrollDataContext.SubmitChanges();
                return;
            }
        }
        public static void SaveReportHeaderFooter(ReportHeaderFooter entity)
        {
            ReportHeaderFooter dbEntity = PayrollDataContext.ReportHeaderFooters.FirstOrDefault(c => 
                c.ReportId ==entity.ReportId);
            if (dbEntity == null)
            {
                PayrollDataContext.ReportHeaderFooters.InsertOnSubmit(entity);
                PayrollDataContext.SubmitChanges();
                return;
            }

            dbEntity.Header = entity.Header;
            dbEntity.Footer = entity.Footer;

            PayrollDataContext.SubmitChanges();
        }
        public static ReportHeaderFooter GetReportHeaderFooter(int id)
        {
            return PayrollDataContext.ReportHeaderFooters.FirstOrDefault(x => x.ReportId == id);
        }

        public static void SaveHead(NIBLVoucherHead entity)
        {
            PayrollDataContext.NIBLVoucherHeads.InsertOnSubmit(entity);
            PayrollDataContext.SubmitChanges();
        }
        public static bool UpdateHead(NIBLVoucherHead entity)
        {
            NIBLVoucherHead dbEntity = PayrollDataContext.NIBLVoucherHeads.SingleOrDefault(c => c.Type == entity.Type && c.SourceId == entity.SourceId);
            if (dbEntity == null)
            {
                PayrollDataContext.NIBLVoucherHeads.InsertOnSubmit(entity);
                PayrollDataContext.SubmitChanges();
                return true;
            }

            dbEntity.Type = entity.Type;
            dbEntity.SourceId = entity.SourceId;
            dbEntity.VoucherGroupRef_ID = entity.VoucherGroupRef_ID;
            dbEntity.Name = entity.Name;

            PayrollDataContext.SubmitChanges();
            return true;

        }


        public static  bool SaveVoucherGorup(NIBLVoucherGroup entity)
        {
            if (PayrollDataContext.NIBLVoucherGroups.Any(x => x.GroupName.ToLower() == entity.GroupName.ToLower()))
            {
                return false;
            }

            PayrollDataContext.NIBLVoucherGroups.InsertOnSubmit(entity);
            PayrollDataContext.SubmitChanges();

            return true;
        }

        public static bool SaveVoucherGorup(NIBLVoucherType entity)
        {
            if (PayrollDataContext.NIBLVoucherTypes.Any(x => x.Name.ToLower() == entity.Name.ToLower()))
            {
                return false;
            }

            PayrollDataContext.NIBLVoucherTypes.InsertOnSubmit(entity);
            PayrollDataContext.SubmitChanges();

            return true;
        }
        public static bool UpdateVoucherGroup(NIBLVoucherGroup entity)
        {

            if (PayrollDataContext.NIBLVoucherGroups.Any(x => x.VoucherGroupID != entity.VoucherGroupID
                 && x.GroupName.ToLower() == entity.GroupName.ToLower()))
            {
                return false;
            }


            NIBLVoucherGroup dbEntity = PayrollDataContext.NIBLVoucherGroups.SingleOrDefault(c => c.VoucherGroupID == entity.VoucherGroupID);
            if (dbEntity == null)
                return false;

            dbEntity.GroupCode = entity.GroupCode;
            dbEntity.GroupName = entity.GroupName;
            dbEntity.TransactionType = entity.TransactionType;
            dbEntity.Group = entity.Group;
            dbEntity.TransactionCode = entity.TransactionCode;
            //dbEntity.DeductFromEmpAccount = entity.DeductFromEmpAccount;
            dbEntity.VoucherTypeRef_ID = entity.VoucherTypeRef_ID;
            //dbEntity.Description = entity.Description;
            dbEntity.NIBLCardDepartmentGroupCode = entity.NIBLCardDepartmentGroupCode;
            dbEntity.EnableLevelFilter = entity.EnableLevelFilter;

            dbEntity.NIBLVoucherStatus.Clear();
            dbEntity.NIBLVoucherStatus.AddRange(entity.NIBLVoucherStatus);

            dbEntity.NIBLVoucherLevels.Clear();
            dbEntity.NIBLVoucherLevels.AddRange(entity.NIBLVoucherLevels);

            dbEntity.NIBLVoucherHeads.Clear();
            dbEntity.NIBLVoucherHeads.AddRange(entity.NIBLVoucherHeads);

            PayrollDataContext.SubmitChanges();
            return true;

        }
        public static bool UpdateVoucherGroup(NIBLVoucherType entity)
        {



            NIBLVoucherType dbEntity = PayrollDataContext.NIBLVoucherTypes.SingleOrDefault(c => c.VoucherTypeID == entity.VoucherTypeID);
            if (dbEntity == null)
                return false;

            dbEntity.Name = entity.Name;
            dbEntity.Description = entity.Description;
            PayrollDataContext.SubmitChanges();
            return true;

        }
        //End Employee Fam


        public  static NIBLVoucherGroup GetVoucher(int id)
        {
            return PayrollDataContext.NIBLVoucherGroups.SingleOrDefault(c => c.VoucherGroupID == id);
        }

        public static NIBLVoucherType GetVoucherType(int id)
        {
            return PayrollDataContext.NIBLVoucherTypes.SingleOrDefault(c => c.VoucherTypeID == id);
        }
        public static List<NIBLVoucherType> GetVoucherTypeList()
        {
            return PayrollDataContext.NIBLVoucherTypes.ToList();
        }
        //public  static NIBLVoucherHead GetVoucherHead(int type,int sourceId)
        //{
        //    return PayrollDataContext.NIBLVoucherHeads.SingleOrDefault(c => c.Type == type && c.SourceId == sourceId);
        //}
        public static List<NIBLVoucherGroup> GetVoucherGroupList()
        {
            List<NIBLVoucherGroup> list =
                PayrollDataContext.NIBLVoucherGroups.OrderBy(x => x.GroupName).ToList();

            List<KeyValue> statusList = (new JobStatus()).GetMembers();

            List<BLevel> levels = NewPayrollManager.GetAllParentLevelList();

            List<CalcGetHeaderListResult> incomeDeductionList = CalculationManager.GetHeaderListForVoucher();

            foreach (NIBLVoucherGroup item in list)
            {

                item.VoucherType = GetVoucherType(item.VoucherTypeRef_ID.Value).Name;

                foreach (NIBLVoucherStatus status in item.NIBLVoucherStatus.ToList())
                {
                    if (item.Status == "")
                        item.Status = statusList.FirstOrDefault(x => x.Key == status.Status.ToString()).Value;
                    else
                        item.Status += ", " + statusList.FirstOrDefault(x => x.Key == status.Status.ToString()).Value;
                }

                foreach (NIBLVoucherLevel status in item.NIBLVoucherLevels.ToList())
                {
                    if (item.Level == "")
                        item.Level = levels.FirstOrDefault(x => x.LevelId == status.LevelID).Name;
                    else
                        item.Level += ", " + levels.FirstOrDefault(x => x.LevelId == status.LevelID).Name;
                }

                foreach (NIBLVoucherHead status in item.NIBLVoucherHeads.OrderBy(x=>x.Name))
                {
                    if (item.IncomeDeduction == "")
                        item.IncomeDeduction = status.Name;
                    else
                        item.IncomeDeduction += ", " + status.Name; ;
                }

            }

            return list;
        }

       
        //End Employee Previous Employment

    }


}
