﻿using System.Collections.Generic;
using System.Linq;
using DAL;
using System;
using Utils;

namespace BLL.Manager
{
    public class RecruitmentManager : BaseBiz
    {
        public static List<Branch> GetBranches()
        {
            return PayrollDataContext.Branches.Where(x => x.CompanyId == SessionManager.CurrentCompanyId).OrderBy(x => x.Name).ToList(); 
        }

        public static List<Department> GetDepartments()
        {
            return PayrollDataContext.Departments.OrderBy(x => x.Name).ToList();
        }

        public static List<EDesignation> GetEDesignations()
        {
            return PayrollDataContext.EDesignations.Where(x => x.CompanyId == SessionManager.CurrentCompanyId).OrderBy(x => x.Name).ToList();

        }

        public static List<RecruitmentHiringManager> GetRecruitmentHiringManagers()
        {
            return PayrollDataContext.RecruitmentHiringManagers.OrderBy(x => x.Name).ToList();
        }

        public static List<RecruitmentJobType> GetRecruitmentJobTypes()
        {
            return PayrollDataContext.RecruitmentJobTypes.OrderBy(x => x.Name).ToList();
        }

        public static List<SkillSet> GetSkillTests()
        {
            return PayrollDataContext.SkillSets.OrderBy(x => x.Name).ToList();
        }

        public static CountryList GetCountryUsingName(string CountryName)
        {
            return PayrollDataContext.CountryLists.SingleOrDefault(x => x.CountryName.ToLower() == CountryName.ToLower());
        }

        public static string GetCountryNameUsingId(int countryId)
        {
            return PayrollDataContext.CountryLists.SingleOrDefault(x => x.CountryId == countryId).CountryName;
        }


        public static Status InsertUpdateRecruitmentOpeningDetails(RecruitmentOpening obj, List<RecruitmentOpeningSkillSet> listSkillSet)
        {
            Status status = new Status();
            RecruitmentOpening dbEntity = new RecruitmentOpening();
            RecruitmentOpeningSkillSet skillSetEntity = new RecruitmentOpeningSkillSet();

            SetConnectionPwd(PayrollDataContext.Connection);
            PayrollDataContext.Transaction = PayrollDataContext.Connection.BeginTransaction();

            try
            {

                if (PayrollDataContext.RecruitmentOpenings.Any(x => x.OpeningID == obj.OpeningID))
                {
                    dbEntity = PayrollDataContext.RecruitmentOpenings.SingleOrDefault(x => x.OpeningID == obj.OpeningID);
                    dbEntity.BranchID = obj.BranchID;
                    dbEntity.DepartmentID = obj.DepartmentID;
                    dbEntity.Location = obj.Location;
                    dbEntity.DesignationID = obj.DesignationID;
                    dbEntity.JobDescription = obj.JobDescription;
                    dbEntity.HiringManagerID = obj.HiringManagerID;
                    dbEntity.AssingedRecruiter = obj.AssingedRecruiter;
                    dbEntity.DateOpened = obj.DateOpened;
                    dbEntity.DateOpenedEng = obj.DateOpenedEng;
                    dbEntity.LastDateOfSubmission = obj.LastDateOfSubmission;
                    dbEntity.LastDateOfSubmissionEng = obj.LastDateOfSubmissionEng;
                    dbEntity.NumberOfPositions = obj.NumberOfPositions;
                    dbEntity.JobTypeID = obj.JobTypeID;
                    dbEntity.CountryID = obj.CountryID;
                    dbEntity.PostedOn = obj.PostedOn;
                    dbEntity.PostedDate = obj.PostedDate;
                    dbEntity.PostedDateEng = obj.PostedDateEng;
                    dbEntity.WorkExperienceYears = obj.WorkExperienceYears;
                    dbEntity.UpperSalaryRange = obj.UpperSalaryRange;
                    dbEntity.AttachedDocumentName = obj.AttachedDocumentName;
                    dbEntity.AttachedDocumentUrl = obj.AttachedDocumentUrl;

                    List<RecruitmentOpeningSkillSet> list = PayrollDataContext.RecruitmentOpeningSkillSets.Where(x => x.OpeningID == obj.OpeningID).ToList();
                    foreach (RecruitmentOpeningSkillSet item in list)
                    {
                        PayrollDataContext.RecruitmentOpeningSkillSets.DeleteOnSubmit(item);
                    }

                    foreach (RecruitmentOpeningSkillSet item in listSkillSet)
                    {
                        item.OpeningID = obj.OpeningID;
                        PayrollDataContext.RecruitmentOpeningSkillSets.InsertOnSubmit(item);
                    }
                    PayrollDataContext.SubmitChanges();
                }
                else
                {
                    if (PayrollDataContext.RecruitmentOpenings.Any(x => (x.BranchID == obj.BranchID && x.DepartmentID == obj.DepartmentID && x.Location == obj.Location && x.DesignationID == obj.DesignationID)))
                    {
                        status.ErrorMessage = "Recruitment Opening Details already exists.";
                        status.IsSuccess = false;
                        return status;
                    }

                    PayrollDataContext.RecruitmentOpenings.InsertOnSubmit(obj);
                    PayrollDataContext.SubmitChanges();

                    RecruitmentOpening dbObj = PayrollDataContext.RecruitmentOpenings.OrderByDescending(x => x.OpeningID).First();

                    foreach (RecruitmentOpeningSkillSet item in listSkillSet)
                    {
                        item.OpeningID = dbObj.OpeningID;
                        PayrollDataContext.RecruitmentOpeningSkillSets.InsertOnSubmit(item);
                        PayrollDataContext.SubmitChanges();
                    }
                    
                }
                PayrollDataContext.Transaction.Commit();
                status.IsSuccess = true;

            }
            catch (Exception exp)
            {
                Log.log("Error while creating Job Opening", exp);
                PayrollDataContext.Transaction.Rollback();
                status.IsSuccess = false;
            }
            return status;           
        }

        public static string GetRecruitmentHiringManagerById(int managerID)
        {
            RecruitmentHiringManager obj = new RecruitmentHiringManager();
            obj = PayrollDataContext.RecruitmentHiringManagers.SingleOrDefault(x => x.ManagerID == managerID);
            return obj.Name;
        }       

        public static Status DeleteRecruitmentHiringManager(int managerId)
        {
            Status status = new Status();
            RecruitmentHiringManager dbEntity = PayrollDataContext.RecruitmentHiringManagers.Single(x => x.ManagerID == managerId);

            if (PayrollDataContext.RecruitmentOpenings.Any(x => x.HiringManagerID == managerId))
            {
                status.ErrorMessage = "Hiring Manager is assigned to recruitment opening.";
                return status;
            }

            PayrollDataContext.RecruitmentHiringManagers.DeleteOnSubmit(dbEntity);
            PayrollDataContext.SubmitChanges();
            status.IsSuccess = true;
            return status;
        }

        public static Status InsertUpdateRecruitmentHiringManager(RecruitmentHiringManager obj)
        {
            Status status = new Status();
            RecruitmentHiringManager dbEntity = new RecruitmentHiringManager();

            if (PayrollDataContext.RecruitmentHiringManagers.Any(x => x.ManagerID == obj.ManagerID))
            {
                dbEntity = PayrollDataContext.RecruitmentHiringManagers.Where(x => x.ManagerID == obj.ManagerID).First();
                dbEntity.Name = obj.Name;
            }
            else
            {
                if (PayrollDataContext.RecruitmentHiringManagers.Any(x => x.Name.ToLower() == obj.Name.ToLower()))
                {
                    status.ErrorMessage = "Hiring manager already exists.";
                    status.IsSuccess = false;
                    return status;
                }

                PayrollDataContext.RecruitmentHiringManagers.InsertOnSubmit(obj);
            }

            PayrollDataContext.SubmitChanges();
            status.IsSuccess = true; ;
            return status;
        }

        public static string GetRecruitmentJobTypeName(int jobTypeId)
        {
            RecruitmentJobType entity = PayrollDataContext.RecruitmentJobTypes.SingleOrDefault(x => x.JobTypeID == jobTypeId);
            return entity.Name;
        }

        public static Status DeleteRecruitmentJobType(int jobTypeId)
        {
            Status status = new Status();
            RecruitmentJobType dbEntity = PayrollDataContext.RecruitmentJobTypes.Single(x => x.JobTypeID == jobTypeId);

            if (PayrollDataContext.RecruitmentOpenings.Any(x => x.JobTypeID == jobTypeId))
            {
                status.ErrorMessage = "Job type is assigned to recruitment opening.";
                return status;
            }

            PayrollDataContext.RecruitmentJobTypes.DeleteOnSubmit(dbEntity);
            PayrollDataContext.SubmitChanges();
            status.IsSuccess = true;
            return status;
        }

        public static Status InsertUpdateRecruitmentJobType(RecruitmentJobType obj)
        {
            Status status = new Status();
            RecruitmentJobType dbEntity = new RecruitmentJobType();
            
            if (PayrollDataContext.RecruitmentJobTypes.Any(x => x.JobTypeID == obj.JobTypeID))
            {
                dbEntity = PayrollDataContext.RecruitmentJobTypes.Where(x => x.JobTypeID == obj.JobTypeID).First();
                dbEntity.Name = obj.Name;
            }
            else
            {
                if (PayrollDataContext.RecruitmentJobTypes.Any(x => x.Name.ToLower() == obj.Name.ToLower()))
                {
                    status.ErrorMessage = "Job type already exists.";
                    status.IsSuccess = false;
                    return status;
                }

                PayrollDataContext.RecruitmentJobTypes.InsertOnSubmit(obj);
            }

            PayrollDataContext.SubmitChanges();
            status.IsSuccess = true;
            return status;
        }

        public static string GetDesignationNameId(int designationId)
        {
            EDesignation obj = PayrollDataContext.EDesignations.SingleOrDefault(x => x.DesignationId == designationId);
            return obj.Name;
        }

        public static List<RecruitmentOpening> GetRecruitmentJobOpenings()
        {
            List<RecruitmentOpening> list = PayrollDataContext.RecruitmentOpenings.ToList();

            BranchManager brManager = new BranchManager();
            DepartmentManager dptManager = new DepartmentManager();
            EmployeeManager empManager = new EmployeeManager();
           

            foreach (RecruitmentOpening item in list)
            {
                item.Branch = brManager.GetById(item.BranchID.Value).Name;
                item.Department = dptManager.GetById(item.DepartmentID.Value).Name;
                item.Designation = GetDesignationNameId(item.DesignationID.Value);
            }

            return list;

        }

        public static Status DeleteJobOpeningById(int openingId)
        {
            Status status = new Status();
            RecruitmentOpening dbEntity = PayrollDataContext.RecruitmentOpenings.Single(x => x.OpeningID == openingId);
            PayrollDataContext.RecruitmentOpenings.DeleteOnSubmit(dbEntity);
            PayrollDataContext.SubmitChanges();
            status.IsSuccess = true;
            return status;
        }

        public static RecruitmentOpening GetRecruitmentOpeningById(int openingId)
        {
            return PayrollDataContext.RecruitmentOpenings.SingleOrDefault(x => x.OpeningID == openingId);
        }

        public static Status DeleteJobOpeningAttachedFile(int openingId)
        {
            Status status = new Status();
            RecruitmentOpening dbEntity = PayrollDataContext.RecruitmentOpenings.SingleOrDefault(x => x.OpeningID == openingId);
            if (dbEntity == null)
            {
                status.ErrorMessage = "Error while deleting the attached file.";
                status.IsSuccess = false;
                return status;
            }
            else
            {
                dbEntity.AttachedDocumentName = "";
                dbEntity.AttachedDocumentUrl = "";
                PayrollDataContext.SubmitChanges();
                status.IsSuccess = true;
                return status;
            }
        }


       public static List<RecruitmentOpeningSkillSet> GetRecruitmentOpeningSkills(int openingID)
       {
           return PayrollDataContext.RecruitmentOpeningSkillSets.Where(x => x.OpeningID == openingID).ToList();
       }

       public static CEducationalDetail GetCEducationalDetailById(int id)
       {
           return PayrollDataContext.CEducationalDetails.SingleOrDefault(x => x.EducationalDetailsID == id);
       }

       public static List<CEducationalDetail> GetEducationalDetails(int candidateId)
       {
           List<CEducationalDetail> list = PayrollDataContext.CEducationalDetails.Where(x => x.CandidateID == candidateId).ToList();
           
           foreach (CEducationalDetail item in list)
           {
               CountryList countryList = PayrollDataContext.CountryLists.Where(x => x.CountryId == item.CountryID).First();
               item.CountryName = countryList.CountryName;
           }

           return list;
       }

       public static Status DeleteCEducationalDetails(int id)
       {
           Status status = new Status();
           CEducationalDetail cEducationalDetial = PayrollDataContext.CEducationalDetails.Single(x => x.EducationalDetailsID == id);
           PayrollDataContext.CEducationalDetails.DeleteOnSubmit(cEducationalDetial);
           PayrollDataContext.SubmitChanges();
           status.IsSuccess = true;
           return status;
       }

       public static Status InsertUpdateCEducationalDetails(CEducationalDetail obj)
       {
           Status status = new Status();

           CEducationalDetail dbEntity = new CEducationalDetail();

           if (PayrollDataContext.CEducationalDetails.Any(x => x.EducationalDetailsID == obj.EducationalDetailsID))
           {
               dbEntity = PayrollDataContext.CEducationalDetails.Where(x => x.EducationalDetailsID == obj.EducationalDetailsID).First();
               dbEntity.CandidateID = obj.CandidateID;
               dbEntity.LevelName = obj.LevelName;
               dbEntity.LevelTypeId = obj.LevelTypeId;
               dbEntity.Institution = obj.Institution;
               dbEntity.CountryID = obj.CountryID;
               dbEntity.Place = obj.Place;
               dbEntity.PassYear = obj.PassYear;
               dbEntity.DivisionID = obj.DivisionID;
               dbEntity.DivisionName = obj.DivisionName;
               dbEntity.Percentage = obj.Percentage;
               dbEntity.Grade = obj.Grade;
           }
           else
           {
               if (PayrollDataContext.CEducationalDetails.Any(x => (x.LevelTypeId == obj.LevelTypeId && x.Institution == obj.Institution && x.PassYear == obj.PassYear && x.CandidateID == obj.CandidateID)))
               {
                   status.ErrorMessage = "Educational Details already exists.";
                   status.IsSuccess = false;
                   return status;
               }

               PayrollDataContext.CEducationalDetails.InsertOnSubmit(obj);
           }
           PayrollDataContext.SubmitChanges();
           status.IsSuccess = true;
           return status;
       }

       public static List<CTrainingDetial> GetTrainingDetail(int candidateId)
       {
           return PayrollDataContext.CTrainingDetials.Where(x => x.CandidateID == candidateId).ToList();
       }

       public static CTrainingDetial GetTrainingDetailById(int trainingDetailId)
       {
           return PayrollDataContext.CTrainingDetials.SingleOrDefault(x => x.TrainingDetailsID == trainingDetailId);
       }

       public static Status DeleteCTrainingDetail(int trainingDetailID)
       {
           Status status = new Status();
           CTrainingDetial dbEntity = PayrollDataContext.CTrainingDetials.Single(x => x.TrainingDetailsID == trainingDetailID);
           PayrollDataContext.CTrainingDetials.DeleteOnSubmit(dbEntity);
           PayrollDataContext.SubmitChanges();
           status.IsSuccess = true;
           return status;
       }

       public static Status InsertUpdateTrainingDetail(CTrainingDetial obj)
       {
           Status status = new Status();
         
           CTrainingDetial dbEntity = new CTrainingDetial();

           

           if (PayrollDataContext.CTrainingDetials.Any(x => x.TrainingDetailsID == obj.TrainingDetailsID))
           {
               dbEntity = PayrollDataContext.CTrainingDetials.Where(x => x.TrainingDetailsID == obj.TrainingDetailsID).First();
               dbEntity.CandidateID = obj.CandidateID;
               dbEntity.TrainingName = obj.TrainingName;
               dbEntity.Institution = obj.Institution;
               dbEntity.Duration = obj.Duration;
               dbEntity.Year = obj.Year;
           }
           else
           {
               if (PayrollDataContext.CTrainingDetials.Any(x => (x.TrainingName == obj.TrainingName && x.CandidateID == obj.CandidateID)))
               {
                   status.ErrorMessage = "Training Name already exists.";
                   status.IsSuccess = false;
                   return status;
               }

               PayrollDataContext.CTrainingDetials.InsertOnSubmit(obj);
           }

           PayrollDataContext.SubmitChanges();
           status.IsSuccess = true;
           return status;
       }

       public static List<CEmploymentDetail> GetEmploymentDetails(int candidateId)
       {
           return PayrollDataContext.CEmploymentDetails.Where(x => x.CandidateID == candidateId).ToList();
       }

       public static CEmploymentDetail GetEmploymentDetailById(int employmentDetialId)
       {
           return PayrollDataContext.CEmploymentDetails.SingleOrDefault(x => x.EmploymentDetailsID == employmentDetialId);
       }

       public static Status DeleteEmploymentDetails(int employmentDetailsId)
       {
           Status status = new Status();
           CEmploymentDetail dbEntity = PayrollDataContext.CEmploymentDetails.Single(x => x.EmploymentDetailsID == employmentDetailsId);
           PayrollDataContext.CEmploymentDetails.DeleteOnSubmit(dbEntity);
           PayrollDataContext.SubmitChanges();
           status.IsSuccess = true;
           return status;
       }

       public static Status InsertUpdateEmploymentDetails(CEmploymentDetail obj)
        {
            Status status = new Status();

            CEmploymentDetail dbEntity = new CEmploymentDetail();

            if (PayrollDataContext.CEmploymentDetails.Any(x => x.EmploymentDetailsID == obj.EmploymentDetailsID))
            {
                dbEntity = PayrollDataContext.CEmploymentDetails.Where(x => x.EmploymentDetailsID == obj.EmploymentDetailsID).First();
                dbEntity.CandidateID = obj.CandidateID;
                dbEntity.State = obj.State;
                dbEntity.Organization = obj.Organization;
                dbEntity.Place = obj.Place;
                dbEntity.Position = obj.Position;
                dbEntity.MajorResponisbility = obj.MajorResponisbility;
                dbEntity.FromDate = obj.FromDate;
                dbEntity.FromDateEng = obj.FromDateEng;
                dbEntity.ToDate = obj.ToDate;
                dbEntity.ToDateEng = obj.ToDateEng;
            }
            else
            {
                if (PayrollDataContext.CEmploymentDetails.Any(x => (x.Organization == obj.Organization && x.FromDateEng == obj.FromDateEng && x.ToDateEng == obj.ToDateEng && x.CandidateID == obj.CandidateID)))
                {
                    status.ErrorMessage = "Employment details already exists.";
                    status.IsSuccess = false;
                    return status;
                }

                PayrollDataContext.CEmploymentDetails.InsertOnSubmit(obj);
            }

            PayrollDataContext.SubmitChanges();
            status.IsSuccess = true;
            return status;
        }

       public static Candidate GetCandidateById(int candidateId)
       {
           return PayrollDataContext.Candidates.SingleOrDefault(x => x.CandidateID == candidateId);
       }

       public static Status DeleteApplicationFormResume(int candidateId)
       {
           Status status = new Status();

           Candidate dbEntity = PayrollDataContext.Candidates.SingleOrDefault(x => x.CandidateID == candidateId);
           if (dbEntity == null)
           {
               status.ErrorMessage = "Error while deleting the attached resume.";
               status.IsSuccess = false;
               return status;
           }
           else
           {
               dbEntity.AttachedResumeUrl = "";
               dbEntity.AttachedResumeName = "";
               PayrollDataContext.SubmitChanges();
               status.IsSuccess = true;
               return status;
           }
          
       }

       public static Status InsertUpdateRecruitmentCandidate(Candidate obj, List<CSkillSet> cSkillSetList)
       {
           Status status = new Status();

           Candidate dbEntity = new Candidate();
           CSkillSet skillSetEntity = new CSkillSet();

           SetConnectionPwd(PayrollDataContext.Connection);
           PayrollDataContext.Transaction = PayrollDataContext.Connection.BeginTransaction();

           try
           {
               if (PayrollDataContext.Candidates.Any(x => x.CandidateID == obj.CandidateID))
               {
                   dbEntity = PayrollDataContext.Candidates.SingleOrDefault(x => x.CandidateID == obj.CandidateID);
                   dbEntity.FirstName = obj.FirstName;
                   dbEntity.MiddleName = obj.MiddleName;
                   dbEntity.LastName = obj.LastName;
                   dbEntity.ContactAddress = obj.ContactAddress;
                   dbEntity.EmailID = obj.EmailID;
                   dbEntity.ContactNumber = obj.ContactNumber;
                   dbEntity.CreatedOnDate = obj.CreatedOnDate;
                   dbEntity.Source = obj.Source;
                   dbEntity.FatherName = obj.FatherName;
                   dbEntity.MotherName = obj.MotherName;
                   dbEntity.CitizenshipNo = obj.CitizenshipNo;
                   dbEntity.IssuedFrom = obj.IssuedFrom;
                   dbEntity.CurrentSalary = obj.CurrentSalary;
                   dbEntity.ExpectedSalary = obj.ExpectedSalary;
                   dbEntity.AttachedResumeName = obj.AttachedResumeName;
                   dbEntity.AttachedResumeUrl = obj.AttachedResumeUrl;
                   dbEntity.AdditionalInfo = obj.AdditionalInfo;

                   List<CSkillSet> list = PayrollDataContext.CSkillSets.Where(x => x.CandidateID == obj.CandidateID).ToList();
                   foreach (CSkillSet item in list)
                   {
                       PayrollDataContext.CSkillSets.DeleteOnSubmit(item);
                   }

                   foreach (CSkillSet item in cSkillSetList)
                   {
                       item.CandidateID = obj.CandidateID;
                       PayrollDataContext.CSkillSets.InsertOnSubmit(item);
                       
                   }

                   PayrollDataContext.SubmitChanges();
               }
               else
               {
                   if (PayrollDataContext.Candidates.Any(x => (x.FirstName == obj.FirstName &&  x.LastName == obj.LastName && x.CitizenshipNo == obj.CitizenshipNo)))
                   {
                       status.ErrorMessage = "Candidate already exists.";
                       status.IsSuccess = false;
                       return status;
                   }

                   PayrollDataContext.Candidates.InsertOnSubmit(obj);
                   PayrollDataContext.SubmitChanges();

                   Candidate candidateObj = PayrollDataContext.Candidates.OrderByDescending(x => x.CandidateID).First();

                   foreach (CSkillSet item in cSkillSetList)
                   {
                       item.CandidateID = candidateObj.CandidateID;
                       PayrollDataContext.CSkillSets.InsertOnSubmit(item);
                       PayrollDataContext.SubmitChanges();
                   }
               }

               PayrollDataContext.Transaction.Commit();
               status.IsSuccess = true;
           }
           catch (Exception exp)
           {
               Log.log("Error while creating Candidate.", exp);
               PayrollDataContext.Transaction.Rollback();
               status.IsSuccess = false;
           }

           return status;
       }

       public static Candidate GetRecruitmentCandidate(string citizenshipNo)
       {
           return PayrollDataContext.Candidates.SingleOrDefault(x => x.CitizenshipNo == citizenshipNo);
       }

       public static Candidate GetCandidateByCandidateId(int candidateId)
       {
           return PayrollDataContext.Candidates.SingleOrDefault(x => x.CandidateID == candidateId);
       }

       public static List<CSkillSet> GetCandidateSkillSet(int candidateId)
       {
           return PayrollDataContext.CSkillSets.Where(x => x.CandidateID == candidateId).ToList();
       }

       public static List<Candidate> GetRecruitmentCandidateList()
       {
           List<Candidate> list = PayrollDataContext.Candidates.OrderBy(x => x.CandidateID).ToList();
           foreach (Candidate item in list)
           {
               if (!string.IsNullOrEmpty(item.MiddleName))
                   item.CandidateName = string.Format("{0}{1}{2}{3}{4}", item.FirstName, " ", item.MiddleName, " ", item.LastName);
               else
                    item.CandidateName = string.Format("{0}{1}{2}", item.FirstName, " ", item.LastName);
           }
           return list;
       }

       public static Candidate GetRecruitmentCandidateById(int candidateId)
       {
           return PayrollDataContext.Candidates.SingleOrDefault(x => x.CandidateID == candidateId);
       }

       public static Status DeleteRecruitmentCandidate(int candidateId)
       {
           Status status = new Status();
           Candidate dbEntity = PayrollDataContext.Candidates.Single(x => x.CandidateID == candidateId);
           PayrollDataContext.Candidates.DeleteOnSubmit(dbEntity);
           PayrollDataContext.SubmitChanges();
           status.IsSuccess = true;
           return status;
       }
    }
}
