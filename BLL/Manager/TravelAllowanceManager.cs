﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using DAL;
using Utils;
using System.Xml.Linq;
using Utils.Calendar;
using BLL.Entity;
using System.Web;
using System.IO;
using BLL.BO;
using Utils.Helper;

namespace BLL.Manager
{
    public class TravelAllowanceManager : BaseBiz
    {

        public static List<TAAllowanceLocation> GetLocationList()
        {
            return PayrollDataContext.TAAllowanceLocations.OrderBy(x => x.LocationName).ToList();
        }
        public static TAAllowanceLocation GetLocationById(int locationid)
        {
            return PayrollDataContext.TAAllowanceLocations.FirstOrDefault(x => x.LocationId == locationid);
        }

        public static void SetExtensionDate(int requestId, DateTime extensionDate, string reason)
        {
            TARequest request = getTARequestByID(requestId);
            request.ExtendArrivalDate = extensionDate;
            request.ExtendReason = reason;
            request.ExtensionEnteredBy = SessionManager.CurrentLoggedInUserID;
            request.ExtensionEnteredOn = DateTime.Now;

            PayrollDataContext.SubmitChanges();
        }

        public static Status InsertUpdateAllowance(TAAllowance allowanceInstance, bool isInsert)
        {
            Status status = new Status();
            TAAllowance allowanceInstance1 = new TAAllowance();

            if (PayrollDataContext.TAAllowances.Any(x => x.AllowanceID != allowanceInstance.AllowanceID && x.Name.ToLower()==allowanceInstance.Name.ToLower()))
            {
                status.ErrorMessage = "Allowance name already exists.";
                return status;
            }

            if (isInsert)
            {
                PayrollDataContext.TAAllowances.InsertOnSubmit(allowanceInstance);
            }
            else
            {
                allowanceInstance1 = PayrollDataContext.TAAllowances.Where(x => x.AllowanceID == allowanceInstance.AllowanceID).First();
                allowanceInstance1.Name = allowanceInstance.Name;
                allowanceInstance1.OptionToEdit = allowanceInstance.OptionToEdit;
                allowanceInstance1.Description = allowanceInstance.Description;
                allowanceInstance1.OptionToEdit = allowanceInstance.OptionToEdit;
                     
               
            }

            PayrollDataContext.SubmitChanges();
            status.IsSuccess = true;
            return status;
        }


        public static Status DeleteTABySelfEmployee(int requestId)
        {
            TARequest request = getTARequestByID(requestId);
            Status status = new Status();
            if (request.EmployeeId != SessionManager.CurrentLoggedInEmployeeId)
            {
                status.ErrorMessage = "Not enough permission.";
                return status;
            }

            if (request.Status == (int)FlowStepEnum.Saved ||
                request.Status == (int)FlowStepEnum.Step1SaveAndSend)
            {
                PayrollDataContext.TARequests.DeleteOnSubmit(request);
                PayrollDataContext.SubmitChanges();
            }
            else
            {
                status.ErrorMessage = "Approved travel request can not be deleted.";
                return status;
            }

            return status;  
        }
        public static Status DeleteTA(int requestId)
        {
            TARequest request = getTARequestByID(requestId);
            Status status = new Status();


            //if (request.Status == (int)FlowStepEnum.Saved ||
            //    request.Status == (int)FlowStepEnum.Step1SaveAndSend)
            //{
            PayrollDataContext.TARequests.DeleteOnSubmit(request);


            // may need to delete attendance also or set deleted status 


            PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(request.EmployeeId.Value,
                (byte)MonetoryChangeLogTypeEnum.TravelRequest
                 , "", request.StatusName + " TA from "
                 + request.TravellingFromEng.Value.ToString("yyyy-MMM-dd") + " to " +
                  request.TravellingToEng.Value.ToString("yyyy-MMM-dd")
                  , ""
                 , LogActionEnum.Delete));

            PayrollDataContext.SubmitChanges();

            //}
            //else
            //{
            //    status.ErrorMessage = "Approved travel request can not be deleted.";
            //    return status;
            //}


            return status;
        }


        public static Status InsertUpdateAllowanceLocation(TAAllowanceLocation locInstance, bool isInsert)
        {
            Status status = new Status();
            TAAllowanceLocation locInstance1 = new TAAllowanceLocation();

            if (PayrollDataContext.TAAllowanceLocations.Any(x => x.LocationId != locInstance.LocationId && x.LocationName.ToLower() == locInstance.LocationName.ToLower()))
            {
                status.ErrorMessage = "Location name already exists.";
                return status;
            }

            if (isInsert)
            {
                PayrollDataContext.TAAllowanceLocations.InsertOnSubmit(locInstance);
            }
            else
            {
                locInstance1 = PayrollDataContext.TAAllowanceLocations.Where(x => x.LocationId == locInstance.LocationId).First();
                locInstance1.LocationName = locInstance.LocationName;


            }

            PayrollDataContext.SubmitChanges();
            status.IsSuccess = true;
            return status;
        }

        public static TAAllowance GetTravelAllowanceById(int levelId)
        {
            return PayrollDataContext.TAAllowances.Where(x => x.AllowanceID == levelId).SingleOrDefault();
        }

        public static TAAllowanceLocation GetTravelAllowanceByLocId(int levelId)
        {
            return PayrollDataContext.TAAllowanceLocations.Where(x => x.LocationId == levelId).SingleOrDefault();
        }


        public static List<TAAllowance> GetAllTravelAllowance()
        {
            List<TAAllowance> list = PayrollDataContext.TAAllowances.OrderBy(x => x.Name).ToList();

            return list;
        }

        public static List<TAAllowanceLocation> GetAllTravelAllowanceLocations()
        {
            List<TAAllowanceLocation> list = PayrollDataContext.TAAllowanceLocations.OrderBy(x => x.LocationName).ToList();

            return list;
        }

        public static Status DeleteTravelAllowance(int allowanceID)
        {
            Status status = new Status();

            if (PayrollDataContext.TARequestLines.Any(x => x.AllowanceId == allowanceID))
            {
                status.ErrorMessage = "Allowance being used in Travel request, can not be deleted.";
                return status;
            }

            TAAllowance dbEntity = PayrollDataContext.TAAllowances.SingleOrDefault(x => x.AllowanceID == allowanceID);
            

            PayrollDataContext.TAAllowances.DeleteOnSubmit(dbEntity);
            PayrollDataContext.SubmitChanges();
            return status;
        }

        public static Status DeleteTravelAllowanceLocation(int allowanceID)
        {
            Status status = new Status();

            if (PayrollDataContext.TAAllowanceRates.Any(x => x.LocationId == allowanceID))
            {
                status.ErrorMessage = "This Location is being used, can not delete.";
                return status;
            }

            TAAllowanceLocation dbEntity = PayrollDataContext.TAAllowanceLocations.SingleOrDefault(x => x.LocationId == allowanceID);

            PayrollDataContext.TAAllowanceLocations.DeleteOnSubmit(dbEntity);
            PayrollDataContext.SubmitChanges();
            return status;
        }


        public static List<TAAllowanceRate> getAllowanceDetailList(int allowanceID)
        {
            return PayrollDataContext.TAAllowanceRates.Where(x => x.AllowanceID == allowanceID).ToList();
        }

        public static Status InsertUpdateAllowanceDetail(List<TAAllowanceRate> lines,int allowanceId)
        {
            Status status = new Status();
            //TAAllowanceRate rate;

            List<TAAllowanceRate> dbList = PayrollDataContext.TAAllowanceRates.Where(
                x => x.AllowanceID == allowanceId).ToList();

            PayrollDataContext.TAAllowanceRates.DeleteAllOnSubmit(dbList);

            foreach (TAAllowanceRate rate in lines)
                rate.AllowanceID = allowanceId;

            PayrollDataContext.TAAllowanceRates.InsertAllOnSubmit(lines);

            PayrollDataContext.SubmitChanges();
            return status;
        }


        public static List<ComboGeneralItems> getTravelByList()
        {

            List<ComboGeneralItems> returnList = new List<ComboGeneralItems> 
                        {
                            new ComboGeneralItems("Bus", ((int)TravelByEnum.Bus).ToString()),
                            new ComboGeneralItems("Airplane", ((int)TravelByEnum.Airplane).ToString()),
                            new ComboGeneralItems("Ferry", ((int)TravelByEnum.Ferry).ToString()),
                            new ComboGeneralItems("Train", ((int)TravelByEnum.Train).ToString()),
                            new ComboGeneralItems("Other", ((int)TravelByEnum.Other).ToString())
                        };

            return returnList;
        }


        public static List<ComboGeneralItems> getApprovalFlowTypeList()
        {

            List<ComboGeneralItems> returnList = new List<ComboGeneralItems> 
                        {
                            new ComboGeneralItems("Travel Order", ((int)FlowTypeEnum.TravelOrder).ToString()),
                            new ComboGeneralItems("Appraisal", ((int)FlowTypeEnum.Appraisal).ToString())
                        };

            return returnList;
        }


        public static List<ComboGeneralItems> getExpensePaidByList()
        {


            List<ComboGeneralItems> returnList = new List<ComboGeneralItems>();
            

            if (CommonManager.SettingTA.IsLocationEditable != null && CommonManager.SettingTA.IsLocationEditable.Value)
            {
                returnList = new List<ComboGeneralItems> 
                        {
                            new ComboGeneralItems("Head Office", ((int)ExpensePaidByEnum.Office).ToString()),
                            new ComboGeneralItems("Area Office", ((int)ExpensePaidByEnum.AreadOffice).ToString()),
                            new ComboGeneralItems("Self", ((int)ExpensePaidByEnum.Self).ToString()),
                            new ComboGeneralItems("Client", ((int)ExpensePaidByEnum.Client).ToString()),
                            new ComboGeneralItems("Thrid Party", ((int)ExpensePaidByEnum.ThirdParty).ToString())
                            

                        };
            }
            else
            {
               returnList = new List<ComboGeneralItems> 
                        {
                            new ComboGeneralItems("Office", ((int)ExpensePaidByEnum.Office).ToString()),
                            new ComboGeneralItems("Self", ((int)ExpensePaidByEnum.Self).ToString()),
                            new ComboGeneralItems("Client", ((int)ExpensePaidByEnum.Client).ToString())

                        };
            }

            return returnList;
        }

        public static TAAllowanceRate GetRate(int empId, int locationId, int allowanceId)
        {
            int levelID = (from val2 in PayrollDataContext.EEmployees
                           where val2.EmployeeId == empId
                           select val2.EDesignation.LevelId.Value).FirstOrDefault();

            return
                PayrollDataContext.TAAllowanceRates.FirstOrDefault(x => x.AllowanceID == allowanceId 
                    && x.LevelID == levelID && x.LocationId == locationId);
                   

        }

        public static List<TARequestLine> getAllowanceByEmployeeID(int EmpID, int? RequestID, int locationId)
        {

            bool isFixedQuantityWithValueOne = false;

            if (CommonManager.SettingTA.IsLocationEditable != null && CommonManager.SettingTA.IsLocationEditable.Value)
                isFixedQuantityWithValueOne = true;
            else
                isFixedQuantityWithValueOne = false;


            List<TARequestLine> retList = new List<TARequestLine>();
            TARequestLine line;
            TAAllowanceRate rate;
            int levelID = (from val2 in PayrollDataContext.EEmployees
                           where val2.EmployeeId == EmpID
                           select val2.EDesignation.LevelId.Value).FirstOrDefault();

            if (RequestID != null)
            {
                retList= PayrollDataContext.TARequestLines.Where(x => x.RequestId == RequestID).ToList();
                foreach (var val1 in retList)
                {
                    if (val1.AllowanceId != null)
                        val1.IsEditable = Convert.ToBoolean(GetAllowanceByID(val1.AllowanceId.ToString()).OptionToEdit);//value1.OptionToEdit.Value;

                }

                return retList;
            }
            else
            {
                
                List<TAAllowance> allowanceList = PayrollDataContext.TAAllowances.ToList();
                //This For Each Inserts the LineID, AllowanceID, Allowance Name and Initial Quantity to 0
                foreach (var value1 in allowanceList)
                {
                    line = new TARequestLine();
                    line.AllowanceId = value1.AllowanceID;
                    line.AllowanceName = value1.Name;
                    line.Quantity = 0;
                    line.Rate = 0;
                    if (value1.AllowanceID != null )
                        line.IsEditable = Convert.ToBoolean(GetAllowanceByID(value1.AllowanceID.ToString()).OptionToEdit);//value1.OptionToEdit.Value;
                    retList.Add(line);

                    if (isFixedQuantityWithValueOne)
                        line.Quantity = 1;

                }

                //Now this For Each will put respective amount if it is in the database according to the country.
                foreach (var value2 in retList)
                {
                
                    rate = PayrollDataContext.TAAllowanceRates.Where(x => x.AllowanceID == value2.AllowanceId && x.LevelID == levelID 
                        && (locationId==0 || x.LocationId==locationId)).FirstOrDefault();
                    if (rate != null)
                    {
                        //if (locationId != 356 /* India*/ && locationId != 524 /*Nepal*/)
                        //{
                        //    value2.Rate = rate.OtherUSD;
                        //}
                        //else if (locationId == 356)
                        //{
                        //    value2.Rate = rate.IndiaINR;
                        //}
                        //else if (locationId == 524)
                        {
                            value2.Rate = rate.Rate == null ? 0 : rate.Rate.Value;
                        }

                    }
                    
                }

                

            }
            return retList;
        }

        private static DAL.TAAllowance GetAllowanceByID(string allowanceID)
        {
            TAAllowance allowance = new TAAllowance();

            if (PayrollDataContext.TAAllowances.Any(x => x.AllowanceID == int.Parse(allowanceID)))
            {
                allowance = PayrollDataContext.TAAllowances.FirstOrDefault(x => x.AllowanceID == int.Parse(allowanceID));
            }

                return allowance;
        }


        public static Status UpdateAdvanceTravelRequests(TARequest request, decimal Advance)
        {
            Status status = new Status();
            TARequest dbEntity = new TARequest();
            if (PayrollDataContext.TARequests.Any(x => x.RequestID == request.RequestID))
            {
                dbEntity = PayrollDataContext.TARequests.Where(x => x.RequestID == request.RequestID).First();
                dbEntity.Advance = Advance;
                dbEntity.AdvanceStatus = (int)AdvanceStatusEnum.HRAdvance;
                dbEntity.AdvanceDate = CommonManager.GetCurrentDateAndTime();
            }
            PayrollDataContext.SubmitChanges();


            if(dbEntity.LocationId !=  null)
            {
                TAAllowanceLocation loc = GetLocationById(dbEntity.LocationId.Value);
                // Process for mail notification
                SendTravelRequestAdvanceMailAndMessage(EmailContentType.TravelRequestAdvanceNotificationToEmployee, dbEntity.EmployeeId.Value,
                    loc.LocationName);
            }   

            status.IsSuccess = true;
            return status;
        }


        public static Status SendTravelRequestMailAndMessage(
            EmailContentType mailType, int toEmployeeId,int traveRequesterEmployeeId)
        {
            Status status = new Status();

            EmailContent content = CommonManager.GetMailContent((int)mailType);
            if (content == null)
            {
                status.ErrorMessage = "Email content not defined.";
                return status;
            }

            EEmployee toEmployee = new EmployeeManager().GetById(toEmployeeId);
            UUser user = UserManager.GetUserByEmployee(toEmployeeId);
            EEmployee forEmployee = null;


            string subject = content.Subject;
            string message = content.Body;

            string toUser = user == null ? "" : user.UserName;

            if (toEmployee == null || string.IsNullOrEmpty(toEmployee.EAddresses[0].CIEmail))
            {
                status.ErrorMessage = "Forward to or email not defined.";
                return status;
            }

            string toEmail = toEmployee.EAddresses[0].CIEmail;

            switch (mailType)
            {
            
                case EmailContentType.TravelRequestReview:

                    forEmployee = new EmployeeManager().GetById(traveRequesterEmployeeId);


                    subject = subject.Replace("{Employee Name}", forEmployee.Name);

                    message = message.Replace("{Authority Name}", toEmployee.Name)
                                    .Replace("{Employee Name}", forEmployee.Name)
                                    .Replace("{Organizations Name}", SessionManager.CurrentCompany.Name);

                    break;
            }



            bool mailSent = false;
            if (!string.IsNullOrEmpty(toEmail))
            {
                mailSent = SMTPHelper.SendAsyncMail(toEmail, message, subject, "");
            }

            //if (!string.IsNullOrEmpty(toUser))
            //{
            //    PayrollMessage msg = new PayrollMessage();
            //    msg.Subject = subject;
            //    msg.Body = message;
            //    //message.DueDateEng = inst.SubmissionDate;
            //    msg.IsRead = false;
            //    DateTime date = SessionManager.GetCurrentDateAndTime();
            //    msg.ReceivedDate = date.ToShortDateString();
            //    msg.ReceivedDateEng = date;
            //    msg.ReceivedTo = toUser;
            //    msg.SendBy = SessionManager.UserName;
            //    PayrollDataContext.PayrollMessages.InsertOnSubmit(msg);
            //    PayrollDataContext.SubmitChanges();
            //}



            return status;
        }

        public static Status SendTravelRequestAdvanceMailAndMessage(
            EmailContentType mailType, int traveRequesterEmployeeId,string location)
        {
            Status status = new Status();

            EmailContent content = CommonManager.GetMailContent((int)mailType);
            if (content == null)
            {
                status.ErrorMessage = "Email content not defined.";
                return status;
            }

            EEmployee toEmployee = new EmployeeManager().GetById(traveRequesterEmployeeId);
            UUser user = UserManager.GetUserByEmployee(traveRequesterEmployeeId);
            

            string subject = content.Subject;
            string message = content.Body;

            string toUser = user == null ? "" : user.UserName;
            string toEmail = toEmployee.EAddresses[0].CIEmail;

            //switch (mailType)
            {

                //case EmailContentType.TravelRequestReview:


                   
                    message = message.Replace("{Location}", location)
                                   .Replace("{Employee Name}", toEmployee.Name)
                                    .Replace("{Organizations Name}", SessionManager.CurrentCompany.Name);

                   // break;
            }



            bool mailSent = false;
            if (!string.IsNullOrEmpty(toEmail))
            {
                mailSent = SMTPHelper.SendAsyncMail(toEmail, message, subject, "");
            }

            //if (!string.IsNullOrEmpty(toUser))
            //{
            //    PayrollMessage msg = new PayrollMessage();
            //    msg.Subject = subject;
            //    msg.Body = message;
            //    //message.DueDateEng = inst.SubmissionDate;
            //    msg.IsRead = false;
            //    DateTime date = SessionManager.GetCurrentDateAndTime();
            //    msg.ReceivedDate = date.ToShortDateString();
            //    msg.ReceivedDateEng = date;
            //    msg.ReceivedTo = toUser;
            //    msg.SendBy = SessionManager.UserName;
            //    PayrollDataContext.PayrollMessages.InsertOnSubmit(msg);
            //    PayrollDataContext.SubmitChanges();
            //}



            return status;
        }

        /// <summary>
        /// Return the list of employee using Approval flow for Travel Request flow
        /// </summary>
        /// <param name="currentStep"></param>
        /// <param name="employeeId"></param>
        /// <returns></returns>
        public static List<TextValue> GetForwardToListForTravelOrder(int currentStep,int employeeId)
        {

            bool isPreDefinedList = 
                (CommonManager.Setting.LeaveRequestSettingUsingPreDefinedList != null &&
                CommonManager.Setting.LeaveRequestSettingUsingPreDefinedList.Value);


            List<TextValue> list = new List<TextValue>();

            // Load Forward to list
            GetDocumentNextStepUsingApprovalFlowResult nextStep =
                                    CommonManager.GetDocumentNextStep(currentStep,
                                    FlowTypeEnum.TravelOrder, employeeId, 0, false);


            if (nextStep.ApprovalEmployee1 != null)
            {
                list.Add(new TextValue { ID = nextStep.ApprovalEmployee1.Value, Text=
                    EmployeeManager.GetEmployeeName(nextStep.ApprovalEmployee1.Value) + " - " + nextStep.ApprovalEmployee1.Value
                });


            }

            if (nextStep.ApprovalEmployee2 != null)
            {
                list.Add(new TextValue
                {
                    ID = nextStep.ApprovalEmployee2.Value,
                    Text =
                        EmployeeManager.GetEmployeeName(nextStep.ApprovalEmployee2.Value) + " - " + nextStep.ApprovalEmployee2.Value
                });

            }

            if (nextStep.AuthorityType == (int)TravelOrderAuthorityEnum.BranchHead)
            {
                int? currentBranchId = PayrollDataContext.GetCurrentBranch(employeeId,DateTime.Now);
                if(currentBranchId != null)
                {
                    Branch branch = BranchManager.GetBranchById(currentBranchId.Value);
                    if (branch != null && branch.BranchHeadId != null)
                    {
                        list.Add(new TextValue { ID = branch.BranchHeadId.Value, Text = EmployeeManager.GetEmployeeName(branch.BranchHeadId.Value) + " - " + branch.BranchHeadId.Value});
                    }
                }
            }

            if (nextStep.Designationid != null)
            {
                list.AddRange
                   (
                       (
                           from l in PayrollDataContext.EEmployees
                           join h in PayrollDataContext.EHumanResources on l.EmployeeId equals h.EmployeeId
                           orderby l.Name
                           where l.DesignationId == nextStep.Designationid
                           &&
                           (
                             (l.IsResigned == false || h.DateOfResignationEng >= DateTime.Now)
                             ||
                             (l.IsRetired == false || h.DateOfRetirementEng >= DateTime.Now)
                           )
                           select new TextValue
                           {
                               Text = l.Name + " - " + l.EmployeeId,
                               ID = l.EmployeeId
                           }

                       ).ToList()
                    );
            }

            if (nextStep.AuthorityType == (int)TravelOrderAuthorityEnum.AllEmployee)
            {
                list.AddRange
                   (
                       (
                           from l in PayrollDataContext.EEmployees
                           join h in PayrollDataContext.EHumanResources on l.EmployeeId equals h.EmployeeId
                           orderby l.Name
                           where l.EmployeeId != employeeId
                           &&
                           (
                             (l.IsResigned==false || h.DateOfResignationEng >= DateTime.Now)
                             ||
                             (l.IsRetired == false || h.DateOfRetirementEng >= DateTime.Now)
                           )
                           select new TextValue
                           {
                               Text = l.Name + " - " + l.EmployeeId,
                               ID = l.EmployeeId
                           }
                           
                       ).ToList()
                    );
            }
            if (nextStep.AuthorityType == (int)TravelOrderAuthorityEnum.LeaveRecommender)
            {
                if (isPreDefinedList)
                {
                    list.AddRange
                    (
                        (
                            from l in PayrollDataContext.GetPreDefinedApprovalList(true, employeeId, (int)PreDefindFlowType.Leave)
                            where l.EmployeeId != employeeId
                            select new TextValue
                            {
                                Text = l.NAME,
                                ID = l.EmployeeId.Value
                            }
                        ).ToList()
                     );
                }
                else
                {
                    GetLeaveApprovalApplyToCCForEmployeeResult entity = LeaveAttendanceManager.GetLeaveApprovalApplyToAndCCForEmployee(employeeId);
                    if (entity.CC4 != null)
                    {
                        list.Add(new TextValue { Text = entity.CC4Name, ID = entity.CC4.Value });
                    }

                    if (entity.CC5 != null)
                    {
                        list.Add(new TextValue { Text = entity.CC5Name, ID = entity.CC5.Value });
                    }
                }
            }

            if (nextStep.AuthorityType == (int)TravelOrderAuthorityEnum.LeaveApproval)
            {
                if (isPreDefinedList)
                {
                    list.AddRange
                    (
                        (
                            from l in PayrollDataContext.GetPreDefinedApprovalList(false, employeeId, (int)PreDefindFlowType.Leave)
                            where l.EmployeeId != employeeId
                            select new TextValue
                            {
                                Text = l.NAME,
                                ID = l.EmployeeId.Value
                            }
                        ).ToList()
                     );
                }
                else
                {
                    GetLeaveApprovalApplyToCCForEmployeeResult entity = LeaveAttendanceManager.GetLeaveApprovalApplyToAndCCForEmployee(employeeId);
                    if (entity.ApplyTo != null)
                    {
                        list.Add(new TextValue { Text = entity.ApplyToName, ID = entity.ApplyTo.Value });
                    }

                    if (entity.CC1 != null)
                    {
                        list.Add(new TextValue { Text = entity.CC1Name, ID = entity.CC1.Value });

                    }

                    if (entity.CC2 != null)
                    {
                        list.Add(new TextValue { Text = entity.CC2Name, ID = entity.CC2.Value });
                    }

                    if (entity.CC3 != null)
                    {
                        list.Add(new TextValue { Text = entity.CC3Name, ID = entity.CC3.Value });
                    }
                }
            }


            // allow to request to same employee for sana kisan
            return list.OrderBy(x => x.Text).ToList();
            //return list.Where(x=>x.ID != SessionManager.CurrentLoggedInEmployeeId).OrderBy(x => x.Text).ToList();

        }

        public static Status InsertUpdateTravelRequests(TARequest request, List<TARequestLine> lines, TARequestStatusHistory hist, bool isStatusIncrease)
        {
            Status status = new Status();
            TARequest dbEntity = new TARequest();
            if (SessionManager.CurrentLoggedInEmployeeId != 0)
            {
                dbEntity.ModifiedBy = SessionManager.CurrentLoggedInUserID;
                hist.ApprovalEmployeeId = SessionManager.CurrentLoggedInEmployeeId;
            }
            else
            {
                if (SessionManager.CurrentLoggedInEmployeeId == 0 &&
                    (SessionManager.User.UserMappedEmployeeId == null || SessionManager.User.UserMappedEmployeeId == 0 || SessionManager.User.UserMappedEmployeeId == -1))
                {
                    status.IsSuccess = false;
                    status.ErrorMessage = "User is not mapped to admin. One user must be associated with Admin.";
                    return status;
                }
                else
                {
                    dbEntity.ModifiedBy = SessionManager.User.UserID;
                    hist.ApprovalEmployeeId = SessionManager.User.UserMappedEmployeeId;
                }
            }

            // in insert mode check already exists for this date
            if (request.RequestID == 0)
            {
                if (PayrollDataContext.TARequests.Any(x => x.EmployeeId == request.EmployeeId &&
                       (
                       (request.TravellingFromEng >= x.TravellingFromEng && request.TravellingFromEng <= x.TravellingToEng)
                       ||
                        (request.TravellingToEng >= x.TravellingFromEng && request.TravellingToEng <= x.TravellingToEng)
                        )))
                {
                    status.IsSuccess = false;
                    status.ErrorMessage = "Travelling request already exists for this date.";
                    return status;
                }
            }

            dbEntity.ModifiedOn = CommonManager.GetCurrentDateAndTime();
            hist.ApprovedOn = dbEntity.ModifiedOn;
            hist.ApprovedBy = dbEntity.ModifiedBy;

            GetDocumentNextStepUsingApprovalFlowResult nextStep = null;
            int travelRequesterEmpId = 0;

            if (PayrollDataContext.TARequests.Any(x => x.RequestID == request.RequestID))
            {

                dbEntity = PayrollDataContext.TARequests.Where(x => x.RequestID == request.RequestID).FirstOrDefault();

                nextStep = CommonManager.GetDocumentNextStep(dbEntity.Status.Value, FlowTypeEnum.TravelOrder, dbEntity.EmployeeId.Value, 0, false);

                travelRequesterEmpId = dbEntity.EmployeeId.Value;

                if (request.Status != (int)FlowStepEnum.Saved)
                {


                    if (nextStep == null)
                    {
                        status.ErrorMessage = "Travel request can not be changed as already ended.";
                        return status;
                    }

                    dbEntity.ApprovedTotal = request.ApprovedTotal;
                    dbEntity.Status = request.Status;
                    dbEntity.StatusName = nextStep.StepStatusName;

                    // only saved higher level comment
                    if (request.Status != (int)FlowStepEnum.Saved && request.Status != (int)FlowStepEnum.Step1SaveAndSend)
                    {
                        hist.Status = dbEntity.Status.Value;
                        hist.ApprovalDisplayTitle = nextStep.AuthorityName;
                        dbEntity.TARequestStatusHistories.Add(hist);
                    }


                }

                // generate number
                if (CommonManager.SettingTA.IsLocationEditable != null && CommonManager.SettingTA.IsLocationEditable.Value
                    && request.Status != (int)FlowStepEnum.Saved && string.IsNullOrEmpty(dbEntity.Number))
                {
                    SetRefNumber(dbEntity);
                }

                dbEntity.TARequestLines.Clear();
                dbEntity.TARequestLines.AddRange(lines);

                if (request.TARequestLocations.Count != 0)
                {
                    dbEntity.TARequestLocations.Clear();
                    dbEntity.TARequestLocations.AddRange(request.TARequestLocations);

                    dbEntity.LocationName = request.LocationName;
                }

                
                dbEntity.IsDomestic = request.IsDomestic;
                dbEntity.PlaceOfTravel = request.PlaceOfTravel;
                dbEntity.CountryId = request.CountryId;
                dbEntity.CountryName = request.CountryName;
                dbEntity.LocationId = request.LocationId;
                dbEntity.PurposeOfTravel = request.PurposeOfTravel;
                dbEntity.TravellingFromEng = request.TravellingFromEng;
                dbEntity.TravellingToEng = request.TravellingToEng;
                dbEntity.Days = request.Days;
                dbEntity.Night = request.Night;
                dbEntity.TravelBy = request.TravelBy;
                dbEntity.TravelByText = request.TravelByText;
                dbEntity.OtherTravelByName = request.OtherTravelByName;
                dbEntity.ExpensePaidBy = request.ExpensePaidBy;
                dbEntity.ExpensePaidByText = request.ExpensePaidByText;
                dbEntity.Total = request.Total;
                dbEntity.CurrentApprovalSelectedEmployeeId = request.CurrentApprovalSelectedEmployeeId;
                dbEntity.RequestedAdvance = request.RequestedAdvance;
                request.EmployeeId = dbEntity.EmployeeId;
            }
            else
            {

                nextStep = CommonManager.GetDocumentNextStep(0, FlowTypeEnum.TravelOrder, request.EmployeeId.Value, 0, false);

                // generate number
                if(CommonManager.SettingTA.IsLocationEditable != null && CommonManager.SettingTA.IsLocationEditable.Value
                    && request.Status != (int)FlowStepEnum.Saved)
                {
                    SetRefNumber(request);
                }

                request.TARequestLines.AddRange(lines);
                request.CreatedBy = dbEntity.ModifiedBy;
                request.CreatedOn = dbEntity.ModifiedOn;
                request.ModifiedBy = dbEntity.ModifiedBy;
                request.ModifiedOn = dbEntity.ModifiedOn;

                travelRequesterEmpId = request.EmployeeId.Value;

                if (request.Status == (int)FlowStepEnum.Saved)
                    request.StatusName = "Saved";
                else
                {
                    request.StatusName = nextStep.StepStatusName;

                    

                }
                PayrollDataContext.TARequests.InsertOnSubmit(request);

            }

           

            // if being approved then set Attendance Time
            if (request != null && request.Status == (int)FlowStepEnum.Step15End)
            {
                SetTimeAttendance(request.EmployeeId.Value, request.TravellingFromEng.Value, request.TravellingToEng.Value, true);
            }

            PayrollDataContext.SubmitChanges();

            // send message
            if (nextStep != null && travelRequesterEmpId != 0)
            {
                if (request.CurrentApprovalSelectedEmployeeId != null)
                    SendTravelRequestMailAndMessage(EmailContentType.TravelRequestReview, request.CurrentApprovalSelectedEmployeeId.Value, travelRequesterEmpId);

            }

           

            status.IsSuccess = true;
            return status;

        }

        public static int? GetTravelRequestApprovalEmployeeId(int reqId)
        {
            TARequestStatusHistory his = 

                PayrollDataContext.TARequestStatusHistories.Where(x => x.RequestID == reqId && x.Status == 15)
                .OrderByDescending(x => x.ApprovedOn).FirstOrDefault();

            if (his != null)
                return his.ApprovalEmployeeId;

            return null;
        }
        public static Status SetRefNumber(TARequest request)
        {
            Status status = new Status();

            FinancialDate year = PayrollDataContext.FinancialDates.FirstOrDefault(x => request.TravellingFromEng >= x.StartingDateEng
                        && request.TravellingFromEng <= x.EndingDateEng);
            if (year == null)
            {
                status.ErrorMessage = "Financial year not exists for the date " + request.TravellingFromEng.Value.ToShortDateString() + ".";
                return status;
            }

            TANumber number = PayrollDataContext.TANumbers.FirstOrDefault(x => x.YearID == year.FinancialDateId);

            if (number == null)
            {
                number = new TANumber();
                number.YearID = year.FinancialDateId;
                number.LastNumber = 0;
                PayrollDataContext.TANumbers.InsertOnSubmit(number);
            }

            number.LastNumber += 1;
            int? branchId = PayrollDataContext.GetCurrentBranch(request.EmployeeId.Value, request.TravellingFromEng.Value);
            Branch branch = BranchManager.GetBranchById(branchId.Value);
            CustomDate date = CustomDate.GetCustomDateFromString(year.StartingDate, IsEnglish);
            request.Number = branch.Code + "/" + date.Year.ToString().Substring(2) + "-" +
                (date.Year + 1).ToString().Substring(2) + "/" + number.LastNumber;

            return status;
        }
        private static void SetTimeAttendance(int employeeId,DateTime startDate,DateTime endDate,bool skipHoliday)
        {
            DAL.AttendanceMapping map = BLL.BaseBiz.PayrollDataContext.AttendanceMappings
                                      .SingleOrDefault(x => x.EmployeeId == employeeId);

            if (map != null)
            {

                DateTime start = startDate.Date;
                GetEmployeeTimeResult normalTime = PayrollDataContext.GetEmployeeTime(employeeId, start, 1, "", "").FirstOrDefault();
                GetEmployeeTimeResult fridayTime = PayrollDataContext.GetEmployeeTime(employeeId, start, 2, "WH/2", "").FirstOrDefault();
                List<WeeklyHoliday> weeklyHolidays = PayrollDataContext.WeeklyHolidays.ToList();
                bool hasFridayHalfDay = weeklyHolidays.Any(x => x.IsFullDay == false);
                

                if (normalTime.OfficeInTime == null || normalTime.OfficeOutTime == null)
                { }
                else
                {


                    for (; start <= endDate.Date; start = start.AddDays(1))
                    {
                        if (skipHoliday == true)
                        {
                            if (weeklyHolidays.Any(x => x.DayOfWeek == (int)start.DayOfWeek && x.IsFullDay == true))
                            {
                                continue;
                            }
                        }

                        AttendanceCheckInCheckOut chkInOut = new AttendanceCheckInCheckOut();
                        chkInOut.ChkInOutID = Guid.NewGuid();
                        chkInOut.DeviceID = map.DeviceId;
                        chkInOut.AuthenticationType = (int)TimeAttendanceAuthenticationType.TravelRequest;
                        chkInOut.InOutMode = (int)ChkINOUTEnum.ChkIn;
                        chkInOut.ModifiedBy = SessionManager.CurrentLoggedInUserID;
                        chkInOut.ModifiedOn = DateTime.Now;

                        if (hasFridayHalfDay && start.DayOfWeek == DayOfWeek.Friday && fridayTime.OfficeInTime != null)
                            chkInOut.DateTime = new DateTime(start.Date.Year, start.Date.Month, start.Date.Day, fridayTime.OfficeInTime.Value.Hours, fridayTime.OfficeInTime.Value.Minutes, fridayTime.OfficeInTime.Value.Seconds);
                        else
                            chkInOut.DateTime = new DateTime(start.Date.Year, start.Date.Month, start.Date.Day, normalTime.OfficeInTime.Value.Hours, normalTime.OfficeInTime.Value.Minutes, normalTime.OfficeInTime.Value.Seconds);

                        chkInOut.Date = chkInOut.DateTime.Date;
                        chkInOut.ModificationRemarks = "Travel Request Attendance";
                        chkInOut.IPAddress = "";

                        PayrollDataContext.AttendanceCheckInCheckOuts.InsertOnSubmit(chkInOut);


                        AttendanceCheckInCheckOut checkOut = new AttendanceCheckInCheckOut();
                        checkOut.ChkInOutID = Guid.NewGuid();
                        checkOut.DeviceID = map.DeviceId;
                        checkOut.AuthenticationType = (int)TimeAttendanceAuthenticationType.TravelRequest;
                        checkOut.InOutMode = (int)ChkINOUTEnum.ChkOut;
                        checkOut.ModifiedBy = SessionManager.CurrentLoggedInUserID;
                        checkOut.ModifiedOn = DateTime.Now;

                        if (hasFridayHalfDay && start.DayOfWeek == DayOfWeek.Friday && fridayTime.OfficeOutTime != null)
                            checkOut.DateTime = new DateTime(start.Date.Year, start.Date.Month, start.Date.Day, fridayTime.OfficeOutTime.Value.Hours, fridayTime.OfficeOutTime.Value.Minutes, fridayTime.OfficeOutTime.Value.Seconds);
                        else
                            checkOut.DateTime = new DateTime(start.Date.Year, start.Date.Month, start.Date.Day, normalTime.OfficeOutTime.Value.Hours, normalTime.OfficeOutTime.Value.Minutes, normalTime.OfficeOutTime.Value.Seconds);

                        checkOut.Date = checkOut.DateTime.Date;
                        checkOut.ModificationRemarks = "Travel Request Attendance";
                        checkOut.IPAddress = "";

                        PayrollDataContext.AttendanceCheckInCheckOuts.InsertOnSubmit(checkOut);

                    }
                    //PayrollDataContext.SubmitChanges();
                }
            }
        }





        public static Status ApproveTravelRequests(TARequest request, List<TARequestLine> lines, TARequestStatusHistory hist, bool isStatusIncrease)
        {
            Status status = new Status();
            TARequest dbEntity = new TARequest();

            if (SessionManager.CurrentLoggedInEmployeeId != 0)
            {
                dbEntity.ModifiedBy = SessionManager.CurrentLoggedInUserID;
                hist.ApprovalEmployeeId = SessionManager.CurrentLoggedInEmployeeId;
            }
            else
            {
                if (SessionManager.CurrentLoggedInEmployeeId == 0 &&
                    (SessionManager.User.UserMappedEmployeeId == null || SessionManager.User.UserMappedEmployeeId == 0 || SessionManager.User.UserMappedEmployeeId == -1))
                {
                    status.IsSuccess = false;
                    status.ErrorMessage = "Current admin user is not mapped with Employee, please use Manage Users to map with employee user.";
                    return status;
                }
                else
                {
                    dbEntity.ModifiedBy = SessionManager.User.UserID;
                     hist.ApprovalEmployeeId = request.CurrentApprovalSelectedEmployeeId;//request.CurrentApprovalSelectedEmployeeId; ;
                }
            }

            if (request.RequestID == 0)
            {
                if (PayrollDataContext.TARequests.Any(x => x.EmployeeId == request.EmployeeId &&
                       (
                       (request.TravellingFromEng >= x.TravellingFromEng && request.TravellingFromEng <= x.TravellingToEng)
                       ||
                        (request.TravellingToEng >= x.TravellingFromEng && request.TravellingToEng <= x.TravellingToEng)
                        )))
                {
                    status.IsSuccess = false;
                    status.ErrorMessage = "Travel request already exists for this date.";
                    return status;
                }
            }

            dbEntity.ModifiedOn = CommonManager.GetCurrentDateAndTime();
            hist.ApprovedOn = dbEntity.ModifiedOn;
            hist.ApprovedBy = dbEntity.ModifiedBy;


            if (PayrollDataContext.TARequests.Any(x => x.RequestID == request.RequestID))
            {

                
            }
            else
            {

                GetDocumentNextStepUsingApprovalFlowResult nextStep =
                                    CommonManager.GetDocumentNextStep(0, FlowTypeEnum.TravelOrder, request.EmployeeId.Value, 0, false);

                if(hist!=null)
                request.TARequestStatusHistories.Add(hist);
                request.TARequestLines.AddRange(lines);
                request.CreatedBy = dbEntity.ModifiedBy;
                request.CreatedOn = dbEntity.ModifiedOn;
                request.ModifiedBy = dbEntity.ModifiedBy;
                request.ModifiedOn = dbEntity.ModifiedOn;

                request.Status = (int)FlowStepEnum.Step15End;
                request.StatusName = "HR Settled";

                // generate number
                if (CommonManager.SettingTA.IsLocationEditable != null && CommonManager.SettingTA.IsLocationEditable.Value
                    && request.Status != (int)FlowStepEnum.Saved && string.IsNullOrEmpty(request.Number))
                {
                    SetRefNumber(request);
                }

                PayrollDataContext.TARequests.InsertOnSubmit(request);

            }

            request.IsHRAssigned = true;

            


            SetTimeAttendance(request.EmployeeId.Value, request.TravellingFromEng.Value, request.TravellingToEng.Value, true);

            PayrollDataContext.SubmitChanges();

            status.IsSuccess = true;
            return status;

        }






        public static Status SettleTravelRequests(TARequest request, List<TARequestLine> lines, TARequestStatusHistory hist, bool isStatusIncrease)
        {
            Status status = new Status();
            TARequest dbEntity = new TARequest();
            dbEntity.ModifiedBy = SessionManager.CurrentLoggedInUserID;
            dbEntity.ModifiedOn = CommonManager.GetCurrentDateAndTime();
            hist.ApprovedOn = dbEntity.ModifiedOn;
            hist.ApprovedBy = dbEntity.ModifiedBy;

            status.IsSuccess = true;
            if (PayrollDataContext.TARequests.Any(x => x.RequestID == request.RequestID))
            {

                dbEntity = PayrollDataContext.TARequests.Where(x => x.RequestID == request.RequestID).First();
                dbEntity.AdvanceStatus = request.AdvanceStatus;


                foreach (var item in dbEntity.TARequestLines.ToList())
                {
                    PayrollDataContext.TARequestLines.DeleteOnSubmit(item);
                }
               
                

                dbEntity.TARequestLines.AddRange(lines);
                dbEntity.ClaimedTotal = request.ClaimedTotal;
                dbEntity.PurposeOfTravel = request.PurposeOfTravel;
                dbEntity.SettTravellingFromEng = request.SettTravellingFromEng;
                dbEntity.SettTravellingToEng = request.SettTravellingToEng;
                dbEntity.SettDays = request.SettDays;
                dbEntity.SettNight = request.SettNight;
                dbEntity.SettTravelBy = request.SettTravelBy;
                dbEntity.SettTravelByText = request.SettTravelByText;
                dbEntity.SettExpensePaidBy = request.SettExpensePaidBy;
                dbEntity.SettExpensePaidByText = request.SettExpensePaidByText;
                dbEntity.SettledTotal = request.SettledTotal;

            }
            else
            {
                status.IsSuccess = false;
                status.ErrorMessage = "Request Not found!";
            }

            PayrollDataContext.SubmitChanges();
            return status;

        }





        public static TARequest getTARequestByID(int RequestID)
        {
            return PayrollDataContext.TARequests.Where(x => x.RequestID == RequestID).FirstOrDefault();
        }


        public static int GetLastStatus(int flowType)
        {
            int lastStatus = 0;
            lastStatus = PayrollDataContext.ApprovalFlows.Where(x => x.FlowType == flowType).OrderBy(y => y.SequenceNo).LastOrDefault().StepID;
            return lastStatus;
        }

        public static List<GetTravelRequestSettledResult> GetAllTravelRequestSettlement(int BranchID,DateTime startDate, DateTime endDate, int StatusType, string NameFilter)
        {
            int lastStatus = 0;
            lastStatus = (int)FlowStepEnum.Step15End;

            List<GetTravelRequestSettledResult> iList = new List<GetTravelRequestSettledResult>();
            iList = PayrollDataContext.GetTravelRequestSettled(SessionManager.CurrentLoggedInEmployeeId, (int)FlowTypeEnum.TravelOrder, startDate, endDate, BranchID, StatusType, NameFilter, lastStatus).ToList();
            foreach (var items in iList)
            {
                items.EstimatedTotal = GetEstimatedTotal(items.RequestID, null);
                
                if (items.Status != (int)FlowStepEnum.Settled)
                {
                    if (items.EstimatedTotal - items.AdvanceAmount > 0)
                    {
                        items.ToBePaid = items.EstimatedTotal - items.AdvanceAmount.Value;
                        items.ToBeRecovered = 0;
                    }
                    else
                    {
                        items.ToBeRecovered = items.AdvanceAmount.Value - items.EstimatedTotal;
                        items.ToBePaid = 0;
                    }
                }
                else
                {
                    items.SettledTotal = GetEstimatedTotal(items.RequestID, (int)FlowStepEnum.Settled);
                    if (items.EstimatedTotal - (items.AdvanceAmount.Value+items.SettledTotal) > 0)
                    {
                        items.ToBePaid = items.EstimatedTotal - (items.AdvanceAmount.Value+items.SettledTotal);
                        items.ToBeRecovered = 0;
                    }
                    else
                    {
                        items.ToBeRecovered = (items.AdvanceAmount.Value + items.SettledTotal)-items.EstimatedTotal;
                        items.ToBePaid = 0;
                    }
                }

                if (items.Status == (int)FlowStepEnum.Step2)
                {
                    items.StatusModified = FlowStepEnum.Step2.ToString();
                }
                if (items.Status == (int)FlowStepEnum.Step6)
                {
                    items.StatusModified = FlowStepEnum.Step6.ToString();
                }
                if (items.Status == (int)FlowStepEnum.Step3)
                {
                    items.StatusModified = FlowStepEnum.Step3.ToString();
                }
                if (items.Status == (int)FlowStepEnum.Step4)
                {
                    items.StatusModified = FlowStepEnum.Step4.ToString();
                }
                if (items.Status == (int)FlowStepEnum.Step5)
                {
                    items.StatusModified = FlowStepEnum.Step5.ToString();
                }

            }
            return iList;
        }


        public static decimal GetEstimatedTotal(int requestID, int? EstOrApproved)
        {
               //null for Approved, int from Enum for Settled
                return PayrollDataContext.TARequests.Where(x => x.RequestID == requestID ).FirstOrDefault().TARequestLines.Where(z=>z.UnitType == EstOrApproved).Sum(y => y.Total).Value;
            
        }


        public static List<GetTravelRequestAdvanceResult> GetAllTravelRequestAdvance(int start, int limit, DateTime? startDate, DateTime? endDate, int? advanceStatus, int type, int employeeId, string empName, string destination, ref int totalRecords)
        {
            int lastStatus = 0;
            lastStatus = (int)FlowStepEnum.Step15End;

            List<GetTravelRequestAdvanceResult> iList = new List<GetTravelRequestAdvanceResult>();
            iList = PayrollDataContext.GetTravelRequestAdvance(SessionManager.CurrentLoggedInEmployeeId, 
                (int)FlowTypeEnum.TravelOrder, startDate, endDate, lastStatus,advanceStatus, type, employeeId, empName, destination).ToList();

            totalRecords = iList.Count;

            if (start >= 0 && limit > 0)
            {
                iList = iList.Skip(start).Take(limit).ToList();
            }

            foreach (var item in iList)
            {
                if (item.AdvanceStatus != null)
                {
                    AdvanceStatusEnum stat = (AdvanceStatusEnum)item.AdvanceStatus.Value;
                    if (stat == AdvanceStatusEnum.HRAdvance)
                        item.AdvanceStatusText = "Advance Set";
                    else if (stat == AdvanceStatusEnum.EmployeeSettle)
                        item.AdvanceStatusText = "Employee Settled";
                    else
                        item.AdvanceStatusText = "HR Settled";
                }
                else
                    item.AdvanceStatusText = "Advance Not Set";
            }

            return iList;
        }




        public static List<GetTravelRequestsForHRResult> GetAllTravelRequestForHR(DateTime? startDate, DateTime? endDate)
        {
            

            List<GetTravelRequestsForHRResult> iList = new List<GetTravelRequestsForHRResult>();
            iList = PayrollDataContext.GetTravelRequestsForHR(SessionManager.CurrentLoggedInEmployeeId,(int)FlowTypeEnum.TravelOrder,startDate, endDate).ToList();
            foreach (var items in iList)
            {
                if (items.Status == (int)FlowStepEnum.Step2)
                {
                    items.StatusModified = FlowStepEnum.Step2.ToString();
                }
                if (items.Status == (int)FlowStepEnum.Step6)
                {
                    items.StatusModified = FlowStepEnum.Step6.ToString();
                }
                if (items.Status == (int)FlowStepEnum.Step3)
                {
                    items.StatusModified = FlowStepEnum.Step3.ToString();
                }
                if (items.Status == (int)FlowStepEnum.Step4)
                {
                    items.StatusModified = FlowStepEnum.Step4.ToString();
                }
                if (items.Status == (int)FlowStepEnum.Step5)
                {
                    items.StatusModified = FlowStepEnum.Step5.ToString();
                }

            }
            return iList;
        }


        public static List<GetTravelRequestsForAdminResult> GetAllTravelRequestForAdmin(int start, int limit, DateTime? startDate, DateTime? endDate,int status, int type, int employeeId, string empSearch, string destinationSearch, ref int totalRecords)
        {
            List<GetTravelRequestsForAdminResult> iList = new List<GetTravelRequestsForAdminResult>();
            iList = PayrollDataContext.GetTravelRequestsForAdmin((int)FlowTypeEnum.TravelOrder, startDate, endDate, status, type, employeeId, empSearch, destinationSearch).ToList();
            totalRecords = iList.Count;

            if (start >= 0 && limit > 0)
            {
                iList = iList.Skip(start).Take(limit).ToList();
            }


            //foreach (var items in iList)
            //{
            //    if (items.Status == (int)FlowStepEnum.Save)
            //    {
            //        items.StatusModified = FlowStepEnum.Save.ToString();
            //    }
            //    if (items.Status == (int)FlowStepEnum.Approve)
            //    {
            //        items.StatusModified = FlowStepEnum.Approve.ToString();
            //    }
            //    if (items.Status == (int)FlowStepEnum.Recomend)
            //    {
            //        items.StatusModified = FlowStepEnum.Recomend.ToString();
            //    }
            //    if (items.Status == (int)FlowStepEnum.Review)
            //    {
            //        items.StatusModified = FlowStepEnum.Review.ToString();
            //    }
            //    if (items.Status == (int)FlowStepEnum.ReviewAndForward)
            //    {
            //        items.StatusModified = FlowStepEnum.ReviewAndForward.ToString();
            //    }

            //}
            return iList;
        }



        public static List<GetTravelRequestsForFinanceResult> GetAllTravelRequestForFinance(DateTime? startDate, DateTime? endDate, int status, string EmpName,int branch)
        {
            List<GetTravelRequestsForFinanceResult> iList = new List<GetTravelRequestsForFinanceResult>();
            iList = PayrollDataContext.GetTravelRequestsForFinance((int)FlowTypeEnum.TravelOrder, startDate, endDate, status, EmpName, branch).ToList();
            foreach (var items in iList)
            {
                if(items.AdvanceStatus == (int)AdvanceStatusEnum.HRAdvance)
                {
                    items.AdvanceStatusModified = "Advance";
                }
                if (items.AdvanceStatus == (int)AdvanceStatusEnum.EmployeeSettle)
                {
                    items.AdvanceStatusModified = "Emp. Settle";
                }
                if (items.AdvanceStatus == (int)AdvanceStatusEnum.HRSettle)
                {
                    items.AdvanceStatusModified = "Settled";
                }

                if (items.ToBePaidOrRec == null)
                    items.ToBePaidOrRec = 0;

                if (items.ToBePaidOrRec >= 0)
                {
                    items.ToBePaid = Math.Abs(items.ToBePaidOrRec.Value);
                    items.ToBeRecieved = 0;
                }
                else
                {
                    items.ToBeRecieved = Math.Abs(items.ToBePaidOrRec.Value);
                    items.ToBePaid = 0;
                }

            }
           
            return iList;
        }


        public static List<GetTravelRequestsByUserResult> GetAllTravelRequestListingByUser( int EmployeeID,DateTime? startDate, DateTime? endDate, int type)
        {
            List<GetTravelRequestsByUserResult> iList = new List<GetTravelRequestsByUserResult>();
            iList = PayrollDataContext.GetTravelRequestsByUser(EmployeeID, startDate, endDate, type).ToList();
           


            return iList;
        }

        public static TARequest getRequestByID(int requestID)
        {
            return PayrollDataContext.TARequests.Where(x => x.RequestID == requestID).FirstOrDefault();
        }

        public static List<TARequestStatusHistory> getTravelRequestForwardHistory(string stringRequestID)
        {
            int requestID = int.Parse(stringRequestID);
            List<TARequestStatusHistory> iList = new List<TARequestStatusHistory>();
            iList = PayrollDataContext.TARequestStatusHistories
                .Where(x => x.RequestID == requestID && x.Status > (int)FlowStepEnum.Step1SaveAndSend).OrderBy(y => y.ApprovedOn).ToList();
          
            
            //foreach (var item in iList)
            //{
            //    item.ApprovedByName = EmployeeManager.GetEmployeeById(item.TARequest.EmployeeId.Value).Name;
            //    item.whoseComment = EmployeeManager.GetEmployeeById(item.TARequest.EmployeeId.Value).Department.Name +" 's Comment";
            //}
            
            return iList;
        }



        public static List<ApprovalFlow> GetDefaultApprovalFlowList(int flowType)
        {
            List<ApprovalFlow> iList = new List<ApprovalFlow>();
            ApprovalFlow inst = new ApprovalFlow();
            if (PayrollDataContext.ApprovalFlows.Any(x => x.FlowType == flowType))
            {
                return PayrollDataContext.ApprovalFlows.Where(y => y.FlowType == flowType).OrderBy(x => x.StepID).ToList();
            }
          
            return iList;
        }
        public static List<ApprovalFlow> GetApprovalFlowListForEmployeeAppraisal(int employeeFormId)
        {
            List<ApprovalFlow> list = new List<ApprovalFlow>();
            ApprovalFlow inst = new ApprovalFlow();
            AppraisalEmployeeForm form = AppraisalManager.GetEmployeeAppraisaleForm(employeeFormId);
            if (PayrollDataContext.ApprovalFlows.Any(x => x.FlowType == (int)FlowTypeEnum.Appraisal))
            {
                list = PayrollDataContext.ApprovalFlows.Where(y => y.FlowType == (int)FlowTypeEnum.Appraisal)
                    .OrderBy(x => x.StepID).ToList();

                List<AppraisalEmployeeFlow> flowList = AppraisalManager.GetFlowList(employeeFormId);
                if (flowList.Count > 0)
                {
                    // remove steps not saved
                    for (int i = list.Count - 1; i >= 0; i--)
                    {
                        if (flowList.Any(x => x.StepRef_ID == list[i].StepID) == false)
                        {
                            list.RemoveAt(i);
                        }
                    }
                }
            }

            return list.Where(x => x.StepID < form.Status).ToList(); ;
        }
        public static List<LeaveApprovalPreDefineEmployee> GetLeaveApprovalFlowList(bool isForRecommend
            , PreDefindFlowType type)
        {
            List<LeaveApprovalPreDefineEmployee> iList = new List<LeaveApprovalPreDefineEmployee>();
            LeaveApprovalPreDefineEmployee inst = new LeaveApprovalPreDefineEmployee();
            return PayrollDataContext.LeaveApprovalPreDefineEmployees
                .Where(x => x.IsRecommend == isForRecommend && x.Type==(int)type).OrderBy(x => x.Sequence).ToList();

         
        }

        public static List<ApprovalFlow> getTravelOrderListForCombo()
        {
            List<ApprovalFlow> returnList = new List<ApprovalFlow>
            {
                new ApprovalFlow{StepID= (int)FlowStepEnum.Step1SaveAndSend, StepName = "Start"},
                new ApprovalFlow{StepID= (int)FlowStepEnum.Step2, StepName = "Step 2"},
                new ApprovalFlow{StepID= (int)FlowStepEnum.Step3, StepName = "Step 3"},
                new ApprovalFlow{StepID= (int)FlowStepEnum.Step4, StepName = "Step 4"},
                new ApprovalFlow{StepID= (int)FlowStepEnum.Step5, StepName = "Step 5"},

                new ApprovalFlow{StepID= (int)FlowStepEnum.Step6, StepName = "Step 6"},
                new ApprovalFlow{StepID= (int)FlowStepEnum.Step7, StepName = "Step 7"},
                new ApprovalFlow{StepID= (int)FlowStepEnum.Step8, StepName = "Step 8"},
                new ApprovalFlow{StepID= (int)FlowStepEnum.Step9, StepName = "Step 9"},
                new ApprovalFlow{StepID= (int)FlowStepEnum.Step10, StepName = "Step 10"},
                new ApprovalFlow{StepID= (int)FlowStepEnum.Step15End, StepName = "End"},
            };

            //List<TextValue> list = NewHelper.GetTextValues(typeof(FlowStepEnum));

            //List<ApprovalFlow> returnList =
            //    (
            //        from l in list
            //        select new ApprovalFlow
            //        {
            //            StepID = int.Parse(l.Value),
            //            StepName = l.Text
            //        }
            //    ).ToList();


            return returnList;
        }

        public static int GetNextStatus(int currentStatus, int FlowType)
        {
            ApprovalFlow item;
            List<ApprovalFlow> lines = new List<ApprovalFlow>();
            lines = PayrollDataContext.ApprovalFlows.Where(x=>x.FlowType == FlowType).OrderBy(y=>y.StepID).ToList();
            item  = lines.Where(x => x.StepID == currentStatus).FirstOrDefault();

            if (item != null)
            {
                //If Current Status is present in FlowType Table then Next Status is returned. 
                int index = lines.IndexOf(item);
                if (index+1 != lines.Count())
                {
                    return lines[index + 1].StepID;
                }
                else
                {
                    return lines[index].StepID;
                }
            }
            else
            {
                //If the Current Status is not in the FlowType Table it assumes it is Create status.
                return lines[0].StepID;
            }

            //return lines[0].StepID;
        }

        /// <summary>
        /// Returns department head of current branch/dep for the employee in case of Head office, if not head office
        /// then it will return branch manager, currently used in appraisal rollout page only
        /// </summary>
        /// <param name="empId"></param>
        /// <returns></returns>
        public static int? GetEmployeeCurrentDepartmentHead(int empId)
        {
            int? depid = PayrollDataContext.GetEmployeeCurrentDepartmentForDate(DateTime.Now, empId);
            int? branchId = PayrollDataContext.GetCurrentBranch(empId, DateTime.Now);

            if(branchId != null && BranchManager.GetHeadOffice().BranchId == branchId)
            {
                if (depid != null)
                {
                    BranchDepartment dep =
                        PayrollDataContext.BranchDepartments.FirstOrDefault(
                        x => x.DepartmentID == depid);
                    if (dep != null && dep.HeadEmployeeId != null)
                        return dep.HeadEmployeeId.Value;
                }
            }

            // non head office branch
            else if(branchId != null)
            {
                return BranchManager.GetBranchById(branchId.Value).BranchHeadId;
            }
           

            return null;
        }


        /// <summary>
        /// Travel Order Approval flow
        /// </summary>
        /// <returns></returns>
        public static List<ApprovalFlow> getAuthorityTypeListForCombo(int type)
        {
            FlowTypeEnum flowType = (FlowTypeEnum)type;

            if (flowType == FlowTypeEnum.Appraisal)
            {

                List<ApprovalFlow> returnList = new List<ApprovalFlow>
                {
                
                    //new ApprovalFlow{AuthorityType= (int)TravelOrderAuthorityEnum.BranchHead, AuthorityTypeName = "Branch Head"},
                    new ApprovalFlow{AuthorityType= (int)TravelOrderAuthorityEnum.DepartmentHead, AuthorityTypeName = "Department Head"},
                    new ApprovalFlow{AuthorityType= (int)TravelOrderAuthorityEnum.Employee, AuthorityTypeName = "Employee"},
                    new ApprovalFlow{AuthorityType= (int)TravelOrderAuthorityEnum.Recommend1, AuthorityTypeName = "Recommend 1"},
                    new ApprovalFlow{AuthorityType= (int)TravelOrderAuthorityEnum.Approval1, AuthorityTypeName = "Approval 1"},
                    new ApprovalFlow{AuthorityType= (int)TravelOrderAuthorityEnum.SpecificPerson, AuthorityTypeName = "Specific Person"},
                    //new ApprovalFlow{AuthorityType= (int)TravelOrderAuthorityEnum.TeamLead, AuthorityTypeName = "Team Manager"}
                };

                return returnList;
            }
            else
            {
                List<ApprovalFlow> returnList = new List<ApprovalFlow>
                {
                
                    new ApprovalFlow{AuthorityType= (int)TravelOrderAuthorityEnum.BranchHead, AuthorityTypeName = "Branch Head"},
                    //new ApprovalFlow{AuthorityType= (int)TravelOrderAuthorityEnum.DepartmentHead, AuthorityTypeName = "Department Head"},
                    new ApprovalFlow{AuthorityType= (int)TravelOrderAuthorityEnum.Employee, AuthorityTypeName = "Employee"},
                    new ApprovalFlow{AuthorityType= (int)TravelOrderAuthorityEnum.LeaveApproval, AuthorityTypeName = "Leave Approval"},
                    new ApprovalFlow{AuthorityType= (int)TravelOrderAuthorityEnum.LeaveRecommender, AuthorityTypeName = "Leave Recommender"},
                    new ApprovalFlow{AuthorityType= (int)TravelOrderAuthorityEnum.SpecificPerson, AuthorityTypeName = "Specific Person"},
                    new ApprovalFlow{AuthorityType= (int)TravelOrderAuthorityEnum.AllEmployee, AuthorityTypeName = "All Employee"}
                };

                return returnList;
            }
        }
        /// <summary>
        /// Travel Order Approval flow
        /// </summary>
        /// <returns></returns>
        public static List<TextValue> GetAdditionalSteps(int type)
        {
            FlowTypeEnum flowType = (FlowTypeEnum)type;

            if (flowType == FlowTypeEnum.Appraisal)
            {

                List<TextValue> returnList = new List<TextValue>
                {
                
                    //new ApprovalFlow{AuthorityType= (int)TravelOrderAuthorityEnum.BranchHead, AuthorityTypeName = "Branch Head"},
                    new TextValue{Value= ((int)AppraisalAdditionalStep.RecommendationBlock).ToString(), Text = "Show Recommendation Block"},
                    new TextValue{Value= ((int)AppraisalAdditionalStep.AddAdditionPoints).ToString(), Text = "Add Additional Points"},
                    new TextValue{Value= ((int)AppraisalAdditionalStep.AddAdditionPointsAndRecommendationBoth).ToString(), Text = "Recommendation and Additioinal Points"}
                };

                return returnList;
            }
            else
            {
                return new List<TextValue>();
            }
        }
        public static List<ApprovalFlow> getLeaveAuthorityTypeListForCombo()
        {

            List<ApprovalFlow> returnList = new List<ApprovalFlow>
            {
                
                //new ApprovalFlow{AuthorityType= (int)TravelOrderAuthorityEnum.BranchHead, AuthorityTypeName = "Branch Head"},
                //new ApprovalFlow{AuthorityType= (int)TravelOrderAuthorityEnum.DepartmentHead, AuthorityTypeName = "Department Head"},
                new ApprovalFlow{AuthorityType= (int)LeaveRequestAuthorityEnum.BranchHead, AuthorityTypeName = "Branch Head"},
                new ApprovalFlow{AuthorityType= (int)LeaveRequestAuthorityEnum.DepartmentHead, AuthorityTypeName = "Department Head"},
                new ApprovalFlow{AuthorityType= (int)LeaveRequestAuthorityEnum.ByDesignation, AuthorityTypeName = "Specific Designation"},
                new ApprovalFlow{AuthorityType= (int)LeaveRequestAuthorityEnum.RegionalHead, AuthorityTypeName = "Regional Head"},
                new ApprovalFlow{AuthorityType= (int)LeaveRequestAuthorityEnum.SpecificPerson, AuthorityTypeName = "Specific Person"}
                //new ApprovalFlow{AuthorityType= (int)TravelOrderAuthorityEnum.TeamLead, AuthorityTypeName = "Team Manager"}
            };

            return returnList;
        }        

        public static Status UpdateTravelOrder(List<ApprovalFlow> lines,int flowType)
        {
            Status status = new Status();

            foreach (var item in PayrollDataContext.ApprovalFlows.Where(x=>x.FlowType==flowType).ToList())
            {
                PayrollDataContext.ApprovalFlows.DeleteOnSubmit(item);
            }

            PayrollDataContext.ApprovalFlows.InsertAllOnSubmit(lines);
            status.IsSuccess = true;
            PayrollDataContext.SubmitChanges();
            return status;
        }

        public static void AddEmail(LeavePredefinedNotification item,List<string> emailList)
        {
            string email = "";
            if (item.EmployeeId1 != null)
            {
                email = EmployeeManager.GetEmployeeOfficialEmail(item.EmployeeId1.Value);
                if (!string.IsNullOrEmpty(email))
                    emailList.Add(email);
            }

            if (item.EmployeeId2 != null)
            {
                email = EmployeeManager.GetEmployeeOfficialEmail(item.EmployeeId2.Value);
                if (!string.IsNullOrEmpty(email))
                    emailList.Add(email);
            }

            if (!string.IsNullOrEmpty(item.Emails))
            {
                string[] values = item.Emails.Split(new char[] { ',' });
                foreach (string value in values)
                    emailList.Add(value);
            }

        }



        public static void SetBCCLeaveApprovalNotification(int leaveEmployeeId, int approvalEmployeeId,int? recommenderEmpId, ref string bcc,ref string from,int leaveTypeId)
        {
            if (CommonManager.Setting.LeaveRequestSettingUsingPreDefinedList == null || CommonManager.Setting.LeaveRequestSettingUsingPreDefinedList == false)
            {

                SetBCCForLeaveTeam(leaveEmployeeId,approvalEmployeeId,ref bcc,ref from);
            }
            else
            {
                SetBCCPreDefinedNotification(leaveEmployeeId, ref bcc, ref from,leaveTypeId);
            }

            if (recommenderEmpId != null)
            {
                string recommenderEmail = EmployeeManager.GetEmployeeEmail(recommenderEmpId.Value);
                if (!string.IsNullOrEmpty(recommenderEmail))
                {
                    if (bcc.EndsWith(",") || bcc == "")
                        bcc += recommenderEmail;
                    else
                        bcc += "," + recommenderEmail;
                }
            }
        }

        private static string SetBCCForLeaveTeam(int leaveEmployeeId,int approvalEmployeeId, ref string bcc,ref string from)
        {
            GetRecipientEmailResult entity = BLL.BaseBiz.PayrollDataContext.GetRecipientEmail(leaveEmployeeId).SingleOrDefault();
            EEmployee emp = new EmployeeManager().GetById(leaveEmployeeId);

            if (entity != null && entity.LeaveApprovalNotificationType != null && entity.LeaveApprovalNotificationType != 0)
            {

                List<GetToEmailResult> list = BLL.BaseBiz.PayrollDataContext.GetToEmail(leaveEmployeeId,
                    approvalEmployeeId, entity.LeaveApprovalNotificationType,
                    emp.BranchId, emp.DepartmentId).ToList();

                if (string.IsNullOrEmpty(from) && list.Count > 0)
                {
                    from = list[0].Email.Trim();
                    list.Remove(list[0]);
                }

                bcc = string.Join(",", list.Select(x => x.Email).ToList().ToArray());
            }


            // append CC Email list for the Employee Team
            LeaveApprovalEmployee empTeam = LeaveAttendanceManager.GetEmployeeLeaveApprovalTeam(leaveEmployeeId);
            if (empTeam != null && !string.IsNullOrEmpty(empTeam.CCEmailList) && empTeam.CCEmailList.Length > 0)
            {
                if (empTeam.CCEmailList[0] == ',')
                    bcc += empTeam.CCEmailList;
                else
                    bcc += "," + empTeam.CCEmailList;
            }
            return bcc;
        }

        private static void SetBCCPreDefinedNotification(int leaveEmployeeId, ref string bcc, ref string from, int leaveTypeId)
        {
            List<LeavePredefinedNotification> notification = 
                PayrollDataContext.LeavePredefinedNotifications
                .Where(x=>x.LeaveTypeId == null || x.LeaveTypeId == leaveTypeId).ToList();

            List<string> emailList = new List<string>();

            bool isBranchHead = PayrollDataContext.Branches.Any(x => x.BranchHeadId == leaveEmployeeId);
            bool isRegionalHead = PayrollDataContext.Branches.Any(x => x.RegionalHeadId == leaveEmployeeId);

            EEmployee emp = new EmployeeManager().GetById(leaveEmployeeId);
            int empLeveGroupId = emp.EDesignation.BLevel.LevelGroupId;

            // All case
            foreach (LeavePredefinedNotification item in notification.Where(x => x.AuthorityType != null))
            {
                LeavePredefinedNotificationEnum type = (LeavePredefinedNotificationEnum)item.AuthorityType.Value;
                switch (type)
                {
                    case LeavePredefinedNotificationEnum.AllEmployee:
                        AddEmail(item, emailList);
                        break;

                    case LeavePredefinedNotificationEnum.BranchHead:
                        if (isBranchHead)
                            AddEmail(item, emailList);
                        break;
                    case LeavePredefinedNotificationEnum.RegionHead:
                        if (isRegionalHead)
                            AddEmail(item, emailList);
                        break;
                    case LeavePredefinedNotificationEnum.SpecificBranch:
                        if (item.BranchId != null && emp.BranchId == item.BranchId)
                            AddEmail(item, emailList);
                        break;
                    case LeavePredefinedNotificationEnum.Group:
                        if (item.LevelGroupId != null && empLeveGroupId == item.LevelGroupId)
                            AddEmail(item, emailList);
                        break;
                }

            }

            bcc = "";

            // If no email in From then set from other email list
            if (string.IsNullOrEmpty(from) && emailList.Count > 0)
            {
                from = emailList[0].Trim();
                emailList.Remove(emailList[0]);
            }

            for (int i = 0; i < emailList.Count; i++)
            {
                if (bcc == "")
                    bcc = emailList[i];
                else
                    bcc += "," + emailList[i];
            }
        }

        public static Status UpdateLeaveOrder(List<LeaveApprovalPreDefineEmployee> lines,List<LeaveApprovalPreDefineEmployee> linesApproval, List<LeavePredefinedNotification> listLeavePredefinedNot
            ,int type)
        {
            Status status = new Status();


            PayrollDataContext.LeaveApprovalPreDefineEmployees.DeleteAllOnSubmit(
                PayrollDataContext.LeaveApprovalPreDefineEmployees.Where(x=>x.Type==type).ToList());
            listLeavePredefinedNot = listLeavePredefinedNot.Where(x => x.AuthorityType != null).ToList();


            foreach (LeaveApprovalPreDefineEmployee item in lines)
                item.Type = type;

            foreach (LeaveApprovalPreDefineEmployee item in linesApproval)
                item.Type = type;

            PayrollDataContext.LeavePredefinedNotifications.DeleteAllOnSubmit(PayrollDataContext.LeavePredefinedNotifications.ToList());

            PayrollDataContext.LeaveApprovalPreDefineEmployees.InsertAllOnSubmit(lines);
            PayrollDataContext.LeaveApprovalPreDefineEmployees.InsertAllOnSubmit(linesApproval);
            PayrollDataContext.LeavePredefinedNotifications.InsertAllOnSubmit(listLeavePredefinedNot);

            status.IsSuccess = true;

            PayrollDataContext.SubmitChanges();
            return status;
        }

        public static List<ApprovalFlow> GetLeavePredefinedAuthorityTypeList()
        {
            List<ApprovalFlow> returnList = new List<ApprovalFlow>
            {
                new ApprovalFlow{AuthorityType = (int)LeavePredefinedNotificationEnum.AllEmployee, AuthorityTypeName = "All Approval"},
                new ApprovalFlow{AuthorityType = (int)LeavePredefinedNotificationEnum.BranchHead, AuthorityTypeName = "Branch Head Approval"},
                new ApprovalFlow{AuthorityType = (int)LeavePredefinedNotificationEnum.RegionHead, AuthorityTypeName = "Region Head Approval"},
                new ApprovalFlow{AuthorityType = (int)LeavePredefinedNotificationEnum.SpecificBranch, AuthorityTypeName = "Branch Employee Approval"},
                 new ApprovalFlow{AuthorityType = (int)LeavePredefinedNotificationEnum.Group, AuthorityTypeName = "Group"}
            };
            return returnList;
        }
       
        public static List<LeavePredefinedNotification> GetLeavePredefinedNotificationList()
        {
            return PayrollDataContext.LeavePredefinedNotifications.OrderBy(x => x.ID).ToList();
        }

        public static Status SaveTravelRequestHR(TARequest request, List<TARequestLine> lines, TARequestStatusHistory hist, bool isStatusIncrease)
        {
            Status status = new Status();
            TARequest dbEntity = new TARequest();
            dbEntity.ModifiedBy = SessionManager.CurrentLoggedInUserID;
            dbEntity.ModifiedOn = CommonManager.GetCurrentDateAndTime();

            status.IsSuccess = true;
            if (PayrollDataContext.TARequests.Any(x => x.RequestID == request.RequestID))
            {

                dbEntity = PayrollDataContext.TARequests.Where(x => x.RequestID == request.RequestID).First();

                foreach (var item in dbEntity.TARequestLines.ToList())
                {
                    item.Total = lines.SingleOrDefault(x => x.AllowanceId == item.AllowanceId && x.RequestId == item.RequestId).Total;
                    item.FinalAllowType = lines.SingleOrDefault(x => x.AllowanceId == item.AllowanceId && x.RequestId == item.RequestId).AllowType;
                    item.FinalTotal = lines.SingleOrDefault(x => x.AllowanceId == item.AllowanceId && x.RequestId == item.RequestId).FinalTotal;
                    
                }

                dbEntity.Total = request.Total;
            }
            else
            {
                status.IsSuccess = false;
                status.ErrorMessage = "Request Not found!";
            }

            PayrollDataContext.SubmitChanges();
            return status;

        }

        public static List<GetTravelRequestsByUserResult> GetAllTravelRequestListingByUserForAS(int start, int limit,int EmployeeID, DateTime? startDate, DateTime? endDate, int type, out int totalRecords)
        {
            List<GetTravelRequestsByUserResult> iList = new List<GetTravelRequestsByUserResult>();
            iList = PayrollDataContext.GetTravelRequestsByUser(EmployeeID, startDate, endDate, type).Where(x => x.AdvanceStatus != null).ToList();
            totalRecords = iList.Count;

            if (start >= 0 && limit > 0)
            {
                iList = iList.Skip(start).Take(limit).ToList();
            }

            return iList;
        }



    }

    public class ComboGeneralItems
    {
        public string Name { get; set; }
        public string Value{ get; set; }
        public ComboGeneralItems(string _name, string _value)
        {
            Value = _value;
            Name = _name;
        }
    }

}
