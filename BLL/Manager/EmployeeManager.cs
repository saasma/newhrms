﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using DAL;
using Utils;
using Utils.Calendar;
using Utils.Helper;
using System.Xml.Linq;
using System.Data.Linq;
using BLL.BO;
using BLL.Entity;
using System.Transactions;
using System.Web.UI;
using System.IO;
using System.Web;

namespace BLL.Manager
{
    public class ServicePeroid
    {
        public string FromDate { get; set; }
        public DateTime FromDateEng { get; set; }

        public int CurrentStatus { get; set; }
        public string CurrentStatusText
        {
            get
            {
                List<KeyValue> list = new JobStatus().GetMembers();


                KeyValue value = list.SingleOrDefault(k => k.Key == this.CurrentStatus.ToString());

                if (value == null)
                {
                    //JobStatusEnum status = (JobStatusEnum)this.CurrentStatus;
                    return "";// status.ToString();
                }
                else
                    return value.Value;


            }
        }


        public string ToDate { get; set; }
        public DateTime? ToDateEng { get; set; }

    }
    public class EmployeeManager : BaseBiz
    {

        #region "CIT Request"

        public static bool ForwardRequestInBulk(List<CITChangeRequest> requests, int status)
        {

            foreach (CITChangeRequest req in requests)
            {
                CITChangeRequest reqInstance = PayrollDataContext.CITChangeRequests
                    .Where(x => x.RequestId == req.RequestId).FirstOrDefault();

                if (reqInstance != null && reqInstance.Status == (int)CITRequestStatus.SaveAndSend)
                {

                    if (status == (int)CITRequestStatus.Approved)
                    {
                        CITChangeType type = (CITChangeType)reqInstance.ChangeType;

                        EFundDetail fund = PayrollDataContext.EFundDetails
                            .FirstOrDefault(x => x.EmployeeId == reqInstance.EmployeeId);


                        if (type == CITChangeType.NoCIT)
                        {
                            if (fund.CITIsRate != null && fund.CITIsRate.Value)
                                fund.CITRate = 0;
                            else
                                fund.CITAmount = 0;

                            OptimumPFAndCIT dbOptimum = PayrollDataContext.OptimumPFAndCITs.FirstOrDefault(x => x.EmployeeId == reqInstance.EmployeeId);
                            if (dbOptimum != null)
                            {
                                PayrollDataContext.OptimumPFAndCITs.DeleteOnSubmit(dbOptimum);
                            }
                        }
                        else if (type == CITChangeType.FixedAmount)
                        {
                            OptimumPFAndCIT dbOptimum = PayrollDataContext.OptimumPFAndCITs.FirstOrDefault(x => x.EmployeeId == reqInstance.EmployeeId);
                            if (dbOptimum != null)
                            {
                                PayrollDataContext.OptimumPFAndCITs.DeleteOnSubmit(dbOptimum);
                            }
                            fund.CITIsRate = false;
                            fund.CITAmount = reqInstance.AmountOrRate;
                        }
                        else if (type == CITChangeType.Rate)
                        {
                            OptimumPFAndCIT dbOptimum = PayrollDataContext.OptimumPFAndCITs.FirstOrDefault(x => x.EmployeeId == reqInstance.EmployeeId);
                            if (dbOptimum != null)
                            {
                                PayrollDataContext.OptimumPFAndCITs.DeleteOnSubmit(dbOptimum);
                            }
                            fund.CITIsRate = true;
                            fund.CITRate = reqInstance.AmountOrRate == null ? 0 : (double)reqInstance.AmountOrRate;
                        }
                        else if (type == CITChangeType.OptimumCIT)
                        {
                            OptimumPFAndCIT dbOptimum = PayrollDataContext.OptimumPFAndCITs.FirstOrDefault(x => x.EmployeeId == reqInstance.EmployeeId);
                            int total = 0;
                            int lastPeriodId = CommonManager.GetLastPayrollPeriod().PayrollPeriodId;
                            List<GetEmployeeListForOptimumPFAndCITResult> optimumList
                                = PayManager.GetEmployeeListForOptimum(lastPeriodId, 0, 0, 10, ref total, -1, -1, new EmployeeManager().GetById(reqInstance.EmployeeId.Value).Name);
                            GetEmployeeListForOptimumPFAndCITResult optimum = optimumList.FirstOrDefault(x => x.EID == reqInstance.EmployeeId.Value);


                            if (dbOptimum == null && optimum != null)
                            {

                                DAL.OptimumPFAndCIT entity = new DAL.OptimumPFAndCIT();
                                entity.EmployeeId = reqInstance.EmployeeId.Value;
                                entity.PayrollPeriodId = lastPeriodId;

                                entity.EmployeePFAmount = optimum.EmployeePFAmount;// decimal.Parse(lblEmpPF.Text);
                                entity.CIT = optimum.CITAmount;// decimal.Parse(lblCIT.Text);
                                entity.YearlyForeCast = 0;// optimum.YearlyForeCast;

                                entity.Limit = optimum.OneThird;
                                entity.Actual = optimum.Actual;
                                entity.Allowed = optimum.Allowed;

                                entity.OptimumPF = optimum.OptimumPF;
                                entity.AdjustedPF = optimum.AdjustedPF;
                                entity.OptimumCIT = optimum.OptimumCIT;
                                entity.TotalOptimumCIT = optimum.TotalOptimumCIT;
                                entity.AdjustedCIT = optimum.AdjustedCIT;


                                PayManager.SaveDeleteOptimumList(entity);

                            }
                        }
                    }

                    reqInstance.Status = status;
                    reqInstance.ApprovedBy = SessionManager.CurrentLoggedInUserID;
                    reqInstance.ApprovedOn = CommonManager.GetCurrentDateAndTime();
                }
            }

            PayrollDataContext.SubmitChanges();
            return true;
        }

        public static List<CITChangeRequest> GetUnapprovedCITChangeRequest(int type, string empName, bool showHistory)
        {
            //return (
            //   from e in PayrollDataContext.EEmployees
            //   where e.EmployeeId == employeeId
            //   && (customRoleDepartmentIDList.Count == 0 || customRoleDepartmentIDList.Contains(e.EmployeeId))
            //   select e
            //   ).FirstOrDefault();

            List<CITChangeRequest> list =
                (
                from cc in PayrollDataContext.CITChangeRequests
                join e in PayrollDataContext.EEmployees on cc.EmployeeId equals e.EmployeeId
                where (showHistory == true || cc.Status == (int)CITRequestStatus.SaveAndSend)
                        && (cc.ChangeType == type || type == -1)
                        && e.Name.Contains(empName)
                select cc
                ).ToList();
            //PayrollDataContext.CITChangeRequests
            //.Where(x => x.Status == (int)CITRequestStatus.SaveAndSend && (x.ChangeType == type || type == -1))
            //.ToList();

            EmployeeManager mgr = new EmployeeManager();

            foreach (CITChangeRequest item in list)
            {
                item.EmployeeName = mgr.GetById(item.EmployeeId.Value).Name;

                if (item.ChangeType == 1)
                    item.Type = "No CIT";
                else if (item.ChangeType == 2)
                    item.Type = "Optimum CIT";
                else if (item.ChangeType == 3)
                    item.Type = "Fixed Amount";
                else if (item.ChangeType == 4)
                    item.Type = "Rate";

            }

            return list;
        }

        public static CITChangeRequest GetLastChangeRequest(int employeeId)
        {
            return
                PayrollDataContext.CITChangeRequests
                .Where(x => x.EmployeeId == employeeId && x.Status == (int)CITRequestStatus.SaveAndSend)
                .OrderByDescending(x => x.CreatedOn)
                .FirstOrDefault();
        }

        public static void SaveCITRequest(CITChangeRequest request)
        {
            CITChangeRequest dbLastReq = GetLastChangeRequest(SessionManager.CurrentLoggedInEmployeeId);
            if (dbLastReq == null)
            {
                PayrollDataContext.CITChangeRequests.InsertOnSubmit(request);
            }
            else
            {

                dbLastReq.ChangeType = request.ChangeType;
                dbLastReq.AmountOrRate = request.AmountOrRate;
                dbLastReq.ModifiedOn = GetCurrentDateAndTime();

            }


            PayrollDataContext.SubmitChanges();
        }

        #endregion


        public static int? GetEmployeeGender(int empId)
        {
            return
                (
                from e in PayrollDataContext.EEmployees
                where e.EmployeeId == empId
                select e.Gender
                ).FirstOrDefault();
        }

        public static string GetEmployeeTitleAndFirstName(int empId)
        {
            return
                (
                from e in PayrollDataContext.EEmployees
                where e.EmployeeId == empId
                select e.Title + " " + e.FirstName
                ).FirstOrDefault();
        }

        public static string GetEmployeePhotoThumbnail(int empId)
        {
            return
                (
                from e in PayrollDataContext.HHumanResources
                where e.EmployeeId == empId
                select e.UrlPhotoThumbnail
                ).FirstOrDefault();
        }



        /// <summary>
        /// Return the Status ID for Contract status, mainly important for Level/Grade assigned Matrix
        /// as for those company the Contract status employee salary will be assigned to each employees
        /// </summary>
        /// <returns></returns>
        public static bool IsContractStatus(int statusID)
        {
            StatusName status = PayrollDataContext.StatusNames.FirstOrDefault(x => x.IsContractStatus != null
                && x.IsContractStatus.Value && x.StatusId == statusID);

            //if (status != null)
            //  return status.StatusId;

            if (status != null)
                return true;

            return false;

            //return (int)JobStatusEnum.Contract;
        }

        public static List<Report_CompleteYearTaxReportResult> GetYearEndTaxReport(int yearId)
        {

            int c1 = 0, c2 = 0, c3 = 0, c4 = 0, c5 = 0, c6 = 0, c7 = 0, c8 = 0, c9 = 0, c10 = 0, c11 = 0, c12 = 0;

            List<PayrollPeriod> list =
                PayrollDataContext.PayrollPeriods.Where(x => x.FinancialDateId == yearId).
                OrderBy(x => x.PayrollPeriodId).ToList();

            int i = 1;
            foreach (PayrollPeriod period in list)
            {


                CCalculation calculation =
                    PayrollDataContext.CCalculations.FirstOrDefault(x => x.PayrollPeriodId == period.PayrollPeriodId);

                if (calculation != null)
                {
                    switch (i)
                    {
                        case 1: c1 = calculation.CalculationId; break;
                        case 2: c2 = calculation.CalculationId; break;
                        case 3: c3 = calculation.CalculationId; break;
                        case 4: c4 = calculation.CalculationId; break;
                        case 5: c5 = calculation.CalculationId; break;
                        case 6: c6 = calculation.CalculationId; break;
                        case 7: c7 = calculation.CalculationId; break;
                        case 8: c8 = calculation.CalculationId; break;
                        case 9: c9 = calculation.CalculationId; break;
                        case 10: c10 = calculation.CalculationId; break;
                        case 11: c11 = calculation.CalculationId; break;
                        case 12: c12 = calculation.CalculationId; break;
                    }
                }

                i += 1;
            }



            List<Report_CompleteYearTaxReportResult> list1 = PayrollDataContext.Report_CompleteYearTaxReport
                (yearId, c1, c2, c3, c4, c5, c6, c7, c8, c9, c10, c11, c12).ToList();


            foreach (Report_CompleteYearTaxReportResult item in list1)
            {
                item.SSTTotalAmount = Convert.ToDecimal(item.SSTTaxAmount1) + Convert.ToDecimal(item.SSTTaxAmount2) + Convert.ToDecimal(item.SSTTaxAmount3) +
                        Convert.ToDecimal(item.SSTTaxAmount4) + Convert.ToDecimal(item.SSTTaxAmount5) + Convert.ToDecimal(item.SSTTaxAmount6) +
                        Convert.ToDecimal(item.SSTTaxAmount7) + Convert.ToDecimal(item.SSTTaxAmount8) + Convert.ToDecimal(item.SSTTaxAmount9) +
                        Convert.ToDecimal(item.SSTTaxAmount10) + Convert.ToDecimal(item.SSTTaxAmount11) + Convert.ToDecimal(item.SSTTaxAmount12);

                item.TDSTotalAmount = Convert.ToDecimal(item.TDSTaxAmount1) + Convert.ToDecimal(item.TDSTaxAmount2) + Convert.ToDecimal(item.TDSTaxAmount3) +
                       Convert.ToDecimal(item.TDSTaxAmount4) + Convert.ToDecimal(item.TDSTaxAmount5) + Convert.ToDecimal(item.TDSTaxAmount6) +
                       Convert.ToDecimal(item.TDSTaxAmount7) + Convert.ToDecimal(item.TDSTaxAmount8) + Convert.ToDecimal(item.TDSTaxAmount9) +
                       Convert.ToDecimal(item.TDSTaxAmount10) + Convert.ToDecimal(item.TDSTaxAmount11) + Convert.ToDecimal(item.TDSTaxAmount12);

                item.TotalSST = Convert.ToDecimal(item.SST1) + Convert.ToDecimal(item.SST2) + Convert.ToDecimal(item.SST3) +
                        Convert.ToDecimal(item.SST4) + Convert.ToDecimal(item.SST5) + Convert.ToDecimal(item.SST6) +
                        Convert.ToDecimal(item.SST7) + Convert.ToDecimal(item.SST8) + Convert.ToDecimal(item.SST9) +
                        Convert.ToDecimal(item.SST10) + Convert.ToDecimal(item.SST11) + Convert.ToDecimal(item.SST12);

                item.TotalTDS = Convert.ToDecimal(item.TDS1) + Convert.ToDecimal(item.TDS2) + Convert.ToDecimal(item.TDS3) +
                       Convert.ToDecimal(item.TDS4) + Convert.ToDecimal(item.TDS5) + Convert.ToDecimal(item.TDS6) +
                       Convert.ToDecimal(item.TDS7) + Convert.ToDecimal(item.TDS8) + Convert.ToDecimal(item.TDS9) +
                       Convert.ToDecimal(item.TDS10) + Convert.ToDecimal(item.TDS11) + Convert.ToDecimal(item.TDS12);

                item.TotalAddOnSST = Convert.ToDecimal(item.AddOnSST12);

                item.TotalAddOnTDS = Convert.ToDecimal(item.AddOnTDS1) + Convert.ToDecimal(item.AddOnTDS2) + Convert.ToDecimal(item.AddOnTDS3) +
                       Convert.ToDecimal(item.AddOnTDS4) + Convert.ToDecimal(item.AddOnTDS5) + Convert.ToDecimal(item.AddOnTDS6) +
                       Convert.ToDecimal(item.AddOnTDS7) + Convert.ToDecimal(item.AddOnTDS8) + Convert.ToDecimal(item.AddOnTDS9) +
                       Convert.ToDecimal(item.AddOnTDS10) + Convert.ToDecimal(item.AddOnTDS11) + Convert.ToDecimal(item.AddOnTDS12);
            }


            return list1;
        }

        public static Status UpdateServiceEventAmountForHistory(int sourceId, decimal income1, decimal income2, decimal income3
            , decimal income4)
        {
            Status status = new Status();
            EmployeeServiceHistory history =
                PayrollDataContext.EmployeeServiceHistories.FirstOrDefault(x => x.EventSourceID == sourceId);
            if (history != null)
            {
                history.IsAmountSetInHistory = true;
                history.AmountEditedBy = SessionManager.CurrentLoggedInUserID;
                history.AmountEditedOn = DateTime.Now;

                history.Income1Amount = income1;
                history.Income2Amount = income2;
                history.Income3Amount = income3;
                history.Income4Amount = income4;
            }

            PayrollDataContext.SubmitChanges();

            return status;
        }

        public static bool HasLevelGradeForStatus(int statusId, bool? skipSalaryMatrix)
        {
            if (CommonManager.CompanySetting.HasLevelGradeSalary == false)
                return false;

            if (skipSalaryMatrix != null && skipSalaryMatrix.Value)
                return false;

            //first check in the company if it has Level/Grade system from setting or not
            if (EmployeeManager.IsContractStatus(statusId))
            {
                return false;
            }
            return true;
        }

        public static List<GetServiceHistoryListResult> GetServiceHistoryList(int empId, DateTime? from, DateTime? to, int currentPage, int size, int? eventId)
        {
            List<GetServiceHistoryListResult> list =
                PayrollDataContext.GetServiceHistoryList(empId, from, to, currentPage, size, eventId).ToList();

            if (list.Count <= 50)
            {
                int sn = 1;
                foreach (GetServiceHistoryListResult item in list)
                {
                    item.SN = sn++;

                    if (empId != -1 && IsEnglish == false && !string.IsNullOrEmpty(item.FromDate))
                    {
                        item.FromDate = CustomDate.GetCustomDateFromString(item.FromDate, false).ToStringMonthEng();
                    }
                }
            }

            return list;
        }

        public static int GetNextEIN()
        {
            int maxEIN =
                (
                    from e in PayrollDataContext.EEmployees
                    select e.EmployeeId
                ).Max();
            return maxEIN + 1;
        }


        public static string GetCurrentBranchDepartment(int empId)
        {
            
            GetEmployeeCurrentBranchDepartmentResult entity =  PayrollDataContext.GetEmployeeCurrentBranchDepartment(DateTime.Now, empId).FirstOrDefault();

            return entity.Branch + " - " + entity.Department;
        }

        public static string GetCurrentBranch(int empId)
        {

            GetEmployeeCurrentBranchDepartmentResult entity = PayrollDataContext.GetEmployeeCurrentBranchDepartment(DateTime.Now, empId).FirstOrDefault();

            return entity.Branch;
        }

        public static Status ReHireEmployee(int empid, string iNo, int statusId, string fromDate, string toDate,
            int branchId, int departmentId, string appointNo, int levelId, int positionId, int gradeStep)
        {
            Status status = new Status();

            EEmployee emp = GetEmployeeById(empid);



            PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(empid, (byte)MonetoryChangeLogTypeEnum.Information
                 , "ReHired", "", "From " + fromDate, LogActionEnum.Update));


            // process for Retire Rehire case
            EmployeeServiceHistory retirementEvent =
                (
                    from x in PayrollDataContext.EmployeeServiceHistories
                    join e in PayrollDataContext.HR_Service_Events on x.EventID equals e.EventID
                    where x.EmployeeId == empid && x.DateEng == GetEngDate(fromDate, IsEnglish)
                    && e.EventID == (int)ServiceEventType.Retirement
                    // only for retirement case
                    select x
                ).FirstOrDefault();

            if (retirementEvent != null)
                SaveForRetireRehireCast(empid, CommonManager.GetLastPayrollPeriod(), retirementEvent);


            ECurrentStatus empStatus = new ECurrentStatus();
            empStatus.EmployeeId = empid;
            empStatus.CurrentStatus = statusId;
            empStatus.FromDate = fromDate;
            empStatus.FromDateEng = GetEngDate(fromDate, IsEnglish);
            if (!string.IsNullOrEmpty(toDate))
            {
                empStatus.ToDate = toDate;
                empStatus.ToDateEng = GetEngDate(toDate, IsEnglish);
            }
            empStatus.Note = "ReHired";
            empStatus.EventID = (int)ServiceEventType.ReAppointment;
            empStatus.CreatedBy = SessionManager.User.UserID;
            empStatus.CreatedOn = GetCurrentDateAndTime();
            PayrollDataContext.ECurrentStatus.InsertOnSubmit(empStatus);


            if (PayrollDataContext.EmployeeServiceHistories.Any(
                x => x.EmployeeId == empid && x.EventID == (int)ServiceEventType.ReAppointment &&
               x.DateEng == empStatus.FromDateEng))
            {
                status.ErrorMessage = "Employee already re-hired on this date.";
                return status;
            }

            DesignationHistory desigHistory = new DesignationHistory();
            desigHistory.EmployeeId = empid;
            desigHistory.CreatedBy = SessionManager.User.UserID;
            desigHistory.CreatedOn = empStatus.CreatedOn;
            desigHistory.DesignationId = positionId;
            desigHistory.FromDate = fromDate;
            desigHistory.FromDateEng = empStatus.FromDateEng;
            desigHistory.IsFirst = false;
            desigHistory.Note = "ReHired";
            desigHistory.EventID = (int)ServiceEventType.ReAppointment;
            if (CommonManager.CompanySetting.HasLevelGradeSalary)
            {

                PIncome basicIncome = new PayManager().GetBasicIncome(SessionManager.CurrentCompanyId);
                PEmployeeIncome basicSalary = new PayManager().GetEmployeeIncome(empid, basicIncome.IncomeId);
                if (EmployeeManager.HasLevelGradeForStatus(statusId, emp.SkipLevelGradeMatrix))
                {
                    basicSalary.LevelId = new CommonManager().GetDesignationById(positionId).LevelId;
                    basicSalary.Step = gradeStep;
                }
                else
                {
                    basicSalary.LevelId = null;
                    basicSalary.Step = 0;
                }
            }
            emp.DesignationId = positionId;
            PayrollDataContext.DesignationHistories.InsertOnSubmit(desigHistory);

            BranchDepartmentHistory branchHistory = new BranchDepartmentHistory();
            branchHistory.EmployeeId = empid;
            branchHistory.FromBranchId = branchId;
            branchHistory.BranchId = branchId;
            branchHistory.CreatedBy = SessionManager.User.UserID;
            branchHistory.CreatedOn = empStatus.CreatedOn;
            branchHistory.FromDepartmentId = departmentId;
            branchHistory.DepeartmentId = departmentId;
            branchHistory.FromDate = fromDate;
            branchHistory.FromDateEng = empStatus.FromDateEng;
            branchHistory.IsDepartmentTransfer = false;
            branchHistory.IsFirst = false;
            branchHistory.LetterNumber = appointNo;
            branchHistory.Note = "ReHired";
            branchHistory.EventID = (int)ServiceEventType.ReAppointment;
            emp.BranchId = branchId;
            emp.DepartmentId = departmentId;
            PayrollDataContext.BranchDepartmentHistories.InsertOnSubmit(branchHistory);

            //EmployeeServiceHistory retired = new EmployeeServiceHistory();
            //retired.EmployeeId = empid;
            //retired.Date = emp.EHumanResources[0].DateOfRetirement;
            //retired.DateEng = emp.EHumanResources[0].DateOfRetirementEng == null ? DateTime.Now.Date :
            //    emp.EHumanResources[0].DateOfRetirementEng.Value;
            //retired.CreatedBy = SessionManager.User.UserID;
            //retired.CreatedOn = empStatus.CreatedOn;
            //retired.LetterNo = "";
            //retired.StatusId = PayrollDataContext.GetEmployeeStatusForDate(retired.DateEng, empid);
            //retired.BranchId = PayrollDataContext.GetCurrentBranch(empid, retired.DateEng);
            //retired.DepartmentId = PayrollDataContext.GetEmployeeCurrentDepartmentForDate(retired.DateEng, empid);
            //retired.DesignationId = PayrollDataContext.GetEmployeeCurrentPositionForDate(retired.DateEng, empid);
            //if (retired.DesignationId != null)
            //{
            //    retired.LevelId = new CommonManager().GetDesignationById(retired.DesignationId.Value).LevelId;
            //}
            //retired.EventID =  emp.EHumanResources[0].RetirementType != null
            //    ?  (short)emp.EHumanResources[0].RetirementType.Value : (short)ServiceEventType.Retirement;
            //retired.PayrollPeriodId = CommonManager.GetLastPayrollPeriod().PayrollPeriodId;
            //PayrollDataContext.EmployeeServiceHistories.InsertOnSubmit(retired);

            EmployeeServiceHistory rehire = new EmployeeServiceHistory();
            rehire.EmployeeId = empid;
            rehire.Date = fromDate;
            rehire.DateEng = empStatus.FromDateEng;
            rehire.LetterNo = appointNo;

            rehire.StatusId = statusId;
            rehire.BranchId = branchId;
            rehire.DepartmentId = departmentId;
            rehire.LevelId = levelId;
            rehire.StepGrade = gradeStep;
            rehire.DesignationId = positionId;

            rehire.EventID = (int)ServiceEventType.ReAppointment;
            rehire.CreatedBy = SessionManager.User.UserID;
            rehire.CreatedOn = empStatus.CreatedOn;
            rehire.PayrollPeriodId = CommonManager.GetLastPayrollPeriod().PayrollPeriodId;
            PayrollDataContext.EmployeeServiceHistories.InsertOnSubmit(rehire);

            // Do should not be changed after rehire
            //List<CCalculationEmployee> list =
            //    PayrollDataContext.CCalculationEmployees.Where(x => x.EmployeeId == empid && x.IsRetiredOrResigned==true).ToList();
            //foreach (CCalculationEmployee item in list)
            //    item.IsRetiredOrResigned = false;

            emp.EHumanResources[0].IdCardNo = iNo;
            emp.IsRetired = false;
            emp.EHumanResources[0].DateOfRetirement = "";
            emp.EHumanResources[0].DateOfRetirementEng = null;

            emp.IsResigned = false;
            emp.EHumanResources[0].DateOfResignation = "";
            emp.EHumanResources[0].DateOfResignationEng = null;

            // set Salary calculate date also if not retire/rehire case, as for retire/rehire whole month salary need to be calcualted
            if (retirementEvent == null)
            {
                emp.EHumanResources[0].SalaryCalculateFrom = rehire.Date;
                emp.EHumanResources[0].SalaryCalculateFromEng = rehire.DateEng;
            }

            PayrollDataContext.SubmitChanges();

            empStatus.EventSourceID = rehire.EventSourceID;
            branchHistory.EventSourceID = rehire.EventSourceID;
            desigHistory.EventSourceID = rehire.EventSourceID;




            PayrollDataContext.SubmitChanges();

            return status;
        }

        /// <summary>
        /// Save when employee is retiring on 15 of the month and then same employee is being rehire from same date as in 
        /// case of nibl employee being retire/rehire after reaching 20 or 25 years then save all leave and salary details
        /// in other tables
        /// </summary>
        public static void SaveForRetireRehireCast(int employeeId, PayrollPeriod payrollPeriod, EmployeeServiceHistory retirementEvent)
        {
            List<CalculationValue> incomeList = new List<CalculationValue>();
            List<CalculationValue> deductionList = new List<CalculationValue>();
            List<CalculationValue> otherincomeList = new List<CalculationValue>();
            List<CalculationValue> otherdeductionList = new List<CalculationValue>();

            CalculationManager.GetRetirementCalculation(payrollPeriod, employeeId, false, out incomeList, out deductionList,
                out otherincomeList, out otherdeductionList);

            // delete old saved if exists
            RetireReHireEmployee dbEntity = PayrollDataContext.RetireReHireEmployees.FirstOrDefault(x =>
                x.EmployeeId == employeeId && x.PayrollPeriodId == payrollPeriod.PayrollPeriodId);
            if (dbEntity != null)
            {
                PayrollDataContext.RetireReHireEmployees.DeleteOnSubmit(dbEntity);
                PayrollDataContext.SubmitChanges();
            }

            EEmployee emp = GetEmployeeById(employeeId);

            RetireReHireEmployee retirement = new RetireReHireEmployee();
            retirement.EmployeeId = employeeId;
            retirement.PayrollPeriodId = payrollPeriod.PayrollPeriodId;
            retirement.Notes = emp.EHumanResources[0].RetirementNotes == null ? "" : emp.EHumanResources[0].RetirementNotes;
            retirement.RetirementDate = retirementEvent.Date;
            retirement.RetirementType = emp.EHumanResources[0].RetirementType == null ? 0 : emp.EHumanResources[0].RetirementType.Value;

            // gratuity information
            GetGratuityServiceYearResult gratuity = PayrollDataContext.GetGratuityServiceYear(employeeId, payrollPeriod.PayrollPeriodId).FirstOrDefault();
            if (gratuity != null)
            {
                retirement.GratuityServiceYear = gratuity.ServiceYear == null ? 0 : (decimal)gratuity.ServiceYear.Value;
                retirement.GratuityAmount = gratuity.Amount == null ? 0 : gratuity.Amount.Value;
            }

            // Income portions
            foreach (var item in incomeList)
            {
                RetireReHireDetail detail = new RetireReHireDetail();
                retirement.RetireReHireDetails.Add(detail);

                detail.EmployeeId = employeeId;
                detail.PayrollPeriodId = payrollPeriod.PayrollPeriodId;
                detail.RetirementPortionType = (int)RetirementPortionTypeEnum.Income;
                detail.Type = item.Type;
                detail.SourceId = item.SourceId;
                detail.HeaderName = item.HeaderName;
                detail.FullAmount = item.FullAmount;
                detail.AmountWithOutAdjustment = item.AmountWithOutAdjustment;
                detail.Amount = item.Amount;
            }

            // Deductions portions
            foreach (var item in deductionList)
            {
                RetireReHireDetail detail = new RetireReHireDetail();
                retirement.RetireReHireDetails.Add(detail);

                detail.EmployeeId = employeeId;
                detail.PayrollPeriodId = payrollPeriod.PayrollPeriodId;
                detail.RetirementPortionType = (int)RetirementPortionTypeEnum.Deduction;
                detail.Type = item.Type;
                detail.SourceId = item.SourceId;
                detail.HeaderName = item.HeaderName;

                detail.FullAmount = item.AmountWithOutAdjustment;
                detail.AmountWithOutAdjustment = item.AmountWithOutAdjustment;
                detail.Amount = item.Amount;
            }

            // Other Income portions
            foreach (var item in otherincomeList)
            {
                RetireReHireDetail detail = new RetireReHireDetail();
                retirement.RetireReHireDetails.Add(detail);

                detail.EmployeeId = employeeId;
                detail.PayrollPeriodId = payrollPeriod.PayrollPeriodId;
                detail.RetirementPortionType = (int)RetirementPortionTypeEnum.OtherIncome;
                detail.Type = item.Type;
                detail.SourceId = item.SourceId;
                detail.HeaderName = item.HeaderName;

                detail.FullAmount = item.FullAmount;
                detail.AmountWithOutAdjustment = item.AdjustedAmount;
                detail.TDS = item.TDS;
                detail.TDSAdjustment = item.TDSAdjustment;
                detail.Amount = item.NetPayable;
            }

            // Other Deductions portions
            foreach (var item in otherdeductionList)
            {
                RetireReHireDetail detail = new RetireReHireDetail();
                retirement.RetireReHireDetails.Add(detail);

                detail.EmployeeId = employeeId;
                detail.PayrollPeriodId = payrollPeriod.PayrollPeriodId;
                detail.RetirementPortionType = (int)RetirementPortionTypeEnum.OtherDeduction;
                detail.Type = item.Type;
                detail.SourceId = item.SourceId;
                detail.HeaderName = item.HeaderName;

                detail.FullAmount = item.AmountWithOutAdjustment;
                detail.AmountWithOutAdjustment = item.AmountWithOutAdjustment;
                detail.Amount = item.Amount;
                detail.Excluded = item.ExcludeFromRetirement;
            }

            // leave details

            List<Report_HR_GetRetirementLeaveDetailsResult> leaveList = LeaveAttendanceManager.GetRetirementLeaveDetails(payrollPeriod, employeeId);
            foreach (var item in leaveList)
            {
                RetireRehireLeaveDetail detail = new RetireRehireLeaveDetail();
                retirement.RetireRehireLeaveDetails.Add(detail);

                detail.EmployeeId = employeeId;
                detail.PayrollPeriodId = payrollPeriod.PayrollPeriodId;
                detail.LeaveTypeId = item.LeaveTypeId;
                detail.LastYearOpening = item.LastYearOpening == null ? 0 : item.LastYearOpening.Value;
                detail.CurrentYearAccured = item.CurrentYearAccured == null ? 0 : (double)item.CurrentYearAccured.Value;

                detail.CurrentYearEligible = item.CurrentYearEligible == null ? 0 : item.CurrentYearEligible.Value;
                detail.Taken = item.Taken == null ? 0 : item.Taken.Value; ;
                detail.Adjusted = item.Adjusted == null ? 0 : (double)item.Adjusted.Value;
                detail.ReminingLeave = item.ReminingLeave == null ? 0 : item.ReminingLeave.Value;

                detail.EncashTotal = item.EncashTotal == null ? 0 : item.EncashTotal.Value;
                detail.Encased = item.Encased;
                detail.RetirementEncash = item.RetirementEncash;
            }

            PayrollDataContext.RetireReHireEmployees.InsertOnSubmit(retirement);
        }

        public static HireMethod GetHireMethod(int hireMethodId)
        {
            return PayrollDataContext.HireMethods.FirstOrDefault(x => x.HireMethodId == hireMethodId);
        }

        public static PayrollPeriod GetPastRetiredPayrollPeriod(int employeeId)
        {
            CCalculationEmployee emp
                 = PayrollDataContext.CCalculationEmployees
                 .Where(x => x.EmployeeId == employeeId)
                 .OrderByDescending(x => x.CalculationId).FirstOrDefault();
            if (emp != null)
            {
                return emp.CCalculation.PayrollPeriod;
            }
            return null;
        }

        public static void SetPastRetirement(int employeeId, string date, DateTime dateEng, int retType)
        {
            PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(employeeId, (byte)MonetoryChangeLogTypeEnum.Information
                   , "Backdated Retirement", "-", date, LogActionEnum.Add));



            PayrollPeriod period = PayrollDataContext.PayrollPeriods.FirstOrDefault(x => dateEng >= x.StartDateEng && dateEng <= x.EndDateEng);
            if (period != null)
            {
                CCalculationEmployee calcEmployee
                    =
                    (
                        from ce in PayrollDataContext.CCalculationEmployees
                        join c in PayrollDataContext.CCalculations on ce.CalculationId equals c.CalculationId
                        where c.PayrollPeriodId == period.PayrollPeriodId
                        && ce.EmployeeId == employeeId
                        select ce
                    ).FirstOrDefault();

                if (calcEmployee != null)
                    calcEmployee.IsRetiredOrResigned = true;

            }

            EEmployee dbEntity = GetEmployeeById(employeeId);

            dbEntity.IsRetired = true;
            dbEntity.IsResigned = false;

            PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(employeeId, (byte)MonetoryChangeLogTypeEnum.Information
                 , "Backdated Retirement", dbEntity.EHumanResources[0].DateOfRetirement, date, LogActionEnum.Add));

            dbEntity.EHumanResources[0].DateOfRetirementEng = dateEng;
            dbEntity.EHumanResources[0].DateOfRetirement = date;
            dbEntity.EHumanResources[0].RetirementType = retType;


            //save in service event also
            EmployeeServiceHistory history = new EmployeeServiceHistory();
            history.CreatedBy = SessionManager.CurrentLoggedInUserID;
            history.CreatedOn = GetCurrentDateAndTime();
            history.Date = date;
            history.DateEng = dateEng;
            history.EmployeeId = employeeId;
            history.EventID = (short)retType;

            GetEmployeeCurrentBranchDepartmentResult branchDep =
               PayrollDataContext.GetEmployeeCurrentBranchDepartment(history.DateEng, history.EmployeeId)
               .FirstOrDefault();

            history.BranchId = branchDep.BranchId;
            history.DepartmentId = branchDep.DepartmentId;
            history.StatusId = PayrollDataContext.GetEmployeeStatusForDate(history.DateEng, history.EmployeeId).Value;
            history.DesignationId = PayrollDataContext.GetEmployeeCurrentPositionForDate(history.DateEng, history.EmployeeId).Value;
            history.LevelId = CommonManager.GetLevelIDForDesignation(history.DesignationId.Value);

            //history.LetterNo =  dbEmployee.EHumanResources[0].AppointmentLetterNo;
            //history.LetterDate = dbEmployee.EHumanResources[0].AppointmentLetterDate;
            //history.LetterDateEng = dbEmployee.EHumanResources[0].AppointmentLetterDateEng;
            //history.LevelId = CommonManager.GetLevelIDForDesignation(history.DesignationId.Value);
            history.ModifiedBy = SessionManager.CurrentLoggedInUserID;
            history.ModifiedOn = history.CreatedOn;
            history.Notes = "";
            history.PayrollPeriodId = CommonManager.GetLastPayrollPeriod().PayrollPeriodId;
            PayrollDataContext.EmployeeServiceHistories.InsertOnSubmit(history);


            PayrollDataContext.SubmitChanges();

        }
        public static List<TextValue> GetApplyToListForEmployee(int employeeId)
        {

            //GetLeaveApprovalApplyToCCForEmployeeResult
            //    entity = LeaveAttendanceManager.GetLeaveApprovalApplyToAndCCForEmployee(employeeId);

            //List<TextValue> list = new List<TextValue>();


            // return LeaveAttendanceManager.GetPreDefinedApprovalList(!isApproval, employeeId, type);

            return
                (
                from l in PayrollDataContext.GetMyLeaveTimesheetApprovalList(employeeId)
                where l.EmployeeId != employeeId
                select new TextValue
                {
                    Text = l.NAME,
                    Value = l.EmployeeId.ToString()
                }
                ).ToList();


            //else
            //{
            //    if (entity != null)
            //    {

            //        EmployeeManager empMgr = new EmployeeManager();
            //        if (isApproval)
            //        {
            //            if (entity.ApplyTo != null)
            //            {
            //                list.Add(new TextValue { Text = entity.ApplyToName, Value = entity.ApplyTo.ToString() });
            //            }

            //            if (entity.CC1 != null)
            //            {
            //                list.Add(new TextValue { Text = entity.CC1Name, Value = entity.CC1.ToString() });

            //            }

            //            if (entity.CC2 != null)
            //            {
            //                list.Add(new TextValue { Text = entity.CC2Name, Value = entity.CC2.ToString() });
            //            }

            //            if (entity.CC3 != null)
            //            {
            //                list.Add(new TextValue { Text = entity.CC3Name, Value = entity.CC3.ToString() });
            //            }
            //        }
            //        else
            //        {
            //            if (entity.CC4 != null)
            //            {
            //                list.Add(new TextValue { Text = entity.CC4Name, Value = entity.CC4.ToString() });
            //            }

            //            if (entity.CC5 != null)
            //            {
            //                list.Add(new TextValue { Text = entity.CC5Name, Value = entity.CC5.ToString() });
            //            }
            //        }

            //    }
            //}


            // return list.OrderBy(x => x.Text).ToList();

        }

        public static Status UpdateINo(int employeeId, string code, string contractINo, string CardSerialNo)
        {
            Status status = new Status();

            EHumanResource empExists = PayrollDataContext.EHumanResources
                .FirstOrDefault(x => x.IdCardNo != "" && x.IdCardNo != null && x.IdCardNo.ToLower() == code.ToLower() && x.EmployeeId != employeeId);
            if (empExists != null)
            {
                status.ErrorMessage = "I No already exists for the employee \"" + empExists.EEmployee.Name + "\".";
                return status;
            }



            EHumanResource empCardSerialNoExits = PayrollDataContext.EHumanResources
             .FirstOrDefault(x => x.CardSerialNo != "" && x.CardSerialNo != null &&
                 x.CardSerialNo.ToLower() == CardSerialNo.ToLower() && x.EmployeeId != employeeId);
            if (empCardSerialNoExits != null)
            {
                status.ErrorMessage = "Card Serial No already exists for the employee \"" + empCardSerialNoExits.EEmployee.Name + "\".";
                return status;
            }


            EEmployee emp = new EmployeeManager().GetById(employeeId);


            emp.EHumanResources[0].IdCardNo = code;
            emp.EHumanResources[0].ContractIdCardNo = contractINo;
            emp.EHumanResources[0].CardSerialNo = CardSerialNo;

            PayrollDataContext.SubmitChanges();

            return status;
        }

        public static Status UpdateReportsTo(int employeeId, int? ReportToEmployeeId)
        {
            Status status = new Status();

            EEmployee emp = new EmployeeManager().GetById(employeeId);
            emp.EHumanResources[0].ReportToEmployeeId = ReportToEmployeeId;



            PayrollDataContext.SubmitChanges();

            return status;
        }

        public static string GetWarningForPSIVoucher()
        {
            var list =
                (
                from e in PayrollDataContext.EEmployees
                join h in PayrollDataContext.EHumanResources on e.EmployeeId equals h.EmployeeId
                where e.CostCodeId == null || h.ProgrammeId == null
                select new
                {
                    EIN = e.EmployeeId,
                    Name = e.Name,
                    CostCodeId = e.CostCodeId,
                    ProgramId = h.ProgrammeId
                }
            ).ToList();

            string html = "";

            foreach (var item in list)
            {
                html += "<br>Define cost code and program " + item.Name + " for " +
                    string.Format("<a href='../../newhr/OtherHRDetails.aspx?ID={0}'> from here.</a>", item.EIN);
            }

            return html;
        }

        public static Status UpdateOtherReportsTo(int employeeId, int? ReportToEmployeeId, int? costCodeId, int? programId, int? voucherGroupID, string AccountCode)
        {
            Status status = new Status();

            EEmployee emp = new EmployeeManager().GetById(employeeId);
            emp.EHumanResources[0].ReportToEmployeeId = ReportToEmployeeId;

            emp.CostCodeId = costCodeId;
            emp.EHumanResources[0].ProgrammeId = programId;
            emp.EHumanResources[0].VoucherDepartmentID = voucherGroupID;
            emp.EHumanResources[0].AccountCode = AccountCode;

            PayrollDataContext.SubmitChanges();

            return status;
        }
        public static Status UpdateProjectCat(int employeeId, string projectCat)
        {
            Status status = new Status();

            EEmployee emp = new EmployeeManager().GetById(employeeId);
            emp.EHumanResources[0].ProjectCategory = projectCat;


            PayrollDataContext.SubmitChanges();

            return status;
        }
        public static Status UpdateReportsInBulk(List<EHumanResource> list)
        {
            Status status = new Status();

            foreach (var val1 in list)
            {
                UpdateReportsTo(val1.EmployeeId, val1.ReportToEmployeeId.Value);
            }

            return status;
        }

        //public static bool HasLevelGrade(JobStatusEnum status)
        //{
        //    if (CommonManager.CompanySetting.HasLevelGradeSalary == false)
        //        return false;
        //    //first check in the company if it has Level/Grade system from setting or not
        //    if (status == (JobStatusEnum)EmployeeManager.GetContractStatus())
        //    {
        //        return false;
        //    }
        //    return true;
        //}

        public static bool IsBranchOrDepartmentOrRegionalHead(int employeeId)
        {
            if (PayrollDataContext.Branches.Any(x => x.BranchHeadId == employeeId))
                return true;
            if (PayrollDataContext.Branches.Any(x => x.RegionalHeadId == employeeId))
                return true;
            if (PayrollDataContext.BranchDepartments.Any(x => x.HeadEmployeeId == employeeId))
                return true;

            return false;
        }

        /// <summary>
        /// Checks if passed employee is of Level Grade type or not
        /// </summary>
        /// <param name="employeeId"></param>
        /// <returns></returns>
        public static bool HasLevelGrade(int employeeId)
        {
            if (CommonManager.CompanySetting.HasLevelGradeSalary == false)
                return false;
            EEmployee emp = new EmployeeManager().GetById(employeeId);
            ECurrentStatus status = emp.ECurrentStatus.OrderByDescending(x => x.FromDateEng).FirstOrDefault();

            if (EmployeeManager.IsContractStatus(status.CurrentStatus))
            {
                return false;
            }
            return true;
        }

        public static List<CalcGetHeaderListResult> GetTaxDetailsSummaryHeader(int payrollPeriodId)
        {

            //PayrollPeriod payroll = CommonManager.GetLastPayrollPeriod();
            if (payrollPeriodId == 0)
            {
                return new List<CalcGetHeaderListResult>();
            }

            List<CalcGetHeaderListResult> list = new List<CalcGetHeaderListResult>();

            list =
                (
                    from i in PayrollDataContext.PIncomes
                    orderby i.Order
                    select new CalcGetHeaderListResult
                    {
                        Type = (i.Calculation == IncomeCalculation.DEEMED_INCOME ? 25 : 1),
                        SourceId = i.IncomeId,
                        HeaderName = (i.Calculation == IncomeCalculation.DEEMED_INCOME ? "Deemed - " + i.Title : i.Title)
                    }
                ).ToList();

            list.Add(new CalcGetHeaderListResult { Type = (int)CalculationColumnType.IncomePF, SourceId = (int)CalculationColumnType.IncomePF, HeaderName = "Income PF" });
            list.Add(new CalcGetHeaderListResult { Type = (int)CalculationColumnType.IncomeLeaveEncasement, SourceId = (int)CalculationColumnType.IncomeLeaveEncasement, HeaderName = "Leave Encashment" });


            return list;
        }

        public static List<Report_GetTaxCalculationSummaryResult> GetTaxDetailsSummary(int payrollPeriodId, int? empId,
            bool skipPastRetired, int branchId)
        {

            if (payrollPeriodId == null)
            {
                return new List<Report_GetTaxCalculationSummaryResult>();
            }

            bool readingSumForMiddleFiscalYearStartedReqd = false;
            //contains the id of starting-ending payroll period to be read for history of regular income
            int startingPayrollPeriodId = 0, endingPayrollPeriodId = 0;
            // int notWorkedMonthInFiscalYear = 0, passedMonthWorked = 0;


            CalculationManager.GenerateForPastIncome(
                     payrollPeriodId, ref readingSumForMiddleFiscalYearStartedReqd,
                     ref startingPayrollPeriodId, ref endingPayrollPeriodId);

            int empStartingPayrollId = startingPayrollPeriodId;
            int empEndingPayrollId = endingPayrollPeriodId;

            List<Report_GetTaxCalculationSummaryResult> list = PayrollDataContext.Report_GetTaxCalculationSummary(payrollPeriodId, SessionManager.CurrentCompanyId,
                 readingSumForMiddleFiscalYearStartedReqd, empStartingPayrollId, empEndingPayrollId,
                  LeaveAttendanceManager.GetTotalDaysInFiscalYearForPeriod(payrollPeriodId)
                                                          , LeaveAttendanceManager.GetLeaveYearStartDate(payrollPeriodId), empId, skipPastRetired, branchId).ToList();



            return list;
        }

        public static List<Report_GetTaxableIncomeIRDDepositedComparisionResult> GetTaxableAndIRDDepositedComparisionReport(int payrollPeriodId, int? empId,
            bool skipPastRetired, int branchId)
        {

            if (payrollPeriodId == 0)
            {
                return new List<Report_GetTaxableIncomeIRDDepositedComparisionResult>();
            }

            bool readingSumForMiddleFiscalYearStartedReqd = false;
            //contains the id of starting-ending payroll period to be read for history of regular income
            int startingPayrollPeriodId = 0, endingPayrollPeriodId = 0;
            // int notWorkedMonthInFiscalYear = 0, passedMonthWorked = 0;


            CalculationManager.GenerateForPastIncome(
                     payrollPeriodId, ref readingSumForMiddleFiscalYearStartedReqd,
                     ref startingPayrollPeriodId, ref endingPayrollPeriodId);

            int empStartingPayrollId = startingPayrollPeriodId;
            int empEndingPayrollId = endingPayrollPeriodId;

            List<Report_GetTaxableIncomeIRDDepositedComparisionResult> list = PayrollDataContext.Report_GetTaxableIncomeIRDDepositedComparision
                (payrollPeriodId, SessionManager.CurrentCompanyId,
                 readingSumForMiddleFiscalYearStartedReqd, empStartingPayrollId, empEndingPayrollId,
                  LeaveAttendanceManager.GetTotalDaysInFiscalYearForPeriod(payrollPeriodId)
                                                          , LeaveAttendanceManager.GetLeaveYearStartDate(payrollPeriodId), empId,skipPastRetired).ToList();



            return list;
        }

        public static List<int> GetAllEmployeeID()
        {
            return PayrollDataContext.EEmployees.Select(x => x.EmployeeId).ToList();
        }

        public static int GetRolloutGroupIdByName(string GroupName)
        {
            return PayrollDataContext.AppraisalRolloutGroups.Where(x => x.Name.ToLower() == GroupName.ToLower()).FirstOrDefault().RolloutGroupID;
        }

        #region Employee Master

        public static List<GetEmployeeMasterOtherHeadersResult> GetEmployeeMasterHeader()
        {
            return PayrollDataContext.GetEmployeeMasterOtherHeaders(SessionManager.CurrentCompanyId).ToList();
        }
        public static List<CalcGetTaxDetailsHeaderListResult> GetTaxDetailsListHeader(int payrollPeriodId)
        {

            //PayrollPeriod payroll = CommonManager.GetLastPayrollPeriod();
            if (payrollPeriodId == 0)
            {
                return new List<CalcGetTaxDetailsHeaderListResult>();
            }

            bool readingSumForMiddleFiscalYearStartedReqd = false;
            //contains the id of starting-ending payroll period to be read for history of regular income
            int startingPayrollPeriodId = 0, endingPayrollPeriodId = 0;
            // int notWorkedMonthInFiscalYear = 0, passedMonthWorked = 0;


            CalculationManager.GenerateForPastIncome(
                     payrollPeriodId, ref readingSumForMiddleFiscalYearStartedReqd,
                     ref startingPayrollPeriodId, ref endingPayrollPeriodId);

            int empStartingPayrollId = startingPayrollPeriodId;
            int empEndingPayrollId = endingPayrollPeriodId;

            return PayrollDataContext.CalcGetTaxDetailsHeaderList(payrollPeriodId, empStartingPayrollId, empEndingPayrollId, SessionManager.CurrentCompanyId).ToList();
        }
        public static List<GetEmployeeMasterD2Result> GetEmployeeMasterD2(int departmentId, string jobCode, int designationId, int gender, string empSearchName)
        {
            return PayrollDataContext.GetEmployeeMasterD2(SessionManager.CurrentCompanyId, departmentId, jobCode, designationId, gender, empSearchName).ToList();
        }



        public static List<GetEmployeeMasterResult> GetEmployeeMaster(int StartRow, int PageSize, string Sort, int branchId, int departmentId,
            int statusId, string name)
        {

            List<GetEmployeeMasterResult> list = PayrollDataContext.GetEmployeeMaster(StartRow, PageSize, Sort, SessionManager.CurrentCompanyId, branchId, departmentId, statusId, name).ToList();
            int count = 1;
            foreach (GetEmployeeMasterResult result in list)
            {
                result.SN = (count++).ToString();
                result.Status = JobStatus.GetValueForDisplay(int.Parse(result.Status)).ToString();
                if (!string.IsNullOrEmpty(result.Insurance))
                {
                    result.Insurance = GetCurrency(result.Insurance);
                }
                if (result.CITIsRate != null)
                {
                    if (result.CITIsRate.Value)
                        result.CIT += "%";
                    else
                        result.CIT = GetCurrency(result.CIT);
                }
            }

            return list;
        }
        public static List<GetEmployeeLatestEventDetailsResult> GetEmployeeLatestEvent
            (int StartRow, int PageSize, string Sort, int branchId, int departmentId, int statusId)
        {

            List<GetEmployeeLatestEventDetailsResult> list = PayrollDataContext.GetEmployeeLatestEventDetails
                (StartRow, PageSize, Sort, SessionManager.CurrentCompanyId, branchId, departmentId, statusId).ToList();


            return list;
        }

        public static List<GetFamilyListReportResult> GetFamilyListReport(int empId, ref List<FamilyHeader> headerList)
        {
            List<GetFamilyListReportResult> list = PayrollDataContext.GetFamilyListReport(empId).ToList();
            int sn = 0;
            foreach (GetFamilyListReportResult item in list)
            {
                item.SN = (++sn);

                if (!string.IsNullOrEmpty(item.FamilyList))
                {
                    string[] values = item.FamilyList.Split(new char[] { ';' });
                    foreach (string value in values)
                    {
                        string[] family = value.Split(new char[] { ':' });
                        if (family.Length >= 3)
                        {
                            int index = int.Parse(family[0]);
                            string relation = family[1].Trim();
                            string familyName = family[2].Trim();
                            string relationWithIndex = relation + " " + index;

                            if (headerList.Any(x => x.RelationNameWithIndex.Equals(relationWithIndex)) == false)
                            {
                                headerList.Add(new FamilyHeader { RelationName = relation, RelationNameWithIndex = relationWithIndex });
                            }

                            int headerIndex = 1;
                            foreach (var header in headerList)
                            {
                                if (header.RelationNameWithIndex.Equals(relationWithIndex))
                                {
                                    switch (headerIndex)
                                    {
                                        case 1: item.Family1 = familyName; break;
                                        case 2: item.Family2 = familyName; break;
                                        case 3: item.Family3 = familyName; break;
                                        case 4: item.Family4 = familyName; break;
                                        case 5: item.Family5 = familyName; break;
                                        case 6: item.Family6 = familyName; break;
                                        case 7: item.Family7 = familyName; break;
                                        case 8: item.Family8 = familyName; break;
                                        case 9: item.Family9 = familyName; break;
                                        case 10: item.Family10 = familyName; break;
                                        case 11: item.Family11 = familyName; break;
                                        case 12: item.Family12 = familyName; break;
                                        case 13: item.Family13 = familyName; break;
                                        case 14: item.Family14 = familyName; break;
                                        case 15: item.Family15 = familyName; break;
                                        case 16: item.Family16 = familyName; break;
                                        case 17: item.Family17 = familyName; break;
                                        case 18: item.Family18 = familyName; break;
                                        case 19: item.Family19 = familyName; break;
                                        case 20: item.Family20 = familyName; break;
                                        case 21: item.Family21 = familyName; break;
                                        case 22: item.Family22 = familyName; break;
                                        case 23: item.Family23 = familyName; break;
                                        case 24: item.Family24 = familyName; break;
                                        case 25: item.Family25 = familyName; break;

                                    }
                                }
                                headerIndex += 1;
                            }





                        }
                    }
                }
            }

            return list;
        }

        public static List<GetFamilyListForInsurenceReportResult> GetFamilyListForInsurenceReport(int empId, ref List<FamilyHeader> headerList)
        {
            List<GetFamilyListForInsurenceReportResult> list = PayrollDataContext.GetFamilyListForInsurenceReport(empId).ToList();
            int sn = 0;
            foreach (GetFamilyListForInsurenceReportResult item in list)
            {
                item.SN = (++sn);

                if (!string.IsNullOrEmpty(item.FamilyList))
                {
                    string[] values = item.FamilyList.Split(new char[] { ';' });
                    foreach (string value in values)
                    {
                        string[] family = value.Split(new char[] { ':' });
                        if (family.Length >= 3)
                        {
                            int index = int.Parse(family[0]);
                            string relation = family[1].Trim();
                            string familyName = family[2].Trim();
                            string relationWithIndex = relation + " " + index;

                            if (headerList.Any(x => x.RelationNameWithIndex.Equals(relationWithIndex)) == false)
                            {
                                headerList.Add(new FamilyHeader { RelationName = relation, RelationNameWithIndex = relationWithIndex });
                            }

                            int headerIndex = 1;
                            foreach (var header in headerList)
                            {
                                if (header.RelationNameWithIndex.Equals(relationWithIndex))
                                {
                                    switch (headerIndex)
                                    {
                                        case 1: item.Family1 = familyName; break;
                                        case 2: item.Family2 = familyName; break;
                                        case 3: item.Family3 = familyName; break;
                                        case 4: item.Family4 = familyName; break;
                                        case 5: item.Family5 = familyName; break;
                                        case 6: item.Family6 = familyName; break;
                                        case 7: item.Family7 = familyName; break;
                                        case 8: item.Family8 = familyName; break;
                                        case 9: item.Family9 = familyName; break;
                                        case 10: item.Family10 = familyName; break;
                                        case 11: item.Family11 = familyName; break;
                                        case 12: item.Family12 = familyName; break;
                                        case 13: item.Family13 = familyName; break;
                                        case 14: item.Family14 = familyName; break;
                                        case 15: item.Family15 = familyName; break;
                                        case 16: item.Family16 = familyName; break;
                                        case 17: item.Family17 = familyName; break;
                                        case 18: item.Family18 = familyName; break;
                                        case 19: item.Family19 = familyName; break;
                                        case 20: item.Family20 = familyName; break;
                                        case 21: item.Family21 = familyName; break;
                                        case 22: item.Family22 = familyName; break;
                                        case 23: item.Family23 = familyName; break;
                                        case 24: item.Family24 = familyName; break;
                                        case 25: item.Family25 = familyName; break;

                                    }
                                }
                                headerIndex += 1;
                            }





                        }
                    }
                }
            }

            return list;
        }

        public static List<ReportNewHR_GetHeadCountResult> GetHeadCount(int StartRow, int PageSize, string Sort, int BranchID, int DepartmentID, DateTime Date, DateTime toDate, int ein)
        {
            List<ReportNewHR_GetHeadCountResult> list = PayrollDataContext.ReportNewHR_GetHeadCount(StartRow, PageSize, Sort, SessionManager.CurrentCompanyId, BranchID, DepartmentID, Date, toDate, ein).ToList();
            return list;
        }

        public static List<ReportNewHR_GetAdditionsListResult> GetEmployeeAdditions(int StartRow, int PageSize, string Sort, int BranchID, int DepartmentID, DateTime Date, DateTime Date1)
        {
            List<ReportNewHR_GetAdditionsListResult> list = PayrollDataContext.ReportNewHR_GetAdditionsList(StartRow, PageSize, Sort, SessionManager.CurrentCompanyId, BranchID, DepartmentID, Date, Date1).ToList();
            return list;
        }

        public static List<ReportNewHR_GetTerminationListResult> GetEmployeeTerminations(int StartRow, int PageSize, string Sort, int BranchID, int DepartmentID, DateTime Date, DateTime Date1)
        {
            List<ReportNewHR_GetTerminationListResult> list = PayrollDataContext.ReportNewHR_GetTerminationList(StartRow, PageSize, Sort, SessionManager.CurrentCompanyId, BranchID, DepartmentID, Date, Date1).ToList();
            return list;
        }


        public static List<ReportNewHR_GetGenderProfileResult> GetGenderProfile(int BranchID, DateTime fromDate, DateTime toDate, int departmentId, int employeeId)
        {
            List<ReportNewHR_GetGenderProfileResult> list = PayrollDataContext.ReportNewHR_GetGenderProfile(SessionManager.CurrentCompanyId, BranchID, departmentId, fromDate, toDate, employeeId).ToList();
            return list;
        }

        public static int GetHeadCountByMonth(int BranchID, int DepartmentID, DateTime startDate)
        {
            int? result = PayrollDataContext.GetMonthWiseHeadCount(SessionManager.CurrentCompanyId, BranchID, DepartmentID, startDate, null, -1);
            return result.Value;
        }


        public static int GetGenderHeadByDepartment(int BranchID, int DepartmentID, DateTime startDate, int Gender, DateTime endDate, int employeeId)
        {
            int? result = PayrollDataContext.GetGenderHeadByDepartment(SessionManager.CurrentCompanyId, BranchID, DepartmentID, endDate, Gender, startDate, employeeId);
            return result.Value;
        }

        public static int GetAgeGroupHeadByDepartment(int BranchID, int DepartmentID, DateTime EndDate, int AgeLimit1, int AgeLimit2, DateTime fromDate, int employeeId)
        {
            int? result = PayrollDataContext.GetAgeGroupHeadByDepartment(SessionManager.CurrentCompanyId, BranchID, DepartmentID, EndDate, AgeLimit1, AgeLimit2, fromDate, employeeId);
            return result.Value;
        }


        public static List<GetHPLVoucherReportResult> GetHPLVoucherReport(int payrollPeriodId, bool? showRetiredOnly)
        {

            return PayrollDataContext.GetHPLVoucherReport(payrollPeriodId, showRetiredOnly).ToList();

        }
        public static List<GetGlobusSalaryVoucherReportResult> GetGlobusVoucherReport(int payrollPeriodId, bool? showRetiredOnly, int categoryId)
        {

            List<GetGlobusSalaryVoucherReportResult> list = PayrollDataContext.GetGlobusSalaryVoucherReport(payrollPeriodId, showRetiredOnly, categoryId).ToList();

            Branch headOffice = BranchManager.GetHeadOffice();

            foreach (var item in list)
            {

                // for income and not head office then branch code will be "TransCode\Branch Code"
                if (item.Credit >= 0 && headOffice.Code.Equals(item.Department) == false)
                    item.TranCode = item.TranCode + "\\" + item.Department;

            }

            return list;
        }

        public static List<GetGlobusReportForMulitpleAddOnResult> GetGlobusAddOnMainVoucher(
            int payrollPeriodId, bool? showRetiredOnly, int categoryId, int addOnId, string selectedEMPID, bool applyAddOndateFilter,
            DateTime? addOnDate)
        {

            List<GetGlobusReportForMulitpleAddOnResult> list1 = BLL.BaseBiz.PayrollDataContext.GetGlobusReportForMulitpleAddOn(payrollPeriodId, addOnId, selectedEMPID, applyAddOndateFilter, addOnDate, categoryId)
                          .ToList();

            Branch headOffice = BranchManager.GetHeadOffice();

            foreach (var item in list1)
            {

                // for income and not head office then branch code will be "TransCode\Branch Code"
                if (item.Credit >= 0 && headOffice.Code.Equals(item.Department) == false)
                    item.TranCode = item.TranCode + "\\" + item.Department;

            }

            return list1;
        }



        public static List<GetGlobusVoucherOtherThanMainVoucherResult> GetGlobusOtherVoucher(
           int payrollPeriodId, bool? showRetiredOnly, int categoryId, int addOnId, string selectedEMPID, bool applyAddOndateFilter,
           DateTime? addOnDate)
        {

            List<GetGlobusVoucherOtherThanMainVoucherResult> list1 = BLL.BaseBiz.PayrollDataContext
                .GetGlobusVoucherOtherThanMainVoucher(payrollPeriodId, showRetiredOnly, categoryId, addOnId, selectedEMPID, applyAddOndateFilter, addOnDate).ToList();


            if (CommonManager.CompanySetting.WhichCompany == WhichCompany.MBL)
            {
                foreach (var item in list1)
                {
                    if (item.Sign.Equals("C"))
                        item.Sign = "|";

                    item.Amount = item.Amount * -1;
                }
            }


            return list1;
        }

        public static List<GlobusCivilMainVoucher> GetGlobusCIVILMainVoucherReport(
            int payrollPeriodId, bool? showRetiredOnly, int categoryId, int addOnId, string selectedEMPID, bool applyAddOndateFilter,
            DateTime? addOnDate)
        {

            List<GlobusCivilMainVoucher> mainList = new List<GlobusCivilMainVoucher>();


            if (addOnId == -1)
            {
                List<GetGlobusSalaryVoucherReportResult> list = PayrollDataContext.GetGlobusSalaryVoucherReport(payrollPeriodId, showRetiredOnly, categoryId).ToList();


                Branch headOffice = BranchManager.GetHeadOffice();

                foreach (var item in list)
                {

                    // for income and not head office then branch code will be "TransCode\Branch Code"
                    //if (item.Credit >= 0 && headOffice.Code.Equals(item.Department) == false)
                    //    item.TranCode = item.TranCode + "\\" + item.Department;

                    mainList.Add(new GlobusCivilMainVoucher
                    {
                        COMPANY = item.Department,
                        DEBITACCTNO = item.TranCode,
                        DEBITCURRENCY = "NPR",
                        DEBITAMOUNT = item.Debit == null ? "0" : item.Debit.ToString(),
                        DEBITTHEIRREF = item.Desc1,
                        ORDERINGCUST = "CIBL",
                        PAYMENTDETAILS = item.Desc1,
                        CREDITACCTNO = item.MainCode,
                        CREDITCURRENCY = "NPR",
                        CREDITTHEIRREF = item.Desc1,
                        PROFITCENTREDEPT = "3"
                    });
                }

            }
            else
            {
                List<GetGlobusReportForMulitpleAddOnResult> list =
                      BLL.BaseBiz.PayrollDataContext.GetGlobusReportForMulitpleAddOn(payrollPeriodId, addOnId, selectedEMPID, applyAddOndateFilter, addOnDate, categoryId).ToList();

                Branch headOffice = BranchManager.GetHeadOffice();

                foreach (var item in list)
                {

                    // for income and not head office then branch code will be "TransCode\Branch Code"
                    //if (item.Credit >= 0 && headOffice.Code.Equals(item.Department) == false)
                    //    item.TranCode = item.TranCode + "\\" + item.Department;

                    mainList.Add(new GlobusCivilMainVoucher
                    {
                        COMPANY = item.Department,
                        DEBITACCTNO = item.TranCode,
                        DEBITCURRENCY = "NPR",
                        DEBITAMOUNT = item.Debit == null ? "0" : item.Debit.ToString(),
                        DEBITTHEIRREF = item.Desc1,
                        ORDERINGCUST = "CIBL",
                        PAYMENTDETAILS = item.Desc1,
                        CREDITACCTNO = item.MainCode,
                        CREDITCURRENCY = "NPR",
                        CREDITTHEIRREF = item.Desc1,
                        PROFITCENTREDEPT = "3"
                    });

                }
            }



            return mainList;
        }

        public static List<GetOriginalHPLVoucherReportResult> GetOriginalHPLVoucherReport(int payrollPeriodId)
        {

            return PayrollDataContext.GetOriginalHPLVoucherReport(payrollPeriodId).ToList();

        }
        public static List<GetNIBLVoucherReportResult> GetNIBLVoucherReport(int payrollPeriodId, bool? showRetiredOnly,
            int? voucherType, int branchId, bool? NIBLCardDepartmentOnly, bool? includeExternalCIT, string einList, string statusList)
        {
            List<GetNIBLVoucherReportResult> list =
             PayrollDataContext.GetNIBLVoucherReport(payrollPeriodId, showRetiredOnly, voucherType, branchId, NIBLCardDepartmentOnly, includeExternalCIT, einList, statusList).ToList();

            //if (CommonManager.CompanySetting.WhichCompany == WhichCompany.Janata)
            //{
            //    foreach (GetNIBLVoucherReportResult item in list)
            //    {
            //        if(item.MainCode.Length != 15)
            //            item.MainCode = item.MainCode.PadLeft(15, '0');
            //    }
            //}

            return list;
        }

        public static List<NCCVoucherBO> ConvertNIBLToNCCVoucherList(List<GetNIBLVoucherMultipleAddOnReportResult> lists)
        {
            List<NCCVoucherBO> newList = new List<NCCVoucherBO>();

            foreach(var item in lists)
            {
                NCCVoucherBO bo = new NCCVoucherBO();
                bo.AMOUNT = item.Amount == null ? 0 : item.Amount.Value;
                bo.LCYamount = item.Amount == null ? 0 : item.Amount.Value;
                bo.BranchCode = item.BranchCode;
                bo.BranchEmployeeName = "";
                bo.Desc1 = item.TranType;
                bo.MainCode = item.MainCode;
                bo.TranCode = item.TranCode;

                newList.Add(bo);
            }


            return newList;
        }

        public static List<NCCVoucherBO> ConvertNIBLToNCCVoucherList(List<GetNIBLVoucherReportResult> lists)
        {
            List<NCCVoucherBO> newList = new List<NCCVoucherBO>();

            foreach (var item in lists)
            {
                NCCVoucherBO bo = new NCCVoucherBO();
                bo.AMOUNT = item.Amount == null ? 0 : item.Amount.Value;
                bo.LCYamount = item.Amount == null ? 0 : item.Amount.Value;
                bo.BranchCode = item.BranchCode;
                bo.BranchEmployeeName = item.EmployeeName;
                bo.Desc1 = item.TranType;
                bo.MainCode = item.MainCode;
                bo.TranCode = item.TranCode;

                newList.Add(bo);
            }


            return newList;
        }

        public static List<FlexCubeVoucherBO> GetFlexCubeVoucher(int payrollPeriodId, bool? showRetiredOnly,
            int? voucherType, int branchId, bool? NIBLCardDepartmentOnly, bool? includeExternalCIT, string einList, string statusList)
        {
            List<GetNIBLVoucherReportResult> list = GetNIBLVoucherReport(payrollPeriodId, showRetiredOnly, voucherType, branchId, NIBLCardDepartmentOnly, includeExternalCIT, einList, statusList);

            List<FlexCubeVoucherBO> newList = new List<FlexCubeVoucherBO>();

            int index = 0;
            foreach (GetNIBLVoucherReportResult item in list)
            {
                newList.Add(new FlexCubeVoucherBO
                {
                    SOURCE_CODE = "FLEXCUBE",
                    BRANCH_CODE = "999",
                    CURR_NO = (++index),
                    UPLOAD_STAT = "U",
                    CCY_CD = "NPR",
                    INITIATION_DATE = DateTime.Now,
                    Amount = item.Amount,
                    ACCOUNT = item.MainCode,
                    ACCOUNT_BRANCH = item.BranchCode,
                    TXN_CODE = "MSC",
                    DR_CR = item.TranCode,
                    LCY_EQUIVALENT = item.Amount,
                    EXCH_RATE = "",
                    VALUE_DATE = DateTime.Now,
                    ADDL_TEXT = item.TranType // Desc1
                });
            }

            return newList;
        }

        public static List<FlexCubeVoucherBO> GetFlexCubeVoucherAddon(int payrollPeriodId, int? voucherType, int addOnId, int branchId, bool? NIBLCardDepartmentOnly, string einList, bool applyDateFilter, DateTime? addoNDate)
        {
            List<GetNIBLVoucherMultipleAddOnReportResult> list = GetNIBLVoucherReportForAddOn(payrollPeriodId, voucherType, addOnId, branchId, NIBLCardDepartmentOnly, einList, applyDateFilter, addoNDate);

            List<FlexCubeVoucherBO> newList = new List<FlexCubeVoucherBO>();

            int index = 0;
            foreach (GetNIBLVoucherMultipleAddOnReportResult item in list)
            {
                newList.Add(new FlexCubeVoucherBO
                {
                    SOURCE_CODE = "FLEXCUBE",
                    BRANCH_CODE = "999",
                    CURR_NO = (++index),
                    UPLOAD_STAT = "U",
                    CCY_CD = "NPR",
                    INITIATION_DATE = DateTime.Now,
                    Amount = item.Amount,
                    ACCOUNT = item.MainCode,
                    ACCOUNT_BRANCH = item.BranchCode,
                    TXN_CODE = "MSC",
                    DR_CR = item.TranCode,
                    LCY_EQUIVALENT = item.Amount,
                    EXCH_RATE = "",
                    VALUE_DATE = DateTime.Now,
                    ADDL_TEXT = item.TranType // Desc1
                });
            }

            return newList;
        }

        public static List<GetNIBLVoucherMultipleAddOnReportResult> GetNIBLVoucherReportForAddOn(
            int payrollPeriodId, int? voucherType, int addOnId, int branchId, bool? NIBLCardDepartmentOnly, string einList, bool applyDateFilter, DateTime? addOnDateFilter)
        {

            List<GetNIBLVoucherMultipleAddOnReportResult> list =
                PayrollDataContext.GetNIBLVoucherMultipleAddOnReport(payrollPeriodId, voucherType, addOnId, branchId, NIBLCardDepartmentOnly, einList, applyDateFilter, addOnDateFilter).ToList();
            //if (CommonManager.CompanySetting.WhichCompany == WhichCompany.Janata)
            //{
            //    foreach (GetNIBLVoucherMultipleAddOnReportResult item in list)
            //    {
            //        if (item.MainCode.Length != 15)
            //            item.MainCode = item.MainCode.PadLeft(15, '0');
            //    }
            //}
            return list;
        }
        public static List<GetHPLVoucherReportForAddOnResult> GetHPLVoucherReportForAddOn(int payrollPeriodId)
        {

            return PayrollDataContext.GetHPLVoucherReportForAddOn(payrollPeriodId).ToList();

        }
        public static List<GetHPLVoucherDeductionReportResult> GetHPLVoucherDeductionReport(int payrollPeriodId, bool isAddOn, bool? showRetiredOnly)
        {

            return PayrollDataContext.GetHPLVoucherDeductionReport(payrollPeriodId, isAddOn, showRetiredOnly).ToList();

        }
        public static List<Report_GetTaxCalculationListResult> GetTaxDetailsList(int payrollPeriodId)
        {

            if (payrollPeriodId == null)
            {
                return new List<Report_GetTaxCalculationListResult>();
            }

            bool readingSumForMiddleFiscalYearStartedReqd = false;
            //contains the id of starting-ending payroll period to be read for history of regular income
            int startingPayrollPeriodId = 0, endingPayrollPeriodId = 0;
            // int notWorkedMonthInFiscalYear = 0, passedMonthWorked = 0;


            CalculationManager.GenerateForPastIncome(
                     payrollPeriodId, ref readingSumForMiddleFiscalYearStartedReqd,
                     ref startingPayrollPeriodId, ref endingPayrollPeriodId);

            int empStartingPayrollId = startingPayrollPeriodId;
            int empEndingPayrollId = endingPayrollPeriodId;

            List<Report_GetTaxCalculationListResult> list = PayrollDataContext.Report_GetTaxCalculationList(payrollPeriodId, SessionManager.CurrentCompanyId,
                 readingSumForMiddleFiscalYearStartedReqd, empStartingPayrollId, empEndingPayrollId,
                 LeaveAttendanceManager.GetTotalDaysInFiscalYearForPeriod(payrollPeriodId)).ToList();



            return list;
        }
        #endregion

        #region "Work days"
        public static List<GetForWorkdaysResult> GetWorkdays(string empName, int currentPage, int pageSize, ref int totalRows)
        {
            List<GetForWorkdaysResult> list = PayrollDataContext.GetForWorkdays(SessionManager.CurrentCompanyId, empName, CommonManager.IsFirstSalaryGenerated(), currentPage, pageSize).ToList();

            if (list.Count > 0)
                totalRows = list[0].TotalRows.Value;
            else
                totalRows = 0;

            return list;
        }
        public static List<TextValue> GetAccountNoForAllEmp()
        {
            return
                (
                   from e in PayrollDataContext.PPays

                   select new TextValue
                   {
                       EIN = e.EmployeeId.Value,
                       Text = e.BankACNo
                   }
                ).ToList();

        }
        public static void SaveForOpeningWorkdays(string xml)
        {
            PayrollDataContext.SaveOpeningWorkdays(XElement.Parse(xml));
        }

        public static void UpdateWorkday(List<GetForWorkdaysResult> list)
        {
            List<OpeningWorkday> dbList = PayrollDataContext.OpeningWorkdays.ToList();

            foreach (GetForWorkdaysResult item in list)
            {
                OpeningWorkday db = dbList.FirstOrDefault(x => x.EmployeeId == item.EmployeeId);
                if (db == null)
                {
                    db = new OpeningWorkday();
                    PayrollDataContext.OpeningWorkdays.InsertOnSubmit(db);
                }

                db.EmployeeId = item.EmployeeId;

                db.WorkdayAdjustment = item.WorkdayAdjustment;
                db.UnpaidLeavesForGratuity = item.UnpaidLeavesForGratuity;
                db.GratuityAmountAdjustment = item.GratuityAmountAdjustment;
            }

            PayrollDataContext.SubmitChanges();
        }

        #endregion

        public static bool IsEmployeeIDExists(int ein)
        {
            return
                PayrollDataContext.EEmployees.Any(x => x.EmployeeId == ein);
        }

        /// <summary>
        /// Do not used this function as it will return all including retired employees also
        /// </summary>
        /// <returns></returns>
        public static List<EEmployee> GetAllEmployees()
        {
            List<int> customRoleDepartmentIDList = SessionManager.CustomRoleDeparmentList;

            //if (CommonManager.CompanySetting.WhichCompany == WhichCompany.RBB)
            //{
            //    return PayrollDataContext.EEmployees
            //        // Custom Role Dep Restriction case
            //        .Where(x => customRoleDepartmentIDList.Count == 0 || customRoleDepartmentIDList.Contains(x.DepartmentId.Value))
            //        .Where(e => e.CompanyId == SessionManager.CurrentCompanyId)
            //        .OrderBy(e => e.Name).ToList();
            //}
            //else
            {

                return PayrollDataContext.EEmployees
                    // Custom Role Dep Restriction case
                    .Where(x => customRoleDepartmentIDList.Count == 0 || customRoleDepartmentIDList.Contains(x.DepartmentId.Value))
                    .Where(e => e.CompanyId == SessionManager.CurrentCompanyId)
                    .OrderBy(e => e.Name).ToList();
            }
        }




        public static System.Collections.Generic.List<GetEmployeeAddressReportResult> GetEmployeeAddressReport(int BranchID, int DepartmentId, string Employee
        , int currentPage, int pageSize, ref int totalRecords, string sortBy, string SearchText)
        {
            List<GetEmployeeAddressReportResult> list = PayrollDataContext.GetEmployeeAddressReport(BranchID, Employee, currentPage, pageSize, sortBy, DepartmentId, SearchText).ToList();

            if (list.Count > 0)
                totalRecords = list[0].TotalRows.Value;
            else
                totalRecords = 0;
            return list;


            //List<GetEmployeeAddressReportResult> list = new List<GetEmployeeAddressReportResult>();
            //list = GetFromCache<List<GetEmployeeAddressReportResult>>("GetEmployeeAddressReportResult");

            //if (list == null)
            //{
            //    list = PayrollDataContext.GetEmployeeAddressReport(-1, "-1",
            //              0, 99999, sortBy).ToList();
            //    SaveToCache<List<GetEmployeeAddressReportResult>>("GetEmployeeAddressReportResult", list);
            //}


            ////if (list != null)//if data in cache then get all data from cache and fileter with employee,paging and branch
            //{
            //    list = list.Where(x => (x.Name.ToLower().Contains(Employee.ToLower()) || Employee == "-1") && (x.BranchID == BranchID || BranchID == -1)).ToList();

            //    totalRecords = list.Count;

            //    if (currentPage >= 0 && pageSize > 0)
            //    {
            //        switch (sortBy.ToLower())
            //        {

            //            default:
            //                list = list.OrderBy(u => u.Name).ToList();
            //                break;
            //            case "name ascending":
            //                list = list.OrderBy(u => u.Name).ToList();
            //                break;
            //            case "name descending":
            //                list = list.OrderByDescending(u => u.Name).ToList();
            //                break;

            //            case "branch ascending":
            //                list = list.OrderBy(u => u.Branch).ToList();
            //                break;
            //            case "branch descending":
            //                list = list.OrderByDescending(u => u.Branch).ToList();
            //                break;


            //            case "department ascending":
            //                list = list.OrderBy(u => u.Department).ToList();
            //                break;
            //            case "department descending":
            //                list = list.OrderByDescending(u => u.Department).ToList();
            //                break;


            //            case "location ascending":
            //                list = list.OrderBy(u => u.Location).ToList();
            //                break;
            //            case "location descending":
            //                list = list.OrderByDescending(u => u.Location).ToList();
            //                break;

            //            case "directline ascending":
            //                list = list.OrderBy(u => u.DirectLine).ToList();
            //                break;
            //            case "directline descending":
            //                list = list.OrderByDescending(u => u.DirectLine).ToList();
            //                break;

            //            case "extension ascending":
            //                list = list.OrderBy(u => u.Extension).ToList();
            //                break;
            //            case "extension descending":
            //                list = list.OrderByDescending(u => u.Extension).ToList();
            //                break;


            //            case "mobile ascending":
            //                list = list.OrderBy(u => u.Mobile).ToList();
            //                break;
            //            case "mobile descending":
            //                list = list.OrderByDescending(u => u.Mobile).ToList();
            //                break;

            //            case "areacode ascending":
            //                list = list.OrderBy(u => u.AreaCode).ToList();
            //                break;
            //            case "areacode descending":
            //                list = list.OrderByDescending(u => u.AreaCode).ToList();
            //                break;

            //            case "branchphone ascending":
            //                list = list.OrderBy(u => u.BranchPhone).ToList();
            //                break;
            //            case "branchphone descending":
            //                list = list.OrderByDescending(u => u.BranchPhone).ToList();
            //                break;

            //            case "fax ascending":
            //                list = list.OrderBy(u => u.Fax).ToList();
            //                break;
            //            case "fax descending":
            //                list = list.OrderByDescending(u => u.Fax).ToList();
            //                break;

            //            case "email ascending":
            //                list = list.OrderBy(u => u.Email).ToList();
            //                break;
            //            case "email descending":
            //                list = list.OrderByDescending(u => u.Email).ToList();
            //                break;


            //        }

            //        list = list.Skip(currentPage).Take(pageSize).ToList();
            //    }
            //}
            ////else
            ////{
            ////    //save all initital data to cache.


            ////    if (currentPage >= 0 && pageSize > 0)
            ////    {
            ////        list = list.Skip(currentPage).Take(pageSize).ToList();
            ////    }

            ////}



            //return list;
        }

        public static List<EEmployee> GetAllActiveEmployees()
        {
            List<int> customRoleDepartmentIDList = SessionManager.CustomRoleDeparmentList;

            //return PayrollDataContext.EEmployees
            //    // Custom Role Dep Restriction case
            //    .Where(x => customRoleDepartmentIDList.Count == 0 || customRoleDepartmentIDList.Contains(x.DepartmentId.Value))
            //    .Where(e => e.CompanyId == SessionManager.CurrentCompanyId)
            //    .OrderBy(e => e.EmployeeId).ToList();
            return
            (
                from e in PayrollDataContext.EEmployees
                join h in PayrollDataContext.EHumanResources on e.EmployeeId equals h.EmployeeId
                where e.CompanyId == SessionManager.CurrentCompanyId
                && (customRoleDepartmentIDList.Count == 0 || customRoleDepartmentIDList.Contains(e.DepartmentId.Value))
                &&
                (
                    (
                        e.IsResigned == false ||
                        h.DateOfResignationEng >= DateTime.Now.Date
                        )
                    &&
                     (
                        e.IsRetired == false ||
                        h.DateOfRetirementEng >= DateTime.Now.Date
                        )
                )

                select e

                ).OrderBy(x => x.Name).ToList();
        }


        public static List<EEmployee> GetAllEmployeesForEmployeeImportExport()
        {

            PayrollPeriod lastPayroll = CommonManager.GetLastPayrollPeriod();

            if (lastPayroll == null)
                throw new Exception("Payroll Period must be defined.");

            List<int> branchList = new List<int>();
            if (SessionManager.IsCustomRole)
                branchList = SessionManager.CustomRoleBranchList;

            bool isPsi = CommonManager.CompanySetting.WhichCompany == WhichCompany.PSI;

            return
                (
                from e in PayrollDataContext.EEmployees
                join h in PayrollDataContext.EHumanResources on e.EmployeeId equals h.EmployeeId
                where

                    e.CompanyId == SessionManager.CurrentCompanyId
                    && (branchList.Count == 0 || branchList.Contains(e.BranchId.Value))
                    && (e.IsResigned == null || e.IsResigned.Value == false || (lastPayroll == null ||
                            (isPsi ? h.DateOfResignationEng.Value.AddMonths(1) : h.DateOfResignationEng) >= lastPayroll.StartDateEng))
                    && (e.IsRetired == null || e.IsRetired.Value == false || (lastPayroll == null ||
                        (isPsi ? h.DateOfRetirementEng.Value.AddMonths(1) : h.DateOfRetirementEng) >= lastPayroll.StartDateEng))
                orderby e.EmployeeId
                select e

                ).ToList();


        }

        public static List<EEmployee> GetAllEmployeesForEmployeeImportExport(bool isAllEmp)
        {

            PayrollPeriod lastPayroll = CommonManager.GetLastPayrollPeriod();

            if (lastPayroll == null)
                throw new Exception("Payroll Period must be defined.");

            List<int> branchList = new List<int>();
            if (SessionManager.IsCustomRole)
                branchList = SessionManager.CustomRoleBranchList;

            bool isPsi = CommonManager.CompanySetting.WhichCompany == WhichCompany.PSI;

            if (isAllEmp)
            {
                return
                    (
                    from e in PayrollDataContext.EEmployees
                    join h in PayrollDataContext.EHumanResources on e.EmployeeId equals h.EmployeeId
                    where

                        e.CompanyId == SessionManager.CurrentCompanyId
                    //&& (branchList.Count == 0 || branchList.Contains(e.BranchId.Value))
                    //&& (e.IsResigned == null || e.IsResigned.Value == false || (lastPayroll == null ||
                    //        (isPsi ? h.DateOfResignationEng.Value.AddMonths(1) : h.DateOfResignationEng) >= lastPayroll.StartDateEng))
                    //&& (e.IsRetired == null || e.IsRetired.Value == false || (lastPayroll == null ||
                    //    (isPsi ? h.DateOfRetirementEng.Value.AddMonths(1) : h.DateOfRetirementEng) >= lastPayroll.StartDateEng))
                    orderby e.EmployeeId
                    select e

                    ).ToList();

            }
            else
            {
                return
                    (
                    from e in PayrollDataContext.EEmployees
                    join h in PayrollDataContext.EHumanResources on e.EmployeeId equals h.EmployeeId
                    where

                        e.CompanyId == SessionManager.CurrentCompanyId
                        && (branchList.Count == 0 || branchList.Contains(e.BranchId.Value))
                        && (e.IsResigned == null || e.IsResigned.Value == false || (lastPayroll == null ||
                                (isPsi ? h.DateOfResignationEng.Value.AddMonths(1) : h.DateOfResignationEng) >= lastPayroll.StartDateEng))
                        && (e.IsRetired == null || e.IsRetired.Value == false || (lastPayroll == null ||
                            (isPsi ? h.DateOfRetirementEng.Value.AddMonths(1) : h.DateOfRetirementEng) >= lastPayroll.StartDateEng))
                    orderby e.EmployeeId
                    select e

                    ).ToList();

            }
        }

        public static void SetNextPreviousEmployeeId(int currentEmployeeId, ref int previousEmpId, ref string previousEmpName, ref int nextEmpId, ref string nextEmpName,
            ref int firstEmpId, ref int lastEmpId)
        {


            List<int> customRoleDepartmentIDList = SessionManager.CustomRoleDeparmentList;



            firstEmpId = PayrollDataContext.EEmployees.Where(e => e.CompanyId == SessionManager.CurrentCompanyId)
                // check if Custom Role restricted dep then filter by dep also
                .Where(x => x.DepartmentId != -1 && (customRoleDepartmentIDList.Count == 0 || customRoleDepartmentIDList.Contains(x.DepartmentId.Value)))
                .OrderBy(e => e.EmployeeId).Take(1).Select(e => e.EmployeeId).SingleOrDefault();

            lastEmpId = PayrollDataContext.EEmployees
                .Where(e => e.CompanyId == SessionManager.CurrentCompanyId)
                .Where(x => customRoleDepartmentIDList.Count == 0 || customRoleDepartmentIDList.Contains(x.DepartmentId.Value))
                .OrderByDescending(e => e.EmployeeId).Take(1).Select(e => e.EmployeeId).SingleOrDefault();



            var nextEmployee =
                  PayrollDataContext.EEmployees.Where(e => e.EmployeeId > currentEmployeeId && e.CompanyId == SessionManager.CurrentCompanyId)
                   .Where(x => customRoleDepartmentIDList.Count == 0 || customRoleDepartmentIDList.Contains(x.DepartmentId.Value))
                  .OrderBy(e => e.EmployeeId)
                  .Take(1).Select(e => new { e.EmployeeId, e.Name }).SingleOrDefault();

            var previousEmployee =
                PayrollDataContext.EEmployees.Where(e => e.EmployeeId < currentEmployeeId && e.CompanyId == SessionManager.CurrentCompanyId)
                 .Where(x => customRoleDepartmentIDList.Count == 0 || customRoleDepartmentIDList.Contains(x.DepartmentId.Value))
                .OrderByDescending(e => e.EmployeeId)
                .Take(1).Select(e => new { e.EmployeeId, e.Name }).SingleOrDefault();

            if (nextEmployee != null)
            {
                nextEmpId = nextEmployee.EmployeeId;
                nextEmpName = nextEmployee.Name;
            }

            if (previousEmployee != null)
            {
                previousEmpId = previousEmployee.EmployeeId;
                previousEmpName = previousEmployee.Name;
            }

        }


        /// <summary>
        /// Check if the Income is used in the Calculation
        /// </summary>
        public static bool HasEmployeeIncludedInCalculation(int employeeId)
        {

            return PayrollDataContext.CCalculationEmployees.Any(
                e => e.EmployeeId == employeeId);
        }

        /// <summary>
        /// Check if the Income is used in the Calculation
        /// </summary>
        public static bool IsRetiredOrResignedIncludedInCalculation(int employeeId)
        {
            //return  PayrollDataContext.CCalculationEmployees.Any(
            //    e => e.EmployeeId == employeeId && e.IsRetiredOrResigned == true);
            return
                PayrollDataContext.IsEmployeeRetiredBothDateAndCalculationChecking(employeeId, null).Value == 1;
        }



        /// <summary>
        /// Check if the Income is used in the Calculation
        /// </summary>
        public static bool HasEmployeeAndIncomeIncludedInCalculation(int incomeId, int employeeId)
        {
            return PayrollDataContext.CalcHasIncomeAndEmployeeIncluded(incomeId,
                     (int)CalculationColumnType.Income, employeeId)
                .Value;

        }

        public static SettlementLeaveDetail GetSettlementLeaveDetail(int payrollPeriodId, int employeeId, bool isEncashment)
        {
            if (isEncashment)
            {

                return
                    PayrollDataContext.SettlementLeaveDetails.SingleOrDefault
                    (x => x.PayrollPeriodId == payrollPeriodId && x.EmployeeId == employeeId
                        && x.LeaveEncashmentAmount != null && x.LeaveEncashmentAmount.Value > 0);
            }
            else
            {

                return
                    PayrollDataContext.SettlementLeaveDetails.SingleOrDefault
                    (x => x.PayrollPeriodId == payrollPeriodId && x.EmployeeId == employeeId
                        && x.LeaveEncashmentAmount != null && x.LeaveEncashmentAmount.Value < 0);
            }
        }

        //public static void SaveSettlementDetail(SettlementDetail entity, int payrollPeriodId, int employeeId, int type)
        //{
        //    SettlementDetail dbEntity = GetSettlementDetail(payrollPeriodId, employeeId, type);

        //    if (dbEntity != null)
        //    {
        //        dbEntity.OldValue = entity.OldValue;
        //        dbEntity.Value = entity.Value;
        //    }
        //    else
        //        PayrollDataContext.SettlementDetails.InsertOnSubmit(entity);

        //    PayrollDataContext.SubmitChanges();
        //}

        public static void SaveSettlementLeaveDetail(SettlementLeaveDetail entity, int payrollPeriodId, int employeeId, bool isEncashment)
        {
            SettlementLeaveDetail dbEntity = GetSettlementLeaveDetail(payrollPeriodId, employeeId, isEncashment);

            if (dbEntity != null)
            {
                //dbEntity.OldLeaveEncashmentAmount = entity.OldLeaveEncashmentAmount;
                dbEntity.LeaveEncashmentAmount = entity.LeaveEncashmentAmount;
            }
            else
                PayrollDataContext.SettlementLeaveDetails.InsertOnSubmit(entity);

            PayrollDataContext.SubmitChanges();
        }
        public static SettlementDetail GetSettlementDetail(int payrollPeriodId, int employeeId, int type)
        {

            return
                PayrollDataContext.SettlementDetails.SingleOrDefault
                (x => x.PayrollPeriodId == payrollPeriodId && x.EmployeeId == employeeId
                    && x.Type == type);

        }
        public static List<EEmployee> GetRetiredResignedEmployeeForPayrollPeriod(int payrollPeriodId)
        {

            return
                (
                    from c in PayrollDataContext.CCalculations
                    join ce in PayrollDataContext.CCalculationEmployees on c.CalculationId equals ce.CalculationId
                    join e in PayrollDataContext.EEmployees on ce.EmployeeId equals e.EmployeeId
                    where c.PayrollPeriodId == payrollPeriodId && ce.IsRetiredOrResigned == true
                    select e
                ).ToList();
        }

        public static List<GetValidEmployeesForPayrollPeriodResult> GetValidEmployeesForPayrollPeriod(int payrollPeriodId,
            int branchId, int departmentId)
        {
            return PayrollDataContext.GetValidEmployeesForPayrollPeriod(
                SessionManager.CurrentCompanyId, payrollPeriodId, branchId, departmentId).ToList();
        }
        public static List<GetEmployeesForProjectResult> GetEmployeesForProject(int payrollPeriodId,
           int branchId, int departmentId)
        {
            return PayrollDataContext.GetEmployeesForProject(
                SessionManager.CurrentCompanyId, branchId, departmentId, payrollPeriodId).ToList();
        }

        /// <summary>
        /// Check if the Income is used in the Calculation
        /// </summary>
        public static bool HasEmployeeAndDeductionIncludedInCalculation(int deductionId, int employeeId)
        {
            return PayrollDataContext.CalcHasDeductionAndEmployeeIncluded(deductionId,
                     (int)CalculationColumnType.Deduction, employeeId)
                .Value;

        }

        public static bool HasEmployeeCITIncludedInCalculation(int employeeId)
        {
            return PayrollDataContext.CalcHasCITIncludedInCalculation(
                employeeId, (int)CalculationColumnType.DeductionCIT).Value;



        }

        public static bool HasOtherFundEmployee()
        {
            return PayrollDataContext.EFundDetails.Any(x => x.HasOtherFund != null && x.HasOtherFund.Value == true);
        }

        public static Status SaveImportedEmployee(List<EEmployee> list)
        {
            Status status = new Status();

            PIncome basicIncome = new PayManager().GetBasicIncome(SessionManager.CurrentCompanyId);
            foreach (EEmployee entity in list)
            {
                if (entity.EmployeeId == 0)
                {
                    status = NewHRManager.IsEmployeeCountValidForNewEmp();
                    if (status.IsSuccess == false)
                        return status;

                    PayrollDataContext.EEmployees.InsertOnSubmit(entity);
                }
                else
                {
                    EEmployee dbEntity = GetEmployeeById(entity.EmployeeId);

                    //if not 0 & not exists in the system then insert the employee with user input EmployeeId
                    if (dbEntity == null)
                    {
                        status = NewHRManager.IsEmployeeCountValidForNewEmp();
                        if (status.IsSuccess == false)
                            return status;

                        PayrollDataContext.CreateBlankEmployee(entity.EmployeeId);
                        dbEntity = GetEmployeeById(entity.EmployeeId);
                        CopyObject<EEmployee>(entity, ref dbEntity, "EmployeeId", "Created", "Modified");


                        dbEntity.EHumanResources.Add(entity.EHumanResources[0]);
                        dbEntity.EAddresses.Add(entity.EAddresses[0]);
                        dbEntity.EFundDetails.Add(entity.EFundDetails[0]);
                        dbEntity.IIndividualInsurances.Add(entity.IIndividualInsurances[0]);
                        dbEntity.PEmployeeIncomes.AddRange(entity.PEmployeeIncomes);
                        dbEntity.PEmployeeDeductions.AddRange(entity.PEmployeeDeductions);
                        dbEntity.LEmployeeLeaves.AddRange(entity.LEmployeeLeaves);
                        dbEntity.ECurrentStatus.AddRange(entity.ECurrentStatus);
                        dbEntity.PPays.AddRange(entity.PPays);

                        if (entity.EWorkShiftHistories.Count > 0)
                            dbEntity.EWorkShiftHistories.Add(entity.EWorkShiftHistories[0]);
                        dbEntity.HHumanResources.Add(entity.HHumanResources[0]);

                        //if (dbEntity.PPays.Count <= 0)
                        //    dbEntity.PPays.Add(new PPay());

                        PayrollDataContext.SubmitChanges();

                    }
                    // Process to update information from Import
                    else if (dbEntity != null)
                    {
                        bool isAttendanceSaved = LeaveAttendanceManager.IsAttendanceSavedForEmployee(dbEntity.EmployeeId);
                        bool isSalaryGenerated = CalculationManager.IsCalculatedSavedForEmployee(dbEntity.EmployeeId);

                        dbEntity.Title = entity.Title;
                        dbEntity.FirstName = entity.FirstName;
                        dbEntity.MiddleName = entity.MiddleName;
                        dbEntity.LastName = entity.LastName;
                        dbEntity.Name = entity.Name;
                        dbEntity.FatherName = entity.FatherName;
                        dbEntity.Gender = entity.Gender;
                        dbEntity.MaritalStatus = entity.MaritalStatus;
                        dbEntity.IsHandicapped = entity.IsHandicapped;
                        dbEntity.HasCoupleTaxStatus = entity.HasCoupleTaxStatus;
                        dbEntity.HasDependent = entity.HasDependent;
                        dbEntity.IsEnglishDOB = entity.IsEnglishDOB;
                        dbEntity.DateOfBirth = entity.DateOfBirth;
                        dbEntity.CostCodeId = entity.CostCodeId;
                        dbEntity.GradeId = entity.GradeId;
                        dbEntity.UnitId = entity.UnitId;

                        try
                        {
                            if (!string.IsNullOrEmpty(entity.DateOfBirth))
                                dbEntity.DateOfBirthEng = GetEngDate(entity.DateOfBirth, entity.IsEnglishDOB.Value);
                            else
                                dbEntity.DateOfBirthEng = null;
                        }
                        catch
                        {
                        }

                        if (NewPayrollManager.IsDesignationEditable(entity.EmployeeId))
                            dbEntity.DesignationId = entity.DesignationId;
                        if (NewHRManager.IsBranchEditable(entity.EmployeeId))
                        {
                            dbEntity.BranchId = entity.BranchId;
                            dbEntity.DepartmentId = entity.DepartmentId;
                            dbEntity.SubDepartmentId = entity.SubDepartmentId;
                        }

                        if (dbEntity.HHumanResources.Count > 0)
                            dbEntity.HHumanResources[0].CasteId = entity.HHumanResources[0].CasteId;
                        else
                            dbEntity.HHumanResources.Add(entity.HHumanResources[0]);

                        dbEntity.EHumanResources[0].LocationId = entity.EHumanResources[0].LocationId;
                        dbEntity.EHumanResources[0].IdCardNo = entity.EHumanResources[0].IdCardNo;
                        dbEntity.EAddresses[0].CIEmail = entity.EAddresses[0].CIEmail;
                        dbEntity.EAddresses[0].CIPhoneNo = entity.EAddresses[0].CIPhoneNo;
                        dbEntity.EAddresses[0].Extension = entity.EAddresses[0].Extension;
                        dbEntity.EFundDetails[0].PFRFNo = entity.EFundDetails[0].PFRFNo;
                        dbEntity.EFundDetails[0].EmployeePan = entity.EFundDetails[0].EmployeePan;
                        dbEntity.EFundDetails[0].PANNo = entity.EFundDetails[0].PANNo;
                        dbEntity.EFundDetails[0].CITNo = entity.EFundDetails[0].CITNo;
                        dbEntity.EFundDetails[0].CITCode = entity.EFundDetails[0].CITCode;

                        dbEntity.PPays[0].PaymentMode = entity.PPays[0].PaymentMode;
                        dbEntity.PPays[0].BankName = entity.PPays[0].BankName;
                        dbEntity.PPays[0].BankACNo = entity.PPays[0].BankACNo;
                        dbEntity.PPays[0].BankID = entity.PPays[0].BankID;

                        //Shift Processing
                        if (entity.EWorkShiftHistories.Count > 0)
                        {
                            // if joined Shift exists then update 
                            if (dbEntity.EWorkShiftHistories.Count > 0
                                && dbEntity.EWorkShiftHistories[0].FromDate == entity.EWorkShiftHistories[0].FromDate)
                            {
                                dbEntity.EWorkShiftHistories[0].WorkShiftId = entity.EWorkShiftHistories[0].WorkShiftId;
                            }
                            // else insert shift for the employee
                            else
                            {
                                dbEntity.EWorkShiftHistories.Add(entity.EWorkShiftHistories[0]);
                            }
                        }

                        // For Insurance
                        IIndividualInsurance dbInsurance =
                            PayrollDataContext.IIndividualInsurances.FirstOrDefault(x => x.EmployeeId == dbEntity.EmployeeId
                                && x.FinancialYearId == SessionManager.CurrentCompanyFinancialDate.FinancialDateId);

                        if (dbInsurance != null)
                            dbInsurance.Premium = entity.IIndividualInsurances[0].Premium;
                        else
                            dbEntity.IIndividualInsurances.Add(entity.IIndividualInsurances[0]);



                        if (isAttendanceSaved == false)
                        {
                            // Status
                            dbEntity.ECurrentStatus.Clear();
                            dbEntity.ECurrentStatus.AddRange(entity.ECurrentStatus);

                            dbEntity.EHumanResources[0].SalaryCalculateFrom = dbEntity.ECurrentStatus[0].FromDate;
                            dbEntity.EHumanResources[0].SalaryCalculateFromEng = dbEntity.ECurrentStatus[0].FromDateEng;


                            // Leaves
                            dbEntity.LEmployeeLeaves.Clear();
                            dbEntity.LEmployeeLeaves.AddRange(entity.LEmployeeLeaves);
                        }

                        if (isSalaryGenerated == false)
                        {
                            // CIT
                            dbEntity.EFundDetails[0].CITEffectiveFromMonth = entity.EFundDetails[0].CITEffectiveFromMonth;
                            dbEntity.EFundDetails[0].CITEffectiveFromYear = entity.EFundDetails[0].CITEffectiveFromYear;
                            dbEntity.EFundDetails[0].HasCIT = entity.EFundDetails[0].HasCIT;

                            dbEntity.EFundDetails[0].CITIsRate = entity.EFundDetails[0].CITIsRate;
                            dbEntity.EFundDetails[0].CITRate = entity.EFundDetails[0].CITRate;
                            dbEntity.EFundDetails[0].CITAmount = entity.EFundDetails[0].CITAmount;

                            // Incomes
                            List<PEmployeeIncome> incomesToBeClear =
                               (
                                from ei in PayrollDataContext.PEmployeeIncomes
                                join i in PayrollDataContext.PIncomes on ei.IncomeId equals i.IncomeId
                                where i.Calculation != IncomeCalculation.DEEMED_INCOME
                                   && ei.EmployeeId == dbEntity.EmployeeId
                                select ei
                               ).ToList();
                            foreach (PEmployeeIncome income in incomesToBeClear)
                                dbEntity.PEmployeeIncomes.Remove(income);

                            // first add basic income for maininging sequence
                            PEmployeeIncome empBasicIncome = entity.PEmployeeIncomes.FirstOrDefault(x => x.IncomeId == basicIncome.IncomeId);
                            if (empBasicIncome != null)
                            {
                                dbEntity.PEmployeeIncomes.Add(empBasicIncome);
                                entity.PEmployeeIncomes.Remove(empBasicIncome);
                            }
                            dbEntity.PEmployeeIncomes.AddRange(entity.PEmployeeIncomes);

                            // Deductions
                            List<PEmployeeDeduction> deductionsToBeClear =
                              (
                               from ei in PayrollDataContext.PEmployeeDeductions
                               join i in PayrollDataContext.PDeductions on ei.DeductionId equals i.DeductionId
                               where i.Calculation != DeductionCalculation.Advance
                                && i.Calculation != DeductionCalculation.LOAN
                                && i.Calculation != DeductionCalculation.LOAN_Repayment
                                && ei.EmployeeId == dbEntity.EmployeeId
                               select ei
                              ).ToList();
                            foreach (PEmployeeDeduction income in deductionsToBeClear)
                                dbEntity.PEmployeeDeductions.Remove(income);
                            dbEntity.PEmployeeDeductions.AddRange(entity.PEmployeeDeductions);

                        }
                    }
                }
            }

            PayrollDataContext.SubmitChanges();
            return status;
        }

        public static string GetAttendanceImportErrorMsg(string msg, AttendanceImportEmployeeLeave emp)
        {

            msg += " for the employee \"" + (emp.Name == null ? "" : emp.Name) + "\" ";
            msg += "taken on " + emp.TakenDate.ToShortDateString() + ".";
            return msg;
        }
        public static bool SaveImportedAttendance(List<AttendanceImportEmployee> empList, int payrollPeriodId, ref string msg, ref int count, int? employeeId)
        {

            float dayValue = 0;
            count = 0;
            StringBuilder employeeIDList = new StringBuilder("");
            foreach (AttendanceImportEmployee emp in empList)
            {
                if (employeeIDList.Length == 0)
                    employeeIDList.Append(emp.EmployeeId.ToString());
                else
                    employeeIDList.Append("," + emp.EmployeeId.ToString());
            }

            if (empList.Count == 0 && employeeId != null)
                employeeIDList.Append(employeeId.ToString());
            //get all leaves for the all saving employee
            List<LeaveGenerateEmployeeAccuredResult> allLeaveList
                = LeaveAttendanceManager.GetBeginningBalaceAccuredForEmployee(0,
                payrollPeriodId, SessionManager.CurrentCompanyId, employeeIDList.ToString());


            foreach (AttendanceImportEmployee emp in empList)
            {

                //if same date multiple leave exists then show warning
                //var employees =
                //   (from p in emp.leaves
                //    group p by new { p.Name, p.TakenDate } into d
                //    where d.Count() > 1
                //    select new { d.Key.Name, d.Key.TakenDate });

                // do the processing,do not stop for this case
                //foreach (var v in employees)
                //{
                //    AttendanceImportEmployeeLeave type = new AttendanceImportEmployeeLeave { Name = v.Name, TakenDate = v.TakenDate };
                //    msg = GetAttendanceImportErrorMsg("Same date is repeated ", type);
                //    return false;
                //}

                count += 1;

                Attendence attendance = PayrollDataContext.Attendences.SingleOrDefault(x => x.EmployeeId == emp.EmployeeId && x.PayrollPeriodId == payrollPeriodId);
                if (attendance == null)
                {
                    msg = GetAttendanceImportErrorMsg("Only saved attendance can be imported, attendance not saved ", emp.leaves[0]);
                    return false;
                }

                List<AttedenceDetail> attendanceList = PayrollDataContext.AttedenceDetails.Where(x => x.AttendenceId == attendance.AttendenceId).ToList();
                double absentDays = 0;
                double deductDays = 0;

                // 1. Process Cell Values
                foreach (AttendanceImportEmployeeLeave leave in emp.leaves)
                {
                    AttedenceDetail detail = attendanceList.SingleOrDefault(x => x.DateEng == leave.TakenDate);
                    if (detail == null)
                    {
                        continue;
                        //msg = GetAttendanceImportErrorMsg("Attendance doesn't exists ", leave);
                        //return false;
                    }

                    // check for Other leave
                    if (leave.HasMulitpleLeave)
                    {
                        detail.HasMulitpleLeave = true;
                        detail.OtherLeaveTypeId = leave.OtherLeaveTypeId;
                    }

                    if (leave.NoLeave)
                    {
                        detail.DayValue = "P";
                        detail.Type = 0;
                        detail.TypeValue = 0;
                        detail.DeductDays = 0;
                        //detail.
                    }
                    else if (leave.IsUnpaidLeave)
                    {
                        detail.DayValue = leave.LeaveAbbr;
                        detail.Type = 0;
                        detail.TypeValue = 0;
                        if (leave.IsHalfDay)
                            detail.DeductDays = 0.5;
                        else
                            detail.DeductDays = 1;

                        if (CommonManager.CalculationConstant.CompanyHasHourlyLeave.Value && detail.DayValue.Contains("-"))
                        {
                            string hourValue = detail.DayValue.Split(new char[] { '-' })[1].Replace("++", "").Replace("--", "");
                            float value = float.Parse(hourValue);
                            detail.DeductDays = value;
                        }
                    }
                    else if (leave.IsAbsentLeave)
                    {
                        detail.DayValue = leave.LeaveAbbr;
                        detail.Type = 0;
                        detail.TypeValue = 0;
                        detail.DeductDays = 1;
                    }
                    else
                    {

                        // processing for sunday leave for first day make it half day
                        if (CommonManager.CompanySetting.WhichCompany == WhichCompany.DishHome
                            && leave.LeaveTypeId == LeaveAttendanceManager.GetSundayLeaveId()
                            )
                        {
                            if (leave.Day == 1)
                            {
                                if (detail.DayValue.EndsWith("/2") == false)
                                    detail.DayValue = leave.LeaveAbbr + "/2";

                            }
                            else if (leave.Day == 3)
                                detail.DayValue = leave.LeaveAbbr;
                        }
                        else
                            detail.DayValue = leave.LeaveAbbr;

                        detail.Type = 1;
                        detail.TypeValue = leave.LeaveTypeId;

                        detail.DeductDays = LeaveAttendanceManager.GetLeaveDeductDays(leave.LeaveTypeId, leave.IsHalfDay);
                    }
                }
                //Count absent days
                foreach (AttedenceDetail detail in attendanceList)
                {
                    //only for leave type
                    if (detail.DayValue.Contains("++") || detail.DayValue.Contains("--"))
                    {
                    }
                    else if (detail.Type == 1)
                    {
                        if (CommonManager.CompanySetting.WhichCompany == WhichCompany.DishHome
                            && detail.DayValue == LeaveAttendanceManager.GetDishHomeSundayHalfDayAbbr())
                        {
                            absentDays += 0;
                            deductDays += 0;
                        }
                        else if (detail.DayValue.Contains("/2"))
                        {
                            absentDays += 0.5;
                            deductDays += LeaveAttendanceManager.GetLeaveDeductDays(detail.TypeValue.Value, true);
                        }
                        else
                        {

                            if (CommonManager.CalculationConstant.CompanyHasHourlyLeave.Value && detail.DayValue.Contains("-"))
                            {
                                string hourValue = detail.DayValue.Split(new char[] { '-' })[1].Replace("++", "").Replace("--", "");
                                float value = float.Parse(hourValue);
                                absentDays += value;
                                //deductDays // assuming if houlry leave then no deduction, add code in future
                            }
                            else
                            {
                                absentDays += 1;
                                deductDays += LeaveAttendanceManager.GetLeaveDeductDays(detail.TypeValue.Value, false);
                            }

                        }
                    }
                    else if (detail.Type == 0)
                    {
                        if (CommonManager.CalculationConstant.CompanyHasHourlyLeave.Value && detail.DayValue.Contains("-"))
                        {
                            string hourValue = detail.DayValue.Split(new char[] { '-' })[1].Replace("++", "").Replace("--", "");
                            float value = float.Parse(hourValue);
                            absentDays += value;
                            deductDays += value;
                        }
                        else
                        {
                            switch (detail.DayValue)
                            {
                                case "UPL":
                                    absentDays += 1;
                                    deductDays += 1;
                                    break;
                                case "UPL/2":
                                    absentDays += 0.5;
                                    deductDays += 0.5;
                                    break;
                                case "ABS":
                                    absentDays += 1;
                                    deductDays += 1;
                                    break;
                            }
                        }
                    }
                }
                attendance.PresentDays = absentDays;
                attendance.PayDays = deductDays;

                // 2. Process LeaveAdjustments
                //Leave adjustment Start
                //get all leaves for the employee
                List<LeaveGenerateEmployeeAccuredResult> leaveList = allLeaveList.Where(l => l.EmployeeId == emp.EmployeeId).ToList();

                foreach (var leave in leaveList)
                {
                    dayValue = 0;
                    foreach (AttedenceDetail detail in attendanceList)
                    {
                        //if cell has leave value
                        if (detail.Type == 1)
                        {

                            // process for other leave for Prabhu Annual Refreshment and Subsibute leave case
                            if (detail.HasMulitpleLeave)
                            {
                                LeaveGenerateEmployeeAccuredResult otherLeave = leaveList.FirstOrDefault(x => x.LeaveTypeId == detail.OtherLeaveTypeId);
                                if (otherLeave != null)
                                {
                                    otherLeave.Count += 1; // 1 day hard coded
                                    detail.HasMulitpleLeave = false;
                                }
                            }

                            if (CommonManager.CompanySetting.WhichCompany == WhichCompany.DishHome
                                && detail.DayValue == LeaveAttendanceManager.GetDishHomeSundayHalfDayAbbr())
                            {
                                continue;
                            }
                            else if (detail.TypeValue.Value == leave.LeaveTypeId)
                            {
                                bool isHalfDayLeaveSelected = false;

                                if (CommonManager.CompanySetting.WhichCompany == WhichCompany.DishHome
                                        && detail.DayValue == LeaveAttendanceManager.GetDishHomeSundayHalfDayAbbr())
                                {
                                    leave.Count += 0;
                                }
                                // for holiday compensatory leave
                                else if (detail.DayValue.Contains("++"))
                                {
                                    LLeaveType leaveType = new LeaveAttendanceManager().GetLeaveType(leave.LeaveTypeId);

                                    // for hourly enable company replace by hours
                                    if (CommonManager.CalculationConstant.CompanyHasHourlyLeave.Value)
                                    {
                                        string hourValue = detail.DayValue.Split(new char[] { '-' })[1].Replace("++", "").Replace("--", "");
                                        float value = float.Parse(hourValue); ;// / CommonManager.CalculationConstant.CompanyLeaveHoursInADay.Value;
                                        leave.Accured += value;
                                    }
                                    // Process if has Holiday value set
                                    else if (leaveType.CompensatoryOnHolidayFulldayValue != null)
                                    {
                                        if (detail.DayValue.Contains("/2"))
                                        {
                                            leave.Accured += (float)leaveType.CompensatoryOnHolidayHalfdayValue.Value;
                                        }
                                        else
                                        {
                                            leave.Accured += (float)leaveType.CompensatoryOnHolidayFulldayValue.Value;
                                        }
                                    }
                                    else if (detail.DayValue.Contains("/2"))
                                        leave.Accured += (float)0.5;
                                    else
                                        leave.Accured += 1;

                                }
                                // for non holiday comensatory leave
                                else if (detail.DayValue.Contains("--"))
                                {
                                    // for hourly enable company replace by hours
                                    if (CommonManager.CalculationConstant.CompanyHasHourlyLeave.Value)
                                    {
                                        string hourValue = detail.DayValue.Split(new char[] { '-' })[1].Replace("++", "").Replace("--", "");
                                        float value = float.Parse(hourValue); ;// / CommonManager.CalculationConstant.CompanyLeaveHoursInADay.Value;
                                        leave.Count += value;
                                    }
                                    else if (detail.DayValue.Contains("/2"))
                                        leave.Count += (float)0.5;
                                    else
                                        leave.Count += 1;
                                }
                                else
                                {
                                    // If half day or hourly leave then mark it
                                    if (detail.DayValue.Contains(LeaveAttendanceManager.HalfDayPostFix))
                                    {
                                        isHalfDayLeaveSelected = true;
                                    }


                                    // for half day it will be .5
                                    if (isHalfDayLeaveSelected)
                                        dayValue = (float).5;

                                    else // for full day, value is 1
                                    {
                                        dayValue = 1;

                                    }

                                    // for hourly enable company replace by hours
                                    if (CommonManager.CalculationConstant.CompanyHasHourlyLeave.Value && detail.DayValue.Contains("-"))
                                    {
                                        string hourValue = detail.DayValue.Split(new char[] { '-' })[1].Replace("++", "").Replace("--", "");
                                        float value = float.Parse(hourValue); ;// / CommonManager.CalculationConstant.CompanyLeaveHoursInADay.Value;
                                        dayValue = value;
                                    }

                                    leave.Count += dayValue;
                                }
                            }
                        }
                    }
                }

                // 3. Save or Update Leave Adjustment

                foreach (var leave in leaveList)
                {
                    if (leave.Accured == null)
                        continue;

                    LeaveAdjustment dbLeaveAdj = PayrollDataContext.LeaveAdjustments
                        .SingleOrDefault(x => x.EmployeeId == emp.EmployeeId && x.PayrollPeriodId == payrollPeriodId && x.LeaveTypeId == leave.LeaveTypeId);

                    if (dbLeaveAdj == null)
                    {
                        dbLeaveAdj = new LeaveAdjustment();
                        dbLeaveAdj.PayrollPeriodId = payrollPeriodId;
                        dbLeaveAdj.EmployeeId = emp.EmployeeId;
                        dbLeaveAdj.LeaveTypeId = leave.LeaveTypeId;

                        PayrollDataContext.LeaveAdjustments.InsertOnSubmit(dbLeaveAdj);
                    }
                    else
                    {

                    }

                    if (dbLeaveAdj.Adjusted == null)
                        dbLeaveAdj.Adjusted = 0;

                    dbLeaveAdj.BeginningBalance = Convert.ToDecimal(leave.BeginningBalance);
                    dbLeaveAdj.Accured = Convert.ToDecimal(leave.Accured);
                    dbLeaveAdj.Taken = Convert.ToDouble(leave.Count);
                    //Encash/Lapse columns rem
                    dbLeaveAdj.InitialFixedNewBalance = (dbLeaveAdj.BeginningBalance + dbLeaveAdj.Accured) - Convert.ToDecimal(dbLeaveAdj.Taken);
                    dbLeaveAdj.NewBalance = dbLeaveAdj.InitialFixedNewBalance + dbLeaveAdj.Adjusted.Value;
                    // reset these columsn values also
                    dbLeaveAdj.Encased = 0;
                    dbLeaveAdj.Lapsed = 0;
                    dbLeaveAdj.WorkDayDaysCount = 0;

                }
            }
            PayrollDataContext.SubmitChanges();
            return true;
        }

        public static string GetEmployeeName(int employeeId)
        {
            return
                (
                from e in PayrollDataContext.EEmployees
                where e.EmployeeId == employeeId
                select e.Name
                ).FirstOrDefault();
        }

        //public static string GetEmployeeEmail(int employeeId)
        //{
        //    return
        //        (
        //        from e in PayrollDataContext.EAddresses
        //        where e.EmployeeId == employeeId
        //        select e.CIEmail
        //        ).FirstOrDefault();
        //}


        public static int? GetAnyLoanIDForEmp(int empId)
        {
            PDeduction loan =
                (
                from e in PayrollDataContext.PEmployeeDeductions
                join d in PayrollDataContext.PDeductions on e.DeductionId equals d.DeductionId
                where (d.Calculation == DeductionCalculation.LOAN && (d.IsEMI == false))
                && e.EmployeeId == empId
                select d
                ).FirstOrDefault();

            if (loan == null)
                return null;
            return loan.DeductionId;
        }
        public static int? GetAnyAdvanceIDForEmp(int empId)
        {
            PDeduction loan =
                (
                from e in PayrollDataContext.PEmployeeDeductions
                join d in PayrollDataContext.PDeductions on e.DeductionId equals d.DeductionId
                where (d.Calculation == DeductionCalculation.Advance)
                && e.EmployeeId == empId
                select d
                ).FirstOrDefault();

            if (loan == null)
                return null;
            return loan.DeductionId;
        }
        public static bool SaveImportedOpeningLeave(List<EEmployee> list, ref int importedCount)
        {

            importedCount = 0;
            //bool leaveExists = false;

            if (list.Count <= 0)
                return false;


            bool isAttendanceSaved = LeaveAttendanceManager.IsAttendanceSavedForEmployee(list[0].EmployeeId);
            int firstPayrollPeriodId = 0;

            if (isAttendanceSaved)
                firstPayrollPeriodId = CommonManager.GetFirstPayrollPeriod(SessionManager.CurrentCompanyId).PayrollPeriodId;

            PayrollDataContext.ChangeUserActivities.InsertOnSubmit(
                  GetChangeUserActivityLog("Opening Leave Imported."));

            foreach (EEmployee entity in list)
            {
                //if (entity.EmployeeId == 0)
                //    PayrollDataContext.EEmployees.InsertOnSubmit(entity);
                //else
                {
                    EEmployee dbEntity = GetEmployeeById(entity.EmployeeId);
                    // Process to update information from Import
                    if (dbEntity != null)
                    {
                        importedCount += 1;

                        foreach (LEmployeeLeave inputLeave in entity.LEmployeeLeaves)
                        {
                            //leaveExists = false;
                            //iterate each item

                            LEmployeeLeave dbleave = dbEntity.LEmployeeLeaves.FirstOrDefault
                                (x => x.EmployeeId == entity.EmployeeId && x.LeaveTypeId == inputLeave.LeaveTypeId);

                            //foreach (LEmployeeLeave dbleave in dbEntity.LEmployeeLeaves)

                            //if (dbleave.LeaveTypeId == inputLeave.LeaveTypeId)
                            if (dbleave != null)
                            {
                                // Place log before changing
                                if (dbleave.BeginningBalance != inputLeave.BeginningBalance || dbleave.CurrentYearBalance != inputLeave.CurrentYearBalance
                                    || dbleave.OpeningTaken != inputLeave.OpeningTaken)
                                    PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(dbleave.EmployeeId, (byte)MonetoryChangeLogTypeEnum.LeaveOpening
                                        , dbleave.LLeaveType.Title, dbleave.BeginningBalance + "_" + dbleave.CurrentYearBalance + "_" + dbleave.OpeningTaken,
                                        inputLeave.BeginningBalance + "_" + inputLeave.CurrentYearBalance + "_" + inputLeave.OpeningTaken, LogActionEnum.Update));

                                dbleave.BeginningBalance = inputLeave.BeginningBalance;
                                dbleave.CurrentYearBalance = inputLeave.CurrentYearBalance;
                                dbleave.OpeningTaken = inputLeave.OpeningTaken;
                                dbleave.IsOpeningBalanceSaved = true;

                                LeaveAdjustment adj = PayrollDataContext.LeaveAdjustments.SingleOrDefault(x => x.EmployeeId == dbEntity.EmployeeId
                                   && x.LeaveTypeId == inputLeave.LeaveTypeId && x.PayrollPeriodId == firstPayrollPeriodId);

                                if (adj != null)
                                {
                                    adj.BeginningBalance = (decimal)(inputLeave.BeginningBalance +
                                        (inputLeave.CurrentYearBalance == null ? ((double)0) : inputLeave.CurrentYearBalance.Value) -
                                        (inputLeave.OpeningTaken == null ? ((double)0) : inputLeave.OpeningTaken.Value));
                                }
                            }
                            else
                            {
                                LEmployeeLeave newDBLeave = new LEmployeeLeave();
                                CopyObject<LEmployeeLeave>(inputLeave, ref newDBLeave, "");
                                dbEntity.LEmployeeLeaves.Add(newDBLeave);
                            }





                        }
                    }
                }
            }

            PayrollDataContext.SubmitChanges();
            return true;
        }

        public static void SetStatusToDate(EntitySet<ECurrentStatus> statues)
        {

            for (int i = 0; i < statues.Count; i++)
            {
                if ((i + 1) < statues.Count)
                {
                    CustomDate date = CustomDate.GetCustomDateFromString(statues[i + 1].FromDate, IsEnglish);
                    statues[i].ToDateValue = date.DecrementByOneDay().ToString();
                }
                else
                    statues[i].ToDateValue = " - ";
            }
        }

        public static EFundDetail GetEmployeeFundDetail(int employeeId)
        {
            return PayrollDataContext.EFundDetails.SingleOrDefault(e => e.EmployeeId == employeeId);
        }

        /// <summary>
        /// Saves all employee information
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public int Save(EEmployee entity, LeaveProjectEmployee empLeaveProject)
        {
            entity.Created = entity.Modified = GetCurrentDateAndTime();

            // set eng date
            entity.EHumanResources[0].SalaryCalculateFromEng
                = GetEngDate(entity.EHumanResources[0].SalaryCalculateFrom, null);


            if (entity.EFundDetails[0].NoCITContribution.Value)
            {
                entity.EFundDetails[0].NoCITFromEng =
                    GetEngDate("1/" + entity.EFundDetails[0].NoCITFromMonth + "/" + entity.EFundDetails[0].NoCITFromYear, null);

                entity.EFundDetails[0].NoCITToEng =
                    GetEngDate("1/" + entity.EFundDetails[0].NoCITToMonth + "/" + entity.EFundDetails[0].NoCITToYear,
                               null);

            }

            if (entity.GradeId != null)
            {
                //process for grade
                EGradeHistory grade = new EGradeHistory();
                grade.GradeId = entity.GradeId;
                grade.FromDate = entity.ECurrentStatus[0].FromDate;
                grade.FromDateEng = entity.ECurrentStatus[0].FromDateEng;
                entity.EGradeHistories.Add(grade);
            }

            if (entity.WorkShiftId != null)
            {
                EWorkShiftHistory workshift = new EWorkShiftHistory();
                workshift.WorkShiftId = entity.WorkShiftId;
                workshift.FromDate = entity.ECurrentStatus[0].FromDate;
                workshift.FromDateEng = entity.ECurrentStatus[0].FromDateEng;
                entity.EWorkShiftHistories.Add(workshift);
            }

            if (entity.EHumanResources[0].LocationId != null)
            {
                ELocationHistory location = new ELocationHistory();
                location.LocationId = entity.EHumanResources[0].LocationId;
                location.FromDate = entity.ECurrentStatus[0].FromDate;
                location.FromDateEng = entity.ECurrentStatus[0].FromDateEng;
                entity.ELocationHistories.Add(location);
            }

            SetConnectionPwd(PayrollDataContext.Connection);
            PayrollDataContext.Transaction = PayrollDataContext.Connection.BeginTransaction();

            try
            {
                PayrollDataContext.EEmployees.InsertOnSubmit(entity);




                PayrollDataContext.SubmitChanges();


                PayrollDataContext.ChangeMonetories.InsertOnSubmit(
                    GetMonetoryChangeLog(entity.EmployeeId, (byte)MonetoryChangeLogTypeEnum.EmployeeAdded
                    , "New Employee", "", entity.Name + " added.", LogActionEnum.Add));

                //add Basic salary/income for the employee          
                PayManager mgr = new PayManager();
                PIncome basicIncome = mgr.GetBasicIncome(entity.CompanyId.Value);
                PEmployeeIncome employeeIncome = new PEmployeeIncome();
                employeeIncome.IncomeId = basicIncome.IncomeId;
                employeeIncome.EmployeeId = entity.EmployeeId;
                employeeIncome.IsEnabled = true;
                PayrollDataContext.PEmployeeIncomes.InsertOnSubmit(employeeIncome);

                //Leave Project Employee
                empLeaveProject.EmployeeId = entity.EmployeeId;
                LeaveRequestManager.SaveUpdateEmployeeProject(empLeaveProject);

                PayrollDataContext.SubmitChanges();
                PayrollDataContext.Transaction.Commit();
            }
            catch (Exception exp)
            {
                Log.log("Error while saving new emp :" + entity.Name, exp);
                PayrollDataContext.Transaction.Rollback();
                return 0;
            }
            finally
            {

                //if (PayrollDataContext.Connection != null)
                //{
                //    PayrollDataContext.Connection.Close();
                //}
            }
            //return true;


            //PayrollDataContext.EEmployees.InsertOnSubmit(entity);
            //PayrollDataContext.SubmitChanges();
            return entity.EmployeeId;
        }

        public static bool IsEmployeeSingle(int employeeId)
        {
            return PayrollDataContext.IsEmployeeSingle(employeeId).Value;
        }

        public bool Delete(int employeeId)
        {
            //catch the excpetion as due to ref in some table table may give error  
            try
            {

                return PayrollDataContext.DeleteEmployee(employeeId) == 1;
            }
            catch (Exception exp)
            {
                Log.log("Employee delete error for id : ", employeeId + "<br>" + exp.Message);
                return false;
            }
        }

        public void SaveEmployeeHRDetails(EHumanResource entity)
        {
            PayrollDataContext.EHumanResources.InsertOnSubmit(entity);
            PayrollDataContext.SubmitChanges();
        }
        public void SaveEmployeeFundDetails(EFundDetail entity)
        {
            PayrollDataContext.EFundDetails.InsertOnSubmit(entity);
            PayrollDataContext.SubmitChanges();
        }

        public static List<EEmployee> GetEmployeeListByCompany(int companyId)
        {
            return PayrollDataContext.EEmployees.Where(c => c.CompanyId == companyId)
                .OrderBy(c => c.Name).ToList();
        }

        public static string GetEmployeeEmail(int empId)
        {
            return (
                from a in PayrollDataContext.EAddresses
                where a.EmployeeId == empId
                select a.CIEmail
                ).FirstOrDefault();
        }


        public static string GetEmployeeEmailForLateEntry(int empId)
        {
            EAddress _EAddresses = BLL.BaseBiz.PayrollDataContext.EAddresses.SingleOrDefault(c => c.EmployeeId == empId);
            if (!string.IsNullOrEmpty(_EAddresses.CIEmail))
                return _EAddresses.CIEmail;
            else
                return "";
        }

        public static EEmployee GetEmployeeById(int employeeId)
        {
            //List<int> customRoleDepartmentIDList = SessionManager.CustomRoleDeparmentList;

            return (
                from e in PayrollDataContext.EEmployees
                where e.EmployeeId == employeeId
                //&& (customRoleDepartmentIDList.Count==0 || customRoleDepartmentIDList.Contains(e.EmployeeId))
                select e
                ).FirstOrDefault();

            //return PayrollDataContext.EEmployees
            //    .Where(x => customRoleDepartmentIDList.Count == 0 || customRoleDepartmentIDList.Contains(x.DepartmentId.Value))
            //    .SingleOrDefault(c => c.EmployeeId == employeeId);
        }

        public static HHumanResource GethHumanResourceById(int employeeId)
        {
            //return (
            //    from h in PayrollDataContext.HHumanResources
            //    where h.EmployeeId == employeeId
            //    select h
            //    ).FirstOrDefault();

            HHumanResource retAdd = new HHumanResource();
            if (PayrollDataContext.HHumanResources.Any(x => x.EmployeeId == employeeId))
                retAdd = PayrollDataContext.HHumanResources.Where(x => x.EmployeeId == employeeId).First();
            return retAdd;
        }



        public static EEmployee GetEmployeeById(int employeeId, PayrollDataContext PayrollDataContext)
        {
            return PayrollDataContext.EEmployees.SingleOrDefault(c => c.EmployeeId == employeeId);
        }



        //public static EEmployee GetEmployeeByIdForEmployeePage(int employeeId)
        //{
        //    BLL.BaseBiz.Dispose();




        //    DataLoadOptions options = new DataLoadOptions();
        //    options.LoadWith<EEmployee>(ii => ii.EAddresses);
        //    options.LoadWith<EEmployee>(ii => ii.EFundDetails);
        //    options.LoadWith<EEmployee>(ii => ii.EHumanResources);
        //    options.LoadWith<EEmployee>(ii => ii.PPays);
        //    PayrollDataContext.LoadOptions = options;

        //    List<int> customRoleDepartmentIDList = SessionManager.CustomRoleDeparmentList;

        //    if (customRoleDepartmentIDList.Count != 0)
        //    {
        //        return PayrollDataContext.EEmployees

        //          .Where(x => customRoleDepartmentIDList.Contains(x.DepartmentId.Value))
        //         .SingleOrDefault(c => c.EmployeeId == employeeId);
        //        PayrollDataContext.LoadOptions = oldoptions;
        //    }
        //    else
        //    {
        //        return PayrollDataContext.EEmployees


        //        .SingleOrDefault(c => c.EmployeeId == employeeId);
        //    }

        //}

        public List<GetEmployeesByBranchResult> GetEmployeesByBranch(int branchId, int depId)
        {
            return PayrollDataContext.GetEmployeesByBranch(branchId, SessionManager.CurrentCompanyId, depId).ToList();
        }


        public List<GetEmployeesByBranchForShiftPlanningResult> GetEmployeesByBranchForShiftPlanning(int branchId, int depId, int employeeId = -1, string empName = "")
        {
            return PayrollDataContext.GetEmployeesByBranchForShiftPlanning(branchId, SessionManager.CurrentCompanyId, depId, empName, employeeId).ToList();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="entity"></param>
        /// <returns>true if updated,false if not, as the record may have been deleted also</returns>
        //public bool Update(EEmployee entity, LeaveProjectEmployee empLeaveProject, ref string retirmentChangeMessage)
        //{
        //    bool isFirstStatusFromDateChanged = false;


        //    EEmployee dbEntity = PayrollDataContext.EEmployees.SingleOrDefault(c => c.EmployeeId == entity.EmployeeId);
        //    //may be deleted
        //    if (dbEntity == null)
        //        return false;

        //    #region "Change Logs"
        //    bool isResigned, dbIsResigned, isRetired, dbIsRetired;
        //    isResigned = entity.IsResigned.Value;
        //    dbIsResigned = dbEntity.IsResigned.Value;
        //    isRetired = entity.IsRetired.Value;
        //    dbIsRetired = dbEntity.IsRetired.Value;
        //    #endregion

        //    dbEntity.EditSequence = entity.EditSequence;
        //    //check for grade set in update then create in history table also
        //    if (dbEntity.GradeId == null && entity.GradeId != null)
        //    {
        //        //process for grade
        //        EGradeHistory grade = new EGradeHistory();
        //        grade.GradeId = entity.GradeId;
        //        grade.FromDate = entity.ECurrentStatus[0].FromDate;
        //        grade.FromDateEng = entity.ECurrentStatus[0].FromDateEng;
        //        dbEntity.EGradeHistories.Add(grade);
        //    }
        //    if (dbEntity.WorkShiftId == null && entity.WorkShiftId != null)
        //    {
        //        EWorkShiftHistory workshift = new EWorkShiftHistory();
        //        workshift.WorkShiftId = entity.WorkShiftId;
        //        workshift.FromDate = entity.ECurrentStatus[0].FromDate;
        //        workshift.FromDateEng = entity.ECurrentStatus[0].FromDateEng;
        //        dbEntity.EWorkShiftHistories.Add(workshift);
        //    }

        //    if (dbEntity.EHumanResources[0].LocationId == null && entity.EHumanResources[0].LocationId != null)
        //    {
        //        ELocationHistory location = new ELocationHistory();
        //        location.LocationId = entity.EHumanResources[0].LocationId;
        //        location.FromDate = entity.ECurrentStatus[0].FromDate;
        //        location.FromDateEng = entity.ECurrentStatus[0].FromDateEng;
        //        dbEntity.ELocationHistories.Add(location);
        //    }

        //    SetEEmployee(dbEntity, entity);
        //    SetEAddress(dbEntity.EAddresses[0], entity.EAddresses[0], true);
        //    SetEFund(dbEntity.EFundDetails[0], entity.EFundDetails[0]);
        //    SetEHumanResource(dbEntity.EHumanResources[0], entity.EHumanResources[0], isResigned, dbIsResigned, isRetired, dbIsRetired, ref retirmentChangeMessage);

        //    PayrollPeriod period = CommonManager.GetLastPayrollPeriod();
        //    if (period != null)
        //    {
        //        if (AttendanceManager.IsAttendanceSavedForEmployee(entity.EmployeeId, period.PayrollPeriodId) == false)
        //        {
        //            retirmentChangeMessage = "";
        //        }
        //    }

        //    SetPay(dbEntity.PPays[0], entity.PPays[0]);


        //    dbEntity.ECurrentStatus[0].CurrentStatus = entity.ECurrentStatus[0].CurrentStatus;
        //    //check if first emp status from date is changed or not
        //    if (!dbEntity.ECurrentStatus[0].FromDate.Equals(entity.ECurrentStatus[0].FromDate))
        //    {
        //        isFirstStatusFromDateChanged = true;
        //        // change from date of first branch transfer is exists
        //        BranchDepartmentHistory firstBranch = PayrollDataContext.BranchDepartmentHistories
        //            .Where(x => x.EmployeeId == entity.EmployeeId).FirstOrDefault();
        //        if (firstBranch != null)
        //        {
        //            firstBranch.FromDate = entity.ECurrentStatus[0].FromDate;
        //            firstBranch.FromDateEng = entity.ECurrentStatus[0].FromDateEng;
        //        }

        //        // change from date of first Designation if exists
        //        DesignationHistory firstDesignation = PayrollDataContext.DesignationHistories
        //            .Where(x => x.EmployeeId == entity.EmployeeId).FirstOrDefault();
        //        if (firstDesignation != null)
        //        {
        //            firstDesignation.FromDate = entity.ECurrentStatus[0].FromDate;
        //            firstDesignation.FromDateEng = entity.ECurrentStatus[0].FromDateEng;
        //        }
        //    }

        //    dbEntity.ECurrentStatus[0].FromDate = entity.ECurrentStatus[0].FromDate;
        //    dbEntity.ECurrentStatus[0].FromDateEng = entity.ECurrentStatus[0].FromDateEng;
        //    dbEntity.ECurrentStatus[0].ToDate = entity.ECurrentStatus[0].ToDate;
        //    dbEntity.ECurrentStatus[0].ToDateEng = entity.ECurrentStatus[0].ToDateEng;
        //    dbEntity.ECurrentStatus[0].DefineToDate = entity.ECurrentStatus[0].DefineToDate;

        //    //dbEntity.ECurrentStatus[0].ToDate = entity.ECurrentStatus[0].ToDate;
        //    //dbEntity.ECurrentStatus[0].ToDateEng = entity.ECurrentStatus[0].ToDateEng;

        //    //PayrollDataContext.SubmitChanges();
        //    //return true;

        //    //if first status from date changed then change the first from date of grade,loc &
        //    //workshift if it has
        //    if (isFirstStatusFromDateChanged)
        //    {
        //        if (dbEntity.EGradeHistories.Count > 0)
        //        {
        //            dbEntity.EGradeHistories[0].FromDate = dbEntity.ECurrentStatus[0].FromDate;
        //            dbEntity.EGradeHistories[0].FromDateEng = dbEntity.ECurrentStatus[0].FromDateEng;
        //        }
        //        if (dbEntity.EWorkShiftHistories.Count > 0)
        //        {
        //            dbEntity.EWorkShiftHistories[0].FromDate = dbEntity.ECurrentStatus[0].FromDate;
        //            dbEntity.EWorkShiftHistories[0].FromDateEng = dbEntity.ECurrentStatus[0].FromDateEng;
        //        }
        //        if (dbEntity.ELocationHistories.Count > 0)
        //        {
        //            dbEntity.ELocationHistories[0].FromDate = dbEntity.ECurrentStatus[0].FromDate;
        //            dbEntity.ELocationHistories[0].FromDateEng = dbEntity.ECurrentStatus[0].FromDateEng;
        //        }
        //    }


        //    //Leave Project Employee
        //    if (empLeaveProject != null)
        //    {
        //        empLeaveProject.EmployeeId = entity.EmployeeId;
        //        LeaveRequestManager.SaveUpdateEmployeeProject(empLeaveProject);
        //    }

        //    dbEntity.Modified = GetCurrentDateAndTime();
        //    UpdateChangeSet();
        //    return true;
        //}

        public static string GetGenderValue(int? gender)
        {
            if (gender == 0)
                return "Female";
            else if (gender == 1)
                return "Male";
            return "Third Gender";
        }

        public void SetEEmployee(EEmployee dbEntity, EEmployee entity)
        {
            dbEntity.Title = entity.Title;
            dbEntity.Name = entity.Name;
            dbEntity.FatherName = entity.FatherName;
            dbEntity.MotherName = entity.MotherName;

            #region "Change Logs"
            if (dbEntity.Gender != entity.Gender)
                PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(entity.EmployeeId, (byte)MonetoryChangeLogTypeEnum.Gender
                    , "Gender", GetGenderValue(dbEntity.Gender), GetGenderValue(entity.Gender), LogActionEnum.Update));

            if (dbEntity.MaritalStatus != entity.MaritalStatus)
                PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(entity.EmployeeId, (byte)MonetoryChangeLogTypeEnum.TaxStatus
                    , "Marital Status", dbEntity.MaritalStatus, entity.MaritalStatus, LogActionEnum.Update));

            if (dbEntity.HasCoupleTaxStatus != entity.HasCoupleTaxStatus)
                PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(entity.EmployeeId, (byte)MonetoryChangeLogTypeEnum.TaxStatus
                    , "Couple Tax", dbEntity.HasCoupleTaxStatus, entity.HasCoupleTaxStatus, LogActionEnum.Update));

            if (dbEntity.HasDependent != entity.HasDependent)
                PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(entity.EmployeeId, (byte)MonetoryChangeLogTypeEnum.TaxStatus
                    , "Dependent", dbEntity.HasDependent, entity.HasDependent, LogActionEnum.Update));

            if (dbEntity.IsHandicapped != entity.IsHandicapped)
                PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(entity.EmployeeId, (byte)MonetoryChangeLogTypeEnum.TaxStatus
                    , "Handicapped", dbEntity.IsHandicapped, entity.IsHandicapped, LogActionEnum.Update));



            if (dbEntity.IsLocalEmployee != entity.IsLocalEmployee)
                PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(entity.EmployeeId, (byte)MonetoryChangeLogTypeEnum.Information
                    , "Local Employee", dbEntity.IsLocalEmployee, entity.IsLocalEmployee, LogActionEnum.Update));

            if (dbEntity.IsNonResident != entity.IsNonResident)
                PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(entity.EmployeeId, (byte)MonetoryChangeLogTypeEnum.Information
                    , "Non Resident", dbEntity.IsNonResident, entity.IsNonResident, LogActionEnum.Update));


            //if (dbEntity.BranchId != entity.BranchId)
            //    PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(entity.EmployeeId, (byte)MonetoryChangeLogTypeEnum.Branch
            //        , "Branch", new BranchManager().GetById(dbEntity.BranchId.Value).Name, new BranchManager().GetById(entity.BranchId.Value).Name, LogActionEnum.Update));

            //if (dbEntity.DepartmentId != entity.DepartmentId)
            //    PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(entity.EmployeeId, (byte)MonetoryChangeLogTypeEnum.Branch
            //        , "Department", new DepartmentManager().GetById(dbEntity.DepartmentId.Value).Name, new DepartmentManager().GetById(entity.DepartmentId.Value).Name, LogActionEnum.Update));

            if (dbEntity.DesignationId != entity.DesignationId)
                PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(entity.EmployeeId, (byte)MonetoryChangeLogTypeEnum.Branch
                    , "Designation", new CommonManager().GetDesignationById(dbEntity.DesignationId.Value).Name, new CommonManager().GetDesignationById(entity.DesignationId.Value).Name, LogActionEnum.Update));

            #endregion

            dbEntity.Gender = entity.Gender;
            dbEntity.MaritalStatus = entity.MaritalStatus;
            dbEntity.HasCoupleTaxStatus = entity.HasCoupleTaxStatus;
            dbEntity.HasDependent = entity.HasDependent;
            dbEntity.IsHandicapped = entity.IsHandicapped;

            dbEntity.IsEnglishDOB = entity.IsEnglishDOB;
            dbEntity.DateOfBirth = entity.DateOfBirth;
            try
            {
                if (!string.IsNullOrEmpty(dbEntity.DateOfBirth))
                    dbEntity.DateOfBirthEng = GetEngDate(dbEntity.DateOfBirth, dbEntity.IsEnglishDOB.Value);
                else
                    dbEntity.DateOfBirthEng = null;
            }
            catch
            {
            }

            //Classification
            dbEntity.DesignationId = entity.DesignationId;
            //dbEntity.TierId = entity.TierId;
            dbEntity.GradeId = entity.GradeId;
            dbEntity.DepartmentId = entity.DepartmentId;
            dbEntity.SubDepartmentId = entity.SubDepartmentId;
            dbEntity.BranchId = entity.BranchId;
            dbEntity.WorkShiftId = entity.WorkShiftId;
            dbEntity.CostCodeId = entity.CostCodeId;
            dbEntity.IsLocalEmployee = entity.IsLocalEmployee;
            dbEntity.IsNonResident = entity.IsNonResident;

            //dbEntity.IsInactive = entity.IsInactive;
            dbEntity.IsResigned = entity.IsResigned;
            dbEntity.IsRetired = entity.IsRetired;
            dbEntity.NoTax = entity.NoTax;
            //dbEntity.ResignedEngDate = entity.ResignedEngDate;
            //dbEntity.RetiredEngDate = entity.RetiredEngDate;

        }

        public static void SetRetirement(int employeeId, string date, DateTime dateEng, bool isRetired, bool isResigned, int? retirementType)
        {
            if (retirementType == -1)
                retirementType = null;

            EEmployee dbEntity = new EmployeeManager().GetById(employeeId);

            bool dbIsResigned = dbEntity.IsResigned == null ? false : dbEntity.IsResigned.Value;
            bool dbIsRetired = dbEntity.IsRetired == null ? false : dbEntity.IsRetired.Value;
            PayrollPeriod period = CommonManager.GetLastPayrollPeriod();

            dbEntity.IsRetired = isRetired;
            dbEntity.EHumanResources[0].DateOfRetirement = date;
            dbEntity.EHumanResources[0].DateOfRetirementEng = dateEng;

            dbEntity.IsResigned = isResigned;
            dbEntity.EHumanResources[0].DateOfRetirement = date;
            dbEntity.EHumanResources[0].DateOfRetirementEng = dateEng;
            dbEntity.EHumanResources[0].RetirementType = retirementType;


            string messageForLeaveEncashmentWhenRetirmentChanges = "";

            //if reg is set
            if (dbIsResigned == false && isResigned == true)
            {
                messageForLeaveEncashmentWhenRetirmentChanges = "Resignation has been set, please load and save the attendance page for this employee for leave encashment calculation.";
                PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(dbEntity.EmployeeId, (byte)MonetoryChangeLogTypeEnum.Information
                    , "Resignation", "-", date, LogActionEnum.Add));

            }
            //if reg set is changed
            else if (dbIsResigned == true && isResigned == true && dbEntity.EHumanResources[0].DateOfResignation != date)
            {
                if (dbEntity.EHumanResources[0].DateOfResignation != date)
                    PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(dbEntity.EmployeeId, (byte)MonetoryChangeLogTypeEnum.Information
                        , "Resignation", dbEntity.EHumanResources[0].DateOfResignation, date, LogActionEnum.Update));
            }
            //if ret date is removed
            else if (dbIsResigned == true && isResigned == false)
            {
                messageForLeaveEncashmentWhenRetirmentChanges = "Resignation has been removed, please load and save the attendance page for this employee for leave encashment removal.";
                PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(dbEntity.EmployeeId, (byte)MonetoryChangeLogTypeEnum.Information
                    , "Resignation", dbEntity.EHumanResources[0].DateOfResignation, "-", LogActionEnum.Delete));
            }


            //if ret is set
            if (dbIsRetired == false && isRetired == true)
            {
                messageForLeaveEncashmentWhenRetirmentChanges = "Retirement has been set, please load and save the attendance page for this employee for leave encashment calculation.";
                PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(dbEntity.EmployeeId, (byte)MonetoryChangeLogTypeEnum.Information
                    , "Retirement", "-", date, LogActionEnum.Add));


            }
            //if ret set is changed
            else if (dbIsRetired == true && isRetired == true && dbEntity.EHumanResources[0].DateOfResignation != date)
            {
                if (dbEntity.EHumanResources[0].DateOfResignation != date)
                    PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(dbEntity.EmployeeId, (byte)MonetoryChangeLogTypeEnum.Information
                        , "Retirement", dbEntity.EHumanResources[0].DateOfRetirement, date, LogActionEnum.Update));
            }
            //if ret date is removed
            else if (dbIsRetired == true && isRetired == false)
            {
                messageForLeaveEncashmentWhenRetirmentChanges = "Retirement has been removed, please load and save the attendance page for this employee for leave encashment removal.";
                PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(dbEntity.EmployeeId, (byte)MonetoryChangeLogTypeEnum.Information
                    , "Retirement", dbEntity.EHumanResources[0].DateOfRetirement, "-", LogActionEnum.Delete));
            }

            ResaveAttendanceOfEmployee(employeeId, period);



            //save in service event also
            EmployeeServiceHistory history = new EmployeeServiceHistory();
            history.CreatedBy = SessionManager.CurrentLoggedInUserID;
            history.CreatedOn = GetCurrentDateAndTime();
            history.Date = date;
            history.DateEng = dateEng;
            history.EmployeeId = employeeId;
            history.EventID = (short)retirementType;
            GetEmployeeCurrentBranchDepartmentResult branchDep =
                PayrollDataContext.GetEmployeeCurrentBranchDepartment(history.DateEng, history.EmployeeId)
                .FirstOrDefault(); ;

            history.BranchId = branchDep.BranchId;
            history.DepartmentId = branchDep.DepartmentId;
            history.StatusId = PayrollDataContext.GetEmployeeStatusForDate(history.DateEng, history.EmployeeId).Value;
            history.DesignationId = PayrollDataContext.GetEmployeeCurrentPositionForDate(history.DateEng, history.EmployeeId).Value;
            history.LevelId = CommonManager.GetLevelIDForDesignation(history.DesignationId.Value);
            //history.LetterNo =  dbEmployee.EHumanResources[0].AppointmentLetterNo;
            //history.LetterDate = dbEmployee.EHumanResources[0].AppointmentLetterDate;
            //history.LetterDateEng = dbEmployee.EHumanResources[0].AppointmentLetterDateEng;
            //history.LevelId = CommonManager.GetLevelIDForDesignation(history.DesignationId.Value);
            history.ModifiedBy = SessionManager.CurrentLoggedInUserID;
            history.ModifiedOn = history.CreatedOn;

            history.Notes = "";
            history.PayrollPeriodId = CommonManager.GetLastPayrollPeriod().PayrollPeriodId;
            PayrollDataContext.EmployeeServiceHistories.InsertOnSubmit(history);

            PayrollDataContext.SubmitChanges();

            dbEntity.EHumanResources[0].RetirementEventSourceRef_ID = history.EventSourceID;

            PayrollDataContext.SubmitChanges();
        }





        public static void ResaveAttendanceOfEmployee(int employeeId, PayrollPeriod period)
        {
            AttedenceDetail detail =
                (
                    from a in PayrollDataContext.Attendences
                    join ad in PayrollDataContext.AttedenceDetails on a.AttendenceId equals ad.AttendenceId
                    where a.EmployeeId == employeeId && a.PayrollPeriodId == period.PayrollPeriodId
                        && ad.DayValue == "P"
                    select ad
                ).FirstOrDefault();

            List<AttendanceImportEmployee> atteList = new List<AttendanceImportEmployee>();
            if (detail != null)
            {
                AttendanceImportEmployee atte = new AttendanceImportEmployee();
                atte.EmployeeId = employeeId;
                AttendanceImportEmployeeLeave leave = new AttendanceImportEmployeeLeave();
                leave.NoLeave = true;
                leave.TakenDate = detail.DateEng.Value;

                atte.leaves.Add(leave);

                atteList.Add(atte);
            }

            string msg = "";
            int count = 0;
            EmployeeManager.SaveImportedAttendance(atteList, period.PayrollPeriodId, ref msg, ref count, employeeId);

            // resave attendance
            LeaveAttendanceManager.CallToResaveAtte(period.PayrollPeriodId, employeeId.ToString(), null);
        }

        public void SetEHumanResource(EHumanResource dbEntity, EHumanResource entity, bool isResigned, bool dbIsResigned, bool isRetired, bool dbIsRetired, ref string messageForLeaveEncashmentWhenRetirmentChanges)
        {

            if (dbEntity.SalaryCalculateFrom != entity.SalaryCalculateFrom)
                dbEntity.SalaryCalculateFromEng = GetEngDate(entity.SalaryCalculateFrom, null);

            dbEntity.SalaryCalculateFrom = entity.SalaryCalculateFrom;

            #region "Change Logs"


            if (dbEntity.EEmployee.IsResigned.Value)
            {
                dbEntity.DateOfResignationEng = GetEngDate(entity.DateOfResignation, null);
                dbEntity.DateOfResignation = entity.DateOfResignation;
            }


            if (dbEntity.EEmployee.IsRetired.Value)
            {
                dbEntity.DateOfRetirementEng = GetEngDate(entity.DateOfRetirement, null);
                dbEntity.DateOfRetirement = entity.DateOfRetirement;
            }


            //if reg is set
            if (dbIsResigned == false && isResigned == true)
            {
                messageForLeaveEncashmentWhenRetirmentChanges = "Resignation has been set, please load and save the attendance page for this employee for leave encashment calculation.";
                PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(dbEntity.EmployeeId, (byte)MonetoryChangeLogTypeEnum.Information
                    , "Resignation", "-", entity.DateOfResignation, LogActionEnum.Add));


                PayrollPeriod period = PayrollDataContext.PayrollPeriods.FirstOrDefault(x => dbEntity.DateOfResignationEng >= x.StartDateEng && dbEntity.DateOfResignationEng <= x.EndDateEng);
                if (period != null)
                {
                    CCalculationEmployee calcEmployee
                        =
                        (
                            from ce in PayrollDataContext.CCalculationEmployees
                            join c in PayrollDataContext.CCalculations on ce.CalculationId equals c.CalculationId
                            where c.PayrollPeriodId == period.PayrollPeriodId
                            && ce.EmployeeId == dbEntity.EmployeeId
                            select ce
                        ).FirstOrDefault();

                    if (calcEmployee != null)
                        calcEmployee.IsRetiredOrResigned = true;

                }


            }
            //if reg set is changed
            else if (dbIsResigned == true && isResigned == true && dbEntity.DateOfResignation != entity.DateOfResignation)
            {
                if (dbEntity.DateOfResignation != entity.DateOfResignation)
                    PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(dbEntity.EmployeeId, (byte)MonetoryChangeLogTypeEnum.Information
                        , "Resignation", dbEntity.DateOfResignation, entity.DateOfResignation, LogActionEnum.Update));
            }
            //if ret date is removed
            else if (dbIsResigned == true && isResigned == false)
            {
                messageForLeaveEncashmentWhenRetirmentChanges = "Resignation has been removed, please load and save the attendance page for this employee for leave encashment removal.";
                PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(dbEntity.EmployeeId, (byte)MonetoryChangeLogTypeEnum.Information
                    , "Resignation", dbEntity.DateOfResignation, "-", LogActionEnum.Delete));
            }


            //if ret is set
            if (dbIsRetired == false && isRetired == true)
            {
                messageForLeaveEncashmentWhenRetirmentChanges = "Retirement has been set, please load and save the attendance page for this employee for leave encashment calculation.";
                PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(dbEntity.EmployeeId, (byte)MonetoryChangeLogTypeEnum.Information
                    , "Retirement", "-", entity.DateOfRetirement, LogActionEnum.Add));

                PayrollPeriod period = PayrollDataContext.PayrollPeriods.FirstOrDefault(x => dbEntity.DateOfRetirementEng >= x.StartDateEng && dbEntity.DateOfRetirementEng <= x.EndDateEng);
                if (period != null)
                {
                    CCalculationEmployee calcEmployee
                        =
                        (
                            from ce in PayrollDataContext.CCalculationEmployees
                            join c in PayrollDataContext.CCalculations on ce.CalculationId equals c.CalculationId
                            where c.PayrollPeriodId == period.PayrollPeriodId
                            && ce.EmployeeId == dbEntity.EmployeeId
                            select ce
                        ).FirstOrDefault();

                    if (calcEmployee != null)
                        calcEmployee.IsRetiredOrResigned = true;

                }
            }
            //if ret set is changed
            else if (dbIsRetired == true && isRetired == true && dbEntity.DateOfResignation != entity.DateOfResignation)
            {
                if (dbEntity.DateOfResignation != entity.DateOfResignation)
                    PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(dbEntity.EmployeeId, (byte)MonetoryChangeLogTypeEnum.Information
                        , "Retirement", dbEntity.DateOfRetirement, entity.DateOfRetirement, LogActionEnum.Update));
            }
            //if ret date is removed
            else if (dbIsRetired == true && isRetired == false)
            {
                messageForLeaveEncashmentWhenRetirmentChanges = "Retirement has been removed, please load and save the attendance page for this employee for leave encashment removal.";
                PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(dbEntity.EmployeeId, (byte)MonetoryChangeLogTypeEnum.Information
                    , "Retirement", dbEntity.DateOfRetirement, "-", LogActionEnum.Delete));
            }

            #endregion


            dbEntity.IdCardNo = entity.IdCardNo;
            dbEntity.JobNature = entity.JobNature;
            dbEntity.LocationId = entity.LocationId;
            dbEntity.ProgrammeId = entity.ProgrammeId;
            dbEntity.NoPFOnRetirementResignation = entity.NoPFOnRetirementResignation;
            dbEntity.VoucherDepartmentID = entity.VoucherDepartmentID;
        }

        public string GetCITValue(EFundDetail fund)
        {
            if (fund.HasCIT != null && fund.HasCIT.Value)
            {
                if (fund.CITIsRate == null || fund.CITIsRate.Value == false)
                {
                    return GetCurrency(fund.CITAmount);
                }
                else
                    return fund.CITRate.ToString() + "%";
            }
            return "";
        }

        public static string GetCITDuration(EFundDetail entity)
        {
            if (entity.NoCITContribution.Value)
            {
                return DateHelper.GetMonthShortName(entity.NoCITFromMonth.Value, IsEnglish) + "/" + entity.NoCITFromYear + "-"
                    + DateHelper.GetMonthShortName(entity.NoCITToMonth.Value, IsEnglish) + "/" + entity.NoCITToYear;
            }
            return "";
        }

        public void SetEFund(EFundDetail dbEntity, EFundDetail entity)
        {
            dbEntity.PFRFNo = entity.PFRFNo;

            dbEntity.Nominee = entity.Nominee;
            dbEntity.Relation = entity.Relation;
            dbEntity.EmployeePan = entity.EmployeePan;
            dbEntity.PANNo = entity.PANNo;
            dbEntity.HasPFLimitContribution = entity.HasPFLimitContribution;
            dbEntity.PFLimitContributionAmount = entity.PFLimitContributionAmount;
            dbEntity.CITNo = entity.CITNo;


            #region "Change Logs"

            //CIT checked
            if (dbEntity.HasCIT == false && entity.HasCIT == true)
            {
                PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(dbEntity.EmployeeId.Value, (byte)MonetoryChangeLogTypeEnum.CIT,
                    "", "-", GetCITValue(entity), LogActionEnum.Add));
            }
            else if (dbEntity.HasCIT == true && entity.HasCIT == true && GetCITValue(dbEntity) != GetCITValue(entity))
            {
                PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(dbEntity.EmployeeId.Value, (byte)MonetoryChangeLogTypeEnum.CIT,
                    "", GetCITValue(dbEntity), GetCITValue(entity), LogActionEnum.Update));
            }
            else if (dbEntity.HasCIT == true && entity.HasCIT == false)
            {

                PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(dbEntity.EmployeeId.Value, (byte)MonetoryChangeLogTypeEnum.CIT,
                    "", GetCITValue(dbEntity), "-", LogActionEnum.Delete));
            }

            //No    CIT checked
            if (dbEntity.NoCITContribution == false && entity.NoCITContribution == true)
            {
                PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(dbEntity.EmployeeId.Value, (byte)MonetoryChangeLogTypeEnum.NoCIT,
                    "", "-", GetCITDuration(entity), LogActionEnum.Add));
            }
            else if (dbEntity.NoCITContribution == true && entity.NoCITContribution == true && GetCITDuration(dbEntity) != GetCITDuration(entity))
            {
                PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(dbEntity.EmployeeId.Value, (byte)MonetoryChangeLogTypeEnum.NoCIT,
                    "", GetCITDuration(dbEntity), GetCITDuration(entity), LogActionEnum.Update));
            }
            else if (dbEntity.NoCITContribution == true && entity.NoCITContribution == false)
            {

                PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(dbEntity.EmployeeId.Value, (byte)MonetoryChangeLogTypeEnum.NoCIT,
                    "", GetCITDuration(dbEntity), "-", LogActionEnum.Delete));
            }
            #endregion

            dbEntity.HasCIT = entity.HasCIT;
            dbEntity.NoCITContribution = entity.NoCITContribution;
            dbEntity.CITEffectiveFromMonth = entity.CITEffectiveFromMonth;
            dbEntity.CITEffectiveFromYear = entity.CITEffectiveFromYear;
            dbEntity.CITAmount = entity.CITAmount;
            dbEntity.CITIsRate = entity.CITIsRate;
            dbEntity.CITRate = entity.CITRate;
            dbEntity.NoCITFromMonth = entity.NoCITFromMonth;
            dbEntity.NoCITFromYear = entity.NoCITFromYear;
            dbEntity.NoCITToMonth = entity.NoCITToMonth;
            dbEntity.NoCITToYear = entity.NoCITToYear;
            dbEntity.CITCode = entity.CITCode;
            dbEntity.HasOtherFund = entity.HasOtherFund;
            if (dbEntity.NoCITContribution.Value)
            {
                dbEntity.NoCITFromEng =
                    GetEngDate("1/" + dbEntity.NoCITFromMonth + "/" + dbEntity.NoCITFromYear, null);

                dbEntity.NoCITToEng =
                    GetEngDate("1/" + dbEntity.NoCITToMonth + "/" + dbEntity.NoCITToYear,
                               null);

            }

        }
        public static string GetEmployeeOfficialEmail(int employeeId)
        {
            EAddress entity = PayrollDataContext.EAddresses.FirstOrDefault(x => x.EmployeeId == employeeId);
            if (entity != null)
                return entity.CIEmail;
            return null;
        }

        public void SetEAddress(EAddress dbEntity, EAddress entity, bool isOfficialEmailChangable)
        {
            dbEntity.PSLocality = entity.PSLocality;
            dbEntity.PSZoneId = entity.PSZoneId;
            dbEntity.PSDistrictId = entity.PSDistrictId;

            #region "Change Logs"
            if (isOfficialEmailChangable)
            {
                if (dbEntity.CIEmail != entity.CIEmail)
                    PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(dbEntity.EmployeeId.Value, (byte)MonetoryChangeLogTypeEnum.Information,
                        "Official Email", dbEntity.CIEmail, entity.CIEmail, LogActionEnum.Update));
            }

            if (dbEntity.PersonalEmail != entity.PersonalEmail)
                PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(dbEntity.EmployeeId.Value, (byte)MonetoryChangeLogTypeEnum.Information,
                    "Personal Email", dbEntity.PersonalEmail, entity.PersonalEmail, LogActionEnum.Update));
            #endregion

            if (isOfficialEmailChangable)
            {
                dbEntity.CIEmail = entity.CIEmail;
            }
            dbEntity.CIPhoneNo = entity.CIPhoneNo;
            dbEntity.CIMobileNo = entity.CIMobileNo;
            dbEntity.CIEmergencyNo = entity.CIEmergencyNo;


            dbEntity.Extension = entity.Extension;
            dbEntity.PersonalEmail = entity.PersonalEmail;
            dbEntity.PersonalPhone = entity.PersonalPhone;

            dbEntity.EmergencyName = entity.EmergencyName;
            dbEntity.EmergencyRelation = entity.EmergencyRelation;
            dbEntity.EmergencyMobile = entity.EmergencyMobile;


            dbEntity.PECountryId = entity.PECountryId;
            dbEntity.PELocality = entity.PELocality;
            dbEntity.PEZoneId = entity.PEZoneId;
            dbEntity.PEDistrictId = entity.PEDistrictId;
            dbEntity.PEHouseNo = entity.PEHouseNo;
            dbEntity.PEState = entity.PEState;
            dbEntity.PEStreet = entity.PEStreet;
            dbEntity.PEZipCode = entity.PEZipCode;

        }

        public void SetPay(PPay dbEntity, PPay entity)
        {
            dbEntity.PaymentFrequency = entity.PaymentFrequency;

            #region "Change Logs"
            if (dbEntity.PaymentMode != entity.PaymentMode)
            {
                PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(dbEntity.EmployeeId.Value, (byte)MonetoryChangeLogTypeEnum.Information,
                    "Payment Mode", dbEntity.PaymentMode, entity.PaymentMode, LogActionEnum.Update));
            }

            if (dbEntity.BankName != entity.BankName)
            {
                PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(dbEntity.EmployeeId.Value, (byte)MonetoryChangeLogTypeEnum.Information,
                    "Bank Name", dbEntity.BankName, entity.BankName, LogActionEnum.Update));
            }
            if (dbEntity.BankID != entity.BankID)
            {
                PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(dbEntity.EmployeeId.Value, (byte)MonetoryChangeLogTypeEnum.Information,
                    "Bank Name", dbEntity.BankID, entity.BankID, LogActionEnum.Update));
            }
            if (dbEntity.BankACNo != entity.BankACNo)
            {
                PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(dbEntity.EmployeeId.Value, (byte)MonetoryChangeLogTypeEnum.Information,
                    "Bank a/c", dbEntity.BankACNo, entity.BankACNo, LogActionEnum.Update));
            }
            if (dbEntity.BankACNo != entity.BankACNo)
            {
                PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(dbEntity.EmployeeId.Value, (byte)MonetoryChangeLogTypeEnum.Information,
                    "Currency Not Nepali", dbEntity.IsCurrencyNotNepali, entity.IsCurrencyNotNepali, LogActionEnum.Update));
            }

            if (dbEntity.EnableDualBankPayment != entity.EnableDualBankPayment
                && !(entity.EnableDualBankPayment != null && entity.EnableDualBankPayment.Value == false && dbEntity.EnableDualBankPayment == null))
            {
                PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(dbEntity.EmployeeId.Value, (byte)MonetoryChangeLogTypeEnum.Information,
                    "Dual Bank Payment", dbEntity.EnableDualBankPayment, entity.EnableDualBankPayment, LogActionEnum.Update));
            }
            if (dbEntity.Bank1ID != entity.Bank1ID)
            {
                PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(dbEntity.EmployeeId.Value, (byte)MonetoryChangeLogTypeEnum.Information,
                    "Dual First Bank", dbEntity.Bank1ID, entity.Bank1ID, LogActionEnum.Update));
            }
            if (dbEntity.Bank1ACNo != entity.Bank1ACNo)
            {
                PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(dbEntity.EmployeeId.Value, (byte)MonetoryChangeLogTypeEnum.Information,
                    "Dual First Bank AC No", dbEntity.Bank1ACNo, entity.Bank1ACNo, LogActionEnum.Update));
            }
            if (dbEntity.Bank2ACNo != entity.Bank2ACNo)
            {
                PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(dbEntity.EmployeeId.Value, (byte)MonetoryChangeLogTypeEnum.Information,
                    "Dual Second Bank AC No", dbEntity.Bank2ACNo, entity.Bank2ACNo, LogActionEnum.Update));
            }
            if (dbEntity.Bank2ID != entity.Bank2ID)
            {
                PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(dbEntity.EmployeeId.Value, (byte)MonetoryChangeLogTypeEnum.Information,
                    "Dual Second Bank", dbEntity.Bank2ID, entity.Bank2ID, LogActionEnum.Update));
            }
            if (dbEntity.IsPercentage != entity.IsPercentage)
            {
                PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(dbEntity.EmployeeId.Value, (byte)MonetoryChangeLogTypeEnum.Information,
                    "Dual Bank Is Percentage", dbEntity.IsPercentage, entity.IsPercentage, LogActionEnum.Update));
            }
            if (dbEntity.PaymentAmount != entity.PaymentAmount)
            {
                PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(dbEntity.EmployeeId.Value, (byte)MonetoryChangeLogTypeEnum.Information,
                    "Dual Bank Amount", dbEntity.PaymentAmount, entity.PaymentAmount, LogActionEnum.Update));
            }
            #endregion
            dbEntity.BankID = entity.BankID;
            dbEntity.Bank1ACNo = entity.Bank1ACNo;
            dbEntity.Bank2ACNo = entity.Bank2ACNo;
            dbEntity.EnableDualBankPayment = entity.EnableDualBankPayment;
            dbEntity.Bank1ID = entity.Bank1ID;
            dbEntity.Bank2ID = entity.Bank2ID;
            dbEntity.IsPercentage = entity.IsPercentage;
            dbEntity.PaymentAmount = entity.PaymentAmount;
            dbEntity.PaymentMode = entity.PaymentMode;
            dbEntity.BankName = entity.BankName;
            dbEntity.BankACNo = entity.BankACNo;
            dbEntity.IsCurrencyNotNepali = entity.IsCurrencyNotNepali;
        }
        //public bool Delete(Employee entity)
        //{
        //    Employee dbEntity = PayrollDataContext.Employees.SingleOrDefault(c => c.EmployeeId == entity.EmployeeId);
        //    if (dbEntity == null)
        //        return false;

        //    PayrollDataContext.Employees.DeleteOnSubmit(entity);
        //    return true;
        //}

        public EEmployee GetById(int employeeId)
        {
            EEmployee emp = PayrollDataContext.EEmployees.SingleOrDefault(c => c.EmployeeId == employeeId);
            return emp;
        }


        public static List<GetPAREmployeeHeaderResult> GetPAREmployeeHeader(string TimeSheetID)
        {
            List<GetPAREmployeeHeaderResult> emp = PayrollDataContext.GetPAREmployeeHeader(TimeSheetID).ToList();
            return emp;
        }

        public static List<GetTimeAllocationInfoByWeekResult> ReportGetTimeAllocationInfoByWeek(int employeeId, DateTime Week1, DateTime Week2, DateTime Week3, DateTime Week4, DateTime Week5, DateTime MonthEndDate, string TimeSheetIds)
        {
            List<GetTimeAllocationInfoByWeekResult> emp = PayrollDataContext.GetTimeAllocationInfoByWeek(Week1, Week2, Week3, Week4, Week5, MonthEndDate, TimeSheetIds).ToList();
            return emp;
        }



        public List<GetEmployeesUnderDepartmentResult> GetEmployeesUnderDepartment(int departmentId)
        {

            return PayrollDataContext.GetEmployeesUnderDepartment(departmentId).ToList();
        }


        public List<GetAllEmployeesNewResult> GetAllEmployeesNew(int currentPage, int pageSize, ref int? total, string sortBy, int companyId, string empNameSearchText, string idCardNoSearchText, bool? showRetiredOnly, int branchId, int departmentId, int designationId, int levelId
            , string customRoleDepartmentIDs, int minStatusID, bool statusOnly)
        {
            List<GetAllEmployeesNewResult> thisList = new List<GetAllEmployeesNewResult>();

            thisList = PayrollDataContext.GetAllEmployeesNew(currentPage, pageSize, sortBy, ref total, companyId, empNameSearchText, idCardNoSearchText, showRetiredOnly, branchId, departmentId, designationId, levelId,
                customRoleDepartmentIDs, minStatusID, statusOnly).ToList();

            if (thisList.Count > 0)
                total = thisList[0].TotalRows.Value;

            List<KeyValue> statusList = new JobStatus().GetMembers();
            foreach (var val1 in thisList)
            {
                if (string.IsNullOrEmpty(val1.UrlPhoto))
                {
                    if (val1.Title.ToLower() == "mr")
                        val1.UrlPhoto = "../images/male.png";
                    else
                        val1.UrlPhoto = "../images/female.png";
                }

                if (val1.StatusID != null && val1.StatusID != 0 && string.IsNullOrEmpty(val1.StatusText))
                {
                    KeyValue keyValue = statusList.SingleOrDefault(x => x.Key == val1.StatusID.ToString());
                    if (keyValue != null)
                        val1.StatusText = keyValue.Value;

                }

            }

            return thisList;
        }


        public List<GetAllEmployeeSearchNewResult> GetAllEmployeesSearchNew(int currentPage, int pageSize, ref int? total, string sortBy, int companyId, string empNameSearchText, string idCardNoSearchText, bool? showRetiredOnly, int branchId, int departmentId, int designationId, int levelId
          , string customRoleDepartmentIDs, int minStatusID, bool statusOnly)
        {
            List<GetAllEmployeeSearchNewResult> thisList = new List<GetAllEmployeeSearchNewResult>();

            thisList = PayrollDataContext.GetAllEmployeeSearchNew(currentPage, pageSize, sortBy, ref total, companyId, empNameSearchText, idCardNoSearchText, showRetiredOnly, branchId, departmentId, designationId, levelId,
                customRoleDepartmentIDs, minStatusID, statusOnly).ToList();

            if (thisList.Count > 0)
                total = thisList[0].TotalRows.Value;

            List<KeyValue> statusList = new JobStatus().GetMembers();
            foreach (var val1 in thisList)
            {
                if (string.IsNullOrEmpty(val1.UrlPhoto))
                {
                    if (val1.Title.ToLower() == "mr")
                        val1.UrlPhoto = "../images/male.png";
                    else
                        val1.UrlPhoto = "../images/female.png";
                }

                if (val1.StatusID != null && val1.StatusID != 0 && string.IsNullOrEmpty(val1.StatusText))
                {
                    KeyValue keyValue = statusList.SingleOrDefault(x => x.Key == val1.StatusID.ToString());
                    if (keyValue != null)
                        val1.StatusText = keyValue.Value;

                }

            }

            return thisList;
        }

        public static DateTime? GetLastPromotionDate(int empId)
        {
            ECurrentStatus lastStatusChange = PayrollDataContext.
                ECurrentStatus.Where(x => x.EmployeeId == empId)
                .OrderByDescending(x => x.FromDateEng).FirstOrDefault();

            DesignationHistory lastDesignationChange = PayrollDataContext
                .DesignationHistories.Where(x => x.EmployeeId == empId && (x.IsFirst == false))
                .OrderByDescending(x => x.FromDateEng).FirstOrDefault();


            // if last is equal to first then it is not promotion so make it null
            if (lastStatusChange.ECurrentStatusId ==
                PayrollDataContext.ECurrentStatus.Where(x => x.EmployeeId == empId).OrderBy(x => x.FromDateEng).FirstOrDefault().ECurrentStatusId)
                lastStatusChange = null;

            if (lastDesignationChange == null && lastStatusChange == null)
                return null;

            if (lastStatusChange != null && lastDesignationChange == null)
                return lastStatusChange.FromDateEng;

            if (lastDesignationChange != null && lastStatusChange == null)
                return lastDesignationChange.FromDateEng.Value;


            if (lastDesignationChange.FromDateEng < lastStatusChange.FromDateEng)
                return lastStatusChange.FromDateEng;
            else
                return lastDesignationChange.FromDateEng.Value;


        }

        public static GetEmployeeCurrentBranchDepartmentResult GetEmployeeCurrentBranchDepartment(int empId)
        {
            DateTime today = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
            return PayrollDataContext.GetEmployeeCurrentBranchDepartment(
                today, empId).FirstOrDefault();
        }

        public List<EEmployee> GetAllEmployees(int currentPage, int pageSize, ref int total, string sortBy, int companyId,
            string empNameSearchText, string idCardNoSearchText, bool? showRetiredOnly, int branchId,
            int departmentId, int designationId, int levelId, string customRoleDepartmentIDs,
            string designationName, int subdepartmentId
            , int payGroupID, int levelGroupID, string statusSelectionList, int unitId)
        {
            //dynamic sql generating sp so no return type from linq so pure ado.net 

            sortBy = sortBy.ToLower();

            currentPage -= 1;

            total = 0;

            List<KeyValue> statusList = new JobStatus().GetMembers();

            DbConnection conn = new SqlConnection(Config.ConnectionString);

            //PayrollDataContext.Connection;
            List<EEmployee> employees = new List<EEmployee>();
            using (conn)
            {
                DbCommand selectCommand = conn.CreateCommand();
                selectCommand.CommandText = "GetAllEmployees";
                selectCommand.CommandType = System.Data.CommandType.StoredProcedure;


                selectCommand.Parameters.Add(new SqlParameter("@currentPage", SqlDbType.Int));
                selectCommand.Parameters.Add(new SqlParameter("@pageSize", SqlDbType.Int));
                selectCommand.Parameters.Add(new SqlParameter("@sortBy", SqlDbType.VarChar));
                selectCommand.Parameters.Add(new SqlParameter("@total", SqlDbType.Int));
                selectCommand.Parameters.Add(new SqlParameter("@CompanyId", SqlDbType.Int));
                selectCommand.Parameters.Add(new SqlParameter("@EmployeeNameSearchText", SqlDbType.VarChar));
                selectCommand.Parameters.Add(new SqlParameter("@IDCardNoSearchText", SqlDbType.VarChar));
                selectCommand.Parameters.Add(new SqlParameter("@IsRetiredOrResignedAlso", SqlDbType.Bit));
                selectCommand.Parameters.Add(new SqlParameter("@BranchId", SqlDbType.Int));
                selectCommand.Parameters.Add(new SqlParameter("@DepartmentId", SqlDbType.Int));
                selectCommand.Parameters.Add(new SqlParameter("@SubDepartmentId", SqlDbType.Int));
                selectCommand.Parameters.Add(new SqlParameter("@DesignationId", SqlDbType.Int));
                selectCommand.Parameters.Add(new SqlParameter("@DesignationName", SqlDbType.VarChar, 100));
                selectCommand.Parameters.Add(new SqlParameter("@LevelId", SqlDbType.Int));
                selectCommand.Parameters.Add(new SqlParameter("@RoleDepartmentIDs", SqlDbType.VarChar, 1000));
                selectCommand.Parameters.Add(new SqlParameter("@PayGroupID", SqlDbType.Int));
                //selectCommand.Parameters.Add(new SqlParameter("@StatusOnly", SqlDbType.Bit));
                selectCommand.Parameters.Add(new SqlParameter("@GroupID", SqlDbType.Int));
                selectCommand.Parameters.Add(new SqlParameter("@StatusList", SqlDbType.VarChar, 1000));
                selectCommand.Parameters.Add(new SqlParameter("@UnitId", SqlDbType.Int));

                selectCommand.Parameters["@currentPage"].Value = currentPage;
                selectCommand.Parameters["@pageSize"].Value = pageSize;
                selectCommand.Parameters["@sortBy"].Value = sortBy;
                selectCommand.Parameters["@CompanyId"].Value = companyId;
                if (showRetiredOnly == null)
                    selectCommand.Parameters["@IsRetiredOrResignedAlso"].Value = DBNull.Value;// : showRetiredOnly.Value;
                else
                    selectCommand.Parameters["@IsRetiredOrResignedAlso"].Value = showRetiredOnly.Value;
                selectCommand.Parameters["@EmployeeNameSearchText"].Value = empNameSearchText;

                selectCommand.Parameters["@IDCardNoSearchText"].Value = idCardNoSearchText;
                selectCommand.Parameters["@BranchId"].Value = branchId;
                selectCommand.Parameters["@DepartmentId"].Value = departmentId;
                selectCommand.Parameters["@SubDepartmentId"].Value = subdepartmentId;
                selectCommand.Parameters["@DesignationId"].Value = designationId;
                selectCommand.Parameters["@DesignationName"].Value = designationName;
                selectCommand.Parameters["@LevelId"].Value = levelId;
                if (customRoleDepartmentIDs == null)
                    selectCommand.Parameters["@RoleDepartmentIDs"].Value = DBNull.Value;
                else
                    selectCommand.Parameters["@RoleDepartmentIDs"].Value = customRoleDepartmentIDs;
                //selectCommand.Parameters["@MinStatus"].Value = minStatusID;
                //selectCommand.Parameters["@StatusOnly"].Value = statusOnly;
                selectCommand.Parameters["@PayGroupID"].Value = payGroupID;
                selectCommand.Parameters["@GroupID"].Value = levelGroupID;
                selectCommand.Parameters["@StatusList"].Value = statusSelectionList;
                selectCommand.Parameters["@UnitId"].Value = unitId;
                selectCommand.Parameters["@total"].Direction = ParameterDirection.Output;

                SetConnectionPwd(conn);
                DbDataReader reader = selectCommand.ExecuteReader();
                using (reader)
                {
                    while (reader.Read())
                    {
                        EEmployee emp = new EEmployee();
                        emp.EmployeeId = (int)reader["EmployeeId"];
                        emp.Name = reader["Name"].ToString();

                        bool isRetiredOrResigned = DBNull.Value.Equals(reader["IsRetiredOrResigned"])
                                                       ? false
                                                       : Convert.ToBoolean(reader["IsRetiredOrResigned"]);



                        //TODO: as no property to directly hold all so this way
                        //emp.Title = reader["Department"].ToString() + "$$" + reader["Branch"].ToString() + "$$"
                        //            + reader["CostCode"].ToString();

                        emp.DepartmentValue = reader["Department"].ToString();
                        emp.SubDepartmentValue = reader["SubDepartment"].ToString();
                        emp.BranchValue = reader["Branch"].ToString();
                        emp.CostCodeValue = reader["CostCode"].ToString();
                        emp.DesignationValue = reader["Designation"].ToString();
                        emp.LevelValue = reader["Level"].ToString();
                        emp.URLPhoto = reader["UrlPhoto"].ToString();
                        emp.Title = reader["Title"].ToString();
                        emp.FunctionalTitle = reader["FunctionalTitle"].ToString();
                        emp.LevelDesignationValue = emp.LevelValue + " - " + emp.DesignationValue;

                        if (string.IsNullOrEmpty(emp.URLPhoto))
                        {
                            if (emp.Title.ToLower() == "mr")
                                emp.URLPhoto = "~/images/male.png";
                            else
                                emp.URLPhoto = "~/images/female.png";
                        }

                        emp.IdCardNoValue = reader["IdCardNo"].Equals(DBNull.Value) ? "" : reader["IdCardNo"].ToString();
                        emp.IsRetiredOrResignedValue = isRetiredOrResigned;
                        if (reader["StatusID"].Equals(DBNull.Value) == false)
                        {
                            emp.StatusID = int.Parse(reader["StatusID"].ToString());
                        }

                        if (emp.StatusID != 0)
                        {
                            KeyValue keyValue = statusList.SingleOrDefault(x => x.Key == emp.StatusID.ToString());
                            if (keyValue != null)
                                emp.StatusText = keyValue.Value;
                        }

                        total = (int)reader["TotalRows"];
                        employees.Add(emp);
                    }
                }
                //total = (int)selectCommand.Parameters["@total"].Value;
                //total = 
                return employees;
            }
        }


        public static int GetTotalEmployeesForPayroll(PayrollPeriod payroll)
        {

            return (

                 from x in PayrollDataContext.EEmployees
                 join h in PayrollDataContext.EHumanResources on x.EmployeeId equals h.EmployeeId

                 where x.CompanyId == SessionManager.CurrentCompanyId
                 && (
                      (
                          x.IsResigned == false ||
                          h.DateOfResignationEng >= payroll.StartDateEng
                          )
                      &&
                       (
                          x.IsRetired == false ||
                          h.DateOfRetirementEng >= payroll.StartDateEng
                          )
                  )
                 select x
                  ).Count();

        }
        public static int GetEmployeesRecruitedForPayroll(PayrollPeriod payroll)
        {
            return DashboardManager.TotalEmpRecruited(payroll.StartDateEng.Value, payroll.EndDateEng.Value);
        }

        public List<GetEmployeesResult> GetEmployees(int companyId)
        {
            List<int> customRoleDepartmentIDList = SessionManager.CustomRoleDeparmentList;

            return PayrollDataContext.GetEmployees(companyId)
                  .Where(x => customRoleDepartmentIDList.Count == 0 || customRoleDepartmentIDList.Contains(x.DepartmentId))
                .ToList();
        }


        public ResponseStatus Save(ECurrentStatus entity)
        {
            ResponseStatus status = new ResponseStatus();

            //validate for prev status

            ECurrentStatus lastStatus = GetCurrentLastStatus(entity.EmployeeId);
            if (lastStatus != null)
            {
                if (lastStatus.DefineToDate != null && lastStatus.DefineToDate.Value && entity.FromDateEng < lastStatus.ToDateEng)
                {
                    status.ErrorMessage = "New status from date must be greater than last to date " + lastStatus.ToDate + ".";
                    return status;
                }
                else if (entity.FromDateEng < lastStatus.FromDateEng)
                {
                    status.ErrorMessage = "New status from date must be greater than last from date " + lastStatus.FromDate + ".";
                    return status;
                }
            }

            if (entity.DefineToDate != null && entity.DefineToDate.Value && entity.FromDateEng > entity.ToDateEng)
            {
                status.ErrorMessage = "Start date cannot be greater than end date.";
                return status;
            }

            PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(entity.EmployeeId, (byte)MonetoryChangeLogTypeEnum.Status,
                "", lastStatus == null ? "" : lastStatus.CurrentStatusText + "," + lastStatus.FromDate +
              (lastStatus.DefineToDate == null || lastStatus.DefineToDate == false ? "" : (lastStatus.ToDate == null ? "" : "," + lastStatus.ToDate)),
                entity.CurrentStatusText + "," + entity.FromDate, LogActionEnum.Add));


            entity.CreatedBy = SessionManager.User.UserID;
            entity.CreatedOn = DateTime.Now;

            PayrollDataContext.ECurrentStatus.InsertOnSubmit(entity);
            SaveChangeSet();

            return status;
        }

        public static int DeleteStatus(int statusId)
        {
            ECurrentStatus dbEntity = PayrollDataContext.ECurrentStatus.SingleOrDefault(c => c.ECurrentStatusId == statusId);

            int empId = dbEntity.EmployeeId;

            PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(dbEntity.EmployeeId, (byte)MonetoryChangeLogTypeEnum.Status,
              "", dbEntity.CurrentStatusText + "," + dbEntity.FromDate +
              (dbEntity.DefineToDate == null || dbEntity.DefineToDate == false ? "" : (dbEntity.ToDate == null ? "" : "," + dbEntity.ToDate)),
              "", LogActionEnum.Delete));

            PayrollDataContext.ECurrentStatus.DeleteOnSubmit(dbEntity);
            DeleteChangeSet();
            return empId;

        }

        public ResponseStatus Update(ECurrentStatus entity)
        {
            ResponseStatus status = new ResponseStatus();

            ECurrentStatus dbEntity = PayrollDataContext.ECurrentStatus.SingleOrDefault(c => c.ECurrentStatusId == entity.ECurrentStatusId);
            //may be deleted
            if (dbEntity == null)
                return status;


            ECurrentStatus prevStatus = PayrollDataContext.ECurrentStatus
                .Where(s => s.EmployeeId == entity.EmployeeId && s.FromDateEng < entity.FromDateEng && s.ECurrentStatusId != entity.ECurrentStatusId)
                .OrderByDescending(s => s.FromDateEng).FirstOrDefault();

            if (prevStatus != null)
            {
                if (prevStatus.DefineToDate != null && prevStatus.DefineToDate.Value && entity.FromDateEng < prevStatus.ToDateEng)
                {
                    status.ErrorMessage = "New status from date must be greater than last to date " + prevStatus.ToDate + " (" + prevStatus.ToDateEng.Value.ToString("yyyy/MM/dd") + ")" + ".";
                    return status;
                }
                else if (entity.FromDateEng < prevStatus.FromDateEng)
                {
                    status.ErrorMessage = "New status from date must be greater than last from date " + prevStatus.FromDate + " (" + prevStatus.ToDateEng.Value.ToString("yyyy/MM/dd") + ")" + ".";
                    return status;
                }
            }


            if (entity.DefineToDate != null && entity.DefineToDate.Value && entity.FromDateEng > entity.ToDateEng)
            {
                status.ErrorMessage = "Start date cannot be greater than end date.";
                return status;
            }

            PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(entity.EmployeeId, (byte)MonetoryChangeLogTypeEnum.Status,
              "",
              dbEntity.CurrentStatusText + "," + dbEntity.FromDate +
              (dbEntity.DefineToDate == null || dbEntity.DefineToDate == false ? "" : (dbEntity.ToDate == null ? "" : "," + dbEntity.ToDate)),
              entity.CurrentStatusText + "," + entity.FromDate +
              (entity.DefineToDate == null || entity.DefineToDate == false ? "" : (entity.ToDate == null ? "" : "," + entity.ToDate)), LogActionEnum.Update));


            dbEntity.FromDate = entity.FromDate;
            //dbEntity.ToDate = entity.ToDate;
            dbEntity.FromDateEng = entity.FromDateEng;
            //  dbEntity.ToDateEng = entity.ToDateEng;
            dbEntity.CurrentStatus = entity.CurrentStatus;
            dbEntity.Note = entity.Note;
            dbEntity.ToDate = entity.ToDate;
            dbEntity.ToDateEng = entity.ToDateEng;
            dbEntity.DefineToDate = entity.DefineToDate;
            dbEntity.EventID = entity.EventID;

            UpdateChangeSet();

            return status;
        }

        public static List<int> ActiveTodayEmployeeCount()
        {
            return
                (
                    from e in PayrollDataContext.EEmployees
                    join h in PayrollDataContext.EHumanResources on e.EmployeeId equals h.EmployeeId
                    where
                    (
                        (
                            e.IsResigned == false ||
                            h.DateOfResignationEng >= DateTime.Now.Date
                            )
                        &&
                         (
                            e.IsRetired == false ||
                            h.DateOfRetirementEng >= DateTime.Now.Date
                            )
                    )
                    select e.EmployeeId
                ).ToList();
        }

        public static List<int> PresentToday()
        {
            return
                (
                    from t in PayrollDataContext.AttendanceCheckInCheckOutViews
                    where t.Date == DateTime.Today.Date
                    select t.EmployeeId
                ).Distinct().ToList();
        }
        public static List<int> LeaveToday()
        {
            return
                (
                    from l in PayrollDataContext.LeaveRequests
                    join d in PayrollDataContext.LeaveDetails on l.LeaveRequestId equals d.LeaveRequestId
                    where d.DateOnEng.Date == DateTime.Now.Date
                    && l.Status == (int)LeaveRequestStatusEnum.Approved
                    select l.EmployeeId
                ).ToList();
        }


        public static List<Report_GetEmployeeCountResult> GetEmployeeCount(string type
            , int branchId, int departmentId, int levelId, int designationId, bool? retOnly)
        {
            List<Report_GetEmployeeCountResult> list =
               BLL.BaseBiz.PayrollDataContext.Report_GetEmployeeCount(type, branchId, departmentId, levelId, designationId, retOnly).ToList();

            int sn = 0;

            foreach (Report_GetEmployeeCountResult item in list)
                item.SN = ++sn;

            Report_GetEmployeeCountResult total = new Report_GetEmployeeCountResult();



            total.Name = "Total";

            total.Total = list.Sum(x => x.Total);

            list.Add(total);

            return list;
        }

        /// <summary>
        /// set 5 fixed income id for Employee Pay Salary Report
        /// </summary>
        /// <param name="incomeID1"></param>
        /// <param name="incomeID2"></param>
        /// <param name="incomeID3"></param>
        /// <param name="incomeID4"></param>
        /// <param name="incomeID5"></param>
        public static void SetIncomeListForEmployeeCurrentPayReport(ref int? incomeID1, ref int? incomeID2,
             ref int? incomeID3, ref int? incomeID4, ref int? incomeID5,
            ref int? incomeID6, ref int? incomeID7, ref int? incomeID8,
            ref int? incomeID9, ref int? incomeID10)
        {

            int incomeID11 = 0;
            int incomeID21 = 0;
            int incomeID31 = 0;
            int incomeID41 = 0;
            int incomeID51 = 0;
            int incomeID61 = 0;
            int incomeID71 = 0;
            int incomeID81 = 0;
            int incomeID91 = 0;
            int incomeID101 = 0;

            if (CommonManager.CompanySetting.WhichCompany == WhichCompany.Quest)
            {
                incomeID11 = 2;// basic
                incomeID21 = 1002;// grade
                incomeID31 = 1003;// dearness
                incomeID41 = 1004;// other
            }
            else
            {
                List<CurrentPayIncome> list = PayrollDataContext.CurrentPayIncomes.ToList();
                List<int> ids = list.Select(x => x.IncomeId).ToList();
                List<PIncome> incomesList = PayManager.GetIncomeList().OrderBy(x => x.Order == null ? 0 : x.Order.Value).ToList();

                foreach (var income in incomesList)
                {
                    if (ids.Contains(income.IncomeId))
                    {
                        if (incomeID11 == 0) incomeID11 = income.IncomeId;
                        else if (incomeID21 == 0) incomeID21 = income.IncomeId;
                        else if (incomeID31 == 0) incomeID31 = income.IncomeId;
                        else if (incomeID41 == 0) incomeID41 = income.IncomeId;
                        else if (incomeID51 == 0) incomeID51 = income.IncomeId;
                        else if (incomeID61 == 0) incomeID61 = income.IncomeId;
                        else if (incomeID71 == 0) incomeID71 = income.IncomeId;
                        else if (incomeID81 == 0) incomeID81 = income.IncomeId;
                        else if (incomeID91 == 0) incomeID91 = income.IncomeId;
                        else if (incomeID101 == 0) incomeID101 = income.IncomeId;
                    }
                }

                //incomeID11 = new PayManager().GetBasicIncome(SessionManager.CurrentCompanyId).IncomeId;
                //PIncome otherIncome = PayrollDataContext.PIncomes.FirstOrDefault(x => x.IncomeId != incomeID11 && x.Calculation == IncomeCalculation.FIXED_AMOUNT);
                //if (otherIncome != null) incomeID21 = otherIncome.IncomeId;

                //otherIncome = PayrollDataContext.PIncomes.FirstOrDefault(x => x.IncomeId != incomeID11 && x.IncomeId != incomeID21 && x.Calculation == IncomeCalculation.FIXED_AMOUNT);
                //if (otherIncome != null) incomeID31 = otherIncome.IncomeId;

                //otherIncome = PayrollDataContext.PIncomes.FirstOrDefault(x => x.IncomeId != incomeID11 && x.IncomeId != incomeID21 && x.IncomeId != incomeID31 && x.Calculation == IncomeCalculation.FIXED_AMOUNT);
                //if (otherIncome != null) incomeID41 = otherIncome.IncomeId;

                //otherIncome = PayrollDataContext.PIncomes.FirstOrDefault(x => x.IncomeId != incomeID11 && x.IncomeId != incomeID21 && x.IncomeId != incomeID31 && x.IncomeId != incomeID41 && x.Calculation == IncomeCalculation.FIXED_AMOUNT);
                //if (otherIncome != null) incomeID51 = otherIncome.IncomeId;
            }

            if (incomeID11 == 0)
                incomeID1 = null;
            else
                incomeID1 = incomeID11;

            if (incomeID21 == 0)
                incomeID2 = null;
            else
                incomeID2 = incomeID21;

            if (incomeID31 == 0)
                incomeID3 = null;
            else
                incomeID3 = incomeID31;

            if (incomeID41 == 0)
                incomeID4 = null;
            else
                incomeID4 = incomeID41;

            if (incomeID51 == 0)
                incomeID5 = null;
            else
                incomeID5 = incomeID51;

            if (incomeID61 == 0)
                incomeID6 = null;
            else
                incomeID6 = incomeID61;
            if (incomeID71 == 0)
                incomeID7 = null;
            else
                incomeID7 = incomeID71;
            if (incomeID81 == 0)
                incomeID8 = null;
            else
                incomeID8 = incomeID81;
            if (incomeID91 == 0)
                incomeID9 = null;
            else
                incomeID9 = incomeID91;
            if (incomeID101 == 0)
                incomeID10 = null;
            else
                incomeID10 = incomeID101;
        }
        public static void SetIncomeListForEmployeeHomeSalaryReport(ref int? incomeID1, ref int? incomeID2,
             ref int? incomeID3, ref int? incomeID4, ref int? incomeID5, ref int? incomeID6)
        {

            int incomeID11 = 0;
            int incomeID21 = 0;
            int incomeID31 = 0;
            int incomeID41 = 0;
            int incomeID51 = 0;
            int incomeID61 = 0;


            if (CommonManager.CompanySetting.WhichCompany == WhichCompany.NIBL)
            {
                incomeID11 = PayManager.niblCompulsoryHomeSalaryIncomesID[0];
                incomeID21 = PayManager.niblCompulsoryHomeSalaryIncomesID[1];
                incomeID31 = PayManager.niblCompulsoryHomeSalaryIncomesID[2];

                //incomeID41 = PayManager.niblCompulsoryHomeSalaryIncomesID[3];
                //incomeID51 = PayManager.niblCompulsoryHomeSalaryIncomesID[4];
                //incomeID61 = PayManager.niblCompulsoryHomeSalaryIncomesID[5];
            }
            else
            {
                incomeID11 = new PayManager().GetBasicIncome(SessionManager.CurrentCompanyId).IncomeId;
                PIncome otherIncome = PayrollDataContext.PIncomes.FirstOrDefault(x => x.IncomeId != incomeID11 && x.Calculation == IncomeCalculation.FIXED_AMOUNT);
                if (otherIncome != null) incomeID21 = otherIncome.IncomeId;

                otherIncome = PayrollDataContext.PIncomes.FirstOrDefault(x => x.IncomeId != incomeID11 && x.IncomeId != incomeID21 && x.Calculation == IncomeCalculation.FIXED_AMOUNT);
                if (otherIncome != null) incomeID31 = otherIncome.IncomeId;

                otherIncome = PayrollDataContext.PIncomes.FirstOrDefault(x => x.IncomeId != incomeID11 && x.IncomeId != incomeID21 && x.IncomeId != incomeID31 && x.Calculation == IncomeCalculation.FIXED_AMOUNT);
                if (otherIncome != null) incomeID41 = otherIncome.IncomeId;

                otherIncome = PayrollDataContext.PIncomes.FirstOrDefault(x => x.IncomeId != incomeID11 && x.IncomeId != incomeID21 && x.IncomeId != incomeID31 && x.IncomeId != incomeID41 && x.Calculation == IncomeCalculation.FIXED_AMOUNT);
                if (otherIncome != null) incomeID51 = otherIncome.IncomeId;

                //otherIncome = PayrollDataContext.PIncomes.FirstOrDefault(x => x.IncomeId != incomeID11 && x.IncomeId != incomeID21 && x.IncomeId != incomeID31 && x.IncomeId != incomeID41 && x.Calculation == IncomeCalculation.FIXED_AMOUNT);
                //if (otherIncome != null) incomeID51 = otherIncome.IncomeId;
            }

            if (incomeID11 == 0)
                incomeID1 = null;
            else
                incomeID1 = incomeID11;

            if (incomeID21 == 0)
                incomeID2 = null;
            else
                incomeID2 = incomeID21;

            if (incomeID31 == 0)
                incomeID3 = null;
            else
                incomeID3 = incomeID31;

            if (incomeID41 == 0)
                incomeID4 = null;
            else
                incomeID4 = incomeID41;

            if (incomeID51 == 0)
                incomeID5 = null;
            else
                incomeID5 = incomeID51;

            if (incomeID61 == 0)
                incomeID6 = null;
            else
                incomeID6 = incomeID61;
        }


        public static void SetLeaveListForEmployee(ref int? leaveID1, ref int? leaveID2,
             ref int? leaveID3, ref int? leaveID4, ref int? leaveID5, ref int? leaveID6, ref int? leaveID7, ref int? leaveID8,
            ref int? leaveID9, ref int? leaveID10, ref int? leaveID11)
        {



            List<LLeaveType> leaveList = LeaveAttendanceManager.GetLeaveListByCompany(SessionManager.CurrentCompanyId)
                .OrderBy(x => x.Order).ToList();

            leaveList.Add(new LLeaveType { LeaveTypeId = 0, Title = "Unpaid Leave" });

            foreach (LLeaveType item in leaveList)
            {

                if (leaveID1 != null)
                    leaveID1 = item.LeaveTypeId;
                if (leaveID2 != null)
                    leaveID2 = item.LeaveTypeId;
                if (leaveID3 != null)
                    leaveID3 = item.LeaveTypeId;
                if (leaveID4 != null)
                    leaveID4 = item.LeaveTypeId;
                if (leaveID5 != null)
                    leaveID5 = item.LeaveTypeId;
                if (leaveID6 != null)
                    leaveID6 = item.LeaveTypeId;
                if (leaveID7 != null)
                    leaveID7 = item.LeaveTypeId;
                if (leaveID8 != null)
                    leaveID8 = item.LeaveTypeId;
                if (leaveID9 != null)
                    leaveID9 = item.LeaveTypeId;
                if (leaveID10 != null)
                    leaveID10 = item.LeaveTypeId;
                if (leaveID11 != null)
                    leaveID11 = item.LeaveTypeId;


            }


        }

        public static List<Report_GetEmployeeCurrentSalaryPayResult> GetEmployeeCurrentPaySalary(
            int branchid, int depId, int levelid, int desigId, int empId, int currentPage, int pageSize, string sortBy, ref int total)
        {
            int? incomeID1 = null;
            int? incomeID2 = null;
            int? incomeID3 = null;
            int? incomeID4 = null;
            int? incomeID5 = null;
            int? incomeID6 = null;
            int? incomeID7 = null;
            int? incomeID8 = null;
            int? incomeID9 = null;
            int? incomeID10 = null;

            SetIncomeListForEmployeeCurrentPayReport(ref incomeID1, ref incomeID2, ref incomeID3, ref incomeID4, ref incomeID5,
                ref incomeID6, ref incomeID7, ref incomeID8, ref incomeID9, ref incomeID10);

            List<Report_GetEmployeeCurrentSalaryPayResult> list =
                PayrollDataContext.Report_GetEmployeeCurrentSalaryPay
                (branchid, depId, empId, levelid, desigId, incomeID1, incomeID2, incomeID3, incomeID4, incomeID5,
                incomeID6, incomeID7, incomeID8, incomeID9, incomeID10, currentPage, pageSize, sortBy)
                .ToList();

            if (list.Count > 0)
                total = list[0].TotalRows.Value;

            return list;
        }

        public static List<Report_GetSalaryDeductionResult> GetSalaryDeductionReport(
            int branchid, int depId, int levelid, int desigId, int empId, int currentPage, int pageSize, string sortBy, ref int total
            , int payrollPeriodId, int pageViewType, int managerEmployeeId)
        {

            List<Report_GetSalaryDeductionResult> list =
                PayrollDataContext.Report_GetSalaryDeduction
                (payrollPeriodId, branchid, depId, empId, levelid, desigId, currentPage, pageSize, sortBy
                , pageViewType, managerEmployeeId)
                .ToList();

            if (list.Count > 0)
                total = list[0].TotalRows.Value;

            return list;
        }

        public static List<Report_GetLoanRepaymentResult> GetLoanRepaymentReport(
         int empId, int currentPage, int pageSize, string sortBy, ref int total
          , int payrollPeriodId, int deductionId, int type)
        {

            List<Report_GetLoanRepaymentResult> list =
                PayrollDataContext.Report_GetLoanRepayment(payrollPeriodId, deductionId, currentPage, pageSize, empId, type)
                .ToList();

            foreach (Report_GetLoanRepaymentResult item in list)
            {
                CustomDate taken = CustomDate.GetCustomDateFromString(item.TakenOn, IsEnglish);
                item.TakenOnText = taken.Year + "-" + DateHelper.GetMonthName(taken.Month, IsEnglish);

                taken = CustomDate.GetCustomDateFromString(item.EndDate, IsEnglish);
                item.EndDateText = taken.Year + "-" + DateHelper.GetMonthName(taken.Month, IsEnglish);

                item.TakenOnEngText = item.TakenOnEng.Value.ToString("dd-MMM-yyyy");
                item.EndDateEngText = item.EndDateEng.Value.ToString("dd-MMM-yyyy");
            }

            if (list.Count > 0)
                total = list[0].TotalRows.Value;

            return list;
        }

        public static List<Report_GetSalaryDeductionDetailsResult> GetSalaryDeductionDetailsReport(
            int branchid, int depId, int levelid, int desigId, int empId, int currentPage, int pageSize, string sortBy, ref int total
            , int payrollPeriodId, int pageType, int managerId)
        {

            List<Report_GetSalaryDeductionDetailsResult> list =
                PayrollDataContext.Report_GetSalaryDeductionDetails
                (payrollPeriodId, branchid, depId, empId, levelid, desigId, currentPage, pageSize, sortBy, pageType, managerId)
                .ToList();

            if (list.Count > 0)
                total = list[0].TotalRows.Value;

            return list;
        }
        public static List<Report_GetCumulativeSummaryResult> GetCumulativeSummaryReport(
          int branchid, int depId, int levelid, int desigId, int empId, int currentPage, int pageSize, string sortBy, ref int total
          , int payrollPeriodId, int pageType, int managerId)
        {

            List<Report_GetCumulativeSummaryResult> list =
                PayrollDataContext.Report_GetCumulativeSummary
                (payrollPeriodId, branchid, depId, empId, levelid, desigId, currentPage, pageSize, sortBy, pageType, managerId)
                .ToList();

            if (list.Count > 0)
                total = list[0].TotalRows.Value;

            return list;
        }
        public static List<GetLeaveFareEmployeeListResult> GetLeaveFareDetails(
           int payrollPeriodId, DateTime startdate, DateTime enddate, int empId, int currentPage, int pageSize, string sortBy, ref int total, bool isApprovedOnly, int paymentStatus)
        {

            PayrollPeriod period = CommonManager.GetPayrollPeriod(payrollPeriodId);

            List<GetLeaveFareEmployeeListResult> list =
                PayrollDataContext.GetLeaveFareEmployeeList
                (startdate, enddate, payrollPeriodId, isApprovedOnly,
                 currentPage, pageSize, sortBy, empId, paymentStatus)
                .ToList();


            foreach (GetLeaveFareEmployeeListResult item in list)
            {
                item.PayrollPeriodId = payrollPeriodId;
            }

            if (list.Count > 0)
                total = list[0].TotalRows.Value;

            return list;
        }
        public static List<GetHolidayAdditionalPayListResult> GetHolidayAdditionalPayDetails(
          int payrollPeriodId, DateTime startdate, DateTime enddate, int empId, int currentPage, int pageSize, string sortBy, ref int total, bool isApprovedOnly, int paymentStatus)
        {

            PayrollPeriod period = CommonManager.GetPayrollPeriod(payrollPeriodId);

            List<GetHolidayAdditionalPayListResult> list =
                PayrollDataContext.GetHolidayAdditionalPayList
                (startdate, enddate, payrollPeriodId, isApprovedOnly,
                 currentPage, pageSize, sortBy, empId, paymentStatus)
                .ToList();


            foreach (GetHolidayAdditionalPayListResult item in list)
            {
                CustomDate date = new CustomDate(item.DateEng.Day, item.DateEng.Month, item.DateEng.Year, true);
                date = CustomDate.ConvertEngToNep(date);

                item.DayValueTextAdded = date.Day + " - " + item.DayValueText;
            }

            if (list.Count > 0)
                total = list[0].TotalRows.Value;

            return list;
        }
        public static List<Report_AgeWiseEmployeeResult> GetEmployeeAge(
            int branchid, int depId, int levelid, int desigId, int empId, int currentPage, int pageSize, string sortBy, ref int total)
        {

            List<Report_AgeWiseEmployeeResult> list =
                PayrollDataContext.Report_AgeWiseEmployee
                (branchid, depId, empId, levelid, desigId, currentPage, pageSize, sortBy)
                .ToList();

            if (list.Count > 0)
                total = list[0].TotalRows.Value;

            return list;
        }
        public static List<Report_EmployeeReferenceResult> GetEmployeeReference(
           int branchid, int depId, int levelid, int desigId, int empId, int currentPage, int pageSize, string sortBy, ref int total)
        {

            List<Report_EmployeeReferenceResult> list =
                PayrollDataContext.Report_EmployeeReference
                (branchid, depId, empId, levelid, desigId, currentPage, pageSize, sortBy)
                .ToList();

            if (list.Count > 0)
                total = list[0].TotalRows.Value;

            return list;
        }
        public static List<Report_StopHoldPaymentResult> GetStopHoldList(
           int empId, int type, DateTime? start, DateTime? end, int currentPage, int pageSize, string sortBy, bool excludeRetirement, ref int total)
        {

            List<Report_StopHoldPaymentResult> list =
                PayrollDataContext.Report_StopHoldPayment
                (empId, type, start, end, currentPage, pageSize, sortBy, excludeRetirement)
                .ToList();

            if (list.Count > 0)
                total = list[0].TotalRows.Value;

            return list;
        }
        public static List<Report_GetHomeSalaryResult> GetHomeSalaryReport(
            int branchid, int depId, int levelid, int desigId, int empId, int currentPage, int pageSize, string sortBy, ref int total)
        {

            int payrollPeriodId = CommonManager.GetLastPayrollPeriod().PayrollPeriodId;

            bool readingSumForMiddleFiscalYearStartedReqd = false;
            //contains the id of starting-ending payroll period to be read for history of regular income
            int startingPayrollPeriodId = 0, endingPayrollPeriodId = 0;
            // int notWorkedMonthInFiscalYear = 0, passedMonthWorked = 0;
            CalculationManager.GenerateForPastIncome(
                     payrollPeriodId, ref readingSumForMiddleFiscalYearStartedReqd,
                     ref startingPayrollPeriodId, ref endingPayrollPeriodId);

            int empStartingPayrollId = startingPayrollPeriodId;
            int empEndingPayrollId = endingPayrollPeriodId;

            List<Report_GetHomeSalaryResult> list =
                PayrollDataContext.Report_GetHomeSalary
                (branchid, depId, empId, levelid, desigId, currentPage, pageSize, sortBy, payrollPeriodId, empStartingPayrollId, empEndingPayrollId,
                readingSumForMiddleFiscalYearStartedReqd, SessionManager.CurrentCompanyId,
                 LeaveAttendanceManager.GetTotalDaysInFiscalYearForPeriod(payrollPeriodId), null)
                .ToList();


            int? incomeID1 = null;
            int? incomeID2 = null;
            int? incomeID3 = null;
            int? incomeID4 = null;
            int? incomeID5 = null;
            int? incomeID6 = null;
            SetIncomeListForEmployeeHomeSalaryReport(ref incomeID1, ref incomeID2, ref incomeID3, ref incomeID4, ref incomeID5, ref incomeID6);


            if (list.Count > 0)
                total = list[0].TotalRows.Value;

            foreach (Report_GetHomeSalaryResult item in list)
            {
                item.Process(incomeID1, incomeID2, incomeID3, incomeID4, incomeID5, incomeID6);
            }

            return list;
        }

        public static List<Report_GetOverTimeAllowanceEmployeesResult> GetOverTimeEmployees(
           int PayrollPeriodId, int currentPage, int pageSize, string sortBy, ref int? total)
        {
            return PayrollDataContext.Report_GetOverTimeAllowanceEmployees(currentPage, pageSize, sortBy, ref total, PayrollPeriodId).ToList();
        }

        public static List<Report_GetOverTimeAllowanceSumResult> GetOverTimeAllowanceSum(
          int PayrollPeriodId)
        {
            return PayrollDataContext.Report_GetOverTimeAllowanceSum(PayrollPeriodId).ToList();
        }

        public static List<Report_HR_YearlyLeaveBalanceResult> GetYearlyLeaveReport(
           int branchid, int depId, int levelid, int desigId, int empId, int currentPage, int pageSize,
            string sortBy, ref int total,
            int? PayrollPeriodId, int? PayrollPeriodEndId,
            int? currentLeavePeriodIdForAccural, DateTime? startDateLeaveTaken, DateTime? endDateLeaveTaken,
            string empName, int? lastYearOrYearOpeningPayrollPeriodId, DateTime yearStart, DateTime yearEnd
            , int pageType, int managerId)
        {

            int daysDiff = 0; int remainingMonth = 0;
            PayrollPeriod payrollPeriod = CommonManager.GetLastPayrollPeriod();
            if (payrollPeriod.Month != SessionManager.CurrentCompany.LeaveStartMonth.Value)
            {

                daysDiff = LeaveAttendanceManager.GetYearlyLeaveMiddleJoiningDays(payrollPeriod, ref remainingMonth);
            }


            List<Report_HR_YearlyLeaveBalanceResult> list =
                PayrollDataContext.Report_HR_YearlyLeaveBalance
                (yearStart, yearEnd, empName, branchid, depId, empId, currentPage, pageSize, sortBy, pageType, managerId)
                .ToList();

            List<LLeaveType> leaveList = LeaveAttendanceManager.GetLeaveListByCompany(SessionManager.CurrentCompanyId)
                .OrderBy(x => x.Order).ToList();

            leaveList.Add(new LLeaveType { LeaveTypeId = 0, Title = "Unpaid Leave" });

            if (list.Count > 0)
                total = list[0].TotalRows.Value;

            string idList = String.Join(",", list.Select(x => x.EmployeeId.ToString()).ToList().ToArray());

            List<Report_HR_YearlyLeaveBalanceDetailsResult> leaveDetails = PayrollDataContext.Report_HR_YearlyLeaveBalanceDetails
                (PayrollPeriodId, PayrollPeriodEndId, currentLeavePeriodIdForAccural, startDateLeaveTaken, endDateLeaveTaken,
                idList, lastYearOrYearOpeningPayrollPeriodId, daysDiff, remainingMonth, SessionManager.CurrentCompanyId, yearStart, yearEnd).ToList();

            foreach (Report_HR_YearlyLeaveBalanceResult item in list)
            {
                item.Process(leaveList, leaveDetails);
            }

            return list;
        }

        public static List<Report_HR_YearlyLeaveBalanceDetailsResult> GetYearlyLeaveReportAsList(
           int branchid, int depId, int levelid, int desigId, int empId, int currentPage, int pageSize,
            string sortBy, ref int total,
            int? PayrollPeriodId, int? PayrollPeriodEndId,
            int? currentLeavePeriodIdForAccural, DateTime? startDateLeaveTaken, DateTime? endDateLeaveTaken,
            string empName, int? lastYearOrYearOpeningPayrollPeriodId, DateTime yearStart, DateTime yearEnd
            , int pageType, int managerId)
        {

            int daysDiff = 0; int remainingMonth = 0;
            PayrollPeriod payrollPeriod = CommonManager.GetLastPayrollPeriod();
            if (payrollPeriod.Month != SessionManager.CurrentCompany.LeaveStartMonth.Value)
            {

                daysDiff = LeaveAttendanceManager.GetYearlyLeaveMiddleJoiningDays(payrollPeriod, ref remainingMonth);
            }


            List<Report_HR_YearlyLeaveBalanceResult> list =
                PayrollDataContext.Report_HR_YearlyLeaveBalance
                (yearStart, yearEnd, empName, branchid, depId, empId, currentPage, pageSize, sortBy, pageType, managerId)
                .ToList();

            List<LLeaveType> leaveList = LeaveAttendanceManager.GetLeaveListByCompany(SessionManager.CurrentCompanyId)
                .OrderBy(x => x.Order).ToList();

            leaveList.Add(new LLeaveType { LeaveTypeId = 0, Title = "Unpaid Leave" });

            if (list.Count > 0)
                total = list[0].TotalRows.Value;

            string idList = String.Join(",", list.Select(x => x.EmployeeId.ToString()).ToList().ToArray());

            // if not employee listing from above even after employee selection then do not process
            if (empId != -1 && string.IsNullOrEmpty(idList))
                idList = empId.ToString();

            List<Report_HR_YearlyLeaveBalanceDetailsResult> leaveDetails = PayrollDataContext.Report_HR_YearlyLeaveBalanceDetails
                (PayrollPeriodId, PayrollPeriodEndId, currentLeavePeriodIdForAccural, startDateLeaveTaken, endDateLeaveTaken,
                idList, lastYearOrYearOpeningPayrollPeriodId, daysDiff, remainingMonth, SessionManager.CurrentCompanyId, yearStart, yearEnd).ToList();



            return leaveDetails;
        }

        public static List<Report_HR_YearlyLeaveBalanceResult> GetPeriodicLeaveTakenReport(
           int branchid, int depId, int levelid, int desigId, int empId, int currentPage, int pageSize,
            string sortBy, ref int total,
            DateTime start, DateTime end, DateTime? PayrollStartDate, DateTime? PayrollEndDate,
            DateTime? startDateLeaveTaken, DateTime? endDateLeaveTaken)
        {



            List<Report_HR_YearlyLeaveBalanceResult> list =
                PayrollDataContext.Report_HR_YearlyLeaveBalance
                (start, end, "", branchid, depId, empId, currentPage, pageSize, sortBy, (int)PageViewType.Admin, 0)
                .ToList();

            List<LLeaveType> leaveList = LeaveAttendanceManager.GetLeaveListByCompany(SessionManager.CurrentCompanyId)
                .OrderBy(x => x.Order).ToList();

            leaveList.Add(new LLeaveType { LeaveTypeId = 0, Title = "Unpaid Leave" });

            if (list.Count > 0)
                total = list[0].TotalRows.Value;

            string idList = String.Join(",", list.Select(x => x.EmployeeId.ToString()).ToList().ToArray());

            List<Report_HR_PeriodicLeaveTakenDetailsResult> leaveDetails = PayrollDataContext.Report_HR_PeriodicLeaveTakenDetails
                (PayrollStartDate, PayrollEndDate, startDateLeaveTaken, endDateLeaveTaken, idList).ToList();

            foreach (Report_HR_YearlyLeaveBalanceResult item in list)
            {
                item.ProcessLeaveTaken(leaveList, leaveDetails);
            }

            return list;
        }


        public static List<Report_GetEmployeeOTCalcResult> GetEmployeeOTCalculation(
           int branchid, int depId, int levelid, int desigId, int empId, DateTime? start, DateTime? end, int currentPage, int pageSize, string sortBy, ref int total)
        {
            //int? incomeID1 = null;
            //int? incomeID2 = null;
            //int? incomeID3 = null;
            //int? incomeID4 = null;
            //int? incomeID5 = null;

            //SetIncomeListForEmployeeCurrentPayReport(ref incomeID1, ref incomeID2, ref incomeID3, ref incomeID4, ref incomeID5);

            List<Report_GetEmployeeOTCalcResult> list =
                PayrollDataContext.Report_GetEmployeeOTCalc
                (branchid, depId, empId, levelid, desigId, currentPage, pageSize, sortBy, start, end)
                .ToList();

            if (list.Count > 0)
                total = list[0].TotalRows.Value;

            return list;
        }

        public static List<Report_GetEmployeeCountDetailsResult> GetEmployeeCountDetails(string type, string value
            , int branchid, int depId, int levelid, int desigId)
        {
            List<Report_GetEmployeeCountDetailsResult> list =
               BLL.BaseBiz.PayrollDataContext.Report_GetEmployeeCountDetails(type, value, branchid, depId, levelid, desigId).ToList();
            int sn = 0;

            foreach (Report_GetEmployeeCountDetailsResult item in list)
                item.SN = ++sn;

            return list;
        }
        public static List<EmployeeServiceBo> GetEmployeeServicePeriodByJobStatus(int jbStatusId, DateTime Todate)
        {

            List<int> _listEmployeByJobStatus = new List<int>();
            _listEmployeByJobStatus = GetEmployeeByJobStatus(jbStatusId);
            List<EEmployee> _EmployeeList = null;
            if (jbStatusId == 0)//ALL Employee
            {
                _EmployeeList = PayrollDataContext.EEmployees.Where(x => x.IsRetired == false && x.IsResigned == false).ToList();
            }
            else
                _EmployeeList = PayrollDataContext.EEmployees.Where(i => _listEmployeByJobStatus.Contains(i.EmployeeId) && i.IsRetired == false && i.IsResigned == false).ToList();

            return (
               from e in _EmployeeList
               join b in PayrollDataContext.Branches on e.BranchId equals b.BranchId
               //where e.EmployeeId == 1 || e.EmployeeId == 2
               orderby e.Name
               select new EmployeeServiceBo
               {
                   Name = e.Name,
                   EmployeeId = e.EmployeeId,
                   JoinDateEng = e.ECurrentStatus.OrderBy(x => x.FromDateEng).First().FromDateEng,
                   JoinDateNepali = e.ECurrentStatus.OrderBy(x => x.FromDateEng).First().FromDate,
                   Branch = e.Branch.Name,
                   Department = e.Department.Name,
                   Designation = e.EDesignation.Name,
                   Level = e.EDesignation.BLevel.Name,
                   ServicePeriod = GetServicePeriodUptoDateByEmployeeID(e.EmployeeId, Todate),
                   Status = JobStatus.GetValueForDisplay(e.ECurrentStatus.OrderByDescending(x => x.FromDateEng).First().CurrentStatus).ToString(),
        }



               ).OrderBy(x => x.JoinDateEng).ToList();

        }


        public static ECurrentStatus GetFirstStatus(int employeeId)
        {
            return PayrollDataContext.ECurrentStatus.OrderBy(x => x.FromDateEng).FirstOrDefault(x => x.EmployeeId == employeeId);
        }

        public static ServicePeroid GetServicePeroidDate(int employeeId)
        {
            // First check for Hire
            ServicePeroid entity = new ServicePeroid();

            EmployeeServiceHistory reHire = PayrollDataContext.EmployeeServiceHistories
                .Where(x => x.EmployeeId == employeeId && x.EventID == (int)ServiceEventType.ReAppointment)
                .OrderByDescending(x => x.DateEng).FirstOrDefault();

            if (reHire != null)
            {
                entity.FromDateEng = reHire.DateEng;
                entity.FromDate = reHire.Date;

                entity.CurrentStatus = reHire.StatusId.Value;
                //return entity;

            }
            else
            {
                ECurrentStatus status = PayrollDataContext.ECurrentStatus.OrderBy(x => x.FromDateEng).FirstOrDefault(x => x.EmployeeId == employeeId);

                entity.FromDate = status.FromDate;
                entity.FromDateEng = status.FromDateEng;
                entity.CurrentStatus = status.CurrentStatus;
            }



            // if already retired or marked as retirement then retirement date
            EEmployee emp = PayrollDataContext.EEmployees.FirstOrDefault(x => x.EmployeeId == employeeId);
            if (emp.IsRetired != null && emp.IsRetired.Value)
            {
                entity.ToDate = emp.EHumanResources[0].DateOfRetirement;
                entity.ToDateEng = emp.EHumanResources[0].DateOfRetirementEng;
            }
            else if (emp.IsResigned != null && emp.IsResigned.Value)
            {
                entity.ToDate = emp.EHumanResources[0].DateOfResignation;
                entity.ToDateEng = emp.EHumanResources[0].DateOfResignationEng;
            }
            else
            {
                entity.ToDateEng = BLL.BaseBiz.GetCurrentDateAndTime();
                entity.ToDate = GetAppropriateDate(entity.ToDateEng.Value);
            }

            return entity;
        }
        public List<ECurrentStatus> GetCurrentStatuses(int employeeId)
        {
            return PayrollDataContext.ECurrentStatus.Where(
                s => s.EmployeeId == employeeId).OrderBy(s => s.FromDateEng).ToList();
        }


        public static ECurrentStatus GetStatusStart(int empId, int statusId)
        {
            return
                PayrollDataContext.ECurrentStatus
                .Where(x => x.EmployeeId == empId && x.CurrentStatus >= statusId)
                .OrderBy(x => x.FromDateEng)
                .FirstOrDefault();
        }

        public ECurrentStatus GetCurrentLastStatus(int employeeId)
        {
            return PayrollDataContext.ECurrentStatus.Where(
                s => s.EmployeeId == employeeId).OrderByDescending(s => s.FromDateEng).FirstOrDefault();
        }
        public ECurrentStatus GetStatus(int statusID)
        {
            return PayrollDataContext.ECurrentStatus.SingleOrDefault(
                s => s.ECurrentStatusId == statusID);
        }
        public int GetStatusCount(int employeeId)
        {
            return PayrollDataContext.ECurrentStatus.Where(
                s => s.EmployeeId == employeeId).Count();
        }

        public static List<int> GetEmployeeByJobStatus(int JobStatus)
        {
            // return PayrollDataContext.ECurrentStatus.Where(x => x.CurrentStatus == status).ToList();

            List<int> _list = new List<int>();

            _list = (
               (from _fields in PayrollDataContext.ECurrentStatus.Where(x => x.CurrentStatus == JobStatus).ToList()
                select _fields.EmployeeId).ToList()
                  );
            return _list.Distinct().ToList();

        }





        public static string GetServicePeriodUptoDateByEmployeeID(int EmployeeID, DateTime Todate)
        {
            int years, months, days, hours;
            int minutes, seconds, milliseconds;

            DateTime joinOrReAppointedDate;

            ECurrentStatus firstStatus = BaseBiz.PayrollDataContext.ECurrentStatus.Where(x => x.EmployeeId == EmployeeID)
                .OrderBy(x => x.FromDateEng).FirstOrDefault();

            joinOrReAppointedDate = firstStatus.FromDateEng;

            EmployeeServiceHistory reHire = PayrollDataContext.EmployeeServiceHistories
                .Where(x => x.EmployeeId == EmployeeID && x.EventID == (int)ServiceEventType.ReAppointment)
                .OrderByDescending(x => x.DateEng).FirstOrDefault();

            if (reHire != null)
            {
                joinOrReAppointedDate = reHire.DateEng;

            }

            if (firstStatus != null)
            {
                string WorkingFor = string.Empty;
                NewEntityHelper.GetElapsedTime(joinOrReAppointedDate, Todate, out years, out months, out days, out hours, out minutes, out seconds, out milliseconds);
                string text = "";
                if (years != 0)
                    text = " " + years + " years";
                if (months != 0)
                    text += " " + months + " months";
                if (days != 0)
                    text += " " + days + " days";

                WorkingFor += text;

                return WorkingFor;
                //WorkingFor += ", Since " +
                //    (HttpContext.Current.SessionManager.CurrentCompany.IsEnglishDate ? firstStatus.FromDateEng.ToShortDateString() : firstStatus.FromDate + " (" + firstStatus.FromDateEng.ToShortDateString() + ")");

            }
            else return "";

        }
        public static string GetServicePeriodUptoDateByEmployeeID(DateTime startDate, DateTime Todate)
        {
            int years, months, days, hours;
            int minutes, seconds, milliseconds;


            string WorkingFor = string.Empty;
            NewEntityHelper.GetElapsedTime(startDate, Todate, out years, out months, out days, out hours, out minutes, out seconds, out milliseconds);
            string text = "";
            if (years != 0)
                text = " " + years + " yrs";
            if (months != 0)
                text += " " + months + " mon";
            if (days != 0)
                text += " " + days + " days";

            WorkingFor += text;

            return WorkingFor;
            //WorkingFor += ", Since " +
            //    (HttpContext.Current.SessionManager.CurrentCompany.IsEnglishDate ? firstStatus.FromDateEng.ToShortDateString() : firstStatus.FromDate + " (" + firstStatus.FromDateEng.ToShortDateString() + ")");



        }
        public static int? GetEmployeeCurrentStatus(int employeeId)
        {

            PayrollPeriod lastPayroll = CommonManager.GetLastPayrollPeriod();

            if (lastPayroll == null)
                return null;

            return PayrollDataContext.GetEmployeeStatusForPayrollPeriod(lastPayroll.PayrollPeriodId, employeeId);
        }

        public string GetCurrentStatus(int employeeId)
        {
            int empId = employeeId;
            //EmployeeManager empMgr = new EmployeeManager();
            List<ECurrentStatus> statuses = GetCurrentStatuses(empId);
            StringBuilder str = new StringBuilder("<table class='table table-bordered table-condensed table-striped table-primary'>");
            str.Append(" <thead><tr> <th style='width:90px;text-align:left'>Status</th> <th style='width:90px;text-align:left'>From Date</th> <th style='width:90px;text-align:left'>To Date</th> <th style='width:150px;text-align:left'>Note</th> <th style='width:60px;text-align:center' class='editDeleteTH'> </th> </tr> </thead>");

            bool first = true;
            for (int i = 0; i < statuses.Count; i++)
            {
                ECurrentStatus status = statuses[i];

                if (first)
                {
                    first = false;
                    continue;

                }
                //if (IsEnglish == false && SessionManager.User.IsEnglishPreferred != null && SessionManager.User.IsEnglishPreferred.Value)
                CustomDate date;
                string dateStr;

                if (IsEnglish == false && SessionManager.User.IsEnglishPreferred != null && SessionManager.User.IsEnglishPreferred)
                {
                    date = CustomDate.GetCustomDateFromString(status.FromDateEng.ToString("dd/MM/yyyy"), true);

                    dateStr = GetInSelectElement(date.Day.ToString(), false) + "/" +
                                     GetInSelectElement(DateHelper.GetMonthShortName(date.Month, true), false) + "/" +
                                     GetInSelectElement(date.Year.ToString(), false);
                }
                else
                {
                    date = CustomDate.GetCustomDateFromString(status.FromDate,
                                                                     SessionManager.CurrentCompany.IsEnglishDate);
                    dateStr = GetInSelectElement(date.Day.ToString(), false) + "/" +
                                     GetInSelectElement(DateHelper.GetMonthShortName(date.Month, IsEnglish), false) + "/" +
                                     GetInSelectElement(date.Year.ToString(), false);
                }


                string toDate = "";
                if (!string.IsNullOrEmpty(status.ToDate))
                {
                    if (IsEnglish == false && SessionManager.User.IsEnglishPreferred != null && SessionManager.User.IsEnglishPreferred)
                    {
                        // sometime to date is null
                        if (status.ToDateEng == null)
                        {
                            status.ToDateEng = GetEngDate(status.ToDate, false);
                        }

                        date = CustomDate.GetCustomDateFromString(status.ToDateEng.Value.ToString("dd/MM/yyyy"), true);
                        toDate = GetInSelectElement(date.Day.ToString(), false) + "/" +
                                     GetInSelectElement(DateHelper.GetMonthShortName(date.Month, true), false) + "/" +
                                     GetInSelectElement(date.Year.ToString(), false);
                    }
                    else
                    {
                        date = CustomDate.GetCustomDateFromString(status.ToDate,
                                                                         SessionManager.CurrentCompany.IsEnglishDate);
                        toDate = GetInSelectElement(date.Day.ToString(), false) + "/" +
                                     GetInSelectElement(DateHelper.GetMonthShortName(date.Month, IsEnglish), false) + "/" +
                                     GetInSelectElement(date.Year.ToString(), false);
                    }
                }
                //CustomDate toDate = CustomDate.GetCustomDateFromString(status.ToDate,
                //                                                       SessionManager.CurrentCompany.IsEnglishDate);
                //string dateTo = GetInSelectElement(toDate.Day.ToString(), false) + "/" +
                //                GetInSelectElement(DateHelper.GetMonthShortName(toDate.Month, IsEnglish), false) + "/" +
                //                GetInSelectElement(toDate.Year.ToString(), false);

                string displayStatus = (i == statuses.Count - 1 ? "inline" : "none");
                if (CommonManager.IsServiceHistoryEnabled)
                    displayStatus = "none";
                str.AppendFormat(
                    @"<tr class='{7}'>
                        <td class='statusWidth'>  {0}</td><td>&nbsp;{1}</td> <td> &nbsp;{2} </td>
<td>
                        {8}
                        </td>                    
                        <td style='text-align:center'>
                            <input type='image' style='border-width: 0px;display:{3}' onclick='return promoteCall({4},{5},{6});' src='../images/edit.gif' />
                            <input type='image' style='border-width: 0px;display:{3}' onclick='return deleteStatus({4});' src='../images/delete.gif' />
                        </td>
                        
                    </tr>",
                    status.CurrentStatusText,
                    dateStr,
                    toDate,
                    displayStatus,
                    status.ECurrentStatusId,
                    "\"" + status.CurrentStatus + "\"",
                    "\"" + status.FromDate + "\"",
                    (i == 0 ? "odd" : "even"),
                    status.Note
                    );
            }
            str.Append("</table>");
            return str.ToString();
        }

        public string GetAllCurrentStatusForAppraisal(int employeeId)
        {
            int empId = employeeId;
            //EmployeeManager empMgr = new EmployeeManager();
            List<ECurrentStatus> statuses = GetCurrentStatuses(empId);
            StringBuilder str = new StringBuilder("<table class='table table-bordered table-condensed table-striped table-primary' style='width:700px'>");
            str.Append(" <thead><tr> <th style='width:90px;text-align:left'>Status</th> <th style='width:90px;text-align:left'>From</th> <th style='width:90px;text-align:left'>Upto</th> <th style='width:150px;text-align:left'>Duration</th>  </tr> </thead>");

            //bool first = true;
            for (int i = 0; i < statuses.Count; i++)
            {
                ECurrentStatus status = statuses[i];




                string dateStr = status.FromDateEng.ToShortDateString();

                string toDate = "";
                if (status.ToDateEng != null)
                {
                    status.TempToDate = status.ToDateEng;
                    toDate = status.ToDateEng.Value.ToShortDateString();
                }
                else if (statuses.Count - 1 > i)
                {
                    status.TempToDate = statuses[i + 1].FromDateEng.AddDays(-1);
                    toDate = statuses[i + 1].FromDateEng.AddDays(-1).ToShortDateString();
                }

                //CustomDate toDate = CustomDate.GetCustomDateFromString(status.ToDate,
                //                                                       SessionManager.CurrentCompany.IsEnglishDate);
                //string dateTo = GetInSelectElement(toDate.Day.ToString(), false) + "/" +
                //                GetInSelectElement(DateHelper.GetMonthShortName(toDate.Month, IsEnglish), false) + "/" +
                //                GetInSelectElement(toDate.Year.ToString(), false);

                string duration = "";
                if (status.TempToDate == null)
                    status.TempToDate = GetCurrentDateAndTime();

                if (status.TempToDate != null)
                {
                    int years, months, days, hours;
                    int minutes, seconds, milliseconds;
                    NewHelper.GetElapsedTime(status.FromDateEng, status.TempToDate.Value, out years, out months, out days, out hours, out minutes, out seconds, out milliseconds);

                    if (years != 0)
                        duration = " " + years + " years";
                    if (months != 0)
                        duration += " " + months + " months";
                    if (days != 0)
                        duration += " " + days + " days";




                }

                string displayStatus = (i == statuses.Count - 1 ? "inline" : "none");
                str.AppendFormat(
                    @"<tr class='{7}'>
                        <td class='statusWidth'>  {0}</td><td>&nbsp;{1}</td> <td> &nbsp;{2} </td>
<td>
                        {8}
                        </td>                    
                        <td style='text-align:center;display:none'>
                            <input type='image' style='border-width: 0px;display:{3}' onclick='return promoteCall({4},{5},{6});' src='../images/edit.gif' />
<input type='image' style='border-width: 0px;display:{3}' onclick='return deleteStatus({4});' src='../images/delete.gif' />
                        </td>
                        
                    </tr>",
                    status.CurrentStatusText,
                    dateStr,
                    toDate,
                    displayStatus,
                    status.ECurrentStatusId,
                    "\"" + status.CurrentStatus + "\"",
                    "\"" + status.FromDate + "\"",
                    (i == 0 ? "odd" : "even"),
                    duration
                    );
            }
            str.Append("</table>");
            return str.ToString();
        }

        private string GetInSelectElement(string value, bool isStatus)
        {
            if (isStatus)

                return string.Format("<span class='statusWidth' style='font-weight:bold' >{0}</span>", value);
            else
                return string.Format("<span style='font-weight:bold' >{0}</span>", value);
        }


        #region "Contribution"
        public string GetContribution(int employeeId)
        {
            //            List<EContribution> items = PayrollDataContext.EContributions.Where(
            //                e => e.EmployeeId == employeeId).ToList();
            //            StringBuilder str = new StringBuilder("<table style='width:510px' class='grid'>");
            //            str.Append("<tr> <th style='width:220px'>Description</th> <th>%</th> <th> Start Date </th> <th> End Date </th> <th></th> </tr> ");
            //            double sum = 0;
            //            foreach (EContribution item in items)
            //            {
            //                str.AppendFormat(@"<tr> <td>{0}</td> <td>{1}</td> <td>{2}</td> <td>{3}</td> 
            //                <td>                     
            //                     <input type='image' style='border-width: 0px;' onclick='deleteContributionCall({4});return false;' src='../images/delete.gif' />                           
            //                </td>            
            //                </tr>",
            //                    item.Description, Util.FormatPercentForInput(item.Percentage), item.StartDate, item.EndDate,item.ContributionId);

            //                sum += item.Percentage.Value;
            //            }

            //            str.AppendFormat(@"<tr> <td></td> <td> <b>{0} %</b> </td> <td></td> <td></td> 
            //                <td>                     
            //                     </td>            
            //                </tr>", Util.FormatPercentForInput(sum));

            //            str.Append("</table>");
            //            return str.ToString();
            return string.Empty;
        }
        public bool Save(EContribution entity)
        {

            System.Nullable<double> totalPercent =
                (from e in PayrollDataContext.EContributions
                 where e.EmployeeId == entity.EmployeeId
                 select e.Percentage).Sum();

            if (totalPercent == null)
            {
                totalPercent = 0.0;
            }
            if (totalPercent + entity.Percentage > 100)
                throw new ArgumentException("Percentage exceeded.");


            PayrollDataContext.EContributions.InsertOnSubmit(entity);
            return SaveChangeSet();
        }
        public bool Delete(EContribution entity)
        {
            EContribution dbEntity = PayrollDataContext.EContributions.SingleOrDefault(
                e => e.ContributionId == entity.ContributionId);
            if (dbEntity == null)
                return false;


            PayrollDataContext.EContributions.DeleteOnSubmit(dbEntity);
            PayrollDataContext.SubmitChanges();
            return true;
        }

        public List<GetPrefixedEmpNamesResult> GetPrefixedEmplyeeNames(string prefix, string customRoleDeparmentList, bool isRetAlso)
        {
            return PayrollDataContext.GetPrefixedEmpNames
                 (prefix, SessionManager.CurrentCompanyId, customRoleDeparmentList, isRetAlso).ToList();


        }

        public List<GetRetiredPrefixedEmpNamesWithIDResult> GetRetiredPrefixedEmpNamesWithID(bool isID, int ein, string prefix, string customRoleDeparmentList, bool isRetAlso)
        {
            return PayrollDataContext.GetRetiredPrefixedEmpNamesWithID
                 (prefix, isID, ein, SessionManager.CurrentCompanyId, customRoleDeparmentList).ToList();


        }


        public List<GetPrefixedEmpNamesWithIDResult> GetPrefixedEmplyeeNamesWithID(bool isID, int ein, string prefix, string customRoleDeparmentList, bool isRetAlso)
        {
            return PayrollDataContext.GetPrefixedEmpNamesWithID
                 (prefix, isID, ein, SessionManager.CurrentCompanyId, customRoleDeparmentList, isRetAlso).ToList();


        }
        public List<GetPrefixedEmpNamesWithIDAndINoResult> GetPrefixedEmplyeeNamesWithIDAndINo(bool isID, int ein, string prefix, string customRoleDeparmentList, bool isRetAlso)
        {
            return PayrollDataContext.GetPrefixedEmpNamesWithIDAndINo
                 (prefix, isID, ein, SessionManager.CurrentCompanyId, customRoleDeparmentList, isRetAlso).ToList();


        }
        public List<GetPrefixedEmpIdCardNoResult> GetPrefixedEmplyeeIDCardNo(string prefix, string customRoleDeparmentList)
        {
            return PayrollDataContext.GetPrefixedEmpIdCardNo
                 (prefix, SessionManager.CurrentCompanyId, customRoleDeparmentList).ToList();


        }

        #endregion


        #region "Grade History"



        #endregion


        public static List<BLL.BO.LeaveApprovalEmployeeBO> GetEmployeeByDepartmentId(int p)
        {
            var result = (from e in PayrollDataContext.EEmployees.Where(x => x.DepartmentId == p)
                          select new BLL.BO.LeaveApprovalEmployeeBO
                          {
                              EmployeeId = e.EmployeeId,
                              Name = e.Name,
                              Email = e.EAddresses.SingleOrDefault().CIEmail
                          }).OrderBy(e => e.Name).ToList();
            return result;
        }
        public static List<BLL.BO.LeaveApprovalEmployeeBO> GetEmployeeForLeaveSetting()
        {
            var result = (from e in PayrollDataContext.EEmployees
                          select new BLL.BO.LeaveApprovalEmployeeBO
                          {
                              EmployeeId = e.EmployeeId,
                              Name = e.Name + " - " + e.EmployeeId,
                              Email = e.EAddresses.SingleOrDefault().CIEmail
                          }).OrderBy(e => e.Name).ToList();
            return result;
        }
        public static bool IsDepHeadOrManagerToViewEmployeeList(int employeeId)
        {


            if (PayrollDataContext.Departments.Any(x => x.HeadEmployeeId == employeeId))
                return true;

            if (PayrollDataContext.LeaveProjects.Any(x => x.ProjectManagerEmployeeId == employeeId))
                return true;

            return false;
        }
        public static List<GetEmployeeListForDepartmentHeaderManagerResult> GetEmpListForDepartmentHeadOrManagerView()
        {
            return PayrollDataContext.GetEmployeeListForDepartmentHeaderManager(
                SessionManager.CurrentLoggedInEmployeeId, SessionManager.CurrentCompanyId).ToList();
        }

        public static List<string> GetEmployeeUserNameList(int p)
        {
            var result = (
                            from name in PayrollDataContext.EEmployees
                            join users in PayrollDataContext.UUsers on name.EmployeeId equals users.EmployeeId
                            where name.CompanyId == p
                            select users.UserName
                         )
                         .ToList();
            return result;
        }

        public static List<TextValue> GetEmployeeUserNamesByCompany(int p)
        {
            var result = (
                            from name in PayrollDataContext.EEmployees
                            join users in PayrollDataContext.UUsers on name.EmployeeId equals users.EmployeeId
                            where name.CompanyId == p
                            select (new TextValue
                            {
                                Text = name.Name,
                                Value = users.UserName
                            })
                         )
                         .ToList();
            return result;
        }

        public static List<string> GetEmployeeUserNameListByBranch(int branchId)
        {
            var result = (
                            from name in PayrollDataContext.EEmployees
                            join users in PayrollDataContext.UUsers on name.EmployeeId equals users.EmployeeId
                            where name.BranchId == branchId
                            select users.UserName
                         )
                         .ToList();
            return result;
        }

        public static List<string> GetEmployeeUserNameListByDepartment(int departmenntId)
        {
            var result = (
                            from name in PayrollDataContext.EEmployees
                            join users in PayrollDataContext.UUsers on name.EmployeeId equals users.EmployeeId
                            where name.DepartmentId == departmenntId
                            select users.UserName
                         )
                         .ToList();
            return result;
        }

        public static List<GetEmployeesCatelogueResult> GetEmployeeCatalogue(int start, int pagesize, string sort, bool GridOrThumbnail, string MemberName
            , int branch, int dep, int desig, string idCardNoSearchText, int levelId, int minStatusID, bool statusOnly)
        {

            //List<GetEmployeesCatelogueResult> resultSet = PayrollDataContext.GetEmployeesCatelogue(start, pagesize, sort, SessionManager.CurrentCompanyId, GridOrThumbnail, MemberName
            //    , branch, dep, desig).ToList();

            List<GetEmployeesCatelogueResult> resultSet = PayrollDataContext.GetEmployeesCatelogue(start, pagesize, sort, SessionManager.CurrentCompanyId, GridOrThumbnail, MemberName,
                idCardNoSearchText, branch, dep, desig, levelId, minStatusID, statusOnly).ToList();

            foreach (GetEmployeesCatelogueResult item in resultSet)
            {
                if (string.IsNullOrEmpty(item.UrlPhoto))
                {
                    if (item.Title.ToLower() == "mr")
                        item.UrlPhoto = "../images/male.png";
                    else
                        item.UrlPhoto = "../images/female.png";
                }

                if (!string.IsNullOrEmpty(item.CIMobileNo))
                {
                    item.Mobile = item.CIMobileNo + " (O)";

                    if (!string.IsNullOrEmpty(item.PersonalMobile))
                    {
                        item.Mobile += " " + item.PersonalMobile + " (P)";
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(item.PersonalMobile))
                    {
                        item.Mobile += item.PersonalMobile + " (P)";
                    }
                }


                // Phone
                if (!string.IsNullOrEmpty(item.CIPhoneNo))
                    item.Telephone = item.CIPhoneNo;
                if (!string.IsNullOrEmpty(item.Extension))
                    item.Telephone += " - " + item.CIPhoneNo;
                if (item.Telephone != "")
                    item.Telephone += " (O)";

                if (!string.IsNullOrEmpty(item.PersonalPhone))
                    item.Telephone += " " + item.PersonalPhone + " (R)";

                item.Since = item.JoinDate.Value.ToShortDateString();
            }
            return resultSet;
        }

        public static List<Resource> LoadComanyResourceTree()
        {
            List<Resource> returnList = new List<Resource>();
            returnList = PayrollDataContext.Resources.ToList();
            foreach (Resource item in returnList)
            {
                item.ResourceCategoryName = item.ResourceCategory.Name;
            }
            return returnList;
        }

        public static List<ECurrentStatus> GetCurrentStatusHist(int employeeId)
        {
            List<ECurrentStatus> statuses = new EmployeeManager().GetCurrentStatuses(employeeId);

            List<ECurrentStatus> statusList = new List<ECurrentStatus>();

            //bool first = true;
            //foreach (ECurrentStatus status in statuses)
            for (int i = 0; i < statuses.Count; i++)
            {
                ECurrentStatus status = statuses[i];

                /*if (first)
                {
                    first = false;
                    continue;
                }*/

                string levelName = "";
                int? desiId = PayrollDataContext.GetEmployeeCurrentPositionForDate(status.FromDateEng, status.EmployeeId);
                if (desiId != null)
                {
                    EDesignation desc = CommonManager.GetDesigId(desiId.Value);
                    levelName = desc.BLevel.Name;
                }
                ECurrentStatus newStatus = new ECurrentStatus()
                {
                    ECurrentStatusId = status.ECurrentStatusId,
                    StatusName = status.CurrentStatusText,
                    FromDate = status.FromDate,
                    ToDate = status.ToDate,
                    Note = status.Note,
                    LevelPosition = levelName
                };
                statusList.Add(newStatus);


                int years, months, days, hours;
                int minutes, seconds, milliseconds;


                DateTime toDate = DateTime.Now.Date;

                // if next row exists the pick one day before from date of next row
                if (status.ToDateEng != null)
                    toDate = status.ToDateEng.Value;
                else if (statuses.Count - 1 > i)
                    toDate = statuses[i + 1].FromDateEng.AddDays(-1);

                if (toDate > DateTime.Now.Date)
                    toDate = DateTime.Now.Date;

                NewHelper.GetElapsedTime(status.FromDateEng, toDate, out years, out months, out days, out hours, out minutes, out seconds, out milliseconds);
                string text = "";
                if (years != 0)
                    text = " " + years + " Y";
                if (months != 0)
                    text += " " + months + " M";

                if (text != "")
                    text += " and ";

                if (days != 0)
                    text += days + " D";


                newStatus.TimeElapsed = text;
            }

            return statusList;
        }

        public List<GetPrefixedEmpNamesResult> GetPrefixedEmplyeeNamesWithIDForHr(bool isID, int ein, string prefix, string customRoleDeparmentList, bool isRetAlso)
        {
            return PayrollDataContext.GetPrefixedEmpNames(prefix, SessionManager.CurrentCompanyId,
                customRoleDeparmentList, false).ToList();

        }


        //public static string GetEmpCountByBranchAndDepartMentName(string BranchName, string DepartmentName, DateTime GivenDate)
        //{
        //    Branch branch = BranchManager.getBranchByBranchName(BranchName.Trim());
        //    Department department = DepartmentManager.getDepartmentByName(DepartmentName.Trim());
        //    if (branch != null && department != null)
        //    {
        //        return EmployeeManager.GetHeadCount(0, 99999, "", branch.BranchId, department.DepartmentId, GivenDate).Count.ToString();
        //    }
        //    return "0";
        //}

        public List<ProbationHistory> GetEmployeeProbationList(int employeeId)
        {
            return PayrollDataContext.ProbationHistories.Where(
                s => s.EmployeeId == employeeId).OrderBy(s => s.ProbationId).ToList();
        }

        public string GetProbationList(int employeeId)
        {
            int empId = employeeId;
            //EmployeeManager empMgr = new EmployeeManager();
            List<ProbationHistory> statuses = GetEmployeeProbationList(empId);


            StringBuilder str = new StringBuilder("<table class='table table-bordered table-condensed table-striped table-primary'>");
            str.Append(" <thead><tr><th style='width:60px;text-align:left'>Letter No</th>  <th style='width:110px;text-align:left'>Probation End Date</th> <th style='width:150px;text-align:left'>Note</th> <th style='width:50px;text-align:center' class='editDeleteTH'> </th> </tr> </thead>");

            //bool first = true;
            for (int i = 0; i < statuses.Count; i++)
            {
                ProbationHistory status = statuses[i];

                //if (first)
                //{
                //    first = false;
                //    continue;

                //}



                string toDate = "";
                CustomDate date = null;
                {
                    date = CustomDate.GetCustomDateFromString(status.ToDate,
                                                                     SessionManager.CurrentCompany.IsEnglishDate);
                    toDate = GetInSelectElement(date.Year.ToString(), false) + "/" +
                                 GetInSelectElement(DateHelper.GetMonthShortName(date.Month, IsEnglish), false) + "/" +
                                 GetInSelectElement(date.Day.ToString(), false);
                }
                //CustomDate toDate = CustomDate.GetCustomDateFromString(status.ToDate,
                //                                                       SessionManager.CurrentCompany.IsEnglishDate);
                //string dateTo = GetInSelectElement(toDate.Day.ToString(), false) + "/" +
                //                GetInSelectElement(DateHelper.GetMonthShortName(toDate.Month, IsEnglish), false) + "/" +
                //                GetInSelectElement(toDate.Year.ToString(), false);

                string displayStatus = (i == statuses.Count - 1 ? "inline" : "none");
                str.AppendFormat(
                    @"<tr class='{5}'>
                       <td> &nbsp;{7} </td>
                        <td> &nbsp;{0} </td>
<td>
                        {6}
                        </td>                    
                        <td style='text-align:center'>
                            <input type='image' style='border-width: 0px;display:{1}' onclick='return ChangeProbation({2},{3});' src='../images/edit.gif' />
<input type='image' style='border-width: 0px;display:{1}' onclick='return deleteProbation({2});' src='../images/delete.gif' />
                        </td>
                        
                    </tr>",

                    toDate,
                    displayStatus,
                    status.ProbationId,
                    "\"" + 0 + "\"",
                    "",
                    (i == 0 ? "odd" : "even"),
                    status.Note,
                    status.LetterNo

                    );
            }
            str.Append("</table>");
            return str.ToString();
        }

        public ProbationHistory GetProbation(int statusID)
        {
            return PayrollDataContext.ProbationHistories.SingleOrDefault(
                s => s.ProbationId == statusID);
        }

        public ResponseStatus Save(ProbationHistory entity)
        {
            ResponseStatus status = new ResponseStatus();

            //validate for prev status




            //PayrollDataContext.ChangeMonetories.InsertOnSubmit(GetMonetoryChangeLog(entity.EmployeeId, (byte)MonetoryChangeLogTypeEnum.Status,
            //    "", lastStatus == null ? "" : lastStatus.CurrentStatusText + "," + lastStatus.FromDate +
            //  (lastStatus.DefineToDate == null || lastStatus.DefineToDate == false ? "" : (lastStatus.ToDate == null ? "" : "," + lastStatus.ToDate)),
            //    entity.CurrentStatusText + "," + entity.FromDate, LogActionEnum.Add));


            entity.CreatedBy = SessionManager.User.UserID;
            entity.CreatedOn = DateTime.Now;

            PayrollDataContext.ProbationHistories.InsertOnSubmit(entity);
            SaveChangeSet();

            return status;
        }

        public ResponseStatus Update(ProbationHistory entity)
        {
            ResponseStatus status = new ResponseStatus();

            ProbationHistory dbEntity = PayrollDataContext.ProbationHistories.SingleOrDefault(c => c.ProbationId == entity.ProbationId);
            //may be deleted
            if (dbEntity == null)
                return status;








            dbEntity.FromDate = entity.FromDate;
            //dbEntity.ToDate = entity.ToDate;
            dbEntity.FromDateEng = entity.FromDateEng;
            //  dbEntity.ToDateEng = entity.ToDateEng;

            dbEntity.Note = entity.Note;
            dbEntity.ToDate = entity.ToDate;
            dbEntity.ToDateEng = entity.ToDateEng;
            dbEntity.LetterNo = entity.LetterNo;


            UpdateChangeSet();

            return status;
        }

        public static int DeleteProbation(int statusId)
        {
            ProbationHistory dbEntity = PayrollDataContext.ProbationHistories.SingleOrDefault(c => c.ProbationId == statusId);

            int empId = dbEntity.EmployeeId;




            PayrollDataContext.ProbationHistories.DeleteOnSubmit(dbEntity);
            DeleteChangeSet();
            return empId;

        }

        public List<GetPrefixedEmpNamesWithIDAndINoAnPhotoUrlResult> GetPrefixedEmplyeeNamesWithIDAndINoWithEmpPhoto(bool isID, int ein, string prefix, string customRoleDeparmentList, bool isRetAlso)
        {
            List<GetPrefixedEmpNamesWithIDAndINoAnPhotoUrlResult> list = PayrollDataContext.GetPrefixedEmpNamesWithIDAndINoAnPhotoUrl
                 (prefix, isID, ein, SessionManager.CurrentCompanyId, customRoleDeparmentList, isRetAlso).ToList();

            return list;


        }

        public static List<EEmployee> GetEmployeesByBranchAndDepartmentId(int branchId, int departmentId)
        {
            //return PayrollDataContext.EEmployees
            //    .Where(x => x.BranchId == branchId && x.DepartmentId == departmentId)
            //    .OrderBy(x => x.Name).ToList();
            return
            (
                from e in PayrollDataContext.EEmployees
                join h in PayrollDataContext.EHumanResources on e.EmployeeId equals h.EmployeeId
                where e.BranchId == branchId && e.DepartmentId == departmentId
                &&
                (
                    (
                        e.IsResigned == false ||
                        h.DateOfResignationEng >= DateTime.Now.Date
                        )
                    &&
                     (
                        e.IsRetired == false ||
                        h.DateOfRetirementEng >= DateTime.Now.Date
                        )
                )
                select e
                ).OrderBy(x => x.Name).ToList();
        }

        public static int GetEmployeeCurrentPositionForDate(int EmployeeID)
        {
            return PayrollDataContext.GetEmployeeCurrentPositionForDate(SessionManager.GetCurrentDateAndTime().Date, EmployeeID).Value;
        }

        public List<GetEmployeeListForAssignShiftResult> GetEmployeesByBranchForShiftPlanningByEmpId(int employeeId)
        {
            return PayrollDataContext.GetEmployeeListForAssignShift(employeeId, SessionManager.CurrentCompanyId).ToList();
        }

        public static List<Report_ClericalNonClericalResult> Get_ReportClericalNonClerical(int branchId, int departmentId, int periodId, int employeeId)
        {
            return PayrollDataContext.Report_ClericalNonClerical(branchId, departmentId, periodId, employeeId).ToList();
        }

        public static List<GetEmployeeRetirementDetailsListResult> GetEmpRetirementDetails(int currentPage, int pageSize, int branchId, int departmentId, string nameSearch, int employeeId)
        {
            List<GetEmployeeRetirementDetailsListResult> list = PayrollDataContext.GetEmployeeRetirementDetailsList(currentPage, pageSize, branchId, departmentId, nameSearch, employeeId).ToList();

            List<KeyValue> statusList = new JobStatus().GetMembers();

            foreach (var item in list)
            {
                if (item.StatusID != 0)
                {
                    KeyValue keyValue = statusList.SingleOrDefault(x => x.Key == item.StatusID.ToString());
                    if (keyValue != null)
                        item.StatusName = keyValue.Value;
                }
            }

            return list;
        }


        public static ResponseStatus SaveEmployeeChangeStatusImport(List<ECurrentStatus> list)
        {
            ResponseStatus status = new ResponseStatus();

            SetConnectionPwd(PayrollDataContext.Connection);
            PayrollDataContext.Transaction = PayrollDataContext.Connection.BeginTransaction();

            EmployeeManager empMgr = new EmployeeManager();

            int row = 2;

            try
            {
                foreach (ECurrentStatus empStatus in list)
                {
                    status = empMgr.Save(empStatus);
                    if (!status.IsSuccessType)
                    {
                        PayrollDataContext.Transaction.Rollback();
                        status.IsSuccessType = false;
                        status.ErrorMessage = string.Format("{0} for EIN {1}, row {2}",  status.ErrorMessage.TrimEnd('.'), empStatus.EmployeeId, row); 
                        return status;
                    }

                    row++;
                }

                PayrollDataContext.SubmitChanges();
                PayrollDataContext.Transaction.Commit();


            }
            catch (Exception exp)
            {
                Log.log("Error while importing employee status change.", exp);
                PayrollDataContext.Transaction.Rollback();
                status.IsSuccessType = false;
            }
            finally
            {

                //if (PayrollDataContext.Connection != null)
                //{
                //    PayrollDataContext.Connection.Close();
                //}
            }
            return status;
        }
    }



    /// 
    /// Container to host a list of Category objects
    /// 
    /*
    public class Categories : List<Resource>, IHierarchicalEnumerable
    {
        List<Resource> returnList = null;
        public Categories()
            : base()
        {
            returnList = PayrollDataContext.Resources.ToList();
            foreach (Resource item in returnList)
            {
                item.ResourceCategoryName = item.ResourceCategory.Name;
            }
        }
        #region IHierarchicalEnumerable Members
        public IHierarchyData GetHierarchyData(object enumeratedItem)
        {
            return enumeratedItem as Category;
        }
        #endregion
    }
     */


    public class ClustomEmployeeList
    {

        public string EmployeeId;
        public string Name;
        public string Branch;
        public string Department;
        public string SubDepartment;
        public string Designation;
        public string Status;
        public string IdCardNo;
        public string IsRetiredOrResigned;


    }






}
