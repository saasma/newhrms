﻿using System.Linq;
using DAL;
using Utils.Security;
using NUnit.Framework.SyntaxHelpers;
using System.Collections.Generic;
using System;
using BLL.BO;
using Utils.Calendar;

namespace BLL.Manager
{
    public class ChartData
    {
        public string[] labels;
        public double[] amounts;

    }
    public class DashboardManager : BaseBiz
    {


        public static ChartData GetSalaryExpenses()
        {
            ChartData data= new ChartData();

            List<Dashboard_GetSalaryExpensesResult>
                expenses = PayrollDataContext.Dashboard_GetSalaryExpenses(SessionManager.CurrentCompanyId).ToList();


            //create x & y axis values
            data.labels = new string[expenses.Count];
            data.amounts = new double[expenses.Count];

            int index = 0;
            //foreach (var expense in expenses)
            for (int i = expenses.Count - 1; i >= 0; i--)
            {
                Dashboard_GetSalaryExpensesResult expense = expenses[i];

                data.labels[index] = expense.Name.Remove(3); //extract 3 characters only
                data.amounts[index] = (double)(expense.Amount.Value / 1000);
                index += 1;
            }
            return data;
        }


        public static List<Report_HRLeaveTakenOnADayResult> GetDayLeaveList(DateTime date)
        {
            return
                PayrollDataContext.Report_HRLeaveTakenOnADay(date.Date, date.Date, null).ToList();
                //date, SessionManager.CurrentCompanyId, "2").ToList();
        }

        public static ChartData
            GetEachIncomes()
        {
            ChartData data = new ChartData();
            List<Dashboard_EachIncomeResult> incomes =
                PayrollDataContext.Dashboard_EachIncome(SessionManager.CurrentCompanyId).ToList();

            if (incomes.Count > 0)
            {
                //if income head percent is less than 5, then add in new header named Other
                Dashboard_EachIncomeResult otherHead = new Dashboard_EachIncomeResult();
                otherHead.Title = "Other";
                otherHead.Total = 0;
                for (int i = incomes.Count - 1; i >= 0; i--)
                {
                    Dashboard_EachIncomeResult item = incomes[i];
                    if (item.TotalPercent <= 5)
                    {
                        otherHead.Total += item.Total;
                        incomes.RemoveAt(i);
                    }
                }
                if (otherHead.Total > 0)
                    incomes.Add(otherHead);
            }

            //create x & y axis values
            data.labels = new string[incomes.Count];
            data.amounts = new double[incomes.Count];

            int index = 0;
            //foreach (var expense in expenses)
            for (int i = incomes.Count - 1; i >= 0; i--)
            {
                Dashboard_EachIncomeResult expense = incomes[i];

                data.labels[index] = expense.Title;
                data.amounts[index] = (double)expense.Total.Value;
                index += 1;
            }
            return data;
        }

        public  static List<Dashboard_EachHeaderListResult>
            GetAllHeaders(int payrollPeriodId,out double total)
        {

            //total = (double) PayrollDataContext.Dashboard_GetNetSalary(payrollPeriodId).SingleOrDefault().Total;
            List<Dashboard_EachHeaderListResult> incomes =
                PayrollDataContext.Dashboard_EachHeaderList(payrollPeriodId).ToList();
            total = 0;
            foreach (var dashboardEachHeaderListResult in incomes)
            {
                total += (double) dashboardEachHeaderListResult.Total.Value;
            }

            return incomes;
        }

        public static int TotalEmpRecruited(DateTime startDate, DateTime endDate)
        {
            return PayrollDataContext.Dashboard_EmpRecruited
                (startDate, endDate, SessionManager.CurrentCompanyId).Value;
        }

        public static int TotalEmpRetiring(int payrollPeriodId)
        {
            return PayrollDataContext.Dashboard_EmpRetiring(payrollPeriodId).Value;
        }

        public static int TotalEmpContractToBeCompleting(int payrollPeriodId)
        {
            return PayrollDataContext.Dashboard_ContractToBeCompleted(payrollPeriodId).Value;
        }


        #region "Employee dashboard"
        public static List<TextValue> GetEmployeeThisWeekList()
        {
            List<TextValue> list = new List<TextValue>();

            DayOfWeek day = Utils.Helper.Util.GetCurrentDateAndTime().DayOfWeek;
            int days = day - DayOfWeek.Sunday;

            DateTime FromDate = DateManager.GetStartDate(Utils.Helper.Util.GetCurrentDateAndTime().AddDays(-days));
            DateTime ToDate = DateManager.GetEndDate(FromDate.AddDays(6));

            // get approved leave 
            list = GetEmployeeWeekList( FromDate, ToDate);

            return list; 
        }

        public static List<TextValue> GetEmployeeWeekList(DateTime FromDate, DateTime ToDate)
        {


            int? currentEmployeeLeaveTeamID = null;
            LeaveProjectEmployee leaveProject = PayrollDataContext.LeaveProjectEmployees
                .FirstOrDefault(x => x.EmployeeId == SessionManager.CurrentLoggedInEmployeeId);
            if (leaveProject != null)
                currentEmployeeLeaveTeamID = leaveProject.LeaveProjectId;


            // add approved leave to Happening
            List<TextValue> list = (
                from l in PayrollDataContext.LeaveRequests
                join e in PayrollDataContext.EEmployees on l.EmployeeId equals e.EmployeeId
                join lp in PayrollDataContext.LeaveProjectEmployees on e.EmployeeId equals lp.EmployeeId
                where l.Status == (int)LeaveRequestStatusEnum.Approved
                && 
                (
                    (currentEmployeeLeaveTeamID != null && currentEmployeeLeaveTeamID==lp.LeaveProjectId)
                    ||
                    l.EmployeeId == SessionManager.CurrentLoggedInEmployeeId
                )
                //&& l.EmployeeId != SessionManager.CurrentLoggedInEmployeeId
                &&
                (
                    (l.FromDateEng >= FromDate && l.FromDateEng <= ToDate)
                    ||
                    (l.ToDateEng >= FromDate && l.ToDateEng <= ToDate)
                    ||
                    (FromDate >= l.FromDateEng && FromDate <= l.ToDateEng)
                    ||
                    (ToDate >= l.FromDateEng && ToDate <= l.ToDateEng)
                )
                && l.ToDateEng >= DateTime.Now.Date
                && (l.IsCancelRequest == null || l.IsCancelRequest==false)
                orderby l.FromDateEng
                select new TextValue
                {
                    Text = e.Name,
                    Value = "On Leave, ",
                    Date = l.FromDateEng,
                    OtherDate = l.ToDateEng,
                    EIN = l.EmployeeId
                    //Other =
                    //(
                    //    l.FromDateEng == l.ToDateEng ? l.FromDateEng.Value.ToString("dddd, MMMM d")  : 
                    //        (l.FromDateEng.Value.ToString("dddd, MMMM d")  + " to " +
                    //        l.ToDateEng.Value.ToString("dddd, MMMM d"))
                    //)
                }


                ).ToList();

            foreach (TextValue item in list)
            {
                if (item.Date == item.OtherDate)
                    item.Other = item.Date.Value.ToString("dddd, MMMM d");
                else
                    item.Other = item.Date.Value.ToString("dddd, MMMM d") + " to " +
                            item.OtherDate.Value.ToString("dddd, MMMM d");
            }

            // add birthday to Happening
            //list.AddRange(
            //     (
            //        from e in PayrollDataContext.EEmployees
            //        where PayrollDataContext.IsEmployeeNotRetOrResigned(e.EmployeeId)==true
                  
            //        &&
            //        (
            //            e.DateOfBirthEng != null &&
            //            e.DateOfBirthEng.Value.AddYears(
            //        )
            //        select new TextValue
            //        {
            //            Text = e.Name,
            //            Value = "On Leave, ",
            //            Date = l.FromDateEng
            //        }

            //        ).ToList()
            //    );

            return list;
        }

        public static List<TextValue> Next30DaysAfterThisWeekList()
        {
            List<TextValue> list = new List<TextValue>();

            DayOfWeek day = Utils.Helper.Util.GetCurrentDateAndTime().DayOfWeek;
            int days = day - DayOfWeek.Sunday;

            DateTime FromDate = DateManager.GetStartDate(Utils.Helper.Util.GetCurrentDateAndTime().AddDays(-days));
            DateTime ToDate = DateManager.GetEndDate(FromDate.AddDays(6));

            FromDate = DateManager.GetStartDate(ToDate.AddDays(1));
            ToDate = DateManager.GetEndDate(FromDate.AddDays(29));

            // get approved leave 
            list = GetEmployeeWeekList( FromDate, ToDate);

            return list;
        }



        #endregion

        #region "HR Dashboard"
        public static List<TextValue> GetEmployeeOnLeaveToday(LeaveRequestStatusEnum status)
        {
            DateTime FromDate = DateManager.GetStartDate(DateTime.Now);
            DateTime ToDate = DateManager.GetEndDate(DateTime.Now);

            return (
               from l in PayrollDataContext.LeaveRequests
               join e in PayrollDataContext.EEmployees on l.EmployeeId equals e.EmployeeId
               where l.Status == (int)status
               
               &&
               (
                   (l.FromDateEng >= FromDate && l.FromDateEng <= ToDate)
                   ||
                   (l.ToDateEng >= FromDate && l.ToDateEng <= ToDate)
                   ||
                   (FromDate >= l.FromDateEng && FromDate <= l.ToDateEng)
                   ||
                   (ToDate >= l.FromDateEng && ToDate <= l.ToDateEng)
               )
               orderby e.Name
               select new TextValue
               {
                   Text = e.Name,
                   DaysOrHours = l.DaysOrHours.Value,//.ToString("#.0") + " days",
                   Date = l.FromDateEng,
                   ID = l.LeaveRequestId
               }
               
               ).ToList();
        }
        public static int CountLeaveRequest(LeaveRequestStatusEnum status)
        {
            return PayrollDataContext.LeaveRequests.Count(x => x.Status == (int)status);
        }
        public static List<TextValue> GetEmployeeOnLeaveThisWeek(LeaveRequestStatusEnum status)
        {
            DayOfWeek day = Utils.Helper.Util.GetCurrentDateAndTime().DayOfWeek;
            int days = day - DayOfWeek.Sunday;

            DateTime FromDate = DateManager.GetStartDate(Utils.Helper.Util.GetCurrentDateAndTime().AddDays(-days));
            DateTime ToDate = DateManager.GetEndDate(FromDate.AddDays(6));

            return (
               from l in PayrollDataContext.LeaveRequests
               join e in PayrollDataContext.EEmployees on l.EmployeeId equals e.EmployeeId
               where l.Status == (int)status

               &&
               (
                   (l.FromDateEng >= FromDate && l.FromDateEng <= ToDate)
                   ||
                   (l.ToDateEng >= FromDate && l.ToDateEng <= ToDate)
                   ||
                   (FromDate >= l.FromDateEng && FromDate <= l.ToDateEng)
                   ||
                   (ToDate >= l.FromDateEng && ToDate <= l.ToDateEng)
               )
               orderby e.Name
               select new TextValue
               {
                   Text = e.Name,
                   DaysOrHours = l.DaysOrHours.Value,// + " days",
                   Date = l.FromDateEng,
                   ID = l.LeaveRequestId
               }

               ).ToList();
        }

        public static List<TextValue> GetEmployeeOnLeaveThisMonth(LeaveRequestStatusEnum status)
        {

            CustomDate today = CustomDate.GetTodayDate(IsEnglish);
            int currentYear = today.Year;

            DateTime FromDate ;// DateManager.GetStartDate(Utils.Helper.Util.GetCurrentDateAndTime().AddDays(-days));
            DateTime ToDate ;// DateManager.GetEndDate(FromDate.AddDays(6));

            CustomDate firstDateOfThisMonth = new CustomDate(1, today.Month, currentYear, IsEnglish);
            CustomDate lastDateOfThisMonth = firstDateOfThisMonth.GetLastDateOfThisMonth();

          

            FromDate = DateManager.GetStartDate(firstDateOfThisMonth.EnglishDate);
            ToDate = DateManager.GetEndDate(lastDateOfThisMonth.EnglishDate);


            return (
               from l in PayrollDataContext.LeaveRequests
               join e in PayrollDataContext.EEmployees on l.EmployeeId equals e.EmployeeId
               where l.Status == (int)status

               &&
               (
                   (l.FromDateEng >= FromDate && l.FromDateEng <= ToDate)
                   ||
                   (l.ToDateEng >= FromDate && l.ToDateEng <= ToDate)
                   ||
                   (FromDate >= l.FromDateEng && FromDate <= l.ToDateEng)
                   ||
                   (ToDate >= l.FromDateEng && ToDate <= l.ToDateEng)
               )
               orderby e.Name
               select new TextValue
               {
                   Text = e.Name,
                   DaysOrHours = l.DaysOrHours.Value,// + " days",
                   Date = l.FromDateEng,
                   ID = l.LeaveRequestId
               }

               ).ToList();
        }


        public static List<TextValue> GetEmployeeRetiring(DateTime FromDate, DateTime ToDate)
        {

          
            return (
               from l in PayrollDataContext.EEmployees
               
               join h in PayrollDataContext.EHumanResources on l.EmployeeId equals h.EmployeeId
               where 

               (
                   
                    (l.IsRetired == true &&  h.DateOfRetirementEng >= FromDate && h.DateOfRetirementEng <= ToDate)
                    ||
                     (l.IsResigned == true && h.DateOfResignationEng >= FromDate && h.DateOfResignationEng <= ToDate)
               )
               orderby l.Name
               select new TextValue
               {
                   Text = l.Name,
                   Date = (l.IsRetired == true ? h.DateOfRetirementEng : h.DateOfResignationEng),
                   ID = l.EmployeeId
               }

               ).OrderBy(x=>x.Date).ToList();
        }

        public static string GetRetirementType(List<HR_Service_Event> list, int? type)
        {
            if (type == null)
                return "";

            HR_Service_Event retType = list.FirstOrDefault(x => x.EventID == type);
            if (retType == null)
                return "";

            return retType.Name;
        }

        public static List<EmployeeServiceBo> GetEmployeeRetiringList(DateTime FromDate, DateTime ToDate,int? type)
        {

            List<HR_Service_Event> types = CommonManager.GetServieEventTypes();

            return (
               from l in PayrollDataContext.EEmployees

               join h in PayrollDataContext.EHumanResources on l.EmployeeId equals h.EmployeeId
               where

               (
                    (l.IsRetired == true && h.DateOfRetirementEng >= FromDate && h.DateOfRetirementEng <= ToDate)
                    ||
                     (l.IsResigned == true && h.DateOfResignationEng >= FromDate && h.DateOfResignationEng <= ToDate)
               )
               && (type==null || type == h.RetirementType )
               orderby l.Name
               select new EmployeeServiceBo
               {
                   Name = l.Name,
                   EmployeeId = l.EmployeeId,
                   JoinDateEng = h.DateOfRetirementEng == null ? h.DateOfResignationEng.Value : h.DateOfRetirementEng.Value, //l.ECurrentStatus.OrderBy(x => x.FromDateEng).First().FromDateEng,
                   JoinDateNepali =GetAppropriateDate( h.DateOfRetirementEng == null ? h.DateOfResignationEng.Value : h.DateOfRetirementEng.Value), //l.ECurrentStatus.OrderBy(x => x.FromDateEng).First().FromDate,
                   Branch = l.Branch.Name,
                   Department = l.Department.Name,
                   Designation = l.EDesignation.Name,
                   Level = l.EDesignation.BLevel.Name,
                   ServicePeriod = GetServicePeriodByEmployeeID(l.EmployeeId),
                   RetirementType = GetRetirementType(types,h.RetirementType)
               }

               ).ToList();
        }

        public static List<EmployeeServiceBo> GetEmployeeRetiringList(DateTime FromDate, DateTime ToDate)
        {


            return (
               from l in PayrollDataContext.EEmployees

               join h in PayrollDataContext.EHumanResources on l.EmployeeId equals h.EmployeeId
               where

               (

                    (l.IsRetired == true && h.DateOfRetirementEng >= FromDate && h.DateOfRetirementEng <= ToDate)
                    ||
                     (l.IsResigned == true && h.DateOfResignationEng >= FromDate && h.DateOfResignationEng <= ToDate)
               )
               orderby l.Name
               select new EmployeeServiceBo
               {
                   Name = l.Name,
                   EmployeeId = l.EmployeeId,
                   IdCardNo = h.IdCardNo,
                   JoinDateEng = l.ECurrentStatus.OrderBy(x => x.FromDateEng).First().FromDateEng,
                   JoinDateNepali = l.ECurrentStatus.OrderBy(x => x.FromDateEng).First().FromDate,
                   Branch = l.Branch.Name,
                   Department = l.Department.Name,
                   Designation = l.EDesignation.Name,
                   Level = l.EDesignation.BLevel.Name,
                   ServicePeriod = GetServicePeriodByEmployeeID(l.EmployeeId)

               }

               ).ToList();
        }

        public static List<EmployeeServiceBo> GetEmployeeRetiringListForDashboard(DateTime FromDate, DateTime ToDate)
        {


            return (
               from l in PayrollDataContext.EEmployees

               join h in PayrollDataContext.EHumanResources on l.EmployeeId equals h.EmployeeId
               where

               (

                    (l.IsRetired == true && h.DateOfRetirementEng >= FromDate && h.DateOfRetirementEng <= ToDate)
                    ||
                     (l.IsResigned == true && h.DateOfResignationEng >= FromDate && h.DateOfResignationEng <= ToDate)
               )
               orderby l.Name
               select new EmployeeServiceBo
               {
                   Name = l.Name,
                   EmployeeId = l.EmployeeId,
                   IdCardNo = h.IdCardNo,
                   RetiringDateEng = 
                   (
                        l.IsRetired ==  true ? h.DateOfRetirementEng : h.DateOfResignationEng
                   ),
                   Branch = l.Branch.Name,
                   Department = l.Department.Name

               }

               ).ToList();
        }
        public static List<TextValue> GetEmployeeJoiningThisMonth()
        {

            CustomDate today = CustomDate.GetTodayDate(IsEnglish);
            int currentYear = today.Year;

            DateTime FromDate;// DateManager.GetStartDate(Utils.Helper.Util.GetCurrentDateAndTime().AddDays(-days));
            DateTime ToDate;// DateManager.GetEndDate(FromDate.AddDays(6));

            CustomDate firstDateOfThisMonth = new CustomDate(1, today.Month, currentYear, IsEnglish);
            CustomDate lastDateOfThisMonth = firstDateOfThisMonth.GetLastDateOfThisMonth();

            FromDate = DateManager.GetStartDate(firstDateOfThisMonth.EnglishDate);
            ToDate = DateManager.GetEndDate(lastDateOfThisMonth.EnglishDate);


            return (
               from e in PayrollDataContext.EEmployees
               join b in PayrollDataContext.Branches on e.BranchId equals b.BranchId
               orderby e.Name
               select new TextValue
               {
                   Text = e.Name,
                   Value = b.Name,
                   Date = e.ECurrentStatus.OrderBy(x => x.FromDateEng).First().FromDateEng
               }


               ).Where(x => x.Date >= FromDate && x.Date <= ToDate).ToList();
        }


        public static List<EmployeeServiceBo> GetEmployeeJoiningList(DateTime FromDate, DateTime ToDate)
        {

            return (
               from e in PayrollDataContext.EEmployees
               join b in PayrollDataContext.Branches on e.BranchId equals b.BranchId
               join h in PayrollDataContext.EHumanResources on e.EmployeeId equals h.EmployeeId
               orderby e.Name
               select new EmployeeServiceBo
               {
                   Name = e.Name,
                   EmployeeId = e.EmployeeId,
                   IdCardNo = h.IdCardNo,
                   JoinDateEng = e.ECurrentStatus.OrderBy(x => x.FromDateEng).First().FromDateEng,
                   JoinDateNepali = e.ECurrentStatus.OrderBy(x => x.FromDateEng).First().FromDate,
                   Branch = e.Branch.Name,
                   Department = e.Department.Name,
                   Designation = e.EDesignation.Name,
                   Level = e.EDesignation.BLevel.Name,
                   ServicePeriod = GetServicePeriodByEmployeeID(e.EmployeeId)
                 }


               ).Where(x => x.JoinDateEng >= FromDate && x.JoinDateEng <= ToDate).ToList();
        }

        public static List<EmployeeServiceBo> GetEmployeeJoiningListForEmpDashboard(DateTime FromDate, DateTime ToDate)
        {

            return (
               from e in PayrollDataContext.EEmployees
               join b in PayrollDataContext.Branches on e.BranchId equals b.BranchId
               join h in PayrollDataContext.EHumanResources on e.EmployeeId equals h.EmployeeId
               orderby e.Name
               select new EmployeeServiceBo
               {
                   Name = e.Name,
                   EmployeeId = e.EmployeeId,
                   IdCardNo = h.IdCardNo,
                   JoinDateEng = e.ECurrentStatus.OrderBy(x => x.FromDateEng).First().FromDateEng,
                   Branch = e.Branch.Name,
                   Department = e.Department.Name
               }


               ).Where(x => x.JoinDateEng >= FromDate && x.JoinDateEng <= ToDate).ToList();
        }

        public static string GetServicePeriodByEmployeeID(int EmployeeID)
        {
            int years, months, days, hours;
            int minutes, seconds, milliseconds;

            ECurrentStatus firstStatus = BaseBiz.PayrollDataContext.ECurrentStatus.Where(x => x.EmployeeId == EmployeeID)
                .OrderBy(x => x.FromDateEng).FirstOrDefault();
            if (firstStatus != null)
            {
                string WorkingFor = string.Empty;
                NewEntityHelper.GetElapsedTime(firstStatus.FromDateEng, Utils.Helper.Util.GetCurrentDateAndTime(), out years, out months, out days, out hours, out minutes, out seconds, out milliseconds);
                string text = "";
                if (years != 0)
                    text = " " + years + " years";
                if (months != 0)
                    text += " " + months + " months";
                if (days != 0)
                    text += " " + days + " days";

                WorkingFor += text;

                return WorkingFor;
                //WorkingFor += ", Since " +
                //    (HttpContext.Current.SessionManager.CurrentCompany.IsEnglishDate ? firstStatus.FromDateEng.ToShortDateString() : firstStatus.FromDate + " (" + firstStatus.FromDateEng.ToShortDateString() + ")");

            }
            else return "";

        }

        public static List<TextValue> GetBranchEmployeeCount()
        {
            CustomDate today = CustomDate.GetTodayDate(IsEnglish);
            int currentYear = today.Year;

            DateTime FromDate;// DateManager.GetStartDate(Utils.Helper.Util.GetCurrentDateAndTime().AddDays(-days));



            CustomDate firstDateOfThisMonth = new CustomDate(1, today.Month, currentYear, IsEnglish);
            FromDate = DateManager.GetStartDate(firstDateOfThisMonth.EnglishDate);

            List<TextValue> list =
                (
                    from b in PayrollDataContext.Branches
                    orderby b.Name
                    select new TextValue
                    {
                        Text = b.Name,
                        Count = b.EEmployees.Where
                        (l => l.IsResigned == false && l.IsRetired == false).Count()
                    }
                ).ToList();



            //list.RemoveRange(1, 15);
            return list;
        }

        public static List<TextValue> GetPeriodCompletionList(int months, DateTime FromDate, DateTime ToDate)
        {


            List<TextValue> list =
                (
                from e in PayrollDataContext.EEmployees
                where PayrollDataContext.GetServiceYearReportDetail(e.EmployeeId, true,
                    months, FromDate, ToDate, -1).SingleOrDefault().IsValidInThisPayroll == true
                    && PayrollDataContext.IsEmployeeNotRetOrResigned(e.EmployeeId) == true
                select new TextValue
                {
                    Text = e.Name,
                    Date = e.ECurrentStatus.OrderBy(x => x.FromDateEng).First().FromDateEng,
                    OtherDate = PayrollDataContext.GetServiceYearReportDetail(e.EmployeeId, true,
                    months,  FromDate, ToDate, -1).SingleOrDefault().CompletionDateEng

                }

                    ).ToList();

            foreach (TextValue item in list)
            {
                int days = (item.OtherDate.Value - DateTime.Now).Days + 1;

                if (days >= 0)
                    item.Value = "in " + days + " days";
                else
                    item.Value = Math.Abs(days) + " days ago";
            }

            return list;
        }



        public static List<EmployeeServiceBo> GetPeriodCompletionListing(int months, DateTime FromDate, DateTime ToDate)
        {


            List<EmployeeServiceBo> list =
                (
                from e in PayrollDataContext.EEmployees
                join h in PayrollDataContext.EHumanResources on e.EmployeeId equals h.EmployeeId
                where PayrollDataContext.GetServiceYearReportDetail(e.EmployeeId, true,
                    months,  FromDate, ToDate, -1).SingleOrDefault().IsValidInThisPayroll == true
                    && PayrollDataContext.IsEmployeeNotRetOrResigned(e.EmployeeId) == true
                select new EmployeeServiceBo
                {
                    Name = e.Name,
                    EmployeeId = e.EmployeeId,
                    IdCardNo = h.IdCardNo,
                    JoinDateEng = e.ECurrentStatus.OrderBy(x => x.FromDateEng).First().FromDateEng,
                    JoinDateNepali = e.ECurrentStatus.OrderBy(x => x.FromDateEng).First().FromDate,
                    Branch = e.Branch.Name,
                    Department = e.Department.Name,
                    Designation = e.EDesignation.Name,
                    Level = e.EDesignation.BLevel.Name,
                    ServicePeriod = GetServicePeriodByEmployeeID(e.EmployeeId)

                }

                    ).ToList();

            //foreach (EmployeeSixMonthCompletingBO item in list)
            //{
            //    int days = (item.OtherDate.Value - DateTime.Now).Days + 1;

            //    if (days >= 0)
            //        item.Value = "in " + days + " days";
            //    else
            //        item.Value = Math.Abs(days) + " days ago";
            //}

            return list;
        }



        public static List<GetContractExpiringEmpListResult> GetContractExpiringList(DateTime FromDate, DateTime ToDate)
        {


            List<GetContractExpiringEmpListResult> list = PayrollDataContext.GetContractExpiringEmpList(FromDate, ToDate).ToList();
            DateTime date = GetCurrentDateAndTime();
            date = new DateTime(date.Year, date.Month, date.Day);
            foreach (GetContractExpiringEmpListResult item in list)
            {
                int days = (item.ToDateEng.Value - date).Days;
                if (days > 0)
                    item.DueDays = "in " + days + " days";
                else
                    item.DueDays = Math.Abs(days) + " days ago";

                if (item.Status != null)
                    item.StatusName = JobStatus.GetStatusForDisplay(item.Status.ToString());
            }

            return list;
        }

        public static List<GetContractExpiringEmpListDetailsResult> GetContractExpiringListReport(DateTime FromDate, DateTime ToDate)
        {


            List<GetContractExpiringEmpListDetailsResult> list = PayrollDataContext.GetContractExpiringEmpListDetails(FromDate, ToDate).ToList();
            DateTime date = GetCurrentDateAndTime();
            date = new DateTime(date.Year, date.Month, date.Day);
            foreach (GetContractExpiringEmpListDetailsResult item in list)
            {
                int days = (item.ToDateEng.Value - date).Days;
                if (days > 0)
                    item.DueDays = "in " + days + " days";
                else
                    item.DueDays = Math.Abs(days) + " days ago";

                if (item.Status != null)
                    item.StatusName = JobStatus.GetStatusForDisplay(item.Status.ToString());
            }

            return list;
        }
        #endregion


        public static List<DashboardEmployeeOnLeaveResult> getEmpOnLeaveForDashboardList(int branch, DateTime givenDay)
        {
            List<DashboardEmployeeOnLeaveResult> iList = new List<DashboardEmployeeOnLeaveResult>();
            iList = PayrollDataContext.DashboardEmployeeOnLeave(branch, givenDay).ToList();
            return iList;
        }

        public static List<DashboardOnLeaveThisMonthResult> getEmpOnLeaveThisMonth(DateTime filterdate)
        {
            List<DashboardOnLeaveThisMonthResult> iList = new List<DashboardOnLeaveThisMonthResult>();
            iList = PayrollDataContext.DashboardOnLeaveThisMonth(filterdate).ToList();
            foreach (var item in iList)
            {
                if (item.SubstituteEmployeeId != null)
                {
                    EEmployee emp =  EmployeeManager.GetEmployeeById(item.SubstituteEmployeeId.Value);
                    if (emp != null)
                        item.SubstituteEmpName = emp.Name;
                }
            }
            return iList;
        }


        public static List<DashboardOnLeaveThisWeekResult> getEmpOnLeaveThisWeek(DateTime filterdate)
        {
            List<DashboardOnLeaveThisWeekResult> iList = new List<DashboardOnLeaveThisWeekResult>();
            iList = PayrollDataContext.DashboardOnLeaveThisWeek(filterdate).ToList();
            return iList;
        }

        public static List<DashboardOnLeaveNextWeekResult> getEmpOnLeaveNextMonth(DateTime filterdate)
        {
            List<DashboardOnLeaveNextWeekResult> iList = new List<DashboardOnLeaveNextWeekResult>();
            iList = PayrollDataContext.DashboardOnLeaveNextWeek(filterdate).ToList();
            return iList;
        }
        protected string GetSubstring(string str, int length)
        {
            return str.Length > length ? str.Substring(0, length) : str;
        }
        public static List<BirthDayBo> GetUpcommingBirthDay(int month,bool todayOnly)
        {
            List<GetUpcomingBirthDayResult> list =
                PayrollDataContext.GetUpcomingBirthDay(todayOnly).Where(x => x.MonthIndex == month).ToList();
            List<BirthDayBo> _BirthDayBoList = new List<BirthDayBo>();
            foreach (GetUpcomingBirthDayResult item in list)
            {
                BirthDayBo _BirthDayBo = new BirthDayBo();
                _BirthDayBo.Name = item.Name;
                _BirthDayBo.EmployeeId = item.EmployeeId;
                _BirthDayBo.Month = item.Month;
                _BirthDayBo.Day = item.Day;
                _BirthDayBo.MonthIndex = item.MonthIndex;
                _BirthDayBo.Email = item.Email;

                if (IsEnglish == false)
                    _BirthDayBo.Month = item.Day + "&nbsp;" + item.Month + " (" +
                       CustomDate.ConvertEngToNep( new CustomDate(item.DateOfBirthEng.Value.Day, item.DateOfBirthEng.Value.Month,
                            item.DateOfBirthEng.Value.Year, true)).ToStringMonthDaySkipYear() + ")";
                else
                    _BirthDayBo.Month = item.Day + "&nbsp;" + item.Month;
                _BirthDayBoList.Add(_BirthDayBo);
            }


            return _BirthDayBoList;
        }

        public static List<GetContractExpiringEmpListResult> GetContractExpiringListForDashboard(
            DateTime FromDate, DateTime ToDate,DateTime nextToDate,DateTime nextNextToDate,
            ref List<GetContractExpiringEmpListResult> list1,
             ref List<GetContractExpiringEmpListResult> list2,
             ref List<GetContractExpiringEmpListResult> list3
            )
        {

             list1 = new List<GetContractExpiringEmpListResult>();
            list2 = new List<GetContractExpiringEmpListResult>();
            list3 = new List<GetContractExpiringEmpListResult>();

            List<GetContractExpiringEmpListResult> list = PayrollDataContext.GetContractExpiringEmpList(null, nextNextToDate).ToList();


            DateTime date = GetCurrentDateAndTime();
            date = new DateTime(date.Year, date.Month, date.Day);
            foreach (GetContractExpiringEmpListResult item in list)
            {
                int days = (item.ToDateEng.Value - date).Days;

                if (days == 0)
                    item.DueDays = "Today";
                else if (days > 0)
                {
                    if (days == 1)
                        item.DueDays = "Tomorrow";
                    else
                        item.DueDays = "in " + days + " days";
                }
                else
                {
                    if (Math.Abs(days) == 1)
                        item.DueDays = "Yesterday";
                    else
                        item.DueDays = Math.Abs(days) + " days ago";
                }

                if (item.Status != null)
                    item.StatusName = JobStatus.GetStatusForDisplay(item.Status.ToString());

                if (item.ToDateEng <= ToDate.Date)
                    list1.Add(item);
                else if (item.ToDateEng <= nextToDate.Date)
                    list2.Add(item);
                else// (item.ToDateEng <= nextNextToDate.Date)
                    list3.Add(item);
            }

          


            return list;
        }


        public static List<GetRetiredEmployeeListResult> GetRetiredEmployeeList(DateTime? fromDate, DateTime? toDate, int? retirementType, int? employeeId, int start, int size, out int totalRecords)
        {
            List<GetRetiredEmployeeListResult> list = PayrollDataContext.GetRetiredEmployeeList(fromDate, toDate, retirementType, employeeId, start, size).ToList();
            foreach (var item in list)
            {
                int years, months, days, hours;
                int minutes, seconds, milliseconds;

                NewEntityHelper.GetElapsedTime(item.JoinDateEng.Value.AddDays(double.Parse(item.DeductDays.Value.ToString())), 
                    item.RetirementDateEng.Value, out years, out months, out days, out hours, out minutes, out seconds, out milliseconds);
                string text = "";
                if (years != 0)
                    text = " " + years + " years";
                if (months != 0)
                    text += " " + months + " months";
                if (days != 0)
                    text += " " + days + " days";

                item.ServicePeriod += text;
            }

            totalRecords = 0;
            if (list.Count > 0)
                totalRecords = list[0].TotalRows.Value;

            return list;
        }

    }
}
