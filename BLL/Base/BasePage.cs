﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Utils.Base;
using Utils.Web;
using DAL;
using BLL.Manager;
using System.IO;
using Utils.Calendar;
using System.Web;
using Ext.Net;
using BLL.BO;

namespace BLL.Base
{
    public class BasePage : Page
    {


        public virtual MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.NotSet;
            }
            set { }
        }


        [DirectMethod]
        public static DateEngToNep GetEngToNepDate(string engDate)
        {
            DateTime date = Convert.ToDateTime(engDate);
            CustomDate nepDate = new CustomDate(date.Day, date.Month, date.Year, true);
            nepDate = CustomDate.ConvertEngToNep(nepDate);

            DateEngToNep data = new DateEngToNep();
            data.NepStartDay = nepDate.Day;
            data.NepMaxDays = DateHelper.GetTotalDaysInTheMonth(nepDate.Year, nepDate.Month, false);
            data.CalendarTitle = "Shr-2071";
            return data;
        }

        public bool IsAccessible(string url)
        {
            return UserManager.IsPageAccessible(url);

        }

        public void RegisterJSCode(string code)
        {
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), Guid.NewGuid().ToString(), code, true);
        }

        public DateTime GetCurrentDateForSalary()
        {
            return DateTime.Now;
        }

        public void NotPermissionContent()
        {
            if (HttpContext.Current != null)
            {
                HttpContext.Current.Response.Write("<h2>Not enough permission to view the document.<h2>");
                HttpContext.Current.Response.End();
            }
        }

        /// <summary>
        /// Property for holding the Id of the editing object/entity
        /// </summary>
        public int CustomId
        {
            get
            {

                if (ViewState["Id"] != null)
                    return int.Parse(ViewState["Id"].ToString());
                return 0;
            }
            set
            {
                ViewState["Id"] = value;
            }
        }

        public string Value
        {
            get
            {

                if (ViewState["Value"] != null)
                    return ViewState["Value"].ToString();
                return "";
            }
            set
            {
                ViewState["Value"] = value;
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            // add onfocus and onblur javascripts to all input controls on the forum,
            // so that the active control has a difference appearance
            //UIHelper.SetInputControlsHighlight(this, "x-form-focus", false);
            //this.MaintainScrollPositionOnPostBack = true;

            // set page title to Master page for display

            //if (!string.IsNullOrEmpty(Page.Title))
            //{
            //    Literal pageControl = this.Page.Master.FindControl("pageTitleBlock") as Literal;
            //    if (pageControl != null)
            //        pageControl.Text = Page.Title;
            //}

            EnableTheDisabledButton();

            base.OnLoad(e);
        }


        public void EnableTheDisabledButton()
        {
            string buttonId = Request.Form["__EVENTARGUMENT"];

            if (buttonId != null)
            {

                try
                {

                    if (buttonId.Contains("|"))
                    {
                        buttonId = buttonId.Substring(0, buttonId.IndexOf("|"));
                    }
                    if (buttonId.Contains("_"))
                    {
                        buttonId = buttonId.Substring(buttonId.LastIndexOf("_") + 1);
                    }

                    Control pageControl = this.Page.Master.FindControl("mainContent") as Control;
                    if(pageControl==null)
                        pageControl = this.Page.Master.FindControl("ContentPlaceHolder_Main") as Control;

                    if (pageControl != null)
                    {

                        IButtonControl baseCtl = pageControl.FindControl(buttonId) as IButtonControl;
                        if (baseCtl != null)
                        {
                            Control ctl = pageControl.FindControl(buttonId);

                            Ext.Net.MenuItem menuItem = ctl as Ext.Net.MenuItem;
                            if (menuItem != null)
                            {
                                menuItem.Disabled = false;
                                menuItem.Enable();
                            }
                            else if (ctl as Ext.Net.Button != null)
                            {
                                Ext.Net.Button button = ctl as Ext.Net.Button;
                                if (button != null)
                                {
                                    button.Disabled = false;
                                    button.Enable();
                                }
                            }
                            else
                            {
                                Ext.Net.LinkButton button = ctl as Ext.Net.LinkButton;
                                if (button != null)
                                {
                                    button.Disabled = false;
                                    button.Enable();
                                }
                            }
                        }
                    }

                }
                catch { }

            }
        }

        protected void DisableChildControls(Control parent)
        {
            BaseHelper.DisableChildControls(parent);
        }

        protected void EnabledChildControls(Control parent)
        {
           BaseHelper.EnabledChildControls(parent);
        }

        /// <summary>
        /// Hide the direct child controls
        /// </summary>
        /// <param name="parent"></param>
        protected void HideChildControlsIncludingVal(Control parent)
        {
            foreach (Control ctl in parent.Controls)
            {
                ctl.Visible = false;
            }
        }


        protected void DisableChildValidationControls(Control parent)
        {
            BaseHelper.DisableChildValidationControls(parent);
        }

        protected void EnabledChildValidationControls(Control parent)
        {
            BaseHelper.EnabledChildValidationControls(parent);
        }

        /// <summary>
        /// Call to disable controls on the page after first salary calculation
        /// </summary>
        protected override void OnPreRender(System.EventArgs e)
        {
            base.OnPreRender(e);

            //High piorioty
            if (this is IDisableAfterCalculation)
            {
                IDisableAfterCalculation disable = (IDisableAfterCalculation)this;
                disable.DisableControlsAfterSalary();
            }
            
        }
        public bool IsEnglish
        {
            get
            {
                return SessionManager.CurrentCompany.IsEnglishDate;
            }
        }

        public string GetCurrency(object amount)
        {
            return BaseHelper.GetCurrency(amount,SessionManager.DecimalPlaces);
        }

        public DateTime GetEngDate(string inputDate)
        {
         
            return BaseBiz.GetEngDate(inputDate, IsEnglish);
        }

        public string GetPrevDate(object date)
        {
            if (date == null || string.IsNullOrEmpty(date.ToString()))
                return "";

            CustomDate date1 = CustomDate.GetCustomDateFromString(date.ToString(), IsEnglish);

            return date1.DecrementByOneDay().ToString();

        }

        /// <summary>
        /// Checks if the salary/calculation is in between initial & final saved
        /// </summary>
        /// <returns></returns>
        public bool IsInitialSaveExists()
        {
            PayrollPeriod payrollPeriod = CommonManager.GetLastPayrollPeriod();
            if (payrollPeriod != null)
            {
                if (CalculationManager.IsCalculationSaved(payrollPeriod.PayrollPeriodId) != null
                    && !CalculationManager.IsAllEmployeeSavedFinally(payrollPeriod.PayrollPeriodId, false))
                {
                    return true;
                }
            }
            return false;
        }

        public bool IsPayrollFinalSaved(int payrollPeriodId)
        {
            return
                CalculationManager.IsAllEmployeeSavedFinally(payrollPeriodId, false);
        }

        public bool IsPayrollFinalSavedButNextPayrollAttendanceAlsoSaved(int payrollPeriodId)
        {
            bool isPayrollFinalSaved = 
                CalculationManager.IsAllEmployeeSavedFinally(payrollPeriodId, false);

            if (isPayrollFinalSaved == false)
                return false;

            // then check next payroll atte is saved or not

            return CalculationManager.IsNextPayrollAtteSaved(payrollPeriodId);

        }

        public bool IsPayrollSaveForEmployee(int payrollPeriodId, int employeeId)
        {
            return
                CalculationManager.IsCalculationSavedForEmployee(payrollPeriodId, employeeId);
        }

        public bool IsAttendanceSavedForEmployee(int payrollPeriodId, int employeeId)
        {
            return
                LeaveAttendanceManager.IsAttendanceSavedForEmployee(payrollPeriodId, employeeId);
        }

        public string CurrentPageUrl
        {
            get
            {
               string currentPage = Path.GetFileName(Request.Url.AbsoluteUri.ToString()).ToLower();
                if (currentPage.IndexOf("?") >= 0)
                    currentPage = currentPage.Remove(currentPage.IndexOf("?"));
                return currentPage;
            }
        }

        public void SetMessage(Ext.Net.Label lbl, string msg)
        {
            lbl.Html =
             string.Format(HttpContext.GetGlobalResourceObject("HTMLMessage", "InformationForDetail").ToString(), msg);
            //SetTimeOut(lbl);
        }
        public void SetMessage(Ext.Net.Container lbl, string msg)
        {
            lbl.Html =
             string.Format(HttpContext.GetGlobalResourceObject("HTMLMessage", "InformationForDetail").ToString(), msg);
            //SetTimeOut(lbl);
        }
        public void SetWarning(Ext.Net.Label lbl, string msg)
        {
            lbl.Html =
             string.Format(HttpContext.GetGlobalResourceObject("HTMLMessage", "WarningForDetail").ToString(), msg);
            lbl.Focus();
            //SetTimeOut(lbl);
        }
        public void SetWarning(Ext.Net.Container lbl, string msg)
        {
            lbl.Html =
             string.Format(HttpContext.GetGlobalResourceObject("HTMLMessage", "WarningForDetail").ToString(), msg);
            lbl.Focus();
            //SetTimeOut(lbl);
        }
        public void SetError(Ext.Net.Label lbl, string msg)
        {
            lbl.Html =
             string.Format(HttpContext.GetGlobalResourceObject("HTMLMessage", "ErrorForDetail").ToString(), msg);

            //SetTimeOut(lbl);
        }
        public void SetError(Ext.Net.Container lbl, string msg)
        {
            lbl.Html =
             string.Format(HttpContext.GetGlobalResourceObject("HTMLMessage", "ErrorForDetail").ToString(), msg);

            //SetTimeOut(lbl);
        }
        
        public static bool IsPwdChangedBeforePwdChaningDays(string userName, ref int days)
        {
            return UserManager.IsPwdChangedBeforePwdChaningDays(userName,ref days);
        }
    }
}
