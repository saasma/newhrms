﻿using System.Web.UI;
using System.Web.UI.WebControls;
using Utils.Base;
using System;
using System.IO;
using System.Web;

namespace BLL.Base
{
    public class BaseUserControl : UserControl
    {
        public void NotPermissionContent()
        {
            if (HttpContext.Current != null)
            {
                HttpContext.Current.Response.Write("<h2>Not enough permission to view the document.<h2>");
                HttpContext.Current.Response.End();
            }
        }


        /// <summary>
        /// Property for holding the Id of the editing object/entity
        /// </summary>
        public int ControlID
        {
            get
            {

                if (ViewState["ControlID"] != null)
                    return int.Parse(ViewState["ControlID"].ToString());
                return 0;
            }
            set
            {
                ViewState["ControlID"] = value;
            }
        }

        public string CurrentPageUrl
        {
            get
            {
                string currentPage = Path.GetFileName(Request.Url.AbsoluteUri.ToString()).ToLower();
                if (currentPage.IndexOf("?") >= 0)
                    currentPage = currentPage.Remove(currentPage.IndexOf("?"));
                return currentPage;
            }
        }

        public bool IsEnglish
        {
            get
            {
                return SessionManager.CurrentCompany.IsEnglishDate;
            }
        }

        

        /// <summary>
        /// Method to output server side control id into javascript array variable
        /// so that the control can be accessed with server side id     in client side
        /// </summary>        
        protected void SaveControlClientIDInJScriptArrayVariable(string scriptVariableName, Control[] controls)
        {
            if (scriptVariableName == null)
                scriptVariableName = "controlID";

            System.Text.StringBuilder ctlObj = new System.Text.StringBuilder();
            ctlObj.Append(string.Format("var {0} = new Array();", scriptVariableName));

            foreach (Control ctl in controls)
            {
                ctlObj.Append(string.Format("{2}['{0}'] = '{1}';\n", ctl.ID, ctl.ClientID, scriptVariableName));
            }
            Page.ClientScript.RegisterStartupScript(this.GetType(), "onload", ctlObj.ToString(), true);
        }
       
        /// <summary>
        /// Must not disable all as it may include validation controls also
        /// </summary>
        /// <param name="parent"></param>
        protected void DisableChildControls(Control parent)
        {
            BaseHelper.DisableChildControls(parent);
        }

        protected void EnabledChildControls(Control parent)
        {
            BaseHelper.EnabledChildControls(parent);
        }


        protected void DisableChildValidationControls(Control parent)
        {
            BaseHelper.DisableChildValidationControls(parent);
        }

        protected void EnabledChildValidationControls(Control parent)
        {
            BaseHelper.EnabledChildValidationControls(parent);
        }

        public void EnableTheDisabledButton()
        {
            string buttonId = Request.Form["__EVENTARGUMENT"];

            if (buttonId != null)
            {

                try
                {

                    if (buttonId.Contains("|"))
                    {
                        buttonId = buttonId.Substring(0, buttonId.IndexOf("|"));
                    }
                    if (buttonId.Contains("_"))
                    {
                        buttonId = buttonId.Substring(buttonId.LastIndexOf("_") + 1);
                    }

                    IButtonControl baseCtl = this.FindControl(buttonId) as IButtonControl;
                    if (baseCtl != null)
                    {
                        Control ctl = this.FindControl(buttonId);

                        Ext.Net.MenuItem menuItem =  ctl as Ext.Net.MenuItem;
                        if (menuItem != null)
                        {
                            menuItem.Disabled = false;
                            menuItem.Enable();
                        }
                        else if(ctl as Ext.Net.Button != null)
                        {
                            Ext.Net.Button button = ctl as Ext.Net.Button;
                            if (button != null)
                            {
                                button.Disabled = false;
                                button.Enable();
                            }
                        }
                        else
                        {
                            Ext.Net.LinkButton button = ctl as Ext.Net.LinkButton;
                            if (button != null)
                            {
                                button.Disabled = false;
                                button.Enable();
                            }
                        }
                    }

                }
                catch { }

            }
        }

        /// <summary>
        /// Call to disable controls on the page after first salary calculation
        /// </summary>
        protected override void OnPreRender(System.EventArgs e)
        {
            base.OnPreRender(e);

            EnableTheDisabledButton();

            //High piorioty
            if (this is IDisableAfterCalculation)
            {
                IDisableAfterCalculation disable = (IDisableAfterCalculation)this;
                disable.DisableControlsAfterSalary();
            }
        }

        public string GetCurrency(object amount)
        {
            return BaseHelper.GetCurrency(amount,SessionManager.DecimalPlaces);
        }

        public DateTime GetEngDate(string inputDate)
        {
            return BaseBiz.GetEngDate(inputDate, IsEnglish);
        }

    }
}
