﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace BLL
{
    public enum TaxCertificateEnum
    {
        Salary = 1,
        Allowances = 2,
        ProvidentFund = 3,
        VehicleFacility = 4,
        TelephoneFacility = 5,
        HousingAllowance = 6,
        Bonus = 7,

        HealthExpenses = 8,
        LeaveEncashment = 9, // company protion leave encashment also
        DashainAllowance = 10,
        LifeInsuranceIncome = 11,
        Reward = 12,
        ConcessessionalInterest = 13,
        Other = 15,


        PFDeductionAndCITSum = 50,
        InsuranceDeduction = 51,
        RemoteAllowance = 52,
        TotalDeduction = 53,
        SingleMarriedDeduction = 54,
        TotalTaxableIncome = 55,
        OnePercent = 56,
        FifteenPercent = 57,
        TwentyFivePercent = 58,
        ThirtyFivePercent = 59,
        TotalTax = 60,
        MedicalTax = 61,
        FemaleRebate = 62,
        TotalFinalTax = 63
    }

    public enum RetirementPortionTypeEnum
    {
        Income = 1,
        Deduction = 2,
        OtherIncome = 3,
        OtherDeduction = 4
    }
    public enum CITChangeType
    {
        NoCIT = 1,
        OptimumCIT = 2,
        FixedAmount = 3,
        Rate = 4
    }
    public enum CITRequestStatus
    {
        SaveAndSend = 1,
        Approved = 2,
        Rejected = 3
    }

    public enum ReportHeaderFooterEnum
    {
        CITReport = 1
    }
    public enum AddOnTypeEnum
    {
        LeaveFare = 1,
        ArrearType = 2,
        Rectification = 3
    }

    public enum LeaveFareStatus
    {
        Pending = 0,
        Saved = 1,
        Paid = 2 // send to add on 
    }

    public enum PageViewType
    {
        Admin = 1,
        ManagerEmployee = 2
    }
    public enum AppraisalBlockType
    {
        Activity = 0,
        Competency = 1,
        Target = 4,
        Question = 2,
        Reivew = 3,
        QualificationAndEducation = 5,
        TransferHistory = 6,
        LevelDesignationWorkHistory = 7
    }

    public enum AppraisalMarkingTypeEnum
    {
        RatingScale = 1,
        Marks = 2,
        PercentWithEachQuestionWeightage = 3
    }

    public enum CITType
    {
        Fixed = 1,
        PercentRate = 2,
        CITTable = 3,
        Optimum = 4
    }

    public enum AttendnanceDeductionType
    {
        Absent = 1, // Auto or Manually Absent mark
        LateDeduction = 2, // Late deduciton marked
        UPL = 3 // unpaid leave request or assinged
    }

    public enum LeaveTypeEnum
    {
        Normal = 1,
        ParentGroup = 2,
        Child = 3
    }
    public enum MenuTypeEnum
    {
        NotSet = 0,
        AdministratorTools = 1,
        HumanResource = 2,
        EmployeeAppraisal = 3,
        LeaveAndAttendance = 4,
        Payroll = 5,
        Retirement = 6,
        ProjectAndTimesheet = 7,
        DocumentManagement = 8
    }

    public enum RequestHistoryTypeEnum
    {
        EveningCounter = 1,
        Overtime = 2,
        Travel = 3
    }


    public enum AttendanceCommentThresholdEnum
    {
        CurrentDay = 0,
        ThisWeek = 1,
        ThisMonth = 2,
        LastMonth = 3,
        Past3Days = 4,
        AnyDate = 5
    }

    public enum ChkINOUTEnum
    {
        ChkIn = 0,
        ChkOut = 1
    }


    public enum PreDefindFlowType
    {
        Leave = 1,
        Overtime = 2 // for Evening Counter allowance also same flow //,
                     //EveningCounterAllowance = 3,

    }
    public enum IncomeCalculationProcessPastSalary
    {
        ProcessForPastSalary = 2,
        CalledFromAddOn = 3
    }

    public enum TravelAllowanceIsEdit
    {
        UserEditable = 0,
        DefinedAmount = 1
    }

    public enum AppraisalStatus
    {
        NotEditable = -1,
        Saved = 0,//saved only by Employee

        SaveAndSend = 1, //emp final saves the appraisal
        SupervisorManagerCommented = 2, // supervisor comments/rates on each block
        EmployeeReviewed = 3 // can add only review


    }

    public enum AppraisalSummaryCommentTypeEnum
    {
        CommentOnly = 1,
        HasRecommendation = 2,
        HasIAgreeOrDisagree = 3, // if employee has set agree or disagree
        HasAdditionalMarks = 4 // in case of ceo will add addition score for questionnaire
    }

    public enum RequestStatusEnum
    {
        Later = 0,
        Send = 1,
        HRForward = 2
    }




    public enum approvalTypeFlag
    {
        CreateEmp = 0,
        EditEmp = 3,
        Approve = 5,
        EditApprove = 7
    }

    public enum FlowTypeEnum
    {
        TravelOrder = 1,
        Appraisal = 2
    }

    public enum NoticeBoardStatusEnum
    {
        Draft = 0,
        Published = 1,
        Unpublished = 2
    }
    public enum AdvanceStatusEnum
    {
        HRAdvance = 1,
        EmployeeSettle = 2,
        HRSettle = 3
    }
    public enum FlowStepEnum
    {
        // Applied for Travel Request, Not applies for Appraisal
        Reject = -1, // any level can reject the flow & will be Flow specific

        //Save, when employee only saved the document with finish later then the status will be NULL or 0
        Saved = 0,

        Step1SaveAndSend = 1,
        Step2 = 2,
        Step3 = 3,
        Step4 = 4,
        Step5 = 5,
        Step6 = 6,
        Step7 = 7,
        Step8 = 8,
        Step9 = 9,
        Step10 = 10,
        Step11 = 11,
        Step15End = 15,

        Settled = 50 // Travel Request specific Settle Step
    }

    public enum TravelOrderAuthorityEnum
    {
        Employee = 1,
        BranchHead = 2,
        DepartmentHead = 3,
        HRHead = 4,
        TeamLead = 5,
        SpecificPerson = 6,
        Recommend1 = 7,
        //Recommend2 = 10,
        LeaveRecommender = 8,
        LeaveApproval = 9,
        AllEmployee = 10,
        Approval1 = 11
    }

    public enum AppraisalAdditionalStep
    {
        // used in case of Dishhome to provide recommendation
        RecommendationBlock = 1,

        AddAdditionPoints = 2, // used in civil by ceo to add addition marks upto 12% in last step

        AddAdditionPointsAndRecommendationBoth = 3
    }

    public enum LeaveRequestAuthorityEnum
    {
        BranchHead = 2,
        DepartmentHead = 3,
        ByDesignation = 4,
        RegionalHead = 5,
        SpecificPerson = 6

    }
    public enum TravelByEnum
    {
        Bus = 1,
        Airplane = 2,
        Ferry = 3,
        Train = 4,
        Other = 5
    }


    public enum EveningCounterStatusEnum
    {
        Pending = 0,
        Recommended = 1,
        Approved = 2,
        Rejected = 3,
        Forwarded = 4
    }


    public enum ExpensePaidByEnum
    {
        Office = 1,
        Self = 2,
        Client = 3,
        AreadOffice = 4,
        ThirdParty = 5
    }

    public enum ReligionEnum
    {
        Hindu = 1,
        Bhuddist = 2
    }

    public enum DesignationCategory
    {
        Administration = 1,
        Technical = 2
    }
    public enum DesignationGroup
    {
        Administration = 1,
        Legal = 2,
        Accounts = 3,
        Engineering = 4,
        Computer = 5,
        Agriculture = 6,
        Electrician = 7,
        Driver = 8
    }
    //public enum HireMethodEnum
    //{
    //    FreeCompetition = 1,
    //    DirectHire = 2,
    //    DalitKota = 3
    //}

    public enum IncomeDefinedTypeEnum
    {
        NotDefined = 0,// in case only x is defined without y
        Branch = 1,
        Level = 2,
        Designation = 3,
        Department = 4
    }

    public enum EmployeeDocumentType
    {
        General = 1,
        Certificate = 2
    }

    #region Deputation
    public enum DeputationAttendanceIn
    {
        SourceBranch = 1,
        DestinationBranch = 2,
        NoAttendance = 3
    }
    public enum DeputationTA
    {
        GoingOnly = 1,
        ReturingOnly = 2,
        GoingAndReturing = 3,
        NoTA = 4
    }
    public enum DeputationDA
    {
        Yes = 1,
        No = 2
    }
    public enum DeputationApplicableSalaryAllowance
    {
        SourceBranch = 1,
        DestinationBranch = 2,
        LowerOf2Branches = 3,
        HigherOf2Branches = 4
    }
    public enum RetirementOptionType
    {
        Age = 1,
        ServiceYear = 2
    }

    public enum LetterModuleType
    {
        Appointment = 1,
        BranchTransfer = 2,
        DepartmentTransfer = 3,
        Deputation = 4
    }

    public enum LevelGradeChangeType
    {
        //Join = 1,
        Promotion = 2,
        Demotion = 3,
        AnnualGradeFitting = 4,
        //GradeReward = 5,
        SalaryStructureChange = 6
    }

    public enum LevelGradeChangeStatusType
    {
        Saved = 1,
        Approved = 2//,
        //Reject = 3
    }

    #endregion

    public enum DARStatusEnum
    {
        Draft = 1,
        Pending = 2,
        Verified = 3
    }

    public enum ModuleEnum
    {
        AdministratorTools = 1,
        HumanResource = 2,
        EmployeeAppraisal = 3,
        LeaveAndAttendance = 4,
        PayrollManagement = 5,
        Retirement = 6,
        ProjectAndTimesheet = 7,
        DocumentManagement = 8
    }


    public enum ControlType
    {
        TextBox = 1,
        DropDownList = 2,
        CheckBox = 3,
        CheckBoxList = 4,
        RadioButtonList = 5
    }

    public enum HRStatusEnum
    {
        Saved = 0,
        Approved = 1
    }

    public enum TimeSheetStatusEnum
    {
        Saved = 0,
        Approved = 1,
        Rejected = 2
    }

    public enum LeavePredefinedNotificationEnum
    {
        AllEmployee = 1,
        BranchHead = 2,
        RegionHead = 3,
        SpecificBranch = 4,
        Group = 5
    }

    public enum TrainingTypeEnum
    {
        Internal = 1,
        External = 2
    }

    public enum TrainingStatusEnum
    {
        Saved = 1,
        Publish = 2,
        Unpublish = 3
    }

    public enum TrainingRequestStatusEnum
    {
        Pending = 1,
        Approved = 2,
        HouseFull = 3,
        WaitingList = 4
    }

    public enum PasswordChangeTypeEnum
    {
        ForcePwdChange = 1,
        RecommendPwdChange = 2
    }

    public enum AwardTypeEnum
    {
        Monetary = 1,
        NonMonetary = 2
    }

    public enum AwardCalculationEnum
    {
        MonthsBasicSalary = 1,
        PercentageOfBasicSalary = 2,
        FixedAmount = 3
    }

    public enum ActivityTypeEnum
    {
        ClientVisit = 1,
        SoftwareTesting = 2,
        Documentation = 3,
        RemoteSupport = 4,
        Other = 5
    }

    public enum AtivityStatusEnum
    {
        Draft = 1,
        SaveAndSend = 2,
        Approved = 3
    }

    public enum AttendanceRequestStatusEnum
    {
        Saved = 0,
        Approved = 1,
        Rejected = 2,
        Recommended = 3
    }
    public enum DocContactTpeEnum
    {
        Dealer = 1,
        Painter = 2
    }

    public enum ReportTypeEnum
    {
        EmpDailyAttendendance = 1
    }

    public enum EmployeeActivityThreshold
    {
        CurrentDay = 0,
        ThisWeek = 1,
        ThisMonth = 2,
        LastMonth = 3,
        Past2Days = 4
    }

    //public enum AttendanceAuthenticationTypeEnum
    //{
    //    TimeRequest = 101,
    //    Training = 102
    //}

    public enum ClaimType
    {
        Medical = 1,
        Accidental = 2
    }


    public enum ClaimStatus
    {
        Uploaded = 1,
        Pending = 2,
        Processing = 3,
        Submitted = 4,
        Completed=5,
        Rejected=6
    }


}
