﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BLL.BO;
using System.Web;

namespace BLL
{
    public class NewHelper
    {

        public static List<TextValue> GetTextValues(Type enumType)
        {
            List<TextValue> optionList = new List<TextValue>();

            foreach (object optionEnum in Enum.GetValues(enumType))
            {
                TextValue obj = new TextValue();

                string option = optionEnum.ToString();
                object optionName = HttpContext.GetGlobalResourceObject("ResourceEnums", option);//.ToString();

                obj.Text = optionName == null ? option : optionName.ToString();
                obj.Value = ((int)optionEnum).ToString();
                optionList.Add(obj);
            }

            return optionList;
        }

        public static string FormatDate(DateTime date)
        {
            return date.ToString("yyyy/MM/dd");
        }
        public static string calculateAge(DateTime birthDate, DateTime now)
        {
            birthDate = birthDate.Date;
            now = now.Date;

            var days = birthDate.Day - now.Day;
            if (days < 0)
            {
                var newNow = now.AddMonths(-1);
                days += (int)(now - newNow).TotalDays;
                now = newNow;
            }
            var months = now.Month - birthDate.Month;
            if (months < 0)
            {
                months += 12;
                now = now.AddYears(-1);
            }
            var years = now.Year - birthDate.Year;
            if (years == 0)
            {
                if (months == 0)
                    return days.ToString() + " days";
                else
                    return months.ToString() + " months";
            }
            return years.ToString();
        } 

        public static string GetElapsedTime(DateTime from, DateTime to)
        {
            int years, months, days, hours;
            int minutes, seconds, milliseconds;

            NewHelper.GetElapsedTime(from, BLL.BaseBiz.GetCurrentDateAndTime(), out years, out months, out days, out hours, out minutes, out seconds, out milliseconds);
            string text = "";
            if (years != 0)
                text = " " + years + " years";
            if (months != 0)
                text += " " + months + " months";
            if (days != 0)
                text += " " + days + " days";

            return text;

        }

        // Return the number of years, months, days, hours, minutes, seconds,
        // and milliseconds you need to add to from_date to get to_date.
        public static void GetElapsedTime(DateTime from_date, DateTime to_date,
            out int years, out int months, out int days, out int hours,
            out int minutes, out int seconds, out int milliseconds)
        {
            // If from_date > to_date, switch them around.
            if (from_date > to_date)
            {
                GetElapsedTime(to_date, from_date,
                    out years, out months, out days, out hours,
                    out minutes, out seconds, out milliseconds);
                years = -years;
                months = -months;
                days = -days;
                hours = -hours;
                minutes = -minutes;
                seconds = -seconds;
                milliseconds = -milliseconds;
            }
            else
            {
                // Handle the years.
                years = to_date.Year - from_date.Year;

                // See if we went too far.
                DateTime test_date = from_date.AddMonths(12 * years);
                if (test_date > to_date)
                {
                    years--;
                    test_date = from_date.AddMonths(12 * years);
                }

                // Add months until we go too far.
                months = 0;
                while (test_date <= to_date)
                {
                    months++;
                    test_date = from_date.AddMonths(12 * years + months);
                }
                months--;

                // Subtract to see how many more days,
                // hours, minutes, etc. we need.
                from_date = from_date.AddMonths(12 * years + months);
                TimeSpan remainder = to_date - from_date;
                days = remainder.Days;
                hours = remainder.Hours;
                minutes = remainder.Minutes;
                seconds = remainder.Seconds;
                milliseconds = remainder.Milliseconds;
            }
        }



    }
}
