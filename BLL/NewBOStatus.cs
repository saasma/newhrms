﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL
{
    public class Status
    {
        public bool IsSuccess { get; set; }
        private string _ErrorMessage = string.Empty;
        public string ErrorMessage
        {
            get
            {
                return _ErrorMessage;
            }
            set
            {
                if (value == "")
                    IsSuccess = true;
                else
                    IsSuccess = false;
                _ErrorMessage = value;
            }
        }

        public Status()
        {
            this.IsSuccess = true;
            this.ErrorMessage = "";
        }
        public int Count { get; set; }
    }

    public class SalaryScale
    {
        public int LevelGroupId { get; set; }
        public string GroupLevel { get; set; }
        public int LevelId { get; set; }
        public string LevelCode { get; set; }
        public decimal PayScale { get; set; }
        public decimal StepRate { get; set; }
        public int NoOfSteps { get; set; }
        public Dictionary<string,decimal?> Amounts { get; set; }

        public decimal? GetAmount(int levelGroupId, int levelId, int step)
        {
            if (Amounts.ContainsKey(levelGroupId + ":" + levelId + ":" + step))
            {
                return Amounts[levelGroupId + ":" + levelId + ":" + step];
            }
            return null;
        }

        public decimal? GetAmountForDecimialInGrade(int levelGroupId, int levelId, double step)
        {
            if (Amounts.ContainsKey(levelGroupId + ":" + levelId + ":" + step))
            {
                return Amounts[levelGroupId + ":" + levelId + ":" + step];
            }
            return null;
        }

    }
    public class SalaryScaleAmount
    {
        public int LevelGroupId { get; set; }
        public int LevelId { get; set; }
        public int Step { get; set; }
        public decimal? Amount { get; set; }
    }

   
    

}
