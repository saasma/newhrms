﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;
using System.Text.RegularExpressions;

namespace Utils.Security
{
    public class AsymmCrypto
    {
        public static readonly string PublicKey = "<RSAKeyValue><Modulus>0psubxZjVgPMSSNtRvGsCzRhH5mAKU4Ugvhi+/ENpawRdSqPr1uN2beXfzTge1QQARnKBwXROUULJ9R8ak5Ce1p6Nbm3xLn17qc8D4+qLH8sWEo62W3aSD0KE/uqK/6m7719Ck9nz1ViJYsPf/TFBmumDjd4CUH3yC59fMhyQ5U=</Modulus><Exponent>AQAB</Exponent></RSAKeyValue>";
        public static readonly string PrivateKey = "<RSAKeyValue><Modulus>0psubxZjVgPMSSNtRvGsCzRhH5mAKU4Ugvhi+/ENpawRdSqPr1uN2beXfzTge1QQARnKBwXROUULJ9R8ak5Ce1p6Nbm3xLn17qc8D4+qLH8sWEo62W3aSD0KE/uqK/6m7719Ck9nz1ViJYsPf/TFBmumDjd4CUH3yC59fMhyQ5U=</Modulus><Exponent>AQAB</Exponent><P>/fDeJgxmI5KmhRf1ZdZ3DPd0CpS8yJ62bQRulca0ZYP8KPK0FwAkFij1Fow9mW4XRO4agfdVQdc3+c8f2PZ/Hw==</P><Q>1FBb73ZXosN30q+DhDe4sxY3wx2I9nVIYuYoEXGStQ2bIm7/xw15iCqWaFDaaYRYFX61kCODGaYLMVP4ZrPKyw==</Q><DP>xEjHXb+dv4degWfofiQM94/aOUeIuMW9Gn0fgVQLIqCna3tWaqToP9y+vQP/pTXsdJJbGPR2CSNzF/vakRjPXw==</DP><DQ>iI8bYv2CRp1lL61WcS+J7B6JRUV099jd80bIVNfxHeUfeFleyQBAwDVsQrJmmWHXahPy4kAFLfUf092Hsz9HWw==</DQ><InverseQ>K2gi5JQwTa9fpyqap3ZSf6Y1iJn8YgF0FNRty2RWqX8lqxWYKVn8xKR7tUwGauRL/uAbPpO8VuE8YdpWFctWoA==</InverseQ><D>mM3a7OKzhC8RQ2ee28Bw8Vg5t7Gp2U47d6M8PkvxeOjVactNoQ+uZe7czoDQ9upRCltpx5qi6Qh9RZLYClFpzb/WiE3l43tT0tIzZzIVcVZBrShV98DxQUkZddeVI1uRWTDBQQDRTgzhXUfIMr6z6sOmYr69VMOE1YlPRm4vmak=</D></RSAKeyValue>";

        public static string Encrypt(string Source)
        {
            ASCIIEncoding ByteConverter = new ASCIIEncoding();
            byte[] bytes = ByteConverter.GetBytes(Source);

            using (RSACryptoServiceProvider rsa = new RSACryptoServiceProvider())
            {
                rsa.FromXmlString(PublicKey);
                bytes = rsa.Encrypt(bytes, false);
            }

            return Convert.ToBase64String(bytes);
        }

        public static string Decrypt(string Source)
        {
            ASCIIEncoding ByteConverter = new ASCIIEncoding();
            byte[] bytes = Convert.FromBase64String(Source);
            using (RSACryptoServiceProvider rsa = new RSACryptoServiceProvider())
            {
                rsa.FromXmlString(PrivateKey);
                bytes = rsa.Decrypt(bytes, false);
            }
            return ByteConverter.GetString(bytes);
        }


        private static string[] GenerateKeys()
        {
            string[] keys = new string[2];
            ASCIIEncoding ByteConverter = new ASCIIEncoding();
            CspParameters param = new CspParameters();
            param.Flags = CspProviderFlags.UseMachineKeyStore;
            using (RSACryptoServiceProvider rsa = new RSACryptoServiceProvider())
            {
                keys[0] = rsa.ToXmlString(false);
                keys[1] = rsa.ToXmlString(true);

                //byte[] bytes = ByteConverter.GetBytes(rsa.ToXmlString(false));
                //keys[0] = Convert.ToBase64String(bytes);
                //bytes = ByteConverter .GetBytes( rsa.ToXmlString(true));
                //keys[1] = Convert.ToBase64String(bytes);
            }
            return keys;
        }

        public static void Main()
        {
           

        }
    }
}


