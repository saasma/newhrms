﻿using System;
using System.IO;
using System.Threading;
using System.Text;
using System.Security.Cryptography;
using System.IO.IsolatedStorage;
using System.Collections;
using System.Collections.Generic;


namespace Utils.Security
{
    /// <summary>
    /// Manages the Encryption/Decryption of the documents used in the system using RijndaelManaged algorithm
    /// </summary>
    public class EncryptorDecryptor
    {
        // Fields
        private byte[] IVByte;
        private byte[] keyByte;
        private RijndaelManaged myrijManaged;
        private string password;
        private byte[] saltByte;

        // Methods
        public EncryptorDecryptor()
        {
            Init();
        }

        private void Init()
        {
            this.password = "fgy7ujm%";
            this.saltByte = Encoding.ASCII.GetBytes("sdf4435dffgdg345");
            this.keyByte = null;
            this.IVByte = null;
            this.myrijManaged = new RijndaelManaged();
            this.GenerateIVandKey();
        }

        /// <summary>
        /// Encrypts the document file
        /// </summary>
        /// <param name="strInputFile">Represents the location of unencrypted file</param>
        /// <param name="strOutputFile">Represents the location of encrypted file</param>
        public string Encrypt(string plainText)
        {

            // System.IO.FileStream fsInput = null;
            
            MemoryStream stream = null;
            CryptoStream csCryptoStream = null;
            try
            {
                ////Setup file streams to handle input and output.
                ////fsInput = new System.IO.FileStream(strInputFile, FileMode.Open, FileAccess.Read);
                //fsOutput = new System.IO.FileStream(strOutputFile, FileMode.OpenOrCreate, FileAccess.Write);
                //fsOutput.SetLength(0);
                ////Declare variables for encrypt/decrypt process.
                //byte[] bytBuffer = new byte[4097];
                ////holds a block of bytes for processing
                //long lngBytesProcessed = 0;
                ////running count of bytes processed
                //long lngFileLength = inputStream.Length;// fsInput.Length;
                ////the input file's length
                //int intBytesInCurrentBlock = 0;
                ////current bytes being processed


                stream = new MemoryStream();
                // Define memory stream which will be used to hold encrypted data.
                byte[] plainTextBytes = Encoding.UTF8.GetBytes(plainText);
                // Define cryptographic stream (always use Write mode for encryption).
                csCryptoStream = new CryptoStream(stream, myrijManaged.CreateEncryptor(keyByte, IVByte), CryptoStreamMode.Write);

                // Start encrypting.
                csCryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);

                // Finish encrypting.
                csCryptoStream.FlushFinalBlock();


                // Convert our encrypted data from a memory stream into a byte array.
                byte[] cipherTextBytes = stream.ToArray();

                // Convert encrypted data into a base64-encoded string.
              return Convert.ToBase64String(cipherTextBytes);
            }
            catch (Exception exp)
            {
                Log.log("Error encrypting", exp);
                //ErrorLogger.LogError(exp, "Document decrypting error...");
                return "";
            }
            finally
            {
                if (stream != null)
                    stream.Close();
                if (csCryptoStream != null)
                    csCryptoStream.Close();
                //if (fsInput != null)
                //    fsInput.Close();
                
            }
            //return "";
        }


        /// <summary>
        /// Encrypts the document file
        /// </summary>
        /// <param name="strInputFile">Represents the location of unencrypted file</param>
        /// <param name="strOutputFile">Represents the location of encrypted file</param>
        public bool Encrypt(Stream inputStream, string strOutputFile)
        {

           // System.IO.FileStream fsInput = null;
            System.IO.FileStream fsOutput = null;
            CryptoStream csCryptoStream = null;
            try
            {
                //Setup file streams to handle input and output.
                //fsInput = new System.IO.FileStream(strInputFile, FileMode.Open, FileAccess.Read);
                fsOutput = new System.IO.FileStream(strOutputFile, FileMode.OpenOrCreate, FileAccess.Write);
                fsOutput.SetLength(0);
                //Declare variables for encrypt/decrypt process.
                byte[] bytBuffer = new byte[4097];
                //holds a block of bytes for processing
                long lngBytesProcessed = 0;
                //running count of bytes processed
                long lngFileLength = inputStream.Length;// fsInput.Length;
                //the input file's length
                int intBytesInCurrentBlock = 0;
                //current bytes being processed

                csCryptoStream = new CryptoStream(fsOutput, myrijManaged.CreateEncryptor(keyByte, IVByte), CryptoStreamMode.Write);

                //Use While to loop until all of the file is processed.
                while (lngBytesProcessed < lngFileLength)
                {
                    //Read file with the input filestream.
                    intBytesInCurrentBlock = inputStream.Read(bytBuffer, 0, 4096);
                    //Write output file with the cryptostream.
                    csCryptoStream.Write(bytBuffer, 0, intBytesInCurrentBlock);
                    //Update lngBytesProcessed
                    lngBytesProcessed = lngBytesProcessed + (long)intBytesInCurrentBlock;
                }
            }
            catch (Exception exp)
            {
                Log.log("Error encrypting", exp);
                //ErrorLogger.LogError(exp, "Document decrypting error...");
                return false;
            }
            finally
            {
                if (csCryptoStream != null)
                    csCryptoStream.Close();
                //if (fsInput != null)
                //    fsInput.Close();
                if (fsOutput != null)
                    fsOutput.Close();
            }
            return true;
        }

        /// <summary>
        /// Descrypts the document
        /// </summary>
        /// <param name="strInputFile">Represents the location of encrypted file</param>
        /// <returns>Decrypted document content</returns>
        public string Decrypt(string encryptedBase64Text)
        {

             byte[] cipherTextBytes = Convert.FromBase64String(encryptedBase64Text);
           
            MemoryStream ms = null;
            CryptoStream csCryptoStream = null;
            try
            {
               // ms = new MemoryStream

               
                // Since at this point we don't know what the size of decrypted data
                // will be, allocate the buffer long enough to hold ciphertext;
                // plaintext is never longer than ciphertext.
                byte[] plainTextBytes = new byte[cipherTextBytes.Length];

                ms = new MemoryStream(cipherTextBytes);

                csCryptoStream = new CryptoStream(ms, myrijManaged.CreateDecryptor(keyByte, IVByte), CryptoStreamMode.Read);

               

                // Start decrypting.
                int decryptedByteCount = csCryptoStream.Read(plainTextBytes,
                                                           0,
                                                           plainTextBytes.Length);


                // Convert decrypted data into a string. 
                // Let us assume that the original plaintext string was UTF8-encoded.
                return Encoding.UTF8.GetString(plainTextBytes,
                                                           0,
                                                           decryptedByteCount);
            }
            catch (Exception exp)
            {
                Log.log("Error decrypting", exp);

                //                ErrorLogger.LogError(exp, "Document decrypting error...");
                return "";
            }
            finally
            {
                if (ms != null)
                    ms.Close();
                if (csCryptoStream != null)
                    csCryptoStream.Close();
              
            }

            //return "";
        }


        /// <summary>
        /// Descrypts the document
        /// </summary>
        /// <param name="strInputFile">Represents the location of encrypted file</param>
        /// <returns>Decrypted document content</returns>
        public byte[] DecryptAsStream(string strInputFile)
        {

            System.IO.FileStream fsInput = null;
            //Dim fsOutput As System.IO.FileStream
            MemoryStream ms = null;
            CryptoStream csCryptoStream = null;
            try
            {
                //Setup file streams to handle input and output.
                fsInput = new System.IO.FileStream(strInputFile, FileMode.Open, FileAccess.Read);
                ms = new MemoryStream();

                //Declare variables for encrypt/decrypt process.
                byte[] bytBuffer = new byte[4097];
                //holds a block of bytes for processing
                long lngBytesProcessed = 0;
                //running count of bytes processed
                long lngFileLength = fsInput.Length;
                //the input file's length
                int intBytesInCurrentBlock = 0;
                //current bytes being processed

                csCryptoStream = new CryptoStream(ms, myrijManaged.CreateDecryptor(keyByte, IVByte), CryptoStreamMode.Write);

                //Use While to loop until all of the file is processed.
                while (lngBytesProcessed < lngFileLength)
                {
                    //Read file with the input filestream.
                    intBytesInCurrentBlock = fsInput.Read(bytBuffer, 0, 4096);
                    //Write output file with the cryptostream.
                    csCryptoStream.Write(bytBuffer, 0, intBytesInCurrentBlock);
                    //Update lngBytesProcessed

                    lngBytesProcessed = lngBytesProcessed + (long)intBytesInCurrentBlock;
                }
            }
            catch(Exception exp)
            {
                Log.log("Error decrypting", exp);
            
//                ErrorLogger.LogError(exp, "Document decrypting error...");
                return null;
            }
            finally
            {
                if (csCryptoStream != null)
                    csCryptoStream.Close();
                if (fsInput != null)
                    fsInput.Close();
            }

            if (ms != null)
                return ms.ToArray();
            else
                return null;
        }

        /// <summary>
        /// Generates the keys for the Encryption algorithm
        /// </summary>
        private void GenerateIVandKey()
        {
            Rfc2898DeriveBytes myDeriveBytes = new Rfc2898DeriveBytes(this.password, this.saltByte);
            this.keyByte = myDeriveBytes.GetBytes(this.myrijManaged.KeySize / 8);
            this.IVByte = myDeriveBytes.GetBytes(this.myrijManaged.BlockSize / 8);
        }

    }
}


