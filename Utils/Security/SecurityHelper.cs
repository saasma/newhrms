﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using System.Security.Cryptography;


namespace Utils.Security
{
    /// <summary>
    /// Contains security helper methods
    /// </summary>
    public class SecurityHelper
    {

        public static bool? hasPwd = null;
        public static string pwd = null;
        public static bool? isValidToRun = null;


        public static string ExecutePagePwd = "8937534f-7b06-44b0-87a1-829920759e7f";

        /// <summary>
        /// Create encrypted authentication ticket to be used for login
        /// </summary>
        /// <param name="userName">Username that has logged in</param>
        /// <param name="removeFirst">Try to delete the existing cookie first is its value is true</param>
        /// <param name="role">Role of the user which will be integrated into UserData</param>
        /// <param name="isPersistent">If the cookie is persistent or not</param>
        public static string CreateCookie(string userName, bool removeFirst, string role, bool isPersistent, bool isADLogin)
        {
            HttpCookie cookie = FormsAuthentication.GetAuthCookie(userName,
                       isPersistent);
            cookie.HttpOnly = true;
            if (removeFirst)
              HttpContext.Current.Response.Cookies.Remove(cookie.Name);

            FormsAuthenticationTicket ticket = FormsAuthentication.Decrypt(cookie.Value);
            string userData = role +":" + isADLogin;

            FormsAuthenticationTicket ticket1 = new
                 FormsAuthenticationTicket(ticket.Version, ticket.Name, ticket.IssueDate,
                 ticket.Expiration, ticket.IsPersistent, userData, ticket.CookiePath);
            
            string encValue = FormsAuthentication.Encrypt(ticket1);

            cookie.Value = encValue;
            HttpContext.Current.Response.Cookies.Add(cookie);

            return encValue;
        }

        public static bool IsLDAPLogin()
        {
                string cookieName = FormsAuthentication.FormsCookieName;
            HttpCookie authCookie = HttpContext.Current.Request.Cookies[cookieName];
            if (null == authCookie)
            {
                // There is no authentication cookie.
                return false;
            }
            FormsAuthenticationTicket authTicket = null;
            try
            {
                authTicket = FormsAuthentication.Decrypt(authCookie.Value);
            }
            catch (Exception ex)
            {
                // Log exception details (omitted for simplicity)
                return false;
            }
            if (null == authTicket)
            {
                // Cookie failed to decrypt.
                return false;
            }
            // When the ticket was created, the UserData property wasassigned
            // a pipe delimited string of role names.
            string[] roles = new string[1];
            roles[0] = authTicket.UserData.ToString().Split(new char[] { ':' })[1];

            return bool.Parse(roles[0]);
        }

        public static string GetHash(string value)
        {
            return FormsAuthentication.HashPasswordForStoringInConfigFile(value, "SHA1");
        }

        public static string GetHash(string value, bool? isNew)
        {
            // for this case it was old hashing without salt, now it uses SHA2
            if (isNew == null || isNew == false)
            {
                HashAlgorithm algorithm = HashAlgorithm.Create("SHA512");
                //if (algorithm == null)
                //{
                //    throw new ArgumentException("Unrecognized hash name", "hashName");
                //}
                byte[] hash = algorithm.ComputeHash(Encoding.UTF8.GetBytes(value));
                return Convert.ToBase64String(hash);

            }
            else
                return FormsAuthentication.HashPasswordForStoringInConfigFile(value + Encoding.ASCII.GetBytes("334k5l34543j4j4"), "SHA1");

            //if (isNew == null || isNew == false)
            //    return FormsAuthentication.HashPasswordForStoringInConfigFile(value, "SHA1");
            //else
            //    return FormsAuthentication.HashPasswordForStoringInConfigFile(value +  Encoding.ASCII.GetBytes("334k5l34543j4j4"), "SHA1");
        }

        public static string EncryptServiceKey(string value)
        {
            return AsymmCrypto.Encrypt(value);
        }
        public static string DescriptServiceKey(string value)
        {
            return AsymmCrypto.Decrypt(value);
        }
    }
}