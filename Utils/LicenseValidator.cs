﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Web;
using System.Collections.Specialized;
using System.Xml;
using System.IO;
using Utils.Security;

namespace Utils
{
    public class LicenseValidator
    {
        /// <summary>
        /// If the Licensing is enabled or not
        /// </summary>
        public static readonly  bool isLicenseEnabled = false;
        /// <summary>
        /// License application/resource name
        /// </summary>
        public const string LicenseFileName = "~/App_Data/lic.dat";

        public const string MaxCompanyText = "MaxCompany";


        public static bool IsAllowedToRun
        {
            get
            {

                return bool.Parse(HttpContext.Current.Application["IsAllowedToRun"].ToString());
            }
            set
            {
                HttpContext.Current.Application.Lock();
                HttpContext.Current.Application["IsAllowedToRun"] = value;
                HttpContext.Current.Application.UnLock();
            }
        }


        public static int MaxCompany
        {
            get
            {
                return int.Parse(HttpContext.Current.Application["MaxCompany"].ToString());
            }
            set
            {
                HttpContext.Current.Application.Lock();
                HttpContext.Current.Application["MaxCompany"] = value;
                HttpContext.Current.Application.UnLock();
            }
        }

        public static string LicenseErrorMessage
        {
            get
            {
                if (HttpContext.Current.Application["LicenseErrorMessage"] != null)
                    return HttpContext.Current.Application["LicenseErrorMessage"].ToString();
                return "";
            }
            set
            {
                HttpContext.Current.Application.Lock();
                HttpContext.Current.Application["LicenseErrorMessage"] = value;
                HttpContext.Current.Application.UnLock();
            }
        }


        /// <summary>
        /// Warning message for demo/license expiration
        /// </summary>
        public string Message { get; set; }
        /// <summary>
        /// If the app. is demo version
        /// </summary>
        public bool IsDemoVersion { get; set; }
        /// <summary>
        /// If the demo version already expired
        /// </summary>
        public bool IsDemoExpired { get; set; }
        /// <summary>
        /// If the app. is licensed version
        /// </summary>
        public bool IsLicensed { get; set; }
        /// <summary>
        /// If the licensed version already expired
        /// </summary>
        public bool IsLicensedExpired { get; set; }


        public bool IsCustomDataInValid { get; set; }


        public void Validate()
        {
            //if (LicenseValidator.isLicenseEnabled)
            //{
            //    IsAllowedToRun = false;
            //    MaxCompany = 0;

            //    string location = HttpContext.Current.Server.MapPath(LicenseFileName);
            //    if (File.Exists(location) == false)
            //    {
            //        LicenseErrorMessage = "License file is missing.";
            //        return;
            //    }


            //    try
            //    {
            //        //-1 for unlimited
            //        //"MaxCompany=10"
            //        string value = AsymmCrypto.Decrypt(File.ReadAllText(location));
            //        StringDictionary keys = GetFormattedServerData(value);

            //        if (keys[MaxCompanyText] != null)
            //        {
            //            IsAllowedToRun = true;
            //            MaxCompany = int.Parse(keys[MaxCompanyText]);
            //        }
            //    }
            //    catch
            //    {
            //        LicenseErrorMessage = "Invalid or tampered license file.";
            //    }
            //}
            //else
            //{
            //    //unlimited
            //    IsAllowedToRun = true;
            //    MaxCompany = -1;
            //}
        }


        private static StringDictionary GetFormattedServerData(string value)
        {
            try
            {
                //string value = "MaxCompany=10 serverurl=localhost,maxusers=10,maxrows=10,validto=2010-6-30"; //validto=permanent
                if (value != null)
                {
                    try
                    {
                        StringDictionary data = new StringDictionary();
                        string[] values = value.Split(new char[] { ';' });

                        foreach (string str in values)
                        {
                            string[] eachValues = str.Split(new char[] { '=' });
                            data.Add(eachValues[0].Trim(), eachValues[1].Trim());
                        }
                        return data;
                    }
                    catch
                    {
                        return null;
                    }
                }
                else
                    return null;
            }
            catch
            {
                return null;
            }
        }

    }
}

