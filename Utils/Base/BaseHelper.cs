﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using Utils.Helper;

namespace Utils.Base
{
    public static class BaseHelper
    {
        public static void DisableChildControls(Control parent)
        {
            foreach (Control ctl in parent.Controls)
            {
                
                if ((ctl as BaseValidator) == null)
                {
                    WebControl webCtl = ctl as WebControl;
                    if (webCtl != null)
                    {
                        CheckBoxList chkList = webCtl as CheckBoxList;
                        if ( chkList != null)
                        {
                            //chkList.Attributes.Add("disabled", "disabled");
                            //for checkbox list enabled = false made, then in postback selected value is not preserved so this way
                            //foreach (ListItem item in chkList.Items)
                            //{
                                
                            //    item.Attributes.Add("disabled", "disabled");
                            //}
                            parent.Page.ClientScript.RegisterClientScriptBlock(
                                parent.GetType(), new Guid().ToString(), "$(document).ready( function(){ disableElement('" + chkList.ClientID + "'); } );", true);
                        }
                        else
                            webCtl.Enabled = false;
                    }
                }
                DisableChildControls(ctl);
            }
        }

        public  static void EnabledChildControls(Control parent)
        {
            foreach (Control ctl in parent.Controls)
            {
                if ((ctl as BaseValidator) == null)
                {
                    WebControl webCtl = ctl as WebControl;
                    if (webCtl != null)
                    {
                        webCtl.Enabled = true;
                    }
                }
                EnabledChildControls(ctl);
            }
        }

        public  static void DisableChildValidationControls(Control parent)
        {
            foreach (Control ctl in parent.Controls)
            {
                //RequiredFieldValidator f;
                //BaseValidator
                BaseValidator webCtl = ctl as BaseValidator;
                if (webCtl != null)
                {
                    webCtl.Enabled = false;
                }
                DisableChildValidationControls(ctl);
            }
        }

        public static void EnabledChildValidationControls(Control parent)
        {
            foreach (Control ctl in parent.Controls)
            {
                BaseValidator webCtl = ctl as BaseValidator;
                if (webCtl != null)
                {
                    webCtl.Enabled = true;
                }
                EnabledChildValidationControls(ctl);
            }
        }


        public static string GetCurrency(object value,int decimalPlaces)
        {

            return Util.FormatAmountWithProperDecimal(Convert.ToDecimal(value),
                                                     decimalPlaces);
            
        }
    }
}
