﻿using System;
using System.Web.UI.WebControls.Adapters;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using System.Web;

//This codes makes the dropdownlist control recognize items with "--"'
//for the label or items with an OptionGroup attribute and render them'
//as  instead of .'
namespace Utils.Web
{

    public class CustomDropDownList : WebControlAdapter
    {

        protected override void RenderContents(HtmlTextWriter writer)
        {
            DropDownList list = Control as DropDownList;
            string currentOptionGroup;
            List<string> renderedOptionGroups = new List<string>();

            foreach (ListItem item in list.Items)
            {
                if (item.Attributes["OptionGroup"] != null)
                {
                    //'The item is part of an option group'
                    currentOptionGroup = item.Attributes["OptionGroup"];
                    //'the option header was already written, just render the list item'
                    if (renderedOptionGroups.Contains(currentOptionGroup))
                        RenderListItem(item, writer);
                    else
                    {
                        //the header was not written- do that first'
                        if (renderedOptionGroups.Count > 0)
                            RenderOptionGroupEndTag(writer); //'need to close previous group'
                        RenderOptionGroupBeginTag(currentOptionGroup, writer);
                        renderedOptionGroups.Add(currentOptionGroup);
                        RenderListItem(item, writer);
                    }
                }
                else if (item.Text == "--") //simple separator
                {
                    RenderOptionGroupBeginTag("--", writer);
                    RenderOptionGroupEndTag(writer);
                }
                else
                {
                    //default behavior: render the list item as normal'
                    RenderListItem(item, writer);
                }
            }

            if (renderedOptionGroups.Count > 0)
                RenderOptionGroupEndTag(writer);
        }

        private void RenderOptionGroupBeginTag(string name, HtmlTextWriter writer)
        {
            writer.WriteBeginTag("optgroup");
            writer.WriteAttribute("label", name);
            writer.Write(HtmlTextWriter.TagRightChar);
            writer.WriteLine();
        }

        private void RenderOptionGroupEndTag(HtmlTextWriter writer)
        {
            writer.WriteEndTag("optgroup");
            writer.WriteLine();
        }

        private void RenderListItem(ListItem item, HtmlTextWriter writer)
        {
            writer.WriteBeginTag("option");
            writer.WriteAttribute("value", item.Value, true);
            if (item.Selected)
                writer.WriteAttribute("selected", "selected", false);


            foreach (string key in item.Attributes.Keys)
                writer.WriteAttribute(key, item.Attributes[key]);

            writer.Write(HtmlTextWriter.TagRightChar);
            HttpUtility.HtmlEncode(item.Text, writer);
            writer.WriteEndTag("option");
            writer.WriteLine();
        }

    }
}