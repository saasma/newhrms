using System;
using System.Data;
using System.Configuration;
using System.IO;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;
using System.Xml.Xsl;
using System.Text;
using System.Reflection;
using System.IO;
using System.Data;
using System.Diagnostics;
using System.Xml;

namespace Utils.Web
{
    /// <summary>
    /// 
    /// </summary>
    public class GridViewExportUtil
    {

        public static void ExportToExcel<T>(string name, List<T> list, List<String> hiddenColumnList, List<string> totalColumnList, Dictionary<string, string> columnRenameList, List<string> numericColumnList, Dictionary<string, string> title, List<String> propertyNamesOrderList)
        {

            int columnCount = 0;
            List<int> hiddenColumn = new List<int>();
            List<int> totalColumn = new List<int>();
            //numeric data type column
            List<int> numericColum = new List<int>();
            Dictionary<int, decimal> columnTotal = new Dictionary<int, decimal>();
            DateTime StartTime = Utils.Helper.Util.GetCurrentDateAndTime();
            StringBuilder rowData = new StringBuilder();

            //set title
            if (title != null)
            {
                foreach (var v in title)
                {
                    rowData.Append("<Row>");
                    rowData.Append(String.Format("<Cell ss:StyleID=\"s70\"><Data ss:Type=\"String\">{0}</Data></Cell>", v.Key));
                    rowData.Append(String.Format("<Cell ss:StyleID=\"s70\"><Data ss:Type=\"String\">{0}</Data></Cell>", v.Value));
                    rowData.Append("</Row>");

                }
            }
            //add empty row
            rowData.Append("<Row></Row>");
            PropertyInfo[] properties = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);


            if (propertyNamesOrderList != null && propertyNamesOrderList.Count != 0)
            {
                List<PropertyInfo> orderList = new List<PropertyInfo>();
                foreach (string property in propertyNamesOrderList)
                {
                    foreach (PropertyInfo p in properties)
                    {
                        if (p.Name == property)
                        {
                            orderList.Add(p);
                        }
                    }
                }

                foreach (PropertyInfo p in properties)
                {
                    if (orderList.Any(x => x.Name == p.Name) == false)
                    {
                        orderList.Add(p);
                    }
                }

                properties = orderList.ToArray();

            }
            rowData.Append("<Row >");
            Boolean HasDepthLevel = false;
            //contains depth level value
            int depthIndex = 0;
            foreach (PropertyInfo p in properties)
            {
                if (p.Name.ToString().Equals("DepthLevel"))
                {
                    HasDepthLevel = true;
                    depthIndex = columnCount;
                }
                if (p.PropertyType.Name != "EntityCollection`1" && p.PropertyType.Name != "EntityReference`1" && p.PropertyType.Name != p.Name)
                {

                    if (hiddenColumnList.Contains(p.Name))
                    {

                        hiddenColumn.Add(columnCount);

                    }

                    else
                    {
                        // track the column to calculate the total value
                        if (totalColumnList.Contains(p.Name))
                        {
                            totalColumn.Add(columnCount);
                            columnTotal.Add(columnCount, 0);
                        }
                        else if (numericColumnList.Contains(p.Name))
                        {
                            //track numeric colum to set the data type numeric
                            numericColum.Add(columnCount);
                        }

                        //prepare column header name
                        string headerName = string.Empty;
                        if (columnRenameList.ContainsKey(p.Name))
                        {
                            headerName = columnRenameList[p.Name];
                        }
                        else
                        {
                            headerName = p.Name;
                        }
                        rowData.Append(String.Format("<Cell ss:StyleID=\"s44\"><Data ss:Type=\"String\">{0}</Data></Cell>", headerName));
                    }
                    columnCount++;

                }
                else
                    break;

            }

            rowData.Append("</Row>");
            decimal total = 0;
            foreach (T item in list)
            {
                int paddingColumn = 0;
                rowData.Append("<Row>");
                for (int x = 0; x < columnCount; x++) //each (PropertyInfo p in properties)
                {
                    if (!hiddenColumn.Contains(x)) //skip hidden column
                    {
                        object o = properties[x].GetValue(item, null);
                        string cellValue = o == null ? "" : o.ToString();
                        string value = o == null ? "" : o.ToString();

                        //total calculation
                        if (totalColumn.Contains(x))
                        {
                            if (value.Equals(""))
                            {
                                value = "0";
                            }
                            total = decimal.Parse(value) + columnTotal[x];
                            columnTotal.Remove(x);
                            columnTotal.Add(x, total);

                            //For trial balance padding
                            if (HasDepthLevel && paddingColumn == 0)
                            {
                                object obj = properties[depthIndex].GetValue(item, null);
                                string spacing = GetDepthLevelSpacing(obj);
                                //text bold style
                                if ((int)obj < 1)
                                {
                                    rowData.Append(String.Format("<Cell ss:StyleID=\"s66\"><Data ss:Type=\"Number\">{0}{1}</Data></Cell>", spacing, cellValue));
                                }
                                else
                                    rowData.Append(String.Format("<Cell ss:StyleID=\"s63\"><Data ss:Type=\"Number\">{0}{1}</Data></Cell>", spacing, cellValue));
                            }
                            else
                            {
                                rowData.Append(String.Format("<Cell ss:StyleID=\"s63\"><Data ss:Type=\"Number\">{0}</Data></Cell>", cellValue));
                            }


                        }
                        else
                        {

                            if (HasDepthLevel && paddingColumn == 0)
                            {

                                object obj = properties[depthIndex].GetValue(item, null);
                                string spacing = GetDepthLevelSpacing(obj);
                                //text bold style
                                if ((int)obj < 1)
                                {
                                    rowData.Append(String.Format("<Cell  ss:StyleID=\"s66\"><Data ss:Type=\"String\">{0}{1}</Data></Cell>", spacing, cellValue));
                                }
                                else
                                    rowData.Append(String.Format("<Cell><Data ss:Type=\"String\">{0}{1}</Data></Cell>", spacing, cellValue));
                            }
                            else if (numericColum.Contains(x)) //sets data type numeric
                            {
                                rowData.Append(String.Format("<Cell ss:StyleID=\"s63\"><Data ss:Type=\"Number\">{0}</Data></Cell>", cellValue));
                            }
                            else
                            {
                                rowData.Append(String.Format("<Cell><Data ss:Type=\"String\">{0}</Data></Cell>", cellValue));
                            }
                        }

                        //increament spacing for column so that spacing for the next column will not be applied
                        paddingColumn++;

                    }
                }

                rowData.Append("</Row >");

            }

            Boolean IsfirstColumn = true;
            if (totalColumnList.Count != 0)
            {
                rowData.Append("<Row >");
                for (int x = 0; x < columnCount; x++)
                {
                    string value = "";
                    if (!hiddenColumn.Contains(x))
                    {
                        if (IsfirstColumn)
                        {
                            //Total label
                            rowData.Append("<Cell ss:StyleID=\"sTotal\"><Data ss:Type=\"String\">" + "Total" + "</Data></Cell>");
                            IsfirstColumn = false;
                        }
                        else if (totalColumn.Contains(x))
                        {
                            rowData.Append(String.Format("<Cell ss:StyleID=\"s14\" ><Data ss:Type=\"Number\">{0}</Data></Cell>", columnTotal[x]));

                        }
                        else
                        {
                            rowData.Append(String.Format("<Cell ss:StyleID=\"sTotal\"><Data ss:Type=\"String\">{0}</Data></Cell>", value));
                        }
                    }

                }
                rowData.Append("</Row>");
            }

            string url = HttpContext.Current.Server.MapPath("~/App_Data/ExcelTemplate.xml");
            StringBuilder sheet = new StringBuilder();
            //here 1 is for empty row
            sheet.AppendFormat(GetXmlString(url), properties.Count(), list.Count() + 2 + title.Count() + 1, rowData.ToString());
            System.Diagnostics.Debug.Print(StartTime.ToString() + " - " + Utils.Helper.Util.GetCurrentDateAndTime());
            System.Diagnostics.Debug.Print((Utils.Helper.Util.GetCurrentDateAndTime() - StartTime).ToString());
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.Charset = "";
            string attachment = "attachment; filename=\"" + name + "\".xls";
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.AddHeader("content-disposition", attachment);
            HttpContext.Current.Response.Write(sheet.ToString());
            HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";
            HttpContext.Current.Response.End();

        }


        public static void ExportToExcel<T>(string name, List<T> list, List<String> hiddenColumnList, List<string> totalColumnList)
        {

            int columnCount = 0;
            List<int> hiddenColumn = new List<int>();
            List<int> totalColumn = new List<int>();
            Dictionary<int, decimal> columnTotal = new Dictionary<int, decimal>();
            DateTime StartTime = DateTime.Now;
            StringBuilder rowData = new StringBuilder();
            PropertyInfo[] properties = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            rowData.Append("<Row ss:StyleID=\"s44\">");
            Boolean HasDepthLevel = false;
            //contains depth level value
            int depthIndex = 0;
            foreach (PropertyInfo p in properties)
            {
                if (p.Name.ToString().Equals("DepthLevel"))
                {
                    HasDepthLevel = true;
                    depthIndex = columnCount;
                }
                if (p.PropertyType.Name != "EntityCollection`1" && p.PropertyType.Name != "EntityReference`1" && p.PropertyType.Name != p.Name)
                {

                    if (hiddenColumnList.Contains(p.Name))
                    {

                        hiddenColumn.Add(columnCount);

                    }
                    else
                    {
                        // track the column to calculate the total value
                        if (totalColumnList.Contains(p.Name))
                        {
                            totalColumn.Add(columnCount);
                            columnTotal.Add(columnCount, 0);
                        }

                        rowData.Append("<Cell><Data ss:Type=\"String\">" + p.Name + "</Data></Cell>");
                    }
                    columnCount++;

                }
                else
                    break;

            }

            rowData.Append("</Row>");
            decimal total = 0;
            foreach (T item in list)
            {
                int paddingColumn = 0;
                rowData.Append("<Row>");
                for (int x = 0; x < columnCount; x++) //each (PropertyInfo p in properties)
                {
                    if (!hiddenColumn.Contains(x)) //skip hidden column
                    {
                        object o = properties[x].GetValue(item, null);
                        string value = o == null ? "" : o.ToString();

                        //total calculation
                        if (totalColumn.Contains(x))
                        {
                            if (value.Equals(""))
                            {
                                value = "0";
                            }
                            total = decimal.Parse(value) + columnTotal[x];
                            columnTotal.Remove(x);
                            columnTotal.Add(x, total);

                            //For trial balance padding
                            if (HasDepthLevel && paddingColumn == 0)
                            {
                                object obj = properties[depthIndex].GetValue(item, null);
                                string spacing = GetDepthLevelSpacing(obj);
                                rowData.Append("<Cell ss:StyleID=\"s63\"><Data ss:Type=\"Number\">" + spacing + value + "</Data></Cell>");
                            }
                            else
                            {
                                rowData.Append("<Cell ss:StyleID=\"s63\"><Data ss:Type=\"Number\">" + value + "</Data></Cell>");
                            }


                        }
                        else
                        {

                            if (HasDepthLevel && paddingColumn == 0)
                            {

                                object obj = properties[depthIndex].GetValue(item, null);
                                string spacing = GetDepthLevelSpacing(obj);
                                rowData.Append("<Cell><Data ss:Type=\"String\">" + spacing + value + "</Data></Cell>");
                            }
                            else
                            {
                                rowData.Append("<Cell><Data ss:Type=\"String\">" + value + "</Data></Cell>");
                            }
                        }

                        //increament spacing column so that spacing for the next column will not be applied
                        paddingColumn++;

                    }
                }

                rowData.Append("</Row >");

            }

            //Boolean IsfirstColumn = true;
            //rowData.Append("<Row ss:StyleID=\"sTotal\">");
            //for (int x = 0; x < columnCount; x++)
            //{
            //    string value = "";
            //    if (!hiddenColumn.Contains(x))
            //    {
            //        if (IsfirstColumn)
            //        {
            //            //Total label
            //            rowData.Append("<Cell><Data ss:Type=\"String\">" + "Total" + "</Data></Cell>");
            //            IsfirstColumn = false;
            //        }
            //        else if (totalColumn.Contains(x))
            //        {
            //            rowData.Append("<Cell ss:StyleID=\"s63\"><Data ss:Type=\"Number\">" + columnTotal[x] + "</Data></Cell>");

            //        }
            //        else
            //        {
            //            rowData.Append("<Cell><Data ss:Type=\"String\">" + value + "</Data></Cell>");
            //        }
            //    }

            //}
            //rowData.Append("</Row>");
            string url = HttpContext.Current.Server.MapPath("~/App_Data/ExcelTemplate/ExcelTemplate.xml");
            StringBuilder sheet = new StringBuilder();
            sheet.AppendFormat(GetXmlString(url), properties.Length, list.Count + 2, rowData.ToString());
         
            string attachment = "attachment; filename=\"" + name + "\".xls";
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.AddHeader("content-disposition", attachment);
            HttpContext.Current.Response.Write(sheet.ToString());
            HttpContext.Current.Response.ContentType = "application/ms-excel";
            HttpContext.Current.Response.End();

        }

        //return row spacing for trial spacing
        public static string GetDepthLevelSpacing(object obj)
        {
            string spacing = string.Empty;
            // object obj = properties[depthIndex].GetValue(item, null);
            string spaceValue = obj == null ? "" : obj.ToString();
            int spaceCount = int.Parse(spaceValue);
            do
            {
                spaceCount--;
                spacing = "   " + spacing;

            }
            while (spaceCount >= 0);
            return spacing;
        }

        static string GetXmlString(string strFile)
        {
            // Load the xml file into XmlDocument object.
            XmlDocument xmlDoc = new XmlDocument();
            try
            {
                xmlDoc.Load(strFile);
            }
            catch (XmlException e)
            {
                //Console.WriteLine(e.Message);
            }
            // Now create StringWriter object to get data from xml document.
            StringWriter sw = new StringWriter();
            XmlTextWriter xw = new XmlTextWriter(sw);
            xmlDoc.WriteTo(xw);
            return sw.ToString();
        }




        /// <summary>
        /// 
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="gv"></param>
        public static void Export(string fileName, GridView gv)
        {
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.AddHeader(
                "content-disposition", string.Format("attachment; filename={0}", fileName));
            HttpContext.Current.Response.ContentType = "application/ms-excel";

            using (StringWriter sw = new StringWriter())
            {
                using (HtmlTextWriter htw = new HtmlTextWriter(sw))
                {
                    //  Create a table to contain the grid
                    Table table = new Table();

                    //  include the gridline settings
                    table.GridLines = GridLines.Both; //gv.GridLines;

                    //  add the header row to the table
                    if (gv.HeaderRow != null)
                    {
                        GridViewExportUtil.PrepareControlForExport(gv.HeaderRow);
                        table.Rows.Add(gv.HeaderRow);
                    }

                    
                    //  add each of the data rows to the table
                    foreach (GridViewRow row in gv.Rows)
                    {
                        GridViewExportUtil.PrepareControlForExport(row);
                        table.Rows.Add(row);
                    }

                    //  add the footer row to the table
                    if (gv.FooterRow != null)
                    {
                        GridViewExportUtil.PrepareControlForExport(gv.FooterRow);
                        table.Rows.Add(gv.FooterRow);
                    }

                    //  render the table into the htmlwriter
                    table.RenderControl(htw);

                    //  render the htmlwriter into the response
                    HttpContext.Current.Response.Write(sw.ToString());
                    HttpContext.Current.Response.End();
                }
            }
        }

        /// <summary>
        /// Replace any of the contained controls with literals
        /// </summary>
        /// <param name="control"></param>
        private static void PrepareControlForExport(Control control)
        {
            for (int i = 0; i < control.Controls.Count; i++)
            {
                Control current = control.Controls[i];
                if (current is LinkButton)
                {
                    control.Controls.Remove(current);
                    control.Controls.AddAt(i, new LiteralControl((current as LinkButton).Text));
                }
                else if (current is ImageButton)
                {
                    control.Controls.Remove(current);
                    control.Controls.AddAt(i, new LiteralControl((current as ImageButton).AlternateText));
                }
                else if (current is HyperLink)
                {
                    control.Controls.Remove(current);
                    control.Controls.AddAt(i, new LiteralControl((current as HyperLink).Text));
                }
                else if (current is DropDownList)
                {
                    control.Controls.Remove(current);
                    control.Controls.AddAt(i, new LiteralControl((current as DropDownList).SelectedItem.Text));
                }
                else if (current is CheckBox)
                {
                    control.Controls.Remove(current);
                    control.Controls.AddAt(i, new LiteralControl((current as CheckBox).Checked ? "True" : "False"));
                }

                if (current.HasControls())
                {
                    GridViewExportUtil.PrepareControlForExport(current);
                }
            }
        }
    }

}