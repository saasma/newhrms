﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using System.Web.UI;

namespace Utils.Web
{
    public static class  UIHelper
    {
        public static void SetSelectedInDropDown(DropDownList ddl, string value)
        {
            if (value == null)
                return;
            ddl.ClearSelection();
            ListItem item = ddl.Items.FindByValue(value);
            if (item != null)
                item.Selected = true;
        }

        public static void SetSelectedLastItemInDropDown(DropDownList ddl)
        {
            ddl.ClearSelection();
            if (ddl.Items.Count > 0)
            {
                ListItem item = ddl.Items[ddl.Items.Count - 1];
                if (item != null)
                    item.Selected = true;
            }
        }

        public static void SelectListItem(DropDownList list)
        {
            list.ClearSelection();
            if( list.Items.Count>0)
            {
                ListItem item = list.Items[list.Items.Count - 1];
                item.Selected = true;
            }
        }

        public static void SetSelectedInDropDownFromText(DropDownList ddl, int? text)
        {
            if (text == null)
                return;
            ddl.ClearSelection();
            ListItem item = ddl.Items.FindByText(text.ToString());
            if (item != null)
                item.Selected = true;
        }
        public static void SetSelectedInDropDownFromText(DropDownList ddl, double? text)
        {
            if (text == null)
                return;
            ddl.ClearSelection();
            ListItem item = ddl.Items.FindByText(text.ToString());
            if (item != null)
                item.Selected = true;
        }
        public static void SetSelectedInDropDownFromText(DropDownList ddl, string text)
        {
            if (text == null)
                return;
            ddl.ClearSelection();
            ListItem item = ddl.Items.FindByText(text.ToString());
            if (item != null)
                item.Selected = true;
        }
        public static void SetSelectedInDropDown(DropDownList ddl, int? value)
        {
            if (value == null)
                return;
            SetSelectedInDropDown(ddl, value.ToString());
        }

        public static void SetText(TextBox txt, string value)
        {
            if (value == null)
                return;
            txt.Text = value;
        }

        public static void SetText(TextBox txt, int? value)
        {
            if (value == null)
                return;
            txt.Text = value.ToString();
        }
        public static void SetText(TextBox txt, double? value)
        {
            if (value == null)
                return;
            txt.Text = value.ToString();
        }
        /// <summary>
        /// Adds the onfocus and onblur attributes to all input controls found in the specified parent,
        /// to change their apperance with the control has the focus
        /// </summary>
        public static void SetInputControlsHighlight(Control container, string className, bool onlyTextBoxes)
        {
            foreach (Control ctl in container.Controls)
            {
                //ctl is DropDownList ||
                if ((onlyTextBoxes && ctl is TextBox) || ctl is TextBox || 
                    ctl is ListBox || ctl is CheckBox || ctl is RadioButton ||
                    ctl is RadioButtonList || ctl is CheckBoxList || ctl is DropDownList )
                {
                    WebControl wctl = ctl as WebControl;
                    if (!string.IsNullOrEmpty(wctl.Attributes["SkipFocusCls"]))
                        continue;
                    wctl.Attributes.Add("onfocus", string.Format("$(this).addClass('{0}');", className));
                    wctl.Attributes.Add("onblur", string.Format("$(this).removeClass('{0}');", className));
                    //wctl.Attributes.Add("onfocus", string.Format("this.className = '{0}';", className));
                    //wctl.Attributes.Add("onblur", "this.className = '';");
                }
                else
                {
                    if (ctl.Controls.Count > 0)
                        SetInputControlsHighlight(ctl, className, onlyTextBoxes);
                }
            }
        }


        public static string GetYesImage(object value,Page page)
        {
            if (value == null)
                return "";
            bool state = Convert.ToBoolean(value);
            if (state)
                return string.Format("<img src='{0}' />", page.ResolveUrl("~/images/yes.png"));
            else
                return "";
        }

        /// <summary>
        /// Returns yes/no image
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string GetYesNoImage(object value)
        {
            if (value == null)
                return "~/images/no.png";
            bool state = Convert.ToBoolean(value);
            if (state)
                return "~/images/yes.png";
            else
                return "~/images/no.png";
        }

        /// <summary>
        /// returns the yes/no image tag, if value is null, then return empty
        /// </summary>
        /// <param name="value"></param>
        /// <param name="page"></param>
        /// <returns></returns>
        public static string GetYesNoImageTag(object value,Page page)
        {
            if (value == null)
                return "";
            return string.Format("<img src='{0}' />", page.ResolveUrl(GetYesNoImage(value)));
        }

        public static void MakeReadonly(params TextBox[] textBoxs)
        {
            foreach (var textBox in textBoxs)
            {
                
                textBox.Attributes.Add("readonly", "readonly");
                textBox.CssClass = "disabledTxt";
            }

        }

        public static void RemoveReadonly(params TextBox[] textBoxs)
        {
            foreach (var textBox in textBoxs)
            {
                textBox.Enabled = true;
                textBox.Attributes.Remove("readonly");
                textBox.CssClass = "";
            }

        }
        public static void SetFirstItemDropDownStyle(DropDownList dropDownList)
        {
            if( dropDownList.Items.Count> 0 && dropDownList.Items[0].Text.Contains("--"))
            {
                dropDownList.Items[0].Attributes.Add("style","color:#999999!important");
            }
        }

        public static void DisableCheckboxList(CheckBoxList list)
        {
            foreach (ListItem item in list.Items)
                item.Enabled = false;
        }
    }
}
