﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace Utils.Helper
{
    public class UrlHelper
    {
        /// <summary>
        /// Gets the root url of the application
        /// </summary>        
        public static string GetRootUrl()
        {
            string Port = HttpContext.Current.Request.ServerVariables["SERVER_PORT"];
            if (Port == null || Port == "80" || Port == "443")
                Port = "";
            else
                Port = ":" + Port;
            string Protocol = HttpContext.Current.Request.ServerVariables["SERVER_PORT_SECURE"];
            if (Protocol == null || Protocol == "0")
                Protocol = "http://";
            else
                Protocol = "https://";

            string BasePath = Protocol + HttpContext.Current.Request.ServerVariables["SERVER_NAME"] +
                                        Port + HttpContext.Current.Request.ApplicationPath;
            return BasePath;
        }


        public static int GetIdFromQueryString(string queryStringName)
        {
            HttpContext context = HttpContext.Current;
            string value = context.Request.QueryString[queryStringName];
            if (value != null)
            {
                int val;
                if (int.TryParse(value, out val))
                    return val;
            }
            return 0;
        }
    }
}
