﻿#region Using directives

using NSpring.Logging;
using NSpring.Logging.DataFormatters;
using NSpring.Logging.EventFormatters;
using NSpring.Logging.Loggers;
using System;
using System.Web;
using System.IO;

#endregion

namespace Utils
{
    public enum LogType
    {
        LogRun = 1, // sql execute by the system
        Schedular=2

    }
	public class Log
	{
		// initialize with default log so that logging
		// always works.
		//
		// A bat file with can be used to show the log: (uses cygwin's tail)
		// cmd /c tail -f /tmp/log.log
		public static readonly Log global = new Log();

		public Logger logger;

		// System.Web.HttpContext.Current.Server.MapPath
		public Log() {
			string filename = @"log";
			logger = OpenLog(filename, false);
		}

		/// <summary>
		/// Open a log at filename (for append)
		/// </summary>
		/// <param name="filename"></param>
		/// <returns></returns>
		public static Logger OpenLog(string filename, bool msgOnlyNoData) {

            try
            {
                string date = DateTime.Now.ToString("d-M-yyyy");

                filename = filename + string.Format("_{0}.log", date);

                Logger logger = new FileLogger(HttpContext.Current.Server.MapPath(Config.LogFileLoc) + filename);

                // DebugLogger logger = new DebugLogger();
                // Logger logger = new TraceLogger();
                logger.Open();

                // EventFormatter eventFormatter = new PatternEventFormatter("\n{message}  {data}");
                string format = "\n{yyyy}-{mm}-{dd} {time}  [{ln:uc}]  {msg}" + ((msgOnlyNoData) ? "" : "\n   {data}");
                EventFormatter eventFormatter = new PatternEventFormatter(format);


                // This is actually the default type of DataFormatter used by PatternEventFormatter,
                // but this line is included for clarity
                eventFormatter.DataFormatter = new FlatDataFormatter();

                logger.EventFormatter = eventFormatter;

                logger.IsContextEnabled = true;

                return logger;
            }
            catch { }
            return null;
		}

		~Log() {
			logger.Close();
		}

		public static void log(string str, object obj) {
			if (global != null && global.logger != null) global.logger.Log(str, obj);
		}

        public static void SaveAdditionalLog(string message,LogType type)
        {
            string date = DateTime.Now.ToString("d-M-yyyy");
            string filename = HttpContext.Current.Server.MapPath(Config.LogFileLoc) +
                type.ToString() + string.Format("_{0}.log", date);

            File.AppendAllText(filename, message);
        }

        
        //public static void log(Level lv, string str, object obj) {
        //    if (global != null && global.logger != null) global.logger.Log(lv, str, obj);
        //}

        //public static void log(string str) {
        //    if (global != null && global.logger != null) global.logger.Log(str);
        //}
	}
}