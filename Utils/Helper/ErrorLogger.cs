using System;
using System.Collections.Generic;
using System.Text;
using Utils;

namespace Utils.Helper
{
    /// <summary>
    /// static class that logs error in specified file 
    /// described in web.config on each error.
    /// </summary>
    public static class ErrorLogger
    {

        /// <summary>
        /// Gets a stacktrace string, including inner exceptions.
        /// </summary>
        /// <param name="ex"></param>
        /// <returns></returns>
        private static string GetStackTrace(Exception ex)
        {
            StringBuilder b = new StringBuilder();
            Exception current = ex;
            int depth = 0;
            while (current != null && depth < 20)
            {
                b.AppendLine(string.Format("{0}: {1}", current.GetType().FullName, current.Message));
                b.AppendLine(current.StackTrace);
                b.AppendLine("");
                current = current.InnerException;
                depth++;
            }
            return b.ToString();
        }

        /// <summary>
        /// Present a lot of information about an exception
        /// </summary>
        /// <param name="ex"></param>
        /// <returns></returns>
        public static string BuildExceptionDescription(Exception ex)
        {
            return string.Format(
                "Unhandled {2}: {0}<br /><br />Source: {3}<br /><br />{1}",
                ex.Message,
                GetStackTrace(ex).Replace("\n", "<br />"),
                ex.GetType().FullName,
                ex.Source);
        }


        /// <summary>
        /// Logs Error of specified Error in txt file whose location is
        /// described in web.config file
        /// </summary>
        /// <param name="errorMessage">Error Message which can be send in as many as comma separated list</param>
        public static void LogError(string errorMessage)
        {
            System.IO.FileStream loggerStream = null;
            System.IO.StreamWriter mylogWriter = null;
            string[] errMessages = errorMessage.Split(new string[] {"$#$#"}, StringSplitOptions.RemoveEmptyEntries);

            try
            {
                string path =
                    System.Web.HttpContext.Current.Server.MapPath(Config.ErrorLogPath + @"\ErrorLog.txt");
                loggerStream = new System.IO.FileStream(path, System.IO.FileMode.Append, System.IO.FileAccess.Write);
                mylogWriter = new System.IO.StreamWriter(loggerStream);

                mylogWriter.WriteLine("\n");
                mylogWriter.WriteLine("Error reported on : " + System.DateTime.Now);
                mylogWriter.WriteLine("Error Type : Javascript ");
                mylogWriter.WriteLine("Server machine name: " + System.Web.HttpContext.Current.Server.MachineName);
                //mylogWriter.WriteLine("Server IP Address: " + //Helper.GetCurrentComputerIPAddress());
                mylogWriter.WriteLine("Server Operating system: " + System.Environment.OSVersion.ToString());
                mylogWriter.WriteLine("Microsoft .net version: " + System.Environment.Version.ToString());
                mylogWriter.WriteLine("Current Request was made to page: " +
                                      System.Web.HttpContext.Current.Request.RawUrl);
                mylogWriter.WriteLine("Browser used: " + System.Web.HttpContext.Current.Request.Browser.Browser +
                                      " Version: " + System.Web.HttpContext.Current.Request.Browser.Version +
                                      " Browser is Beta version?: " +
                                      System.Web.HttpContext.Current.Request.Browser.Beta.ToString());
                mylogWriter.WriteLine("User is authorized?: " +
                                      System.Web.HttpContext.Current.Request.IsAuthenticated.ToString() +
                                      " and is identified as: " + System.Web.HttpContext.Current.User.Identity.Name +
                                      " and is the user is in Admin role?: " +
                                      System.Web.HttpContext.Current.User.IsInRole("Admin").ToString());
                mylogWriter.WriteLine("Error Reports: ");
                mylogWriter.WriteLine("--------------");
                for (int i = 0; i < errMessages.Length; i++)
                {
                    mylogWriter.WriteLine(errMessages[i]);
                }
                mylogWriter.WriteLine("\n\n");
                mylogWriter.WriteLine("*************END************END****************END**************");
            }
            catch
            {
            }
            finally
            {
                if (mylogWriter != null)
                    mylogWriter.Close();
                if (loggerStream != null)
                    loggerStream.Close();
            }
        }

        /// <summary>
        /// Logs Error of specified Error in txt file whose location is
        /// described in web.config file
        /// </summary>
        /// <param name="Exception">Exception generated to be written</param>
        /// <param name="ExtraText">Extra Text to be written. This can be null value</param>
        public static void LogError(System.Exception Exception, string ExtraText)
        {
            System.IO.FileStream loggerStream = null;
            System.IO.StreamWriter mylogWriter = null;
            try
            {
                string path =
                    System.Web.HttpContext.Current.Server.MapPath(Config.ErrorLogPath + @"\ErrorLog.txt");
                loggerStream = new System.IO.FileStream(path, System.IO.FileMode.Append, System.IO.FileAccess.Write);
                mylogWriter = new System.IO.StreamWriter(loggerStream);

                mylogWriter.WriteLine("\n");
                mylogWriter.WriteLine("Error reported on : " + System.DateTime.Now);
                mylogWriter.WriteLine("Error Type= Asp.net ");
                mylogWriter.WriteLine("Server machine name: " + System.Web.HttpContext.Current.Server.MachineName);
                mylogWriter.WriteLine("Server Operating system: " + System.Environment.OSVersion.ToString());
                mylogWriter.WriteLine("Microsoft .net version: " + System.Environment.Version.ToString());
                mylogWriter.WriteLine("Current Request was made to page: " +
                                      System.Web.HttpContext.Current.Request.RawUrl);
                mylogWriter.WriteLine("Browser used: " + System.Web.HttpContext.Current.Request.Browser.Browser +
                                      " Version: " + System.Web.HttpContext.Current.Request.Browser.Version +
                                      " Browser is Beta version?: " +
                                      System.Web.HttpContext.Current.Request.Browser.Beta.ToString());
                mylogWriter.WriteLine("User is authorized?: " +
                                      System.Web.HttpContext.Current.Request.IsAuthenticated.ToString() +
                                      " and is identified as: " + System.Web.HttpContext.Current.User.Identity.Name +
                                      " and is the user is in Admin role?: " +
                                      System.Web.HttpContext.Current.User.IsInRole("Admin").ToString());
                mylogWriter.WriteLine("Error Reports: ");
                mylogWriter.WriteLine("--------------");
                mylogWriter.WriteLine("Error Message: " + Exception.Message);
                mylogWriter.WriteLine("Error Source: " + Exception.Source);
                mylogWriter.WriteLine("Error TargetSite: " + Exception.TargetSite);
                mylogWriter.WriteLine("Error StackTrace: " + Exception.StackTrace);
                mylogWriter.WriteLine("Error InnerException: " + Exception.InnerException);
                if (!string.IsNullOrEmpty(ExtraText))
                {
                    mylogWriter.WriteLine("Extra messages: " + ExtraText);
                }
                mylogWriter.WriteLine("\n\n");
                mylogWriter.WriteLine("*************END************END****************END**************");
            }
            catch
            {
            }
            finally
            {
                if (mylogWriter != null)
                    mylogWriter.Close();
                if (loggerStream != null)
                    loggerStream.Close();
            }
        }
    }
}