﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.DirectoryServices;

using System.Collections;


namespace Utils
{
    public class LdapHelper 
    {
        /// <summary>
        /// LDAP header form/grid name
        /// </summary>
        public const string LDAPHeaderName = "Active directory login name";
        public const string LDAPFieldName = "activeDirectoryLoginName";

        public  static  DirectoryEntry GetDirectoryEntry(string userName,string pasword)
        {
            string domainpath = Config.ActiveDirectoryConnection;
            if(string.IsNullOrEmpty(domainpath))
                return null;
            DirectoryEntry searchRoot = new DirectoryEntry(domainpath);
            searchRoot.Username = userName == null ? Config.ActiveDirectoryAccessUserName : userName;
            searchRoot.Password = pasword == null ? Config.ActiveDirectoryAccessPassword : pasword;
            return searchRoot;
        }

        public static DirectoryEntry GetDirectoryEntryConnection2(string userName, string pasword)
        {
            string domainpath = Config.ActiveDirectoryConnection2;
            if (string.IsNullOrEmpty(domainpath))
                return null;
            DirectoryEntry searchRoot = new DirectoryEntry(domainpath);
            searchRoot.Username = userName == null ? Config.ActiveDirectoryAccessUserName : userName;
            searchRoot.Password = pasword == null ? Config.ActiveDirectoryAccessPassword : pasword;
            return searchRoot;
        }

        /// <summary>
        /// Checks if active directory is enabled or not
        /// </summary>
        /// <returns></returns>
        private static bool HasLDAPEnabled()
        {
            return !string.IsNullOrEmpty(Config.ActiveDirectoryConnection);
                //&&
                //!string.IsNullOrEmpty(Config.ActiveDirectoryAccessUserName) &&
                //!string.IsNullOrEmpty(Config.ActiveDirectoryAccessPassword);
        }

        public static bool IsLDAPAccessible()
        {
            return
                LdapHelper.HasLDAPEnabled();
        }

        //public static ArrayList GetAllAdDomainUsers()
        //{
        //    try
        //    {
        //        ArrayList allUsers = new ArrayList();

        //        if (LdapHelper.HasLDAPEnabled())
        //        {
        //            DirectoryEntry searchRoot = GetDirectoryEntry(null, null);

        //            if (searchRoot != null)
        //            {
        //                DirectorySearcher search = new DirectorySearcher(searchRoot);
        //                search.Filter = "(&(objectClass=user)(objectCategory=person))";
        //                search.PropertiesToLoad.Add("samaccountname");

        //                SearchResult result;
        //                SearchResultCollection resultCol = search.FindAll();
        //                if (resultCol != null)
        //                {
        //                    for (int counter = 0; counter < resultCol.Count; counter++)
        //                    {
        //                        result = resultCol[counter];
        //                        if (result.Properties.Contains("samaccountname"))
        //                        {
        //                            allUsers.Add((String)result.Properties["samaccountname"][0]);
        //                        }
        //                    }
        //                }
        //                allUsers.Sort(new UserSorting());
        //                allUsers.Insert(0, "");
        //                return allUsers;
        //            }
        //        }
        //    }
        //    catch (Exception exp)
        //    {
        //        Log.log("LDAP access", exp);
        //    }
        //    return new ArrayList();
        //}

        //public static List<string> GetAllAdDomainUserList()
        //{
        //    try
        //    {
        //        List<string> allUsers = new List<string>();
        //        if (LdapHelper.HasLDAPEnabled())
        //        {

        //            DirectoryEntry searchRoot = GetDirectoryEntry(null, null);

        //            if (searchRoot != null)
        //            {
        //                DirectorySearcher search = new DirectorySearcher(searchRoot);
        //                search.Filter = "(&(objectClass=user)(objectCategory=person))";
        //                search.PropertiesToLoad.Add("samaccountname");

        //                SearchResult result;
        //                SearchResultCollection resultCol = search.FindAll();
        //                if (resultCol != null)
        //                {
        //                    for (int counter = 0; counter < resultCol.Count; counter++)
        //                    {
        //                        result = resultCol[counter];
        //                        if (result.Properties.Contains("samaccountname"))
        //                        {
        //                            allUsers.Add((String)result.Properties["samaccountname"][0]);
        //                        }
        //                    }
        //                }

        //                allUsers.Sort();
        //                allUsers.Insert(0, "");
        //                return allUsers;
        //            }
        //        }
        //    }
        //    catch (Exception exp)
        //    {
        //        Log.log("LDAP access", exp);
        //    }
        //    return new List<string>();
        //}


        public static bool IsAuthenticated(string username, string password)
        {
            username = username.Trim();

            try
            {
                using (DirectoryEntry entry = GetDirectoryEntry(username, password))
                {

                    //Bind to the native AdsObject to force authentication.
                    object obj = entry.NativeObject;

                    using (DirectorySearcher search = new DirectorySearcher(entry))
                    {

                        search.Filter = "(SAMAccountName=" + username + ")";
                        search.PropertiesToLoad.Add("cn");

                        using (SearchResultCollection results = search.FindAll())
                        {
                            if (results.Count > 0)
                                return true;

                            return false;
                        }


                    }
                }
            }
            catch (Exception ex)
            {
                Log.log("Active directory 1 login error for user " + username ,ex);
                //return false;
               


                // try second Connection
                try
                {
                    using (DirectoryEntry entry = GetDirectoryEntryConnection2(username, password))
                    {
                        //Bind to the native AdsObject to force authentication.
                        object obj = entry.NativeObject;
                        using (DirectorySearcher search = new DirectorySearcher(entry))
                        {
                            search.Filter = "(SAMAccountName=" + username + ")";
                            search.PropertiesToLoad.Add("cn");
                            using (SearchResultCollection results = search.FindAll())
                            {
                                if (results.Count > 0)
                                    return true;

                                return false;
                            }

                        }
                    }
                }
                catch (Exception ex1)
                {
                    //byte[] byt = System.Text.Encoding.UTF8.GetBytes(password);

                    // convert the byte array to a Base64 string

                    //string strModified = Convert.ToBase64String(byt);

                    Log.log("Active directory 2 login error for user " + username, ex);
                    return false;
                }
                



            }
            //return true;
        }

        

        public  class UserSorting :IComparer
        {

            #region IComparer Members

            public int Compare(object x, object y)
            {
                string xx = x.ToString();
                string yy = y.ToString();
                return xx.CompareTo(yy);
            }

            #endregion
        }
    }
}
