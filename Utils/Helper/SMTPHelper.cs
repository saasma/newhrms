﻿#region Using
using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Net;
using System.Net.Mail;
using System.IO;
using Utils;
using System.Net.Mime;
#endregion

namespace Utils.Helper
{

    /// <summary>
    /// SMTPHelper class is responsible for sending email
    /// </summary>
    public class SMTPHelper
    {
        SmtpClient smtpClient;
        public SMTPHelper()
        {
            smtpClient = new SmtpClient();//Config.GetEmailServerName, int.Parse(Config.GetEmailServerPortName));

         

            if (!string.IsNullOrEmpty(Config.EnableSsl))
            {
                smtpClient.EnableSsl = bool.Parse(Config.EnableSsl.ToString());

                if (smtpClient.EnableSsl)
                {
                    smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;

                    if (Config.RemoveSSLServerValidation != "true")
                    {
                        //Add this line to bypass the certificate validation
                        System.Net.ServicePointManager.ServerCertificateValidationCallback = delegate(object s,
                                System.Security.Cryptography.X509Certificates.X509Certificate certificate,
                                System.Security.Cryptography.X509Certificates.X509Chain chain,
                                System.Net.Security.SslPolicyErrors sslPolicyErrors)
                        {
                            return true;
                        };
                    }
                }
            }

            //String[] struserpwd = Config.GetEmailUserNameAndPassword;
            //if ((!String.IsNullOrEmpty(struserpwd[0])) && (!String.IsNullOrEmpty(struserpwd[1])))
            //{
            //NetworkCredential myNetWorkCredentials = new NetworkCredential(Config.EmailUserName, Config.EmailPassword);
            //smtpClient.Credentials = myNetWorkCredentials;
            ////}
            //smtpClient.EnableSsl = true;
        }

        public static bool SendToRigoNepalDomainOnlyIfLocalAccess(string to,string bcc)
        {
            
            // if accessed from localhost then send mail to only rigonepal.com domain only for using client email accidentally
            if (HttpContext.Current != null)
            {

                if (HttpContext.Current.Request.IsLocal && to.ToLower().Contains("rigonepal.com") == false)
                    return false;

                string[] emails = to.Split(new char[] {',' });

                foreach (string email in emails)
                {
                    if (!string.IsNullOrEmpty(email) && HttpContext.Current.Request.IsLocal && email.ToLower().Contains("rigonepal.com") == false)
                        return false;
                }

                emails = to.Split(new char[] { ';' });

                foreach (string email in emails)
                {
                    if (!string.IsNullOrEmpty(email) && HttpContext.Current.Request.IsLocal && email.ToLower().Contains("rigonepal.com") == false)
                        return false;
                }

                if (!string.IsNullOrEmpty(bcc) && bcc.Equals(",") == false)
                {
                    if (HttpContext.Current.Request.IsLocal && bcc.ToLower().Contains("rigonepal.com") == false)
                        return false;

                    string[] emails1 = bcc.Split(new char[] { ',' });

                    foreach (string email in emails1)
                    {
                        if (!string.IsNullOrEmpty(email) && HttpContext.Current.Request.IsLocal && email.ToLower().Contains("rigonepal.com") == false)
                            return false;
                    }
                }

            }
            return true;
        }



        public static string TestMail(string to, string body, string subject)
        {
            SMTPHelper smtp = new SMTPHelper();

            //if (!string.IsNullOrEmpty(Config.EnableSsl))
            //{
            //    var client = new SmtpClient();
            //    client.EnableSsl = bool.Parse(Config.EnableSsl.ToString());
            //}


            if (SendToRigoNepalDomainOnlyIfLocalAccess(to,"") == false)
                return "Not Allowed to send mail to this email from localhost";

            try
            {

                //MailAddress from = new MailAddress(Config.NoReplyEmail, subject);
                MailAddress from = new MailAddress(Config.GetEmailSenderName, subject);
                using (MailMessage mailMessage = new MailMessage(from, new MailAddress(to)))//, subject, body))
                {

                    //mailMessage.Bcc.Add("sailendra@rigonepal.com,sailendra@rigonepal.com");

                    mailMessage.IsBodyHtml = true;

                    mailMessage.Subject = subject;
                    mailMessage.Body = body;

                    smtp.smtpClient.Send(mailMessage);

                }



                return "";
            }
            catch (Exception exp)
            {
                // here must be ToString to identity email sending error
                return exp.ToString();
                //return false;
                // MarketBrick.Helpers.ErrorLogger.LogError(exp, "Email setup error.");
                //Log.log("Error mail sending to : " + to, exp);
            }
            finally
            {
                //excelAttachment.Close();
            }



            //return "";
        }

        public static bool SendMail(string to, string body, string subject,ref string errorMsg)
        {
            SMTPHelper smtp = new SMTPHelper();
            //if (!string.IsNullOrEmpty(Config.EnableSsl))
            //{
            //    var client = new SmtpClient();
            //    client.EnableSsl = bool.Parse(Config.EnableSsl.ToString());
            //}
            if (SendToRigoNepalDomainOnlyIfLocalAccess(to,"") == false)
            {
                errorMsg = "Not Allowed to send mail to this email from localhost";
                return false;
            }

            try
            {

                //MailAddress from = new MailAddress(Config.NoReplyEmail, subject);
                MailAddress from = new MailAddress(Config.GetEmailSenderName, subject);
                using ( MailMessage mailMessage = new MailMessage(from, new MailAddress(to)))//, subject, body))
                {
                    
                    mailMessage.IsBodyHtml = true;

                    mailMessage.Subject = subject;
                    mailMessage.Body = body;

                    smtp.smtpClient.Send(mailMessage);

                }



                return true;
            }
            catch (Exception exp)
            {
               
                // MarketBrick.Helpers.ErrorLogger.LogError(exp, "Email setup error.");
                Log.log("Error mail sending to : " + to, exp);
                errorMsg = exp.Message;
                return false;
            }
            finally
            {
                //excelAttachment.Close();
            }



            //return true;
        }

        public delegate bool SendMailDelegate(string to, string body, string subject, string bcc,string senderEmail);

        ///// <summary>
        ///// Risky
        ///// </summary>
        ///// <param name="to"></param>
        ///// <param name="body"></param>
        ///// <param name="subject"></param>
        ///// <param name="bcc"></param>
        ///// <returns></returns>
        public static bool SendAsyncMail(string to, string body, string subject, string bcc)
        {
            if (SendToRigoNepalDomainOnlyIfLocalAccess(to,bcc) == false)
                return false;

            SendMailDelegate del = new SendMailDelegate(SendMailAsync);
            // Warning if current execution is not completed and calling again may generate error like 
            //{System.InvalidOperationException: An asynchronous call is already in progress. It must be completed or canceled before you can call this method.
            // so better to use SmtpClient async method instead of creation one
            del.BeginInvoke(to, body, subject, bcc,Config.GetEmailSenderName, null, null);

            //SendMailDelegate(to, body, subject, bcc);

            return true;
        }

        public static bool SendAsyncMail(string to, string body, string subject, string bcc, string senderFromEmail)
        {
            if (SendToRigoNepalDomainOnlyIfLocalAccess(to,bcc) == false)
                return false;

            SendMailDelegate del = new SendMailDelegate(SendMailAsync);
            // Warning if current execution is not completed and calling again may generate error like 
            //{System.InvalidOperationException: An asynchronous call is already in progress. It must be completed or canceled before you can call this method.
            // so better to use SmtpClient async method instead of creation one

            if (string.IsNullOrEmpty(senderFromEmail))
                senderFromEmail = Config.GetEmailSenderName;

            del.BeginInvoke(to, body, subject, bcc, senderFromEmail, null, null);

            //SendMailDelegate(to, body, subject, bcc);

            return true;
        }
         

        //public static bool SendAsyncMail(string from ,string to, string body, string subject, string bcc)
        //{
        //    if (SendToRigoNepalDomainOnlyIfLocalAccess(to) == false)
        //        return false;
        //    SendMailDelegate del = new SendMailDelegate(SendMailAsync);
        //    del.BeginInvoke(to, body, subject, bcc,from, null, null);

        //    //SendMailDelegate(to, body, subject, bcc);

        //    return true;
        //}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="to"></param>
        /// <param name="body"></param>
        /// <param name="subject"></param>
        /// <param name="bcc">Must not used bcc with semi column separator as some smpt only supports comma so always use Comma</param>
        /// <returns></returns>
        public static bool SendMailAsync(string to, string body, string subject, string bcc, string senderEmail)
        {
            if (SendToRigoNepalDomainOnlyIfLocalAccess(to,bcc) == false)
                return false;


            SMTPHelper smtp = new SMTPHelper();
           
            try
            {

                MailAddress from = new MailAddress(senderEmail, subject);

                using (MailMessage mailMessage = new MailMessage()) //, new MailAddress(to)))
                {
                    mailMessage.From = from;

                    string[] ToMuliId = to.Split(',');
                    foreach (string ToEMailId in ToMuliId)
                    {
                        mailMessage.To.Add(new MailAddress(ToEMailId)); //adding multiple TO Email Id
                    }

                    // skip 0 also as sometime in case of citizen like 0 coming
                    if (!string.IsNullOrEmpty(bcc) && !bcc.Equals("0"))
                        mailMessage.Bcc.Add(bcc);
                    mailMessage.IsBodyHtml = true;

                    mailMessage.Subject = subject;
                    mailMessage.Body = body;

                    smtp.smtpClient.Send(mailMessage);

                }



                return true;
            }
            catch (Exception exp)
            {
               
                // MarketBrick.Helpers.ErrorLogger.LogError(exp, "Email setup error.");
                Log.log("Error mail sending to, cc=" + to + " ,bcc=" + (bcc== null ? "" : bcc) + ", sender email : " + senderEmail
                    , exp);
                return false;
            }
            finally
            {
                //excelAttachment.Close();
            }



            //return true;
        }

        public static bool SendMailAsyncWebServiceOnly(string to, string body, string subject, string bcc, string senderEmail)
        {

            //send mail also from local domain. so code is commented. 

            //if (SendToRigoNepalDomainOnlyIfLocalAccess(to,bcc) == false)
            //    return false;

            SMTPHelper smtp = new SMTPHelper();

            try
            {

                MailAddress from = new MailAddress(senderEmail, subject);

                using (MailMessage mailMessage = new MailMessage(from, new MailAddress(to)))//, subject, body))
                {
                    // skip 0 also as sometime in case of citizen like 0 coming
                    if (!string.IsNullOrEmpty(bcc) && !bcc.Equals("0"))
                        mailMessage.Bcc.Add(bcc);
                    mailMessage.IsBodyHtml = true;

                    mailMessage.Subject = subject;
                    mailMessage.Body = body;
                    smtp.smtpClient.Send(mailMessage);

                }
                return true;
            }
            catch (Exception exp)
            {

                // MarketBrick.Helpers.ErrorLogger.LogError(exp, "Email setup error.");
                Log.log("Error mail sending to, cc=" + to + " ,bcc=" + (bcc == null ? "" : bcc), exp);
                return false;
            }
            finally
            {
                //excelAttachment.Close();
            }



            //return true;
        }




        public static bool SendMailWebServiceOnly(string to, string body, string subject, string bcc, string senderEmail)
        {
          


            SMTPHelper smtp = new SMTPHelper();

            try
            {

                if (string.IsNullOrEmpty(senderEmail))
                    senderEmail = Config.GetEmailSenderName;

                MailAddress from = new MailAddress(senderEmail, subject);

                using (MailMessage mailMessage = new MailMessage(from, new MailAddress(to)))//, subject, body))
                {
                    // skip 0 also as sometime in case of citizen like 0 coming
                    if (!string.IsNullOrEmpty(bcc) && !bcc.Equals("0"))
                        mailMessage.Bcc.Add(bcc);
                    mailMessage.IsBodyHtml = true;

                    mailMessage.Subject = subject;
                    mailMessage.Body = body;

                    smtp.smtpClient.Send(mailMessage);

                }



                return true;
            }
            catch (Exception exp)
            {

                // MarketBrick.Helpers.ErrorLogger.LogError(exp, "Email setup error.");
                Log.log("Error mail sending to, cc=" + to + " ,bcc=" + (bcc == null ? "" : bcc), exp);
                return false;
            }
            finally
            {
                //excelAttachment.Close();
            }



            //return true;
        }

        public static bool SendMail(string to, string body, string subject, string bcc, string senderEmail)
        {
            if (SendToRigoNepalDomainOnlyIfLocalAccess(to, bcc) == false)
                return false;


            SMTPHelper smtp = new SMTPHelper();

            try
            {

                if (string.IsNullOrEmpty(senderEmail))
                    senderEmail = Config.GetEmailSenderName;

                MailAddress from = new MailAddress(senderEmail, subject);

                using (MailMessage mailMessage = new MailMessage(from, new MailAddress(to)))//, subject, body))
                {
                    // skip 0 also as sometime in case of citizen like 0 coming
                    if (!string.IsNullOrEmpty(bcc) && !bcc.Equals("0"))
                        mailMessage.Bcc.Add(bcc);
                    mailMessage.IsBodyHtml = true;

                    mailMessage.Subject = subject;
                    mailMessage.Body = body;

                    smtp.smtpClient.Send(mailMessage);

                }



                return true;
            }
            catch (Exception exp)
            {

                // MarketBrick.Helpers.ErrorLogger.LogError(exp, "Email setup error.");
                Log.log("Error mail sending to, cc=" + to + " ,bcc=" + (bcc == null ? "" : bcc), exp);
                return false;
            }
            finally
            {
                //excelAttachment.Close();
            }



            //return true;
        }

        public static bool SendMail(string to, string body, string subject, string bcc, string senderEmail,ref string errorMsg)
        {
            if (SendToRigoNepalDomainOnlyIfLocalAccess(to,bcc) == false)
                return false;


            SMTPHelper smtp = new SMTPHelper();

            try
            {

                if (string.IsNullOrEmpty(senderEmail))
                    senderEmail = Config.GetEmailSenderName;

                MailAddress from = new MailAddress(senderEmail, subject);

                using (MailMessage mailMessage = new MailMessage(from, new MailAddress(to)))//, subject, body))
                {
                    // skip 0 also as sometime in case of citizen like 0 coming
                    if (!string.IsNullOrEmpty(bcc) && !bcc.Equals("0"))
                        mailMessage.Bcc.Add(bcc);
                    mailMessage.IsBodyHtml = true;

                    mailMessage.Subject = subject;
                    mailMessage.Body = body;

                    smtp.smtpClient.Send(mailMessage);

                }



                return true;
            }
            catch (Exception exp)
            {
                errorMsg = exp.Message;
                // MarketBrick.Helpers.ErrorLogger.LogError(exp, "Email setup error.");
                Log.log("Error mail sending to, cc=" + to + " ,bcc=" + (bcc == null ? "" : bcc), exp);
                return false;
            }
            finally
            {
                //excelAttachment.Close();
            }



            //return true;
        }
        public static bool SendMail(string to, string body, string subject, string bcc,string senderName,string senderEmail)
        {
            if (SendToRigoNepalDomainOnlyIfLocalAccess(to,bcc) == false)
                return false;


            SMTPHelper smtp = new SMTPHelper();
            //if (!string.IsNullOrEmpty(Config.EnableSsl))
            //{
            //    var client = new SmtpClient();
            //    client.EnableSsl = bool.Parse(Config.EnableSsl.ToString());
            //}

            try
            {

                // MailAddress from = new MailAddress(Config.NoReplyEmail, subject);
                MailAddress from = new MailAddress(senderEmail, senderName);
                //(Config.GetEmailSenderName, subject);

                using (MailMessage mailMessage = new MailMessage(from, new MailAddress(to)))//, subject, body))
                {
                    // skip 0 also as sometime in case of citizen like 0 coming
                    if (!string.IsNullOrEmpty(bcc) && !bcc.Equals("0"))
                        mailMessage.Bcc.Add(bcc);
                    mailMessage.IsBodyHtml = true;

                    mailMessage.Subject = subject;
                    mailMessage.Body = body;

                    smtp.smtpClient.Send(mailMessage);

                }



                return true;
            }
            catch (Exception exp)
            {

                // MarketBrick.Helpers.ErrorLogger.LogError(exp, "Email setup error.");
                Log.log("Error mail sending to, cc=" + to + " ,bcc=" + (bcc == null ? "" : bcc), exp);
                return false;
            }
            finally
            {
                //excelAttachment.Close();
            }



            //return true;
        }

        public bool SendSalary(string to, string body, string subject, Stream pdfStream,ref string errorMsg)
        {

            if (SendToRigoNepalDomainOnlyIfLocalAccess(to,"") == false)
                return false;


            try
            {
                body += "<p> <img src=cid:Pic1 /> </p>";

                //MailAddress from = new MailAddress(Config.NoReplyEmail, subject);
                MailAddress from = new MailAddress(Config.GetEmailSenderName, subject);

                using (pdfStream)
                {

                    using (MailMessage mailMessage = new MailMessage(from, new MailAddress(to)))//, subject, body))
                    {

                        mailMessage.IsBodyHtml = true;

                        mailMessage.Subject = subject;
                        //mailMessage.Body = body;


                        //pdfStream.Seek(
                        //Attach as inline image
                        // Create a LinkedResource object for each embedded image
                        LinkedResource pic1 = new LinkedResource(pdfStream, "image/jpeg");
                        //pic1.TransferEncoding = System.Net.Mime.TransferEncoding.Base64;
                        pic1.ContentId = "Pic1";
                        AlternateView avHtml = AlternateView.CreateAlternateViewFromString
                           (body, null, "text/html");
                        avHtml.LinkedResources.Add(pic1);

                        mailMessage.AlternateViews.Add(avHtml);

                        //Attachment attachment = new Attachment(pdfStream, subject ,MediaTypeNames.Image.Jpeg);
                        //pdfStream.Seek(0, SeekOrigin.Begin);
                        //mailMessage.Attachments.Add(attachment);


                        smtpClient.Send(mailMessage);

                    }
                }



                return true;
            }
            catch (Exception exp)
            {
                errorMsg = exp.ToString();
                Log.log("Error mail sending to : " + to, exp);
                return false;
                // MarketBrick.Helpers.ErrorLogger.LogError(exp, "Email setup error.");

            }
            finally
            {
                try
                {
                    // File.Delete(fileLoc);
                    pdfStream.Close();
                }
                catch { }
                //pdfStream.Close();
            }


            //return true;
        }

        public delegate bool SendSalaryMailDelegate(string senderEmail,string to, string body, string subject, Stream pdfStream, ref string errorMsg);
        

        public static string SendForgotPasswordLinkInMail(string emailId, string body)
        {
            SMTPHelper smtp = new SMTPHelper();
            //if (!string.IsNullOrEmpty(Config.EnableSsl))
            //{
            //    var client = new SmtpClient();
            //    client.EnableSsl = bool.Parse(Config.EnableSsl.ToString());
            //}

            string msg = "";
            try
            {
                string subject = "HR Password Reset";

                using (MailMessage mailMessage = new MailMessage(new MailAddress(emailId), new MailAddress(emailId)))
                {
                    mailMessage.IsBodyHtml = true;

                    mailMessage.Subject = subject;
                    mailMessage.Body = body;

                    smtp.smtpClient.Send(mailMessage);
                }
            }
            catch (Exception exp)
            {
                Log.log("Error mail sending to : " + emailId, exp);
                msg = exp.Message;
            }
            finally
            {
                //excelAttachment.Close();
            }



            return msg ;
        }

        public static bool SendMailWithAttachment(string to, string body, string subject, string bcc, string senderName, string senderEmail, string userFileName, string serverFileName)
        {
            if (SendToRigoNepalDomainOnlyIfLocalAccess(to,bcc) == false)
                return false;


            SMTPHelper smtp = new SMTPHelper();

            try
            {

                // MailAddress from = new MailAddress(Config.NoReplyEmail, subject);
                MailAddress from = new MailAddress(senderEmail, senderName);
                //(Config.GetEmailSenderName, subject);

                using (MailMessage mailMessage = new MailMessage(from, new MailAddress(to)))//, subject, body))
                {
                    // skip 0 also as sometime in case of citizen like 0 coming
                    if (!string.IsNullOrEmpty(bcc) && !bcc.Equals("0"))
                        mailMessage.Bcc.Add(bcc);
                    mailMessage.IsBodyHtml = true;

                    mailMessage.Subject = subject;
                    mailMessage.Body = body;

                    //System.Net.Mail.Attachment attachment;
                    
                    //attachment = new System.Net.Mail.Attachment(attachmentName);
                    //attachment.ContentDisposition.FileName = fileName;
                    //mailMessage.Attachments.Add(attachment);


                    FileStream fs = new FileStream(serverFileName, FileMode.Open, FileAccess.Read);
                    StreamReader s = new StreamReader(fs);
                    int errors = 0;
                    while (s.ReadLine() != null)
                    {
                        errors++;
                    }
                    
                    s.Close();
                    fs = new FileStream(serverFileName, FileMode.Open, FileAccess.Read);
                    ContentType ct = new ContentType();
                    ct.MediaType = MediaTypeNames.Text.Plain;
                    ct.Name = userFileName;
                    Attachment data = new Attachment(fs, ct);
                    mailMessage.Attachments.Add(data);

                    smtp.smtpClient.Send(mailMessage);

                }



                return true;
            }
            catch (Exception exp)
            {

                // MarketBrick.Helpers.ErrorLogger.LogError(exp, "Email setup error.");
                Log.log("Error mail sending to, cc=" + to + " ,bcc=" + (bcc == null ? "" : bcc), exp);
                return false;
            }
            finally
            {
                //excelAttachment.Close();
            }



            //return true;
        }

    }
}
