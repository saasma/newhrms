﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Xml;
using System.Web;
using Utils.Security;
using System.Web.Configuration;
using System.Net.Configuration;

namespace Utils
{
    public static class Config
    {

        public static bool PreventPasswordChange
        {
            get
            {
                string value = ConfigurationManager.AppSettings["PreventPasswordChange"];
                if (value == null)
                    return false;


                bool prevent;
                if (bool.TryParse(value, out prevent))
                    return prevent;

                return false;
            }
        }

        // replaced with popup dropdown so no need
        //public static bool? ImportDateIsEnglish
        //{
        //    get
        //    {
        //        string value =ConfigurationManager.AppSettings["ImportDateIsEnglish"] ;
        //        if (value== null)
        //            return null;

        //        bool isEng;
        //        if (bool.TryParse(value, out isEng))
        //            return isEng;

        //        return null;

        //    }
        //}

        public static string ImportDateFormat
        {
            get
            {
                string format = ConfigurationManager.AppSettings["ImportDateFormat"];

                if (format == null)
                    return "yyyy/mm/dd";
                return format;
            }
        }
        //public static bool DateFormat_YYYY_MM_DD
        //{
        //    get
        //    {
        //        string format = ConfigurationManager.AppSettings["DateFormat_YYYY_MM_DD"];

        //        if (string.IsNullOrEmpty(format))
        //            return false;
        //        return Convert.ToBoolean(format);
        //    }
        //}
        public static string EmailAddressRegEx
        {
            get
            {
                return @"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*";
            }
        }

        

        #region "Active Directory"
        public static string ActiveDirectoryConnection
        {
            get
            {
                if( ConfigurationManager.AppSettings["ActiveDirectoryConnection"] != null)
                    return ConfigurationManager.AppSettings["ActiveDirectoryConnection"].ToString();
                return "";
            }
        }

        public static string ActiveDirectoryConnection2
        {
            get
            {
                if (ConfigurationManager.AppSettings["ActiveDirectoryConnection2"] != null)
                    return ConfigurationManager.AppSettings["ActiveDirectoryConnection2"].ToString();
                return "";
            }
        }

        public static string ActiveDirectoryAccessUserName
        {
            get
            {
                if (ConfigurationManager.AppSettings["ActiveDirectoryAccessUserName"] != null)
                    return ConfigurationManager.AppSettings["ActiveDirectoryAccessUserName"].ToString();
                return "";
            }
        }


        public static string EnableSsl
        {
            get
            {
                if (ConfigurationManager.AppSettings["SSLSMTP"] != null)
                    return ConfigurationManager.AppSettings["SSLSMTP"].ToString();
                return "";
            }
        }
        public static string RemoveSSLServerValidation
        {
            get
            {
                if (ConfigurationManager.AppSettings["RemoveSSLServerValidation"] != null)
                    return ConfigurationManager.AppSettings["RemoveSSLServerValidation"].ToString();
                return "";
            }
        }
        public static bool EnableWeeklyAbsentScheduler
        {
            get
            {
                if (ConfigurationManager.AppSettings["EnableWeeklyAbsentScheduler"] != null)
                    return bool.Parse(ConfigurationManager.AppSettings["EnableWeeklyAbsentScheduler"].ToString());
                return false;
            }
        }

        public static bool EnableDailyLateEntryWithAbsent
        {
            get
            {
                if (ConfigurationManager.AppSettings["EnableDailyLateEntryWithAbsent"] != null)
                    return bool.Parse(ConfigurationManager.AppSettings["EnableDailyLateEntryWithAbsent"].ToString());
                return false;
            }
        }


        public static string ActiveDirectoryAccessPassword
        {
            get
            {
                if (ConfigurationManager.AppSettings["ActiveDirectoryAccessPassword"] != null)
                {
                    string pwd =  ConfigurationManager.AppSettings["ActiveDirectoryAccessPassword"].ToString();
                    if (!string.IsNullOrEmpty(pwd))
                    {
                        EncryptorDecryptor enc = new EncryptorDecryptor();
                        return enc.Decrypt(pwd);
                    }
                    return "";
                }
                return "";
            }
        }
        public static string ActiveDirectorySSOIP
        {
            get
            {
                if (ConfigurationManager.AppSettings["ActiveDirectorySSOIP"] != null)
                    return ConfigurationManager.AppSettings["ActiveDirectorySSOIP"].ToString();
                return "";
            }
        }


        #endregion

        public static bool AllowPayrollPeriodCreationBeforeFinalSave
        {
            get
            {
                string AllowPayrollPeriodCreationBeforeFinalSave = ConfigurationManager.AppSettings["AllowPayrollPeriodCreationBeforeFinalSave"];
                if (!string.IsNullOrEmpty(AllowPayrollPeriodCreationBeforeFinalSave))
                    return bool.Parse(AllowPayrollPeriodCreationBeforeFinalSave);
                return false;
            }
        }

        public static string NoReplyEmail
        {
            get
            {
                return ConfigurationManager.AppSettings["NoReplyEmail"].ToString();
            }
        }
        public static string SalaryMailSubject
        {
            get
            {
                return ConfigurationManager.AppSettings["SalaryMailSubject"].ToString();
            }
        }

        public static string PasswordMailSubject
        {
            get
            {
                return ConfigurationManager.AppSettings["PasswordMailSubject"].ToString();
            }
        }

        
        /// <summary>
        /// Email Server is set in web.config file.
        /// This method gets the sender name as set in web.config.
        /// </summary>
        public static string SenderEmail = null;
        public static System.String GetEmailSenderName
        {

            get
            {
                if (SenderEmail != null)
                    return SenderEmail;

                //string str = string.Empty;
                try
                {
                    var config = WebConfigurationManager.OpenWebConfiguration(HttpContext.Current.Request.ApplicationPath);
                    var settings = config.GetSectionGroup("system.net/mailSettings") as MailSettingsSectionGroup;
                    SenderEmail = settings.Smtp.From;

                    //ConfigXmlDocument myDocument = new ConfigXmlDocument();
                    //myDocument.Load(GetWebConfigPhysicalLocationPath + @"/web.config");
                    //XmlNodeList nodeList = myDocument.GetElementsByTagName("smtp");
                    //str = nodeList[0].Attributes["from"].FirstChild.Value;
                }
                catch (Exception exp)
                {

                }
                return SenderEmail;
            }
        }

        public static System.String EmailUserName
        {

            get
            {
                string str = string.Empty;
                try
                {
                    var config = WebConfigurationManager.OpenWebConfiguration(HttpContext.Current.Request.ApplicationPath);
                    var settings = config.GetSectionGroup("system.net/mailSettings") as MailSettingsSectionGroup;
                    str = settings.Smtp.Network.UserName;

                    //ConfigXmlDocument myDocument = new ConfigXmlDocument();
                    //myDocument.Load(GetWebConfigPhysicalLocationPath + @"/web.config");
                    //XmlNodeList nodeList = myDocument.GetElementsByTagName("smtp");
                    //str = nodeList[0].Attributes["userName"].FirstChild.Value;
                }
                catch (Exception exp)
                {

                }
                return str;
            }
        }

        public static System.String EmailPassword
        {

            get
            {
                string str = string.Empty;
                try
                {
                    var config = WebConfigurationManager.OpenWebConfiguration(HttpContext.Current.Request.ApplicationPath);
                    var settings = config.GetSectionGroup("system.net/mailSettings") as MailSettingsSectionGroup;
                    str = settings.Smtp.Network.Password;

                    //ConfigXmlDocument myDocument = new ConfigXmlDocument();
                    //myDocument.Load(GetWebConfigPhysicalLocationPath + @"/web.config");
                    //XmlNodeList nodeList = myDocument.GetElementsByTagName("smtp");
                    //str = nodeList[0].Attributes["password"].FirstChild.Value;
                }
                catch (Exception exp)
                {

                }
                return str;
            }
        }
        /// <summary>
        /// Email server is set in web.config file.
        /// This method reads the server name from web.config.
        /// </summary>
        public static System.String GetEmailServerName
        {
            get
            {
                string str = string.Empty;
                try
                {
                    var config = WebConfigurationManager.OpenWebConfiguration(HttpContext.Current.Request.ApplicationPath);
                    var settings = config.GetSectionGroup("system.net/mailSettings") as MailSettingsSectionGroup;
                    str = settings.Smtp.Network.Host;

                    //ConfigXmlDocument myDocument = new ConfigXmlDocument();
                    //myDocument.Load(GetWebConfigPhysicalLocationPath + @"/web.config");
                    //XmlNodeList nodeList = myDocument.GetElementsByTagName("network");
                    //str = nodeList[0].Attributes["host"].FirstChild.Value;
                }
                catch (Exception exp)
                {
                }
                return str;
            }
        }

        public static string GetWebConfigPhysicalLocationPath
        {
            get
            {
                string str = string.Empty;
                try
                {
                    if (HttpContext.Current == null)
                        return HttpRuntime.AppDomainAppPath;
                    else
                    {
                        HttpContext currContext = HttpContext.Current;
                        str = currContext.Server.MapPath(currContext.Request.ApplicationPath);
                    }
                }
                catch (Exception exp)
                {

                }
                return str;
            }


        }


        /// <summary>
        /// Email server is set in web.config file. 
        /// It retrieves the port used.
        /// </summary>
        public static System.String GetEmailServerPortName
        {
            get
            {
                string str = string.Empty;
                try
                {
                    var config = WebConfigurationManager.OpenWebConfiguration(HttpContext.Current.Request.ApplicationPath);
                    var settings = config.GetSectionGroup("system.net/mailSettings") as MailSettingsSectionGroup;
                    str = settings.Smtp.Network.Port.ToString();

                    //ConfigXmlDocument myDocument = new ConfigXmlDocument();
                    //myDocument.Load(GetWebConfigPhysicalLocationPath + @"/web.config");
                    //XmlNodeList nodeList = myDocument.GetElementsByTagName("network");
                    //str = nodeList[0].Attributes["port"].FirstChild.Value;
                }
                catch (Exception exp)
                {   

                }
                return str;
            }
        }

        public static string DefaultBackupLocation
        {
            get
            {
                return ConfigurationManager.AppSettings["DefaultBackupLocation"].ToString();
            }
        }


        public static string HPLOtherLeaveColor
        {
            get
            {
                return ConfigurationManager.AppSettings["HPLOtherLeaveColor"].ToString();
            }
        }

        public static string HPLOtherLeaveAbbr
        {
            get
            {
                return ConfigurationManager.AppSettings["HPLOtherLeaveAbbr"].ToString();
            }
        }

        public static string LogFileLoc
        {
            get
            {
                return ConfigurationManager.AppSettings["LogFileLoc"].ToString();
            }
        }

        public static string LogFileSchedularLoc
        {
            get
            {
                return ConfigurationManager.AppSettings["LogFileSchedularLoc"].ToString();
            }
        }
        public static int ImageWidth
        {
            get
            {
                return int.Parse(ConfigurationManager.AppSettings["ImageWidth"].ToString());
            }
        }

        public static int MaxInvalidPasswordAttempts
        {
            get
            {
                if (ConfigurationManager.AppSettings["MaxInvalidPasswordAttempts"] == null)
                    return 5;//default 5 attempts if not specified in web.config

                try
                {
                    return int.Parse(ConfigurationManager.AppSettings["MaxInvalidPasswordAttempts"].ToString());
                }
                catch
                {
                    return 0;
                }
            }
        }

        public static int MinsToLockUserNameForInvalidAttempts
        {
            get
            {
                if (ConfigurationManager.AppSettings["MinsToLockUserNameForInvalidAttempts"] == null)
                    return 5;//default 5 mins if not specified in web.config

                try
                {
                    return int.Parse(ConfigurationManager.AppSettings["MinsToLockUserNameForInvalidAttempts"].ToString());
                }
                catch
                {
                    return 0;
                }
            }
        }

        public static int DumpTaxEmployeeId
        {
            get
            {
                try
                {
                    return int.Parse(ConfigurationManager.AppSettings["DumpTaxEmployeeId"].ToString());
                }
                catch
                {
                    return 0;
                }
            }
        }

        public static bool DumpTaxCalculation
        {
            get
            {
                try
                {
                    return bool.Parse(ConfigurationManager.AppSettings["DumpTaxCalculation"].ToString());
                }
                catch
                {
                    return false;
                }
            }
        }

        public static int ImageHeight
        {
            get
            {
                return int.Parse(ConfigurationManager.AppSettings["ImageHeight"].ToString());
            }
        }

        public static string ExceptionEmail
        {
            get
            {
                return ConfigurationManager.AppSettings["ExceptionEmail"].ToString();
            }
        }
        public static string UploadLocation
        {
            get
            {
                return ConfigurationManager.AppSettings["UploadLocation"].ToString();
            }
        }

        public static string NoticeLocation
        {
            get
            {
                return UploadLocation + "/Notice";
            }
        }
        public static string ConnectionString
        {
            get
            {
                //EncryptorDecryptor enc = new EncryptorDecryptor();
                //return enc.Decrypt(ConfigurationManager.ConnectionStrings["PayrollMgmtConnectionString"].ConnectionString);
                return ConfigurationManager.ConnectionStrings["PayrollMgmtConnectionString"].ConnectionString;
            }
        }

        public static string ErrorLogPath
        {
            get
            {
                return ConfigurationManager.AppSettings["ErrorLogPath"].ToString();
            }
        }

        public static string PhoneRegExp
        {
            get
            {
                return ConfigurationManager.AppSettings["PhoneRegExp"].ToString();
            }
        }

        public static string FaxRegExp
        {
            get
            {
                return ConfigurationManager.AppSettings["FaxRegExp"].ToString();
            }
        }
        public static string CellRegExp
        {
            get
            {
                return ConfigurationManager.AppSettings["CellRegExp"].ToString();
            }
        }
        public static string PanNoRegExp
        {
            get
            {
                return ConfigurationManager.AppSettings["PanNoRegExp"].ToString();
            }
        }

        public static string UPLColor
        {
            get
            {
                object value = ConfigurationManager.AppSettings["UPL"];
                if (value == null || value.ToString() == "")
                    return "green";
                return value.ToString();
            }
        }

        public static string atteColor(string s)
        {
            
                object value = ConfigurationManager.AppSettings[s];
                if (value == null || value.ToString() == "")
                    return "green";
                return value.ToString();
            
        }



        /// <summary>
        /// If null then not valid to run
        /// </summary>
        public static string[] Values
        {
            get
            {
                try
                {
                    EncryptorDecryptor enc = new EncryptorDecryptor();
                    if (ConfigurationManager.AppSettings["Value"] == null)
                        return null;
                    string encrypted = ConfigurationManager.AppSettings["Value"].ToString().Trim();
                    if (string.IsNullOrEmpty(encrypted))
                        return null;

                    string value = enc.Decrypt(encrypted);
                    string[] strs = value.Split(new char[] { ':' });
                    if (strs.Length <3)
                        return null;

                    bool has;

                    if (bool.TryParse(strs[0], out has) == false)
                        return null;

                    return strs;
                }
                catch
                {
                    return null;
                }
            }
        }

        public static string AutomaticBackupLocation
        {
            get
            {
                if (ConfigurationManager.AppSettings["AutomatictBackupLocation"] == null)
                    return "";
                return ConfigurationManager.AppSettings["AutomatictBackupLocation"].ToString();
            }
        }
    }
}
