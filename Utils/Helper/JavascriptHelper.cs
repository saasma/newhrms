﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Utils.Helper
{
    public static class JavascriptHelper
    {
        public  static  void CheckboxUncheckedConfirm(CheckBox chk,string message)
        {
            string code = string.Format("checkboxUncheckedConfirm(this,'{0}');", message);
            chk.Attributes.Add("onclick", code);
        }

        public static void CheckboxCheckedConfirm(CheckBox chk, string message)
        {
            string code = string.Format("checkboxCheckedConfirm(this,'{0}');", message);
            chk.Attributes.Add("onclick", code);
        }

        public static void DisplayClientMsg(string msg, Page page)
        {
            string code = "window.onload = function(){ alert('" + msg + "'); }\n\n";
            string key = "MsgKey321";

            if (!page.ClientScript.IsClientScriptBlockRegistered(key))
                page.ClientScript.RegisterStartupScript(page.GetType(), key, code, true);
        }

        /// <summary>
        /// Displays client side message on window.onload with timeout in case of if page has
        /// TabContainer
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="page"></param>
        public static void DisplayClientMsgWithTimeout(string msg, Page page)
        {
            StringBuilder javaScript = new StringBuilder();
            javaScript.Append("function displayMessage123()\n");
            javaScript.Append("{\n");
            javaScript.Append(" alert('" + msg + "');\n");
            javaScript.Append("}\n");
            javaScript.Append("\n");
            // Set the interval up or down to insure that the tab container is completely loaded
            javaScript.Append("window.onload = function() {  window.setTimeout('displayMessage123()', 100); }\n\n");

            //string code = "window.onload = function(){  alert('" + msg + "'); }";
            string key = "MsgKey321";

            if (!page.ClientScript.IsClientScriptBlockRegistered(key))
                page.ClientScript.RegisterStartupScript(page.GetType(), key, javaScript.ToString(), true);
        }

        public static void DisplayClientMsg(string msg, Page page,string codeAfterMsg)
        {
            string code = "window.onload = function(){ alert('" + msg + "'); " + codeAfterMsg + " };\n\n";
            string key = "MsgKey321";

            if (!page.ClientScript.IsClientScriptBlockRegistered(key))
                page.ClientScript.RegisterStartupScript(page.GetType(), key, code, true);
        }

        //public static void DisplayClientAjaxMsg(string msg, Page page)
        //{
        //    string code = " alert('" + msg + "'); ";
        //    string key = "MsgKey321";

        //    ScriptManager.RegisterClientScriptBlock(page, page.GetType(), key, code, true);

        //    //if (!page.ClientScript.IsClientScriptBlockRegistered(key))
        //      //  page.ClientScript.RegisterStartupScript(page.GetType(), key, code, true);
        //}


        //public static void DisplayWhenTabContainerExists(string msg, Page page)
        //{
        //    string code = "function displayMessage(){ alert('" + msg + "'); }\n\n";            
        //    string key = "MsgKey321";


            
        //    ScriptManager.RegisterStartupScript(page, page.GetType(), key, code, true);
            
        //}

        public static void AttachPopUpCode(Page page,string popFuncName,string url,  int width, int height)
        {
            //appending to prevent from caching
            url = url + "?rand=";
            //https://developer.mozilla.org/En/DOM/window.open
            string code= @"                 
                 if( typeof(queryStr) == 'undefined')
                    queryStr = '';
                 //calculate center left/top   
                 var left = parseInt((screen.availWidth / 2) - ({2} / 2));
                 var top = parseInt((screen.availHeight / 2) - ({3} / 2));   
                 var myRand = Math.random(50000);    
                 var myWindow;             
                
                    myWindow =  window.open('{0}' + myRand  + '&' + queryStr, '{4}','height={3},scrollbars=1,width={2},toolbar=no,directories=no,status=no,menubar=no,center=yes,left=' + left + ',top=' + top);    
                               
try
{{
    myWindow.focus();    
}}
catch(err)
{{
}}                       
            ";
            code = string.Format(code,url,"",width+25,height,page.Title.Replace(" ",""));

            code = "function " + popFuncName + "(queryStr){" + code + "}\n\n";            
            page.ClientScript.RegisterStartupScript(page.GetType(), popFuncName, code, true);
           
        }
        public static void AttachNonDialogPopUpCode(Page page, string popFuncName, string url, int width, int height)
        {
            //appending to prevent from caching
            url = url + "?rand=";
            //https://developer.mozilla.org/En/DOM/window.open
            string code = @"
                 
                 if( typeof(queryStr) == 'undefined')
                    queryStr = '';

                 //calculate center left/top   
                 var left = parseInt((screen.availWidth / 2) - ({2} / 2));
                 var top = parseInt((screen.availHeight / 2) - ({3} / 2));   

                 var myRand = Math.random(50000);    
                 var myWindow;                    
                
                 myWindow =  window.open('{0}' + myRand  + '&' + queryStr, '{4}','height={3},width={2},toolbar=no,directories=no,status=no,menubar=no,scrollbars=no,resizable=no,modal=no,center=yes,left=' + left + ',top=' + top);    
                    
                 try
{{
    myWindow.focus();    
}}
catch(err)
{{
}}                          
            "
                ;
            code = string.Format(code, url, "", width+25, height,page.Title);


            code = "function " + popFuncName + "(queryStr){" + code + "}\n\n";
            //ctl.Attributes.Add("onclick", popFuncName + "();return false;");
            page.ClientScript.RegisterStartupScript(page.GetType(), popFuncName, code, true);

        }

        /// <summary>
        /// Attached javascript code on click like for CheckBox to disable other controls
        /// </summary>
        /// <param name="source"></param>
        /// <param name="target"></param>
        [Obsolete]
        public static void AttachCodeToDisableOther(WebControl source, WebControl target)
        {
            source.Attributes.Add("onclick",
                string.Format("toggleReverseElement(this,'{0}');", target.ClientID));
        }
        /// <summary>
        /// Attach code to enable/disable other,also disables the target element
        /// </summary>
        /// <param name="source"></param>
        /// <param name="target"></param>
        [Obsolete]
        public static void AttachCodeToEnableOther(WebControl source, WebControl target)
        {
            source.Attributes.Add("onclick",
                string.Format("toggleElement(this,'{0}');", target.ClientID));
            target.Attributes.Add("disabled", "disabled");
        }


        /// <summary>
        /// Attach other element enabling/disabling code
        /// </summary>
        /// <param name="checkBox"></param>
        /// <param name="targetElementId"></param>
        /// <param name="isReverse"></param>
        public static void AttachEnableDisablingJSCode(CheckBox checkBox, string targetElementId, bool isReverse)
        {
            string boolValue = (isReverse == true) ? "true" : "false";
            string code = string.Format("toggleElementUsingCheckBoxState(this,'{0}',{1});", targetElementId, boolValue);
            checkBox.Attributes.Add("onclick", code);
        }
        public static void AttachShowHideJSCode(CheckBox checkBox, string targetElementId)
        {
            string code = string.Format("if(document.getElementById('{0}').style.display=='') document.getElementById('{0}').style.display='none'; else document.getElementById('{0}').style.display='';", targetElementId);
            checkBox.Attributes.Add("onclick", code);
        }
        public static void AttachEnableDisablingJSCode(CheckBox checkBox, string targetElementId, bool isReverse,string callBackFuncName)
        {
            string boolValue = (isReverse == true) ? "true" : "false";
            string code = string.Format("toggleElementUsingCheckBoxState(this,'{0}',{1},{2});", targetElementId, boolValue,callBackFuncName);
            checkBox.Attributes.Add("onclick", code);
        }
    }   
}
