﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Utils.Helper
{
    public class Finance
    {

        public double Cumipmt(double rate, int nper, double pv, int firstPeriod, int lastPeriod)
        {
            if (nper <= 0 || pv <= 0.0 || firstPeriod < 1 || lastPeriod < 1 || firstPeriod > lastPeriod)
            {
                return double.NaN;
            }
            double num = 0.0;
            for (int i = firstPeriod; i <= lastPeriod; i++)
            {
                num += Ipmt(rate, i, nper, pv, 0.0);
            }
            return num;

        }

        private static double Ipmt(double rate, int period, int nper, double pv, double fv)
        {
            //Finance.a(time);
            if (period < 1 || nper < period)
            {
                return double.NaN;
            }
            if (rate == 0.0)
            {
                return 0.0;
            }
            double num = PeriodicPayment(rate, nper, pv, fv);
            double num2 = 1.0 + rate;
            double num3 = Math.Pow(num2, (double)(nper - period + 1));
            double num4 = num * (num3 - 1.0) / rate;
            double num5;
            //switch (time)
            {
                //case Finance.Period.AtEnd:
                num5 = (fv + num4) / num3;
                //break;
                //case Finance.Period.AtBeginning:
                //    if (period == 1)
                //    {
                //        return 0.0;
                //    }
                //    num5 = (fv / num2 + num4) / num3;
                //    break;
                //default:
                //    throw new ArgumentException("period=" + period);
            }
            return num5 * rate;
        }

        private static double PeriodicPayment(double rate, int nper, double pv, double fv)
        {
            //Finance.a(period);
            double num;
            if (rate == 0.0)
            {
                num = (pv + fv) / (double)nper;
            }
            else
            {
                double num2 = 1.0 + rate;
                double num3 = Math.Pow(num2, (double)nper);
                num = (pv * num3 + fv) * rate / (num3 - 1.0);
                //if (period == Finance.Period.AtBeginning)
                //{
                //    num /= num2;
                //}
            }
            return 0.0 - num;
        }
    }
}
