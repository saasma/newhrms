﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Web;

namespace Utils.Helper
{
    public class FileHelper
    {
        /// <summary>
        /// Helper method to convert Bytes in KiloBytes
        /// </summary>
        /// <param name="byteValue"></param>
        /// <returns></returns>
        public static int ConvertBytesIntoKB(int byteValue)
        {
            double mb = (byteValue / 1024);
            if (mb.ToString("#") == "")
                return 0;
            return Convert.ToInt32(mb.ToString("#"));
        }
        public static string ConvertKBIntoMB(object value)
        {
            long kb = Convert.ToInt64(value);
            double mb = (kb / 1024.0);
            return mb.ToString("#.#");
        }

        public static string GetNewUserNameMailContent()
        {
            string path = "~/App_Data/EmailTemplates/NewUserNameEmailContent.txt";
            return GetAbsPath(path);


        }

        public static string GetPasswordMailContent()
        {
            string path = "~/App_Data/EmailTemplates/PasswordEmailContent.txt";
            return GetAbsPath(path);


        }

        //public static string GetLeaveRequestContent()
        //{
        //    string path = "~/App_Data/EmailTemplates/LeaveRequest.txt";
        //    return GetAbsPath(path);

        //}

        public static string GetLeaveApproveContent()
        {

            string path = "~/App_Data/EmailTemplates/LeaveApproval.txt";
            return GetAbsPath(path);

       
        }

        public static string GetLeaveApproveNotification()
        {
            string path = "~/App_Data/EmailTemplates/LeaveApprovalNotification.txt";
            return GetAbsPath(path);

        }

        public static string GetHourlyLeaveApproveContent()
        {
            string path = "~/App_Data/EmailTemplates/HourlyLeaveApproval.txt";
            return GetAbsPath(path);
        }

        public static string GetHoulyLeaveApproveNotification()
        {
            string path = "~/App_Data/EmailTemplates/HourlyLeaveApprovalNotification.txt";
            return GetAbsPath(path);

        }

        public static string GetAbsPath(string path)
        {
            if (HttpContext.Current == null)
                return  File.ReadAllText(HttpRuntime.AppDomainAppPath + path.Replace("~", string.Empty).Replace('/', '\\'));

            return File.ReadAllText(HttpContext.Current.Server.MapPath(path));
        }

        public static string GetHourlyLeaveRequest()
        {
            string path = "~/App_Data/EmailTemplates/HourlyLeaveRequest.txt";
            return GetAbsPath(path);

        }
    }
}
