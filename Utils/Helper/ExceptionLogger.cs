﻿using System;
using System.Text;
using System.Threading;
using System.Web;

using Utils;

namespace MyUtils.Exceptions
{
    /// <summary>
    /// Make the developers aware of unexpected exceptions
    /// </summary>
    public abstract class ExceptionLogger
    {
        /// <summary>
        /// Gets a stacktrace string, including inner exceptions.
        /// </summary>
        /// <param name="ex"></param>
        /// <returns></returns>
        private static string GetStackTrace(Exception ex)
        {
            StringBuilder b = new StringBuilder();
            Exception current = ex;
            int depth = 0;
            while (current != null && depth < 20)
            {
                b.AppendLine(string.Format("{0}: {1}", current.GetType().FullName, current.Message));
                b.AppendLine(current.StackTrace);
                b.AppendLine("");
                current = current.InnerException;
                depth++;
            }
            return b.ToString();
        }

        /// <summary>
        /// Present a lot of information about an exception
        /// </summary>
        /// <param name="ex"></param>
        /// <returns></returns>
        public static string BuildExceptionDescription(Exception ex)
        {
            return string.Format(
                "Unhandled {2}: {0}<br /><br />Source: {3}<br /><br />{1}",
                ex.Message,
                GetStackTrace(ex).Replace("\n", "<br />"),
                ex.GetType().FullName,
                ex.Source);
        }

        /// <summary>
        /// Should be placed in all catch-all try-catch
        /// Send an email, maybe log in the database (tcdatabase)
        /// </summary>
        /// <param name="exc"></param>
        public static void UnexpectedException(Exception exc)
        {
            try
            {
                if (exc != null && !(exc is ThreadAbortException))
                {
                    SendExceptionEmail(exc);
                }
/*
                if (exc != null && exc is OutOfMemoryException)
                {
                    MaybeRestartWorkApplication(exc);
                }


                if (exc != null && exc is OutOfMemoryException)
                {
                    TactonWebParts2.TEMPLATE.LAYOUTS.Tacton2.TCAdminCodeBehind.ClearInternalCaches();
                }
*/


            }
            catch
            {
            }
        }

        /// <summary>
        /// Create a nice email with the exception
        /// </summary>
        /// <param name="ex"></param>
        private static void SendExceptionEmail(Exception ex)
        {
            string to = Config.ExceptionEmail;
            if (string.IsNullOrEmpty(to)) return;

            HttpContext context = HttpContext.Current;
            HttpServerUtility server = (context == null) ? null : context.Server;
            // ignore Error: System.IO.FileNotFoundException: c:\inetpub\wwwroot\mailer\get_aspx_ver.aspx

            string name = server == null ? "" : server.MachineName;
            string path = server == null ? "" : server.MapPath("");

            string subject = "Unexpected Exception on " + name + " " + path;

            string body = BuildExceptionDescription(ex);
            //Mailer.send_V1(to, subject, body, true);

        }
    }
}
