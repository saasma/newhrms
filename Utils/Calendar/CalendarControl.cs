﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ext.Net;

namespace Utils.Calendar
{
    public class CalendarExtControl : TextField
    {



        public CalendarExtControl()
        {
    


            this.FieldCls = "textfieldCalendarIcon";

           

            this.Listeners.Focus.Handler = "if( typeof('showCalendar') != 'undefined') showCalendar(this);";

           

            this.Listeners.Render.Handler = "this.getEl().on('click', calendarOnClick);";
            this.Listeners.Blur.Handler = "checkDateIfValid(this);";

            this.EnableKeyEvents = true;
            this.Listeners.KeyDown.Fn = "calendarKeyPress";

            //click event handler also to display calendar popup
            this.Listeners.AfterRender.Fn = "afterRender";
         
        }

    }
}
