﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Globalization;

namespace Utils.Calendar
{

    public class CustomDate
    {

        public int Day { get; set; }
        public int Month { get; set; }
        public int Year { get; set; }

        private bool? _isEnglishDate;
        public bool? IsEnglishDate
        {
            get { return _isEnglishDate; }
            set { _isEnglishDate = value; }
        }

        public DateTime EnglishDate
        {
            get
            {
                if (IsEnglishDate != null && IsEnglishDate.Value)
                    return new DateTime(this.Year, this.Month, this.Day);
                else
                {
                    CustomDate engDate = CustomDate.ConvertNepToEng(this);
                    return new DateTime(engDate.Year, engDate.Month, engDate.Day);
                }
            }
        }

        public CustomDate() { }
        //public CustomDate(int day, int month, int year)
        //{
        //    this.Day = day;
        //    this.Month = month;
        //    this.Year = year;
        //}

        public static CustomDate GetTodayDate(bool IsEnglish)
        {

            DateTime now =  Utils.Helper.Util. GetCurrentDateAndTime();
            CustomDate date = new CustomDate(now.Day, now.Month, now.Year, true);
            if (IsEnglish == false)
            {
                date = CustomDate.ConvertEngToNep(date);
            }
            return date;
        }

        public CustomDate GetLastDateOfThisMonth()
        {
            if (IsEnglishDate.Value)
            {
                DateTime date = new DateTime(this.Year, this.Month, this.Day);

                return new CustomDate(DateTime.DaysInMonth(this.Year, this.Month), date.Month, date.Year, IsEnglishDate.Value);
            }
            else
            {
                int totalDaysInMonth = DateHelper.GetTotalDaysInTheMonth(this.Year, this.Month, _isEnglishDate.Value);

                return new CustomDate(totalDaysInMonth, this.Month, this.Year, IsEnglishDate.Value);


            }
        }

        public CustomDate GetFirstDateOfThisMonth()
        {
            if (IsEnglishDate.Value)
            {
                DateTime date = new DateTime(this.Year, this.Month, this.Day);

                return new CustomDate(1, date.Month, date.Year, IsEnglishDate.Value);
            }
            else
            {
                return new CustomDate(1, this.Month, this.Year, IsEnglishDate.Value);


            }
        }

        public CustomDate(int day, int month, int year, bool isEnglishDate)
        {
            this.Day = day;
            this.Month = month;
            this.Year = year;
            this.IsEnglishDate = isEnglishDate;
        }

        public override bool Equals(object obj)
        {
            CustomDate otherDate = obj as CustomDate;
            if (otherDate.Day == this.Day && otherDate.Month == this.Month && otherDate.Year == this.Year
                && otherDate.IsEnglishDate == this.IsEnglishDate)
                return true;
            return false;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        public override string ToString()
        {
            //return Day + "/" + Month + "/" + Year;
            //if(Config.DateFormat_YYYY_MM_DD)
                return Year + "/" + Month.ToString().PadLeft(2, '0') + "/" + Day.ToString().PadLeft(2, '0');
            //return Day + "/" + Month + "/" + Year;
        }

        public string ToStringWithFormat(string dateFormat)
        {
            if (dateFormat.Equals("dd/mm/yyyy"))
                return Day + "/" + Month + "/" + Year;
            else if (dateFormat.Equals("yyyy/mm/dd"))
                return Year + "/" + Month + "/" + Day;
            else
                return Month + "/" + Day + "/" + Year;
        }

        public string ToStringMonthEng()
        {
            return Year + "-" + DateHelper.GetMonthShortName( Month,IsEnglishDate.Value) + "-" + Day;
        }
        public string ToStringShortMonthName()
        {
            return Year + "-" + DateHelper.GetMonthShortNameForBoth(Month, IsEnglishDate.Value) + "-" + Day;
        }
        public string ToStringSkipDay()
        {
            return DateHelper.GetMonthShortName(Month, IsEnglishDate.Value) + "-" + Year;
        }

        public string ToStringSkipYear()
        {
            return Day + "-" + DateHelper.GetMonthShortName(Month, IsEnglishDate.Value);
        }
        public string ToStringMonthDaySkipYear()
        {
            return DateHelper.GetMonthShortNameForBoth(Month, IsEnglishDate.Value) + "-" + Day;
        }
        public CustomDate AddMonthIfFirstDay(int monthAdd)
        {
            if (this.Day != 1)
                throw new Exception("Day must be first day of the month.");

            int day = this.Day;
            int month = this.Month;
            int year = this.Year;

            for (int i = 1; i <= monthAdd; i++)
            {
                month++;

                if (month > 12)
                {
                    month = 1;
                    year += 1;
                }
            }

            return new CustomDate(day, month, year, IsEnglishDate.Value);
        }

        /// <summary>
        /// Increments by one day
        /// </summary>
        public CustomDate IncrementByOneDay()
        {
            if( IsEnglishDate.Value)
            {
                DateTime date = new DateTime(this.Year,this.Month,this.Day);
                date = date.AddDays(1);
                return new CustomDate(date.Day, date.Month, date.Year, IsEnglishDate.Value);
            }
            else
            {
                //for nepali incrementing by one day
                CustomDate engDate = CustomDate.ConvertNepToEng(this);
                DateTime date = new DateTime(engDate.Year, engDate.Month, engDate.Day);
                date = date.AddDays(1);
                engDate = new CustomDate(date.Day, date.Month, date.Year,true);
                return CustomDate.ConvertEngToNep(engDate);
            }
        }

        /// <summary>
        /// Increments by one day
        /// </summary>
        public CustomDate DecrementByOneDay()
        {
            if (IsEnglishDate.Value)
            {
                DateTime date = new DateTime(this.Year, this.Month, this.Day);
                date = date.AddDays(-1);
                return new CustomDate(date.Day, date.Month, date.Year, IsEnglishDate.Value);
            }
            else
            {
                //for nepali incrementing by one day
                CustomDate engDate = CustomDate.ConvertNepToEng(this);
                DateTime date = new DateTime(engDate.Year, engDate.Month, engDate.Day);
                date = date.AddDays(-1);
                engDate = new CustomDate(date.Day, date.Month, date.Year, true);
                return CustomDate.ConvertEngToNep(engDate);
            }
        }


        ///// <summary>
        ///// Increments by one day
        ///// </summary>
        //public CustomDate IncrementByOneMonth()
        //{
        //    if (IsEnglishDate.Value)
        //    {
        //        DateTime date = new DateTime(this.Year, this.Month, this.Day);
        //        date = date.AddMonths(1);
        //        return new CustomDate(date.Day, date.Month, date.Year, IsEnglishDate.Value);
        //    }
        //    else
        //    {
        //        //for nepali incrementing by one day
        //        CustomDate engDate = CustomDate.ConvertNepToEng(this);
        //        DateTime date = new DateTime(engDate.Year, engDate.Month, engDate.Day);
        //        date = date.AddMonths(1);
        //        engDate = new CustomDate(date.Day, date.Month, date.Year, IsEnglishDate.Value);
        //        return CustomDate.ConvertEngToNep(engDate);
        //    }
        //}

        public static CustomDate GetCustomDateFromString(string dateValue, bool isEnglishDate)
        {
            dateValue = dateValue.Replace('-', '/');
            if (dateValue == null)
                return null;
            string[] values = dateValue.Split(new char[] { '/' });
            CustomDate cDate = null;

            //if (Config.DateFormat_YYYY_MM_DD)
                cDate = new CustomDate(int.Parse(values[2]), int.Parse(values[1]), int.Parse(values[0]), isEnglishDate);
            //else
            //    cDate = new CustomDate(int.Parse(values[0]), int.Parse(values[1]), int.Parse(values[2]), isEnglishDate);
            return cDate;
        }

        public static CustomDate GetCustomDateFromString(string dateValue, bool isEnglishDate,string dateFormat)
        {
            if (dateValue == null)
                return null;
            string[] values = dateValue.Split(new char[] { '/' });

            //if(dateFormat.Equals("dd/mm/yyyy"))
                return new CustomDate(int.Parse(values[2]), int.Parse(values[1]), int.Parse(values[0]), isEnglishDate);
            //else if(dateFormat.Equals("yyyy/mm/dd"))
            //    return new CustomDate(int.Parse(values[0]), int.Parse(values[1]), int.Parse(values[2]), isEnglishDate); 
            //else 
            //    // mm/dd/yyyy
            //    return new CustomDate(int.Parse(values[1]), int.Parse(values[2]), int.Parse(values[0]), isEnglishDate);
        }

        public static CustomDate ConvertNepToEng(CustomDate date)
        {
            if (date.IsEnglishDate == null)
                throw new ArgumentException("Date IsEnglishDate property must be defined.");

            if (date.IsEnglishDate == false)
            {
                //Check for nepali date range that only be converted by the code

                //if (date.Year < 2000 || date.Year > 2089)
                //    throw new ArgumentException("Year must be between 2000-2089.");
                if (date.Month < 1 || date.Month > 12)
                    throw new ArgumentException("Month must be between 1-12.");
                if (date.Day < 1 || date.Day > 32)
                    throw new ArgumentException("Day must be between 1-32.");

                return GetEngDate(date);
            }
            else
                throw new ArgumentException("Date is in already nepali.");

        }

        [NUnit.Framework.Test]
        public void TestDate()
        {
            CustomDate date = new CustomDate(11,4,2068,false);
            date = ConvertNepToEng(date);

        }

       

        public static CustomDate ConvertEngToNep(CustomDate date)
        {
            if (date._isEnglishDate == null)
                throw new ArgumentException("Date IsEnglishDate property must be defined.");

            if (date.IsEnglishDate == true)
            {
                //Check for nepali date range that only be converted by the code
                // *Changed from 1944 to 1914 and, 2033 to 2053
                if (date.Year < 1914 || date.Year > 2053)
                    throw new ArgumentException("Year must be between 1914-2053.");
                if (date.Month < 1 || date.Month > 12)
                    throw new ArgumentException("Month must be between 1-12.");
                if (date.Day < 1 || date.Day > 31)
                    throw new ArgumentException("Day must be between 1-31.");

                return GetNepDate(date);
            }
            else
                //throw new ArgumentException("Date is in already nepali.");
                return date;
        }

        private static CustomDate GetNepDate(CustomDate date)
        {
            int dd = date.Day;
            int mm = date.Month;
            int yy = date.Year;

            // english month data.
            int[] month = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
            int[] lmonth = { 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

             int def_eyy = 1914; //spear head english date... *Changed from 1944 to 1914
            int def_nyy = 1970; // *Changed from 2000
            int def_nmm = 9;    
            int def_ndd = 18 - 1; //spear head nepali date... *Original 17-1
            int total_eDays = 0;
            int total_nDays = 0;
            int a = 0;
            int day = 5 - 1; //all the initializations...
            int m = 0;
            int y = 0;
            int i = 0;
            int j = 0;
            int numDay = 0;

            // count total no. of days in-terms of year
            for (i = 0; i < (yy - def_eyy); i++)
            {
                if (DateTime.IsLeapYear(def_eyy + i))
                {
                    for (j = 0; j < 12; j++)
                        total_eDays += lmonth[j];
                }
                else
                {
                    for (j = 0; j < 12; j++)
                        total_eDays += month[j];
                }
            }

            // count total no. of days in-terms of month			
            for (i = 0; i < (mm - 1); i++)
            {
                if (DateTime.IsLeapYear(yy))
                    total_eDays += lmonth[i];
                else
                    total_eDays += month[i];
            }

            // count total no. of days in-terms of date
            total_eDays += dd;

            i = 0;
            j = def_nmm;
            total_nDays = def_ndd;
            m = def_nmm;
            y = def_nyy;

            while (total_eDays != 0)
            {
                a = DateHelper.Months[i, j];
                total_nDays++;
                day++;
                if (total_nDays > a)
                {
                    m++;
                    total_nDays = 1;
                    j++;
                }
                if (day > 7)
                    day = 1;
                if (m > 12)
                {
                    y++;
                    m = 1;
                }
                if (j > 12)
                {
                    j = 1;
                    i++;
                }
                total_eDays--;
            }

            numDay = day;

            //set new eng date
            CustomDate nepDate = new CustomDate();
            nepDate.Year = y;
            nepDate.Month = m;
            nepDate.Day = total_nDays;
            nepDate.IsEnglishDate = false;

            return nepDate;

        }

        private static CustomDate GetEngDate(CustomDate date)
        {
            int yy = date.Year, mm = date.Month, dd = date.Day;

            int def_eyy = 1913; int def_emm = 4; int def_edd = 13 - 1;		// init english date. *Original 1943,4,14-1
            int def_nyy = 1970; 		// equivalent nepali date.          // *Original 2000
            int total_eDays = 0; int total_nDays = 0; int a = 0; int day = 0 - 1;		// initializations...
            int m = 0; int y = 0;
            int i = 0, j = 0;
            int k = 0; int numDay = 0;

            //may be the total months of the nep starting year
            int[] month = { 0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
            int[] lmonth = { 0, 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };


            // count total days in-terms of year
            for (i = 0; i < (yy - def_nyy); i++)
            {
                for (j = 1; j <= 12; j++)
                {
                    total_nDays += DateHelper.Months[k, j];
                }
                k++;
            }

            // count total days in-terms of month	
            for (j = 1; j < mm; j++)
            {
                total_nDays += DateHelper.Months[k, j];
            }

            // count total days in-terms of dat
            total_nDays += dd;

            //calculation of equivalent english date...
            total_eDays = def_edd;
            m = def_emm;
            y = def_eyy;
            while (total_nDays != 0)
            {
                if (DateTime.IsLeapYear(y))
                {
                    a = lmonth[m];
                }
                else
                {
                    a = month[m];
                }
                total_eDays++;
                day++;
                if (total_eDays > a)
                {
                    m++;
                    total_eDays = 1;
                    if (m > 12)
                    {
                        y++;
                        m = 1;
                    }
                }
                if (day > 7)
                    day = 1;
                total_nDays--;
            }

            numDay = day;

            //set new eng date
            CustomDate engDate = new CustomDate();
            engDate.Year = y;
            engDate.Month = m;
            engDate.Day = total_eDays;
            engDate.IsEnglishDate = true;
            return engDate;
        }
    }


    public class DateHelper
    {

        public static readonly string[] EngShortMonths = { "","Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };
        public static readonly string[] EngFullMonths = { "","January", "Febuary", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" };
        public static readonly string[] NepMonths = { "","Baisakh", "Jestha", "Ashadh", "Shrawan", "Bhadra", "Ashwin", "Kartik", "Mangsir", "Poush", "Magh", "Falgun", "Chaitra" };
        public static readonly string[] NepShortMonths = { "", "Bai", "Jes", "Ash", "Shr", "Bha", "Ash", "Kar", "Man", "Pou", "Mag", "Fal", "Chai" };

        public static string GetMonthName(int month, bool isEnglishDate)
        {
            if (isEnglishDate)
                return EngFullMonths[month];
            else
                return NepMonths[month];
        }


        public static string GetEngDateRangeForNepaliDate(CustomDate nepDate)
        {
            CustomDate start = new CustomDate(1, nepDate.Month, nepDate.Year, false);
            CustomDate end = new CustomDate(
                DateHelper.GetTotalDaysInTheMonth(nepDate.Year, nepDate.Month, false), nepDate.Month, nepDate.Year, false);

            return start.EnglishDate.ToString("MMM dd") + "-" + end.EnglishDate.ToString("MMM dd");
        }

        // Get British school Payroll Period month name
        public static string GetBSPayrollPeriodForPayslip(int monthIndex, bool isEnglish)
        {
            switch (monthIndex)
            {
                case 1:
                    return EngFullMonths[5];

                case 2:
                    return EngFullMonths[6];

                case 3:
                    return EngFullMonths[7];

                case 4:
                    return EngFullMonths[8];

                case 5:
                    return EngFullMonths[9];

                case 6:
                    return EngFullMonths[10];

                case 7:
                    return EngFullMonths[11];

                case 8:
                    return EngFullMonths[12];

                case 9:
                    return EngFullMonths[1];

                case 10:
                    return EngFullMonths[2];

                case 11:
                    return EngFullMonths[3];

                case 12:
                    return EngFullMonths[4];

            }

            return "";
        }


        public static string GetMonthsForOtherPayrollPeriod(int monthIndex,bool isEnglish)
        {
            try
            {
                if (!isEnglish)
                {
                    switch (monthIndex)
                    {
                        case 1:
                            return EngShortMonths[4] + "/" + EngShortMonths[5];
                           
                        case 2:
                            return EngShortMonths[5] + "/" + EngShortMonths[6];
                           
                        case 3:
                            return EngShortMonths[6] + "/" + EngShortMonths[7];
                           
                        case 4:
                            return EngShortMonths[7] + "/" + EngShortMonths[8];
                           
                        case 5:
                            return EngShortMonths[8] + "/" + EngShortMonths[9];
                           
                        case 6:
                            return EngShortMonths[9] + "/" + EngShortMonths[10];
                           
                        case 7:
                            return EngShortMonths[10] + "/" + EngShortMonths[11];
                            
                        case 8:
                            return EngShortMonths[11] + "/" + EngShortMonths[12];
                           
                        case 9:
                            return EngShortMonths[12] + "/" + EngShortMonths[1];
                            
                        case 10:
                            return EngShortMonths[1] + "/" + EngShortMonths[2];
                            
                        case 11:
                            return EngShortMonths[2] + "/" + EngShortMonths[3];
                           
                        case 12:
                            return EngShortMonths[3] + "/" + EngShortMonths[4];
                           
                    }
                }
                else
                {
                    switch (monthIndex)
                    {
                        case 1:
                            return NepMonths[9] + "/" + NepMonths[10];
                         
                        case 2:
                            return NepMonths[10] + "/" + NepMonths[11];
                          
                        case 3:
                            return NepMonths[11] + "/" + NepMonths[12];
                        
                        case 4:
                            return NepMonths[12] + "/" + NepMonths[1];
                           
                        case 5:
                            return NepMonths[1] + "/" + NepMonths[2];
                            
                        case 6:
                            return NepMonths[2] + "/" + NepMonths[3];
                           
                        case 7:
                            return NepMonths[3] + "/" + NepMonths[4];
                           
                        case 8:
                            return NepMonths[4] + "/" + NepMonths[5];
                           
                        case 9:
                            return NepMonths[5] + "/" + NepMonths[6];
                          
                        case 10:
                            return NepMonths[6] + "/" + NepMonths[7];
                            
                        case 11:
                            return NepMonths[7] + "/" + NepMonths[8];
                            
                        case 12:
                            return NepMonths[8] + "/" + NepMonths[9];
                           
                    }
                }
            }
            catch (Exception exp)
            {
               
            }
            return "";
        }

        public static string GetMonthShortName(int month, bool isEnglishDate)
        {
            if (isEnglishDate)
                return EngShortMonths[month];
            else
                return NepMonths[month];
        }

        public static string GetMonthShortNameForBoth(int month, bool isEnglishDate)
        {
            if (isEnglishDate)
                return EngShortMonths[month];
            else
                return NepShortMonths[month];
        }
        public static int GetTotalDaysInTheMonth(int year, int month, bool isEnglishCalendar)
        {
            int totalDays;
            if (isEnglishCalendar)
            {
                totalDays = DateTime.DaysInMonth(year, month);
            }
            else
            {
                int index = year - 1970;
                if (DateHelper.Months.GetLength(0) > index)
                    totalDays = DateHelper.Months[index, month];
                else
                    // invalid year, should throw error and stop processing as if proced further invalid calculation may occur
                    return 0;
            }
            return totalDays;
        }

        /// <summary>
        /// Days in each month of each year for nepali date
        /// </summary>
        public static readonly int[,] Months =        
    {
			{1970, 31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30},
            {1971, 31, 31, 32, 31, 32, 30, 30, 29, 30, 29, 30, 30},
			{1972, 31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 30},
			{1973, 30, 32, 31, 32, 31, 30, 30, 30, 29, 30, 29, 31},
			{1974, 31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30},
			{1975, 31, 31, 32, 32, 31, 31, 30, 29, 30, 29, 30, 30},
			{1976, 31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 31},
			{1977, 30, 32, 31, 32, 31, 31, 29, 30, 29, 30, 29, 31},
			{1978, 31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30},
			{1979, 31, 31, 32, 32, 31, 30, 30, 29, 30, 29, 30, 30},
			{1980, 31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 31},
			{1981, 31, 31, 31, 32, 31, 31, 29, 30, 30, 29, 30, 30},
			{1982, 31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30},
			{1983, 31, 31, 32, 32, 31, 30, 30, 29, 30, 29, 30, 30},
			{1984, 31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 31},
			{1985, 31, 31, 31, 32, 31, 31, 29, 30, 30, 29, 30, 30},
			{1986, 31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30},
			{1987, 31, 32, 31, 32, 31, 30, 30, 29, 30, 29, 30, 30},
			{1988, 31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 31},
			{1989, 31, 31, 31, 32, 31, 31, 30, 29, 30, 29, 30, 30},
			{1990, 31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30},
			{1991, 31, 32, 31, 32, 31, 30, 30, 29, 30, 29, 30, 30},
			{1992, 31, 32, 31, 32, 31, 30, 30, 30, 29, 30, 29, 31},
			{1993, 31, 31, 31, 32, 31, 31, 30, 29, 30, 29, 30, 30},
			{1994, 31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30},
			{1995, 31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 30},
			{1996, 31, 32, 31, 32, 31, 30, 30, 30, 29, 30, 29, 31},
			{1997, 31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30},
			{1998, 31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30},
			{1999, 31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 31},
            {2000,30,32,31,32,31,30,30,30,29,30,29,31},
			{2001,31,31,32,31,31,31,30,29,30,29,30,30},
			{2002,31,31,32,32,31,30,30,29,30,29,30,30},
			{2003,31,32,31,32,31,30,30,30,29,29,30,31},
			{2004,30,32,31,32,31,30,30,30,29,30,29,31},
			{2005,31,31,32,31,31,31,30,29,30,29,30,30},
			{2006,31,31,32,32,31,30,30,29,30,29,30,30},
			{2007,31,32,31,32,31,30,30,30,29,29,30,31},
			{2008,31,31,31,32,31,31,29,30,30,29,29,31},
			{2009,31,31,32,31,31,31,30,29,30,29,30,30},
			{2010,31,31,32,32,31,30,30,29,30,29,30,30},
			{2011,31,32,31,32,31,30,30,30,29,29,30,31},
			{2012,31,31,31,32,31,31,29,30,30,29,30,30},
			{2013,31,31,32,31,31,31,30,29,30,29,30,30},
			{2014,31,31,32,32,31,30,30,29,30,29,30,30},
			{2015,31,32,31,32,31,30,30,30,29,29,30,31},
			{2016,31,31,31,32,31,31,29,30,30,29,30,30},
			{2017,31,31,32,31,31,31,30,29,30,29,30,30},
			{2018,31,32,31,32,31,30,30,29,30,29,30,30},
			{2019,31,32,31,32,31,30,30,30,29,30,29,31},
			{2020,31,31,31,32,31,31,30,29,30,29,30,30},
			{2021,31,31,32,31,31,31,30,29,30,29,30,30},
			{2022,31,32,31,32,31,30,30,30,29,29,30,30},
			{2023,31,32,31,32,31,30,30,30,29,30,29,31},
			{2024,31,31,31,32,31,31,30,29,30,29,30,30},
			{2025,31,31,32,31,31,31,30,29,30,29,30,30},
			{2026,31,32,31,32,31,30,30,30,29,29,30,31},
			{2027,30,32,31,32,31,30,30,30,29,30,29,31},
			{2028,31,31,32,31,31,31,30,29,30,29,30,30},
			{2029,31,31,32,31,32,30,30,29,30,29,30,30},
			{2030,31,32,31,32,31,30,30,30,29,29,30,31},
			{2031,30,32,31,32,31,30,30,30,29,30,29,31},
			{2032,31,31,32,31,31,31,30,29,30,29,30,30},
			{2033,31,31,32,32,31,30,30,29,30,29,30,30},
			{2034,31,32,31,32,31,30,30,30,29,29,30,31}, 
			{2035,30,32,31,32,31,31,29,30,30,29,29,31},
			{2036,31,31,32,31,31,31,30,29,30,29,30,30},
			{2037,31,31,32,32,31,30,30,29,30,29,30,30},
			{2038,31,32,31,32,31,30,30,30,29,29,30,31},
			{2039,31,31,31,32,31,31,29,30,30,29,30,30},
			{2040,31,31,32,31,31,31,30,29,30,29,30,30},
			{2041,31,31,32,32,31,30,30,29,30,29,30,30},
			{2042,31,32,31,32,31,30,30,30,29,29,30,31},
			{2043,31,31,31,32,31,31,29,30,30,29,30,30},
			{2044,31,31,32,31,31,31,30,29,30,29,30,30},
			{2045,31,32,31,32,31,30,30,29,30,29,30,30},
			{2046,31,32,31,32,31,30,30,30,29,29,30,31},
			{2047,31,31,31,32,31,31,30,29,30,29,30,30},
			{2048,31,31,32,31,31,31,30,29,30,29,30,30},
			{2049,31,32,31,32,31,30,30,30,29,29,30,30},
			{2050,31,32,31,32,31,30,30,30,29,30,29,31},
			{2051,31,31,31,32,31,31,30,29,30,29,30,30},
			{2052,31,31,32,31,31,31,30,29,30,29,30,30},
			{2053,31,32,31,32,31,30,30,30,29,29,30,30},
			{2054,31,32,31,32,31,30,30,30,29,30,29,31},
			{2055,31,31,32,31,31,31,30,29,30,29,30,30},
			{2056,31,31,32,31,32,30,30,29,30,29,30,30},
			{2057,31,32,31,32,31,30,30,30,29,29,30,31},
			{2058,30,32,31,32,31,30,30,30,29,30,29,31},
			{2059,31,31,32,31,31,31,30,29,30,29,30,30},
			{2060,31,31,32,32,31,30,30,29,30,29,30,30},
			{2061,31,32,31,32,31,30,30,30,29,29,30,31},
		    {2062,30,32,31,32,31,31,29,30,29,30,29,31},
			{2063,31,31,32,31,31,31,30,29,30,29,30,30},
			{2064,31,31,32,32,31,30,30,29,30,29,30,30},
			{2065,31,32,31,32,31,30,30,30,29,29,30,31},
			{2066,31,31,31,32,31,31,29,30,30,29,29,31},
			{2067,31,31,32,31,31,31,30,29,30,29,30,30},
			{2068,31,31,32,32,31,30,30,29,30,29,30,30},
			{2069,31,32,31,32,31,30,30,30,29,29,30,31},
			{2070,31,31,31,32,31,31,29,30,30,29,30,30},
			{2071,31,31,32,31,31,31,30,29,30,29,30,30},
			{2072,31,32,31,32,31,30,30,29,30,29,30,30},
			{2073,31,32,31,32,31,30,30,30,29,29,30,31},
			{2074,31,31,31,32,31,31,30,29,30,29,30,30},
			{2075,31,31,32,31,31,31,30,29,30,29,30,30},
			{2076,31,32,31,32,31,30,30,30,29,29,30,30},
			{2077,31,32,31,32,31,30,30,30,29,30,29,31},
			{2078,31,31,31,32,31,31,30,29,30,29,30,30},
			{2079,31,31,32,31,31,31,30,29,30,29,30,30},
			{2080,31,32,31,32,31,30,30,30,29,29,30,30},
			{2081, 31, 31, 32, 32, 31, 30, 30, 30, 29, 30, 30, 30},
			{2082, 30, 32, 31, 32, 31, 30, 30, 30, 29, 30, 30, 30},
			{2083, 31, 31, 32, 31, 31, 30, 30, 30, 29, 30, 30, 30},
			{2084, 31, 31, 32, 31, 31, 30, 30, 30, 29, 30, 30, 30},
			{2085, 31, 32, 31, 32, 30, 31, 30, 30, 29, 30, 30, 30},
			{2086, 30, 32, 31, 32, 31, 30, 30, 30, 29, 30, 30, 30},
			{2087, 31, 31, 32, 31, 31, 31, 30, 30, 29, 30, 30, 30},
			{2088, 30, 31, 32, 32, 30, 31, 30, 30, 29, 30, 30, 30},
			{2089, 30, 32, 31, 32, 31, 30, 30, 30, 29, 30, 30, 30},
            {2090, 30, 32, 31, 32, 31, 30, 30, 30, 29, 30, 30, 30},
			{2091, 31, 31, 32, 31, 31, 30, 30, 30, 29, 30, 30, 30},
            {2092, 31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30},
            {2093, 31, 32, 31, 31, 31, 31, 30, 29, 30, 29, 30, 31},
            {2094, 31, 31, 31, 32, 31, 30, 30, 30, 29, 30, 30, 30},
            {2095, 31, 31, 32, 31, 31, 30, 30, 30, 29, 30, 30, 30},
			{2096, 31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30},
			{2097, 31, 32, 30, 31, 31, 31, 30, 30, 29, 30, 29, 31},
			{2098, 31, 31, 31, 32, 31, 30, 30, 30, 29, 30, 30, 30},
			{2099, 31, 31, 32, 31, 31, 30, 30, 30, 29, 30, 30, 30},
			{2100, 31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30},
			{2101, 31, 32, 31, 31, 31, 31, 30, 30, 29, 30, 29, 31},
			{2102, 31, 31, 31, 32, 31, 30, 30, 30, 29, 30, 30, 30},
			{2103, 31, 31, 32, 31, 31, 30, 30, 30, 30, 29, 30, 30},
			{2104, 31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 31},
			{2105, 30, 32, 31, 31, 31, 31, 30, 30, 29, 30, 29, 31},
			{2106, 31, 31, 31, 32, 31, 30, 30, 30, 29, 30, 30, 30},
			{2107, 31, 31, 32, 31, 31, 30, 30, 30, 30, 29, 30, 30},
			{2108, 31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 31},
			{2109, 30, 32, 31, 31, 31, 31, 30, 30, 29, 30, 29, 31},
			{2110, 31, 31, 31, 32, 31, 30, 30, 30, 29, 30, 30, 30}
    };
    }
}