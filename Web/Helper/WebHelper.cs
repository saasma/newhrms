﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DAL;
using BLL.Manager;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.Data;

namespace Web.Helper
{
    public class WebHelper
    {

        /// <summary>
        /// Export data table to excel file
        /// </summary>
        /// <param name="dt">Data table source</param>
        /// 
        public static void ExportDtToExcel(DataTable dt, string filename)
        {

            if (dt.Rows.Count > 0)
            {
                string attachment = "attachment; filename=" + filename + ".xls";
                HttpContext.Current.Response.ClearContent();
                HttpContext.Current.Response.AddHeader("content-disposition", attachment);
                HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";
                string tab = "";
                foreach (DataColumn dc in dt.Columns)
                {
                    HttpContext.Current.Response.Write(tab + dc.ColumnName);
                    tab = "\t";
                }
                HttpContext.Current.Response.Write("\n");
                int i;
                foreach (DataRow dr in dt.Rows)
                {
                    tab = "";
                    for (i = 0; i < dt.Columns.Count; i++)
                    {
                        HttpContext.Current.Response.Write(tab + dr[i].ToString());
                        tab = "\t";
                    }
                    HttpContext.Current.Response.Write("\n");
                }
                HttpContext.Current.Response.End();

            }
        }


        public static string TimeAgoWithMinSecond(DateTime dateTime)
        {
            string result = string.Empty;


            var timeSpan = DateTime.Now.Subtract(dateTime);

            if (timeSpan <= TimeSpan.FromSeconds(60))
            {
                result = string.Format("{0} seconds ago", timeSpan.Seconds);
            }
            else if (timeSpan <= TimeSpan.FromMinutes(60))
            {
                result = timeSpan.Minutes > 1 ?
                    String.Format("{0} minutes ago", timeSpan.Minutes) :
                    "about a minute ago";
            }
            else if (timeSpan <= TimeSpan.FromHours(24))
            {
                result = timeSpan.Hours > 1 ?
                    String.Format("{0} hours ago", timeSpan.Hours) :
                    "about an hour ago";
            }
            else if (timeSpan <= TimeSpan.FromDays(30))
            {
                result = timeSpan.Days > 1 ?
                    String.Format("{0} days ago", timeSpan.Days) :
                    "yesterday";
            }
            else if (timeSpan <= TimeSpan.FromDays(365))
            {
                result = timeSpan.Days > 30 ?
                    String.Format("{0} months ago", timeSpan.Days / 30) :
                    "about a month ago";
            }
            else
            {
                result = timeSpan.Days > 365 ?
                    String.Format("{0} years ago", timeSpan.Days / 365) :
                    "about a year ago";
            }

            return result;
        }

        public static string TimeAgoOrInWithDays(DateTime dateTime)
        {
            string result = string.Empty;

            if (dateTime.Date == DateTime.Now.Date)
                return "Today";



            // ago
            if (DateTime.Now.Date > dateTime.Date)
            {

                var timeSpan = DateTime.Now.Date.Subtract(dateTime.Date);

                if (timeSpan <= TimeSpan.FromDays(30))
                {
                    result =
                        String.Format("{0} days ago", timeSpan.Days);


                }
                else if (timeSpan <= TimeSpan.FromDays(365))
                {
                    result = timeSpan.Days > 30 ?
                        String.Format("{0} months ago", timeSpan.Days / 30) :
                        "about a month ago";
                }
                else
                {
                    result = timeSpan.Days > 365 ?
                        String.Format("{0} years ago", timeSpan.Days / 365) :
                        "about a year ago";
                }
            }
            else
            {
                // in
                var timeSpan = dateTime.Date.Subtract(DateTime.Now.Date);


                if (timeSpan <= TimeSpan.FromDays(30))
                {
                    result =
                        String.Format("in {0} days", timeSpan.Days);

                    //if (timeSpan.Hours > 0)
                    //    result += " " + timeSpan.Hours + " hours";
                }
                else if (timeSpan <= TimeSpan.FromDays(365))
                {
                    result = timeSpan.Days > 30 ?
                        String.Format("in {0} months", timeSpan.Days / 30) :
                        "in a month";
                }
                else
                {
                    result = timeSpan.Days > 365 ?
                        String.Format("in {0} years", timeSpan.Days / 365) :
                        "in a year";
                }
            }
            return result;
        }
        public static string FormatDateForExcel(DateTime? date)
        {
            if (date == null)
                return "";
            return date.Value.ToString("yyyy-MM-dd");
        }
        public static void GetAddressHTML(EAddress entity, ref string present, ref string permanent)
        {
            // Locality
            if (!string.IsNullOrEmpty(entity.PSLocality))
            {
                present = entity.PSLocality + "<br><br>";
            }
            // Street and House no
            if (!string.IsNullOrEmpty(entity.PSStreet))
                present += entity.PSStreet;
            if (!string.IsNullOrEmpty(entity.PSWardNo))
                present += " - " + entity.PSWardNo;
            if (!string.IsNullOrEmpty(entity.PSHouseNo))
                present += " - " + entity.PSHouseNo;

            // VDC 
            if (!string.IsNullOrEmpty(entity.PSVDCMuncipality))
                present += "<br>" + entity.PSVDCMuncipality;
            //Distirct and Zone
            if (entity.PSDistrictId != null && entity.PSDistrictId != -1 && entity.PSDistrictId != 0)
                present += "<br>" + CommonManager.GetDistrictById(entity.PSDistrictId.Value).District;
            if (entity.PSZoneId != null && entity.PSZoneId != -1 && entity.PSZoneId != 0)
                present += ", " + CommonManager.GetZoneById(entity.PSZoneId.Value).Zone;
            //Country
            if (entity.PSCountryId != null && entity.PSCountryId != -1 && entity.PSCountryId != 0)
                present += "<br>" + CommonManager.GetCountryById(entity.PSCountryId.Value).CountryName;
            //if (!string.IsNullOrEmpty(entity.PSCitIssDis))
            //    present += "<br>" + entity.PSCitIssDis;




            // Locality
            if (!string.IsNullOrEmpty(entity.PELocality))
            {
                permanent = entity.PELocality + "<br><br>";
            }
            // Street and House no
            if (!string.IsNullOrEmpty(entity.PEStreet))
                permanent += entity.PEStreet;
            if (!string.IsNullOrEmpty(entity.PEWardNo))
                permanent += " - " + entity.PEWardNo;
            if (!string.IsNullOrEmpty(entity.PEHouseNo))
                permanent += " - " + entity.PEHouseNo;

            // VDC 
            if (!string.IsNullOrEmpty(entity.PEVDCMuncipality))
                permanent += "<br>" + entity.PEVDCMuncipality;
            //Distirct and Zone
            if (entity.PEDistrictId != null && entity.PEDistrictId != -1)
                permanent += "<br>" + CommonManager.GetDistrictById(entity.PEDistrictId.Value).District;
            if (entity.PEZoneId != null && entity.PEZoneId != -1)
                permanent += ", " + CommonManager.GetZoneById(entity.PEZoneId.Value).Zone;
            //Country
            if (entity.PECountryId != null && entity.PECountryId != -1)
                permanent += "<br>" + CommonManager.GetCountryById(entity.PECountryId.Value).CountryName;
        }

        public static void GetAddressHTMLForEmployeeProfile(EAddress entity, ref string present, ref string permanent)
        {
            // Locality
            if (!string.IsNullOrEmpty(entity.PSLocality))
            {
                present = entity.PSLocality + "<br><br>";
            }
            // Street and House no
            if (!string.IsNullOrEmpty(entity.PSStreet))
                present += entity.PSStreet;
            if (!string.IsNullOrEmpty(entity.PSHouseNo))
                present += " - " + entity.PSHouseNo;

            // VDC 
            if (!string.IsNullOrEmpty(entity.PSVDCMuncipality))
                present += " " + entity.PSVDCMuncipality;
            //Distirct and Zone
            if (entity.PSDistrictId != null && entity.PSDistrictId != -1)
                present += " " + CommonManager.GetDistrictById(entity.PSDistrictId.Value).District;
            if (entity.PSZoneId != null && entity.PSZoneId != -1)
                present += ", " + CommonManager.GetZoneById(entity.PSZoneId.Value).Zone;
            //Country
            if (entity.PSCountryId != null && entity.PSCountryId != -1)
                present += " " + CommonManager.GetCountryById(entity.PSCountryId.Value).CountryName;




            // Locality
            if (!string.IsNullOrEmpty(entity.PELocality))
            {
                permanent = entity.PELocality + "<br><br>";
            }
            // Street and House no
            if (!string.IsNullOrEmpty(entity.PEStreet))
                permanent += entity.PEStreet;
            if (!string.IsNullOrEmpty(entity.PEHouseNo))
                permanent += " - " + entity.PEHouseNo;

            // VDC 
            if (!string.IsNullOrEmpty(entity.PEVDCMuncipality))
                permanent += " " + entity.PEVDCMuncipality;
            //Distirct and Zone
            if (entity.PEDistrictId != null && entity.PEDistrictId != -1)
                permanent += " " + CommonManager.GetDistrictById(entity.PEDistrictId.Value).District;
            if (entity.PEZoneId != null && entity.PEZoneId != -1)
                permanent += ", " + CommonManager.GetZoneById(entity.PEZoneId.Value).Zone;
            //Country
            if (entity.PECountryId != null && entity.PECountryId != -1)
                permanent += " " + CommonManager.GetCountryById(entity.PECountryId.Value).CountryName;
        }

        public static void CreateThumbnail(string filename, int desiredWidth, int desiredHeight, string outFilename)
        {

            //using (System.Drawing.Image img = System.Drawing.Image.FromFile(filename))
            //{
            //    float widthRatio = (float)img.Width / (float)desiredWidth;
            //    float heightRatio = (float)img.Height / (float)desiredHeight;

            //    float ratio = heightRatio > widthRatio ? heightRatio : widthRatio;
            //    int newWidth = Convert.ToInt32(Math.Floor((float)img.Width / ratio));
            //    int newHeight = Convert.ToInt32(Math.Floor((float)img.Height / ratio));
            //    using (System.Drawing.Image thumb = img.GetThumbnailImage(newWidth, newHeight, new System.Drawing.Image.GetThumbnailImageAbort(ThumbnailImageAbortCallback), IntPtr.Zero))
            //    {
            //        thumb.Save(outFilename, System.Drawing.Imaging.ImageFormat.Jpeg);

            //    }
            //}



            //==========================================
            //  string newFullPath = Request.PhysicalApplicationPath + GalleryWebImageDir + imageId + "_" + origVer + "_orig.jpg";

            using (System.Drawing.Image img = System.Drawing.Image.FromFile(filename))
            {

                float widthRatio = (float)img.Width / (float)desiredWidth;
                float heightRatio = (float)img.Height / (float)desiredHeight;

                float ratio = heightRatio > widthRatio ? heightRatio : widthRatio;
                int newWidth = Convert.ToInt32(Math.Floor((float)img.Width / ratio));
                int newHeight = Convert.ToInt32(Math.Floor((float)img.Height / ratio));

                using (Bitmap dstImg = new Bitmap(newWidth, newHeight))
                {
                    dstImg.SetResolution(300, 300);
                    using (Graphics g = Graphics.FromImage(dstImg))
                    {
                        g.InterpolationMode = InterpolationMode.HighQualityBicubic;
                        g.SmoothingMode = SmoothingMode.HighQuality;
                        g.PixelOffsetMode = PixelOffsetMode.HighQuality;
                        g.CompositingQuality = CompositingQuality.HighQuality;


                        // Resize the original
                        g.DrawImage(img, 0, 0, newWidth, newHeight);
                        //g.Dispose();
                    }

                    using (EncoderParameters encoderParameters = new EncoderParameters(1))
                    {
                        encoderParameters.Param[0] = new EncoderParameter(Encoder.Compression, 100);          // 100% Percent Compression
                        dstImg.Save(outFilename, ImageCodecInfo.GetImageEncoders()[1], encoderParameters);   // jpg format
                        //dstImg.Dispose();
                    }
                }

                //img.Dispose();

            }


            //  Bitmap thumpnail = GetBitMapImage(filename, desiredWidth, desiredHeight);
            // Save the image as a Jpeg.
            //  thumpnail.Save(outFilename, System.Drawing.Imaging.ImageFormat.Jpeg);

        }



        public static bool ThumbnailImageAbortCallback()
        {
            return true;
        }

        public static int Version
        {
            get
            {
                return System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.Revision;
            }
        }


        static string[] ones = new string[] { "", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine" };
        static string[] teens = new string[] { "Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen", "Nineteen" };
        static string[] tens = new string[] { "Twenty", "Thirty", "Forty", "Fifty", "Sixty", "Seventy", "Eighty", "Ninety" };
        static string[] thousandsGroups = { "", " Thousand", " Million", " Billion" };

        private static string FriendlyInteger(int n, string leftDigits, int thousands)
        {
            if (n == 0)
            {
                return leftDigits;
            }

            string friendlyInt = leftDigits;

            if (friendlyInt.Length > 0)
            {
                friendlyInt += " ";
            }

            if (n < 10)
            {
                friendlyInt += ones[n];
            }
            else if (n < 20)
            {
                friendlyInt += teens[n - 10];
            }
            else if (n < 100)
            {
                friendlyInt += FriendlyInteger(n % 10, tens[n / 10 - 2], 0);
            }
            else if (n < 1000)
            {
                friendlyInt += FriendlyInteger(n % 100, (ones[n / 100] + " Hundred"), 0);
            }
            else
            {
                friendlyInt += FriendlyInteger(n % 1000, FriendlyInteger(n / 1000, "", thousands + 1), 0);
            }

            return friendlyInt + thousandsGroups[thousands];
        }

        public static string IntegerToWritten(int n)
        {
            if (n == 0)
            {
                return "Zero";
            }
            else if (n < 0)
            {
                return "Negative " + IntegerToWritten(-n);
            }

            return FriendlyInteger(n, "", 0);
        }

        public static string GetEmployeeNoImage(int employeeId)
        {
            int gender = 0;

            //if (empId!= 0)
            //{
            if (employeeId != 0)
                gender = EmployeeManager.GetEmployeeGender(employeeId).Value;
            //}
            //if (employeeId != 0)
            //{
            //    gender = new EmployeeManager().GetById(employeeId).Gender.Value;
            //}

            if (gender == 1)
                return "~/images/male.png";
            else if (gender == 0)
                return "~/images/female.png";

            return "~/images/female.png";
        }
    }
}