﻿using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System;
using Web.Helper;
using Web.Master;
using Utils.Calendar;
using BLL.Manager;
using System.IO;

namespace BLL.Base
{
    public class BaseMasterPage : MasterPage
    {


        protected void CheckLogin()
        {

            //extra protection so checking if the user is logged in or not
            if (!this.Page.User.Identity.IsAuthenticated)
            {
                if (Request.Url.AbsoluteUri.ToString().ToLower().Contains("/employee/"))
                {
                    //Response.Redirect("~/Employee/Default.aspx");
                    ReturnUrl();
                    Response.End();
                }
                else
                    Response.Redirect("~/Default.aspx");
            }
            else
            {
                //if user is authenticated & is employee then can only access the page with /employee/ in the url
                if (SessionManager.CurrentLoggedInEmployeeId != 0)
                {
                    if (Request.Url.AbsoluteUri.ToString().ToLower().Contains("/employee/") == false)
                    {
                        Response.Redirect("~/Employee/Default.aspx");
                        //ReturnUrl();
                    }
                }
            }

            if (Page.User.Identity.IsAuthenticated)
            {
                if (SessionManager.CurrentLoggedInEmployeeId == 0 && SessionManager.IsCustomRole)
                {
                    string currentPage = Path.GetFileName(Request.Url.AbsoluteUri.ToString()).ToLower();
                    if (currentPage.IndexOf("?") >= 0)
                        currentPage = currentPage.Remove(currentPage.IndexOf("?"));

                    if (currentPage.ToLower().Contains("changepassword.aspx"))
                    {
                    }

                    // for now allow ExcelWindow url also
                    else if (currentPage.ToLower().Contains("dashboardhr.aspx") == false
                        && Request.Url.AbsoluteUri.ToString().ToLower().Contains("excelwindow") == false)
                    {
                        // check if user has permission for the page
                        if (UserManager.IsPageAccessible(currentPage) == false)
                        {
                            if (Page.Request.UrlReferrer != null && !string.IsNullOrEmpty(Page.Request.UrlReferrer.ToString()))
                                Response.Redirect(Page.Request.UrlReferrer.ToString(), true);
                            else
                                Response.Redirect("~/newhr/DashboardHR.aspx", true);
                        }
                    }
                }
            }
        }

        protected void ReturnUrl()
        {
            this.Response.Redirect("~/Employee/Default.aspx" +
               "?ReturnUrl=" + this.Request.Url.PathAndQuery);
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            //CheckForEngNepDatePref method is to check whether the page is in list of pages which have english and nepali date preferences 
            //if the date system of the current is nepali.

            if (SessionManager.CurrentCompany != null &&
                BLL.BaseBiz.IsEnglish == false && SessionManager.User.IsEnglishPreferred != null && SessionManager.User.IsEnglishPreferred
                && CheckForEngNepDatePref())
            {
                //skip for some child master pages like popup
                Page.ClientScript
                    .RegisterClientScriptBlock(this.GetType(), "Date111", string.Format("isEnglish = true;"), true);



                Page.ClientScript
                    .RegisterClientScriptBlock(this.GetType(), "Date222", string.Format("todayDate = '{0}';function calendarOnClick(event, sourceElement, e3){{}};",
                                                                          CustomDate.GetTodayDate(true).ToString()), true);
            }


            else if (SessionManager.CurrentCompany != null)
            {
                //skip for some child master pages like popup
                Page.ClientScript
                    .RegisterClientScriptBlock(this.GetType(), "Date111", string.Format("isEnglish = {0};", BLL.BaseBiz.IsEnglish.ToString().ToLower()), true);



                Page.ClientScript
                    .RegisterClientScriptBlock(this.GetType(), "Date222", string.Format("todayDate = '{0}';function calendarOnClick(event, sourceElement, e3){{}};",
                                                                          CustomDate.GetTodayDate(BLL.BaseBiz.IsEnglish).ToString()), true);
            }
        }
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            CheckLogin();
        }
        protected override void OnLoad(EventArgs e)
        {

            bool isEmpLogin = Request.Url.ToString().ToLower().Contains("/employee/") &&
                CommonManager.CompanySetting.WhichCompany == WhichCompany.NIBL;

            CheckLogin();

            if ((this as ForPopupPage) == null)
            {

                ContentPlaceHolder header = this.FindControl("header1") as ContentPlaceHolder;
                PlaceHolder scripts = this.FindControl("scripts") as PlaceHolder;

                //RegisterCSS("~/Styles/calendar/calendar.css?v=" + WebHelper.Version, header);
                //RegisterCSS("~/bootstrap/css/bootstrap.css?v=" + WebHelper.Version, header);
                //RegisterCSS("~/theme/css/glyphicons.css?v=" + WebHelper.Version, header);
                //RegisterCSS("~/theme/css/style.min.css?v=" + WebHelper.Version, header);
                //RegisterCSS("~/css/newcss.css?v=" + WebHelper.Version, header);

                //// new theme related starts
                //RegisterJS("~/theme/scripts/jquery-1.11.1.min.js", header);
                ////RegisterJS("~/theme/scripts/less-1.3.3.min.js?v=" + WebHelper.Version, scripts);
                ////RegisterJS("~/bootstrap/js/bootstrap.min.js?v=" + WebHelper.Version, scripts);
                //RegisterJS("~/theme/scripts/load.js?v=" + WebHelper.Version, scripts);
                //return;

                RegisterCSS("~/css/style.default.css", header);
                RegisterCSS("~/css/newDesignChange.css?v=" + WebHelper.Version, header);



                if (isEmpLogin == false)
                {
                    RegisterJS("~/js/jquery-1.11.1.min.js", header);
                    RegisterJS("~/js/jquery-migrate-1.2.1.min.js", header);
                    RegisterJS("~/js/bootstrap.min.js", header);
                    RegisterJS("~/js/modernizr.min.js", header);
                    //RegisterJS("~/js/retina.min.js", header);
                    RegisterJS("~/js/custom.js", header);


                }

                RegisterJS("~/Scripts/jquery-1.3.2.min.js", header);

                RegisterCSS("~/styles/calendar/calendar.css?v=" + WebHelper.Version, header);


                RegisterJS("~/scripts/common.js?v=" + WebHelper.Version, scripts);

                RegisterJS("~/Scripts/Calendar/nepCal.js?v=" + WebHelper.Version, scripts);
                RegisterJS("~/Scripts/enCal.js?v=" + WebHelper.Version, scripts);
                RegisterJS("~/scripts/calendar/calendar.js?v=" + WebHelper.Version, scripts);

                // new theme related ends
                if (isEmpLogin == false)
                {



                    // Don't place in this base page, use this specific in each page as most will not be using JCrop
                    //new added
                    //RegisterCSS("~/css/jquery.Jcrop.css", header);
                    //RegisterCSS("~/css/jquery.Jcrop.min.css", header);

                    //RegisterJS("~/js/jquery.Jcrop.js", scripts);
                    //RegisterJS("~/js/jquery.Jcrop.min.js", scripts);
                    //RegisterJS("~/js/jquery-ui-1.10.3.min.js", scripts);

                }



            }
            else
            {
                ContentPlaceHolder header = this.FindControl("header1") as ContentPlaceHolder;
                PlaceHolder scripts = this.FindControl("scripts") as PlaceHolder;
                //RegisterCSS("~/css/newcss.css?v=" + WebHelper.Version, header);
                //RegisterJS("~/theme/scripts/jquery-1.8.2.min.js", header);

                RegisterJS("~/scripts/common.js?v=" + WebHelper.Version, scripts);
                RegisterJS("~/js/jquery-1.11.1.min.js", header);
                RegisterJS("~/scripts/calendar/calendar.js?v=" + WebHelper.Version, scripts);
                RegisterJS("~/Scripts/Calendar/nepCal.js?v=" + WebHelper.Version, scripts);
                RegisterJS("~/Scripts/enCal.js?v=" + WebHelper.Version, scripts);
            }

            base.OnLoad(e);

        }

        void RegisterJS(string fileName, ContentPlaceHolder header1)
        {
            HtmlGenericControl Include = new HtmlGenericControl("script");
            Include.Attributes.Add("type", "text/javascript");
            Include.Attributes.Add("src", Page.ResolveUrl(fileName));
            header1.Controls.Add(Include);
        }
        public void RegisterCSS(string fileName, ContentPlaceHolder header1)
        {
            HtmlGenericControl Include = new HtmlGenericControl("link");
            Include.Attributes.Add("rel", "stylesheet");
            Include.Attributes.Add("type", "text/css");
            Include.Attributes.Add("href", Page.ResolveUrl(fileName));
            header1.Controls.Add(Include);
        }

        void RegisterJS(string fileName, PlaceHolder scripts)
        {
            HtmlGenericControl Include = new HtmlGenericControl("script");
            Include.Attributes.Add("type", "text/javascript");
            Include.Attributes.Add("src", Page.ResolveUrl(fileName));
            scripts.Controls.Add(Include);
        }

        public bool CheckForEngNepDatePref()
        {
            string url = Path.GetFileName(Request.PhysicalPath).ToLower();

            string[] urls = { "branchtransferlist.aspx", "departmenttransfernew.aspx", "locationtransfernew.aspx", "personaldetail.aspx" };

            foreach (var item in urls)
            {
                if (item.Trim().Equals(url.Trim()))
                {
                    return true;
                }
            }

            return false;
        }
    }
}
