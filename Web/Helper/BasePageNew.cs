﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL.Base
{
    public abstract class BasePageNew<T> : BasePage
    {
        /// <summary>
        /// Method to display or create appropriate object(LINQ) for passing to DAL
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="entity"></param>
        /// <returns></returns>
        public abstract T Process(T entity);

        /// <summary>
        /// To be called in Page_Load for initialise controls for first load
        /// </summary>
        public abstract void Initialise();


        /// <summary>
        /// Method to hide,disable control to be called from "Page_PreRender" 
        /// </summary>
        public abstract void EnableDisableControls();
    }
}
