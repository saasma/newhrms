﻿using DevExpress.DataAccess.ConnectionParameters;
using DevExpress.DataAccess.Sql;
using System.Configuration;
using System.Web.SessionState;
using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.IO;
using System.Web;
using BLL;
using DAL;
using System.Xml.Linq;
using DevExpress.DataAccess.Native.Sql;
using System.Data.Linq;
using System;
using System.Xml.Linq;
using DevExpress.DataAccess.Sql;
using DevExpress.XtraReports.Web;
using DevExpress.DataAccess.Native.Sql;
using Utils;
using System.Collections.Generic;
using BLL.Manager;

public class QueryBuilderHelper : BLL.BaseBiz {

    static readonly string ShowTooltipKey = "ShowTooltip";
    static readonly string SelectQueryKey = "SelectQuery";
    static readonly string SelectCommandKey = "SelectCommand";
    static readonly string DefaultSelectCommand =
@"";

    static SelectQuery GenerateDefaultQuery() {
        return SelectQueryFluentBuilder
            .AddTable("Suppliers")
            .SelectColumns("CompanyName", "ContactName", "City", "Country")
            .Join("Products", DevExpress.Xpo.DB.JoinType.LeftOuter, "SupplierID", "SupplierID")
            .SelectColumns("ProductName", "UnitPrice")
            .Build("Query1");
    }
    
    public static string NorthwindConnectionString {
        get {
            string sqlExpressString = ConfigurationManager.ConnectionStrings["PayrollMgmtConnectionString"].ConnectionString;
            return DevExpress.Internal.DbEngineDetector.PatchConnectionString(sqlExpressString);
        }
    }

    public static CustomStringConnectionParameters NorthwindConnectionParameters {
        get {
            return new CustomStringConnectionParameters(NorthwindConnectionString + ";XpoProvider=MSSqlServer");
        }
    }

    public static SelectQuery LoadQuery(HttpSessionState session) {
        return (session[SelectQueryKey] as SelectQuery) ?? GenerateDefaultQuery();
    }

    public static string LoadSelectCommand(HttpSessionState session) {
        return (session[SelectCommandKey] as string) ?? DefaultSelectCommand;
    }

    public static ReportItem GetReport(string name)
    {
        Reports report = GetReports();

        ReportItem item = null;

        foreach (var i1 in report.list)
        {
            if (i1.Name.ToLower().Equals(name.ToString().Trim().ToLower()))
            {
                return i1;
            }
        }

        return null;
    }


    //https://www.devexpress.com/Support/Center/Question/Details/T447797
    public static void SaveQuery(string selectCommand, SelectQuery query, HttpSessionState session,string queryid) 
    {



        using (DevExpress.DataAccess.Sql.SqlDataSource ds =
            new DevExpress.DataAccess.Sql.SqlDataSource("test"))
        {
            ds.Queries.Add(query);
            string serializedQuery = ds.SaveToXml().ToString();


            QueryBuilderList dbQuery = new QueryBuilderList();
            dbQuery.QueryID = Guid.NewGuid();

            if (!string.IsNullOrEmpty(queryid))
            {
                dbQuery = CommonManager.GetQueryBuilder(new System.Guid(queryid));
            }
            else
                PayrollDataContext.QueryBuilderLists.InsertOnSubmit(dbQuery);
            
            dbQuery.Name = query.ToString();
            dbQuery.SerializedSql = serializedQuery;
            dbQuery.Sql = selectCommand;
            
            PayrollDataContext.SubmitChanges();
        }

        //using (StringWriter writer = new StringWriter())
        //{

        //    XElement queryXML = QuerySerializer.SaveToXml(query, null);
        //    queryXML.Save(writer);


        //    QueryBuilderList dbQuery = new QueryBuilderList();
        //    dbQuery.QueryID = Guid.NewGuid();
        //    dbQuery.Name = query.ToString();
        //    dbQuery.SerializedSql = writer.ToString();

        //    PayrollDataContext.QueryBuilderLists.InsertOnSubmit(dbQuery);
        //    PayrollDataContext.SubmitChanges();
        //}
            

        
       // queryXML.Save(QueryXmlFile);


        //Reports report = GetReports();

        //ReportItem item = null;

        //foreach (var i1 in report.list)
        //{
        //    if (i1.Name.ToLower().Equals(query.ToString().Trim().ToLower()))
        //    {
        //        item = i1;
        //        break;
        //    }
        //}

        //if (item == null)
        //{
        //    item = new ReportItem();
           
        //    item.Name = query.ToString().Trim();
        //    report.list.Add(item);
        //}

        //item.SQL = selectCommand;

        //SaveReports(report);

        //session[SelectQueryKey] = query;
        //session[SelectCommandKey] = selectCommand;
    }

    public static void HideTooltip(HttpSessionState session) {
        session[ShowTooltipKey] = false;
    }
    public static bool NeedToShowTooltip(HttpSessionState session) {
        var showTooltipValue = session[ShowTooltipKey];
        return showTooltipValue is bool ? (bool)showTooltipValue : true;
    }


    public static Reports GetReports()
    {
        string xmlfile =  HttpContext.Current.Server.MapPath("~/app_data/Report.xml");
        XmlSerializer xml = new XmlSerializer(typeof(Reports));
        Reports recruiter = null;
        using (TextReader reader = new StreamReader(xmlfile))
        {
            recruiter = (Reports)xml.Deserialize(reader);
            reader.Close();
        }

        return recruiter;
    }


    public static void SaveReports(Reports reports)
    {
        string xmlfile = HttpContext.Current.Server.MapPath("~/app_data/Report.xml");

        XmlSerializer xml = new XmlSerializer(typeof(Reports));

        // Create a new file stream to write the serialized object to a file
        using (TextWriter WriteFileStream = new StreamWriter(xmlfile, false))
        {
            xml.Serialize(WriteFileStream, reports);

            // Cleanup
            WriteFileStream.Close();
        }
    }
}


[Serializable()]
public class Reports
{
    [XmlElement("Report")]
    public List<ReportItem> list = new List<ReportItem>();
}

[Serializable()]
//[XmlAttribute("Candidate")]
public class ReportItem
{
    public string Name { get; set; }
    public string SQL { get; set; }
}
