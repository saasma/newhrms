﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using System.Xml;

namespace Web.CP
{
    public partial class SettlementReportFinance : BasePage
    {
        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.HumanResource;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();
            }
        }

        public void btnLoad_Click(object sender, DirectEventArgs e)
        {
            LoadLevels();
        }
        public void Initialise()
        {

            //List<DAL.ApprovalFlow> list =
            //    TravelAllowanceManager.getTravelOrderList((int)FlowTypeEnum.TravelOrder);

            List<DAL.ApprovalFlow> list = new List<DAL.ApprovalFlow>();
                
            list.Insert(0, new DAL.ApprovalFlow { StepID = 3, StepName = "Settled" });
            list.Insert(0, new DAL.ApprovalFlow { StepID = -1, StepName = "All" });
            cmbStatus.SelectedItems.Add(new Ext.Net.ListItem { Index = 0 });

            txtStartDate.SelectedDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            txtEndDate.SelectedDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.DaysInMonth(DateTime.Now.Year,DateTime.Now.Month));

            cmbStatus.Store[0].DataSource = list;
            cmbStatus.Store[0].DataBind();
            cmbStatus.SetValue(list.First().StepID);

            List<Branch> branchList = new List<Branch>();
            branchList = ActivityManager.GetAllBranches();
            branchList.Insert(0, new Branch { BranchId = -1, Name = "All" });
            StoreBranchFilter.DataSource = branchList;
            StoreBranchFilter.DataBind();
            cmbBranch.SelectedItems.Add(new Ext.Net.ListItem { Index = 0 });

            LoadLevels();
        }
        
        private void LoadLevels()
        {
            int status = -1;
            int branch = -1;
            string employeeName;
            int a;
            DateTime? startDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            DateTime? endDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month));

            if (X.IsAjaxRequest)
            {
                if (cmbStatus.SelectedItem.Value != null && int.TryParse(cmbStatus.SelectedItem.Value,out  a))
                    status = int.Parse(cmbStatus.SelectedItem.Value);

                if (cmbBranch.SelectedItem.Value != null && int.TryParse(cmbBranch.SelectedItem.Value, out  a))
                    branch = int.Parse(cmbBranch.SelectedItem.Value);

                if (!string.IsNullOrEmpty(txtStartDate.Text))
                {
                    startDate = txtStartDate.SelectedDate;
                }
                if (!string.IsNullOrEmpty(txtEndDate.Text))
                {
                    endDate = txtEndDate.SelectedDate;
                }
            }
            
            
            employeeName = txtEmployeeName.Text.Trim();

            GridLevels.GetStore().DataSource = TravelAllowanceManager.GetAllTravelRequestForFinance(startDate, endDate, status, employeeName, branch);
            GridLevels.GetStore().DataBind();
        }


        
        public void ClearLevelFields()
        {
            hiddenValue.Text = "";
            //txtName.Text = "";
            //txtDescription.Text = "";
        }

      
        protected void btnEditLevel_Click(object sender, DirectEventArgs e)
        {

            TARequest request = new TARequest();
            request = TravelAllowanceManager.getRequestByID(int.Parse(hiddenValue.Text));

            TravelRequestCtl1.LoadTravelRequest(request);

            TravelRequestCtl1.HideButtonBlock();
        }


        
 
    }
}

