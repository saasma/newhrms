﻿<%@ Page Title="Advance Settlement Report" Language="C#" MasterPageFile="~/Master/NewDetails.Master"
    AutoEventWireup="true" CodeBehind="SettlementReportFinance.aspx.cs" Inherits="Web.CP.SettlementReportFinance" %>

<%@ Register Src="~/Employee/UserControls/TravelRequestCtl.ascx" TagName="TravelRequestCtl"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">


        
        var CommandHandler1 = function(command, record){
            <%= hiddenValue.ClientID %>.setValue(record.data.RequestID);
                if(command=="Edit")
                {
                    <%= btnEditLevel.ClientID %>.fireEvent('click');
                }
             }
             

             var totalCost = function (records) {
            var i = 0,
                length = records.length,
                total = 0,
                record;

            for (; i < length; ++i) {
                record = records[i];
                total += record.get('Quantity') * record.get('Rate');
            }

            return total;
        };
        
    </script>
    <style type="text/css">
        .hideLeftBlockCssInPage
        {
            margin: 0px !important;
            padding-left: 10px !important;
        }
        #menu
        {
            display: none;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <%--<ext:ResourceManager ID="resourceManager1" runat="server" />--%>
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Advance Settlement Report
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <ext:Hidden runat="server" ID="hiddenValue" />
        <ext:Hidden runat="server" ID="HiddenStatus">
        </ext:Hidden>
        <ext:LinkButton runat="server" Hidden="true" ID="btnEditLevel">
            <DirectEvents>
                <Click OnEvent="btnEditLevel_Click">
                    <EventMask ShowMask="true" />
                </Click>
            </DirectEvents>
        </ext:LinkButton>
        <div class="separator bottom">
        </div>
        <div>
            <table class="fieldTable firsttdskip">
                <tr>
                    <td>
                        <ext:ComboBox FieldLabel="Status" ID="cmbStatus" Width="180px" runat="server" ValueField="StepID"
                            DisplayField="StepName" LabelAlign="Top" LabelSeparator="" ForceSelection="true"
                            QueryMode="Local">
                            <Store>
                                <ext:Store ID="Store1" runat="server">
                                    <Model>
                                        <ext:Model ID="Model1" IDProperty="StepID" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="StepID" />
                                                <ext:ModelField Name="StepName" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                        </ext:ComboBox>
                    </td>
                    <td>
                        <ext:DateField FieldLabel="Start Date" ID="txtStartDate" Width="150" runat="server"
                            LabelAlign="Top" LabelSeparator="" ForceSelection="true">
                        </ext:DateField>
                    </td>
                    <td>
                        <ext:DateField FieldLabel="End Date" ID="txtEndDate" Width="150" runat="server" LabelAlign="Top"
                            LabelSeparator="" ForceSelection="true">
                        </ext:DateField>
                    </td>
                    <td>
                        <ext:TextField FieldLabel="Employee Name" ID="txtEmployeeName" Width="150" runat="server"
                            LabelAlign="Top" LabelSeparator="" ForceSelection="true">
                        </ext:TextField>
                    </td>
                    <td>
                        <ext:ComboBox runat="server" ID="cmbBranch" FieldLabel="Branch" Width="150" LabelAlign="Top"
                            LabelSeparator="" DisplayField="Name" ValueField="BranchId" ForceSelection="true">
                            <Store>
                                <ext:Store ID="StoreBranchFilter" runat="server">
                                    <Model>
                                        <ext:Model runat="server" IDProperty="BranchId">
                                            <Fields>
                                                <ext:ModelField Name="BranchId" Type="Int" />
                                                <ext:ModelField Name="Name" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                        </ext:ComboBox>
                    </td>
                    <td style="padding-top: 20px">
                        <ext:Button runat="server" ID="btnLoad" Cls="btn btn-default" StyleSpec="margin-top:10px"
                            Height="30" Text="<i></i>Search" runat="server">
                            <DirectEvents>
                                <Click OnEvent="btnLoad_Click">
                                    <EventMask ShowMask="true" />
                                </Click>
                            </DirectEvents>
                        </ext:Button>
                    </td>
                </tr>
            </table>
            <ext:GridPanel StyleSpec="margin-top:15px;" ID="GridLevels" runat="server" Height="500">
                <Store>
                    <ext:Store ID="Store3" runat="server">
                        <Model>
                            <ext:Model ID="Model4" runat="server" IDProperty="RequestID">
                                <Fields>
                                    <ext:ModelField Name="RequestID" Type="String" />
                                    <ext:ModelField Name="EmployeeId" Type="string" />
                                    <ext:ModelField Name="FromDate" Type="Date" />
                                    <ext:ModelField Name="EmpName" Type="string" />
                                    <ext:ModelField Name="Position" Type="string" />
                                    <ext:ModelField Name="Department" Type="string" />
                                    <ext:ModelField Name="Destination" Type="string" />
                                    <ext:ModelField Name="Reason" Type="string" />
                                    <ext:ModelField Name="Duration" Type="string" />
                                    <ext:ModelField Name="Status" Type="string" />
                                    <ext:ModelField Name="StatusName" Type="string" />
                                    <ext:ModelField Name="ExpensePaidByText" Type="string" />
                                    <ext:ModelField Name="TravelByText" Type="string" />
                                    <ext:ModelField Name="ToDate" Type="string" />
                                    <ext:ModelField Name="EligibleExpenses" Type="string" />
                                    <ext:ModelField Name="AdvanceAmount" Type="Float" />
                                    <ext:ModelField Name="AdvanceDate" Type="Date" />
                                    <ext:ModelField Name="EmpTotal" Type="Float" />
                                    <ext:ModelField Name="SettledTotal" Type="Float" />
                                    <ext:ModelField Name="ToBePaid" Type="String" />
                                    <ext:ModelField Name="ToBeRecieved" Type="String" />
                                    <ext:ModelField Name="LocationName" Type="string" />
                                    <ext:ModelField Name="AdvanceStatusModified" Type="string" />
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                <ColumnModel>
                    <Columns>
                        <ext:DateColumn ID="DateColumn1" runat="server" Text="From Date" Sortable="true"
                            DataIndex="FromDate" Width="86" Align="Left" Format="MM/dd/yyyy">
                        </ext:DateColumn>
                        <ext:DateColumn ID="DateColumn2" runat="server" Text="To Date" Sortable="true" DataIndex="ToDate"
                            Width="86" Align="Left" Format="MM/dd/yyyy">
                        </ext:DateColumn>
                        <ext:Column ID="Column15" Sortable="false" MenuDisabled="true" runat="server" Text="Name"
                            Width="125" Align="Left" DataIndex="EmpName" />
                        <ext:Column ID="Column5" Sortable="false" MenuDisabled="true" runat="server" Text="Destination"
                            Width="125" Align="Left" DataIndex="Destination" />
                        <ext:Column ID="Column9" Sortable="false" MenuDisabled="true" runat="server" Text="Location"
                            Width="150" Align="Left" DataIndex="LocationName" />
                        <ext:Column ID="Column6" Sortable="false" MenuDisabled="true" runat="server" Text="Reason"
                            Width="225" Align="Left" DataIndex="Reason" />
                        <ext:Column ID="Column7" Sortable="false" MenuDisabled="true" runat="server" Text="Duration"
                            Width="125" Align="Left" DataIndex="Duration" />
                        <ext:Column ID="Column1" Sortable="false" MenuDisabled="true" runat="server" Text="Expense Paid By"
                            Width="125" Align="Left" DataIndex="ExpensePaidByText" />
                        <ext:Column ID="Column2" Sortable="false" MenuDisabled="true" runat="server" Text="Travel By"
                            Width="125" Align="Left" DataIndex="TravelByText" />
                        <ext:Column ID="Column8" Sortable="false" MenuDisabled="true" runat="server" Text="Status"
                            Width="100" Align="Left" DataIndex="StatusName">
                        </ext:Column>
                        <ext:Column ID="Column3" Sortable="false" MenuDisabled="true" runat="server" Text="Eligible Expenses"
                            Width="100" Align="Left" DataIndex="EligibleExpenses">
                        </ext:Column>
                        <ext:Column ID="Column4" Sortable="false" MenuDisabled="true" runat="server" Text="Advance"
                            Width="100" Align="Left" DataIndex="AdvanceAmount">
                        </ext:Column>
                        <ext:DateColumn ID="DateColumn3" runat="server" Text="Advance Date" Sortable="true"
                            DataIndex="AdvanceDate" Width="86" Align="Left" Format="MM/dd/yyyy">
                        </ext:DateColumn>
                        <ext:Column ID="Column10" Sortable="false" MenuDisabled="true" runat="server" Text="Claimed Amount"
                            Width="100" Align="Left" DataIndex="EmpTotal">
                        </ext:Column>
                        <ext:Column ID="Column11" Sortable="false" MenuDisabled="true" runat="server" Text="Settled Total"
                            Width="100" Align="Left" DataIndex="SettledTotal">
                        </ext:Column>
                        <ext:Column ID="Column12" Sortable="false" MenuDisabled="true" runat="server" Text="To Be Recovered"
                            Width="100" Align="Left" DataIndex="ToBeRecieved">
                        </ext:Column>
                        <ext:Column ID="Column13" Sortable="false" MenuDisabled="true" runat="server" Text="To Be Paid"
                            Width="100" Align="Left" DataIndex="ToBePaid">
                        </ext:Column>
                        <ext:Column ID="Column14" Sortable="false" MenuDisabled="true" runat="server" Text="Settlement Status"
                            Width="100" Align="Left" DataIndex="AdvanceStatusModified">
                        </ext:Column>
                        <%--<ext:CommandColumn ID="CommandColumn1" runat="server" Align="Center" Width="40">
                            <Commands>
                                <ext:CommandSeparator />
                                <ext:GridCommand Text="<i></i>" ToolTip-Text="View" Cls="btn-action glyphicons pencil btn-success"
                                    CommandName="Edit" />
                            </Commands>
                            <Listeners>
                                <Command Handler="CommandHandler1(command,record);" />
                            </Listeners>
                        </ext:CommandColumn>--%>
                    </Columns>
                </ColumnModel>
                <SelectionModel>
                    <ext:RowSelectionModel ID="RowSelectionModel2" runat="server" Mode="Single" />
                </SelectionModel>
            </ext:GridPanel>
        </div>
    </div>
    <uc1:TravelRequestCtl ID="TravelRequestCtl1" runat="server" />
</asp:Content>
