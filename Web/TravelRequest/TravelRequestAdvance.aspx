﻿<%@ Page Title="Travel Advance" Language="C#" MasterPageFile="~/Master/NewDetails.Master"
    AutoEventWireup="true" CodeBehind="TravelRequestAdvance.aspx.cs" Inherits="Web.Appraisal.TravelRequestAdvance" %>

<%@ Register Src="~/Employee/UserControls/TravelRequestCtl.ascx" TagName="TravelRequestCtl"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">


        
        var CommandHandler1 = function(command, record){
            <%= hiddenValue.ClientID %>.setValue(record.data.RequestID);
                if(command=="Edit")
                {
                    <%= btnEditLevel.ClientID %>.fireEvent('click');
                }
             }
             


        var totalCost = function (records) {
            var i = 0,
                length = records.length,
                total = 0,
                record;

            for (; i < length; ++i) {
                record = records[i];
                total += record.get('Quantity') * record.get('Rate');
            }

            return total;
        };
        



        var ChangeTotal = function (records) {
            var i = 0,
                length = records.length,
                total = 0,
                record;

            for (; i < length; ++i) {
                record = records[i];
                if(record.get('AllowType')==1)
                {
                    total = record.get('ActualAmount');
                }
                else if(record.get('AllowType')==2)
                {
                    total = record.get('ApprovedAmout');
                }
                else total =0;
                //total += record.get('Quantity') * record.get('Rate');
            }
            

            return total;
        };

    var positionRenderer = function(value, metaData, record, rowIndex, colIndex, store) 
    {
        var r = value.toString();
        if(Ext.isEmpty(r)) 
        {
            return "";
        }

        if(r=="1")
            return "Actual";
        else if(r=="2")
            return "Approved";
        else
            return "Edit";
    };


    var beforeEditAmount = function (e1, e, e2, e3) {
        
        var AllowType = e.record.data.AllowType;
        if(AllowType!=3 && e.field=='Total')
        {
            e.cancel = true;
            return;
        }

    }
         var afterEditSettle = function()
        {
            var records = <%=GridSettlementFinal.ClientID %>.getStore();
             var i = 0,
                length = records.data.length,
                total = 0,
                record;

            for (; i < length; ++i) {
                record = records.data.items[i];
               
                 var col1Value = record.data.AllowType;

                if(col1Value ==1)
                {
                    record.data.Total = record.data.ActualAmount;
                }
                else if(col1Value ==2)
                {
                    record.data.Total = record.data.ApprovedAmout;
                }
                record.commit();
                total += Number(record.data.Total);
               
            }
            var advance;
            advance=  Number(<%=txtAdvanceAmount.ClientID %>.getValue().replaceAll(",",""));
            var payment;
            payment = Number((Number(total)-Number(advance)));
            
            //if(payment > 0)
            {
                payment = getFormattedAmount(payment);
            } 
          
            total = getFormattedAmount(total);
            <%=dispTotalSettle.ClientID %>.setValue(total);
            <%=txtPayment.ClientID %>.setValue(payment);
        }

        function DateTypeSelect()
        {
            var type = <%=cmbType.ClientID %>.getValue();

            if(type == -1)
            {
                <%=txtFromDate.ClientID %>.show();
                <%=txtToDate.ClientID %>.show();
            }
            else
            {
                <%=txtFromDate.ClientID %>.hide();
                <%=txtToDate.ClientID %>.hide();
            }
        }

        function searchList() {
             <%=GridLevels.ClientID %>.getStore().pageSize = <%=cmbPageSize.ClientID %>.getValue();
            <%=PagingToolbar1.ClientID %>.doRefresh();
        }

    </script>
    <style type="text/css">
        .hideLeftBlockCssInPage
        {
            margin: 0px !important;
            padding-left: 10px !important;
        }
        #menu
        {
            display: none;
        }
    </style>
    <style type="text/css">
        .innerLR
        {
            padding: 0px !important;
        }
        <%--#ctl00_ContentPlaceHolder_Main_txtAdvanceAmount-labelEl
        {
            margin-left:-25px;
        }--%>
              
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <div class="contentArea">
        <ext:Hidden runat="server" ID="hiddenValue" />
        <ext:Hidden runat="server" ID="Hidden_QuerryStringID">
        </ext:Hidden>
        <ext:LinkButton runat="server" Hidden="true" ID="btnEditLevel">
            <DirectEvents>
                <Click OnEvent="btnEditLevel_Click">
                    <EventMask ShowMask="true" />
                </Click>
            </DirectEvents>
        </ext:LinkButton>
        <ext:LinkButton runat="server" Hidden="true" ID="btnDetailLevel">
            <DirectEvents>
                <Click OnEvent="btnDetailLevel_Click">
                    <EventMask ShowMask="true" />
                </Click>
            </DirectEvents>
        </ext:LinkButton>
        <ext:Button ID="btnDeleteLevel" runat="server" Hidden="true">
            <DirectEvents>
                <Click OnEvent="btnDeleteLevel_Click">
                    <Confirmation ConfirmRequest="true" Message="Are you sure, you want to delete the Allowance settings?" />
                    <EventMask ShowMask="true" />
                </Click>
            </DirectEvents>
        </ext:Button>
        <div class="separator bottom">
        </div>
        <div class="pageheader">
            <div class="media">
                <div class="media-body">
                    <h4>
                        Travel Settlement
                    </h4>
                </div>
            </div>
        </div>
        <div class="contentpanel">
            <div class="innerLR">
                <table class="fieldTable firsttdskip">
                    <tr>
                        <td>
                            <ext:ComboBox FieldLabel="Status" ID="cmbStatus" Width="180px" runat="server" LabelAlign="Top"
                                LabelSeparator="" ForceSelection="true" QueryMode="Local">
                                <Items>
                                    <ext:ListItem Text="All" Value="-1" />
                                    <ext:ListItem Text="Advance Not Set" Value="0" />
                                    <ext:ListItem Text="Advance Set" Value="1" />
                                    <ext:ListItem Text="Employee Settled" Value="2" />
                                    <ext:ListItem Text="HR Settled" Value="3" />
                                </Items>
                                <SelectedItems>
                                    <ext:ListItem Index="0" />
                                </SelectedItems>
                            </ext:ComboBox>
                        </td>
                        <td>
                            <ext:ComboBox ID="cmbType" runat="server" Width="150" LabelWidth="40" FieldLabel="Show"
                                LabelAlign="Top" LabelSeparator="" ForceSelection="true" QueryMode="Local">
                                <Items>
                                    <ext:ListItem Text="All" Value="0" />
                                    <ext:ListItem Text="This Month" Value="1" />
                                    <ext:ListItem Text="Last Month" Value="2" />
                                    <ext:ListItem Text="This Year" Value="3" />
                                    <ext:ListItem Text="Date Range" Value="-1" />
                                </Items>
                                <SelectedItems>
                                    <ext:ListItem Text="This Month" Value="0" />
                                </SelectedItems>
                                <Listeners>
                                    <Select Fn="DateTypeSelect" />
                                </Listeners>
                            </ext:ComboBox>
                        </td>
                        <td>
                            <ext:DateField ID="txtFromDate" runat="server" FieldLabel="From Date" LabelSeparator=""
                                LabelAlign="Top" Width="150" Hidden="true" />
                        </td>
                        <td>
                            <ext:DateField ID="txtToDate" runat="server" FieldLabel="To Date" LabelSeparator=""
                                LabelAlign="Top" Width="150" Hidden="true" />
                        </td>
                        <td>
                            <ext:Store runat="server" ID="storeSearch" AutoLoad="false">
                                <Proxy>
                                    <ext:AjaxProxy Url="../Handler/EmpSearch.ashx">
                                        <ActionMethods Read="GET" />
                                        <Reader>
                                            <ext:JsonReader Root="plants" TotalProperty="total" />
                                        </Reader>
                                    </ext:AjaxProxy>
                                </Proxy>
                                <Model>
                                    <ext:Model ID="Model5" IDProperty="Name" Name="ItemLineModel" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="Name" Type="String" />
                                            <ext:ModelField Name="EmployeeId" Type="String" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                            <ext:ComboBox LabelSeparator="" ID="cmbEmpSearch" FieldLabel="Employee Search" EmptyText="Employee Search"
                                LabelWidth="80" LabelAlign="Top" runat="server" DisplayField="Name" ValueField="EmployeeId"
                                StoreID="storeSearch" TypeAhead="false" Width="200" PageSize="9999" HideBaseTrigger="true"
                                MinChars="1" TriggerAction="All" ForceSelection="false">
                                <ListConfig LoadingText="Searching..." MinWidth="200" StyleSpec="border-top:1px solid #98c0f4;">
                                    <ItemTpl ID="ItemTpl2" runat="server">
                                        <Html>
                                            <tpl>
                                                        <div class="search-item">
                                                                        <span>{Name}</span>  
                                                         </div>
					                            </tpl>
                                        </Html>
                                    </ItemTpl>
                                </ListConfig>
                                <Triggers>
                                    <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                                </Triggers>
                                <Listeners>
                                    <Select Handler="this.getTrigger(0).show();" />
                                    <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                    <TriggerClick Handler="if (index == 0) { 
                                                       this.clearValue(); 
                                                       this.getTrigger(0).hide();
                                                   }" />
                                </Listeners>
                            </ext:ComboBox>
                        </td>
                        <td>
                            <ext:TextField ID="txtDestination" Width="150" runat="server" EmptyText="Destination Search"
                                FieldLabel="Destination" LabelAlign="Top" LabelSeparator="">
                            </ext:TextField>
                        </td>
                        <td style="padding-top: 20px">
                            <ext:Button runat="server" ID="btnLoad" Cls="btn btn-default" StyleSpec="margin-top:10px"
                                Height="30" Text="<i></i>Load">
                                <Listeners>
                                    <Click Fn="searchList">
                                    </Click>
                                </Listeners>
                            </ext:Button>
                        </td>
                    </tr>
                </table>
                <div class="widget-body" style="width: auto;">
                    <ext:GridPanel StyleSpec="margin-top:15px;" ID="GridLevels" runat="server" Scroll="Both"
                        OverflowX="auto" Height="450" OnReadData="Store_ReadData">
                        <Store>
                            <ext:Store ID="storeTR" runat="server" AutoLoad="true" OnReadData="Store_ReadData"
                                RemotePaging="true" RemoteSort="true">
                                <Model>
                                    <ext:Model ID="Model4" runat="server" IDProperty="RequestID">
                                        <Fields>
                                            <ext:ModelField Name="RequestID" Type="String" />
                                            <ext:ModelField Name="EmployeeId" Type="string" />
                                            <ext:ModelField Name="FromDate" Type="Date" />
                                            <ext:ModelField Name="ToDate" Type="string" />
                                            <ext:ModelField Name="EmpName" Type="string" />
                                            <ext:ModelField Name="Position" Type="string" />
                                            <ext:ModelField Name="Department" Type="string" />
                                            <ext:ModelField Name="Destination" Type="string" />
                                            <ext:ModelField Name="Reason" Type="string" />
                                            <ext:ModelField Name="Duration" Type="string" />
                                            <ext:ModelField Name="Status" Type="string" />
                                            <ext:ModelField Name="AdvanceStatusText" Type="string" />
                                            <ext:ModelField Name="LocationName" Type="string" />
                                            <ext:ModelField Name="ApprovedAmount" Type="string" />
                                            <ext:ModelField Name="AdvanceAmount" Type="string" />
                                            <ext:ModelField Name="ClaimAmount" Type="string" />
                                            <ext:ModelField Name="SettledAmount" Type="string" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:DateColumn ID="DateColumn1" runat="server" Text="Date" Sortable="true" DataIndex="FromDate"
                                    Width="85" Align="Left" Format="MM/dd/yyyy">
                                </ext:DateColumn>
                                <ext:DateColumn ID="DateColumn2" runat="server" Text="To Date" Sortable="true" DataIndex="ToDate"
                                    Width="86" Align="Left" Format="MM/dd/yyyy">
                                </ext:DateColumn>
                                <ext:Column ID="Column10" Sortable="false" MenuDisabled="true" runat="server" Text="Employee Name"
                                    Width="150" Align="Left" DataIndex="EmpName" />
                                <ext:Column ID="Column3" Sortable="false" MenuDisabled="true" runat="server" Text="Position"
                                    Width="100" Align="Left" DataIndex="Position" />
                                <ext:Column ID="Column4" Sortable="false" MenuDisabled="true" runat="server" Text="Department"
                                    Width="100" Align="Left" DataIndex="Department" />
                                <ext:Column ID="Column5" Sortable="false" MenuDisabled="true" runat="server" Text="Destination"
                                    Width="100" Align="Left" DataIndex="Destination" />
                                <ext:Column ID="Column18" Sortable="false" MenuDisabled="true" runat="server" Text="Location"
                                    Width="120" Align="Left" DataIndex="LocationName" />
                                <ext:Column ID="Column7" Sortable="false" MenuDisabled="true" runat="server" Text="Duration"
                                    Width="120" Align="Left" DataIndex="Duration" />
                                <ext:Column ID="Column8" Sortable="false" MenuDisabled="true" runat="server" Text="Status"
                                    Width="120" Align="Left" DataIndex="AdvanceStatusText">
                                </ext:Column>
                                <ext:Column ID="Column6" Sortable="false" MenuDisabled="true" runat="server" Text="Approved"
                                    Width="90" Align="Right" DataIndex="ApprovedAmount">
                                     <Renderer Handler="getFormattedAmount" />
                                </ext:Column>
                                <ext:Column ID="Column19" Sortable="false" MenuDisabled="true" runat="server" Text="Advance"
                                    Width="90" Align="Right" DataIndex="AdvanceAmount">
                                     <Renderer Handler="getFormattedAmount" />
                                </ext:Column>
                                <ext:Column ID="Column21" Sortable="false" MenuDisabled="true" runat="server" Text="Claimed"
                                    Width="90" Align="Right" DataIndex="ClaimAmount">
                                     <Renderer Handler="getFormattedAmount" />
                                </ext:Column>
                                <ext:Column ID="Column22" Sortable="false" MenuDisabled="true" runat="server" Text="Settled"
                                    Width="90" Align="Right" DataIndex="SettledAmount">
                                    <Renderer Handler="getFormattedAmount" />
                                </ext:Column>
                                <ext:CommandColumn runat="server" Align="Center" Width="80">
                                    <Commands>
                                        <ext:CommandSeparator />
                                        <ext:GridCommand Cls="editGridButton" ToolTip-Text="Edit" Text="<i class='fa fa-pencil'></i>"
                                            CommandName="Edit" />
                                    </Commands>
                                    <Listeners>
                                        <Command Handler="CommandHandler1(command,record);" />
                                    </Listeners>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <SelectionModel>
                            <ext:RowSelectionModel ID="RowSelectionModel2" runat="server" Mode="Single" />
                        </SelectionModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar1" runat="server" StoreID="storeTR" DisplayInfo="true">
                                <Items>
                                    <ext:Label ID="lblPageSize" runat="server" Text="Page size:" />
                                    <ext:ToolbarSpacer ID="ToolbarSpacer_listItems" runat="server" Width="10" />
                                    <ext:ComboBox ID="cmbPageSize" runat="server" Width="80" SelectOnFocus="true" Selectable="true"
                                        ValueField="Value" DisplayField="Text" ForceSelection="true" AllowBlank="false">
                                        <Listeners>
                                            <Select Handler="searchList()" />
                                            <%--#{cmbPageSize}.pageSize = this.getValue();#{PagingToolbar1}.moveFirst();--%>
                                        </Listeners>
                                        <Items>
                                            <ext:ListItem Value="20" Text="20" />
                                            <ext:ListItem Value="50" Text="50" />
                                            <ext:ListItem Value="100" Text="100" />
                                        </Items>
                                        <SelectedItems>
                                            <ext:ListItem Index="0">
                                            </ext:ListItem>
                                        </SelectedItems>
                                    </ext:ComboBox>
                                </Items>
                            </ext:PagingToolbar>
                        </BottomBar>
                    </ext:GridPanel>
                </div>
            </div>
        </div>
        <div style="float: right; padding-right: 30px; padding-bottom: 20px;">
            <ext:Button runat="server" AutoPostBack="true" OnClick="btnExportTR_Click" ID="btnExportTR"
                Text="<i></i>Export To Excel">
            </ext:Button>
        </div>
        <uc1:TravelRequestCtl ID="TravelRequestCtl1" runat="server" />
        <ext:Window ID="Window1" runat="server" AutoScroll="true" Title="Travel Allowance Settlement"
            Icon="Application" Width="1150" Height="670" BodyPadding="5" Hidden="true" Modal="true">
            <Content>
                <div style="margin-bottom: 10px;">
                    <div style="float: right; width: 90px">
                        <ext:LinkButton ID="btnPrint" runat="server" Text="Print" OnClick="btnExport_Click"
                            Icon="Printer" AutoPostBack="true">
                        </ext:LinkButton>
                    </div>
                    <div style="width: 400px; float: left">
                        <table id="albums1" width="400px;">
                            <tr>
                                <td>
                                    <table>
                                        <tr>
                                            <td valign="top">
                                                <ext:Image ID="image" ImageUrl="~/images/sample.jpg" runat="server" Width="120px"
                                                    Height="120px" />
                                            </td>
                                            <td valign="top" style="padding-left: 10px">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <ext:Label ID="lblName" StyleSpec="font-weight:bold;color:#0099FF;font-size:16px;"
                                                                runat="server">
                                                            </ext:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <ext:Label ID="lblBranch" runat="server">
                                                            </ext:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <ext:Label ID="lblDepartment" runat="server">
                                                            </ext:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <ext:Label ID="lblDesignation" runat="server">
                                                            </ext:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div style="width: 500px; float: left; padding-left: 25px; margin-bottom: 15px;">
                        <table id="albums" cellspacing="0px">
                            <tr style="padding-bottom: 1em;">
                                <td colspan="1" style="margin-right: 25px; margin-left: 25px;">
                                    <ext:TextField LabelSeparator="" ID="txtPlaceToTravelSett" Width="200" runat="server"
                                        FieldLabel="Place to Travel" ValidationGroup="InsertUpdateSettlements" ReadOnly="true"
                                        LabelAlign="Top">
                                    </ext:TextField>
                                </td>
                                <td colspan="1" style="padding-left: 25px;">
                                    <ext:ComboBox LabelSeparator="" ID="cmbCountrySett" runat="server" Width="200" FieldLabel="Country"
                                        LabelAlign="Top" ReadOnly="true" QueryMode="Local" DisplayField="CountryName"
                                        ValueField="CountryId">
                                        <Store>
                                            <ext:Store ID="store2" runat="server">
                                                <Fields>
                                                    <ext:ModelField Name="CountryId" Type="String" />
                                                    <ext:ModelField Name="CountryName" />
                                                </Fields>
                                            </ext:Store>
                                        </Store>
                                        <%--<DirectEvents>
                                        <Change OnEvent="Country_Change">
                                        </Change>
                                    </DirectEvents>--%>
                                    </ext:ComboBox>
                                </td>
                            </tr>
                            <tr style="padding-bottom: 1em;">
                                <td colspan="5">
                                    <ext:TextArea LabelSeparator="" ID="txtPurposeOfTravelSett" Width="550" runat="server"
                                        Height="50" FieldLabel="Purpose of Travel" ReadOnly="true" LabelAlign="Top">
                                    </ext:TextArea>
                                </td>
                            </tr>
                            <tr style="margin-bottom: 1em;">
                                <td colspan="1">
                                    <ext:DateField FieldLabel="Travelling from" Width="150" ID="txtFromDateSett" runat="server"
                                        EmptyDate="" LabelAlign="Top" LabelSeparator="" ReadOnly="true">
                                        <DirectEvents>
                                            <Change OnEvent="CheckInOutSelectChange">
                                            </Change>
                                        </DirectEvents>
                                    </ext:DateField>
                                </td>
                                <td colspan="1">
                                    <ext:DateField FieldLabel="Travelling to" Width="150" ID="txtToDateSett" runat="server"
                                        LabelAlign="Top" EmptyDate="" LabelSeparator="" ReadOnly="true">
                                        <DirectEvents>
                                            <Change OnEvent="CheckInOutSelectChange">
                                            </Change>
                                        </DirectEvents>
                                    </ext:DateField>
                                </td>
                                <td colspan="1" style="padding-top: 5px;">
                                    <ext:Label ID="TextArea4" Width="30" runat="server" Style="margin-right: 25px; margin-left: -35px;"
                                        Text="=">
                                    </ext:Label>
                                </td>
                                <td colspan="1" style="padding-right: 7px;">
                                    <ext:NumberField LabelSeparator="" ID="txtDayCountSett" runat="server" Width="60"
                                        FieldLabel="Days" LabelAlign="Top" ReadOnly="true">
                                    </ext:NumberField>
                                </td>
                                <td colspan="1">
                                    <ext:NumberField LabelSeparator="" ID="txtNightCountSett" runat="server" Width="60"
                                        FieldLabel="Night" LabelAlign="Top" ReadOnly="true">
                                    </ext:NumberField>
                                </td>
                            </tr>
                            <tr style="margin-bottom: 7px;">
                                <td colspan="1">
                                    <ext:ComboBox LabelSeparator="" ID="cmbTravelBySett" runat="server" FieldLabel="Travel by"
                                        LabelAlign="Top" QueryMode="Local" DisplayField="Name" ValueField="Value" ReadOnly="true">
                                        <Store>
                                            <ext:Store ID="storeTravelBy" runat="server">
                                                <Fields>
                                                    <ext:ModelField Name="Name" />
                                                    <ext:ModelField Name="Value" Type="String" />
                                                </Fields>
                                            </ext:Store>
                                        </Store>
                                    </ext:ComboBox>
                                </td>
                                <td colspan="1">
                                    <ext:ComboBox LabelSeparator="" ID="cmbExpensePaidBySett" runat="server" FieldLabel="Expense Paid by"
                                        LabelAlign="Top" QueryMode="Local" DisplayField="Name" ValueField="Value" ReadOnly="true">
                                        <Store>
                                            <ext:Store ID="storeExpensePaidBy" runat="server">
                                                <Fields>
                                                    <ext:ModelField Name="Name" />
                                                    <ext:ModelField Name="Value" Type="String" />
                                                </Fields>
                                            </ext:Store>
                                        </Store>
                                    </ext:ComboBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <ext:ComboBox ID="cmbLocations" ForceSelection="true" runat="server" Width="150"
                                        FieldLabel="Location for Allowance *" LabelAlign="Top" LabelSeparator=" " QueryMode="Local"
                                        DisplayField="LocationName" ValueField="LocationId" ReadOnly="true">
                                        <Store>
                                            <ext:Store ID="store1" runat="server">
                                                <Fields>
                                                    <ext:ModelField Name="LocationId" Type="String" />
                                                    <ext:ModelField Name="LocationName" />
                                                </Fields>
                                            </ext:Store>
                                        </Store>
                                        <DirectEvents>
                                            <Select OnEvent="cmbLocations_Change">
                                                <EventMask ShowMask="true" />
                                                <ExtraParams>
                                                    <ext:Parameter Name="Values" Value="Ext.encode(#{gridAllowances}.getRowsValues({selectedOnly : false}))"
                                                        Mode="Raw" />
                                                </ExtraParams>
                                            </Select>
                                        </DirectEvents>
                                    </ext:ComboBox>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <table style="margin-top: 10px; padding-top: 10px; clear: both;">
                    <tr>
                        <td>
                            <div>
                                <span style="color: Blue">Approved</span>
                                <ext:GridPanel StyleSpec="margin-top:15px;" ID="gridAllowances" runat="server" Width="390"
                                    Height="170" Style="color: #f7f7f7" Cls="itemgrid">
                                    <Store>
                                        <ext:Store ID="storeAllowances" runat="server">
                                            <Model>
                                                <ext:Model ID="Model1" runat="server">
                                                    <Fields>
                                                        <ext:ModelField Name="LineId" Type="String" />
                                                        <ext:ModelField Name="RequestId" Type="string" />
                                                        <ext:ModelField Name="AllowanceId" Type="string" />
                                                        <ext:ModelField Name="AllowanceName" Type="string" />
                                                        <ext:ModelField Name="Quantity" Type="Float" />
                                                        <ext:ModelField Name="Rate" Type="Float" />
                                                        <ext:ModelField Name="Total" Type="Float" />
                                                    </Fields>
                                                </ext:Model>
                                            </Model>
                                        </ext:Store>
                                    </Store>
                                    <Plugins>
                                        <ext:CellEditing ID="CellEditing1" ClicksToEdit="1" runat="server">
                                            <Listeners>
                                            </Listeners>
                                        </ext:CellEditing>
                                    </Plugins>
                                    <ColumnModel>
                                        <Columns>
                                            <ext:Column ID="Column1" Sortable="false" MenuDisabled="true" runat="server" Text="Allowances"
                                                Align="Left" DataIndex="AllowanceName" Width="140" />
                                            <ext:Column ID="Column2" Sortable="false" MenuDisabled="true" runat="server" Text="Qty"
                                                Align="Left" DataIndex="Quantity" Width="50">
                                            </ext:Column>
                                            <ext:Column ID="Column9" Sortable="false" MenuDisabled="true" runat="server" Text="Rate"
                                                Align="Right" DataIndex="Rate" Width="100">
                                                <Renderer Fn="getFormattedAmount" />
                                            </ext:Column>
                                            <ext:Column runat="server" Align="Right" Width="100" ID="Total" Text="Total" Sortable="false"
                                                Groupable="false" DataIndex="Total" CustomSummaryType="totalCost">
                                                <Renderer Handler="return Ext.util.Format.number((record.data.Quantity * record.data.Rate),'0,000.00');" />
                                            </ext:Column>
                                        </Columns>
                                    </ColumnModel>
                                    <SelectionModel>
                                        <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" Mode="Single" />
                                    </SelectionModel>
                                </ext:GridPanel>
                            </div>
                        </td>
                        <td>
                            <div style="margin-left: 7px;">
                                <span style="color: Blue">Actual</span>
                                <ext:GridPanel ID="GridPanel1" runat="server" Width="335" Height="170" Cls="itemgrid">
                                    <Store>
                                        <ext:Store ID="storeSettlement" runat="server">
                                            <Model>
                                                <ext:Model ID="Model2" runat="server">
                                                    <Fields>
                                                        <ext:ModelField Name="LineId" Type="String" />
                                                        <ext:ModelField Name="RequestId" Type="string" />
                                                        <ext:ModelField Name="AllowanceId" Type="string" />
                                                        <ext:ModelField Name="AllowanceName" Type="string" />
                                                        <ext:ModelField Name="FromDate" Type="string" />
                                                        <ext:ModelField Name="ToDate" Type="string" />
                                                        <ext:ModelField Name="Quantity" Type="Float" />
                                                        <ext:ModelField Name="Rate" Type="Float" />
                                                        <ext:ModelField Name="Total" Type="Float" />
                                                    </Fields>
                                                </ext:Model>
                                            </Model>
                                        </ext:Store>
                                    </Store>
                                    <ColumnModel>
                                        <Columns>
                                            <ext:Column ID="Column11" Sortable="false" MenuDisabled="true" runat="server" Text="Allowances"
                                                Align="Left" DataIndex="AllowanceName" Visible="false" Width="85" />
                                            <ext:Column ID="Column15" Sortable="false" MenuDisabled="true" runat="server" Text="From Date"
                                                Align="Left" DataIndex="FromDate" Width="90" />
                                            <ext:Column ID="Column16" Sortable="false" MenuDisabled="true" runat="server" Text="To Date"
                                                Align="Left" DataIndex="ToDate" Width="90" />
                                            <ext:Column ID="Column12" Sortable="false" MenuDisabled="true" runat="server" Text="Qty"
                                                Align="Left" DataIndex="Quantity" Width="50">
                                            </ext:Column>
                                            <ext:Column ID="Column13" Sortable="false" Visible="false" MenuDisabled="true" runat="server"
                                                Text="Rate" Align="Left" DataIndex="Rate" Width="85">
                                            </ext:Column>
                                            <ext:Column runat="server" Width="100" ID="Column14" Align="Right" Text="Total" Sortable="false"
                                                Groupable="false" DataIndex="Total" CustomSummaryType="totalCost">
                                                <Renderer Handler="return Ext.util.Format.number((record.data.Quantity * record.data.Rate),'0,000.00');" />
                                            </ext:Column>
                                        </Columns>
                                    </ColumnModel>
                                </ext:GridPanel>
                            </div>
                        </td>
                        <td>
                            <div style="margin-left: 7px;">
                                <span style="color: Blue">Final Settlement</span>
                                <ext:GridPanel StyleSpec="margin-top:15px;" ID="GridSettlementFinal" runat="server"
                                    Width="320" Height="170" Style="color: #f7f7f7" Cls="itemgrid">
                                    <Store>
                                        <ext:Store ID="storeSettlementFinal" runat="server">
                                            <Model>
                                                <ext:Model ID="Model3" runat="server">
                                                    <Fields>
                                                        <ext:ModelField Name="LineId" Type="String" />
                                                        <ext:ModelField Name="RequestId" Type="string" />
                                                        <ext:ModelField Name="AllowanceId" Type="string" />
                                                        <ext:ModelField Name="AllowanceName" Type="string" />
                                                        <ext:ModelField Name="Quantity" Type="Float" />
                                                        <ext:ModelField Name="Rate" Type="Float" />
                                                        <ext:ModelField Name="Total" Type="Float" />
                                                        <ext:ModelField Name="AllowType" Type="Int" />
                                                        <ext:ModelField Name="ActualAmount" Type="Float" />
                                                        <ext:ModelField Name="ApprovedAmout" Type="Float" />
                                                    </Fields>
                                                </ext:Model>
                                            </Model>
                                        </ext:Store>
                                    </Store>
                                    <Plugins>
                                        <ext:CellEditing ID="CellEditing3" ClicksToEdit="1" runat="server">
                                            <Listeners>
                                                <BeforeEdit Fn="beforeEditAmount" />
                                                <Edit Fn="afterEditSettle" />
                                            </Listeners>
                                        </ext:CellEditing>
                                    </Plugins>
                                    <ColumnModel>
                                        <Columns>
                                            <ext:Column runat="server" Width="200" ID="Column17" Text="Allow" Sortable="false"
                                                Groupable="false" DataIndex="AllowType">
                                                <Renderer Fn="positionRenderer" />
                                                <Editor>
                                                    <ext:ComboBox ID="ComboBox1" runat="server" QueryMode="Local">
                                                        <Items>
                                                            <ext:ListItem Text="Actual" Value="1" Mode="Raw" />
                                                            <ext:ListItem Text="Approved" Value="2" Mode="Raw" />
                                                            <ext:ListItem Text="Edit" Value="3" Mode="Raw" />
                                                        </Items>
                                                    </ext:ComboBox>
                                                </Editor>
                                            </ext:Column>
                                            <ext:Column runat="server" Width="120" Align="Right" ID="Column20" Text="Total" Sortable="false"
                                                Groupable="false" DataIndex="Total">
                                                <Renderer Handler="return Ext.util.Format.number((record.data.Total),'0.00');" />
                                                <%--<Renderer Fn="ChangeTotal(record.data.ApprovedAmout)" />--%>
                                                <Editor>
                                                    <ext:NumberField ID="txtTotal" SelectOnFocus="true" runat="server" AllowBlank="false" />
                                                </Editor>
                                            </ext:Column>
                                            <%--<ext:Column runat="server" Width="50" ID="Column18" Text="Act" Sortable="false" Groupable="false"  DataIndex="ActualAmount">
                                             </ext:Column>

                                             <ext:Column runat="server" Width="50" ID="Column19" Text="App" Sortable="false" Groupable="false"  DataIndex="ApprovedAmout">
                                             </ext:Column>--%>
                                        </Columns>
                                    </ColumnModel>
                                    <SelectionModel>
                                        <ext:RowSelectionModel ID="RowSelectionModel3" runat="server" Mode="Single" />
                                    </SelectionModel>
                                </ext:GridPanel>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div style="float: right; padding-right: 5px;">
                                <ext:DisplayField ID="dispTotalApproved" Text="0" runat="server" FieldLabel="Total "
                                    LabelAlign="Left" PaddingSpec="0 0 0 25">
                                </ext:DisplayField>
                            </div>
                        </td>
                        <td>
                            <div style="float: right; padding-right: 5px;">
                                <ext:DisplayField ID="dispTotal" Text="0" runat="server" FieldLabel="Total " LabelAlign="Left"
                                    PaddingSpec="0 0 0 25">
                                </ext:DisplayField>
                            </div>
                        </td>
                        <td>
                            <div style="float: right; padding-right: 5px;">
                                <ext:DisplayField ID="dispTotalSettle" Text="0" runat="server" FieldLabel="Total "
                                    LabelAlign="Left" PaddingSpec="0 0 0 25" LabelWidth="200">
                                </ext:DisplayField>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                            <div style="float: right; padding-right: 5px;">
                                <ext:DisplayField ID="txtAdvanceAmount" Text="0" runat="server" FieldLabel="Advance "
                                    LabelAlign="Left" PaddingSpec="0 0 0 25" LabelWidth="200">
                                </ext:DisplayField>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                            <div style="float: right; padding-right: 5px;">
                                <ext:DisplayField ID="txtPayment" Text="0" runat="server" FieldLabel="Amount to be received (paid) "
                                    LabelAlign="Left" PaddingSpec="0 0 0 25" LabelWidth="200">
                                </ext:DisplayField>
                            </div>
                        </td>
                    </tr>
                </table>
                <table>
                    <tr>
                        <td style="width: 90px;">
                            <ext:Button ID="btnSaveTR" Cls="btn btn-primary" Width="90" runat="server" StyleSpec=" float:right; margin-right:40px;"
                                Text="&nbsp;&nbsp;Save&nbsp;&nbsp;" ValidationGroup="InsertUpdateSettlements">
                                <DirectEvents>
                                    <Click OnEvent="btnSaveTR_Click">
                                        <Confirmation ConfirmRequest="true" Message="Are you sure, you want to save the Travel Request?" />
                                        <EventMask ShowMask="true" />
                                        <ExtraParams>
                                            <ext:Parameter Name="AllowanceValuesActual" Value="Ext.encode(#{GridPanel1}.getRowsValues({selectedOnly : false}))"
                                                Mode="Raw" />
                                            <ext:Parameter Name="AllowanceValuesApproved" Value="Ext.encode(#{gridAllowances}.getRowsValues({selectedOnly : false}))"
                                                Mode="Raw" />
                                            <ext:Parameter Name="AllowanceValuesSettled" Value="Ext.encode(#{GridSettlementFinal}.getRowsValues({selectedOnly : false}))"
                                                Mode="Raw" />
                                        </ExtraParams>
                                    </Click>
                                </DirectEvents>
                                <Listeners>
                                    <Click Handler="valGroup = 'InsertUpdateSettlements'; return CheckValidation();">
                                    </Click>
                                </Listeners>
                            </ext:Button>
                        </td>
                        <td>
                            <ext:Button ID="btnSaveAndLaterSett" Cls="btn btn-primary" Width="150" runat="server"
                                StyleSpec=" float:right; margin-right:40px;" Text="&nbsp;&nbsp;Settle&nbsp;&nbsp;"
                                ValidationGroup="InsertUpdateSettlements">
                                <DirectEvents>
                                    <Click OnEvent="ButtonNextSett_Click">
                                        <Confirmation ConfirmRequest="true" Message="Are you sure, you want to settle the Travel Request?" />
                                        <EventMask ShowMask="true" />
                                        <ExtraParams>
                                            <ext:Parameter Name="AllowanceValuesActual" Value="Ext.encode(#{GridPanel1}.getRowsValues({selectedOnly : false}))"
                                                Mode="Raw" />
                                            <ext:Parameter Name="AllowanceValuesApproved" Value="Ext.encode(#{gridAllowances}.getRowsValues({selectedOnly : false}))"
                                                Mode="Raw" />
                                            <ext:Parameter Name="AllowanceValuesSettled" Value="Ext.encode(#{GridSettlementFinal}.getRowsValues({selectedOnly : false}))"
                                                Mode="Raw" />
                                        </ExtraParams>
                                    </Click>
                                </DirectEvents>
                                <Listeners>
                                    <Click Handler="valGroup = 'InsertUpdateSettlements'; return CheckValidation();">
                                    </Click>
                                </Listeners>
                            </ext:Button>
                        </td>
                    </tr>
                </table>
            </Content>
        </ext:Window>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtPlaceToTravelSett"
            ErrorMessage="Place to travel is required." Display="None" ValidationGroup="InsertUpdateSettlements">
        </asp:RequiredFieldValidator>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="cmbCountrySett"
            ErrorMessage="Country is required" Display="None" ValidationGroup="InsertUpdateSettlements">
        </asp:RequiredFieldValidator>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="cmbLocations"
            ErrorMessage="Location is required" Display="None" ValidationGroup="InsertUpdateSettlements">
        </asp:RequiredFieldValidator>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtPurposeOfTravelSett"
            ErrorMessage="Purpose of Travel is required" E Display="None" ValidationGroup="InsertUpdateSettlements">
        </asp:RequiredFieldValidator>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtFromDateSett"
            ErrorMessage="From Date is required" Display="None" ValidationGroup="InsertUpdateSettlements">
        </asp:RequiredFieldValidator>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="txtToDateSett"
            ErrorMessage="To Date is required" Display="None" ValidationGroup="InsertUpdateSettlements">
        </asp:RequiredFieldValidator>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="cmbTravelBySett"
            ErrorMessage="Travel is required" Display="None" ValidationGroup="InsertUpdateSettlements">
        </asp:RequiredFieldValidator>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ControlToValidate="cmbExpensePaidBySett"
            ErrorMessage="Expense Paid by is required" Display="None" ValidationGroup="InsertUpdateSettlements">
        </asp:RequiredFieldValidator>
    </div>
</asp:Content>
