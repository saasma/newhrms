﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using System.Xml;
using Utils;
using System.IO;
using Web.CP.Report.Templates.HR;

namespace Web.Appraisal
{
    public partial class TravelRequestAdvance : BasePage
    {
        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.HumanResource;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();
            }
        }

        

        public void Initialise()
        {

            cmbLocations.Store[0].DataSource = TravelAllowanceManager.GetLocationList().OrderBy(x => x.LocationName).ToList();
            cmbLocations.Store[0].DataBind();

            CommonManager comManager = new CommonManager();
            cmbCountrySett.Store[0].DataSource = comManager.GetAllCountries();
            cmbCountrySett.Store[0].DataBind();

            cmbTravelBySett.Store[0].DataSource = TravelAllowanceManager.getTravelByList();
            cmbTravelBySett.Store[0].DataBind();

            cmbExpensePaidBySett.Store[0].DataSource = TravelAllowanceManager.getExpensePaidByList();
            cmbExpensePaidBySett.Store[0].DataBind();

            string status = Request.QueryString["status"];
            if (!string.IsNullOrEmpty(status))
            {
                ExtControlHelper.ComboBoxSetSelected(status, cmbStatus);
            }

            //LoadLevels();
            X.Js.Call("searchList");
        }
        
        private void LoadLevels()
        {

            

        }
        

        
        public void ClearLevelFields()
        {
            hiddenValue.Text = "";
            //txtName.Text = "";
            //txtDescription.Text = "";
        }

      
        
        protected void btnAddLevel_Click(object sender, DirectEventArgs e)
        {
            
            ClearLevelFields();
            //WindowLevel.Show();
        }


        protected void btnDeleteLevel_Click(object sender, DirectEventArgs e)
        {
            int TAllowance = int.Parse(hiddenValue.Text.Trim());
            Status status = TravelAllowanceManager.DeleteTravelAllowance(TAllowance);
            if (status.IsSuccess)
            {
                //LoadLevels();
                X.Js.Call("searchList");
                NewMessage.ShowNormalMessage("Travel Allowance deleted.");
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }
        }

        

        protected void btnEditLevel_Click(object sender, DirectEventArgs e)
        {

            TARequest request = new TARequest();
            request = TravelAllowanceManager.getRequestByID(int.Parse(hiddenValue.Text));

            
            if (request.AdvanceStatus == null || request.AdvanceStatus == (int)AdvanceStatusEnum.HRAdvance)
            {
                TravelRequestCtl1.LoadTravelRequest(request);
                TravelRequestCtl1.SetForAdvance();
            }
            else
            {
                if (request.AdvanceStatus == (int)AdvanceStatusEnum.EmployeeSettle || request.AdvanceStatus == (int)AdvanceStatusEnum.HRSettle)
                {
                    LoadEmployeeInfo(request.EmployeeId.Value);
                    LoadEditData(request);
                    //LoadAllowanceGrid(request.LocationId.Value, request.RequestID, request);
                    Window1.Show();
                }
            }
        }


        protected void LoadEmployeeInfo(int EmployeeID)
        {
            EEmployee eemployee = EmployeeManager.GetEmployeeById(EmployeeID);
            lblName.Text = eemployee.Name;
            lblDepartment.Text = eemployee.Department.Name;
            lblDesignation.Text = eemployee.EDesignation.Name;
            lblBranch.Text = eemployee.Branch.Name;

            bool hasPhoto = false;
            if (eemployee.HHumanResources.Count > 0)
            {
                if (!string.IsNullOrEmpty(eemployee.HHumanResources[0].UrlPhotoThumbnail))
                {
                    image.ImageUrl = Page.ResolveUrl(Config.UploadLocation + "/" + eemployee.HHumanResources[0].UrlPhotoThumbnail);
                    if (File.Exists(Server.MapPath(image.ImageUrl)))
                        hasPhoto = true;
                }
            }
            if (!hasPhoto)
            {
                image.ImageUrl = Page.ResolveUrl(Web.Helper.WebHelper.GetEmployeeNoImage(eemployee.EmployeeId));

            }

        }
               

        protected void btnExport_Click(object sender, EventArgs e)
        {
            CP.Report.ReportHelper.TravelRequestPrint(int.Parse(hiddenValue.Text), txtFromDate.SelectedDate.ToShortDateString(), txtToDate.SelectedDate.ToShortDateString());
            //Export();
        }
        protected void Export()
        {
            bool isPDF = true;
           
            ReportTravellAllowanceSettlement report = GetTravellAllowanceSettlementReport();
            
            using (MemoryStream stream = new MemoryStream())
            {
                if (isPDF)
                    report.ExportToPdf(stream);
                else
                    report.ExportToXls(stream);

                HttpContext.Current.Response.Clear();
                if (isPDF)
                {

                    HttpContext.Current.Response.ContentType = "application/pdf";
                    HttpContext.Current.Response.AppendHeader("Content-Type", "application/pdf");

                    HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment; filename=\"" + report.labelTitle.Text + " Statement.pdf\"");

                }
                else
                {

                    report.ExportToXls(stream);
                    HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";
                    HttpContext.Current.Response.AppendHeader("Content-Type", "application/vnd.ms-excel");

                    HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment; filename=\"" + report.labelTitle.Text + " Statement.xls\"");
                }

                HttpContext.Current.Response.AddHeader("Content-Length", stream.ToArray().Length.ToString());

                HttpContext.Current.Response.OutputStream.Write(stream.ToArray(), 0, stream.ToArray().Length);
                HttpContext.Current.Response.End();
            }
        }




        

        private ReportTravellAllowanceSettlement GetTravellAllowanceSettlementReport()
        {

            //List<GetPAREmployeeHeaderResult> data = EmployeeManager.GetPAREmployeeHeader(hdnEmployeeID.Value.ToString()).ToList();


            TARequest request = new TARequest();
            request = TravelAllowanceManager.getRequestByID(int.Parse(hiddenValue.Text));

            
   

            ReportDataSet.Report_TravellAllowanceSettlementHeaderInfoDataTable dtHeader = new ReportDataSet.Report_TravellAllowanceSettlementHeaderInfoDataTable();
            ReportDataSet.Report_TravellAllowanceSettlementPhaseIDataTable dtTravellAllowancePhaseI = new ReportDataSet.Report_TravellAllowanceSettlementPhaseIDataTable();
            ReportDataSet.Report_TravellAllowanceSettlementPhaseIIDataTable dtTravellAllowancePhaseII = new ReportDataSet.Report_TravellAllowanceSettlementPhaseIIDataTable();
            ReportDataSet.Report_TravellAllowanceSettlementPhaseIIIDataTable dtTravellAllowancePhaseIII = new ReportDataSet.Report_TravellAllowanceSettlementPhaseIIIDataTable();

            //ReportDataSetTableAdapters.GetPAREmployeeHeaderTableAdapter adap =
            //    new ReportDataSetTableAdapters.GetPAREmployeeHeaderTableAdapter();
            ReportDataSet ds = new ReportDataSet();



            EEmployee eemployee = EmployeeManager.GetEmployeeById(request.EmployeeId.Value);

            txtAdvanceAmount.Text = GetCurrency(request.Advance);
            string LocationText = TravelAllowanceManager.GetLocationList().SingleOrDefault(x => x.LocationId == request.LocationId.Value).LocationName;


            if (request.AdvanceStatus == (int)AdvanceStatusEnum.HRAdvance)
            {
                List<TARequestLine> lines = new List<TARequestLine>();
                lines = TravelAllowanceManager.getAllowanceByEmployeeID(SessionManager.CurrentLoggedInEmployeeId, request.RequestID, request.LocationId.Value);

                foreach (TARequestLine _item in lines)
                {
                    dtTravellAllowancePhaseI.Rows.Add(_item.AllowanceName,_item.Quantity,_item.Rate,(_item.Rate.Value *  decimal.Parse(_item.Quantity.Value.ToString())));

                }

                foreach (var line in lines)
                {
                    line.FromDate = txtFromDateSett.SelectedDate.ToShortDateString();
                    line.ToDate = txtToDateSett.SelectedDate.ToShortDateString();
                    line.AllowType = 1;
                    if (line.Total != null)
                    {
                        line.ActualAmount = line.Total.Value;
                        line.ApprovedAmout = (decimal)line.Quantity * line.Rate.Value; ;
                    }
                }

                decimal total = lines.Sum(x => x.Total).Value;
                dispTotal.Text = GetCurrency(total);
                dispTotalApproved.Text = GetCurrency(lines.Sum(x => (decimal)x.Quantity.Value * x.Rate.Value));
                dispTotalSettle.Text = GetCurrency(lines.Sum(x => x.FinalTotal == null ? x.SettTotal.Value : x.FinalTotal));

                foreach (TARequestLine _item in lines)
                {
                    dtTravellAllowancePhaseII.Rows.Add(_item.FromDate, _item.ToDate, _item.Quantity, GetCurrency(_item.Total));
                    dtTravellAllowancePhaseIII.Rows.Add(GetAllowTextById(_item.AllowType), GetCurrency(_item.Total));
                }


                dtHeader.Rows.Add(eemployee.Name, eemployee.Branch.Name + "," + eemployee.Department.Name, eemployee.EDesignation.Name, request.PlaceOfTravel + "," + request.CountryName,
                    request.CountryName, request.PurposeOfTravel, Convert.ToDateTime(request.SettTravellingFromEng).ToShortDateString(), Convert.ToDateTime(request.SettTravellingToEng).ToShortDateString(), request.Days + " Days " + request.Night + " Night", request.Night,
                    request.TravelByText, request.ExpensePaidByText, LocationText, dispTotalApproved, dispTotal, dispTotalSettle,
                    GetCurrency(request.Advance), GetCurrency(total - (request.Advance==null?0:request.Advance)).ToString());
            }
            else
            {


                List<TARequestLine> lines = new List<TARequestLine>();
                lines = TravelAllowanceManager.getAllowanceByEmployeeID(SessionManager.CurrentLoggedInEmployeeId, request.RequestID, request.LocationId.Value);
                //storeAllowances.DataSource = lines;
                //storeAllowances.DataBind();
                foreach (TARequestLine _item in lines)
                {
                    dtTravellAllowancePhaseI.Rows.Add(_item.AllowanceName, _item.Quantity, GetCurrency(_item.Rate), GetCurrency(_item.Rate.Value * decimal.Parse(_item.Quantity.Value.ToString())));

                }

                dispTotalApproved.Text = GetCurrency(lines.Sum(x => (decimal)x.Quantity.Value * x.Rate.Value));
                foreach (var line in lines)
                {
                    line.FromDate = txtFromDateSett.SelectedDate.ToShortDateString();
                    line.ToDate = txtToDateSett.SelectedDate.ToShortDateString();
                    line.ApprovedAmout = (decimal)line.Quantity * line.Rate.Value; ;
                    line.Quantity = line.SettQuantity;
                    //if (line.Total != null)
                    
                    line.Total = line.SettTotal;
                    if (line.Total != null)
                        line.ActualAmount = line.Total.Value;
                    //if null Actual or else selected
                    line.AllowType = line.FinalAllowType == null ? 1 : line.FinalAllowType.Value;

                }

                decimal settlementTotal = lines.Sum(x => x.FinalTotal == null ? x.Total : x.FinalTotal.Value).Value;
                decimal total = lines.Sum(x => x.Total).Value;

                //dispTotal.Text = GetCurrency(total);
                //dispTotalSettle.Text = GetCurrency(total);

                //storeSettlement.DataSource = lines;
                //storeSettlement.DataBind();

                //storeSettlementFinal.DataSource = lines;
                //storeSettlementFinal.DataBind();
               // txtPayment.Text = GetCurrency((Math.Abs(decimal.Parse(dispTotalSettle.Text) - decimal.Parse(txtAdvanceAmount.Text)).ToString()));

                foreach (TARequestLine _item in lines)
                {
                    dtTravellAllowancePhaseII.Rows.Add(_item.FromDate, _item.ToDate, _item.Quantity, GetCurrency(_item.Total));

                    dtTravellAllowancePhaseIII.Rows.Add(GetAllowTextById(_item.AllowType), GetCurrency(_item.FinalTotal == null ? _item.Total : _item.FinalTotal.Value));
                }

                dtHeader.Rows.Add(eemployee.Name, eemployee.Branch.Name + "," + eemployee.Department.Name, eemployee.Department.Name, eemployee.EDesignation.Name, request.PlaceOfTravel + "," + request.CountryName,
               request.CountryName, request.PurposeOfTravel, Convert.ToDateTime(request.SettTravellingFromEng).ToShortDateString(), Convert.ToDateTime(request.SettTravellingToEng).ToShortDateString(), request.SettDays + " Days " + request.SettNight + " Night", request.SettNight,
               request.TravelByText, request.ExpensePaidByText,LocationText, dispTotalApproved, dispTotal, dispTotalSettle,
               GetCurrency(request.Advance), GetCurrency(settlementTotal - (request.Advance == null ? 0 : request.Advance)).ToString());

            }


          
            //adap.Connection.ConnectionString = Config.ConnectionString;
          //  ReportDataSet.GetPAREmployeeHeaderDataTable table = dtHeader.GetData(TimeSheetID);


            // Process for report

            HttpContext.Current.Items["dtTravellAllowancePhaseI"] = dtTravellAllowancePhaseI;
            HttpContext.Current.Items["dtTravellAllowancePhaseII"] = dtTravellAllowancePhaseII;
            HttpContext.Current.Items["dtTravellAllowancePhaseIII"] = dtTravellAllowancePhaseIII;

            ReportTravellAllowanceSettlement report = new ReportTravellAllowanceSettlement();
            report.DataSource = dtHeader;
            report.DataMember = "Report";

            //            report.labelTitle.Text = "";



            return report;
        }


        protected string GetAllowTextById(int value)
        {
            string rtnvalue = "";
            switch (value)
            {
                case 1:
                    rtnvalue = "Actual";
                    break;
                case 2:
                    rtnvalue = "Approved";
                    break;
                case 3:
                    rtnvalue = "Edit";

                    break;
            }
            return rtnvalue;
        }

        protected void LoadAllowanceGrid(int LocationID, int? RequestID, TARequest request)
        {
            if (request.AdvanceStatus == (int)AdvanceStatusEnum.HRSettle)
            {
                btnSaveAndLaterSett.Hide();
                btnSaveTR.Hide();
            }
            else
            {
                btnSaveAndLaterSett.Show();
                btnSaveTR.Show();
            }

 

            if (request.AdvanceStatus == (int)AdvanceStatusEnum.HRAdvance)
            {
                List<TARequestLine> lines = new List<TARequestLine>();
                List<TARequestLine> linesSettlement = new List<TARequestLine>();
                lines = TravelAllowanceManager.getAllowanceByEmployeeID(SessionManager.CurrentLoggedInEmployeeId, RequestID, LocationID);
                storeAllowances.DataSource = lines;
                storeAllowances.DataBind();

                linesSettlement = lines;

                foreach (var line in lines)
                {
                    line.FromDate = request.TravellingFromEng.Value.ToShortDateString();
                    line.ToDate = request.TravellingToEng.Value.ToShortDateString();
                    line.AllowType = 1;
                    
                    if (line.Total != null)
                    {
                        line.ActualAmount = line.Total.Value;
                        line.ApprovedAmout = (decimal)line.Quantity * line.Rate.Value; ;
                    }
                }


                decimal total = lines.Sum(x => x.Total).Value;
                dispTotal.Text = GetCurrency(total);
                dispTotalApproved.Text = GetCurrency(lines.Sum(x => (decimal)x.Quantity.Value * x.Rate.Value));
                dispTotalSettle.Text = GetCurrency(lines.Sum(x => x.FinalTotal == null ? x.SettTotal.Value : x.FinalTotal)); ;


                storeSettlement.DataSource = lines;
                storeSettlement.DataBind();

                foreach (var line in linesSettlement)
                {
                    line.FromDate = request.TravellingFromEng.Value.ToShortDateString();
                    line.ToDate = request.TravellingToEng.Value.ToShortDateString();

                    if (line.FinalAllowType != null)
                        line.AllowType = line.FinalAllowType.Value;
                    else
                        line.AllowType = 1;

                    if (line.Total != null)
                    {
                        line.ActualAmount = line.Total.Value;
                        line.ApprovedAmout = (decimal)line.Quantity * line.Rate.Value; ;
                    }
                }

                storeSettlementFinal.DataSource = linesSettlement;
                storeSettlementFinal.DataBind();

                txtPayment.Text = GetCurrency(((decimal.Parse(dispTotalSettle.Text) - decimal.Parse(txtAdvanceAmount.Text)).ToString()));




            }
            else
            {
                List<TARequestLine> lines = new List<TARequestLine>();
                lines = TravelAllowanceManager.getAllowanceByEmployeeID(SessionManager.CurrentLoggedInEmployeeId, RequestID, LocationID);
                storeAllowances.DataSource = lines;
                storeAllowances.DataBind();
                dispTotalApproved.Text = GetCurrency(lines.Sum(x => (decimal)x.Quantity.Value * x.Rate.Value));
                foreach (var line in lines)
                {
                    line.FromDate = request.TravellingFromEng.Value.ToShortDateString();
                    line.ToDate = request.TravellingToEng.Value.ToShortDateString();
                    line.ApprovedAmout = (decimal)line.Quantity * line.Rate.Value;
                    line.Quantity = line.SettQuantity;
                    //if (line.Total != null)
                    
                    line.Total = line.SettTotal;
                    if (line.Total != null)
                        line.ActualAmount = line.Total.Value;
                    line.AllowType = 1;

                }
                decimal total = lines.Sum(x => x.Total).Value;
                dispTotal.Text = GetCurrency(total);
                dispTotalSettle.Text = GetCurrency(lines.Sum(x => x.FinalTotal == null ? x.SettTotal.Value : x.FinalTotal)); ;

                storeSettlement.DataSource = lines;
                storeSettlement.DataBind();
                

                // if final settlement is already saved then show from saved values as values chould be changed
                foreach (var line in lines)
                {
                    if (line.FinalAllowType != null && line.FinalTotal != null)
                    {
                        line.AllowType = line.FinalAllowType.Value;
                        line.Total = line.FinalTotal;
                    }
                }

                storeSettlementFinal.DataSource = lines;
                storeSettlementFinal.DataBind();
                txtPayment.Text = GetCurrency(((decimal.Parse(dispTotalSettle.Text) - decimal.Parse(txtAdvanceAmount.Text)).ToString()));
            }

        }


        private void  LoadEditData(TARequest loadRequest)
        {
            txtAdvanceAmount.Text = GetCurrency(loadRequest.Advance);

            if (loadRequest.AdvanceStatus == (int)AdvanceStatusEnum.HRAdvance)
            {

                txtPlaceToTravelSett.Text = loadRequest.PlaceOfTravel;

                cmbCountrySett.SetValue(loadRequest.CountryId.ToString());

                cmbLocations.SetValue(loadRequest.LocationId.Value.ToString());
                
                LoadAllowanceGrid(loadRequest.LocationId.Value, loadRequest.RequestID, loadRequest);

                txtPurposeOfTravelSett.Text = loadRequest.PurposeOfTravel;
                txtFromDateSett.Text = loadRequest.TravellingFromEng.Value.ToShortDateString();
                txtToDateSett.Text = loadRequest.TravellingToEng.Value.ToShortDateString();

                cmbTravelBySett.SetValue(loadRequest.TravelBy.Value.ToString());


                cmbExpensePaidBySett.SetValue(loadRequest.ExpensePaidBy.Value.ToString());

                txtDayCountSett.Text = loadRequest.Days.ToString();
                txtNightCountSett.Text = loadRequest.Night.ToString();

            }
            else
            {


                txtPlaceToTravelSett.Text = loadRequest.PlaceOfTravel;

                cmbCountrySett.SetValue(loadRequest.CountryId.ToString());
                cmbLocations.SetValue(loadRequest.LocationId.Value.ToString());
                LoadAllowanceGrid(loadRequest.LocationId.Value, loadRequest.RequestID,loadRequest);

                txtPurposeOfTravelSett.Text = loadRequest.PurposeOfTravel;
                txtFromDateSett.Text = loadRequest.SettTravellingFromEng.Value.ToShortDateString();
                txtToDateSett.Text = loadRequest.SettTravellingToEng.Value.ToShortDateString();

                cmbTravelBySett.SetValue(loadRequest.SettTravelBy.Value.ToString());


                cmbExpensePaidBySett.SetValue(loadRequest.SettExpensePaidBy.Value.ToString());

                txtDayCountSett.Text = loadRequest.SettDays.ToString();
                txtNightCountSett.Text = loadRequest.SettNight.ToString();

            }


        }

      

        protected void btnDetailLevel_Click(object sender, DirectEventArgs e)
        {
            
        }




        protected void CheckInOutSelectChange(object sender, DirectEventArgs e)
        {
            string actualInDate = "";
            string actualOutDate = "";
            if (txtFromDateSett.Text != "0001-01-01 12:00:00 AM")
                actualInDate = txtFromDateSett.Text;
            if (txtToDateSett.Text != "0001-01-01 12:00:00 AM")
                actualOutDate = txtToDateSett.Text;
            DateTime englishInDate;
            DateTime englishOutDate;
            if (!string.IsNullOrEmpty(actualInDate) && !string.IsNullOrEmpty(actualOutDate))
            {
                englishInDate = DateTime.Parse(actualInDate);
                englishOutDate = DateTime.Parse(actualOutDate);

                txtDayCountSett.Text = (Math.Abs((englishInDate - englishOutDate).TotalDays) + 1).ToString();
                txtNightCountSett.Text = (Math.Abs((englishInDate - englishOutDate).TotalDays)).ToString();

                if ((englishInDate - englishOutDate).TotalDays == 0)
                {
                    txtDayCountSett.Text = "1";
                    txtNightCountSett.Text = "1";
                }

            }
            else
            {
                txtDayCountSett.Text = "0";
                txtNightCountSett.Text = "0";
            }
        }



        protected void ButtonNextSett_Click(object sender, DirectEventArgs e)
        {

            TARequest request = new TARequest();
            TARequestStatusHistory hist = new TARequestStatusHistory();
            bool isEdit;
            bool isStatusIncrease = true;

            isEdit = string.IsNullOrEmpty(hiddenValue.Text);
            if (!isEdit)
            {
                request.RequestID = int.Parse(hiddenValue.Text);
            }

            request = TravelAllowanceManager.getRequestByID(request.RequestID);

           
            
            request.AdvanceStatus = (int)AdvanceStatusEnum.HRSettle;

            List<TARequestLine> linesActual = new List<TARequestLine>();
            List<TARequestLine> linesApproved = new List<TARequestLine>();
            List<TARequestLine> linesSetteld = new List<TARequestLine>();
            List<TARequestLine> lines = new List<TARequestLine>();
            string jsonActual = e.ExtraParams["AllowanceValuesActual"];
            if (string.IsNullOrEmpty(jsonActual))
            {
                return;
            }


            string jsonApproved = e.ExtraParams["AllowanceValuesApproved"];
            if (string.IsNullOrEmpty(jsonApproved))
            {
                return;
            }

            string jsonSettled = e.ExtraParams["AllowanceValuesSettled"];
            if (string.IsNullOrEmpty(jsonSettled))
            {
                return;
            }

            linesActual = JSON.Deserialize<List<TARequestLine>>(jsonActual);
            linesApproved = JSON.Deserialize<List<TARequestLine>>(jsonApproved);
            linesSetteld = JSON.Deserialize<List<TARequestLine>>(jsonSettled);

            TARequestLine line = new TARequestLine();
            foreach (var val1 in linesApproved)
            {
                int index;
                index = linesApproved.IndexOf(val1);
                line = new TARequestLine();
                line.RequestId = linesApproved[index].RequestId;
                line.AllowanceId = linesApproved[index].AllowanceId;
                line.AllowanceName = linesApproved[index].AllowanceName;
                line.Quantity = linesApproved[index].Quantity;
                line.Rate = linesApproved[index].Rate;
                line.Total = linesApproved[index].Total;
                line.SettQuantity = linesActual[index].Quantity;
                line.SettTotal = linesActual[index].Total;
                line.FinalTotal = linesSetteld[index].Total;
                line.FinalAllowType = linesSetteld[index].AllowType;
                lines.Add(line);
            }


            request.SettledTotal = linesSetteld.Sum(x => x.Total);
            
            Status status = TravelAllowanceManager.SettleTravelRequests(request, lines, hist, isStatusIncrease);
            if (status.IsSuccess)
            {
                NewMessage.ShowNormalMessage("TR settled.");
                Window1.Close();
                //LoadLevels();
                X.Js.Call("searchList");
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }
            

            
        }

        protected void btnSaveTR_Click(object sender, DirectEventArgs e)
        {
            TARequest request = new TARequest();
            TARequestStatusHistory hist = new TARequestStatusHistory();
            bool isEdit;
            bool isStatusIncrease = false;

            isEdit = string.IsNullOrEmpty(hiddenValue.Text);
            if (!isEdit)
            {
                request.RequestID = int.Parse(hiddenValue.Text);
            }

            request = TravelAllowanceManager.getRequestByID(request.RequestID);



            request.AdvanceStatus = (int)AdvanceStatusEnum.EmployeeSettle;

            List<TARequestLine> linesActual = new List<TARequestLine>();
            List<TARequestLine> linesApproved = new List<TARequestLine>();
            List<TARequestLine> linesSetteld = new List<TARequestLine>();
            List<TARequestLine> lines = new List<TARequestLine>();
            string jsonActual = e.ExtraParams["AllowanceValuesActual"];
            if (string.IsNullOrEmpty(jsonActual))
            {
                return;
            }


            string jsonApproved = e.ExtraParams["AllowanceValuesApproved"];
            if (string.IsNullOrEmpty(jsonApproved))
            {
                return;
            }

            string jsonSettled = e.ExtraParams["AllowanceValuesSettled"];
            if (string.IsNullOrEmpty(jsonSettled))
            {
                return;
            }

            linesActual = JSON.Deserialize<List<TARequestLine>>(jsonActual);
            linesApproved = JSON.Deserialize<List<TARequestLine>>(jsonApproved);
            linesSetteld = JSON.Deserialize<List<TARequestLine>>(jsonSettled);

            TARequestLine line = new TARequestLine();
            foreach (var val1 in linesApproved)
            {
                int index;
                index = linesApproved.IndexOf(val1);
                line = new TARequestLine();
                line.RequestId = linesApproved[index].RequestId;
                line.AllowanceId = linesApproved[index].AllowanceId;
                line.AllowanceName = linesApproved[index].AllowanceName;
                line.Quantity = linesApproved[index].Quantity;
                line.Rate = linesApproved[index].Rate;
                line.Total = linesSetteld[index].Total;
                line.AllowType = linesSetteld[index].AllowType;
                line.FinalTotal = linesSetteld[index].Total;
                line.AllowType = linesSetteld[index].AllowType;
                lines.Add(line);
            }


            //request.SettledTotal = linesSetteld.Sum(x => x.Total);
            request.Total = linesApproved.Sum(x => x.Total);

            Status status = TravelAllowanceManager.SaveTravelRequestHR(request, lines, hist, isStatusIncrease);
            if (status.IsSuccess)
            {
                NewMessage.ShowNormalMessage("TR saved.");
                Window1.Close();
                //LoadLevels();
                X.Js.Call("searchList");
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }
        }


        protected void cmbLocations_Change(object sender, DirectEventArgs e)
        {

            List<TARequestLine> lines = new List<TARequestLine>();
            string json = e.ExtraParams["Values"];
            if (!string.IsNullOrEmpty(json))
            {

                lines = JSON.Deserialize<List<TARequestLine>>(json);

                foreach (TARequestLine line in lines)
                {

                    TAAllowanceRate rate = TravelAllowanceManager.GetRate(SessionManager.CurrentLoggedInEmployeeId,
                        int.Parse(cmbLocations.SelectedItem.Value),
                         line.AllowanceId.Value);

                    if (rate != null && rate.Rate != null)
                        line.Rate = rate.Rate;
                    else
                        line.Rate = 0;


                }

            }


            storeAllowances.DataSource = lines;
            storeAllowances.DataBind();

        }

        protected void Store_ReadData(object sender, StoreReadDataEventArgs e)
        {

            int? advanceStatus = null;

            int totalRecords = 0;

            if (cmbStatus.SelectedItem != null && cmbStatus.SelectedItem.Value != null)
            {
                advanceStatus = int.Parse(cmbStatus.SelectedItem.Value);
            }

            int type = 1;

            if (!string.IsNullOrEmpty(cmbType.SelectedItem.Value))
                type = int.Parse(cmbType.SelectedItem.Value);

            int employeeId = -1;

            int.TryParse(cmbEmpSearch.SelectedItem.Value, out employeeId);

            if (employeeId == 0)
                employeeId = -1;

            string empSearch = "", destinationSearch = "";

            if (employeeId == -1)
            {
                if (!string.IsNullOrEmpty(cmbEmpSearch.Text))
                    empSearch = cmbEmpSearch.Text.Trim();
            }

            DateTime? startDate = null, endDate = null;
            if (type == -1)
            {
                if (txtFromDate.SelectedDate == new DateTime())
                {
                    NewMessage.ShowWarningMessage("Please select from date.");
                    txtFromDate.Focus();
                    return;
                }

                if (txtToDate.SelectedDate == new DateTime())
                {
                    NewMessage.ShowWarningMessage("Please select to date.");
                    txtToDate.Focus();
                    return;
                }

                startDate = txtFromDate.SelectedDate;
                endDate = txtToDate.SelectedDate;
            }

            destinationSearch = txtDestination.Text.Trim();

            List<GetTravelRequestAdvanceResult> list = TravelAllowanceManager.GetAllTravelRequestAdvance(e.Start, int.Parse(cmbPageSize.SelectedItem.Value), startDate, endDate, advanceStatus, type, employeeId, empSearch, destinationSearch, ref totalRecords);

            e.Total = totalRecords;
            storeTR.DataSource = list;
            storeTR.DataBind();
        }

        public void btnExportTR_Click(object sender, EventArgs e)
        {
            int? advanceStatus = null;

            int totalRecords = 0;

            if (cmbStatus.SelectedItem != null && cmbStatus.SelectedItem.Value != null)
            {
                advanceStatus = int.Parse(cmbStatus.SelectedItem.Value);
            }

            int type = 1;

            if (!string.IsNullOrEmpty(cmbType.SelectedItem.Value))
                type = int.Parse(cmbType.SelectedItem.Value);

            int employeeId = -1;

            int.TryParse(cmbEmpSearch.SelectedItem.Value, out employeeId);

            if (employeeId == 0)
                employeeId = -1;

            string empSearch = "", destinationSearch = "";

            if (employeeId == -1)
            {
                if (!string.IsNullOrEmpty(cmbEmpSearch.Text))
                    empSearch = cmbEmpSearch.Text.Trim();
            }

            DateTime? startDate = null, endDate = null;
            if (type == -1)
            {
                if (txtFromDate.SelectedDate == new DateTime())
                {
                    NewMessage.ShowWarningMessage("Please select from date.");
                    txtFromDate.Focus();
                    return;
                }

                if (txtToDate.SelectedDate == new DateTime())
                {
                    NewMessage.ShowWarningMessage("Please select to date.");
                    txtToDate.Focus();
                    return;
                }

                startDate = txtFromDate.SelectedDate;
                endDate = txtToDate.SelectedDate;
            }

            destinationSearch = txtDestination.Text.Trim();

            List<GetTravelRequestAdvanceResult> list = TravelAllowanceManager.GetAllTravelRequestAdvance(0, 999999, startDate, endDate, advanceStatus, type, employeeId, empSearch, destinationSearch, ref totalRecords);

            foreach (var item in list)
            {
                item.FromDateEngFormatted = item.FromDate.Value.ToShortDateString();
                item.ToDateEngFormatted = item.ToDate.Value.ToShortDateString();
            }

            Bll.ExcelHelper.ExportToExcel("Travel Settlement Report", list,
                new List<String>() { "RequestID", "EmployeeId", "FromDate", "ToDate",  "AdvDate", "Status", "StatusName", "StatusModified", "AdvanceStatus", "LocationId", "EstimatedTotal" },
            new List<String>() { },
            new Dictionary<string, string>() { { "FromDateEngFormatted", "Date" }, { "ToDateEngFormatted", "To Date" }, { "EmpName", "Employee Name" }, { "LocationName", "Location" }, { "AdvanceStatusText", "Status" } },
            new List<string>() { "ApprovedAmount", "AdvanceAmount", "ClaimAmount", "SettledAmount"}
            , new Dictionary<string, string>() { } 
            , new List<string> { "FromDateEngFormatted", "ToDateEngFormatted", "EmpName", "Position", "Department", "Destination", "LocationName", "Reason", "Duration", "AdvanceStatusText" });



        }
    }
}