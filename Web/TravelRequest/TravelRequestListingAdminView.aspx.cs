﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using System.Xml;

namespace Web.CP
{
    public partial class TravelRequestListingAdminView : BasePage
    {
        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.HumanResource;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();
            }
        }

        protected void btnDelete_Click(object sender, DirectEventArgs e)
        {

            int taid = int.Parse(hiddenValue.Text);

            Status status = TravelAllowanceManager.DeleteTA(taid);
            if (status.IsSuccess)
            {
                X.Js.Call("searchList");
                NewMessage.ShowNormalMessage("Travel Request has been deleted.");
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }

        }

        public void btnAssign_Click(object sender, DirectEventArgs e)
        {

            //X.Msg.Alert(cmbLocationsAssign.SelectedItem.Value, cmbLocationsAssign.SelectedItem.Value).Show();
            TravelAssignCtl2.Assign_Click();
        }
        public void Initialise()
        {

            if (CommonManager.SettingTA.IsLocationEditable != null && CommonManager.SettingTA.IsLocationEditable.Value)
            {
                //btnAssign.Hide();
                ColumnAdvance.Hide();
            }

            List<DAL.ApprovalFlow> list =
                TravelAllowanceManager.GetDefaultApprovalFlowList((int)FlowTypeEnum.TravelOrder);

            list.Insert(0, new DAL.ApprovalFlow { StepID = 0, StepName = "Pending/Saved" });
            list.Insert(0, new DAL.ApprovalFlow { StepID = -1, StepName = "All" });
            cmbStatus.SelectedItems.Add(new Ext.Net.ListItem { Index = 0 });


            cmbStatus.Store[0].DataSource = list;
            cmbStatus.Store[0].DataBind();


            CommonManager comManager = new CommonManager();
            

            //cmbLocationsAssign_Change(cmbEmployeeAssign, null);

            X.Js.Call("searchList");
        }

      
        protected void Store_ReadData(object sender, StoreReadDataEventArgs e)
        {
            int status = -1, totalRecords = 0;
            if (!string.IsNullOrEmpty(cmbStatus.SelectedItem.Value))
            {
                status = int.Parse(cmbStatus.SelectedItem.Value);
            }

            int type = 1;

            if (!string.IsNullOrEmpty(cmbType.SelectedItem.Value))
                type = int.Parse(cmbType.SelectedItem.Value);

            int employeeId = -1;

            int.TryParse(cmbEmpSearch.SelectedItem.Value, out employeeId);

            if (employeeId == 0)
                employeeId = -1;

            string empSearch = "", destinationSearch = "";

            if (employeeId == -1)
            {
                if (!string.IsNullOrEmpty(cmbEmpSearch.Text))
                    empSearch = cmbEmpSearch.Text.Trim();
            }

            DateTime? startDate = null, endDate = null;
            if (type == -1)
            {
                if (txtFromDate.SelectedDate == new DateTime())
                {
                    //NewMessage.ShowWarningMessage("Please select from date.");
                    //txtFromDate.Focus();
                    //return;
                }
                else
                    startDate = txtFromDate.SelectedDate;

                if (txtToDate.SelectedDate == new DateTime())
                {
                    //NewMessage.ShowWarningMessage("Please select to date.");
                    //txtToDate.Focus();
                    //return;
                }
                else               
                    endDate = txtToDate.SelectedDate;
            }

            destinationSearch = txtDestination.Text.Trim();

            List<GetTravelRequestsForAdminResult> list = TravelAllowanceManager.GetAllTravelRequestForAdmin(e.Start, int.Parse(cmbPageSize.SelectedItem.Value), startDate, endDate, status, type, employeeId, empSearch, destinationSearch, ref totalRecords);

            e.Total = totalRecords;
            storeTR.DataSource = list;
            storeTR.DataBind();
        }
        
        public void ClearLevelFields()
        {
            hiddenValue.Text = "";
            //txtName.Text = "";
            //txtDescription.Text = "";
        }

      
        protected void btnEditLevel_Click(object sender, DirectEventArgs e)
        {

            TARequest request = new TARequest();
            request = TravelAllowanceManager.getRequestByID(int.Parse(hiddenValue.Text));

            TravelRequestCtl2.LoadTravelRequest(request);

            TravelRequestCtl2.HideButtonBlock();
        }


        public void btnExport_Click(object sender, EventArgs e)
        {
            int status = -1, totalRecords = 0;
            if (!string.IsNullOrEmpty(cmbStatus.SelectedItem.Value))
            {
                status = int.Parse(cmbStatus.SelectedItem.Value);
            }

            int type = 1;

            if (!string.IsNullOrEmpty(cmbType.SelectedItem.Value))
                type = int.Parse(cmbType.SelectedItem.Value);

            int employeeId = -1;

            int.TryParse(cmbEmpSearch.SelectedItem.Value, out employeeId);

            if (employeeId == 0)
                employeeId = -1;

            string empSearch = "", destinationSearch = "";

            if (employeeId == -1)
            {
                if (!string.IsNullOrEmpty(cmbEmpSearch.Text))
                    empSearch = cmbEmpSearch.Text.Trim();
            }

            DateTime? startDate = null, endDate = null;
            if (type == -1)
            {
                if (txtFromDate.SelectedDate == new DateTime())
                {
                   
                }
                else
                    startDate = txtFromDate.SelectedDate;

                if (txtToDate.SelectedDate == new DateTime())
                {
                }
                else
                    endDate = txtToDate.SelectedDate;
                
                
            }

            destinationSearch = txtDestination.Text.Trim();

            List<GetTravelRequestsForAdminResult> list = TravelAllowanceManager.GetAllTravelRequestForAdmin(0, 999999, startDate, endDate, status, type, employeeId, empSearch, destinationSearch, ref totalRecords);
            foreach (var item in list)
            {
                item.FromDateEngFormatted = item.FromDate.Value.ToShortDateString();
                item.ToDateEngFormatted = item.ToDate.Value.ToShortDateString();

                if (item.ExtendedDate != null)
                    item.ExtendedFormatted = item.ExtendedDate.Value.ToShortDateString();
                if (item.Total != null)
                    item.TotalInString = item.Total.Value.ToString("N2");
            }

            Bll.ExcelHelper.ExportToExcel("Travel Request Report", list,
                new List<String>() { "RequestID","Total", "FromDate", "ToDate","ExtendedDate", "Position", "Department", "Status", "StatusModified", "LocationId", "Total" },
            new List<String>() { },
            new Dictionary<string, string>() { { "FromDateEngFormatted", "From Date" }, { "ToDateEngFormatted", "To Date" }, { "ExtendedFormatted", "Extended Date" }, { "EmployeeId", "EIN" }, { "EmpName", "Employee Name" }, { "LocationName", "Location" }, { "ExpensePaidByText", "Expense by" }, { "TravelByText", "Travel by" }, { "TotalInString", "Total Amount" }, { "RequestedAdvance", "Requested Advance" } },
            new List<string>() { "RequestedAdvance"}
            , new Dictionary<string, string>() { }
            , new List<string> { "Number","FromDateEngFormatted", "ToDateEngFormatted","ExtendedDate", "EmployeeId", "EmpName", "Destination", "LocationName", "Reason", "Duration", "ExpensePaidByText", "TravelByText", "TotalInString","RequestedAdvance" });



        }



        

        


        


    }
}

