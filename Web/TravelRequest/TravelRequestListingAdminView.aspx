﻿<%@ Page Title="Travel Request Listing Admin View" Language="C#" MasterPageFile="~/Master/NewDetails.Master"
    AutoEventWireup="true" CodeBehind="TravelRequestListingAdminView.aspx.cs" Inherits="Web.CP.TravelRequestListingAdminView" %>

<%@ Register Src="~/Employee/UserControls/TravelRequestCtl.ascx" TagName="TravelRequestCtl"
    TagPrefix="uc1" %>
<%@ Register Src="~/Employee/UserControls/TravelAssignCtl.ascx" TagName="TravelAssignCtl"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">


        
        var CommandHandler1 = function(command, record){
            <%= hiddenValue.ClientID %>.setValue(record.data.RequestID);
                if(command=="Edit")
                {
                    <%= btnEditLevel.ClientID %>.fireEvent('click');
                }
                    if(command=="Delete")
                {
                    <%= btnDelete.ClientID %>.fireEvent('click');
                }
             }
             

             var totalCost = function (records) {
            var i = 0,
                length = records.length,
                total = 0,
                record;

            for (; i < length; ++i) {
                record = records[i];
                total += record.get('Quantity') * record.get('Rate');
            }

            return total;
        };

        
        function searchListAssign() {
            searchList();
        }

        function DateTypeSelect()
        {
            var type = <%=cmbType.ClientID %>.getValue();

            if(type == -1)
            {
                <%=txtFromDate.ClientID %>.show();
                <%=txtToDate.ClientID %>.show();
            }
            else
            {
                <%=txtFromDate.ClientID %>.hide();
                <%=txtToDate.ClientID %>.hide();
            }
        }

        function searchList() {
             <%=GridLevels.ClientID %>.getStore().pageSize = <%=cmbPageSize.ClientID %>.getValue();
            <%=PagingToolbar1.ClientID %>.doRefresh();
        }


    </script>
    <style type="text/css">
        .hideLeftBlockCssInPage
        {
            margin: 0px !important;
            padding-left: 10px !important;
        }
        #menu
        {
            display: none;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <%--<ext:ResourceManager ID="resourceManager1" runat="server" />--%>
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Travel Requests
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <ext:Hidden runat="server" ID="hiddenValue" />
        <ext:Hidden runat="server" ID="HiddenStatus">
        </ext:Hidden>
        <ext:LinkButton runat="server" Hidden="true" ID="btnEditLevel">
            <DirectEvents>
                <Click OnEvent="btnEditLevel_Click">
                    <EventMask ShowMask="true" />
                </Click>
            </DirectEvents>
        </ext:LinkButton>
         <ext:LinkButton runat="server" Hidden="true" ID="btnDelete">
            <DirectEvents>
                <Click OnEvent="btnDelete_Click">
                    <Confirmation Message="Are you sure, you want to delete the Travel Request, after deletion process can not be reverted?" ConfirmRequest="true" />
                    <EventMask ShowMask="true" />
                </Click>
            </DirectEvents>
        </ext:LinkButton>
      
        <div class="innerLR">
            <table class="fieldTable firsttdskip">
                <tr>
                    <td>
                        <ext:ComboBox FieldLabel="Status" ID="cmbStatus" Width="150" runat="server" ValueField="StepID"
                            DisplayField="StepName" LabelAlign="Top" LabelSeparator="" ForceSelection="true"
                            QueryMode="Local">
                            <Store>
                                <ext:Store ID="Store1" runat="server">
                                    <Model>
                                        <ext:Model ID="Model1" IDProperty="StepID" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="StepID" />
                                                <ext:ModelField Name="StepName" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                        </ext:ComboBox>
                    </td>
                    <td>
                        <ext:ComboBox ID="cmbType" runat="server" Width="150" LabelWidth="40" FieldLabel="Show"
                            LabelAlign="Top" LabelSeparator="" ForceSelection="true" QueryMode="Local">
                            <Items>
                                <ext:ListItem Text="This Month" Value="1" />
                                <ext:ListItem Text="Last Month" Value="2" />
                                <ext:ListItem Text="This Year" Value="3" />
                                <ext:ListItem Text="Date Range" Value="-1" />
                            </Items>
                            <SelectedItems>
                                <ext:ListItem  Index="3" />
                            </SelectedItems>
                            <Listeners>
                                <Select Fn="DateTypeSelect" />
                            </Listeners>
                        </ext:ComboBox>
                    </td>
                    <td>
                        <ext:DateField ID="txtFromDate" runat="server" FieldLabel="From Date" LabelSeparator=""
                            LabelAlign="Top" Width="150"  />
                    </td>
                    <td>
                        <ext:DateField ID="txtToDate" runat="server" FieldLabel="To Date" LabelSeparator=""
                            LabelAlign="Top" Width="150"  />
                    </td>
                    <td>
                        <ext:Store runat="server" ID="storeSearch" AutoLoad="false">
                            <Proxy>
                                <ext:AjaxProxy Url="../Handler/EmpSearch.ashx">
                                    <ActionMethods Read="GET" />
                                    <Reader>
                                        <ext:JsonReader Root="plants" TotalProperty="total" />
                                    </Reader>
                                </ext:AjaxProxy>
                            </Proxy>
                            <Model>
                                <ext:Model ID="Model5" IDProperty="Name" Name="ItemLineModel" runat="server">
                                    <Fields>
                                        <ext:ModelField Name="Name" Type="String" />
                                        <ext:ModelField Name="EmployeeId" Type="String" />
                                    </Fields>
                                </ext:Model>
                            </Model>
                        </ext:Store>
                        <ext:ComboBox LabelSeparator="" ID="cmbEmpSearch" FieldLabel="Employee Search" EmptyText="Employee Search"
                            LabelWidth="80" LabelAlign="Top" runat="server" DisplayField="Name" ValueField="EmployeeId"
                            StoreID="storeSearch" TypeAhead="false" Width="200" PageSize="9999" HideBaseTrigger="true"
                            MinChars="1" TriggerAction="All" ForceSelection="false">
                            <ListConfig LoadingText="Searching..." MinWidth="200" StyleSpec="border-top:1px solid #98c0f4;">
                                <ItemTpl ID="ItemTpl2" runat="server">
                                    <Html>
                                        <tpl>
                                                        <div class="search-item">
                                                                        <span>{Name}</span>  
                                                         </div>
					                            </tpl>
                                    </Html>
                                </ItemTpl>
                            </ListConfig>
                            <Triggers>
                                <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                            </Triggers>
                            <Listeners>
                                <Select Handler="this.getTrigger(0).show();" />
                                <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                <TriggerClick Handler="if (index == 0) { 
                                                       this.clearValue(); 
                                                       this.getTrigger(0).hide();
                                                   }" />
                            </Listeners>
                        </ext:ComboBox>
                    </td>
                    <td>
                        <ext:TextField ID="txtDestination" Width="150" runat="server" EmptyText="Destination Search"
                            FieldLabel="Destination" LabelAlign="Top" LabelSeparator="">
                        </ext:TextField>
                    </td>
                    <td style="padding-top: 20px">
                        <ext:Button runat="server" Cls="btn btn-default btn-sm" StyleSpec="margin-top:10px"
                            Height="30" ID="btnLoad" Text="<i></i>Load" runat="server">
                            <Listeners>
                                <Click Fn="searchList">
                                </Click>
                            </Listeners>
                        </ext:Button>
                    </td>
                    <td style="padding-top: 20px">
                        <ext:Button runat="server" Cls="btn btn-warning btn-sm" StyleSpec="margin-top:10px"
                            Height="30" ID="btnAssign" Text="<i></i>Assign Travel" runat="server">
                            <DirectEvents>
                                <Click OnEvent="btnAssign_Click">
                                    <EventMask ShowMask="true" />
                                </Click>
                            </DirectEvents>
                        </ext:Button>
                    </td>
                </tr>
            </table>
            <ext:GridPanel StyleSpec="margin-top:15px;" ID="GridLevels" runat="server" OnReadData="Store_ReadData">
                <Store>
                    <ext:Store ID="storeTR" runat="server" AutoLoad="true" OnReadData="Store_ReadData"
                        RemotePaging="true" RemoteSort="true">
                        <Proxy>
                            <ext:PageProxy />
                        </Proxy>
                        <Model>
                            <ext:Model ID="Model4" runat="server" IDProperty="RequestID">
                                <Fields>
                                    <ext:ModelField Name="RequestID" Type="String" />
                                    <ext:ModelField Name="EmployeeId" Type="string" />
                                    <ext:ModelField Name="FromDate" Type="Date" />
                                    <ext:ModelField Name="EmpName" Type="string" />
                                    <ext:ModelField Name="Position" Type="string" />
                                    <ext:ModelField Name="Department" Type="string" />
                                    <ext:ModelField Name="Destination" Type="string" />
                                    <ext:ModelField Name="Reason" Type="string" />
                                    <ext:ModelField Name="Duration" Type="string" />
                                    <ext:ModelField Name="Status" Type="string" />
                                    <ext:ModelField Name="StatusName" Type="string" />
                                    <ext:ModelField Name="ExpensePaidByText" Type="string" />
                                    <ext:ModelField Name="TravelByText" Type="string" />
                                    <ext:ModelField Name="ToDate" Type="string" />
                                    <ext:ModelField Name="Total" Type="string" />
                                    <ext:ModelField Name="LocationName" Type="string" />
                                    <ext:ModelField Name="Number" Type="string" />
                                    <ext:ModelField Name="RequestedAdvance" Type="String" />
                                     <ext:ModelField Name="ExtendedDate" Type="String" />
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                <ColumnModel>
                    <Columns>
                         <ext:Column ID="Column3" Locked="true"  Sortable="false" MenuDisabled="true" runat="server" Text="Number"
                            Width="100" Align="Left" DataIndex="Number" />
                        <ext:DateColumn Locked="true"  ID="DateColumn1" runat="server" Text="From Date" Sortable="true"
                            DataIndex="FromDate" Width="85" Align="Left" Format="MM/dd/yyyy">
                        </ext:DateColumn>
                        <ext:DateColumn Locked="true"  ID="DateColumn2" runat="server" Text="To Date" Sortable="true" DataIndex="ToDate"
                            Width="85" Align="Left" Format="MM/dd/yyyy">
                        </ext:DateColumn>
                         <ext:DateColumn Locked="true"  ID="DateColumn3" runat="server" Text="Extended Date" Sortable="true" DataIndex="ExtendedDate"
                            Width="85" Align="Left" Format="MM/dd/yyyy">
                        </ext:DateColumn>
                        <ext:Column Locked="true"  ID="Column4" Sortable="false" MenuDisabled="true" runat="server" Text="EIN"
                            Width="45" Align="Left" DataIndex="EmployeeId" />
                        <ext:Column Locked="true"  ID="Column10" Sortable="false" MenuDisabled="true" runat="server" Text="Employee Name"
                            Width="140" Align="Left" DataIndex="EmpName" />
                       
                        <ext:Column ID="Column9" Sortable="false" MenuDisabled="true" runat="server" Text="Location"
                            Width="125" Align="Left" DataIndex="LocationName" />
                        <ext:Column ID="Column6" Sortable="false" MenuDisabled="true" runat="server" Text="Reason"
                            Width="140" Align="Left" DataIndex="Reason" />
                        <ext:Column ID="Column7" Sortable="false" MenuDisabled="true" runat="server" Text="Duration"
                            Width="120" Align="Left" DataIndex="Duration" />
                        <ext:Column ID="Column1" Sortable="false" MenuDisabled="true" runat="server" Text="Expense by"
                            Width="100" Align="Left" DataIndex="ExpensePaidByText" />
                        <ext:Column ID="Column2" Sortable="false" MenuDisabled="true" runat="server" Text="Travel by"
                            Width="75" Align="Left" DataIndex="TravelByText" />
                        <ext:Column ID="ColumnTotal" Sortable="false" MenuDisabled="true" runat="server" Text="Total Amount"
                            Width="90" Align="Right" DataIndex="Total">
                            <Renderer Fn="getFormattedAmount" />
                        </ext:Column>
                         <ext:Column ID="ColumnAdvance" Sortable="false" MenuDisabled="true" runat="server" Text="Advance Req"
                            Width="90" Align="Right" DataIndex="RequestedAdvance">
                            <Renderer Fn="getFormattedAmount" />
                        </ext:Column>
                        <ext:Column ID="Column8" Sortable="false" MenuDisabled="true" runat="server" Text="Status"
                            Width="80" Align="Left" DataIndex="StatusName">
                        </ext:Column>
                        <ext:CommandColumn ID="CommandColumn1" runat="server" Align="Center" Width="40">
                            <Commands>
                                <ext:CommandSeparator />
                                <ext:GridCommand Cls="editGridButton" ToolTip-Text="View" Text="<i class='fa fa-pencil'></i>"
                                    CommandName="Edit" />
                               
                            </Commands>
                            <Listeners>
                                <Command Handler="CommandHandler1(command,record);" />
                            </Listeners>
                        </ext:CommandColumn>
                        <ext:CommandColumn ID="CommandColumnDelete" runat="server" Align="Center" Width="40">
                            <Commands>
                                <ext:CommandSeparator />
                                 <ext:GridCommand Cls="editGridButton" ToolTip-Text="Edit" Text="<i class='fa fa-trash-o'></i>"
                                    CommandName="Delete" />
                            </Commands>
                            <Listeners>
                                <Command Handler="CommandHandler1(command,record);" />
                            </Listeners>
                        </ext:CommandColumn>
                    </Columns>
                </ColumnModel>
                <SelectionModel>
                    <ext:RowSelectionModel ID="RowSelectionModel2" runat="server" Mode="Single" />
                </SelectionModel>
                <BottomBar>
                    <ext:PagingToolbar ID="PagingToolbar1" runat="server" StoreID="storeTR" DisplayInfo="true">
                        <Items>
                            <ext:Label ID="lblPageSize" runat="server" Text="Page size:" />
                            <ext:ToolbarSpacer ID="ToolbarSpacer_listItems" runat="server" Width="10" />
                            <ext:ComboBox ID="cmbPageSize" runat="server" Width="80" SelectOnFocus="true" Selectable="true"
                                ValueField="Value" DisplayField="Text" ForceSelection="true" AllowBlank="false">
                                <Listeners>
                                    <Select Handler="searchList()" />
                                    <%--#{cmbPageSize}.pageSize = this.getValue();#{PagingToolbar1}.moveFirst();--%>
                                </Listeners>
                                <Items>
                                    <ext:ListItem Value="20" Text="20" />
                                    <ext:ListItem Value="50" Text="50" />
                                    <ext:ListItem Value="100" Text="100" />
                                </Items>
                                <SelectedItems>
                                    <ext:ListItem Index="0">
                                    </ext:ListItem>
                                </SelectedItems>
                            </ext:ComboBox>
                        </Items>
                    </ext:PagingToolbar>
                </BottomBar>
            </ext:GridPanel>
        </div>
    </div>
    <div style="float: right; padding-right: 30px;">
        <ext:Button runat="server" AutoPostBack="true" OnClick="btnExport_Click" ID="btnExport"
            Text="<i></i>Export To Excel">
        </ext:Button>
    </div>
    <uc1:TravelRequestCtl Id="TravelRequestCtl2" runat="server" />
    <uc1:TravelAssignCtl Id="TravelAssignCtl2" runat="server" />
</asp:Content>
