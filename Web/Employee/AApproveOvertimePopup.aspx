﻿<%@ Page Language="C#" Title="Overtime Approval" AutoEventWireup="true" MasterPageFile="~/Master/ForEmployeePopupPage.Master"
    CodeBehind="AApproveOvertimePopup.aspx.cs" Inherits="Web.Employee.AApproveOvertimePopup" %>

    <%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<%@ Register Assembly="DevExpress.Web.v16.1, Version=16.1.8.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function ConfirmInstitutionDeletion() {
            if (ddllist.selectedIndex == -1) {
                alert('No Insurance company to delete.');
                clearUnload();
                return false;
            }
            if (confirm('Do you want to delete the insurance company?')) {
                clearUnload();
                return true;
            }
            else {
                return false;
            }
        }

        function DisplayInstitutionInTextBox(dropdown_id) {

        }



        function closePopup() {
            if (hasImport)
                window.opener.refreshEventList(window); ;
        }


        window.onunload = closePopup;  
    </script>
    <style type="text/css">
        body
        {
            background-color: #E8F1FF !important;
        }
        strong
        {
            font-size: 15px !important;
            font-weight: bold !important;
        }
        .tbl > tbody > tr > td
        {
            padding-bottom: 10px;
        }
        div.notify
        {
            width: inherit !important;
        }
        .marginal
        {
            margin-top: 10px !important;
        }
        .tableLightColor th
        {
            font-weight:normal !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
    <div class="popupHeader">
        <span style='margin-left: 20px; padding-top: 8px; display: block' runat="server"
            id="spanOvertime">Overtime approval for : </span>
    </div>
    <div class="marginal">

        <asp:HiddenField ID="Hidden_MinutesPrev" runat="server" />

        <uc2:MsgCtl ID="divMsgCtl" EnableViewState="false" Hide="true" Width="390px" runat="server" />
        <uc2:WarningCtl ID="divWarningMsg" EnableViewState="false" Hide="true" runat="server" />
        <asp:HiddenField ID="Hidden_RequestID" runat="server" />
        <table style="margin-bottom: 25px;" class="tbl">
            <tbody>
                <tr>
                    <td>
                        <strong>Date : </strong>
                    </td>
                    <td>
                        <asp:Label runat="server" ID="lblDate"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong>Name : </strong>
                    </td>
                    <td>
                        <asp:Label runat="server" ID="lblName"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong>Description : </strong>
                    </td>
                    <td>
                        <asp:Label runat="server" ID="lblReason"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong>In Time : </strong>
                    </td>
                    <td>
                        <asp:Label runat="server" ID="lblInTime"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong>Out Time : </strong>
                    </td>
                    <td>
                        <asp:Label runat="server" ID="lblOutTime"></asp:Label>
                    </td>
                </tr>
            </tbody>
        </table>
        <table class="tbl">
            <tr>
                <td>
                    <strong>Requested Hour : </strong>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:TextBox ID="txtRequested" runat="server" ValidationGroup="SaveUpdate" Style="width: 100px;"
                        ReadOnly="true"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <strong>Approve For : </strong>
                </td>
            </tr>
            <tr>
                <td>
                    Hour<br />
                    <asp:TextBox ID="txtHour" runat="server" ValidationGroup="SaveUpdate" Style="width: 100px;"></asp:TextBox>
                </td>
                <td  runat="server" id="colMin">
                    Minute<br />
                    <asp:TextBox ID="txtMinute" runat="server" ValidationGroup="SaveUpdate" Style="width: 100px;"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Button ID="btnApprove" ValidationGroup="SaveUpdate" runat="server" OnClientClick="valGroup='SaveUpdate'; if(confirm('Are you sure you want to approve this request?')) return CheckValidation(); else return false;"
                        Text="Approve" Width="150" Height="30" Style="margin-top: 25px;" OnClick="btnApprove_Click"
                        CssClass="save" />
                    <asp:Button ID="btnRecommend" ValidationGroup="SaveUpdate" runat="server" OnClientClick="valGroup='SaveUpdate'; if(confirm('Are you sure you want to recommend this request?')) return CheckValidation(); else return false;"
                        Text="Recommend" Width="150" Height="30" Style="margin-top: 25px;" OnClick="btnRecommend_Click"
                        CssClass="save" />
                </td>
                <td colspan="2">
                    <asp:Button ID="btnReject" runat="server" Text="Reject" Width="150" Height="30" Style="margin-top: 25px;"
                        OnClientClick="return confirm('Are you sure you want to Reject this request?');"
                        OnClick="btnReject_Click" />
                </td>
                
            </tr>
            <tr>
                <td colspan="10">
                    <cc2:EmptyDisplayGridView CssClass="tableLightColor" UseAccessibleHeader="true" ShowHeaderWhenEmpty="True"
                        ID="gridHistory" runat="server"  AutoGenerateColumns="False"
                        CellPadding="4" GridLines="None" ShowFooterWhenEmpty="False" >
                        <RowStyle BackColor="#E3EAEB" />
                        <Columns>
                            
                            <asp:BoundField HeaderStyle-Width="160px" HeaderStyle-HorizontalAlign="Left" HeaderText="Modified by"
                                DataField="ModifiedByName"></asp:BoundField>
                            <asp:BoundField DataField="ModifiedOn" DataFormatString="{0:dd-M-yyyy}" HeaderStyle-Width="125px"
                                HeaderStyle-HorizontalAlign="Left" HeaderText="Modified on"></asp:BoundField>

                            <asp:BoundField DataField="Column1Before"  HeaderStyle-Width="160px" HeaderStyle-HorizontalAlign="Left"
                                HeaderText="Before"></asp:BoundField>
                    
                            <asp:BoundField DataField="Column1After"  HeaderStyle-Width="160px" HeaderStyle-HorizontalAlign="Left"
                                HeaderText="After"></asp:BoundField>

                            
                    
                        </Columns>

                        <RowStyle CssClass="odd" />
                        <AlternatingRowStyle CssClass="even" />
                        <EmptyDataTemplate>
                            <b>No list. </b>
                        </EmptyDataTemplate>
                    </cc2:EmptyDisplayGridView>
                </td>
            </tr>
        </table>
        <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator2" ValidationGroup="SaveUpdate"
            ControlToValidate="txtHour" Display="None" ErrorMessage="Approve hour is required."></asp:RequiredFieldValidator>
        <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ValidationGroup="SaveUpdate"
            ControlToValidate="txtMinute" Display="None" ErrorMessage="Approve minute is required."></asp:RequiredFieldValidator>
        <asp:CompareValidator Type="Integer" Operator="GreaterThanEqual" ValueToCompare="0"  runat="server" ID="RequiredFieldValidator3" ValidationGroup="SaveUpdate"
            ControlToValidate="txtHour" Display="None" ErrorMessage="Invalid approve hour."></asp:CompareValidator>
        <asp:CompareValidator Type="Integer" Operator="GreaterThanEqual" ValueToCompare="0"  runat="server" ID="RequiredFieldValidator4" ValidationGroup="SaveUpdate"
            ControlToValidate="txtMinute" Display="None" ErrorMessage="Invalid approve minute."></asp:CompareValidator>
    </div>
</asp:Content>
