﻿<%@ Page Title="Approve Time Attenance" Language="C#" MasterPageFile="~/Master/EmployeeExt.Master" AutoEventWireup="true" CodeBehind="ApproveTimeAttendance.aspx.cs" Inherits="Web.Employee.ApproveTimeAttendance" %>

<%@ Register Src="~/Employee/UserControls/TimeAttendCtrl.ascx" TagName="TimeAttCtrl" TagPrefix="uc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">


<script type="text/javascript" src="<%= ResolveUrl("~/Employee/override.js?v=") + Web.Helper.WebHelper.Version %>"></script>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="mainContent" runat="server">

<br />
<uc:TimeAttCtrl Id="ucTimeAttend" runat="server" />

</asp:Content>
