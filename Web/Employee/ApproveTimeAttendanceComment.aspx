﻿<%@ Page Title="Late Attendance Approval" Language="C#" MasterPageFile="~/Master/EmployeeExt.Master"
    AutoEventWireup="true" CodeBehind="ApproveTimeAttendanceComment.aspx.cs" Inherits="Web.Employee.ApproveTimeAttendanceComment" %>

<%@ Register Src="~/Employee/UserControls/TimeAttendCommentCtrl.ascx" TagName="TimeAttCtrl"
    TagPrefix="uc" %>
<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" src="<%= ResolveUrl("~/Employee/override.js?v=") + Web.Helper.WebHelper.Version %>"></script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="mainContent" runat="server">
    <h4>Late Attendance Approval</h4>
    <uc:TimeAttCtrl Id="ucTimeAttend" runat="server" />
</asp:Content>
