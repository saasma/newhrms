﻿<%@ Page Title="Assign Shift" Language="C#" MasterPageFile="~/Master/EmployeeExt.Master"
    AutoEventWireup="true" CodeBehind="AssignShift.aspx.cs" Inherits="Web.Employee.AssignShift" %>

<%@ Register Src="~/NewHR/UserControls/ShiftPlanningCtrl.ascx" TagName="ShiftPlanCtrl"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="mainContent" runat="server">
    <uc1:ShiftPlanCtrl Id="ShiftPlanCtrol" runat="server" />
</asp:Content>
