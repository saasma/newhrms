﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TimesheetViewCtl.ascx.cs"
    Inherits="Web.Employee.UserControls.TimesheetViewCtl" %>
<script type="text/javascript">
    var getFormattedAmount2 = function (value) {
        var returnValue = "";
        if (parseFloat(value) == 0)
            returnValue = "";
        else
            returnValue = value;
        return returnValue;
    }
    var getFormattedAmount1 = function (value, record, dayIndex) {
        var comment = record.get('D' + dayIndex + 'Comment');
        var isFixed = record.get('D' + dayIndex + 'IsFixed');



        var returnValue = "";
        if (parseFloat(value) == 0)
            returnValue = "";
        else
            returnValue = value;

        if (isFixed) {
            if (comment != "")
                return '<span style="display:block" class="jiraClass" title="Imported from Jira">' + returnValue + '</span>';
            else
                return '<span  style="display:block"  class="jiraClass" title="Imported from Jira, Comment : ' + comment + '">' + returnValue + '</span>';
        }

        if (comment != "") {
            return '<span  style="display:block"  class="commentClass" title="Comment : ' + comment + '">' + returnValue + '</span>';
        }

        return returnValue;
    }
    var totalRenderer = function (value) {
        return "<b class='totalBlock'>" + value + "</b>";
    }
    var getFormattedAmountTotalOrTimecard = function (value, e1, record) {
        if (record.data.Name == "Timecard") {
            if (value == null)
                return "";

            if (value == 0)
                return "";

            var hours = Math.floor(parseFloat(value) / 60);
            var minutes = Math.floor(parseFloat(value) % 60);

            if (minutes.toString().length <= 1)
                minutes = "0" + minutes;

            return hours + ':' + minutes;
        }
        return getFormattedAmount2(value);
    }
    
</script>
<style type="text/css">
    thead td, th
    {
        border: 0px solid;
    }
    .total
    {
        width: 90%;
        text-align: Center;
        display: block;
        padding-right: 5px;
    }
    .jiraClass
    {
        font-weight: bolder;
    }
    .commentClass
    {
        color: blue;
        display: block;
    }
    
    .tableLightColor1 > tr > td
    {
        padding: 0px !important;
    }
    
    .x-grid-row .x-grid-cell
    {
        border-right-color: #EDEDED;
        border-right-style: solid;
        border-right-width: 1px !important;
    }
    .weeklyColor
    {
        background: #FABF8F !important;
    }
    .totalBlock
    {
        display: block;
        width: 100% !important;
        text-align: right;
    }
    .x-grid-header-ct
    {
        border: 1px solid #A3BAD9;
    }
    .x-panel-default-outer-border-rbl
    {
        border-top-color: #A3BAD9 !important;
        border-bottom-color: #A3BAD9 !important;
        border-right-color: #A3BAD9 !important;
        border-left-color: #A3BAD9 !important;
    }
    .x-panel-default-outer-border-trl
    {
        border-top-color: #A3BAD9 !important;
        border-right-color: #A3BAD9 !important;
        border-left-color: #A3BAD9 !important;
    }
    .x-column-header-inner
    {
        padding-top: 5px;
        padding-bottom: 5px;
    }
    .weekInfo
    {
        font-weight: bold !important;
        font-size: 15px;
    }
    .x-column-header-text
    {
        font-size: 12px;
    }
</style>
<table>
    <tr>
        <td valign="top" style="padding-left: 10px">
            <ext:DisplayField ID="dispDate" FieldCls="weekInfo" runat="server">
            </ext:DisplayField>
            <ext:GridPanel AutoHeight="true" Border="true" Width="900" ID="gridProjects" runat="server">
                <Store>
                    <ext:Store runat="server" ID="storeRequest">
                        <Model>
                            <ext:Model ID="Model1" runat="server" Name="WeekModel">
                                <Fields>
                                    <ext:ModelField Name="WeekStartDate" Type="String" />
                                    <ext:ModelField Name="Name" Type="String" />
                                    <ext:ModelField Name="Activity" Type="String" />
                                    <ext:ModelField Name="Category" Type="String" />
                                    <ext:ModelField Name="D1" Type="Float" />
                                    <ext:ModelField Name="D1Comment" Type="String" />
                                    <ext:ModelField Name="D1IsFixed" Type="Boolean" />
                                    <ext:ModelField Name="D2" Type="Float" />
                                    <ext:ModelField Name="D2Comment" Type="String" />
                                    <ext:ModelField Name="D2IsFixed" Type="Boolean" />
                                    <ext:ModelField Name="D3" Type="Float" />
                                    <ext:ModelField Name="D3Comment" Type="String" />
                                    <ext:ModelField Name="D3IsFixed" Type="Boolean" />
                                    <ext:ModelField Name="D4" Type="Float" />
                                    <ext:ModelField Name="D4Comment" Type="String" />
                                    <ext:ModelField Name="D4IsFixed" Type="Boolean" />
                                    <ext:ModelField Name="D5" Type="Float" />
                                    <ext:ModelField Name="D5Comment" Type="String" />
                                    <ext:ModelField Name="D5IsFixed" Type="Boolean" />
                                    <ext:ModelField Name="Total" Type="Float" />
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                <View>
                    <ext:GridView ID="GridView1" runat="server">
                    </ext:GridView>
                </View>
                <ColumnModel runat="server" ID="columnmodle1">
                    <Columns>
                        <ext:Column ID="Column1" Sortable="false" MenuDisabled="true" StyleSpec="font-weight:bold"
                            Width="160" runat="server" Header="Project" DataIndex="Name">
                        </ext:Column>
                        <ext:Column ID="Column4" Sortable="false" MenuDisabled="true" StyleSpec="font-weight:bold"
                            Width="160" runat="server" Header="Activity" DataIndex="Activity">
                        </ext:Column>
                        <ext:Column ID="Column5" Sortable="false" MenuDisabled="true" StyleSpec="font-weight:bold"
                            Width="160" runat="server" Header="Category" DataIndex="Category">
                        </ext:Column>
                        <ext:Column ID="D1" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Week 1" Width="70" DataIndex="D1">
                            <Renderer Handler="return getFormattedAmount1(value,record,1)" />
                        </ext:Column>
                        <ext:Column ID="D2" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Week 2" Width="70" DataIndex="D2">
                            <Renderer Handler="return getFormattedAmount1(value,record,2)" />
                        </ext:Column>
                        <ext:Column ID="D3" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Week 3" Width="70" DataIndex="D3">
                            <Renderer Handler="return getFormattedAmount1(value,record,3)" />
                        </ext:Column>
                        <ext:Column ID="D4" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Week 4" Width="70" DataIndex="D4">
                            <Renderer Handler="return getFormattedAmount1(value,record,4)" />
                        </ext:Column>
                        <ext:Column ID="D5" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Week 5" Width="70" DataIndex="D5">
                            <Renderer Handler="return getFormattedAmount1(value,record,5)" />
                        </ext:Column>
                        <ext:Column ID="Column2" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Total" Width="70" DataIndex="Total">
                            <Renderer Fn="getFormattedAmount2" />
                        </ext:Column>
                    </Columns>
                </ColumnModel>
                <SelectionModel>
                    <ext:CellSelectionModel EnableKeyNav="true" runat="server" ID="CellSelectionModel_ItemGrid">
                    </ext:CellSelectionModel>
                </SelectionModel>
            </ext:GridPanel>
            <ext:GridPanel AutoHeight="true" DisableSelection="true" HideHeaders="true" Border="false"
                Width="900" ID="gridProjectsTotal" runat="server">
                <Store>
                    <ext:Store runat="server" ID="store2">
                        <Model>
                            <ext:Model ID="Model7" runat="server" Name="WeekModelTotal">
                                <Fields>
                                    <ext:ModelField Name="Name" Type="String" />
                                    <ext:ModelField Name="D1" Type="Float" />
                                    <ext:ModelField Name="D2" Type="Float" />
                                    <ext:ModelField Name="D3" Type="Float" />
                                    <ext:ModelField Name="D4" Type="Float" />
                                    <ext:ModelField Name="D5" Type="Float" />
                                    <ext:ModelField Name="Total" Type="Float" />
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                <ColumnModel runat="server" ID="ColumnModel2">
                    <Columns>
                        <ext:Column ID="Column6" MenuDisabled="true" StyleSpec="font-weight:bold" Width="480"
                            runat="server" DataIndex="Name">
                            <Renderer Fn="totalRenderer" />
                        </ext:Column>
                        <ext:Column ID="Column9" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="70" DataIndex="D1">
                            <Renderer Fn="getFormattedAmountTotalOrTimecard" />
                        </ext:Column>
                        <ext:Column ID="Column10" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="70" DataIndex="D2">
                            <Renderer Fn="getFormattedAmountTotalOrTimecard" />
                        </ext:Column>
                        <ext:Column ID="Column12" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="70" DataIndex="D3">
                            <Renderer Fn="getFormattedAmountTotalOrTimecard" />
                        </ext:Column>
                        <ext:Column ID="Column13" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="70" DataIndex="D4">
                            <Renderer Fn="getFormattedAmountTotalOrTimecard" />
                        </ext:Column>
                        <ext:Column ID="Column14" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="70" DataIndex="D5">
                            <Renderer Fn="getFormattedAmountTotalOrTimecard" />
                        </ext:Column>
                        <ext:Column ID="Column17" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Total" Width="70" DataIndex="Total">
                            <Renderer Fn="getFormattedAmountTotalOrTimecard" />
                        </ext:Column>
                    </Columns>
                </ColumnModel>
                <SelectionModel>
                    <ext:CellSelectionModel EnableKeyNav="true" runat="server" ID="CellSelectionModel2">
                    </ext:CellSelectionModel>
                </SelectionModel>
                <View>
                    <ext:GridView ID="GridView4" StripeRows="false" runat="server">
                    </ext:GridView>
                </View>
            </ext:GridPanel>
            <ext:GridPanel AutoHeight="true" StyleSpec="margin-top:25px" DisableSelection="true"
                Border="true" Width="900" ID="gridLeaves" runat="server">
                <Store>
                    <ext:Store runat="server" ID="store1">
                        <Model>
                            <ext:Model ID="Model3" runat="server" Name="WeekModelLeave">
                                <Fields>
                                    <ext:ModelField Name="Id" Type="String" />
                                    <ext:ModelField Name="Name" Type="String" />
                                    <ext:ModelField Name="L1" Type="Float" />
                                    <ext:ModelField Name="L2" Type="Float" />
                                    <ext:ModelField Name="L3" Type="Float" />
                                    <ext:ModelField Name="L4" Type="Float" />
                                    <ext:ModelField Name="L5" Type="Float" />
                                    <ext:ModelField Name="Total" Type="Float" />
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                <View>
                    <ext:GridView ID="GridView2" runat="server">
                    </ext:GridView>
                </View>
                <ColumnModel runat="server" ID="ColumnModel1">
                    <Columns>
                        <ext:Column ID="Column3" Sortable="false" MenuDisabled="true" StyleSpec="font-weight:bold"
                            Width="480" runat="server" Header="Leaves" DataIndex="Name">
                        </ext:Column>
                        <ext:Column ID="L1" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Week 1" Width="70" DataIndex="L1">
                            <Renderer Fn="getFormattedAmount2" />
                        </ext:Column>
                        <ext:Column ID="L2" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Week 2" Width="70" DataIndex="L2">
                            <Renderer Fn="getFormattedAmount2" />
                        </ext:Column>
                        <ext:Column ID="L3" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Week 3" Width="70" DataIndex="L3">
                            <Renderer Fn="getFormattedAmount2" />
                        </ext:Column>
                        <ext:Column ID="L4" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Week 4" Width="70" DataIndex="L4">
                            <Renderer Fn="getFormattedAmount2" />
                        </ext:Column>
                        <ext:Column ID="L5" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Week 5" Width="70" DataIndex="L5">
                            <Renderer Fn="getFormattedAmount2" />
                        </ext:Column>
                        <ext:Column ID="Column11" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Total" Width="70" DataIndex="Total">
                            <Renderer Fn="getFormattedAmount2" />
                        </ext:Column>
                    </Columns>
                </ColumnModel>
                <SelectionModel>
                    <ext:CellSelectionModel ID="CellSelectionModel1" runat="server" Mode="Simple" />
                </SelectionModel>
            </ext:GridPanel>
            <ext:GridPanel AutoHeight="true" DisableSelection="true" HideHeaders="true" Border="false"
                Width="900" ID="gridLeavesTotal" runat="server">
                <Store>
                    <ext:Store runat="server" ID="store3">
                        <Model>
                            <ext:Model ID="Model8" runat="server" Name="WeekModelTotal">
                                <Fields>
                                    <ext:ModelField Name="Name" Type="String" />
                                    <ext:ModelField Name="D1" Type="Float" />
                                    <ext:ModelField Name="D2" Type="Float" />
                                    <ext:ModelField Name="D3" Type="Float" />
                                    <ext:ModelField Name="D4" Type="Float" />
                                    <ext:ModelField Name="D5" Type="Float" />
                                    <ext:ModelField Name="Total" Type="Float" />
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                <ColumnModel runat="server" ID="ColumnModel3">
                    <Columns>
                        <ext:Column ID="Column20" MenuDisabled="true" StyleSpec="font-weight:bold" Width="480"
                            runat="server" DataIndex="Name">
                            <Renderer Fn="totalRenderer" />
                        </ext:Column>
                        <ext:Column ID="Column23" Cls="weeklyColor" runat="server" Sortable="false" Align="Center"
                            MenuDisabled="true" Text="Name" Width="70" DataIndex="D1">
                            <Renderer Fn="getFormattedAmount2" />
                        </ext:Column>
                        <ext:Column ID="Column24" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="70" DataIndex="D2">
                            <Renderer Fn="getFormattedAmount2" />
                        </ext:Column>
                        <ext:Column ID="Column25" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="70" DataIndex="D3">
                            <Renderer Fn="getFormattedAmount2" />
                        </ext:Column>
                        <ext:Column ID="Column26" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="70" DataIndex="D4">
                            <Renderer Fn="getFormattedAmount2" />
                        </ext:Column>
                        <ext:Column ID="Column27" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="70" DataIndex="D5">
                            <Renderer Fn="getFormattedAmount2" />
                        </ext:Column>
                        <ext:Column ID="Column30" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Total" Width="70" DataIndex="Total">
                            <Renderer Fn="getFormattedAmount2" />
                        </ext:Column>
                    </Columns>
                </ColumnModel>
                <SelectionModel>
                    <ext:CellSelectionModel EnableKeyNav="true" runat="server" ID="CellSelectionModel3">
                    </ext:CellSelectionModel>
                </SelectionModel>
                <View>
                    <ext:GridView ID="GridView3" StripeRows="false" runat="server">
                    </ext:GridView>
                </View>
            </ext:GridPanel>
        </td>
    </tr>
    <tr>
        <td>
            <ext:DisplayField ID="lblApproval" runat="server" StyleSpec="margin-left:10px" FieldLabel="Approval">
            </ext:DisplayField>
        </td>
    </tr>
</table>
