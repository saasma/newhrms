﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using BLL.Manager;
using BLL.BO;
using BLL;
using DAL;
using BLL.Entity;
using Utils.Calendar;
using Web.CP.Report.Templates.HR;
using System.IO;
using Utils;

namespace Web.Employee.UserControls
{
    public partial class TimesheetViewCtl : BaseUserControl
    {
        /// <summary>
       

    
        

       
  
        public void LoadTimesheet(int timesheetId)
        {
            DAL.Timesheet timesheet = NewTimeSheetManager.GetTimeSheetById(timesheetId);

            DateTime date = Convert.ToDateTime(timesheet.StartDate);

            DateTime weekStartDate = NewTimeSheetManager.GetMonthStartDate(date);


            string rejectionNotes = "";

           
            TimeSheetStatus status = NewTimeSheetManager.GetTimesheetStatus(weekStartDate,
                timesheet.EmployeeId, ref rejectionNotes, ref timesheet);

            if (timesheet != null && timesheet.ApprovedEmployeeId != null)
            {
                lblApproval.Text = new EmployeeManager().GetById(timesheet.ApprovedEmployeeId.Value).Name;
            }
            else
            {
                lblApproval.Text = "";
            }

           

            //DateTime weekEndDate = weekStartDate.AddDays(6);
            dispDate.Text = "Month : " + DateHelper.GetMonthName(weekStartDate.Month, true) + " " + weekStartDate.Year
                + ", Status : " + (status == TimeSheetStatus.NotSubmitted ? "Not Filled" : status.ToString());

            if (status == TimeSheetStatus.Rejected)
            {
              
                dispDate.Text += ", Rejection Notes : " + rejectionNotes;
            }
        
            List<TimesheetGridNewBO> projectList = NewTimeSheetManager.GetRepeatedProjectList(weekStartDate,
                timesheet.EmployeeId);

            gridProjects.Store[0].DataSource = projectList;
            gridProjects.Store[0].DataBind();

            TimesheetGridNewBO total = new TimesheetGridNewBO{Name="Total"};
            List<TimesheetGridNewBO> totalList = new List<TimesheetGridNewBO>();
            totalList.Add(total);           
            
            total.D1 = projectList.Sum(x => x.D1 == null ? 0 : x.D1.Value);
            total.D2 = projectList.Sum(x => x.D2 == null ? 0 : x.D2.Value);
            total.D3 = projectList.Sum(x => x.D3 == null ? 0 : x.D3.Value);
            total.D4 = projectList.Sum(x => x.D4 == null ? 0 : x.D4.Value);
            total.D5 = projectList.Sum(x => x.D5 == null ? 0 : x.D5.Value);
           
            total.Total= projectList.Sum(x => x.Total == null ? 0 : x.Total.Value);

            // add time card row
            //TimesheetGridNewBO timecard = NewTimeSheetManager.GetTimecard(
            //    this.ReadonlyEmployeeId == 0 ? this.EmployeeId : this.ReadonlyEmployeeId, weekStartDate);
            //totalList.Add(timecard);

            gridProjectsTotal.Store[0].DataSource = totalList;
            gridProjectsTotal.Store[0].DataBind();


            List<TimesheetGridNewBO> leaveList = NewTimeSheetManager.GetLeaveList(weekStartDate, timesheet.EmployeeId);
            foreach (TimesheetGridNewBO item in leaveList)
            {
                item.L1 = item.D1;
                item.L2 = item.D2;
                item.L3 = item.D3;
                item.L4 = item.D4;
                item.L5 = item.D5;
            }
            gridLeaves.Store[0].DataSource = leaveList;
            gridLeaves.Store[0].DataBind();


            TimesheetGridNewBO leavetotal = new TimesheetGridNewBO { Name = "Total" };
            List<TimesheetGridNewBO> totalLeaveList = new List<TimesheetGridNewBO>();
            totalLeaveList.Add(leavetotal);

            leavetotal.D1 = leaveList.Sum(x => x.L1 == null ? 0 : x.L1.Value);
            leavetotal.D2 = leaveList.Sum(x => x.L2 == null ? 0 : x.L2.Value);
            leavetotal.D3 = leaveList.Sum(x => x.L3 == null ? 0 : x.L3.Value);
            leavetotal.D4 = leaveList.Sum(x => x.L4 == null ? 0 : x.L4.Value);
            leavetotal.D5 = leaveList.Sum(x => x.L5 == null ? 0 : x.L5.Value);
            leavetotal.Total = leaveList.Sum(x => x.Total == null ? 0 : x.Total.Value);

          


            TimesheetGridNewBO grandTotal = new TimesheetGridNewBO { Name = "Grand Total" };
            totalLeaveList.Add(grandTotal);

            grandTotal.D1 = projectList.Sum(x => x.D1 == null ? 0 : x.D1.Value)+ leaveList.Sum(x => x.L1 == null ? 0 : x.L1.Value);
            grandTotal.D2 = projectList.Sum(x => x.D2 == null ? 0 : x.D2.Value) + leaveList.Sum(x => x.L2 == null ? 0 : x.L2.Value);
            grandTotal.D3 = projectList.Sum(x => x.D3 == null ? 0 : x.D3.Value) + leaveList.Sum(x => x.L3 == null ? 0 : x.L3.Value);
            grandTotal.D4 = projectList.Sum(x => x.D4 == null ? 0 : x.D4.Value) + leaveList.Sum(x => x.L4 == null ? 0 : x.L4.Value);
            grandTotal.D5 = projectList.Sum(x => x.D5 == null ? 0 : x.D5.Value) + leaveList.Sum(x => x.L5 == null ? 0 : x.L5.Value);
            grandTotal.Total = projectList.Sum(x => x.Total == null ? 0 : x.Total.Value) +leaveList.Sum(x => x.Total == null ? 0 : x.Total.Value);

            gridLeavesTotal.Store[0].DataSource = totalLeaveList;
            gridLeavesTotal.Store[0].DataBind();
        }

        private void Initialise()
        {
            

        }

//        protected void Click_Export(object sender, EventArgs e)
//        {

//            bool isPDF = (sender == btnExportPDF) ? true : false;


//            DAL.Timesheet timesheet = null;
//            string rejectionNotes = "";
//            DateTime weekStart = Convert.ToDateTime(hiddenWeekStartDateExcel.Text);
//            weekStart = new DateTime(weekStart.Year, weekStart.Month, 1);

//            TimeSheetStatus status = NewTimeSheetManager.GetTimesheetStatus(weekStart,
//                this.ReadonlyEmployeeId == 0 ? this.EmployeeId : this.ReadonlyEmployeeId, ref rejectionNotes, ref timesheet);

//            if (timesheet == null)
//            {
//                NewMessage.ShowNormalMessage("Timesheet not saved.");
//                return;
//            }

            
//            PAReport report = GetPAR();
//            GetSubReportPAR();
//          //  PAReportTimeAllocationInfo PASubReport = GetSubReportPAR();

//            using (MemoryStream stream = new MemoryStream())
//            {
//                if (isPDF)
//                    report.ExportToPdf(stream);
//                else
//                    report.ExportToXls(stream);

//                HttpContext.Current.Response.Clear();
//                if (isPDF)
//                {

//                    HttpContext.Current.Response.ContentType = "application/pdf";
//                    HttpContext.Current.Response.AppendHeader("Content-Type", "application/pdf");

//                    HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment; filename=\"" + report.labelTitle.Text + " Statement.pdf\"");

//                }
//                else
//                {
//                    report.ExportToXls(stream);
//                    HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";
//                    HttpContext.Current.Response.AppendHeader("Content-Type", "application/vnd.ms-excel");

//                    HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment; filename=\"" + report.labelTitle.Text + " Statement.xls\"");
//                }

//                HttpContext.Current.Response.AddHeader("Content-Length", stream.ToArray().Length.ToString());

//                HttpContext.Current.Response.OutputStream.Write(stream.ToArray(), 0, stream.ToArray().Length);
//                HttpContext.Current.Response.End();
//            }
//        }

//        private PAReport GetPAR()
//        {

//            List<GetPAREmployeeHeaderResult> data = EmployeeManager.GetPAREmployeeHeader(this.ReadonlyEmployeeId == 0 ? this.EmployeeId : this.ReadonlyEmployeeId).ToList();

           


//            ReportDataSetTableAdapters.GetPAREmployeeHeaderTableAdapter adap =
//                new ReportDataSetTableAdapters.GetPAREmployeeHeaderTableAdapter();
//            ReportDataSet ds = new ReportDataSet();

//            adap.Connection.ConnectionString = Config.ConnectionString;
//            ReportDataSet.GetPAREmployeeHeaderDataTable table = adap.GetData(this.ReadonlyEmployeeId == 0 ? this.EmployeeId : this.ReadonlyEmployeeId);

//            // Process for report
//            PAReport report = new PAReport();
//            report.DataSource = table;
//            report.DataMember = "Report";
////            report.labelTitle.Text = "";

            

//            return report;
//        }


//        private void GetSubReportPAR()
//        {
            


//            DateTime weekStart = Convert.ToDateTime(hiddenWeekStartDateExcel.Text);
//            weekStart = new DateTime(weekStart.Year, weekStart.Month, 1);

            
//            DAL.Timesheet timesheet = null;
//            string rejectionNotes = "";
//            TimeSheetStatus status = NewTimeSheetManager.GetTimesheetStatus(weekStart,
//                this.ReadonlyEmployeeId == 0 ? this.EmployeeId : this.ReadonlyEmployeeId, ref rejectionNotes, ref timesheet);

         
//            DateTime monthEndDate = new DateTime(weekStart.Year, weekStart.Month,
//            DateTime.DaysInMonth(weekStart.Year, weekStart.Month));

//            DateTime week1 = weekStart;
//            DateTime week2 = week1.AddDays(7);
//            DateTime week3 =week2.AddDays(7);
//            DateTime week4 = week3.AddDays(7);
//            DateTime week5 = week4.AddDays(7);

//            if (week4 < monthEndDate)
//            {
//                week5 = week4.AddDays(7);
//            }

//          //  List<GetTimeAllocationInfoByWeekResult> data = EmployeeManager.ReportGetTimeAllocationInfoByWeek(this.ReadonlyEmployeeId == 0 ? this.EmployeeId : this.ReadonlyEmployeeId,week1,week2,week3,week4,week5);

//            ReportDataSetTableAdapters.GetTimeAllocationInfoByWeekTableAdapter adap =
//                new ReportDataSetTableAdapters.GetTimeAllocationInfoByWeekTableAdapter();
//            ReportDataSet ds = new ReportDataSet();


//            adap.Connection.ConnectionString = Config.ConnectionString;
//            ReportDataSet.GetTimeAllocationInfoByWeekDataTable table = adap.GetData(this.ReadonlyEmployeeId == 0 ? this.EmployeeId : this.ReadonlyEmployeeId, week1, week2, week3, week4, week5, monthEndDate, timesheet.TimesheetId);
//            HttpContext.Current.Items["PARTimeAllocationInfo"] = table;

//            //store Employee Leave information

//            ReportDataSetTableAdapters.ReportGetEmployeeLeaveListTableAdapter adap1 =
//                new ReportDataSetTableAdapters.ReportGetEmployeeLeaveListTableAdapter();
//            ReportDataSet ds1 = new ReportDataSet();


//            adap.Connection.ConnectionString = Config.ConnectionString;
//            ReportDataSet.ReportGetEmployeeLeaveListDataTable table1 = adap1.GetData(this.ReadonlyEmployeeId == 0 ? this.EmployeeId : this.ReadonlyEmployeeId, timesheet.TimesheetId);
//            HttpContext.Current.Items["PAREmployeeLeaveInfo"] = table1;
//            //-----------------------------



//        }


    }
}
