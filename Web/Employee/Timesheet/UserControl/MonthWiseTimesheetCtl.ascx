﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MonthWiseTimesheetCtl.ascx.cs"
    Inherits="Web.Employee.UserControls.MonthWiseTimesheetCtl" %>
<style type="text/css">
    .x-grid-cell-inner
    {
        padding: 3px !important;
    }
    
    .x-panel .x-form-text
    {
        padding: 1px !important;
    }
</style>
<script type="text/javascript">
    //method that will be called when item selected changes for Item in the GridPanel




    var projectRenderer = function (value) {
        
        if(value=="" || value==null)
          return "<b class='totalBlock'> Total </b>";
        var r = <%=storeProjects.ClientID %>.getById(value);
        if (Ext.isEmpty(r)) {
            return "";
        }
        return r.data.Name
    };
  
  
    var leaveRenderer = function (value) {

      if(value==-99)
          return "<b class='totalBlock'> Total </b>";
            if(value==-100)
          return "<b class='totalBlock'> Grand Total </b>";

        var r = <%=storeLeaves.ClientID %>.getById(value);
      
        if (Ext.isEmpty(r)) {
            if(parseFloat(value)==0 || parseFloat(value)==-1)
                return "Unpaid Leave";
             if(parseFloat(value)==-2)
                return "Public Holiday";
            return "";
        }

        return r.data.Title;
    };

    var record;
    var dayField;
    var taskName = "";
    var taskId = "";
    
    var beforeEdit = function(e1,e) 
    {


        var varColumID = <%=gridProjects.ClientID %>.columns[e.colIdx].id;

        if(document.getElementById(varColumID).className.indexOf("weeklyColor") > -1)
        {
          e.cancel = true;
                return;
        }

        var lastIndexOfProjectGrid = (<%=gridProjects.ClientID %>.getStore().getCount())-1;
        if(e.rowIdx.toString()==lastIndexOfProjectGrid.toString())
        {
             e.cancel = true;
            return;
        }


          var HolidayIndexOfLeaveGrid = (<%=gridLeaves.ClientID %>.getStore().getCount())-2;
          var PublicHolidayrec = <%=gridLeaves.ClientID %>.getStore().getAt(HolidayIndexOfLeaveGrid);
          if(PublicHolidayrec.data[e.field]=="8" )
          {
            var val1 = e.record.data[e.field];
            var val2 = 0;
            try{
                val2 = parseFloat(val1);
            }
            catch(e){}

            if(val2 == 0)
            {

                e.cancel = true;
                return;
            }
          }
          
            if(IsViewMode)
            {
                 e.cancel = true;
                return;
            }

            record = e.record;
            dayField = e.field;
      



       
    }

    var MoreHandler1 = function (column, command, record, recordIndex, cellIndex) {

        var store = this.grid.store;
        store.remove(record);
        setTotal(null);
    };

    
    var afterEdit = function (e1, e2, e3, e4,e5,e6) {
        if (taskId != "")
            e2.record.data.TaskId = taskId;
        e2.record.commit();
        setTotal(e2.record);  
    }

   
    var totalRenderer = function(value)
    {
        return "<b class='totalBlock'>"+ value + "</b>";
    }
    var setTotal = function (currentRow) {
           
        var records = <%=gridProjects.ClientID %>.getStore();
        var lastIndexOfProjectGrid = (<%=gridProjects.ClientID %>.getStore().getCount())-1;
        var lastIndexOfLeaveGrid = (<%=gridLeaves.ClientID %>.getStore().getCount())-1;
        var i = 0,
        length = records.data.length,
        total = 0,
        grandTotal = 0,
        record;

        
        var D1 = 0;
        var D2 = 0;
        var D3 = 0;
        var D4 = 0;
        var D5 = 0;
        var D6 = 0;
        var D7 = 0;
        var D8 = 0;
        var D9 = 0;
        var D10 = 0;
        var D11 = 0;
        var D12 = 0;
        var D13 = 0;
        var D14 = 0;
        var D15 = 0;
        var D16 = 0;
        var D17 = 0;
        var D18 = 0;
        var D19 = 0;
        var D20 = 0;
        var D21 = 0;
        var D22 = 0;
        var D23 = 0;
        var D24 = 0;
        var D25 = 0;
        var D26 = 0;
        var D27 = 0;
        var D28 = 0;
        var D29 = 0;
        var D30 = 0;
        var D31 = 0;
        var D32 = 0;
        var total = 0;

        for (; i < length-1; ++i) {
            record = records.data.items[i];

            D1 +=  record.data.D1;
            D2 +=  record.data.D2;
            D3 +=  record.data.D3;
            D4 +=  record.data.D4;
            D5 +=  record.data.D5;
            D6 +=  record.data.D6;
            D7 +=  record.data.D7;
            D8 +=  record.data.D8;
            D9 +=  record.data.D9;
            D10 +=  record.data.D10;
            D11 +=  record.data.D11;
            D12 +=  record.data.D12;
            D13 +=  record.data.D13;
            D14 +=  record.data.D14;
            D15 +=  record.data.D15;
            D16 +=  record.data.D16;
            D17 +=  record.data.D17;
            D18 +=  record.data.D18;
            D19 +=  record.data.D19;
            D20 +=  record.data.D20;
            D21 +=  record.data.D21;
            D22 +=  record.data.D22;
            D23 +=  record.data.D23;
            D24 +=  record.data.D24;
            D25 +=  record.data.D25;
            D26 +=  record.data.D26;
            D27 +=  record.data.D27;
            D28 +=  record.data.D28;
            D29 +=  record.data.D29;
            D30 +=  record.data.D30;
            D31 +=  record.data.D31;
            D32 +=  record.data.D32;
           
            
            total = record.data.D1 + record.data.D2+ record.data.D3+ record.data.D4
                    + record.data.D5+ record.data.D6+ record.data.D7+ record.data.D8+ record.data.D9+ record.data.D10+ record.data.D11+ record.data.D12+ record.data.D13
                    + record.data.D14+ record.data.D15+ record.data.D16+ record.data.D17+ record.data.D18+ record.data.D19+ record.data.D20+ record.data.D21
                    + record.data.D22+ record.data.D23+ record.data.D24+ record.data.D25+ record.data.D26+ record.data.D27+ record.data.D28+ record.data.D29
                    + record.data.D30+ record.data.D31+ record.data.D32;

            record.data.Total = total;
            grandTotal += total;
            
        }            

        
       

       var totalRecord = <%=gridProjects.ClientID%>.getStore().data.items[lastIndexOfProjectGrid];
        totalRecord.data.D1 = D1;
        totalRecord.data.D2 = D2;
        totalRecord.data.D3 = D3;
        totalRecord.data.D4 = D4;
        totalRecord.data.D5 = D5;
        totalRecord.data.D6 = D6;
        totalRecord.data.D7 = D7;
        totalRecord.data.D8 = D8;
        totalRecord.data.D9 = D9;
        totalRecord.data.D10 = D10;
        totalRecord.data.D11 = D11;
        totalRecord.data.D12 = D12;
        totalRecord.data.D13 = D13;
        totalRecord.data.D14 = D14;
        totalRecord.data.D15 = D15;
        totalRecord.data.D16 = D16;
        totalRecord.data.D17 = D17;
        totalRecord.data.D18 = D18;
        totalRecord.data.D19 = D19;
        totalRecord.data.D20 = D20;
        totalRecord.data.D21 = D21;
        totalRecord.data.D22 = D22;
        totalRecord.data.D23 = D23;
        totalRecord.data.D24 = D24;
        totalRecord.data.D25 = D25;
        totalRecord.data.D26 = D26;
        totalRecord.data.D27 = D27;
        totalRecord.data.D28 = D28;
        totalRecord.data.D29 = D29;
        totalRecord.data.D30 = D30;
        totalRecord.data.D31 = D31;
        totalRecord.data.D32 = D32;
        totalRecord.data.Total = grandTotal;            
        totalRecord.commit();
       
        if(currentRow != null)
            currentRow.commit();


        // set Grand Total
       // var totalLeave = <%=gridLeavesTotal.ClientID%>.getStore().data.items[0];
        var totalLeave = <%=gridLeaves.ClientID%>.getStore().data.items[lastIndexOfLeaveGrid-1];
       // var grandTotal = <%=gridLeavesTotal.ClientID%>.getStore().data.items[1];
        var grandTotal = <%=gridLeaves.ClientID%>.getStore().data.items[lastIndexOfLeaveGrid];//last row of leaves is the grand total

        grandTotal.data.D1 = totalRecord.data.D1 + totalLeave.data.D1;
        grandTotal.data.D2 = totalRecord.data.D2 + totalLeave.data.D2;
        grandTotal.data.D3 = totalRecord.data.D3 + totalLeave.data.D3;
        grandTotal.data.D4 = totalRecord.data.D4 + totalLeave.data.D4;
        grandTotal.data.D5 = totalRecord.data.D5 + totalLeave.data.D5;
        grandTotal.data.D6 = totalRecord.data.D6 + totalLeave.data.D6;
        grandTotal.data.D7 = totalRecord.data.D7 + totalLeave.data.D7;
        grandTotal.data.D8 = totalRecord.data.D8 + totalLeave.data.D8;
        grandTotal.data.D9 = totalRecord.data.D9 + totalLeave.data.D9;
        grandTotal.data.D10 = totalRecord.data.D10 + totalLeave.data.D10;
        grandTotal.data.D11 = totalRecord.data.D11 + totalLeave.data.D11;
        grandTotal.data.D12 = totalRecord.data.D12 + totalLeave.data.D12;
        grandTotal.data.D13 = totalRecord.data.D13 + totalLeave.data.D13;
        grandTotal.data.D14 = totalRecord.data.D14 + totalLeave.data.D14;
        grandTotal.data.D15 = totalRecord.data.D15 + totalLeave.data.D15;
        grandTotal.data.D16 = totalRecord.data.D16 + totalLeave.data.D16;
        grandTotal.data.D17 = totalRecord.data.D17 + totalLeave.data.D17;
        grandTotal.data.D18 = totalRecord.data.D18 + totalLeave.data.D18;
        grandTotal.data.D19 = totalRecord.data.D19 + totalLeave.data.D19;
        grandTotal.data.D20 = totalRecord.data.D20 + totalLeave.data.D20;
        grandTotal.data.D21 = totalRecord.data.D21 + totalLeave.data.D21;
        grandTotal.data.D22 = totalRecord.data.D22 + totalLeave.data.D22;
        grandTotal.data.D23 = totalRecord.data.D23 + totalLeave.data.D23;
        grandTotal.data.D24 = totalRecord.data.D24 + totalLeave.data.D24;
        grandTotal.data.D25 = totalRecord.data.D25 + totalLeave.data.D25;
        grandTotal.data.D26 = totalRecord.data.D26 + totalLeave.data.D26;
        grandTotal.data.D27 = totalRecord.data.D27 + totalLeave.data.D27;
        grandTotal.data.D28 = totalRecord.data.D28 + totalLeave.data.D28;
        grandTotal.data.D29 = totalRecord.data.D29 + totalLeave.data.D29;
        grandTotal.data.D30 = totalRecord.data.D30 + totalLeave.data.D30;
        grandTotal.data.D31 = totalRecord.data.D31 + totalLeave.data.D31;
        grandTotal.data.D32 = totalRecord.data.D32 + totalLeave.data.D32;


        grandTotal.data.Total = totalRecord.data.Total + totalLeave.data.Total;      
        grandTotal.commit();
    }

  
    var projectDisplay = function (value, rowIndex, comboBox,e1,e3) {
        
        taskName = '';
        taskId = null;

         var currentRowData = <%=storeRequest.ClientID %>.data.items[rowIndex];
         currentRowData.data.TaskId = taskId;
         currentRowData.data.TaskName = taskName;
         currentRowData.data.SubProjectId = null;
    }
   
    var afterEditLeave = function (e1, e2, e3, e4) {
        e2.record.commit();          
    }

    var prepareCommand = function (grid, command, record, row) {
            // you can prepare group command
        if (record.data.D1IsFixed == true || record.data.D2IsFixed == true || record.data.D3IsFixed == true 
            || record.data.D4IsFixed == true || record.data.D5IsFixed == true || record.data.D6IsFixed == true 
            || record.data.D7IsFixed == true )
        {
            command.hidden = true;
        }

    };


    var getFormattedAmountTotalOrTimecard = function(value,e1,record)
    {
        if(record.data.Name=="Timecard")
        {
            if(value==null)
                return "";

            if(value==0)
                return "";

            var hours = Math.floor( parseFloat(value) / 60);          
            var minutes = Math.floor( parseFloat(value) % 60);

            if(minutes.toString().length<=1)
                minutes = "0" + minutes;

            return  hours + ':' + minutes;
        }
        return getFormattedAmount2(value);
    }
    var getFormattedAmount2 = function(value)
    {
        var returnValue = "";
        if(parseFloat(value)==0)
            returnValue = "";
        else
            returnValue = value;
        return returnValue;
    }


    var getFormattedAmount1= function(value,record,dayIndex)
    {            
//            var comment = record.get('D' + dayIndex + 'Comment');
//            var isFixed = record.get('D' + dayIndex + 'IsFixed');
            
            
                       
            var returnValue = "";
            if(parseFloat(value)==0)
                returnValue = "";
            else
                returnValue = value;

//            if(isFixed)
//            {
//                if( comment != "")
//                    return '<span style="display:block" class="jiraClass" title="Imported from Jira">' + returnValue +'</span>';
//                else
//                    return '<span  style="display:block"  class="jiraClass" title="Imported from Jira, Comment : ' + comment + '">' + returnValue +'</span>';
//            }

//            if(comment != "")
//            {
//                return '<span  style="display:block"  class="commentClass" title="Comment : ' + comment + '">' + returnValue +'</span>';
//            }
        
            return returnValue;
    }

    Date.prototype.getShortDate = function(){
        return this.getFullYear() + 
        "/" +  (this.getMonth() + 1) +
        "/" +  this.getDate();
    }



    
function selectCurrentCell() {
//    <%=gridProjects.ClientID %>.getView().focus();
//    var row = <%=gridProjects.ClientID %>.getSelectionModel().getCurrentPosition().row;
//    var column = <%=gridProjects.ClientID %>.getSelectionModel().getCurrentPosition().column;
//    <%=gridProjects.ClientID %>.getSelectionModel().setCurrentPosition({ row: row, column: column });
}
function deSelectCurrentCell() {
    //gridItems.getView().focus();
    //var row = gridItems.getSelectionModel().getCurrentPosition().row;
    //var column = gridItems.getSelectionModel().getCurrentPosition().column;
    <%=gridProjects.ClientID %>.getSelectionModel().deselectAll();
}

    var loadHoliday = function (month, year) {
        Ext.net.Mask.show();
        Ext.net.DirectMethod.request({
            url: "../../LeaveRequestService.asmx/GetTimesheet",
            cleanRequest: true,
            json: true,
            params: {
                month: month,
                year: year
            },
            success: function (result) {
                holidayList = result;
                //Ext.Msg.alert("Json Message", result);
               // CompanyX.ctl01_dateField.highlightDates(result);
                Ext.net.Mask.hide();

                //find the range case
               
            },
            failure: function (result) { Ext.net.Mask.hide(); }
        });
    };

    function getHolidayObject(date) {
            var str = (date.getMonth() + 1) + "/" + date.getDate() + "/" + date.getFullYear();
            for (var i = 0; i < holidayList.length; i++) {
                if (holidayList[i].DateEngText == str) {
                    return holidayList[i];
                }
            }
            return null;
        }
    
//    Ext.ns("Ext.ux.DatePicker");
//        Ext.ux.DatePicker.HighlightDates = Ext.extend(Object, {
//            constructor : function (config) {
//                this.dates = config.dates;
//                this.cls = config.cls;
//            },
//  
//            init : function (datePicker) {
//                var me = this;
//                if (datePicker instanceof Ext.DatePicker) {
//                    datePicker.getHighlighDates = Ext.Function.bind(function () {return this;}, me);
//                    datePicker.highlightDates = Ext.Function.bind(me.highlightDates, datePicker);
//                    datePicker.update = Ext.Function.createSequence(datePicker.update, function () {
//                        var highlighDates = this.getHighlighDates();
//                        this.highlightDates(highlighDates.dates, highlighDates.cls);
//                    });
//                } else if (datePicker instanceof Ext.form.DateField) {
//                    datePicker.onTriggerClick = Ext.Function.createSequence(datePicker.onTriggerClick, function () {
//                        if (!me.gotHighlighDates) {
//                            me.init(this.picker);
//                            this.picker.highlightDates(me.dates, me.cls);
//                            me.gotHighlighDates = true;
//                        }
//                    });
//                }
//            },
//  
//            highlightDates : function (dates, cls) {
//                var dateValues = [],
//                    el = this.getEl();
//  
//                cells = el.select("a.x-datepicker-date");
//                Ext.each(dates, function (d) {
//                    // save datetime format
//                    var date = new Date(d.DateEngText);
//                    dateValues.push(date.getTime());
//                });
//  
//                //cells.removeCls(cls);
//                //clear background of all cells
//                if(dateValues.length >0)
//                {
//                    cells.filter(function (el) {
//                        el.dom.style.backgroundColor = "";
//                        el.dom.title = "";
//                    });
//                }

//                cells.filter(function (el) {
//                    var hasHoliday = dateValues.indexOf(el.dom.dateValue) > -1;
//                    // if holiday then highlight cell
//                    if (hasHoliday) {

//                        var date = new Date(el.dom.dateValue);
//                        var holidayObject = getHolidayObject(date);
//                        if (holidayObject != null) {
//                            //set background color & tooltip
//                            if(holidayObject.Type != 0 )
//                                el.dom.style.backgroundColor = 'yellow';//legends[holidayObject.Abbreviation];
//                            el.dom.title = holidayObject.Name;
//                        }
//                    }
//                    return hasHoliday;
//                }).addCls(cls);
//            }
//    });

var currentMonth = null,currentYear = null;
    ///// Override DatePicker & is triggered when month or date selection changes
Ext.DatePicker.prototype.update = function (date, forceRefresh) {
    
    var me = this,
            active = me.activeDate;
    if (me.rendered) {
        me.activeDate = date;
        if (!forceRefresh && active && me.el && active.getMonth() == date.getMonth() && active.getFullYear() == date.getFullYear()) {
            me.selectedUpdate(date, active);
        } else {
            me.fullUpdate(date, active);
        }
    }

   
    //if (this.id == "DatePicker1") 
    {

        if (currentMonth == null) {
            currentMonth = date.getMonth();
            currentYear = date.getFullYear();
            loadHoliday(date.getMonth(), date.getFullYear());

        }
        else if (currentMonth != date.getMonth() || currentYear != date.getFullYear()) 
        {
            currentMonth = date.getMonth();
            currentYear = date.getFullYear();
            loadHoliday(date.getMonth(), date.getFullYear());
            //if (CompanyX.getCalendar() != undefined)
               // CompanyX.getCalendar().setStartDate(date);
            //reloadGrids();
        }
    }

    return me;

};


</script>
<style type="text/css">
    thead td, th
    {
        border: 0px solid;
    }
    .total
    {
        width: 90%;
        text-align: Center;
        display: block;
        padding-right: 5px;
    }
    .jiraClass
    {
        font-weight: bolder;
    }
    .commentClass
    {
        color: blue;
        display: block;
        font-weight: bolder;
    }
    
    .tableLightColor1 > tr > td
    {
        padding: 0px !important;
    }
    
    .x-grid-row .x-grid-cell
    {
        border-right-color: #EDEDED;
        border-right-style: solid;
        border-right-width: 1px !important;
    }
    .weeklyColor
    {
        background: #FABF8F !important;
    }
    .totalBlock
    {
        display: block;
        width: 100% !important;
        text-align: right;
    }
    .x-grid-header-ct
    {
        border: 1px solid #A3BAD9;
    }
    .x-panel-default-outer-border-rbl
    {
        border-top-color: #A3BAD9 !important;
        border-bottom-color: #A3BAD9 !important;
        border-right-color: #A3BAD9 !important;
        border-left-color: #A3BAD9 !important;
    }
    .x-panel-default-outer-border-trl
    {
        border-top-color: #A3BAD9 !important;
        border-right-color: #A3BAD9 !important;
        border-left-color: #A3BAD9 !important;
    }
    .x-column-header-inner
    {
        padding-top: 5px;
        padding-bottom: 5px;
    }
    .weekInfo
    {
        font-weight: bold !important;
        font-size: 15px;
    }
    .x-column-header-text
    {
        font-size: 12px;
    }
    .x-column-header-inner, .x-grid-cell-inner
    {
        padding-left: 5px;
    }
</style>
<div class="">
    <ext:Store runat="server" ID="storeSearch" AutoLoad="false">
        <Proxy>
            <ext:AjaxProxy Url="../../../Handler/EmpSearch.ashx">
                <ActionMethods Read="GET" />
                <Reader>
                    <ext:JsonReader Root="plants" TotalProperty="total" />
                </Reader>
            </ext:AjaxProxy>
        </Proxy>
        <Model>
            <ext:Model ID="Model6" IDProperty="Name" Name="ItemLineModel" runat="server">
                <Fields>
                    <ext:ModelField Name="Name" Type="String" />
                    <ext:ModelField Name="EmployeeId" Type="String" />
                </Fields>
            </ext:Model>
        </Model>
    </ext:Store>
    <ext:Store runat="server" ID="storeProjects">
        <Model>
            <ext:Model ID="Model2" runat="server" IDProperty="ProjectId">
                <Fields>
                    <ext:ModelField Name="ProjectId" Type="String" />
                    <ext:ModelField Name="Name" Type="String" />
                </Fields>
            </ext:Model>
        </Model>
    </ext:Store>
    <ext:Hidden runat="server" ID="Hidden_RowID" Text="-1">
    </ext:Hidden>
    <ext:Hidden runat="server" ID="Hidden_ReadonlyEmployeeId" Text="">
    </ext:Hidden>
  <ext:Store runat="server" ID="storeLeaves">
        <Model>
            <ext:Model ID="Model4" runat="server" IDProperty="LeaveTypeId">
                <Fields>
                    <ext:ModelField Name="LeaveTypeId" Type="String" />
                    <ext:ModelField Name="Title" Type="String" />
                </Fields>
            </ext:Model>
        </Model>
    </ext:Store>
    <h3 id="Pagetitle" style='margin-top: 10px; margin-bottom: 0px; font-size: 16px;'
        runat="server">
        Time Sheet</h3>
    <ext:Store ID="storeEmployee" runat="server">
        <Model>
            <ext:Model ID="Model9" IDProperty="Value" runat="server">
                <Fields>
                    <ext:ModelField Name="Value" Type="String" />
                    <ext:ModelField Name="Text" />
                </Fields>
            </ext:Model>
        </Model>
    </ext:Store>
    <%-- <ext:DateField runat="server" ID="dateField" />--%>
    <%--  <div style="padding: 5px 0 0 10px; margin: 20px 0 5px 0; display: block;">
        Select any date of the desired month</div>--%>
    <%--  <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Employee is required."
                ControlToValidate="cmbSearch" ValidationGroup="AssignTimeSheet" Display="None" />--%>
    <ext:ComboBox LabelSeparator="" ID="cmbSearch" runat="server" FieldLabel="" Hidden="true"
        EmptyText="Select Employee" DisplayField="Text" ValueField="Value" Width="185"
        ForceSelection="true" StoreID="storeEmployee" QueryMode="Local">
        <DirectEvents>
            <Select OnEvent="btnLoad_Click">
                <EventMask ShowMask="true" />
            </Select>
        </DirectEvents>
    </ext:ComboBox>
    <br />
    <table runat="server" id="tbl">
        <tr>
            <td style="padding-left: 0px">
                <ext:DateField runat="server" ID="txtFromDate" FieldLabel="Date" LabelAlign="Left"
                    LabelWidth="30" LabelSeparator="">
                    <Listeners>
                        <Select Handler="#{hiddenDate}.setValue(#{dateField}.getValue().getShortDate());" />
                    </Listeners>
                </ext:DateField>
            </td>
            <td style="padding-left: 10px">
                <ext:Button ID="btnPrevious" runat="server" Text="&nbsp;&nbsp;<&nbsp;&nbsp;" StyleSpec="float:left">
                    <DirectEvents>
                        <Click OnEvent="btnPrevious_Click">
                            <EventMask ShowMask="true">
                            </EventMask>
                        </Click>
                    </DirectEvents>
                </ext:Button>
            </td>
            <td>
                <ext:Button ID="btnNext" runat="server" Text="&nbsp;&nbsp;>&nbsp;&nbsp;" StyleSpec="float:left;margin-left:10px;">
                    <DirectEvents>
                        <Click OnEvent="btnNext_Click">
                            <EventMask ShowMask="true">
                            </EventMask>
                        </Click>
                    </DirectEvents>
                </ext:Button>
            </td>
            <td style="padding-left: 10px">
                <ext:Button ID="btnLoad" runat="server" Text="Load">
                    <DirectEvents>
                        <Click OnEvent="btnLoad_Click">
                            <EventMask ShowMask="true">
                            </EventMask>
                        </Click>
                    </DirectEvents>
                </ext:Button>
            </td>
        </tr>
    </table>
</div>
<div style="clear: both; padding-top: 10px">
</div>
<table>
    <tr>
        <td valign="top">
            <ext:Hidden ID="hiddenDate" runat="server" />
            <ext:Hidden ID="hiddenWeekStartDate" runat="server" />
            <div style="padding-top: 5px; margin-bottom: 5px; display: none;">
                Select any date of the desired week</div>
            <ext:DatePicker Width="220" StyleSpec='StyleSpec="border:1px solid #A3BAD9;' Hidden="true"
                runat="server" ID="dateField">
                <Listeners>
                    <Select Handler="#{hiddenDate}.setValue(#{dateField}.getValue().getShortDate());#{btnLoad}.fireEvent('click');" />
                </Listeners>
                <%-- <Plugins>
                    <ext:GenericPlugin ID="HighlightDates1" runat="server" InstanceName="Ext.ux.DatePicker.HighlightDates" />
                </Plugins>--%>
            </ext:DatePicker>
        </td>
        <td valign="top" style="padding-left: 0px">
            <ext:DisplayField ID="dispDate" FieldCls="weekInfo" runat="server">
            </ext:DisplayField>
            <ext:GridPanel AutoHeight="true" Border="true" Width="1200" ID="gridProjects" runat="server">
                <Store>
                    <ext:Store runat="server" ID="storeRequest">
                        <Model>
                            <ext:Model ID="Model1" runat="server" Name="WeekModel">
                                <Fields>
                                    <ext:ModelField Name="WeekStartDate" Type="String" />
                                    <ext:ModelField Name="Id" Type="String" />
                                    <ext:ModelField Name="D1" Type="Float" />
                                    <ext:ModelField Name="D2" Type="Float" />
                                    <ext:ModelField Name="D3" Type="Float" />
                                    <ext:ModelField Name="D4" Type="Float" />
                                    <ext:ModelField Name="D5" Type="Float" />
                                    <ext:ModelField Name="D6" Type="Float" />
                                    <ext:ModelField Name="D7" Type="Float" />
                                    <ext:ModelField Name="D8" Type="Float" />
                                    <ext:ModelField Name="D9" Type="Float" />
                                    <ext:ModelField Name="D10" Type="Float" />
                                    <ext:ModelField Name="D11" Type="Float" />
                                    <ext:ModelField Name="D12" Type="Float" />
                                    <ext:ModelField Name="D13" Type="Float" />
                                    <ext:ModelField Name="D14" Type="Float" />
                                    <ext:ModelField Name="D15" Type="Float" />
                                    <ext:ModelField Name="D16" Type="Float" />
                                    <ext:ModelField Name="D17" Type="Float" />
                                    <ext:ModelField Name="D18" Type="Float" />
                                    <ext:ModelField Name="D19" Type="Float" />
                                    <ext:ModelField Name="D20" Type="Float" />
                                    <ext:ModelField Name="D21" Type="Float" />
                                    <ext:ModelField Name="D22" Type="Float" />
                                    <ext:ModelField Name="D23" Type="Float" />
                                    <ext:ModelField Name="D24" Type="Float" />
                                    <ext:ModelField Name="D25" Type="Float" />
                                    <ext:ModelField Name="D26" Type="Float" />
                                    <ext:ModelField Name="D27" Type="Float" />
                                    <ext:ModelField Name="D28" Type="Float" />
                                    <ext:ModelField Name="D29" Type="Float" />
                                    <ext:ModelField Name="D30" Type="Float" />
                                    <ext:ModelField Name="D31" Type="Float" />
                                    <ext:ModelField Name="D32" Type="Float" />
                                    <ext:ModelField Name="Total" Type="Float" />
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                <Listeners>
                    <BeforeEdit Fn="beforeEdit" />
                </Listeners>
                <Plugins>
                    <ext:CellEditing ID="CellEditing1" ClicksToEdit="1" runat="server">
                        <Listeners>
                            <Edit Fn="afterEdit" />
                        </Listeners>
                    </ext:CellEditing>
                </Plugins>
                <Listeners>
                </Listeners>
                <View>
                    <ext:GridView ID="GridView1" runat="server">
                    </ext:GridView>
                </View>
                <ColumnModel runat="server" ID="columnmodle1">
                    <Columns>
                        <ext:Column ID="Casdolumn1" Sortable="false" MenuDisabled="true" StyleSpec="font-weight:bold"
                            Flex="1" Width="160" runat="server" Header="Project" DataIndex="Id">
                            <Editor>
                                <%-- <ext:ComboBox ID="cmbProjects" ForceSelection="true" QueryMode="Local" runat="server"
                                    DisplayField="Name" ValueField="ProjectId" StoreID="storeProjects">
                                    <Listeners>
                                        <Select Handler="projectDisplay(this.getValue(),#{gridProjects}.getSelectionModel().getCurrentPosition().row,this);" />
                                        <BeforeQuery Handler="var q = queryEvent.query;if(q=='') return;
                                              queryEvent.query = new RegExp('\\b' + q, 'i');
                                              queryEvent.query.length = q.length;" />
                                    </Listeners>
                                </ext:ComboBox>--%>
                            </Editor>
                            <Renderer Fn="projectRenderer" />
                        </ext:Column>
                        <ext:Column ID="D1" Cls="weeklyColor" runat="server" Sortable="false" Align="Center"
                            MenuDisabled="true" Text="Name" Width="31" DataIndex="D1">
                            <Renderer Handler="return getFormattedAmount1(value,record,1)" />
                            <Editor>
                                <ext:TextField ContextMenuID="menu" ID="TextField6" MaskRe="[0-9]|\.|%" StyleSpec="text-align:right;"
                                    PaddingSpec="0" SelectOnFocus="true" runat="server" AllowBlank="true">
                                    <Listeners>
                                        <Focus Handler="if(parseFloat(this.getValue())==0) this.setValue('');" />
                                    </Listeners>
                                </ext:TextField>
                            </Editor>
                        </ext:Column>
                        <ext:Column ID="D2" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="31" DataIndex="D2">
                            <Renderer Handler="return getFormattedAmount1(value,record,2)" />
                            <Editor>
                                <ext:TextField ContextMenuID="menu" ID="TextField1" MaskRe="[0-9]|\.|%" StyleSpec="text-align:right"
                                    SelectOnFocus="true" runat="server" AllowBlank="true">
                                    <Listeners>
                                        <Focus Handler="if(parseFloat(this.getValue())==0) this.setValue('');" />
                                    </Listeners>
                                </ext:TextField>
                            </Editor>
                        </ext:Column>
                        <ext:Column ID="D3" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="31" DataIndex="D3">
                            <Renderer Handler="return getFormattedAmount1(value,record,3)" />
                            <Editor>
                                <ext:TextField ContextMenuID="menu" ID="TextField2" MaskRe="[0-9]|\.|%" StyleSpec="text-align:right"
                                    SelectOnFocus="true" runat="server" AllowBlank="true">
                                    <Listeners>
                                        <Focus Handler="if(parseFloat(this.getValue())==0) this.setValue('');" />
                                    </Listeners>
                                </ext:TextField>
                            </Editor>
                        </ext:Column>
                        <ext:Column ID="D4" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="31" DataIndex="D4">
                            <Renderer Handler="return getFormattedAmount1(value,record,4)" />
                            <Editor>
                                <ext:TextField ContextMenuID="menu" ID="TextField3" MaskRe="[0-9]|\.|%" StyleSpec="text-align:right"
                                    SelectOnFocus="true" runat="server" AllowBlank="true">
                                    <Listeners>
                                        <Focus Handler="if(parseFloat(this.getValue())==0) this.setValue('');" />
                                    </Listeners>
                                </ext:TextField>
                            </Editor>
                        </ext:Column>
                        <ext:Column ID="D5" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="31" DataIndex="D5">
                            <Renderer Handler="return getFormattedAmount1(value,record,5)" />
                            <Editor>
                                <ext:TextField ContextMenuID="menu" ID="TextField4" MaskRe="[0-9]|\.|%" StyleSpec="text-align:right"
                                    SelectOnFocus="true" runat="server" AllowBlank="true">
                                    <Listeners>
                                        <Focus Handler="if(parseFloat(this.getValue())==0) this.setValue('');" />
                                    </Listeners>
                                </ext:TextField>
                            </Editor>
                        </ext:Column>
                        <ext:Column ID="D6" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="31" DataIndex="D6">
                            <Renderer Handler="return getFormattedAmount1(value,record,6)" />
                            <Editor>
                                <ext:TextField ContextMenuID="menu" ID="TextField5" MaskRe="[0-9]|\.|%" StyleSpec="text-align:right"
                                    SelectOnFocus="true" runat="server" AllowBlank="true">
                                    <Listeners>
                                        <Focus Handler="if(parseFloat(this.getValue())==0) this.setValue('');" />
                                    </Listeners>
                                </ext:TextField>
                            </Editor>
                        </ext:Column>
                        <ext:Column ID="D7" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="31" DataIndex="D7">
                            <Renderer Handler="return getFormattedAmount1(value,record,7)" />
                            <Editor>
                                <ext:TextField ContextMenuID="menu" ID="TextField7" MaskRe="[0-9]|\.|%" StyleSpec="text-align:right"
                                    SelectOnFocus="true" runat="server" AllowBlank="true">
                                    <Listeners>
                                        <Focus Handler="if(parseFloat(this.getValue())==0) this.setValue('');" />
                                    </Listeners>
                                </ext:TextField>
                            </Editor>
                        </ext:Column>
                        <ext:Column ID="D8" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="31" DataIndex="D8">
                            <Renderer Handler="return getFormattedAmount1(value,record,8)" />
                            <Editor>
                                <ext:TextField ContextMenuID="menu" ID="TextField8" MaskRe="[0-9]|\.|%" StyleSpec="text-align:right"
                                    SelectOnFocus="true" runat="server" AllowBlank="true">
                                    <Listeners>
                                        <Focus Handler="if(parseFloat(this.getValue())==0) this.setValue('');" />
                                    </Listeners>
                                </ext:TextField>
                            </Editor>
                        </ext:Column>
                        <ext:Column ID="D9" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="31" DataIndex="D9">
                            <Renderer Handler="return getFormattedAmount1(value,record,9)" />
                            <Editor>
                                <ext:TextField ContextMenuID="menu" ID="TextField9" MaskRe="[0-9]|\.|%" StyleSpec="text-align:right"
                                    SelectOnFocus="true" runat="server" AllowBlank="true">
                                    <Listeners>
                                        <Focus Handler="if(parseFloat(this.getValue())==0) this.setValue('');" />
                                    </Listeners>
                                </ext:TextField>
                            </Editor>
                        </ext:Column>
                        <ext:Column ID="D10" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="31" DataIndex="D10">
                            <Renderer Handler="return getFormattedAmount1(value,record,10)" />
                            <Editor>
                                <ext:TextField ContextMenuID="menu" ID="TextField10" MaskRe="[0-9]|\.|%" StyleSpec="text-align:right"
                                    SelectOnFocus="true" runat="server" AllowBlank="true">
                                    <Listeners>
                                        <Focus Handler="if(parseFloat(this.getValue())==0) this.setValue('');" />
                                    </Listeners>
                                </ext:TextField>
                            </Editor>
                        </ext:Column>
                        <ext:Column ID="D11" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="31" DataIndex="D11">
                            <Renderer Handler="return getFormattedAmount1(value,record,11)" />
                            <Editor>
                                <ext:TextField ContextMenuID="menu" ID="TextField11" MaskRe="[0-9]|\.|%" StyleSpec="text-align:right"
                                    SelectOnFocus="true" runat="server" AllowBlank="true">
                                    <Listeners>
                                        <Focus Handler="if(parseFloat(this.getValue())==0) this.setValue('');" />
                                    </Listeners>
                                </ext:TextField>
                            </Editor>
                        </ext:Column>
                        <ext:Column ID="D12" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="31" DataIndex="D12">
                            <Renderer Handler="return getFormattedAmount1(value,record,12)" />
                            <Editor>
                                <ext:TextField ContextMenuID="menu" ID="TextField12" MaskRe="[0-9]|\.|%" StyleSpec="text-align:right"
                                    SelectOnFocus="true" runat="server" AllowBlank="true">
                                    <Listeners>
                                        <Focus Handler="if(parseFloat(this.getValue())==0) this.setValue('');" />
                                    </Listeners>
                                </ext:TextField>
                            </Editor>
                        </ext:Column>
                        <ext:Column ID="D13" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="31" DataIndex="D13">
                            <Renderer Handler="return getFormattedAmount1(value,record,13)" />
                            <Editor>
                                <ext:TextField ContextMenuID="menu" ID="TextField13" MaskRe="[0-9]|\.|%" StyleSpec="text-align:right"
                                    SelectOnFocus="true" runat="server" AllowBlank="true">
                                    <Listeners>
                                        <Focus Handler="if(parseFloat(this.getValue())==0) this.setValue('');" />
                                    </Listeners>
                                </ext:TextField>
                            </Editor>
                        </ext:Column>
                        <ext:Column ID="D14" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="31" DataIndex="D14">
                            <Renderer Handler="return getFormattedAmount1(value,record,14)" />
                            <Editor>
                                <ext:TextField ContextMenuID="menu" ID="TextField14" MaskRe="[0-9]|\.|%" StyleSpec="text-align:right"
                                    SelectOnFocus="true" runat="server" AllowBlank="true">
                                    <Listeners>
                                        <Focus Handler="if(parseFloat(this.getValue())==0) this.setValue('');" />
                                    </Listeners>
                                </ext:TextField>
                            </Editor>
                        </ext:Column>
                        <ext:Column ID="D15" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="31" DataIndex="D15">
                            <Renderer Handler="return getFormattedAmount1(value,record,15)" />
                            <Editor>
                                <ext:TextField ContextMenuID="menu" ID="TextField15" MaskRe="[0-9]|\.|%" StyleSpec="text-align:right"
                                    SelectOnFocus="true" runat="server" AllowBlank="true">
                                    <Listeners>
                                        <Focus Handler="if(parseFloat(this.getValue())==0) this.setValue('');" />
                                    </Listeners>
                                </ext:TextField>
                            </Editor>
                        </ext:Column>
                        <ext:Column ID="D16" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="31" DataIndex="D16">
                            <Renderer Handler="return getFormattedAmount1(value,record,16)" />
                            <Editor>
                                <ext:TextField ContextMenuID="menu" ID="TextField16" MaskRe="[0-9]|\.|%" StyleSpec="text-align:right"
                                    SelectOnFocus="true" runat="server" AllowBlank="true">
                                    <Listeners>
                                        <Focus Handler="if(parseFloat(this.getValue())==0) this.setValue('');" />
                                    </Listeners>
                                </ext:TextField>
                            </Editor>
                        </ext:Column>
                        <ext:Column ID="D17" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="31" DataIndex="D17">
                            <Renderer Handler="return getFormattedAmount1(value,record,17)" />
                            <Editor>
                                <ext:TextField ContextMenuID="menu" ID="TextField17" MaskRe="[0-9]|\.|%" StyleSpec="text-align:right"
                                    SelectOnFocus="true" runat="server" AllowBlank="true">
                                    <Listeners>
                                        <Focus Handler="if(parseFloat(this.getValue())==0) this.setValue('');" />
                                    </Listeners>
                                </ext:TextField>
                            </Editor>
                        </ext:Column>
                        <ext:Column ID="D18" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="31" DataIndex="D18">
                            <Renderer Handler="return getFormattedAmount1(value,record,18)" />
                            <Editor>
                                <ext:TextField ContextMenuID="menu" ID="TextField18" MaskRe="[0-9]|\.|%" StyleSpec="text-align:right"
                                    SelectOnFocus="true" runat="server" AllowBlank="true">
                                    <Listeners>
                                        <Focus Handler="if(parseFloat(this.getValue())==0) this.setValue('');" />
                                    </Listeners>
                                </ext:TextField>
                            </Editor>
                        </ext:Column>
                        <ext:Column ID="D19" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="31" DataIndex="D19">
                            <Renderer Handler="return getFormattedAmount1(value,record,19)" />
                            <Editor>
                                <ext:TextField ContextMenuID="menu" ID="TextField19" MaskRe="[0-9]|\.|%" StyleSpec="text-align:right"
                                    SelectOnFocus="true" runat="server" AllowBlank="true">
                                    <Listeners>
                                        <Focus Handler="if(parseFloat(this.getValue())==0) this.setValue('');" />
                                    </Listeners>
                                </ext:TextField>
                            </Editor>
                        </ext:Column>
                        <ext:Column ID="D20" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="31" DataIndex="D20">
                            <Renderer Handler="return getFormattedAmount1(value,record,20)" />
                            <Editor>
                                <ext:TextField ContextMenuID="menu" ID="TextField20" MaskRe="[0-9]|\.|%" StyleSpec="text-align:right"
                                    SelectOnFocus="true" runat="server" AllowBlank="true">
                                    <Listeners>
                                        <Focus Handler="if(parseFloat(this.getValue())==0) this.setValue('');" />
                                    </Listeners>
                                </ext:TextField>
                            </Editor>
                        </ext:Column>
                        <ext:Column ID="D21" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="31" DataIndex="D21">
                            <Renderer Handler="return getFormattedAmount1(value,record,21)" />
                            <Editor>
                                <ext:TextField ContextMenuID="menu" ID="TextField21" MaskRe="[0-9]|\.|%" StyleSpec="text-align:right"
                                    SelectOnFocus="true" runat="server" AllowBlank="true">
                                    <Listeners>
                                        <Focus Handler="if(parseFloat(this.getValue())==0) this.setValue('');" />
                                    </Listeners>
                                </ext:TextField>
                            </Editor>
                        </ext:Column>
                        <ext:Column ID="D22" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="31" DataIndex="D22">
                            <Renderer Handler="return getFormattedAmount1(value,record,22)" />
                            <Editor>
                                <ext:TextField ContextMenuID="menu" ID="TextField22" MaskRe="[0-9]|\.|%" StyleSpec="text-align:right"
                                    SelectOnFocus="true" runat="server" AllowBlank="true">
                                    <Listeners>
                                        <Focus Handler="if(parseFloat(this.getValue())==0) this.setValue('');" />
                                    </Listeners>
                                </ext:TextField>
                            </Editor>
                        </ext:Column>
                        <ext:Column ID="D23" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="31" DataIndex="D23">
                            <Renderer Handler="return getFormattedAmount1(value,record,23)" />
                            <Editor>
                                <ext:TextField ContextMenuID="menu" ID="TextField23" MaskRe="[0-9]|\.|%" StyleSpec="text-align:right"
                                    SelectOnFocus="true" runat="server" AllowBlank="true">
                                    <Listeners>
                                        <Focus Handler="if(parseFloat(this.getValue())==0) this.setValue('');" />
                                    </Listeners>
                                </ext:TextField>
                            </Editor>
                        </ext:Column>
                        <ext:Column ID="D24" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="31" DataIndex="D24">
                            <Renderer Handler="return getFormattedAmount1(value,record,24)" />
                            <Editor>
                                <ext:TextField ContextMenuID="menu" ID="TextField24" MaskRe="[0-9]|\.|%" StyleSpec="text-align:right"
                                    SelectOnFocus="true" runat="server" AllowBlank="true">
                                    <Listeners>
                                        <Focus Handler="if(parseFloat(this.getValue())==0) this.setValue('');" />
                                    </Listeners>
                                </ext:TextField>
                            </Editor>
                        </ext:Column>
                        <ext:Column ID="D25" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="31" DataIndex="D25">
                            <Renderer Handler="return getFormattedAmount1(value,record,25)" />
                            <Editor>
                                <ext:TextField ContextMenuID="menu" ID="TextField25" MaskRe="[0-9]|\.|%" StyleSpec="text-align:right"
                                    SelectOnFocus="true" runat="server" AllowBlank="true">
                                    <Listeners>
                                        <Focus Handler="if(parseFloat(this.getValue())==0) this.setValue('');" />
                                    </Listeners>
                                </ext:TextField>
                            </Editor>
                        </ext:Column>
                        <ext:Column ID="D26" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="31" DataIndex="D26">
                            <Renderer Handler="return getFormattedAmount1(value,record,26)" />
                            <Editor>
                                <ext:TextField ContextMenuID="menu" ID="TextField26" MaskRe="[0-9]|\.|%" StyleSpec="text-align:right"
                                    SelectOnFocus="true" runat="server" AllowBlank="true">
                                    <Listeners>
                                        <Focus Handler="if(parseFloat(this.getValue())==0) this.setValue('');" />
                                    </Listeners>
                                </ext:TextField>
                            </Editor>
                        </ext:Column>
                        <ext:Column ID="D27" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="31" DataIndex="D27">
                            <Renderer Handler="return getFormattedAmount1(value,record,27)" />
                            <Editor>
                                <ext:TextField ContextMenuID="menu" ID="TextField27" MaskRe="[0-9]|\.|%" StyleSpec="text-align:right"
                                    SelectOnFocus="true" runat="server" AllowBlank="true">
                                    <Listeners>
                                        <Focus Handler="if(parseFloat(this.getValue())==0) this.setValue('');" />
                                    </Listeners>
                                </ext:TextField>
                            </Editor>
                        </ext:Column>
                        <ext:Column ID="D28" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="31" DataIndex="D28">
                            <Renderer Handler="return getFormattedAmount1(value,record,28)" />
                            <Editor>
                                <ext:TextField ContextMenuID="menu" ID="TextField28" MaskRe="[0-9]|\.|%" StyleSpec="text-align:right"
                                    SelectOnFocus="true" runat="server" AllowBlank="true">
                                    <Listeners>
                                        <Focus Handler="if(parseFloat(this.getValue())==0) this.setValue('');" />
                                    </Listeners>
                                </ext:TextField>
                            </Editor>
                        </ext:Column>
                        <ext:Column ID="D29" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Hidden="true" Text="Name" Width="31" DataIndex="D29">
                            <Renderer Handler="return getFormattedAmount1(value,record,29)" />
                            <Editor>
                                <ext:TextField ContextMenuID="menu" ID="TextField29" MaskRe="[0-9]|\.|%" StyleSpec="text-align:right"
                                    SelectOnFocus="true" runat="server" AllowBlank="true">
                                    <Listeners>
                                        <Focus Handler="if(parseFloat(this.getValue())==0) this.setValue('');" />
                                    </Listeners>
                                </ext:TextField>
                            </Editor>
                        </ext:Column>
                        <ext:Column ID="D30" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Hidden="true" Text="Name" Width="31" DataIndex="D30">
                            <Renderer Handler="return getFormattedAmount1(value,record,30)" />
                            <Editor>
                                <ext:TextField ContextMenuID="menu" ID="TextField30" MaskRe="[0-9]|\.|%" StyleSpec="text-align:right"
                                    SelectOnFocus="true" runat="server" AllowBlank="true">
                                    <Listeners>
                                        <Focus Handler="if(parseFloat(this.getValue())==0) this.setValue('');" />
                                    </Listeners>
                                </ext:TextField>
                            </Editor>
                        </ext:Column>
                        <ext:Column ID="D31" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Hidden="true" Text="Name" Width="31" DataIndex="D31">
                            <Renderer Handler="return getFormattedAmount1(value,record,31)" />
                            <Editor>
                                <ext:TextField ContextMenuID="menu" ID="TextField31" MaskRe="[0-9]|\.|%" StyleSpec="text-align:right;"
                                    SelectOnFocus="true" runat="server" AllowBlank="true">
                                    <Listeners>
                                        <Focus Handler="if(parseFloat(this.getValue())==0) this.setValue('');" />
                                    </Listeners>
                                </ext:TextField>
                            </Editor>
                        </ext:Column>
                        <ext:Column ID="D32" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Hidden="true" Text="Name" Width="31" DataIndex="D32">
                            <Renderer Handler="return getFormattedAmount1(value,record,32)" />
                            <Editor>
                                <ext:TextField ContextMenuID="menu" ID="TextField32" MaskRe="[0-9]|\.|%" StyleSpec="text-align:right"
                                    SelectOnFocus="true" runat="server" AllowBlank="true">
                                    <Listeners>
                                        <Focus Handler="if(parseFloat(this.getValue())==0) this.setValue('');" />
                                    </Listeners>
                                </ext:TextField>
                            </Editor>
                        </ext:Column>
                        <ext:Column ID="Column2" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Total" Width="45" DataIndex="Total">
                            <Renderer Fn="getFormattedAmount2" />
                        </ext:Column>
                        <ext:ImageCommandColumn ID="CommandColumn1" runat="server" Width="36" Align="Center"
                            ButtonAlign="Center" Hidden="false">
                        </ext:ImageCommandColumn>
                    </Columns>
                </ColumnModel>
                <SelectionModel>
                    <ext:CellSelectionModel EnableKeyNav="true" runat="server" ID="CellSelectionModel_ItemGrid">
                    </ext:CellSelectionModel>
                </SelectionModel>
            </ext:GridPanel>
            <%-- <ext:GridPanel AutoHeight="true" DisableSelection="true" HideHeaders="true" Width="1200"
                ID="gridProjectsTotal" runat="server">
                <Store>
                    <ext:Store runat="server" ID="store2">
                        <Model>
                            <ext:Model ID="Model7" runat="server" Name="WeekModelTotal">
                                <Fields>
                                    <ext:ModelField Name="Name" Type="String" />
                                    <ext:ModelField Name="D1" Type="Float" />
                                    <ext:ModelField Name="D2" Type="Float" />
                                    <ext:ModelField Name="D3" Type="Float" />
                                    <ext:ModelField Name="D4" Type="Float" />
                                    <ext:ModelField Name="D5" Type="Float" />
                                    <ext:ModelField Name="D6" Type="Float" />
                                    <ext:ModelField Name="D7" Type="Float" />
                                    <ext:ModelField Name="D8" Type="Float" />
                                    <ext:ModelField Name="D9" Type="Float" />
                                    <ext:ModelField Name="D10" Type="Float" />
                                    <ext:ModelField Name="D11" Type="Float" />
                                    <ext:ModelField Name="D12" Type="Float" />
                                    <ext:ModelField Name="D13" Type="Float" />
                                    <ext:ModelField Name="D14" Type="Float" />
                                    <ext:ModelField Name="D15" Type="Float" />
                                    <ext:ModelField Name="D16" Type="Float" />
                                    <ext:ModelField Name="D17" Type="Float" />
                                    <ext:ModelField Name="D18" Type="Float" />
                                    <ext:ModelField Name="D19" Type="Float" />
                                    <ext:ModelField Name="D20" Type="Float" />
                                    <ext:ModelField Name="D21" Type="Float" />
                                    <ext:ModelField Name="D22" Type="Float" />
                                    <ext:ModelField Name="D23" Type="Float" />
                                    <ext:ModelField Name="D24" Type="Float" />
                                    <ext:ModelField Name="D25" Type="Float" />
                                    <ext:ModelField Name="D26" Type="Float" />
                                    <ext:ModelField Name="D27" Type="Float" />
                                    <ext:ModelField Name="D28" Type="Float" />
                                    <ext:ModelField Name="D29" Type="Float" />
                                    <ext:ModelField Name="D30" Type="Float" />
                                    <ext:ModelField Name="D31" Type="Float" />
                                    <ext:ModelField Name="D32" Type="Float" />
                                    <ext:ModelField Name="Total" Type="Float" />
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                <ColumnModel runat="server" ID="ColumnModel2">
                    <Columns>
                        <ext:Column ID="Column6" MenuDisabled="true" StyleSpec="font-weight:bold" Width="160"
                            Flex="1" runat="server" DataIndex="Name">
                            <Renderer Fn="totalRenderer" />
                        </ext:Column>
                        <ext:Column ID="Column6661" Cls="weeklyColor" runat="server" Sortable="false" Align="Center"
                            MenuDisabled="true" Text="Name" Width="31" DataIndex="D1">
                            <Renderer Fn="getFormattedAmountTotalOrTimecard" />
                        </ext:Column>
                        <ext:Column ID="Columasdn2" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="31" DataIndex="D2">
                            <Renderer Fn="getFormattedAmountTotalOrTimecard" />
                        </ext:Column>
                        <ext:Column ID="Column12" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="31" DataIndex="D3">
                            <Renderer Fn="getFormattedAmountTotalOrTimecard" />
                        </ext:Column>
                        <ext:Column ID="Column13" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="31" DataIndex="D4">
                            <Renderer Fn="getFormattedAmountTotalOrTimecard" />
                        </ext:Column>
                        <ext:Column ID="Column14" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="31" DataIndex="D5">
                            <Renderer Fn="getFormattedAmountTotalOrTimecard" />
                        </ext:Column>
                        <ext:Column ID="Column15" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="31" DataIndex="D6">
                            <Renderer Fn="getFormattedAmountTotalOrTimecard" />
                        </ext:Column>
                        <ext:Column ID="Column16" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="31" DataIndex="D7">
                            <Renderer Fn="getFormattedAmountTotalOrTimecard" />
                        </ext:Column>
                        <ext:Column ID="Column4" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="31" DataIndex="D8">
                            <Renderer Fn="getFormattedAmountTotalOrTimecard" />
                        </ext:Column>
                        <ext:Column ID="Column5" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="31" DataIndex="D9">
                            <Renderer Fn="getFormattedAmountTotalOrTimecard" />
                        </ext:Column>
                        <ext:Column ID="Column7" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="31" DataIndex="D10">
                            <Renderer Fn="getFormattedAmountTotalOrTimecard" />
                        </ext:Column>
                        <ext:Column ID="Column8" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="31" DataIndex="D11">
                            <Renderer Fn="getFormattedAmountTotalOrTimecard" />
                        </ext:Column>
                        <ext:Column ID="Column18" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="31" DataIndex="D12">
                            <Renderer Fn="getFormattedAmountTotalOrTimecard" />
                        </ext:Column>
                        <ext:Column ID="Column19" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="31" DataIndex="D13">
                            <Renderer Fn="getFormattedAmountTotalOrTimecard" />
                        </ext:Column>
                        <ext:Column ID="Column21" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="31" DataIndex="D14">
                            <Renderer Fn="getFormattedAmountTotalOrTimecard" />
                        </ext:Column>
                        <ext:Column ID="Column22" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="31" DataIndex="D15">
                            <Renderer Fn="getFormattedAmountTotalOrTimecard" />
                        </ext:Column>
                        <ext:Column ID="Column31" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="31" DataIndex="D16">
                            <Renderer Fn="getFormattedAmountTotalOrTimecard" />
                        </ext:Column>
                        <ext:Column ID="Column32" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="31" DataIndex="D17">
                            <Renderer Fn="getFormattedAmountTotalOrTimecard" />
                        </ext:Column>
                        <ext:Column ID="Column33" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="31" DataIndex="D18">
                            <Renderer Fn="getFormattedAmountTotalOrTimecard" />
                        </ext:Column>
                        <ext:Column ID="Column34" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="31" DataIndex="D19">
                            <Renderer Fn="getFormattedAmountTotalOrTimecard" />
                        </ext:Column>
                        <ext:Column ID="Column35" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="31" DataIndex="D20">
                            <Renderer Fn="getFormattedAmountTotalOrTimecard" />
                        </ext:Column>
                        <ext:Column ID="Column36" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="31" DataIndex="D21">
                            <Renderer Fn="getFormattedAmountTotalOrTimecard" />
                        </ext:Column>
                        <ext:Column ID="Column3a7" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="31" DataIndex="D22">
                            <Renderer Fn="getFormattedAmountTotalOrTimecard" />
                        </ext:Column>
                        <ext:Column ID="Columasddna37" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="31" DataIndex="D23">
                            <Renderer Fn="getFormattedAmountTotalOrTimecard" />
                        </ext:Column>
                        <ext:Column ID="Columnad37" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="31" DataIndex="D24">
                            <Renderer Fn="getFormattedAmountTotalOrTimecard" />
                        </ext:Column>
                        <ext:Column ID="Columnda37" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="31" DataIndex="D25">
                            <Renderer Fn="getFormattedAmountTotalOrTimecard" />
                        </ext:Column>
                        <ext:Column ID="Coluasdfmdna37" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="31" DataIndex="D26">
                            <Renderer Fn="getFormattedAmountTotalOrTimecard" />
                        </ext:Column>
                        <ext:Column ID="Coludamn37" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="31" DataIndex="D27">
                            <Renderer Fn="getFormattedAmountTotalOrTimecard" />
                        </ext:Column>
                        <ext:Column ID="Columaan37" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="31" DataIndex="D28">
                            <Renderer Fn="getFormattedAmountTotalOrTimecard" />
                        </ext:Column>
                        <ext:Column ID="DT29" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="31" DataIndex="D29" Hidden="true">
                            <Renderer Fn="getFormattedAmountTotalOrTimecard" />
                        </ext:Column>
                        <ext:Column ID="DT30" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="31" DataIndex="D30" Hidden="true">
                            <Renderer Fn="getFormattedAmountTotalOrTimecard" />
                        </ext:Column>
                        <ext:Column ID="DT31" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="31" DataIndex="D31" Hidden="true">
                            <Renderer Fn="getFormattedAmountTotalOrTimecard" />
                        </ext:Column>
                        <ext:Column ID="DT32" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="31" DataIndex="D32" Hidden="true">
                            <Renderer Fn="getFormattedAmountTotalOrTimecard" />
                        </ext:Column>
                        <ext:Column ID="Column17" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Total" Width="45" DataIndex="Total">
                            <Renderer Fn="getFormattedAmountTotalOrTimecard" />
                        </ext:Column>
                        <ext:ImageCommandColumn ID="ImageCommandColumn1" runat="server" Width="36" ButtonAlign="Center"
                            Hidden="false">
                        </ext:ImageCommandColumn>
                    </Columns>
                </ColumnModel>
                <SelectionModel>
                    <ext:CellSelectionModel EnableKeyNav="true" runat="server" ID="CellSelectionModel2">
                    </ext:CellSelectionModel>
                </SelectionModel>
                <View>
                    <ext:GridView ID="GridView4" StripeRows="false" runat="server">
                    </ext:GridView>
                </View>
            </ext:GridPanel>--%>
            <%-- <ext:Button ID="btnAddProject" Width="100" Height="30" runat="server" StyleSpec="margin-top:10px"
                Text="Add Project">
                <Listeners>
                    <Click Handler="var newRow = new WeekModel();newRow.D1=0;newRow.D2=0;newRow.D3=0;newRow.D4=0;newRow.D5=0;newRow.D6=0;newRow.D7=0; var rowIndex = #{gridProjects}.getStore().data.items.length;#{gridProjects}.getStore().insert(rowIndex, newRow);">
                    </Click>
                </Listeners>
            </ext:Button>--%>
            <ext:GridPanel AutoHeight="true" StyleSpec="margin-top:25px" DisableSelection="true"
                Border="true" Width="1200" ID="gridLeaves" runat="server">
                <Store>
                    <ext:Store runat="server" ID="store1">
                        <Model>
                            <ext:Model ID="Model3" runat="server" Name="WeekModelLeave">
                                <Fields>
                                    <ext:ModelField Name="Id" Type="String" />
                                    <ext:ModelField Name="D1" Type="Float" />
                                    <ext:ModelField Name="D2" Type="Float" />
                                    <ext:ModelField Name="D3" Type="Float" />
                                    <ext:ModelField Name="D4" Type="Float" />
                                    <ext:ModelField Name="D5" Type="Float" />
                                    <ext:ModelField Name="D6" Type="Float" />
                                    <ext:ModelField Name="D7" Type="Float" />
                                    <ext:ModelField Name="D8" Type="Float" />
                                    <ext:ModelField Name="D9" Type="Float" />
                                    <ext:ModelField Name="D10" Type="Float" />
                                    <ext:ModelField Name="D11" Type="Float" />
                                    <ext:ModelField Name="D12" Type="Float" />
                                    <ext:ModelField Name="D13" Type="Float" />
                                    <ext:ModelField Name="D14" Type="Float" />
                                    <ext:ModelField Name="D15" Type="Float" />
                                    <ext:ModelField Name="D16" Type="Float" />
                                    <ext:ModelField Name="D17" Type="Float" />
                                    <ext:ModelField Name="D18" Type="Float" />
                                    <ext:ModelField Name="D19" Type="Float" />
                                    <ext:ModelField Name="D20" Type="Float" />
                                    <ext:ModelField Name="D21" Type="Float" />
                                    <ext:ModelField Name="D22" Type="Float" />
                                    <ext:ModelField Name="D23" Type="Float" />
                                    <ext:ModelField Name="D24" Type="Float" />
                                    <ext:ModelField Name="D25" Type="Float" />
                                    <ext:ModelField Name="D26" Type="Float" />
                                    <ext:ModelField Name="D27" Type="Float" />
                                    <ext:ModelField Name="D28" Type="Float" />
                                    <ext:ModelField Name="D29" Type="Float" />
                                    <ext:ModelField Name="D30" Type="Float" />
                                    <ext:ModelField Name="D31" Type="Float" />
                                    <ext:ModelField Name="D32" Type="Float" />
                                    <ext:ModelField Name="Total" Type="Float" />
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                <Listeners>
                    <BeforeEdit Fn="beforeEdit" />
                </Listeners>
                <Plugins>
                    <ext:CellEditing ID="CellEditing2" ClicksToEdit="1" runat="server">
                        <Listeners>
                            <Edit Fn="afterEditLeave" />
                        </Listeners>
                    </ext:CellEditing>
                </Plugins>
                <View>
                    <ext:GridView ID="GridView2" runat="server">
                    </ext:GridView>
                </View>
                <ColumnModel runat="server" ID="ColumnModel1">
                    <Columns>
                        <ext:Column ID="Column3" Sortable="false" MenuDisabled="true" StyleSpec="font-weight:bold"
                            Flex="1" Width="160" runat="server" Header="Leaves" DataIndex="Id">
                            <Renderer Fn="leaveRenderer" />
                        </ext:Column>
                        <ext:Column ID="L1" Cls="weeklyColor" runat="server" Sortable="false" Align="Center"
                            MenuDisabled="true" Text="Name" Width="31" DataIndex="D1">
                            <Renderer Fn="getFormattedAmount2" />
                        </ext:Column>
                        <ext:Column ID="L2" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="31" DataIndex="D2">
                            <Renderer Fn="getFormattedAmount2" />
                        </ext:Column>
                        <ext:Column ID="L3" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="31" DataIndex="D3">
                            <Renderer Fn="getFormattedAmount2" />
                        </ext:Column>
                        <ext:Column ID="L4" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="31" DataIndex="D4">
                            <Renderer Fn="getFormattedAmount2" />
                        </ext:Column>
                        <ext:Column ID="L5" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="31" DataIndex="D5">
                            <Renderer Fn="getFormattedAmount2" />
                        </ext:Column>
                        <ext:Column ID="L6" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="31" DataIndex="D6">
                            <Renderer Fn="getFormattedAmount2" />
                        </ext:Column>
                        <ext:Column ID="L7" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="31" DataIndex="D7">
                            <Renderer Fn="getFormattedAmount2" />
                        </ext:Column>
                        <ext:Column ID="L8" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="31" DataIndex="D8">
                            <Renderer Fn="getFormattedAmount2" />
                        </ext:Column>
                        <ext:Column ID="L9" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="31" DataIndex="D9">
                            <Renderer Fn="getFormattedAmount2" />
                        </ext:Column>
                        <ext:Column ID="L10" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="31" DataIndex="D10">
                            <Renderer Fn="getFormattedAmount2" />
                        </ext:Column>
                        <ext:Column ID="L11" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="31" DataIndex="D11">
                            <Renderer Fn="getFormattedAmount2" />
                        </ext:Column>
                        <ext:Column ID="L12" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="31" DataIndex="D12">
                            <Renderer Fn="getFormattedAmount2" />
                        </ext:Column>
                        <ext:Column ID="L13" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="31" DataIndex="D13">
                            <Renderer Fn="getFormattedAmount2" />
                        </ext:Column>
                        <ext:Column ID="L14" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="31" DataIndex="D14">
                            <Renderer Fn="getFormattedAmount2" />
                        </ext:Column>
                        <ext:Column ID="L15" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="31" DataIndex="D15">
                            <Renderer Fn="getFormattedAmount2" />
                        </ext:Column>
                        <ext:Column ID="L16" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="31" DataIndex="D16">
                            <Renderer Fn="getFormattedAmount2" />
                        </ext:Column>
                        <ext:Column ID="L17" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="31" DataIndex="D17">
                            <Renderer Fn="getFormattedAmount2" />
                        </ext:Column>
                        <ext:Column ID="L18" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="31" DataIndex="D18">
                            <Renderer Fn="getFormattedAmount2" />
                        </ext:Column>
                        <ext:Column ID="L19" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="31" DataIndex="D19">
                            <Renderer Fn="getFormattedAmount2" />
                        </ext:Column>
                        <ext:Column ID="L20" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="31" DataIndex="D20">
                            <Renderer Fn="getFormattedAmount2" />
                        </ext:Column>
                        <ext:Column ID="L21" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="31" DataIndex="D21">
                            <Renderer Fn="getFormattedAmount2" />
                        </ext:Column>
                        <ext:Column ID="L22" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="31" DataIndex="D22">
                            <Renderer Fn="getFormattedAmount2" />
                        </ext:Column>
                        <ext:Column ID="L23" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="31" DataIndex="D23">
                            <Renderer Fn="getFormattedAmount2" />
                        </ext:Column>
                        <ext:Column ID="L24" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="31" DataIndex="D24">
                            <Renderer Fn="getFormattedAmount2" />
                        </ext:Column>
                        <ext:Column ID="L25" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="31" DataIndex="D25">
                            <Renderer Fn="getFormattedAmount2" />
                        </ext:Column>
                        <ext:Column ID="L26" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="31" DataIndex="D26">
                            <Renderer Fn="getFormattedAmount2" />
                        </ext:Column>
                        <ext:Column ID="L27" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="31" DataIndex="D27">
                            <Renderer Fn="getFormattedAmount2" />
                        </ext:Column>
                        <ext:Column ID="L28" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="31" DataIndex="D28">
                            <Renderer Fn="getFormattedAmount2" />
                        </ext:Column>
                        <ext:Column ID="L29" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="31" DataIndex="D29" Hidden="true">
                            <Renderer Fn="getFormattedAmount2" />
                        </ext:Column>
                        <ext:Column ID="L30" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="31" DataIndex="D30" Hidden="true">
                            <Renderer Fn="getFormattedAmount2" />
                        </ext:Column>
                        <ext:Column ID="L31" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="31" DataIndex="D31" Hidden="true">
                            <Renderer Fn="getFormattedAmount2" />
                        </ext:Column>
                        <ext:Column ID="L32" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="31" DataIndex="D32" Hidden="true">
                            <Renderer Fn="getFormattedAmount2" />
                        </ext:Column>
                        <ext:Column ID="Column11" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Total" Width="45" DataIndex="Total">
                            <Renderer Fn="getFormattedAmount2" />
                        </ext:Column>
                        <ext:ImageCommandColumn ID="ImageCommandColumn3" runat="server" Width="36" ButtonAlign="Center"
                            Hidden="false">
                        </ext:ImageCommandColumn>
                    </Columns>
                </ColumnModel>
                <SelectionModel>
                    <ext:CellSelectionModel ID="CellSelectionModel1" runat="server" Mode="Simple" />
                </SelectionModel>
            </ext:GridPanel>
            <ext:GridPanel AutoHeight="true" DisableSelection="true" HideHeaders="true" Border="false"
                Width="1200" ID="gridLeavesTotal" runat="server">
                <Store>
                    <ext:Store runat="server" ID="store3">
                        <Model>
                            <ext:Model ID="Model8" runat="server" Name="WeekModelTotal">
                                <Fields>
                                    <ext:ModelField Name="Name" Type="String" />
                                    <ext:ModelField Name="D1" Type="Float" />
                                    <ext:ModelField Name="D2" Type="Float" />
                                    <ext:ModelField Name="D3" Type="Float" />
                                    <ext:ModelField Name="D4" Type="Float" />
                                    <ext:ModelField Name="D5" Type="Float" />
                                    <ext:ModelField Name="D6" Type="Float" />
                                    <ext:ModelField Name="D7" Type="Float" />
                                    <ext:ModelField Name="D8" Type="Float" />
                                    <ext:ModelField Name="D9" Type="Float" />
                                    <ext:ModelField Name="D10" Type="Float" />
                                    <ext:ModelField Name="D11" Type="Float" />
                                    <ext:ModelField Name="D12" Type="Float" />
                                    <ext:ModelField Name="D13" Type="Float" />
                                    <ext:ModelField Name="D14" Type="Float" />
                                    <ext:ModelField Name="D15" Type="Float" />
                                    <ext:ModelField Name="D16" Type="Float" />
                                    <ext:ModelField Name="D17" Type="Float" />
                                    <ext:ModelField Name="D18" Type="Float" />
                                    <ext:ModelField Name="D19" Type="Float" />
                                    <ext:ModelField Name="D20" Type="Float" />
                                    <ext:ModelField Name="D21" Type="Float" />
                                    <ext:ModelField Name="D22" Type="Float" />
                                    <ext:ModelField Name="D23" Type="Float" />
                                    <ext:ModelField Name="D24" Type="Float" />
                                    <ext:ModelField Name="D25" Type="Float" />
                                    <ext:ModelField Name="D26" Type="Float" />
                                    <ext:ModelField Name="D27" Type="Float" />
                                    <ext:ModelField Name="D28" Type="Float" />
                                    <ext:ModelField Name="D29" Type="Float" />
                                    <ext:ModelField Name="D30" Type="Float" />
                                    <ext:ModelField Name="D31" Type="Float" />
                                    <ext:ModelField Name="D32" Type="Float" />
                                    <ext:ModelField Name="Total" Type="Float" />
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                <ColumnModel runat="server" ID="ColumnModel3">
                    <Columns>
                        <ext:Column ID="Column20" MenuDisabled="true" StyleSpec="font-weight:bold" Width="160"
                            Flex="1" runat="server" DataIndex="Name">
                            <Renderer Fn="totalRenderer" />
                        </ext:Column>
                        <ext:Column ID="Column23" Cls="weeklyColor" runat="server" Sortable="false" Align="Center"
                            MenuDisabled="true" Text="Name" Width="31" DataIndex="D1">
                            <Renderer Fn="getFormattedAmount2" />
                        </ext:Column>
                        <ext:Column ID="Column24" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="31" DataIndex="D2">
                            <Renderer Fn="getFormattedAmount2" />
                        </ext:Column>
                        <ext:Column ID="Column25" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="31" DataIndex="D3">
                            <Renderer Fn="getFormattedAmount2" />
                        </ext:Column>
                        <ext:Column ID="Column26" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="31" DataIndex="D4">
                            <Renderer Fn="getFormattedAmount2" />
                        </ext:Column>
                        <ext:Column ID="Column27" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="31" DataIndex="D5">
                            <Renderer Fn="getFormattedAmount2" />
                        </ext:Column>
                        <ext:Column ID="Column28" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="31" DataIndex="D6">
                            <Renderer Fn="getFormattedAmount2" />
                        </ext:Column>
                        <ext:Column ID="Column29" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="31" DataIndex="D7">
                            <Renderer Fn="getFormattedAmount2" />
                        </ext:Column>
                        <ext:Column ID="Column60" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="31" DataIndex="D8">
                            <Renderer Fn="getFormattedAmount2" />
                        </ext:Column>
                        <ext:Column ID="Column61" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="31" DataIndex="D9">
                            <Renderer Fn="getFormattedAmount2" />
                        </ext:Column>
                        <ext:Column ID="Column62" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="31" DataIndex="D10">
                            <Renderer Fn="getFormattedAmount2" />
                        </ext:Column>
                        <ext:Column ID="Column63" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="31" DataIndex="D11">
                            <Renderer Fn="getFormattedAmount2" />
                        </ext:Column>
                        <ext:Column ID="Column64" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="31" DataIndex="D12">
                            <Renderer Fn="getFormattedAmount2" />
                        </ext:Column>
                        <ext:Column ID="Column66" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="31" DataIndex="D13">
                            <Renderer Fn="getFormattedAmount2" />
                        </ext:Column>
                        <ext:Column ID="Column67" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="31" DataIndex="D14">
                            <Renderer Fn="getFormattedAmount2" />
                        </ext:Column>
                        <ext:Column ID="Column68" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="31" DataIndex="D15">
                            <Renderer Fn="getFormattedAmount2" />
                        </ext:Column>
                        <ext:Column ID="Column69" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="31" DataIndex="D16">
                            <Renderer Fn="getFormattedAmount2" />
                        </ext:Column>
                        <ext:Column ID="Column70" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="31" DataIndex="D17">
                            <Renderer Fn="getFormattedAmount2" />
                        </ext:Column>
                        <ext:Column ID="Column71" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="31" DataIndex="D18">
                            <Renderer Fn="getFormattedAmount2" />
                        </ext:Column>
                        <ext:Column ID="Column72" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="31" DataIndex="D19">
                            <Renderer Fn="getFormattedAmount2" />
                        </ext:Column>
                        <ext:Column ID="Column73" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="31" DataIndex="D20">
                            <Renderer Fn="getFormattedAmount2" />
                        </ext:Column>
                        <ext:Column ID="Column74" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="31" DataIndex="D21">
                            <Renderer Fn="getFormattedAmount2" />
                        </ext:Column>
                        <ext:Column ID="Column75" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="31" DataIndex="D22">
                            <Renderer Fn="getFormattedAmount2" />
                        </ext:Column>
                        <ext:Column ID="Column76" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="31" DataIndex="D23">
                            <Renderer Fn="getFormattedAmount2" />
                        </ext:Column>
                        <ext:Column ID="Column77" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="31" DataIndex="D24">
                            <Renderer Fn="getFormattedAmount2" />
                        </ext:Column>
                        <ext:Column ID="Column78" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="31" DataIndex="D25">
                            <Renderer Fn="getFormattedAmount2" />
                        </ext:Column>
                        <ext:Column ID="Column79" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="31" DataIndex="D26">
                            <Renderer Fn="getFormattedAmount2" />
                        </ext:Column>
                        <ext:Column ID="Column80" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="31" DataIndex="D27">
                            <Renderer Fn="getFormattedAmount2" />
                        </ext:Column>
                        <ext:Column ID="Column81" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="31" DataIndex="D28">
                            <Renderer Fn="getFormattedAmount2" />
                        </ext:Column>
                        <ext:Column ID="LT29" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="31" DataIndex="D29" Hidden="true">
                            <Renderer Fn="getFormattedAmount2" />
                        </ext:Column>
                        <ext:Column ID="LT30" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="31" DataIndex="D30" Hidden="true">
                            <Renderer Fn="getFormattedAmount2" />
                        </ext:Column>
                        <ext:Column ID="LT31" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="31" DataIndex="D31" Hidden="true">
                            <Renderer Fn="getFormattedAmount2" />
                        </ext:Column>
                        <ext:Column ID="LT32" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="31" DataIndex="D32" Hidden="true">
                            <Renderer Fn="getFormattedAmount2" />
                        </ext:Column>
                        <ext:Column ID="Column30" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Total" Width="45" DataIndex="Total">
                            <Renderer Fn="getFormattedAmount2" />
                        </ext:Column>
                        <ext:ImageCommandColumn ID="ImageCommandColumn2" runat="server" Width="36" ButtonAlign="Center"
                            Hidden="false">
                        </ext:ImageCommandColumn>
                    </Columns>
                </ColumnModel>
                <SelectionModel>
                    <ext:CellSelectionModel EnableKeyNav="true" runat="server" ID="CellSelectionModel3">
                    </ext:CellSelectionModel>
                </SelectionModel>
                <View>
                    <ext:GridView ID="GridView3" StripeRows="false" runat="server">
                    </ext:GridView>
                </View>
            </ext:GridPanel>
        </td>
    </tr>
    <tr>
    </tr>
</table>
<table>
    <tr>
        <td style="width: 220px">
            <ext:ComboBox ID="cmbApplyTo" LabelAlign="Top" QueryMode="Local" DisplayField="Text"
                LabelWidth="120" Width="200" ValueField="Value" LabelSeparator="" ForceSelection="true"
                runat="server" FieldLabel="Approving Authority*">
                <Store>
                    <ext:Store ID="store4" runat="server">
                        <Model>
                            <ext:Model ID="Model5" runat="server" IDProperty="Value">
                                <Fields>
                                    <ext:ModelField Name="Value" Type="String" />
                                    <ext:ModelField Name="Text" />
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
            </ext:ComboBox>
        </td>
        <td valign="bottom">
            <ext:Button ID="btnAssign" runat="server" Width="150" Height="30" Text="Assign TimeSheet"
                Hidden="true">
                <DirectEvents>
                    <Click OnEvent="btnAssign_Click">
                        <%--<Confirmation ConfirmRequest="true" Message="Are you sure, you want to save the timesheet?" />--%>
                        <EventMask ShowMask="true">
                        </EventMask>
                        <ExtraParams>
                            <ext:Parameter Name="Values" Value="Ext.encode(#{gridProjects}.getRowsValues({selectedOnly : false}))"
                                Mode="Raw" />
                            <ext:Parameter Name="ValuesLeaves" Value="Ext.encode(#{gridLeaves}.getRowsValues({selectedOnly : false}))"
                                Mode="Raw" />
                            <ext:Parameter Name="TotalValues" Value="Ext.encode(#{gridLeavesTotal}.getRowsValues({selectedOnly : false}))"
                                Mode="Raw" />
                        </ExtraParams>
                    </Click>
                </DirectEvents>
                <%-- <Listeners>
                    <Click Handler="valGroup='AssignTimeSheet';return CheckValidation();" />
                </Listeners>--%>
            </ext:Button>
        </td>
        <td style="padding-top: 15px">
            <ext:Button ID="btnSave" runat="server" Width="150" Height="30" Text="Save and Finish Later">
                <DirectEvents>
                    <Click OnEvent="btnSave_Click">
                        <%--<Confirmation ConfirmRequest="true" Message="Are you sure, you want to save the timesheet?" />--%>
                        <EventMask ShowMask="true">
                        </EventMask>
                        <ExtraParams>
                            <ext:Parameter Name="Values" Value="Ext.encode(#{gridProjects}.getRowsValues({selectedOnly : false}))"
                                Mode="Raw" />
                            <ext:Parameter Name="ValuesLeaves" Value="Ext.encode(#{gridLeaves}.getRowsValues({selectedOnly : false}))"
                                Mode="Raw" />
                            <ext:Parameter Name="TotalValues" Value="Ext.encode(#{gridLeavesTotal}.getRowsValues({selectedOnly : false}))"
                                Mode="Raw" />
                        </ExtraParams>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </td>
        <td style="width: 20px">
        </td>
        <td style="padding-top: 15px">
            <ext:Button ID="btnSaveAndSend" runat="server" Width="150" Height="30" Text="Save and Send">
                <DirectEvents>
                    <Click OnEvent="btnSave_Click">
                        <Confirmation ConfirmRequest="true" Message="Are you sure, you want to submit the timesheet, after submission timesheet can not be changed?" />
                        <EventMask ShowMask="true">
                        </EventMask>
                        <ExtraParams>
                            <ext:Parameter Name="Values" Value="Ext.encode(#{gridProjects}.getRowsValues({selectedOnly : false}))"
                                Mode="Raw" />
                            <ext:Parameter Name="ValuesLeaves" Value="Ext.encode(#{gridLeaves}.getRowsValues({selectedOnly : false}))"
                                Mode="Raw" />
                            <ext:Parameter Name="TotalValues" Value="Ext.encode(#{gridLeavesTotal}.getRowsValues({selectedOnly : false}))"
                                Mode="Raw" />
                        </ExtraParams>
                    </Click>
                </DirectEvents>
            </ext:Button>
        </td>
    </tr>
</table>
<asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="Approval is required."
    ControlToValidate="cmbApplyTo" ValidationGroup="Timesheet" Display="None" />
