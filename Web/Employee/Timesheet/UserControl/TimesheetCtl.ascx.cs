﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using BLL.Manager;
using BLL.BO;
using BLL;
using DAL;
using BLL.Entity;
using Utils.Calendar;
using Web.CP.Report.Templates.HR;
using System.IO;
using Utils;
using Utils.Helper;

namespace Web.Employee.UserControls
{
    public partial class TimesheetCtl : BaseUserControl
    {
        /// <summary>
        /// Hold employee timesheet currently displaying 
        /// </summary>
        public int ReadonlyTimesheetId
        {
            set;get;
        }

        public int ReadonlyEmployeeId
        {
            set;
            get;
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {

            X.Js.AddScript("selectCurrentCell();");
        }
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!X.IsAjaxRequest)
            {
                DateTime date = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
                hiddenDate.Text = date.ToShortDateString();
                dateField.SelectedDate = date;


                // display for viewing week
                if (!string.IsNullOrEmpty(Request.QueryString["month"]))
                {
                    DateTime date1;
                    if (DateTime.TryParse(Request.QueryString["month"].ToString(), out date1))
                    {

                        date = new DateTime(date1.Year, date1.Month, date1.Day);
                        hiddenDate.Text = date.ToShortDateString();
                        dateField.SelectedDate = date;
                    }
                }

                // display selected employee timesheet from approval page
                string id = Request.QueryString["id"];
                int timesheetId = 0;
                if (int.TryParse(id, out timesheetId))
                {
                    DAL.Timesheet sheet = NewTimeSheetManager.GetTimeSheetById(timesheetId);
                    if (sheet != null && NewTimeSheetManager.IsTimesheetViewableForManager(timesheetId))
                    {
                        this.ReadonlyTimesheetId = timesheetId;
                        this.ReadonlyEmployeeId = sheet.EmployeeId;
                        date = new DateTime(sheet.StartDate.Value.Year, sheet.StartDate.Value.Month, sheet.StartDate.Value.Day);
                        hiddenDate.Text = date.ToShortDateString();
                        dateField.SelectedDate = date;
                    }
                }


                SetProjectHeader(NewTimeSheetManager.GetMonthStartDate(date));
                Initialise();
                
            }


        }

        public int EmployeeId
        {
            set { }
            get
            {
                return SessionManager.CurrentLoggedInEmployeeId;
            }
        }

        public void SetProjectHeader(DateTime weekStartDate)
        {
            //List<WeeklyHoliday> list = NewTimeSheetManager.GetWeeklyHolidayList();

            //for (int i = 1; i <= 7; i++)
            //{

            //    Column column = gridProjects.ColumnModel.FindControl("D" + i) as Column;

            //    if (column != null)
            //    {
            //        column.Text ="<b>" +  weekStartDate.ToString("ddd") + "</b>" + " </br> "+weekStartDate.ToString("MMM d");

            //        if (IsWeeklyHoliday(list, weekStartDate))
            //            column.Cls = "weeklyColor";   
            //    }

            //    column = gridLeaves.ColumnModel.FindControl("L" + i) as Column;

            //    if (column != null)
            //    {
            //        column.Text = "<b>" + weekStartDate.ToString("ddd") + "</b>" + " </br> " + weekStartDate.ToString("MMM d");

            //        if (IsWeeklyHoliday(list, weekStartDate))
            //            column.Cls = "weeklyColor";
            //    }

            //    weekStartDate = weekStartDate.AddDays(1);
            //}
        }

        public bool IsWeeklyHoliday(List<WeeklyHoliday> list, DateTime date)
        {
            DayOfWeek day = date.DayOfWeek;
            foreach (WeeklyHoliday item in list)
            {
                if (item.DayOfWeek == (int)day)
                    return true;
            }
            return false;
        }

        protected void btnPrevious_Click(object sender, DirectEventArgs e)
        {
            DateTime date = Convert.ToDateTime(hiddenDate.Text.Trim()); ;
            date = date.AddMonths(-1);


            hiddenDate.Text = date.ToShortDateString();

            X.Js.AddScript("#{{dateField}}.setValue(new Date({0},{1},{2}));", date.Year, date.Month-1, date.Day);

            btnLoad_Click(null, null);
            
        }
        protected void btnNext_Click(object sender, DirectEventArgs e)
        {
            DateTime date = Convert.ToDateTime(hiddenDate.Text.Trim()); ;
            date = date.AddMonths(1);
            hiddenDate.Text = date.ToShortDateString();

            X.Js.AddScript("#{{dateField}}.setValue(new Date({0},{1},{2}));", date.Year, date.Month-1, date.Day);


            btnLoad_Click(null, null);
        }

        private bool SetTimesheetDetails(out List<TimesheetProject> projectList, out List<TimesheetLeave> leaveList,
            List<TimesheetGridNewBO> inputProjectList, List<TimesheetGridNewBO> inputLeavesList,DateTime weekStart,int EmpID
            , TimeSheetStatus statusMode)
        {

           

            projectList = new List<TimesheetProject>();
            leaveList = new List<TimesheetLeave>();
            //double value;
            TimesheetGridNewBO row;
            weekStart = new DateTime(weekStart.Year, weekStart.Month, 1);
           
             DateTime monthEndDate =new DateTime(weekStart.Year,weekStart.Month, 
                 DateTime.DaysInMonth(weekStart.Year,weekStart.Month));

            int projectId = 0;

            DateTime week = weekStart;

             if (inputProjectList.Count <= 0)
                return false;
            

            for (int i = 0; i < inputProjectList.Count; i++)
            {


                week = weekStart;
                row = inputProjectList[i];

                if (row.Id == null || row.Id == 0)
                    continue;

                
                projectId = row.Id.Value;

                if (statusMode != TimeSheetStatus.Draft)
                {
                    if (row.D1.Value != 0 || row.D2.Value != 0 || row.D3.Value != 0 || row.D4.Value != 0 || row.D5.Value != 0)
                    {
                        if (inputProjectList[i].Activity <= 0)
                        {
                            NewMessage.ShowWarningMessage("Activity is required");
                            return false;
                        }
                    }
                }

                if (row.D1.Value != 0 || row.D2.Value != 0 || row.D3.Value != 0 || row.D4.Value != 0 || row.D5.Value != 0)
                {

                    string msg = "";
                    if (NewTimeSheetManager.IsValidActivityIDForCare((long)inputProjectList[i].Activity, ref msg, true) == false)
                    {
                        NewMessage.ShowWarningMessage(msg);
                        return false;
                    }

                    //if (inputProjectList[i].Activity > 500 && inputProjectList[i].Activity < 999999999999999)
                    //{
                    //    NewMessage.ShowWarningMessage("Activity should be 1-500 or 999999999999999");
                    //    return false;
                    //}

                    //if (inputProjectList[i].Activity > 999999999999999)
                    //{
                    //    NewMessage.ShowWarningMessage("Activity should be 1-500 or 999999999999999");
                    //    return false;
                    //}
                }

                if (row.D1 != null && row.D1 != 0)
                    projectList.Add(new TimesheetProject { ProjectId = projectId, Activity = inputProjectList[i].Activity, Category = inputProjectList[i].Category, WeekNumber = 1, StartDate = week, EndDate = week.AddDays(6), Hours = row.D1.Value, EmployeeId = EmpID, Notes = row.D1Comment });
                week = week.AddDays(7);
                if (row.D2 != null && row.D2 != 0)
                    projectList.Add(new TimesheetProject { ProjectId = projectId, Activity = inputProjectList[i].Activity, Category = inputProjectList[i].Category, WeekNumber = 2, StartDate = week, EndDate = week.AddDays(6), Hours = row.D2.Value, EmployeeId = EmpID, Notes = row.D2Comment });
                week = week.AddDays(7);
                if (row.D3 != null && row.D3 != 0)
                    projectList.Add(new TimesheetProject { ProjectId = projectId, Activity = inputProjectList[i].Activity, Category = inputProjectList[i].Category, WeekNumber = 3, StartDate = week, EndDate = week.AddDays(6), Hours = row.D3.Value, EmployeeId = EmpID, Notes = row.D3Comment });
                week = week.AddDays(7);
                if (row.D4 != null && row.D4 != 0)
                    projectList.Add(new TimesheetProject { ProjectId = projectId, Activity = inputProjectList[i].Activity, Category = inputProjectList[i].Category, WeekNumber = 4, StartDate = week, EndDate = week.AddDays(6), Hours = row.D4.Value, EmployeeId = EmpID, Notes = row.D4Comment });
                week = week.AddDays(7);

            
                // for 28 days month,now working for feb month,so comment validation
                //if (week < monthEndDate)
                {
                    if (row.D5 != null && row.D5 != 0)
                        projectList.Add(new TimesheetProject { ProjectId = projectId, Activity = inputProjectList[i].Activity, Category = inputProjectList[i].Category, WeekNumber = 5, StartDate = week, EndDate = monthEndDate, Hours = row.D5.Value, EmployeeId = EmpID, Notes = row.D5Comment });
                }
            }


            int leaveTypeId = 0;
            
            for (int i = 0; i < inputLeavesList.Count; i++)
            {
                week = weekStart;

                row = inputLeavesList[i];
                leaveTypeId = row.Id.Value;

                if (row.L1 != null && row.L1 != 0)
                    leaveList.Add(new TimesheetLeave { LeaveId = leaveTypeId, WeekNumber = 1, StartDate = week, EndDate = week.AddDays(6), Hours = row.L1.Value, EmployeeId = EmpID });
                week = week.AddDays(7);
                if (row.L2 != null && row.L2 != 0)
                    leaveList.Add(new TimesheetLeave { LeaveId = leaveTypeId, WeekNumber = 2, StartDate = week, EndDate = week.AddDays(6), Hours = row.L2.Value, EmployeeId = EmpID });
                week = week.AddDays(7);
                if (row.L3 != null && row.L3 != 0)
                    leaveList.Add(new TimesheetLeave { LeaveId = leaveTypeId, WeekNumber = 3, StartDate = week, EndDate = week.AddDays(6), Hours = row.L3.Value, EmployeeId = EmpID });
                week = week.AddDays(7);
                if (row.L4 != null && row.L4 != 0)
                    leaveList.Add(new TimesheetLeave { LeaveId = leaveTypeId, WeekNumber = 4, StartDate = week, EndDate = week.AddDays(6), Hours = row.L4.Value, EmployeeId = EmpID });
                week = week.AddDays(7);

                // for 28 days month,now working for feb month,so comment validation
                //if (week < monthEndDate)
                {
                    if (row.L5 != null && row.L5 != 0)
                        leaveList.Add(new TimesheetLeave { LeaveId = leaveTypeId, WeekNumber = 5, StartDate = week, EndDate = monthEndDate, Hours = row.L5.Value, EmployeeId = EmpID });
                }
            }
            //int leaveId = 0;
            //int payrollPeriodId = int.Parse(ddlPayrollPeriods.SelectedValue);
            //PayrollPeriod period = CommonManager.GetPayrollPeriod(payrollPeriodId);

            //for (int i = 0; i < gvw2Leaves.Rows.Count; i++)
            //{
            //    row = gvw2Leaves.Rows[i];
            //    leaveId = (int)gvw2Leaves.DataKeys[row.RowIndex][0];
            //    DateTime currentDate = period.StartDateEng.Value;
            //    for (int day = 1; day <= 31; day++)
            //    {
            //        if (day != 1)
            //            currentDate = currentDate.AddDays(1);

            //        txt = row.FindControl("txtDay" + day) as TextBox;
            //        if (txt != null && txt.Text != "" && txt.Text != "." && double.TryParse(txt.Text.Trim(), out value))
            //        {
            //            if (value != 0)
            //            {
            //                leaveList.Add(
            //                    new TimesheetLeave { LeaveId = leaveId, DayNumber = (byte)day, Hours = value, DateEng = currentDate }
            //                    );
            //            }
            //        }


            //        // Previous Month
            //        if (day >= 27)
            //        {
            //            txt = row.FindControl("txtP" + day) as TextBox;
            //            if (txt != null && txt.Text != "" && txt.Text != "." && double.TryParse(txt.Text.Trim(), out value))
            //            {
            //                if (value != 0)
            //                {
            //                    leaveList.Add(
            //                        new TimesheetLeave { LeaveId = leaveId, DayNumber = (byte)day, Hours = value, DateEng = currentDate, IsPreviousMonth = true }
            //                        );
            //                }
            //            }
            //        }
            //    }
            //}
            return true;
        }

        protected void btnAssign_Click(object sender, DirectEventArgs e)
        {

            string json = e.ExtraParams["Values"];
            string jsonLeaves = e.ExtraParams["ValuesLeaves"];
            string TotalValues = e.ExtraParams["TotalValues"];
            TimeSheetStatus statusMode = statusMode = TimeSheetStatus.Approved;

            if (string.IsNullOrEmpty(json))
            {
                return;
            }

            List<TimesheetGridNewBO> lines = JSON.Deserialize<List<TimesheetGridNewBO>>(json);
            List<TimesheetGridNewBO> leaveLines = JSON.Deserialize<List<TimesheetGridNewBO>>(jsonLeaves);
            List<TimesheetGridNewBO> totalValues = JSON.Deserialize<List<TimesheetGridNewBO>>(TotalValues);

            //check for duplicate Project Name in list
            foreach (TimesheetGridNewBO _item in lines)
            {

                List<TimesheetGridNewBO> ListFindDuplicate = lines.Where(x => x.Id == _item.Id).ToList();
                if (ListFindDuplicate.Count > 1)
                {
                    NewMessage.ShowWarningMessage("Same Project exists in list.");
                    return;
                }
            }

            // prepare project list
            int EmpID = EmployeeId;

            if (!string.IsNullOrEmpty(cmbSearch.SelectedItem.Value) && isSuperviserEntry())
            {
                EmpID = int.Parse(cmbSearch.SelectedItem.Value);
            }

            double totalHours = 0;

            totalHours = totalValues[1].Total == null ? 0 : totalValues[1].Total.Value;

            //if (totalHours < 40 || totalHours > 70)
            //{
            //    NewMessage.ShowWarningMessage("Total hours should be in the range 40-70.");
            //    return;
            //}

            DateTime weekStartDate = Convert.ToDateTime(hiddenWeekStartDate.Text);

            List<TimesheetProject> projectList;
            List<TimesheetLeave> leaveList;
            if (!SetTimesheetDetails(out projectList, out leaveList, lines, leaveLines, weekStartDate, EmpID, statusMode))
                return;
           
            ResponseStatus status = NewTimeSheetManager.SaveUpdateProjectTimesheet(weekStartDate, totalHours,
                 EmpID, projectList, leaveList, (int)statusMode,SessionManager.CurrentLoggedInEmployeeId);

            if (status.IsSuccessType == true)
            {

                if (statusMode == TimeSheetStatus.Approved)
                {
                    NewMessage.ShowNormalMessage("Timesheet has been  approved.");
                    btnSave.Hide();
                    btnSaveAndSend.Hide();
                    btnLoad_Click(null, null);

                }
                
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);

            }
        }

        protected void btnSave_Click(object sender, DirectEventArgs e)
        {

            string json = e.ExtraParams["Values"];
            string jsonLeaves = e.ExtraParams["ValuesLeaves"];
            string TotalValues = e.ExtraParams["TotalValues"];
            TimeSheetStatus statusMode = statusMode = (sender == btnSaveAndSend ? TimeSheetStatus.AwaitingApproval : TimeSheetStatus.Draft);
       
            if (string.IsNullOrEmpty(json))
            {
                return;
            }

            List<TimesheetGridNewBO> lines = JSON.Deserialize<List<TimesheetGridNewBO>>(json);
            List<TimesheetGridNewBO> leaveLines = JSON.Deserialize<List<TimesheetGridNewBO>>(jsonLeaves);
            List<TimesheetGridNewBO> totalValues = JSON.Deserialize<List<TimesheetGridNewBO>>(TotalValues);

            //check for duplicate Project Name in list
            foreach (TimesheetGridNewBO _item in lines)
            {

                List<TimesheetGridNewBO> ListFindDuplicate = lines.Where(x => x.Id == _item.Id).ToList();
                if (ListFindDuplicate.Count>1)
                {
                    NewMessage.ShowWarningMessage("Same Project exists in list.");
                    return;
                }
            }

            // prepare project list
            int EmpID = EmployeeId;

            double totalHours = 0;

            totalHours = totalValues[1].Total == null ? 0 : totalValues[1].Total.Value;

            if (totalHours <= 0)
            {
                NewMessage.ShowWarningMessage("Total hours in project is required");
                return;
            }


            //if (totalHours < 40 || totalHours > 70)
            //{
            //    NewMessage.ShowWarningMessage("Total hours should be in the range 40-70.");
            //    return;
            //}

            DateTime weekStartDate = Convert.ToDateTime(hiddenWeekStartDate.Text);

            List<TimesheetProject> projectList;
            List<TimesheetLeave> leaveList;
            if (!SetTimesheetDetails(out projectList, out leaveList, lines, leaveLines, weekStartDate, EmpID, statusMode))
                return ;
            //string otherProjectName = Request.Form[hiddenOtherProjectName.ClientID.Replace("_", "$")];
            //string changeTotalHours = Request.Form[txtGrandTotal.ClientID.Replace("_", "$")];
            //if (double.TryParse(changeTotalHours, out totalHours))
            //{ }
            //else
            //{
            //    if (txtGrandTotal.Text.Trim() == "")
            //        totalHours = 0;
            //    else
            //        totalHours = float.Parse(txtGrandTotal.Text.Trim());

            //}

            


            ResponseStatus status = NewTimeSheetManager.SaveUpdateProjectTimesheet(weekStartDate, totalHours,
                 EmpID, projectList, leaveList, (int)statusMode, int.Parse(cmbApplyTo.SelectedItem.Value));

            if (status.IsSuccessType == true)
            {

                if (statusMode == TimeSheetStatus.AwaitingApproval)
                {
                    NewMessage.ShowNormalMessage("Timesheet has been submitted for approval.");
                    btnSave.Hide();
                    btnSaveAndSend.Hide();
                    SendMailForApproval(EmpID);
                    btnLoad_Click(null, null);

                }
                else
                {
                    NewMessage.ShowNormalMessage("Timesheet has been saved.");
                    btnLoad_Click(null, null);
                }


                //LoadAttedance();
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);

            }

        }

        protected void SendMailForApproval(int Requestor)
        {

            SMTPHelper mailSender = new SMTPHelper();
            EmailContent dbMailContent = CommonManager.GetMailContent((int)EmailContentType.TimeSheetSendForApproval);

            DateTime weekStartDate = Convert.ToDateTime(hiddenWeekStartDate.Text);

            string MonthName = weekStartDate.Year + " - " +
            DateHelper.GetMonthName(weekStartDate.Month, true);

            EEmployee _RequestorEmployeee = EmployeeManager.GetEmployeeById(Requestor);
            string RequestBy = _RequestorEmployeee.Title + " "  + _RequestorEmployeee.Name;

            string subject = dbMailContent.Subject.Replace("#YearMonth#", MonthName).Replace("#Requester#", RequestBy);

            string body = dbMailContent.Body.Replace("#YearMonth#", MonthName).Replace("#Requester#", RequestBy);
            string to = "";

            EEmployee _Employeee = EmployeeManager.GetEmployeeById(int.Parse(cmbApplyTo.SelectedItem.Value));
            if (string.IsNullOrEmpty(_Employeee.EAddresses[0].CIEmail))
            {
                NewMessage.ShowNormalMessage("Email address not found for Employee " + _Employeee.Name);
                return;
            }

            if (!string.IsNullOrEmpty(to))
                to += "," + _Employeee.EAddresses[0].CIEmail;
            else
                to = _Employeee.EAddresses[0].CIEmail;


            string[] ToEmailIDs = to.Split(',');
            int count = 0;
            foreach (string ToEMailId in ToEmailIDs)
            {
                bool isSendSuccess = SMTPHelper.SendMail(ToEMailId, body, subject, "","");
                if (isSendSuccess)
                    count++;
            }

            if (count > 0)
            {
                //  NewMessage.ShowNormalMessage(string.Format(Resources.Messages.SentMailSuccessMsg, count));
                return;
            }
           

        }
        protected void ShowHideControls()
        {
            cmbApplyTo.Hide();
            btnSave.Hide();
            btnSaveAndSend.Hide();
            btnAssign.Show();
        }

       

        protected void btnLoad_Click(object sender, DirectEventArgs e)
        {

            int EmployeeId = this.ReadonlyEmployeeId == 0 ? this.EmployeeId : this.ReadonlyEmployeeId;

            if (!string.IsNullOrEmpty(cmbSearch.SelectedItem.Value) && isSuperviserEntry())
            {

                EmployeeId = int.Parse(cmbSearch.SelectedItem.Value);
                List<GetAssginedEmployeeProjectListResult> projectListbyEmployee = NewTimeSheetManager.GetEmployeeProjectList(EmployeeId);

                storeProjects.DataSource = projectListbyEmployee;
                storeProjects.DataBind();
            }

            DateTime date = Convert.ToDateTime(hiddenDate.Text.Trim());

            DateTime weekStartDate = NewTimeSheetManager.GetMonthStartDate(date);

            //DateTime MonthEndDate =  CustomDate.GetCustomDateFromString(date.ToString(), IsEnglish).GetLastDateOfThisMonth().EnglishDate;

            //if (Convert.ToDateTime(hiddenWeekStartDate.Text) == weekStartDate)
            //    return;


            hiddenWeekStartDate.Text = weekStartDate.ToShortDateString();

            string rejectionNotes = "";

            DAL.Timesheet timesheet = null;
            TimeSheetStatus status = NewTimeSheetManager.GetTimesheetStatus(weekStartDate,
               EmployeeId, ref rejectionNotes, ref timesheet);

            if (timesheet != null && timesheet.ApprovedEmployeeId != null)
            {
                if (!X.IsAjaxRequest)
                    ExtControlHelper.ComboBoxSetSelected(timesheet.ApprovedEmployeeId.ToString(), cmbApplyTo);
                else
                    cmbApplyTo.SetValue(timesheet.ApprovedEmployeeId.ToString());
            }
            else
            {
                if (X.IsAjaxRequest)
                    cmbApplyTo.ClearValue();
            }


            if (status == TimeSheetStatus.NotSubmitted || status == TimeSheetStatus.Draft)
            {
                btnSave.Show();
                if (isSuperviserEntry())
                btnAssign.Show();
                btnSaveAndSend.Show();
            }
            // if submitted show save and send to change approval
            else if (status == TimeSheetStatus.AwaitingApproval)
            {
                btnSave.Hide();
                btnSaveAndSend.Show();
            }
            else
            {
                btnSave.Hide();
                btnAssign.Hide();
                btnSaveAndSend.Hide();
            }

            //for setting lastDays for Week5
            int lastDays = 0;
            for (int i = 29; i <= DateTime.DaysInMonth(weekStartDate.Year, weekStartDate.Month); i++)
            {
                DateTime day = new DateTime(weekStartDate.Year, weekStartDate.Month, i);
                if (day.DayOfWeek != DayOfWeek.Saturday && day.DayOfWeek != DayOfWeek.Sunday)
                {
                    lastDays += 1;
                }
            }

            hdnLastCount.Text = lastDays.ToString();

            //DateTime weekEndDate = weekStartDate.AddDays(6);
            dispDate.Text = DateHelper.GetMonthName(weekStartDate.Month, true) + " " + weekStartDate.Year;
            dispStatus.Text =  (status == TimeSheetStatus.NotSubmitted ? "Not Filled" : status.ToString());

            if (status == TimeSheetStatus.Rejected)
            {
                btnSaveAndSend.Show();
                dispStatus.Text += ", Rejection Notes : " + rejectionNotes;
            }
            // if timesheet is being viewed by manager then hide all buttons
            if (this.ReadonlyEmployeeId != 0)
            {
                btnSave.Hide();
                btnAssign.Hide();
                btnSaveAndSend.Hide();
                //Button1.Hide();
                btnNext.Hide();
                btnPrevious.Hide();
                dateField.Disable();
            }

            List<TimesheetGridNewBO> projectList = NewTimeSheetManager.GetRepeatedProjectList(weekStartDate,
                EmployeeId);


            gridProjects.Store[0].DataSource = projectList;
            gridProjects.Store[0].DataBind();

            TimesheetGridNewBO total = new TimesheetGridNewBO{Name="Total"};
            List<TimesheetGridNewBO> totalList = new List<TimesheetGridNewBO>();
            totalList.Add(total);           
            
            total.D1 = projectList.Sum(x => x.D1 == null ? 0 : x.D1.Value);
            total.D2 = projectList.Sum(x => x.D2 == null ? 0 : x.D2.Value);
            total.D3 = projectList.Sum(x => x.D3 == null ? 0 : x.D3.Value);
            total.D4 = projectList.Sum(x => x.D4 == null ? 0 : x.D4.Value);
            total.D5 = projectList.Sum(x => x.D5 == null ? 0 : x.D5.Value);
           
            total.Total= projectList.Sum(x => x.Total == null ? 0 : x.Total.Value);

            // add time card row
            //TimesheetGridNewBO timecard = NewTimeSheetManager.GetTimecard(
            //    this.ReadonlyEmployeeId == 0 ? this.EmployeeId : this.ReadonlyEmployeeId, weekStartDate);
            //totalList.Add(timecard);

            gridProjectsTotal.Store[0].DataSource = totalList;
            gridProjectsTotal.Store[0].DataBind();


            List<TimesheetGridNewBO> leaveList = NewTimeSheetManager.GetLeaveList(weekStartDate, EmployeeId);
            foreach (TimesheetGridNewBO item in leaveList)
            {
                item.L1 = item.D1;
                item.L2 = item.D2;
                item.L3 = item.D3;
                item.L4 = item.D4;
                item.L5 = item.D5;
            }
            gridLeaves.Store[0].DataSource = leaveList;
            gridLeaves.Store[0].DataBind();


            TimesheetGridNewBO leavetotal = new TimesheetGridNewBO { Name = "Total" };
            List<TimesheetGridNewBO> totalLeaveList = new List<TimesheetGridNewBO>();
            totalLeaveList.Add(leavetotal);

            leavetotal.D1 = leaveList.Sum(x => x.L1 == null ? 0 : x.L1.Value);
            leavetotal.D2 = leaveList.Sum(x => x.L2 == null ? 0 : x.L2.Value);
            leavetotal.D3 = leaveList.Sum(x => x.L3 == null ? 0 : x.L3.Value);
            leavetotal.D4 = leaveList.Sum(x => x.L4 == null ? 0 : x.L4.Value);
            leavetotal.D5 = leaveList.Sum(x => x.L5 == null ? 0 : x.L5.Value);
            leavetotal.Total = leaveList.Sum(x => x.Total == null ? 0 : x.Total.Value);

          


            TimesheetGridNewBO grandTotal = new TimesheetGridNewBO { Name = "Grand Total" };
            totalLeaveList.Add(grandTotal);

            grandTotal.D1 = projectList.Sum(x => x.D1 == null ? 0 : x.D1.Value)+ leaveList.Sum(x => x.L1 == null ? 0 : x.L1.Value);
            grandTotal.D2 = projectList.Sum(x => x.D2 == null ? 0 : x.D2.Value) + leaveList.Sum(x => x.L2 == null ? 0 : x.L2.Value);
            grandTotal.D3 = projectList.Sum(x => x.D3 == null ? 0 : x.D3.Value) + leaveList.Sum(x => x.L3 == null ? 0 : x.L3.Value);
            grandTotal.D4 = projectList.Sum(x => x.D4 == null ? 0 : x.D4.Value) + leaveList.Sum(x => x.L4 == null ? 0 : x.L4.Value);
            grandTotal.D5 = projectList.Sum(x => x.D5 == null ? 0 : x.D5.Value) + leaveList.Sum(x => x.L5 == null ? 0 : x.L5.Value);
            grandTotal.Total = projectList.Sum(x => x.Total == null ? 0 : x.Total.Value) +leaveList.Sum(x => x.Total == null ? 0 : x.Total.Value);

            gridLeavesTotal.Store[0].DataSource = totalLeaveList;
            gridLeavesTotal.Store[0].DataBind();

            if (isSuperviserEntry())
            {
                ShowHideControls();
                if (status == TimeSheetStatus.NotSubmitted || status == TimeSheetStatus.Draft)
                {
                    btnAssign.Show();
                }
                else
                    btnAssign.Hide();
            }

        }

        private void Initialise()
        {
           

            List<GetAssginedEmployeeProjectListResult> projectList = NewTimeSheetManager.GetEmployeeProjectList(
                this.ReadonlyEmployeeId == 0 ? SessionManager.CurrentLoggedInEmployeeId : this.ReadonlyEmployeeId);

            storeProjects.DataSource = projectList;
            storeProjects.DataBind();

            storeLeaves.DataSource = LeaveAttendanceManager.GetAllLeavesWithManualAlso(SessionManager.CurrentCompanyId);
            storeLeaves.DataBind();

            List<TextValue> listApproval = LeaveRequestManager.GetApplyToListForEmployee(SessionManager.CurrentLoggedInEmployeeId, true, PreDefindFlowType.Leave);
            cmbApplyTo.Store[0].DataSource = listApproval;
            cmbApplyTo.Store[0].DataBind();
            if (isSuperviserEntry())
            {
                storeEmployee.DataSource = LeaveAttendanceManager.GetEmployeeListForLeaveAssign();
                storeEmployee.DataBind();
                cmbSearch.Show();
                ShowHideControls();
                Pagetitle.InnerText = "Assign TimeSheet";

                //TextValue _TextValue = new TextValue();
                //_TextValue.Value = SessionManager.CurrentLoggedInEmployeeId.ToString();
                //_TextValue.Text = EmployeeManager.GetEmployeeById(SessionManager.CurrentLoggedInEmployeeId).Name;
                //listApproval.Clear();
                //listApproval.Add(_TextValue);
            }
            else
                btnLoad_Click(null, null);
          
            

         


        }

        protected bool isSuperviserEntry()
        {
            bool isSuperviserEntry = false;
            if (!string.IsNullOrEmpty(Request.QueryString["SuperviserEntry"]))
                isSuperviserEntry = bool.Parse(Request.QueryString["SuperviserEntry"]);
            return isSuperviserEntry;
        }

        protected void Click_Export(object sender, EventArgs e)
        {

            bool isPDF = (sender == btnExportPDF) ? true : false;


            DAL.Timesheet timesheet = null;
            string rejectionNotes = "";
            DateTime weekStart = Convert.ToDateTime(hiddenWeekStartDateExcel.Text);
            weekStart = new DateTime(weekStart.Year, weekStart.Month, 1);

            TimeSheetStatus status = NewTimeSheetManager.GetTimesheetStatus(weekStart,
                this.ReadonlyEmployeeId == 0 ? this.EmployeeId : this.ReadonlyEmployeeId, ref rejectionNotes, ref timesheet);

            if (timesheet == null)
            {
                NewMessage.ShowWarningMessage("Timesheet not saved.");
                return;
            }

            
            PAReport report = GetPAR(timesheet.TimesheetId.ToString());
            GetSubReportPAR(timesheet.TimesheetId.ToString());
          //  PAReportTimeAllocationInfo PASubReport = GetSubReportPAR();

            using (MemoryStream stream = new MemoryStream())
            {
                if (isPDF)
                    report.ExportToPdf(stream);
                else
                    report.ExportToXls(stream);

                HttpContext.Current.Response.Clear();
                if (isPDF)
                {

                    HttpContext.Current.Response.ContentType = "application/pdf";
                    HttpContext.Current.Response.AppendHeader("Content-Type", "application/pdf");

                    HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment; filename=\"" + report.labelTitle.Text + " Statement.pdf\"");

                }
                else
                {
                    
                    report.ExportToXls(stream);
                    HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";
                    HttpContext.Current.Response.AppendHeader("Content-Type", "application/vnd.ms-excel");

                    HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment; filename=\"" + report.labelTitle.Text + " Statement.xls\"");
                }

                HttpContext.Current.Response.AddHeader("Content-Length", stream.ToArray().Length.ToString());

                HttpContext.Current.Response.OutputStream.Write(stream.ToArray(), 0, stream.ToArray().Length);
                HttpContext.Current.Response.End();
            }
        }

        private PAReport GetPAR(string TimeSheetID)
        {

            List<GetPAREmployeeHeaderResult> data = EmployeeManager.GetPAREmployeeHeader(TimeSheetID).ToList();

           


            ReportDataSetTableAdapters.GetPAREmployeeHeaderTableAdapter adap =
                new ReportDataSetTableAdapters.GetPAREmployeeHeaderTableAdapter();
            ReportDataSet ds = new ReportDataSet();

            adap.Connection.ConnectionString = Config.ConnectionString;
            ReportDataSet.GetPAREmployeeHeaderDataTable table = adap.GetData(TimeSheetID);

            // Process for report
            PAReport report = new PAReport();
            report.DataSource = table;
            report.DataMember = "Report";
//            report.labelTitle.Text = "";

            

            return report;
        }


        private void GetSubReportPAR(string TimeSheetID)
        {
            


            DateTime weekStart = Convert.ToDateTime(hiddenWeekStartDateExcel.Text);
            weekStart = new DateTime(weekStart.Year, weekStart.Month, 1);

            
            DAL.Timesheet timesheet = null;
            string rejectionNotes = "";
            TimeSheetStatus status = NewTimeSheetManager.GetTimesheetStatus(weekStart,
                this.ReadonlyEmployeeId == 0 ? this.EmployeeId : this.ReadonlyEmployeeId, ref rejectionNotes, ref timesheet);

         
            DateTime monthEndDate = new DateTime(weekStart.Year, weekStart.Month,
            DateTime.DaysInMonth(weekStart.Year, weekStart.Month));

            DateTime week1 = weekStart;
            DateTime week2 = week1.AddDays(7);
            DateTime week3 =week2.AddDays(7);
            DateTime week4 = week3.AddDays(7);
            DateTime week5 = week4.AddDays(7);

            if (week4 < monthEndDate)
            {
                week5 = week4.AddDays(7);
            }

          //  List<GetTimeAllocationInfoByWeekResult> data = EmployeeManager.ReportGetTimeAllocationInfoByWeek(this.ReadonlyEmployeeId == 0 ? this.EmployeeId : this.ReadonlyEmployeeId,week1,week2,week3,week4,week5);

            ReportDataSetTableAdapters.GetTimeAllocationInfoByWeekTableAdapter adap =
                new ReportDataSetTableAdapters.GetTimeAllocationInfoByWeekTableAdapter();
            ReportDataSet ds = new ReportDataSet();


            adap.Connection.ConnectionString = Config.ConnectionString;
            ReportDataSet.GetTimeAllocationInfoByWeekDataTable table = adap.GetData(week1, week2, week3, week4, week5, monthEndDate, TimeSheetID);
            HttpContext.Current.Items["PARTimeAllocationInfo"] = table;

            //store Employee Leave information

            ReportDataSetTableAdapters.ReportGetEmployeeLeaveListTableAdapter adap1 =
                new ReportDataSetTableAdapters.ReportGetEmployeeLeaveListTableAdapter();
            ReportDataSet ds1 = new ReportDataSet();


            adap.Connection.ConnectionString = Config.ConnectionString;
            ReportDataSet.ReportGetEmployeeLeaveListDataTable table1 = adap1.GetData(week1, week2, week3, week4, week5, monthEndDate, TimeSheetID);
            HttpContext.Current.Items["PAREmployeeLeaveInfo"] = table1;
            //-----------------------------



        }


    }
}
