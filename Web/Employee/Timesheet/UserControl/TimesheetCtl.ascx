﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TimesheetCtl.ascx.cs"
    Inherits="Web.Employee.UserControls.TimesheetCtl" %>
<script type="text/javascript">
    //method that will be called when item selected changes for Item in the GridPanel
    var projectRenderer = function (value) {
        
        var r = <%=storeProjects.ClientID %>.getById(value);
        if (Ext.isEmpty(r)) {
            return "";
        }
        return r.data.Name;
    };

   
    var leaveRenderer = function (value) {
        var r = <%=storeLeaves.ClientID %>.getById(value);

        if (Ext.isEmpty(r)) {
            if(parseFloat(value)==0 || parseFloat(value)==-1)
                return "Unpaid Leave";
            if(parseFloat(value)==-2)
                return "Public Holiday";

            return "";
        }

        return r.data.Title;
    };

    var record;
    var dayField;

    var beforeEditLeave = function(e1,e)
    {
        //disable for Category and project
       //  if(e.colIdx==6 || e.colIdx==0)
        {
             e.cancel = true;
            return;
        }

//        //for not holiday enable Hours in some extreme case
//        if( e.record.data.Id != -2)
//        {
//            e.cancel = true;
//            return;
//        }
    }

    var beforeEdit = function(e1,e) 
    {
        record = e.record;
        dayField = e.field;



        //disable for Category and project
        if(e.colIdx==2 || e.colIdx==0)
        {
             e.cancel = true;
            return;
        }
        
        <%=txtComment.ClientID %>.setValue(e.record.get(dayField + 'Comment'));
          
        var isFixed = e.record.get(dayField + 'IsFixed');

        if(isFixed)
        {
            e.cancel = true;
            return;
        }

     

        if(e.field != "Id" && e.record.data.Id=="")
        {
            e.cancel = true;
            return;
        }
       
      
    }

    var MoreHandler1 = function (column, command, record, recordIndex, cellIndex) {

        var store = this.grid.store;
        store.remove(record);
        setTotal(null);
    };

    var setComment=function()
    {
        var comment = <%=txtComment.ClientID %>.getValue();
        record.set(dayField + 'Comment',comment);
        <%=window.ClientID %>.hide();
    }
    var afterEdit = function (e1, e2, e3, e4,e5,e6) {
        e2.record.commit();
        var projectTotal =  setTotal(e2.record);

        var records = <%=gridProjects.ClientID %>.getStore();
        var i = 0;
        for (; i < records.data.length; ++i) {
            record = records.data.items[i];

           
            
            var rowTotal = record.data.D1 + record.data.D2+ record.data.D3+ record.data.D4
                    + record.data.D5;

            var percent = (parseFloat(rowTotal / projectTotal)) * 100;
           


            if(isNaN(percent))
                percent = 0;
            
            record.data.ProjectPercent = percent;
            record.commit();  
        }            


    }

    var displayComment = function(x,y)
    {   
   
         <%=window.ClientID %>.show();
         <%=window.ClientID %>.setLocalXY(x,y);
    }

    var totalRenderer = function(value)
    {
        return "<b class='totalBlock'>"+ value + "</b>";
    }

    var setLeaveTotal = function (currentRow) {
           
        var records = <%=gridLeaves.ClientID %>.getStore();
        var i = 0,
        length = records.data.length,
        total = 0,
        grandTotal = 0,
        record;

        
        var D1 = 0;
        var D2 = 0;
        var D3 = 0;
        var D4 = 0;
        var D5 = 0;
        var D6 = 0;
        var D7 = 0;
        var total = 0;

        for (; i < length; ++i) {
            record = records.data.items[i];

            D1 +=  record.data.L1;
            D2 +=  record.data.L2;
            D3 +=  record.data.L3;
            D4 +=  record.data.L4;
            D5 +=  record.data.L5;
            
            total = record.data.L1 + record.data.L2+ record.data.L3+ record.data.L4
                    + record.data.L5;

            record.data.Total = total;
            grandTotal += total;
            
        }            

        var totalRecord = <%=gridLeavesTotal.ClientID%>.getStore().data.items[0];
        totalRecord.data.D1 = D1;
        totalRecord.data.D2 = D2;
        totalRecord.data.D3 = D3;
        totalRecord.data.D4 = D4;
        totalRecord.data.D5 = D5;
        totalRecord.data.Total = grandTotal;            
        totalRecord.commit();
       
        if(currentRow != null)
            currentRow.commit();

    }
    var setTotal = function (currentRow) {
           
        var records = <%=gridProjects.ClientID %>.getStore();
        var i = 0,
        length = records.data.length,
        total = 0,
        grandTotal = 0,
        record;
        var projectTotal = 0;

        
        var D1 = 0;
        var D2 = 0;
        var D3 = 0;
        var D4 = 0;
        var D5 = 0;
        var D6 = 0;
        var D7 = 0;
        var total = 0;

        for (; i < length; ++i) {
            record = records.data.items[i];

            D1 +=  record.data.D1;
            D2 +=  record.data.D2;
            D3 +=  record.data.D3;
            D4 +=  record.data.D4;
            D5 +=  record.data.D5;
            
            total = record.data.D1 + record.data.D2+ record.data.D3+ record.data.D4
                    + record.data.D5;

            // this need to done form server side as well
            //total = parseFloat(parseFloat(total).toFixed(2));
            projectTotal += total;

            record.data.Total = total;
            grandTotal += total;
            
        }            

        var totalRecord = <%=gridProjectsTotal.ClientID%>.getStore().data.items[0];
        totalRecord.data.D1 = D1;
        totalRecord.data.D2 = D2;
        totalRecord.data.D3 = D3;
        totalRecord.data.D4 = D4;
        totalRecord.data.D5 = D5;
        totalRecord.data.Total = grandTotal;            
        totalRecord.commit();
       
        if(currentRow != null)
            currentRow.commit();


        // set Grand Total
        var totalLeave = <%=gridLeavesTotal.ClientID%>.getStore().data.items[0];
        var grandTotal = <%=gridLeavesTotal.ClientID%>.getStore().data.items[1];

        grandTotal.data.D1 = totalRecord.data.D1 + totalLeave.data.D1;
        grandTotal.data.D2 = totalRecord.data.D2 + totalLeave.data.D2;
        grandTotal.data.D3 = totalRecord.data.D3 + totalLeave.data.D3;
        grandTotal.data.D4 = totalRecord.data.D4 + totalLeave.data.D4;
        grandTotal.data.D5 = totalRecord.data.D5 + totalLeave.data.D5;
        grandTotal.data.Total = totalRecord.data.Total + totalLeave.data.Total;      
        grandTotal.commit();

        return projectTotal;
    }

    var afterEditLeave = function (e1, e2, e3, e4) {
        e2.record.commit(); 
        setLeaveTotal(e2.record);
        setTotal(null);           
    }

    var prepareCommand = function (grid, command, record, row) {
            // you can prepare group command
        if (record.data.D1IsFixed == true || record.data.D2IsFixed == true || record.data.D3IsFixed == true 
            || record.data.D4IsFixed == true || record.data.D5IsFixed)
        {
            command.hidden = true;
        }

    };


    var getFormattedAmountTotalOrTimecard = function(value,e1,record)
    {
        if(record.data.Name=="Timecard")
        {
            if(value==null)
                return "";

            if(value==0)
                return "";

            var hours = Math.floor( parseFloat(value) / 60);          
            var minutes = Math.floor( parseFloat(value) % 60);

            if(minutes.toString().length<=1)
                minutes = "0" + minutes;

            return  hours + ':' + minutes;
        }
        return getFormattedAmount2(value);
    }
    var getFormattedAmount2 = function(value)
    {
        var returnValue = "";
        if(parseFloat(value)==0)
            returnValue = "";
        else
            returnValue = value;
        return returnValue;
    }
    var getFormattedAmount1= function(value,record,dayIndex)
    {            
            var comment = record.get('D' + dayIndex + 'Comment');
            var isFixed = record.get('D' + dayIndex + 'IsFixed');
            
            
                       
            var returnValue = "";
            if(parseFloat(value)==0)
                returnValue = "";
            else
                returnValue = value;

            if(isFixed)
            {
                if( comment != "")
                    return '<span style="display:block" class="jiraClass" title="Imported from Jira">' + returnValue +'</span>';
                else
                    return '<span  style="display:block"  class="jiraClass" title="Imported from Jira, Comment : ' + comment + '">' + returnValue +'</span>';
            }

            if(comment != "")
            {
                return '<span  style="display:block"  class="commentClass" title="Comment : ' + comment + '">' + returnValue +'</span>';
            }
        
            return returnValue;
    }

    Date.prototype.getShortDate = function(){
        return this.getFullYear() + 
        "/" +  (this.getMonth() + 1) +
        "/" +  this.getDate();
    }



    
function selectCurrentCell() {
//    <%=gridProjects.ClientID %>.getView().focus();
//    var row = <%=gridProjects.ClientID %>.getSelectionModel().getCurrentPosition().row;
//    var column = <%=gridProjects.ClientID %>.getSelectionModel().getCurrentPosition().column;
//    <%=gridProjects.ClientID %>.getSelectionModel().setCurrentPosition({ row: row, column: column });
}
function deSelectCurrentCell() {
    //gridItems.getView().focus();
    //var row = gridItems.getSelectionModel().getCurrentPosition().row;
    //var column = gridItems.getSelectionModel().getCurrentPosition().column;
    <%=gridProjects.ClientID %>.getSelectionModel().deselectAll();
}

    var loadHoliday = function (month, year) {
        Ext.net.Mask.show();
        Ext.net.DirectMethod.request({
            url: "../../LeaveRequestService.asmx/GetTimesheet",
            cleanRequest: true,
            json: true,
            params: {
                month: month,
                year: year
            },
            success: function (result) {
                holidayList = result;
                //Ext.Msg.alert("Json Message", result);
               // CompanyX.ctl01_dateField.highlightDates(result);
                Ext.net.Mask.hide();

                //find the range case
               
            },
            failure: function (result) { Ext.net.Mask.hide(); }
        });
    };

    function getHolidayObject(date) {
            var str = (date.getMonth() + 1) + "/" + date.getDate() + "/" + date.getFullYear();
            for (var i = 0; i < holidayList.length; i++) {
                if (holidayList[i].DateEngText == str) {
                    return holidayList[i];
                }
            }
            return null;
        }
    
//    Ext.ns("Ext.ux.DatePicker");
//        Ext.ux.DatePicker.HighlightDates = Ext.extend(Object, {
//            constructor : function (config) {
//                this.dates = config.dates;
//                this.cls = config.cls;
//            },
//  
//            init : function (datePicker) {
//                var me = this;
//                if (datePicker instanceof Ext.DatePicker) {
//                    datePicker.getHighlighDates = Ext.Function.bind(function () {return this;}, me);
//                    datePicker.highlightDates = Ext.Function.bind(me.highlightDates, datePicker);
//                    datePicker.update = Ext.Function.createSequence(datePicker.update, function () {
//                        var highlighDates = this.getHighlighDates();
//                        this.highlightDates(highlighDates.dates, highlighDates.cls);
//                    });
//                } else if (datePicker instanceof Ext.form.DateField) {
//                    datePicker.onTriggerClick = Ext.Function.createSequence(datePicker.onTriggerClick, function () {
//                        if (!me.gotHighlighDates) {
//                            me.init(this.picker);
//                            this.picker.highlightDates(me.dates, me.cls);
//                            me.gotHighlighDates = true;
//                        }
//                    });
//                }
//            },
//  
//            highlightDates : function (dates, cls) {
//                var dateValues = [],
//                    el = this.getEl();
//  
//                cells = el.select("a.x-datepicker-date");
//                Ext.each(dates, function (d) {
//                    // save datetime format
//                    var date = new Date(d.DateEngText);
//                    dateValues.push(date.getTime());
//                });
//  
//                //cells.removeCls(cls);
//                //clear background of all cells
//                if(dateValues.length >0)
//                {
//                    cells.filter(function (el) {
//                        el.dom.style.backgroundColor = "";
//                        el.dom.title = "";
//                    });
//                }

//                cells.filter(function (el) {
//                    var hasHoliday = dateValues.indexOf(el.dom.dateValue) > -1;
//                    // if holiday then highlight cell
//                    if (hasHoliday) {

//                        var date = new Date(el.dom.dateValue);
//                        var holidayObject = getHolidayObject(date);
//                        if (holidayObject != null) {
//                            //set background color & tooltip
//                            if(holidayObject.Type != 0 )
//                                el.dom.style.backgroundColor = 'yellow';//legends[holidayObject.Abbreviation];
//                            el.dom.title = holidayObject.Name;
//                        }
//                    }
//                    return hasHoliday;
//                }).addCls(cls);
//            }
//    });

var currentMonth = null,currentYear = null;
    ///// Override DatePicker & is triggered when month or date selection changes
Ext.DatePicker.prototype.update = function (date, forceRefresh) {
    
    var me = this,
            active = me.activeDate;
    if (me.rendered) {
        me.activeDate = date;
        if (!forceRefresh && active && me.el && active.getMonth() == date.getMonth() && active.getFullYear() == date.getFullYear()) {
            me.selectedUpdate(date, active);
        } else {
            me.fullUpdate(date, active);
        }
    }

   
    //if (this.id == "DatePicker1") 
    {

        if (currentMonth == null) {
            currentMonth = date.getMonth();
            currentYear = date.getFullYear();
            loadHoliday(date.getMonth(), date.getFullYear());

        }
        else if (currentMonth != date.getMonth() || currentYear != date.getFullYear()) 
        {
            currentMonth = date.getMonth();
            currentYear = date.getFullYear();
            loadHoliday(date.getMonth(), date.getFullYear());
            //if (CompanyX.getCalendar() != undefined)
               // CompanyX.getCalendar().setStartDate(date);
            //reloadGrids();
        }
    }

    return me;

};

     function processBeforeExport(btnId) {
     var hdate =  <%=hiddenWeekStartDate.ClientID%>.getValue();
     
      <%=hiddenWeekStartDateExcel.ClientID%>.setValue(hdate);
      
}

 // Fix for Group panel Expand/Collapse case : Uncaught TypeError: Cannot read property 'isCollapsedPlaceholder' of undefined

     Ext.view.Table.override({
        indexInStore: function(node) {
            node = (node && node.isCollapsedPlaceholder) ? this.getNode(node) : this.getNode(node, false);

            if (!node && node !== 0) {
                return -1;
            }

            var recordIndex = node.getAttribute('data-recordIndex');

            if (recordIndex) {
                return parseInt(recordIndex, 10);
            }

            return this.dataSource.indexOf(this.getRecord(node));
        }
    });
</script>
<style type="text/css">
    thead td, th
    {
        border: 0px solid;
    }
    .total
    {
        width: 90%;
        text-align: Center;
        display: block;
        padding-right: 5px;
    }
    .jiraClass
    {
        font-weight: bolder;
    }
    .commentClass
    {
        color: blue;
        display: block;
    }
    
    .tableLightColor1 > tr > td
    {
        padding: 0px !important;
    }
    
    .x-grid-row .x-grid-cell
    {
        border-right-color: #EDEDED;
        border-right-style: solid;
        border-right-width: 1px !important;
    }
    .weeklyColor
    {
        background: #FABF8F !important;
    }
    .totalBlock
    {
        display: block;
        width: 100% !important;
        text-align: right;
    }
    .x-grid-header-ct
    {
        border: 1px solid #A3BAD9;
    }
    .x-panel-default-outer-border-rbl
    {
        border-top-color: #A3BAD9 !important;
        border-bottom-color: #A3BAD9 !important;
        border-right-color: #A3BAD9 !important;
        border-left-color: #A3BAD9 !important;
    }
    .x-panel-default-outer-border-trl
    {
        border-top-color: #A3BAD9 !important;
        border-right-color: #A3BAD9 !important;
        border-left-color: #A3BAD9 !important;
    }
    .x-column-header-inner
    {
        padding-top: 5px;
        padding-bottom: 5px;
    }
    .weekInfo
    {
        font-weight: bold !important;
        font-size: 15px;
        color: #2A7BC2;
    }
    .statusStyle
    {
        font-size: 15px;
        color: black;
        font-style: italic;
    }
    .x-column-header-text
    {
        font-size: 12px;
    }
</style>
<div class="attribute1">
    <ext:Store runat="server" ID="storeSearch" AutoLoad="false">
        <Proxy>
            <ext:AjaxProxy Url="../../../Handler/EmpSearch.ashx">
                <ActionMethods Read="GET" />
                <Reader>
                    <ext:JsonReader Root="plants" TotalProperty="total" />
                </Reader>
            </ext:AjaxProxy>
        </Proxy>
        <Model>
            <ext:Model ID="Model6" IDProperty="Name" Name="ItemLineModel" runat="server">
                <Fields>
                    <ext:ModelField Name="Name" Type="String" />
                    <ext:ModelField Name="EmployeeId" Type="String" />
                </Fields>
            </ext:Model>
        </Model>
    </ext:Store>
    <ext:Store runat="server" ID="storeProjects">
        <Model>
            <ext:Model ID="Model2" runat="server" IDProperty="ProjectId">
                <Fields>
                    <ext:ModelField Name="ProjectId" Type="String" />
                    <ext:ModelField Name="Name" Type="String" />
                </Fields>
            </ext:Model>
        </Model>
    </ext:Store>
    <ext:Store runat="server" ID="storeLeaves">
        <Model>
            <ext:Model ID="Model4" runat="server" IDProperty="LeaveTypeId">
                <Fields>
                    <ext:ModelField Name="LeaveTypeId" Type="String" />
                    <ext:ModelField Name="Title" Type="String" />
                </Fields>
            </ext:Model>
        </Model>
    </ext:Store>
    <%-- <ext:DateField runat="server" ID="dateField" />--%>
    <ext:Button ID="btnLoad" runat="server" Text="Load Week" Hidden="true">
        <DirectEvents>
            <Click OnEvent="btnLoad_Click">
                <EventMask ShowMask="true">
                </EventMask>
            </Click>
        </DirectEvents>
    </ext:Button>
</div>
<div style="clear: both">
</div>
<table>
    <tr>
        <td valign="top">
            <ext:Menu runat="server" ID="menu">
                <Items>
                    <ext:MenuItem runat="server" Text="Place Comment">
                        <Listeners>
                            <Click Handler="displayComment();" />
                        </Listeners>
                    </ext:MenuItem>
                </Items>
            </ext:Menu>
            <ext:Window Layout="BorderLayout" Width="250" Border="false" Height="180" Hidden="true"
                runat="server" ID="window">
                <Items>
                    <ext:TextArea runat="server" ID="txtComment" Region="Center">
                    </ext:TextArea>
                </Items>
                <Buttons>
                    <ext:Button runat="server" Text="Ok" Width="70">
                        <Listeners>
                            <Click Handler="setComment();" />
                        </Listeners>
                    </ext:Button>
                </Buttons>
            </ext:Window>
            <ext:Hidden ID="hiddenDate" runat="server" />
            <ext:Hidden ID="hdnLastCount" runat="server" />
            <ext:Hidden ID="hiddenWeekStartDate" runat="server" />
            <ext:Hidden ID="hiddenWeekStartDateExcel" runat="server" />
            <h3 runat="server" id="Pagetitle">
                My Timesheet</h3>
            <ext:Store ID="storeEmployee" runat="server">
                <Model>
                    <ext:Model ID="Model9" IDProperty="Value" runat="server">
                        <Fields>
                            <ext:ModelField Name="Value" Type="String" />
                            <ext:ModelField Name="Text" />
                        </Fields>
                    </ext:Model>
                </Model>
            </ext:Store>
            <ext:ComboBox LabelSeparator="" ID="cmbSearch" runat="server" FieldLabel="" Hidden="true"
                EmptyText="Select Employee" DisplayField="Text" ValueField="Value" Width="200"
                ForceSelection="true" StoreID="storeEmployee" QueryMode="Local">
                <DirectEvents>
                    <Select OnEvent="btnLoad_Click">
                        <EventMask ShowMask="true" />
                    </Select>
                </DirectEvents>
            </ext:ComboBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Employee is required."
                ControlToValidate="cmbSearch" ValidationGroup="AssignTimeSheet" Display="None" />
            <div style="padding-top: 20px; padding-bottom: 5px;">
                Select any date of the desired month</div>
            <ext:DatePicker Width="220" StyleSpec='StyleSpec="border:1px solid #A3BAD9;' runat="server"
                ID="dateField">
                <Listeners>
                    <Select Handler="#{hiddenDate}.setValue(#{dateField}.getValue().getShortDate());#{btnLoad}.fireEvent('click');" />
                </Listeners>
                <%-- <Plugins>
                    <ext:GenericPlugin ID="HighlightDates1" runat="server" InstanceName="Ext.ux.DatePicker.HighlightDates" />
                </Plugins>--%>
            </ext:DatePicker>
        </td>
        <td valign="top" style="padding-left: 10px">
            <table>
                <tr>
                    <td>
                        <ext:Button ID="btnPrevious" runat="server" Text="&nbsp;&nbsp;<&nbsp;&nbsp;" StyleSpec="float:left">
                            <DirectEvents>
                                <Click OnEvent="btnPrevious_Click">
                                    <EventMask ShowMask="true">
                                    </EventMask>
                                </Click>
                            </DirectEvents>
                        </ext:Button>
                    </td>
                    <td>
                        <ext:Button ID="btnNext" runat="server" Text="&nbsp;&nbsp;>&nbsp;&nbsp;" StyleSpec="float:left;margin-left:10px;">
                            <DirectEvents>
                                <Click OnEvent="btnNext_Click">
                                    <EventMask ShowMask="true">
                                    </EventMask>
                                </Click>
                            </DirectEvents>
                        </ext:Button>
                    </td>
                    <td style="padding-top: 5px; padding-left: 10px;">
                        <ext:DisplayField ID="dispDate" FieldCls="weekInfo" runat="server">
                        </ext:DisplayField>
                    </td>
                    <td style="padding-top: 5px; padding-left: 10px;">
                        <ext:DisplayField ID="dispStatus" FieldCls="statusStyle" runat="server">
                        </ext:DisplayField>
                    </td>
                </tr>
            </table>
            <ext:GridPanel AutoHeight="true" Width="970" StyleSpec="margin-top:5px;" ID="gridProjects"
                runat="server">
                <Store>
                    <ext:Store runat="server" ID="storeRequest">
                        <Model>
                            <ext:Model ID="Model1" runat="server" Name="WeekModel">
                                <Fields>
                                    <ext:ModelField Name="WeekStartDate" Type="String" />
                                    <ext:ModelField Name="Id" Type="String" />
                                    <ext:ModelField Name="Activity"  />
                                    <ext:ModelField Name="Category" Type="String" />
                                    <ext:ModelField Name="ProjectPercent" Type="Float" />
                                    <ext:ModelField Name="D1" Type="Float" />
                                    <ext:ModelField Name="D1Comment" Type="String" />
                                    <ext:ModelField Name="D1IsFixed" Type="Boolean" />
                                    <ext:ModelField Name="D2" Type="Float" />
                                    <ext:ModelField Name="D2Comment" Type="String" />
                                    <ext:ModelField Name="D2IsFixed" Type="Boolean" />
                                    <ext:ModelField Name="D3" Type="Float" />
                                    <ext:ModelField Name="D3Comment" Type="String" />
                                    <ext:ModelField Name="D3IsFixed" Type="Boolean" />
                                    <ext:ModelField Name="D4" Type="Float" />
                                    <ext:ModelField Name="D4Comment" Type="String" />
                                    <ext:ModelField Name="D4IsFixed" Type="Boolean" />
                                    <ext:ModelField Name="D5" Type="Float" />
                                    <ext:ModelField Name="D5Comment" Type="String" />
                                    <ext:ModelField Name="D5IsFixed" Type="Boolean" />
                                    <ext:ModelField Name="Total" Type="Float" />
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                <Listeners>
                    <BeforeEdit Fn="beforeEdit" />
                </Listeners>
                <Plugins>
                    <ext:CellEditing ID="CellEditing1" ClicksToEdit="1" runat="server">
                        <Listeners>
                            <Edit Fn="afterEdit" />
                        </Listeners>
                    </ext:CellEditing>
                </Plugins>
                <Listeners>
                </Listeners>
                <View>
                    <ext:GridView ID="GridView1" runat="server">
                    </ext:GridView>
                </View>
                <ColumnModel runat="server" ID="columnmodle1">
                    <Columns>
                        <ext:Column ID="Column1" Sortable="false" MenuDisabled="true" StyleSpec="font-weight:bold"
                            Width="160" runat="server" Header="Project" DataIndex="Id">
                            <Editor>
                                <ext:ComboBox ID="cmbProjects" ForceSelection="true" QueryMode="Local" runat="server"
                                    DisplayField="Name" ValueField="ProjectId" StoreID="storeProjects">
                                </ext:ComboBox>
                            </Editor>
                            <Renderer Fn="projectRenderer" />
                        </ext:Column>
                        <ext:Column ID="Column4" Sortable="false" MenuDisabled="true" StyleSpec="font-weight:bold"
                            Width="160" runat="server" Header="Activity" DataIndex="Activity">
                             <Editor>
                                <ext:NumberField ID="txtActivity" MinValue="1" runat="server">
                                </ext:NumberField>
                            </Editor>
                        </ext:Column>
                        <ext:Column ID="Column5" Sortable="false" MenuDisabled="true" StyleSpec="font-weight:bold"
                            Width="160" runat="server" Header="Category" DataIndex="Category">
                            <Editor>
                                <ext:TextField ID="txtCategory" MinValue="1" runat="server">
                                </ext:TextField>
                            </Editor>
                        </ext:Column>
                        <ext:Column ID="Column7" Sortable="false" MenuDisabled="true" StyleSpec="font-weight:bold" Align="Center"
                            Width="70" runat="server" Header="Percent" DataIndex="ProjectPercent">
                            <Renderer  Fn="getFormattedAmount" />
                        </ext:Column>
                        <ext:Column ID="D1" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Week 1" Width="70" DataIndex="D1">
                            <Renderer Handler="return getFormattedAmount1(value,record,1)" />
                            <Editor>
                                <ext:TextField ContextMenuID="menu" ID="TextField6" MaskRe="[0-9]|\.|%" StyleSpec="text-align:right"
                                    SelectOnFocus="true" runat="server" AllowBlank="true">
                                    <Listeners>
                                        <Focus Handler="if(parseFloat(this.getValue())==0) this.setValue('');" />
                                    </Listeners>
                                </ext:TextField>
                            </Editor>
                        </ext:Column>
                        <ext:Column ID="D2" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Week 2" Width="70" DataIndex="D2">
                            <Renderer Handler="return getFormattedAmount1(value,record,2)" />
                            <Editor>
                                <ext:TextField ContextMenuID="menu" ID="TextField1" MaskRe="[0-9]|\.|%" StyleSpec="text-align:right"
                                    SelectOnFocus="true" runat="server" AllowBlank="true">
                                    <Listeners>
                                        <Focus Handler="if(parseFloat(this.getValue())==0) this.setValue('');" />
                                    </Listeners>
                                </ext:TextField>
                            </Editor>
                        </ext:Column>
                        <ext:Column ID="D3" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Week 3" Width="70" DataIndex="D3">
                            <Renderer Handler="return getFormattedAmount1(value,record,3)" />
                            <Editor>
                                <ext:TextField ContextMenuID="menu" ID="TextField2" MaskRe="[0-9]|\.|%" StyleSpec="text-align:right"
                                    SelectOnFocus="true" runat="server" AllowBlank="true">
                                    <Listeners>
                                        <Focus Handler="if(parseFloat(this.getValue())==0) this.setValue('');" />
                                    </Listeners>
                                </ext:TextField>
                            </Editor>
                        </ext:Column>
                        <ext:Column ID="D4" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Week 4" Width="70" DataIndex="D4">
                            <Renderer Handler="return getFormattedAmount1(value,record,4)" />
                            <Editor>
                                <ext:TextField ContextMenuID="menu" ID="TextField3" MaskRe="[0-9]|\.|%" StyleSpec="text-align:right"
                                    SelectOnFocus="true" runat="server" AllowBlank="true">
                                    <Listeners>
                                        <Focus Handler="if(parseFloat(this.getValue())==0) this.setValue('');" />
                                    </Listeners>
                                </ext:TextField>
                            </Editor>
                        </ext:Column>
                        <ext:Column ID="D5" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Week 5" Width="70" DataIndex="D5">
                            <Renderer Handler="return getFormattedAmount1(value,record,5)" />
                            <Editor>
                                <ext:TextField ContextMenuID="menu" ID="TextField4" MaskRe="[0-9]|\.|%" StyleSpec="text-align:right"
                                    SelectOnFocus="true" runat="server" AllowBlank="true">
                                    <Listeners>
                                        <Focus Handler="if(parseFloat(this.getValue())==0) this.setValue('');" />
                                    </Listeners>
                                </ext:TextField>
                            </Editor>
                        </ext:Column>
                        <ext:Column ID="Column2" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Total" Width="70" DataIndex="Total">
                            <Renderer Fn="getFormattedAmount2" />
                        </ext:Column>
                    </Columns>
                </ColumnModel>
                <SelectionModel>
                    <ext:CellSelectionModel EnableKeyNav="true" runat="server" ID="CellSelectionModel_ItemGrid">
                    </ext:CellSelectionModel>
                </SelectionModel>
            </ext:GridPanel>
            <ext:GridPanel AutoHeight="true" DisableSelection="true" HideHeaders="true" Border="false"
                Width="970" ID="gridProjectsTotal" runat="server">
                <Store>
                    <ext:Store runat="server" ID="store2">
                        <Model>
                            <ext:Model ID="Model7" runat="server" Name="WeekModelTotal">
                                <Fields>
                                    <ext:ModelField Name="Name" Type="String" />
                                    <ext:ModelField Name="D1" Type="Float" />
                                    <ext:ModelField Name="D2" Type="Float" />
                                    <ext:ModelField Name="D3" Type="Float" />
                                    <ext:ModelField Name="D4" Type="Float" />
                                    <ext:ModelField Name="D5" Type="Float" />
                                    <ext:ModelField Name="Total" Type="Float" />
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                <ColumnModel runat="server" ID="ColumnModel2">
                    <Columns>
                        <ext:Column ID="Column6" MenuDisabled="true" StyleSpec="font-weight:bold" Width="550"
                            runat="server" DataIndex="Name">
                            <Renderer Fn="totalRenderer" />
                        </ext:Column>
                     
                        <ext:Column ID="Column9" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="70" DataIndex="D1">
                            <Renderer Fn="getFormattedAmountTotalOrTimecard" />
                        </ext:Column>
                        <ext:Column ID="Column10" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="70" DataIndex="D2">
                            <Renderer Fn="getFormattedAmountTotalOrTimecard" />
                        </ext:Column>
                        <ext:Column ID="Column12" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="70" DataIndex="D3">
                            <Renderer Fn="getFormattedAmountTotalOrTimecard" />
                        </ext:Column>
                        <ext:Column ID="Column13" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="70" DataIndex="D4">
                            <Renderer Fn="getFormattedAmountTotalOrTimecard" />
                        </ext:Column>
                        <ext:Column ID="Column14" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="70" DataIndex="D5">
                            <Renderer Fn="getFormattedAmountTotalOrTimecard" />
                        </ext:Column>
                        <ext:Column ID="Column17" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Total" Width="70" DataIndex="Total">
                            <Renderer Fn="getFormattedAmountTotalOrTimecard" />
                        </ext:Column>
                    </Columns>
                </ColumnModel>
                <SelectionModel>
                    <ext:CellSelectionModel EnableKeyNav="true" runat="server" ID="CellSelectionModel2">
                    </ext:CellSelectionModel>
                </SelectionModel>
                <View>
                    <ext:GridView ID="GridView4" StripeRows="false" runat="server">
                    </ext:GridView>
                </View>
            </ext:GridPanel>
            <ext:GridPanel AutoHeight="true" StyleSpec="margin-top:45px" DisableSelection="true"
                Width="970" ID="gridLeaves" runat="server">
                <Store>
                    <ext:Store runat="server" ID="store1">
                        <Model>
                            <ext:Model ID="Model3" runat="server" Name="WeekModelLeave">
                                <Fields>
                                    <ext:ModelField Name="Id" Type="String" />
                                    <ext:ModelField Name="L1" Type="Float" />
                                    <ext:ModelField Name="L2" Type="Float" />
                                    <ext:ModelField Name="L3" Type="Float" />
                                    <ext:ModelField Name="L4" Type="Float" />
                                    <ext:ModelField Name="L5" Type="Float" />
                                    <ext:ModelField Name="Total" Type="Float" />
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                <Listeners>
                    <BeforeEdit Fn="beforeEditLeave" />
                </Listeners>
                <Plugins>
                    <ext:CellEditing ID="CellEditing2" ClicksToEdit="1" runat="server">
                        <Listeners>
                            <Edit Fn="afterEditLeave" />
                        </Listeners>
                    </ext:CellEditing>
                </Plugins>
                <View>
                    <ext:GridView ID="GridView2" runat="server">
                    </ext:GridView>
                </View>
                <ColumnModel runat="server" ID="ColumnModel1">
                    <Columns>
                        <ext:Column ID="Column3" Sortable="false" MenuDisabled="true" StyleSpec="font-weight:bold"
                            Width="550" runat="server" Header="Leaves" DataIndex="Id">
                            <Renderer Fn="leaveRenderer" />
                        </ext:Column>
                         
                        <ext:Column ID="L1" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Week 1" Width="70" DataIndex="L1">
                            <Renderer Fn="getFormattedAmount2" />
                            <Editor>
                                <ext:TextField ContextMenuID="menu" ID="TextField5" MaskRe="[0-9]|\.|%" StyleSpec="text-align:right"
                                    SelectOnFocus="true" runat="server" AllowBlank="true">
                                    <Listeners>
                                        <Focus Handler="if(parseFloat(this.getValue())==0) this.setValue('');" />
                                    </Listeners>
                                </ext:TextField>
                            </Editor>
                        </ext:Column>
                        <ext:Column ID="L2" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Week 2" Width="70" DataIndex="L2">
                            <Renderer Fn="getFormattedAmount2" />
                            <Editor>
                                <ext:TextField ContextMenuID="menu" ID="TextField7" MaskRe="[0-9]|\.|%" StyleSpec="text-align:right"
                                    SelectOnFocus="true" runat="server" AllowBlank="true">
                                    <Listeners>
                                        <Focus Handler="if(parseFloat(this.getValue())==0) this.setValue('');" />
                                    </Listeners>
                                </ext:TextField>
                            </Editor>
                        </ext:Column>
                        <ext:Column ID="L3" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Week 3" Width="70" DataIndex="L3">
                            <Renderer Fn="getFormattedAmount2" />
                            <Editor>
                                <ext:TextField ContextMenuID="menu" ID="TextField8" MaskRe="[0-9]|\.|%" StyleSpec="text-align:right"
                                    SelectOnFocus="true" runat="server" AllowBlank="true">
                                    <Listeners>
                                        <Focus Handler="if(parseFloat(this.getValue())==0) this.setValue('');" />
                                    </Listeners>
                                </ext:TextField>
                            </Editor>
                        </ext:Column>
                        <ext:Column ID="L4" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Week 4" Width="70" DataIndex="L4">
                            <Renderer Fn="getFormattedAmount2" />
                            <Editor>
                                <ext:TextField ContextMenuID="menu" ID="TextField9" MaskRe="[0-9]|\.|%" StyleSpec="text-align:right"
                                    SelectOnFocus="true" runat="server" AllowBlank="true">
                                    <Listeners>
                                        <Focus Handler="if(parseFloat(this.getValue())==0) this.setValue('');" />
                                    </Listeners>
                                </ext:TextField>
                            </Editor>
                        </ext:Column>
                        <ext:Column ID="L5" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Week 5" Width="70" DataIndex="L5">
                            <Renderer Fn="getFormattedAmount2" />
                            <Editor>
                                <ext:TextField ContextMenuID="menu" ID="TextField10" MaskRe="[0-9]|\.|%" StyleSpec="text-align:right"
                                    SelectOnFocus="true" runat="server" AllowBlank="true">
                                    <Listeners>
                                        <Focus Handler="if(parseFloat(this.getValue())==0) this.setValue('');" />
                                    </Listeners>
                                </ext:TextField>
                            </Editor>
                        </ext:Column>
                        <ext:Column ID="Column11" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Total" Width="70" DataIndex="Total">
                            <Renderer Fn="getFormattedAmount2" />
                        </ext:Column>
                    </Columns>
                </ColumnModel>
                <SelectionModel>
                    <ext:CellSelectionModel EnableKeyNav="true" runat="server" ID="CellSelectionModel12">
                    </ext:CellSelectionModel>
                </SelectionModel>
            </ext:GridPanel>
            <ext:GridPanel AutoHeight="true" DisableSelection="true" HideHeaders="true" Border="false"
                Width="970" ID="gridLeavesTotal" runat="server">
                <Store>
                    <ext:Store runat="server" ID="store3">
                        <Model>
                            <ext:Model ID="Model8" runat="server" Name="WeekModelTotal">
                                <Fields>
                                    <ext:ModelField Name="Name" Type="String" />
                                    <ext:ModelField Name="D1" Type="Float" />
                                    <ext:ModelField Name="D2" Type="Float" />
                                    <ext:ModelField Name="D3" Type="Float" />
                                    <ext:ModelField Name="D4" Type="Float" />
                                    <ext:ModelField Name="D5" Type="Float" />
                                    <ext:ModelField Name="Total" Type="Float" />
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                <ColumnModel runat="server" ID="ColumnModel3">
                    <Columns>
                        <ext:Column ID="Column20" MenuDisabled="true" StyleSpec="font-weight:bold" Width="550"
                            runat="server" DataIndex="Name">
                            <Renderer Fn="totalRenderer" />
                        </ext:Column>
                        
                        <ext:Column ID="Column23" Cls="weeklyColor" runat="server" Sortable="false" Align="Center"
                            MenuDisabled="true" Text="Name" Width="70" DataIndex="D1">
                            <Renderer Fn="getFormattedAmount2" />
                        </ext:Column>
                        <ext:Column ID="Column24" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="70" DataIndex="D2">
                            <Renderer Fn="getFormattedAmount2" />
                        </ext:Column>
                        <ext:Column ID="Column25" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="70" DataIndex="D3">
                            <Renderer Fn="getFormattedAmount2" />
                        </ext:Column>
                        <ext:Column ID="Column26" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="70" DataIndex="D4">
                            <Renderer Fn="getFormattedAmount2" />
                        </ext:Column>
                        <ext:Column ID="Column27" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Name" Width="70" DataIndex="D5">
                            <Renderer Fn="getFormattedAmount2" />
                        </ext:Column>
                        <ext:Column ID="Column30" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                            Text="Total" Width="70" DataIndex="Total">
                            <Renderer Fn="getFormattedAmount2" />
                        </ext:Column>
                    </Columns>
                </ColumnModel>
                <SelectionModel>
                    <ext:CellSelectionModel EnableKeyNav="true" runat="server" ID="CellSelectionModel3">
                    </ext:CellSelectionModel>
                </SelectionModel>
                <View>
                    <ext:GridView ID="GridView3" StripeRows="false" runat="server">
                    </ext:GridView>
                </View>
            </ext:GridPanel>
        </td>
    </tr>
    <tr>
    </tr>
    <tr>
        <td>
        </td>
        <td style="padding-left: 10px; padding-top: 40px; padding-right: 85px;">
            <div class="attribute" style="padding: 10px;">
                <table>
                    <tr>
                        <td>
                            <ext:ComboBox ID="cmbApplyTo" LabelAlign="Top" QueryMode="Local" DisplayField="Text"
                                ValueField="Value" LabelSeparator="" ForceSelection="true" LabelStyle="font-weight:bold"
                                runat="server" FieldLabel="Approving Authority *">
                                <Store>
                                    <ext:Store ID="store4" runat="server">
                                        <Model>
                                            <ext:Model ID="Model5" runat="server" IDProperty="Value">
                                                <Fields>
                                                    <ext:ModelField Name="Value" Type="String" />
                                                    <ext:ModelField Name="Text" />
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                            </ext:ComboBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="Approval is required."
                                ControlToValidate="cmbApplyTo" ValidationGroup="Timesheet" Display="None" />
                        </td>
                        <td valign="bottom">
                            <ext:Button ID="btnAssign" runat="server" Width="150" Height="30" Text="Assign TimeSheet"
                                Hidden="true">
                                <DirectEvents>
                                    <Click OnEvent="btnAssign_Click">
                                        <%--<Confirmation ConfirmRequest="true" Message="Are you sure, you want to save the timesheet?" />--%>
                                        <EventMask ShowMask="true">
                                        </EventMask>
                                        <ExtraParams>
                                            <ext:Parameter Name="Values" Value="Ext.encode(#{gridProjects}.getRowsValues({selectedOnly : false}))"
                                                Mode="Raw" />
                                            <ext:Parameter Name="ValuesLeaves" Value="Ext.encode(#{gridLeaves}.getRowsValues({selectedOnly : false}))"
                                                Mode="Raw" />
                                            <ext:Parameter Name="TotalValues" Value="Ext.encode(#{gridLeavesTotal}.getRowsValues({selectedOnly : false}))"
                                                Mode="Raw" />
                                        </ExtraParams>
                                    </Click>
                                </DirectEvents>
                                <Listeners>
                                    <Click Handler="valGroup='AssignTimeSheet';return CheckValidation();" />
                                </Listeners>
                            </ext:Button>
                        </td>
                        <td valign="bottom">
                            <ext:Button ID="btnSave" runat="server" Width="150" Height="30" Text="Save and Finish Later">
                                <DirectEvents>
                                    <Click OnEvent="btnSave_Click">
                                        <%--<Confirmation ConfirmRequest="true" Message="Are you sure, you want to save the timesheet?" />--%>
                                        <EventMask ShowMask="true">
                                        </EventMask>
                                        <ExtraParams>
                                            <ext:Parameter Name="Values" Value="Ext.encode(#{gridProjects}.getRowsValues({selectedOnly : false}))"
                                                Mode="Raw" />
                                            <ext:Parameter Name="ValuesLeaves" Value="Ext.encode(#{gridLeaves}.getRowsValues({selectedOnly : false}))"
                                                Mode="Raw" />
                                            <ext:Parameter Name="TotalValues" Value="Ext.encode(#{gridLeavesTotal}.getRowsValues({selectedOnly : false}))"
                                                Mode="Raw" />
                                        </ExtraParams>
                                    </Click>
                                </DirectEvents>
                                <Listeners>
                                    <Click Handler="valGroup='Timesheet';return CheckValidation();" />
                                </Listeners>
                            </ext:Button>
                        </td>
                        <td style="width: 20px">
                        </td>
                        <td valign="bottom">
                            <ext:Button ID="btnSaveAndSend" runat="server" Width="150" Height="30" Text="Save and Send">
                                <DirectEvents>
                                    <Click OnEvent="btnSave_Click">
                                        <Confirmation ConfirmRequest="true" Message="Are you sure, you want to submit the timesheet, after submission timesheet can not be changed?" />
                                        <EventMask ShowMask="true">
                                        </EventMask>
                                        <ExtraParams>
                                            <ext:Parameter Name="Values" Value="Ext.encode(#{gridProjects}.getRowsValues({selectedOnly : false}))"
                                                Mode="Raw" />
                                            <ext:Parameter Name="ValuesLeaves" Value="Ext.encode(#{gridLeaves}.getRowsValues({selectedOnly : false}))"
                                                Mode="Raw" />
                                            <ext:Parameter Name="TotalValues" Value="Ext.encode(#{gridLeavesTotal}.getRowsValues({selectedOnly : false}))"
                                                Mode="Raw" />
                                        </ExtraParams>
                                    </Click>
                                </DirectEvents>
                                <Listeners>
                                    <Click Handler="valGroup='Timesheet';return CheckValidation();" />
                                </Listeners>
                            </ext:Button>
                        </td>
                        <td style="width: 20px">
                        </td>
                        <td style="width: 600px; text-align: right">
                            <ext:Button ID="Button2" Hidden="true" runat="server" Text="Export" Cls="updatebtn"
                                MarginSpec="18 0 0 0">
                                <Menu>
                                    <ext:Menu ID="Menu2" runat="server" ShowSeparator="false" Cls="dropdownbtn">
                                        <Items>
                                            <ext:LinkButton ID="btnExportPDF" runat="server" AutoPostBack="true" OnClick="Click_Export"
                                                Hidden="true" Text="PDF">
                                                <Listeners>
                                                    <Click Fn="processBeforeExport" />
                                                </Listeners>
                                            </ext:LinkButton>
                                            <ext:LinkButton ID="btnExportExcel" runat="server" AutoPostBack="true" OnClick="Click_Export"
                                                Text="Excel">
                                                <Listeners>
                                                    <Click Fn="processBeforeExport" />
                                                </Listeners>
                                            </ext:LinkButton>
                                        </Items>
                                    </ext:Menu>
                                </Menu>
                            </ext:Button>
                        </td>
                    </tr>
                </table>
            </div>
        </td>
    </tr>
    <tr>
        <td>
        </td>
    </tr>
</table>
