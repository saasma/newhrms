﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using BLL.Manager;
using BLL.BO;
using BLL;
using DAL;
using BLL.Entity;
using Utils.Web;
using Utils.Helper;
using Utils.Calendar;

namespace Web.Employee.UserControls
{
    public partial class MonthWiseTimesheetCtl : BaseUserControl
    {
        /// <summary>
        /// Hold employee timesheet currently displaying 
        /// </summary>
        public int ReadonlyTimesheetId
        {
            set;
            get;
        }

        public int ReadonlyEmployeeId
        {
            set;
            get;
        }

        public Ext.Net.Button SaveAndSend
        {
            get
            {
                return this.btnSaveAndSend;
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {

            X.Js.AddScript("selectCurrentCell();");
        }
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!X.IsAjaxRequest)
            {
                DateTime date = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
                hiddenDate.Text = date.ToShortDateString();
                txtFromDate.Text = date.ToShortDateString();

                dateField.SelectedDate = date;


                // display for viewing week
                if (!string.IsNullOrEmpty(Request.QueryString["week"]))
                {
                    DateTime date1;
                    if (DateTime.TryParse(Request.QueryString["week"].ToString(), out date1))
                    {

                        date = new DateTime(date1.Year, date1.Month, date1.Day);
                        hiddenDate.Text = date.ToShortDateString();
                        txtFromDate.Text = date.ToShortDateString();
                        dateField.SelectedDate = date;
                    }
                }

                // display selected employee timesheet from approval page
                string id = Request.QueryString["id"];
                int timesheetId = 0;
                if (int.TryParse(id, out timesheetId))
                {
                    DAL.Timesheet sheet = NewTimeSheetManager.GetTimeSheetById(timesheetId);
                    if (sheet != null && NewTimeSheetManager.IsTimesheetViewableForManager(timesheetId))
                    {
                        this.ReadonlyTimesheetId = timesheetId;
                        this.ReadonlyEmployeeId = sheet.EmployeeId;

                        date = new DateTime(sheet.StartDate.Value.Year, sheet.StartDate.Value.Month, sheet.StartDate.Value.Day);
                        hiddenDate.Text = date.ToShortDateString();
                        txtFromDate.Text = date.ToShortDateString();
                        dateField.SelectedDate = date;
                        if (sheet.ApprovalEmployeeId != null)
                        {
                            if (!X.IsAjaxRequest)
                                ExtControlHelper.ComboBoxSetSelected(sheet.ApprovalEmployeeId.ToString(), cmbApplyTo);
                            else
                                cmbApplyTo.SetValue(sheet.ApprovalEmployeeId.ToString());
                        }
                    }
                }

                DateTime FirtDayOfSelectedDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                SetProjectHeader(FirtDayOfSelectedDate);
                Initialise();
                btnLoad_Click(null, null);
            }


        }

        public int EmployeeId
        {
            set { }
            get
            {
                return SessionManager.CurrentLoggedInEmployeeId;
            }
        }

        public void SetProjectHeader(DateTime weekStartDate)
        {
            List<WeeklyHoliday> list = NewTimeSheetManager.GetWeeklyHolidayList();

            int totalDays = (weekStartDate.AddMonths(1).AddDays(-1).Day);

            for (int i = 29; i <= 32; i++)
            {
                Column columnProject = gridProjects.ColumnModel.FindControl("D" + i) as Column;
                columnProject.Hide();

                //Column columnProjectTotal = gridProjectsTotal.ColumnModel.FindControl("DT" + i) as Column;
                //columnProjectTotal.Hide();

                Column columnLeave = gridLeaves.ColumnModel.FindControl("L" + i) as Column;
                columnLeave.Hide();

                Column columnLeaveTotal = gridLeavesTotal.ColumnModel.FindControl("LT" + i) as Column;
                columnLeaveTotal.Hide();


            }


            for (int i = 1; i <= totalDays; i++)
            {

                Column column = gridProjects.ColumnModel.FindControl("D" + i) as Column;
                column.RemoveCls("weeklyColor");
                if (i > 28)
                {
                    column.Show();
                    //Column columnProjectTotal = gridProjectsTotal.ColumnModel.FindControl("DT" + i) as Column;
                    //columnProjectTotal.Show();
                }
                if (column != null)
                {
                    column.Text = i.ToString();

                    if (weekStartDate.DayOfWeek == DayOfWeek.Saturday ||
                        weekStartDate.DayOfWeek == DayOfWeek.Sunday)
                        column.AddCls("weeklyColor");
                    else
                        column.RemoveCls("weeklyColor");
                }


                column = gridLeaves.ColumnModel.FindControl("L" + i) as Column;

                column.RemoveCls("weeklyColor");

                if (i > 28)
                {
                    column.Show();
                    Column columnLeaveTotal = gridLeavesTotal.ColumnModel.FindControl("LT" + i) as Column;
                    columnLeaveTotal.Show();
                }
                if (column != null)
                {
                    column.Text = i.ToString();
                    if (weekStartDate.DayOfWeek == DayOfWeek.Saturday ||
                                          weekStartDate.DayOfWeek == DayOfWeek.Sunday)
                        column.AddCls("weeklyColor");
                    else
                        column.RemoveCls("weeklyColor");
                }

                weekStartDate = weekStartDate.AddDays(1);
            }
        }

        public bool IsWeeklyHoliday(List<WeeklyHoliday> list, DateTime date)
        {
            DayOfWeek day = date.DayOfWeek;
            foreach (WeeklyHoliday item in list)
            {
                if (item.DayOfWeek == (int)day)
                    return true;
            }
            return false;
        }

        protected void btnPrevious_Click(object sender, DirectEventArgs e)
        {
            DateTime date = Convert.ToDateTime(hiddenDate.Text.Trim()); ;
            date = date.AddMonths(-1);


            hiddenDate.Text = date.ToShortDateString();
            txtFromDate.Text = date.ToShortDateString();

            X.Js.AddScript("#{{dateField}}.setValue(new Date({0},{1},{2}));", date.Year, date.Month - 1, date.Day);

            btnLoad_Click(null, null);

        }
        protected void btnNext_Click(object sender, DirectEventArgs e)
        {
            DateTime date = Convert.ToDateTime(hiddenDate.Text.Trim()); ;
            date = date.AddMonths(1);
            hiddenDate.Text = date.ToShortDateString();
            txtFromDate.Text = date.ToShortDateString();

            X.Js.AddScript("#{{dateField}}.setValue(new Date({0},{1},{2}));", date.Year, date.Month - 1, date.Day);


            btnLoad_Click(null, null);
        }

        private void SetTimesheetDetails(out List<TimesheetProject> projectList, out List<TimesheetLeave> leaveList,
            List<TimesheetGridMonthWiseNewBO> inputProjectList, List<TimesheetGridMonthWiseNewBO> inputLeavesList, DateTime weekStart,int EmpID)
        {
            projectList = new List<TimesheetProject>();
            leaveList = new List<TimesheetLeave>();
            //double value;
            TimesheetGridMonthWiseNewBO row;

            DateTime week = weekStart;

            int projectId = 0;


            for (int i = 0; i < inputProjectList.Count; i++)
            {
                row = inputProjectList[i];

                if (row.Id == null || row.Id == 0)
                    continue;


                projectId = row.Id.Value;
                int? subProjectId = row.SubProjectId;
                int? taskId = row.TaskId;


                if (row.D1 != null && row.D1 != 0)
                    projectList.Add(new TimesheetProject { ProjectId = projectId, SubProjectId = subProjectId, TaskId = taskId, DayNumber = 1, Hours = row.D1.Value, DateEng = week, EmployeeId = EmpID });
                if (row.D2 != null && row.D2 != 0)
                    projectList.Add(new TimesheetProject { ProjectId = projectId, SubProjectId = subProjectId, TaskId = taskId, DayNumber = 2, Hours = row.D2.Value, DateEng = week.AddDays(1), EmployeeId = EmpID });
                if (row.D3 != null && row.D3 != 0)
                    projectList.Add(new TimesheetProject { ProjectId = projectId, SubProjectId = subProjectId, TaskId = taskId, DayNumber = 3, Hours = row.D3.Value, DateEng = week.AddDays(2), EmployeeId = EmpID });
                if (row.D4 != null && row.D4 != 0)
                    projectList.Add(new TimesheetProject { ProjectId = projectId, SubProjectId = subProjectId, TaskId = taskId, DayNumber = 4, Hours = row.D4.Value, DateEng = week.AddDays(3), EmployeeId = EmpID });
                if (row.D5 != null && row.D5 != 0)
                    projectList.Add(new TimesheetProject { ProjectId = projectId, SubProjectId = subProjectId, TaskId = taskId, DayNumber = 5, Hours = row.D5.Value, DateEng = week.AddDays(4), EmployeeId = EmpID });
                if (row.D6 != null && row.D6 != 0)
                    projectList.Add(new TimesheetProject { ProjectId = projectId, SubProjectId = subProjectId, TaskId = taskId, DayNumber = 6, Hours = row.D6.Value, DateEng = week.AddDays(5), EmployeeId = EmpID });
                if (row.D7 != null && row.D7 != 0)
                    projectList.Add(new TimesheetProject { ProjectId = projectId, SubProjectId = subProjectId, TaskId = taskId, DayNumber = 7, Hours = row.D7.Value, DateEng = week.AddDays(6), EmployeeId = EmpID });
                if (row.D8 != null && row.D8 != 0)
                    projectList.Add(new TimesheetProject { ProjectId = projectId, SubProjectId = subProjectId, TaskId = taskId, DayNumber = 8, Hours = row.D8.Value, DateEng = week.AddDays(7), EmployeeId = EmpID });
                if (row.D9 != null && row.D9 != 0)
                    projectList.Add(new TimesheetProject { ProjectId = projectId, SubProjectId = subProjectId, TaskId = taskId, DayNumber = 9, Hours = row.D9.Value, DateEng = week.AddDays(8), EmployeeId = EmpID });
                if (row.D10 != null && row.D10 != 0)
                    projectList.Add(new TimesheetProject { ProjectId = projectId, SubProjectId = subProjectId, TaskId = taskId, DayNumber = 10, Hours = row.D10.Value, DateEng = week.AddDays(9), EmployeeId = EmpID });
                if (row.D11 != null && row.D11 != 0)
                    projectList.Add(new TimesheetProject { ProjectId = projectId, SubProjectId = subProjectId, TaskId = taskId, DayNumber = 11, Hours = row.D11.Value, DateEng = week.AddDays(10), EmployeeId = EmpID });
                if (row.D12 != null && row.D12 != 0)
                    projectList.Add(new TimesheetProject { ProjectId = projectId, SubProjectId = subProjectId, TaskId = taskId, DayNumber = 12, Hours = row.D12.Value, DateEng = week.AddDays(11), EmployeeId = EmpID });
                if (row.D13 != null && row.D13 != 0)
                    projectList.Add(new TimesheetProject { ProjectId = projectId, SubProjectId = subProjectId, TaskId = taskId, DayNumber = 13, Hours = row.D13.Value, DateEng = week.AddDays(12), EmployeeId = EmpID });
                if (row.D14 != null && row.D14 != 0)
                    projectList.Add(new TimesheetProject { ProjectId = projectId, SubProjectId = subProjectId, TaskId = taskId, DayNumber = 14, Hours = row.D14.Value, DateEng = week.AddDays(13), EmployeeId = EmpID });
                if (row.D15 != null && row.D15 != 0)
                    projectList.Add(new TimesheetProject { ProjectId = projectId, SubProjectId = subProjectId, TaskId = taskId, DayNumber = 15, Hours = row.D15.Value, DateEng = week.AddDays(14), EmployeeId = EmpID });
                if (row.D16 != null && row.D16 != 0)
                    projectList.Add(new TimesheetProject { ProjectId = projectId, SubProjectId = subProjectId, TaskId = taskId, DayNumber = 16, Hours = row.D16.Value, DateEng = week.AddDays(15), EmployeeId = EmpID });
                if (row.D17 != null && row.D17 != 0)
                    projectList.Add(new TimesheetProject { ProjectId = projectId, SubProjectId = subProjectId, TaskId = taskId, DayNumber = 17, Hours = row.D17.Value, DateEng = week.AddDays(16), EmployeeId = EmpID });
                if (row.D18 != null && row.D18 != 0)
                    projectList.Add(new TimesheetProject { ProjectId = projectId, SubProjectId = subProjectId, TaskId = taskId, DayNumber = 18, Hours = row.D18.Value, DateEng = week.AddDays(17), EmployeeId = EmpID });
                if (row.D19 != null && row.D19 != 0)
                    projectList.Add(new TimesheetProject { ProjectId = projectId, SubProjectId = subProjectId, TaskId = taskId, DayNumber = 19, Hours = row.D19.Value, DateEng = week.AddDays(18), EmployeeId = EmpID });
                if (row.D20 != null && row.D20 != 0)
                    projectList.Add(new TimesheetProject { ProjectId = projectId, SubProjectId = subProjectId, TaskId = taskId, DayNumber = 20, Hours = row.D20.Value, DateEng = week.AddDays(19), EmployeeId = EmpID });
                if (row.D21 != null && row.D21 != 0)
                    projectList.Add(new TimesheetProject { ProjectId = projectId, SubProjectId = subProjectId, TaskId = taskId, DayNumber = 21, Hours = row.D21.Value, DateEng = week.AddDays(20), EmployeeId = EmpID });
                if (row.D22 != null && row.D22 != 0)
                    projectList.Add(new TimesheetProject { ProjectId = projectId, SubProjectId = subProjectId, TaskId = taskId, DayNumber = 22, Hours = row.D22.Value, DateEng = week.AddDays(21), EmployeeId = EmpID });
                if (row.D23 != null && row.D23 != 0)
                    projectList.Add(new TimesheetProject { ProjectId = projectId, SubProjectId = subProjectId, TaskId = taskId, DayNumber = 23, Hours = row.D23.Value, DateEng = week.AddDays(22), EmployeeId = EmpID });
                if (row.D24 != null && row.D24 != 0)
                    projectList.Add(new TimesheetProject { ProjectId = projectId, SubProjectId = subProjectId, TaskId = taskId, DayNumber = 24, Hours = row.D24.Value, DateEng = week.AddDays(23), EmployeeId = EmpID });
                if (row.D25 != null && row.D25 != 0)
                    projectList.Add(new TimesheetProject { ProjectId = projectId, SubProjectId = subProjectId, TaskId = taskId, DayNumber = 25, Hours = row.D25.Value, DateEng = week.AddDays(24), EmployeeId = EmpID });
                if (row.D26 != null && row.D26 != 0)
                    projectList.Add(new TimesheetProject { ProjectId = projectId, SubProjectId = subProjectId, TaskId = taskId, DayNumber = 26, Hours = row.D26.Value, DateEng = week.AddDays(25), EmployeeId = EmpID });
                if (row.D27 != null && row.D27 != 0)
                    projectList.Add(new TimesheetProject { ProjectId = projectId, SubProjectId = subProjectId, TaskId = taskId, DayNumber = 27, Hours = row.D27.Value, DateEng = week.AddDays(26), EmployeeId = EmpID });
                if (row.D28 != null && row.D28 != 0)
                    projectList.Add(new TimesheetProject { ProjectId = projectId, SubProjectId = subProjectId, TaskId = taskId, DayNumber = 28, Hours = row.D28.Value, DateEng = week.AddDays(27), EmployeeId = EmpID });
                if (row.D29 != null && row.D29 != 0)
                    projectList.Add(new TimesheetProject { ProjectId = projectId, SubProjectId = subProjectId, TaskId = taskId, DayNumber = 29, Hours = row.D29.Value, DateEng = week.AddDays(28), EmployeeId = EmpID });
                if (row.D30 != null && row.D30 != 0)
                    projectList.Add(new TimesheetProject { ProjectId = projectId, SubProjectId = subProjectId, TaskId = taskId, DayNumber = 30, Hours = row.D30.Value, DateEng = week.AddDays(29), EmployeeId = EmpID });
                if (row.D31 != null && row.D31 != 0)
                    projectList.Add(new TimesheetProject { ProjectId = projectId, SubProjectId = subProjectId, TaskId = taskId, DayNumber = 31, Hours = row.D31.Value, DateEng = week.AddDays(30), EmployeeId = EmpID });
                if (row.D32 != null && row.D32 != 0)
                    projectList.Add(new TimesheetProject { ProjectId = projectId, SubProjectId = subProjectId, TaskId = taskId, DayNumber = 32, Hours = row.D32.Value, DateEng = week.AddDays(31), EmployeeId = EmpID });

            }


            int leaveTypeId = 0;
            week = weekStart;
            for (int i = 0; i < inputLeavesList.Count; i++)
            {
                row = inputLeavesList[i];
                leaveTypeId = row.Id.Value;

                if (row.L1 != null && row.L1 != 0)
                    leaveList.Add(new TimesheetLeave { LeaveId = leaveTypeId, DayNumber = 1, Hours = row.L1.Value, DateEng = week, EmployeeId = this.EmployeeId });
                if (row.L2 != null && row.L2 != 0)
                    leaveList.Add(new TimesheetLeave { LeaveId = leaveTypeId, DayNumber = 2, Hours = row.L2.Value, DateEng = week.AddDays(1), EmployeeId = this.EmployeeId });
                if (row.L3 != null && row.L3 != 0)
                    leaveList.Add(new TimesheetLeave { LeaveId = leaveTypeId, DayNumber = 3, Hours = row.L3.Value, DateEng = week.AddDays(2), EmployeeId = this.EmployeeId });
                if (row.L4 != null && row.L4 != 0)
                    leaveList.Add(new TimesheetLeave { LeaveId = leaveTypeId, DayNumber = 4, Hours = row.L4.Value, DateEng = week.AddDays(3), EmployeeId = this.EmployeeId });
                if (row.L5 != null && row.L5 != 0)
                    leaveList.Add(new TimesheetLeave { LeaveId = leaveTypeId, DayNumber = 5, Hours = row.L5.Value, DateEng = week.AddDays(4), EmployeeId = this.EmployeeId });
                if (row.L6 != null && row.L6 != 0)
                    leaveList.Add(new TimesheetLeave { LeaveId = leaveTypeId, DayNumber = 6, Hours = row.L6.Value, DateEng = week.AddDays(5), EmployeeId = this.EmployeeId });
                if (row.L7 != null && row.L7 != 0)
                    leaveList.Add(new TimesheetLeave { LeaveId = leaveTypeId, DayNumber = 7, Hours = row.L7.Value, DateEng = week.AddDays(6), EmployeeId = this.EmployeeId });
                if (row.L8 != null && row.L8 != 0)
                    leaveList.Add(new TimesheetLeave { LeaveId = leaveTypeId, DayNumber = 8, Hours = row.L8.Value, DateEng = week.AddDays(7), EmployeeId = this.EmployeeId });
                if (row.L9 != null && row.L9 != 0)
                    leaveList.Add(new TimesheetLeave { LeaveId = leaveTypeId, DayNumber = 9, Hours = row.L9.Value, DateEng = week.AddDays(8), EmployeeId = this.EmployeeId });
                if (row.L10 != null && row.L10 != 0)
                    leaveList.Add(new TimesheetLeave { LeaveId = leaveTypeId, DayNumber = 10, Hours = row.L10.Value, DateEng = week.AddDays(9), EmployeeId = this.EmployeeId });
                if (row.L11 != null && row.L11 != 0)
                    leaveList.Add(new TimesheetLeave { LeaveId = leaveTypeId, DayNumber = 11, Hours = row.L11.Value, DateEng = week.AddDays(10), EmployeeId = this.EmployeeId });
                if (row.L12 != null && row.L12 != 0)
                    leaveList.Add(new TimesheetLeave { LeaveId = leaveTypeId, DayNumber = 12, Hours = row.L12.Value, DateEng = week.AddDays(11), EmployeeId = this.EmployeeId });
                if (row.L13 != null && row.L13 != 0)
                    leaveList.Add(new TimesheetLeave { LeaveId = leaveTypeId, DayNumber = 13, Hours = row.L13.Value, DateEng = week.AddDays(12), EmployeeId = this.EmployeeId });
                if (row.L14 != null && row.L14 != 0)
                    leaveList.Add(new TimesheetLeave { LeaveId = leaveTypeId, DayNumber = 14, Hours = row.L14.Value, DateEng = week.AddDays(13), EmployeeId = this.EmployeeId });
                if (row.L15 != null && row.L15 != 0)
                    leaveList.Add(new TimesheetLeave { LeaveId = leaveTypeId, DayNumber = 15, Hours = row.L15.Value, DateEng = week.AddDays(14), EmployeeId = this.EmployeeId });
                if (row.L16 != null && row.L16 != 0)
                    leaveList.Add(new TimesheetLeave { LeaveId = leaveTypeId, DayNumber = 16, Hours = row.L16.Value, DateEng = week.AddDays(15), EmployeeId = this.EmployeeId });
                if (row.L17 != null && row.L17 != 0)
                    leaveList.Add(new TimesheetLeave { LeaveId = leaveTypeId, DayNumber = 17, Hours = row.L17.Value, DateEng = week.AddDays(16), EmployeeId = this.EmployeeId });
                if (row.L18 != null && row.L18 != 0)
                    leaveList.Add(new TimesheetLeave { LeaveId = leaveTypeId, DayNumber = 18, Hours = row.L18.Value, DateEng = week.AddDays(17), EmployeeId = this.EmployeeId });
                if (row.L19 != null && row.L19 != 0)
                    leaveList.Add(new TimesheetLeave { LeaveId = leaveTypeId, DayNumber = 19, Hours = row.L19.Value, DateEng = week.AddDays(18), EmployeeId = this.EmployeeId });
                if (row.L20 != null && row.L20 != 0)
                    leaveList.Add(new TimesheetLeave { LeaveId = leaveTypeId, DayNumber = 20, Hours = row.L20.Value, DateEng = week.AddDays(19), EmployeeId = this.EmployeeId });
                if (row.L21 != null && row.L21 != 0)
                    leaveList.Add(new TimesheetLeave { LeaveId = leaveTypeId, DayNumber = 21, Hours = row.L21.Value, DateEng = week.AddDays(20), EmployeeId = this.EmployeeId });
                if (row.L22 != null && row.L22 != 0)
                    leaveList.Add(new TimesheetLeave { LeaveId = leaveTypeId, DayNumber = 22, Hours = row.L22.Value, DateEng = week.AddDays(21), EmployeeId = this.EmployeeId });
                if (row.L23 != null && row.L23 != 0)
                    leaveList.Add(new TimesheetLeave { LeaveId = leaveTypeId, DayNumber = 23, Hours = row.L23.Value, DateEng = week.AddDays(22), EmployeeId = this.EmployeeId });
                if (row.L24 != null && row.L24 != 0)
                    leaveList.Add(new TimesheetLeave { LeaveId = leaveTypeId, DayNumber = 24, Hours = row.L24.Value, DateEng = week.AddDays(23), EmployeeId = this.EmployeeId });
                if (row.L25 != null && row.L25 != 0)
                    leaveList.Add(new TimesheetLeave { LeaveId = leaveTypeId, DayNumber = 25, Hours = row.L25.Value, DateEng = week.AddDays(24), EmployeeId = this.EmployeeId });
                if (row.L26 != null && row.L26 != 0)
                    leaveList.Add(new TimesheetLeave { LeaveId = leaveTypeId, DayNumber = 26, Hours = row.L26.Value, DateEng = week.AddDays(25), EmployeeId = this.EmployeeId });
                if (row.L27 != null && row.L27 != 0)
                    leaveList.Add(new TimesheetLeave { LeaveId = leaveTypeId, DayNumber = 27, Hours = row.L27.Value, DateEng = week.AddDays(26), EmployeeId = this.EmployeeId });
                if (row.L28 != null && row.L28 != 0)
                    leaveList.Add(new TimesheetLeave { LeaveId = leaveTypeId, DayNumber = 28, Hours = row.L28.Value, DateEng = week.AddDays(27), EmployeeId = this.EmployeeId });
                if (row.L29 != null && row.L29 != 0)
                    leaveList.Add(new TimesheetLeave { LeaveId = leaveTypeId, DayNumber = 29, Hours = row.L29.Value, DateEng = week.AddDays(28), EmployeeId = this.EmployeeId });
                if (row.L30 != null && row.L30 != 0)
                    leaveList.Add(new TimesheetLeave { LeaveId = leaveTypeId, DayNumber = 30, Hours = row.L30.Value, DateEng = week.AddDays(29), EmployeeId = this.EmployeeId });
                if (row.L31 != null && row.L31 != 0)
                    leaveList.Add(new TimesheetLeave { LeaveId = leaveTypeId, DayNumber = 31, Hours = row.L31.Value, DateEng = week.AddDays(30), EmployeeId = this.EmployeeId });
                if (row.L32 != null && row.L32 != 0)
                    leaveList.Add(new TimesheetLeave { LeaveId = leaveTypeId, DayNumber = 32, Hours = row.L32.Value, DateEng = week.AddDays(31), EmployeeId = this.EmployeeId });

            }
            //int leaveId = 0;
            //int payrollPeriodId = int.Parse(ddlPayrollPeriods.SelectedValue);
            //PayrollPeriod period = CommonManager.GetPayrollPeriod(payrollPeriodId);

            //for (int i = 0; i < gvw2Leaves.Rows.Count; i++)
            //{
            //    row = gvw2Leaves.Rows[i];
            //    leaveId = (int)gvw2Leaves.DataKeys[row.RowIndex][0];
            //    DateTime currentDate = period.StartDateEng.Value;
            //    for (int day = 1; day <= 31; day++)
            //    {
            //        if (day != 1)
            //            currentDate = currentDate.AddDays(1);

            //        txt = row.FindControl("txtDay" + day) as TextBox;
            //        if (txt != null && txt.Text != "" && txt.Text != "." && double.TryParse(txt.Text.Trim(), out value))
            //        {
            //            if (value != 0)
            //            {
            //                leaveList.Add(
            //                    new TimesheetLeave { LeaveId = leaveId, DayNumber = (byte)day, Hours = value, DateEng = currentDate }
            //                    );
            //            }
            //        }


            //        // Previous Month
            //        if (day >= 27)
            //        {
            //            txt = row.FindControl("txtP" + day) as TextBox;
            //            if (txt != null && txt.Text != "" && txt.Text != "." && double.TryParse(txt.Text.Trim(), out value))
            //            {
            //                if (value != 0)
            //                {
            //                    leaveList.Add(
            //                        new TimesheetLeave { LeaveId = leaveId, DayNumber = (byte)day, Hours = value, DateEng = currentDate, IsPreviousMonth = true }
            //                        );
            //                }
            //            }
            //        }
            //    }
            //}
        }

        protected void btnAssign_Click(object sender, DirectEventArgs e)
        {

            //if (string.IsNullOrEmpty(cmbApplyTo.SelectedItem.Value))
            //{
            //    NewMessage.ShowNormalMessage("Approval is required.");
            //    return;
            //}

            if (string.IsNullOrEmpty(cmbSearch.SelectedItem.Value))
            {
                NewMessage.ShowNormalMessage("Select Employee.");
                return;
            }


            string json = e.ExtraParams["Values"];
            string jsonLeaves = e.ExtraParams["ValuesLeaves"];
            string TotalValues = e.ExtraParams["TotalValues"];

            //TimeSheetStatus statusMode = (sender == btnSaveAndSend ? TimeSheetStatus.AwaitingApproval : TimeSheetStatus.Draft);
            TimeSheetStatus statusMode = statusMode = TimeSheetStatus.Approved;

            if (string.IsNullOrEmpty(json))
            {
                return;
            }

            List<TimesheetGridMonthWiseNewBO> lines = JSON.Deserialize<List<TimesheetGridMonthWiseNewBO>>(json);
            List<TimesheetGridMonthWiseNewBO> leaveLines = JSON.Deserialize<List<TimesheetGridMonthWiseNewBO>>(jsonLeaves);
            // List<TimesheetGridMonthWiseNewBO> totalValues = JSON.Deserialize<List<TimesheetGridMonthWiseNewBO>>(TotalValues);

            List<TimesheetGridMonthWiseNewBO> totalValues = new List<TimesheetGridMonthWiseNewBO>();

            TimesheetGridMonthWiseNewBO _Total = leaveLines[leaveLines.ToList().Count() - 2];
            TimesheetGridMonthWiseNewBO _GrandTotal = leaveLines[leaveLines.ToList().Count() - 1];

            totalValues.Add(_Total);
            totalValues.Add(_GrandTotal);

            // prepare project list
            int employeeId = EmployeeId;
            double totalHours = 0;

            totalHours = totalValues[1].Total == null ? 0 : totalValues[1].Total.Value;

            DateTime weekStartDate = Convert.ToDateTime(hiddenWeekStartDate.Text);

            //DateTime currentMonthStartDate = NewTimeSheetManager.GetWeekStartDate(DateTime.Now);
            //DateTime currentMonthEndDate = currentMonthStartDate.AddDays(6);

            //if (weekStartDate.AddDays(6) > currentMonthEndDate)
            //{
            //    NewMessage.ShowWarningMessage("Future days timesheet cannot be saved.");
            //    return;
            //}

            // prepare project list

            if (!string.IsNullOrEmpty(cmbSearch.SelectedItem.Value) && isSuperviserEntry())
            {
                employeeId = int.Parse(cmbSearch.SelectedItem.Value);
            }

            List<TimesheetProject> projectList;
            List<TimesheetLeave> leaveList;
            DateTime StartingDateofMonth = new DateTime(weekStartDate.Year, weekStartDate.Month, 1);
            DateTime CurrentStartDate = Convert.ToDateTime(CommonManager.GetCurrentDateAndTime());

            DateTime StartingDateofCurrentMonth = new DateTime(CurrentStartDate.Year, CurrentStartDate.Month, 1);

            if (StartingDateofMonth > StartingDateofCurrentMonth)
            {
                NewMessage.ShowWarningMessage("Future days timesheet cannot be saved.");
                return;
            }

            SetTimesheetDetails(out projectList, out leaveList, lines, leaveLines, StartingDateofMonth, employeeId);
            //string otherProjectName = Request.Form[hiddenOtherProjectName.ClientID.Replace("_", "$")];
            //string changeTotalHours = Request.Form[txtGrandTotal.ClientID.Replace("_", "$")];
            //if (double.TryParse(changeTotalHours, out totalHours))
            //{ }
            //else
            //{
            //    if (txtGrandTotal.Text.Trim() == "")
            //        totalHours = 0;
            //    else
            //        totalHours = float.Parse(txtGrandTotal.Text.Trim());

            //}

            if (projectList.Any(x => x.Hours != null && x.Hours != 0) == false)
            {
                NewMessage.ShowWarningMessage("Project hour should be placed.");
                return;
            }

            //else
            //{
            //    if (projectList.Any(x => x.Hours != 8))
            //    {
            //        NewMessage.ShowWarningMessage("Project hour should be 8 hrs for each day.");
            //        return;
            //    }
            //}

            if (statusMode.ToString().ToLower().Equals("awaitingapproval"))
            {
                TimesheetGridMonthWiseNewBO GrandTotal = totalValues[1];
                if ((GrandTotal.D1 > 0 && GrandTotal.D1 != 8) || (GrandTotal.D2 > 0 && GrandTotal.D2 != 8) || (GrandTotal.D3 > 0 && GrandTotal.D3 != 8) || (GrandTotal.D4 > 0 && GrandTotal.D4 != 8)
                    || (GrandTotal.D5 > 0 && GrandTotal.D5 != 8) || (GrandTotal.D6 > 0 && GrandTotal.D6 != 8) || (GrandTotal.D7 > 0 && GrandTotal.D7 != 8) || (GrandTotal.D8 > 0 && GrandTotal.D8 != 8)
                    || (GrandTotal.D9 > 0 && GrandTotal.D9 != 8) || (GrandTotal.D10 > 0 && GrandTotal.D10 != 8) || (GrandTotal.D11 > 0 && GrandTotal.D11 != 8) || (GrandTotal.D12 > 0 && GrandTotal.D12 != 8)
                    || (GrandTotal.D13 > 0 && GrandTotal.D13 != 8) || (GrandTotal.D14 > 0 && GrandTotal.D14 != 8) || (GrandTotal.D15 > 0 && GrandTotal.D15 != 8) || (GrandTotal.D16 > 0 && GrandTotal.D16 != 8)
                    || (GrandTotal.D17 > 0 && GrandTotal.D17 != 8) || (GrandTotal.D18 > 0 && GrandTotal.D18 != 8) || (GrandTotal.D19 > 0 && GrandTotal.D19 != 8) || (GrandTotal.D20 > 0 && GrandTotal.D20 != 8)
                    || (GrandTotal.D21 > 0 && GrandTotal.D21 != 8) || (GrandTotal.D22 > 0 && GrandTotal.D22 != 8) || (GrandTotal.D23 > 0 && GrandTotal.D23 != 8) || (GrandTotal.D24 > 0 && GrandTotal.D24 != 8)
                    || (GrandTotal.D25 > 0 && GrandTotal.D25 != 8) || (GrandTotal.D26 > 0 && GrandTotal.D26 != 8) || (GrandTotal.D27 > 0 && GrandTotal.D27 != 8) || (GrandTotal.D28 > 0 && GrandTotal.D28 != 8)
                    || (GrandTotal.D29 > 0 && GrandTotal.D29 != 8) || (GrandTotal.D30 > 0 && GrandTotal.D30 != 8) || (GrandTotal.D31 > 0 && GrandTotal.D31 != 8) || (GrandTotal.D32 > 0 && GrandTotal.D32 != 8))
                {
                    NewMessage.ShowWarningMessage("Project hour should be 8 hrs.");
                    return;
                }
            }

            double? totalProjectHours = projectList.Sum(x => x.Hours);

            //if(CommonManager.CompanySetting.IsD2)
            //{
            //    int row = 0;
            //    foreach (TimesheetGridNewBO item in lines)
            //    {
            //        row += 1;
            //        if (item.Id != null && (item.D1 != 0 || item.D2 != 0 || item.D3 != 0 || item.D4 != 0  || item.D4 != 0 || item.D5 != 0 || item.D6 != 0 || item.D7 != 0))
            //        {
            //            if (item.SubProjectId == null)
            //            {
            //                NewMessage.ShowWarningMessage("Sub project should be selected for the row " + row + ".");
            //                return;
            //            }
            //            if (item.TaskId == null)
            //            {
            //                NewMessage.ShowWarningMessage("Task should be selected for the row " + row + ".");
            //                return;
            //            }
            //        }
            //    }
            //}

            // 40 weekly hour validation
            //if (totalHours < 40 && statusMode==TimeSheetStatus.AwaitingApproval)
            //{
            //    NewMessage.ShowWarningMessage("Minimum 40 hour is required in timesheet for submission, current total hour is only " + totalHours + ".");
            //    return;
            //}

            ResponseStatus status = NewTimeSheetManager.SaveUpdateMonthlyProjectTimesheet(weekStartDate, totalHours,
                 employeeId, projectList, leaveList, (int)statusMode, true, null, totalProjectHours, SessionManager.CurrentLoggedInEmployeeId);

            if (status.IsSuccessType == true)
            {

                SetTimesheetWeekAndStatus(weekStartDate, "", statusMode);

                if (statusMode == TimeSheetStatus.Approved)
                {
                    NewMessage.ShowNormalMessage("Timesheet has been  approved.");
                    btnSave.Hide();
                    btnSaveAndSend.Hide();
                    btnLoad_Click(null, null);

                }
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);

            }

        }

        protected void btnSave_Click(object sender, DirectEventArgs e)
        {

            if (cmbApplyTo.SelectedItem == null || string.IsNullOrEmpty(cmbApplyTo.SelectedItem.Value))
            {
                NewMessage.ShowNormalMessage("Approval is required.");
                return;
            }

            string json = e.ExtraParams["Values"];
            string jsonLeaves = e.ExtraParams["ValuesLeaves"];
            string TotalValues = e.ExtraParams["TotalValues"];

            TimeSheetStatus statusMode = (sender == btnSaveAndSend ? TimeSheetStatus.AwaitingApproval : TimeSheetStatus.Draft);

            if (string.IsNullOrEmpty(json))
            {
                return;
            }

            List<TimesheetGridMonthWiseNewBO> lines = JSON.Deserialize<List<TimesheetGridMonthWiseNewBO>>(json);
            List<TimesheetGridMonthWiseNewBO> leaveLines = JSON.Deserialize<List<TimesheetGridMonthWiseNewBO>>(jsonLeaves);
           // List<TimesheetGridMonthWiseNewBO> totalValues = JSON.Deserialize<List<TimesheetGridMonthWiseNewBO>>(TotalValues);

             List<TimesheetGridMonthWiseNewBO> totalValues = new List<TimesheetGridMonthWiseNewBO>();

             TimesheetGridMonthWiseNewBO _Total = leaveLines[leaveLines.ToList().Count() - 2];
             TimesheetGridMonthWiseNewBO _GrandTotal = leaveLines[leaveLines.ToList().Count() - 1];

             totalValues.Add(_Total);
             totalValues.Add(_GrandTotal);

            // prepare project list
            int employeeId = EmployeeId;
            double totalHours = 0;

            totalHours = totalValues[1].Total == null ? 0 : totalValues[1].Total.Value;

            DateTime weekStartDate = Convert.ToDateTime(hiddenWeekStartDate.Text);

            //DateTime currentMonthStartDate = NewTimeSheetManager.GetWeekStartDate(DateTime.Now);
            //DateTime currentMonthEndDate = currentMonthStartDate.AddDays(6);

            //if (weekStartDate.AddDays(6) > currentMonthEndDate)
            //{
            //    NewMessage.ShowWarningMessage("Future days timesheet cannot be saved.");
            //    return;
            //}

            List<TimesheetProject> projectList;
            List<TimesheetLeave> leaveList;
            DateTime StartingDateofMonth = new DateTime(weekStartDate.Year, weekStartDate.Month, 1);
            DateTime CurrentStartDate = Convert.ToDateTime(CommonManager.GetCurrentDateAndTime());

            DateTime StartingDateofCurrentMonth = new DateTime(CurrentStartDate.Year, CurrentStartDate.Month, 1);

            if (StartingDateofMonth > StartingDateofCurrentMonth)
            {
                NewMessage.ShowWarningMessage("Future days timesheet cannot be saved.");
                return;
            }

            SetTimesheetDetails(out projectList, out leaveList, lines, leaveLines, StartingDateofMonth, employeeId);
            //string otherProjectName = Request.Form[hiddenOtherProjectName.ClientID.Replace("_", "$")];
            //string changeTotalHours = Request.Form[txtGrandTotal.ClientID.Replace("_", "$")];
            //if (double.TryParse(changeTotalHours, out totalHours))
            //{ }
            //else
            //{
            //    if (txtGrandTotal.Text.Trim() == "")
            //        totalHours = 0;
            //    else
            //        totalHours = float.Parse(txtGrandTotal.Text.Trim());

            //}

            //if (projectList.Any(x => x.Hours != null && x.Hours != 0) == false)
            //{
            //    NewMessage.ShowWarningMessage("Project hour should be placed.");
            //    return;
            //}

            //else
            //{
            //    if (projectList.Any(x => x.Hours != 8))
            //    {
            //        NewMessage.ShowWarningMessage("Project hour should be 8 hrs for each day.");
            //        return;
            //    }
            //}

            if (statusMode.ToString().ToLower().Equals("awaitingapproval"))
            {
                TimesheetGridMonthWiseNewBO GrandTotal = totalValues[1];
                if ((GrandTotal.D1 > 0 && GrandTotal.D1 != 8) || (GrandTotal.D2 > 0 && GrandTotal.D2 != 8) || (GrandTotal.D3 > 0 && GrandTotal.D3 != 8) || (GrandTotal.D4 > 0 && GrandTotal.D4 != 8)
                    || (GrandTotal.D5 > 0 && GrandTotal.D5 != 8) || (GrandTotal.D6 > 0 && GrandTotal.D6 != 8) || (GrandTotal.D7 > 0 && GrandTotal.D7 != 8) || (GrandTotal.D8 > 0 && GrandTotal.D8 != 8)
                    || (GrandTotal.D9 > 0 && GrandTotal.D9 != 8) || (GrandTotal.D10 > 0 && GrandTotal.D10 != 8) || (GrandTotal.D11 > 0 && GrandTotal.D11 != 8) || (GrandTotal.D12 > 0 && GrandTotal.D12 != 8)
                    || (GrandTotal.D13 > 0 && GrandTotal.D13 != 8) || (GrandTotal.D14 > 0 && GrandTotal.D14 != 8) || (GrandTotal.D15 > 0 && GrandTotal.D15 != 8) || (GrandTotal.D16 > 0 && GrandTotal.D16 != 8)
                    || (GrandTotal.D17 > 0 && GrandTotal.D17 != 8) || (GrandTotal.D18 > 0 && GrandTotal.D18 != 8) || (GrandTotal.D19 > 0 && GrandTotal.D19 != 8) || (GrandTotal.D20 > 0 && GrandTotal.D20 != 8)
                    || (GrandTotal.D21 > 0 && GrandTotal.D21 != 8) || (GrandTotal.D22 > 0 && GrandTotal.D22 != 8) || (GrandTotal.D23 > 0 && GrandTotal.D23 != 8) || (GrandTotal.D24 > 0 && GrandTotal.D24 != 8)
                    || (GrandTotal.D25 > 0 && GrandTotal.D25 != 8) || (GrandTotal.D26 > 0 && GrandTotal.D26 != 8) || (GrandTotal.D27 > 0 && GrandTotal.D27 != 8) || (GrandTotal.D28 > 0 && GrandTotal.D28 != 8)
                    || (GrandTotal.D29 > 0 && GrandTotal.D29 != 8) || (GrandTotal.D30 > 0 && GrandTotal.D30 != 8) || (GrandTotal.D31 > 0 && GrandTotal.D31 != 8) || (GrandTotal.D32 > 0 && GrandTotal.D32 != 8))
                {
                    NewMessage.ShowWarningMessage("Project hour should be 8 hrs.");
                    return;
                }
            }

            double? totalProjectHours = projectList.Sum(x => x.Hours);

            //if(CommonManager.CompanySetting.IsD2)
            //{
            //    int row = 0;
            //    foreach (TimesheetGridNewBO item in lines)
            //    {
            //        row += 1;
            //        if (item.Id != null && (item.D1 != 0 || item.D2 != 0 || item.D3 != 0 || item.D4 != 0  || item.D4 != 0 || item.D5 != 0 || item.D6 != 0 || item.D7 != 0))
            //        {
            //            if (item.SubProjectId == null)
            //            {
            //                NewMessage.ShowWarningMessage("Sub project should be selected for the row " + row + ".");
            //                return;
            //            }
            //            if (item.TaskId == null)
            //            {
            //                NewMessage.ShowWarningMessage("Task should be selected for the row " + row + ".");
            //                return;
            //            }
            //        }
            //    }
            //}

            // 40 weekly hour validation
            //if (totalHours < 40 && statusMode==TimeSheetStatus.AwaitingApproval)
            //{
            //    NewMessage.ShowWarningMessage("Minimum 40 hour is required in timesheet for submission, current total hour is only " + totalHours + ".");
            //    return;
            //}

            ResponseStatus status = NewTimeSheetManager.SaveUpdateMonthlyProjectTimesheet(weekStartDate, totalHours,
                 employeeId, projectList, leaveList, (int)statusMode, true, null, totalProjectHours, int.Parse(cmbApplyTo.SelectedItem.Value));

            if (status.IsSuccessType == true)
            {

                SetTimesheetWeekAndStatus(weekStartDate, "", statusMode);

                if (statusMode == TimeSheetStatus.AwaitingApproval)
                {
                    NewMessage.ShowNormalMessage("Timesheet has been submitted for approval.");
                    SendMailForApproval(int.Parse(cmbApplyTo.SelectedItem.Value),employeeId);
                    btnSave.Hide();
                    btnSaveAndSend.Hide();
                }
                else
                    NewMessage.ShowNormalMessage("Timesheet has been saved.");


                //LoadAttedance();
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);

            }

        }

        protected void SendMailForApproval(int approval,int Requestor)
        {

            EEmployee _Employeee = EmployeeManager.GetEmployeeById(Requestor);
            if (string.IsNullOrEmpty(NewTimeSheetManager.GetManagerEmailForTimesheetRequestForMonthlyPSI(approval)))
            {
                //NewMessage.ShowNormalMessage("Email address not found for Employee " + _Employeee.Name);
                return;
            }

            SMTPHelper mailSender = new SMTPHelper();
            EmailContent dbMailContent = CommonManager.GetMailContent((int)EmailContentType.TimeSheetSendForApproval);

            if (dbMailContent != null)
            {
                DateTime weekStartDate = Convert.ToDateTime(hiddenWeekStartDate.Text);
                // string MonthName = weekStartDate.Year + " - " +
                //DateHelper.GetMonthName(weekStartDate.Month, true);

                string YearMonth = weekStartDate.Year + " - " +
                DateHelper.GetMonthName(weekStartDate.Month, true);


                EEmployee _RequestorEmployeee = EmployeeManager.GetEmployeeById(Requestor);
                string RequestBy = _RequestorEmployeee.Title + " " + _RequestorEmployeee.Name;

                string subject = dbMailContent.Subject.Replace("#YearMonth#", YearMonth + " - ").Replace("#Requester#", RequestBy);

                string body = dbMailContent.Body.Replace("#YearMonth#", YearMonth + " - ").Replace("#Requester#", RequestBy);

                //EEmployee _Employeee = EmployeeManager.GetEmployeeById(int.Parse(cmbApplyTo.SelectedItem.Value));
                //if (string.IsNullOrEmpty(_Employeee.EAddresses[0].CIEmail))
                //{
                //    NewMessage.ShowNormalMessage("Email address not found for Employee " + _Employeee.Name);
                //    return;
                //}

                int count = 0;
                //foreach (string ToEMailId in ToEmailIDs)
                //{
                string ApprovalEmailId = NewTimeSheetManager.GetManagerEmailForTimesheetRequestForMonthlyPSI(approval);
                bool isSendSuccess = SMTPHelper.SendAsyncMail(ApprovalEmailId, body, subject, "");
                if (isSendSuccess)
                    count++;
                //}

                if (count > 0)
                {
                    //  NewMessage.ShowNormalMessage(string.Format(Resources.Messages.SentMailSuccessMsg, count));
                    return;
                }
                else
                {
                    //NewMessage.ShowWarningMessage("error while sending email");
                    return;
                }

            }

        }

        protected void btnLoad_Click(object sender, DirectEventArgs e)
        {

            LoadMonthWiseTimeSheet(0);
            
        }

        protected bool isSuperviserEntry()
        {
            bool isSuperviserEntry = false;
            if (!string.IsNullOrEmpty(Request.QueryString["SuperviserEntry"]))
                isSuperviserEntry = bool.Parse(Request.QueryString["SuperviserEntry"]);
            return isSuperviserEntry;
        }
        public void LoadMonthWiseTimeSheet(int timesheetId)
        {


            
            DAL.Timesheet timesheet = new DAL.Timesheet();

            int EmployeeID = this.ReadonlyEmployeeId == 0 ? this.EmployeeId : this.ReadonlyEmployeeId;
            hiddenDate.Text = txtFromDate.Text.Trim();


            if (!string.IsNullOrEmpty(cmbSearch.SelectedItem.Value) && isSuperviserEntry())
                EmployeeID = int.Parse(cmbSearch.SelectedItem.Value);
            List<GetAssginedEmployeeProjectListResult> storeprojectList = new List<GetAssginedEmployeeProjectListResult>();
            if (timesheetId > 0)
            {
                timesheet = NewTimeSheetManager.GetTimeSheetById(timesheetId);
                EmployeeID = timesheet.EmployeeId;
            }

            storeprojectList = NewTimeSheetManager.GetEmployeeProjectList(EmployeeID);
            storeProjects.DataSource = storeprojectList;
            storeProjects.DataBind();

            if (timesheetId > 0)//view mode
            {

              

                storeLeaves.DataSource = LeaveAttendanceManager.GetAllLeavesWithManualAlso(SessionManager.CurrentCompanyId);
                storeLeaves.DataBind();

             
                hiddenDate.Text = timesheet.StartDate.ToString();
                EmployeeID = timesheet.EmployeeId;
                txtFromDate.Text = timesheet.StartDate.ToString();

                ShowHideControl(true);
            }

            DateTime date = Convert.ToDateTime(hiddenDate.Text.Trim());

            //DateTime weekStartDate = NewTimeSheetManager.GetWeekStartDate(date);

            DateTime weekStartDate = new DateTime(date.Year, date.Month, 1);


            //if (Convert.ToDateTime(hiddenWeekStartDate.Text) == weekStartDate)
            //    return;

            SetProjectHeader(weekStartDate);

            hiddenWeekStartDate.Text = weekStartDate.ToShortDateString();

            string rejectionNotes = "";

            TimeSheetStatus status = NewTimeSheetManager.GetTimesheetStatus(weekStartDate, EmployeeID, ref rejectionNotes, ref timesheet);

            if ((status == TimeSheetStatus.NotSubmitted || status == TimeSheetStatus.Draft) && !isSuperviserEntry())
            {
                btnSave.Show();
                btnSaveAndSend.Show();
            }
            else
            {
                btnSave.Hide();
                btnSaveAndSend.Hide();
            }

         
            if (timesheet!=null)
            {
                if (timesheet.ApprovalEmployeeId != null)
                {
                    if (!X.IsAjaxRequest)
                        ExtControlHelper.ComboBoxSetSelected(timesheet.ApprovalEmployeeId.ToString(), cmbApplyTo);
                    else
                        cmbApplyTo.SetValue(timesheet.ApprovalEmployeeId.ToString());
                }
                else
                    cmbApplyTo.Clear();
               
            }
            else
                cmbApplyTo.Clear();
        
                
            

            SetTimesheetWeekAndStatus(weekStartDate, rejectionNotes, status);
            // if timesheet is being viewed by manager then hide all buttons
            if (this.ReadonlyEmployeeId != 0)
            {
                btnSave.Hide();
                btnSaveAndSend.Hide();
                //btnAddProject.Hide();
                btnNext.Hide();
                btnPrevious.Hide();
                dateField.Disable();
            }

            List<TimesheetGridMonthWiseNewBO> projectList = NewTimeSheetManager.GetRepeatedProjectListMonthWise(weekStartDate, EmployeeID);

            //gridProjects.Store[0].DataSource = projectList;
            //gridProjects.Store[0].DataBind();

            TimesheetGridMonthWiseNewBO total = new TimesheetGridMonthWiseNewBO { Name = "Total" };
            List<TimesheetGridMonthWiseNewBO> totalList = new List<TimesheetGridMonthWiseNewBO>();
            totalList.Add(total);

            total.D1 = projectList.Sum(x => x.D1 == null ? 0 : x.D1.Value);
            total.D2 = projectList.Sum(x => x.D2 == null ? 0 : x.D2.Value);
            total.D3 = projectList.Sum(x => x.D3 == null ? 0 : x.D3.Value);
            total.D4 = projectList.Sum(x => x.D4 == null ? 0 : x.D4.Value);
            total.D5 = projectList.Sum(x => x.D5 == null ? 0 : x.D5.Value);
            total.D6 = projectList.Sum(x => x.D6 == null ? 0 : x.D6.Value);
            total.D7 = projectList.Sum(x => x.D7 == null ? 0 : x.D7.Value);
            total.D8 = projectList.Sum(x => x.D8 == null ? 0 : x.D8.Value);
            total.D9 = projectList.Sum(x => x.D9 == null ? 0 : x.D9.Value);
            total.D10 = projectList.Sum(x => x.D10 == null ? 0 : x.D10.Value);
            total.D11 = projectList.Sum(x => x.D11 == null ? 0 : x.D11.Value);
            total.D12 = projectList.Sum(x => x.D12 == null ? 0 : x.D12.Value);
            total.D13 = projectList.Sum(x => x.D13 == null ? 0 : x.D13.Value);
            total.D14 = projectList.Sum(x => x.D14 == null ? 0 : x.D14.Value);
            total.D15 = projectList.Sum(x => x.D15 == null ? 0 : x.D15.Value);
            total.D16 = projectList.Sum(x => x.D16 == null ? 0 : x.D16.Value);
            total.D17 = projectList.Sum(x => x.D17 == null ? 0 : x.D17.Value);
            total.D18 = projectList.Sum(x => x.D18 == null ? 0 : x.D18.Value);
            total.D19 = projectList.Sum(x => x.D19 == null ? 0 : x.D19.Value);
            total.D20 = projectList.Sum(x => x.D20 == null ? 0 : x.D20.Value);
            total.D21 = projectList.Sum(x => x.D21 == null ? 0 : x.D21.Value);
            total.D22 = projectList.Sum(x => x.D22 == null ? 0 : x.D22.Value);
            total.D23 = projectList.Sum(x => x.D23 == null ? 0 : x.D23.Value);
            total.D24 = projectList.Sum(x => x.D24 == null ? 0 : x.D24.Value);
            total.D25 = projectList.Sum(x => x.D25 == null ? 0 : x.D25.Value);
            total.D26 = projectList.Sum(x => x.D26 == null ? 0 : x.D26.Value);
            total.D27 = projectList.Sum(x => x.D27 == null ? 0 : x.D27.Value);
            total.D28 = projectList.Sum(x => x.D28 == null ? 0 : x.D28.Value);
            total.D29 = projectList.Sum(x => x.D29 == null ? 0 : x.D29.Value);
            total.D30 = projectList.Sum(x => x.D30 == null ? 0 : x.D30.Value);
            total.D31 = projectList.Sum(x => x.D31 == null ? 0 : x.D31.Value);
            total.D32 = projectList.Sum(x => x.D32 == null ? 0 : x.D32.Value);
            total.Total = projectList.Sum(x => x.Total == null ? 0 : x.Total.Value);


            //if (CommonManager.CompanySetting.IsD2)
            //{
            //    // add time card row
            //    TimesheetGridMonthWiseNewBO timecard = NewTimeSheetManager.GetTimecardMonthWise(EmployeeID, weekStartDate);
            //    totalList.Add(timecard);
            //}

            projectList.AddRange(totalList);

            gridProjects.Store[0].DataSource = projectList;
            gridProjects.Store[0].DataBind();


            //gridProjectsTotal.Store[0].DataSource = totalList;
            //gridProjectsTotal.Store[0].DataBind();


            List<TimesheetGridMonthWiseNewBO> leaveList = NewTimeSheetManager.GetLeaveListMonthWise(weekStartDate, EmployeeID, true);
            //foreach (TimesheetGridMonthWiseNewBO item in leaveList)
            //{
            //    item.L1 = item.D1;
            //    item.L2 = item.D2;
            //    item.L3 = item.D3;
            //    item.L4 = item.D4;
            //    item.L5 = item.D5;
            //    item.L6 = item.D6;
            //    item.L7 = item.D7;
            //    item.L8 = item.D8;
            //    item.L9 = item.D9;
            //    item.L10 = item.D10;
            //    item.L11 = item.D11;
            //    item.L12 = item.D12;
            //    item.L13 = item.D13;
            //    item.L14 = item.D14;
            //    item.L15 = item.D15;
            //    item.L16 = item.D16;
            //    item.L17 = item.D17;
            //    item.L18 = item.D18;
            //    item.L19 = item.D19;
            //    item.L20 = item.D20;
            //    item.L21 = item.D21;
            //    item.L22 = item.D22;
            //    item.L23 = item.D23;
            //    item.L24 = item.D24;
            //    item.L25 = item.D25;
            //    item.L26 = item.D26;
            //    item.L27 = item.D27;
            //    item.L28 = item.D28;
            //    item.L29 = item.D29;
            //    item.L30 = item.D30;
            //    item.L31 = item.D31;
            //    item.L32 = item.D32;
            //}


            //gridLeaves.Store[0].DataSource = leaveList;
            //gridLeaves.Store[0].DataBind();

            TimesheetGridMonthWiseNewBO leavetotal = new TimesheetGridMonthWiseNewBO { Id = -99 };//Total
            List<TimesheetGridMonthWiseNewBO> totalLeaveList = new List<TimesheetGridMonthWiseNewBO>();


            leavetotal.D1 = leaveList.Sum(x => x.D1 == null ? 0 : x.D1.Value);
            leavetotal.D2 = leaveList.Sum(x => x.D2 == null ? 0 : x.D2.Value);
            leavetotal.D3 = leaveList.Sum(x => x.D3 == null ? 0 : x.D3.Value);
            leavetotal.D4 = leaveList.Sum(x => x.D4 == null ? 0 : x.D4.Value);
            leavetotal.D5 = leaveList.Sum(x => x.D5 == null ? 0 : x.D5.Value);
            leavetotal.D6 = leaveList.Sum(x => x.D6 == null ? 0 : x.D6.Value);
            leavetotal.D7 = leaveList.Sum(x => x.D7 == null ? 0 : x.D7.Value);
            leavetotal.D8 = leaveList.Sum(x => x.D8 == null ? 0 : x.D8.Value);
            leavetotal.D9 = leaveList.Sum(x => x.D9 == null ? 0 : x.D9.Value);
            leavetotal.D10 = leaveList.Sum(x => x.D10 == null ? 0 : x.D10.Value);
            leavetotal.D11 = leaveList.Sum(x => x.D11 == null ? 0 : x.D11.Value);
            leavetotal.D12 = leaveList.Sum(x => x.D12 == null ? 0 : x.D12.Value);
            leavetotal.D13 = leaveList.Sum(x => x.D13 == null ? 0 : x.D13.Value);
            leavetotal.D14 = leaveList.Sum(x => x.D14 == null ? 0 : x.D14.Value);
            leavetotal.D15 = leaveList.Sum(x => x.D15 == null ? 0 : x.D15.Value);
            leavetotal.D16 = leaveList.Sum(x => x.D16 == null ? 0 : x.D16.Value);
            leavetotal.D17 = leaveList.Sum(x => x.D17 == null ? 0 : x.D17.Value);
            leavetotal.D18 = leaveList.Sum(x => x.D18 == null ? 0 : x.D18.Value);
            leavetotal.D19 = leaveList.Sum(x => x.D19 == null ? 0 : x.D19.Value);
            leavetotal.D20 = leaveList.Sum(x => x.D20 == null ? 0 : x.D20.Value);
            leavetotal.D21 = leaveList.Sum(x => x.D21 == null ? 0 : x.D21.Value);
            leavetotal.D22 = leaveList.Sum(x => x.D22 == null ? 0 : x.D22.Value);
            leavetotal.D23 = leaveList.Sum(x => x.D23 == null ? 0 : x.D23.Value);
            leavetotal.D24 = leaveList.Sum(x => x.D24 == null ? 0 : x.D24.Value);
            leavetotal.D25 = leaveList.Sum(x => x.D25 == null ? 0 : x.D25.Value);
            leavetotal.D26 = leaveList.Sum(x => x.D26 == null ? 0 : x.D26.Value);
            leavetotal.D27 = leaveList.Sum(x => x.D27 == null ? 0 : x.D27.Value);
            leavetotal.D28 = leaveList.Sum(x => x.D28 == null ? 0 : x.D28.Value);
            leavetotal.D29 = leaveList.Sum(x => x.D29 == null ? 0 : x.D29.Value);
            leavetotal.D30 = leaveList.Sum(x => x.D30 == null ? 0 : x.D30.Value);
            leavetotal.D31 = leaveList.Sum(x => x.D31 == null ? 0 : x.D31.Value);
            leavetotal.D32 = leaveList.Sum(x => x.D32 == null ? 0 : x.D32.Value);
            leavetotal.Total = leaveList.Sum(x => x.Total == null ? 0 : x.Total.Value);

            totalLeaveList.Add(leavetotal);

            TimesheetGridMonthWiseNewBO grandTotal = new TimesheetGridMonthWiseNewBO { Id = -100 };//"Grand Total"
          

            //grandTotal.D1 = projectList.Sum(x => x.D1 == null ? 0 : x.D1.Value) + leaveList.Sum(x => x.L1 == null ? 0 : x.L1.Value);
            //grandTotal.D2 = projectList.Sum(x => x.D2 == null ? 0 : x.D2.Value) + leaveList.Sum(x => x.L2 == null ? 0 : x.L2.Value);
            //grandTotal.D3 = projectList.Sum(x => x.D3 == null ? 0 : x.D3.Value) + leaveList.Sum(x => x.L3 == null ? 0 : x.L3.Value);
            //grandTotal.D4 = projectList.Sum(x => x.D4 == null ? 0 : x.D4.Value) + leaveList.Sum(x => x.L4 == null ? 0 : x.L4.Value);
            //grandTotal.D5 = projectList.Sum(x => x.D5 == null ? 0 : x.D5.Value) + leaveList.Sum(x => x.L5 == null ? 0 : x.L5.Value);
            //grandTotal.D6 = projectList.Sum(x => x.D6 == null ? 0 : x.D6.Value) + leaveList.Sum(x => x.L6 == null ? 0 : x.L6.Value);
            //grandTotal.D7 = projectList.Sum(x => x.D7 == null ? 0 : x.D7.Value) + leaveList.Sum(x => x.L7 == null ? 0 : x.L7.Value);
            //grandTotal.D8 = projectList.Sum(x => x.D8 == null ? 0 : x.D8.Value) + leaveList.Sum(x => x.L8 == null ? 0 : x.L8.Value);
            //grandTotal.D9 = projectList.Sum(x => x.D9 == null ? 0 : x.D9.Value) + leaveList.Sum(x => x.L9 == null ? 0 : x.L9.Value);
            //grandTotal.D10 = projectList.Sum(x => x.D10 == null ? 0 : x.D10.Value) + leaveList.Sum(x => x.L10 == null ? 0 : x.L10.Value);
            //grandTotal.D11 = projectList.Sum(x => x.D11 == null ? 0 : x.D11.Value) + leaveList.Sum(x => x.L11 == null ? 0 : x.L11.Value);
            //grandTotal.D12 = projectList.Sum(x => x.D12 == null ? 0 : x.D12.Value) + leaveList.Sum(x => x.L12 == null ? 0 : x.L12.Value);
            //grandTotal.D13 = projectList.Sum(x => x.D13 == null ? 0 : x.D13.Value) + leaveList.Sum(x => x.L13 == null ? 0 : x.L13.Value);
            //grandTotal.D14 = projectList.Sum(x => x.D14 == null ? 0 : x.D14.Value) + leaveList.Sum(x => x.L14 == null ? 0 : x.L14.Value);
            //grandTotal.D15 = projectList.Sum(x => x.D15 == null ? 0 : x.D15.Value) + leaveList.Sum(x => x.L15 == null ? 0 : x.L15.Value);
            //grandTotal.D16 = projectList.Sum(x => x.D16 == null ? 0 : x.D16.Value) + leaveList.Sum(x => x.L16 == null ? 0 : x.L16.Value);
            //grandTotal.D17 = projectList.Sum(x => x.D17 == null ? 0 : x.D17.Value) + leaveList.Sum(x => x.L17 == null ? 0 : x.L17.Value);
            //grandTotal.D18 = projectList.Sum(x => x.D18 == null ? 0 : x.D18.Value) + leaveList.Sum(x => x.L18 == null ? 0 : x.L18.Value);
            //grandTotal.D19 = projectList.Sum(x => x.D19 == null ? 0 : x.D19.Value) + leaveList.Sum(x => x.L19 == null ? 0 : x.L19.Value);
            //grandTotal.D20 = projectList.Sum(x => x.D20 == null ? 0 : x.D20.Value) + leaveList.Sum(x => x.L20 == null ? 0 : x.L20.Value);
            //grandTotal.D21 = projectList.Sum(x => x.D21 == null ? 0 : x.D21.Value) + leaveList.Sum(x => x.L21 == null ? 0 : x.L21.Value);
            //grandTotal.D22 = projectList.Sum(x => x.D22 == null ? 0 : x.D22.Value) + leaveList.Sum(x => x.L22 == null ? 0 : x.L22.Value);
            //grandTotal.D23 = projectList.Sum(x => x.D23 == null ? 0 : x.D23.Value) + leaveList.Sum(x => x.L23 == null ? 0 : x.L23.Value);
            //grandTotal.D24 = projectList.Sum(x => x.D24 == null ? 0 : x.D24.Value) + leaveList.Sum(x => x.L24 == null ? 0 : x.L24.Value);
            //grandTotal.D25 = projectList.Sum(x => x.D25 == null ? 0 : x.D25.Value) + leaveList.Sum(x => x.L25 == null ? 0 : x.L25.Value);
            //grandTotal.D26 = projectList.Sum(x => x.D26 == null ? 0 : x.D26.Value) + leaveList.Sum(x => x.L26 == null ? 0 : x.L26.Value);
            //grandTotal.D27 = projectList.Sum(x => x.D27 == null ? 0 : x.D27.Value) + leaveList.Sum(x => x.L27 == null ? 0 : x.L27.Value);
            //grandTotal.D28 = projectList.Sum(x => x.D28 == null ? 0 : x.D28.Value) + leaveList.Sum(x => x.L28 == null ? 0 : x.L28.Value);
            //grandTotal.D29 = projectList.Sum(x => x.D29 == null ? 0 : x.D29.Value) + leaveList.Sum(x => x.L29 == null ? 0 : x.L29.Value);
            //grandTotal.D30 = projectList.Sum(x => x.D30 == null ? 0 : x.D30.Value) + leaveList.Sum(x => x.L30 == null ? 0 : x.L30.Value);
            //grandTotal.D31 = projectList.Sum(x => x.D31 == null ? 0 : x.D31.Value) + leaveList.Sum(x => x.L31 == null ? 0 : x.L31.Value);
            //grandTotal.D32 = projectList.Sum(x => x.D32 == null ? 0 : x.D32.Value) + leaveList.Sum(x => x.L32 == null ? 0 : x.L32.Value);
            //grandTotal.Total = projectList.Sum(x => x.Total == null ? 0 : x.Total.Value) + leaveList.Sum(x => x.Total == null ? 0 : x.Total.Value);

            TimesheetGridMonthWiseNewBO totalOfProject = new TimesheetGridMonthWiseNewBO();
            totalOfProject = totalList[0];

            TimesheetGridMonthWiseNewBO totalOfLeave = new TimesheetGridMonthWiseNewBO();
            totalOfLeave = totalLeaveList[0];

            grandTotal.D1 = (totalOfProject.D1 == null ? 0 : totalOfProject.D1) + (totalOfLeave.D1 == null ? 0 : totalOfLeave.D1);
            grandTotal.D2 = (totalOfProject.D2 == null ? 0 : totalOfProject.D2) + (totalOfLeave.D2 == null ? 0 : totalOfLeave.D2);
            grandTotal.D3 = (totalOfProject.D3 == null ? 0 : totalOfProject.D3) + (totalOfLeave.D3 == null ? 0 : totalOfLeave.D3);
            grandTotal.D4 = (totalOfProject.D4 == null ? 0 : totalOfProject.D4) + (totalOfLeave.D4 == null ? 0 : totalOfLeave.D4);
            grandTotal.D5 = (totalOfProject.D5 == null ? 0 : totalOfProject.D5) + (totalOfLeave.D5 == null ? 0 : totalOfLeave.D5);
            grandTotal.D6 = (totalOfProject.D6 == null ? 0 : totalOfProject.D6) + (totalOfLeave.D6 == null ? 0 : totalOfLeave.D6);
            grandTotal.D7 = (totalOfProject.D7 == null ? 0 : totalOfProject.D7) + (totalOfLeave.D7 == null ? 0 : totalOfLeave.D7);
            grandTotal.D8 = (totalOfProject.D8 == null ? 0 : totalOfProject.D8) + (totalOfLeave.D8 == null ? 0 : totalOfLeave.D8);
            grandTotal.D9 = (totalOfProject.D9 == null ? 0 : totalOfProject.D9) + (totalOfLeave.D9 == null ? 0 : totalOfLeave.D9);
            grandTotal.D10 = (totalOfProject.D10 == null ? 0 : totalOfProject.D10) + (totalOfLeave.D10 == null ? 0 : totalOfLeave.D10);
            grandTotal.D11 = (totalOfProject.D11 == null ? 0 : totalOfProject.D11) + (totalOfLeave.D11 == null ? 0 : totalOfLeave.D11);
            grandTotal.D12 = (totalOfProject.D12 == null ? 0 : totalOfProject.D12) + (totalOfLeave.D12 == null ? 0 : totalOfLeave.D12);
            grandTotal.D13 = (totalOfProject.D13 == null ? 0 : totalOfProject.D13) + (totalOfLeave.D13 == null ? 0 : totalOfLeave.D13);
            grandTotal.D14 = (totalOfProject.D14 == null ? 0 : totalOfProject.D14) + (totalOfLeave.D14 == null ? 0 : totalOfLeave.D14);
            grandTotal.D15 = (totalOfProject.D15 == null ? 0 : totalOfProject.D15) + (totalOfLeave.D15 == null ? 0 : totalOfLeave.D15);
            grandTotal.D16 = (totalOfProject.D16 == null ? 0 : totalOfProject.D16) + (totalOfLeave.D16 == null ? 0 : totalOfLeave.D16);
            grandTotal.D17 = (totalOfProject.D17 == null ? 0 : totalOfProject.D17) + (totalOfLeave.D17 == null ? 0 : totalOfLeave.D17);
            grandTotal.D18 = (totalOfProject.D18 == null ? 0 : totalOfProject.D18) + (totalOfLeave.D18 == null ? 0 : totalOfLeave.D18);
            grandTotal.D19 = (totalOfProject.D19 == null ? 0 : totalOfProject.D19) + (totalOfLeave.D19 == null ? 0 : totalOfLeave.D19);
            grandTotal.D20 = (totalOfProject.D20 == null ? 0 : totalOfProject.D20) + (totalOfLeave.D20 == null ? 0 : totalOfLeave.D20);
            grandTotal.D21 = (totalOfProject.D21 == null ? 0 : totalOfProject.D21) + (totalOfLeave.D21 == null ? 0 : totalOfLeave.D21);
            grandTotal.D22 = (totalOfProject.D22 == null ? 0 : totalOfProject.D22) + (totalOfLeave.D22 == null ? 0 : totalOfLeave.D22);
            grandTotal.D23 = (totalOfProject.D23 == null ? 0 : totalOfProject.D23) + (totalOfLeave.D23 == null ? 0 : totalOfLeave.D23);
            grandTotal.D24 = (totalOfProject.D24 == null ? 0 : totalOfProject.D24) + (totalOfLeave.D24 == null ? 0 : totalOfLeave.D24);
            grandTotal.D25 = (totalOfProject.D25 == null ? 0 : totalOfProject.D25) + (totalOfLeave.D25 == null ? 0 : totalOfLeave.D25);
            grandTotal.D26 = (totalOfProject.D26 == null ? 0 : totalOfProject.D26) + (totalOfLeave.D26 == null ? 0 : totalOfLeave.D26);
            grandTotal.D27 = (totalOfProject.D27 == null ? 0 : totalOfProject.D27) + (totalOfLeave.D27 == null ? 0 : totalOfLeave.D27);
            grandTotal.D28 = (totalOfProject.D28 == null ? 0 : totalOfProject.D28) + (totalOfLeave.D28 == null ? 0 : totalOfLeave.D28);
            grandTotal.D29 = (totalOfProject.D29 == null ? 0 : totalOfProject.D29) + (totalOfLeave.D29 == null ? 0 : totalOfLeave.D29);
            grandTotal.D30 = (totalOfProject.D30 == null ? 0 : totalOfProject.D30) + (totalOfLeave.D30 == null ? 0 : totalOfLeave.D30);
            grandTotal.D31 = (totalOfProject.D31 == null ? 0 : totalOfProject.D31) + (totalOfLeave.D31 == null ? 0 : totalOfLeave.D31);
            grandTotal.D32 = (totalOfProject.D32 == null ? 0 : totalOfProject.D32) + (totalOfLeave.D32 == null ? 0 : totalOfLeave.D32);
            grandTotal.Total = (totalOfProject.Total == null ? 0 : totalOfProject.Total) + (totalOfLeave.Total == null ? 0 : totalOfLeave.Total);
                //projectList.Sum(x => x.Total == null ? 0 : x.Total.Value) + leaveList.Sum(x => x.Total == null ? 0 : x.Total.Value);

            totalLeaveList.Add(grandTotal);
            leaveList.AddRange(totalLeaveList);

            gridLeaves.Store[0].DataSource = leaveList;
            gridLeaves.Store[0].DataBind();

            //gridLeavesTotal.Store[0].DataSource = totalLeaveList;
            //gridLeavesTotal.Store[0].DataBind();


            if (isSuperviserEntry())
            {

                ShowHideControlsWithSupervisor();
                if (timesheet != null)
                {

                    if ((timesheet.Status == (int)TimeSheetStatus.NotSubmitted) || (timesheet.Status == (int)TimeSheetStatus.Draft))
                    {
                        btnAssign.Show();
                    }
                    else
                        btnAssign.Hide();
                }
            }
            
        }
        private void SetTimesheetWeekAndStatus(DateTime weekStartDate, string rejectionNotes, TimeSheetStatus status)
        {
            DateTime weekEndDate = weekStartDate.AddMonths(1).AddDays(-1);
            dispDate.Text =
                //"Week : " + NewTimeSheetManager.GetIso8601WeekOfYear(weekStartDate) +
                //    ", " +
                weekStartDate.ToString("MMM") + ", " + weekStartDate.Year +
                //+" - " +
                //weekEndDate.ToString("MMM d") + ", " + weekEndDate.Year +
                ", Status : " + (status == TimeSheetStatus.NotSubmitted ? "Not Filled" : status.ToString());

            if (status == TimeSheetStatus.Rejected)
            {
                btnSaveAndSend.Show();
                dispDate.Text += ", Rejection Notes : " + rejectionNotes;
            }
        }

        protected void ShowHideControl(bool IsViewMode)
        {
            if (IsViewMode)
            {
                txtFromDate.Hide();
                cmbApplyTo.Hide();
                btnNext.Hide();
                btnPrevious.Hide();
                btnSave.Hide();
                btnSaveAndSend.Hide();
                CommandColumn1.Hide();
                ImageCommandColumn3.Hide();
                ImageCommandColumn2.Hide();
                //ImageCommandColumn1.Hide();
                //btnAddProject.Hide();
                btnLoad.Hide();
                btnAssign.Hide();

            }
        }
        private void Initialise()
        {
            List<GetAssginedEmployeeProjectListResult> projectList = NewTimeSheetManager.GetEmployeeProjectList(
             this.ReadonlyEmployeeId == 0 ? SessionManager.CurrentLoggedInEmployeeId : this.ReadonlyEmployeeId);

            storeProjects.DataSource = projectList;
            storeProjects.DataBind();

            List<TextValue> listApproval = EmployeeManager.GetApplyToListForEmployee(SessionManager.CurrentLoggedInEmployeeId);
            cmbApplyTo.Store[0].DataSource = listApproval;
            cmbApplyTo.Store[0].DataBind();


            //storeTasks.DataSource = NewTimeSheetManager.GetAllTaskList(this.ReadonlyEmployeeId == 0 ? SessionManager.CurrentLoggedInEmployeeId : this.ReadonlyEmployeeId);
            //storeTasks.DataBind();

            storeLeaves.DataSource = LeaveAttendanceManager.GetAllLeavesWithManualAlso(SessionManager.CurrentCompanyId);
            storeLeaves.DataBind();

            if (isSuperviserEntry())
            {
                storeEmployee.DataSource = LeaveAttendanceManager.GetEmployeeListForLeaveAssign();
                storeEmployee.DataBind();
                cmbSearch.Show();
                ShowHideControlsWithSupervisor();
                Pagetitle.InnerText = "Assign TimeSheet";

                //TextValue _TextValue = new TextValue();
                //_TextValue.Value = SessionManager.CurrentLoggedInEmployeeId.ToString();
                //_TextValue.Text = EmployeeManager.GetEmployeeById(SessionManager.CurrentLoggedInEmployeeId).Name;
                //listApproval.Clear();
                //listApproval.Add(_TextValue);
            }
            else
                btnLoad_Click(null, null);



        }

        protected void ShowHideControlsWithSupervisor()
        {
            if (isSuperviserEntry())
            {
                cmbApplyTo.Hide();
                btnSave.Hide();
                btnSaveAndSend.Hide();
                btnAssign.Show();
            }
        }
    }
}