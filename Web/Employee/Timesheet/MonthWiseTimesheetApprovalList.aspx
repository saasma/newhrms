﻿<%@ Page Title="Time Sheet Approval List" Language="C#" AutoEventWireup="true" CodeBehind="MonthWiseTimesheetApprovalList.aspx.cs"
    Inherits="Web.NewHR.MonthWiseTimesheetApprovalList" %>

<%@ Register Src="~/Employee/Timesheet/UserControl/MonthWiseTimesheetCtl.ascx" TagName="TimesheetMonthwiseCtl"
    TagPrefix="ucTimeSheet" %>
<%@ Register Src="~/Controls/FooterCtl.ascx" TagName="FooterCtl" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/HeaderCtl.ascx" TagName="HeaderCtl" TagPrefix="uc3" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head id="Head1" runat="server">
    <title>Timesheet Entry</title>
    <link id="Link5" runat="server" rel="stylesheet" href="~/payrollmenu_files/mbcsmbmcp.css"
        type="text/css" />
    <link id="Link1" runat="server" rel="stylesheet" type="text/css" href="~/css/core.css" />
    <link id="Link2" runat="server" rel="Stylesheet" type="text/css" href="~/Styles/calendar/calendar.css" />
    <link id="Link4" runat="server" rel="stylesheet" href="~/Styles/design.css" />
    <ext:ResourcePlaceHolder ID="ResourcePlaceHolder1" runat="server" Mode="Script" />
    <style type="text/css">
        .bodypart
        {
            margin: 0 auto !important;
        }
        thead td, th
        {
            border: 0px;
        }
          #mbmcpebul_table
        {
            margin-top: 0px !important;
        }
    </style>
    <script type="text/javascript">
  

   var IsViewMode = false;

    var CommandHandler = function (command, record) {
             <%= hdnTimeSheetId.ClientID %>.setValue(record.data.TimesheetId); 
            if(command == 'Reject')
            { 
                
                 <%= hdnWeekNo.ClientID %>.setValue(record.data.WeekNo);
                 <%= btnReject.ClientID %>.fireEvent('click');
            }           
             if(command == 'View')
            { 
              IsViewMode=true;
                <%= btnView.ClientID %>.fireEvent('click');
//                var win = window.open( 'ViewTimesheet.aspx?id=' + record.data.TimesheetId, '_blank');
//                win.focus();
            
            }    
           
        };

    function focusEvent(e1, tab) {

    
        if (tab.id.toString().toLowerCase() == "panelapproval") {

         <%= hdnTabStatus.ClientID %>.setValue("1");
            <%= btnApprove.ClientID %>.show();
          searchList();
       
        }
           else if (tab.id.toString().toLowerCase() == "panelapproved") {
             <%= hdnTabStatus.ClientID %>.setValue("2");
             <%= btnApprove.ClientID %>.hide();
             searchList();
        }
           else if (tab.id.toString().toLowerCase() == "panelrejected") {
             <%= hdnTabStatus.ClientID %>.setValue("10");
             <%= btnApprove.ClientID %>.hide();
             searchList();
        }

         else if (tab.id.toString().toLowerCase() == "panelreviewed") {
             <%= hdnTabStatus.ClientID %>.setValue("11");
             <%= btnApprove.ClientID %>.hide();
             searchList();
        }


    };

    

    function searchList() {
            <%=PagingToolbar1.ClientID %>.doRefresh();
        }
           var prepareCommandList = function (grid, command, record, row) {
            if(row.data.Status == 1) //if awaiting for approval
            {
              command.items.get(0).show();
              command.items.get(1).show();// command seperator
               // command.hideMode = 'visibility';    //you can try 'display' also
            }
            else
            {
              command.items.get(1).hide();// command seperator
            }


           
        };

    </script>
</head>
<body style="margin: 0px; background-color: White;">
    <form id="Form1" runat="server">
    <ext:ResourceManager ID="ResourceManager1" runat="server" DisableViewState="false"
        ShowWarningOnAjaxFailure="false" ScriptMode="Release" Namespace="CompanyX" />
    <ext:Hidden ID="hdnTimeSheetId" runat="server" />
    <ext:Hidden ID="hdnTabStatus" runat="server" Text="1" />
    <ext:Hidden ID="hdnWeekNo" runat="server" />
    <ext:LinkButton ID="btnReject" runat="server" Hidden="true">
        <DirectEvents>
            <Click OnEvent="btnReject_Click">
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:LinkButton>
    <ext:LinkButton ID="btnView" runat="server" Hidden="true">
        <DirectEvents>
            <Click OnEvent="btnView_Click">
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:LinkButton>
    <uc3:HeaderCtl ID="HeaderCtl1" runat="server" />
    <div id="bodypart">
        <div class="bodypart">
            <h3>
                Time sheet list for approval</h3>
            <div class="attribute" style="padding:10px">
                <table>
                    <tr>
                        <td style="width: 30px;">
                            Date
                        </td>
                        <td>
                            <ext:DateField Width="120px" ID="txtStartDate" runat="server" LabelSeparator="" />
                        </td>
                        <td>
                            <ext:DateField Width="120px" ID="txtEndDate" runat="server" LabelSeparator="" />
                        </td>
                        <td style="width: 10px;">
                        </td>
                        <td>
                            <ext:ComboBox Width="200" ForceSelection="true" LabelWidth="45" ID="cmbStatus" runat="server"
                                Hidden="true" FieldLabel="Status" LabelSeparator="">
                                <Items>
                                    <ext:ListItem Text="All" Value="-2" />
                                    <%-- <ext:ListItem Text="Saved" Value="0" />--%>
                                    <ext:ListItem Text="Awaiting Approval" Value="1" />
                                    <ext:ListItem Text="Approved" Value="2" />
                                    <ext:ListItem Text="Rejected" Value="10" />
                                    <ext:ListItem Text="Reviewed" Value="11" />
                                </Items>
                                <SelectedItems>
                                    <ext:ListItem Index="1" />
                                </SelectedItems>
                            </ext:ComboBox>
                        </td>
                        <td style="width: 60px; padding-left: 10px">
                            Employee
                        </td>
                        <td style="width: 165px;">
                            <ext:Store runat="server" ID="storeSearch" AutoLoad="true">
                                <%--<Proxy>
                                    <ext:AjaxProxy Url="../../Handler/EmpSearch.ashx">
                                        <ActionMethods Read="GET" />
                                        <Reader>
                                            <ext:JsonReader Root="plants" TotalProperty="total" />
                                        </Reader>
                                    </ext:AjaxProxy>
                                </Proxy>--%>
                                <Model>
                                    <ext:Model ID="Model2" IDProperty="Value" Name="ItemLineModel" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="Text" Type="String" />
                                            <ext:ModelField Name="Value" Type="String" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                            <ext:ComboBox LabelSeparator="" QueryMode="Local" ID="cmbSearch" LabelWidth="70"
                                runat="server" DisplayField="Text" ValueField="Value" StoreID="storeSearch" TypeAhead="false"
                                Width="180" MinChars="1" TriggerAction="All" ForceSelection="true">
                                <ListConfig LoadingText="Searching..." MinWidth="200" StyleSpec="border-top:1px solid #98c0f4;">
                                    <ItemTpl ID="ItemTpl1" runat="server">
                                        <Html>
                                            <tpl>
                                            <div class="search-item">
                                                            <span>{Text}</span>  
                                             </div>
					                </tpl>
                                        </Html>
                                    </ItemTpl>
                                </ListConfig>
                                <Triggers>
                                    <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                                </Triggers>
                                <Listeners>
                                    <Select Handler="this.getTrigger(0).show();" />
                                    <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                    <TriggerClick Handler="if (index == 0) { 
                                           this.clearValue(); 
                                           this.getTrigger(0).hide();
                                       }" />
                                </Listeners>
                            </ext:ComboBox>
                        </td>
                        <td style="width: 20px">
                        </td>
                        <td>
                            <ext:Button runat="server" Width="100" Height="30" ID="btnLoad" Text="Load">
                                <Listeners>
                                    <Click Handler="searchList();" />
                                </Listeners>
                                <DirectEvents>
                                    <Click OnEvent="btnLoad_Click">
                                        <EventMask ShowMask="true" />
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                        </td>
                        <td>
                            <ext:Button runat="server" Width="125" Height="30" ID="btnAssignTimeSheet" Text="Assign TimeSheet">
                                <Listeners>
                                    <Click Handler="window.location ='MonthWiseTimesheetEntry.aspx?SuperviserEntry=true' ">
                                    </Click>
                                </Listeners>
                            </ext:Button>
                        </td>
                    </tr>
                </table>
            </div>
            <br />
            <table>
                <tr>
                    <td>
                        <div>
                            <ext:TabPanel ID="TabPanel_Main" ActiveIndex="0" Border="false" Unstyled="true" runat="server"
                                Width="1150" Plain="true" OverflowY="Auto">
                                <Items>
                                    <ext:Panel ID="PanelApproval" runat="server" AutoHeight="true" Title="Awaiting Approval"
                                        Header="False" Border="false" OverflowY="Auto">
                                    </ext:Panel>
                                    <ext:Panel ID="PanelApproved" AutoHeight="true" runat="server" Title="Approved" Header="False"
                                        Border="false">
                                    </ext:Panel>
                                    <ext:Panel ID="PanelRejected" AutoHeight="true" runat="server" Title="Rejected" Header="False"
                                        Border="false">
                                        <Content>
                                        </Content>
                                    </ext:Panel>
                                    <ext:Panel ID="PanelReviewed" AutoHeight="true" runat="server" Title="Reviewed" Header="False"
                                        Border="false">
                                        <Content>
                                        </Content>
                                    </ext:Panel>
                                </Items>
                                <Listeners>
                                    <TabChange Fn="focusEvent">
                                    </TabChange>
                                </Listeners>
                            </ext:TabPanel>
                        </div>
                        <ext:GridPanel ID="gridTimeSheet" runat="server" Cls="itemgrid" Width="1150" Scroll="None">
                            <Store>
                                <ext:Store ID="Store3" runat="server" AutoLoad="true" PageSize="50">
                                    <Proxy>
                                        <ext:AjaxProxy Json="true" Url="../../Handler/TimeSheetListForApproval.ashx">
                                            <ActionMethods Read="GET" />
                                            <Reader>
                                                <ext:JsonReader Root="data" TotalProperty="total" />
                                            </Reader>
                                        </ext:AjaxProxy>
                                    </Proxy>
                                    <AutoLoadParams>
                                        <ext:Parameter Name="start" Value="0" Mode="Raw" />
                                        <ext:Parameter Name="IsEmpView" Value="-1" Mode="Raw" />
                                    </AutoLoadParams>
                                    <Parameters>
                                        <ext:StoreParameter Name="EmployeeId" Value="#{cmbSearch}.getValue()" Mode="Raw"
                                            ApplyMode="Always" />
                                        <ext:StoreParameter Name="StartDate" Value="#{txtStartDate}.getValue()" Mode="Raw"
                                            ApplyMode="Always" />
                                        <ext:StoreParameter Name="EndDate" Value="#{txtEndDate}.getValue()" Mode="Raw" ApplyMode="Always" />
                                        <%--   <ext:StoreParameter Name="status" Value="#{cmbStatus}.getValue()" Mode="Raw" />--%>
                                        <ext:StoreParameter Name="IsMonthWise" Value="true" Mode="Raw" />
                                        <ext:StoreParameter Name="status" Value="#{hdnTabStatus}.getValue()" Mode="Raw" />
                                    </Parameters>
                                    <Model>
                                        <ext:Model ID="Model1" runat="server" IDProperty="TimesheetId">
                                            <Fields>
                                                <ext:ModelField Name="TimesheetId" />
                                                <ext:ModelField Name="EmployeeId" />
                                                <ext:ModelField Name="IdCardNo" />
                                                <ext:ModelField Name="Name" />
                                                <ext:ModelField Name="WeekNo" />
                                                <ext:ModelField Name="StartDate" />
                                                <ext:ModelField Name="ApprovedByName" />
                                                <ext:ModelField Name="ApprovedOn" Type="Date" />
                                                <ext:ModelField Name="TotalHours" />
                                                <ext:ModelField Name="StatusText" />
                                                <ext:ModelField Name="Status" Type="Int" />
                                                <ext:ModelField Name="WeekText" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                            <ColumnModel ID="ColumnModel2" runat="server">
                                <Columns>
                                    <ext:Column ID="colEId" runat="server" Text="EIN" Width="50" DataIndex="EmployeeId"
                                        Align="Center" Sortable="false" MenuDisabled="true">
                                    </ext:Column>
                                    <ext:Column ID="colIDCardNo" runat="server" Text="I No" Width="60" DataIndex="IdCardNo"
                                        Sortable="false" Align="Center" MenuDisabled="true">
                                    </ext:Column>
                                    <ext:Column ID="Column1" runat="server" Text="Employee Name" Width="180" DataIndex="Name"
                                        Sortable="false" MenuDisabled="true">
                                    </ext:Column>
                                    <ext:Column ID="Column2" runat="server" Text="Month" Width="90" DataIndex="WeekNo"
                                        Hidden="true" Align="Center" Sortable="false" MenuDisabled="true">
                                    </ext:Column>
                                    <ext:Column ID="colStartDate" runat="server" Text="Month" Width="180" DataIndex="WeekText"
                                        Sortable="false" MenuDisabled="true" />
                                    <ext:Column ID="Column4" runat="server" Align="Center" Text="Hours" Width="60" DataIndex="TotalHours"
                                        Sortable="false" MenuDisabled="true">
                                    </ext:Column>
                                    <ext:Column ID="Column5" runat="server" Text="Approved By" Width="150" DataIndex="ApprovedByName"
                                        Sortable="false" MenuDisabled="true">
                                    </ext:Column>
                                    <ext:DateColumn ID="colApprovedDate" runat="server" Text="Approved Date" Width="110"
                                        DataIndex="ApprovedOn" Sortable="false" MenuDisabled="true" Format="MM/dd/Y" />
                                    <ext:Column ID="Column3" runat="server" Text="Status" Width="120" DataIndex="StatusText"
                                        Sortable="false" MenuDisabled="true">
                                    </ext:Column>
                                    <ext:CommandColumn ID="CommandColumn1" runat="server" Width="120" Sortable="false"
                                        MenuDisabled="true" Text="Actions" Align="Center">
                                        <Commands>
                                            <ext:GridCommand Text="<i></i>Reject" CommandName="Reject" ToolTip-Text="Reject"
                                                Hidden="true" />
                                            <ext:CommandSeparator>
                                            </ext:CommandSeparator>
                                            <ext:GridCommand Text="<i></i>View" CommandName="View" ToolTip-Text="View" />
                                        </Commands>
                                        <PrepareToolbar Fn="prepareCommandList" />
                                        <Listeners>
                                            <Command Handler="CommandHandler(command,record);" />
                                        </Listeners>
                                    </ext:CommandColumn>
                                </Columns>
                            </ColumnModel>
                            <SelectionModel>
                                <ext:CheckboxSelectionModel ID="CheckboxSelectionModel1" runat="server" Mode="Multi" />
                            </SelectionModel>
                            <BottomBar>
                                <ext:PagingToolbar ID="PagingToolbar1" runat="server" PageSize="50" DisplayInfo="true"
                                    DisplayMsg="Displaying Time Sheet {0} - {1} of {2}" EmptyMsg="No Records to display">
                                    <%--<Items>
                                        <ext:ComboBox Hidden="true" runat="server" SelectedIndex="0" Width="80" ID="cmbPageSize">
                                            <Items>
                                                <ext:ListItem Text="20" Value="20" />
                                                <ext:ListItem Text="30" Value="30" />
                                                <ext:ListItem Text="50" Value="50" />
                                            </Items>
                                            <Listeners>
                                                <Select Handler="#{PagingToolbar1}.pageSize = parseInt( '50'); searchList();" />
                                            </Listeners>
                                        </ext:ComboBox>
                                    </Items>--%>
                                </ext:PagingToolbar>
                            </BottomBar>
                        </ext:GridPanel>
                    </td>
                </tr>
            </table>
            <div class="buttonBlock" style="width: 68%;">
                <table>
                    <tr>
                        <td>
                            <ext:Button ID="btnApprove" runat="server" Text="Approve" Width="100" Height="30"
                                Hidden="true">
                                <DirectEvents>
                                    <Click OnEvent="btnApprove_Click">
                                        <EventMask ShowMask="true" />
                                        <Confirmation ConfirmRequest="true" Message="Are you sure, you want to approve the timesheets?" />
                                        <ExtraParams>
                                            <ext:Parameter Name="gridItems" Value="Ext.encode(#{gridTimeSheet}.getRowsValues({selectedOnly:true}))"
                                                Mode="Raw" />
                                        </ExtraParams>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                        </td>
                        <%-- <td>
                            <ext:Button ID="btnReview" runat="server" Text="Review" Width="100" Height="30">
                                <DirectEvents>
                                    <Click OnEvent="btnReview_Click">
                                        <EventMask ShowMask="true" />
                                        <Confirmation ConfirmRequest="true" Message="Are you sure, you want to review the timesheets?" />
                                        <ExtraParams>
                                            <ext:Parameter Name="gridItems" Value="Ext.encode(#{gridTimeSheet}.getRowsValues({selectedOnly:true}))"
                                                Mode="Raw" />
                                        </ExtraParams>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                        </td>--%>
                    </tr>
                </table>
            </div>
            <br />
        </div>
    </div>
    <uc1:FooterCtl ID="FooterCtl1" runat="server" />
    <ext:Window ID="windowTimesheetDetails" Width="1200" AutoScroll="true" Height="600"
        BodyPadding="5" runat="server" Hidden="true" Modal="true">
        <Content>
            <ucTimeSheet:TimesheetMonthwiseCtl Id="TimesheetCtl1" runat="server" />
            <div style="margin-top: 10px; margin-left: 0px">
                <table>
                    <tr>
                        <td>
                            <ext:Button runat="server" ID="btnApproveView" StyleSpec="margin-left:0px" Height="28"
                                Width="100" Text="Approve">
                                <DirectEvents>
                                    <Click OnEvent="btnApproveView_Click">
                                        <EventMask ShowMask="true" />
                                        <Confirmation Message="Confirm approve timesheet?" ConfirmRequest="true">
                                        </Confirmation>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                        </td>
                        <td style="padding-left: 10px">
                            <ext:LinkButton runat="server" StyleSpec="padding:0px;" ID="LinkButton1" Cls="btnFlatLeftGap"
                                Text="<i></i>Cancel">
                                <Listeners>
                                    <Click Handler="#{windowTimesheetDetails}.hide();">
                                    </Click>
                                </Listeners>
                            </ext:LinkButton>
                        </td>
                    </tr>
                </table>
            </div>
        </Content>
    </ext:Window>
    <ext:Window ID="WTimeSheet" runat="server" Title="Time Sheet Details" Icon="Application"
        Width="550" Height="275" BodyPadding="5" Hidden="true" Modal="true">
        <Content>
            <table class="fieldTable">
                <tr>
                    <td>
                        <ext:DisplayField Width="300px" ID="txtEmployeeName" runat="server" FieldLabel="Employee Name"
                            ReadOnly="true" LabelAlign="Left" />
                    </td>
                    <td style="display: none">
                        <ext:DisplayField Width="200px" ID="txtWeekNo" runat="server" FieldLabel="Week No"
                            ReadOnly="true" LabelAlign="Left" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <ext:DisplayField Width="300px" LabelAlign="Left" runat="server" FieldLabel="Date"
                            ID="txtWeekDate" ReadOnly="true" />
                    </td>
                    <td>
                        <ext:DisplayField Width="200px" ID="txtTotalHours" runat="server" FieldLabel="Total Hours"
                            ReadOnly="true" LabelAlign="Left" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <ext:TextArea ID="txtNotes" runat="server" FieldLabel="Notes *" LabelSeparator=""
                            LabelAlign="Left" Rows="4" Cols="55" />
                        <asp:RequiredFieldValidator Display="None" ID="val1" runat="server" ValidationGroup="SaveRejectNote"
                            ControlToValidate="txtNotes" ErrorMessage="Reject Note is required." />
                    </td>
                </tr>
                <tr>
                    <td valign="bottom" colspan="2">
                        <div class="popupButtonDiv">
                            <table>
                                <tr>
                                    <td>
                                        <ext:Button runat="server" ID="btnRejectSave" Height="30" Width="100" Text="Reject">
                                            <DirectEvents>
                                                <Click OnEvent="btnRejectSave_Click">
                                                    <EventMask ShowMask="true" />
                                                    <Confirmation Message="Are you sure, you want to reject the time sheet?" ConfirmRequest="true">
                                                    </Confirmation>
                                                </Click>
                                            </DirectEvents>
                                        </ext:Button>
                                    </td>
                                    <td style="padding-left: 10px; padding-right: 10px">
                                        <div class="btnFlatOr">
                                            or</div>
                                    </td>
                                    <td>
                                        <ext:LinkButton runat="server" StyleSpec="padding:0px;" ID="LinkButton2" Cls="btnFlatLeftGap"
                                            Text="<i></i>Cancel">
                                            <Listeners>
                                                <Click Handler="#{WTimeSheet}.hide();">
                                                </Click>
                                            </Listeners>
                                        </ext:LinkButton>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
        </Content>
    </ext:Window>
    </form>
</body>
</html>
