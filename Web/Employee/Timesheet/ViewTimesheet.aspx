﻿<%@ Page Title="View Timesheet" Language="C#" EnableViewState="false" AutoEventWireup="true"
    CodeBehind="ViewTimesheet.aspx.cs" Inherits="Web.Employee.Timesheet.ViewTimesheet" %>

<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/FooterCtl.ascx" TagName="FooterCtl" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/HeaderCtl.ascx" TagName="HeaderCtl" TagPrefix="uc3" %>
<%@ Register Src="~/UserControls/LogoutCtl.ascx" TagName="LogoutCtl" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/TopMenu.ascx" TagName="TopMenu" TagPrefix="uc2" %>
<%@ Register Src="~/Employee/Timesheet/UserControl/TimesheetCtl.ascx" TagName="TimesheetCtl"
    TagPrefix="uc4" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head id="Head1" runat="server">
    <title>Timesheet Entry</title>
    <link id="Link5" runat="server" rel="stylesheet" href="~/payrollmenu_files/mbcsmbmcp.css"
        type="text/css" />
    <link id="Link1" runat="server" rel="stylesheet" type="text/css" href="~/css/core.css" />
    <link id="Link2" runat="server" rel="Stylesheet" type="text/css" href="~/Styles/calendar/calendar.css" />
    <link id="Link4" runat="server" rel="stylesheet" href="~/Styles/design.css" />
    <ext:ResourcePlaceHolder ID="ResourcePlaceHolder1" runat="server" Mode="Script" />
    <style type="text/css">
        .bodypart
        {
            margin: 0 auto !important;
        }
        
        /*hide calendar icon*/
        .ext-cal-ic-rem
        {
            display: none;
        }
         #mbmcpebul_table{margin-top:0px!important;}
    </style>
</head>
<body style="margin: 0px; background-color: White;">
    <form id="Form1" runat="server">
    <ext:ResourceManager ID="ResourceManager1" runat="server" DisableViewState="true"
        ShowWarningOnAjaxFailure="false" ScriptMode="Release" Namespace="CompanyX" />
    <uc3:HeaderCtl ID="HeaderCtl1" runat="server" />
    <div id="bodypart">
        <div class="bodypart">
            <h3 runat="server" id="title">
                Timesheet : </h3>
            <uc4:TimesheetCtl Id="TimesheetCtl1" runat="server" />
        </div>
    </div>
    <uc1:FooterCtl ID="FooterCtl1" runat="server" />
    </form>
</body>
</html>
