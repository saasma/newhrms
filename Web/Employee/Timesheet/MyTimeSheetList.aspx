﻿<%@ Page Title="My Timesheet List" Language="C#" AutoEventWireup="true" CodeBehind="MyTimeSheetList.aspx.cs"
    Inherits="Web.Employee.EmployeeTimeSheetList" %>

<%@ Register Src="~/Controls/FooterCtl.ascx" TagName="FooterCtl" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/HeaderCtl.ascx" TagName="HeaderCtl" TagPrefix="uc3" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head id="Head1" runat="server">
    <title>Timesheet Entry</title>
    <link id="Link5" runat="server" rel="stylesheet" href="~/payrollmenu_files/mbcsmbmcp.css"
        type="text/css" />
    <link id="Link1" runat="server" rel="stylesheet" type="text/css" href="~/css/core.css" />
    <link id="Link2" runat="server" rel="Stylesheet" type="text/css" href="~/Styles/calendar/calendar.css" />
    <link id="Link4" runat="server" rel="stylesheet" href="~/Styles/design.css" />
    <ext:ResourcePlaceHolder ID="ResourcePlaceHolder1" runat="server" Mode="Script" />
    <style type="text/css">
        .bodypart
        {
            margin: 0 auto !important;
        }
        thead td, th
        {
            border: 0px;
        }
        #mbmcpebul_table
        {
            margin-top: 0px !important;
        }
    </style>
    <script type="text/javascript">

    function searchList() {
            <%=PagingToolbar1.ClientID %>.doRefresh();
        }

    
    var CommandHandler = function (command, record) {
            
            window.location = 'TimesheetEntry.aspx?month='
              + record.data.StartDate; 
        };

   var prepareToolbar = function (grid, toolbar, rowIndex, record) {

            if(record.data.Status ==-1) {
                toolbar.hide();
            }

        
        };

    </script>
</head>
<body style="margin: 0px; background-color: White;">
    <form id="Form1" runat="server">
    <ext:ResourceManager ID="ResourceManager1" runat="server" DisableViewState="false"
        ShowWarningOnAjaxFailure="false" ScriptMode="Release" Namespace="CompanyX" />
    <ext:Hidden ID="hdnTimeSheetId" runat="server" />
    <ext:Hidden ID="hdnEmployeeId" runat="server" />
    <uc3:HeaderCtl ID="HeaderCtl1" runat="server" />
    <div id="bodypart">
        <div class="bodypart">
            <h3>
                My Timesheet List</h3>
            <div class="attribute" style="padding: 10px">
                <table>
                    <tr>
                        <td style="width: 30px;">
                            Date
                        </td>
                        <td>
                            <ext:DateField Width="120px" ID="txtStartDate" runat="server" LabelSeparator="" />
                        </td>
                        <td>
                            <ext:DateField Width="120px" ID="txtEndDate" runat="server" LabelSeparator="" />
                        </td>
                        <td style="width: 10px;">
                        </td>
                        <td>
                            <ext:ComboBox Width="190" ForceSelection="true" LabelWidth="50" ID="cmbStatus" runat="server"
                                FieldLabel="Status" LabelSeparator="">
                                <Items>
                                    <ext:ListItem Text="All" Value="-2" />
                                    <ext:ListItem Text="Draft" Value="0" />
                                    <ext:ListItem Text="Awaiting Approval" Value="1" />
                                    <ext:ListItem Text="Approved" Value="2" />
                                    <ext:ListItem Text="Rejected" Value="10" />
                                    <ext:ListItem Text="Not Filled" Value="-1" />
                                </Items>
                                <SelectedItems>
                                    <ext:ListItem Index="0" />
                                </SelectedItems>
                            </ext:ComboBox>
                        </td>
                        <td style="width: 20px;">
                        </td>
                        <td>
                            <ext:Button runat="server" Width="100" Height="30" ID="btnLoad" Text="Load">
                                <Listeners>
                                    <Click Handler="searchList();" />
                                </Listeners>
                            </ext:Button>
                        </td>
                    </tr>
                </table>
            </div>
            <br />
            <table>
                <tr>
                    <td>
                        <ext:GridPanel ID="gridTimeSheet" runat="server" Cls="itemgrid" Width="900" Scroll="None">
                            <Store>
                                <ext:Store ID="Store3" runat="server" AutoLoad="true" PageSize="50">
                                    <Proxy>
                                        <ext:AjaxProxy Json="true" Url="../../Handler/MyTimeSheetList.ashx">
                                            <ActionMethods Read="GET" />
                                            <Reader>
                                                <ext:JsonReader Root="data" TotalProperty="total" />
                                            </Reader>
                                        </ext:AjaxProxy>
                                    </Proxy>
                                    <AutoLoadParams>
                                        <ext:Parameter Name="start" Value="0" Mode="Raw" />
                                    </AutoLoadParams>
                                    <Parameters>
                                        <ext:StoreParameter Name="EmployeeId" Value="#{hdnEmployeeId}.getValue()" Mode="Raw"
                                            ApplyMode="Always" />
                                        <ext:StoreParameter Name="StartDate" Value="#{txtStartDate}.getValue()" Mode="Raw"
                                            ApplyMode="Always" />
                                        <ext:StoreParameter Name="EndDate" Value="#{txtEndDate}.getValue()" Mode="Raw" ApplyMode="Always" />
                                        <ext:StoreParameter Name="status" Value="#{cmbStatus}.getValue()" Mode="Raw" />
                                    </Parameters>
                                    <Model>
                                        <ext:Model ID="Model1" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="TimesheetId" />
                                                <ext:ModelField Name="EmployeeId" />
                                                <ext:ModelField Name="IdCardNo" />
                                                <ext:ModelField Name="Name" />
                                                <ext:ModelField Name="Month" />
                                                <ext:ModelField Name="StartDate" />
                                                <ext:ModelField Name="CreatedByName" />
                                                <ext:ModelField Name="ApprovedByName" />
                                                <ext:ModelField Name="ApprovedOn" Type="Date" />
                                                <ext:ModelField Name="TotalHours" />
                                                <ext:ModelField Name="StatusText" />
                                                 <ext:ModelField Name="Status" />
                                                <ext:ModelField Name="WeekText" />
                                                <ext:ModelField Name="StartDate" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                            <ColumnModel ID="ColumnModel2" runat="server">
                                <Columns>
                                    <ext:Column ID="Column2" runat="server" Text="Month" Width="120" DataIndex="Month"
                                        Align="Left" Sortable="false" MenuDisabled="true">
                                    </ext:Column>
                                    <ext:Column ID="Column1" runat="server" Align="Center" Text="Hours" Width="60" DataIndex="TotalHours"
                                        Sortable="false" MenuDisabled="true">
                                    </ext:Column>
                                    <ext:Column ID="Column5" runat="server" Text="Created By" Width="180" DataIndex="CreatedByName"
                                        Sortable="false" MenuDisabled="true">
                                    </ext:Column>
                                    <ext:Column ID="Column4" runat="server" Text="Approved By" Width="180" DataIndex="ApprovedByName"
                                        Sortable="false" MenuDisabled="true">
                                    </ext:Column>
                                    <ext:DateColumn ID="colApprovedDate" runat="server" Text="Approved Date" Width="110"
                                        DataIndex="ApprovedOn" Sortable="false" MenuDisabled="true" Format="MM/dd/Y" />
                                    <ext:Column ID="Column3" runat="server" Text="Status" Width="90" DataIndex="StatusText"
                                        Sortable="false" MenuDisabled="true">
                                    </ext:Column>
                                    <ext:CommandColumn ID="CommandColumn1" runat="server" Width="70" Sortable="false"
                                        MenuDisabled="true" Text="Actions" Align="Center">
                                        <Commands>
                                            <ext:CommandSeparator />
                                            <ext:GridCommand Text="<i></i>View" CommandName="View" ToolTip-Text="View" />
                                        </Commands>
                                        <Listeners>
                                            <Command Handler="CommandHandler(command,record);" />
                                        </Listeners>
                                         <PrepareToolbar Fn="prepareToolbar" />
                                    </ext:CommandColumn>
                                </Columns>
                            </ColumnModel>
                            <SelectionModel>
                                <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" Mode="Single" />
                            </SelectionModel>
                            <BottomBar>
                                <ext:PagingToolbar ID="PagingToolbar1" runat="server" PageSize="50" DisplayInfo="true"
                                    DisplayMsg="Displaying time sheet {0} - {1} of {2}" EmptyMsg="No Records to display">
                                    <%--<Items>
                                        <ext:ComboBox Hidden="true" runat="server" SelectedIndex="0" Width="80" ID="cmbPageSize">
                                            <Items>
                                                <ext:ListItem Text="20" Value="20" />
                                                <ext:ListItem Text="30" Value="30" />
                                                <ext:ListItem Text="50" Value="50" />
                                            </Items>
                                            <Listeners>
                                                <Select Handler="#{PagingToolbar1}.pageSize = parseInt( '50'); searchList();" />
                                            </Listeners>
                                        </ext:ComboBox>
                                    </Items>--%>
                                </ext:PagingToolbar>
                            </BottomBar>
                        </ext:GridPanel>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <uc1:FooterCtl ID="FooterCtl1" runat="server" />
    </form>
</body>
</html>
