﻿<%@ Page Title="Time Sheet Entry" Language="C#" EnableViewState="false" AutoEventWireup="true"
    CodeBehind="TimesheetEntry.aspx.cs" Inherits="Web.Employee.Timesheet.TimesheetEntry" %>

<%@ Register Src="~/Controls/ContentHeader.ascx" TagName="ContentHeader" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/FooterCtl.ascx" TagName="FooterCtl" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/HeaderCtl.ascx" TagName="HeaderCtl" TagPrefix="uc3" %>
<%@ Register Src="~/UserControls/LogoutCtl.ascx" TagName="LogoutCtl" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/TopMenu.ascx" TagName="TopMenu" TagPrefix="uc2" %>
<%@ Register Src="~/Employee/Timesheet/UserControl/TimesheetCtl.ascx" TagName="TimesheetCtl"
    TagPrefix="uc4" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head id="Head1" runat="server">
    <title>Timesheet Entry</title>
    <link id="Link5" runat="server" rel="stylesheet" href="~/payrollmenu_files/mbcsmbmcp.css"
        type="text/css" />
   
   <link id="Link1" rel="stylesheet" type="text/css" href="<%= ResolveUrl("~/css/core.css?v=") + Web.Helper.WebHelper.Version %>" />
    <link id="Link2" rel="Stylesheet" type="text/css" href="<%= ResolveUrl("~/Styles/calendar/calendar.css?v=") + Web.Helper.WebHelper.Version %>"  />
    <link id="Link4" runat="server" rel="stylesheet" href="~/Styles/design.css" />
    <ext:ResourcePlaceHolder ID="ResourcePlaceHolder1" runat="server" Mode="Script" />
     <script src="<%=ResolveUrl("~/theme/scripts/jquery-1.8.2.min.js?v=")+ Web.Helper.WebHelper.Version %>" type="text/javascript"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/scripts/common.js?v=") + Web.Helper.WebHelper.Version %>"></script>
    <style type="text/css">
        .bodypart
        {
            margin: 0 auto !important;
        }
        
        /*hide calendar icon*/
        .ext-cal-ic-rem
        {
            display: none;
        }
         #mbmcpebul_table{margin-top:0px!important;}
    </style>
</head>
<body style="margin: 0px; background-color: White;">
    <form id="Form1" runat="server">
    <uc2:ContentHeader runat="server" />
    <uc3:HeaderCtl ID="HeaderCtl1" runat="server" />
    <div id="bodypart">
        <div class="bodypart" style="padding-top:10px">
            
            <uc4:TimesheetCtl Id="TimesheetCtl1" runat="server" />
        </div>
    </div>
    <uc1:FooterCtl ID="FooterCtl1" runat="server" />
    </form>
</body>
</html>
