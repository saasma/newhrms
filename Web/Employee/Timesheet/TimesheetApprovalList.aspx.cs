﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using BLL.BO;
using System.IO;
using Utils.Calendar;
using Utils.Helper;

namespace Web.NewHR
{
    public partial class TimesheetApprovalList : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
               
            }
        }       

        protected void btnApprove_Click(object sender, DirectEventArgs e)
        {
            
            string gridItemsJson = e.ExtraParams["gridItems"];
          
            List<GetTimeSheetListForApprovalResult> list = JSON.Deserialize<List<GetTimeSheetListForApprovalResult>>(gridItemsJson);
            List<Timesheet> timesheetList = new List<Timesheet>();

            
            List<String> _EmployeeList = new List<string>();
            foreach (var item in list)
            {
                Timesheet obj = new Timesheet()
                {
                    EmployeeId = item.EmployeeId,
                    TimesheetId = item.TimesheetId
                };
                timesheetList.Add(obj);
                _EmployeeList.Add(item.EmployeeId.ToString());
            }

            if (list.Count == 0)
                return;

            Status status = NewTimeSheetManager.ApproveTimeSheet(timesheetList);
            if (status.IsSuccess)
            {
                CheckboxSelectionModel1.ClearSelection();
                NewMessage.ShowNormalMessage( "Time sheets approved successfully.");
                SendApproveMail(timesheetList);
                PagingToolbar1.DoRefresh();
            }
            else
            {
                NewMessage.ShowWarningMessage( status.ErrorMessage);
            }
        }

        private void ClearFields()
        {
            txtEmployeeName.Text = "";
            txtWeekNo.Text = "";
            txtWeekDate.Text = "";
            txtTotalHours.Text = "";
            txtNotes.Text = "";
        }

        protected void btnView_Click(object sender, DirectEventArgs e)
        {

            int timesheetid = int.Parse(hdnTimeSheetId.Text);
            windowTimesheetDetails.Center();
            windowTimesheetDetails.Show();
            DAL.Timesheet timesheet = NewTimeSheetManager.GetTimeSheetById(timesheetid);
            if (timesheet == null)
            {
                NewMessage.ShowWarningMessage("Time sheet does not exists.");
                return;
            }
            else
            {
                windowTimesheetDetails.Title = "Timeheet of " +
                    new EmployeeManager().GetById(timesheet.EmployeeId).Name;
            }
            TimesheetCtl1.LoadTimesheet(timesheetid);
        }
        protected void btnReject_Click(object sender, DirectEventArgs e)
        {
            ClearFields();

            int timeSheetId = int.Parse(hdnTimeSheetId.Text);

            Timesheet obj = NewTimeSheetManager.GetTimeSheetById(timeSheetId);
            if (obj != null && NewTimeSheetManager.IsTimesheetViewableForManager(obj.TimesheetId))
            {
                txtEmployeeName.Text = new EmployeeManager().GetById(obj.EmployeeId).Name;
                //if (!string.IsNullOrEmpty(hdnWeekNo.Text))



                txtWeekNo.Text = obj.StartDate.Value.Year + " - " +
                    DateHelper.GetMonthName(obj.StartDate.Value.Month, true);

                if (obj.StartDate != null)
                    txtWeekDate.Text = NewTimeSheetManager.GetWeekRangeName(
                        obj.StartDate.Value, obj.EndDate.Value);

                if (obj.TotalHours != null)
                    txtTotalHours.Text = obj.TotalHours.Value.ToString();
                WTimeSheet.Show();
            }
        }

        protected void btnRejectSave_Click(object sender, DirectEventArgs e)
        {
            int timeSheetId = int.Parse(hdnTimeSheetId.Text);

            Timesheet obj = new Timesheet()
            {
                TimesheetId = timeSheetId,
                Status = (int)TimeSheetStatus.Rejected,
                Notes = string.IsNullOrEmpty(txtNotes.Text.Trim()) ? null : txtNotes.Text.Trim()
            };

            Status status = NewTimeSheetManager.RejectTimeSheet(obj);
            if(status.IsSuccess)
            {
                WTimeSheet.Close();
                Timesheet _Timesheet = NewTimeSheetManager.GetTimeSheetById(timeSheetId);
                SendRejectMail(_Timesheet.EmployeeId.ToString(), obj.Notes);
                NewMessage.ShowNormalMessage( "Time sheet rejected successfully.");
                PagingToolbar1.DoRefresh();
            }
            else
            {
                NewMessage.ShowWarningMessage( status.ErrorMessage);
            }
        }



        protected void SendApproveMail(List<Timesheet> TimesheetList)
        {

            SMTPHelper mailSender = new SMTPHelper();

            EmailContent dbMailContent = CommonManager.GetMailContent((int)EmailContentType.TimeSheetApprovedOrReject);

            //string MonthName = Convert.ToDateTime(txtStartDate.Text).Year + " - " +
            //DateHelper.GetMonthName(Convert.ToDateTime(txtStartDate.Text).Month, true);

            int index = 0;
            int count = 0;
            foreach (Timesheet timesheet in TimesheetList)
            {

                Timesheet _TimeSheet = NewTimeSheetManager.GetTimeSheetById(timesheet.TimesheetId);
                //string MonthName = GetTimesheetWeeksForMail(Convert.ToDateTime(_TimeSheet.StartDate.Value)) + " - ";

                string MonthName = Convert.ToDateTime(_TimeSheet.StartDate.Value).Year + " - " +
                DateHelper.GetMonthName(Convert.ToDateTime(_TimeSheet.StartDate.Value).Month, true);

                string subject = dbMailContent.Subject.Replace("#YearMonth#", MonthName).Replace("#Status#", "Approved");
                string body = dbMailContent.Body.Replace("#YearMonth#", MonthName).Replace("#Status#", "Approved").Replace("#Reason#", "");

                EEmployee _Employeee = EmployeeManager.GetEmployeeById(timesheet.EmployeeId);
                if (string.IsNullOrEmpty(_Employeee.EAddresses[0].CIEmail))
                {
                    continue;
                    //NewMessage.ShowNormalMessage("Email address not found for Employee " + _Employeee.Name);
                    //return;
                }


                bool isSendSuccess = SMTPHelper.SendAsyncMail(_Employeee.EAddresses[0].CIEmail, body, subject, "");
                if (isSendSuccess)
                    count++;

                //if (!string.IsNullOrEmpty(to))
                //    to += "," + _Employeee.EAddresses[0].CIEmail;
                //else
                //    to = _Employeee.EAddresses[0].CIEmail;
                //index++;
            }


            //string[] ToEmailIDs = to.Split(',');
            //int count = 0;
            //foreach (string ToEMailId in ToEmailIDs)
            //{
            //    bool isSendSuccess = SMTPHelper.SendAsyncMail(ToEMailId, body, subject, "");
            //    if (isSendSuccess)
            //        count++;
            //}

            //if (count > 0)
            //{
            //    NewMessage.ShowNormalMessage(string.Format(Resources.Messages.SentMailSuccessMsg, count));
            //    return;
            //}
            //else
            //{
            //    NewMessage.ShowWarningMessage("error while sending email");
            //    return;
            //}


        }


        //protected void SendApproveMail(string[] EmployeeIDs)
        //{

        //    SMTPHelper mailSender = new SMTPHelper();
        //    EmailContent dbMailContent = CommonManager.GetMailContent((int)EmailContentType.TimeSheetApprovedOrReject);

        //    string MonthName = Convert.ToDateTime(txtStartDate.Text).Year + " - " +
        //    DateHelper.GetMonthName(Convert.ToDateTime(txtStartDate.Text).Month, true);

        //    string subject = dbMailContent.Subject.Replace("#YearMonth#", MonthName).Replace("#Status#", "Approved");

        //    string body = dbMailContent.Body.Replace("#YearMonth#", MonthName).Replace("#Status#", "Approved").Replace("#Reason#", "");

        //    string to = "";
        //    foreach (string EmployeeID in EmployeeIDs)
        //    {

        //        EEmployee _Employeee = EmployeeManager.GetEmployeeById(int.Parse(EmployeeID));
        //        if (string.IsNullOrEmpty(_Employeee.EAddresses[0].CIEmail))
        //        {
        //            NewMessage.ShowNormalMessage("Email address not found for Employee " + _Employeee.Name);
        //            return;
        //        }

        //        if (!string.IsNullOrEmpty(to))
        //            to += "," + _Employeee.EAddresses[0].CIEmail;
        //        else
        //            to = _Employeee.EAddresses[0].CIEmail;

        //    }


        //    string[] ToEmailIDs = to.Split(',');
        //    int count = 0;
        //    foreach (string ToEMailId in ToEmailIDs)
        //    {
        //        bool isSendSuccess = SMTPHelper.SendMail(ToEMailId, body, subject, "");
        //        if (isSendSuccess)
        //            count++;
        //    }

        //    if (count > 0)
        //    {
        //        NewMessage.ShowNormalMessage(string.Format(Resources.Messages.SentMailSuccessMsg, count));
        //        return;
        //    }
           


        //}

        protected void SendRejectMail(string EmployeeID,string Reason)
        {

            SMTPHelper mailSender = new SMTPHelper();
            EmailContent dbMailContent = CommonManager.GetMailContent((int)EmailContentType.TimeSheetApprovedOrReject);

            string MonthName = Convert.ToDateTime(txtStartDate.Text).Year + " - " +
            DateHelper.GetMonthName(Convert.ToDateTime(txtStartDate.Text).Month, true);

            string subject = dbMailContent.Subject.Replace("#YearMonth#", MonthName).Replace("#Status#", "Rejected");

            string body = dbMailContent.Body.Replace("#YearMonth#", MonthName).Replace("#Status#", "Rejected").Replace("#Reason#", Reason);

            string to = "";
          
                EEmployee _Employeee = EmployeeManager.GetEmployeeById(int.Parse(EmployeeID));
                if (string.IsNullOrEmpty(_Employeee.EAddresses[0].CIEmail))
                {
                    NewMessage.ShowNormalMessage("Email address not found for Employee " + _Employeee.Name);
                    return;
                }

                if (!string.IsNullOrEmpty(to))
                    to += "," + _Employeee.EAddresses[0].CIEmail;
                else
                    to = _Employeee.EAddresses[0].CIEmail;

            
            string[] ToEmailIDs = to.Split(',');
            int count = 0;
            foreach (string ToEMailId in ToEmailIDs)
            {
                bool isSendSuccess = SMTPHelper.SendMail(ToEMailId, body, subject, "","");
                if (isSendSuccess)
                    count++;
            }

            if (count > 0)
            {
              //  NewMessage.ShowNormalMessage(string.Format(Resources.Messages.SentMailSuccessMsg, count));
                return;
            }
           

        }
    }
}