﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL;
using BLL.Manager;
using Web.ReportDataSetTableAdapters;
using Web.Master;
using BLL;
using Web.CP.Report.Templates.Pay.Detail;
using Utils;
using BLL.Base;
using System.IO;
using Ext.Net;
using System.Text;
using Utils.Helper;
using BLL.BO;


namespace Web.Employee
{
    public partial class EmpAttTimeCommentRequestList : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialize();                
            }
        }

        private void Initialize()
        {
            Setting setting = OvertimeManager.GetSetting();

            if (setting.MultipleTimeRequestApproval != null && setting.MultipleTimeRequestApproval.Value)
            {
                lblApplyTo.Show();
                string review = "", approval = "";
                LeaveRequestManager.GetApplyToForEmployee(SessionManager.CurrentLoggedInEmployeeId, ref review, ref approval);
                lblApplyTo.Text = approval;
            }
            else
            {
                lblApplyTo.Hide();
            }

            BindGrids();
            if (!string.IsNullOrEmpty(Request.QueryString["linkreq"]))
            {
                if (Request.QueryString["linkreq"].ToString().ToLower() == "true")
                {
                    Clear();
                    eventWindow.Center();
                    eventWindow.Show();
                }
            }

        }

        private void BindGrids()
        {
            List<AttendanceEmpComment> list = AttendanceManager.GetTimeCommentRequestsByEmployeeId(SessionManager.CurrentLoggedInEmployeeId);
            gridPending.GetStore().DataSource = list;
            gridPending.GetStore().DataBind();
        }

        protected void btnShow_Click(object sender, DirectEventArgs e)
        {
            Clear();
            
            if (!string.IsNullOrEmpty(hdnRequestID.Text.Trim()))
            {
                txtStartDateAdd.Disabled = true;
                Guid requestID = new Guid(hdnRequestID.Text.Trim());
                AttendanceEmpComment _AttendanceEmpComment = AttendanceManager.GetTimeCommentRequestByRequestId(requestID);
                if (_AttendanceEmpComment != null)
                {
                    if (_AttendanceEmpComment.AttendanceDate != null)
                        txtStartDateAdd.SelectedDate = _AttendanceEmpComment.AttendanceDate.Value;
                    if (_AttendanceEmpComment.InTime != null)
                        txtInTime.Text = _AttendanceEmpComment.InTime.Value.ToString("hh:mm tt"); 
                    if (_AttendanceEmpComment.OutTime != null)
                        txtOutTime.Text = _AttendanceEmpComment.OutTime.Value.ToString("hh:mm tt"); 
                    if (!string.IsNullOrEmpty(_AttendanceEmpComment.InComment))
                        txtInComment.Text = _AttendanceEmpComment.InComment;
                    if (!string.IsNullOrEmpty(_AttendanceEmpComment.OutComment))
                        txtOutComment.Text = _AttendanceEmpComment.OutComment;
                }

                eventWindow.Center();
                eventWindow.Show();
            }
        }

        protected void btnSend_Click(object sender, DirectEventArgs e)
        {
            if (txtStartDateAdd.SelectedDate == txtStartDateAdd.EmptyDate)
            {
                NewMessage.ShowWarningMessage("Date is required.");
                return;
            }

            if (txtStartDateAdd.SelectedDate.Date > BLL.BaseBiz.GetCurrentDateAndTime().Date)
            {
                NewMessage.ShowWarningMessage("Future date is not allowed.");
                return;
            }
            if (string.IsNullOrEmpty(txtInComment.Text.Trim()) && string.IsNullOrEmpty(txtOutComment.Text.Trim()))
            {
                NewMessage.ShowWarningMessage("Comment is required.");
                return;
            }

           
            AttendanceEmpComment objTimeRequest = new AttendanceEmpComment();
            bool isSave = false;
            if (!string.IsNullOrEmpty(hdnRequestID.Text.Trim()))
                objTimeRequest.LineID = new Guid(hdnRequestID.Text.Trim());
            else
            {
                objTimeRequest.LineID = Guid.NewGuid();
                isSave = true;
            }

            if (isSave)
            {
                if (AttendanceManager.CheckTimeCommentRequestIsSaved(txtStartDateAdd.SelectedDate, SessionManager.CurrentLoggedInEmployeeId))
                {
                    NewMessage.ShowWarningMessage("Request already saved for the Date.");
                    return;
                }
            }


            objTimeRequest.EmpID = SessionManager.CurrentLoggedInEmployeeId;
            DateTime? Date = null;
            if (txtStartDateAdd.SelectedDate != txtStartDateAdd.EmptyDate)
            {
                objTimeRequest.AttendanceDate = txtStartDateAdd.SelectedDate;
                Date = txtStartDateAdd.SelectedDate;
            }
            objTimeRequest.InComment = txtInComment.Text;
            objTimeRequest.OutComment = txtOutComment.Text;
            AttendanceReportResult _timeForThisDay = AttendanceManager.GetEmployeeAttendanceTimeOFDay(Date.Value);
            if (_timeForThisDay != null)
            {
                if (_timeForThisDay.InTime != null)
                    objTimeRequest.InTime = _timeForThisDay.InTime;
                if (_timeForThisDay.OutTime != null)
                    objTimeRequest.OutTime = _timeForThisDay.OutTime;
            }

            objTimeRequest.Status = (int)AttendanceRequestStatusEnum.Saved;

            Setting setting = OvertimeManager.GetSetting();

            if (setting.MultipleTimeRequestApproval != null && setting.MultipleTimeRequestApproval.Value)
            {
                List<TextValue> listApproval = LeaveRequestManager.GetApplyToListForEmployee(SessionManager.CurrentLoggedInEmployeeId, true, PreDefindFlowType.Overtime);

                if (listApproval.Count > 0)
                {
                    if (listApproval.Count == 1)
                        objTimeRequest.ApprovalId1 = int.Parse(listApproval[0].Value);
                    else
                    {
                        objTimeRequest.ApprovalId1 = int.Parse(listApproval[0].Value);
                        objTimeRequest.ApprovalId2 = int.Parse(listApproval[1].Value);
                    }

                    // remove self approval from the list
                    if (objTimeRequest.ApprovalId1 == SessionManager.CurrentLoggedInEmployeeId)
                        objTimeRequest.ApprovalId1 = null;

                    if (objTimeRequest.ApprovalId2 == SessionManager.CurrentLoggedInEmployeeId)
                        objTimeRequest.ApprovalId2 = null;

                }
                else
                {
                    NewMessage.ShowWarningMessage("Approval is required.");
                    return;
                }
            }
            else
            {
                NewMessage.ShowWarningMessage("Approval is required.");
                return;
            }

            Status status = AttendanceManager.SaveTimeCommentRequest(objTimeRequest, isSave);
            if (status.IsSuccess)
            {
                if (isSave)
                    NewMessage.ShowNormalMessage("Record saved successfully.");
                else
                    NewMessage.ShowNormalMessage("Record updated successfully.");
                eventWindow.Close();
                BindGrids();
            }
            else
                NewMessage.ShowWarningMessage(status.ErrorMessage);
        }


        protected void btnDelete_Click(object sender, DirectEventArgs e)
        {
            Guid requestID = new Guid(hdnRequestID.Text.Trim());
            Status status = AttendanceManager.DeleteTimeCommentRequest(requestID);
            if (status.IsSuccess)
            {
                NewMessage.ShowNormalMessage("Record deleted successfully.");
                BindGrids();
            }
            else
                NewMessage.ShowWarningMessage(status.ErrorMessage);
        }


        protected void ShowWindow(object sender, DirectEventArgs e)
        {
            Clear();
            hdnRequestID.Clear();
            eventWindow.Center();
            eventWindow.Show();
        }
        protected void btnLoad_Click(object sender, DirectEventArgs e)
        {
            txtInTime.Clear();
            txtOutTime.Clear();

            DateTime Date = new DateTime();
            if (txtStartDateAdd.SelectedDate != new DateTime())
                Date = txtStartDateAdd.SelectedDate;
            else
            {
                NewMessage.ShowNormalMessage("Date is required.");
                return;
            }

            AttendanceReportResult _timeForThisDay = AttendanceManager.GetEmployeeAttendanceTimeOFDay(Date);
            if (_timeForThisDay != null)
            {
                if (_timeForThisDay.InTime != null)
                    txtInTime.Text = _timeForThisDay.InTime.Value.ToString("hh:mm tt"); 
                if (_timeForThisDay.OutTime != null)
                    txtOutTime.Text = _timeForThisDay.OutTime.Value.ToString("hh:mm tt"); 

                btnSend.Show();
                eventWindow.Center();
                eventWindow.Show();
            }
        }


        private void Clear()
        {
            txtStartDateAdd.Clear();
            txtInComment.Clear();
            txtOutComment.Clear();
            txtInTime.Clear();
            txtOutTime.Clear();
            txtStartDateAdd.Disabled = false;
            
        }
    
    }
}
