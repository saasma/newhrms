﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL.Entity;
using BLL;
using System.IO;
using BLL.BO;
using Utils.Helper;
using Bll;


namespace Web.Employee
{
    public partial class AssignOvertimePopup : System.Web.UI.Page
    {
        public bool HasImport
        {
            get
            {
                if (ViewState["import"] == null)
                    return false;
                return Convert.ToBoolean(ViewState["import"]);
            }
            set
            {
                ViewState["import"] = value;
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "sdf", "var hasImport = " + (this.HasImport ? "true;" : "false;"), true);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest && !IsPostBack)
            {
                Initialize();
            }

            JavascriptHelper.AttachPopUpCode(Page, "overtimeImp", "SupOTImport.aspx", 450, 500);
        }

        private void Initialize()
        {
            cmbOvertimeType.GetStore().DataSource = OvertimeManager.GetOvertimeList();
            cmbOvertimeType.GetStore().DataBind();

            cmbEmployeeAdd.GetStore().DataSource = LeaveAttendanceManager.GetEmployeeListForLeaveAssign();
            cmbEmployeeAdd.GetStore().DataBind();
        }

        protected void btnLoadGrid_Click(object sender, DirectEventArgs e)
        {
            if (Session["AssignOTImp"] != null)
            {
                List<OverTimeClsWithEmp> list =(List<OverTimeClsWithEmp>)Session["AssignOTImp"];

                if (list.Count > 0)
                {
                    DateTime dt = DateTime.Now;
                   
                    gridOTList.GetStore().DataSource = list;
                    gridOTList.GetStore().DataBind();
                }

            }
        }

        protected void btnAssignOvertime_Click(object sender, DirectEventArgs e)
        {
            if (string.IsNullOrEmpty(cmbOvertimeType.Text))
            {
                NewMessage.ShowWarningMessage("Overtime type id required.");
                btnAssignOvertime.Enable();                
                return;
            }

            Page.Validate("AssignOvertime");
            if (Page.IsValid)
            {
                int overtimeTpyeId = int.Parse(cmbOvertimeType.SelectedItem.Value);

                string jsonItems = e.ExtraParams["gridItems"];
                List<OverTimeClsWithEmp> timeRequestLines = JSON.Deserialize<List<OverTimeClsWithEmp>>(jsonItems);

                if (timeRequestLines.Count == 0)
                {
                    NewMessage.ShowWarningMessage("Please add/import overtime lists to be assigned.");
                    return;
                }

                List<OvertimeRequest> list = new List<OvertimeRequest>();
                int count = 1;

                foreach (var item in timeRequestLines)
                {
                    OvertimeRequest obj = new OvertimeRequest();

                    bool isEdit = false;
                    OvertimeRequest requestInstance = new OvertimeRequest();

                    requestInstance.OvertimeTypeId = overtimeTpyeId;

                    TimeSpan tsStartTime, tsEndTime;
                    DateTime dt;

                    if (item.DateEng != null && item.DateEng != new DateTime())
                    {
                        try
                        {
                            dt = item.DateEng;
                        }
                        catch (Exception ex)
                        {
                            NewMessage.ShowWarningMessage("Valid Date is required for the row " + count.ToString());
                            return;
                        }
                    }
                    else
                    {
                        NewMessage.ShowWarningMessage("Date is required for the row " + count.ToString());
                        return;
                    }


                    requestInstance.Date = dt;
                    if (!string.IsNullOrEmpty(item.Value))
                        requestInstance.EmployeeID = int.Parse(item.Value);
                    else
                    {
                        NewMessage.ShowWarningMessage("Please select employee for the row " + count.ToString());
                        return;
                    }

                    if (item.InTime != null && item.InTime != new DateTime())
                    {
                        tsStartTime = new TimeSpan(item.InTime.Hour, item.InTime.Minute, 0);
                        requestInstance.StartTime = new DateTime(dt.Year, dt.Month, dt.Day, tsStartTime.Hours, tsStartTime.Minutes, 0);
                    }
                    else
                    {
                        NewMessage.ShowWarningMessage("Start time is required for the row " + count.ToString());
                        return;
                    }

                    if (item.OutTime != null && item.OutTime != new DateTime())
                    {
                        tsEndTime = new TimeSpan(item.OutTime.Hour, item.OutTime.Minute, 0);
                        requestInstance.EndTime = new DateTime(dt.Year, dt.Month, dt.Day, tsEndTime.Hours, tsEndTime.Minutes, 0);
                    }
                    else
                    {
                        NewMessage.ShowWarningMessage("End time is required for the row " + count.ToString());
                        return;
                    }

                    if (string.IsNullOrEmpty(item.OutNote))
                    {
                        NewMessage.ShowWarningMessage("Reason is required for the row " + count.ToString());
                        return;
                    }
                    requestInstance.Reason = item.OutNote;

                    requestInstance.Status = (int)OvertimeStatusEnum.Approved;

                    requestInstance.ApprovalID = SessionManager.CurrentLoggedInEmployeeId;
                    requestInstance.ApprovalName = EmployeeManager.GetEmployeeById(SessionManager.CurrentLoggedInEmployeeId).Name;

                    ResponseStatus isSuccess = new ResponseStatus();

                    ResponseStatus isValidDate = OvertimeManager.isRequestedDateValid(requestInstance.EmployeeID, requestInstance.Date, requestInstance.StartTime, requestInstance.EndTime, isEdit);

                    if (!OvertimeManager.CanEmployeeRequestForOverTime(requestInstance.EmployeeID.Value, requestInstance.OvertimeTypeId.Value, requestInstance.Date.Value))
                    {
                        NewMessage.ShowWarningMessage("Employee is not eligible for " + cmbOvertimeType.SelectedItem.Text + " for the row " + count.ToString());
                        return;
                    }

                    int minimumOvertimeBuffer = OvertimeManager.GetOvertimeMinimumBufferTime(requestInstance.EmployeeID.Value, requestInstance.Date.Value);

                    if (minimumOvertimeBuffer != 0)
                    {
                        TimeSpan EndTimeSpan = OvertimeManager.getOfficeEndTime(SessionManager.CurrentLoggedInEmployeeId);

                        TimeSpan ts = tsStartTime - EndTimeSpan;

                        if (ts.TotalMinutes < minimumOvertimeBuffer)
                        {
                            NewMessage.ShowWarningMessage("You cannot assign overtime earlier than " + minimumOvertimeBuffer + " minutes from end of office time for the row " + count.ToString());
                            return;
                        }
                    }

                    requestInstance.NepaliDate = requestInstance.Date.Value.ToShortDateString().Replace('-', '/');

                    count++;

                    if (isValidDate.IsSuccessType)
                    {
                        list.Add(requestInstance);
                    }
                    else
                    {
                        NewMessage.ShowWarningMessage(isValidDate.ErrorMessage);
                        return;
                    }
                }

                Status status = OvertimeManager.AssignOvertimes(list);
                if (status.IsSuccess)
                {
                    NewMessage.ShowNormalMessage("Overtime requests assigned successfully.","closePopup()");
                }
                else
                {
                    NewMessage.ShowWarningMessage(status.ErrorMessage);
                }

            }
        }


    }
}