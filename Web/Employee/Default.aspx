﻿<%@ Page Language="C#" AutoEventWireup="true" Title="Rigo Payroll" CodeBehind="Default.aspx.cs"
    Inherits="Web.Employee.Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <style type="text/css">
        #bodypart
        {
            background-color: #a7c2db !important;
        }
        #wrapper
        {
            background-color: #a7c2db !important;
        }
        .submit
        {
            text-align: center !important;
            font-weight: bold;
        }
    </style>
    <link rel="stylesheet" href="../css/core.css" type="text/css" />
    <script type="text/javascript">
       
        function preprocess() {
            
            var hash = "";
            if (window.location.hash.toString() != "") {
                hash = window.location.hash.toString().replace("#", "");
                if (hash != null) {
                    <%= hiddenLeaveRequestID.ClientID %>.value = hash;
                }
            }

            
        }
    </script>
</head>
<body id="wrapper">
    <form id="form1" runat="server">
    <div id="login_header">
        <div class="header">
            <div class="login_head">
                Login Authentication
            </div>
        </div>
    </div>
    <div id="sec_nav">
    </div>
    <asp:HiddenField ID="hiddenLeaveRequestID" runat="server" Value="" />
    <div id="bodypart" style="min-height: 580px">
        <div class="login_part">
            <div class="right login_header" style="margin-top: 40px;">
                &nbsp;
                <img id="imgCompanyLogo" runat="server" src="../images/company_logo.png" alt=" Company Logo"
                    style="" />
            </div>
            <asp:Label ID="lblDemoMsg" runat="server" EnableViewState="true" CssClass="invalid"
                ForeColor="Red"></asp:Label>
            <asp:Label ID="lblMsg" runat="server" EnableViewState="False" CssClass="invalid"
                ForeColor="Red"></asp:Label>
            <asp:Panel runat="server" ID="Panel1" DefaultButton="btnLogin" class="main_login roundIE">
                <!--<div style="background-color:#1F8AC9; padding:0px 20px 20px 10px;">-->
                <table>
                    <tr style="color: white;">
                        <td colspan="2" style="font-style: normal; font-weight: bolder; font-size: 25px;
                            padding-bottom: 20px">
                            Employee Log In
                        </td>
                        <td colspan="4">
                        </td>
                    </tr>
                    <tr>
                        <td style="color: white">
                            Login ID
                        </td>
                        <td>
                            <asp:TextBox ID="txtUserName" runat="server" Width="143px"></asp:TextBox>
                        </td>
                        <td style="width: 5px">
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtUserName"
                                Display="Dynamic" ErrorMessage="RequiredFieldValidator" ToolTip="User name is required.">*</asp:RequiredFieldValidator>
                        </td>
                        <td style="color: white">
                            Password
                        </td>
                        <td>
                            <asp:TextBox AutoComplete="off" ID="txtPassword" TextMode="Password" runat="server"
                                Width="141px"></asp:TextBox>
                        </td>
                        <td style="width: 10px">
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtPassword"
                                Display="Dynamic" ErrorMessage="RequiredFieldValidator" ToolTip="Password is required.">*</asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                        </td>
                        <td style="float: right">
                            <asp:Button ID="btnLogin" Text="Login" class="submit" OnClientClick="preprocess();"
                                OnClick="btnLogin_Click" runat="server" />
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6" style="text-align: right; padding-right: 17px; padding-top: 15px;">
                            <a runat="server" id="linkForgotPwd" href="~/Employee/ForgotPassword.aspx" style="color: white">
                                Forgot Password</a>
                        </td>
                    </tr>
                </table>
                <!--</div>-->
            </asp:Panel>
            <span class="right"><a id="A1" runat="server" href="~/Employee/Default.aspx">
                <%--   <img src="../images/sec_logo.png" alt="Rigo Logo" style="margin-left: 120px; margin-top: 0px;
                    padding-top: 0px" />
                --%> </span></a>
        </div>
    </div>
    </form>
    <div style="position: fixed; color: white; left: 0px; bottom: 0px; height: 45px;
        width: 100%; text-align: center; padding-bottom: 10px; padding-top: 10px; background-color: #355D81;
        border-top: thin #1F8AC9 outset;">
        &nbsp;<br />
        <a class="anchorC" href="http://rigonepal.com">Rigo Technologies </a>© 2015</strong>
        (<asp:Label runat="server" ID="version" />) &nbsp;<br />
        &nbsp;<br />
    </div>
    <script type="text/javascript" src="../Scripts/jquery-1.3.2.min.js"></script>
    <%--  <script>
        var $buoop = { vs: { i: 9, f: 25, o: 17, s: 6, c: 30} }; 
        function $buo_f() {
            var e = document.createElement("script");
            e.src = "../scripts/update.js";
            document.body.appendChild(e);
        };
        try { document.addEventListener("DOMContentLoaded", $buo_f, false) }
        catch (e) { window.attachEvent("onload", $buo_f) }
</script> --%>
</body>
<style type="text/css">
    .anchorC
    {
        color: White;
    }
    .anchorC:hover
    {
        color: White;
    }
    #bodypart
    {
        background-color: #a7c2db !important;
    }
    #wrapper
    {
        background-color: #a7c2db !important;
    }
</style>
</html>
