﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Manager;
using DAL;
using BLL;
using Utils.Security;
using System.Web.Security;
using System.Data.Linq;
using Utils;
using System.IO;
using Web.Helper;
namespace Web.Employee
{

  
    public partial class Default : System.Web.UI.Page
    {
        public bool IsLocalNetwork()
        {
            string[] IPs = Config.ActiveDirectorySSOIP.Split(new char[] { ',' });
            string userIP = Request.UserHostAddress;

            foreach (string serverIP in IPs)
            {
                if (userIP.StartsWith(serverIP))// && Request.Browser.Browser.ToLower().Equals("ie"))
                {
                    return true;
                }
            }

            if (this.Request.IsLocal)
                return true;
            return false;
        }

        //private bool CheckAndAutoLoginAsADUser()
        //{
        //    //Response.Write("Is Local Net  : " + IsLocalNetwork());

        //    //Response.Write("LOGON_USER  : " + this.Request.Params["LOGON_USER"]);

        //    if (this.Request.IsAuthenticated == false)
        //    {

        //        string name = this.Request.Params["LOGON_USER"];

                

        //        if (string.IsNullOrEmpty(name) == false)
        //        {
        //            ProcessWindowAuthLogin(name);
        //        }
        //        else
        //        {
        //            //don't reuest if not coming then must be configured at the client side
        //            Response.StatusCode = 401;
        //            Response.StatusDescription = "Unauthorized";
        //            //Response.End();
        //            return false;
        //        }

        //    }
        //    return true;
        //}

        protected void ProcessWindowAuthLogin(string name)
        {
            string username ="";
            if (name.Contains("\\"))
                username = name.Substring(name.LastIndexOf("\\") + 1);
            else
                username = name;

            //Response.Write("<br>LOGON_USER  : " + username);


            // ProcessFlow.AccountController accountController = new ProcessFlow.AccountController();
            string role = "";
            int roleId = 0;
            string systemuserName = UserManager.ProcessWindowsAuthLogin(username, ref role, ref roleId);
            //Response.Write("<br>System LOGON_USER  : " + systemuserName);

            if (systemuserName != "")
            {

                SuccessLogin(systemuserName, role, roleId,true);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

            Response.Redirect("~/Default.aspx");
            return;

   //         if (CommonManager.CompanySetting.WhichCompany == WhichCompany.Care)
   //         {
   //             this.Page.Title = "Care HRIS";
   //         }
         
   //         if (User.Identity.IsAuthenticated)
   //         {
   //             //Checck for Windows authentication
   //             if (User.Identity.Name.Contains("\\"))
   //                 ProcessWindowAuthLogin(User.Identity.Name);
   //             else
   //             {
   //                  string role = "";
   //                 int roleId = 0;
   //                 if (new UserManager().SetUserInfo(User.Identity.Name, ref role, ref roleId))
   //                 {
   //                     SuccessLogin(User.Identity.Name, role, roleId, SessionManager.IsLDAPLogin );
   //                 }
   //             }
   //         }

   //         version.Text = WebHelper.Version.ToString();

   //         // if LDAP is enabled in license or (no license and has enabled LDAP)
   //         //if (LdapHelper.IsLDAPAccessible())
   //         //    CheckAndAutoLoginAsADUser();

   //         //For Load Testing
   //         //SecurityHelper.CreateCookie("hari", false, "Employee", false, false);
   //         //Response.Redirect("~/Employee/PersonalDetails.aspx");


   //         // if LDAP is enabled in license or (no license and has enabled LDAP)
   //         if (LdapHelper.IsLDAPAccessible())
   //             linkForgotPwd.Visible = false;

   //         if (!IsPostBack)
   //         {
   //             if (!DemoHelper.IsValidInstallation())
   //             {
   //                 btnLogin.Visible = false;
   //                 lblDemoMsg.Text = Resources.Messages.DemoInvalidLicenseMsg;
   //                 lblDemoMsg.Style.Add("display", "block");
   //                 return;
   //             }

   //             int remDays = DemoHelper.GetRemainingDays();
   //             if (remDays != 0)
   //             {
   //                 if (remDays == -1)
   //                 {
   //                     btnLogin.Visible = false;
   //                     lblDemoMsg.Text = Resources.Messages.DemoExpiredMsg;
   //                     lblDemoMsg.Style.Add("display", "block");
   //                 }
   //                 else
   //                 {
   //                     lblDemoMsg.Text = string.Format(Resources.Messages.DemoRemainingDays,remDays);
   //                     lblDemoMsg.Style.Add("display", "block");
   //                 }
   //             }
   //         }
			////check if logo exists or not
   //         string loc = Server.MapPath(imgCompanyLogo.Src);
   //         if (!File.Exists(loc))
   //             imgCompanyLogo.Visible = false;
        }

        public string GetClientIP()
        {
            HttpRequest currentRequest = HttpContext.Current.Request;
            string ipAddress = currentRequest.ServerVariables["HTTP_X_FORWARDED_FOR"];

            if (ipAddress == null || ipAddress.ToLower() == "unknown")
                ipAddress = currentRequest.ServerVariables["REMOTE_ADDR"];

            return ipAddress;
        }


        protected void btnLogin_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                string pwd = (txtPassword.Text.Trim());
                string userName = txtUserName.Text.Trim();
                Process(userName, pwd);
            }
        }

        public bool IsIPAllowableToAccess(string ipList)
        {
            string[] IPs = ipList.Split(new char[] { ',' });
            string userIP = GetClientIP();

            foreach (string serverIP in IPs)
            {
                if (userIP.StartsWith(serverIP))// && Request.Browser.Browser.ToLower().Equals("ie"))
                {
                    return true;
                }
            }

            if (this.Request.IsLocal)
                return true;
            return false;
        }
        protected void Process(string username,string pwd)
        {
            UserManager mgr = new UserManager();
            string role = "";
            UUser user = null;
            int roleId = 0;

            user = UserManager.GetUserByUserName(username, true);
            if (user != null && !string.IsNullOrEmpty(user.AllowableIPList))
            {
                //if not not in the allowable IP list then don't allow to login
                if (!IsIPAllowableToAccess(user.AllowableIPList))
                {
                    BLL.BaseBiz.PayrollDataContext.ChangeUserActivities.InsertOnSubmit(
                    BLL.BaseBiz.GetChangeUserActivityLog("Logging fail for user " + username + " from IP " + GetClientIP() + ", access from inaccessible IP address.",
                        username));
                    BLL.BaseBiz.PayrollDataContext.SubmitChanges();


                    lblMsg.Style.Add("display", "block");
                    lblMsg.Text = "Inaccessible IP address.";
                    return;
                }

            }

            string remTime = "";
            if (BrutalForceAttackPrevention.IsAcccountLocked(username, ref remTime))
            {
                BLL.BaseBiz.PayrollDataContext.ChangeUserActivities.InsertOnSubmit(
                  BLL.BaseBiz.GetChangeUserActivityLog("Locked account access for " + username + " from IP " + GetClientIP() + ".", username));
                BLL.BaseBiz.PayrollDataContext.SubmitChanges();


                lblMsg.Style.Add("display", "block");
                lblMsg.Text = "Account is locked for " + remTime + " mins. Please try after sometime.";
                return;
            }

            bool isEmpRetired = false;
            var isEmpPortalDisabled = false;
            if (mgr.ValidateUser(username, pwd, ref role, ref roleId, ref user, true, ref isEmpRetired,ref isEmpPortalDisabled))
            {

                BrutalForceAttackPrevention.ClearLockedForUserName(username);

                SuccessLogin(username, role, roleId,false);
            }
            else
            {
                if (isEmpPortalDisabled)
                {
                    lblMsg.Style.Add("display", "block");
                    lblMsg.Text = "Employee portal login has been disabled, please contact HR for details.";
                    return;
                }

                BrutalForceAttackPrevention.IncrementInvalidPasswordLockTimes(username);


                BLL.BaseBiz.PayrollDataContext.ChangeUserActivities.InsertOnSubmit(
                    BLL.BaseBiz.GetChangeUserActivityLog("Logging fail for user " + username + " from IP " + GetClientIP() + "."));
                BLL.BaseBiz.PayrollDataContext.SubmitChanges();

                Log.log("Employee Logging fail for user " + username + " at with " + DateTime.Now, "");

                if (isEmpRetired)
                {
                    lblMsg.Style.Add("display", "block");
                    lblMsg.Text = "Employee already retired.";
                }
                else
                {
                    lblMsg.Style.Add("display", "block");
                    lblMsg.Text = "Invalid user name or password.";
                }

                //if trying to login from empoloyee then rediect to actual login page
                //if (UserManager.IsNonEmployeeUserName(username))
                //{
                //    string code = "setTimeout(function() {window.location = '../Default.aspx';},3000);";
                //    lblMsg.Text = "User can not login from this page, redirecting to user login.";
                //    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "sdfsdf23",
                //        code, true);
                //    //return;
                //}
            }

        }

        protected void SuccessLogin(string username,string role,int roleId,bool isADLogin)
        {
            BLL.BaseBiz.PayrollDataContext.ChangeUserActivities.InsertOnSubmit(
                  BLL.BaseBiz.GetChangeUserActivityLog("User logged in " + username + " from IP " + GetClientIP() + "."));
            BLL.BaseBiz.PayrollDataContext.SubmitChanges();

            Log.log("User logged in " + username + " at " + DateTime.Now, "");

            SecurityHelper.CreateCookie(username,
        true, role, false, isADLogin);

            Role userRole = (Role)roleId;

            //If no default company as happens at starting, then redirect to
            //AECompany to create new one
            if (SessionManager.CurrentCompanyId == 0)
            {
                Response.Redirect("~/CP/AddEditCompany.aspx", true);
                return;
            }

            switch (userRole)
            {
                case Role.Administrator:

                    Response.Redirect("~/CP/Dashboard.aspx", true);
                    break;
                case Role.Employee:

                    int leaveRequestId = 0;
                    // if has leave request id redirect to leave approval
                    if (int.TryParse(hiddenLeaveRequestID.Value, out leaveRequestId))
                    {
                        Response.Redirect("~/Employee/LeaveApproval.aspx#" + leaveRequestId,true);
                        return;
                    }

                    //if (Request.QueryString["ReturnUrl"] != null)
                    //    Response.Redirect(Request.QueryString["ReturnUrl"]);
                    //else
                    {
                        if (CommonManager.CompanySetting.WhichCompany == WhichCompany.AceTravels)
                        {
                            Response.Redirect("~/Employee/PersonalDetails.aspx", true);
                        }
                        else
                            Response.Redirect("~/Employee/Dashboard.aspx", true);
                    }
                    break;
                default:
                    Response.Redirect("~/newhr/DashboardHR.aspx", true);
                    ////for other custom role user, redirect to the first accessible page
                    //UUser user = UserManager.GetUserByUserName(username);
                    //if (user != null)
                    //{
                    //    string[] pages = UserManager.GetAccessiblePagesForRole(user.RoleId.Value);
                    //    if (pages.Length > 0)
                    //    {
                    //        //find PageUrl not null page
                    //        foreach (int pageId in pages)
                    //        {

                    //            UPage page = UserManager.GetPageById(pageId);
                    //            if (page.PageUrl != null)
                    //                Response.Redirect("~/" + page.PageUrl, true);
                    //        }
                    //    }
                    //    else
                    //    {

                    //        lblMsg.Style.Add("display", "block");
                    //        lblMsg.Text = "Page(s) is not assigned to the associated role of the current user.";
                    //    }
                    //}
                    break;
            }
        }
        protected void ReturnUrl()
        {
            this.Response.Redirect("~/Employee/Default.aspx" +
               "?ReturnUrl=" + this.Request.Url.PathAndQuery);
        }
    }
}
