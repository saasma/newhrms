﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//using Web.CP.Report.Templates;

using BLL;
using DevExpress.XtraReports.UI;
using Web.ReportDataSetTableAdapters;
using Utils.Calendar;
using BLL.Base;
using BLL.Manager;
using DAL;
using BLL.BO;
using System.Data;
using DevExpress.XtraPrinting;

namespace Web.CP.Report
{
    public partial class LeaveSummary : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            report.Filter.PayrollTo = true;
            report.Filter.Leave = true;
            report.Filter.Department = false;
            report.Filter.Branch = false;
            report.Filter.Employee = false;
            report.Filter.Leave = false;

            //if (IsPostBack)
                LoadReport();

        }
        protected void Page_PreRender(object sender, EventArgs e)
        {
            //if (!IsPostBack)
            //    report.Filter.SelectLastPayroll();
        }

        protected void LoadReport()
        {

            //if (!IsPostBack)
            //    return;

            PayrollPeriod payrollPeriod = CommonManager.GetPayrollPeriod(report.Filter.StartDate.Month,
                                                                      report.Filter.StartDate.Year);
            PayrollPeriod payrollPeriodTo = CommonManager.GetPayrollPeriod(report.Filter.EndDate.Month,
                                                                                     report.Filter.EndDate.Year);
            //if null get first payroll period
            if (payrollPeriod == null)
                payrollPeriod = CommonManager.GetFirstPayrollPeriod(SessionManager.CurrentCompanyId);

            if (payrollPeriodTo == null)
                payrollPeriodTo = CommonManager.GetLastPayrollPeriod();

            //again null then no report
            if (payrollPeriod == null || payrollPeriodTo == null)
                return;

            //for report to date should be greater or equal to from date
            if (payrollPeriodTo.PayrollPeriodId < payrollPeriod.PayrollPeriodId)
                return;


            //ReportDataSetTableAdapters.Report_HR_LeaveBalanceTableAdapter
            //    adap = new Report_HR_LeaveBalanceTableAdapter();

            ReportDataSetTableAdapters.Report_HR_LeaveBalanceMonthsTableAdapter
                adap = new Report_HR_LeaveBalanceMonthsTableAdapter();

            BLL.BaseBiz.SetConnectionPwd(adap.Connection);

            ReportDataSet.Report_HR_LeaveBalanceMonthsDataTable data = adap.GetData(
               payrollPeriod.Month, payrollPeriod.Year, payrollPeriodTo.Month, payrollPeriodTo.Year, SessionManager.CurrentCompanyId,
              report.Filter.EmployeeName, report.Filter.LeaveTypeId, report.Filter.BranchId, report.Filter.DepartmentId, report.Filter.SubDepartmentId,
              SessionManager.CurrentLoggedInEmployeeId, report.Filter.DepartmentName, SessionManager.CurrentLoggedInEmployeeId != 0);

            GroupChildLeave(data);

          
            if (CommonManager.CompanySetting.WhichCompany == WhichCompany.NIBL)
            {
                Report.Templates.HR.ReportLeaveBalanceWithOutEncashColumn report1 = new Report.Templates.HR.ReportLeaveBalanceWithOutEncashColumn();
                //report1.cellEIN1.Visible = false;
                //report1.cellEIN2.Visible = false;
                //report1.cellEIN3.Visible = false;
                report1.DataSource = data;
                report1.DataMember = "Report";
                report1.labelTitle.Text = "Leave Summary from ";

                report1.labelTitle.Text +=
                       string.Format("{0}/{1} ({4}) to {2}/{3} ({5})",
                       DateHelper.GetMonthShortName(payrollPeriod.Month, IsEnglish),
                       payrollPeriod.Year,
                       DateHelper.GetMonthShortName(payrollPeriodTo.Month, IsEnglish),
                        payrollPeriodTo.Year,
                       payrollPeriod.StartDateEng.Value.ToString("yyyy-MMM-dd"),
                       payrollPeriodTo.EndDateEng.Value.ToString("yyyy-MMM-dd")
                       );

                report.DisplayReport(report1);
            }
            else
            {
                Report.Templates.HR.ReportLeaveBalance report1 = new Report.Templates.HR.ReportLeaveBalance();
                //report1.cellEIN1.Visible = false;
                //report1.cellEIN2.Visible = false;
                //report1.cellEIN3.Visible = false;
                report1.DataSource = data;
                report1.DataMember = "Report";
                report1.labelTitle.Text = "Leave Summary from ";

                report1.labelTitle.Text +=
                       string.Format("{0}/{1} ({4}) to {2}/{3} ({5})",
                       DateHelper.GetMonthShortName(payrollPeriod.Month, IsEnglish),
                       payrollPeriod.Year,
                       DateHelper.GetMonthShortName(payrollPeriodTo.Month, IsEnglish),
                        payrollPeriodTo.Year,
                       payrollPeriod.StartDateEng.Value.ToString("yyyy-MMM-dd"),
                       payrollPeriodTo.EndDateEng.Value.ToString("yyyy-MMM-dd")
                       );

                report.DisplayReport(report1);
            }

        }

        void GroupChildLeave(ReportDataSet.Report_HR_LeaveBalanceMonthsDataTable data)
        {
            if (BLL.BaseBiz.PayrollDataContext.LLeaveTypes.Any(x => x.IsParentGroupLeave != null && x.IsParentGroupLeave.Value))
            {
                //ReportDataSet.Report_HR_LeaveBalanceMonthsDataTable newData = new ReportDataSet.Report_HR_LeaveBalanceMonthsDataTable();
                List<LLeaveType> leaves = LeaveAttendanceManager.GetAllLeaves(SessionManager.CurrentCompanyId);



                LLeaveType parentLeave = leaves.FirstOrDefault(x => x.IsParentGroupLeave != null && x.IsParentGroupLeave.Value);

                ReportDataSet.Report_HR_LeaveBalanceMonthsRow parentRow = data.NewReport_HR_LeaveBalanceMonthsRow();

                parentRow.LeaveTypeId = parentLeave.LeaveTypeId;
                parentRow.Title = parentLeave.Title;

                bool hasChild = false;  

                foreach (ReportDataSet.Report_HR_LeaveBalanceMonthsRow item in data.Rows)
                {
                    LLeaveType currentLeave = leaves.FirstOrDefault(x => x.LeaveTypeId == item.LeaveTypeId);
                    if (currentLeave != null && currentLeave.ParentLeaveTypeId == parentLeave.LeaveTypeId)
                    {
                        hasChild = true;

                        parentRow.EIN = item.EIN;
                        parentRow.Name = item.Name;
                        parentRow.INo = item.INo == null ? "" : item.INo;

                        if (parentRow.IsBFNull())
                            parentRow.BF = 0;

                        parentRow.BF += item.BF;

                        if (parentRow.IsEarnedNull())
                            parentRow.Earned = 0;

                        parentRow.Earned += item.Earned;

                        if (parentRow.IsTotalNull())
                            parentRow.Total = 0;

                        parentRow.Total += item.Total;

                        if (parentRow.IsTakenNull())
                            parentRow.Taken = 0;

                        parentRow.Taken += item.Taken;

                        if (parentRow.IsLapsedNull())
                            parentRow.Lapsed = 0;

                        parentRow.Lapsed += item.Lapsed;

                        if (parentRow.IsManualAdjustmentNull())
                            parentRow.ManualAdjustment = 0;

                        parentRow.ManualAdjustment += item.ManualAdjustment;

                        if (parentRow.IsBalanceNull())
                            parentRow.Balance = 0;

                        parentRow.Balance += item.Balance;
                    }

                }

                for (int i = data.Rows.Count - 1; i >= 0; i--)
                {
                    ReportDataSet.Report_HR_LeaveBalanceMonthsRow row =
                        data.Rows[i] as ReportDataSet.Report_HR_LeaveBalanceMonthsRow;


                    if (row.LeaveTypeId == parentLeave.LeaveTypeId)
                    {
                        data.Rows.RemoveAt(i);
                        continue;
                    }

                    if (leaves.Any(x => x.LeaveTypeId == row.LeaveTypeId && x.IsChildLeave != null && x.IsChildLeave.Value))
                    {
                        data.Rows.RemoveAt(i);
                        
                    }
                }


                if(hasChild)
                data.Rows.InsertAt(parentRow, 0);

            }

        }

    }
}
