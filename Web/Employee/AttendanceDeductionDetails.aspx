﻿<%@ Page Title="Attendance Deduction Details Report" Language="C#" MasterPageFile="~/Master/EmployeeExt.Master"
    AutoEventWireup="true" CodeBehind="AttendanceDeductionDetails.aspx.cs" Inherits="Web.Employee.AttendanceDeductionDetails" %>

<%@ MasterType VirtualPath="~/Master/NewDetails.Master" %>
<%@ Register Src="~/newhr/UserControls/AttendanceDeductionDetailsCtl.ascx" TagName="AttendanceDeductionReportCtl"
    TagPrefix="uc" %>
<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="mainContent" runat="server">
    
    <div class="contentpanel" style="margin-top: 0px !important; padding-top: 0px !important;">
        <div class="innerLR">
              <h4>
                    Attendance Deduction Details Report
                </h4>
            <uc:AttendanceDeductionReportCtl Id="AttendanceDeductionReportCtl1" PageView="ManagerEmployee" runat="server" />
        </div>
    </div>
</asp:Content>
