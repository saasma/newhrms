<%@ Page Title="Employee CV" Language="C#" MaintainScrollPositionOnPostback="true"
    MasterPageFile="~/Master/EmployeeMS.Master" AutoEventWireup="true" CodeBehind="EmployeeCV.aspx.cs"
    Inherits="Web.CP.Report.EmployeeCV" %>

<%@ Register Assembly="DevExpress.Web.v16.1, Version=16.1.8.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.XtraReports.v16.1.Web, Version=16.1.8.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.XtraReports.Web" TagPrefix="dxxr" %>
<%@ Register Src="~/Controls/Report/ReportFilterBranchDep.ascx" TagName="ReportFilterBranchDep"
    TagPrefix="uc1" %>
<%@ Register Assembly="DevExpress.XtraReports.v16.1.Web, Version=16.1.8.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.XtraReports.Web" TagPrefix="dxxr" %>
<%@ Register Src="~/Controls/Report/ReportFilterBranchDep.ascx" TagName="ReportFilter"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
    .details a:hover
    {
    	background-color:lightgray;
    }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
    <div class="contentArea">
     
            <uc1:ReportFilter runat="server" id="rptFilter" />
            
         
       
                    <div class="clear" style="">
                        <dxxr:ReportToolbar CssClass="toolbarContainer" ID="ReportToolbar1" ReportViewerID="rptViewer"
                            runat="server" ShowDefaultButtons="False" Width="100%" >
                            <Images SpriteCssFilePath="~/App_Themes/Aqua/{0}/sprite.css">
                            </Images>
                            <Styles CssFilePath="~/App_Themes/Aqua/{0}/styles.css" CssPostfix="Aqua">
                                <LabelStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                <ButtonStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                </ButtonStyle>
                                <EditorStyle>
                                    <Paddings PaddingBottom="1px" PaddingLeft="3px" PaddingRight="3px" PaddingTop="2px" />
                                </EditorStyle>
                            </Styles>
                            <Items>
                                <dxxr:ReportToolbarButton ItemKind="Search" />
                                <dxxr:ReportToolbarSeparator />
                                <dxxr:ReportToolbarButton ItemKind="PrintReport" />
                                <dxxr:ReportToolbarButton ItemKind="PrintPage" />
                                <dxxr:ReportToolbarSeparator />
                                <dxxr:ReportToolbarButton Enabled="False" ItemKind="FirstPage" />
                                <dxxr:ReportToolbarButton Enabled="False" ItemKind="PreviousPage" />
                                <dxxr:ReportToolbarLabel ItemKind="PageLabel" />
                                <dxxr:ReportToolbarComboBox ItemKind="PageNumber" Width="65px">
                                </dxxr:ReportToolbarComboBox>
                                <dxxr:ReportToolbarLabel ItemKind="OfLabel" />
                                <dxxr:ReportToolbarTextBox ItemKind="PageCount" />
                                <dxxr:ReportToolbarButton ItemKind="NextPage" />
                                <dxxr:ReportToolbarButton ItemKind="LastPage" />
                                <dxxr:ReportToolbarSeparator />
                                <dxxr:ReportToolbarButton ItemKind="SaveToDisk" />
                                <dxxr:ReportToolbarButton ItemKind="SaveToWindow" />
                                <dxxr:ReportToolbarComboBox ItemKind="SaveFormat" Width="70px">
                                    <Elements>
                                        <dxxr:ListElement Value="pdf" />
                                        <dxxr:ListElement Value="xls" />
                                        <dxxr:ListElement Value="xlsx" />
                                        <dxxr:ListElement Value="rtf" />
                                        <dxxr:ListElement Value="mht" />
                                        <dxxr:ListElement Value="html" />
                                        <dxxr:ListElement Value="txt" />
                                        <dxxr:ListElement Value="csv" />
                                        <dxxr:ListElement Value="png" />
                                    </Elements>
                                </dxxr:ReportToolbarComboBox>
                            </Items>
                        </dxxr:ReportToolbar>
                        <dxxr:ReportViewer BackColor="White" OnRestoreReportDocumentFromCache="ReportViewer1_RestoreReportDocumentFromCache"
                            OnCacheReportDocument="ReportViewer1_CacheReportDocument" ClientInstanceName="ReportViewer1"
                            ID="rptViewer" runat="server" CssFilePath="~/App_Themes/Aqua/{0}/styles.css"
                            CssPostfix="Aqua" AutoSize="false" Height="1170px" Width="100%" LoadingPanelText=""
                            SpriteCssFilePath="~/App_Themes/Aqua/{0}/sprite.css">
                          
                            <Border BorderColor="#C1C1C1" BorderStyle="Solid" BorderWidth="0px" />
                            <LoadingPanelStyle ForeColor="#303030">
                            </LoadingPanelStyle>
                            <Paddings Padding="10px" PaddingLeft="55px" PaddingBottom="5px" PaddingTop="35px" />
                        </dxxr:ReportViewer>
                    </div>
              
    </div>
</asp:Content>
