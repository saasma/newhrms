﻿<%@ Page Title="Dashboard" Language="C#" MasterPageFile="~/Master/EmployeeMS.Master"
    AutoEventWireup="true" CodeBehind="DashboardReviewed.aspx.cs" Inherits="Web.Employee.DashboardReviewed" %>

<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Employee/UserControls/AssignLeave.ascx" TagName="AssingLeave"
    TagPrefix="uc5" %>
<%@ Register Src="~/Employee/UserControls/TimeRequestApprovalCtrl.ascx" TagName="TimeReqCtrl" TagPrefix="ucTR" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .details tr td
        {
            padding-top: 4px;
        }
        .leaveList
        {
            list-style-type: none; /*border: 2px solid #BFBFBF;
            background-color: #D0CECE;*/
            display: inline-block;
            padding: 10px;
        }
        .leaveList li
        {
            border: 1px solid #DEDEDE;
            background-color: #F2F2F2;
            float: left;
            padding: 5px;
            padding-left: 8px;
            padding-right: 8px;
            text-align: center;
            margin-bottom: 10px;
            margin-right: 10px;
        }
        
        .noticeContainer td
        {
            padding: 0px;
        }
        .blockTitle
        {
            color: #428BCA; /*text-transform: uppercase;*/
            font-size: 14px;
            font-weight: bold;
            padding-left: 2px;
        }
        .x-grid-body
        {
            border-top-color: white;
        }
        .blockText
        {
            color: #305496;
        }
        .blockBottomBorder
        {
            border-bottom: 1px solid #D9E7F3;
            padding-bottom: 1px;
        }
        .weekBlock
        {
            background-color: #DEDEDE;
            display: inline-block;
            padding: 4px;
            margin-top: 10px;
            padding-left: 8px;
            padding-right: 50px;
            color: #333333;
            margin-left: 90px;
        }
        
        
        .submitAttendance
        {
            color: #ffffff;
            font-size: 14px;
            background-position: left;
            background-repeat: no-repeat;
            background-color: #5CB85C;
            padding: 10px 5px 10px 20px;
            text-decoration: none;
            width: 100px;
            margin-right: 5px;
            margin-left: 5px;
            padding-left: 30px;
            padding-right: 30px;
        }
        
        .submitAttendance:hover
        {
            color: #ffffff;
            font-size: 14px;
            background-position: left;
            background-repeat: no-repeat;
            background-color: #71DB8E;
            padding: 10px 5px 10px 20px;
            text-decoration: none;
            width: 100px;
            margin-right: 5px;
            margin-left: 5px;
            padding-left: 30px;
            padding-right: 30px;
        }
        
        .searchEmployee
        {
            color: #ffffff;
            font-size: 14px;
            background: url('images/searchEmployee.png');
            background-position: left;
            background-repeat: no-repeat;
            background-color: #5CB85C;
            padding: 10px 5px 10px 20px;
            text-decoration: none;
            width: 100px;
            margin-right: 5px;
            margin-left: 5px;
            padding-left: 32px;
            background-size: 27px;
        }
        
        .leaveList td
        {
            padding-bottom: 8px;
        }
        
        .searchEmployee:hover
        {
            color: #ffffff;
            font-size: 14px;
            background: url('images/searchEmployee.png');
            background-position: left;
            background-repeat: no-repeat;
            background-color: #71DB8E;
            padding: 10px 5px 10px 20px;
            text-decoration: none;
            width: 100px;
            margin-right: 5px;
            margin-left: 5px;
            padding-left: 32px;
            background-size: 27px;
        }
        .scheduleLeave
        {
            color: #ffffff;
            font-size: 14px;
            background-position: left;
            background-repeat: no-repeat;
            background-color: #3498db;
            padding: 10px 5px 10px 20px;
            text-decoration: none;
            width: 100px;
            margin-right: 5px;
            margin-left: 5px;
            padding-left: 30px;
            padding-right: 30px;
        }
        
        .scheduleLeave:hover
        {
            color: #ffffff;
            font-size: 14px;
            background-position: left;
            background-repeat: no-repeat;
            background-color: #3cb0fd;
            padding: 10px 5px 10px 20px;
            text-decoration: none;
            width: 100px;
            margin-right: 5px;
            margin-left: 5px;
            padding-left: 30px;
            padding-right: 30px;
        }
        
        .widget .widget-body.list ul li
        {
            line-height: inherit !important;
        }
        td
        {
            padding: 2px;
        }
        
        .activityClass
        {
            width: 300px;
            height: 25px;
            float: left;
            padding-top: 5px;
            font-weight: bold;
            padding-left: 5px;
            background-color: #5395CE;
            color: White;
            margin-top: 10px;
        }
        .bottomBlock
        {
            clear: both;
            padding: 0 0 10px 10px;
            background: #D7E7FF;
            float: left;
            width: 400px;
            margin-left: -5px;
            margin-top: 20px;
        }
        .lblClass
        {
            font-weight: bold;
            color: Red;
            margin-top: 10px;
        }
        
        .divBlockCls
        {
            background-color:#EFEFEF;
            padding:7px;
        }
       
       #bodypart
       {
           background-color:whitesmoke !important;
       }
       
       .row-imagecommand span {
        padding: 0 9px 0 10px !important;
        line-height: 16px !important;
        vertical-align: middle !important;
        white-space: nowrap !important;
    }
    </style>
    <script type="text/javascript" src="<%= ResolveUrl("~/Employee/override.js?v=") + Web.Helper.WebHelper.Version %>"></script>
    <link href="<%=ResolveUrl("~/bootstrap/css/bootstrap.css?v=") + Web.Helper.WebHelper.Version %>"
        rel="stylesheet" />
    <!-- Glyphicons -->
    <link rel="stylesheet" href="<%=ResolveUrl("~/theme/css/glyphicons.css?v=")  + Web.Helper.WebHelper.Version %>" />
    <script src="<%=ResolveUrl("~/theme/scripts/jquery-1.8.2.min.js?v=")+ Web.Helper.WebHelper.Version %>"
        type="text/javascript"></script>
    <!-- Theme -->
    <link rel="stylesheet" href="<%=ResolveUrl("~/theme/css/style.min.css?v=") + Web.Helper.WebHelper.Version %>" />
    <!-- LESS 2 CSS -->
    <script src="<%=ResolveUrl("~/theme/scripts/less-1.3.3.min.js?v=")+ Web.Helper.WebHelper.Version %>"
        type="text/javascript"></script>
    <script type="text/javascript">

        var currentMonth = null;
        ///// Override DatePicker & is triggered when month or date selection changes
        Ext.DatePicker.prototype.update = function (date, forceRefresh) {

            var me = this,
            active = me.activeDate;
            if (me.rendered) {
                me.activeDate = date;
                if (!forceRefresh && active && me.el && active.getMonth() == date.getMonth() && active.getFullYear() == date.getFullYear()) {
                    me.selectedUpdate(date, active);
                } else {
                    me.fullUpdate(date, active);
                }
            }


            if (this.id == 'ctl00_mainContent_DatePickerWithHolidayDisplay') {

                if (currentMonth == null) {
                    currentMonth = date.getMonth();
                    currentYear = date.getFullYear();
                    loadHoliday(date.getMonth(), date.getFullYear());

                }
                else if (currentMonth != date.getMonth() || currentYear != date.getFullYear()) {
                    currentMonth = date.getMonth();
                    currentYear = date.getFullYear();
                    loadHoliday(date.getMonth(), date.getFullYear());

                }
            }

            return me;

        };


        var myRenderer1 = function (value, metadata) {

            if (value != null && value != '') {
                if (value == 'On')
                    metadata.style = "background-color: #92d050; color:#006100;";
                else
                    metadata.style = "background-color: lightsalmon; color:#9c0006;";
            }

            return value;
        };

        var loadHoliday = function (month, year) {
            Ext.net.Mask.show();
            Ext.net.DirectMethod.request({
                url: "../LeaveRequestService.asmx/GetHolidays",
                cleanRequest: true,
                json: true,
                params: {
                    month: month,
                    year: year
                },
                success: function (result) {
                    holidayList = result;
                    //Ext.Msg.alert("Json Message", result);
                    CompanyX.ctl00_mainContent_DatePickerWithHolidayDisplay.highlightDates(result);
                    Ext.net.Mask.hide();

                    //find the range case
                    for (i = 0; i < result.length; i++) {
                        if (result[i].Id == 0) {
                            CompanyX.ctl00_mainContent_lblCalendarTitle.setText(result[i].Abbreviation);
                            break;
                        }
                    }

                },
                failure: function (result) { Ext.net.Mask.hide(); }
            });
        };


    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
    <ext:ResourceManager ID="ResourceManager1" runat="server" DisableViewState="false"
        ShowWarningOnAjaxFailure="false" ScriptMode="Release" Namespace="CompanyX" />
    <ext:Hidden ID="hdnLoggedInTime" runat="server" />
    <ext:Hidden ID="hdnActivityId" runat="server" />

    <ext:Hidden ID="hdnLeaveRequestId" runat="server" />
    <ext:Hidden ID="hdnTimeRequestId" runat="server" />
    <ext:Hidden ID="hdnOTAllowReqId" runat="server" />
    <ext:Hidden ID="hdnOTAllowType" runat="server" />


<ext:Button ID="btnApproveLeaveReq" runat="server" Hidden="true">
    <DirectEvents>
        <Click OnEvent="btnApproveLeaveReq_Click">
            <Confirmation Message="Confirm Approve the leave?" ConfirmRequest="true" />
            <EventMask ShowMask="true" />
        </Click>
    </DirectEvents>
</ext:Button>
<ext:Button ID="btnDenyLeaveReq" runat="server" Hidden="true">
    <DirectEvents>
        <Click OnEvent="btnDenyLeaveReq_Click">
            <Confirmation Message="Confirm Deny the leave?" ConfirmRequest="true" />
            <EventMask ShowMask="true" />
        </Click>
    </DirectEvents>
</ext:Button>
<ext:LinkButton ID="btnViewLeaveReq" Hidden="true" runat="server">
    <DirectEvents>
        <Click OnEvent="btnViewLeaveReq_Click">
            <EventMask ShowMask="true" />
        </Click>
    </DirectEvents>
</ext:LinkButton>
<ext:LinkButton ID="btnLeaveReqReload" Hidden="true" runat="server">
    <DirectEvents>
        <Click OnEvent="btnLeaveReqReload_Click">
            <EventMask ShowMask="true" />
        </Click>
    </DirectEvents>
</ext:LinkButton>

<ext:LinkButton ID="btnViewTimeReq" Hidden="true" runat="server">
    <DirectEvents>
        <Click OnEvent="btnViewTimeReq_Click">
            <EventMask ShowMask="true" />
        </Click>
    </DirectEvents>
</ext:LinkButton>
<ext:LinkButton ID="btnApproveTimeReq" Hidden="true" runat="server">
    <DirectEvents>
        <Click OnEvent="btnApproveTimeReq_Click">
            <EventMask ShowMask="true" />
            <Confirmation ConfirmRequest="true" Message="Are you sure, you want to approve the time request?" />
        </Click>
    </DirectEvents>
</ext:LinkButton>
<ext:LinkButton ID="btnRejectTimeReq" Hidden="true" runat="server">
    <DirectEvents>
        <Click OnEvent="btnRejectTimeReq_Click">
            <EventMask ShowMask="true" />
            <Confirmation ConfirmRequest="true" Message="Are you sure, you want to reject the time request?" />
        </Click>
    </DirectEvents>
</ext:LinkButton>
<ext:LinkButton ID="btnReloadTimeReqGrid" Hidden="true" runat="server">
    <DirectEvents>
        <Click OnEvent="btnReloadTimeReqGrid_Click">
            <EventMask ShowMask="true" />
        </Click>
    </DirectEvents>
</ext:LinkButton>

<ext:LinkButton ID="btnApproveOTAllow" Hidden="true" runat="server">
    <DirectEvents>
        <Click OnEvent="btnApproveOTAllow_Click">
            <EventMask ShowMask="true" />
            <Confirmation ConfirmRequest="true" Message="Are you sure, you want to approve the request?" />
        </Click>
    </DirectEvents>
</ext:LinkButton>
<ext:LinkButton ID="btnRejectOTAllow" Hidden="true" runat="server">
    <DirectEvents>
        <Click OnEvent="btnRejectOTAllow_Click">
            <EventMask ShowMask="true" />
            <Confirmation ConfirmRequest="true" Message="Are you sure, you want to reject the request?" />
        </Click>
    </DirectEvents>
</ext:LinkButton>
<ext:LinkButton ID="btnReloadOTAllowGrid" Hidden="true" runat="server">
    <DirectEvents>
        <Click OnEvent="btnReloadOTAllowGrid_Click">
            <EventMask ShowMask="true" />
        </Click>
    </DirectEvents>
</ext:LinkButton>


    <div class="contentArea" >
        <uc2:WarningCtl ID="divWarningMsg" EnableViewState="false" Hide="true" runat="server" />
        <table>
            <tr>
                <td valign="top">
                    <div style="margin-top: 15px">
                        <table>
                            <tr>
                                <td>
                                    <asp:Image ID="image" runat="server" Width="120px" Height="120px" />
                                </td>
                                <td style="padding-left: 30px;">
                                    <div>
                                        <span id="welcome" style="color: #44546A; font-size: 22px; font-weight: bold" runat="server">
                                            Welcome {0},</span>
                                        <br />
                                        <asp:Label Style="text-align: center; font-size: 16px; color: #339900; line-height: 20px;
                                            padding-left: 3px" ID="lblDay" runat="server" Text=""></asp:Label>
                                        <asp:Label Style="text-align: center; font-size: 16px; color: #FF3300; line-height: 20px;
                                            padding-left: 15px" ID="lblNepDate" runat="server" Text=""></asp:Label>
                                        <asp:Label Style="text-align: center; font-size: 16px; color: #336699; line-height: 20px;
                                            padding-left: 15px" ID="lblEngDate" runat="server" Text=""></asp:Label>
                                        <%-- <div style="padding-top: 5px;">
                                            <ul class="leaveList">
                                                <asp:Repeater runat="server" ID="rptLeaveList">
                                                    <ItemTemplate>
                                                        <li>
                                                            <%#Eval("Text") %></li>
                                                    </ItemTemplate>
                                                   
                                                </asp:Repeater>
                                            </ul>
                                        </div>--%>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div style="padding-top: 20px">
                    </div>
                    <div style="padding-bottom: 15px; padding-left: 173px">
                        <a href="LeaveRequest.aspx" class="scheduleLeave" title="Leave Request">Leave Request</a>
                        <a href="EmpAttTimeRequestList.aspx" class="scheduleLeave" title="Leave Request">Time Request</a>
                        <a href="OvertimeRequester.aspx" class="scheduleLeave" title="Leave Request">Overtime</a>
                        <a href="SubmitAttendance.aspx" class="submitAttendance">Check In </a>
                        
                    </div>
                    <div style="margin-top: 10px">
                        <div class="divBlockCls">
                            
                            <a href="NoticeList.aspx" style="color:Black; font-weight:bold;"><span>Notices</span><span id="spanNotices"
                                runat="server" style="color: Red; padding-left: 4px">0</span></a>                           

                        </div>
                        <div>
                            <div style="font-size: 13px;">
                                <ext:GridPanel ID="gridNoticeBoard" StripeRows="true" Border="false" Width="860"
                                StyleSpec="border:none" Cls="itemgrid" runat="server" AutoHeight="true" HideHeaders="true">
                                    <Store>
                                        <ext:Store ID="storeNoticeBoard" runat="server">
                                            <Model>
                                                <ext:Model ID="model1" runat="server">
                                                    <Fields>
                                                        <ext:ModelField Name="NoticeID" />
                                                        <ext:ModelField Name="Title" />
                                                        <ext:ModelField Name="PublishedDateStr" />
                                                        <ext:ModelField Name="FileName" />
                                                        <ext:ModelField Name="URL" />
                                                        <ext:ModelField Name="StatusStr" />
                                                    </Fields>
                                                </ext:Model>
                                            </Model>
                                        </ext:Store>
                                    </Store>
                                    <ColumnModel>
                                        <Columns>
                                            <ext:Column ID="Column4" Sortable="false" Align="Left" MenuDisabled="true" runat="server"
                                                Width="300" DataIndex="Title">
                                            </ext:Column>
                                            <ext:Column ID="Column7" Sortable="false" Align="Left" MenuDisabled="true" runat="server"
                                                Width="200" DataIndex="PublishedDateStr">
                                            </ext:Column>
                                            <ext:TemplateColumn ID="TemplateColumn4" runat="server" Text="Attachment" Width="160"
                                                DataIndex="NoticeID" TemplateString='<tpl for="."><tpl if="FileName.trim().length!=0"><a href="../NoticeFileDownloader.ashx?id={URL}"> <img src="../images/paperclip.png" width="15" height="15" title="{FileName}"/></a></tpl></tpl>' />
                                            <ext:TemplateColumn ID="TemplateColumn3" runat="server" Text="Action" Width="100"
                                                DataIndex="NoticeID" TemplateString='<a href="NoticeDetail.aspx?id={NoticeID}" style="color:blue;"> View Detail</a>' />
                                            <ext:TemplateColumn ID="TemplateColumn5" runat="server" Text="Action" Width="100"
                                                DataIndex="NoticeID" TemplateString='<a href="NoticeDetail.aspx?id={NoticeID}" style="color:green;">Mark Read</a>' />
                                        </Columns>
                                    </ColumnModel>
                                </ext:GridPanel>
                            </div>
                        </div>
                    </div>
                        <br />
                        <div style="margin-top: 10px">
                        <div class="divBlockCls">
                            <a href="LeaveApproval.aspx" style="color:Black; font-weight:bold;"><span>Leave Requests</span><span id="spnLeaveRequestCount"
                                            runat="server" style="color: Red; padding-left: 4px">4</span></a>
                            
                            <div style="float:right;">                            
                                <ext:Button ID="btnLeaveReqCollapse" runat="server" Icon="BulletArrowUp" StyleSpec="background-color:transparent;">
                                    <Listeners>
                                        <Click Fn="showHideLeaveReqDiv" />
                                    </Listeners>
                                </ext:Button>
                                    <ext:Button ID="btnLeaveReqExpand" runat="server" Icon="BulletArrowDown" Hidden="true" StyleSpec="background-color:transparent;">
                                        <Listeners>
                                            <Click Fn="showHideLeaveReqDiv" />
                                        </Listeners>
                                    </ext:Button>
                            </div>
                        </div>
                        <div id="divLeaveReq" style="font-size: 13px;">
                            <ext:GridPanel ID="gridPendingRequest" StripeRows="true" Border="false" Width="860"
                                StyleSpec="border:none" Cls="itemgrid" runat="server" AutoHeight="true">
                                <Store>
                                    <ext:Store runat="server" ID="storeLeaveApproval">
                                        <Model>
                                            <ext:Model ID="Model3" runat="server">
                                                <Fields>
                                                    <ext:ModelField Name="EmployeeName" Type="String" />
                                                    <ext:ModelField Name="EmployeeId" Type="String" />
                                                    <ext:ModelField Name="DepartmentName" Type="String" />
                                                    <ext:ModelField Name="LeaveRequestId" Type="String" />
                                                    <ext:ModelField Name="LeaveTypeId" Type="String" />
                                                    <ext:ModelField Name="Title" Type="String" />
                                                    <ext:ModelField Name="IsHour" Type="String" />
                                                    <ext:ModelField Name="Range" Type="String" />
                                                    <ext:ModelField Name="DaysOrHours" Type="String" />
                                                    <ext:ModelField Name="Reason" Type="String" />
                                                    <ext:ModelField Name="From" Type="String" />
                                                    <ext:ModelField Name="To" Type="String" />
                                                    <ext:ModelField Name="Status" Type="string" />
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <View>
                                </View>
                                <ColumnModel runat="server" ID="columnmodle1">
                                    <Columns>
                                        <ext:Column Sortable="false" MenuDisabled="true" Header="Employee" Width="180" DataIndex="EmployeeName"
                                            runat="server">
                                        </ext:Column>
                                        <ext:Column ID="Column1" runat="server" Sortable="false" MenuDisabled="true" Header="Status"
                                            Width="120" DataIndex="Status">
                                            <Renderer Fn="leaveStatusRenderer" />
                                        </ext:Column>
                                        <ext:Column Sortable="false" MenuDisabled="true" Header="Leave" Width="160" runat="server"
                                            DataIndex="Title">
                                        </ext:Column>
                                        <ext:Column ID="Column3" Sortable="false" MenuDisabled="true" Align="Left" Header="Date"
                                            Width="180" runat="server" DataIndex="Range" />
                                        <ext:Column ID="ColumnDaysorHours1" Sortable="false" MenuDisabled="true" Align="Center" Width="80"
                                            Text="Days" DataIndex="DaysOrHours" runat="server">
                                        </ext:Column>
                                        <ext:ImageCommandColumn ID="ImageCommandColumn3" runat="server" Width="60"
                                            MenuDisabled="true" Sortable="false" Align="Left">
                                            <Commands>
                                                <ext:ImageCommand CommandName="View" Text="View" Style="color:Blue;" >
                                                    <ToolTip Text="View" />
                                                </ext:ImageCommand>
                                            </Commands>
                                            <Listeners>
                                                <Command Fn="CommandHandlerLeaveReq">
                                                </Command>
                                            </Listeners>
                                        </ext:ImageCommandColumn>
                                        <ext:ImageCommandColumn ID="ImageCommandColumn1" runat="server" Width="40"
                                            MenuDisabled="true" Sortable="false" Align="Center">
                                            <Commands>
                                                <ext:ImageCommand CommandName="Approve" Icon="Accept"> 
                                                    <ToolTip Text="Approve" />
                                                </ext:ImageCommand>
                                            </Commands>
                                            <Listeners>
                                                <Command Fn="CommandHandlerLeaveReq">
                                                </Command>
                                            </Listeners>
                                        </ext:ImageCommandColumn>
                                        <ext:ImageCommandColumn ID="ImageCommandColumn2" runat="server" Width="40"
                                            MenuDisabled="true" Sortable="false" Align="Center">
                                            <Commands>
                                                <ext:ImageCommand CommandName="Deny" Icon="Decline" >
                                                    <ToolTip Text="Deny" />
                                                </ext:ImageCommand>
                                            </Commands>
                                            <Listeners>
                                                <Command Fn="CommandHandlerLeaveReq">
                                                </Command>
                                            </Listeners>
                                        </ext:ImageCommandColumn>
                                    </Columns>
                                </ColumnModel>
                            </ext:GridPanel>
                        </div>
                        </div>
                        <br />

                        <div style="margin-top: 10px">
                        <div class="divBlockCls">
                            <a href="ApproveTimeAttendance.aspx" style="color:Black; font-weight:bold;"><span>Time Requests</span><span id="spanTimeRequestCount" runat="server"
                                style="color: Red; padding-left: 4px">0</span></a>

                                <div style="float:right;">                            
                                    <ext:Button ID="btnTimeReqCollapse" runat="server" Icon="BulletArrowUp" StyleSpec="background-color:transparent;">
                                        <Listeners>
                                            <Click Fn="showHideTimeReqDiv" />
                                        </Listeners>
                                    </ext:Button>
                                        <ext:Button ID="btnTimeReqExpand" runat="server" Icon="BulletArrowDown" Hidden="true" StyleSpec="background-color:transparent;">
                                            <Listeners>
                                                <Click Fn="showHideTimeReqDiv" />
                                            </Listeners>
                                        </ext:Button>
                                </div>
                         </div>

                        <div id="divTimeReq" style="font-size: 13px;">
                            <ext:GridPanel ID="gridTimeRequest" StripeRows="true" Border="false" Width="860"
                                StyleSpec="border:none" Cls="itemgrid" runat="server" AutoHeight="true">
                                <Store>
                                    <ext:Store ID="storeTimeReq" runat="server" AutoLoad="true" RemotePaging="true" RemoteSort="true">
                                        <Model>
                                            <ext:Model ID="Model4" runat="server" IDProperty="RequestID">
                                                <Fields>
                                                    <ext:ModelField Name="RequestID" Type="String" />
                                                    <ext:ModelField Name="EmployeeId" Type="Int" />
                                                    <ext:ModelField Name="Name" Type="String" />
                                                    <ext:ModelField Name="Branch" Type="String" />
                                                    <ext:ModelField Name="StartDateEng" Type="Date" />
                                                    <ext:ModelField Name="EndDateEng" Type="Date" />
                                                    <ext:ModelField Name="WorkDays" Type="Int" />
                                                    <ext:ModelField Name="SubmittedOnEng" Type="Date" />
                                                    <ext:ModelField Name="IPAddress" Type="string" />
                                                    <ext:ModelField Name="Status" Type="Int" />
                                                    <ext:ModelField Name="StatusText" Type="string" />
                                                    <ext:ModelField Name="ShowHide" Type="Int" />
                                                    <ext:ModelField Name="StatusText" Type="String" />
                                                    <ext:ModelField Name="ReqType" Type="String" />
                                                    <ext:ModelField Name="Reason" Type="String" />
                                                    <ext:ModelField Name="DateRange" Type="String" />
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <ColumnModel>
                                    <Columns>
                                        <ext:Column ID="Colasumn4" Sortable="true" MenuDisabled="true" runat="server" Text="Name"
                                            Width="180" Align="Left" DataIndex="Name">
                                        </ext:Column>
                                        <ext:Column ID="Column5" Sortable="true" MenuDisabled="true" runat="server" Text="Status"
                                            Width="120" Align="Left" DataIndex="StatusText">
                                            <Renderer Fn="leaveStatusRenderer" />
                                        </ext:Column>
                                        <ext:Column ID="Column6" Sortable="true" MenuDisabled="true" runat="server" Text="Reason"
                                            Width="160" Align="Left" DataIndex="Reason">
                                        </ext:Column>
                                        <ext:Column ID="Column8" Sortable="false" MenuDisabled="true" Align="Left" Header="Date"
                                            Width="180" runat="server" DataIndex="DateRange" />
                                        <ext:Column ID="Column9" Sortable="false" MenuDisabled="true" Align="Center" Header="Type"
                                            Width="80" runat="server" DataIndex="ReqType" />
                                        <ext:ImageCommandColumn ID="ImageCommandColumn4" runat="server" Width="60"
                                            MenuDisabled="true" Sortable="false" Align="Left">
                                            <Commands>
                                                <ext:ImageCommand CommandName="View" Text="View" Style="color:Blue;">
                                                    <ToolTip Text="View" />
                                                </ext:ImageCommand>
                                            </Commands>
                                            <Listeners>
                                                <Command Fn="CommandHandlerTimeReq">
                                                </Command>
                                            </Listeners>
                                        </ext:ImageCommandColumn>
                                        <ext:ImageCommandColumn ID="ImageCommandColumn5" runat="server" Width="40"
                                            MenuDisabled="true" Sortable="false" Align="Center">
                                            <Commands>
                                                <ext:ImageCommand CommandName="Approve" Icon="Accept" >
                                                    <ToolTip Text="Approve" />
                                                </ext:ImageCommand>
                                            </Commands>
                                            <Listeners>
                                                <Command Fn="CommandHandlerTimeReq">
                                                </Command>
                                            </Listeners>
                                        </ext:ImageCommandColumn>
                                        <ext:ImageCommandColumn ID="ImageCommandColumn6" runat="server" Width="40"
                                            MenuDisabled="true" Sortable="false" Align="Center">
                                            <Commands>
                                                <ext:ImageCommand CommandName="Reject" Icon="Decline">
                                                    <ToolTip Text="Reject" />
                                                </ext:ImageCommand>
                                            </Commands>
                                            <Listeners>
                                                <Command Fn="CommandHandlerTimeReq">
                                                </Command>
                                            </Listeners>
                                        </ext:ImageCommandColumn>
                                    </Columns>
                                </ColumnModel>
                                <View>
                                    <ext:GridView ID="GridView1" runat="server" StripeRows="true" />
                                </View>
                            </ext:GridPanel>
                        </div>
                        </div>

                        <br />
                        <div style="margin-top: 10px">
                        <div class="divBlockCls">
                            <span style="font-weight:bold;">Overtime and Allowance Requests</span><span id="spanOvertimeAllowanceReq" runat="server"
                                style="color: Red; padding-left: 4px; font-weight:bold;">0</span>

                                <div style="float:right;">                            
                                    <ext:Button ID="btnOTAllowCollapse" runat="server" Icon="BulletArrowUp" StyleSpec="background-color:transparent;">
                                        <Listeners>
                                            <Click Fn="showHideOTAllowDiv" />
                                        </Listeners>
                                    </ext:Button>
                                        <ext:Button ID="btnOTAllowExpand" runat="server" Icon="BulletArrowDown" Hidden="true" StyleSpec="background-color:transparent;">
                                            <Listeners>
                                                <Click Fn="showHideOTAllowDiv" />
                                            </Listeners>
                                        </ext:Button>
                                </div>

                         </div>

                        <div id="divOTAllow" style="font-size: 13px;">
                            <ext:GridPanel ID="gridOvertimeAllowance" StripeRows="true" Border="false" Width="860"
                                StyleSpec="border:none" Cls="itemgrid" runat="server" AutoHeight="true">
                                <Store>
                                    <ext:Store ID="store1" runat="server" AutoLoad="true" RemotePaging="true" RemoteSort="true">
                                        <Model>
                                            <ext:Model ID="Model5" runat="server" IDProperty="Id">
                                                <Fields>
                                                    <ext:ModelField Name="Id" Type="String" />
                                                    <ext:ModelField Name="Name" Type="String" />
                                                    <ext:ModelField Name="Type" Type="String" />
                                                    <ext:ModelField Name="DateString" Type="String" />
                                                    <ext:ModelField Name="Days" Type="String" />
                                                    <ext:ModelField Name="StatusText" Type="String" />
                                                    <ext:ModelField Name="OTAllowanceType" Type="String" />
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <ColumnModel>
                                    <Columns>
                                        <ext:Column ID="Column10" Sortable="true" MenuDisabled="true" runat="server" Text="Name"
                                            Width="180" Align="Left" DataIndex="Name">
                                        </ext:Column>
                                        <ext:Column ID="Column11" Sortable="true" MenuDisabled="true" runat="server" Text="Status"
                                            Width="120" Align="Left" DataIndex="StatusText">
                                            <Renderer Fn="leaveStatusRenderer" />
                                        </ext:Column>
                                        <ext:Column ID="Column12" Sortable="true" MenuDisabled="true" runat="server" Text="Type"
                                            Width="160" Align="Left" DataIndex="Type">
                                        </ext:Column>
                                        <ext:Column ID="Column13" Sortable="false" MenuDisabled="true" Align="Left" Header="Date"
                                            Width="180" runat="server" DataIndex="DateString" />
                                        <ext:Column ID="Column14" Sortable="false" MenuDisabled="true" Align="Center" Header="Days/Hours"
                                            Width="80" runat="server" DataIndex="Days" />
                                        <ext:ImageCommandColumn ID="ImageCommandColumn7" runat="server" Width="60"
                                            MenuDisabled="true" Sortable="false" Align="Left">
                                            <Commands>
                                                <ext:ImageCommand CommandName="View" Text="View" Style="color:Blue;">
                                                    <ToolTip Text="View" />
                                                </ext:ImageCommand>
                                            </Commands>
                                            <Listeners>
                                                <Command Fn="CommandHandlerOTAllowReq">
                                                </Command>
                                            </Listeners>
                                        </ext:ImageCommandColumn>
                                        <ext:ImageCommandColumn ID="ImageCommandColumn8" runat="server" Width="40"
                                            MenuDisabled="true" Sortable="false" Align="Center">
                                            <Commands>
                                                <ext:ImageCommand CommandName="Approve" Icon="Accept" >
                                                    <ToolTip Text="Approve" />
                                                </ext:ImageCommand>
                                            </Commands>
                                            <Listeners>
                                                <Command Fn="CommandHandlerOTAllowReq">
                                                </Command>
                                            </Listeners>
                                        </ext:ImageCommandColumn>
                                        <ext:ImageCommandColumn ID="ImageCommandColumn9" runat="server" Width="40"
                                            MenuDisabled="true" Sortable="false" Align="Center">
                                            <Commands>
                                                <ext:ImageCommand CommandName="Reject" Icon="Decline">
                                                    <ToolTip Text="Reject" />
                                                </ext:ImageCommand>
                                            </Commands>
                                            <Listeners>
                                                <Command Fn="CommandHandlerOTAllowReq">
                                                </Command>
                                            </Listeners>
                                        </ext:ImageCommandColumn>
                                    </Columns>
                                </ColumnModel>
                                <View>
                                    <ext:GridView ID="GridView2" runat="server" StripeRows="true" />
                                </View>
                            </ext:GridPanel>
                        </div>
                    </div>

                    <br />
                   
        <div style="margin-top: 10px">
                        <div class="divBlockCls" style="font-weight:bold;">
                            On Leave
                        </div>
                        <div style="font-size: 13px;">
        <ext:Store ID="storeAbsentEmployee" runat="server" >
            <Model>
                <ext:Model ID="Model7" runat="server" IDProperty="name">
                    <Fields>
                        <ext:ModelField Name="Name" />
                        <ext:ModelField Name="UrlPhoto" />
                    </Fields>
                </ext:Model>
            </Model>
        </ext:Store>

        <ext:Panel FrameHeader="false"
            ID="Panel1"
            runat="server"
            Cls="images-view"
            Width="856"
            Collapsible="false" Frame="false"
            Layout="FitLayout" Header="false"
            Title="Simple DataView (0 items selected)">
            <Items>
        <ext:DataView
                    ID="ImageView"
                    runat="server"
                    StoreID="storeAbsentEmployee"
                    MultiSelect="true"
                    OverItemCls="x-item-over"
                    ItemSelector="div.thumb-wrap"
                    EmptyText=""
                    TrackOver="true">
                    <Tpl ID="Tpl1" runat="server">
                        <Html>
                            <tpl for=".">
                                <div class="thumb-wrap" id="{Name}">
                                    <div class="thumb"><img src="../Uploads/{UrlPhoto}" width="100" height="120">
                                                                    </img></div>
                                    <span class="x-editable" style="margin-top:10px;">{Name}</span>
                                </div>
                            </tpl>
                            <div class="x-clear"></div>
                        </html>                    </Tpl>
                    
                </ext:DataView>
            </Items>
           </ext:Panel>
        </div>
    </div>

    <br />
    



             
                    <div style="margin-top: 10px; margin-left: -7px;" id="divTimeAttRejectComment" runat="server"
                        visible="false">
                        <div class="blockBottomBorder">
                            <img src="images/clock.png" />
                            <asp:Label ID="lblTimeAttRejectedComment" runat="server" CssClass="lblClass" />
                        </div>
                    </div>

                   
                </td>
                <td style="width: 20px">
                    &nbsp;
                </td>
                <td valign="top" style="width: 250px; padding-top: 20px;">
                    <div>
                        <div>

                            <ext:GridPanel ID="gridLeaveRequest" StripeRows="true" Border="false" Width="231" HideHeaders="true"
                                StyleSpec="border:nonel margin-top:10px; margin-left:10px;" Cls="itemgridNew" runat="server" AutoHeight="true">
                                <Store>
                                    <ext:Store runat="server" ID="store2">
                                        <Model>
                                            <ext:Model ID="Model8" runat="server">
                                                <Fields>
                                                    <ext:ModelField Name="LeaveTypeId" Type="String" />
                                                    <ext:ModelField Name="NewBalance" Type="String" />
                                                    <ext:ModelField Name="Title" Type="String" />
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <View>
                                </View>
                                <ColumnModel runat="server" ID="ColumnModel1">
                                    <Columns>
                                        <ext:Column ID="colTitle" Sortable="false" MenuDisabled="true" Header="Title" Width="125" DataIndex="Title"
                                            runat="server">
                                        </ext:Column>
                                        <ext:Column ID="Column15" Sortable="false" MenuDisabled="true" Header="" Width="35" DataIndex="NewBalance" Align="Center"
                                            runat="server">
                                        </ext:Column>      
                                        
                                        <ext:ImageCommandColumn ID="ImageCommandColumn10" runat="server" Width="70" 
                                            MenuDisabled="true" Sortable="false" Align="Left">
                                            <Commands>
                                                <ext:ImageCommand CommandName="Request" Text="Request" Style="color:White; background-color:Blue; height:23px !important;   padding-top:3px; " >
                                                    <ToolTip Text="Leave request" />
                                                </ext:ImageCommand>
                                            </Commands>
                                            <Listeners>
                                                <Command Fn="CommandHandlerEmpLeaveReq">
                                                </Command>
                                            </Listeners>
                                        </ext:ImageCommandColumn>
                                    </Columns>
                                </ColumnModel>
                            </ext:GridPanel>
                            
                            <div class="widget" runat="server" id="Div3" visible="true" style="clear: both; margin-right: 0px; margin-left:10px;
                                width: 231px; margin-top: 10px; border-color: #4E5154;">
                                <div class="widget-head" style="background-color: #4E5154">
                                    <h4 class="heading glyphicons history">
                                        <i></i>My Times This Week</h4>
                                </div>
                                <div style="clear: both">
                                </div>
                                <asp:GridView CssClass="tableLightColor itemgridTime" ID="gvwWeekTimes" runat="server" ShowHeader="false"
                                    AutoGenerateColumns="false" GridLines="None" PagerStyle-CssClass="defaultPagingBar"
                                    PagerStyle-HorizontalAlign="Center">
                                    <RowStyle BackColor="#E3EAEB" />
                                    <Columns>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:Label ID="Label1" runat="server" Width="32px" Text='<%# Eval("ActualDate") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:Label ID="Label1" runat="server" Width="75px" Text='<%# Eval("DayType") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:Label ID="Label1" runat="server" Width="40px" Text='<%# Eval("RefinedInRemarks") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:Label ID="Label1" runat="server" Width="40px" Text='<%# Eval("RefinedOutRemarks") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <RowStyle CssClass="odd" />
                                    <AlternatingRowStyle CssClass="even" />
                                    <EmptyDataTemplate>
                                        <b>No list. </b>
                                    </EmptyDataTemplate>
                                </asp:GridView>
                            </div>

                            <div style="margin-left: 10px;">
                                <div style="margin-top: 5px; margin-bottom: 5px">
                                    <ext:Label runat="server" Width="200" Height="30" ID="lblCalendarTitle" Text="&nbsp;" />
                                </div>
                                <ext:DatePicker ID="DatePickerWithHolidayDisplay" runat="server" Width="215" StyleSpec="border:1px solid #A3BAD9;">
                                    <Listeners>
                                        <Select Fn="CompanyX.setStartDate" Scope="CompanyX" />
                                    </Listeners>
                                    <Plugins>
                                        <ext:GenericPlugin ID="HighlightDates1" runat="server" InstanceName="Ext.ux.DatePicker.HighlightDates" />
                                    </Plugins>
                                </ext:DatePicker>
                            </div>
                            <div class="blockBottomBorder" style="padding-top: 5px">
                                <span class="blockTitle">Upcoming Holidays</span> <span style="padding-left: 20px"
                                    class="blockText"><a href="AllHolidayReport.aspx">View All</a></span>
                            </div>
                            <ext:GridPanel EmptyText="there are no immediate holidays." ID="GridPanelMYTimesheet"
                                runat="server" Width="245" Header="false" HideHeaders="true">
                                <Store>
                                    <ext:Store ID="storeMYTimesheet" runat="server">
                                        <Model>
                                            <ext:Model ID="model2" runat="server">
                                                <Fields>
                                                    <ext:ModelField Name="EngDateLongForDashboard" Type="String">
                                                    </ext:ModelField>
                                                    <ext:ModelField Name="Id" Type="String">
                                                    </ext:ModelField>
                                                    <ext:ModelField Name="Name" Type="String">
                                                    </ext:ModelField>
                                                    <ext:ModelField Name="NepDateLongForDashboard" Type="String">
                                                    </ext:ModelField>
                                                    <ext:ModelField Name="DaysLeftForDashboard" Type="String">
                                                    </ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <ColumnModel>
                                    <Columns>
                                        <ext:TemplateColumn ID="TemplateColumn1" runat="server" MenuDisabled="true" Header="Template"
                                            Width="245">
                                            <Template ID="Template1" runat="server" Width="245">
                                                <Html>
                                                    <tpl for=".">
                                                               
                                                                <table style="width:245px;">
                                                                    <tr>
                                                                        <td>
                                                                            <div style="width:120px;">
                                                                                {EngDateLongForDashboard}
                                                                            </div>
                                                                        </td>
                                                                        <td>
                                                                            <div style="width:175px;">
                                                                                {Name}
                                                                            </div>
                                                                        </td>
                                                                    </tr>

                                                                    <tr>
                                                                        <td>
                                                                            <div style="width:120px;">
                                                                                {NepDateLongForDashboard}
                                                                            </div>
                                                                        </td>
                                                                        <td>
                                                                            <div style="width:120px;">
                                                                                {DaysLeftForDashboard}
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                </table>

                                                         </tpl>
                                                </Html>
                                            </Template>
                                        </ext:TemplateColumn>
                                    </Columns>
                                </ColumnModel>
                            </ext:GridPanel>
                        </div>
                    </div>
                </td>
            </tr>
        </table>
        <%--HPL duty list--%>
        <div runat="server" id="divHPLDuty">
            <span style='margin-top: 20px; display: block; margin-bottom: 15px; font-weight: bold;'>
                Employee Duty Schedule</span>
            <ext:GridPanel StyleSpec="margin-top:5px;" ID="gridSchedule" runat="server" Cls="itemgrid"
                Width="870">
                <Store>
                    <ext:Store ID="storeSchedule" runat="server">
                        <Model>
                            <ext:Model ID="Model6" runat="server" IDProperty="ScheduleId">
                                <Fields>
                                    <ext:ModelField Name="ScheduleId" Type="String" />
                                    <ext:ModelField Name="EmployeeId" Type="String" />
                                    <ext:ModelField Name="Name" Type="String" />
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                <ColumnModel>
                    <Columns>
                        <ext:Column ID="Column2" Sortable="false" MenuDisabled="true" runat="server" Text="EIN"
                            Width="40" Align="Center" DataIndex="EmployeeId">
                        </ext:Column>
                        <ext:Column ID="colEmployeeName" Sortable="false" MenuDisabled="true" runat="server"
                            Text="Name" Width="139" Align="Left" DataIndex="Name">
                        </ext:Column>
                    </Columns>
                </ColumnModel>
                <SelectionModel>
                    <ext:RowSelectionModel ID="RowSelectionModel2" runat="server" Mode="Single" />
                </SelectionModel>
                <Plugins>
                    <ext:CellEditing ID="CellEditing1" ClicksToEdit="1" runat="server">
                        <%--<Listeners>                              
                                                                    <Edit Fn="afterEdit" />
                                                                </Listeners>--%>
                    </ext:CellEditing>
                </Plugins>
            </ext:GridPanel>
        </div>
    </div>
    <ext:Window ID="WPasswordNotification" runat="server" Title="Password Change Notification"
        Icon="None" Height="150" Width="300" BodyPadding="5" Hidden="true" Modal="true">
        <Content>
            <div style="background-color: White; height: 150px; width: 300px;">
                <br />
                <p style="margin-left: 20px; padding-bottom: 5px;">
                    <b>Your password has expired.</b></p>
                <b><a style="margin-left: 20px; text-decoration: underline;" href="ChangePassword.aspx">
                    Click here</a> to change your password.</b>
            </div>
        </Content>
    </ext:Window>
    <ext:Window ID="WCheckInOut" runat="server" Title="Check In" Icon="Application" Frame="true"
        Resizable="false" Visible="false" Width="400" Height="260" BodyPadding="5" Hidden="true"
        Modal="true">
        <Content>
            <table class="fieldTable" style="margin-left: 15px;">
                <tr>
                    <td style="color: Green;">
                        <ext:DisplayField ID="dfMessage" runat="server" FieldStyle="color:Green;" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <ext:DisplayField ID="dfLoginDateTime" runat="server" FieldStyle="color:#6AAFE0; background-color:#DBE9F5; padding-left:5px; height:25px; padding-top:3px; padding-right:30px" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <ext:TextField ID="txtNote" runat="server" FieldLabel="Note" Text="" LabelAlign="Top"
                            LabelSeparator="" Width="300px" />
                    </td>
                </tr>
            </table>
            <div class="popupButtonDiv bottomBlock" style="padding-left: 35px;">
                <ext:Button runat="server" ID="btnCheckIn" Cls="btn btn-primary" Text="<i></i>Check In">
                    <DirectEvents>
                        <Click OnEvent="btnCheckIn_Click">
                            <EventMask ShowMask="true" />
                        </Click>
                    </DirectEvents>
                </ext:Button>
                <ext:Button runat="server" ID="btnCheckOut" Cls="btn btn-primary" Text="<i></i>Check Out">
                    <DirectEvents>
                        <Click OnEvent="btnCheckIn_Click">
                            <EventMask ShowMask="true" />
                        </Click>
                    </DirectEvents>
                </ext:Button>
                <div class="btnFlatOr" style="width: 10px;">
                </div>
                <ext:Button runat="server" ID="btnCheckInCancel" Cls="btn btn-primary" Text="<i></i>Cancel">
                    <DirectEvents>
                        <Click OnEvent="btnCheckInCancel_Click">
                            <EventMask ShowMask="true" />
                        </Click>
                    </DirectEvents>
                </ext:Button>
                <ext:Button runat="server" ID="btnCheckOutCancel" Cls="btn btn-primary" Text="<i></i>Cancel">
                    <DirectEvents>
                        <Click OnEvent="btnCheckInCancel_Click">
                            <EventMask ShowMask="true" />
                        </Click>
                    </DirectEvents>
                </ext:Button>
            </div>
        </Content>
    </ext:Window>
    <ext:Window ID="WComment" runat="server" Title="Activity Comment" Icon="None" Height="140"
        Width="1100" BodyPadding="5" Hidden="true" Modal="true">
        <Content>
            <table style="margin-left: 20px;">
                <tr>
                    <td>
                        <ext:TextField ID="txtActivityComment" runat="server" EmptyText="Write your comment..."
                            FieldLabel="Your Comment" Text="" LabelAlign="Top" LabelSeparator="" Width="1040">
                            <Listeners>
                                <SpecialKey Fn="enterKeyPressHandler" />
                            </Listeners>
                        </ext:TextField>
                        <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator2" runat="server"
                            ValidationGroup="SaveUpdComment" ControlToValidate="txtActivityComment" ErrorMessage="Comment is required." />
                    </td>
                </tr>
                <tr>
                    <td>
                        
                        <ext:Button runat="server" ID="btnSaveComment" Cls="btn btn-primary" Text="<i></i>Save"
                            MarginSpec="20 0 0 0" Hidden="true">
                            <DirectEvents>
                                <Click OnEvent="btnSaveComment_Save">
                                    <EventMask ShowMask="true" />
                                </Click>
                            </DirectEvents>
                            <Listeners>
                                <Click Handler="valGroup = 'SaveUpdComment'; if(CheckValidation()) return ''; else return false;">
                                </Click>
                            </Listeners>
                        </ext:Button>
                    </td>
                </tr>
            </table>
        </Content>
    </ext:Window>
    <uc5:AssingLeave Id="AssignLeave" runat="server" />
    <ucTR:TimeReqCtrl Id="TimeReqCtrl1" runat="server" />
    
    <style type="text/css">
    
        .images-view .x-panel-body {
            background: white;
            font: 11px Arial, Helvetica, sans-serif;
        }
        .images-view .thumb {
            background: #dddddd;
           
            padding-bottom: 0;
        }

        .x-quirks .images-view .thumb {
            padding-bottom: 3px;
        }

        .images-view .thumb img {
            height: 110px;
            width: 100px;
        }

        .images-view .thumb-wrap {
            float: left;
            margin: 4px;
            margin-right: 0;
            padding: 5px;
        }

        .images-view .thumb-wrap span {
            display: block;
            overflow: hidden;
            text-align: center;
            width: 100px; 
        }
         
         .itemgrid .x-column-header {
            font-weight: normal;
            color:Gray;
            background-color:transparent;
            border:none;
            border-bottom:1px solid whitesmoke;
        }
        
        .itemgrid tr
        {
            border-bottom:1px solid whitesmoke;
        }
        
        .itemgridNew .x-column-header {
            font-weight: normal;
            color:Black;
            background-color:transparent;
            border:none;
        }
        
        .itemgridNew tr td
        {
            border-bottom:2px solid white !important;
            background-color:#EFEFEF !important;
            color:Black;
        }
        
        .itemgridTime tr td
        {
            font-size:10px !important;
        }
        
       
    </style>

    <script type="text/javascript">
    var stub = "Your Password has been changed.";
    function ShowMsg() {
        Ext.net.Notification.show({
            iconCls: 'icon-information',
            pinEvent: 'click',
            html: 'Your Password has been changed.',
            title: 'Title'
        });
  }


  function activityComment() {

    <%= WComment.ClientID %>.center();
    <%= WComment.ClientID %>.show();
    
  }

  var enterKeyPressHandler = function (f, e) {
            if (e.getKey() == e.ENTER) {
                var commentMsg = <%= txtActivityComment.ClientID %>.getValue();
               
                <%= btnSaveComment.ClientID %>.fireEvent('click');
                e.stopEvent();
            }
        };

    var CommandHandlerLeaveReq = function(value, command,record,p2) {
            
            <%=hdnLeaveRequestId.ClientID %>.setValue(record.data.LeaveRequestId);
            if(command == 'Approve')
            { 
                <%= btnApproveLeaveReq.ClientID %>.fireEvent('click');
            }
            else if(command == 'Deny')
            {
                <%= btnDenyLeaveReq.ClientID %>.fireEvent('click');
            }
            else if(command == 'View')
            {
                <%=btnViewLeaveReq.ClientID %>.fireEvent('click');
            }
       };

       var template = '<span style="color:{0};">{1}</span>';
       var leaveStatusRenderer = function (value) {
            if(value.toLowerCase() == 'request' || value.toLowerCase() == 'pending'){
                return Ext.String.format(template, "blue", 'REQUESTED');
             }
             else {
                return Ext.String.format(template, "brown", 'RECOMMENDED');
             }
        };

        var CommandHandlerTimeReq = function(value, command,record,p2) {
            
            <%=hdnTimeRequestId.ClientID %>.setValue(record.data.RequestID);
            if(command == 'Approve')
            { 
                <%= btnApproveTimeReq.ClientID %>.fireEvent('click');
            }
            else if(command == 'Reject')
            {
                <%= btnRejectTimeReq.ClientID %>.fireEvent('click');
            }
            else if(command == 'View')
            {                
                <%= btnViewTimeReq.ClientID %>.fireEvent('click');
            }
       };

       function reloadTimeReqGrid()
       {
            <%= btnReloadTimeReqGrid.ClientID %>.fireEvent('click');
       }

       function reloadGridsAfterApproval()
       {
            <%=btnLeaveReqReload.ClientID %>.fireEvent('click');
       }
       
       var CommandHandlerOTAllowReq = function(value, command,record,p2) {
            
            <%=hdnOTAllowReqId.ClientID %>.setValue(record.data.Id);
            <%=hdnOTAllowType.ClientID %>.setValue(record.data.OTAllowanceType);
            if(command == 'Approve')
            { 
                <%= btnApproveOTAllow.ClientID %>.fireEvent('click');
            }
            else if(command == 'Reject')
            {
                <%= btnRejectOTAllow.ClientID %>.fireEvent('click');
            }
            else if(command == 'View')
            {      
                if(record.data.OTAllowanceType == "ot")          
                    overtimePopup("isPopup=true&reqid=" + record.data.Id);
                else
                    allowActionPopup("isPopup=true&reqid=" + record.data.Id);
            }
       };

       function refreshEventList(popupWindow)
       {
            if (typeof (popupWindow) != 'undefined')
                popupWindow.close();
            <%= btnReloadOTAllowGrid.ClientID %>.fireEvent('click');
       }

      var CommandHandlerEmpLeaveReq = function(value, command,record,p2) {
            
            window.location = 'LeaveRequest.aspx?ReqLeaveTpyeId=' + record.data.LeaveTypeId;
       };

       function showHideLeaveReqDiv() {
            var x = document.getElementById('divLeaveReq');
            if (x.style.display === 'none') {
                x.style.display = 'block';
                <%= btnLeaveReqCollapse.ClientID %>.show();
                <%= btnLeaveReqExpand.ClientID %>.hide();
            } else {
                x.style.display = 'none';          
                <%= btnLeaveReqExpand.ClientID %>.show();
                <%= btnLeaveReqCollapse.ClientID %>.hide();
            }
        }

        function showHideTimeReqDiv() {
            var x = document.getElementById('divTimeReq');
            if (x.style.display === 'none') {
                x.style.display = 'block';
                <%= btnTimeReqCollapse.ClientID %>.show();
                <%= btnTimeReqExpand.ClientID %>.hide();
            } else {
                x.style.display = 'none';          
                <%= btnTimeReqExpand.ClientID %>.show();
                <%= btnTimeReqCollapse.ClientID %>.hide();
            }
        }

        function showHideOTAllowDiv() {
            var x = document.getElementById('divOTAllow');
            if (x.style.display === 'none') {
                x.style.display = 'block';
                <%= btnOTAllowCollapse.ClientID %>.show();
                <%= btnOTAllowExpand.ClientID %>.hide();
            } else {
                x.style.display = 'none';          
                <%= btnOTAllowExpand.ClientID %>.show();
                <%= btnOTAllowCollapse.ClientID %>.hide();
            }
        }
    
    </script>
</asp:Content>
