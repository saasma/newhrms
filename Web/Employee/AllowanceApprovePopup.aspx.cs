﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Manager;
using DAL;
using Utils.Helper;
using Utils.Web;
using System.Text;
using BLL;
using System.Text.RegularExpressions;
using Utils.Calendar;
using Ext.Net;
using BLL.BO;

namespace Web.Employee
{


    public partial class AllowanceApprovePopup : System.Web.UI.Page
    {
        InsuranceManager insMgr = new InsuranceManager();
        List<IInsuranceInstitution> source = new List<IInsuranceInstitution>();
        public bool IsDisplayedAsPopup
        {
            get
            {
                if (Request.QueryString["isPopup"] != null)
                    return true;
                return false;
            }
        }

        public bool HasImport
        {
            get
            {
                if (ViewState["import"] == null)
                    return false;
                return Convert.ToBoolean(ViewState["import"]);
            }
            set
            {
                ViewState["import"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                Initialise();

        }

        void Initialise()
        {
            txtDays.FieldLabel = AllowanceManager.AllowanceDaysOrHourLabel + " *";

            Hidden_RequestID.Value = Request.QueryString["reqid"];
            Load();

            if (CommonManager.CompanySetting.WhichCompany == WhichCompany.HPL)
                lblRate.Visible = false;
        }

        new void Load()
        {

            //btnApprove.Enabled = false;
            //btnRecommend.Enabled = false;
            if (!string.IsNullOrEmpty(Hidden_RequestID.Value))
            {
                int RequestID = int.Parse(Hidden_RequestID.Value);

                EveningCounterRequest request = new EveningCounterRequest();
                request = AllowanceManager.getEveningCounterRequestByID(RequestID);

                if (request != null)
                {

                    gridHistory.DataSource = AllowanceManager.GetAllowanceHistory(request.CounterRequestID, (int)RequestHistoryTypeEnum.EveningCounter);
                    gridHistory.DataBind();

                    EveningCounterType type1 = new EveningCounterType();
                    type1 = AllowanceManager.GetEveningCounterType(request.EveningCounterTypeId.Value);

                    if (type1 != null)
                    {
                        lblCounterType.Text = type1.Name;
                        Hidden_CounterTypePrev.Value = type1.Name;
                    }

                    lblDate.Text = request.EmployeeID.ToString();
                    lblName.Text = request.EmpName;
                    lblReason.Text = request.Reason;


                    //lblStartDate.Text = request.StartDate.Value.ToShortDateString() + " (" +
                    //    CustomDate.GetCustomDateFromString(DateManager.GetAppropriateDate(request.StartDate.Value), false).ToString() + ")";
                    //lblEndDate.Text = request.EndDate.Value.ToShortDateString() + " (" +
                    //   CustomDate.GetCustomDateFromString(DateManager.GetAppropriateDate(request.EndDate.Value), false).ToString() + ")";

                    //lblDays.Text = request.Days.ToString();

                    int? branchId = BLL.BaseBiz.PayrollDataContext.GetCurrentBranch(request.EmployeeID.Value, DateTime.Now.Date);
                    if (branchId != null)
                        lblBranch.Text = BranchManager.GetBranchById(branchId.Value).Name;

                    int? desigId = BLL.BaseBiz.PayrollDataContext.GetEmployeeCurrentPositionForDate(DateTime.Now.Date, request.EmployeeID.Value);
                    if (desigId != null)
                    {
                        EDesignation desig = CommonManager.GetDesigId(desigId.Value);
                        lblPosition.Text = desig.LevelAndDesignation;
                    }


                    txtFromDate.SelectedDate = request.StartDate.Value;
                    txtToDate.SelectedDate = request.EndDate.Value;

                    if (request.Days == null)
                        txtDays.Text = "";
                    else
                        txtDays.Text = request.Days.ToString();


                    EveningCounterType allowanceType = BLL.BaseBiz.PayrollDataContext.EveningCounterTypes.FirstOrDefault(x => x.EveningCounterTypeId
                        == request.EveningCounterTypeId);

                    if (CommonManager.CompanySetting.WhichCompany != WhichCompany.HPL && allowanceType.DoNowShowDaysWarning != true)
                    {
                        decimal? rate = BLL.BaseBiz.PayrollDataContext.GetAllowanceRate(request.EmployeeID, request.CounterRequestID);
                        if (rate != null)
                            lblRate.Text = BLL.BaseBiz.GetCurrency(rate) + " / Unit";
                    }

                    Date_Change(null, null);

                    Hidden_FromPrev.Value = txtFromDate.Text;
                    Hidden_ToPrev.Value = txtToDate.Text;
                    Hidden_DaysPrev.Value = txtDays.Text;


                    if(request.RecommenderForwardedBy != null)
                        lblRecommendBy.Text = EmployeeManager.GetEmployeeName(request.RecommenderForwardedBy.Value) + ", ";

                    if (request.RecommendedBy != null)
                        lblRecommendBy.Text += EmployeeManager.GetEmployeeName(request.RecommendedBy.Value);

                    if (request.ApprovalID != null)
                        lblApprovedBy.Text = EmployeeManager.GetEmployeeName(request.ApprovalID.Value);

                    if (request.Status == (int)OvertimeStatusEnum.Pending)
                    {
                        btnRecommend.Visible = true;
                        btnApprove.Visible = false;

                        if (CommonManager.Setting.LeaveAllowMultipleReview != null && CommonManager.Setting.LeaveAllowMultipleReview.Value)
                        {
                            btnRecommendForReview.Show();

                            List<TextValue> listReview = LeaveRequestManager.GetApplyToListForEmployee(request.EmployeeID.Value, false, PreDefindFlowType.Overtime);

                            cmbReviewerList.Store[0].DataSource = listReview;
                            cmbReviewerList.Store[0].DataBind();

                            //cmbReviewerList.Store[0].DataSource = LeaveRequestManager.GetFilterRecommenderForForward(listReview, leaveRequestId);
                            //cmbReviewerList.Store[0].DataBind();

                        }

                        if (request.RecommendedBy == request.ApprovalID)
                        {
                            btnRecommend.Visible = false;
                            btnApprove.Visible = true;
                            btnForward.Visible = true;
                        }
                    }
                    else if (request.Status == (int)OvertimeStatusEnum.Recommended)
                    {
                        btnRecommend.Visible = false;
                        btnApprove.Visible = true;


                    }

                    if (request.Status == (int)OvertimeStatusEnum.Pending && request.RecommendedBy == SessionManager.CurrentLoggedInEmployeeId)
                    {
                    }
                    else if ((request.Status == (int)OvertimeStatusEnum.Forwarded || request.Status == (int)OvertimeStatusEnum.Rejected)
                        ||
                        (request.RecommendedBy == SessionManager.CurrentLoggedInEmployeeId && request.ApprovalID != SessionManager.CurrentLoggedInEmployeeId)
                        ||
                        (request.Status == (int)OvertimeStatusEnum.Approved)
                        )
                    {

                        divWarningMsg.InnerHtml = "Status has been changed. This request cannot be edited.";
                        divWarningMsg.Hide = false;

                        //txtHour.Enabled = false;
                        //txtMinute.Enabled = false;
                        btnApprove.Visible = false;
                        btnRecommend.Visible = false;
                        btnReject.Visible = false;
                    }

                }


            }
        }


        protected void btnForward_Click(object sender, DirectEventArgs e)
        {
            int RequestID = int.Parse(Hidden_RequestID.Value);
            Status status = LeaveAttendanceManager.ForwardAllowance(RequestID
                , int.Parse(cmbReviewerList.SelectedItem.Value));

            if (status.IsSuccess)
            {

                NewMessage.ShowNormalMessage("Allowance has been forwarded.", "hasImport=true;closePopup();\n");

                btnApprove.Hide();
                btnRecommend.Hide();
                btnReject.Hide();
                btnForward.Hide();

                this.windowReview.Hide();

            }
            else
            {
                BLL.Helper.ShowExtWarningMessage(status.ErrorMessage);
            }
        }

        protected void Date_Change(object sender, DirectEventArgs e)
        {
            DateTime TodayDate = new DateTime();
            TodayDate = CommonManager.GetCurrentDateAndTime();


            string FromDateNep;
            string EndDateNep;




            if (txtFromDate.SelectedDate != null &&
                txtFromDate.SelectedDate.Date > TodayDate.AddYears(-25).Date && txtFromDate.SelectedDate.Date < TodayDate.AddYears(25).Date)
            {
                FromDateNep = DateManager.GetAppropriateDate(txtFromDate.SelectedDate);
                txtFromDateNep.Text = FromDateNep;
            }
            else
            {
                txtFromDateNep.Text = "";
                txtFromDate.SelectedDate = CommonManager.GetCurrentDateAndTime();
            }



            if (txtToDate.SelectedDate != null &&
                txtToDate.SelectedDate.Date > TodayDate.AddYears(-25).Date && txtToDate.SelectedDate.Date < TodayDate.AddYears(25).Date)
            {
                EndDateNep = DateManager.GetAppropriateDate(txtToDate.SelectedDate);
                txtToDateNep.Text = EndDateNep;

            }
            else
            {
                txtToDateNep.Text = "";
                txtToDate.SelectedDate = CommonManager.GetCurrentDateAndTime();
            }




            //if (txtFromDate.SelectedDate != null && txtToDate.SelectedDate != null &&
            //    txtFromDate.SelectedDate.Date > TodayDate.AddYears(-25).Date && txtFromDate.SelectedDate.Date < TodayDate.AddYears(25).Date &&
            //    txtToDate.SelectedDate.Date > TodayDate.AddYears(-25).Date && txtToDate.SelectedDate.Date < TodayDate.AddYears(25).Date)
            //{
            //    if (txtFromDate.SelectedDate.Date > txtToDate.SelectedDate.Date)
            //    {
            //        txtToDate.SelectedDate = txtFromDate.SelectedDate.Date;
            //    }
            //    //if (txtFromDate.SelectedDate.Date != null && txtToDate.SelectedDate.Date != null)
            //    //{
            //    //    string days = ((txtToDate.SelectedDate.Date - txtFromDate.SelectedDate.Date).TotalDays + 1).ToString();
            //    //    txtDays.Text = Math.Round(decimal.Parse(days), MidpointRounding.ToEven).ToString();
            //    //}
            //}



        }


        protected void Page_PreRender(object sender, EventArgs e)
        {
            //Page.ClientScript.RegisterOnSubmitStatement(this.GetType(), "sdfdsffd", "window.onunload = null;");
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "sdf", "var hasImport = " + (this.HasImport ? "true;" : "false;"), true);


            //output all as js array to be updatable in parent window
            //if (IsDisplayedAsPopup && source != null)
            //{
            //    //Page.ClientScript.
            //    StringBuilder str = new StringBuilder("");
            //    bool first = true;
            //    foreach (IInsuranceInstitution obj in source)
            //    {
            //        if (first == false)
            //            str.Append(",");
            //        str.Append("'" + obj.Id + "$$" + obj.InstitutionName + "'");
            //        first = false;
            //    }
            //    Page.ClientScript.RegisterArrayDeclaration("texts", str.ToString());
            //}
        }

        protected void btnApprove_Click(object sender, DirectEventArgs e)
        {
             Page.Validate("SaveUpdate");
             if (Page.IsValid)
             {
                 EveningCounterRequest requestInstance = new EveningCounterRequest();

                 requestInstance.CounterRequestID = int.Parse(Hidden_RequestID.Value);

                 requestInstance.StartDate = txtFromDate.SelectedDate;
                 requestInstance.EndDate = txtToDate.SelectedDate;
                 requestInstance.Days = double.Parse(txtDays.Text.Trim());

                 List<RequestEditHistory> edits = new List<RequestEditHistory>();
                 RequestEditHistory edit = new RequestEditHistory();

                 if (Hidden_FromPrev.Value.Trim() != txtFromDate.Text.Trim())
                 {
                     edit = new RequestEditHistory();
                     edit.Column1Before = Hidden_FromPrev.Value;
                     edit.Column1After = txtFromDate.Text;
                     edits.Add(edit);
                     Hidden_FromPrev.Value = txtFromDate.Text;
                 }
                 if (Hidden_ToPrev.Value.Trim() != txtToDate.Text.Trim())
                 {
                     edit = new RequestEditHistory();
                     edit.Column1Before = Hidden_ToPrev.Value;
                     edit.Column1After = txtToDate.Text;
                     edits.Add(edit);
                     Hidden_ToPrev.Value = txtToDate.Text;
                 }

                 if (Hidden_DaysPrev.Value.Trim() != txtDays.Text.Trim())
                 {
                     edit = new RequestEditHistory();
                     edit.Column1Before = Hidden_DaysPrev.Value;
                     edit.Column1After = txtDays.Text;
                     edits.Add(edit);
                     Hidden_DaysPrev.Value = txtDays.Text;
                 }

                 

                
                 requestInstance.Status = (int)EveningCounterStatusEnum.Approved;
                 Status status = AllowanceManager.ApproveRequest(requestInstance, edits);
                 if (status.IsSuccess)
                 {
                     this.HasImport = true;
                     NewMessage.ShowNormalMessage("Allowance has been approved.", "hasImport=true;closePopup();\n");
                 }
                 else
                 {
                     NewMessage.ShowWarningMessage(status.ErrorMessage);
                     //divWarningMsg.InnerHtml = status.ErrorMessage;
                     //divWarningMsg.Hide = false;
                 }
                  
             }
             else
             {
                 JavascriptHelper.DisplayClientMsg("Validation Failed", this);
             }
        }

        public void btnHistory_Click(object sender, DirectEventArgs e)
        {
            int RequestID = int.Parse(Hidden_RequestID.Value);

            EveningCounterRequest request = AllowanceManager.getEveningCounterRequestByID(RequestID);

            grid.Store[0].DataSource = AllowanceManager.GetAllowanceHistoryForEmployee(request.EmployeeID.Value, request.CounterRequestID);
            grid.Store[0].DataBind();

            windowNew.Show();

        }
        protected void btnRecommend_Click(object sender, DirectEventArgs e)
        {
            Page.Validate("SaveUpdate");
            if (Page.IsValid)
            {
                EveningCounterRequest requestInstance = new EveningCounterRequest();
                requestInstance.CounterRequestID = int.Parse(Hidden_RequestID.Value);

                requestInstance.StartDate = txtFromDate.SelectedDate.Date;
                requestInstance.EndDate = txtToDate.SelectedDate.Date;

                //TimeSpan difference = new TimeSpan();
                //if (requestInstance.StartDate != null && requestInstance.EndDate != null)
                //{
                //    requestInstance.Days = Math.Abs(difference.TotalDays);
                //}
                requestInstance.Days = double.Parse(txtDays.Text);
                requestInstance.Status = (int)EveningCounterStatusEnum.Recommended;

                List<RequestEditHistory> edits = new List<RequestEditHistory>();
                RequestEditHistory edit = new RequestEditHistory();

                if (Hidden_FromPrev.Value.Trim() != txtFromDate.Text.Trim())
                {
                    edit = new RequestEditHistory();
                    edit.Column1Before = Hidden_FromPrev.Value;
                    edit.Column1After = txtFromDate.Text;
                    edits.Add(edit);
                    Hidden_FromPrev.Value = txtFromDate.Text;
                }
                if (Hidden_ToPrev.Value.Trim() != txtToDate.Text.Trim())
                {
                    edit = new RequestEditHistory();
                    edit.Column1Before = Hidden_ToPrev.Value;
                    edit.Column1After = txtToDate.Text;
                    edits.Add(edit);
                    Hidden_ToPrev.Value = txtToDate.Text;
                }

                if (Hidden_DaysPrev.Value.Trim() != txtDays.Text.Trim())
                {
                    edit = new RequestEditHistory();
                    edit.Column1Before = Hidden_DaysPrev.Value;
                    edit.Column1After = txtDays.Text;
                    edits.Add(edit);
                    Hidden_DaysPrev.Value = txtDays.Text;
                }

                Status status = AllowanceManager.RecommendRequest(requestInstance, edits);
                if (status.IsSuccess)
                {
                    this.HasImport = true;
                    //JavascriptHelper.DisplayClientMsg("Allowance has been recommended.", this, "closePopup();\n");
                    NewMessage.ShowNormalMessage("Allowance has been recommended.", "hasImport=true;closePopup();\n");
                }
                else
                {
                    //divWarningMsg.InnerHtml = status.ErrorMessage;
                    //divWarningMsg.Hide = false;
                    NewMessage.ShowWarningMessage(status.ErrorMessage);
                }

            }
            else
            {
                JavascriptHelper.DisplayClientMsg("Validation Failed", this);
            }
        }
        protected void btnReject_Click(object sender, DirectEventArgs e)
        {
            int requestId = int.Parse(Hidden_RequestID.Value);
            bool isSuccess = AllowanceManager.RejectRequest(requestId); 
            
            if (isSuccess)
            {
                this.HasImport = true;
                //JavascriptHelper.DisplayClientMsg("Allowance has been rejected.", this, "closePopup();\n");
                NewMessage.ShowNormalMessage("Allowance has been rejected.", "hasImport=true;closePopup();\n");
            }

        }


       

    }
}
