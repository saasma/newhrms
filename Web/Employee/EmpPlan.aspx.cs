﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL.Entity;
using BLL;
using System.IO;
using Utils;

namespace Web.Employee
{
    public partial class EmpPlan : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
            }
        }


        private void Clear()
        {
            txtDate.Text = "";

            int employeeId = SessionManager.CurrentLoggedInEmployeeId;
            EEmployee emp = EmployeeManager.GetEmployeeById(employeeId);
            bool hasPhoto = false;
            if (emp.HHumanResources.Count > 0)
            {
                if (!string.IsNullOrEmpty(emp.HHumanResources[0].UrlPhotoThumbnail))
                {
                    img.ImageUrl = Page.ResolveUrl(Config.UploadLocation + "/" + emp.HHumanResources[0].UrlPhotoThumbnail);
                    if (File.Exists(Server.MapPath(img.ImageUrl)))
                        hasPhoto = true;
                }
            }
            if (!hasPhoto)
            {
                img.ImageUrl = Page.ResolveUrl(Web.Helper.WebHelper.GetEmployeeNoImage(employeeId));

            }

            dfName.Text = emp.Name;
            txtDate.Enable();
        }

        private void BindEmptyGrid()
        {
            List<EmployeePlanDetail> list = new List<EmployeePlanDetail>();
            for (int i = 1; i <= 5; i++)
            {
                EmployeePlanDetail obj = new EmployeePlanDetail()
                {
                    SN = i
                };
                list.Add(obj);
            }

            gridPlan.Store[0].DataSource = list;
            gridPlan.Store[0].DataBind();

        }

        protected void btnAdd_Click(object sender, DirectEventArgs e)
        {
            Clear();
            BindEmptyGrid();
            hdnPlanId.Text = "";
            wPlan.Center();
            wPlan.Show();
        }

        protected void btnSave_Click(object sender, DirectEventArgs e)
        {
            Page.Validate("SaveUpdPlan");
            if (Page.IsValid)
            {
                EmployeePlan obj = new EmployeePlan();

                if (!string.IsNullOrEmpty(hdnPlanId.Text))
                    obj.PlanId = int.Parse(hdnPlanId.Text);

                List<EmployeePlanDetail> listEmployeePlanDetail = new List<EmployeePlanDetail>();

                string gridItemsJSON = e.ExtraParams["gridItems"];
                List<EmployeePlanDetail> list = JSON.Deserialize<List<EmployeePlanDetail>>(gridItemsJSON);

                int i = 1;
                foreach (var item in list)
                {
                    EmployeePlanDetail objDetail = new EmployeePlanDetail();

                    if (!string.IsNullOrEmpty(item.Description))
                    {
                        objDetail.Description = item.Description;

                        if (!string.IsNullOrEmpty(item.TotalTime))
                        {
                            objDetail.TotalTime = item.TotalTime;
                            objDetail.SN = i;
                            i++;
                            listEmployeePlanDetail.Add(objDetail);
                        }
                        else
                        {
                            NewMessage.ShowWarningMessage(string.Format("Time is required in row {0}.", item.SN));
                            return;
                        }
                    }
                }

                if (listEmployeePlanDetail.Count == 0)
                {
                    NewMessage.ShowWarningMessage("Please add new Record.");
                    return;
                }

                obj.EmployeePlanDetails.AddRange(listEmployeePlanDetail);
                
                obj.EmployeeId = SessionManager.CurrentLoggedInEmployeeId;

                if (txtDate.SelectedDate != new DateTime())
                {
                    obj.DateEng = txtDate.SelectedDate;
                    obj.Date = BLL.BaseBiz.GetAppropriateDate(obj.DateEng.Value);
                }
                else
                {
                    NewMessage.ShowWarningMessage("Date is required.");
                    txtDate.Focus();
                    return;
                }
                
                obj.Status = (int)AtivityStatusEnum.Draft;

                Status status = NewHRManager.SaveUpdateEmployeePlan(obj);
                if (status.IsSuccess)
                {
                    if(!string.IsNullOrEmpty(hdnPlanId.Text))
                        NewMessage.ShowNormalMessage("Record saved successfully.");
                    else
                        NewMessage.ShowNormalMessage("Record udpated successfully.");

                    X.Js.Call("searchList");
                    wPlan.Close();
                }
                else
                    NewMessage.ShowWarningMessage(status.ErrorMessage);
            }
        }

        protected void btnEdit_Click(object sender, DirectEventArgs e)
        {
            int planId = int.Parse(hdnPlanId.Text);
            Clear();

            EmployeePlan obj = NewHRManager.GetEmployeePlanById(planId);
            if (obj != null)
            {
                txtDate.SelectedDate = obj.DateEng.Value;
                txtDate.Disable();

                if (obj.EmployeePlanDetails.Count < 5)
                {
                    for (int i = obj.EmployeePlanDetails.Count + 1; i <= 5; i++)
                    {
                        obj.EmployeePlanDetails.Add(new EmployeePlanDetail()
                        {
                            SN = i
                        });
                    }
                }
                
                gridPlan.Store[0].DataSource = obj.EmployeePlanDetails;
                gridPlan.Store[0].DataBind();

                wPlan.Center();
                wPlan.Show();
            }
        }

        protected void btnDelete_Click(object sender, DirectEventArgs e)
        {
            int planId = int.Parse(hdnPlanId.Text);
            Status status = NewHRManager.DeleteEmployeePlan(planId);
            if (status.IsSuccess)
            {
                NewMessage.ShowNormalMessage("Record deleted successfully.");
                X.Js.Call("searchList");
            }
            else
                NewMessage.ShowWarningMessage(status.ErrorMessage);
        }

        protected void Store_ReadData(object sender, StoreReadDataEventArgs e)
        {
            int totalRecords = 0;
            DateTime? startDate = null, endDate = null;

            int type = 0;
            bool all = false;

            if (TabPanel1.ActiveTabIndex == 0)
                type = 1;
            else if (TabPanel1.ActiveTabIndex == 1)
                type = 2;
            else if (TabPanel1.ActiveTabIndex == 2)
                type = 3;
            else if (TabPanel1.ActiveTabIndex == 3)
            {
                type = -1;
                all = true;

                if (txtFromDateFilter.SelectedDate != new DateTime())
                {
                    startDate = txtFromDateFilter.SelectedDate;

                    if (txtToDateFilter.SelectedDate == new DateTime())
                    {
                        NewMessage.ShowWarningMessage("To Date is required for date filter.");
                        txtToDateFilter.Focus();
                        return;
                    }
                }

                if (txtToDateFilter.SelectedDate != new DateTime())
                {
                    endDate = txtToDateFilter.SelectedDate;

                    if (txtFromDateFilter.SelectedDate == new DateTime())
                    {
                        NewMessage.ShowWarningMessage("From Date is required for date filter.");
                        txtFromDateFilter.Focus();
                        return;
                    }
                }
            }

            int pageSize = int.Parse(cmbPageSize.SelectedItem.Value); ;
            List<GetEmployeePlanListForEmployeeResult> list = NewHRManager.GetEmployeePlanListForEmp(e.Start / pageSize, pageSize, SessionManager.CurrentLoggedInEmployeeId, type, startDate, endDate, all);

            foreach (var item in list)
            {
                if (!NewHRManager.IsEmployeePlanUsedInActivities(item.PlanId))
                    item.IsEditable = 1;
            }

            if (list.Count > 0)
                totalRecords = list[0].TotalRows.Value;

            e.Total = totalRecords;
         
            storePlanList.DataSource = list;
            storePlanList.DataBind();

        }

        [DirectMethod]
        public static string GetGrid(Dictionary<string, string> parameters)
        {
            int planId = int.Parse(parameters["id"]);

            EmployeePlan obj = NewHRManager.GetEmployeePlanById(planId);
            if (obj != null)
            {
                List<object> data = new List<object>();

                foreach (var item in obj.EmployeePlanDetails)
                {
                    data.Add(new { SN = item.SN, Description = item.Description, TotalTimeDate = item.TotalTime });
                }

                int count = obj.EmployeePlanDetails.Count();
                int height = 32 + 32 * count;

                GridPanel grid = new GridPanel
                {
                    Height = height,
                    Width = 690,
                    Scroll = ScrollMode.Vertical,
                    EnableColumnHide = false,
                    Store = 
                { 
                    new Store 
                    { 
                        Model = {
                            new Model {
                                IDProperty = "SN",
                                Fields = 
                                {
                                    new ModelField("SN"),
                                    new ModelField("Description"),
                                    new ModelField("TotalTimeDate")
                                }
                            }
                        },
                        DataSource = data
                    }
                },
                    ColumnModel =
                    {
                        Columns = 
                    { 
                        new Column { Text = "SN", DataIndex = "SN", Width=140, Sortable = false, MenuDisabled = true },
                        new Column { Text = "Description", DataIndex = "Description", Width=700, Sortable = false, MenuDisabled = true, Wrap = true },
                        new Column { Text = "Time", DataIndex = "TotalTimeDate", Width=150, Sortable = false, MenuDisabled = true  }
                    }
                    }
                };

                grid.Width = 690;
                return ComponentLoader.ToConfig(grid);
            }
            else
                return "";
        }


    }
}