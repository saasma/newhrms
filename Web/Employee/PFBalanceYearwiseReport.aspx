<%@ Page Title="PF Balance Yearly Report" Language="C#" MasterPageFile="~/Master/EmployeeMS.Master"
    AutoEventWireup="true" CodeBehind="PFBalanceYearwiseReport.aspx.cs" Inherits="Web.Employee.PFBalanceYearwiseReport" %>

<%@ Register Src="../UserControls/PFBalanceYearwiseReportUC.ascx" TagName="PFBalanceYearwiseReportUC" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
  <h4 style="margin-left:20px">PF Balance Yearly Report</h4>
<ext:ResourceManager ID="ResourceManager1" DisableViewState="false"  runat="server" ShowWarningOnAjaxFailure="false"
    ScriptMode="Release" Namespace="Rigo" />
    <div style='margin-top:15px'>
    <uc1:PFBalanceYearwiseReportUC Id="PFBalanceYearwiseReportUC1" runat="server" />
    </div>
</asp:Content>
