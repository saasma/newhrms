﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//using Web.CP.Report.Templates;

using BLL;
using DevExpress.XtraReports.UI;
using Web.ReportDataSetTableAdapters;
using Utils.Calendar;
using BLL.Base;
using BLL.Manager;
using DAL;
using BLL.BO;
using System.Data;
using DevExpress.XtraPrinting;

namespace Web.CP.Report
{
    public partial class LeaveRecord : BasePage
    {


       
        private int Name_Column_Index = 1;

        protected void LoadReportHandler()
        { }

        protected void Page_Load(object sender, EventArgs e)
        {
            report.Filter.PayrollTo = true;
            report.Filter.Leave = true;
            report.Filter.Department = false;
            report.Filter.Branch = false;
            report.Filter.Employee = false;

            if (IsPostBack)
                LoadReport();
        }

        private void Event_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (Web.CP.Report.ReportHelper.IsReportExportState())
            {
                ((XRLabel)sender).BackColor = System.Drawing.Color.Transparent;
                ((XRLabel)sender).BorderColor = System.Drawing.Color.Black;
            }

        }

        protected void ClearBorderControlEvent(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            ReportHelper.TableCell_BeforePrintEvent(sender, e);
        }
        protected void ClearBorderLessControlEvent(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            ReportHelper.TableCell_BeforePrintEvent(sender, e);
        }

        private void CreateUsingLabels(XtraReport report, List<TextValue> headerList)
        //Report.Templates.Pay.SalarySummary report, List<CalcGetHeaderListResult> headerList)
        {
            DataTable dataTable = (report.DataSource as DataSet).Tables[0];
            int firstSecondColumnExtraWidth = 120;

            Color colorHeader = Color.FromArgb(201, 214, 237);
            Color colorEven = Color.FromArgb(239, 243, 250);

            int colCount = dataTable.Columns.Count; ;
            int colWidth = 40;

            report.PageWidth = (colWidth * colCount) + 200;

            //(report as Report.Templates.Pay.SalarySummary).labelTitle.WidthF = report.PageWidth;

            // report.Bands[BandKind.PageHeader].HeightF += 100;

            int currentXLocation = 0;
            int prevWidth = 0;
            int currentWidth = 0;

            // Create header group captions
            XRLabel labelGroup = null;
            bool newLabel = false;
            for (int i = 0; i < colCount; i++)
            {
                newLabel = false;
                if (i == 0 || i == 1 || i == 2)
                {
                    labelGroup = new XRLabel();

                    if (i == 0 || i == 1)
                        currentWidth = firstSecondColumnExtraWidth;
                    else if (i == 2)
                        currentWidth = colWidth;

                    labelGroup.Size = new Size(currentWidth, 40);
                    newLabel = true;


                    labelGroup.Borders = DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom;


                }
                else if (dataTable.Columns[i].Caption == "+") //for second last one
                {
                    labelGroup = new XRLabel();

                    //if last pair then only 2
                    if (i >= colCount - 3)
                        currentWidth = colWidth * 2;
                    else
                        currentWidth = colWidth * 3;
                    labelGroup.Size = new Size(currentWidth, 40);
                    newLabel = true;

                    labelGroup.Text = dataTable.Columns[i].ColumnName.Replace("Earned", "").Replace("_", "");

                    labelGroup.Borders = DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom;

                }

                if (newLabel)
                {

                    currentXLocation += (prevWidth);
                    prevWidth = currentWidth;

                    labelGroup.TextAlignment = TextAlignment.MiddleCenter;
                    labelGroup.BeforePrint += new System.Drawing.Printing.PrintEventHandler(Event_BeforePrint);

                    labelGroup.Location = new Point(currentXLocation, 10);

                    labelGroup.Padding = new PaddingInfo(2, 2, 2, 2);
                    labelGroup.Font = new Font(labelGroup.Font.FontFamily, labelGroup.Font.Size + 1, FontStyle.Bold);
                    labelGroup.CanGrow = false;//dont allow to grow as design will be meshed up 
                    ReportHelper.HeaderLabelStyle(labelGroup);
                    // Place the headers onto a PageHeader band
                    report.Bands[BandKind.PageHeader].Controls.Add(labelGroup);
                }
            }

            currentXLocation = 0;
            prevWidth = 0;
            currentWidth = 0;
            // Create header captions
            for (int i = 0; i < colCount; i++)
            {
                XRLabel label = new XRLabel();
                label.BeforePrint += new System.Drawing.Printing.PrintEventHandler(Event_BeforePrint);
                label.TextAlignment = TextAlignment.MiddleLeft;

                if (i == 0 || i == 1)
                {
                    currentWidth = firstSecondColumnExtraWidth; //for SN
                }
                else
                {
                    currentWidth = colWidth;//for other amount columns
                    label.TextAlignment = TextAlignment.MiddleCenter;
                }

                currentXLocation += (prevWidth);
                prevWidth = currentWidth;

                label.Location = new Point(currentXLocation, 50);
                label.Text = dataTable.Columns[i].Caption;
                label.Padding = new PaddingInfo(2, 2, 2, 2);

                label.Font = new Font(label.Font.FontFamily, label.Font.Size + 1, FontStyle.Bold);
                label.CanGrow = false;//dont allow to grow as design will be meshed up

                if (i == 0)
                {
                    label.Borders = DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom;
                    label.Size = new Size(currentWidth, 50);
                }
                else
                    label.Borders = DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom;

                label.Size = new Size(currentWidth, 50);

                ReportHelper.HeaderLabelStyle(label);
                // Place the headers onto a PageHeader band
                report.Bands[BandKind.PageHeader].Controls.Add(label);
            }


            currentXLocation = 0;
            prevWidth = 0;
            currentWidth = 0;
            // Create data-bound labels with different odd and even backgrounds
            for (int i = 0; i < colCount; i++)
            {
                XRLabel label = new XRLabel();
                label.BeforePrint += new System.Drawing.Printing.PrintEventHandler(Event_BeforePrint);

                if (i == 0 || i == 1)
                {
                    currentWidth = firstSecondColumnExtraWidth;
                    label.TextAlignment = TextAlignment.MiddleLeft;
                }

                else
                {
                    currentWidth = colWidth;
                    label.TextAlignment = TextAlignment.MiddleRight;
                }
                currentXLocation += (prevWidth);
                prevWidth = currentWidth;

                // if (i == Name_Column_Index)
                label.Location = new Point(currentXLocation, 0);
                //else
                //{
                //    label.Location = new Point((colWidth * i) + firstColumnExtraWidth, 0);                   
                //}
                label.Padding = new PaddingInfo(2, 2, 2, 0);
                if (i == Name_Column_Index)
                    label.DataBindings.Add("Text", null, dataTable.Columns[i].ColumnName);
                else
                    label.DataBindings.Add("Text", null, dataTable.Columns[i].ColumnName, "{0:0.##}");




                label.CanGrow = false;



                //if (i != Name_Column_Index)
                //{
                if (i == 0)

                    label.Borders = DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom;

                else
                    label.Borders = DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom;

                label.Size = new Size(currentWidth, 20);
                //}
                //else
                //{
                //    label.Borders = DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom;
                //    label.TextAlignment = TextAlignment.MiddleLeft;
                //    label.Size = new Size(colWidth + firstColumnExtraWidth, 20);

                //}
                label.Font = new Font(label.Font.FontFamily, label.Font.Size + 1, FontStyle.Regular);

                //label.BackColor = i == 0 ? colorEven : Color.White;
                ReportHelper.LabelStyle(label);

                // Place the labels onto a Detail band
                report.Bands[BandKind.Detail].Controls.Add(label);
            }


        }


        protected void LoadReport()
        {
            PayrollPeriod payrollPeriodFrom = CommonManager.GetPayrollPeriod(report.Filter.StartDate.Month,
                                                                         report.Filter.StartDate.Year);
            PayrollPeriod payrollPeriodTo = CommonManager.GetPayrollPeriod(report.Filter.EndDate.Month,
                                                                                     report.Filter.EndDate.Year);
            //if null get first payroll period
            if (payrollPeriodFrom == null)
                payrollPeriodFrom = CommonManager.GetFirstPayrollPeriod(SessionManager.CurrentCompanyId);

            if (payrollPeriodTo == null)
                payrollPeriodTo = CommonManager.GetLastPayrollPeriod();

            //again null then no report
            if (payrollPeriodFrom == null || payrollPeriodTo == null)
                return;

            //for report to date should be greater or equal to from date
            if (payrollPeriodTo.PayrollPeriodId < payrollPeriodFrom.PayrollPeriodId)
                return;

           



            Report.Templates.Pay.ConsolidatedLeaveReport mainReport = new Web.CP.Report.Templates.Pay.ConsolidatedLeaveReport();
            mainReport.Name = "Consolidated Leave Report";

            //mainReport.labelTitle.Text =
            //    string.Format("Consolidated Leave Summary : {0}({2}) to {1}({3}) ",payrollPeriodFrom.Name,payrollPeriodTo.Name
            //    ,DateHelper.GetMonthsForOtherPayrollPeriod(payrollPeriodFrom.Month,IsEnglish),DateHelper.GetMonthsForOtherPayrollPeriod(payrollPeriodTo.Month,IsEnglish));

            List<Report_HR_ConsolidatedLeaveBalanceResult> data = ReportManager.GetLeaveBalances(
                payrollPeriodFrom.PayrollPeriodId,
                payrollPeriodTo.PayrollPeriodId, report.Filter.EmployeeName, report.Filter.LeaveTypeId, report.Filter.BranchId, report.Filter.DepartmentId,
                 SessionManager.CurrentLoggedInEmployeeId, report.Filter.DepartmentName,true);




            List<TextValue> headerList = ReportManager.GetConsolidatedLeaveHeaderList(payrollPeriodFrom.PayrollPeriodId, payrollPeriodTo.PayrollPeriodId);

            //headerList = CalculationValue.SortHeaders(headerList,PayManager.GetIncomeOrderValues(),PayManager.GetDeductionOrderValues());



            DataTable dataTable = CreateDataTable(data, headerList, payrollPeriodFrom.PayrollPeriodId, payrollPeriodTo.PayrollPeriodId);
            DataSet dset = new DataSet();
            dset.Tables.Add(dataTable);
            mainReport.DataSource = dset;
            mainReport.DataMember = "Report";




            CreateUsingLabels(mainReport, headerList);

            if (dataTable.Rows.Count > 0)
                report.DisplayReport(mainReport);

            if (ReportHelper.IsReportExportState())
            {
                this.report.clearCache = true;
            }
            // report.ReportToolbar.Width = new Unit(958);
            //report.ReportViewer.Width = new Unit(mainReport.PageWidth);

        }

        private DataTable CreateDataTable(List<Report_HR_ConsolidatedLeaveBalanceResult> data, List<TextValue> headerList, int payrollStartId, int payrollEndId)
        {
            DataTable dataTable = new DataTable();
            dataTable.TableName = "Report";


            //dataTable.Columns.Add("SN", typeof(string));
            dataTable.Columns.Add("Name", typeof(string));
            dataTable.Columns.Add("Leave", typeof(string));
            //dataTable.Columns.Add("CostCode", typeof(string));

            // Create table cells, fill the header cells with text, bind the cells to data
            for (int i = 2; i < headerList.Count; i++)
            {
                DataColumn column = new DataColumn(headerList[i].Value, typeof(decimal));
                column.Caption = headerList[i].Text;
                dataTable.Columns.Add(column);
            }

            //bool isFirstColumn = true;

            var empLeaveListGroup = data.GroupBy(x => new { x.EIN, x.Title },
                (key, group) => new { EIN = key.EIN, Leave = key.Title, Count = group.Count() }).ToList();



            List<PayrollPeriod> payrollList = CommonManager.GetPayrollList(payrollStartId, payrollEndId);

            //add rows


            //foreach (Report_HR_ConsolidatedLeaveBalanceResult row in data)
            foreach (var empLeave in empLeaveListGroup)
            {

                List<object> list = new List<object>();

                List<Report_HR_ConsolidatedLeaveBalanceResult> leaveList = data.Where(x => x.EIN == empLeave.EIN && x.Title == empLeave.Leave).ToList();

                if (leaveList.Count <= 0)
                    continue;


                list.Add(leaveList[0].Name);
                list.Add(leaveList[0].Title);

                decimal earned = 0;
                decimal taken = 0;


                //for (int i = 2; i < headerList.Count; i++)
                for (int i = 0; i < payrollList.Count; i++)
                {
                    Report_HR_ConsolidatedLeaveBalanceResult payrollEmpLeave = data.SingleOrDefault(x =>
                        x.EIN == empLeave.EIN && x.PayrollPeriod == payrollList[i].Name && x.Title == empLeave.Leave);

                    if (payrollEmpLeave == null)
                    {
                        payrollEmpLeave = new Report_HR_ConsolidatedLeaveBalanceResult();

                    }

                    if (i == 0)
                    {
                        // Add opening for first payroll
                        earned += Convert.ToDecimal(payrollEmpLeave.BF);
                        list.Add(payrollEmpLeave.BF);
                    }

                    list.Add((payrollEmpLeave.Earned == 0 ? null : payrollEmpLeave.Earned));
                    list.Add((payrollEmpLeave.Taken == 0 ? null : payrollEmpLeave.Taken));
                    list.Add((payrollEmpLeave.Balance == 0 ? null : payrollEmpLeave.Balance));

                    earned += Convert.ToDecimal(payrollEmpLeave.Earned);
                    taken += Convert.ToDecimal(payrollEmpLeave.Taken);


                    //list.Add(row.GetCellValue(headerList[i].Type.Value, headerList[i].SourceId.Value, 2, null));
                }

                //list.Add(earned);
                //list.Add(taken);

                dataTable.Rows.Add(list.ToArray());
            }



            return dataTable;


        }
    }
}
