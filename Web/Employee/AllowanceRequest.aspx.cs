﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using System.Xml;
using BLL.BO;
using Web.Helper;

namespace Web.Employee
{
    public partial class AllowanceRequest : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();
              
            }
        }

        public void Initialise()
        {

            //ColumnDaysOrHours.Text = AllowanceManager.AllowanceDaysOrHourLabel;
            //txtDays.FieldLabel = AllowanceManager.AllowanceDaysOrHourLabel + " *";

            List<EveningCounterType> listECT = AllowanceManager.GetAllEveningCounterList()
                .Where(x => x.DoNotShowInEmployeePortal == false || x.DoNotShowInEmployeePortal == null).ToList();

            storeCounterType.DataSource = listECT;
            storeCounterType.DataBind();

            listECT.Insert(0, new EveningCounterType() { Name="All", EveningCounterTypeId = -1 });
            storeCounterTypeFilter.DataSource = listECT;
            storeCounterTypeFilter.DataBind();
            cmbCounterTypeFilter.SelectedItem.Value = "-1";

            List<TextValue> listReview = LeaveRequestManager.GetApplyToListForEmployee(SessionManager.CurrentLoggedInEmployeeId, false, PreDefindFlowType.Overtime);
            List<TextValue> listApproval = LeaveRequestManager.GetApplyToListForEmployee(SessionManager.CurrentLoggedInEmployeeId, true, PreDefindFlowType.Overtime);

            storeRecomender.DataSource = listReview;
            storeRecomender.DataBind();

            if (listReview.Count == 1)
                ExtControlHelper.ComboBoxSetSelected(listReview[0].Value, cmbRecomender);

            storeApprover.DataSource = listApproval;
            storeApprover.DataBind();

            if (listApproval.Count == 1)
                ExtControlHelper.ComboBoxSetSelected(listApproval[0].Value, cmbApprover);

            Load();

            //LoadLevels();
            PagingToolbar1.DoRefresh();
            txtFromDate.SelectedDate = CommonManager.GetCurrentDateAndTime();
            txtToDate.SelectedDate = CommonManager.GetCurrentDateAndTime();

            txtFromDateNep.Text = DateManager.GetAppropriateDate(CommonManager.GetCurrentDateAndTime());
            txtToDateNep.Text = DateManager.GetAppropriateDate(CommonManager.GetCurrentDateAndTime());


            //txtDays.Text = "1";


        }

        public new void Load()
        {
            List<PIncome> list = PayManager.GetFixedIncomeListByCompany(SessionManager.CurrentCompanyId);

            Node rootNode = new Node();

            foreach (PIncome module in list)
            {
                Node moduleChild = new Node();
                moduleChild.Checked = false;
                moduleChild.Expanded = true;


                moduleChild.CustomAttributes.Add(new ConfigItem { Name = "Title", Value = module.Title });
                moduleChild.CustomAttributes.Add(new ConfigItem { Name = "IncomeId", Value = module.IncomeId.ToString() });

                rootNode.Children.Add(moduleChild);


            }

        }

        public bool IsContainsPage(int incomeId, List<OvertimeTypeIncome> pages)
        {
            foreach (OvertimeTypeIncome page in pages)
            {
                if(page.IncomeId == incomeId)
                    return true;
            }
            return false;
        }


        public void LoadNodes(List<OvertimeTypeIncome> pages)
        {
      

            Node rootNode = new Node();

            List<PIncome> list = PayManager.GetFixedIncomeListByCompany(SessionManager.CurrentCompanyId);

            foreach (PIncome module in list)
            {
                Node moduleChild = new Node();
                moduleChild.Checked = false;
                moduleChild.Expanded = true;
                moduleChild.Leaf = true;


                if (pages != null && IsContainsPage(module.IncomeId,pages))
                {
                    moduleChild.Checked = true;
                }
                else
                {
                    moduleChild.Checked = false;
                }

                moduleChild.CustomAttributes.Add(new ConfigItem { Name = "Title", Value = module.Title });
                moduleChild.CustomAttributes.Add(new ConfigItem { Name = "IncomeId", Value = module.IncomeId.ToString() });


                rootNode.Children.Add(moduleChild);

            }

            //treePanel.SetRootNode(rootNode);
        }

        private void LoadLevels()
        {
            
            //int EmpID = SessionManager.CurrentLoggedInEmployeeId;
            //GridLevels.GetStore().DataSource = AllowanceManager.GetEveningCounterRequestForEmp(EmpID);
            //GridLevels.GetStore().DataBind();

            X.Js.AddScript("searchList();");
        }


        
        public void ClearLevelFields()
        {
            hiddenValue.Text = "";
            txtFromDate.Text = "";
            cmbCounterType.Clear();
            //txtToDate.Text = "";

        }

      
        
        protected void btnAddLevel_Click(object sender, DirectEventArgs e)
        {
            ClearLevelFields();
        }


        protected void Date_Change(object sender, DirectEventArgs e)
        {
            

            DateTime TodayDate = new DateTime();
            TodayDate = CommonManager.GetCurrentDateAndTime();


            string FromDateNep;
            string EndDateNep;




            if (txtFromDate.SelectedDate != null &&
                txtFromDate.SelectedDate.Date > TodayDate.AddYears(-25).Date && txtFromDate.SelectedDate.Date < TodayDate.AddYears(25).Date)
            {
                FromDateNep = DateManager.GetAppropriateDate(txtFromDate.SelectedDate);
                txtFromDateNep.Text = FromDateNep;
            }
            else
            {
                txtFromDateNep.Text = "";
                txtFromDate.SelectedDate = CommonManager.GetCurrentDateAndTime();
            }



            if (txtToDate.SelectedDate != null &&
                txtToDate.SelectedDate.Date > TodayDate.AddYears(-25).Date && txtToDate.SelectedDate.Date < TodayDate.AddYears(25).Date)
            {
                EndDateNep = DateManager.GetAppropriateDate(txtToDate.SelectedDate);
                txtToDateNep.Text = EndDateNep;

            }
            else
            {
                txtToDateNep.Text = "";
                txtToDate.SelectedDate = CommonManager.GetCurrentDateAndTime();
            }




            if (txtFromDate.SelectedDate != null && txtToDate.SelectedDate != null &&
                txtFromDate.SelectedDate.Date > TodayDate.AddYears(-25).Date && txtFromDate.SelectedDate.Date < TodayDate.AddYears(25).Date &&
                txtToDate.SelectedDate.Date > TodayDate.AddYears(-25).Date && txtToDate.SelectedDate.Date < TodayDate.AddYears(25).Date)
            {
                if (txtFromDate.SelectedDate.Date > txtToDate.SelectedDate.Date)
                {
                    txtToDate.SelectedDate = txtFromDate.SelectedDate.Date;
                }
                
            }



        }

        protected void btnDeleteLevel_Click(object sender, DirectEventArgs e)
        {
            int overtimeTypeId = int.Parse(hiddenValue.Text.Trim());
            Status status = AllowanceManager.DeleteEveningCounterRequest(overtimeTypeId);
            if (status.IsSuccess)
            {
                //LoadLevels();
                PagingToolbar1.DoRefresh();
                NewMessage.ShowNormalPopup("Allowance Request deleted.");
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }
        }

        

        protected void btnEditLevel_Click(object sender, DirectEventArgs e)
        {

            int requestID = int.Parse(hiddenValue.Text.Trim());
            DAL.EveningCounterRequest counter = AllowanceManager.GetEveningCounterByID(requestID);
            if (counter != null)
            {
                if (counter.StartDate != null)
                {
                    txtFromDate.SelectedDate = counter.StartDate.Value;
                }

                txtFromDateNep.Text = DateManager.GetAppropriateDate(counter.StartDate.Value);
                txtToDateNep.Text = DateManager.GetAppropriateDate(counter.EndDate.Value);

                txtToDate.SelectedDate = counter.EndDate.Value;
                if (counter.Days == null)
                    txtDays.Text = "";
                else
                    txtDays.Text = counter.Days.ToString();

                //txtDays.Number = counter.Days.Value;
                
                if (counter.EveningCounterTypeId != null)
                {
                    //cmbCounterType.SetValue(counter.EveningCounterTypeId);
                    cmbCounterType.SetValue(counter.EveningCounterTypeId.ToString());

                }

                if (counter.ApprovalID != null)
                {
                    cmbApprover.SetValue(counter.ApprovalID.ToString());
                }

                if (counter.RecommendedBy != null)
                {
                    cmbRecomender.SetValue(counter.RecommendedBy.ToString());
                }

                txtReason.Text = counter.Reason;
            }
            windowAddEditOvertime.Show();

        }



        protected void btnDetailLevel_Click(object sender, DirectEventArgs e)
        {
            //int overtypeId = int.Parse(hiddenValue.Text.Trim());
            //List<OvertimeTypeLevel> detailGridList = OvertimeManager.getAllowanceDetailList(overtypeId);
           
        }


        public void btnOvertimeType_Click(object sender, DirectEventArgs e)
        {
            //if (Page.IsValid)
            {
                DAL.EveningCounterRequest request = new DAL.EveningCounterRequest();
                bool isInsert = string.IsNullOrEmpty(hiddenValue.Text.Trim());
                if (!isInsert)
                    request.CounterRequestID = int.Parse(hiddenValue.Text.Trim());

                if (txtFromDate.SelectedDate == DateTime.MinValue)
                {
                    NewMessage.ShowWarningMessage("Invalid from date.");
                    return;
                }

                if (txtFromDate.SelectedDate != null)
                {
                    request.StartDate = txtFromDate.SelectedDate;

                }
                else
                {
                    NewMessage.ShowWarningMessage("From Date is required");
                    return;
                }

                if (txtToDate.SelectedDate != null)
                {
                    request.EndDate = txtToDate.SelectedDate; 
                }
                else
                {
                    NewMessage.ShowWarningMessage("To Date is required");
                    return;
                }

                //if (txtToDate.SelectedDate != null)
                //    request.EndDate = txtToDate.SelectedDate;
                //else
                //{
                //   NewMessage.ShowNormalPopup("To Date is required");
                //   return;
                //}

                if (cmbCounterType.SelectedItem != null && cmbCounterType.SelectedItem.Index != -1)
                    request.EveningCounterTypeId = int.Parse(cmbCounterType.SelectedItem.Value);
                else
                {
                    NewMessage.ShowNormalPopup("Allowance Type is required");
                    return;
                }

                if (cmbRecomender.SelectedItem != null && cmbRecomender.SelectedItem.Index != -1)
                    request.RecommendedBy = int.Parse(cmbRecomender.SelectedItem.Value);
                else
                {
                    NewMessage.ShowNormalPopup("Recommender is required");
                    return;
                }

                if (cmbApprover.SelectedItem != null && cmbApprover.SelectedItem.Index != -1)
                    request.ApprovalID = int.Parse(cmbApprover.SelectedItem.Value);
                else
                {
                    NewMessage.ShowNormalPopup("Approval is required");
                    return;
                }


                string daysMathStr = ((txtToDate.SelectedDate.Date - txtFromDate.SelectedDate.Date).TotalDays + 1).ToString();

                string daysUserStr = txtDays.Text;
                float daysUser;
                float daysMath;
                bool isValidDaysMath = float.TryParse(daysMathStr, out daysMath);
                bool isValidDaysUser = float.TryParse(daysUserStr, out daysUser);

                EveningCounterType allowanceType = BLL.BaseBiz.PayrollDataContext.EveningCounterTypes.FirstOrDefault(x => x.EveningCounterTypeId
                    == request.EveningCounterTypeId);

                if (CommonManager.CompanySetting.WhichCompany != WhichCompany.HPL && allowanceType.DoNowShowDaysWarning != true)
                {
                    if (isValidDaysUser && isValidDaysMath)
                    {
                        if (daysUser > daysMath)
                        {
                            NewMessage.ShowWarningMessage("Number of units between start and end date is higher than expected,max unit is " + daysMath + ".");
                            return;
                        }
                        else
                        {
                            request.Days = daysUser;
                        }
                    }
                    else
                    {
                        NewMessage.ShowWarningMessage("Invalid units input.");
                        return;
                    }
                }
                else
                    request.Days = daysUser;

                request.Reason = txtReason.Text;
                request.Status = (int)EveningCounterStatusEnum.Pending;

                request.EmployeeID = SessionManager.CurrentLoggedInEmployeeId;
                request.CreatedBy = SessionManager.CurrentLoggedInUserID;
                request.CreatedOn = CommonManager.GetCurrentDateAndTime();

                Status status = AllowanceManager.InsertUpdateEveningRequest(request, isInsert);
                if (status.IsSuccess)
                {
                    windowAddEditOvertime.Hide();
                    //LoadLevels();
                    PagingToolbar1.DoRefresh();
                    NewMessage.ShowNormalPopup("Allowance Request saved.");
                    //PagingToolbar1.DoRefresh();
                }
                else
                {
                    NewMessage.ShowWarningMessage(status.ErrorMessage);
                }
            }

        }

        protected void btnDetailSaveUpdate_Click(object sender, DirectEventArgs e)
        {
            List<EveningCounterTypeLevel> lines = new List<EveningCounterTypeLevel>();
            string json = e.ExtraParams["Values"];

            if (string.IsNullOrEmpty(json))
            {
                return;
            }


            lines = JSON.Deserialize<List<EveningCounterTypeLevel>>(json);

            Status status = OvertimeManager.InsertUpdateAllowanceLevelDetail(lines);
            if (status.IsSuccess)
            {
                //LoadLevels();
                PagingToolbar1.DoRefresh();
                NewMessage.ShowNormalPopup("Allowance rate saved.");
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }


        }


        protected void btnLevelSaveUpdate_Click(object sender, DirectEventArgs e)
        {

             
        }



        protected void RefreshGrid(object s, StoreReadDataEventArgs e)
        {
            BindActivityGrid();
        }

        private void BindActivityGrid()
        {
            //string name = txtName.Text;
            //int? branchId, levelId;
            //DateTime startDate, endDate;
            //int groupBy;

            //if (dateStart.IsEmpty || dateEnd.IsEmpty)
            //{
            //    NewMessage.ShowWarningMessage("Both start and end date must be selected.");
            //    return;
            //}
            //else
            //{
            //    startDate = dateStart.SelectedDate;
            //    endDate = dateEnd.SelectedDate;
            //}

            //if (cmbBranch.SelectedItem.Value == null)
            //{
            //    branchId = null;
            //}
            //else
            //{
            //    branchId = int.Parse(cmbBranch.SelectedItem.Value);
            //}

            //if (cmbLevel.SelectedItem.Value == null)
            //{
            //    levelId = null;
            //}
            //else
            //{
            //    levelId = int.Parse(cmbLevel.SelectedItem.Value);
            //}

            //groupBy = cmbGroupBy.SelectedItem.Index;

            //this.storeActivity.DataSource = ActivityManager.GetVerifiedActivities(name, branchId, levelId, groupBy, startDate, endDate, IsAdmin);
            //this.storeActivity.DataBind();
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            List<GetEveningCounterRequestForEmpSPResult> resultSet = AllowanceManager.GetEveningCounterRequestForEmpReport(0, 0, SessionManager.CurrentLoggedInEmployeeId, int.Parse(cmbType.SelectedItem.Value),
                int.Parse(cmbStatus.SelectedItem.Value), int.Parse(cmbCounterTypeFilter.SelectedItem.Value));

            foreach (var item in resultSet)
            {
                item.FormattedStartDate = WebHelper.FormatDateForExcel(item.StartDate.Value);
                item.FormattedEndDate = WebHelper.FormatDateForExcel(item.EndDate.Value);          
            }

            Dictionary<string, string> title = new Dictionary<string, string>();
            title["Allowance Request Report"] = "";

            Bll.ExcelHelper.ExportToExcel("Allowance Request Report", resultSet,
                   new List<String>() { "CounterRequestID", "EmployeeID", "CreatedBy", "CreatedOn", "ApprovalID", "ApprovalName",
                    "ForwardedOn", "ForwardedBy", "PayrollPeriodId", "ApprovedOn", "ApprovedBy", "RecommendedBy", "RecommendedOn", "RejectedBy", "RejectedOn",
                    "EveningCounterTypeId", "MonthName", "MonthTotalDays", "Status", "StartDate", "EndDate", "TotalRows", "RowNumber" },
               new List<String>() { },
               new Dictionary<string, string>() { { "EveningCounterTypeStr", "Allowance Type" }, { "FormattedStartDate", "Start Date" }, { "FormattedEndDate", "End Date" }, { "StatusStr", "Status" } },
               new List<string>() { }
               , new List<string> { }
               , new List<string> { "FormattedStartDate", "FormattedEndDate" }
               , new Dictionary<string, string>() { }
               , new List<string> { "EveningCounterTypeStr", "FormattedStartDate", "FormattedEndDate", "Days", "Reason", "StatusStr" });


        }
 
    }
}

/*
  
 
 
*/