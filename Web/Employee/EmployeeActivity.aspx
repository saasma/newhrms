﻿<%@ Page Title="Employee Activity" Language="C#" MasterPageFile="~/Master/EmployeeExt.Master"
    AutoEventWireup="true" CodeBehind="EmployeeActivity.aspx.cs" Inherits="Web.Employee.EmployeeActivity" %>

<%@ Register Src="~/Employee/UserControls/EmpActivityCtrl.ascx" TagName="ea" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">

    
      var CommandHandler = function(command, record){
            <%= hdnActivityId.ClientID %>.setValue(record.data.ActivityId);            
            <%= hdnDetailId.ClientID %>.setValue(record.data.DetailId);
                if(command=="Edit")
                {
                    <%= btnEdit.ClientID %>.fireEvent('click');
                }
                else if(command == "Delete")
                {
                    <%= btnDelete.ClientID %>.fireEvent('click');
                }
                else
                {
                    <%= btnView.ClientID %>.fireEvent('click');
                }
           }

           var prepareActivity = function (grid, toolbar, rowIndex, record) {
                var editButton = toolbar.items.get(1);
                var deleteButton = toolbar.items.get(3);
                if (record.data.Status > 1) {
                    //editButton.setVisible(false);
                    deleteButton.setVisible(false);
                }

            } 


        function searchList() {
            <%=gridActivity.ClientID %>.getStore().pageSize = <%=cmbPageSize.ClientID %>.getValue();
            <%=PagingToolbar1.ClientID %>.doRefresh();
        }

        var RemoveItemLine = function (column, command, record, recordIndex, cellIndex) {
            var store = this.grid.store;
            store.remove(record);
        };

        var clientRenderer = function (value, metaData, record, rowIndex, colIndex, store) {

            var r = <%= storeClient.ClientID %>.getById(value.toString());
            if (Ext.isEmpty(r)) {
                return "";
            }
            return r.data.Name;
        };

         var typeRenderer = function (value, metaData, record, rowIndex, colIndex, store) {
            var r = <%= storeType.ClientID %>.getById(value.toString());
            if (Ext.isEmpty(r)) {
                return "";
            }
            return r.data.Name;
        };       
        

        window.addEventListener('keydown', function(e) {
          var key = e.keyCode || e.which;
          if (key == 8 /*BACKSPACE*/ || key == 46/*DELETE*/) {
            var len=window.location.href.length;
            if(window.location.href[len-1]!='#') window.location.href += "#";
          }
        },false);


      var rendererClass = function(value, meta, record) {
            meta.tdCls = 'blueCls';
            return value;
        }; 

    var getRowClass = function (record) {
            if (record.data.NoOfEmpComments > 0) {
                return "employeeCommentedCls";
            }
            else if(record.data.NoOfComments > 0) {
                return "commentCls";
            }
            else{
                return "new-row";
            }
        };

        
        var commentRenderer = function (value, metaData, record, rowIndex, colIndex, store) {

           if(value != null && value > 0)
                return 'Yes';
            else
             {
                if(record.data.NoOfComments > 0)
                    return 'No';
                else
                    return '';

              }
        };

    </script>
    <style type="text/css">
        .bottomBlock
        {
            clear: both;
            padding: 0 0 10px 10px;
            background: #D7E7FF;
            float: left;
            width: 1150px;
            margin-left: -5px;
            margin-top: 20px;
        }
        .blueCls
        {
            color:Blue !important;
        }
        .grid1 .x-panel-header
        {
            background-color:Transparent !important;        
        }
        .grid1 .x-header-text
        {
            color:Blue !important;       
        }
        .commentCls td
        {
            background-color: #FFF3C3 !important;
        }
        .employeeCommentedCls td
        {
            background-color: #C6E0B4 !important;
        }
        
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="mainContent" runat="server">
    <script type="text/javascript" src="<%= ResolveUrl("~/Employee/override.js?v=") + Web.Helper.WebHelper.Version %>"></script>
    <ext:Hidden ID="hdnActivityId" runat="server" />
    <ext:Hidden ID="hdnPlanId" runat="server" />
    <ext:Hidden ID="hdnIsNotDateChange" runat="server" />
    <ext:Hidden ID="hdnDetailId" runat="server" />

    <ext:LinkButton runat="server" Hidden="true" ID="btnEdit">
        <DirectEvents>
            <Click OnEvent="btnEdit_Click">
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:LinkButton>
    <ext:Button ID="btnDelete" runat="server" Hidden="true">
        <DirectEvents>
            <Click OnEvent="btnDelete_Click">
                <Confirmation ConfirmRequest="true" Message="Are you sure, you want to delete the record?" />
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:Button>

    <ext:LinkButton ID="btnView" runat="server" Hidden="true">
        <DirectEvents>
            <Click OnEvent="btnView_Click">
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:LinkButton>


        <uc1:ea ID="EmpActCtrl1" runat="server" />

    <div class="innerLR1">
        <h3 style="margin-top: 10px">
            My Daily Activities
        </h3>
        <ext:Store ID="storeClient" runat="server">
            <Model>
                <ext:Model ID="Model7" runat="server" IDProperty="ClientSoftwareId">
                    <Fields>
                        <ext:ModelField Name="ClientSoftwareId" Type="String" />
                        <ext:ModelField Name="Name" />
                    </Fields>
                </ext:Model>
            </Model>
        </ext:Store>
        <ext:Store ID="storeType" runat="server">
            <Model>
                <ext:Model ID="Model8" runat="server" IDProperty="ActivityType">
                    <Fields>
                        <ext:ModelField Name="ActivityType" Type="String"/>
                        <ext:ModelField Name="Name"/>
                    </Fields>
                </ext:Model>
            </Model>
        </ext:Store>
        <table>
            <tr>
                <td style="width: 140px;">
                    <ext:DateField ID="txtFromDateFilter" runat="server" Width="120px" LabelAlign="Top"
                        FieldLabel="From Date" LabelSeparator="">
                        <Plugins>
                            <ext:GenericPlugin ID="HighlightDates1" runat="server" InstanceName="Ext.ux.DatePicker.HighlightWithNepDates" />
                        </Plugins>
                    </ext:DateField>
                </td>
                <td style="width: 140px;">
                    <ext:DateField ID="txtToDateFilter" runat="server" Width="120px" LabelAlign="Top"
                        FieldLabel="To Date" LabelSeparator="">
                        <Plugins>
                            <ext:GenericPlugin ID="GenericPlugin1" runat="server" InstanceName="Ext.ux.DatePicker.HighlightWithNepDates" />
                        </Plugins>
                    </ext:DateField>
                </td>
                <td style="width: 220px; display: none;">
                    <ext:ComboBox ID="cmbClientFilter" runat="server" ValueField="ClientId" DisplayField="ClientName"
                        FieldLabel="Client Name" Width="200" LabelAlign="top" LabelSeparator="" ForceSelection="true"
                        QueryMode="Local" StoreID="storeClient">
                        <Triggers>
                            <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                        </Triggers>
                        <Listeners>
                            <Select Handler="this.getTrigger(0).show();" />
                            <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                            <TriggerClick Handler="if (index == 0) { 
                                    this.clearValue(); 
                                    this.getTrigger(0).hide();
                                }" />
                        </Listeners>
                    </ext:ComboBox>
                </td>
                <td style="width: 220px; display: none;">
                    <ext:ComboBox ID="cmbTypeFilter" runat="server" ValueField="ActivityTypeId" DisplayField="Name"
                        FieldLabel="Type" Width="200" LabelAlign="top" LabelSeparator="" ForceSelection="true"
                        QueryMode="Local" StoreID="storeType">
                        <Triggers>
                            <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                        </Triggers>
                        <Listeners>
                            <Select Handler="this.getTrigger(0).show();" />
                            <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                            <TriggerClick Handler="if (index == 0) { 
                                    this.clearValue(); 
                                    this.getTrigger(0).hide();
                                }" />
                        </Listeners>
                    </ext:ComboBox>
                </td>
                <td style="padding-top: 12px;">
                    <ext:Button runat="server" ID="btnLoad" Cls="btn btn-primary" Text="<i></i>Load">
                        <Listeners>
                            <Click Fn="searchList">
                            </Click>
                        </Listeners>
                    </ext:Button>
                </td>
                 <td style="padding-left: 12px;padding-top: 12px;">
                     <ext:Button runat="server" ID="btnAdd" Cls="btn btn-success" Text="<i></i>Add New Activity">
                        <DirectEvents>
                            <Click OnEvent="btnAdd_Click">
                                <EventMask ShowMask="true" />
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                
                </td>
            </tr>
        </table>
        <br />
       
        <ext:TabPanel ID="TabPanel1" runat="server" Width="1080">
            <Items>
                <ext:Panel ID="Panel1" runat="server" Title="This Week">
                    <Content>
                    </Content>
                </ext:Panel>
                <ext:Panel ID="panelRecommended" runat="server" Title="Last Week">
                    <Content>
                    </Content>
                </ext:Panel>
                <ext:Panel ID="Panel2" runat="server" Title="This Month">
                    <Content>
                    </Content>
                </ext:Panel>
                <ext:Panel ID="Panel3" runat="server" Title="All">
                    <Content>
                    </Content>
                </ext:Panel>
            </Items>
            <Listeners>
                <TabChange Fn="searchList" />
            </Listeners>
        </ext:TabPanel>

        <ext:GridPanel StyleSpec="margin-top:15px;" ID="gridActivity" runat="server" Cls="itemgrid"
            Scroll="None" Width="1100">
                <Store>
                    <ext:Store ID="storeActivity" runat="server" AutoLoad="true" OnReadData="Store_ReadData" GroupField="DateString"
                    RemotePaging="true" RemoteSort="true">
                    <Proxy>
                        <ext:PageProxy />
                    </Proxy>
                        <Model>
                            <ext:Model ID="Model3" runat="server" IDProperty="DetailId">
                                <Fields>
                                    <ext:ModelField Name="ActivityId" Type="Int" />
                                    <ext:ModelField Name="DetailId" Type="Int" />
                                    <ext:ModelField Name="DateEng" Type="Date" />
                                    <ext:ModelField Name="Status" Type="Int" />
                                    <ext:ModelField Name="NoOfActivities" Type="Int" />
                                    <ext:ModelField Name="StatusName" Type="String" />
                                    <ext:ModelField Name="NoOfComments" Type="Int" />
                                    <ext:ModelField Name="NoOfEmpComments" Type="Int" />
                                    <ext:ModelField Name="ActivityType" Type="String" />
                                    <ext:ModelField Name="Clients" Type="String" />
                                    <ext:ModelField Name="DateString" Type="String" />
                                    <ext:ModelField Name="Description" Type="String" />
                                    <ext:ModelField Name="ActivityDurationPeriod" Type="String" />
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                <ColumnModel>
                    <Columns>
                        <ext:Column ID="Column6" Sortable="false" MenuDisabled="true" runat="server" Text="Date"
                            Width="180" Align="Left" DataIndex="DateString">
                        </ext:Column>
                        <%--<ext:Column ID="Column6" Sortable="false" MenuDisabled="true" runat="server" Text="No of Activities"
                            Width="300" Align="Center" DataIndex="NoOfActivities">
                        </ext:Column>--%>
                        <ext:Column ID="Column15" Sortable="false" MenuDisabled="true" runat="server" Text="Activity"
                            Width="150" Align="Left" DataIndex="ActivityType">
                        </ext:Column>
                        <ext:Column ID="ColClients" Sortable="false" MenuDisabled="true" runat="server" Text="Clients"
                            Width="200" Align="Left" DataIndex="Clients">
                        </ext:Column>
                        <ext:Column ID="Column13" Sortable="false" MenuDisabled="true" runat="server"
                            Text="Details" Width="250" Align="Left" DataIndex="Description">
                        </ext:Column>
                          <ext:Column ID="Column1" Sortable="false" MenuDisabled="true" runat="server"
                            Text="No of Comments" Width="105" Align="Center" DataIndex="NoOfComments">
                        </ext:Column>
                        <ext:Column ID="Column7" Sortable="false" MenuDisabled="true" runat="server"
                            Text="Emp Comment" Width="105" Align="Center" DataIndex="NoOfEmpComments">
                            <Renderer Fn="commentRenderer" />
                        </ext:Column>
                       
                        <ext:Column ID="Column14" Sortable="false" MenuDisabled="true" runat="server" Text="Status"
                            Width="110" Align="Left" DataIndex="StatusName">
                        </ext:Column>
                       <ext:Column ID="Column2" Sortable="false" MenuDisabled="true" runat="server" Text="Hours:Minutes"
                            Width="95" Align="Center" DataIndex="ActivityDurationPeriod">
                        </ext:Column>

                        <ext:CommandColumn ID="CommandColumn3" runat="server" Width="85" Text="Action" Align="Center">
                            <Commands>
                                <ext:CommandSeparator />
                                <ext:GridCommand Cls="editGridButton" ToolTip-Text="Edit" Text="<i class='fa fa-pencil'></i>"
                                    CommandName="Edit" />
                                <ext:CommandSeparator />
                                <ext:GridCommand Cls="editGridButton" ToolTip-Text="Delete" Text="<i class='fa fa-trash-o'></i>"
                                    CommandName="Delete" />
                            </Commands>
                            <Listeners>
                                <Command Handler="CommandHandler(command,record);" />
                            </Listeners>
                            <PrepareToolbar Fn="prepareActivity" />
                        </ext:CommandColumn>
                        <ext:CommandColumn ID="CommandColumn4" runat="server" Width="60" Text="" Align="Center" Hidden="true">
                            <Commands>
                                <ext:GridCommand Text="View" CommandName="View" />
                            </Commands>
                            <Listeners>
                                <Command Handler="CommandHandler(command,record);" />
                            </Listeners>
                        </ext:CommandColumn>
                    </Columns>
                </ColumnModel>
                <SelectionModel>
                    <ext:RowSelectionModel ID="RowSelectionModel6" runat="server" Mode="Single" />
                </SelectionModel>
                <View>
                    <ext:GridView ID="GridView2" runat="server">
                        <GetRowClass Fn="getRowClass" />
                    </ext:GridView>
                </View>
                <Features>
                    <ext:GroupingSummary ID="Group1" runat="server" GroupHeaderTplString="{name}" HideGroupedHeader="true"
                        EnableGroupingMenu="false" />
                </Features>
                   <BottomBar>
                        <ext:PagingToolbar Height="40" ID="PagingToolbar1" runat="server" StoreID="storeActivity"
                            DisplayInfo="true">
                            <Items>
                                <ext:Label ID="lblPageSize" runat="server" Text="Page size:" />
                                <ext:ToolbarSpacer ID="ToolbarSpacer_listItems" runat="server" Width="10" />
                                <ext:ComboBox ID="cmbPageSize" runat="server" Width="80" SelectOnFocus="true" Selectable="true"
                                    ValueField="Value" DisplayField="Text" ForceSelection="true" AllowBlank="false">
                                    <Listeners>
                                        <Select Handler="searchList()" />
                                        <%--#{cmbPageSize}.pageSize = this.getValue();#{PagingToolbar1}.moveFirst();--%>
                                    </Listeners>
                                    <Items>
                                        <ext:ListItem Value="20" Text="20" />
                                        <ext:ListItem Value="50" Text="50" />
                                        <ext:ListItem Value="100" Text="100" />
                                    </Items>
                                    <SelectedItems>
                                        <ext:ListItem Index="0">
                                        </ext:ListItem>
                                    </SelectedItems>
                                </ext:ComboBox>
                            </Items>
                        </ext:PagingToolbar>
                    </BottomBar>
            </ext:GridPanel>

       
        <br />
    </div>
    <ext:Store runat="server" ID="storeSearch" AutoLoad="false">
        <Proxy>
            <ext:AjaxProxy Url="../Employee/RepresentativeSearch.ashx">
                <ActionMethods Read="GET" />
                <Reader>
                    <ext:JsonReader Root="plants" TotalProperty="total" />
                </Reader>
            </ext:AjaxProxy>
        </Proxy>
        <Model>
            <ext:Model ID="Model5" IDProperty="Value" Name="ItemLineModel" runat="server">
                <Fields>
                    <ext:ModelField Name="Value" Type="String" />
                    <ext:ModelField Name="Value" Type="String" />
                </Fields>
            </ext:Model>
        </Model>
    </ext:Store>
    <ext:Window ID="WActivity" runat="server" Title="Activity Submission" Icon="Application" 
        Resizable="false" Width="1160" Height="360" BodyPadding="5" Hidden="true" Modal="true">
        <Content>
            <table style="margin-left: 20px; margin-top: 20px;">
                <tr>
                    <td style="width: 850px;">
                        <ext:DateField ID="txtDate" runat="server" Width="120" LabelAlign="Top" FieldLabel=""
                            LabelSeparator="">
                            <Plugins>
                                <ext:GenericPlugin ID="GenericPlugin2" runat="server" InstanceName="Ext.ux.DatePicker.HighlightWithNepDates" />
                            </Plugins>
                            <DirectEvents>
                                <Change OnEvent="txtDate_Change">
                                    <EventMask ShowMask="true" />
                                </Change>
                            </DirectEvents>
                        </ext:DateField>
                        <asp:RequiredFieldValidator Display="None" ID="rfvDateCV" runat="server" ValidationGroup="SaveUpdActivity"
                            ControlToValidate="txtDate" ErrorMessage="Date is required." />
                    </td>
                    <td>
                        <ext:Image ID="img" runat="server" Width="30" Height="30" Cls="img-circle" />
                    </td>
                    <td>
                        <ext:DisplayField ID="dfName" runat="server" />
                    </td>
                </tr>
            </table>
            <ext:GridPanel StyleSpec="margin-top:15px; margin-left:20px;" ID="gridActivityAdd"
                runat="server" Cls="itemgrid" Scroll="Vertical" Width="1100">
                <Store>
                    <ext:Store ID="storeActivityAdd" runat="server">
                        <Model>
                            <ext:Model ID="modelActivityAdd" Name="SettingModel" runat="server" IDProperty="SN">
                                <Fields>
                                    <ext:ModelField Name="SN" Type="Int" />
                                    <ext:ModelField Name="ClientSoftwareId" Type="String" />
                                    <ext:ModelField Name="ActivityType" Type="String" />
                                    <ext:ModelField Name="StartTimeDate" Type="Date" />
                                    <ext:ModelField Name="EndTimeDate" Type="Date"/>
                                    <ext:ModelField Name="Representative" Type="String" />
                                    <ext:ModelField Name="Description" Type="string" />
                                    <ext:ModelField Name="DurationTime" Type="string" />
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                <ColumnModel>
                    <Columns>
                        <ext:Column ID="colSN" Sortable="false" MenuDisabled="true" runat="server" Text="Client"
                            Hidden="true" Align="Left" Width="150" DataIndex="SN">
                        </ext:Column>

                        <ext:Column ID="colClientAdd" Sortable="false" MenuDisabled="true" runat="server"
                            Text="Client" Align="Left" Width="150" DataIndex="ClientSoftwareId">
                            <Renderer Fn="clientRenderer" />
                            <Editor>
                                <ext:ComboBox DisplayField="Name" QueryMode="Local" ForceSelection="true" ValueField="ClientSoftwareId"
                                        StoreID="storeClient" ID="cmbClientAdd" runat="server">
                                    </ext:ComboBox>
                            </Editor>
                        </ext:Column>
                        <ext:Column ID="colTypeAdd" Sortable="false" MenuDisabled="true" runat="server" Text="Type"
                            Align="Left" Width="150" DataIndex="ActivityType">
                            <Renderer Fn="typeRenderer" />
                            <Editor>
                                <ext:ComboBox DisplayField="Name" QueryMode="Local" ForceSelection="true" ValueField="ActivityType"
                                        StoreID="storeType" ID="cmbTypeAdd" runat="server">
                                    </ext:ComboBox>
                            </Editor>
                        </ext:Column>
                        <ext:DateColumn ID="colStartTimeAdd" Sortable="false" MenuDisabled="true" runat="server"
                            Text="Start Time" Align="Center" Width="100" DataIndex="StartTimeDate" Format="hh:mm tt">
                            <Editor>
                                <ext:TimeField ID="TimeField1" runat="server" MinTime="00:00" MaxTime="23:59" Increment="1"
                                    Format="hh:mm tt" SelectedTime="10:00">
                                </ext:TimeField>
                            </Editor>
                        </ext:DateColumn>
                        
                        <ext:Column ID="colDuration" Sortable="false" MenuDisabled="true" runat="server" Text="Duration(h:m)"
                            Align="Center" Width="100" DataIndex="DurationTime">
                            <Editor>
                                <ext:TextField ID="txtDuration" runat="server" />
                            </Editor>
                        </ext:Column>

                        <ext:Column ID="colRepresentativeAdd" Sortable="false" MenuDisabled="true" runat="server"
                            Text="Person" Align="Left" Width="220" DataIndex="Representative">
                            <Editor>
                                <ext:ComboBox LabelSeparator="" ID="cmbSearch" runat="server" DisplayField="Value"
                                    FieldLabel="" ValueField="Value" EmptyText="Search" StoreID="storeSearch" TypeAhead="false"
                                    HideBaseTrigger="true" MinChars="1" TriggerAction="All" ForceSelection="false">
                                    <ListConfig LoadingText="Searching..." MinWidth="200" StyleSpec="border-top:1px solid #98c0f4;">
                                        <ItemTpl ID="ItemTpl1" runat="server">
                                            <Html>
                                                <tpl>
                                                            <div class="search-item">
                                                                            <span>{Value}</span>  
                                                             </div>
					                                </tpl>
                                            </Html>
                                        </ItemTpl>
                                    </ListConfig>
                                    <Triggers>
                                        <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                                    </Triggers>
                                    <Listeners>
                                        <Select Handler="this.getTrigger(0).show();" />
                                        <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                        <TriggerClick Handler="if (index == 0) { 
                                                           this.clearValue(); 
                                                           this.getTrigger(0).hide();
                                                       }" />
                                    </Listeners>
                                </ext:ComboBox>
                            </Editor>
                        </ext:Column>
                        <ext:Column ID="colDescription" Sortable="false" MenuDisabled="true" runat="server" Wrap="true"
                            Text="Description" Align="Left" Width="350" DataIndex="Description">
                            <Editor>
                                <ext:TextField ID="txtDesciption" runat="server">
                                </ext:TextField>
                            </Editor>
                        </ext:Column>
                        <ext:CommandColumn ID="CommandColumn1" runat="server" Sortable="false" MenuDisabled="true"
                            Width="30" Align="Center">
                            <Commands>
                                <ext:GridCommand Icon="Delete" CommandName="Delete" Cls="deleteicon" />
                            </Commands>
                            <Listeners>
                                <Command Fn="RemoveItemLine">
                                </Command>
                            </Listeners>
                        </ext:CommandColumn>
                    </Columns>
                </ColumnModel>
                <SelectionModel>
                    <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" Mode="Single" />
                </SelectionModel>
                <Plugins>
                    <ext:CellEditing ID="CellEditing1" runat="server" ClicksToEdit="1">
                    </ext:CellEditing>
                </Plugins>
            </ext:GridPanel>
            <div class="popupButtonDiv bottomBlock" style="padding-left: 35px;">
                <ext:Button runat="server" ID="btnSave" Cls="btn btn-primary" Text="<i></i>Save">
                    <DirectEvents>
                        <Click OnEvent="btnSave_Click">
                            <EventMask ShowMask="true" />
                            <ExtraParams>
                                <ext:Parameter Name="gridItems" Value="Ext.encode(#{gridActivityAdd}.getRowsValues({ selectedOnly: false }))"
                                    Mode="Raw" />
                            </ExtraParams>
                        </Click>
                    </DirectEvents>
                    <Listeners>
                        <Click Handler="valGroup = 'SaveUpdActivity'; if(CheckValidation()) return ''; else return false;">
                        </Click>
                    </Listeners>
                </ext:Button>
                <div class="btnFlatOr">
                </div>
                <ext:Button runat="server" ID="Button1" Cls="btn btn-default" Text="<i></i>Cancel">
                    <Listeners>
                        <Click Handler="#{WActivity}.hide();" />
                    </Listeners>
                </ext:Button>
            </div>
        </Content>
    </ext:Window>

    <ext:Window ID="WActivityView" runat="server" Title="Activity" Icon="Application" OverflowY="Scroll"
        Resizable="false" Width="1160" Height="600" BodyPadding="5" Hidden="true" Modal="true">
        <Content>
            <table style="margin-left: 20px; margin-top: 20px;">
                <tr>
                    <td style="width: 900px;">
                        <ext:DateField ID="txtDateView" runat="server" Width="120" LabelAlign="Top" FieldLabel=""
                            LabelSeparator="">
                            <Plugins>
                                <ext:GenericPlugin ID="GenericPlugin3" runat="server" InstanceName="Ext.ux.DatePicker.HighlightWithNepDates" />
                            </Plugins>
                        </ext:DateField>
                        <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator1" runat="server" ValidationGroup="SaveUpdActivity"
                            ControlToValidate="txtDate" ErrorMessage="Date is required." />
                    </td>
                    <td>
                        <ext:Image ID="Image1" runat="server" Width="30" Height="30" Cls="img-circle" />
                    </td>
                    <td>
                        <ext:DisplayField ID="dfNameView" runat="server" />
                    </td>
                </tr>
            </table>
            <div style="margin-left: 20px; font-weight: bold; margin-top: 20px;">
                Reported Activities</div>
            <ext:GridPanel StyleSpec="margin-top:5px; margin-left:20px;" ID="gridActivityView"
                runat="server" Cls="itemgrid" Scroll="Vertical" Width="1100">
                <Store>
                    <ext:Store ID="store1" runat="server">
                        <Model>
                            <ext:Model ID="model1" Name="SettingModel" runat="server" IDProperty="SN">
                                <Fields>
                                    <ext:ModelField Name="SN" Type="Int" />
                                    <ext:ModelField Name="ClientSoftwareId" Type="string" />
                                    <ext:ModelField Name="ActivityType" Type="string" />
                                    <ext:ModelField Name="StartTimeDate" Type="Date" />
                                    <ext:ModelField Name="DurationTime" Type="String" />
                                    <ext:ModelField Name="Representative" Type="String" />
                                    <ext:ModelField Name="Description" Type="string" />
                                    <ext:ModelField Name="ClientName" Type="String" />
                                    <ext:ModelField Name="ActivityName" Type="String" />
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                <ColumnModel>
                    <Columns>
                        <ext:Column ID="ColumnSoftware" Sortable="false" MenuDisabled="true" runat="server" Text="Client"
                            Hidden="true" Align="Left" Width="150" DataIndex="ClientSoftwareId">
                        </ext:Column>
                        <ext:Column ID="ColumnClient" Sortable="false" MenuDisabled="true" runat="server"
                            Text="Client" Align="Left" Width="150" DataIndex="ClientName">
                        </ext:Column>
                        <ext:Column ID="ColumnName" Sortable="false" MenuDisabled="true" runat="server" Text="Type"
                            Align="Left" Width="150" DataIndex="ActivityName">
                        </ext:Column>
                        <ext:DateColumn ID="DateColumn1" Sortable="false" MenuDisabled="true" runat="server"
                            Text="Start Time" Align="Center" Width="100" DataIndex="StartTimeDate" Format="HH:mm tt">
                        </ext:DateColumn>
                        <ext:Column ID="ColumnDuration" Sortable="false" MenuDisabled="true" runat="server"
                            Text="Duration(h:m)" Align="Center" Width="100" DataIndex="DurationTime">
                        </ext:Column>
                        <ext:Column ID="ColumnRep" Sortable="false" MenuDisabled="true" runat="server"
                            Text="Person" Align="Left" Width="200" DataIndex="Representative">
                        </ext:Column>
                        <ext:Column ID="ColumnViewDesc" Sortable="false" MenuDisabled="true" runat="server" Wrap="true"
                            Text="Description" Align="Left" Width="400" DataIndex="Description">
                        </ext:Column>
                    </Columns>
                </ColumnModel>
                <SelectionModel>
                    <ext:RowSelectionModel ID="RowSelectionModel2" runat="server" Mode="Single" />
                </SelectionModel>
            </ext:GridPanel>
            <br />
           

            <ext:GridPanel StyleSpec="margin-top:5px; margin-left:20px;" ID="gridComments" Title="Comments" Hidden="true"
                runat="server" Cls="itemgrid grid1" Scroll="Vertical" Width="1100" HideHeaders="true">
                <Store>
                    <ext:Store ID="store2" runat="server">
                        <Model>
                            <ext:Model ID="model4" Name="SettingModel" runat="server" IDProperty="SN">
                                <Fields>
                                    <ext:ModelField Name="Id" Type="Int" />
                                    <ext:ModelField Name="Name" Type="String" />
                                    <ext:ModelField Name="Comment" Type="String" />
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                <ColumnModel>
                    <Columns>
                        <ext:Column ID="Column10" Sortable="false" MenuDisabled="true" runat="server" Text="Name"
                           Align="Left" Width="200" DataIndex="Name">
                           <Renderer Fn="rendererClass" />
                        </ext:Column>
                        <ext:Column ID="Column11" Sortable="false" MenuDisabled="true" runat="server" Wrap="true"
                            Text="Comment" Align="Left" Width="900" DataIndex="Comment">
                        </ext:Column>
                    </Columns>
                </ColumnModel>
                <SelectionModel>
                    <ext:RowSelectionModel ID="RowSelectionModel4" runat="server" Mode="Single" />
                </SelectionModel>
            </ext:GridPanel>

            <br />

            
            <table style="margin-left:20px;">
                <tr>
                    <td>
                        <ext:TextField ID="txtEmpComment" runat="server" LabelSeparator="" EmptyText="Write your comment..." LabelCls="blueCls" FieldLabel="Your Comment" Text="" LabelAlign="Top" Width="1100">
                           <%--<Listeners>
                                <SpecialKey Fn="enterKeyPressHandler" />
                            </Listeners>--%>
                        </ext:TextField>
                        <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator2" runat="server" ValidationGroup="SaveUpdComment"
                                        ControlToValidate="txtEmpComment" ErrorMessage="Comment is required." />
                    </td>
                </tr>
                <tr>
                    <td>
                        <ext:Button runat="server" ID="btnSaveComment" Cls="btn btn-primary" Text="<i></i>Save Comment" MarginSpec="20 0 0 0" >
                            <DirectEvents>
                                <Click OnEvent="btnSaveComment_Click">
                                    <EventMask ShowMask="true" />
                                </Click>
                            </DirectEvents>
                            <Listeners>
                                <Click Handler="valGroup = 'SaveUpdComment'; if(CheckValidation()) return ''; else return false;">
                                </Click>
                            </Listeners>
                        </ext:Button>
                    </td>
                </tr>
            </table>
           
        </Content>
    </ext:Window>

</asp:Content>
