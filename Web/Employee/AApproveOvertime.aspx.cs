﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using Utils.Helper;
using Utils.Web;
using BLL.Base;
using DAL;
using Utils.Calendar;
using BLL.BO;

namespace Web.Employee
{
    public partial class AApproveOvertime : BasePage
    {


        //private int _tempCurrentPage;
       // private int _tempCount;
        private PayManager payMgr = new PayManager();

        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (!IsPostBack)
            {

                if (CommonManager.CompanySetting.WhichCompany == WhichCompany.NIBL)
                    Response.Redirect("AApproveOvertimeNewNew.aspx");

                Initialise();

                if (LeaveAttendanceManager.IsEmployeeAllowedToApproveLeave(SessionManager.CurrentLoggedInEmployeeId))
                    btnRecommendOrApprove.Text = "Approve";

            }
            JavascriptHelper.AttachPopUpCode(Page, "assignovertimePopup", "OvertimeRequesterPopup.aspx", 475, 550);
            JavascriptHelper.AttachPopUpCode(Page, "positionHistoryPopup", "AApproveOvertimePopup.aspx", 475, 500);
        
        }

        public bool CanRecommendOrApprove(int status, int employeeId)
        {
            if (status == (int)OvertimeStatusEnum.Recommended &&
                LeaveRequestManager.CanApprove(employeeId, SessionManager.CurrentLoggedInEmployeeId,PreDefindFlowType.Overtime))
                return true;

            if (status == 0)
                return true;


            return false;
        }


        protected void btnRecommendOrApprove_Click(object sender, EventArgs e)
        {

            bool employeeAdded = false;
            List<OvertimeRequest> requestList = new List<OvertimeRequest>();
            foreach (GridViewRow row in gvw.Rows)
            {


                OvertimeRequest request = new OvertimeRequest();
                int RequestID = int.Parse(gvw.DataKeys[row.RowIndex]["RequestID"].ToString());


                if (row.FindControl("chkDelete") == null)
                    continue;

                CheckBox chkDelete = (CheckBox)row.FindControl("chkDelete");

                if (chkDelete.Checked)
                {
                    employeeAdded = true;

                    request.OvertimeRequestID = RequestID;
                    requestList.Add(request);
                }
            }

            int count = 0;
            if (OvertimeManager.RecommendOrApprove(requestList, out count))
            {
                LoadEmployees();

                

                divMsgCtl.InnerHtml = count + " overtime has been forwarded.";
                divMsgCtl.Hide = false;
            }

        }


        void Initialise()
        {
            if (OvertimeManager.IsRequestAutoGroupType)
            {
                gvw.Columns[4].Visible = false;
                gvw.Columns[5].Visible = false;
            }

            ddlOvertimeType.DataSource = OvertimeManager.GetOvertimeList();
            ddlOvertimeType.DataBind();

            ddlBranch.DataSource = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId);
            ddlBranch.DataBind();

            // if has Overtime approval permission then set default status filter to Approve
            //if (LeaveAttendanceManager.IsEmployeeAllowedToApproveOvertime())
            //{
            //    ddlStatus.SelectedValue = "1";
            //}

            List<KeyValue> statues = new JobStatus().GetMembers();
            LoadEmployees();
        }

        protected void btnLoad_Click(object sender, EventArgs e)
        {
        
            LoadEmployees();
        }
        protected void gvwEmployees_RowCreated(object sender, GridViewRowEventArgs e)
        {

            //if (e.Row.RowType == DataControlRowType.DataRow)
            //{
            //    e.Row.Attributes.Add("onmouseover",
            //    "this.originalClass=this.className;this.className='selected'");

            //    e.Row.Attributes.Add("onmouseout",
            //    "this.className=this.originalClass;");


            //}

        }
        protected void LoadEmployees()
        {
            int type=1;
            int status = -1;
            int overtimeTypeId = -1;


            if( ddlType.SelectedItem != null)
                type = int.Parse(ddlType.SelectedValue);

            if (ddlStatus.SelectedItem != null)
                status = int.Parse(ddlStatus.SelectedValue);

            if (ddlOvertimeType.SelectedItem != null)
                overtimeTypeId = int.Parse(ddlOvertimeType.SelectedItem.Value);

            //gvw.DataSource = OvertimeManager.GetLeaveRequestsForManager(type, status);
            //gvw.DataBind();

            int totalRecords = 0;

            DateTime? fromDate = null, toDate = null;

            if (!string.IsNullOrEmpty(calFrom.Text.Trim()) && calFrom.SelectedDate != new DateTime())
            {
                fromDate = calFrom.SelectedDate;

                if (string.IsNullOrEmpty(calTo.Text.Trim()) || calTo.SelectedDate == new DateTime())
                {
                    NewMessage.ShowWarningMessage("To date is required.");
                    calTo.Focus();
                    return;
                }
            }

            if (!string.IsNullOrEmpty(calTo.Text.Trim()) && calTo.SelectedDate != new DateTime())
            {
                toDate = calTo.SelectedDate;

                if (string.IsNullOrEmpty(calFrom.Text.Trim()) || calFrom.SelectedDate == new DateTime())
                {
                    NewMessage.ShowWarningMessage("From date is required.");
                    calFrom.Focus();
                    return;
                }
            }


            List<GetOvertimeRequestForManagerResult> list = new List<GetOvertimeRequestForManagerResult>();
            list = OvertimeManager.GetLeaveRequestsForManager(type, status, pagintCtl.CurrentPage, int.Parse(pagintCtl.DDLRecords.SelectedValue), overtimeTypeId, int.Parse(ddlBranch.SelectedValue),
                fromDate, toDate, ref totalRecords);

            gvw.DataSource = list;
            gvw.DataBind();


            pagintCtl.UpdatePagingBar(totalRecords);

            
        }

        public string GetEngOrNepDate(object value)
        {
            if (value == null)
                return "";

            DateTime date = Convert.ToDateTime(value);

            CustomDate c = new CustomDate(date.Day, date.Month, date.Year, true);

            if (IsEnglish)
                return c.ToString();

            else
                return CustomDate.ConvertEngToNep(c).ToString();
        }

       
        protected void btnExport_Click(object sender, EventArgs e)
        {
            GridViewExportUtil.Export("OvertimeList.xls", gvw);
        }

        //protected void btnPostToSalary_Click(object sender, EventArgs e)
        //{

        //    foreach (GridViewRow row in gvw.Rows)
        //    {


        //        int payrollPeriodId = (int)gvw.DataKeys[row.RowIndex]["PayrollPeriodId"];

        //        PayManager.PostOvertimePay(payrollPeriodId);
        //        msgCtl.InnerHtml = Resources.Messages.OvertimePostedMessage;
        //        msgCtl.Hide = false;
        //        break;
        //    }


        //}
        protected void txtEmployeeName_TextChanged(object sender, EventArgs e)
        {
            

        }


        protected void gvwRoles_PageIndexChanged(object sender, EventArgs e)
        {

        }

        protected void gvwRoles_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {

            LoadEmployees();
            //Clear();
        }

        protected void btnNext_Click()
        {
            pagintCtl.CurrentPage += 1;
            LoadEmployees();
        }



        protected void btnPrevious_Click()
        {
            pagintCtl.CurrentPage -= 1;
            LoadEmployees();
        }

        protected void ddlRecords_SelectedIndexChanged()
        {
            pagintCtl.CurrentPage = 1;
            LoadEmployees();
        }

    }
}
