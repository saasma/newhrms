﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ELanguageCtrl.ascx.cs" Inherits="Web.Employee.UserControls.ELanguageCtrl" %>

<script type="text/javascript">

        var prepareLang = function (grid, toolbar, rowIndex, record) {
            var editDelBtn = toolbar.items.get(0);

            if (record.data.IsEditable == 0) {
                editDelBtn.setVisible(false);
            }

        }

        function reloadLangGrid() {
            <%= btnLoadLangGrid.ClientID %>.fireEvent('click');
        }

        function AddLanguage()
        {
            addLang();
        }

        function EditLanguage()
        {
            editLang('LangId=' + <%=hdnLanguageSetsID.ClientID %>.getValue());
        }

    
    </script>

<ext:Hidden runat="server" ID="hdnLanguageSetsID">
</ext:Hidden>

<ext:LinkButton ID="btnLoadLangGrid" runat="server" Hidden="true">
    <DirectEvents>
        <Click OnEvent="btnLoadLangGrid_Click">
            <EventMask ShowMask="true" />
        </Click>
    </DirectEvents>
</ext:LinkButton>

    <div class="widget-body">
        <table class="fieldTable firsttdskip">
            <tr>
                <td>
                    <ext:GridPanel ID="GridLanguageSets" runat="server" Width="1000" Scroll="None">
                        <Store>
                            <ext:Store ID="StoreLanguageSets" runat="server">
                                <Model>
                                    <ext:Model ID="Model1" runat="server" IDProperty="LanguageSetId">
                                        <Fields>
                                            <ext:ModelField Name="LanguageSetName" Type="string" />
                                            <ext:ModelField Name="FluencySpeakName" Type="String" />
                                            <ext:ModelField Name="FluencyWriteName" Type="String" />
                                            <ext:ModelField Name="IsEditable" Type="Int" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:Column ID="Column1" runat="server" Text="Language" DataIndex="LanguageSetName" Wrap="true"
                                    Width="200" Flex="1" />
                                <ext:Column ID="Column2" runat="server" Text="Fluency : Speaking" DataIndex="FluencySpeakName" Wrap="true"
                                    Width="150" />
                                <ext:Column ID="Column3" runat="server" Text="Fluency : Writing" DataIndex="FluencyWriteName" Wrap="true"
                                    Width="150" />
                                <ext:CommandColumn ID="CommandColumn1" runat="server" Width="40">
                                    <Commands>
                                        <ext:GridCommand ToolTip-Text="Edit" Icon="ApplicationEdit" CommandName="Edit" />                                                                     
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GridLanguageSets_Command">
                                            <EventMask ShowMask="true">
                                            </EventMask>
                                            <ExtraParams>
                                                <ext:Parameter Name="ID" Value="record.data.LanguageSetId" Mode="Raw">
                                                </ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw">
                                                </ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                                <ext:CommandColumn ID="CommandColumn2" runat="server" Width="40">
                                    <Commands>                                       
                                        <ext:GridCommand ToolTip-Text="Delete" Icon="ApplicationDelete"
                                            CommandName="Delete" />                                      
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GridLanguageSets_Command">
                                            <EventMask ShowMask="true">
                                            </EventMask>
                                            <Confirmation ConfirmRequest="true" Message="Are you sure, you want to delete the record?" />
                                            <ExtraParams>
                                                <ext:Parameter Name="ID" Value="record.data.LanguageSetId" Mode="Raw">
                                                </ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw">
                                                </ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                    <PrepareToolbar Fn="prepareLang" />
                                </ext:CommandColumn>
                                <ext:Column ID="Column4" runat="server" Text=""
                                    Width="420" />
                            </Columns>
                        </ColumnModel>
                        <SelectionModel>
                            <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" Mode="Single" />
                        </SelectionModel>
                    </ext:GridPanel>
                </td>
            </tr>
        </table>
        <div class="buttonBlockSection">
            <ext:Button runat="server" ID="btnAddNewLine" Cls="btn btn-primary btn-sect" runat="server" Width="120" StyleSpec="margin-top:0px" Height="30"
             Text="<i></i>Add Language" OnClientClick="AddLanguage();return false;">
            </ext:Button>
        </div>
    </div>