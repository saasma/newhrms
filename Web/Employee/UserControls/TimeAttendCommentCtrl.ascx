﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TimeAttendCommentCtrl.ascx.cs"
    Inherits="Web.Employee.UserControls.TimeAttendCommentCtrl" %>
<style type="text/css">
    .BlueCls
    {
        color: Blue;
    }
    .GreenCls
    {
        color: Green;
    }
    .RedCls
    {
        color: Red;
    }
</style>
<script type="text/javascript">

    var CommandHandler = function(value, command,record,p2) {
             if(command == 'Approve')
            {
                <%= hdnRequestId.ClientID %>.setValue(record.data.LineID);
                <%= btnApprove.ClientID %>.fireEvent('click');
            }
            else
            {
                <%= hdnRequestId.ClientID %>.setValue(record.data.LineID);
                <%= btnReject.ClientID %>.fireEvent('click');
            }
       };

    function searchListEL() {

             <%=gridAttRequest.ClientID %>.getStore().pageSize = <%=cmbPageSize.ClientID %>.getValue();
            <%=PagingToolbar1.ClientID %>.doRefresh();
        }
    
 var checkboxRenderer = function (v, p, record) {
            if(record.data.Status > 0){            
                return "";
            }
            
          return '<div class="x-grid-row-checker">&nbsp;</div>'
        };

  
    var prepareCommand = function (grid, command, record, row) {
            // you can prepare group command
            if ((command.command == 'Approve' || command.command == 'Reject') && ( record.data.Status > 0))
            {
                command.hidden = true;
                command.hideMode = 'visibility'; 
            }
        };

</script>
<ext:Hidden ID="hdnRequestId" runat="server" />
<ext:Hidden ID="hdnEmployeeId" runat="server" />
<ext:Hidden ID="hdnEmployeeIdFilter" runat="server" />
<ext:Hidden ID="Hidden1" runat="server" />
<ext:Hidden ID="hdnSortBy" runat="server" />
<ext:LinkButton ID="btnApprove" Hidden="true" runat="server">
    <DirectEvents>
        <Click OnEvent="btnApprove_Click">
            <EventMask ShowMask="true" />
            <Confirmation ConfirmRequest="true" Message="Are you sure, you want to approve request?" />
        </Click>
    </DirectEvents>
</ext:LinkButton>
<ext:LinkButton ID="btnReject" Hidden="true" runat="server">
    <DirectEvents>
        <Click OnEvent="btnReject_Click">
            <EventMask ShowMask="true" />
            <Confirmation ConfirmRequest="true" Message="Are you sure, you want to reject the request?" />
        </Click>
    </DirectEvents>
</ext:LinkButton>
<ext:Store ID="storeEmployees" runat="server">
    <Model>
        <ext:Model ID="modelEmployees" IDProperty="Value" runat="server">
            <Fields>
                <ext:ModelField Name="Text" Type="String" />
                <ext:ModelField Name="Value" Type="String" />
            </Fields>
        </ext:Model>
    </Model>
</ext:Store>
<ext:Store runat="server" ID="storeSearch" AutoLoad="false">
    <Proxy>
        <ext:AjaxProxy Url="../../Handler/EmployeeSearch.ashx">
            <ActionMethods Read="GET" />
            <Reader>
                <ext:JsonReader Root="plants" TotalProperty="total" />
            </Reader>
        </ext:AjaxProxy>
    </Proxy>
    <Model>
        <ext:Model ID="Model2" IDProperty="Name" Name="ItemLineModel" runat="server">
            <Fields>
                <ext:ModelField Name="Name" Type="String" />
                <ext:ModelField Name="EmployeeId" Type="String" />
            </Fields>
        </ext:Model>
    </Model>
</ext:Store>
<div class="attribute" style="padding: 10px;padding-top:0px;">
    <table class="fieldTable firsttdskip" runat="server" id="tbl">
        <tr>
            <td>
                <ext:DateField LabelSeparator="" FieldLabel="From" runat="server" LabelAlign="Top"
                    ID="txtFromDateFilter" Width="150">
                    <Plugins>
                        <ext:GenericPlugin ID="GenericPlugin3" runat="server" InstanceName="Ext.ux.DatePicker.HighlightWithNepDates" />
                    </Plugins>
                </ext:DateField>
            </td>
            <td>
                <ext:DateField LabelSeparator="" FieldLabel="To" runat="server" LabelAlign="Top"
                    ID="txtToDateFilter" Width="150">
                    <Plugins>
                        <ext:GenericPlugin ID="GenericPlugin4" runat="server" InstanceName="Ext.ux.DatePicker.HighlightWithNepDates" />
                    </Plugins>
                </ext:DateField>
            </td>
            <td>
                <ext:Store runat="server" ID="storeEmpSearch" AutoLoad="false">
                    <Proxy>
                        <ext:AjaxProxy Url="../../Handler/EmpSearchID.ashx">
                            <ActionMethods Read="GET" />
                            <Reader>
                                <ext:JsonReader Root="plants" TotalProperty="total" />
                            </Reader>
                        </ext:AjaxProxy>
                    </Proxy>
                    <Model>
                        <ext:Model ID="Model9" IDProperty="Name" Name="ItemLineModel" runat="server">
                            <Fields>
                                <ext:ModelField Name="Name" Type="String" />
                                <ext:ModelField Name="EmployeeId" Type="String" />
                            </Fields>
                        </ext:Model>
                    </Model>
                </ext:Store>
                <ext:ComboBox LabelSeparator="" ID="cmbEmpSearch" FieldLabel="Employee" LabelWidth="70"
                    EmptyText="Search Employee" LabelAlign="Top" runat="server" DisplayField="Name"
                    ValueField="EmployeeId" StoreID="storeEmpSearch" TypeAhead="false" Width="180"
                    PageSize="9999" HideBaseTrigger="true" MinChars="1" Hidden="true" TriggerAction="All"
                    ForceSelection="true">
                    <ListConfig LoadingText="Searching..." MinWidth="200" StyleSpec="border-top:1px solid #98c0f4;">
                        <ItemTpl ID="ItemTpl2" runat="server">
                            <Html>
                                <tpl>
                                                <div class="search-item">
                                                                <span>{Name}</span>  
                                                 </div>
					                    </tpl>
                            </Html>
                        </ItemTpl>
                    </ListConfig>
                    <Triggers>
                        <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                    </Triggers>
                    <Listeners>
                        <Select Handler="this.getTrigger(0).show();" />
                        <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                        <TriggerClick Handler="if (index == 0) { 
                                               this.clearValue(); 
                                               this.getTrigger(0).hide();
                                           }" />
                    </Listeners>
                </ext:ComboBox>
            </td>
            <td>
                <ext:ComboBox ID="cmbStatus" runat="server" Width="100" SelectOnFocus="true" Selectable="true"
                    LabelAlign="Top" FieldLabel="Status" ValueField="Value" DisplayField="Text" ForceSelection="true"
                    AllowBlank="false" LabelSeparator="">
                    <Items>
                        <ext:ListItem Value="-1" Text="All" />
                        <ext:ListItem Value="0" Text="Requested" />
                        <ext:ListItem Value="1" Text="Approved" />
                        <ext:ListItem Value="2" Text="Rejected" />
                    </Items>
                    <SelectedItems>
                        <ext:ListItem Index="1">
                        </ext:ListItem>
                    </SelectedItems>
                </ext:ComboBox>
            </td>
            <td style="padding-top: 25px">
                <div>
                    <ext:Button Cls="btn btn-primary" Height="30" runat="server" ID="Button1" Text="<i></i>Load">
                        <Listeners>
                            <Click Fn="searchListEL" />
                        </Listeners>
                    </ext:Button>
                </div>
            </td>
        </tr>
    </table>
</div>
<ext:GridPanel StyleSpec="margin-top:15px;clear:both;" ID="gridAttRequest" runat="server"
    Cls="itemgrid" OnReadData="Store_ReadData" Scroll="None">
    <Store>
        <ext:Store ID="Store1" runat="server" AutoLoad="true" OnReadData="Store_ReadData"
            RemotePaging="true" RemoteSort="true">
            <Proxy>
                <ext:PageProxy />
            </Proxy>
            <Model>
                <ext:Model ID="Model4" runat="server" IDProperty="RequestID">
                    <Fields>
                        <ext:ModelField Name="LineID" Type="String" />
                        <ext:ModelField Name="EmpID" />
                        <ext:ModelField Name="Name" Type="String" />
                        <ext:ModelField Name="AttendanceDate" Type="Date" />
                        <ext:ModelField Name="InTime" Type="string" />
                        <ext:ModelField Name="InComment" Type="string" />
                        <ext:ModelField Name="OutTime" Type="string" />
                        <ext:ModelField Name="OutComment" Type="string" />
                        <ext:ModelField Name="StatusText" Type="string" />
                        <ext:ModelField Name="Status" />
                    </Fields>
                </ext:Model>
            </Model>
             <Sorters>
                <ext:DataSorter Property="AttendanceDate" Direction="ASC" />
            </Sorters>
        </ext:Store>
    </Store>
    <ColumnModel>
        <Columns>
            <ext:Column ID="adsfa" Sortable="true" MenuDisabled="true" runat="server" Text="EIN"
                Width="50" Align="Center" DataIndex="EmpID">
            </ext:Column>
            <ext:Column ID="Column4" Sortable="true" MenuDisabled="true" runat="server" Text="Name"
                Width="140" Align="Left" DataIndex="Name">
            </ext:Column>
            <ext:DateColumn ID="colSubmittedOn" runat="server" Align="Right" Text="Date" Width="90"
                MenuDisabled="true" Sortable="true" Format="dd-MMM-yyyy" DataIndex="AttendanceDate">
            </ext:DateColumn>
            <ext:Column ID="dCfasolumn2" Sortable="false" MenuDisabled="true" runat="server"
                Text="In Time" Width="80" Align="Center" DataIndex="InTime">
            </ext:Column>
            <ext:Column ID="Column5" Sortable="false" MenuDisabled="true" runat="server" Text="In Comment"
                Width="200" Align="Left" DataIndex="InComment">
            </ext:Column>
            <ext:Column ID="DateColumn2" Sortable="false" MenuDisabled="true" runat="server"
                Text="Out Time" Width="80" Align="Center" DataIndex="OutTime">
            </ext:Column>
            <ext:Column ID="Column6" Sortable="false" MenuDisabled="true" runat="server" Text="Out Comment"
                Width="200" Align="Left" DataIndex="OutComment">
            </ext:Column>
            <ext:Column ID="colStatusName" Sortable="true" MenuDisabled="true" runat="server"
                Text="Status" Width="100" Align="Left" DataIndex="StatusText">
            </ext:Column>
            <ext:ImageCommandColumn ID="ImageCommandColumn2" runat="server" Width="70" Text=""
                MenuDisabled="true" Sortable="false" Align="Center">
                <Commands>
                    <ext:ImageCommand CommandName="Approve" Text="Approve" Cls="GreenCls">
                        <ToolTip Text="Approve" />
                    </ext:ImageCommand>
                </Commands>
                <Listeners>
                    <Command Fn="CommandHandler">
                    </Command>
                </Listeners>
                <PrepareCommand Fn="prepareCommand" />
            </ext:ImageCommandColumn>
            <ext:ImageCommandColumn ID="ImageCommandColumn3" runat="server" Width="70" Text=""
                MenuDisabled="true" Sortable="false" Align="Center">
                <Commands>
                    <ext:ImageCommand CommandName="Reject" Text="Reject" Cls="RedCls">
                        <ToolTip Text="Reject" />
                    </ext:ImageCommand>
                </Commands>
                <Listeners>
                    <Command Fn="CommandHandler">
                    </Command>
                </Listeners>
                <PrepareCommand Fn="prepareCommand" />
            </ext:ImageCommandColumn>
        </Columns>
    </ColumnModel>
    <SelectionModel>
        <ext:CheckboxSelectionModel ID="CheckboxSelectionModel1" runat="server" Mode="Simple">
            <CustomConfig>
                <ext:ConfigItem Name="renderer" Value="checkboxRenderer" Mode="Raw" />
            </CustomConfig>
            <Listeners>
                <BeforeSelect Handler="return record.data.Status == 0;" />
            </Listeners>
        </ext:CheckboxSelectionModel>
    </SelectionModel>
    <View>
        <ext:GridView ID="GridView1" runat="server" StripeRows="true" />
    </View>
    <BottomBar>
        <ext:PagingToolbar ID="PagingToolbar1" runat="server" StoreID="Store1" DisplayInfo="true">
            <Items>
                <ext:Label ID="lblPageSize" runat="server" Text="Page size:" />
                <ext:ToolbarSpacer ID="ToolbarSpacer_listItems" runat="server" Width="10" />
                <ext:ComboBox ID="cmbPageSize" runat="server" Width="80" SelectOnFocus="true" Selectable="true"
                    ValueField="Value" DisplayField="Text" ForceSelection="true" AllowBlank="false">
                    <Listeners>
                        <Select Handler="searchListEL()" />
                        <%--#{cmbPageSize}.pageSize = this.getValue();#{PagingToolbar1}.moveFirst();--%>
                    </Listeners>
                    <Items>
                        <ext:ListItem Value="50" Text="50" />
                        <ext:ListItem Value="100" Text="100" />
                        <ext:ListItem Value="200" Text="200" />
                    </Items>
                    <SelectedItems>
                        <ext:ListItem Index="0">
                        </ext:ListItem>
                    </SelectedItems>
                </ext:ComboBox>
            </Items>
        </ext:PagingToolbar>
    </BottomBar>
</ext:GridPanel>
<br />
<ext:Button ID="btnApproveAll" StyleSpec="float:left;margin-left:20px" runat="server"
    Cls="btn btn-success" Text="Approve" Width="120">
    <DirectEvents>
        <Click OnEvent="btnApproveAll_Click">
            <EventMask ShowMask="true" />
            <Confirmation ConfirmRequest="true" Message="Are you sure, you want to approve requests?" />
            <ExtraParams>
                <ext:Parameter Name="gridItems" Value="Ext.encode(#{gridAttRequest}.getRowsValues({selectedOnly:true}))"
                    Mode="Raw" />
            </ExtraParams>
        </Click>
    </DirectEvents>
</ext:Button>
<ext:Button runat="server" OnClick="btnExport_Click" StyleSpec="float:left;margin-left:20px"
    Cls="btn btn-info" AutoPostBack="true" ID="btnExport" Text="<i></i>Export To Excel">
</ext:Button>
<div style="clear: both">
</div>
