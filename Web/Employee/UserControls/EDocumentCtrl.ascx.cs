﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using Utils.Helper;
using System.IO;
using Utils;
using Utils.Security;
namespace Web.Employee.UserControls
{
    public partial class EDocumentCtrl : BaseUserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();

                if (SessionManager.CurrentLoggedInEmployeeId != 0)
                    hdnIsEmployee.Text = "1";
                else
                    hdnIsEmployee.Text = "0";
            }

            JavascriptHelper.AttachPopUpCode(Page, "addDocument", "EDocumentDetailsPopup.aspx", 500, 480);
        }

        public int GetEmployeeId()
        {
            if (SessionManager.CurrentLoggedInEmployeeId == 0)
                return int.Parse(Request.QueryString["id"]);
            else
                return SessionManager.CurrentLoggedInEmployeeId;
        }

        public void Initialise()
        {
            LoadDocuments();
        }

        private void LoadDocuments()
        {
            GridLevels.Store[0].DataSource = HRManager.GetDocuments(GetEmployeeId());
            GridLevels.Store[0].DataBind();
        }

        protected void btnDeleteLevel_Click(object sender, DirectEventArgs e)
        {
            int levelId = int.Parse(hiddenValue.Text.Trim());

            HDocument doc = new NewHRManager().GetDocument(levelId);
            bool status = new HRManager().DeleteDocument(doc);

            if (status)
            {
                LoadDocuments();
                NewMessage.ShowNormalMessage("Document deleted.");
            }
            else
            {
                NewMessage.ShowWarningMessage("Document could not be deleted.");
            }
        }


        protected void btnDownload_Click(object sender, DirectEventArgs e)
        {
            int documentId = int.Parse(hiddenValue.Text);
            HDocument obj = NewPayrollManager.GetDocumentById(documentId);

            string fileName = obj.Url.ToString();
            if (!string.IsNullOrEmpty(fileName))
                Response.Redirect("~/DocumentHandler.ashx?id=" + System.IO.Path.GetFileNameWithoutExtension(fileName).ToString());
            else
                NewMessage.ShowWarningMessage("This file does not exist.");

        }

        protected void btnReloadDoc_Click(object sender, DirectEventArgs e)
        {
            LoadDocuments();
        }

    }
}