﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL.Base;
using BLL;
using Utils.Helper;
using BLL.BO;

namespace Web.Employee.UserControls
{
    public partial class TimeAttendCtrl : BaseUserControl
    {


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialize();

                if (SessionManager.CurrentLoggedInEmployeeId == 0)
                {
                    btnAssignTimeAtten.Hide();
                    cmbEmpSearch.Show();
                }
            }

        }

        private void Initialize()
        {
            if (CommonManager.CompanySetting.WhichCompany == WhichCompany.NIBL)
            {
                colInTime1.Editor.Clear();
                colOutTime1.Editor.Clear();
                btnAssignTimeAtt.Visible = false;
                btnAssignTimeAtten.Visible = false;
            }

            Setting setting = OvertimeManager.GetSetting();
            if (setting.TimeRequestTeamUsingRecommendApproval != null && setting.TimeRequestTeamUsingRecommendApproval.Value)
                tblRecommenderApproval.Style.Remove("display");

            if (setting.HideReviewInLeaveRequest != null && setting.HideReviewInLeaveRequest.Value)
            {
                cmbRecommender.Hide();
                tdRecommend.Visible = false;
            }

            BindStatusCombo();
            X.Js.Call("searchListEL");
        }

        private void BindStatusCombo()
        {
            List<TextValue> list = new List<TextValue>();
            list.Add(new TextValue() { Text = "All", Value = "-1" });
            list.Add(new TextValue() { Text = "Requested", Value = "0" });
            list.Add(new TextValue() { Text = "Approved", Value = "1" });
            list.Add(new TextValue() { Text = "Rejected", Value = "2" });

            Setting setting = OvertimeManager.GetSetting();
            if (setting.TimeRequestTeamUsingRecommendApproval != null && setting.TimeRequestTeamUsingRecommendApproval.Value)
                list.Insert(2, new TextValue() { Text = "Recommended", Value = "3" });

            cmbStatus.GetStore().DataSource = list;
            cmbStatus.GetStore().DataBind();

            //cmbStatus.SelectedItem.Text = "Requested";

            if (setting.TimeRequestTeamUsingRecommendApproval != null && setting.TimeRequestTeamUsingRecommendApproval.Value
                && LeaveAttendanceManager.IsEmployeeAllowedToApproveLeave(SessionManager.CurrentLoggedInEmployeeId, false))
            {
                //cmbStatus.SelectedItem.Text = "Recommended";
                cmbStatus.SelectedItems.Add(new Ext.Net.ListItem { Value = "0" });
                cmbStatus.SelectedItems.Add(new Ext.Net.ListItem { Value = "3" });
            }
            else
            {
                cmbStatus.SelectedItems.Add(new Ext.Net.ListItem { Value = "0" });
            }
        }

        private void Clear()
        {
            txtStartDateAdd.Text = "";
            txtEndDateAdd.Text = "";
            hdnEmployeeId.Text = "";
            hdnRequestId.Text = "";
            txtInNoteAdd.Text = "";
            cmbSearch.Text = "";
            cmbRecommender.Clear();
            cmbApproval.Clear();
        }

        private void LoadWindow(int requestId)
        {           
            List<TimeRequestLine> list = new List<TimeRequestLine>();

            if (requestId != 0)
                list = AttendanceManager.GetTimeRequestLinesByRequestID(requestId);

            string status = "";

            foreach (Ext.Net.ListItem item in cmbStatus.SelectedItems)
            {
                if (status == "")
                    status = item.Value;
                else
                    status += "," + item.Value;
            }

            if (status == "")
                status = "-1";

            if (status == "-1" || status.Contains("0"))
            {
                DateTime? fromDate = null, toDate = null;
                int employeeId = -1, managerId = -1;

                if (txtFromDateFilter.SelectedDate != null && txtFromDateFilter.SelectedDate != new DateTime())
                    fromDate = txtFromDateFilter.SelectedDate;

                if (txtToDateFilter.SelectedDate != null && txtToDateFilter.SelectedDate != new DateTime())
                    toDate = txtToDateFilter.SelectedDate;

                if (fromDate != null && toDate == null)
                {
                    NewMessage.ShowNormalMessage("To date is required for date filter.");
                    txtToDateFilter.Focus();
                    return;
                }

                if (toDate != null && fromDate == null)
                {
                    NewMessage.ShowNormalMessage("From date is required for date filter.");
                    txtFromDateFilter.Focus();
                    return;
                }

                if (cmbEmpSearch.SelectedItem != null && cmbEmpSearch.SelectedItem.Value != null)
                    employeeId = int.Parse(cmbEmpSearch.SelectedItem.Value);

                int totalRecords = 0;

                if (SessionManager.CurrentLoggedInEmployeeId != 0)
                    managerId = SessionManager.CurrentLoggedInEmployeeId;

                status = "0"; //requested status

                List<GetTimeRequestsForApprovalResult> listApproval = AttendanceManager.GetTimeRequestForApproval(employeeId, managerId, fromDate, toDate, 0, 5, out totalRecords, status);

                if (listApproval.Count > 0 && listApproval[0].TotalRows > 1)
                {
                    btnViewApproveAndProcessNext.Show();
                }
                else
                    btnViewApproveAndProcessNext.Hide();

                if (requestId == 0)
                {
                    requestId = listApproval[0].RequestID;
                    hdnRequestId.Text = requestId.ToString();
                    list = AttendanceManager.GetTimeRequestLinesByRequestID(requestId);

                    NewMessage.ShowNormalMessage("Time request approved successfully.");
                }

                btnViewApprove.Show();
                btnViewReject.Show();

                WAttendanceReq.Height = (list.Count * 25) + 120 + 70;
            }
            else
            {                
                btnViewApprove.Hide();
                btnViewReject.Hide();                
                btnViewApproveAndProcessNext.Hide();
                
                WAttendanceReq.Height = (list.Count * 25) + 120;
            }

            gridAttTimeDetl.GetStore().DataSource = list;
            gridAttTimeDetl.GetStore().DataBind();

            TimeRequest objTR = AttendanceManager.GetTimeRequestByRequestID(requestId);
            int empId = objTR.EmployeeId.Value;
            if (objTR.Status == 0)
            {
                btnViewApprove.Show();
                btnViewReject.Show();
                WAttendanceReq.Height = (list.Count * 25) + 120 + 70;
            }
            else
            {
                btnViewApproveAndProcessNext.Hide();
                btnViewApprove.Hide();
                btnViewReject.Hide();
            }

            // if in approved status then show reject button to admin only
            if(objTR.Status == (int)AttendanceRequestStatusEnum.Approved && SessionManager.CurrentLoggedInEmployeeId == 0)
            {
                btnViewReject.Show();
            }
            
            WAttendanceReq.Title = string.Format("Time Request Details:{0}", EmployeeManager.GetEmployeeById(empId).Name);
        }

        protected void btnView_Click(object sender, DirectEventArgs e)
        {
            int requestId = int.Parse(hdnRequestId.Text);
            LoadWindow(requestId);
            WAttendanceReq.Center();
            WAttendanceReq.Show();
        }


        public void btnExport_Click(object sender, EventArgs e)
        {
            DateTime? fromDate = null, toDate = null;
            int employeeId = -1, managerId = -1;
            string status = "";

            if (txtFromDateFilter.SelectedDate != null && txtFromDateFilter.SelectedDate != new DateTime())
                fromDate = txtFromDateFilter.SelectedDate;

            if (txtToDateFilter.SelectedDate != null && txtToDateFilter.SelectedDate != new DateTime())
                toDate = txtToDateFilter.SelectedDate;

            if (fromDate != new DateTime() && toDate == new DateTime())
            {
                NewMessage.ShowNormalMessage("To date is required for date filter.");
                txtToDateFilter.Focus();
                return;
            }

            if (toDate != new DateTime() && fromDate == new DateTime())
            {
                NewMessage.ShowNormalMessage("From date is required for date filter.");
                txtFromDateFilter.Focus();
                return;
            }

            if (cmbEmpSearch.SelectedItem != null && cmbEmpSearch.SelectedItem.Value != null)
                employeeId = int.Parse(cmbEmpSearch.SelectedItem.Value);

            int totalRecords = 0;

            if (SessionManager.CurrentLoggedInEmployeeId != 0)
                managerId = SessionManager.CurrentLoggedInEmployeeId;


            foreach (Ext.Net.ListItem item in cmbStatus.SelectedItems)
            {
                if (status == "")
                    status = item.Value;
                else
                    status += "," + item.Value;
            }
            //if (cmbStatus.SelectedItem != null && cmbStatus.SelectedItem.Value != null)
            //    status = int.Parse(cmbStatus.SelectedItem.Value);

            List<GetTimeRequestsForApprovalResult> list = AttendanceManager.GetTimeRequestForApproval(employeeId, managerId, fromDate, toDate, 0, 999999, out totalRecords, status);
            

            List<string> hiddenList = new List<string>();
            hiddenList.Add("RequestID");
            hiddenList.Add("Status");
            hiddenList.Add("TotalRows");
            hiddenList.Add("RowNumber");
            hiddenList.Add("StartDateEng");
            hiddenList.Add("EndDateEng");
            hiddenList.Add("SubmittedOnEng");
            hiddenList.Add("ShowHide");

            Dictionary<string, string> renameList = new Dictionary<string, string>();

            renameList.Add("EmployeeId", "EIN");
            renameList.Add("StartDateEngText", "From");
            renameList.Add("EndDateEngText", "To");
            renameList.Add("SubmittedOnEngText", "Submitted On");

            Bll.ExcelHelper.ExportToExcel("Time Request List", list,
                hiddenList,
            new List<String>() { },
            renameList,
            new List<string>() { }, new List<string> { "WorkDays", "EmployeeId" }
            , new List<string>() { "StartDateEngText", "EndDateEngText", "SubmittedOnEngText" }
            , new Dictionary<string, string>() { { "Time Request List", "" } }
            , new List<string> { "EmployeeId", "Name","Branch", "StartDateEngText", "EndDateEngText", "SubmittedOnEngText", "WorkDays" });


        }

        protected void btnApprove_Click(object sender, DirectEventArgs e)
        {
            int requestId = int.Parse(hdnRequestId.Text);

            List<TimeRequest> list = new List<TimeRequest>();
            TimeRequest dbObj = AttendanceManager.GetTimeRequestByRequestID(requestId);
            list.Add(dbObj);

            Status status = AttendanceManager.ApproveAttRequestTime(list, false,false);
            if (status.IsSuccess)
            {
                NewMessage.ShowNormalMessage("Time request approved successfully.");
                CheckboxSelectionModel1.ClearSelection();
                SendApproveMail(list);
                X.Js.Call("searchListEL");
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }

        }

        protected void SendApproveMail(List<TimeRequest> AttendanceEmpCommentList)
        {

            SMTPHelper mailSender = new SMTPHelper();
            EmailContent dbMailContent = CommonManager.GetMailContent((int)EmailContentType.TimeRequestApprovedOrReject);

            if (dbMailContent != null)
            {
                foreach (TimeRequest objAttendanceEmpComment in AttendanceEmpCommentList)
                {
                    string subject = dbMailContent.Subject.Replace("#Status#", "Approved").Replace("#Date#", objAttendanceEmpComment.StartDateEng.Value.ToShortDateString());
                    string body = dbMailContent.Body.Replace("#Status#", "Approved").Replace("#Date#", objAttendanceEmpComment.StartDateEng.Value.ToShortDateString());
                    body = body.Replace("#Reason#", "");

                    EEmployee _Employeee = EmployeeManager.GetEmployeeById(objAttendanceEmpComment.EmployeeId.Value);
                    if (string.IsNullOrEmpty(_Employeee.EAddresses[0].CIEmail))
                    {
                        continue;
                    }

                    bool isSendSuccess = SMTPHelper.SendAsyncMail(_Employeee.EAddresses[0].CIEmail, body, subject, "");
                }
            }

        }

        protected void SendRejectMail(TimeRequest objAttendanceEmpComment)
        {

            SMTPHelper mailSender = new SMTPHelper();
            EmailContent dbMailContent = CommonManager.GetMailContent((int)EmailContentType.TimeRequestApprovedOrReject);
            if (dbMailContent != null)
            {
                
                string subject = dbMailContent.Subject.Replace("#Status#", "Rejected").Replace("#Date#", objAttendanceEmpComment.StartDateEng.Value.ToShortDateString());
                string body = dbMailContent.Body.Replace("#Status#", "Rejected").Replace("#Date#", objAttendanceEmpComment.StartDateEng.Value.ToShortDateString());
                body = body.Replace("#Reason#", "");

                EEmployee _Employeee = EmployeeManager.GetEmployeeById(objAttendanceEmpComment.EmployeeId.Value);
                if (string.IsNullOrEmpty(_Employeee.EAddresses[0].CIEmail))
                {
                    return;
                }

                bool isSendSuccess = SMTPHelper.SendAsyncMail(_Employeee.EAddresses[0].CIEmail, body, subject, "");
                
            }
        }
        protected void btnReject_Click(object sender, DirectEventArgs e)
        {
            txtRejectComment.Text = "";
            WReject.Center();
            WReject.Show();
        }

        protected void btnApproveAll_Click(object sender, DirectEventArgs e)
        {
            string gridItemsJSON = e.ExtraParams["gridItems"];
            List<TimeRequest> list = JSON.Deserialize<List<TimeRequest>>(gridItemsJSON);

            List<TimeRequest> dbList = new List<TimeRequest>();

            if (list.Count == 0)
                return;

            foreach (TimeRequest item in list)
            {

                TimeRequest dbObj = AttendanceManager.GetTimeRequestByRequestID(item.RequestID);
                dbList.Add(dbObj);
            }

            Status status = AttendanceManager.ApproveAttRequestTime(dbList, false, false);
            if (status.IsSuccess)
            {
                NewMessage.ShowNormalMessage("Time requests approved successfully.");
                CheckboxSelectionModel1.ClearSelection();
                SendApproveMail(dbList);
                X.Js.Call("searchListEL");
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }

        }

        protected void btnAssignTimeAtten_Click(object sender, DirectEventArgs e)
        {
            Clear();
            gridOTList.GetStore().DataSource = new List<TimeRequestLine>();
            gridOTList.GetStore().DataBind();

            winAssignTimeAttend.Center();
            winAssignTimeAttend.Show();
        }

        protected void  btnAssignTimeAtt_Click(object sender, DirectEventArgs e)
        {
            string entryLineJson = e.ExtraParams["gridItems"];

            List<TimeRequestLine> timeRequestList = JSON.Deserialize<List<TimeRequestLine>>(entryLineJson);
            List<TimeRequestLine> newTimeRequestList = new List<TimeRequestLine>();
            TimeRequest objTimeRequest = new TimeRequest();

            if (timeRequestList.Count == 0)
            {
                btnAssignTimeAtt.Enable();
                NewMessage.ShowWarningMessage("No row added to assign time.");
                return;
            }

            int workDaysCount = 0;
            int row = 0;

            int employeeId = 0;
            if (!string.IsNullOrEmpty(hdnEmployeeId.Text))
                employeeId = int.Parse(hdnEmployeeId.Text);
            else
            {
                NewMessage.ShowWarningMessage("Please select employee.");
                cmbSearch.Focus();
                return;
            }

            foreach (var item in timeRequestList)
            {
                TimeRequestLine obj = new TimeRequestLine();
                row++;
                if (item.DateEng != null)
                {
                    obj.DateEng = item.DateEng;
                    obj.DateName = BLL.BaseBiz.GetAppropriateDate(obj.DateEng.Value);
                }
                else
                {
                    NewMessage.ShowWarningMessage("Date is required for the row " + row.ToString());
                    btnAssignTimeAtt.Enable();
                    return;
                }

                objTimeRequest.EmployeeId = employeeId;
                obj.OvernightShift = item.OvernightShift;

                TimeSpan tsStartTime, tsEndTime;

                obj.Description = item.Description;

                if (string.IsNullOrEmpty(obj.Description))
                    workDaysCount++;

                if (item.InTimeDT != null && item.InTimeDT != new DateTime())
                {

                    tsStartTime = new TimeSpan(item.InTimeDT.Value.Hour, item.InTimeDT.Value.Minute, 0);
                    obj.InTime = tsStartTime;
                }

                obj.InNote = item.InNote;

                if (item.OutTimeDT != null && item.OutTimeDT != new DateTime())
                {
                    tsEndTime = new TimeSpan(item.OutTimeDT.Value.Hour, item.OutTimeDT.Value.Minute, 0);
                    obj.OutTime = tsEndTime;
                }

                if (obj.InTime == null && obj.OutTime == null)
                {
                    NewMessage.ShowWarningMessage("Start time or End time is required for the row " + row.ToString());
                    btnAssignTimeAtt.Enable();
                    return;
                }

                obj.OutNote = item.OutNote;

                if ((item.InTimeDT != null && item.InTimeDT != new DateTime()) && (item.OutTimeDT != null && item.OutTimeDT != new DateTime()))
                {
                    if (obj.OutTime.Value <= obj.InTime.Value && obj.OvernightShift == false)
                    {
                        NewMessage.ShowWarningMessage("Out time must be greater than In time for row " + row.ToString());
                        return;
                    }

                    TimeSpan tsOutTime = new TimeSpan();
                    if (item.OvernightShift.Value)
                    {
                        tsOutTime = obj.OutTime.Value.Add(new TimeSpan(24, 0, 0));
                    }
                    else
                        tsOutTime = obj.OutTime.Value;

                    TimeSpan totalWorkHourTS = tsOutTime - obj.InTime.Value;
                    double time = 0;

                    if (item.OvernightShift.Value)
                    {
                        time = double.Parse((totalWorkHourTS.Hours).ToString() + "." + totalWorkHourTS.Minutes.ToString());
                        obj.WorkHours = time;
                    }
                    else
                    {
                        time = double.Parse(totalWorkHourTS.Hours.ToString() + "." + totalWorkHourTS.Minutes.ToString());
                        obj.WorkHours = time;
                    }

                }

                newTimeRequestList.Add(obj);

            }

            objTimeRequest.StartDateEng = newTimeRequestList[0].DateEng;
            objTimeRequest.StartDate = newTimeRequestList[0].DateName;
            objTimeRequest.EndDateEng = newTimeRequestList[newTimeRequestList.Count - 1].DateEng;
            objTimeRequest.EndDate = newTimeRequestList[newTimeRequestList.Count - 1].DateName;
            objTimeRequest.WorkDays = workDaysCount;
            objTimeRequest.SubmittedOnEng = System.DateTime.Now;
            objTimeRequest.SubmittedOn = BLL.BaseBiz.GetAppropriateDate(objTimeRequest.SubmittedOnEng.Value);
            objTimeRequest.Status = (int)AttendanceRequestStatusEnum.Approved;

            //string IPAddress = AttendanceManager.GetIPAddressByEmployeeId(objTimeRequest.EmployeeId.Value);
            //if (IPAddress != "")
            objTimeRequest.IPAddress = ""; ;

            if (SessionManager.CurrentLoggedInEmployeeId != 0)
            {
                Setting setting = OvertimeManager.GetSetting();
                if (setting.TimeRequestTeamUsingRecommendApproval != null && setting.TimeRequestTeamUsingRecommendApproval.Value)
                {
                    bool hideRecommender = setting.HideReviewInLeaveRequest != null && setting.HideReviewInLeaveRequest.Value;

                    if (hideRecommender == false)
                    {
                        if (cmbRecommender.SelectedItem != null && cmbRecommender.SelectedItem.Value != null && cmbRecommender.SelectedItem.Value != "-1")
                            objTimeRequest.RecommenderEmployeeId = int.Parse(cmbRecommender.SelectedItem.Value);
                        else
                        {
                            NewMessage.ShowWarningMessage("Recommender is required.");
                            cmbRecommender.Focus();
                            return;
                        }
                    }

                    if (cmbApproval.SelectedItem != null && cmbApproval.SelectedItem.Value != null && cmbApproval.SelectedItem.Value != "-1")
                        objTimeRequest.ApprovalEmployeeId = int.Parse(cmbApproval.SelectedItem.Value);
                    else
                    {
                        NewMessage.ShowWarningMessage("Approval is required.");
                        cmbApproval.Focus();
                        return;
                    }

                    if (hideRecommender)
                    {
                        objTimeRequest.RecommenderEmployeeId = objTimeRequest.ApprovalEmployeeId;
                    }
                }
                else
                    objTimeRequest.ApprovalId1 = SessionManager.CurrentLoggedInEmployeeId;

            }       


            objTimeRequest.TimeRequestLines.AddRange(newTimeRequestList);

            Status status = AttendanceManager.ApproveAttRequestTime(new List<TimeRequest> { objTimeRequest }, true, false);

            if (status.IsSuccess)
            {
                NewMessage.ShowNormalMessage("Time request saved successfully.");
                winAssignTimeAttend.Close();
            }
            else
                NewMessage.ShowWarningMessage(status.ErrorMessage);
        }

        protected void btnLoad_Click(object sender, DirectEventArgs e)
        {
            DateTime startDate = new DateTime();
            DateTime endDate = new DateTime();

            int status = int.Parse(cmbStatus.SelectedItem.Value);

            if (txtStartDateAdd.SelectedDate != new DateTime())
                startDate = txtStartDateAdd.SelectedDate;
            else
            {
                NewMessage.ShowWarningMessage("Start date is required.");
                return;
            }

            if (txtEndDateAdd.SelectedDate != new DateTime())
                endDate = txtEndDateAdd.SelectedDate;
            else
            {
                NewMessage.ShowWarningMessage("End date is required.");
                return;
            }

            if (startDate > DateTime.Now.Date || endDate.Date > DateTime.Now.Date)
            {
                NewMessage.ShowWarningMessage("You cannot assign attendance in the future.");
                return;
            }

            int employeeId = 0;

            if (!string.IsNullOrEmpty(hdnEmployeeId.Text))
                employeeId = int.Parse(hdnEmployeeId.Text);
            else
            {
                NewMessage.ShowWarningMessage("Please select employee.");
                cmbSearch.Focus();
                return;
            }

            if (AttendanceManager.CheckTimeRequestIsSaved(startDate, endDate, employeeId))
            {
                NewMessage.ShowWarningMessage("Time request is already saved for the period.");
                return;
            }

            string message = AttendanceManager.CheckEmpTimeAttRequestThreshold(startDate, endDate);
            if (message != "")
            {
                NewMessage.ShowWarningMessage(message);
                return;
            }

            List<TimeRequestLine> list = AttendanceManager.GetTimeRequestLineForDateRange(startDate, endDate, txtInNoteAdd.Text.Trim(), employeeId);


            gridOTList.GetStore().DataSource = list;
            gridOTList.GetStore().DataBind();

            //winAssignTimeAttend.Height = (list.Count * 25) + 300;
            winAssignTimeAttend.Center();
            winAssignTimeAttend.Show();


        }

        protected void Store_ReadData(object sender, StoreReadDataEventArgs e)
        {
            DateTime? fromDate = null, toDate = null;
            int employeeId = -1, managerId = -1;
            string status = "";

            if (txtFromDateFilter.SelectedDate != null && txtFromDateFilter.SelectedDate != new DateTime())
                fromDate = txtFromDateFilter.SelectedDate;

            if (txtToDateFilter.SelectedDate != null && txtToDateFilter.SelectedDate != new DateTime())
                toDate = txtToDateFilter.SelectedDate;

            if (fromDate != null && toDate == null)
            {
                NewMessage.ShowNormalMessage("To date is required for date filter.");
                txtToDateFilter.Focus();
                return;
            }

            if (toDate != null && fromDate == null)
            {
                NewMessage.ShowNormalMessage("From date is required for date filter.");
                txtFromDateFilter.Focus();
                return;
            }

            if (cmbEmpSearch.SelectedItem != null && cmbEmpSearch.SelectedItem.Value != null)
                employeeId = int.Parse(cmbEmpSearch.SelectedItem.Value);

            int totalRecords = 0;

            int currentPage = e.Start / int.Parse(cmbPageSize.SelectedItem.Value);

            if (SessionManager.CurrentLoggedInEmployeeId != 0)
                managerId = SessionManager.CurrentLoggedInEmployeeId;

            foreach (Ext.Net.ListItem item in cmbStatus.SelectedItems)
            {
                if (status == "")
                    status = item.Value;
                else
                    status += "," + item.Value;
            }

            List<GetTimeRequestsForApprovalResult> list = AttendanceManager.GetTimeRequestForApproval(employeeId, managerId, fromDate, toDate, currentPage, int.Parse(cmbPageSize.SelectedItem.Value), out totalRecords, status);

            e.Total = totalRecords;
            Store1.DataSource = list;
            Store1.DataBind();        


        }

        protected void btnSelectEmp_Click(object sender, DirectEventArgs e)
        {
            int employeeId = int.Parse(hdnEmployeeId.Text);
            List<TextValue> listReview = LeaveRequestManager.GetApplyToListForEmployee(employeeId, false, PreDefindFlowType.Overtime);

            List<TextValue> listApproval = LeaveRequestManager.GetApplyToListForEmployee(employeeId, true, PreDefindFlowType.Overtime);

            cmbRecommender.GetStore().DataSource = listReview;
            cmbRecommender.GetStore().DataBind();
            cmbRecommender.SetValue("-1");

            cmbApproval.GetStore().DataSource = listApproval;
            cmbApproval.GetStore().DataBind();
            cmbApproval.SetValue("-1");
        }

        protected void btnRejectWithComment_Click(object sender, DirectEventArgs e)
        {
            int requestId = int.Parse(hdnRequestId.Text);

            string comment = null;
            if (!string.IsNullOrEmpty(txtRejectComment.Text.Trim()))
                comment = txtRejectComment.Text.Trim();

            Status status = AttendanceManager.RejectAttRequestTime(requestId, comment);
            
            if (status.IsSuccess)
            {
                NewMessage.ShowNormalMessage("Time request rejected successfully.");
                TimeRequest _objTimeRequest = AttendanceManager.GetTimeRequestByRequestID(requestId);

                if (WAttendanceReq.Hidden == false)
                    WAttendanceReq.Close();
                WReject.Close();
                SendRejectMail(_objTimeRequest);
                X.Js.Call("searchListEL");
            }
            else
                NewMessage.ShowWarningMessage(status.ErrorMessage);
        }

        protected void btnViewApprove_Click(object sender, DirectEventArgs e)
        {
            int requestId = int.Parse(hdnRequestId.Text);

            List<TimeRequest> list = new List<TimeRequest>();
            TimeRequest dbObj = AttendanceManager.GetTimeRequestByRequestID(requestId);
            list.Add(dbObj);

            Status status = AttendanceManager.ApproveAttRequestTime(list, false, false);
            if (status.IsSuccess)
            {
                NewMessage.ShowNormalMessage("Time request approved successfully.");
                WAttendanceReq.Close();
                CheckboxSelectionModel1.ClearSelection();
                SendApproveMail(list);
                X.Js.Call("searchListEL");
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }

        }

        protected void btnViewApproveAndProcessNext_Click(object sender, DirectEventArgs e)
        {
            int requestId = int.Parse(hdnRequestId.Text);

            List<TimeRequest> list = new List<TimeRequest>();
            TimeRequest dbObj = AttendanceManager.GetTimeRequestByRequestID(requestId);
            list.Add(dbObj);

            Status status = AttendanceManager.ApproveAttRequestTime(list, false, false);
            if (status.IsSuccess)
            {
                CheckboxSelectionModel1.ClearSelection();
                SendApproveMail(list);
                X.Js.Call("searchListEL");
                LoadWindow(0);
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }

        }


    }
}