﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL.Base;
using BLL;
using Utils.Helper;
using BLL.BO;

namespace Web.Employee.UserControls
{
    public partial class TimeRequestApprovalCtrl : BaseUserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public void LoadWindow(int requestId)
        {
            hdnTimeRequestCtrlId.Text = requestId.ToString();

            List<TimeRequestLine> list = new List<TimeRequestLine>();

            if (requestId != 0)
                list = AttendanceManager.GetTimeRequestLinesByRequestID(requestId);
            
            gridAttTimeDetl.GetStore().DataSource = list;
            gridAttTimeDetl.GetStore().DataBind();

            TimeRequest objTR = AttendanceManager.GetTimeRequestByRequestID(requestId);
            int empId = objTR.EmployeeId.Value;
            if (objTR.Status == 0)
            {
                btnViewApprove.Show();
                btnViewReject.Show();
                WAttendanceReq.Height = (list.Count * 25) + 120 + 70;
            }
            else
            {
                btnViewApprove.Hide();
                btnViewReject.Hide();
            }

            WAttendanceReq.Title = string.Format("Time Request Details:{0}", EmployeeManager.GetEmployeeById(empId).Name);

            WAttendanceReq.Center();
            WAttendanceReq.Show();
        }

        protected void btnViewApprove_Click(object sender, DirectEventArgs e)
        {
            int requestId = int.Parse(hdnTimeRequestCtrlId.Text);

            List<TimeRequest> list = new List<TimeRequest>();
            TimeRequest dbObj = AttendanceManager.GetTimeRequestByRequestID(requestId);
            list.Add(dbObj);

            Status status = AttendanceManager.ApproveAttRequestTime(list, false, false);
            if (status.IsSuccess)
            {
                NewMessage.ShowNormalMessage("Time request approved successfully.", "reloadTimeReqGrid()");
                SendApproveMail(list);
                WAttendanceReq.Close();              
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }

        }

        public void SendApproveMail(List<TimeRequest> AttendanceEmpCommentList)
        {

            SMTPHelper mailSender = new SMTPHelper();
            EmailContent dbMailContent = CommonManager.GetMailContent((int)EmailContentType.TimeRequestApprovedOrReject);

            if (dbMailContent != null)
            {
                foreach (TimeRequest objAttendanceEmpComment in AttendanceEmpCommentList)
                {
                    string subject = dbMailContent.Subject.Replace("#Status#", "Approved").Replace("#Date#", objAttendanceEmpComment.StartDateEng.Value.ToShortDateString());
                    string body = dbMailContent.Body.Replace("#Status#", "Approved").Replace("#Date#", objAttendanceEmpComment.StartDateEng.Value.ToShortDateString());
                    body = body.Replace("#Reason#", "");

                    EEmployee _Employeee = EmployeeManager.GetEmployeeById(objAttendanceEmpComment.EmployeeId.Value);
                    if (string.IsNullOrEmpty(_Employeee.EAddresses[0].CIEmail))
                    {
                        continue;
                    }

                    bool isSendSuccess = SMTPHelper.SendAsyncMail(_Employeee.EAddresses[0].CIEmail, body, subject, "");
                }
            }

        }

        protected void btnReject_Click(object sender, DirectEventArgs e)
        {
            txtRejectComment.Text = "";
            WReject.Center();
            WReject.Show();
        }

        public void LoadRejectWindow(int requestId)
        {
            hdnTimeRequestCtrlId.Text = requestId.ToString();
            txtRejectComment.Text = "";
            WReject.Center();
            WReject.Show();
        }

        protected void btnRejectWithComment_Click(object sender, DirectEventArgs e)
        {
            int requestId = int.Parse(hdnTimeRequestCtrlId.Text);

            string comment = null;
            if (!string.IsNullOrEmpty(txtRejectComment.Text.Trim()))
                comment = txtRejectComment.Text.Trim();

            Status status = AttendanceManager.RejectAttRequestTime(requestId, comment);

            if (status.IsSuccess)
            {
                NewMessage.ShowNormalMessage("Time request rejected successfully.", "reloadTimeReqGrid");
                TimeRequest _objTimeRequest = AttendanceManager.GetTimeRequestByRequestID(requestId);

                if (WAttendanceReq.Hidden == false)
                    WAttendanceReq.Close();
                WReject.Close();
                SendRejectMail(_objTimeRequest);
            }
            else
                NewMessage.ShowWarningMessage(status.ErrorMessage);
        }

        protected void SendRejectMail(TimeRequest objAttendanceEmpComment)
        {

            SMTPHelper mailSender = new SMTPHelper();
            EmailContent dbMailContent = CommonManager.GetMailContent((int)EmailContentType.TimeRequestApprovedOrReject);
            if (dbMailContent != null)
            {

                string subject = dbMailContent.Subject.Replace("#Status#", "Rejected").Replace("#Date#", objAttendanceEmpComment.StartDateEng.Value.ToShortDateString());
                string body = dbMailContent.Body.Replace("#Status#", "Rejected").Replace("#Date#", objAttendanceEmpComment.StartDateEng.Value.ToShortDateString());
                body = body.Replace("#Reason#", "");

                EEmployee _Employeee = EmployeeManager.GetEmployeeById(objAttendanceEmpComment.EmployeeId.Value);
                if (string.IsNullOrEmpty(_Employeee.EAddresses[0].CIEmail))
                {
                    return;
                }

                bool isSendSuccess = SMTPHelper.SendAsyncMail(_Employeee.EAddresses[0].CIEmail, body, subject, "");

            }
        }


    }
}