﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TravelAssignCtl.ascx.cs"
    Inherits="Web.Employee.UserControls.TravelAssignCtl" %>
<ext:Hidden runat="server" ID="hiddenValue" />
<style>
    .commentBlock
    {
        font-size: 13px;
        margin-top: 5px;
    }
    .nameBlock
    {
        font-size: 13px;
        color: #3399FF;
        font-style: italic;
    }
</style>
<script type="text/javascript">
var afterEditAssign = function()
        {
            var records = <%=gridAllowancesAssign.ClientID %>.getStore();
             var i = 0,
                length = records.data.length,
                total = 0,
                record;

            for (; i < length; ++i) {
                record = records.data.items[i];
                total += record.data.Quantity * record.data.Rate;
            }

            total = getFormattedAmount(total);
            <%=dispTotal.ClientID %>.setValue(total);
        }

        var beforeEdit = function(e1, e) {
            
            if(e.field=="Rate" && e.record.data.IsEditable!="true")
            {
                e.cancel = true;
                return;
            }
        
    };

      var addNewRow = function (grid) {
            var newRow = new Object();

            if(<%=radioDomestic.ClientID %>.getValue() == true)
                newRow.Country="Nepal";

            var rowIndex = grid.getStore().data.items.length;

            //gridProjectSplitter.stopEditing();
            grid.getStore().insert(rowIndex, newRow);
        }

          var RemoveItemLine = function (column, command, record, recordIndex, cellIndex) {
            var store = this.grid.store;
            store.remove(record);
        };
</script>
<ext:Window ID="Window1Assign" runat="server" Title="Assign Travel"  AutoScroll="true" Icon="Application"
    Width="1020" Height="625" BodyPadding="5" Hidden="true" Modal="true">
    <Content>
        <div style="width: 400px; float: left">
            <table>
                <tr>
                    <td colspan="4">
                        <div style="margin-bottom: 25px; margin-left: 10px;">
                            <ext:ComboBox ID="cmbEmployeeAssign" ForceSelection="true" runat="server" Width="250"
                                FieldLabel="Assign To" LabelAlign="Top" LabelWidth="140" QueryMode="Local" DisplayField="NameEIN"
                                ValueField="EmployeeId" Margins="0 0 25 0">
                                <Store>
                                    <ext:Store ID="storeEmployeeListAssign" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="EmployeeId" Type="String" />
                                            <ext:ModelField Name="NameEIN" />
                                        </Fields>
                                    </ext:Store>
                                </Store>
                                <DirectEvents>
                                    <Select OnEvent="cmbLocationsAssign_Change">
                                        <EventMask ShowMask="true" />
                                        <ExtraParams>
                                            <ext:Parameter Name="ValuesAssign" Value="Ext.encode(#{gridAllowancesAssign}.getRowsValues({selectedOnly : false}))"
                                                Mode="Raw" />
                                        </ExtraParams>
                                    </Select>
                                </DirectEvents>
                            </ext:ComboBox>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td valign="top" style="padding-left: 10px;">
                        <ext:Image ID="image" ImageUrl="~/images/sample.jpg" runat="server" Width="120px"
                            Height="120px" />
                    </td>
                    <td valign="top" style="padding-left: 10px">
                        <table>
                            <tr>
                                <td>
                                    <ext:Label ID="lblName" StyleSpec="font-weight:bold;color:#0099FF;font-size:16px;"
                                        runat="server" Text="Name">
                                    </ext:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <ext:Label ID="lblBranch" Text="Branch" runat="server">
                                    </ext:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <ext:Label ID="lblDepartment" Text="Department" runat="server">
                                    </ext:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <ext:Label ID="lblDesignation" Text="Designation" runat="server">
                                    </ext:Label>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
         <ext:Store runat="server" ID="storeCountry" >
            <Model>
                <ext:Model ID="Model3"  runat="server">
                    <Fields>
                        <ext:ModelField Name="Country" />
                    </Fields>
                </ext:Model>
            </Model>
        </ext:Store>
        <div style="width: 500px; float: left; padding-left: 25px;">
            <table id="albums" cellspacing="0px">
                <tr style="padding-bottom: 1em;">
                    <tr style='display: none' id="rowDomestic" runat="server">
                    <td>
                        <table>
                            <tr>
                                <td style="margin-right: 25px; margin-left: 25px;">
                                    <h4 style='margin-bottom:2px;    font-size: 15px;'>
                                        Places to Travel</h4>
                                    <ext:Radio Name="domestic" runat="server" Checked="true" ID="radioDomestic" BoxLabel="Domestic" />
                                </td>
                                <td  style="padding-left: 15px;padding-top:36px;">
                                    <ext:Radio Name="domestic" runat="server" ID="radioInternaltional" BoxLabel="International" />
                                </td>
                            </tr>
                        </table>
                    </td>
                   
                </tr>
                <tr style='display: none' id="rowEditableLocation" runat="server">
                    <td colspan="5">
                        <ext:GridPanel Width="560" StyleSpec="margin-bottom:10px" ID="gridLocations" runat="server" Cls="itemgrid">
                            <Store>
                                <ext:Store ID="store4" runat="server">
                                    <Model>
                                        <ext:Model ID="Model1" Name="SettingModel" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="LocationPlace" Type="String" />
                                                <ext:ModelField Name="Country" Type="String" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                             <Plugins>
                            <ext:CellEditing ID="CellEditing2" ClicksToEdit="1" runat="server">
                            </ext:CellEditing>
                        </Plugins>     
                            <ColumnModel>
                                <Columns>
                                    <ext:RowNumbererColumn ID="RowNumbererColumn1" DataIndex="SequenceNo" runat="server"
                                        Width="35" />
                                    <ext:Column ID="Column3" Flex="1" runat="server" Text="Place" Sortable="false" MenuDisabled="true"
                                        Width="200" DataIndex="LocationPlace" Border="false">
                                         <Editor>
                                          <ext:TextField ID="TextField1" runat="server" />
                                          </Editor>
                                    </ext:Column>
                                    <ext:Column ID="colTravelOrder" runat="server" Text="Country" Sortable="false" MenuDisabled="true"
                                        Width="150" DataIndex="Country" Border="false">
                                         <Editor>
                                        <ext:ComboBox DisplayField="Country" QueryMode="Local" ForceSelection="true" ValueField="Country"
                                            StoreID="storeCountry" ID="cmbCountryLocations" runat="server">
                                        </ext:ComboBox>
                                    </Editor>
                                    </ext:Column>
                                     <ext:CommandColumn ID="CommandColumn1" runat="server" Sortable="false" MenuDisabled="true"
                                    Width="50" Align="Center">
                                    <Commands>
                                        <ext:GridCommand Icon="Delete" CommandName="Delete" Cls="deleteicon" />
                                    </Commands>
                                    <Listeners>
                                        <Command Fn="RemoveItemLine">
                                        </Command>
                                    </Listeners>
                                </ext:CommandColumn>
                                </Columns>
                            </ColumnModel>
                        </ext:GridPanel>
                        <ext:Button Cls="btn btn-primary" ID="btnAddNewLineToMain" runat="server" Icon="Add"
                        Height="26" Text="Add New Line">
                        <Listeners>
                            <Click Handler="addNewRow(#{gridLocations});" />
                        </Listeners>
                    </ext:Button>
                    </td>
                </tr>
                    <td colspan="1" style="margin-right: 25px; margin-left: 25px;">
                        <ext:TextField ID="txtPlaceToTravelAssign" Width="150" runat="server" FieldLabel="Place to Travel"
                            LabelAlign="Top">
                        </ext:TextField>
                    </td>
                    <td colspan="1" style="padding-left: 25px; padding-top: 15px;">
                        <ext:ComboBox ID="cmbCountryAssign" runat="server" Width="150" FieldLabel="Country"
                            LabelAlign="Top" QueryMode="Local" DisplayField="CountryName" ValueField="CountryId">
                            <Store>
                                <ext:Store ID="storeCountryAssign" runat="server">
                                    <Fields>
                                        <ext:ModelField Name="CountryId" Type="String" />
                                        <ext:ModelField Name="CountryName" />
                                    </Fields>
                                </ext:Store>
                            </Store>
                            <%--<DirectEvents>
                                            <Change OnEvent="CountryAssign_Change">
                                            </Change>
                                        </DirectEvents>--%>
                        </ext:ComboBox>
                    </td>
                </tr>
                <tr style="padding-bottom: 1em;">
                    <td colspan="5">
                        <ext:TextArea ID="txtPurposeOfTravelAssign" Width="550" runat="server" FieldLabel="Purpose of Travel"
                            LabelAlign="Top">
                        </ext:TextArea>
                    </td>
                </tr>
                <tr style="padding-bottom: 1em;">
                    <td colspan="1">
                        <ext:DateField FieldLabel="Travelling from" Width="150" ID="txtFromDateAssign" runat="server"
                            EmptyDate="" LabelAlign="Top" LabelSeparator="" AllowBlank="false">
                            <DirectEvents>
                                <Change OnEvent="CheckInOutAssignSelectChange">
                                </Change>
                            </DirectEvents>
                        </ext:DateField>
                    </td>
                    <td colspan="1">
                        <ext:DateField FieldLabel="Travelling to" Width="150" ID="txtToDateAssign" runat="server"
                            LabelAlign="Top" EmptyDate="" LabelSeparator="" AllowBlank="false">
                            <DirectEvents>
                                <Change OnEvent="CheckInOutAssignSelectChange">
                                </Change>
                            </DirectEvents>
                        </ext:DateField>
                    </td>
                    <td colspan="1" style="padding-top: 5px;">
                        <ext:Label ID="TextArea4" Width="30" runat="server" Style="margin-right: 25px; margin-left: -35px;"
                            Text="=">
                        </ext:Label>
                    </td>
                    <td colspan="1" style="padding-right: 7px;">
                        <ext:NumberField ID="txtDayCountAssign" runat="server" Width="60" FieldLabel="Days"
                            LabelAlign="Top">
                        </ext:NumberField>
                    </td>
                    <td colspan="1">
                        <ext:NumberField ID="txtNightCountAssign" runat="server" Width="60" FieldLabel="Night"
                            LabelAlign="Top">
                        </ext:NumberField>
                    </td>
                </tr>
                <tr style="padding-bottom: 1em;">
                     <td colspan="1">
                        <ext:ComboBox ID="cmbExpensePaidByAssign" runat="server" FieldLabel="Expense Paid by"
                            LabelAlign="Top" QueryMode="Local" DisplayField="Name" ValueField="Value">
                            <Store>
                                <ext:Store ID="storeExpensePaidByAssign" runat="server">
                                    <Fields>
                                        <ext:ModelField Name="Name" />
                                        <ext:ModelField Name="Value" Type="String" />
                                    </Fields>
                                </ext:Store>
                            </Store>
                        </ext:ComboBox>
                    </td>
                    <td colspan="1">
                        <ext:ComboBox ID="cmbTravelByAssign" runat="server" FieldLabel="Travel by" LabelAlign="Top"
                            QueryMode="Local" DisplayField="Name" ValueField="Value">
                            <Store>
                                <ext:Store ID="storeTravelByAssign" runat="server">
                                    <Fields>
                                        <ext:ModelField Name="Name" />
                                        <ext:ModelField Name="Value" Type="String" />
                                    </Fields>
                                </ext:Store>
                            </Store>
                            <Listeners>
                                <Select Handler="if(this.getValue()==='5') {#{txtOtherName}.show();} else {#{txtOtherName}.hide();}" />
                            </Listeners>
                        </ext:ComboBox>
                    </td>
                   <td colspan="3" style='padding-left:22px'>
                        <ext:TextField ID="txtOtherName" Width="120"  Hidden="true" runat="server" FieldLabel="Other Travel by"
                            LabelAlign="Top" LabelSeparator="">
                        </ext:TextField>
                    </td>
                </tr>
                <tr>
                    <td>
                        <ext:ComboBox ID="cmbLocationsAssign" ForceSelection="true" runat="server" Width="150"
                            FieldLabel="Location for Allowance *" LabelAlign="Top" LabelSeparator=" " QueryMode="Local"
                            DisplayField="LocationName" ValueField="LocationId">
                            <Store>
                                <ext:Store ID="storeLocationsAssign" runat="server">
                                    <Fields>
                                        <ext:ModelField Name="LocationId" Type="String" />
                                        <ext:ModelField Name="LocationName" />
                                    </Fields>
                                </ext:Store>
                            </Store>
                            <DirectEvents>
                                <Select OnEvent="cmbLocationsAssign_Change">
                                    <EventMask ShowMask="true" />
                                    <ExtraParams>
                                        <ext:Parameter Name="ValuesAssign" Value="Ext.encode(#{gridAllowancesAssign}.getRowsValues({selectedOnly : false}))"
                                            Mode="Raw" />
                                         <ext:Parameter Name="Locations" Value="Ext.encode(#{gridLocations}.getRowsValues({selectedOnly : false}))"
                                                        Mode="Raw" />
                                    </ExtraParams>
                                </Select>
                            </DirectEvents>
                        </ext:ComboBox>
                    </td>
                </tr>
                <tr style="padding-bottom: 1em;">
                    <td colspan="5">
                        <ext:GridPanel StyleSpec="margin-top:15px;" ID="gridAllowancesAssign" runat="server"
                            Width="550" Height="125" Cls="itemgrid">
                            <Store>
                                <ext:Store ID="storeAllowancesAssign" runat="server">
                                    <Model>
                                        <ext:Model ID="Model2" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="LineId" Type="String" />
                                                <ext:ModelField Name="RequestId" Type="string" />
                                                <ext:ModelField Name="AllowanceId" Type="string" />
                                                <ext:ModelField Name="AllowanceName" Type="string" />
                                                <ext:ModelField Name="Quantity" Type="Float" />
                                                <ext:ModelField Name="Rate" Type="Float" />
                                                <ext:ModelField Name="Total" Type="Float" />
                                                <ext:ModelField Name="IsEditable" Type="String" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                            <Plugins>
                                <ext:CellEditing ID="CellEditing1" ClicksToEdit="1" runat="server">
                                    <Listeners>
                                        <Edit Fn="afterEditAssign" />
                                    </Listeners>
                                </ext:CellEditing>
                            </Plugins>
                            <ColumnModel>
                                <Columns>
                                    <ext:Column ID="Column11" Sortable="false" MenuDisabled="true" runat="server" Text="Allowances"
                                        Align="Left" DataIndex="AllowanceName" Width="200" />
                                    <ext:Column ID="Column123" Sortable="false" MenuDisabled="true" runat="server" Text="Qty"
                                        Align="Left" DataIndex="Quantity" Width="100">
                                        <Editor>
                                            <ext:NumberField ID="txtQTY" SelectOnFocus="true" runat="server" AllowBlank="false" />
                                        </Editor>
                                    </ext:Column>
                                    <ext:Column ID="Column13" Sortable="false" MenuDisabled="true" runat="server" Text="Rate"
                                        Align="Left" DataIndex="Rate" Width="125">
                                        <Editor>
                                            <ext:NumberField ID="txtRate" SelectOnFocus="true" runat="server" AllowBlank="false">
                                            </ext:NumberField>
                                        </Editor>
                                    </ext:Column>
                                    <ext:Column runat="server" Width="125" ID="Total" Text="Total" Sortable="false" Groupable="false"
                                        DataIndex="Total" CustomSummaryType="totalCost">
                                        <Renderer Handler="return Ext.util.Format.number((record.data.Quantity * record.data.Rate),'0,000.00');" />
                                    </ext:Column>
                                </Columns>
                            </ColumnModel>
                            <SelectionModel>
                                <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" Mode="Single" />
                            </SelectionModel>
                            <Listeners>
                                <BeforeEdit Fn="beforeEdit" />
                            </Listeners>
                        </ext:GridPanel>
                    </td>
                </tr>
                <tr>
                    <td colspan="5" style='padding-left: 350px'>
                        <ext:DisplayField FieldStyle="text-align:right" Width="200px" Text="0" LabelSeparator=""
                            FieldLabel="Total" ID="dispTotal" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <div style="height: 5px;">
                        </div>
                    </td>
                </tr>
            </table>
            <table>
                <tr>
                    <td>
                        <div style="margin-top: 25px;">
                            <ext:Button ID="btnSaveAndLaterAssign" Width="150" Cls="btn btn-primary" runat="server"
                                StyleSpec=" float:right;" Text="&nbsp;&nbsp;Assign&nbsp;&nbsp;" ValidationGroup="InsertUpdateAssign">
                                <DirectEvents>
                                    <Click OnEvent="ButtonNextAssign_Click">
                                        <EventMask ShowMask="true" />
                                        <Confirmation ConfirmRequest="true" Message="Confirm each information is correct, after saving information can not be changed." />
                                        <ExtraParams>
                                            <ext:Parameter Name="AllowanceValuesAssign" Value="Ext.encode(#{gridAllowancesAssign}.getRowsValues({selectedOnly : false}))"
                                                Mode="Raw" />
                                            <ext:Parameter Name="Locations" Value="Ext.encode(#{gridLocations}.getRowsValues({selectedOnly : false}))"
                                                        Mode="Raw" />
                                        </ExtraParams>
                                    </Click>
                                </DirectEvents>
                                <Listeners>
                                    <Click Handler="valGroup = 'InsertUpdateAssign'; return CheckValidation();">
                                    </Click>
                                </Listeners>
                            </ext:Button>
                        </div>
                    </td>
                    <td style="padding-top: 24px;
    padding-left: 20px;">
                                    <ext:ComboBox ForceSelection="true" ID="cmbForwardList" runat="server" FieldLabel="Approval *"
                                        LabelAlign="Left" LabelWidth="60" Width="250" LabelSeparator=" " QueryMode="Local" DisplayField="Text"
                                        ValueField="ID">
                                        <Store>
                                            <ext:Store ID="store3" runat="server">
                                                <Fields>
                                                    <ext:ModelField Name="Text" />
                                                    <ext:ModelField Name="ID" Type="String" />
                                                </Fields>
                                            </ext:Store>
                                        </Store>
                                    </ext:ComboBox>
                                </td>
                </tr>
            </table>
        </div>
    </Content>
</ext:Window>
<asp:RequiredFieldValidator ID="reqdPlaceToTravel1" runat="server" ControlToValidate="txtPlaceToTravelAssign"
    ErrorMessage="Place to travel is required." Display="None" ValidationGroup="InsertUpdateAssign">
</asp:RequiredFieldValidator>
<asp:RequiredFieldValidator ID="reqdCountry1" runat="server" ControlToValidate="cmbCountryAssign"
    ErrorMessage="Country is required" Display="None" ValidationGroup="InsertUpdateAssign">
</asp:RequiredFieldValidator>
<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtPurposeOfTravelAssign"
    ErrorMessage="Purpose of Travel is required" Display="None" ValidationGroup="InsertUpdateAssign">
</asp:RequiredFieldValidator>
<asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtFromDateAssign"
    ErrorMessage="From Date is required" Display="None" ValidationGroup="InsertUpdateAssign">
</asp:RequiredFieldValidator>
<asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="txtToDateAssign"
    ErrorMessage="To Date is required" Display="None" ValidationGroup="InsertUpdate">
</asp:RequiredFieldValidator>
<asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="cmbTravelByAssign"
    ErrorMessage="Travel is required" Display="None" ValidationGroup="InsertUpdate">
</asp:RequiredFieldValidator>
<asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ControlToValidate="cmbExpensePaidByAssign"
    ErrorMessage="Expense Paid by is required" Display="None" ValidationGroup="InsertUpdateAssign">
</asp:RequiredFieldValidator>
<asp:RequiredFieldValidator ID="reqdLocations1" runat="server" ControlToValidate="cmbLocationsAssign"
    ErrorMessage="Location is required" Display="None" ValidationGroup="InsertUpdateAssign">
</asp:RequiredFieldValidator>



<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="cmbForwardList"
    ErrorMessage="Approval is required" Display="None" ValidationGroup="InsertUpdateAssign">
</asp:RequiredFieldValidator>