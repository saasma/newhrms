﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL;
using BLL.Manager;
using BLL;
using BLL.BO;
using System.IO;
using BLL.Base;
using Ext.Net;
using Utils.Helper;

namespace Web.Employee.UserControls
{
    public partial class EExtraActivitiesCtrl : BaseUserControl
    {
        public bool ShowEmpty = false;

        private bool _isDisplayMode = false;
        public bool IsDisplayMode
        {
            set
            {
                if (value == true)
                {
                    _isDisplayMode = value;
                    btnAddNewLine.Visible = false;
                    CommandColumn21.Visible = false;
                    CommandColumn2.Visible = false;
                    gridExtraActivity.Width = 920;
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();
            }

            JavascriptHelper.AttachPopUpCode(Page, "addActivity", "EExtraActivityDetlsPopup.aspx", 450, 500);
            JavascriptHelper.AttachPopUpCode(Page, "editActiv", "EExtraActivityDetlsPopup.aspx", 450, 500);
        }

        private void HideButtonBlock()
        {
            btnAddNewLine.Hide();
        }

        private void Initialise()
        {
            BindGrid();
        }

        private void BindGrid()
        {
            int EmployeeID = GetEmployeeID();
            gridExtraActivity.Store[0].DataSource = NewHRManager.GetExtraActivityByEmployeeId(EmployeeID);
            gridExtraActivity.Store[0].DataBind();
        }

        protected int GetEmployeeID()
        {
            int EmpID = 0;
            if (!string.IsNullOrEmpty(Request.QueryString["ID"]))
            {
                EmpID = int.Parse(Request.QueryString["ID"]);
            }
            else
            {
                EmpID = SessionManager.CurrentLoggedInEmployeeId;
            }
            return EmpID;
        }


        protected void gridExtraActivity_Command(object sender, DirectEventArgs e)
        {
            string commandName = e.ExtraParams["command"];
            int curricularId = int.Parse(e.ExtraParams["ID"]);



            switch (commandName)
            {
                case "Delete":
                    this.DeleteExtraActivity(curricularId);
                    break;
                case "Edit":
                    {
                        hdnCurricularId.Text = curricularId.ToString();
                        X.Js.Call("EditActivity");
                        break;
                    }
            }

        }

        private void DeleteExtraActivity(int curricularId)
        {
            Status status = NewHRManager.DeleteExtraActivity(curricularId);
            if (status.IsSuccess)
            {
                NewMessage.ShowNormalMessage("Record deleted successfully.");
                BindGrid();
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }
        }

        protected void btnReloadEActGrid_Click(object sender, DirectEventArgs e)
        {
            BindGrid();
        }
     

    }
}