﻿<%@ Control Language="C#" AutoEventWireup="true" EnableViewState="false" CodeBehind="EmpDetails.ascx.cs"
    Inherits="Web.Employee.UserControls.EmpDetails" %>
<%@ Register Src="~/NewHR/UserControls/EmploymentCtrlView.ascx" TagName="EmploymentHistoryCtl"
    TagPrefix="ucEmploymentHistoryCtl" %>
<%@ Register Src="~/NewHR/UserControls/HealthCtrlView.ascx" TagName="HealthCtl" TagPrefix="ucHealthCtl" %>
<%@ Register Src="~/NewHR/UserControls/FamilyCtrlView.ascx" TagName="FamilyCtl" TagPrefix="uc1" %>
<%@ Register Src="~/NewHR/UserControls/EducationCtrlView.ascx" TagName="EducationCtl" TagPrefix="ucEducation" %>
<%@ Register Src="~/NewHR/UserControls/TrainingCtrlView.ascx" TagName="TrainingCtl"
    TagPrefix="ucTraining" %>
<%@ Register Src="~/NewHR/UserControls/SeminarCtrlView.ascx" TagName="SeminarCtl" TagPrefix="ucSeminar" %>
<%@ Register Src="~/NewHR/UserControls/SkillSetCtrlView.ascx" TagName="SkillSetsCtl"
    TagPrefix="ucSkillSets" %>
<%@ Register Src="~/NewHR/UserControls/PublicationCtrlView.ascx" TagName="PublicationCtl"
    TagPrefix="ucPublication" %>
<%@ Register Src="~/NewHR/UserControls/LanguageCtrlView.ascx" TagName="LanguageSetsCtl"
    TagPrefix="ucLanguageSets" %>
<%@ Register Src="~/NewHR/UserControls/BranchTransferListCtl.ascx" TagName="BranchTrasferCtl"
    TagPrefix="uc1" %>
<%@ Register Src="~/NewHR/UserControls/StatusChangeHistoryCtrl.ascx" TagName="StatusChangeCtrl"
    TagPrefix="ucStatusChange" %>
<%@ Register Src="~/NewHR/UserControls/ExtraActivitiesCtrlView.ascx" TagName="ExtraActivity"
    TagPrefix="EA" %>
<%@ Register Src="~/NewHR/UserControls/NomineeCtrlView.ascx" TagName="HrNominee" TagPrefix="HrN" %>
<%@ Register Src="~/NewHR/UserControls/NonMonetaryAwardCtrlView.ascx" TagName="NMAward"
    TagPrefix="NMA" %>
<%@ Register Src="~/NewHR/UserControls/HobbyCtrlView.ascx" TagName="EHobbyControl" TagPrefix="EHC" %>
<%@ Register Src="~/NewHR/UserControls/ActingCtrlView.ascx" TagName="ActingCtl"
    TagPrefix="ctl" %>
<%@ Register Src="~/newHR/UserControls/EmployeeSummaryCtl.ascx" TagName="EmployeeSummaryCtl"
    TagPrefix="uc1" %>
<%@ Register Src="~/NewHR/UserControls/CitizenshipCtrlView.ascx" TagName="CitizenshipCtl"
    TagPrefix="ucCitizenshipCtl" %>
<%@ Register Src="~/NewHR/UserControls/DrivingLicenseCtrlView.ascx" TagName="DrivingLiscenceCtl"
    TagPrefix="ucDrivingLiscenceCtl" %>
<%@ Register Src="~/NewHR/UserControls/PassportCtrlView.ascx" TagName="PassportCtl"
    TagPrefix="ucPassportCtl" %>
<%@ Register Src="~/NewHR/UserControls/DocumentCtrlView.ascx" TagName="DocCtrl" TagPrefix="ucDoc" %>
<%@ Register Src="~/NewHR/UserControls/EmpPayInfoCtl.ascx" TagName="EmpPayCtl" TagPrefix="ucEmpPayCtl" %>
<%@ Register Src="~/NewHR/UserControls/EmpPayInsuranceInfoCtl.ascx" TagName="EmpPayInsurannceCtl"
    TagPrefix="ucEmpPayInsurannceCtl" %>
<%@ Register Src="~/NewHR/UserControls/EmpPayIncomeInfoCtl.ascx" TagName="EmpPayIncomeCtl"
    TagPrefix="ucEmpPayIncomeCtl" %>
<style type="text/css">
        .items
        {
            padding-left: 20px;
        }
        .items span
        {
            
            display: block;
            font-family: 'Open Sans' ,sans-serif;
            padding-bottom: 2px;
        }
        .items img
        {
            float:left;
            width:18px;height:18px;
            
margin-right: 10px;
        }
        
        .fieldTable span
        {
            width: 400px;
        }
        
        /*ext grid to nomal grid*/


        .buttonBlock
        {
            display: none;
        }
        #content
        {
            background: white;
        }

        }
        .accordion-edit:hover, .accordion-edit
        {
            font-weight: normal;
        }
        a:hover
        {
            
            font-weight: normal;
        }
        .accordion {
margin-bottom: 5px;

}

   </style>
<script type="text/javascript">

    function expandAllCollapseAllHandler(btn) {
        if (btn.getText() == "EXPAND") {
            btn.setText("COLLAPSE");

            var list = Ext.query(".accordion-toggle");
            for (i = 0; i < list.length; i++) {
                var item = list[i];
                item.className = "accordion-toggle collapsed";
            }


            list = Ext.query(".accordion-body");
            for (i = 0; i < list.length; i++) {
                var item = list[i];

                item.className = "accordion-body in collapse";
                item.style.height = "";
            }
        }
        else {
            btn.setText("EXPAND");


            var list = Ext.query(".accordion-toggle");
            for (i = 0; i < list.length; i++) {
                var item = list[i];
                item.className = "accordion-toggle";
            }

            list = Ext.query(".accordion-body");
            for (i = 0; i < list.length; i++) {
                var item = list[i];


                item.className = "accordion-body collapse";
                item.style.height = "";
            }
        }
    }


    function EditPrfl() {
        var companyName = <%=hdnCompanyName.ClientID %>.getValue();

        if(companyName == 'nibl')
        {
            window.location = 'EmpEditDetailsNew.aspx';
        }
        else
        {
            window.location = 'EditEmpDetails.aspx';
        }
    }

     function focusEvent(e1, tab) {
         <%= hdnSelectedTabName.ClientID %>.setValue(tab.title.toString().toLowerCase());
           <%= btnFireTab.ClientID %>.fireEvent('click');
    
    };


</script>
<ext:Label ID="lblMsg" runat="server" />
<ext:Hidden ID="hdnSelectedTabName" runat="server" />
<ext:Hidden ID="hdnCompanyName" runat="server" />
<ext:LinkButton ID="btnFireTab" runat="server" Hidden="true">
    <DirectEvents>
        <Click OnEvent="Tab_Change">
            <EventMask ShowMask="true" />
        </Click>
    </DirectEvents>
</ext:LinkButton>
<div style="margin-bottom: 10px">
    <div style="height: 250px; overflow-y: hidden; display: inline">
        <%-- <asp:Image ID="image1" runat="server" Width="200px" />--%>
        <asp:Image ID="image" CssClass="left" runat="server" ImageUrl="~/images/sample.jpg"
            Width="180px" />
    </div>
    <div class="left items">
        <h3 runat="server" class="heading" style="color: #214B71; margin: 0; margin-top: 2px;
            margin-bottom: 3px;" id="title">
        </h3>
        <span runat="server" style="font-size: 15px; font-weight: bold" id="positionDesignation">
        </span>
        <img runat="server" id="groupLevelImg" src="../../Styles/images/emp_position.png"
            style="margin-top: 8px;" />
        <span runat="server" style="padding-top: 10px;" id="groupLevel"></span>
        <img src="../../Styles/images/emp_br_dept.png" />
        <span runat="server" id="branch"></span>
        <img runat="server" id="imgMobile" src="../../Styles/images/emp__phone.png" />
        <span runat="server" id="contactMobile"></span>
        <img src="../../Styles/images/telephone.png" />
        <span runat="server" id="contactPhone"></span>
        <img src="../../Styles/images/emp_email.png" />
        <span runat="server" id="email">&nbsp;</span>
        <img src="../../Styles/images/emp_work.png" runat="server" id="workingForImage" />
        <span runat="server" id="workingFor" style="width: 500px"></span>
    </div>
    <div style="float: left; margin-left: 100px;">
        <ext:Button runat="server" Height="30px" Width="120px" ID="btnEdit" Text="<i></i>Edit Profile"
            runat="server">
            <Listeners>
                <Click Fn="EditPrfl" />
            </Listeners>
        </ext:Button>
    </div>
    <div style="clear: both">
    </div>
</div>
<br />
<ext:TabPanel ID="Panel2" runat="server" MinHeight="600" AutoHeight="true" StyleSpec="padding-top:10px"
    AutoWidth="true" DefaultBorder="false">
    <Items>
        <ext:Panel ID="Panel1" Title="Personal Info" runat="server">
            <Content>
                <div class="panel panel-default">
                    <div class="panel-heading" style="padding-top: 4px!important; padding-bottom: 4px!important">
                        <h3 class="panel-title">
                            Employee Details</h3>
                        <a href="~/newhr/PersonalDetail.aspx?ID=" runat="server" id="editPersonalInfo" class="accordion-edit">
                            <img src="../../Styles/images/side_edit.png" />
                        </a>
                    </div>
                    <div class="panel-body panel-body-small">
                        <table>
                            <tr>
                                <td valign="top">
                                    <table class="fieldTable firsttdskip">
                                        <tr>
                                            <td style="width: 170px;">
                                                I No
                                            </td>
                                            <td>
                                                <span runat="server" id="ein"></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Date of Birth
                                            </td>
                                            <td>
                                                <span runat="server" id="dob"></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td runat="server" id="genderText">
                                                {0}
                                            </td>
                                            <td>
                                                <span runat="server" id="gender"></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Marital Status
                                            </td>
                                            <td>
                                                <span runat="server" id="maritalStatus"></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Father's Name
                                            </td>
                                            <td>
                                                <span runat="server" id="fatherName"></span>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td style="width: 100px">
                                </td>
                                <td valign="top">
                                    <table class="fieldTable firsttdskip">
                                        <tr runat="server" id="trLevel">
                                            <td style="width: 170px;">
                                                Level/Position
                                            </td>
                                            <td>
                                                <span runat="server" id="level"></span>
                                            </td>
                                        </tr>
                                        <tr runat="server" id="trGrade">
                                            <td style="width: 170px;">
                                                Grade
                                            </td>
                                            <td>
                                                <span runat="server" id="gradeDisplay"></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Hire Method
                                            </td>
                                            <td>
                                                <span runat="server" id="hireMethod"></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Hire Date
                                            </td>
                                            <td>
                                                <span runat="server" id="hireDate"></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Service Status
                                            </td>
                                            <td>
                                                <span runat="server" id="currentJobStatus"></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Contract End Date
                                            </td>
                                            <td>
                                                <span runat="server" id="contractEndDate"></span>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" style="padding-top: 4px!important; padding-bottom: 4px!important">
                        <h3 class="panel-title">
                            Address</h3>
                        <a class="accordion-toggle"></a><a href="~/Employee/HR/EmpAddress.aspx?ID=" runat="server"
                            id="editAddress" class="accordion-edit">
                            <img src="../../Styles/images/side_edit.png" /></a>
                    </div>
                    <div class="panel-body panel-body-small">
                        <table class="fieldTable firsttdskip">
                            <%--locality district country--%>
                            <tr>
                                <td style="width: 170px;" valign="top">
                                    Permanent Address
                                </td>
                                <td valign="top">
                                    <span runat="server" id="addressPermanent"></span>
                                </td>
                                <td width="50">
                                </td>
                                <td valign="top">
                                    Temporary Address
                                </td>
                                <td valign="top">
                                    <span runat="server" id="addressTemporary"></span>
                                </td>
                            </tr>
                            <tr runat="server" id="rowCitizenIssueAddress" visible="false">
                                <td style="width: 170px;" valign="top">
                                   Citizenship Issue Address
                                </td>
                                <td valign="top">
                                    <span runat="server" id="spanCitizenIssueAddress"></span>
                                </td>
                              
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" style="padding-top: 4px!important; padding-bottom: 4px!important">
                        <h3 class="panel-title">
                            Contact Information
                        </h3>
                        <a href="~/Employee/HR/EmpAddress.aspx?ID=" runat="server" id="editContact" class="accordion-edit">
                            <img src="../../Styles/images/side_edit.png" /></a>
                    </div>
                    <div class="panel-body panel-body-small">
                        <table class="fieldTable firsttdskip">
                            <tr>
                                <td style="width: 170px;">
                                    Extension No.
                                </td>
                                <td>
                                    <span runat="server" id="extNo"></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Personal No.
                                </td>
                                <td>
                                    <span runat="server" id="noPersonalNo"></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Mobile No.
                                </td>
                                <td>
                                    <span runat="server" id="noPersonalMobile"></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Emergency Contact Person .
                                </td>
                                <td>
                                    <span runat="server" id="contactEmergency"></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Emergency Contact No.
                                </td>
                                <td>
                                    <span runat="server" id="contactEmergencyNo"></span>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </Content>
        </ext:Panel>
        <ext:Panel ID="Panel3" Title="Service Details" runat="server">
            <Content>
                <div class="panel panel-default" runat="server" id="divStatusChange">
                    <div class="panel-heading" style="padding-top: 4px!important; padding-bottom: 4px!important">
                        <h3 class="panel-title">
                            Status Change
                        </h3>
                    </div>
                    <div class="panel-body panel-body-small">
                        <ucStatusChange:StatusChangeCtrl Id="statusChgCtrl" runat="server" />
                    </div>
                </div>
                <div class="panel panel-default" runat="server" id="levelGradeHistory">
                    <div class="panel-heading" style="padding-top: 4px!important; padding-bottom: 4px!important">
                        <h3 class="panel-title">
                            Level and Grade History
                        </h3>
                    </div>
                    <div class="panel-body panel-body-small">
                        <ext:GridPanel ID="gridLevelGradeHistory" runat="server" Width="1100" Cls="itemgrid">
                            <Store>
                                <ext:Store ID="StorePreviousEmployment" runat="server">
                                    <Model>
                                        <ext:Model ID="Model1" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="FromDate" Type="String" />
                                                <ext:ModelField Name="OldLevel" Type="string" />
                                                <ext:ModelField Name="PrevStepGrade" Type="string" />
                                                <ext:ModelField Name="NewLevel" Type="string" />
                                                <ext:ModelField Name="StepGrade" Type="string" />
                                                <ext:ModelField Name="ChangeType" Type="string" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                            <ColumnModel>
                                <Columns>
                                    <ext:Column ID="Column1" runat="server" Text="From Date" DataIndex="FromDate" Width="120" />
                                    <ext:Column ID="Column2" runat="server" Text="Old Level" DataIndex="OldLevel" Width="280">
                                    </ext:Column>
                                    <ext:Column ID="Column3" runat="server" Text="Old Grade" DataIndex="PrevStepGrade"
                                        Width="120">
                                    </ext:Column>
                                    <ext:Column ID="Column4" runat="server" Text="New Level" DataIndex="NewLevel" Width="280">
                                    </ext:Column>
                                    <ext:Column ID="Column5" runat="server" Text="New Grade" DataIndex="StepGrade" Width="120">
                                    </ext:Column>
                                    <ext:Column ID="Column6" runat="server" Text="Change Type" DataIndex="ChangeType"
                                        Width="200">
                                    </ext:Column>
                                    <ext:Column ID="Column7" Width="200" runat="server" Text="">
                                    </ext:Column>
                                </Columns>
                            </ColumnModel>
                              <View>
                                                        <ext:GridView EnableTextSelection="true" />
                                                    </View>
                            <SelectionModel>
                                <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" Mode="Single" />
                            </SelectionModel>
                        </ext:GridPanel>
                    </div>
                </div>
                <div class="panel panel-default" runat="server" id="divNIBLServiceHistory">
                    <div class="panel-heading" style="padding-top: 4px!important; padding-bottom: 4px!important">
                        <h3 class="panel-title">
                            Service History
                        </h3>
                    </div>
                    <div class="panel-body panel-body-small">
                        <asp:GridView ID="gridServiceHistory" runat="server" GridLines="None" CssClass="table mb30 "
                            HeaderStyle-BackColor="#428BCA" HeaderStyle-ForeColor="White" ShowHeader="true"
                            Width="1000" AutoGenerateColumns="false">
                            <Columns>
                                <asp:BoundField DataField="RowNumber" ItemStyle-HorizontalAlign="Center" HeaderText="SN"
                                    ItemStyle-Width="60" />
                                <asp:BoundField DataField="EventName" HeaderText="Event" ItemStyle-Width="160" />
                                <asp:BoundField DataField="Date" HeaderText="Nepali Date" ItemStyle-Width="100" />
                                <asp:BoundField DataField="DateEng" DataFormatString="{0:dd-MMM-yyyy}" HeaderText="English Date"
                                    ItemStyle-Width="100" />
                                <asp:BoundField DataField="Branch" HeaderText="Branch" ItemStyle-Width="120" />
                                <asp:BoundField DataField="Department" HeaderText="Department" ItemStyle-Width="120" />
                                <asp:BoundField DataField="ServiceStatus" HeaderText="Status" ItemStyle-Width="120" />
                                <asp:BoundField DataField="Designation" HeaderText="Position" ItemStyle-Width="120" />
                                <asp:TemplateField ItemStyle-Width="100">
                                    <ItemTemplate>
                                        <asp:HyperLink ID="HyperLink1" NavigateUrl='javscript:void(0)' onclick='<%# "showDetailsNew(" + Eval("EventSourceID") +"," + Eval("RowNumber") + ");return false;" %>'
                                            runat="server" Text="Details">
                                                            
                                        </asp:HyperLink>
                                       <%-- <asp:HyperLink ID="HyperLink1" NavigateUrl='javscript:void(0)' 
                                            runat="server" Text="Details">
                                                            
                                        </asp:HyperLink>--%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                    <uc1:EmployeeSummaryCtl Id="empSummaryDetails" runat="server" />
                </div>
                <div class="panel panel-default" runat="server" id="divBranchTransfer">
                    <div class="panel-heading" style="padding-top: 4px!important; padding-bottom: 4px!important">
                        <h3 class="panel-title">
                            Branch Transfer
                        </h3>
                    </div>
                    <div class="panel-body panel-body-small">
                        <uc1:BranchTrasferCtl Id="ActingCtl1" runat="server" />
                    </div>
                </div>
                <div class="panel panel-default" runat="server" id="actingHistory">
                    <div class="panel-heading" style="padding-top: 4px!important; padding-bottom: 4px!important">
                        <h3 class="panel-title">
                            Acting
                        </h3>
                    </div>
                    <div class="panel-body panel-body-small">
                        <ctl:ActingCtl Id="ctlActing" runat="server" />
                    </div>
                </div>
            </Content>
        </ext:Panel>
          <ext:Panel ID="Panel9" Title="&nbsp;&nbsp;&nbsp;Pay&nbsp;&nbsp;&nbsp;" runat="server">
            <Content>
              
                <div class="panel panel-default">
                    <div class="panel-heading" style="padding-top: 4px!important; padding-bottom: 4px!important">
                        <h3 class="panel-title">
                            Pay Info
                        </h3>
                      
                    </div>
                    <div class="panel-body panel-body-small">
                        <ucEmpPayCtl:EmpPayCtl Id="ucEmpPayInfo" IsDisplayMode="true" runat="server" />
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" style="padding-top: 4px!important; padding-bottom: 4px!important">
                        <h3 class="panel-title">
                            Current Year Insurance
                        </h3>
                        <%--   <a href="~/Employee/HR/EmpEducationTraining.aspx?id=" runat="server" id="A2"
                            class="accordion-edit">
                            <img src="../../Styles/images/side_edit.png" /></a>--%>
                    </div>
                    <div class="panel-body panel-body-small">
                        <ucEmpPayInsurannceCtl:EmpPayInsurannceCtl Id="ucEmpPayInsurannceCtl1" IsDisplayMode="true"
                            runat="server" />
                    </div>
                </div>
                <div class="panel panel-default" runat="server" id="pnlIncomeList">
                    <div class="panel-heading" style="padding-top: 4px!important; padding-bottom: 4px!important">
                        <h3 class="panel-title">
                            Income List
                        </h3>
                     
                    </div>
                    <div class="panel-body panel-body-small">
                        <ucEmpPayIncomeCtl:EmpPayIncomeCtl Id="EmpPayIncomeCtl1" IsDisplayMode="true"
                            runat="server" />
                    </div>
                </div>
            </Content>
        </ext:Panel>
        <ext:Panel ID="Panel4" Title="Education and Training" runat="server">
            <Content>
                <div class="panel panel-default">
                    <div class="panel-heading" style="padding-top: 4px!important; padding-bottom: 4px!important">
                        <h3 class="panel-title">
                            Education
                        </h3>
                        <a href="~/Employee/HR/EmpEducationTraining.aspx?id=" runat="server" id="skill9"
                            class="accordion-edit">
                            <img src="../../Styles/images/side_edit.png" /></a>
                    </div>
                    <div class="panel-body panel-body-small">
                        <ucEducation:EducationCtl Id="EducationCtl" runat="server" />
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" style="padding-top: 4px!important; padding-bottom: 4px!important">
                        <h3 class="panel-title">
                            Training
                        </h3>
                        <a href="~/Employee/HR/EmpEducationTraining.aspx?id=" runat="server" id="skill10"
                            class="accordion-edit">
                            <img src="../../Styles/images/side_edit.png" /></a>
                    </div>
                    <div class="panel-body panel-body-small">
                        <ucTraining:TrainingCtl Id="TrainingCtl1" runat="server" />
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" style="padding-top: 4px!important; padding-bottom: 4px!important">
                        <h3 class="panel-title">
                            Seminar
                        </h3>
                        <a href="~/Employee/HR/EmpEducationTraining.aspx?id=" runat="server" id="skill11"
                            class="accordion-edit">
                            <img src="../../Styles/images/side_edit.png" /></a>
                    </div>
                    <div class="panel-body panel-body-small">
                        <ucSeminar:SeminarCtl Id="SeminarCtl1" runat="server" />
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" style="padding-top: 4px!important; padding-bottom: 4px!important">
                        <h3 class="panel-title">
                            Previous Employment
                        </h3>
                        <a href="~/Employee/HR/EmpPreviousEmployment.aspx?id=" runat="server" id="prevEmployment"
                            class="accordion-edit">
                            <img src="../../Styles/images/side_edit.png" /></a>
                    </div>
                    <div class="panel-body panel-body-small">
                        <ucEmploymentHistoryCtl:EmploymentHistoryCtl Id="ucEmploymentHistoryCtl1"
                            runat="server" />
                    </div>
                </div>
            </Content>
        </ext:Panel>
        <ext:Panel ID="Panel5" Title="Skills and Languages" runat="server">
            <Content>
                <div class="panel panel-default">
                    <div class="panel-heading" style="padding-top: 4px!important; padding-bottom: 4px!important">
                        <h3 class="panel-title">
                            Skill Set
                        </h3>
                        <a href="~/Employee/HR/EmpEducationTraining.aspx?id=" runat="server" id="skill12"
                            class="accordion-edit">
                            <img src="../../Styles/images/side_edit.png" /></a>
                    </div>
                    <div class="panel-body panel-body-small">
                        <ucSkillSets:SkillSetsCtl Id="SkillSetsCtl1" runat="server" />
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" style="padding-top: 4px!important; padding-bottom: 4px!important">
                        <h3 class="panel-title">
                            Languages
                        </h3>
                        <a href="~/Employee/HR/EmpEducationTraining.aspx?id=" runat="server" id="skill13"
                            class="accordion-edit">
                            <img src="../../Styles/images/side_edit.png" /></a>
                    </div>
                    <div class="panel-body panel-body-small">
                        <ucLanguageSets:LanguageSetsCtl Id="LanguageSetsCtl1" runat="server" />
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" style="padding-top: 4px!important; padding-bottom: 4px!important">
                        <h3 class="panel-title">
                            Publication
                        </h3>
                        <a href="~/Employee/HR/EmpEducationTraining.aspx?id=" runat="server" id="skill14"
                            class="accordion-edit">
                            <img src="../../Styles/images/side_edit.png" /></a>
                    </div>
                    <div class="panel-body panel-body-small">
                        <ucPublication:PublicationCtl Id="PublicationCtl1" runat="server" />
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" style="padding-top: 4px!important; padding-bottom: 4px!important">
                        <h3 class="panel-title">
                            Extra Activities
                        </h3>
                        <a href="~/Employee/HR/EmpEducationTraining.aspx?id=" runat="server" id="skill15"
                            class="accordion-edit">
                            <img src="../../Styles/images/side_edit.png" /></a>
                    </div>
                    <div class="panel-body panel-body-small">
                        <EA:ExtraActivity Id="ExtAct" runat="server" />
                    </div>
                </div>
            </Content>
        </ext:Panel>
        <ext:Panel ID="Panel6" Title="Health and Family" runat="server">
            <Content>
                <div class="panel panel-default">
                    <div class="panel-heading" style="padding-top: 4px!important; padding-bottom: 4px!important">
                        <h3 class="panel-title">
                            Health
                        </h3>
                        <a href="~/newhr/health.aspx?id=" runat="server" id="health" class="accordion-edit">
                            <img src="../../Styles/images/side_edit.png" /></a>
                    </div>
                    <div class="panel-body panel-body-small">
                        <ucHealthCtl:HealthCtl Id="ucHealthCtl1" runat="server" />
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" style="padding-top: 4px!important; padding-bottom: 4px!important">
                        <h3 class="panel-title">
                            Family
                        </h3>
                        <a href="~/Employee/HR/EmpFamilyList.aspx?id=" runat="server" id="family" class="accordion-edit">
                            <img src="../../Styles/images/side_edit.png" /></a>
                    </div>
                    <div class="panel-body panel-body-small">
                        <uc1:FamilyCtl Id="EmployeeDetailsCtl2" runat="server" />
                    </div>
                </div>
            </Content>
        </ext:Panel>
        <ext:Panel ID="Panel8" Title="Identification" runat="server">
            <Content>
                <div class="panel panel-default">
                    <div class="panel-heading" style="padding-top: 4px!important; padding-bottom: 4px!important">
                        <h3 class="panel-title">
                            Citizenship
                        </h3>
                    </div>
                    <div class="panel-body panel-body-small">
                        <ucCitizenshipCtl:CitizenshipCtl Id="ucCitizenshipCtl1" runat="server" IsDisplayMode="true" />
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" style="padding-top: 4px!important; padding-bottom: 4px!important">
                        <h3 class="panel-title">
                            Driving License
                        </h3>
                    </div>
                    <div class="panel-body panel-body-small">
                        <ucDrivingLiscenceCtl:DrivingLiscenceCtl Id="DrivingLiscenceCtl1" runat="server"
                            IsDisplayMode="true" />
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" style="padding-top: 4px!important; padding-bottom: 4px!important">
                        <h3 class="panel-title">
                            Passport
                        </h3>
                    </div>
                    <div class="panel-body panel-body-small">
                        <ucPassportCtl:PassportCtl Id="PassportCtl1" runat="server" IsDisplayMode="true" />
                    </div>
                </div>
            </Content>
        </ext:Panel>
        <ext:Panel ID="PanelDocument" Title="Document" runat="server">
            <Content>
                <div class="panel panel-default">
                    <br />
                    <ucDoc:DocCtrl Id="ucDocument" IsDisplayMode="true" runat="server" />
                </div>
            </Content>
        </ext:Panel>
        <ext:Panel ID="Panel7" Title="Others" runat="server">
            <Content>
                <div class="panel panel-default">
                    <div class="panel-heading" style="padding-top: 4px!important; padding-bottom: 4px!important">
                        <h3 class="panel-title">
                            Nominees
                        </h3>
                        <a href="~/Employee/HR/EmpEducationTraining.aspx?id=" runat="server" id="skill16"
                            class="accordion-edit">
                            <img src="../../Styles/images/side_edit.png" /></a>
                    </div>
                    <div class="panel-body panel-body-small">
                        <HrN:HrNominee IsDisplayMode="true" Id="HrNom" runat="server" />
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" style="padding-top: 4px!important; padding-bottom: 4px!important">
                        <h3 class="panel-title">
                            Non Monetary Award
                        </h3>
                        <a href="~/Employee/HR/EmpEducationTraining.aspx?id=" runat="server" id="skill17"
                            class="accordion-edit"></a>
                    </div>
                    <div class="panel-body panel-body-small">
                        <NMA:NMAward Id="NMAwardCtrl" runat="server" IsDisplayMode="true" />
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" style="padding-top: 4px!important; padding-bottom: 4px!important">
                        <h3 class="panel-title">
                            Hobby
                        </h3>
                        <a href="~/Employee/HR/EmpEducationTraining.aspx?id=" runat="server" id="skill18"
                            class="accordion-edit"></a>
                    </div>
                    <div class="panel-body panel-body-small">
                        <EHC:EHobbyControl Id="EHCCtrl" runat="server" IsDisplayMode="true" />
                    </div>
                </div>
            </Content>
        </ext:Panel>
      
    </Items>
    <Listeners>
        <TabChange Fn="focusEvent">
        </TabChange>
    </Listeners>
</ext:TabPanel>
