﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ECitizenshipCtrl.ascx.cs"
    Inherits="Web.Employee.UserControls.ECitizenshipCtrl" %>
<ext:Hidden runat="server" ID="hdnCitizenshipID">
</ext:Hidden>
<ext:Hidden runat="server" ID="hdnIsEmployee" />
<ext:LinkButton ID="btnLoadCtznGrid" runat="server" Hidden="true">
    <DirectEvents>
        <Click OnEvent="btnLoadCtznGrid_Click">
            <EventMask ShowMask="true" />
        </Click>
    </DirectEvents>
</ext:LinkButton>
<ext:GridPanel ID="GridCitizenship" StyleSpec="margin-top:15px;" runat="server" Width="1000"
    Cls="itemgrid" Scroll="None">
    <Store>
        <ext:Store ID="StoreCitizenship" runat="server">
            <Model>
                <ext:Model ID="Model1" runat="server" IDProperty="CitizenshipId">
                    <Fields>
                        <ext:ModelField Name="Nationality" Type="string" />
                        <ext:ModelField Name="CitizenshipNo" Type="String" />
                        <ext:ModelField Name="IssueDate" Type="string" />
                        <ext:ModelField Name="Place" Type="string" />
                        <ext:ModelField Name="Status" Type="Int" />
                        <ext:ModelField Name="ServerFileName" Type="String" />
                    </Fields>
                </ext:Model>
            </Model>
        </ext:Store>
    </Store>
    <ColumnModel>
        <Columns>
            <ext:Column ID="Column1" runat="server" Text="Nationality" DataIndex="Nationality"
                Wrap="true" Width="150" Flex="1" />
            <ext:Column ID="Column2" runat="server" Text="Citizenship Number" DataIndex="CitizenshipNo"
                Wrap="true" Width="150">
            </ext:Column>
            <ext:Column ID="Column3" runat="server" Text="Issue Date" DataIndex="IssueDate" Width="100">
            </ext:Column>
            <ext:Column ID="Column4" runat="server" Text="Place" DataIndex="Place" Width="200"
                Wrap="true">
            </ext:Column>
            <ext:CommandColumn ID="CommandColumn1" runat="server" Width="40">
                <Commands>
                    <ext:CommandSpacer />
                    <ext:GridCommand Icon="PageWhitePut" CommandName="DownLoad">
                        <ToolTip Text="DownLoad" />
                    </ext:GridCommand>
                </Commands>
                <DirectEvents>
                    <Command OnEvent="GridCitizenship_Command">
                        <ExtraParams>
                            <ext:Parameter Name="ID" Value="record.data.CitizenshipId" Mode="Raw">
                            </ext:Parameter>
                            <ext:Parameter Name="command" Value="command" Mode="Raw">
                            </ext:Parameter>
                        </ExtraParams>
                    </Command>
                </DirectEvents>
                <PrepareToolbar Fn="prepareDownloadCtzn" />
            </ext:CommandColumn>
            <ext:CommandColumn ID="CommandColumn3" runat="server" Width="40">
                <Commands>
                    <ext:CommandSpacer />
                    <ext:GridCommand ToolTip-Text="Edit" Icon="ApplicationEdit" CommandName="Edit" />
                </Commands>
                <Listeners>
                    <Command Handler="CommandHandlerCtzn(command,record);" />
                </Listeners>
            </ext:CommandColumn>
            <ext:CommandColumn ID="CommandColumn2" runat="server" Width="40">
                <Commands>
                    <ext:CommandSpacer />
                    <ext:GridCommand ToolTip-Text="Delete" Icon="ApplicationDelete" CommandName="Delete" />
                </Commands>
                <DirectEvents>
                    <Command OnEvent="GridCitizenship_Command">
                        <ExtraParams>
                            <ext:Parameter Name="ID" Value="record.data.CitizenshipId" Mode="Raw">
                            </ext:Parameter>
                            <ext:Parameter Name="command" Value="command" Mode="Raw">
                            </ext:Parameter>
                        </ExtraParams>
                        <Confirmation ConfirmRequest="true" Message="Are you sure, you want to delete the record?" />
                    </Command>
                </DirectEvents>
                <PrepareToolbar Fn="prepareCitizenship" />
            </ext:CommandColumn>
        </Columns>
    </ColumnModel>
    <SelectionModel>
        <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" Mode="Single" />
    </SelectionModel>
</ext:GridPanel>
<div class="buttonBlockSection">
    <ext:Button runat="server" Cls="btn btn-primary btn-sect" Width="150" StyleSpec="margin-top:10px"
        OnClientClick="AddCitizenship();return false;" Height="30" ID="btnAddCitizenship"
        Text="<i></i>Add New Line">
    </ext:Button>
</div>
<script type="text/javascript">

    var prepareCitizenship = function (grid, toolbar, rowIndex, record) {
        var delBtn = toolbar.items.get(1);
        var IsEmployee = <%= hdnIsEmployee.ClientID %>.getValue();

        if (record.data.Status == 1 && IsEmployee == "1" ) {
            delBtn.setVisible(false);
        }

    } 

    var prepareDownloadCtzn = function(grid, toolbar, rowIndex, record){
        var downloadBtn = toolbar.items.get(1);
        if(record.data.ServerFileName == null || record.data.ServerFileName == ''){
            downloadBtn.setVisible(false);     
        }
    }

    function reloadCtznGrid() {  
        <%= btnLoadCtznGrid.ClientID %>.fireEvent('click');
    }

    function AddCitizenship()
    {
        addCitizenship();
    }

    var CommandHandlerCtzn = function(command, record){
            if(command=="Edit")
            {
                addCitizenship('CitizenshipId=' + record.data.CitizenshipId);
            }
                
        }

</script>
