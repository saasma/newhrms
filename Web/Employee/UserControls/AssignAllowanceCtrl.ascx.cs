﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Manager;
using DAL;
using Utils.Helper;
using Utils.Web;
using System.Text;
using BLL;
using System.Text.RegularExpressions;
using Ext.Net;
using BLL.Base;
using BLL.BO;

namespace Web.Employee.UserControls
{
    public partial class AssignAllowanceCtrl : BaseUserControl
    {
        public bool IsDisplayedAsPopup
        {
            get
            {
                if (Request.QueryString["isPopup"] != null)
                    return true;
                return false;
            }
        }

        public bool HasImport
        {
            get
            {
                if (ViewState["import"] == null)
                    return false;
                return Convert.ToBoolean(ViewState["import"]);
            }
            set
            {
                ViewState["import"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                Initialise();

        }

        void Initialise()
        {
            txtDays.FieldLabel = AllowanceManager.AllowanceDaysOrHourLabel + " *";

            //ddlEmployeeList.DataSource = LeaveAttendanceManager.GetEmployeeListForLeaveAssign();
            //ddlEmployeeList.DataBind();

            ddlAllowanceType.DataSource = AllowanceManager.GetAllEveningCounterList()
                .Where(x => x.DoNotShowInEmployeePortal == false || x.DoNotShowInEmployeePortal == null).ToList();
            ddlAllowanceType.DataBind();

            if (SessionManager.CurrentLoggedInEmployeeId == 0)
            {
                rowManager.Visible = false;
                rowManager2.Visible = false;
            }
        }

        new void Load()
        {
            string hr;
            string min;
            string hrmin;
            string employeename;


            EveningCounterRequest request = new EveningCounterRequest();
            request = AllowanceManager.getEveningCounterRequestByID(int.Parse(Hidden_RequestID.Value));
            
            if (request.EveningCounterTypeId != null)
            {
                UIHelper.SetSelectedInDropDown(ddlAllowanceType, request.EveningCounterTypeId.Value);
                EveningCounterType type1 = new EveningCounterType();
                type1 = AllowanceManager.GetEveningCounterType(request.EveningCounterTypeId.Value);

                if (type1 != null)
                    Hidden_CounterTypePrev.Value = type1.Name;
            }


            if (request.Status != (int)EveningCounterStatusEnum.Approved)
            {
                divWarningMsg.InnerHtml = "Allowance is in " +
                    ((EveningCounterStatusEnum)request.Status) + " status, only Approved status allowance can be edited.";
                divWarningMsg.Hide = false;

                btnUpdate.Visible = false;
            }


            txtFromDate.SelectedDate = request.StartDate.Value;
            txtToDate.SelectedDate = request.EndDate.Value;

            if (request.Days == null)
                txtDays.Text = "";
            else
                txtDays.Text = request.Days.ToString();
            //txtDays.Text = request.Days.ToString();

            Hidden_FromPrev.Value = txtFromDate.Text;
            Hidden_ToPrev.Value = txtToDate.Text;
            Hidden_DaysPrev.Value = txtDays.Text;


            Date_Change(null, null);

        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "sdf", "var hasImport = " + (this.HasImport ? "true;" : "false;"), true);
        }

        public void ddlEmployeeList_SelectionChange(object sender, EventArgs e)
        {
            int employeeId = int.Parse(hdnEmployeeId.Value);
            System.Web.UI.WebControls.ListItem itemRec = cmbRecommender.Items[0];
            System.Web.UI.WebControls.ListItem itemApr = cmbApproval.Items[0];
            if (employeeId == -1)
            {
                cmbRecommender.DataSource = new List<TextValue>();
                cmbRecommender.DataBind();

                cmbApproval.DataSource = new List<TextValue>();
                cmbApproval.DataBind();
            }
            else
            {
                List<TextValue> listReview = LeaveRequestManager.GetApplyToListForEmployee(employeeId, false, PreDefindFlowType.Overtime);
                List<TextValue> listApproval = LeaveRequestManager.GetApplyToListForEmployee(employeeId, true, PreDefindFlowType.Overtime);

                cmbRecommender.DataSource = listReview;
                cmbRecommender.DataBind();

                cmbApproval.DataSource = listApproval;
                cmbApproval.DataBind();
            }

            cmbRecommender.Items.Insert(0, itemRec);
            cmbApproval.Items.Insert(0, itemApr);

            UIHelper.SetSelectedInDropDown(cmbRecommender, SessionManager.CurrentLoggedInEmployeeId);
            UIHelper.SetSelectedInDropDown(cmbApproval, SessionManager.CurrentLoggedInEmployeeId);
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            Page.Validate("SaveUpdate");
            if (Page.IsValid)
            {
                EveningCounterRequest requestInstance = new EveningCounterRequest();


                if (ddlAllowanceType.SelectedItem != null && ddlAllowanceType.SelectedItem.Value != "-1")
                    requestInstance.EveningCounterTypeId = int.Parse(ddlAllowanceType.SelectedValue);
                else
                {
                    NewMessage.ShowWarningMessage("Allowance Type is required.");
                    ddlAllowanceType.Focus();
                    return;
                }

                requestInstance.StartDate = txtFromDate.SelectedDate;
                requestInstance.EndDate = txtToDate.SelectedDate;
                requestInstance.Days = double.Parse(txtDays.Text.Trim());


                if (txtFromDate.SelectedDate == DateTime.MinValue)
                {
                    NewMessage.ShowWarningMessage("Invalid from date.");
                    return;
                }

                if (txtFromDate.SelectedDate != null)
                {
                    requestInstance.StartDate = txtFromDate.SelectedDate;

                }
                else
                {
                    NewMessage.ShowWarningMessage("From Date is required.");
                    return;
                }

                if (txtToDate.SelectedDate != null)
                {
                    requestInstance.EndDate = txtToDate.SelectedDate;
                }
                else
                {
                    NewMessage.ShowWarningMessage("To Date is required");
                    return;
                }


                if (ddlAllowanceType.SelectedItem != null)
                    requestInstance.EveningCounterTypeId = int.Parse(ddlAllowanceType.SelectedItem.Value);
                else
                {
                    NewMessage.ShowNormalPopup("Allowance Type is required.");
                    return;
                }

                if(!string.IsNullOrEmpty(hdnEmployeeId.Value))
                    requestInstance.EmployeeID = int.Parse(hdnEmployeeId.Value);
                else
                {
                    NewMessage.ShowNormalPopup("Employee is required.");
                    return;
                }

                requestInstance.CreatedBy = SessionManager.CurrentLoggedInUserID;
                requestInstance.CreatedOn = CommonManager.GetCurrentDateAndTime();

                requestInstance.Reason = txtReason.Text;

                if (LeaveAttendanceManager.IsEmployeeAllowedToApproveLeaveForEmployee(requestInstance.EmployeeID.Value))
                    requestInstance.Status = (int)OvertimeStatusEnum.Approved;
                else
                    requestInstance.Status = (int)OvertimeStatusEnum.Recommended;
                requestInstance.ApprovalID = int.Parse(cmbApproval.SelectedItem.Value);
                requestInstance.ApprovalName = cmbApproval.SelectedItem.Text;
                requestInstance.RecommendedBy = int.Parse(cmbRecommender.SelectedItem.Value);

                string daysMathStr = ((txtToDate.SelectedDate.Date - txtFromDate.SelectedDate.Date).TotalDays + 1).ToString();

                string daysUserStr = txtDays.Text;
                float daysUser;
                float daysMath;
                bool isValidDaysMath = float.TryParse(daysMathStr, out daysMath);
                bool isValidDaysUser = float.TryParse(daysUserStr, out daysUser);

                EveningCounterType allowanceType = BLL.BaseBiz.PayrollDataContext.EveningCounterTypes.FirstOrDefault(x => x.EveningCounterTypeId
                    == requestInstance.EveningCounterTypeId);

                // do not validate for HPL
                if (CommonManager.CompanySetting.WhichCompany != WhichCompany.HPL && allowanceType.DoNowShowDaysWarning != true)
                {
                    if (isValidDaysUser && isValidDaysMath)
                    {
                        if (daysUser > daysMath)
                        {
                            NewMessage.ShowWarningMessage("Number of days between start and end date is Higher than Expected");
                            return;
                        }
                        else
                        {
                            requestInstance.Days = daysUser;
                        }
                    }
                    else
                    {
                        NewMessage.ShowWarningMessage("Invalid Days Input");
                        return;
                    }
                }
                else
                    requestInstance.Days = daysUser;

                if (requestInstance.StartDate > requestInstance.EndDate)
                {
                    NewMessage.ShowWarningMessage("From date should not be greater then To date.");
                    return;
                }

                Status status = new Status();
                status = AllowanceManager.InsertUpdateEveningRequest(requestInstance, true);

                if (status.IsSuccess)
                {
                    this.HasImport = true;
                    NewMessage.ShowNormalMessage("Allowance has been Assigned.", "closePopup();\n");                    
                }

                else
                {
                    divWarningMsg.InnerHtml = status.ErrorMessage;
                    divWarningMsg.Hide = false;
                }
            }
            else
            {
                JavascriptHelper.DisplayClientMsg("Validation Failed", this.Page);
            }
        }

        protected void Date_Change(object sender, DirectEventArgs e)
        {
            DateTime TodayDate = new DateTime();
            TodayDate = CommonManager.GetCurrentDateAndTime();


            string FromDateNep;
            string EndDateNep;
            
            if (txtFromDate.SelectedDate != null &&
                txtFromDate.SelectedDate.Date > TodayDate.AddYears(-25).Date && txtFromDate.SelectedDate.Date < TodayDate.AddYears(25).Date)
            {
                FromDateNep = DateManager.GetAppropriateDate(txtFromDate.SelectedDate);
                txtFromDateNep.Text = FromDateNep;
            }
            else
            {
                txtFromDateNep.Text = "";
                txtFromDate.SelectedDate = CommonManager.GetCurrentDateAndTime();
            }



            if (txtToDate.SelectedDate != null &&
                txtToDate.SelectedDate.Date > TodayDate.AddYears(-25).Date && txtToDate.SelectedDate.Date < TodayDate.AddYears(25).Date)
            {
                EndDateNep = DateManager.GetAppropriateDate(txtToDate.SelectedDate);
                txtToDateNep.Text = EndDateNep;

            }
            else
            {
                txtToDateNep.Text = "";
                txtToDate.SelectedDate = CommonManager.GetCurrentDateAndTime();
            }

            if (txtFromDate.SelectedDate != null && txtToDate.SelectedDate != null &&
                txtFromDate.SelectedDate.Date > TodayDate.AddYears(-25).Date && txtFromDate.SelectedDate.Date < TodayDate.AddYears(25).Date &&
                txtToDate.SelectedDate.Date > TodayDate.AddYears(-25).Date && txtToDate.SelectedDate.Date < TodayDate.AddYears(25).Date)
            {
                if (txtFromDate.SelectedDate.Date > txtToDate.SelectedDate.Date)
                {
                    txtToDate.SelectedDate = txtFromDate.SelectedDate.Date;
                }
                if (txtFromDate.SelectedDate.Date != null && txtToDate.SelectedDate.Date != null)
                {
                    string days = ((txtToDate.SelectedDate.Date - txtFromDate.SelectedDate.Date).TotalDays + 1).ToString();
                    txtDays.Text = Math.Round(decimal.Parse(days), MidpointRounding.ToEven).ToString();
                }
            }
        }

        
    }
}