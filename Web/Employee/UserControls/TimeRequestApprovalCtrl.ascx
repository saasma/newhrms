﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TimeRequestApprovalCtrl.ascx.cs" Inherits="Web.Employee.UserControls.TimeRequestApprovalCtrl" %>

<ext:Hidden ID="hdnTimeRequestCtrlId" runat="server" />
<ext:Window ID="WAttendanceReq" runat="server" Title="Time Request Details" Icon="Application" CloseAction="Hide"
    Width="1060" Height="400" BodyPadding="5" Hidden="false" Modal="false" X="-1000"
    Y="-1000">
    <Content>
        <br />
       
                    <ext:GridPanel StyleSpec="margin-top:15px; margin-bottom:15px;" ID="gridAttTimeDetl" runat="server" Cls="itemgrid"
                        Scroll="None" Width="1020">
                        <Store>
                            <ext:Store ID="Store2" runat="server">
                                <Model>
                                    <ext:Model ID="Model1" runat="server" IDProperty="RequestLineID">
                                        <Fields>
                                            <ext:ModelField Name="RequestLineID" Type="String" />
                                            <ext:ModelField Name="DateEng" Type="String" />
                                            <ext:ModelField Name="DayName" Type="string" />
                                            <ext:ModelField Name="Description" Type="string" />
                                            <ext:ModelField Name="InTimeString" Type="string" />
                                            <ext:ModelField Name="InNote" Type="string" />
                                            <ext:ModelField Name="OutTimeString" Type="string" />
                                            <ext:ModelField Name="OutNote" Type="string" />
                                            <ext:ModelField Name="WorkHoursString" Type="string" />
                                            <ext:ModelField Name="SubmittedOnString" Type="String" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:DateColumn ID="colDate" runat="server" Align="Right" Text="Date" Width="100"
                                    MenuDisabled="true" Sortable="false" Format="yyyy/MM/dd" DataIndex="DateEng">
                                </ext:DateColumn>
                                <ext:Column ID="colDayName" Sortable="false" MenuDisabled="true" runat="server" Text="Day"
                                    Width="100" Align="Left" DataIndex="DayName">
                                </ext:Column>
                                <ext:Column ID="colDescription" Sortable="false" MenuDisabled="true" runat="server"
                                    Text="Description" Width="150" Align="Left" DataIndex="Description">
                                </ext:Column>
                                <ext:Column ID="colInTime" Sortable="false" MenuDisabled="true" runat="server" Text="In Time"
                                    Align="Right" Width="80" DataIndex="InTimeString">
                                </ext:Column>
                                <ext:Column ID="colInNote" Sortable="false" MenuDisabled="true" runat="server" Text="In Note"
                                    Align="Left" Width="150" DataIndex="InNote">
                                </ext:Column>
                                <ext:Column ID="colOutTime" Sortable="false" MenuDisabled="true" runat="server" Text="Out Time"
                                    Align="Right" Width="80" DataIndex="OutTimeString">
                                </ext:Column>
                                <ext:Column ID="colOutNote" Sortable="false" MenuDisabled="true" runat="server" Text="Out Note"
                                    Align="Left" Width="150" DataIndex="OutNote">
                                </ext:Column>
                                <ext:Column ID="Column7" Sortable="false" MenuDisabled="true" runat="server" Text="Requested On"
                                    Align="Left" Width="120" DataIndex="SubmittedOnString">
                                </ext:Column>
                                <ext:Column ID="colWorkHours" Sortable="false" MenuDisabled="true" runat="server"
                                    Text="Work Hours" Align="Center" Width="80" DataIndex="WorkHoursString">
                                </ext:Column>
                            </Columns>
                        </ColumnModel>
                        <SelectionModel>
                            <ext:RowSelectionModel ID="RowSelectionModel2" runat="server" Mode="Single" />
                        </SelectionModel>
                    </ext:GridPanel>
                    <ext:ToolTip ID="ToolTip1"
                        runat="server"
                        Target="={#{gridAttTimeDetl}.getView().el}"
                        Delegate=".x-grid-cell"
                        TrackMouse="true">
                        <Listeners>
                            <Show Handler="onShow(this, #{gridAttTimeDetl});" />
                        </Listeners>
                    </ext:ToolTip>
              
            <table>
            <tr>
                <td style="width:135px;">
                    <ext:Button ID="btnViewApprove" StyleSpec="float:left;margin-left:20px" runat="server"
                        Cls="btn btn-success" Text="Approve" Width="120">
                        <DirectEvents>
                            <Click OnEvent="btnViewApprove_Click">
                                <EventMask ShowMask="true" />
                                <Confirmation ConfirmRequest="true" Message="Are you sure, you want to approve the time request?" />
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                </td>
                <td style="width:115px;">
                    <ext:Button ID="btnViewReject" StyleSpec="float:left;margin-left:20px;" runat="server"
                        Cls="btn btn-failure" Text="Reject" Width="100">
                        <DirectEvents>
                            <Click OnEvent="btnReject_Click">
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                </td>
            </tr>
        </table>
        
        <br />
    </Content>
</ext:Window>


<ext:Window runat="server" Hidden="false" X="-1000" Y="-1000" Title="Reject Time Attendance"
    ID="WReject" Modal="false" Width="380" Height="210">
    <Content>
        <table class="fieldTable" style="margin-left: 20px;">
            <tr>
                <td>
                    <ext:TextArea ID="txtRejectComment" runat="server" FieldLabel="Comment *" LabelSeparator=""
                        LabelAlign="Top" Rows="3" Width="300" Height="80" />
                    <asp:RequiredFieldValidator runat="server" ID="rfvRejectComment" ValidationGroup="TimeAttComment"
                        ControlToValidate="txtRejectComment" Display="None" ErrorMessage="Reject Comment is required."></asp:RequiredFieldValidator>
                </td>
            </tr>
        </table>
        <br />
        <div style="margin-left: 20px;">
            <ext:Button runat="server" ID="btnRejectWithComment" Cls="btn btn-primary" Text="<i></i>Reject">
                <DirectEvents>
                    <Click OnEvent="btnRejectWithComment_Click">
                        <EventMask ShowMask="true" />
                    </Click>
                </DirectEvents>
                <Listeners>
                    <Click Handler="valGroup = 'TimeAttComment'; if(CheckValidation()) return ''; else return false;">
                    </Click>
                </Listeners>
            </ext:Button>
        </div>
    </Content>
</ext:Window>