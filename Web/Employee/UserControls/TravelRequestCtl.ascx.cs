﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using System.Xml;
using Utils;
using System.IO;
using BLL.BO;
namespace Web.Employee.UserControls
{
    public partial class TravelRequestCtl : BaseUserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();
            }
        }

        public void Initialise()
        {

            btnExtendShow.Visible= SessionManager.CurrentLoggedInEmployeeId == 0;

            cmbLocations.Store[0].DataSource = TravelAllowanceManager.GetLocationList().OrderBy(x => x.LocationName).ToList();
            cmbLocations.Store[0].DataBind();

            CommonManager comManager = new CommonManager();

            cmbCountry.Store[0].DataSource = comManager.GetAllCountries();
            cmbCountry.Store[0].DataBind();

            cmbTravelBy.Store[0].DataSource = TravelAllowanceManager.getTravelByList();
            cmbTravelBy.Store[0].DataBind();

            cmbExpensePaidBy.Store[0].DataSource = TravelAllowanceManager.getExpensePaidByList();
            cmbExpensePaidBy.Store[0].DataBind();

            if (CommonManager.SettingTA.IsLocationEditable != null && CommonManager.SettingTA.IsLocationEditable.Value)
            {
                rowDomestic.Style["display"] = "";
                rowEditableLocation.Style["display"] = "";
                rowRefNumber.Style["display"] = "";

                ColumnQuantity.Hide();
                ColumnTotal.Hide();
                ColumnRate.Text = "Amount";
                txtRequestedAdvanceAmount.Hide();
            }

        }

        protected void btnExtend_Click(object sender, DirectEventArgs e)
        {
            int requestId = int.Parse(hiddenValue.Text);
            TARequest request = TravelAllowanceManager.getTARequestByID(requestId);

            DateTime extendDate = dateExtension.SelectedDate;
            if (extendDate <= request.TravellingToEng.Value)
            {
                NewMessage.ShowWarningMessage("Extension date should be after the Travelling to or Arrival date.");
                return;
            }

            TravelAllowanceManager.SetExtensionDate(requestId, extendDate, txtExtensionNotes.Text.Trim());

            windowExtend.Hide();

            extensionDateValue.SelectedDate = dateExtension.SelectedDate;
            extensionReasonValue.Text = txtExtensionNotes.Text;

            extensionDateValue.Show();
            extensionReasonValue.Show();

        }
       
        protected void btnAdvance_Click(object sender, DirectEventArgs e)
        {

            TARequest request = new TARequest();
            TARequestStatusHistory hist = new TARequestStatusHistory();
            bool isEdit;


            isEdit = string.IsNullOrEmpty(hiddenValue.Text);
            if (!isEdit)
            {
                request.RequestID = int.Parse(hiddenValue.Text);
            }

            decimal AdvanceAmount = 0;
            if (!string.IsNullOrEmpty(txtAdvanceAmount.Text))
            {
                AdvanceAmount = decimal.Parse(txtAdvanceAmount.Text);
            }

            Status status = TravelAllowanceManager.UpdateAdvanceTravelRequests(request, AdvanceAmount);
            if (status.IsSuccess)
            {

                NewMessage.ShowNormalMessage("Allowance Advance Added");
                Window1.Close();
                X.Js.AddScript("if(typeof(searchList) != 'undefined') searchList();");
                //Response.Redirect("TravelRequestHRView.aspx");
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }


        }

        public void LoadTravelRequest(TARequest request)
        {
            hiddenValue.Text = request.RequestID.ToString();
            btnPrint.Show();
            LoadEmployeeInfo(request.EmployeeId.Value);
            if (request.LocationId != null)
                LoadAllowanceGrid(request.LocationId.Value, request.RequestID);
            else
            {
                LoadAllowanceGrid(0, request.RequestID);
                cmbLocations.Hide();
                txtPlaceToTravel.Hide();
                cmbCountry.Hide();
               
            }
            LoadEditData(request);
            Window1.Center();
            Window1.Show();
        }

        public void SetForAdvance()
        {
            btnAdvance.Show();
            txtHRComment.Hide();
            X.Js.AddScript("if(document.getElementById('rowAdvance') != null){document.getElementById('rowAdvance').style.display='';}");
        }

        public void HideButtonBlock()
        {
            X.Js.AddScript("if(document.getElementById('divButtons') != null){document.getElementById('divButtons').style.display='none';}");

        }
        protected void ButtonNext_Click(object sender, DirectEventArgs e)
        {
            

            TARequest request = new TARequest();
            TARequestStatusHistory hist = new TARequestStatusHistory();
            bool isEdit;
            bool isStatusIncrease = true;

            isEdit = string.IsNullOrEmpty(hiddenValue.Text);
            if (!isEdit)
            {
                request.RequestID = int.Parse(hiddenValue.Text);
                request.Status = TravelAllowanceManager.getRequestByID(int.Parse(hiddenValue.Text)).Status;
            }

            if (!string.IsNullOrEmpty(txtDayCount.Text))
            {
                request.Days = int.Parse(txtDayCount.Text);
            }
            if (!string.IsNullOrEmpty(txtNightCount.Text))
            {
                request.Night = int.Parse(txtNightCount.Text);
            }

            hist.ApprovalRemarks = txtHRComment.Text;
            request.PlaceOfTravel = txtPlaceToTravel.Text;

            if (cmbCountry.SelectedItem != null && cmbCountry.SelectedItem.Value != null)
                request.CountryId = int.Parse(cmbCountry.SelectedItem.Value);
            if (cmbLocations.SelectedItem != null && cmbLocations.SelectedItem.Value != null)
                request.LocationId = int.Parse(cmbLocations.SelectedItem.Value);
            request.CountryName = cmbCountry.SelectedItem.Text;
            request.PurposeOfTravel = txtPurposeOfTravel.Text;
            request.TravellingFromEng = DateTime.Parse(txtFromDate.Text);
            request.TravellingToEng = DateTime.Parse(txtToDate.Text);
            request.TravelBy = int.Parse(cmbTravelBy.SelectedItem.Value);
            request.TravelByText = cmbTravelBy.SelectedItem.Text;
            request.ExpensePaidBy = int.Parse(cmbExpensePaidBy.SelectedItem.Value);
            request.ExpensePaidByText = cmbExpensePaidBy.SelectedItem.Text;

            if (!string.IsNullOrEmpty(txtRequestedAdvanceAmount.Text.Trim()))
                request.RequestedAdvance = decimal.Parse(txtRequestedAdvanceAmount.Text.Trim());

            if (request.TravelByText.Equals("Other") && string.IsNullOrEmpty(txtOtherName.Text.Trim()))
            {
                NewMessage.ShowWarningMessage("Other travel by name is required.");
                return;
            }
            request.OtherTravelByName = txtOtherName.Text.Trim();

            if (radioDomestic.Checked)
                request.IsDomestic = true;
            else
                request.IsDomestic = false;

            //request.EmployeeId = SessionManager.CurrentLoggedInEmployeeId;

            DAL.TARequest dbRequest = TravelAllowanceManager.getRequestByID(request.RequestID);
            GetDocumentNextStepUsingApprovalFlowResult nextStep =
                CommonManager.GetDocumentNextStep(dbRequest.Status.Value, FlowTypeEnum.TravelOrder, dbRequest.EmployeeId.Value, dbRequest.RequestID, false);

            if (nextStep != null)
            {
                request.Status = nextStep.NextStepID;
                request.StatusName = nextStep.StepStatusName;


                if (nextStep.NextStepID != (int)FlowStepEnum.Step15End)
                {
                    if (cmbForwardList.SelectedItem == null || cmbForwardList.SelectedItem.Value == null)
                    {
                        NewMessage.ShowWarningMessage("Forward To selection is required.", "#{cmbForwardList}.focus();");

                        return;
                    }
                    request.CurrentApprovalSelectedEmployeeId = int.Parse(cmbForwardList.SelectedItem.Value);
                }
            }
            

            List<TARequestLine> lines = new List<TARequestLine>();
            string json = e.ExtraParams["AllowanceValues"];
            if (string.IsNullOrEmpty(json))
            {
                return;
            }


            lines = JSON.Deserialize<List<TARequestLine>>(json);
            foreach (TARequestLine line in lines)
            {
                line.Total = Convert.ToDecimal(line.Quantity) * Convert.ToDecimal(line.Rate);
            }

            request.Total = lines.Sum(x => x.Total).Value;

            //if (!string.IsNullOrEmpty(dispTotal.Text))
            request.ApprovedTotal = lines.Sum(x => Convert.ToDecimal(x.Quantity) * Convert.ToDecimal(x.Rate));

            Status status = TravelAllowanceManager.InsertUpdateTravelRequests(request, lines, hist, isStatusIncrease);
            if (status.IsSuccess)
            {

                NewMessage.ShowNormalMessage("Allowance Forwarded");
                Window1.Close();
                X.Js.AddScript("if(typeof(searchList) != 'undefined') searchList();");
                //LoadLevels();
                //Response.Redirect("TravelRequestHRView.aspx");
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }


        }

        protected void LoadAllowanceGrid(int LocationID, int? RequestID)
        {
            List<TARequestLine> lines = new List<TARequestLine>();
            lines = TravelAllowanceManager.getAllowanceByEmployeeID(SessionManager.CurrentLoggedInEmployeeId, RequestID, LocationID).Where(x => x.UnitType == null).ToList();
            storeAllowances.DataSource = lines;
            storeAllowances.DataBind();

        }


        private void LoadEditData(TARequest loadRequest)
        {

            GetDocumentNextStepUsingApprovalFlowResult nextStep =
                CommonManager.GetDocumentNextStep(loadRequest.Status.Value, FlowTypeEnum.TravelOrder, loadRequest.EmployeeId.Value, loadRequest.RequestID, false);


            if (loadRequest.Status == (int)FlowStepEnum.Step15End || (nextStep != null && nextStep.NextStepID == (int)FlowStepEnum.Step15End))
            {
                btnSaveAndLater.Text = "&nbsp;Approve&nbsp;";
                cmbForwardList.Hide();
                //btnSaveAndLater.DirectEvents.Click.Confirmation.Message = "Are you sure, you want to approve the Travel Request?";

            }
            else
            {
                cmbForwardList.Show();
                btnSaveAndLater.Text = "&nbsp;Forward&nbsp;";
                //btnSaveAndLater.DirectEvents.Click.Confirmation.Message = "Are you sure, you want to forward the Travel Request?";
            }

            if (loadRequest.Advance != null)
                txtAdvanceAmount.Text = GetCurrency(loadRequest.Advance.ToString());
            txtPlaceToTravel.Text = loadRequest.PlaceOfTravel;
            //cmbCountry.SelectedItem.Value = loadRequest.CountryId.Value.ToString();
            //ExtControlHelper.ComboBoxSetSelected(loadRequest.CountryId.Value.ToString(), cmbCountry);
            cmbCountry.SetValue(loadRequest.CountryId.ToString());

            txtPurposeOfTravel.Text = loadRequest.PurposeOfTravel;
            txtFromDate.Text = loadRequest.TravellingFromEng.Value.ToShortDateString();
            txtToDate.Text = loadRequest.TravellingToEng.Value.ToShortDateString();

            if (loadRequest.LocationId != null)
            {
                cmbLocations.SetValue(loadRequest.LocationId.Value.ToString());
                LoadAllowanceGrid(loadRequest.LocationId.Value);
            }

            gridLocations.Store[0].DataSource = loadRequest.TARequestLocations.OrderBy(x => x.LineID).ToList();
            gridLocations.Store[0].DataBind();

            if (loadRequest.ExtendArrivalDate != null)
            {
                extensionDateValue.SelectedDate = loadRequest.ExtendArrivalDate.Value;
                extensionReasonValue.Text = loadRequest.ExtendReason;

                extensionDateValue.Show();
                extensionReasonValue.Show();
            }
            else
            {
                extensionDateValue.Hide();
                extensionReasonValue.Hide();
            }

            txtRefNumber.Text = loadRequest.Number;


            if (loadRequest.IsDomestic != null)
            {
                if (loadRequest.IsDomestic == false)
                {
                    radioDomestic.Checked = false;
                    radioInternaltional.Checked = true;
                }
            }

            //cmbTravelBy.SelectedItem.Value = loadRequest.TravelBy.Value.ToString();
            //ExtControlHelper.ComboBoxSetSelected(loadRequest.TravelBy.Value.ToString(), cmbTravelBy);
            cmbTravelBy.SetValue(loadRequest.TravelBy.Value.ToString());
            //cmbExpensePaidBy.SelectedItem.Value = loadRequest.ExpensePaidBy.Value.ToString();
            //ExtControlHelper.ComboBoxSetSelected(loadRequest.ExpensePaidBy.Value.ToString(), cmbExpensePaidBy);
            cmbExpensePaidBy.SetValue(loadRequest.ExpensePaidBy.Value.ToString());

            if (loadRequest.TravelByText.Equals("Other"))
            {
                txtOtherName.Text = loadRequest.OtherTravelByName == null ? "" : loadRequest.OtherTravelByName;
                txtOtherName.Show();
            }

            txtDayCount.Text = (Math.Abs((loadRequest.TravellingFromEng.Value - loadRequest.TravellingToEng.Value).TotalDays) + 1).ToString();
            txtNightCount.Text = (Math.Abs((loadRequest.TravellingFromEng.Value - loadRequest.TravellingToEng.Value).TotalDays)).ToString();

            dispTotal.Text = GetCurrency(loadRequest.Total);

            if (loadRequest.RequestedAdvance != null)
                txtRequestedAdvanceAmount.Text = GetCurrency(loadRequest.RequestedAdvance);

            if (CommonManager.CanChangeDocuemnt(FlowTypeEnum.TravelOrder, loadRequest.Status.Value, loadRequest.EmployeeId.Value,
                SessionManager.CurrentLoggedInEmployeeId,loadRequest.RequestID))
            {

                List<TextValue> approvalList = TravelAllowanceManager.GetForwardToListForTravelOrder(loadRequest.Status.Value,
                     loadRequest.EmployeeId.Value);
               
                cmbForwardList.Store[0].DataSource = approvalList;
                cmbForwardList.Store[0].DataBind();

                // hard code approval for Prabhu
                if (CommonManager.CompanySetting.WhichCompany == WhichCompany.Prabhu)
                {
                    //Approved By……………….Tara Manandhar (801) OR Ashok Sherchan(785)  …Default Tara Manandhar
                    if (approvalList.Any(x => x.ID == 801))
                        cmbForwardList.SetValue("801".ToString());
                }

                if (approvalList.Count == 1)
                {
                    cmbForwardList.SetValue(approvalList[0].ID.ToString());
                }
                //else if (approvalList.Count <= 0)
                //    cmbForwardList.Hide();

                btnSaveAndLater.Show();
                X.Js.AddScript("if(document.getElementById('cancelBlock') != null){document.getElementById('cancelBlock').style.display='';}");
            }
            else
            {
                X.Js.AddScript("if(document.getElementById('cancelBlock') != null){document.getElementById('cancelBlock').style.display='none';}");
                btnSaveAndLater.Hide();
            }

        }

        protected void LoadEmployeeInfo(int EmployeeID)
        {
            EEmployee eemployee = EmployeeManager.GetEmployeeById(EmployeeID);
            lblName.Text = eemployee.Name;
            lblDepartment.Text = eemployee.Department.Name;
            lblDesignation.Text = eemployee.EDesignation.Name;
            lblBranch.Text = eemployee.Branch.Name;

            bool hasPhoto = false;
            if (eemployee.HHumanResources.Count > 0)
            {
                if (!string.IsNullOrEmpty(eemployee.HHumanResources[0].UrlPhotoThumbnail))
                {
                    image.ImageUrl = Page.ResolveUrl(Config.UploadLocation + "/" + eemployee.HHumanResources[0].UrlPhotoThumbnail);
                    if (File.Exists(Server.MapPath(image.ImageUrl)))
                        hasPhoto = true;
                }
            }
            if (!hasPhoto)
            {
                image.ImageUrl = Page.ResolveUrl(Web.Helper.WebHelper.GetEmployeeNoImage(eemployee.EmployeeId));

            }

            List<TARequestStatusHistory> statusHistory = TravelAllowanceManager.getTravelRequestForwardHistory(hiddenValue.Text);

            string body = "";
            foreach (var status in statusHistory)
            {
                body +=
                    string.Format("<div class='commentBlock'>{0}</div>",
                    (status.ApprovalEmployeeId != null ? EmployeeManager.GetEmployeeName(status.ApprovalEmployeeId.Value) : status.ApprovalDisplayTitle) + " Comment")
                    +
                    string.Format("<div class='nameBlock'>{0}</div>", status.ApprovalRemarks + " - " + status.ApprovedOn.Value.ToShortDateString());
                //body = body + "<tr><td  colspan='2' style='font-weight:bold'>" + status.whoseComment + "</td></tr><tr><td colspan='2' style='color:blue'>" + status.ApprovalRemarks + "</td></tr><tr><td>" + status.ApprovedByName + "</td><td>" + status.ApprovedOn.Value.ToShortDateString() + "</td></tr>";
            }
            //body = "<table>" + body + "</table>";

            lblCommentSection.Html = body;

        }


        protected void Country_Change(object sender, DirectEventArgs e)
        {
            //LoadAllowanceGrid(int.Parse(cmbCountry.SelectedItem.Value));
        }


        protected void CheckInOutSelectChange(object sender, DirectEventArgs e)
        {
            string actualInDate = "";
            string actualOutDate = "";
            if (txtFromDate.Text != "0001-01-01 12:00:00 AM")
                actualInDate = txtFromDate.Text;
            if (txtToDate.Text != "0001-01-01 12:00:00 AM")
                actualOutDate = txtToDate.Text;
            DateTime englishInDate;
            DateTime englishOutDate;
            if (!string.IsNullOrEmpty(actualInDate) && !string.IsNullOrEmpty(actualOutDate))
            {
                englishInDate = DateTime.Parse(actualInDate);
                englishOutDate = DateTime.Parse(actualOutDate);

                txtDayCount.Text = (Math.Abs((englishInDate - englishOutDate).TotalDays) + 1).ToString();
                txtNightCount.Text = (Math.Abs((englishInDate - englishOutDate).TotalDays)).ToString();

                if ((englishInDate - englishOutDate).TotalDays == 0)
                {
                    txtDayCount.Text = "1";
                    txtNightCount.Text = "1";
                }

            }
            else
            {
                txtDayCount.Text = "0";
                txtNightCount.Text = "0";
            }
        }


        protected void LoadAllowanceGrid(int LocationID)
        {
            List<TARequestLine> lines = new List<TARequestLine>();
            int? RequestID = null;
            if (!string.IsNullOrEmpty(hiddenValue.Text))
            {
                RequestID = int.Parse(hiddenValue.Text);
            }
            lines = TravelAllowanceManager.getAllowanceByEmployeeID(SessionManager.CurrentLoggedInEmployeeId, RequestID, LocationID);
            storeAllowances.DataSource = lines;
            storeAllowances.DataBind();

        }

        protected void cmbLocations_Change(object sender, DirectEventArgs e)
        {

            List<TARequestLine> lines = new List<TARequestLine>();
            string json = e.ExtraParams["Values"];
            if (!string.IsNullOrEmpty(json))
            {

                lines = JSON.Deserialize<List<TARequestLine>>(json);

                foreach (TARequestLine line in lines)
                {

                    TAAllowanceRate rate = TravelAllowanceManager.GetRate(SessionManager.CurrentLoggedInEmployeeId,
                        int.Parse(cmbLocations.SelectedItem.Value),
                         line.AllowanceId.Value);

                    if (rate != null && rate.Rate != null)
                        line.Rate = rate.Rate;
                    else
                        line.Rate = 0;

                    //line.Total = Convert.ToDecimal(line.Quantity) * Convert.ToDecimal(line.Rate);
                }

            }


            storeAllowances.DataSource = lines;
            storeAllowances.DataBind();
            //LoadAllowanceGrid(int.Parse(cmbLocations.SelectedItem.Value));
        }

        protected void btnPrint_Click(object sender, EventArgs e)
        {
            CP.Report.ReportHelper.TravelRequestPrint(int.Parse(hiddenValue.Text), txtFromDate.SelectedDate.ToShortDateString(), txtToDate.SelectedDate.ToShortDateString());
        }   

    }
}