﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using System.Xml;
using Utils;
using System.IO;
using BLL.BO;

namespace Web.Employee.UserControls
{
    public partial class TravelAssignCtl : BaseUserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();
            }
        }

        public void Initialise()
        {

            storeCountry.DataSource = CommonManager.GetCountryNames();
            storeCountry.DataBind();

            //cmbLocationsAssign.Store[0].DataSource = TravelAllowanceManager.GetLocationList().OrderBy(x => x.LocationName).ToList();
            //cmbLocationsAssign.Store[0].DataBind();

            //CommonManager comManager = new CommonManager();

            //cmbCountryAssign.Store[0].DataSource = comManager.GetAllCountries();
            //cmbCountryAssign.Store[0].DataBind();

            //cmbTravelByAssign.Store[0].DataSource = TravelAllowanceManager.getTravelByList();
            //cmbTravelByAssign.Store[0].DataBind();

            //cmbExpensePaidByAssign.Store[0].DataSource = TravelAllowanceManager.getExpensePaidByList();
            //cmbExpensePaidByAssign.Store[0].DataBind();

            if (CommonManager.SettingTA.IsLocationEditable != null && CommonManager.SettingTA.IsLocationEditable.Value)
            {
                cmbLocationsAssign.Hide();
                txtPlaceToTravelAssign.Hide();
                cmbCountryAssign.Hide();

                rowDomestic.Style["display"] = "";
                rowEditableLocation.Style["display"] = "";

                reqdCountry1.Enabled = false;
                reqdLocations1.Enabled = false;
                reqdPlaceToTravel1.Enabled = false;
            }
            else
            {
               
            }


            CommonManager comManager = new CommonManager();
            cmbCountryAssign.Store[0].DataSource = comManager.GetAllCountries();
            cmbCountryAssign.Store[0].DataBind();

            cmbTravelByAssign.Store[0].DataSource = TravelAllowanceManager.getTravelByList();
            cmbTravelByAssign.Store[0].DataBind();

            cmbExpensePaidByAssign.Store[0].DataSource = TravelAllowanceManager.getExpensePaidByList();
            cmbExpensePaidByAssign.Store[0].DataBind();

            cmbLocationsAssign.Store[0].DataSource = TravelAllowanceManager.GetLocationList().OrderBy(x => x.LocationName).ToList();
            cmbLocationsAssign.Store[0].DataBind();

            storeEmployeeListAssign.DataSource = EmployeeManager.GetAllEmployeesForEmployeeImportExport().OrderBy(x => x.Name).ToList();
            storeEmployeeListAssign.DataBind();

            txtFromDateAssign.SelectedDate = DateTime.Now;
            txtToDateAssign.SelectedDate = DateTime.Now;

            txtDayCountAssign.Text = "2";
            txtNightCountAssign.Text = "1";


            cmbEmployeeAssign.SelectedItem.Index = 0;
            cmbCountryAssign.SelectedItem.Value = "524";
            cmbLocationsAssign.SelectedItem.Index = 0;


        }


        public void Assign_Click()
        {
            int locationId = 0;

            hiddenValue.Text = "";

            if (CommonManager.SettingTA.IsLocationEditable != null && CommonManager.SettingTA.IsLocationEditable.Value)
            {

            }
            else if (cmbLocationsAssign.SelectedItem == null || cmbLocationsAssign.SelectedItem.Value == null)
            {
                NewMessage.ShowWarningMessage("Location not defined.");
                return;
            }
            else
                locationId = int.Parse(cmbLocationsAssign.SelectedItem.Value);

            LoadAllowanceGridAssign(locationId);

            if (cmbEmployeeAssign.SelectedItem != null &&
                    cmbEmployeeAssign.SelectedItem.Value != null &&
                    cmbEmployeeAssign.SelectedItem.Index != -1 &&
                   !string.IsNullOrEmpty(cmbEmployeeAssign.SelectedItem.Value))
            {
                LoadEmployeeInfo(int.Parse(cmbEmployeeAssign.SelectedItem.Value));
            }


            Window1Assign.Show();
        }



        public void LoadAllowanceGrid(int LocationID, int? RequestID)
        {
            List<TARequestLine> lines = new List<TARequestLine>();
            lines = TravelAllowanceManager.getAllowanceByEmployeeID(SessionManager.CurrentLoggedInEmployeeId, RequestID, LocationID).Where(x => x.UnitType == null).ToList();
            storeAllowancesAssign.DataSource = lines;
            storeAllowancesAssign.DataBind();

        }


        
        


        protected void Country_Change(object sender, DirectEventArgs e)
        {
            //LoadAllowanceGrid(int.Parse(cmbCountry.SelectedItem.Value));
        }


        protected void LoadAllowanceGridAssign(int locationId)
        {

            if (locationId == 0)
            {
                LoadAllowanceGrid(0);
            }
            else if (cmbLocationsAssign.SelectedItem != null && cmbEmployeeAssign.SelectedItem != null &&
                 cmbEmployeeAssign.SelectedItem.Value != null && cmbLocationsAssign.SelectedItem.Value != null &&
                cmbLocationsAssign.SelectedItem.Index != -1 && cmbEmployeeAssign.SelectedItem.Index != -1 &&
                !string.IsNullOrEmpty(cmbEmployeeAssign.SelectedItem.Value) && !string.IsNullOrEmpty(cmbLocationsAssign.SelectedItem.Value))
            {
                List<TARequestLine> lines = new List<TARequestLine>();
                int? RequestID = null;

                lines = TravelAllowanceManager.getAllowanceByEmployeeID(int.Parse(cmbEmployeeAssign.SelectedItem.Value), RequestID, locationId).Where(x => x.UnitType == null).ToList();

                storeAllowancesAssign.DataSource = lines;
                storeAllowancesAssign.DataBind();
            }

        }

        public void LoadAllowanceGrid(int LocationID)
        {
            List<TARequestLine> lines = new List<TARequestLine>();
            int? RequestID = null;
            if (!string.IsNullOrEmpty(hiddenValue.Text))
            {
                RequestID = int.Parse(hiddenValue.Text);
            }
            lines = TravelAllowanceManager.getAllowanceByEmployeeID(SessionManager.CurrentLoggedInEmployeeId, RequestID, LocationID);
            storeAllowancesAssign.DataSource = lines;
            storeAllowancesAssign.DataBind();

        }

        



        protected void CheckInOutAssignSelectChange(object sender, DirectEventArgs e)
        {
            string actualInDate = "";
            string actualOutDate = "";
            if (txtFromDateAssign.Text != "0001-01-01 12:00:00 AM")
                actualInDate = txtFromDateAssign.Text;
            if (txtToDateAssign.Text != "0001-01-01 12:00:00 AM")
                actualOutDate = txtToDateAssign.Text;
            DateTime englishInDate;
            DateTime englishOutDate;
            if (!string.IsNullOrEmpty(actualInDate) && !string.IsNullOrEmpty(actualOutDate))
            {
                englishInDate = DateTime.Parse(actualInDate);
                englishOutDate = DateTime.Parse(actualOutDate);

                txtDayCountAssign.Text = (Math.Abs((englishInDate - englishOutDate).TotalDays) + 1).ToString();
                txtNightCountAssign.Text = (Math.Abs((englishInDate - englishOutDate).TotalDays)).ToString();

                if ((englishInDate - englishOutDate).TotalDays == 0)
                {
                    txtDayCountAssign.Text = "1";
                    txtNightCountAssign.Text = "1";
                }

            }
            else
            {
                txtDayCountAssign.Text = "0";
                txtNightCountAssign.Text = "0";
            }
        }




        protected void cmbLocationsAssign_Change(object sender, DirectEventArgs e)
        {


            Ext.Net.ComboBox combo = (Ext.Net.ComboBox)sender;
            if (combo != null && combo.ID == "cmbEmployeeAssign")
            {
                if (cmbEmployeeAssign.SelectedItem != null &&
                     cmbEmployeeAssign.SelectedItem.Value != null &&
                     cmbEmployeeAssign.SelectedItem.Index != -1 &&
                    !string.IsNullOrEmpty(cmbEmployeeAssign.SelectedItem.Value))
                {
                    LoadEmployeeInfo(int.Parse(cmbEmployeeAssign.SelectedItem.Value));
                }
            }

            if (cmbLocationsAssign.SelectedItem != null && cmbEmployeeAssign.SelectedItem != null &&
                 cmbEmployeeAssign.SelectedItem.Value != null && cmbLocationsAssign.SelectedItem.Value != null &&
                cmbLocationsAssign.SelectedItem.Index != -1 && cmbEmployeeAssign.SelectedItem.Index != -1 &&
                !string.IsNullOrEmpty(cmbEmployeeAssign.SelectedItem.Value) && !string.IsNullOrEmpty(cmbLocationsAssign.SelectedItem.Value))
            {
                string isRateEditable = "false";
                string LocationID = cmbLocationsAssign.SelectedItem.Value;
                //isRateEditable = AllowanceManager.GetLocationByID(LocationID);


                List<TARequestLine> lines = new List<TARequestLine>();
                string json = e.ExtraParams["ValuesAssign"];
                if (!string.IsNullOrEmpty(json))
                {

                    lines = JSON.Deserialize<List<TARequestLine>>(json);

                    foreach (TARequestLine line in lines)
                    {

                        TAAllowanceRate rate = TravelAllowanceManager.GetRate(int.Parse(cmbEmployeeAssign.SelectedItem.Value),
                            int.Parse(cmbLocationsAssign.SelectedItem.Value),
                             line.AllowanceId.Value);

                        if (rate != null && rate.Rate != null)
                            line.Rate = rate.Rate;
                        else
                            line.Rate = 0;

                        //line.Total = Convert.ToDecimal(line.Quantity) * Convert.ToDecimal(line.Rate);
                    }

                }


                storeAllowancesAssign.DataSource = lines;
                storeAllowancesAssign.DataBind();
            }
            //LoadAllowanceGrid(int.Parse(cmbLocations.SelectedItem.Value));
        }


        protected void ButtonNextAssign_Click(object sender, DirectEventArgs e)
        {

            Page.Validate("InsertUpdateAssign");
            if (Page.IsValid)
            {
                Ext.Net.Button btn = (Ext.Net.Button)sender;
                TARequest request = new TARequest();
                TARequestStatusHistory hist = new TARequestStatusHistory();

                bool isEdit = false;

                request.Status = (int)FlowStepEnum.Step15End;
                hist.Status = (int)FlowStepEnum.Step15End;


                if (!string.IsNullOrEmpty(txtDayCountAssign.Text))
                {
                    request.Days = int.Parse(txtDayCountAssign.Text);
                }
                if (!string.IsNullOrEmpty(txtNightCountAssign.Text))
                {
                    request.Night = int.Parse(txtNightCountAssign.Text);
                }

                request.PlaceOfTravel = txtPlaceToTravelAssign.Text;

                // no location for sana kisan like Company case
                if (CommonManager.SettingTA.IsLocationEditable != null && CommonManager.SettingTA.IsLocationEditable.Value)
                {
                    request.IsDomestic = (radioDomestic.Checked ? true : false);

                    // sometime both are un-selected then set as domestic
                    if (radioDomestic.Checked == false && radioInternaltional.Checked == false)
                        request.IsDomestic = true;

                    string json1 = e.ExtraParams["Locations"];
                    if (string.IsNullOrEmpty(json1))
                    {
                        NewMessage.ShowWarningMessage("Place need to be added.");
                        return;
                    }

                    List<TARequestLocation> list = JSON.Deserialize<List<TARequestLocation>>(json1);
                    List<TARequestLocation> newList = new List<TARequestLocation>();

                    foreach (var item in list)
                    {
                        if (string.IsNullOrEmpty(item.LocationPlace) && string.IsNullOrEmpty(item.Country))
                            continue;
                        if (!string.IsNullOrEmpty(item.LocationPlace) && string.IsNullOrEmpty(item.Country))
                        {
                            NewMessage.ShowWarningMessage("Country is required.");
                            return;
                        }
                        if (string.IsNullOrEmpty(item.LocationPlace) && !string.IsNullOrEmpty(item.Country))
                        {
                            NewMessage.ShowWarningMessage("Location is required.");
                            return;
                        }
                        if (radioDomestic.Checked && !item.Country.ToLower().Equals("Nepal".ToLower()))
                        {
                            NewMessage.ShowWarningMessage("For domestic, all country must be Nepal.");
                            return;
                        }

                        if (string.IsNullOrEmpty(request.LocationName))
                            request.LocationName = item.LocationPlace;
                        else
                            request.LocationName += ", " + item.LocationPlace;

                        newList.Add(item);
                    }

                    request.TARequestLocations.AddRange(newList);
                    if (request.TARequestLocations.Count == 0)
                    {
                        NewMessage.ShowWarningMessage("Location is empty.");
                        return;
                    }
                }
                else
                {
                    request.CountryId = int.Parse(cmbCountryAssign.SelectedItem.Value);
                    request.CountryName = cmbCountryAssign.SelectedItem.Text;
                    request.LocationId = int.Parse(cmbLocationsAssign.SelectedItem.Value);
                }


                request.PurposeOfTravel = txtPurposeOfTravelAssign.Text;
                request.TravellingFromEng = DateTime.Parse(txtFromDateAssign.Text);
                request.TravellingToEng = DateTime.Parse(txtToDateAssign.Text);
                request.TravelBy = int.Parse(cmbTravelByAssign.SelectedItem.Value);
                request.TravelByText = cmbTravelByAssign.SelectedItem.Text;

                if (request.TravelByText.Equals("Other") && string.IsNullOrEmpty(txtOtherName.Text.Trim()))
                {
                    NewMessage.ShowWarningMessage("Other travel by name is required.");
                    return;
                }
                request.OtherTravelByName = txtOtherName.Text.Trim();

                request.ExpensePaidBy = int.Parse(cmbExpensePaidByAssign.SelectedItem.Value);
                request.ExpensePaidByText = cmbExpensePaidByAssign.SelectedItem.Text;
                request.EmployeeId = int.Parse(cmbEmployeeAssign.SelectedItem.Value);

                if (request.TravellingFromEng != null)
                    request.SettTravellingFromEng = request.TravellingFromEng;

                if (request.TravellingToEng != null)
                    request.SettTravellingToEng = request.TravellingToEng;

                if (request.Days != null)
                    request.SettDays = request.Days;

                if (request.Night != null)
                    request.SettNight = request.Night;

                if (request.TravelBy != null)
                    request.SettTravelBy = request.TravelBy;

                if (request.ExpensePaidBy != null)
                    request.SettExpensePaidBy = request.ExpensePaidBy;

                request.SettExpensePaidByText = request.ExpensePaidByText;
                request.TravelByText = request.TravelByText;


                List<TARequestLine> lines = new List<TARequestLine>();
                string json = e.ExtraParams["AllowanceValuesAssign"];
                if (string.IsNullOrEmpty(json))
                {
                    return;
                }

                lines = JSON.Deserialize<List<TARequestLine>>(json);

                foreach (TARequestLine line in lines)
                {
                    line.Total = Convert.ToDecimal(line.Quantity) * Convert.ToDecimal(line.Rate);
                    if (line.Quantity != null)
                    {
                        line.SettQuantity = line.Quantity;
                    }
                    if (line.Total != null)
                    {
                        line.FinalTotal = line.Total;
                        line.SettTotal = line.Total;
                    }

                    line.FinalAllowType = 2; //1=Actual, 2= Approved,3=edited
                    
                    
                }

                request.Total = lines.Sum(x => x.Total).Value;
                if (request.Total != null)
                {
                    request.ClaimedTotal = request.Total;
                    request.ApprovedTotal = request.Total;
                    request.SettledTotal = request.Total;
                }
                request.AdvanceStatus = (int)AdvanceStatusEnum.HRSettle;
                request.CurrentApprovalSelectedEmployeeId = int.Parse(cmbForwardList.SelectedItem.Value);
                hist.ApprovalRemarks = "Assigned Travel";
                hist.ApprovalDisplayTitle = "Assigned By Admin";
                hist.ApprovedByName = "Assigned By Admin";
                

                //hist.ApprovalDisplayTitle

                Status status = TravelAllowanceManager.ApproveTravelRequests(request, lines, hist, false);
                if (status.IsSuccess)
                {
                    X.Msg.Alert("", "Travel request has been Assigned").Show();
                    Window1Assign.Hide();
                    X.Js.AddScript("if(typeof(searchListAssign) != 'undefined') searchListAssign();");
                    //LoadLevels();
                }
                else
                {
                    X.Msg.Alert("", status.ErrorMessage).Show();

                }

            }

        }




        protected void LoadEmployeeInfo(int EmployeeID)
        {
            EEmployee eemployee = EmployeeManager.GetEmployeeById(EmployeeID);
            lblName.Text = eemployee.Name;
            lblDepartment.Text = eemployee.Department.Name;
            lblDesignation.Text = eemployee.EDesignation.Name;
            lblBranch.Text = eemployee.Branch.Name;

            bool hasPhoto = false;
            if (eemployee.HHumanResources.Count > 0)
            {
                if (!string.IsNullOrEmpty(eemployee.HHumanResources[0].UrlPhotoThumbnail))
                {
                    image.ImageUrl = Page.ResolveUrl(Utils.Config.UploadLocation + "/" + eemployee.HHumanResources[0].UrlPhotoThumbnail);
                    if (System.IO.File.Exists(Server.MapPath(image.ImageUrl)))
                        hasPhoto = true;
                }
            }
            if (!hasPhoto)
            {
                image.ImageUrl = Page.ResolveUrl(Web.Helper.WebHelper.GetEmployeeNoImage(eemployee.EmployeeId));

            }

            //List<TARequestStatusHistory> statusHistory = TravelAllowanceManager.getTravelRequestForwardHistory(hiddenValue.Text);

            //string body = "";
            //foreach (var status in statusHistory)
            //{
            //    body +=
            //        string.Format("<div class='commentBlock'>{0}</div>", status.ApprovalDisplayTitle + "'s Comment")
            //        +
            //        string.Format("<div class='nameBlock'>{0}</div>", status.ApprovalRemarks + " - " + status.ApprovedOn.Value.ToShortDateString());
            //    //body = body + "<tr><td  colspan='2' style='font-weight:bold'>" + status.whoseComment + "</td></tr><tr><td colspan='2' style='color:blue'>" + status.ApprovalRemarks + "</td></tr><tr><td>" + status.ApprovedByName + "</td><td>" + status.ApprovedOn.Value.ToShortDateString() + "</td></tr>";
            //}


            // Load Forward to list
            List<TextValue> approvalList = TravelAllowanceManager.GetForwardToListForTravelOrder((int)FlowStepEnum.Saved,
                EmployeeID);
            cmbForwardList.Store[0].DataSource = approvalList;
            cmbForwardList.Store[0].DataBind();
            if (approvalList.Count == 1)
                ExtControlHelper.ComboBoxSetSelected(approvalList[0].ID.ToString(), cmbForwardList);

        }

    


    }
}