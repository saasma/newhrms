﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Manager;
using DAL;
using Utils.Helper;
using Utils.Web;
using System.Text;
using BLL;
using BLL.Entity;
using BLL.Base;
using BLL.BO;
using Utils.Calendar;
using Ext.Net;

namespace Web.Employee.UserControls
{
    public partial class OvertimeRequestApproveCtl : System.Web.UI.UserControl
    {
        public bool AssignOvertime { get; set; }
        public bool AssignOvertimeFromAdmin { get; set; }

        public bool IsDisplayedAsPopup
        {
            get
            {
                if (Request.QueryString["isPopup"] != null)
                    return true;
                return false;
            }
        }

        public bool HasImport
        {
            get
            {
                if (ViewState["import"] == null)
                    return false;
                return Convert.ToBoolean(ViewState["import"]);
            }
            set
            {
                ViewState["import"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["assign"]))
            {
                AssignOvertime = true;
            }

            if (!IsPostBack && !X.IsAjaxRequest)
                Initialise();

            if (!IsPostBack && !X.IsAjaxRequest)
            {
                if (!string.IsNullOrEmpty(Request.QueryString["assign"]))
                {
                    header.InnerHtml = "Assign Overtime";
                    

                    if (AssignOvertimeFromAdmin)
                    {
                        cmbRecommender.Visible = false;
                        cmbRecommenderValidation.Enabled = false;
                        rowManager.Visible = false;
                        cmbApproval.Visible = false;
                        cmbApprovalValidation.Visible = false;

                        //List<TextValue> list = new List<TextValue>();

                        //list = new EmployeeManager().GetEmployeesByBranch(-1, -1).Select
                        //    (x => new TextValue { Text = x.Name + " - " + x.EmployeeId.ToString(), Value = x.EmployeeId.ToString() }).ToList();

                        //ddlEmployeeList.DataSource
                        //    = list;
                        //ddlEmployeeList.DataBind();

                        //ddlEmployeeList.AutoPostBack = false;
                    }
                    else
                    {
                        //ddlEmployeeList.DataSource = LeaveAttendanceManager.GetEmployeeListForLeaveAssign();
                        //ddlEmployeeList.DataBind();
                    }

                    btnSave.Visible = false;
                    btnAssignOvertime.Visible = true;
                }
                else
                {
                    row1.Visible = false;
                    row2.Visible = false;
                    reqdEmployee.Enabled = false;
                }
            }
        }

        

        void Initialise()
        {

            if (OvertimeManager.IsRequestAutoGroupType)
            {
                TimePicker1.Hide();
                btnGenerateHour1.Show();
                rowEndDateLabel.Style["display"] = "none";
                rowEndDateValue.Style["display"] = "none";

                rowActualHourLabel.Style["display"] = "";
                rowActualHourValue.Style["display"] = "";

                calDate.Listeners.Change.Handler = "";
            }

            // for Supervisor or Manager
            if (SessionManager.CurrentLoggedInEmployeeId != 0)
            {
                List<TextValue> list = LeaveAttendanceManager.GetEmployeeListForLeaveAssign().ToList();

                storeEmployee.DataSource = list;
                storeEmployee.DataBind();
            }
            else
            {


              
                EmployeeManager mgr = new EmployeeManager();
                List<TextValue> list = mgr.GetPrefixedEmplyeeNames("", null, false).Select
                    (
                    x => new TextValue { Text = x.Name, Value = x.EmployeeId.ToString() }
                    ).ToList();

                storeEmployee.DataSource = list;
                storeEmployee.DataBind();
            }


            EEmployee employee = new EEmployee();
            employee = EmployeeManager.GetEmployeeById(SessionManager.CurrentLoggedInEmployeeId);

            Status status = new Status();
            status = OvertimeManager.IsEmployeeEligibleToOvertime(employee);


            ddlOvertimeType.DataSource = OvertimeManager.GetOvertimeList();
            ddlOvertimeType.DataBind();

            //if (!SessionManager.IsEnglish)
            {
                //calDOB.IsEnglishCalendar = true;
            }

            //cmbSupervisor.DataSource = OvertimeManager.GetSupervisorList(SessionManager.CurrentLoggedInEmployeeId);
            //cmbSupervisor.DataBind();

            Hidden_RequestID.Value = Request.QueryString["reqid"];

            //System.Web.UI.WebControls.ListItem itemRec = cmbRecommender.Items[0];
            //System.Web.UI.WebControls.ListItem itemApr = cmbApproval.Items[0];

            List<TextValue> listReview = LeaveRequestManager.GetApplyToListForEmployee(SessionManager.CurrentLoggedInEmployeeId, false, PreDefindFlowType.Overtime);
            List<TextValue> listApproval = LeaveRequestManager.GetApplyToListForEmployee(SessionManager.CurrentLoggedInEmployeeId, true, PreDefindFlowType.Overtime);

            storeR.DataSource = listReview;
            storeR.DataBind();

            storeApp.DataSource = listApproval;
            storeApp.DataBind();

            //
            if(AssignOvertime == false && AssignOvertimeFromAdmin==false && string.IsNullOrEmpty(Request.QueryString["reqid"]))
            {
                if (listReview.Count == 1)
                    ExtControlHelper.ComboBoxSetSelected(listReview[0].Value.ToString(), cmbRecommender);

                if (listApproval.Count == 1)
                    ExtControlHelper.ComboBoxSetSelected(listApproval[0].Value.ToString(), cmbApproval);
            }

            if (CommonManager.Setting.HideReviewInLeaveRequest != null && CommonManager.Setting.HideReviewInLeaveRequest.Value)
            {
                cmbRecommender.Hide();
                cmbRecommenderValidation.Enabled = false;
            }

            Load();
        }

        public int GetActualOTHour(int ein, DateTime date)
        {
            //int ein = SessionManager.CurrentLoggedInEmployeeId;
            //DateTime date = calDate.SelectedDate;

            BLL.BaseBiz.PayrollDataContext.GenerateAttendanceEmployee(ein, date, date, false);

            AttendanceEmployee atte = BLL.BaseBiz.PayrollDataContext.AttendanceEmployees
                .FirstOrDefault(x => x.EmployeeID == ein && x.Date == date);

            if (atte != null && atte.OTMinute != null)
            {
                return (int)(atte.OTMinute / 60.0);
                //txtActualHour.Text = (atte.OTMinute / 60.0).ToString();
            }

            return 0;
        }
        public void btnGenerateHour_Click(object sender, DirectEventArgs e)
        {
            
            int ein = SessionManager.CurrentLoggedInEmployeeId;
            DateTime date = calDate.SelectedDate;

            if (AssignOvertime == true || AssignOvertimeFromAdmin == true)
            {
                if (cboEmployee.SelectedItem != null && cboEmployee.SelectedItem.Value != null)
                    ein = int.Parse(cboEmployee.SelectedItem.Value);
                else
                    ein = 0;
            }
            
                txtActualHour.Text = GetActualOTHour(ein, date).ToString();

            

            if (string.IsNullOrEmpty(txtHour.Text.Trim()))
                txtHour.Text = txtActualHour.Text;

        }

        new void Load()
        {
            //source = insMgr.GetInsuranceInstitution();
            OvertimeRequest requestInstance = new OvertimeRequest();

            //calDOB.SelectTodayDate();

            calDate.SetValue(DateTime.Today);
            //da.SetValue(DateTime.Today);
            dateEnd.SetValue(DateTime.Today);

            if (!string.IsNullOrEmpty(Hidden_RequestID.Value))
            {
                requestInstance = OvertimeManager.getRequestByID(Hidden_RequestID.Value);
                if (requestInstance.Status != 0)
                {

                    TimePicker1.Enabled = false;
                    TimePicker2.Enabled = false;
                    txtReason.Enabled = false;
                    btnSave.Enabled = false;
                    //calDOB.Enabled = false;
                    calDate.Disable();
                    btnSave.Visible = false;

                    //divWarningMsg.InnerHtml = "Status has been changed. This request cannot be edited.";
                    //divWarningMsg.Hide = false;

                    NewMessage.ShowWarningMessage("Status has been changed. This request cannot be edited.");
                }

                //calDOB.Enabled = false;
                calDate.Disable();
                btnSave.Text = "Update";
                //calDOB.SetSelectedDate(requestInstance.Date.Value.ToString(), true);// = requestInstance.Date.Value;

                if (requestInstance.StartTime != null)
                {
                    TimePicker1.SelectedTime = requestInstance.StartTime.Value.TimeOfDay;
                    TimePicker2.SelectedTime = requestInstance.EndTime.Value.TimeOfDay;
                }
                txtReason.Text = requestInstance.Reason;
                if (requestInstance.RecommendedBy != null)
                    ExtControlHelper.ComboBoxSetSelected(requestInstance.RecommendedBy.ToString(), cmbRecommender);
                if (requestInstance.ApprovalID != null)
                    ExtControlHelper.ComboBoxSetSelected(requestInstance.ApprovalID.ToString(), cmbApproval);
              
                if (requestInstance.OvertimeTypeId != null)
                    UIHelper.SetSelectedInDropDown(ddlOvertimeType, requestInstance.OvertimeTypeId.Value);

                if (requestInstance.RequestedTimeInMin != null)
                    txtHour.Text = OvertimeManager.GetHourMin(requestInstance.RequestedTimeInMin.Value);

                if (requestInstance.ActualTimeInMin != null)
                    txtActualHour.Text = (requestInstance.ActualTimeInMin.Value / 60.0).ToString();

                calDate.SelectedDate = requestInstance.Date.Value;
                if (requestInstance.EndTime != null)
                    dateEnd.SelectedDate = requestInstance.EndTime.Value;
            }
            else
            {
                TimeSpan EndTimeSpan = OvertimeManager.getOfficeEndTime(SessionManager.CurrentLoggedInEmployeeId);
                DateTime EndTime = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, EndTimeSpan.Hours, EndTimeSpan.Minutes, EndTimeSpan.Seconds);
                TimePicker1.SelectedTime = EndTime.TimeOfDay;
                TimePicker2.SelectedTime = DateTime.Now.TimeOfDay;



            }


            if (OvertimeManager.getTopRounding().IsOTRequestTypeAutoGroup != null && OvertimeManager.getTopRounding().IsOTRequestTypeAutoGroup.Value)
            { }
            else if(string.IsNullOrEmpty(txtHour.Text.Trim()))
            {
                DateTime otstart = new DateTime(calDate.SelectedDate.Year, calDate.SelectedDate.Month, calDate.SelectedDate.Day,
                    TimePicker1.SelectedTime.Hours, TimePicker1.SelectedTime.Minutes, 0);
                DateTime otend = new DateTime(dateEnd.SelectedDate.Year, dateEnd.SelectedDate.Month, dateEnd.SelectedDate.Day,
                    TimePicker2.SelectedTime.Hours, TimePicker2.SelectedTime.Minutes, 0);

                int min =
                       (int)otend.Subtract(otstart).TotalMinutes;
                txtHour.Text = AttendanceManager.MinuteToHourMinuteConverterWithOutLabelDisplay(min);
            }
        }






        protected void Page_PreRender(object sender, EventArgs e)
        {
            //Page.ClientScript.RegisterOnSubmitStatement(this.GetType(), "sdfdsffd", "window.onunload = null;");
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "sdf", "var hasImport = " + (this.HasImport ? "true;" : "false;"), true);

            if (!X.IsAjaxRequest && !IsPostBack)
            {
                if (AssignOvertime == false && AssignOvertimeFromAdmin == false)
                {
                    if (cmbRecommender.Items.Count == 1)
                        ExtControlHelper.ComboBoxSetSelected(cmbRecommender.Items[0].Value,cmbRecommender);// = 1;

                    if (cmbApproval.Items.Count == 1)
                        ExtControlHelper.ComboBoxSetSelected(cmbApproval.Items[0].Value, cmbApproval);
                }
            }
        }

        public void cboEmployee_Select(object sender, DirectEventArgs e)
        {
            //int employeeId = int.Parse(ddlEmployeeList.SelectedValue);
           
            int employeeId = int.Parse(cboEmployee.SelectedItem.Value);
            //System.Web.UI.WebControls.ListItem itemRec = cmbRecommender.Items[0];
            //System.Web.UI.WebControls.ListItem itemApr = cmbApproval.Items[0];
            if (employeeId == -1)
            {
                storeR.DataSource = new List<TextValue>();
                storeR.DataBind();

                storeApp.DataSource = new List<TextValue>();
                storeApp.DataBind();
            }
            else
            {
                List<TextValue> listReview = LeaveRequestManager.GetApplyToListForEmployee(employeeId, false, PreDefindFlowType.Overtime);
                List<TextValue> listApproval = LeaveRequestManager.GetApplyToListForEmployee(employeeId, true, PreDefindFlowType.Overtime);

               

                storeR.DataSource = listReview;
                storeR.DataBind();

                storeApp.DataSource = listApproval;
                storeApp.DataBind();
            }

            cmbRecommender.SetValue(SessionManager.CurrentLoggedInEmployeeId.ToString());
            cmbApproval.SetValue(SessionManager.CurrentLoggedInEmployeeId.ToString());

            //UIHelper.SetSelectedInDropDown(cmbRecommender, SessionManager.CurrentLoggedInEmployeeId);
            //UIHelper.SetSelectedInDropDown(cmbApproval, SessionManager.CurrentLoggedInEmployeeId);
        }

        protected void btnAssignOvertime_Click(object sender, DirectEventArgs e)
        {

            if (AssignOvertimeFromAdmin)
            {
                if (SessionManager.User.UserMappedEmployeeId == null)
                {
                    //divWarningMsg.InnerHtml = "Employee has not been mapped to this User.";
                    //divWarningMsg.Hide = false;
                    NewMessage.ShowWarningMessage("Employee has not been mapped to this User.");
                    //return;
                    return;

                    //NewMessage.ShowNormalMessage("Employee has not been mapped to this User", "closePopup();\n");
                    //return;
                }
            }

            Page.Validate("SaveUpdate");
            if (Page.IsValid)
            {
                bool isEdit = false;
                OvertimeRequest requestInstance = new OvertimeRequest();
                if (!string.IsNullOrEmpty(Hidden_RequestID.Value))
                {
                    requestInstance.OvertimeRequestID = int.Parse(Hidden_RequestID.Value);
                    isEdit = true;
                }
                else
                {
                    //requestInstance.RequestID = Guid.NewGuid();
                }

                requestInstance.OvertimeTypeId = int.Parse(ddlOvertimeType.SelectedValue);

                //requestInstance.Date = calDOB.SelectedDate.EnglishDate;
                DateTime selectedDate = calDate.SelectedDate;
                requestInstance.Date = selectedDate;


                if (cboEmployee.SelectedItem == null || cboEmployee.SelectedItem.Value == null)
                {
                    NewMessage.ShowWarningMessage("Employee is required.");
                    return;
                }

                requestInstance.EmployeeID = int.Parse(cboEmployee.SelectedItem.Value);

                if (OvertimeManager.IsRequestAutoGroupType == false)
                {
                    requestInstance.StartTime = new DateTime(calDate.SelectedDate.Year, calDate.SelectedDate.Month, calDate.SelectedDate.Day,
                      TimePicker1.SelectedTime.Hours, TimePicker1.SelectedTime.Minutes, 0);
                    requestInstance.EndTime = new DateTime(dateEnd.SelectedDate.Year, dateEnd.SelectedDate.Month, dateEnd.SelectedDate.Day,
                       TimePicker2.SelectedTime.Hours, TimePicker2.SelectedTime.Minutes, 0);

                    if ((requestInstance.EndTime.Value - requestInstance.StartTime.Value).TotalHours >= 24)
                    {
                        NewMessage.ShowWarningMessage("Requested overtime hour should be under 24 hrs, <br>current total hour is "
                             + (requestInstance.EndTime.Value - requestInstance.StartTime.Value).TotalHours.ToString("n2") + ".");
                        return;
                    }
                }
                
                requestInstance.Reason = txtReason.Text;

                if (AssignOvertimeFromAdmin)
                {
                    requestInstance.Status = (int)OvertimeStatusEnum.Forwarded;
                }
                else
                {

                    if (LeaveRequestManager.CanApprove(requestInstance.EmployeeID.Value, SessionManager.CurrentLoggedInEmployeeId, PreDefindFlowType.Overtime))
                    {
                        requestInstance.Status = (int)OvertimeStatusEnum.Approved;
                    }
                    else
                        requestInstance.Status = (int)OvertimeStatusEnum.Recommended;

                    requestInstance.ApprovalID = int.Parse(cmbApproval.SelectedItem.Value);
                    requestInstance.ApprovalName = cmbApproval.SelectedItem.Text;

                    // if no Review then set Approval emp in Review also
                    if (CommonManager.Setting.HideReviewInLeaveRequest != null && CommonManager.Setting.HideReviewInLeaveRequest.Value)
                        requestInstance.RecommendedBy = requestInstance.ApprovalID;
                    else
                        requestInstance.RecommendedBy = int.Parse(cmbRecommender.SelectedItem.Value);
                }

                ResponseStatus isSuccess = new ResponseStatus();


                ResponseStatus isValidDate = OvertimeManager.isRequestedDateValid(requestInstance.EmployeeID, requestInstance.Date, requestInstance.StartTime, requestInstance.EndTime, isEdit);

                if (!OvertimeManager.CanEmployeeRequestForOverTime(requestInstance.EmployeeID.Value, requestInstance.OvertimeTypeId.Value, requestInstance.Date.Value))
                {

                    NewMessage.ShowWarningMessage("Employee is not eligible for " + ddlOvertimeType.SelectedItem.Text + ".");
                    return;
                    //return;
                }

                //requestInstance.NepaliDate = calDOB.SelectedDate.ToString();

                CustomDate d = new CustomDate(selectedDate.Day, selectedDate.Month, selectedDate.Year, true);
                requestInstance.NepaliDate = CustomDate.ConvertEngToNep(d).ToString();



                string message = "";
                int requestMin = 0, validMaxMinute = 0;
                if (OvertimeManager.IsHourMinValid(txtHour.Text.Trim(), ref requestMin, ref message) == false)
                {
                    NewMessage.ShowWarningMessage(message);
                    return;
                }
                requestInstance.RequestedTimeInMin = requestMin;

                if (OvertimeManager.IsRequestAutoGroupType)
                {
                   // if (!string.IsNullOrEmpty(txtActualHour.Text.Trim()))
                       // requestInstance.ActualTimeInMin = int.Parse(txtActualHour.Text.Trim()) * 60;
                    requestInstance.ActualTimeInMin = GetActualOTHour(SessionManager.CurrentLoggedInEmployeeId, requestInstance.Date.Value);


                    if ((requestInstance.RequestedTimeInMin / 60.0) > requestInstance.ActualTimeInMin)
                    {
                        txtActualHour.Text = requestInstance.ActualTimeInMin.ToString();
                        NewMessage.ShowWarningMessage("OT can be assigned for max of " + (requestInstance.ActualTimeInMin) + " hour only.");
                        return;
                    }

                }
                else
                {

                    // validate if requested hour:min is not greater than input hh:mm
                    validMaxMinute = (int)requestInstance.EndTime.Value.Subtract(requestInstance.StartTime.Value).TotalMinutes;
                    if (requestMin > validMaxMinute)
                    {
                        NewMessage.ShowWarningMessage("OT can be assigned for maximum of " + OvertimeManager.GetHourMin(validMaxMinute) + " hour only.");
                        return;
                    }
                }

                if (requestInstance.RequestedTimeInMin == null || requestInstance.RequestedTimeInMin.Value <= 0)
                {
                    NewMessage.ShowWarningMessage("Valid hour is required to assign the ot.");
                    return;
                }


                if (isValidDate.IsSuccessType)
                {
                    this.HasImport = true;

                    isSuccess = OvertimeManager.AssignOvertime(requestInstance);
                    if (isSuccess.IsSuccessType)
                    {

                        //JavascriptHelper.DisplayClientMsg("Overtime request has been " +
                        //  ((OvertimeStatusEnum)requestInstance.Status).ToString() + ".", this.Page, "closePopup();\n");
                        NewMessage.ShowNormalMessage("Overtime request has been " +
                          ((OvertimeStatusEnum)requestInstance.Status).ToString() + ".", "closePopup();\n");


                        //else
                        //    JavascriptHelper.DisplayClientMsg("Overtime request has been updated.", this, "closePopup();\n");
                    }
                    else
                    {
                        // Response.Write("<script>alert('" + isSuccess.ErrorMessage + "');</script>");
                        //lblMsg.Text = isSuccess.ErrorMessage;
                        //JavascriptHelper.DisplayClientMsg(isSuccess.ErrorMessage, this);
                        //divWarningMsg.InnerHtml = isSuccess.ErrorMessage;
                        //divWarningMsg.Hide = false;
                        NewMessage.ShowWarningMessage(isSuccess.ErrorMessage);
                    }
                }
                else
                {
                    //Response.Write("<script>alert('" + isValidDate.ErrorMessage + "');</script>");
                    //lblMsg.Text = isValidDate.ErrorMessage;
                    //JavascriptHelper.DisplayClientMsg(isValidDate.ErrorMessage, this);
                    //divWarningMsg.InnerHtml = isValidDate.ErrorMessage;
                    //divWarningMsg.Hide = false;
                    NewMessage.ShowWarningMessage(isValidDate.ErrorMessage);
                }

            }
            else
            {
                JavascriptHelper.DisplayClientMsg("Validation fail.", this.Page);
            }
        }

        protected void btnSave_Click(object sender, DirectEventArgs e)
        {
            Page.Validate("SaveUpdate");
            if (Page.IsValid)
            {
                bool isEdit = false;
                OvertimeRequest requestInstance = new OvertimeRequest();
                if (!string.IsNullOrEmpty(Hidden_RequestID.Value))
                {
                    requestInstance.OvertimeRequestID = int.Parse(Hidden_RequestID.Value);
                    isEdit = true;
                }
                else
                {
                    //requestInstance.RequestID = Guid.NewGuid();
                }

                requestInstance.OvertimeTypeId = int.Parse(ddlOvertimeType.SelectedValue);

                //requestInstance.Date = calDOB.SelectedDate.EnglishDate;
                DateTime selectedDate = calDate.SelectedDate;
                requestInstance.Date = selectedDate;
               
                requestInstance.EmployeeID = SessionManager.CurrentLoggedInEmployeeId;

               

                if (OvertimeManager.IsRequestAutoGroupType == false)                
                {
                    requestInstance.StartTime = new DateTime(calDate.SelectedDate.Year, calDate.SelectedDate.Month, calDate.SelectedDate.Day,
                   TimePicker1.SelectedTime.Hours, TimePicker1.SelectedTime.Minutes, 0);

                    requestInstance.EndTime = new DateTime(dateEnd.SelectedDate.Year, dateEnd.SelectedDate.Month, dateEnd.SelectedDate.Day,
                       TimePicker2.SelectedTime.Hours, TimePicker2.SelectedTime.Minutes, 0);

                    if ((requestInstance.EndTime.Value - requestInstance.StartTime.Value).TotalHours >= 24)
                    {
                        NewMessage.ShowWarningMessage("Requested overtime hour should be under 24 hrs, <br>current total hour is "
                             + (requestInstance.EndTime.Value - requestInstance.StartTime.Value).TotalHours.ToString("n2") + ".");
                        return;
                    }
                }

                //requestInstance.EndTime = DateTime.Parse(TimePicker2.Text);
                requestInstance.Reason = txtReason.Text;
                requestInstance.Status = (int)OvertimeStatusEnum.Pending;
                requestInstance.ApprovalID = int.Parse(cmbApproval.SelectedItem.Value);
                requestInstance.ApprovalName = cmbApproval.SelectedItem.Text;
                if (OvertimeManager.IsRequestAutoGroupType)
                    requestInstance.RecommendedBy = requestInstance.ApprovalID;
                else
                    requestInstance.RecommendedBy = int.Parse(cmbRecommender.SelectedItem.Value);
                ResponseStatus isSuccess = new ResponseStatus();

                ResponseStatus isValidDate = OvertimeManager.isRequestedDateValid(requestInstance.EmployeeID, requestInstance.Date, requestInstance.StartTime, requestInstance.EndTime, isEdit);

                if (!OvertimeManager.CanEmployeeRequestForOverTime(requestInstance.EmployeeID.Value, requestInstance.OvertimeTypeId.Value, requestInstance.Date.Value))
                {

                    //divWarningMsg.InnerHtml = "Employee is not eligible for " + ddlOvertimeType.SelectedItem.Text + ".";
                    //divWarningMsg.Hide = false;
                    NewMessage.ShowWarningMessage("Employee is not eligible for " + ddlOvertimeType.SelectedItem.Text + ".");
                    return;
                }



                //requestInstance.NepaliDate = calDOB.SelectedDate.ToString();
                CustomDate d = new CustomDate(selectedDate.Day, selectedDate.Month, selectedDate.Year, true);
                requestInstance.NepaliDate = CustomDate.ConvertEngToNep(d).ToString();


                string message = "";
                int requestMin = 0, validMaxMinute = 0;
                if (OvertimeManager.IsHourMinValid(txtHour.Text.Trim(), ref requestMin, ref message) == false)
                {
                    NewMessage.ShowWarningMessage(message);
                    return;
                }
                requestInstance.RequestedTimeInMin = requestMin;

                if (OvertimeManager.IsRequestAutoGroupType)
                {
                    //if (!string.IsNullOrEmpty(txtActualHour.Text.Trim()))
                      //  requestInstance.ActualTimeInMin = int.Parse(txtActualHour.Text.Trim()) * 60;

                    requestInstance.ActualTimeInMin = GetActualOTHour(SessionManager.CurrentLoggedInEmployeeId, requestInstance.Date.Value);



                    if ((requestInstance.RequestedTimeInMin / 60.0) > requestInstance.ActualTimeInMin)
                    {
                        txtActualHour.Text = requestInstance.ActualTimeInMin.ToString();
                        NewMessage.ShowWarningMessage("OT can be requested for max of " + (requestInstance.ActualTimeInMin) + " hour only.");
                        return;
                    }

                }

                // validate if requested hour:min is not greater than input hh:mm
                if (requestInstance.EndTime != null)
                {
                    validMaxMinute = (int)requestInstance.EndTime.Value.Subtract(requestInstance.StartTime.Value).TotalMinutes;
                    if (requestMin > validMaxMinute)
                    {
                        NewMessage.ShowWarningMessage("OT can be requested for maximum of " + OvertimeManager.GetHourMin(validMaxMinute) + " hour only.");
                        return;
                    }
                }

                if (isValidDate.IsSuccessType)
                {
                    this.HasImport = true;

                    isSuccess = OvertimeManager.InsertUpdateRequest(requestInstance);
                    if (isSuccess.IsSuccessType)
                    {
                        //if (isEdit == false)
                        //    JavascriptHelper.DisplayClientMsg("Overtime request has been saved.", Page, "closePopup();\n");
                        //else
                        //    JavascriptHelper.DisplayClientMsg("Overtime request has been updated.", Page, "closePopup();\n");

                        if (isEdit == false)
                            NewMessage.ShowNormalMessage("Overtime request has been saved.", "closePopup();\n");
                        else
                            NewMessage.ShowNormalMessage("Overtime request has been updated.", "closePopup();\n");
                    }
                    else
                    {
                        // Response.Write("<script>alert('" + isSuccess.ErrorMessage + "');</script>");
                        //lblMsg.Text = isSuccess.ErrorMessage;
                        //JavascriptHelper.DisplayClientMsg(isSuccess.ErrorMessage, this);
                        NewMessage.ShowWarningMessage(isSuccess.ErrorMessage);
                        //divWarningMsg.InnerHtml = isSuccess.ErrorMessage;
                        //divWarningMsg.Hide = false;
                    }
                }
                else
                {
                    //Response.Write("<script>alert('" + isValidDate.ErrorMessage + "');</script>");
                    //lblMsg.Text = isValidDate.ErrorMessage;
                    //JavascriptHelper.DisplayClientMsg(isValidDate.ErrorMessage, this);
                    NewMessage.ShowWarningMessage(isValidDate.ErrorMessage);
                }

            }
            else
            {
                JavascriptHelper.DisplayClientMsg("Validation fail.", Page);
            }
        }

    }
}