﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using BLL.BO;
using System.IO;
using Utils.Helper;

namespace Web.Employee.UserControls
{
    public partial class ECitizenshipCtrl : BaseUserControl
    {
        private bool _isDisplayMode = false;
        public bool IsDisplayMode
        {
            set
            {
                if (value == true)
                {
                    _isDisplayMode = value;
                    CommandColumn3.Visible = false;
                    CommandColumn2.Visible = false;
                    btnAddCitizenship.Visible = false;
                    GridCitizenship.Width = 920;
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();

                if (SessionManager.CurrentLoggedInEmployeeId != 0)
                    hdnIsEmployee.Text = "1";
                else
                    hdnIsEmployee.Text = "0";
            }

            JavascriptHelper.AttachPopUpCode(Page, "addCitizenship", "ECitizenshipDetailsPopup.aspx", 500, 480);
        }
        protected void Initialise()
        {
            int EmployeeID = GetEmployeeID();

            this.LoadCitizenshipGrid(EmployeeID);

        }

        protected void CitizenshipDownLoad(int ID)
        {

            //string contentType = "";
            HCitizenship doc = NewHRManager.GetCitizenshipDetailsById(ID);

            string path = Context.Server.MapPath(@"~/uploads/" + doc.FileLocation + doc.ServerFileName);
            string contentType = doc.FileType;
            string name = doc.UserFileName;

            byte[] bytes = File.ReadAllBytes(path);

            Context.Response.Clear();
            Context.Response.ClearHeaders();
            Context.Response.AppendHeader("Content-Disposition", "attachment; filename=\"" + name + "\"");
            Context.Response.ContentType = contentType;
            Context.Response.BinaryWrite(bytes);
            Context.Response.End();
            // Response.Redirect("~/DocumentHandler.ashx?ID=" + ID);
        }

        protected void LoadCitizenshipGrid(int EmployeeID)
        {
            List<HCitizenship> _HCitizenship = NewHRManager.GetCitizenshipByEmployeeID(EmployeeID);
            if (_HCitizenship != null)
            {
                this.StoreCitizenship.DataSource = _HCitizenship;
                this.StoreCitizenship.DataBind();
            }
            else
            {
                this.StoreCitizenship.DataSource = this.CitizenshipFillData;
                this.StoreCitizenship.DataBind();
            }
        }
        protected int GetEmployeeID()
        {
            //Guid? AssetID = null;
            int EmpID = 0;
            if (!string.IsNullOrEmpty(Request.QueryString["ID"]))
            {
                EmpID = int.Parse(Request.QueryString["ID"]);
            }
            else
                EmpID = SessionManager.CurrentLoggedInEmployeeId;
            return EmpID;
        }
        
        private object[] CitizenshipFillData
        {
            get
            {
                return new object[]
            {
                new object[] { "", "", "",""},
               
            };
            }
        }
        protected void GridCitizenship_Command(object sender, DirectEventArgs e)
        {
            string commandName = e.ExtraParams["command"];
            int CitizenshipID = int.Parse(e.ExtraParams["ID"]);
            HCitizenship _HCitizenship = NewHRManager.GetCitizenshipDetailsById(int.Parse(e.ExtraParams["ID"]));
            string Path = Server.MapPath(@"~/uploads/" + _HCitizenship.FileLocation + _HCitizenship.ServerFileName);
            switch (commandName)
            {
                case "Delete":
                    this.DeleteCitizenship(CitizenshipID, Path);
                    break;
                case "DownLoad":
                    this.CitizenshipDownLoad(CitizenshipID);
                    break;               
            }

        }
        protected void DeleteCitizenship(int ID, string path)
        {

            bool result = NewHRManager.DeleteCitizenshipByID(ID);
            if (result)
            {
                if (File.Exists(path))
                    File.Delete(path);
                //refresh grid
                NewMessage.ShowNormalMessage("Record deleted successfully.");
                this.LoadCitizenshipGrid(this.GetEmployeeID());
            }

        }
       

        protected void btnLoadCtznGrid_Click(object sender, DirectEventArgs e)
        {
            int employeeId = 0;
            if (Request.QueryString["ID"] != null)
                employeeId = int.Parse(Request.QueryString["ID"].ToString());
            else
                employeeId = SessionManager.CurrentLoggedInEmployeeId;

            LoadCitizenshipGrid(employeeId);
        }

    }
}