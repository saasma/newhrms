﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using BLL.BO;
using System.IO;
using Utils.Helper;

namespace Web.Employee.UserControls
{
    public partial class ESkillSetCtrl : BaseUserControl
    {
        private bool _isDisplayMode = false;
        public bool IsDisplayMode
        {
            set
            {
                if (value == true)
                {
                    _isDisplayMode = value;
                    LinkButton1.Visible = false;
                    CommandColumn1.Visible = false;
                    CommandColumn2.Visible = false;
                    GridSkillSets.Width = 920;
                }
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();
                HideButtonAndGridColumn();
            }
            JavascriptHelper.AttachPopUpCode(Page, "addSkillSets", "ESkillSetDetlsPopup.aspx", 450, 500);
            JavascriptHelper.AttachPopUpCode(Page, "editSkillSets", "ESkillSetDetlsPopup.aspx", 450, 500);
        }

        protected void Initialise()
        {
            int EmployeeID = GetEmployeeID();
            this.LoadSkillSetsGrid(EmployeeID);
        }

        protected void LoadSkillSetsGrid(int EmployeeID)
        {

            List<NewHREmployeeSkillSetBO> _EmployeeSkillSetBO = NewHRManager.GetEmployeesSkillSet(EmployeeID);



            this.StoreSkillSets.DataSource = _EmployeeSkillSetBO;
            this.StoreSkillSets.DataBind();

            if (_isDisplayMode && _EmployeeSkillSetBO.Count <= 0)
                GridSkillSets.Hide();
        }


        protected int GetEmployeeID()
        {
            //Guid? AssetID = null;
            int EmpID = 0;
            if (!string.IsNullOrEmpty(Request.QueryString["ID"]))
            {
                EmpID = int.Parse(Request.QueryString["ID"]);
            }
            else
            {
                EmpID = SessionManager.CurrentLoggedInEmployeeId;
            }
            return EmpID;
        }
               

        private object[] SkillSetsFillData
        {
            get
            {
                return new object[]
            {
                new object[] { "", "", "",""},
               
            };
            }
        }

        protected void GridSkillSets_Command(object sender, DirectEventArgs e)
        {
            string commandName = e.ExtraParams["command"];
            int SkillSetsID = int.Parse(e.ExtraParams["ID"]);
            SkillSetEmployee _SkillSetEmployee = NewHRManager.GetSkillSetsDetailsById(int.Parse(e.ExtraParams["ID"]), GetEmployeeID());
            switch (commandName)
            {
                case "Delete":
                    this.DeleteData(SkillSetsID);
                    break;

                case "Edit":
                    {                        
                        hdnSkillSetsID.Text = SkillSetsID.ToString();
                        X.Js.Call("EditSkills");
                        break;
                    }
            }

        }

        protected void DeleteData(int ID)
        {

            bool result = NewHRManager.DeleteSkillSetsByID(ID, GetEmployeeID());
            if (result)
            {
                NewMessage.ShowNormalMessage("Record deleted successfully.");
                this.LoadSkillSetsGrid(this.GetEmployeeID());
            }
        }


        protected void btnLoadSkillGrid_Click(object sender, DirectEventArgs e)
        {
            int employeeId = 0;

            if (Request.QueryString["ID"] != null)
                employeeId = int.Parse(Request.QueryString["ID"].ToString());
            else
                employeeId = SessionManager.CurrentLoggedInEmployeeId;

            LoadSkillSetsGrid(employeeId);
        }

        private void HideButtonAndGridColumn()
        {
            if (Request.QueryString["ID"] != null && SessionManager.CurrentLoggedInEmployeeId != 0)
            {
                if (SessionManager.CurrentLoggedInEmployeeId != int.Parse(Request.QueryString["ID"].ToString()))
                {
                    IsDisplayMode = true;
                }
            }
        }


    }
}