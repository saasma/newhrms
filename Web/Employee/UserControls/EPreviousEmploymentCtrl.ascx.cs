﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using BLL.BO;
using System.IO;
using Utils.Helper;

namespace Web.Employee.UserControls
{
    public partial class EPreviousEmploymentCtrl : BaseUserControl
    {
        private bool _isDisplayMode = false;
        public bool IsDisplayMode
        {
            set
            {
                if (value == true)
                {
                    _isDisplayMode = value;
                    LinkButton1.Visible = false;
                    CommandColumn1.Visible = false;
                    CommandColumn2.Visible = false;
                    GridPreviousEmployment.Width = 920;
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                CheckPermission();

                HideButtonsAndGridColumn();
                Initialise();
            }

            JavascriptHelper.AttachPopUpCode(Page, "addPrevEmployment", "EPreviousEmployDetls.aspx", 500, 500);
            JavascriptHelper.AttachPopUpCode(Page, "editPrevEmp", "EPreviousEmployDetls.aspx", 500, 500);
        }
        protected void Initialise()
        {
            int EmployeeID = GetEmployeeID();
            this.LoadPreviousEmploymentGrid(EmployeeID);

        }

        protected void LoadPreviousEmploymentGrid(int EmployeeID)
        {
            List<HPreviousEmployment> _HPreviousEmployment = NewHRManager.GetPreviousEmploymentByEmployeeID(EmployeeID);

            if (SessionManager.CurrentLoggedInEmployeeId == 0)
                _HPreviousEmployment = _HPreviousEmployment.Where(x => x.Status == (int)HRStatusEnum.Approved).ToList();

            if (Request.QueryString["ID"] != null)
            {
                if (int.Parse(Request.QueryString["ID"].ToString()) != SessionManager.CurrentLoggedInEmployeeId)
                    _HPreviousEmployment = _HPreviousEmployment.Where(x => x.Status == (int)HRStatusEnum.Approved).ToList();
            }

            this.StorePreviousEmployment.DataSource = _HPreviousEmployment;
            this.StorePreviousEmployment.DataBind();

            if (_isDisplayMode && _HPreviousEmployment.Count <= 0)
                GridPreviousEmployment.Hide();
        }
        protected int GetEmployeeID()
        {
            int EmpID = 0;
            if (!string.IsNullOrEmpty(Request.QueryString["ID"]))
            {
                EmpID = int.Parse(Request.QueryString["ID"]);
            }
            else
            {
                EmpID = SessionManager.CurrentLoggedInEmployeeId;
            }
            return EmpID;
        }
        
        private object[] PreviousEmploymentFillData
        {
            get
            {
                return new object[]
            {
                new object[] { "", "", "",""},
               
            };
            }
        }
        protected void GridPreviousEmployment_Command(object sender, DirectEventArgs e)
        {
            string commandName = e.ExtraParams["command"];
            int PreviousEmploymentID = int.Parse(e.ExtraParams["ID"]);
            HPreviousEmployment _HPreviousEmployment = NewHRManager.GetPreviousEmploymentDetailsById(int.Parse(e.ExtraParams["ID"]));

            switch (commandName)
            {
                case "Delete":
                    this.DeletePreviousEmployment(PreviousEmploymentID);
                    break;

                case "Edit":
                    {
                        hdnPreviousEmploymentID.Text = PreviousEmploymentID.ToString();
                        X.Js.Call("editPrevEmploy");
                        break;
                    }
            }

        }
        protected void DeletePreviousEmployment(int ID)
        {

            bool result = NewHRManager.DeletePreviousEmploymentByID(ID);
            if (result)
            {
                //refresh grid
                NewMessage.ShowNormalMessage("Record deleted successfully.");
                this.LoadPreviousEmploymentGrid(this.GetEmployeeID());
            }

        }



        protected void btnReloadPrevEmpGrid_Click(object sender, DirectEventArgs e)
        {
            this.LoadPreviousEmploymentGrid(GetEmployeeID());
        }

        private void HideButtonsAndGridColumn()
        {
            if (Request.QueryString["ID"] != null && SessionManager.CurrentLoggedInEmployeeId != 0)
            {
                if (SessionManager.CurrentLoggedInEmployeeId != int.Parse(Request.QueryString["ID"].ToString()))
                {
                    IsDisplayMode = true;
                }
            }
        }

        private void CheckPermission()
        {
            int employeeId = 0;
            if (Request.QueryString["ID"] != null && SessionManager.CurrentLoggedInEmployeeId != 0)
            {
                if (SessionManager.CurrentLoggedInEmployeeId != int.Parse(Request.QueryString["ID"].ToString()))
                {
                    employeeId = int.Parse(Request.QueryString["ID"]);
                    List<GetOtherEmployeeListForBranchDepartmentHeadResult> list = NewHRManager.GetOtherEmployeeListByEmpId(SessionManager.CurrentLoggedInEmployeeId);
                    bool found = false;
                    foreach (GetOtherEmployeeListForBranchDepartmentHeadResult obj in list)
                    {
                        if (obj.EmployeeId == employeeId)
                        {
                            found = true;
                            break;
                        }
                    }

                    if (found == false)
                    {
                        Response.Redirect("~/Employee/EmpNoPermissionPage.aspx");
                    }
                }
            }

        }


    }
}