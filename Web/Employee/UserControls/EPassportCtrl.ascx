﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EPassportCtrl.ascx.cs"
    Inherits="Web.Employee.UserControls.EPassportCtrl" %>
<ext:Hidden runat="server" ID="hdnPassportID">
</ext:Hidden>
<ext:Hidden runat="server" ID="hdnIsEmployee" />
<ext:LinkButton ID="btnLoadPassportGrid" runat="server" Hidden="true">
    <DirectEvents>
        <Click OnEvent="btnLoadPassportGrid_Click">
            <EventMask ShowMask="true" />
        </Click>
    </DirectEvents>
</ext:LinkButton>
<ext:GridPanel ID="GridPassport" runat="server" Width="1000" Cls="itemgrid" StyleSpec="margin-top:15px;"
    Scroll="None">
    <Store>
        <ext:Store ID="StorePassport" runat="server">
            <Model>
                <ext:Model ID="Model1" runat="server" IDProperty="PassportId">
                    <Fields>
                        <ext:ModelField Name="PassportNo" Type="String" />
                        <ext:ModelField Name="IssuingDate" Type="string" />
                        <ext:ModelField Name="ValidUpto" Type="string" />
                        <ext:ModelField Name="Status" Type="Int" />
                        <ext:ModelField Name="ServerFileName" Type="String" />
                    </Fields>
                </ext:Model>
            </Model>
        </ext:Store>
    </Store>
    <ColumnModel>
        <Columns>
            <ext:Column ID="Column1" runat="server" Text="Passport Number" DataIndex="PassportNo" Wrap="true"
                Width="200" Flex="1" />
            <ext:Column ID="Column2" runat="server" Text="Issuing Date" DataIndex="IssuingDate"
                Width="200">
            </ext:Column>
            <ext:Column ID="Column3" runat="server" Text="Valid Upto" DataIndex="ValidUpto" Width="100">
            </ext:Column>
            <ext:CommandColumn ID="CommandColumn1" runat="server" Width="40">
                <Commands>
                    <ext:CommandSeparator />
                    <ext:GridCommand Icon="PageWhitePut" CommandName="DownLoad">
                        <ToolTip Text="DownLoad" />
                    </ext:GridCommand>
                </Commands>
                <DirectEvents>
                    <Command OnEvent="GridPassport_Command">
                        <ExtraParams>
                            <ext:Parameter Name="ID" Value="record.data.PassportId" Mode="Raw">
                            </ext:Parameter>
                            <ext:Parameter Name="command" Value="command" Mode="Raw">
                            </ext:Parameter>
                        </ExtraParams>
                    </Command>
                </DirectEvents>
                <PrepareToolbar Fn="prepareDownloadPassport" />
            </ext:CommandColumn>
            <ext:CommandColumn ID="CommandColumn3" runat="server" Width="40">
                <Commands>
                    <ext:CommandSeparator />
                    <ext:GridCommand ToolTip-Text="Edit" Icon="ApplicationEdit" CommandName="Edit" />
                </Commands>
                <Listeners>
                    <Command Handler="CommandHandlerPassport(command,record);" />
                </Listeners>
            </ext:CommandColumn>
            <ext:CommandColumn ID="CommandColumn2" runat="server" Width="40">
                <Commands>
                    <ext:CommandSeparator />
                    <ext:GridCommand ToolTip-Text="Delete" Icon="ApplicationDelete"
                                        CommandName="Delete" />
                </Commands>
                <DirectEvents>
                    <Command OnEvent="GridPassport_Command">
                        <ExtraParams>
                            <ext:Parameter Name="ID" Value="record.data.PassportId" Mode="Raw">
                            </ext:Parameter>
                            <ext:Parameter Name="command" Value="command" Mode="Raw">
                            </ext:Parameter>
                        </ExtraParams>
                        <Confirmation ConfirmRequest="true" Message="Are you sure, you want to delete the record?" />
                    </Command>
                </DirectEvents>
                <PrepareToolbar Fn="preparePassport" />
            </ext:CommandColumn>
            <ext:Column ID="Column4" runat="server" Text="" Width="380">
            </ext:Column>
        </Columns>
    </ColumnModel>
    <SelectionModel>
        <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" Mode="Single" />
    </SelectionModel>
</ext:GridPanel>
<div class="buttonBlockSection">
    <ext:Button runat="server" Cls="btn btn-primary btn-sect" Width="120" StyleSpec="margin-top:10px"
        Height="30" ID="LinkButton1" Text="<i></i>Add New Line" OnClientClick="AddPassport();return false;">
    </ext:Button>
</div>
<script type="text/javascript">

    var preparePassport = function (grid, toolbar, rowIndex, record) {
        var delBtn = toolbar.items.get(1);
        var IsEmployee = <%= hdnIsEmployee.ClientID %>.getValue();

        if (record.data.Status == 1 && IsEmployee == "1" ) {
            delBtn.setVisible(false);
        }

    } 

    var prepareDownloadPassport = function(grid, toolbar, rowIndex, record){
        var downloadBtn = toolbar.items.get(1);
        if(record.data.ServerFileName == null || record.data.ServerFileName == ''){
            downloadBtn.setVisible(false);     
        }
    }

    function reloadPassportGrid() {  
        <%= btnLoadPassportGrid.ClientID %>.fireEvent('click');
    }

    function AddPassport()
        {
            addPassport();
        }

    var CommandHandlerPassport = function(command, record){
        if(command=="Edit")
        {
            addPassport('PassportId=' + record.data.PassportId);
        }
    }

</script>
