﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL;
using BLL.Manager;
using BLL;
using BLL.BO;
using System.IO;
using BLL.Base;
using Ext.Net;
using Utils.Helper;

namespace Web.Employee.UserControls
{
    public partial class EHobbyCtrl : BaseUserControl
    {
        public bool ShowEmpty = false;

        private bool _isDisplayMode = false;
        public bool IsDisplayMode
        {
            set
            {
                if (value == true)
                {
                    _isDisplayMode = value;
                    btnAddNewLine.Visible = false;
                    CommandColumn21.Visible = false;
                    CommandColumn2.Visible = false;
                    gridHobbies.Width = 920;
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();
            }

            JavascriptHelper.AttachPopUpCode(Page, "addNewHobby", "EHobbyDetlsPopup.aspx", 450, 500);
            JavascriptHelper.AttachPopUpCode(Page, "edtHobby", "EHobbyDetlsPopup.aspx", 450, 500);
        }

        private void HideButtonBlock()
        {
            btnAddNewLine.Visible = false;
        }

        private void Initialise()
        {
            BindGrid();
        }

        private void BindGrid()
        {
            int EmployeeID = GetEmployeeID();
            gridHobbies.Store[0].DataSource = NewHRManager.GetEHobbyByEmployeeId(EmployeeID);
            gridHobbies.Store[0].DataBind();
        }

        protected int GetEmployeeID()
        {
            int EmpID = 0;
            if (!string.IsNullOrEmpty(Request.QueryString["ID"]))
            {
                EmpID = int.Parse(Request.QueryString["ID"]);
            }
            else
            {
                EmpID = SessionManager.CurrentLoggedInEmployeeId;
            }
            return EmpID;
        }


        protected void gridHobbies_Command(object sender, DirectEventArgs e)
        {
            string commandName = e.ExtraParams["command"];
            int hobbyId = int.Parse(e.ExtraParams["ID"]);

            switch (commandName)
            {
                case "Delete":
                    this.DeleteHobby(hobbyId);
                    break;
                case "Edit":
                    {
                        hdnHobbyId.Text = hobbyId.ToString();
                        X.Js.Call("EditHobby");
                        break;
                    }
            }

        }

        private void DeleteHobby(int hobbyId)
        {
            Status status = NewHRManager.DeleteEHobby(hobbyId);
            if (status.IsSuccess)
            {
                NewMessage.ShowNormalMessage("Record deleted successfully.");
                BindGrid();
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }
        }

        protected void btnReloadHobbyGrid_Click(object sender, DirectEventArgs e)
        {
            BindGrid();
        }

    }
}