﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using BLL.BO;
using System.IO;
using Utils.Helper;

namespace Web.Employee.UserControls
{
    public partial class EDrivingLicenseCtrl : BaseUserControl
    {
        private bool _isDisplayMode = false;
        public bool IsDisplayMode
        {
            set
            {
                if (value == true)
                {
                    _isDisplayMode = value;
                    CommandColumn3.Visible = false;
                    CommandColumn2.Visible = false;
                    btnAddDrLic.Visible = false;
                    GridDrivingLiscence.Width = 920;
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();

                if (SessionManager.CurrentLoggedInEmployeeId != 0)
                    hdnIsEmployee.Text = "1";
                else
                    hdnIsEmployee.Text = "0";
            }
            JavascriptHelper.AttachPopUpCode(Page, "addDrivingLicense", "EDrivingLicenseDetailsPopup.aspx", 500, 480);
        }
        protected void Initialise()
        {
            int EmployeeID = GetEmployeeID();
            this.LoadDrivingLiscenceGrid(EmployeeID);
        }

        protected void DrivingLiscenceDownLoad(int ID)
        {

            //string contentType = "";
            HDrivingLicence doc = NewHRManager.GetDrivingLicenceDetailsById(ID);

            string path = Context.Server.MapPath(@"~/uploads/" + doc.FileLocation + doc.ServerFileName);
            string contentType = doc.FileType;
            string name = doc.UserFileName;

            byte[] bytes = File.ReadAllBytes(path);

            Context.Response.Clear();
            Context.Response.ClearHeaders();
            Context.Response.AppendHeader("Content-Disposition", "attachment; filename=\"" + name + "\"");
            Context.Response.ContentType = contentType;
            Context.Response.BinaryWrite(bytes);
            Context.Response.End();
            // Response.Redirect("~/DocumentHandler.ashx?ID=" + ID);
        }

        protected void LoadDrivingLiscenceGrid(int EmployeeID)
        {
            List<HDrivingLicence> _HDrivingLiscence = NewHRManager.GetDrivingLicenceByEmployeeID(EmployeeID);
            if (_HDrivingLiscence != null)
            {
                this.StoreDrivingLiscence.DataSource = _HDrivingLiscence;
                this.StoreDrivingLiscence.DataBind();
            }
            else
            {
                this.StoreDrivingLiscence.DataSource = this.DrivingLiscenceFillData;
                this.StoreDrivingLiscence.DataBind();
            }
        }
        protected int GetEmployeeID()
        {
            //Guid? AssetID = null;
            int EmpID = 0;
            if (!string.IsNullOrEmpty(Request.QueryString["ID"]))
            {
                EmpID = int.Parse(Request.QueryString["ID"]);
            }
            else
                EmpID = SessionManager.CurrentLoggedInEmployeeId;

            return EmpID;
        }
        
        private object[] DrivingLiscenceFillData
        {
            get
            {
                return new object[]
            {
                new object[] { "", "", "",""},
               
            };
            }
        }
        protected void GridDrivingLiscence_Command(object sender, DirectEventArgs e)
        {
            string commandName = e.ExtraParams["command"];
            int DrivingLiscenceID = int.Parse(e.ExtraParams["ID"]);
            HDrivingLicence _HDrivingLiscence = NewHRManager.GetDrivingLicenceDetailsById(int.Parse(e.ExtraParams["ID"]));
            string Path = Server.MapPath(@"~/uploads/" + _HDrivingLiscence.FileLocation + _HDrivingLiscence.ServerFileName);
            switch (commandName)
            {
                case "Delete":
                    this.DeleteDrivingLiscence(DrivingLiscenceID, Path);
                    break;
                case "DownLoad":
                    this.DrivingLiscenceDownLoad(DrivingLiscenceID);
                    break;
            }

        }
        protected void DeleteDrivingLiscence(int ID, string path)
        {

            bool result = NewHRManager.DeleteDrivingLicenceByID(ID);
            if (result)
            {
                if (File.Exists(path))
                    File.Delete(path);
                //refresh grid
                NewMessage.ShowNormalMessage("Record deleted successfully.");
                this.LoadDrivingLiscenceGrid(this.GetEmployeeID());
            }

        }

        protected void btnLoadDrLicGrid_Click(object sender, DirectEventArgs e)
        {
            int employeeId = 0;
            if (Request.QueryString["ID"] != null)
                employeeId = int.Parse(Request.QueryString["ID"].ToString());
            else
                employeeId = SessionManager.CurrentLoggedInEmployeeId;

            LoadDrivingLiscenceGrid(employeeId);
        }

    }
}