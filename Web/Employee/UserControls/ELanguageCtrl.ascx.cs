﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using BLL.BO;
using System.IO;
using Utils.Helper;

namespace Web.Employee.UserControls
{
    public partial class ELanguageCtrl : BaseUserControl
    {
        private bool _isDisplayMode = false;
        public bool IsDisplayMode
        {
            set
            {
                if (value == true)
                {
                    _isDisplayMode = value;
                    btnAddNewLine.Visible = false;
                    CommandColumn1.Visible = false;
                    CommandColumn2.Visible = false;
                    GridLanguageSets.Width = 920;
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();
                HideButtonAndGridColumn();
            }

            JavascriptHelper.AttachPopUpCode(Page, "addLang", "ELanguageDetlsPopup.aspx", 450, 500);
            JavascriptHelper.AttachPopUpCode(Page, "editLang", "ELanguageDetlsPopup.aspx", 450, 500);
        }
        protected void Initialise()
        {
            int EmployeeID = GetEmployeeID();
            this.LoadLanguageSetsGrid(EmployeeID);
        }

        protected void LoadLanguageSetsGrid(int EmployeeID)
        {

            List<NewHREmployeeLanguageSetBO> _EmployeeLanguageSetBO = NewHRManager.GetEmployeesLanguageSet(EmployeeID);
            //List<LanguageSetEmployee> _LanguageSetEmployee = NewHRManager.GetEmployeesLanguageSet(EmployeeID);



            this.StoreLanguageSets.DataSource = _EmployeeLanguageSetBO;
            this.StoreLanguageSets.DataBind();
            if (_isDisplayMode && _EmployeeLanguageSetBO.Count <= 0)
                GridLanguageSets.Hide();

        }
        protected int GetEmployeeID()
        {
            //Guid? AssetID = null;
            int EmpID = 0;
            if (!string.IsNullOrEmpty(Request.QueryString["ID"]))
            {
                EmpID = int.Parse(Request.QueryString["ID"]);
            }
            else
            {
                EmpID = SessionManager.CurrentLoggedInEmployeeId;
            }
            return EmpID;
        }

        private object[] LanguageSetsFillData
        {
            get
            {
                return new object[]
            {
                new object[] { "", "", "",""},
               
            };
            }
        }
        protected void GridLanguageSets_Command(object sender, DirectEventArgs e)
        {
            string commandName = e.ExtraParams["command"];
            int LanguageSetsID = int.Parse(e.ExtraParams["ID"]);
            LanguageSetEmployee _LanguageSetEmployee = NewHRManager.GetLanguageSetsDetailsById(int.Parse(e.ExtraParams["ID"]), GetEmployeeID());
            switch (commandName)
            {
                case "Delete":
                    this.DeleteData(LanguageSetsID);
                    break;

                case "Edit":
                    {
                        hdnLanguageSetsID.Text = LanguageSetsID.ToString();
                        X.Js.Call("EditLanguage");
                        break;
                    }
            }

        }
        protected void DeleteData(int ID)
        {

            bool result = NewHRManager.DeleteLanguageSetsByID(ID, GetEmployeeID());
            if (result)
            {
                NewMessage.ShowNormalMessage("Record deleted successfully.");
                this.LoadLanguageSetsGrid(this.GetEmployeeID());
            }
        }

        protected void btnLoadLangGrid_Click(object sender, DirectEventArgs e)
        {
            int employeeId = 0;
            if (Request.QueryString["ID"] != null)
                employeeId = int.Parse(Request.QueryString["ID"].ToString());
            else
                employeeId = SessionManager.CurrentLoggedInEmployeeId;
            this.LoadLanguageSetsGrid(employeeId);
        }

        private void HideButtonAndGridColumn()
        {
            if (Request.QueryString["ID"] != null && SessionManager.CurrentLoggedInEmployeeId != 0)
            {
                if (SessionManager.CurrentLoggedInEmployeeId != int.Parse(Request.QueryString["ID"].ToString()))
                {
                    IsDisplayMode = true;
                }
            }
        }


    }
}