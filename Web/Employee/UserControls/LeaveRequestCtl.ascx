﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LeaveRequestCtl.ascx.cs"
    Inherits="Web.Employee.UserControls.LeaveRequestCtl" %>
<asp:ScriptManager ID="ScriptManager1" runat="server" ScriptMode="Release">
    <Services>
        <asp:ServiceReference Path="~/PayrollService.asmx" />
    </Services>
</asp:ScriptManager>
<style type="text/css">
    .itemList
    {
        margin: 0px;
    }
    .itemList li
    {
        float: left;
        list-style-type: none;
    }
    .tblDetails > tbody > tr > td
    {
        padding-top: 5px;
    }
</style>
<script type="text/javascript">
    /* new change */
    var txtReason=null;
    var txtStartDate=null;
    var txtEndDate=null;
    var lblTotalDaysValue=null;

    var chkHalfDay=null;
    var txtLeaveType1=null;
    var lblLeaveType1=null;
    var txtLeaveType2=null;
    var lblLeaveType2=null;

    var cmbLeaveType1 = null;
    var cmbLeaveType2 = null;
    var cmbAMPM = null;
    var lblTotalDaysValue = null;            
    var lblTotalDaysText = null;

    var eventWindow = null;
    var btnShow = null;
    var hdnLeaveRequestId= null;
    var trCompensatory = null;
    var trCompensatoryCancel = null;
    var btnClear =null;
    
    Ext.onReady(
        function()
        {
            initialiseChildCtl();
            
        }
    );

    function initialiseChildCtl()
    {
        trCompensatory = <%=trCompensatory.ClientID %>;
            trCompensatoryCancel = <%=trCompensatoryCancel.ClientID %>;
            btnClear = <%=btnClear.ClientID %>;
            cmbAMPM = <%=cmbAMPM.ClientID %>;

            
            hdnLeaveRequestId = <%=hdnLeaveRequestId.ClientID %>;
            btnShow = <%=btnShow.ClientID %>;
            lblTotalDaysValue = <%=lblTotalDaysValue.ClientID %>;
            lblTotalDaysText = <%=lblTotalDaysText.ClientID %>;

            txtReason=<%=txtReason.ClientID %>;
            txtStartDate=<%=txtStartDate.ClientID %>;
            txtEndDate=<%=txtEndDate.ClientID %>;
            lblTotalDaysValue=<%=lblTotalDaysValue.ClientID %>;

            chkHalfDay=<%=chkHalfDay.ClientID %>;
            txtLeaveType1=<%=txtLeaveType1.ClientID %>;
            lblLeaveType1=<%=lblLeaveType1.ClientID %>;
            txtLeaveType2=<%=txtLeaveType2.ClientID %>;
                lblLeaveType2=<%=lblLeaveType2.ClientID %>;

                cmbLeaveType1=<%=cmbLeaveType1.ClientID %>;
                cmbLeaveType2=<%=cmbLeaveType2.ClientID %>;

                eventWindow = <%=eventWindow.ClientID %>;
    }
   

    function firstLeaveChangeForHalfDay(e1,value,e3,e4)
    {
        if( typeof(hasHourLeave) != 'undefined' && hasHourLeave)
        {}
        else
        {
            if(cmbLeaveType1 == null)
                initialiseChildCtl();
        }

        var r = cmbLeaveType1.getStore().getById((value));
        if( Ext.isEmpty(r) == false)
        {
            if( typeof(hasHourLeave) != 'undefined' && hasHourLeave)
            {}
            else
            {
                if (r.data.IsHalfDayAllowed) {
                    chkHalfDay.show();
                }
                else {
               
                    chkHalfDay.setValue(false);
                    chkHalfDay.hide();
                    cmbAMPM.hide();


                }
            }

            // hide Leave2 for NIBL
            if(typeof(WhichCompany) != 'undefined' && WhichCompany == 34)
            {
            }
            else
            {
                if(r.data.FreqOfAccrual == "Compensatory")
                    trCompensatory.style.display='block';
                else
                    trCompensatory.style.display='none';
            }
            
        }


       if( typeof(hasHourLeave) != 'undefined' && hasHourLeave)
        {
        }
        else
        {
            halfDayLeaveChange();
            setLeaveDays(null,'');
        }
    }
    function halfDayLeaveChange()
    {
        // hide Leave2 for NIBL
        if(typeof(WhichCompany) != 'undefined' && WhichCompany == 27)       
            cmbLeaveType2.show();

        //if(cmbChildLeaves.hidden == false)
        {
            if(chkHalfDay.getValue()==true)
            {
                cmbAMPM.show();   
            }
            else
            {
                cmbAMPM.hide();   
            }
        }
        if(chkHalfDay.getValue()==true)
        {
            cmbLeaveType2.clearValue();
            cmbLeaveType2.hide();
            txtLeaveType1.hide();
            txtLeaveType2.hide();
            lblLeaveType1.hide();
            lblLeaveType2.hide();
            txtEndDate.hide();
            lblTotalDaysValue.hide();
            lblTotalDaysText.hide();
            cmbAMPM.show();                
        }
        else
        {            
            txtLeaveType1.show();
            txtLeaveType2.show();
            lblLeaveType1.show();
            lblLeaveType2.show();
            txtEndDate.show();
            lblTotalDaysValue.show();
            lblTotalDaysText.show();
            cmbAMPM.hide();

            if(cmbLeaveType2.getValue()==null)
            {
                txtLeaveType1.hide();
                txtLeaveType2.hide();
                lblLeaveType1.hide();
                lblLeaveType2.hide();
            }
            else{
                txtLeaveType1.show();
                txtLeaveType2.show();
                lblLeaveType1.show();
                lblLeaveType2.show();
            }
        }
    }

    function datechange(dispElement,changedDate)
    {
        setLeaveDays(dispElement,changedDate);
    }

    //var dispField;
    function fromDateChanged(dispElement,changedDate)
    {
        var value = CompanyX.direct.GetNepDate(changedDate,
            {
                success : function(result)
                {
                    dispElement.setValue(result.NepDate);
                }
            }
            );
    }

    function setLeaveDays(dispElement,changedDate)
    {
        var isHour = false;
        var isHalfDay = chkHalfDay.hidden == false ? chkHalfDay.getValue() : false;
        var from = txtStartDate.getRawValue()
        var to = txtEndDate.getRawValue()
        var leaveTypeId1 = cmbLeaveType1.getValue();
        var leaveTypeId2 = cmbLeaveType2.getValue();
        
        var days1 = txtLeaveType1.getValue();
        var compensatoryIsAdd = <%=compensatoryIsAdd.ClientID %>.getValue();

        var record = cmbLeaveType1.getStore().getById(leaveTypeId1);

        var value = CompanyX.direct.GetDaysForLeave
        (   isHour,isHalfDay,from,to,leaveTypeId1,leaveTypeId2,0,days1,compensatoryIsAdd,changedDate,
            {
                success : function(result)
                {
                    lblTotalDaysValue.setValue(result.DaysCount);
                    if(dispElement != null)
                        dispElement.setValue(result.NepDate);
                }
            }
        );

    }
    

    function CheckValidationCtl()
    {
        if( txtReason.getValue()=="")
        {
            Ext.Msg.show({title:"Warning",buttons:Ext.Msg.OK,icon:Ext.Msg.WARNING,msg:"Reason is required."});
            return false;
        }

         if( <%=cmbLeaveType1.ClientID %>.getValue()=="" || <%=cmbLeaveType1.ClientID %>.getValue()== null)
        {
            Ext.Msg.show({title:"Warning",buttons:Ext.Msg.OK,icon:Ext.Msg.WARNING,msg:"Leave type is required."});

            return false;
        }

        return true;
    }
    function CheckValidationCtlCancel()
    {
        if( <%=txtReasonCancel.ClientID %>.getValue()=="")
        {
            Ext.Msg.show({title:"Warning",buttons:Ext.Msg.OK,icon:Ext.Msg.WARNING,msg:"Cancel Reason is required."});
            return false;
        }

       

        return true;
    }
    function clearField()
    {
        btnClear.fireEvent('click');

    }

    function BindEmployeeId()
    {
        var val = <%= cmbSubstitute.ClientID %>.getValue();
        <%= hdnEmployeeId.ClientID %>.setValue(val);
    }

</script>
<ext:Hidden ID="hdnLeaveRequestId" runat="server" />
<ext:Hidden ID="hdnEmployeeId" runat="server" />
<ext:Button ID="btnShow" Hidden="true" runat="server" Text="Save">
    <DirectEvents>
        <Click OnEvent="btnShow_Click">
            <EventMask ShowMask="true" />
        </Click>
    </DirectEvents>
</ext:Button>
<ext:Button ID="btnClear" Hidden="true" runat="server" Text="Save">
    <DirectEvents>
        <Click OnEvent="btnClear_Click">
            <EventMask ShowMask="true" />
        </Click>
    </DirectEvents>
</ext:Button>
<ext:Window runat="server" Hidden="false" X="-1000" Y="-1000" Title="Apply for Leave"
    ID="eventWindow" Modal="false" Width="570" ButtonAlign="Left" StyleSpec="" Height="500">
    <Content>
        <div style="padding: 15px; padding-top: 5px;">
            <table class="tblDetails">
                <tr>
                    <td>
                        <ext:TextField FieldStyle="border:1px solid lightblue" LabelSeparator="" FieldLabel="Reason *"
                            runat="server" ID="txtReason" LabelWidth="75" Width="450" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <ul class="itemList">
                            <li>
                                <ext:DateField LabelSeparator="" StyleSpec="200" FieldLabel="When *" runat="server"
                                    Format="yyyy/MM/dd" ID="txtStartDate" LabelWidth="75" Width="180">
                                    <Listeners>
                                        <Change Handler="datechange(#{dispNepFrom},this.getRawValue());" />
                                    </Listeners>
                                    <Plugins>
                                        <ext:GenericPlugin ID="HighlightDates1" runat="server" InstanceName="Ext.ux.DatePicker.HighlightWithNepDates" />
                                    </Plugins>
                                </ext:DateField>
                            </li>
                            <li>
                                <ext:TimeField Hidden="true" SelectedTime="09:00" StyleSpec="margin-left:10px" ID="inTime"
                                    LabelSeparator="" runat="server" Increment="1" Format="hh:mm tt" Width="100">
                                </ext:TimeField>
                            </li>
                            <li>
                                <ext:DateField FieldLabel="to" StyleSpec="margin-left:10px" LabelWidth="15" LabelSeparator=""
                                    Format="yyyy/MM/dd" runat="server" ID="txtEndDate" Width="125">
                                    <Listeners>
                                        <Change Handler="datechange(#{dispNepTo},this.getRawValue());" />
                                    </Listeners>
                                    <Plugins>
                                        <ext:GenericPlugin ID="GenericPlugin1" runat="server" InstanceName="Ext.ux.DatePicker.HighlightWithNepDates" />
                                    </Plugins>
                                </ext:DateField>
                            </li>
                            <li>
                                <ext:TimeField Hidden="true" SelectedTime="06:00" ID="outTime" StyleSpec="margin-left:10px"
                                    runat="server" LabelSeparator="" Width="100" Increment="1" Format="hh:mm tt">
                                </ext:TimeField>
                            </li>
                            <li>
                                <ext:DisplayField runat="server" LabelSeparator="" LabelWidth="10" Width="40" Text="0"
                                    FieldLabel="&nbsp;" LabelAlign="Left" ID="lblTotalDaysValue" />
                            </li>
                            <li>
                                <ext:DisplayField runat="server" LabelSeparator="" LabelWidth="0" Width="32" Text="days"
                                    FieldLabel="&nbsp;" LabelAlign="Left" ID="lblTotalDaysText" />
                            </li>
                        </ul>
                    </td>
                </tr>
                <tr runat="server" id="rowNepDate">
                    <td>
                        <ul class="itemList">
                            <li>
                                <ext:DisplayField LabelSeparator="" FieldStyle="color:#6194E9" StyleSpec="" FieldLabel="&nbsp;"
                                    runat="server" ID="dispNepFrom" LabelWidth="75" Width="180">
                                </ext:DisplayField>
                            </li>
                            <li>
                                <ext:DisplayField FieldLabel="" FieldStyle="color:#6194E9" StyleSpec="margin-left:30px;"
                                    LabelWidth="15" LabelSeparator="" runat="server" ID="dispNepTo" Width="125">
                                </ext:DisplayField>
                            </li>
                            <li></li>
                            <li></li>
                        </ul>
                    </td>
                </tr>
                <tr>
                    <td>
                        <ul class="itemList">
                            <li>
                                <ext:ComboBox LabelSeparator="" TypeAhead="true" ForceSelection="true" QueryMode="Local"
                                    FieldLabel="Leave type *" ValueField="LeaveTypeId" DisplayField="Title" runat="server"
                                    ID="cmbLeaveType1" LabelWidth="75" Width="315">
                                    <Store>
                                        <ext:Store ID="storeLeaveType" runat="server">
                                            <Model>
                                                <ext:Model ID="Model1" runat="server" IDProperty="LeaveTypeId">
                                                    <Fields>
                                                        <ext:ModelField Name="LeaveTypeId" Type="String" />
                                                        <ext:ModelField Name="Title" />
                                                        <ext:ModelField Name="IsHalfDayAllowed" Type="Boolean" />
                                                        <ext:ModelField Name="FreqOfAccrual" Type="String" />
                                                        <ext:ModelField Name="IsParentGroupLeave" Type="Boolean" />
                                                    </Fields>
                                                </ext:Model>
                                            </Model>
                                        </ext:Store>
                                    </Store>
                                    <Listeners>
                                        <Change Fn="firstLeaveChangeForHalfDay" />
                                    </Listeners>
                                </ext:ComboBox>
                            </li>
                            <li>
                                <ext:NumberField Hidden="true" MinValue="0" AllowDecimals="false" runat="server"
                                    LabelSeparator="" LabelWidth="5" Width="70" Text="0" FieldLabel="&nbsp;" LabelAlign="Left"
                                    ID="txtLeaveType1">
                                </ext:NumberField>
                            </li>
                            <li>
                                <ext:DisplayField Hidden="true" runat="server" LabelSeparator="" LabelWidth="5" Width="100"
                                    Text="days" FieldLabel="&nbsp;" LabelAlign="Left" ID="lblLeaveType1" />
                            </li>
                        </ul>
                    </td>
                </tr>
                <tr runat="server" id="trCompensatory" style="display: none">
                    <td>
                        <ul class="itemList">
                            <li style="margin-left: 80px;">
                                <ext:Radio LabelSeparator="" LabelWidth="40"  Name="Comensatory" ID="compensatoryIsAdd"
                                    BoxLabel="Add" runat="server" />
                            </li>
                            <li style="padding-left: 15px">
                                <ext:Radio LabelSeparator="" LabelWidth="40"   Name="Comensatory" ID="compensatoryIsDeduct"
                                    BoxLabel="Deduct" runat="server" />
                            </li>
                        </ul>
                    </td>
                </tr>
                <tr>
                    <td>
                        <ul class="itemList">
                            <li>
                                <ext:ComboBox ForceSelection="true" QueryMode="Local" FieldLabel="&nbsp;" ValueField="LeaveTypeId"
                                    DisplayField="Title" LabelSeparator="" runat="server" ID="cmbLeaveType2" LabelWidth="75"
                                    Width="315">
                                    <Store>
                                        <ext:Store ID="store1" runat="server">
                                            <Model>
                                                <ext:Model ID="Model2" runat="server" IDProperty="LeaveTypeId">
                                                    <Fields>
                                                        <ext:ModelField Name="LeaveTypeId" Type="String" />
                                                        <ext:ModelField Name="Title" />
                                                        <ext:ModelField Name="IsHalfDayAllowed" Type="Boolean" />
                                                    </Fields>
                                                </ext:Model>
                                            </Model>
                                        </ext:Store>
                                    </Store>
                                    <Triggers>
                                        <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                                    </Triggers>
                                    <Listeners>
                                        <Select Handler="this.getTrigger(0).show();halfDayLeaveChange();" />
                                        <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                        <TriggerClick Handler="if (index == 0) { 
                                                       this.clearValue(); 
                                                       this.getTrigger(0).hide();
                                                   halfDayLeaveChange();}" />
                                    </Listeners>
                                </ext:ComboBox>
                            </li>
                            <li>
                                <ext:NumberField Hidden="true" MinValue="0" AllowDecimals="false" runat="server"
                                    LabelSeparator="" LabelWidth="5" Width="70" Text="0" FieldLabel="&nbsp;" LabelAlign="Left"
                                    ID="txtLeaveType2" />
                            </li>
                            <li>
                                <ext:DisplayField Hidden="true" runat="server" LabelSeparator="" LabelWidth="5" Width="100"
                                    Text="days" FieldLabel="&nbsp;" LabelAlign="Left" ID="lblLeaveType2" />
                            </li>
                        </ul>
                    </td>
                </tr>
                <tr>
                    <td>
                        <ul class="itemList">
                            <li style="width: 195px" runat="server" id="liHalfDay">
                                <ext:NumberField MinValue="0.5" AllowDecimals="true" runat="server" LabelSeparator=""
                                    LabelWidth="75" Width="150" Text="1" FieldLabel="Hours" LabelAlign="Left" ID="txtHourlyLeaveHours">
                                </ext:NumberField>
                                <ext:Checkbox LabelSeparator="" StyleSpec="margin-left:78px;" BoxLabel="Half day leave"
                                    BoxLabelAlign="After" runat="server" ID="chkHalfDay" LabelWidth="75" Width="180">
                                    <Listeners>
                                        <Change Handler="halfDayLeaveChange();" />
                                    </Listeners>
                                </ext:Checkbox>
                            </li>
                            <li>
                                <ext:ComboBox Hidden="true" QueryMode="Local" LabelWidth="0" ForceSelection="true"
                                    ID="cmbAMPM" runat="server" Width="120" FieldLabel="&nbsp;" LabelSeparator="">
                                    <Items>
                                        <ext:ListItem Text="First half" Value="AM" />
                                        <ext:ListItem Text="Second half" Value="PM" />
                                    </Items>
                                    <SelectedItems>
                                        <ext:ListItem Index="0" />
                                    </SelectedItems>
                                </ext:ComboBox>
                            </li>
                        </ul>
                    </td>
                </tr>
                <tr>
                    <td style="padding-left: 80px">
                        <ext:Checkbox StyleSpec="padding-left: 40px;" Hidden="true" ID="chkIsSpecialCase"
                            runat="server" BoxLabel="Special case (Ignores min days)">
                        </ext:Checkbox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <ext:Store runat="server" ID="storeSearch" AutoLoad="false">
                            <Proxy>
                                <ext:AjaxProxy Url="../../Handler/EmpSearch.ashx">
                                    <ActionMethods Read="GET" />
                                    <Reader>
                                        <ext:JsonReader Root="plants" TotalProperty="total" />
                                    </Reader>
                                </ext:AjaxProxy>
                            </Proxy>
                            <Model>
                                <ext:Model ID="Model7" IDProperty="Name" Name="ItemLineModel" runat="server">
                                    <Fields>
                                        <ext:ModelField Name="Name" Type="String" />
                                        <ext:ModelField Name="EmployeeId" Type="Int" />
                                    </Fields>
                                </ext:Model>
                            </Model>
                        </ext:Store>
                        <ext:ComboBox LabelSeparator="" ID="cmbSubstitute" FieldLabel="Substitute" LabelWidth="75"
                            LabelAlign="Left" runat="server" DisplayField="Name" ValueField="EmployeeId"
                            StoreID="storeSearch" TypeAhead="false" Width="315" PageSize="9999" HideBaseTrigger="true"
                            MinChars="1" TriggerAction="All" ForceSelection="true">
                            <ListConfig LoadingText="Searching..." MinWidth="200" StyleSpec="border-top:1px solid #98c0f4;">
                                <ItemTpl ID="ItemTpl1" runat="server">
                                    <Html>
                                        <tpl>
                                            <div class="search-item">
                                                            <span>{Name}</span>  
                                             </div>
					                </tpl>
                                    </Html>
                                </ItemTpl>
                            </ListConfig>
                            <Triggers>
                                <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                            </Triggers>
                            <Listeners>
                                <Select Handler="this.getTrigger(0).show();BindEmployeeId();" />
                                <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                <TriggerClick Handler="if (index == 0) { 
                                           this.clearValue(); 
                                           this.getTrigger(0).hide();
                                       }" />
                            </Listeners>
                        </ext:ComboBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <ext:DisplayField Hidden="true" ID="lblReviewTo" LabelSeparator="" LabelStyle="font-weight:bold"
                            LabelWidth="75" runat="server" FieldLabel="Review">
                        </ext:DisplayField>
                        <ext:ComboBox ID="cmbReviewTo" QueryMode="Local" DisplayField="Text" ValueField="Value"
                            Hidden="true" LabelSeparator="" ForceSelection="true" Width="315" LabelStyle="font-weight:bold"
                            LabelWidth="75" runat="server" FieldLabel="Review *">
                            <Store>
                                <ext:Store ID="store3" runat="server">
                                    <Model>
                                        <ext:Model ID="Model4" runat="server" IDProperty="Value">
                                            <Fields>
                                                <ext:ModelField Name="Value" Type="String" />
                                                <ext:ModelField Name="Text" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                        </ext:ComboBox>
                    </td>
                </tr>
                <tr>
                    <td style="padding-top: 0px">
                        <ext:DisplayField ID="lblApplyTo" LabelSeparator="" LabelStyle="font-weight:bold"
                            LabelWidth="75" runat="server" FieldLabel="Approval">
                        </ext:DisplayField>
                        <ext:ComboBox ID="cmbApplyTo" QueryMode="Local" DisplayField="Text" ValueField="Value"
                            Hidden="true" LabelSeparator="" ForceSelection="true" Width="315" LabelStyle="font-weight:bold"
                            LabelWidth="75" runat="server" FieldLabel="Approval *">
                            <Store>
                                <ext:Store ID="store4" runat="server">
                                    <Model>
                                        <ext:Model ID="Model5" runat="server" IDProperty="Value">
                                            <Fields>
                                                <ext:ModelField Name="Value" Type="String" />
                                                <ext:ModelField Name="Text" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                        </ext:ComboBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <ext:DisplayField ID="lblStatus" LabelWidth="75" LabelSeparator="" Text="" StyleSpec="font-weight:bold"
                            runat="server" FieldLabel="Status">
                        </ext:DisplayField>
                    </td>
                </tr>
            </table>
        </div>
    </Content>
    <Buttons>
        <ext:Button Hidden="true" StyleSpec="margin-left:10px" ID="btnCancelRequest" Width="120"
            Height="30" runat="server" Text="Cancel Request" ValidationGroup="InsertUpdate">
            <DirectEvents>
                <Click OnEvent="btnCancelShowRequest_Click">
                    <EventMask ShowMask="true" />
                </Click>
            </DirectEvents>
            <Listeners>
                <Click Handler="return CheckValidationCtl();">
                </Click>
            </Listeners>
        </ext:Button>
        <ext:Button ID="btnSave" StyleSpec="margin-left:10px" Width="100" Height="30" runat="server"
            Text="Apply" ValidationGroup="InsertUpdate">
            <DirectEvents>
                <Click OnEvent="btnSave_Click">
                    <EventMask ShowMask="true" />
                </Click>
            </DirectEvents>
            <Listeners>
                <Click Handler="return CheckValidationCtl();">
                </Click>
            </Listeners>
        </ext:Button>
        <%--Disable update feature as if leave type is changed the prev is preserved in LeaveDetail table, test all cases the only give--%>
       <%-- <ext:Button ID="btnUpdate" StyleSpec="margin-left:10px" Hidden="true" Width="100"
            Height="30" runat="server" Text="Update" ValidationGroup="InsertUpdate">
            <DirectEvents>
                <Click OnEvent="btnSave_Click">
                    <Confirmation ConfirmRequest="true" Message="Are you sure, to update the leave request?" />
                    <EventMask ShowMask="true" />
                </Click>
            </DirectEvents>
            <Listeners>
                <Click Handler="return CheckValidationCtl();">
                </Click>
            </Listeners>
        </ext:Button>--%>
        <ext:Button ID="btnCancel" StyleSpec="margin-left:10px" Hidden="true" Width="100"
            Height="30" runat="server" Text="Delete" ValidationGroup="InsertUpdate">
            <DirectEvents>
                <Click OnEvent="btnCancel_Click">
                    <Confirmation ConfirmRequest="true" Message="Cancel the leave request?">
                    </Confirmation>
                    <EventMask ShowMask="true" />
                </Click>
            </DirectEvents>
            <Listeners>
                <Click Handler="return CheckValidationCtl();">
                </Click>
            </Listeners>
        </ext:Button>
        <ext:Button ID="btnActualCancel" StyleSpec="margin-left:10px" Hidden="true" Width="100"
            Height="30" runat="server" Text="Cancel" ValidationGroup="InsertUpdate">
            <DirectEvents>
                <Click OnEvent="btnActualCancel_Click">
                    <Confirmation ConfirmRequest="true" Message="Cancel the leave request?">
                    </Confirmation>
                    <EventMask ShowMask="true" />
                </Click>
            </DirectEvents>
            <Listeners>
                <Click Handler="return CheckValidationCtl();">
                </Click>
            </Listeners>
        </ext:Button>
        <ext:LinkButton ID="btnClose" StyleSpec="margin-left:10px;padding-top:5px;" Width="100"
            Height="30" runat="server" Text="Close" ValidationGroup="InsertUpdate">
            <Listeners>
                <Click Handler="#{eventWindow}.hide();">
                </Click>
            </Listeners>
        </ext:LinkButton>
    </Buttons>
</ext:Window>
<ext:Window runat="server" Hidden="false" X="-1000" Y="-1000" ButtonAlign="Left"
    Title="Cancel Leave Request" ID="windowCancelRequest" Modal="false" Width="510"
    Height="350">
    <Content>
        <div style="padding: 15px; padding-top: 5px;">
            <table class="tblDetails">
                <tr>
                    <td>
                        <ext:TextField LabelSeparator="" FieldLabel="Reason *" runat="server" ID="txtReasonCancel"
                            LabelWidth="75" Width="470" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <ul class="itemList">
                            <li>
                                <ext:DateField LabelSeparator="" StyleSpec="200" FieldLabel="When *" runat="server"
                                    ID="txtStartDateCancel" LabelWidth="75" Width="180">
                                    <Plugins>
                                        <ext:GenericPlugin ID="GenericPlugin2" runat="server" InstanceName="Ext.ux.DatePicker.HighlightWithNepDates" />
                                    </Plugins>
                                </ext:DateField>
                            </li>
                            <li>
                                <ext:DateField FieldLabel="to" StyleSpec="margin-left:10px" LabelWidth="15" LabelSeparator=""
                                    runat="server" ID="txtEndDateCancel" Width="125">
                                    <Listeners>
                                        <Change Handler="datechange(#{dispNepTo},this.getRawValue());" />
                                    </Listeners>
                                    <Plugins>
                                        <ext:GenericPlugin ID="GenericPlugin3" runat="server" InstanceName="Ext.ux.DatePicker.HighlightWithNepDates" />
                                    </Plugins>
                                </ext:DateField>
                            </li>
                        </ul>
                    </td>
                </tr>
                <tr runat="server" id="Tr1">
                    <td>
                        <ul class="itemList">
                            <li>
                                <ext:DisplayField LabelSeparator="" FieldStyle="color:#6194E9" StyleSpec="" FieldLabel="&nbsp;"
                                    runat="server" ID="dispNepFromCancel" LabelWidth="75" Width="180">
                                </ext:DisplayField>
                            </li>
                            <li>
                                <ext:DisplayField FieldLabel="" FieldStyle="color:#6194E9" StyleSpec="margin-left:30px;"
                                    LabelWidth="15" LabelSeparator="" runat="server" ID="dispNepToCancel" Width="125">
                                </ext:DisplayField>
                            </li>
                            <li></li>
                            <li></li>
                        </ul>
                    </td>
                </tr>
                <tr>
                    <td>
                        <ul class="itemList">
                            <li>
                                <ext:ComboBox LabelSeparator="" TypeAhead="true" ForceSelection="true" QueryMode="Local"
                                    FieldLabel="Leave type *" ValueField="LeaveTypeId" DisplayField="Title" runat="server"
                                    ID="cmbLeaveType1Cancel" Disabled="true" LabelWidth="75" Width="315">
                                    <Store>
                                        <ext:Store ID="store2" runat="server">
                                            <Model>
                                                <ext:Model ID="Model3" runat="server" IDProperty="LeaveTypeId">
                                                    <Fields>
                                                        <ext:ModelField Name="LeaveTypeId" Type="String" />
                                                        <ext:ModelField Name="Title" />
                                                        <ext:ModelField Name="IsHalfDayAllowed" Type="Boolean" />
                                                        <ext:ModelField Name="FreqOfAccrual" Type="String" />
                                                        <ext:ModelField Name="IsParentGroupLeave" Type="Boolean" />
                                                    </Fields>
                                                </ext:Model>
                                            </Model>
                                        </ext:Store>
                                    </Store>
                                    <%--<Listeners>
                                        <Change Fn="firstLeaveChangeForHalfDay" />
                                    </Listeners>--%>
                                </ext:ComboBox>
                            </li>
                        </ul>
                    </td>
                </tr>
                <tr runat="server" id="trCompensatoryCancel" style="display: none">
                    <td>
                        <ul class="itemList">
                            <li style="margin-left: 80px;">
                                <ext:Radio LabelSeparator="" Disabled="true" LabelWidth="40" Name="Comensatory" ID="compensatoryIsAddCancel"
                                    FieldLabel="Add" runat="server" />
                            </li>
                            <li style="padding-left: 15px">
                                <ext:Radio LabelSeparator="" LabelWidth="40" Name="Comensatory" ID="compensatoryIsDeductCancel"
                                    FieldLabel="Deduct" runat="server" />
                            </li>
                        </ul>
                    </td>
                </tr>
                <tr>
                    <td>
                        <ul class="itemList">
                            <li style="width: 195px; display: none">
                                <ext:Checkbox LabelSeparator="" Disabled="true" StyleSpec="margin-left:78px;" BoxLabel="Half day leave"
                                    BoxLabelAlign="After" runat="server" ID="chkHalfDayCancel" LabelWidth="75" Width="180">
                                </ext:Checkbox>
                            </li>
                            <li>
                                <ext:ComboBox Hidden="true" Disabled="true" QueryMode="Local" LabelWidth="0" ForceSelection="true"
                                    ID="cmbAMPMCancel" runat="server" Width="120" FieldLabel="&nbsp;" LabelSeparator="">
                                    <Items>
                                        <ext:ListItem Text="First half" Value="AM" />
                                        <ext:ListItem Text="Second half" Value="PM" />
                                    </Items>
                                    <SelectedItems>
                                        <ext:ListItem Index="0" />
                                    </SelectedItems>
                                </ext:ComboBox>
                            </li>
                        </ul>
                    </td>
                </tr>
                <tr>
                    <td>
                        <ext:DisplayField ID="lblApplyToCancel" LabelSeparator="" LabelStyle="font-weight:bold"
                            LabelWidth="75" runat="server" FieldLabel="Approval">
                        </ext:DisplayField>
                    </td>
                </tr>
                <%--<tr>
                    <td>
                        <ext:ComboBox ID="cmbReviewToCancel" QueryMode="Local" DisplayField="Text" ValueField="Value"
                            LabelSeparator="" ForceSelection="true" Width="315" LabelStyle="font-weight:bold"
                            LabelWidth="75" runat="server" FieldLabel="Review *">
                            <Store>
                                <ext:Store ID="store9" runat="server">
                                    <Model>
                                        <ext:Model ID="Model11" runat="server" IDProperty="Value">
                                            <Fields>
                                                <ext:ModelField Name="Value" Type="String" />
                                                <ext:ModelField Name="Text" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                        </ext:ComboBox>
                    </td>
                </tr>
                <tr>
                    <td style="padding-top: 0px">
                        <ext:ComboBox ID="cmbApplyToCancel" QueryMode="Local" DisplayField="Text" ValueField="Value"
                            LabelSeparator="" ForceSelection="true" Width="315" LabelStyle="font-weight:bold"
                            LabelWidth="75" runat="server" FieldLabel="Approval *">
                            <Store>
                                <ext:Store ID="store10" runat="server">
                                    <Model>
                                        <ext:Model ID="Model12" runat="server" IDProperty="Value">
                                            <Fields>
                                                <ext:ModelField Name="Value" Type="String" />
                                                <ext:ModelField Name="Text" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                        </ext:ComboBox>
                    </td>
                </tr>--%>
                <tr>
                    <td>
                        <ext:DisplayField ID="lblStatusCancel" LabelWidth="75" LabelSeparator="" Text=""
                            StyleSpec="font-weight:bold" runat="server" FieldLabel="Status">
                        </ext:DisplayField>
                    </td>
                </tr>
            </table>
        </div>
    </Content>
    <Buttons>
        <ext:Button ID="btnSaveCancel" Width="100" StyleSpec="margin-left:10px" Height="30" runat="server" Text="Apply"
            ValidationGroup="InsertUpdateCancel">
            <DirectEvents>
                <Click OnEvent="btnSaveCancel_Click">
                    <EventMask ShowMask="true" />
                </Click>
            </DirectEvents>
            <Listeners>
                <Click Handler="return CheckValidationCtlCancel();">
                </Click>
            </Listeners>
        </ext:Button>
        <ext:Button ID="btnDeleteCancel" StyleSpec="margin-left:10px" Hidden="true" Width="100"
            Height="30" runat="server" Text="Delete" ValidationGroup="InsertUpdateCancel">
            <DirectEvents>
                <Click OnEvent="btnDeleteCancel_Click">
                    <Confirmation ConfirmRequest="true" Message="Delete the cancel request?">
                    </Confirmation>
                    <EventMask ShowMask="true" />
                </Click>
            </DirectEvents>
            <Listeners>
                <Click Handler="return CheckValidationCtlCancel();">
                </Click>
            </Listeners>
        </ext:Button>
        <ext:LinkButton ID="btnCloseCancel" StyleSpec="margin-left:10px;padding-top:5px;"
            Width="100" Height="30" runat="server" Text="Close" ValidationGroup="InsertUpdateCancel">
            <Listeners>
                <Click Handler="#{windowCancelRequest}.hide();">
                </Click>
            </Listeners>
        </ext:LinkButton>
    </Buttons>
</ext:Window>
