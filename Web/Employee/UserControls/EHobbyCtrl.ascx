﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EHobbyCtrl.ascx.cs" Inherits="Web.Employee.UserControls.EHobbyCtrl" %>

<script type="text/javascript">

    var prepareHobby = function (grid, toolbar, rowIndex, record) {
        var editDelBtn = toolbar.items.get(1);
        if (record.data.IsEditable == 0) {
            editDelBtn.setVisible(false);
        }

    };

    function reloadHobbyGrid()
    {
        <%= btnReloadHobbyGrid.ClientID %>.fireEvent('click');
    }

    function AddHobby()
    {
        addNewHobby();
    }

    function EditHobby()
    {
        edtHobby('HobbyId=' + <%=hdnHobbyId.ClientID %>.getValue());
    }

    
</script>

<ext:Hidden runat="server" ID="hdnHobbyId">
</ext:Hidden>

<ext:LinkButton runat="server" Hidden="true" ID="btnReloadHobbyGrid">
    <DirectEvents>
        <Click OnEvent="btnReloadHobbyGrid_Click">
            <EventMask ShowMask="true" />
        </Click>
    </DirectEvents>
</ext:LinkButton>

<ext:GridPanel ID="gridHobbies" runat="server" Width="1000" Cls="itemgrid">
                    <Store>
                        <ext:Store ID="storeHobbies" runat="server">
                            <Model>
                                <ext:Model ID="Model1" runat="server" IDProperty="HobbyId">
                                    <Fields>
                                        <ext:ModelField Name="HobbyId" Type="String" />
                                        <ext:ModelField Name="HobbyTypeId" Type="String" />
                                        <ext:ModelField Name="HobbyName" Type="string" />
                                        <ext:ModelField Name="InvolvedFrom" Type="string" />
                                        <ext:ModelField Name="Description" Type="string" />
                                        <ext:ModelField Name="IsEditable" Type="Int" />
                                    </Fields>
                                </ext:Model>
                            </Model>
                        </ext:Store>
                    </Store>
                    <ColumnModel>
                        <Columns>                            
                            <ext:Column ID="Column1" Width="200" runat="server" Text="Hobby Name" DataIndex="HobbyName" Wrap="true" />
                            <ext:Column ID="Column2" Width="200" runat="server" Text="Involved From" DataIndex="InvolvedFrom">
                            </ext:Column>
                            <ext:Column ID="Column3" Width="250" runat="server" Text="Description" DataIndex="Description" Wrap="true">
                            </ext:Column>
                          
                       <ext:CommandColumn ID="CommandColumn21" runat="server" Width="40">
                                <Commands>
                                    <ext:CommandSeparator />
                                    <ext:GridCommand ToolTip-Text="Edit" Icon="ApplicationEdit" CommandName="Edit" />
                                    
                                </Commands>
                                <DirectEvents>
                                    <Command OnEvent="gridHobbies_Command">
                                        <EventMask ShowMask="true">
                                        </EventMask>
                                        <ExtraParams>
                                            <ext:Parameter Name="ID" Value="record.data.HobbyId" Mode="Raw">
                                            </ext:Parameter>
                                            <ext:Parameter Name="command" Value="command" Mode="Raw">
                                            </ext:Parameter>
                                        </ExtraParams>
                                    </Command>
                                </DirectEvents>
                            </ext:CommandColumn>
                            <ext:CommandColumn ID="CommandColumn2" runat="server" Width="40">
                                <Commands>
                                    <ext:CommandSeparator />
                                    <ext:GridCommand ToolTip-Text="Delete" Icon="ApplicationDelete"
                                        CommandName="Delete" />
                                </Commands>
                                <DirectEvents>
                                    <Command OnEvent="gridHobbies_Command">
                                        <EventMask ShowMask="true">
                                        </EventMask>
                                        <Confirmation ConfirmRequest="true" Message="Are you sure, you want to delete the record?" />
                                        <ExtraParams>
                                            <ext:Parameter Name="ID" Value="record.data.HobbyId" Mode="Raw">
                                            </ext:Parameter>
                                            <ext:Parameter Name="command" Value="command" Mode="Raw">
                                            </ext:Parameter>
                                        </ExtraParams>
                                    </Command>
                                </DirectEvents>
                                <PrepareToolbar Fn="prepareHobby" />
                            </ext:CommandColumn>
                            <ext:Column ID="Column4" Width="270" runat="server" Text="">
                            </ext:Column>
                        </Columns>
                    </ColumnModel>
                    <SelectionModel>
                        <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" Mode="Single" />
                    </SelectionModel>
                </ext:GridPanel>

    <ext:Button runat="server" Cls="btn btn-primary" Width="120" StyleSpec="margin-top:10px" Height="30"
            ID="btnAddNewLine" Text="<i></i>Add Hobby" OnClientClick="AddHobby(); return false;">
        </ext:Button>