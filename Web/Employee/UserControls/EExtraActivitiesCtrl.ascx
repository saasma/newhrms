﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EExtraActivitiesCtrl.ascx.cs" Inherits="Web.Employee.UserControls.EExtraActivitiesCtrl" %>

<script type="text/javascript">

    var prepareEActivities = function (grid, toolbar, rowIndex, record) {
        var editDelBtn = toolbar.items.get(1);
        if (record.data.IsEditable == 0) {
            editDelBtn.setVisible(false);
        }

    };

    function reloadActivityGrid()
        {
            <%= btnReloadEActGrid.ClientID %>.fireEvent('click');
        }

    function AddNewActivity() {
        addActivity();
    }

    function EditActivity() {
        editActiv('ActivityId=' + <%=hdnCurricularId.ClientID %>.getValue());
    }

</script>

<ext:Hidden runat="server" ID="hdnCurricularId">
</ext:Hidden>


<ext:LinkButton runat="server" Hidden="true" ID="btnReloadEActGrid">
    <DirectEvents>
        <Click OnEvent="btnReloadEActGrid_Click">
            <EventMask ShowMask="true" />
        </Click>
    </DirectEvents>
</ext:LinkButton>

<table class="fieldTable firsttdskip">
        <tr>
            <td>
                <ext:GridPanel ID="gridExtraActivity" runat="server" Width="1000" Scroll="None">
                    <Store>
                        <ext:Store ID="storeExtraActivity" runat="server">
                            <Model>
                                <ext:Model ID="Model1" runat="server" IDProperty="CurricularId">
                                    <Fields>
                                        <ext:ModelField Name="CurricularId" Type="String" />
                                        <ext:ModelField Name="AcitivityName" Type="string" />
                                        <ext:ModelField Name="Proficiency" Type="string" />
                                        <ext:ModelField Name="Award" Type="string" />
                                        <ext:ModelField Name="Year" Type="string" />
                                        <ext:ModelField Name="IsEditable" Type="Int" />
                                    </Fields>
                                </ext:Model>
                            </Model>
                        </ext:Store>
                    </Store>
                    <ColumnModel>
                        <Columns>
                            <ext:Column ID="Column1" Width="200" runat="server" Text="Activity Name" DataIndex="AcitivityName" Wrap="true" />
                            <ext:Column ID="Column2" Width="100" runat="server" Text="Proficiency" DataIndex="Proficiency" Wrap="true">
                            </ext:Column>
                            <ext:Column ID="Column6" Width="200" runat="server" Text="Award" DataIndex="Award" Wrap="true">
                            </ext:Column>
                            <ext:Column ID="Column81" Width="100" runat="server" Text="Year" DataIndex="Year" Wrap="true">
                            </ext:Column>                          
                           
                       <ext:CommandColumn ID="CommandColumn21" runat="server" Width="40">
                                <Commands>
                                    <ext:CommandSeparator />
                                    <ext:GridCommand ToolTip-Text="Edit" Icon="ApplicationEdit" CommandName="Edit" />
                                    
                                </Commands>
                                <DirectEvents>
                                    <Command OnEvent="gridExtraActivity_Command">
                                        <EventMask ShowMask="true">
                                        </EventMask>
                                        <ExtraParams>
                                            <ext:Parameter Name="ID" Value="record.data.CurricularId" Mode="Raw">
                                            </ext:Parameter>
                                            <ext:Parameter Name="command" Value="command" Mode="Raw">
                                            </ext:Parameter>
                                        </ExtraParams>
                                    </Command>
                                </DirectEvents>
                            </ext:CommandColumn>
                            <ext:CommandColumn ID="CommandColumn2" runat="server" Width="40">
                                <Commands>
                                    <ext:CommandSeparator />
                                    <ext:GridCommand ToolTip-Text="Delete" Icon="ApplicationDelete"
                                        CommandName="Delete" />
                                </Commands>
                                <DirectEvents>
                                    <Command OnEvent="gridExtraActivity_Command">
                                        <EventMask ShowMask="true">
                                        </EventMask>
                                        <Confirmation ConfirmRequest="true" Message="Are you sure, you want to delete the record?" />
                                        <ExtraParams>
                                            <ext:Parameter Name="ID" Value="record.data.CurricularId" Mode="Raw">
                                            </ext:Parameter>
                                            <ext:Parameter Name="command" Value="command" Mode="Raw">
                                            </ext:Parameter>
                                        </ExtraParams>
                                    </Command>
                                </DirectEvents>
                                <PrepareToolbar Fn="prepareEActivities" />
                            </ext:CommandColumn>
                            <ext:Column ID="Column3" Width="420" runat="server" Text="">
                            </ext:Column>
                        </Columns>
                    </ColumnModel>
                    <SelectionModel>
                        <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" Mode="Single" />
                    </SelectionModel>
                </ext:GridPanel>
            </td>
        </tr>
    </table>

     <ext:Button runat="server" Cls="btn btn-primary" Width="120" StyleSpec="margin-top:10px" Height="30"
            ID="btnAddNewLine" Text="<i></i>Add Activity" OnClientClick="AddNewActivity(); return false;">
        </ext:Button>