﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using BLL.Manager;
using BLL;
using DAL;
using Utils.Helper;
using BLL.Entity;
using BLL.BO;
using Utils.Calendar;

namespace Web.UserControls
{
    public partial class AssignLeave : BaseUserControl
    {
        private bool _isHrView = false;
        /// <summary>
        /// When is viewed from HR
        /// </summary>
        public bool IsHRView
        {
            get
            {
                return _isHrView;
            }
            set
            {
                _isHrView = value;
            }
        }

        private const string Leave_Key = "L{0}";
        //static String LoggedInEmployeeName = String.Empty;
        public List<GetLeaveListAsPerEmployeeResult> leaves = new List<GetLeaveListAsPerEmployeeResult>();
        public Window AssignLeaveWindow
        {
            get { return this.wdoLeaveApproval; }
        }

        public string SetStartDate
        {
            set { this.hdStartDateEng.Text = value; }
        }
        public string SetEndDate
        {
            set { this.hdEndDateEng.Text = value; }
        }
        void DisableButtonsForCCIfNotAllowToApprove(int requesterEmployeeId)
        {
            if (IsHRView == false)
            {

                int leaveReuestId = 0;
                int.TryParse(hdLeaveRequestId.Text, out leaveReuestId);


                bool isAllowedToApprove = LeaveAttendanceManager.IsAllowedToApproveLeave(SessionManager.CurrentLoggedInEmployeeId, requesterEmployeeId, PreDefindFlowType.Leave, leaveReuestId);
                if (!isAllowedToApprove)
                {
                    btnApprove.ToolTip = "Not enough permission.";
                    btnRecommend.Hide();
                    btnRecommendForReview.Hide();
                    btnApprove.Hide();
                    btnDeny.Hide();
                    btnReDraft.Hide();
                    btnActualCancel.Hide();
                    btnAssign.Hide();
                }
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (IsHRView == false)
            {

                //if (cboEmployee.SelectedItem != null && cboEmployee.SelectedItem.Value != null && cboEmployee.SelectedItem.Value != "")
                // prev set value is picked so use Value property
                if (cboEmployee.Value != null && cboEmployee.Value.ToString() != "")
                {
                    int empid = 0;
                    if (int.TryParse(cboEmployee.Value.ToString(), out empid))
                        DisableButtonsForCCIfNotAllowToApprove(empid);

                }
            }

            if (CommonManager.Setting.HideReviewInLeaveRequest != null && CommonManager.Setting.HideReviewInLeaveRequest.Value)
            {
                cmbReviewTo.Hide();
                lblReviewTo.Hide();
                btnRecommend.Hide();
            }

            X.Js.AddScript("WhichCompany=" + ((int)CommonManager.CompanySetting.WhichCompany).ToString() + ";");

            if (CommonManager.CompanySetting.WhichCompany == WhichCompany.DishHome
                || (cboLeaveType2.Value != null && cboLeaveType2.Value.ToString() != ""))
                cboLeaveType2.Show();
            else
                cboLeaveType2.Hide();

            if (CommonManager.CompanySetting.WhichCompany == WhichCompany.NIBL)
            {
                cboLeaveType2.Hide();
                lblReviewTo.Hide();
                // hide partial cancel for NIBL as they have leave cancellation requst
                btnPartialCancel.Hide();
            }
            else
                rowSpecialCase.Visible = false;

            if (CommonManager.CalculationConstant.CompanyHasHourlyLeave != null
                && CommonManager.CalculationConstant.CompanyHasHourlyLeave.Value)
            {

                X.Js.AddScript("hasHourLeave=true;");
                lblTotalDaysValue.Hide();
                lblTotalDaysText.Hide();
                btnPartialCancel.Hide();

            }

            // hide temporary for all company
            // hide partial cancel for NIBL as they have leave cancellation requst
            //btnPartialCancel.Hide();
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            //startTime.SelectedTime = DateTime.Now.TimeOfDay;
            //endTime.SelectedTime = DateTime.Now.TimeOfDay;

        }
        protected void Page_Load(object sender, EventArgs e)
        {
            //hdIsAllowedToApprove.SetValue(LeaveAttendanceManager.IsAllowedToApproveLeave(SessionManager.CurrentLoggedInEmployeeId));
            if (!X.IsAjaxRequest)
            {
                // Change Days or Hours column name
                //ColumnDaysOrHours.Text = LeaveAttendanceManager.DaysOrHourTitle;
                Initialise();
                // DisableButtonsForCCIfNotAllowToApprove();
                //LoggedInEmployeeName = EmployeeManager.GetEmployeeById(SessionManager.CurrentLoggedInEmployeeId).Name;

                if (this.IsHRView)
                {
                    LoadCombo(0, "");
                }
            }
        }

        void Initialise()
        {

            if (CommonManager.CompanySetting.WhichCompany == WhichCompany.NIBL)
                tdHalfDay.Style["display"] = "none";

            if (CommonManager.CalculationConstant.CompanyHasHourlyLeave != null && CommonManager.CalculationConstant.CompanyHasHourlyLeave.Value)
            {
                chkHourlyLeave.Show();
                chkHourlyLeave.Checked = true;
            }

            //int? CompanyLeaveHoursInADay = CommonManager.CalculationConstant.CompanyLeaveHoursInADay;
            //List<object> list = new List<object>();
            //if (CompanyLeaveHoursInADay != null)
            //{
            //    for (int i = 1; i <= CompanyLeaveHoursInADay; i++)
            //    {
            //        list.Add(new { Text = i.ToString(), Value = i.ToString() });
            //    }
            //}

            //this.storeHourlyLeave.DataSource = list;
            //this.storeHourlyLeave.DataBind();
            //fixed for now
            txtFromDate.Text = DateTime.Now.AddDays(-30).ToShortDateString();
            this.LoadHourlyLeave();
        }
        void LoadHourlyLeave()
        {

            bool? HasCompanyHasHourlyLeave = CommonManager.CalculationConstant.CompanyHasHourlyLeave;
            chkHourlyLeave.Checked = (bool)HasCompanyHasHourlyLeave;
            hdCompanyHasHourlyLeave.Text = chkHourlyLeave.Checked.ToString();
            if (!(bool)HasCompanyHasHourlyLeave)
            {
                chkHourlyLeave.Hide();
                dtTo.Show();
                cboHour.Hide();
                startTime.Hide();
                endTime.Hide();
                chkIsHalfDay.Show();
                chkIsHalfDay.Checked = false;
                chkHalfDayType.Hide();
            }
            else
            {

                chkHourlyLeave.Show();
                dtTo.Show();
                chkHalfDayType.Hide();
                cboHour.Show();
                startTime.Show();
                endTime.Show();
                chkIsHalfDay.Hide();
            }

            this.cboHour.Value = 1;
            //this.cboHour.SelectedIndex = 0;
        }

        public void LoadCombo(int employeeId, string name)
        {
            if (this.IsHRView)
            {
                if (!X.IsAjaxRequest)
                {
                    List<TextValue> list = new List<TextValue>();

                    list = new EmployeeManager().GetEmployeesByBranch(-1, -1).Select
                        (x => new TextValue { Text = x.Name + " - " + x.EmployeeId.ToString(), Value = x.EmployeeId.ToString() }).ToList();

                    storeEmployee.DataSource
                        = list;
                    storeEmployee.DataBind();
                }
            }
            else
            {
                if (employeeId != 0)
                {
                    List<TextValue> list = new List<TextValue>();
                    list.Add(new TextValue { Value = employeeId.ToString(), Text = name });

                    storeEmployee.DataSource = list;
                    storeEmployee.DataBind();
                }
                else
                {
                    storeEmployee.DataSource = LeaveAttendanceManager.GetEmployeeListForLeaveAssign();
                    storeEmployee.DataBind();
                }
            }
        }


        protected void btnAssign_Click(object sender, DirectEventArgs e)
        {
            
            LeaveRequestStatusEnum statusAssign;
            // If HR login or Can Approval permission for the Employee then Approve or else Recommend
            if (LeaveRequestManager.CanApprove(int.Parse(cboEmployee.SelectedItem.Value), SessionManager.CurrentLoggedInEmployeeId, PreDefindFlowType.Leave)
                || this.IsHRView)
            {
                statusAssign = LeaveRequestStatusEnum.Approved;
            }
            else
            {
                statusAssign = LeaveRequestStatusEnum.Recommended;
            }




            string message = "";
            int currentEmployeeId = 0; string currentEmployeeUserName = "";
            UserManager.SetCurrentEmployeeAndUser(ref currentEmployeeId, ref currentEmployeeUserName);
            LeaveRequest leaveRequest = ProcessUpdate((int)statusAssign, ref message);



            if (!string.IsNullOrEmpty(message))
            {
                X.MessageBox.Show(
                       new MessageBoxConfig
                       {
                           Message = message,
                           Buttons = MessageBox.Button.OK,
                           Title = "Warning",
                           Icon = MessageBox.Icon.WARNING,
                           MinWidth = 300
                       });
                return;
            }

            // validate date lock exceeding case
            if (CommonManager.Setting.DateLockForLeaveChangeFromEmployee != null && leaveRequest.FromDateEng != null && SessionManager.CurrentLoggedInEmployeeId != 0)
            {
                if (leaveRequest.FromDateEng.Value.Date <= CommonManager.Setting.DateLockForLeaveChangeFromEmployee.Value.Date)
                {
                    string lockDate = BLL.BaseBiz.GetAppropriateDate(CommonManager.Setting.DateLockForLeaveChangeFromEmployee.Value);
                    if (IsEnglish == false)
                    {
                        lockDate += " (" + CommonManager.Setting.DateLockForLeaveChangeFromEmployee.Value.ToString("yyyy-MMM-dd") + ")";
                    }

                    X.MessageBox.Show(
                       new MessageBoxConfig
                       {
                           Message = "Requesting leave start date should be after " + lockDate + ", please contact HR for details.",
                           Buttons = MessageBox.Button.OK,
                           Title = "Warning",
                           Icon = MessageBox.Icon.WARNING,
                           MinWidth = 300
                       });
                    //status.ErrorMessage = "Requesting leave start date should be after " + lockDate + ", please contact HR for details.";
                    //status.IsSuccess = "false";
                    return;
                }
            }

            if (CommonManager.CompanySetting.LeaveRequestToOnlyOneSupervisor ||
                (CommonManager.Setting.LeaveRequestSettingUsingPreDefinedList != null && CommonManager.Setting.LeaveRequestSettingUsingPreDefinedList.Value)
                || (CommonManager.Setting.LeaveRequestSettngUsingOneToOne != null && CommonManager.Setting.LeaveRequestSettngUsingOneToOne.Value)
                )
            {

                if (CommonManager.Setting.HideReviewInLeaveRequest != null && CommonManager.Setting.HideReviewInLeaveRequest.Value)
                {
                }
                else
                {

                    if ((cmbReviewTo.SelectedItem == null || cmbReviewTo.SelectedItem.Value == null))
                    {
                        NewMessage.ShowWarningMessage("Reviewer is required.");
                        return;
                    }

                    leaveRequest.RecommendEmployeeId = int.Parse(cmbReviewTo.SelectedItem.Value);
                }

                if ((cmbApplyTo.SelectedItem == null || cmbApplyTo.SelectedItem.Value == null))
                {
                    NewMessage.ShowWarningMessage("Approval is required.");
                    return;
                }
                leaveRequest.SupervisorEmployeeIdInCaseOfSelection = int.Parse(cmbApplyTo.SelectedItem.Value);

                if (statusAssign == LeaveRequestStatusEnum.Recommended)
                    leaveRequest.RecommendEmployeeId = currentEmployeeId;
                else if (statusAssign == LeaveRequestStatusEnum.Approved)
                    leaveRequest.SupervisorEmployeeIdInCaseOfSelection = currentEmployeeId;
            }

            //if (statusAssign == LeaveRequestStatusEnum.Recommended)
            //{
            //    leaveRequest.RecommendEmployeeId = 
            //}

            

            //Call to validate for leave
            leaveRequest.FromDate = leaveRequest.FromDateEng.Value.ToShortDateString();
            leaveRequest.ToDate = leaveRequest.ToDateEng.Value.ToShortDateString();
            DAL.LeaveRequest testLeave = new LeaveRequest();
            testLeave.CompensatorIsAddType = leaveRequest.CompensatorIsAddType;
            ResponseStatus statusValidation = LeaveRequestManager.GetLeaveRequestFromCalendarEntity(leaveRequest, testLeave, true, leaveRequest.EmployeeId);
            leaveRequest.ChildLeaveTypeId = testLeave.ChildLeaveTypeId;
            leaveRequest.ChildLeaveTypeId2 = testLeave.ChildLeaveTypeId2;

            if (leaveRequest.ChildLeaveTypeId != null)
            {
                leaveRequest.LeaveDetails.AddRange(testLeave.LeaveDetails);
            }

            if (statusValidation.IsSuccess == "false")
            {
                NewMessage.ShowWarningMessage(statusValidation.ErrorMessage);
                return;
            }

            ResponseStatus status = LeaveAttendanceManager.HandleLeaveRequest(leaveRequest, true);
            if (status.IsSuccessType == true)
            {
                X.MessageBox.Show(
                       new MessageBoxConfig
                       {
                           Message = "Leave " + ((LeaveRequestStatusEnum)statusAssign).ToString() + ".",
                           Buttons = MessageBox.Button.OK,
                           Title = "Information",
                           Icon = MessageBox.Icon.INFO,
                           MinWidth = 300,
                           Handler = "reloadGrids();"
                       });
                LeaveAttendanceManager.NotifyThroughMessageAndEmail(Resources.Messages.LeaveAssigned.ToString(), leaveRequest, leaveRequest.EmployeeId, leaveRequest.FromDateEng.ToString(), leaveRequest.ToDateEng.ToString(), "approved");
                this.wdoLeaveApproval.Hide();

            }
            else
            {
                BLL.Helper.ShowExtWarningMessage(status.ErrorMessage);
            }


        }

        protected void btnCreateLeaveRequest_Click(object sender, DirectEventArgs e)
        {
            string message = "";
            LeaveRequestStatusEnum statusAssign;
            statusAssign = LeaveRequestStatusEnum.Request;

            int currentEmployeeId = 0; string currentEmployeeUserName = "";
            UserManager.SetCurrentEmployeeAndUser(ref currentEmployeeId, ref currentEmployeeUserName);

            if (currentEmployeeId == 0)
            {
                NewMessage.ShowWarningMessage("Please assign the employee from Manage user before make leave request.");
                return;
            }

            LeaveRequest leaveRequest = ProcessUpdate((int)statusAssign, ref message);
            if (!string.IsNullOrEmpty(message))
            {
                X.MessageBox.Show(
                       new MessageBoxConfig
                       {
                           Message = message,
                           Buttons = MessageBox.Button.OK,
                           Title = "Warning",
                           Icon = MessageBox.Icon.WARNING,
                           MinWidth = 300
                       });
                return;
            }

            DAL.LeaveRequest leave = new DAL.LeaveRequest();
            leaveRequest.IsRequestCreatedFromHR = true;




            leaveRequest.FromDate = leaveRequest.FromDateEng.Value.ToShortDateString();
            leaveRequest.ToDate = leaveRequest.ToDateEng.Value.ToShortDateString();

            ResponseStatus status = LeaveRequestManager.GetLeaveRequestFromCalendarEntity(leaveRequest, leave, true, leaveRequest.EmployeeId);

            leave.IsRequestCreatedFromHR = true;
            leave.SubstituteEmployeeId = leaveRequest.SubstituteEmployeeId;
            leave.CreatedBy = currentEmployeeId;

            if (CommonManager.CompanySetting.LeaveRequestToOnlyOneSupervisor ||
                (CommonManager.Setting.LeaveRequestSettingUsingPreDefinedList != null && CommonManager.Setting.LeaveRequestSettingUsingPreDefinedList.Value)
                || (CommonManager.Setting.LeaveRequestSettngUsingOneToOne != null && CommonManager.Setting.LeaveRequestSettngUsingOneToOne.Value)
                )
            {
                if (CommonManager.Setting.HideReviewInLeaveRequest != null && CommonManager.Setting.HideReviewInLeaveRequest.Value)
                {
                }
                else
                {
                    if (cmbReviewTo.SelectedItem != null && cmbReviewTo.SelectedItem.Value != null)
                        leave.RecommendEmployeeId = int.Parse(cmbReviewTo.SelectedItem.Value);
                }

                if (cmbApplyTo.SelectedItem == null || cmbApplyTo.SelectedItem.Value == null)
                {
                    NewMessage.ShowWarningMessage("Approval must be selected.");
                    return;
                }

                if (CommonManager.Setting.HideReviewInLeaveRequest != null && CommonManager.Setting.HideReviewInLeaveRequest.Value)
                {
                    leave.RecommendEmployeeId = int.Parse(cmbApplyTo.SelectedItem.Value);
                }

                leave.SupervisorEmployeeIdInCaseOfSelection = int.Parse(cmbApplyTo.SelectedItem.Value);
            }
            //if (!string.IsNullOrEmpty(selectedSupervisor))
            //    leave.SupervisorEmployeeIdInCaseOfSelection = int.Parse(selectedSupervisor);

            if (status.IsSuccess == "true")
            {
                bool email = true;
                //leave.HalfDayType = halfDayType;
                LeaveAttendanceManager.SaveUpdateLeaveRequest(leave, ref email);

                LeaveAttendanceManager.NotifyThroughMessageAndEmail(leave.LeaveRequestId, leave.IsHour,
                    leave.DaysOrHours.ToString(), leave.Reason, leave.FromDateEng.ToString(), leave.ToDateEng.ToString(), ((LeaveRequestStatusEnum)leave.Status).ToString(), leave.SupervisorEmployeeIdInCaseOfSelection
                    , leave.RecommendEmployeeId, null);

                X.Js.AddScript("reloadGrids();");
                this.AssignLeaveWindow.Hide();

            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }

        }

        protected void btnCancel_Click(object sender, DirectEventArgs e)
        {
            ClearField();
            this.wdoLeaveApproval.Hide();
        }



        public void DisableControls()
        {
            //cboLeaveType.Disabled = true;
            //cboLeaveType2.Disabled = true;
            //txtLeaveType1.Disabled = true;
            //txtLeaveType2.Disabled = true;
            chkIsHalfDay.ReadOnly = true;
            dtFrom.ReadOnly = true;
            dtTo.ReadOnly = true;

            //this.cboLeaveType.Disabled = true;
            //this.cboHour.Disabled = true;
            //this.dtFrom.Disabled = true;
            //this.dtTo.Disabled = true;
            this.txtReason.ReadOnly = true;
            this.cboEmployee.ReadOnly = true;
        }

        public void EnableControl()
        {
            this.cboLeaveType.ReadOnly = false;
            this.cboHour.ReadOnly = false;
            this.startTime.ReadOnly = false;
            this.endTime.ReadOnly = false;
            this.dtFrom.ReadOnly = false;
            this.dtTo.ReadOnly = false;
            this.chkHalfDayType.ReadOnly = false;

            this.txtReason.ReadOnly = false;
            this.cboEmployee.ReadOnly = false;
        }



        public void ShowForAssignLeave()
        {
            this.btnCancel.Show();
            this.btnAssign.Show();
            this.btnApprove.Hide();
            this.btnRecommend.Hide();
            btnRecommendForReview.Hide();
            this.btnReDraft.Hide();
            this.btnDeny.Hide();
            //this.btnHRViewDelete.Hide();
            this.btnActualCancel.Hide();

            this.hdEmployeeId.Text = "0";

            X.ResourceManager.RegisterClientScriptBlock("sdkjfksdf", "ReloadGrid();");
            // clear grids
            BuildGrid(0);
        }

        public void ShowForHRCreateRequest()
        {
            this.btnCancel.Show();
            this.btnAssign.Show();
            this.btnApprove.Hide();
            this.btnRecommend.Hide();
            btnRecommendForReview.Hide();
            this.btnReDraft.Hide();
            this.btnDeny.Hide();
            this.btnActualCancel.Hide();

            this.hdEmployeeId.Text = "0";

            //this.btnHRViewDelete.Hide();
            this.btnAssign.Hide();
            this.btnCreateLeaveRequest.Show();





            X.ResourceManager.RegisterClientScriptBlock("sdkjfksdf", "ReloadGrid();");
            // clear grids
            BuildGrid(0);
        }

        public void ClearField()
        {
            this.divGrid.InnerHtml = string.Empty;
            this.cboEmployee.ReadOnly = false;
            this.cboLeaveType.ReadOnly = false;
            this.cboLeaveType2.ReadOnly = false;
            txtLeaveType1.ReadOnly = false;
            txtLeaveType2.ReadOnly = false;
            this.cboHour.ReadOnly = false;
            this.startTime.ReadOnly = false;
            this.endTime.ReadOnly = false;
            this.dtFrom.ReadOnly = false;
            this.dtTo.ReadOnly = false;
            this.chkHalfDayType.ReadOnly = false;
            this.txtReason.ReadOnly = false;
            this.txtComment.ReadOnly = false;
            this.btnAssign.Disabled = false;
            this.btnCancel.Disabled = false;
            this.chkIsHalfDay.ReadOnly = false;
            this.chkHalfDayType.ReadOnly = false;

            this.lblStatus.Text = "";
            this.lblDesignation.Text = "";
            this.lblBranch.Text = "";
            this.chkIsHalfDay.Checked = false;

            if (CommonManager.CalculationConstant.CompanyHasHourlyLeave != null && CommonManager.CalculationConstant.CompanyHasHourlyLeave.Value)
            {
                this.chkHourlyLeave.Checked = true;
                this.chkHourlyLeave.ReadOnly = true;
            }
            else
                this.chkHourlyLeave.Checked = false;


            chkIsSpecialCase.ReadOnly = false;
            txtSpecialComment.ReadOnly = false;

            this.btnCreateLeaveRequest.Hide();
            this.cboEmployee.ClearValue();
            this.cboLeaveType.ClearValue();
            //this.cboHou

            this.dtFrom.Text = dtFrom.Text;
            this.dtTo.Text = dtTo.Text;
            this.txtReason.Text = string.Empty;
            this.txtComment.Text = string.Empty;
            this.lblApplyTo.Text = string.Empty;

            //cboEmployee.GetStore().ClearFilter();

            txtLeaveType1.Hide();
            txtLeaveType2.Hide();
            lblLeaveType1.Hide();
            lblLeaveType2.Hide();

            this.lblApplyTo.Show();
            this.lblReviewTo.Show();
            this.cmbApplyTo.Hide();
            this.cmbReviewTo.Hide();

            if (CommonManager.CalculationConstant.CompanyHasHourlyLeave.Value)
            {
                this.chkHourlyLeave.Show();
            }

            txtSpecialComment.Hide();
            hdLeaveRequestId.Text = "0";
        }

        public void SetMaxDate(DateTime? maxDate)
        {
            if (maxDate != null)
            {

                //dtFrom.MaxDate = maxDate.Value;
                //dtTo.MaxDate = maxDate.Value;
            }
        }
        public void SetMinDate(DateTime? minDate)
        {
            if (minDate != null)
            {
                //if min is set then prev of that date is not being displayed so commented
                //dtFrom.MinDate = minDate.Value;
                //dtTo.MinDate = minDate.Value;
            }

        }
        protected void cboEmployee_Select(object sender, DirectEventArgs e)
        {
            if (cboEmployee.SelectedItem == null || cboEmployee.SelectedItem.Value == null)
                return;

            int empId = int.Parse(cboEmployee.SelectedItem.Value);

            string review = "", approval = "";
            LeaveRequestManager.GetApplyToForEmployee(empId, ref review, ref approval);
            lblReviewTo.Text = review;
            lblApplyTo.Text = approval;

            LoadEmployeeGrid(empId, 0);

            // Substiture
            //EEmployee employee = new EmployeeManager().GetById(empId);
            //cmbSubstitute.Store[0].DataSource = new EmployeeManager().GetEmployeesByBranch(-1, -1)
            //    .Where(x => x.EmployeeId != empId).ToList();
            //cmbSubstitute.Store[0].DataBind();
        }

        private void LoadEmployeeGrid(int empId, int leaveRequestId)
        {
            this.hdEmployeeId.SetValue(empId.ToString());
            LeaveAttendanceManager lam = new LeaveAttendanceManager();
            // Load Leave list for Employee
            int daysDiff = 0;
            PayrollPeriod payrollPeriod = CommonManager.GetLastPayrollPeriod();



          

            leaves = LeaveAttendanceManager.GetAllLeavesForLeaveRequest(empId, payrollPeriod.PayrollPeriodId, false).ToList();

            GetLeaveListAsPerEmployeeResult upl = new GetLeaveListAsPerEmployeeResult();
            upl.LeaveTypeId = 0;
            upl.IsHalfDayAllowed = true;
            upl.Title = Resources.Messages.UnpaidLeaveTitleForLeaveRequest;
            leaves.Add(upl);


            // in case of edit mode load all leave types as leave selection is not editable, otherwise old application leave if not 
            // applicable in current months then it will not be displayed
            if (leaveRequestId != 0)
            {
                List<LLeaveType> list = LeaveAttendanceManager.GetAllLeaves(SessionManager.CurrentCompanyId);

                LLeaveType upl1 = new LLeaveType();
                upl1.LeaveTypeId = 0;
                upl1.IsHalfDayAllowed = true;
                upl1.Title = Resources.Messages.UnpaidLeaveTitleForLeaveRequest;
                list.Add(upl1);
                

                storeLeaveType.DataSource = list;

            }
            else
            {


                if (CommonManager.Setting.HideUnpaidLeaveInLeaveRequest != null && CommonManager.Setting.HideUnpaidLeaveInLeaveRequest.Value
                    && leaveRequestId == 0)
                    storeLeaveType.DataSource = leaves.Where(x => x.IsChildLeave != true).Where(x => x.LeaveTypeId != 0).ToList();
                else
                    storeLeaveType.DataSource = leaves.Where(x => x.IsChildLeave != true).ToList();
            }
            storeLeaveType.DataBind();

            //cmbChildLeaves.Store[0].DataSource = leaves.Where(x => x.IsChildLeave == true).ToList();
            //cmbChildLeaves.Store[0].DataBind();



            // For Hr view leave request show Review/Apply to Combo Box 
            if (btnCreateLeaveRequest.Hidden == false)
            {
                if (CommonManager.CompanySetting.LeaveRequestToOnlyOneSupervisor
                    || (CommonManager.Setting.LeaveRequestSettingUsingPreDefinedList != null && CommonManager.Setting.LeaveRequestSettingUsingPreDefinedList.Value)
                    || (CommonManager.Setting.LeaveRequestSettngUsingOneToOne != null && CommonManager.Setting.LeaveRequestSettngUsingOneToOne.Value)
                    )
                {
                    List<TextValue> listReview = LeaveRequestManager.GetApplyToListForEmployee(empId, false, PreDefindFlowType.Leave);
                    List<TextValue> listApproval = LeaveRequestManager.GetApplyToListForEmployee(empId, true, PreDefindFlowType.Leave);

                    cmbApplyTo.Store[0].DataSource = listApproval;
                    cmbApplyTo.Store[0].DataBind();
                    cmbApplyTo.SelectedItems.Add(new Ext.Net.ListItem { Index = 0 });

                    cmbReviewTo.Store[0].DataSource = listReview;
                    cmbReviewTo.Store[0].DataBind();
                    cmbReviewTo.SelectedItems.Add(new Ext.Net.ListItem { Index = 0 });

                    if (leaveRequestId != 0 &&
                        CommonManager.Setting.LeaveAllowMultipleReview != null && CommonManager.Setting.LeaveAllowMultipleReview.Value)
                    {
                        cmbReviewerList.Store[0].DataSource = LeaveRequestManager.GetFilterRecommenderForForward(listReview, leaveRequestId);
                        cmbReviewerList.Store[0].DataBind();
                    }


                    cmbApplyTo.Show();
                    cmbReviewTo.Show();

                    lblApplyTo.Hide();
                    lblReviewTo.Hide();
                }
                else
                {
                    cmbApplyTo.Hide();
                    cmbReviewTo.Hide();

                    this.lblApplyTo.Show();
                    this.lblReviewTo.Show();
                }
            }

            string des;
            des = string.IsNullOrEmpty(CommonManager.GetLevelNameByEmpId(empId)) ? "" : CommonManager.GetLevelNameByEmpId(empId);
            des = des + (!string.IsNullOrEmpty(CommonManager.GetLevelNameByEmpId(empId)) ? " - " : "") + CommonManager.GetDesignationNameByEmpId(empId);
            lblDesignation.Text = des;

            lblBranch.Text = EmployeeManager.GetCurrentBranchDepartment(empId);

            //lblApplyTo.Text = LeaveRequestManager.GetApplyToForEmployee(empId);
            BuildGrid(empId);
            if (X.IsAjaxRequest)
                this.cboLeaveType.ClearValue();
            X.ResourceManager.RegisterClientScriptBlock("sdfsdfsdf", "ReloadGrid();");
        }

        #region Build GridPanel
        void BuildGrid(int empId)
        {
            // javascript error is being generated by this code
            GridPanel grid = this.BuildGridPanel(empId);
            grid.Height = 300;

            grid.Cls = "gridtbl";
            grid.Title = "Leaves Taken By the Employee";
            grid.Header = false;
            grid.ColumnModel.Columns.Add(new Column { Text = "Month", Locked = true, DataIndex = "MonthName", Width = 75 });
            grid.ColumnModel.Columns.AddRange(GetColumns());
            grid.AddTo(divGrid);
        }

        private GridPanel BuildGridPanel(int empId)
        {
            Store store = this.BuildStore(empId);
            //this.Page.Form.Controls.Add(store);
            //store.Render();


            GridPanel grid = new GridPanel
            {
                ID = "gridSummary",
                Height = 330,
                Width = 548,
                Border = true,
                //StripeRows = true,
                //TrackMouseOver = true,
                //    Store =  
                //{
                //    store
                //},
                SelectionModel =
            {
                new RowSelectionModel { }
            }
            };

            grid.Store.Add(store);

            return grid;
        }

        private Store BuildStore(int empId)
        {
            Store store = new Store();
            store.ID = "Store111";
            Model arr = new Model();
            //arr.
            //arr.
            arr.Fields.Add(new ModelField("MonthName"));



            arr.Fields.AddRange(GetRecordField());

            store.Model.Add(arr);

            //store.Reader.Add(arr);
            store.DataSource = GetData(empId);
            store.DataBind();

            return store;
        }

        protected object[] GetData(int empId)
        {

            List<LeaveAdjustment> leaveAdjustments = LeaveAttendanceManager.GetLeaveSummary(empId);



            //CommonManager.GetLastPayrollPeriod(ref readingSumForMiddleFiscalYearStartedReqd, ref startingPayrollPeriodId, ref endingPayrollPeriodId);
            List<PayrollPeriod> periods = LeaveAttendanceManager.GetPayrollPeriodList(empId);

            if (IsEnglish == false)
            {
                foreach (PayrollPeriod payroll in periods)
                {
                    // payroll.Name = payroll.star
                    payroll.Name = payroll.StartDateEng.Value.ToString("MMM") + "-" + payroll.EndDateEng.Value.ToString("MMM");
                }
            }

            // if has Advance approved leave then only show Advance
            if (leaveAdjustments.Any(x => x.PayrollPeriodId == -1))
                periods.Insert(0, new PayrollPeriod { PayrollPeriodId = -1, Name = "Future" });
            periods.Insert(0, new PayrollPeriod { PayrollPeriodId = 0, Name = "<b> Balance </b>" });
            // Add balance in LeaveAdj list
            List<GetEmployeeLeaveBalanceResult> balanceList = LeaveAttendanceManager.GetEmployeeLeaveBalance(empId, false, leaves);
            foreach (GetEmployeeLeaveBalanceResult balance in balanceList)
            {
                leaveAdjustments.Add(new LeaveAdjustment { LeaveTypeId = balance.LeaveTypeId, PayrollPeriodId = 0, Taken = (double)(balance.NewBalance == null ? double.Parse("0") : double.Parse(balance.NewBalance.Value.ToString("N2"))) });
            }

            Dictionary<string, string> PayrollLeave = new Dictionary<string, string>();
            // combine child to parent 

            List<LLeaveType> leaveList = LeaveAttendanceManager.GetAllLeaves(SessionManager.CurrentCompanyId);


            foreach (LeaveAdjustment lvAdj in leaveAdjustments)
            {

                int leaveTypeId = lvAdj.LeaveTypeId;
                int periodId = lvAdj.PayrollPeriodId;

                LLeaveType leave = leaveList.FirstOrDefault(x => x.LeaveTypeId == leaveTypeId);
                if (leave != null && leave.IsChildLeave != null && leave.IsChildLeave.Value
                    && leave.ParentLeaveTypeId != null)
                {
                    leaveTypeId = leave.ParentLeaveTypeId.Value;
                }

                string key = string.Concat
                        (
                            string.Concat(periodId.ToString(), "_"), leaveTypeId.ToString()
                        );


                if (PayrollLeave.ContainsKey(key))
                {
                    string currentValue = PayrollLeave[key];
                    double value;
                    if (double.TryParse(currentValue, out value) && lvAdj.Taken != null)
                    {
                        value += lvAdj.Taken.Value;
                        PayrollLeave[key] = value.ToString();
                    }
                }
                else
                {
                    PayrollLeave.Add
                        (
                        key
                            ,
                            (string)lvAdj.Taken.ToString()
                        );
                }
            }

            List<object> data = new List<object>();

            // remove child leaves
            List<GetLeaveListAsPerEmployeeResult> newList = new List<GetLeaveListAsPerEmployeeResult>();
            foreach (GetLeaveListAsPerEmployeeResult item in leaves)
            {
                LLeaveType leave = leaveList.FirstOrDefault(x => x.LeaveTypeId == item.LeaveTypeId);
                if (leave != null && leave.IsChildLeave != null && leave.IsChildLeave.Value)
                {
                }
                else
                    newList.Add(item);
            }
            leaves = newList;

            for (int i = 0; i < periods.Count; i++)
            {

                if (i > 12)
                    break;

                object[] rowData = new object[leaves.Count + 1];

                int columnIndex = 0;
                //for balance row

                rowData[columnIndex] = periods[i].Name;

                for (int j = 0; j < leaves.Count; j++)
                {
                    if (PayrollLeave.ContainsKey(periods[i].PayrollPeriodId + "_" + leaves[j].LeaveTypeId))
                    {
                        string value = PayrollLeave[periods[i].PayrollPeriodId + "_" + leaves[j].LeaveTypeId].ToString();
                        double number;
                        ++columnIndex;
                        if (double.TryParse(value, out number))
                        {
                            if (number == 0)
                                rowData[columnIndex] = null;
                            else
                                rowData[columnIndex] = value;
                        }
                        else
                            rowData[columnIndex] = null;

                    }
                    else
                    {
                        rowData[++columnIndex] = null;
                    }
                }


                data.Add(rowData);
            }




            return data.ToArray();


        }

        ModelField[] GetRecordField()
        {
            ModelField[] fields = new ModelField[leaves.Where(x => x.IsChildLeave == false).Count()];
            int i = 0;
            ModelField recordfield = null;
            foreach (GetLeaveListAsPerEmployeeResult leave in leaves.Where(x => x.IsChildLeave == false).ToList())
            {
                recordfield = new ModelField("L" + leave.LeaveTypeId);
                fields[i] = recordfield;
                i++;
            }

            return fields;
        }

        Column[] GetColumns()
        {
            Column[] cols = new Column[leaves.Count];
            Column newCol = null;
            int i = 0;
            foreach (GetLeaveListAsPerEmployeeResult leave in leaves)
            {
                newCol = new Column();
                newCol.Sortable = false;
                newCol.MenuDisabled = true;
                newCol.Text = leave.Title;
                newCol.Align = Alignment.Center;
                newCol.DataIndex = "L" + leave.LeaveTypeId.ToString();
                //newCol.Width = new Unit(100);
                cols[i] = newCol;
                i++;

            }
            return cols;
        }
        #endregion

        internal bool LoadSelectedData(int leaveRequestId) //Dictionary<string, string> row)
        {
            LeaveRequest leaveRequest = LeaveAttendanceManager.GetLeaveRequestById(leaveRequestId);

            //if (leaveRequest.LeaveTypeId != 0 &&
            //    LeaveAttendanceManager.IsLeaveActiveForEmployee(leaveRequest.EmployeeId, leaveRequest.LeaveTypeId) == false)
            //    return false;

            hdEmployeeId.SetValue(leaveRequest.EmployeeId.ToString());

            if (leaveRequest.IsCancelRequest != null && leaveRequest.IsCancelRequest.Value)
            {
                wdoLeaveApproval.Title = "Cancel Leave Request";
            }
            else
            {
                wdoLeaveApproval.Title = "Leave Request";
            }

            this.lblStatus.Text = ((LeaveRequestStatusEnum)leaveRequest.Status).ToString();

            // storeEmployee.ClearFilter();

            if (this.IsHRView == false)
            {
                LoadCombo(leaveRequest.EmployeeId, leaveRequest.EEmployee.Name);
            }
            cboEmployee.Reset();
            this.cboEmployee.SetValue(leaveRequest.EmployeeId.ToString());


            if (leaveRequest.LeaveTypeId != 0)
            {
                if (new LeaveAttendanceManager().GetLeaveType(leaveRequest.LeaveTypeId).FreqOfAccrual == LeaveAccrue.COMPENSATORY)
                {
                    if (CommonManager.CompanySetting.WhichCompany != WhichCompany.NIBL)
                        X.AddScript("ulCompensatoryLeave.style.display = 'block';");

                    if (leaveRequest.CompensatorIsAddType != null)
                    {
                        if (leaveRequest.CompensatorIsAddType.Value)
                        {
                            compensatoryIsAdd.Checked = true;
                            compensatoryIsDeduct.Checked = false;
                        }
                        else
                        {
                            compensatoryIsAdd.Checked = false;
                            compensatoryIsDeduct.Checked = true;
                        }
                    }
                    cboLeaveType2.Hide();
                }
                else
                {
                    X.AddScript("ulCompensatoryLeave.style.display = 'none';");
                    cboLeaveType2.Show();
                }
            }
            else
            {
                X.AddScript("ulCompensatoryLeave.style.display = 'none';");
                cboLeaveType2.Show();
            }


            //this.cboLeaveType.SelectedItem.Value = row["LeaveTypeId"];
            //this.cboLeaveType.SelectByValue(row["LeaveTypeId"]);
            LoadEmployeeGrid(leaveRequest.EmployeeId, leaveRequestId);
            storeLeaveType.ClearFilter();

            if (leaveRequest.IsCancelRequest != null && leaveRequest.IsCancelRequest.Value)
            {
                int parentLeaveId = 0;
                if (LeaveAttendanceManager.IsChildLeave(leaveRequest.LeaveTypeId, ref parentLeaveId))
                {
                    this.cboLeaveType.SetValue(parentLeaveId.ToString());
                }
                else
                    this.cboLeaveType.SetValue(leaveRequest.LeaveTypeId.ToString());
            }
            else
                this.cboLeaveType.SetValue(leaveRequest.LeaveTypeId.ToString());



            if (leaveRequest.LeaveTypeId2 != null)
            {
                txtLeaveType1.Show();
                txtLeaveType2.Show();
                lblLeaveType1.Show();
                lblLeaveType2.Show();

                this.cboLeaveType2.SetValue(leaveRequest.LeaveTypeId2.ToString());

                if (leaveRequest.Days1 != null)
                    txtLeaveType1.Number = leaveRequest.Days1.Value;
                if (leaveRequest.Days2 != null)
                    txtLeaveType2.Number = leaveRequest.Days2.Value;
            }
            else
            {
                lblLeaveType1.Hide();
                lblLeaveType2.Hide();
                txtLeaveType1.Hide();
                txtLeaveType2.Hide();
                cboLeaveType2.ClearValue();
            }

            DisableControls();

            if (leaveRequest.IsSpecialCase != null && leaveRequest.IsSpecialCase.Value)
            {
                chkIsSpecialCase.Checked = leaveRequest.IsSpecialCase.Value;
                txtSpecialComment.Text = leaveRequest.SpecialComment;
            }
            chkIsSpecialCase.ReadOnly = true;
            txtSpecialComment.ReadOnly = true;


            this.dtFrom.SelectedDate = leaveRequest.FromDateEng.Value;
            this.lblTotalDaysValue.Text = leaveRequest.DaysOrHours.ToString();
            this.dtTo.SelectedDate = leaveRequest.ToDateEng.Value;

            this.chkHourlyLeave.Checked = leaveRequest.IsHour == null ? false : leaveRequest.IsHour.Value;

            this.hdLeaveRequestId.Value = leaveRequest.LeaveRequestId.ToString();

            // Substiture
            EEmployee employee = new EmployeeManager().GetById(leaveRequest.EmployeeId);
            //cmbSubstitute.Store[0].DataSource = new EmployeeManager().GetEmployeesByBranch(employee.BranchId.Value, -1)
            //    .Where(x => x.EmployeeId != leaveRequest.EmployeeId).ToList();
            //cmbSubstitute.Store[0].DataBind();
            if (leaveRequest.SubstituteEmployeeId != null)  
            {
                cmbSubstitute.GetStore().RemoveAll();
                cmbSubstitute.InsertItem(0, EmployeeManager.GetEmployeeName(leaveRequest.SubstituteEmployeeId.Value), leaveRequest.SubstituteEmployeeId.Value);
                cmbSubstitute.SelectedItem.Value = leaveRequest.SubstituteEmployeeId.Value.ToString();
                cmbSubstitute.SelectedItem.Text = EmployeeManager.GetEmployeeName(leaveRequest.SubstituteEmployeeId.Value);
                cmbSubstitute.UpdateSelectedItems();

                hdnEmployeeId.Text = leaveRequest.SubstituteEmployeeId.ToString();

                lblSubstituteInfo.Text = EmployeeManager.GetCurrentBranch(leaveRequest.SubstituteEmployeeId.Value) + " - " +
                    CommonManager.GetDesignationNameByEmpId(leaveRequest.SubstituteEmployeeId.Value);

            }
            else
            {
                hdnEmployeeId.Text = "";
            }


            if (this.chkHourlyLeave.Checked)
            {
                lblTotalDaysValue.Hide();
                lblTotalDaysText.Hide();

                // this.dtFrom.FieldLabel = "On";
                //this.cboHour.SelectedItem.Value = leaveRequest.DaysOrHours.ToString();
                this.cboHour.Text = leaveRequest.DaysOrHours.Value.ToString();
                //this.startTime.SelectedTime = TimeSpan.Parse(leaveRequest.StartTime);
                //this.endTime.SelectedTime = TimeSpan.Parse(leaveRequest.EndTime);

                try
                {
                    startTime.SelectedTime = DateTime.Parse(leaveRequest.StartTime).TimeOfDay;
                    endTime.SelectedTime = DateTime.Parse(leaveRequest.EndTime).TimeOfDay;
                    //X.Js.AddScript(string.Format("CompanyX.ctl27_startTime.setValue('{0}');", leaveRequest.StartTime.ToUpper()));
                    //X.Js.AddScript(string.Format("CompanyX.ctl27_endTime.setValue('{0}');", leaveRequest.EndTime.ToUpper()));
                }
                catch { }

            }
            else
            {
                //this.dtFrom.FieldLabel = "From";
            }

            SetReviewApplyToNames(leaveRequest);



            this.txtReason.Text = Server.HtmlDecode(leaveRequest.Reason);

            this.chkHourlyLeave.ReadOnly = true;
            // this.chkIsHalfDay.Disabled = true;
            this.txtComment.ReadOnly = false;
            this.cboLeaveType.ReadOnly = true;

            this.btnRecommend.Hide();
            btnRecommendForReview.Hide();
            this.btnCancel.Hide();
            this.btnAssign.Hide();
            this.btnApprove.Show();
            //this.btnReDraft.Show();
            this.btnDeny.Show();



            LeaveRequest request = LeaveAttendanceManager.GetLeaveRequestById(int.Parse(this.hdLeaveRequestId.Value.ToString()));
            this.txtComment.Text = request.Comment;
            if (request != null)
            {

                if (request.IsCancelRequest != null && request.IsCancelRequest.Value)
                {
                    btnPartialCancel.Hide();
                }
                else
                    btnPartialCancel.Show();

                if (CommonManager.CalculationConstant.CompanyHasHourlyLeave.Value)
                {
                    if (request.IsHour.Value)
                    {

                        chkHourlyLeave.Show();
                        chkIsHalfDay.Hide();
                        //dtTo.Hide();
                    }
                    else
                    {

                        chkHourlyLeave.Hide();
                        chkIsHalfDay.Hide();
                        dtTo.Show();

                    }
                }
                else
                {
                    chkIsHalfDay.Checked = request.IsHalfDayLeave != null ? request.IsHalfDayLeave.Value : false;
                    if (chkIsHalfDay.Checked)
                    {
                        chkHalfDayType.SetValue(string.IsNullOrEmpty(request.HalfDayType) ? "" : request.HalfDayType);
                        chkHalfDayType.Show();
                        dtTo.Hide();
                    }
                    else
                    {
                        chkHalfDayType.ClearValue();
                        chkHalfDayType.Hide();
                        dtTo.Show();
                    }
                    cboHour.Hide();
                    startTime.Hide();
                    endTime.Hide();
                }
            }


            if (LeaveRequestManager.IsEmployeeLeaveRequiredRecommendation(request.EmployeeId))
            {
                if (leaveRequest.Status == (int)LeaveRequestStatusEnum.Request)
                {
                    if (this.IsHRView
                            || LeaveRequestManager.CanRecommend(request.EmployeeId, SessionManager.CurrentLoggedInEmployeeId, PreDefindFlowType.Leave)
                            || request.RecommendEmployeeId == SessionManager.CurrentLoggedInEmployeeId
                            )
                    {
                        btnRecommend.Show();
                        if (CommonManager.Setting.LeaveAllowMultipleReview != null && CommonManager.Setting.LeaveAllowMultipleReview.Value
                            && this.IsHRView == false)
                            btnRecommendForReview.Show();
                        btnApprove.Hide();
                    }
                    else
                    {
                        btnRecommend.Hide();
                        btnRecommendForReview.Hide();
                        btnApprove.Hide();
                    }
                    btnDeny.Show();
                    //btnReDraft.Show();

                    // if same employee can recommend and approve the leave then only show Approve & hide recommend
                    if ((request.SupervisorEmployeeIdInCaseOfSelection == null &&
                            LeaveRequestManager.CanApprove(request.EmployeeId, SessionManager.CurrentLoggedInEmployeeId, PreDefindFlowType.Leave))
                        ||
                        (request.SupervisorEmployeeIdInCaseOfSelection != null &&
                            SessionManager.CurrentLoggedInEmployeeId == request.SupervisorEmployeeIdInCaseOfSelection)
                        )
                    {
                        btnRecommend.Hide();
                        btnRecommendForReview.Hide();
                        btnApprove.Show();
                    }

                }
                else if (leaveRequest.Status == (int)LeaveRequestStatusEnum.Recommended)
                {
                    if (this.IsHRView ||
                        CommonManager.CompanySetting.LeaveRequestToOnlyOneSupervisor == false ||
                        (
                            leaveRequest.SupervisorEmployeeIdInCaseOfSelection != null &&
                            leaveRequest.SupervisorEmployeeIdInCaseOfSelection.Value == SessionManager.CurrentLoggedInEmployeeId)
                       )
                    {
                        btnRecommend.Hide();
                        btnRecommendForReview.Hide();
                        btnApprove.Show();
                        btnDeny.Show();
                        //btnReDraft.Show();
                    }
                    else
                    {
                        btnRecommend.Hide();
                        btnRecommendForReview.Hide();
                        btnApprove.Hide();
                        btnDeny.Hide();
                        btnReDraft.Hide();
                    }

                }
                else
                {
                    btnRecommend.Hide();
                    btnRecommendForReview.Hide();
                    btnApprove.Hide();
                    btnDeny.Hide();
                    btnReDraft.Hide();
                }
            }
            else
            {
                btnRecommend.Hide();
                btnRecommendForReview.Hide();
                if (leaveRequest.Status == (int)LeaveRequestStatusEnum.Request
                    // just in case if in setting previously it had Recommend step but later removed case
                    || leaveRequest.Status == (int)LeaveRequestStatusEnum.Recommended)
                {
                    btnApprove.Show();
                    btnDeny.Show();
                    //btnReDraft.Show();
                }
                else
                {
                    btnApprove.Hide();
                    btnDeny.Hide();
                    btnReDraft.Hide();
                }
            }

            if (this.IsHRView)
            {
                if (leaveRequest.Status == (int)LeaveRequestStatusEnum.Request)
                {
                    btnApprove.Show();
                    //btnHRViewDelete.Show();
                }
                //else
                //    btnHRViewDelete.Hide();

                // Hide buttons for HR view for Mega
                //if (CommonManager.CompanySetting.WhichCompany == WhichCompany.Mega)
                //{
                //    btnRecommend.Hide();
                //    btnApprove.Hide();
                //    btnDeny.Hide();
                //    btnReDraft.Hide();
                //}
            }

            // Show Review/Apply To Level
            cmbApplyTo.Hide();
            cmbReviewTo.Hide();
            this.lblApplyTo.Show();
            this.lblReviewTo.Show();


            int diff = 0;
            // show Cancelled button if leave is approved & is with date range for change
            if (leaveRequest.Status == (int)LeaveRequestStatusEnum.Approved &&
                LeaveAttendanceManager.IsValidLeaveToChangeByApproval(leaveRequest.ToDateEng.Value, leaveRequest.FromDateEng.Value, ref diff))
            {
                this.cboLeaveType.ReadOnly = true;
                this.chkIsHalfDay.ReadOnly = true;
                this.chkHalfDayType.ReadOnly = true;
                this.dtFrom.ReadOnly = true;
                this.dtTo.ReadOnly = true;
                if (this.IsHRView)// || 
                {
                    this.btnActualCancel.Show();
                }
                else if (LeaveRequestManager.CanApprove(request.EmployeeId, SessionManager.CurrentLoggedInEmployeeId, PreDefindFlowType.Leave))
                {

                    btnActualCancel.Show();


                    // validate date lock exceeding case
                    if (CommonManager.Setting.DateLockForLeaveChangeFromEmployee != null && request.FromDateEng != null)
                    {
                        if (request.FromDateEng.Value.Date <= CommonManager.Setting.DateLockForLeaveChangeFromEmployee.Value.Date)
                        {
                            string lockDate = BLL.BaseBiz.GetAppropriateDate(CommonManager.Setting.DateLockForLeaveChangeFromEmployee.Value);
                            if (IsEnglish == false)
                            {
                                lockDate += " (" + CommonManager.Setting.DateLockForLeaveChangeFromEmployee.Value.ToString("yyyy-MMM-dd") + ")";
                            }
                            btnActualCancel.Hide();
                        }
                    }
                }
                else
                    this.btnActualCancel.Hide();

                this.btnCancel.Show();
            }
            else
            {
                btnActualCancel.Hide();
            }


            // for mega if approval employee then hide buttons like Redraft,Deny,Actual cancel
            if (SessionManager.CurrentLoggedInEmployeeId != 0 && CommonManager.CompanySetting.WhichCompany == WhichCompany.Mega)
            {
                btnActualCancel.Hide();
                btnDeny.Hide();
                btnReDraft.Hide();
            }

            if (string.IsNullOrEmpty(txtSpecialComment.Text.Trim()))
                txtSpecialComment.Hide();
            else
                txtSpecialComment.Show();

            return true;
        }

        private void SetReviewApplyToNames(LeaveRequest leaveRequest)
        {
            lblReviewTo.Text = "";
            lblApplyTo.Text = "";

            if (
                CommonManager.CompanySetting.LeaveRequestToOnlyOneSupervisor ||
                (CommonManager.Setting.LeaveRequestSettingUsingPreDefinedList != null && CommonManager.Setting.LeaveRequestSettingUsingPreDefinedList.Value)
                || (CommonManager.Setting.LeaveRequestSettngUsingOneToOne != null && CommonManager.Setting.LeaveRequestSettngUsingOneToOne.Value)
                )
            {
                if (leaveRequest.SupervisorEmployeeIdInCaseOfSelection != null)
                    lblApplyTo.Text = EmployeeManager.GetEmployeeById(leaveRequest.SupervisorEmployeeIdInCaseOfSelection.Value).Name;

                if (leaveRequest.RecommendEmployeeId != null)
                {
                    if (CommonManager.Setting.LeaveAllowMultipleReview != null && CommonManager.Setting.LeaveAllowMultipleReview.Value)
                    {
                        lblReviewTo.Text = LeaveRequestManager.GetLeaveRequestReviewerList(
                            leaveRequest.LeaveRequestId, leaveRequest.RecommendEmployeeId.Value);
                    }
                    else
                        lblReviewTo.Text = EmployeeManager.GetEmployeeById(leaveRequest.RecommendEmployeeId.Value).Name;
                }
            }
            else
            {
                string review = "", approval = "";
                LeaveRequestManager.GetApplyToForEmployee(leaveRequest.EmployeeId, ref review, ref approval);
                lblReviewTo.Text = review;
                lblApplyTo.Text = approval;
            }

            // change recommender or approval name
            if (leaveRequest.RecommendedBy != null && leaveRequest.RecommendedBy != 0 && leaveRequest.RecommendedBy != -1)
            {
                lblReviewTo.Text = EmployeeManager.GetEmployeeName(leaveRequest.RecommendedBy.Value);
            }
            if (leaveRequest.ApprovedBy != null && leaveRequest.ApprovedBy != 0 && leaveRequest.ApprovedBy != -1)
            {
                lblApplyTo.Text = EmployeeManager.GetEmployeeName(leaveRequest.ApprovedBy.Value);
            }

        }


        //protected void btnHRViewDelete_Click(object sender, DirectEventArgs e)
        //{
        //    int leaveRequestId = 0;

        //    if (int.TryParse(hdLeaveRequestId.Text.Trim(), out leaveRequestId) == false)
        //        return;

        //    if (leaveRequestId == 0)
        //        return;

        //    bool success= LeaveRequestManager.DeleteSelfLeave(leaveRequestId);

        //    if (success)
        //    {

        //        wdoLeaveApproval.Hide();
        //        X.MessageBox.Show(new MessageBoxConfig
        //        {
        //            Title = "Information",
        //            Message = "Leave has been deleted.",
        //            Buttons = MessageBox.Button.OK,
        //            Handler = "reloadGrids();",
        //            Icon = MessageBox.Icon.INFO
        //        });

        //        //LeaveRequestManager.NotifyThroughMessageAndEmail(LeaveAttendanceManager.GetLeaveRequestById(leaveRequestId));
        //        //LeaveRequest leaveRequest = LeaveAttendanceManager.GetLeaveRequestById(leaveRequestId);

        //        //NotifyThroughMessageAndEmail("", leaveRequest, leaveRequest.EmployeeId, leaveRequest.FromDateEng.ToString(), leaveRequest.ToDateEng.ToString(), "cancelled");


        //    }
        //    else
        //    {
        //        X.MessageBox.Show(new MessageBoxConfig
        //        {
        //            Title = "Warning",
        //            Message = "Could not delete the leave.",
        //            Buttons = MessageBox.Button.OK,
        //            //Handler = "reloadGrids();",
        //            Icon = MessageBox.Icon.WARNING
        //        });
        //    }
        //}

        protected void btnPartialCancel_Click(object sender, DirectEventArgs e)
        {
            int leaveRequestId = 0;

            if (int.TryParse(hdLeaveRequestId.Text.Trim(), out leaveRequestId) == false)
                return;

            if (leaveRequestId == 0)
                return;


            LeaveRequest request = LeaveAttendanceManager.GetLeaveRequestById(leaveRequestId);
            windowPartialCancelRequest.Center();
            windowPartialCancelRequest.Show();

            txtStartDateCancel.SelectedDate = request.FromDateEng.Value;
            txtEndDateCancel.SelectedDate = request.ToDateEng.Value;


        }
        protected void btnActualCancel_Click(object sender, DirectEventArgs e)
        {
            int leaveRequestId = 0;

            if (int.TryParse(hdLeaveRequestId.Text.Trim(), out leaveRequestId) == false)
                return;

            if (leaveRequestId == 0)
                return;

            ResponseStatus status = LeaveRequestManager.CancelledApprovedLeave(leaveRequestId, txtComment.Text.Trim(), IsHRView);

            if (status.IsSuccess == "true")
            {
                status.IsSuccess = "true";
                wdoLeaveApproval.Hide();
                X.MessageBox.Show(new MessageBoxConfig
                {
                    Title = "Information",
                    Message = "Leave has been cancelled.",
                    Buttons = MessageBox.Button.OK,
                    Handler = "reloadGrids();",
                    Icon = MessageBox.Icon.INFO
                });

                //LeaveRequestManager.NotifyThroughMessageAndEmail(LeaveAttendanceManager.GetLeaveRequestById(leaveRequestId));
                LeaveRequest leaveRequest = LeaveAttendanceManager.GetLeaveRequestById(leaveRequestId);

                LeaveAttendanceManager.NotifyThroughMessageAndEmail("", leaveRequest, leaveRequest.EmployeeId, leaveRequest.FromDateEng.ToString(), leaveRequest.ToDateEng.ToString(), "cancelled");


            }
            else
            {
                X.MessageBox.Show(new MessageBoxConfig
                {
                    Title = "Warning",
                    Message = status.ErrorMessage,
                    Buttons = MessageBox.Button.OK,
                    //Handler = "reloadGrids();",
                    Icon = MessageBox.Icon.WARNING
                });
            }
        }


        protected void btnForward_Click(object sender, DirectEventArgs e)
        {
            string message = "";
            LeaveRequest leaveRequest = ProcessUpdate((int)BLL.LeaveRequestStatusEnum.Recommended, ref message);

            if (!string.IsNullOrEmpty(message))
            {
                NewMessage.ShowWarningMessage(message);
                return;
            }


            //Call to validate for leave
            leaveRequest.FromDate = leaveRequest.FromDateEng.Value.ToShortDateString();
            leaveRequest.ToDate = leaveRequest.ToDateEng.Value.ToShortDateString();
            DAL.LeaveRequest testLeave = new LeaveRequest();
            ResponseStatus statusValidation = ValidateLeave(leaveRequest, testLeave);

            if (statusValidation.IsSuccessType == false)
            {
                NewMessage.ShowWarningMessage(statusValidation.ErrorMessage);
                return;
            }

            leaveRequest.Status = (int)LeaveRequestStatusEnum.Request;
            ResponseStatus status = LeaveAttendanceManager.ForwardLeaveRequest(leaveRequest,
                int.Parse(cmbReviewerList.SelectedItem.Value), txtComment.Text.Trim());

            if (status.IsSuccess == "true")
            {

                X.MessageBox.Show(
                       new MessageBoxConfig
                       {
                           Message = "Leave has been forwarded.",
                           Buttons = MessageBox.Button.OK,
                           Title = "Information",
                           Icon = MessageBox.Icon.INFO,
                           MinWidth = 300,
                           Handler = "reloadGridsAfterApproval();"
                       });
                //NotifyThroughMessageAndEmail(Resources.Messages.LeaveApproved.ToString(), leaveRequest, leaveRequest.EmployeeId,
                //    leaveRequest.FromDateEng.ToString(), leaveRequest.ToDateEng.ToString(), "recommended");


                LeaveAttendanceManager.NotifyThroughMessageAndEmail(leaveRequest.LeaveRequestId, leaveRequest.IsHour,
                leaveRequest.DaysOrHours.ToString(), leaveRequest.Reason, leaveRequest.FromDateEng.ToString(), leaveRequest.ToDateEng.ToString(), ((LeaveRequestStatusEnum)leaveRequest.Status).ToString()
                , int.Parse(cmbReviewerList.SelectedItem.Value), leaveRequest.RecommendEmployeeId, (int)LeaveRequestStatusEnum.Recommended);


                this.wdoLeaveApproval.Hide();
                this.windowReview.Hide();

            }
            else
            {
                BLL.Helper.ShowExtWarningMessage(status.ErrorMessage);
            }
        }

        protected void btnRecommend_Click(object sender, DirectEventArgs e)
        {
            string message = "";
            LeaveRequest leaveRequest = ProcessUpdate((int)BLL.LeaveRequestStatusEnum.Recommended, ref message);

            if (!string.IsNullOrEmpty(message))
            {
                X.MessageBox.Show(
                       new MessageBoxConfig
                       {
                           Message = message,
                           Buttons = MessageBox.Button.OK,
                           Title = "Warning",
                           Icon = MessageBox.Icon.WARNING,
                           MinWidth = 300
                       });
                return;
            }


            //Call to validate for leave
            leaveRequest.FromDate = leaveRequest.FromDateEng.Value.ToShortDateString();
            leaveRequest.ToDate = leaveRequest.ToDateEng.Value.ToShortDateString();
            DAL.LeaveRequest testLeave = new LeaveRequest();
            testLeave.IsSpecialCase = leaveRequest.IsSpecialCase;
            testLeave.SpecialComment = leaveRequest.SpecialComment;
            ResponseStatus statusValidation = ValidateLeave(leaveRequest, testLeave);
            if (statusValidation.IsSuccessType == false)
            {
                NewMessage.ShowWarningMessage(statusValidation.ErrorMessage);
                return;
            }


            ResponseStatus status = LeaveAttendanceManager.HandleLeaveRequest(leaveRequest, false);
            if (status.IsSuccessType == true)
            {

                X.MessageBox.Show(
                       new MessageBoxConfig
                       {
                           Message = "Leave Recommended.",
                           Buttons = MessageBox.Button.OK,
                           Title = "Information",
                           Icon = MessageBox.Icon.INFO,
                           MinWidth = 300,
                           Handler = "reloadGridsAfterApproval();"
                       });
                //NotifyThroughMessageAndEmail(Resources.Messages.LeaveApproved.ToString(), leaveRequest, leaveRequest.EmployeeId,
                //    leaveRequest.FromDateEng.ToString(), leaveRequest.ToDateEng.ToString(), "recommended");

                LeaveAttendanceManager.NotifyThroughMessageAndEmail(leaveRequest.LeaveRequestId, leaveRequest.IsHour,
                leaveRequest.DaysOrHours.ToString(), leaveRequest.Reason, leaveRequest.FromDateEng.ToString(), leaveRequest.ToDateEng.ToString(), ((LeaveRequestStatusEnum)leaveRequest.Status).ToString()
                , leaveRequest.SupervisorEmployeeIdInCaseOfSelection, leaveRequest.RecommendEmployeeId, null);


                this.wdoLeaveApproval.Hide();

            }
            else
            {
                BLL.Helper.ShowExtWarningMessage(status.ErrorMessage);
            }
        }

        protected ResponseStatus ValidateLeave(LeaveRequest entity, LeaveRequest leave)
        {
            ResponseStatus status = new ResponseStatus();
            // 1. Leave Type
            // If Unpaid Leave selected then LeaveTypeId will be 0
            if (entity.LeaveTypeId == -1)
                leave.LeaveTypeId = 0;
            else
                leave.LeaveTypeId = entity.LeaveTypeId;

            if (entity.LeaveTypeId2 != null && (entity.IsHalfDayLeave == null || entity.IsHalfDayLeave == false))
            {
                if (entity.LeaveTypeId2 == -1)
                    leave.LeaveTypeId2 = 0;
                else
                    leave.LeaveTypeId2 = entity.LeaveTypeId2;

                leave.Days1 = entity.Days1;
                leave.Days2 = entity.Days2;

                if (leave.LeaveTypeId == leave.LeaveTypeId2)
                {
                    status.ErrorMessage = "Please select diferent leaves in the leave type boxes.";
                    status.IsSuccess = "false";
                    return status;
                }
            }

            // Validate Days should exists if multiple leave selected
            if (entity.LeaveTypeId2 != null)
            {
                if (entity.Days1 == null || entity.Days1 == 0)
                {
                    status.ErrorMessage = "Please enter days for both the leaves or un-select second leave.";
                    status.IsSuccess = "false";
                    return status;
                }
                if (entity.Days2 == null || entity.Days2 == 0)
                {
                    status.ErrorMessage = "Please enter days for both the leaves or un-select second leave.";
                    status.IsSuccess = "false";
                    return status;
                }
            }

            DateTime date;

            if (string.IsNullOrEmpty(entity.FromDate) || DateTime.TryParse(entity.FromDate, out date) == false)
            {
                status.ErrorMessage = "Start date is not correct.";
                status.IsSuccess = "false";
                return status;
            }

            // 2. Half Day
            leave.IsHalfDayLeave = entity.IsHalfDayLeave;

            // 3. Set Dates
            leave.FromDateEng = new DateTime(date.Year, date.Month, date.Day);
            if (entity.IsHalfDayLeave == true)
            {
                leave.ToDateEng = leave.FromDateEng;
            }
            else
            {
                if (string.IsNullOrEmpty(entity.ToDate) || DateTime.TryParse(entity.ToDate, out date) == false)
                {
                    status.ErrorMessage = "End date is not correct.";
                    status.IsSuccess = "false";
                    return status;
                }
                leave.ToDateEng = new DateTime(date.Year, date.Month, date.Day);
            }


            // 4. Set Hour or Half Day 
            leave.IsHour = false;
            if (CommonManager.CalculationConstant.CompanyHasHourlyLeave.Value == false)
                leave.IsHour = false;

            leave.CompensatorIsAddType = entity.CompensatorIsAddType;

            if (leave.IsHour.Value && CommonManager.CalculationConstant.CompanyHasHourlyLeave.Value)
            {
                leave.DaysOrHours = 0;// double.Parse(entity.Url.Split(';')[0].ToString() == "" ? "0" : entity.Url.Split(';')[0].ToString());


                //if (!string.IsNullOrEmpty(leave.StartTime) && !string.IsNullOrEmpty(leave.EndTime))
                //{
                //    TimeSpan startTime = new TimeSpan(DateTime.Parse(leave.StartTime).Hour, DateTime.Parse(leave.StartTime).Minute, 0);
                //    TimeSpan endTime = new TimeSpan(DateTime.Parse(leave.EndTime).Hour, DateTime.Parse(leave.EndTime).Minute, 0);

                //    if (endTime <= startTime)
                //    {
                //        status.ErrorMessage = "End time should be greater than the start time.";
                //        status.IsSuccess = "false";
                //        return status;
                //    }

                //    if ((endTime - startTime).Hours != leave.DaysOrHours)
                //    {
                //        status.ErrorMessage = "Hours typed and start/end time does not match.";
                //        status.IsSuccess = "false";
                //        return status;
                //    }
                //}

            }
            else if (leave.IsHalfDayLeave.Value)
            {
                leave.ToDateEng = leave.FromDateEng;
                leave.DaysOrHours = (double)0.5;
                leave.LeaveTypeId2 = null;
                leave.Days2 = 0;
                leave.HalfDayType = entity.HalfDayType;
            }
            // If not Hour or Half day then it must be more than one day leave
            else
            {
                leave.DaysOrHours = (double)(leave.ToDateEng.Value - leave.FromDateEng.Value).Days + 1;

                if ((leave.IsHour == null || leave.IsHour == false)
                     && (leave.IsHalfDayLeave == null || leave.IsHalfDayLeave == false)
                     && leave.LeaveTypeId2 == null)
                {
                    // get actual leave days deducting Holiday if required
                    HolidayManager hmanager = new HolidayManager();
                    List<GetHolidaysForAttendenceResult> holiday = hmanager.GetMonthsHolidaysForAttendence(0, 0, SessionManager.CurrentCompanyId, 0, leave.FromDateEng, leave.ToDateEng, entity.EmployeeId).ToList();
                    bool onlyCountPublicHolidayAsLeaveWeeklySaturdayIsHoliday = false;
                    bool onlyCountWeeklySaturdayAsLeavePublicHolidayIsHoliday = false;
                    bool countHoliayAsLeave = LeaveAttendanceManager.GetLeaveRequestCountHolidayAsLeaveSetting(leave.LeaveTypeId, ref onlyCountPublicHolidayAsLeaveWeeklySaturdayIsHoliday,ref onlyCountWeeklySaturdayAsLeavePublicHolidayIsHoliday);
                    List<LeaveDetail> leaveEachDayList = LeaveAttendanceManager.
                        GetLeaveDetail(leave, new EmployeeManager().GetById(entity.EmployeeId),
                         countHoliayAsLeave, onlyCountPublicHolidayAsLeaveWeeklySaturdayIsHoliday,onlyCountWeeklySaturdayAsLeavePublicHolidayIsHoliday, holiday);

                    leave.DaysOrHours = leaveEachDayList.Count;
                }


            }

            // Validate total days = day1 + day2
            if (leave.LeaveTypeId2 != null)
            {
                if (leave.DaysOrHours != (leave.Days1 + leave.Days2))
                {
                    status.ErrorMessage = "The total leave days is not equal to the sum of two leave days.";
                    status.IsSuccess = "false";
                    return status;
                }
            }

            leave.FromDate = BLL.BaseBiz.GetAppropriateDate(leave.FromDateEng.Value);
            leave.ToDate = BLL.BaseBiz.GetAppropriateDate(leave.ToDateEng.Value);

            leave.Reason = entity.Reason.Trim();
            if (leave.Reason.Length > 500)
            {
                status.ErrorMessage = "Pleae write a shorter reason.";
                status.IsSuccess = "false";
                return status;
            }

            if (leave.Reason.Equals(string.Empty))
            {
                status.ErrorMessage = "Please write a reason for the leave.";
                status.IsSuccess = "false";
                return status;
            }

            double firstLeaveTaken;
            //TODO: remaining
            //if (CommonManager.CompanySetting.LeaveRequestNegativeLeaveReqBalanceNotAllowed)
            //{
            //    int daysDiff = 0; int remainingMonth = 0;

            //    if (payrollPeriod != null && payrollPeriod.Month != SessionManager.CurrentCompany.LeaveStartMonth.Value)
            //    {
            //        daysDiff = LeaveAttendanceManager.GetYearlyLeaveMiddleJoiningDays(payrollPeriod, ref remainingMonth);
            //    }


            //    //check for leave balance.
            //    GetLeaveListAsPerEmployeeResult leaveBalance =
            //        LeaveAttendanceManager.GetLeaveListAsPerEmployee(leave.LeaveTypeId, leave.EmployeeId, payrollPeriod.PayrollPeriodId, SessionManager.CurrentCompanyId, daysDiff, remainingMonth);

            //    firstLeaveTaken = leave.LeaveTypeId2 != null ? leave.Days1.Value : leave.DaysOrHours.Value;

            //    if ((leaveBalance.NewBalance - firstLeaveTaken) < 0)
            //    {
            //        status.ErrorMessage = "You do not have any leaves of this type left.";
            //        status.IsSuccess = "false";
            //        return status;
            //    }

            //    if (leave.LeaveTypeId2 != null)
            //    {
            //        //check for leave balance.
            //        leaveBalance =
            //            LeaveAttendanceManager.GetLeaveListAsPerEmployee(leave.LeaveTypeId2.Value, leave.EmployeeId, payrollPeriod.PayrollPeriodId, SessionManager.CurrentCompanyId, daysDiff, remainingMonth);
            //        if ((leaveBalance.NewBalance - (double)leave.Days2) < 0)
            //        {
            //            status.ErrorMessage = "You do not have any leaves of this type left.";
            //            status.IsSuccess = "false";
            //            return status;
            //        }
            //    }
            //}

            // Check Min/Max for each Leave Balance
            firstLeaveTaken = leave.LeaveTypeId2 != null ? leave.Days1.Value : leave.DaysOrHours.Value;
            string minMaxBalance = LeaveRequestManager.ValidateLeaveMinMaxBalance(leave.LeaveTypeId, firstLeaveTaken, leave);
            if (!string.IsNullOrEmpty(minMaxBalance))
            {
                status.ErrorMessage = minMaxBalance;
                status.IsSuccess = "false";
                return status;
            }
            if (leave.LeaveTypeId2 != null)
            {
                minMaxBalance = LeaveRequestManager.ValidateLeaveMinMaxBalance(leave.LeaveTypeId2.Value, leave.Days2.Value, leave);
                if (!string.IsNullOrEmpty(minMaxBalance))
                {
                    status.ErrorMessage = minMaxBalance;
                    status.IsSuccess = "false";
                    return status;
                }
            }

            string orderValidation = LeaveRequestManager.ValidateForLeaveOrder(leave);
            if (!string.IsNullOrEmpty(orderValidation))
            {
                status.ErrorMessage = orderValidation;
                status.IsSuccess = "false";
                return status;
            }



            return status;
        }

        protected void btnSavePartialCancel_Click(object sender, DirectEventArgs e)
        {

            LeaveRequest partial = new LeaveRequest();
            partial.LeaveRequestRef_Id = int.Parse(hdLeaveRequestId.Value.ToString());
            LeaveRequest leave = LeaveAttendanceManager.GetLeaveRequestById(partial.LeaveRequestRef_Id.Value);
            partial.IsCancelRequest = true;
            partial.EmployeeId = leave.EmployeeId;
            partial.LeaveTypeId = leave.LeaveTypeId;
            partial.FromDateEng = txtStartDateCancel.SelectedDate;
            partial.ToDateEng = txtEndDateCancel.SelectedDate;
            partial.FromDate = BLL.BaseBiz.GetAppropriateDate(partial.FromDateEng.Value);
            partial.ToDate = BLL.BaseBiz.GetAppropriateDate(partial.ToDateEng.Value);
            partial.DaysOrHours = (partial.ToDateEng.Value - partial.FromDateEng.Value).TotalDays + 1;
            partial.IsHour = false;
            partial.Reason = txtReasonCancel.Text.Trim();
            partial.Reason = HttpUtility.HtmlEncode(partial.Reason);
            partial.Comment = "";
            partial.IsHalfDayLeave = false;
            partial.CreatedOn = DateTime.Now;
            partial.ModifiedOn = partial.CreatedOn;
            partial.ApprovedOn = partial.CreatedOn;
            partial.Status = (int)LeaveRequestStatusEnum.Approved;
            partial.ApprovedBy = SessionManager.CurrentLoggedInEmployeeId;
            if (IsHRView)
                partial.ApprovedBy = SessionManager.User.UserMappedEmployeeId;

            partial.EditSequence = 1;

            if (leave.LeaveTypeId != -1
                 && LeaveAttendanceManager.GetLeaveTypeById(leave.LeaveTypeId).FreqOfAccrual == LeaveAccrue.COMPENSATORY)
            {
                NewMessage.ShowWarningMessage("Compensatory leave can not be partially cancelled.");
                return;
            }
            if (leave.IsHalfDayLeave != null && leave.IsHalfDayLeave.Value)
            {
                NewMessage.ShowWarningMessage("Half day leave can not be partially cancelled.");
                return;
            }

            if (partial.FromDateEng < leave.FromDateEng ||
               partial.ToDateEng > leave.ToDateEng)
            {
                NewMessage.ShowWarningMessage("From and To date should lie within the approved leave.");
                return;
            }

            if (partial.ToDateEng < partial.FromDateEng)
            {
                NewMessage.ShowWarningMessage("To date must be greater than From date.");
                return;
            }

            if (LeaveAttendanceManager.IsPartiallyCancelLeaveExists(leave.EmployeeId, partial.FromDateEng.Value, partial.ToDateEng.Value))
            {
                NewMessage.ShowWarningMessage("Leave cancellation already exists for this period.");
                return;
            }

            if (SessionManager.CurrentLoggedInEmployeeId == 0
               && (SessionManager.User.UserMappedEmployeeId == null || SessionManager.User.UserMappedEmployeeId == 0 || SessionManager.User.UserMappedEmployeeId == -1))
            {
                NewMessage.ShowWarningMessage("Please map the current user with Employee from User management before changing the leave.");
                return;
            }

            ResponseStatus status = LeaveAttendanceManager.HandleLeaveRequest(partial, true);

            if (status.IsSuccessType == true)
            {

                X.MessageBox.Show(
                       new MessageBoxConfig
                       {
                           Message = "Leave Cancellation Approved.",
                           Buttons = MessageBox.Button.OK,
                           Title = "Information",
                           Icon = MessageBox.Icon.INFO,
                           MinWidth = 300,
                           Handler = "reloadGridsAfterApproval();"
                       });


                this.wdoLeaveApproval.Hide();
                this.windowPartialCancelRequest.Hide();
            }
            else
            {
                BLL.Helper.ShowExtWarningMessage(status.ErrorMessage);
            }
        }

        protected void btnApprove_Click(object sender, DirectEventArgs e)
        {
            string message = "";
            LeaveRequest leaveRequest = ProcessUpdate((int)BLL.LeaveRequestStatusEnum.Approved, ref message);

            if (!string.IsNullOrEmpty(message))
            {
                X.MessageBox.Show(
                       new MessageBoxConfig
                       {
                           Message = message,
                           Buttons = MessageBox.Button.OK,
                           Title = "Warning",
                           Icon = MessageBox.Icon.WARNING,
                           MinWidth = 300
                       });
                return;
            }

            //Call to validate for leave
            leaveRequest.FromDate = leaveRequest.FromDateEng.Value.ToShortDateString();
            leaveRequest.ToDate = leaveRequest.ToDateEng.Value.ToShortDateString();
            DAL.LeaveRequest testLeave = new LeaveRequest();
            ResponseStatus statusValidation = ValidateLeave(leaveRequest, testLeave);
            if (statusValidation.IsSuccessType == false)
            {
                NewMessage.ShowWarningMessage(statusValidation.ErrorMessage);
                return;
            }



            ResponseStatus status = LeaveAttendanceManager.HandleLeaveRequest(leaveRequest, false);

            if (status.IsSuccessType == true)
            {
                if (leaveRequest.IsCancelRequest != null && leaveRequest.IsCancelRequest.Value)
                {
                    X.MessageBox.Show(
                           new MessageBoxConfig
                           {
                               Message = "Leave Cancellation has been Approved.",
                               Buttons = MessageBox.Button.OK,
                               Title = "Information",
                               Icon = MessageBox.Icon.INFO,
                               MinWidth = 300,
                               Handler = "reloadGridsAfterApproval();"
                           });
                }
                else
                {
                    X.MessageBox.Show(
                           new MessageBoxConfig
                           {
                               Message = "Leave has been Approved.",
                               Buttons = MessageBox.Button.OK,
                               Title = "Information",
                               Icon = MessageBox.Icon.INFO,
                               MinWidth = 300,
                               Handler = "reloadGridsAfterApproval();"
                           });
                }

                LeaveAttendanceManager.NotifyThroughMessageAndEmail(Resources.Messages.LeaveApproved.ToString(), leaveRequest, leaveRequest.EmployeeId,
                    leaveRequest.FromDateEng.ToString(), leaveRequest.ToDateEng.ToString(), "approved");
                this.wdoLeaveApproval.Hide();

            }
            else
            {
                BLL.Helper.ShowExtWarningMessage(status.ErrorMessage);
            }
        }

        protected void btnDeny_Click(object sender, DirectEventArgs e)
        {
            string message = "";

            int requestId = int.Parse(hdLeaveRequestId.Value.ToString());


            Status status = LeaveAttendanceManager.ProcessLeaveLikeForDeny(requestId, LeaveRequestStatusEnum.Denied, txtComment.Text.Trim());

            LeaveRequest leaveRequest = LeaveAttendanceManager.GetLeaveRequestById(requestId);


            if (status.IsSuccess == true)
            {

                X.MessageBox.Show(
                       new MessageBoxConfig
                       {
                           Message = "Leave Denied.",
                           Buttons = MessageBox.Button.OK,
                           Title = "Information",
                           Icon = MessageBox.Icon.INFO,
                           MinWidth = 300,
                           Handler = "reloadGridsAfterApproval();"
                       });
                LeaveAttendanceManager.NotifyThroughMessageAndEmail(Resources.Messages.LeaveDeny, leaveRequest, leaveRequest.EmployeeId, leaveRequest.FromDateEng.ToString(), leaveRequest.ToDateEng.ToString(), "denied");
                this.wdoLeaveApproval.Hide();

            }
            else
            {
                BLL.Helper.ShowExtWarningMessage(status.ErrorMessage);
            }
        }

        protected void btnReDraft_Click(object sender, DirectEventArgs e)
        {
            string message = "";
            LeaveRequest leaveRequest = ProcessUpdate((int)BLL.LeaveRequestStatusEnum.ReDraft, ref message);

            if (!string.IsNullOrEmpty(message))
            {
                X.MessageBox.Show(
                       new MessageBoxConfig
                       {
                           Message = message,
                           Buttons = MessageBox.Button.OK,
                           Title = "Warning",
                           Icon = MessageBox.Icon.WARNING,
                           MinWidth = 300
                       });
                return;
            }
            ResponseStatus status = LeaveAttendanceManager.HandleLeaveRequest(leaveRequest, false);

            if (status.IsSuccessType == true)
            {
                X.MessageBox.Show(
                       new MessageBoxConfig
                       {
                           Message = "Leave is re-dreft.",
                           Buttons = MessageBox.Button.OK,
                           Title = "Information",
                           Icon = MessageBox.Icon.INFO,
                           MinWidth = 300,
                           Handler = "reloadGridsAfterApproval();"
                       });
                LeaveAttendanceManager.NotifyThroughMessageAndEmail(Resources.Messages.LeaveReDraft, leaveRequest, leaveRequest.EmployeeId, leaveRequest.FromDateEng.ToString(), leaveRequest.ToDateEng.ToString(), "re-draft");
                this.wdoLeaveApproval.Hide();

            }
            else
            {
                BLL.Helper.ShowExtWarningMessage(status.ErrorMessage);
            }
        }

        ///// <summary>
        ///// Retrieve LeaveRequest object from th
        ///// </summary>
        //public ResponseStatus ProcessUpdate(LeaveRequest leave)
        //{
        //    bool HasCompanyHasHourlyLeave = CommonManager.CalculationConstant.CompanyHasHourlyLeave.Value;
        //    ResponseStatus status = new ResponseStatus();
        //    Event lq = new Event();
        //    lq.CalendarId = int.Parse(cboLeaveType.SelectedItem.Value);

        //    if (!string.IsNullOrEmpty(dtFrom.Text))
        //    {
        //        DateTime start;
        //        if (DateTime.TryParse(dtFrom.Text.Trim(), out start) && start != DateTime.MinValue)
        //            lq.StartDate = start;
        //        else
        //            lq.StartDate = null;
        //    }
        //    else
        //        lq.StartDate = null;

        //    if (!string.IsNullOrEmpty(dtTo.Text))
        //    {
        //        DateTime end;
        //        if (DateTime.TryParse(dtTo.Text.Trim(), out end) && end != DateTime.MinValue)
        //            lq.EndDate = end;
        //        else
        //            lq.EndDate = null;
        //    }
        //    else
        //        lq.EndDate = null;

        //    lq.Title = txtReason.Text.Trim();
        //    // Is Hour
        //    lq.Reminder = chkHourlyLeave.Checked.ToString();
        //    // Is Half Day
        //    lq.Location = chkIsHalfDay.Checked.ToString();

        //    if (chkIsHalfDay.Checked == false && chkHourlyLeave.Checked == false && lq.EndDate == null)
        //    {
        //        status.IsSuccess = "false";
        //        status.ErrorMessage = "End date is required.";
        //        return status;
        //    }

        //    int employeeId = int.Parse(cboEmployee.SelectedItem.Value);
        //    status = LeaveRequestManager.GetLeaveRequestFromCalendarEntity(lq, leave, true, employeeId);
        //    if (status.IsSuccess == "false")
        //        return status;

        //    leave.Comment = txtComment.Text.Trim();
        //    leave.Status = (int)BLL.LeaveRequestStatusEnum.Approved;

        //    leave.ApprovedBy = SessionManager.CurrentLoggedInEmployeeId;
        //    leave.ApprovedOn = System.DateTime.Now;


        //    return status;
        //}


        public LeaveRequest ProcessUpdate(int status, ref string message)
        {
            message = "";

            LeaveRequest lq = new LeaveRequest();
            lq.EmployeeId = int.Parse(cboEmployee.SelectedItem.Value);
            lq.LeaveTypeId = int.Parse(cboLeaveType.SelectedItem.Value);

            if (cboLeaveType2.SelectedItem != null && cboLeaveType2.SelectedItem.Value != null)
            {
                lq.LeaveTypeId2 = int.Parse(cboLeaveType2.SelectedItem.Value);
                lq.Days1 = txtLeaveType1.Number;
                lq.Days2 = txtLeaveType2.Number;
            }

            if (hdLeaveRequestId.Value.ToString() != "")
                lq.LeaveRequestId = int.Parse(hdLeaveRequestId.Value.ToString()); //LeaveRequestId;


            LeaveRequest request = LeaveAttendanceManager.GetLeaveRequestById(lq.LeaveRequestId);


            #region "Date Validation"

            if (!string.IsNullOrEmpty(dtFrom.Text))
            {
                DateTime start;
                if (DateTime.TryParse(dtFrom.Text.Trim(), out start) && start != DateTime.MinValue)
                    lq.FromDateEng = start;
                else
                {
                    message = "Invalid From date.";
                    return null;
                }
            }
            else
            {
                message = "From date is required.";
                return null;
            }

            lq.IsSpecialCase = chkIsSpecialCase.Checked;
            lq.SpecialComment = txtSpecialComment.Text.Trim();

            lq.IsHalfDayLeave = chkIsHalfDay.Checked;

            if (lq.IsHalfDayLeave.Value)
            {
                lq.ToDateEng = lq.FromDateEng;
                lq.HalfDayType = chkHalfDayType.SelectedItem.Value;
            }
            else
            //if (chkHourlyLeave.Checked == false)
            {
                if (!string.IsNullOrEmpty(dtTo.Text))
                {
                    DateTime end;
                    if (DateTime.TryParse(dtTo.Text.Trim(), out end) && end != DateTime.MinValue)
                        lq.ToDateEng = end;
                    else
                    {
                        message = "Invalid To date.";
                        return null;
                    }
                }
                else
                {
                    message = "To date is required.";
                    return null;
                }
            }

            //if (chkHourlyLeave.Checked)
            //{
            //    lq.ToDate = lq.FromDate;
            //    lq.ToDateEng = lq.FromDateEng;
            //}


            if ((lq.ToDateEng.Value - lq.FromDateEng.Value).Days < 0)
            {
                message = "End date must be greater then or equal to the start date.";
                return null;
            }


            lq.Reason = HttpUtility.HtmlEncode(txtReason.Text.Trim());
            if (lq.Reason.Equals(string.Empty))
            {
                message = "Reason can not be empty.";
                return null;
            }

            if (request == null || (request != null && (request.IsCancelRequest == null || request.IsCancelRequest == false)))
            {
                if (LeaveAttendanceManager.IsLeaveRequestMadeOnDay(lq))
                {
                    message = "Leave request already exists on this date(s).";
                    return null;
                }
            }

            #endregion

            if (chkHourlyLeave.Checked)
            {
                lq.IsHour = true;
                lq.DaysOrHours = double.Parse(cboHour.Text == "" ? "0" : cboHour.Text);
                if (startTime.SelectedTime != TimeSpan.MinValue)
                    lq.StartTime = DateTime.Parse(startTime.Text.Trim()).ToString("hh:mm tt");
                if (endTime.SelectedTime != TimeSpan.MinValue)
                    lq.EndTime = DateTime.Parse(endTime.Text.Trim()).ToString("hh:mm tt");
            }
            else
            {
                if (lq.IsHalfDayLeave.Value)
                    lq.DaysOrHours = 0.5;
                else
                {

                    lq.DaysOrHours = (lq.ToDateEng.Value - lq.FromDateEng.Value).Days + 1;
                }


                lq.IsHour = false;

            }

            int currentEmployeeId = 0; string currentEmployeeUserName = "";
            UserManager.SetCurrentEmployeeAndUser(ref currentEmployeeId, ref currentEmployeeUserName);

            lq.FromDate = LeaveAttendanceManager.GetAppropriateDate(lq.FromDateEng.Value);
            lq.ToDate = LeaveAttendanceManager.GetAppropriateDate(lq.ToDateEng.Value);
            lq.CreatedOn = BLL.BaseBiz.GetCurrentDateAndTime();
            lq.CreatedBy = currentEmployeeId;
            lq.ModifiedOn = BLL.BaseBiz.GetCurrentDateAndTime();

            lq.EditSequence = 1;

            lq.Status = status;

            //if (cmbSubstitute.SelectedItem != null && cmbSubstitute.SelectedItem.Value != null)
            ////    lq.SubstituteEmployeeId = int.Parse(cmbSubstitute.SelectedItem.Value);

            if (!string.IsNullOrEmpty(cmbSubstitute.Text))
            {
                if (!string.IsNullOrEmpty(hdnEmployeeId.Text))
                    lq.SubstituteEmployeeId = int.Parse(hdnEmployeeId.Text);
            }

            //if (status == (int)LeaveRequestStatusEnum.Approved)
            //{

            //}
            if (status == (int)LeaveRequestStatusEnum.Recommended)
            {
                lq.RecommendedComment = HttpUtility.HtmlEncode(txtComment.Text.Trim());
                lq.RecommendedOn = BLL.BaseBiz.GetCurrentDateAndTime();
                lq.RecommendedBy = currentEmployeeId;
            }
            // set Approve info for Approve,Deny like status except Recommend
            else
            {
                lq.Comment = HttpUtility.HtmlEncode(txtComment.Text.Trim());
                lq.ApprovedOn = BLL.BaseBiz.GetCurrentDateAndTime();
                lq.ApprovedBy = currentEmployeeId;
            }

            if (lq.LeaveTypeId != 0)
            {
                LLeaveType leaveType = LeaveAttendanceManager.GetLeaveTypeById(lq.LeaveTypeId);

                if (leaveType.FreqOfAccrual == LeaveAccrue.COMPENSATORY)
                {
                    if (compensatoryIsAdd.Checked)
                        lq.CompensatorIsAddType = true;
                    else if (compensatoryIsDeduct.Checked)
                        lq.CompensatorIsAddType = false;
                }

                if (leaveType.IsParentGroupLeave != null && leaveType.IsParentGroupLeave.Value)
                {


                    //if (cmbChildLeaves.SelectedItem != null && cmbChildLeaves.SelectedItem.Value != null)
                    //{
                    //    lq.ChildLeaveTypeId = int.Parse(cmbChildLeaves.SelectedItem.Value);
                    //}
                    //else
                    //{
                    //    message = "Child leave must be selected for Group type.";
                    //    return lq;
                    //}
                }
            }





            return lq;
        }




    }

}