﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL.Base;
using BLL;
using Utils.Helper;

namespace Web.Employee.UserControls
{
    public partial class TimeAttendCommentCtrl : BaseUserControl
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialize();

                if (SessionManager.CurrentLoggedInEmployeeId == 0)
                {
                    
                    cmbEmpSearch.Show();
                }
            }
        }

        private void Initialize()
        {
            X.Js.Call("searchListEL");
        }
        protected void btnView_Click(object sender, DirectEventArgs e)
        {
        }

        public void btnExport_Click(object sender, EventArgs e)
        {
            DateTime? fromDate = null, toDate = null;
            int employeeId = -1, managerId = -1;

            if (txtFromDateFilter.SelectedDate != null && txtFromDateFilter.SelectedDate != new DateTime())
                fromDate = txtFromDateFilter.SelectedDate;

            if (txtToDateFilter.SelectedDate != null && txtToDateFilter.SelectedDate != new DateTime())
                toDate = txtToDateFilter.SelectedDate;

            if (fromDate != new DateTime() && toDate == new DateTime())
            {
                NewMessage.ShowNormalMessage("To date is required for date filter.");
                txtToDateFilter.Focus();
                return;
            }

            if (toDate != new DateTime() && fromDate == new DateTime())
            {
                NewMessage.ShowNormalMessage("From date is required for date filter.");
                txtFromDateFilter.Focus();
                return;
            }

            if (cmbEmpSearch.SelectedItem != null && cmbEmpSearch.SelectedItem.Value != null)
                employeeId = int.Parse(cmbEmpSearch.SelectedItem.Value);

            int totalRecords = 0;

            if (SessionManager.CurrentLoggedInEmployeeId != 0)
                managerId = SessionManager.CurrentLoggedInEmployeeId;

            List<GetAttendanceCommentRequestsForApprovalResult> list = AttendanceManager.GetAttendanceCommentRequestForApproval(employeeId, managerId, fromDate, toDate, 0, 999999, out totalRecords, int.Parse(cmbStatus.SelectedItem.Value)
                ,hdnSortBy.Text);
            

            List<string> hiddenList = new List<string>();
            hiddenList.Add("LineID");
            hiddenList.Add("Status");
            hiddenList.Add("TotalRows");
            hiddenList.Add("RowNumber");

            Dictionary<string, string> renameList = new Dictionary<string, string>();

            renameList.Add("EmpID", "EIN");
            renameList.Add("InTime", "In Time");
            renameList.Add("OutTime", "Out Time");
            renameList.Add("InComment", "In Comment");
            renameList.Add("OutComment", "Out Comment");
            renameList.Add("AttendanceDate", "Date");
            renameList.Add("StatusText", "Status");

            Bll.ExcelHelper.ExportToExcel("Attendance Comment List", list,
                hiddenList,
            new List<String>() { },
            renameList,
            new List<string>() { }, new List<string> {}
            , new List<string>() {}
            , new Dictionary<string, string>() { { "Attendance Comment Request List","" } }
            , new List<string> { "EmpID", "Name", "AttendanceDate", "InTime", "InComment", "OutTime","OutComment","StatusText" });


        }

        protected void btnApprove_Click(object sender, DirectEventArgs e)
        {
            Guid requestId = new Guid(hdnRequestId.Text);

            List<AttendanceEmpComment> list = new List<AttendanceEmpComment>();
            AttendanceEmpComment dbObj = AttendanceManager.GetTimeCommentRequestByRequestId(requestId);
            list.Add(dbObj);

            Status status = AttendanceManager.ApproveAttCommentRequest(list);
            if (status.IsSuccess)
            {
                NewMessage.ShowNormalMessage("Request approved successfully.");
                CheckboxSelectionModel1.ClearSelection();
                SendApproveMail(list);
                X.Js.Call("searchListEL");
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }

        }

        protected void btnReject_Click(object sender, DirectEventArgs e)
        {
            Guid requestId = new Guid(hdnRequestId.Text);
            
            Status status = AttendanceManager.RevertAttCommentRequest(requestId);
           AttendanceEmpComment _AttendanceEmpComment =  AttendanceManager.GetTimeCommentRequestByRequestId(requestId);
            if (status.IsSuccess)
            {
                NewMessage.ShowNormalMessage("Request rejected successfully.");
               // CheckboxSelectionModel1.ClearSelection();
                SendRejectMail(_AttendanceEmpComment);
                X.Js.Call("searchListEL");
            }
            else
                NewMessage.ShowWarningMessage(status.ErrorMessage);
        }

        protected void btnApproveAll_Click(object sender, DirectEventArgs e)
        {
            string gridItemsJSON = e.ExtraParams["gridItems"];
            List<AttendanceEmpComment> list = JSON.Deserialize<List<AttendanceEmpComment>>(gridItemsJSON);
          //  List<EmpAttTimeCommentRequestList> dbList = new List<EmpAttTimeCommentRequestList>();

            if (list.Count == 0)
                return;

            //foreach (EmpAttTimeCommentRequestList item in list)
            //{

            //    AttendanceEmpComment dbObj = AttendanceManager.GetTimeCommentRequestByRequestId(item.LineID);
            //    dbList.Add(dbObj);
            //}

            Status status = AttendanceManager.ApproveAttCommentRequest(list);
            if (status.IsSuccess)
            {
                NewMessage.ShowNormalMessage("Requests approved successfully.");
                CheckboxSelectionModel1.ClearSelection();
                SendApproveMail(list);
                X.Js.Call("searchListEL");
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }

        }

        protected void SendApproveMail(List<AttendanceEmpComment> AttendanceEmpCommentList)
        {

            SMTPHelper mailSender = new SMTPHelper();
            EmailContent dbMailContent = CommonManager.GetMailContent((int)EmailContentType.TimeAttendanceCommentApprovedOrReject);

            if (dbMailContent != null)
            {
                foreach (AttendanceEmpComment objAttendanceEmpComment in AttendanceEmpCommentList)
                {
                    string subject = dbMailContent.Subject.Replace("#Status#", "Approved").Replace("#Date#", objAttendanceEmpComment.AttendanceDate.Value.ToShortDateString());
                    string body = dbMailContent.Body.Replace("#Status#", "Approved").Replace("#Date#", objAttendanceEmpComment.AttendanceDate.Value.ToShortDateString());
                    body = body.Replace("#Reason#", "");

                    EEmployee _Employeee = EmployeeManager.GetEmployeeById(objAttendanceEmpComment.EmpID.Value);
                    if (string.IsNullOrEmpty(_Employeee.EAddresses[0].CIEmail))
                    {
                        continue;
                    }

                    bool isSendSuccess = SMTPHelper.SendAsyncMail(_Employeee.EAddresses[0].CIEmail, body, subject, "");
                }
            }
            
        }


        protected void SendRejectMail(AttendanceEmpComment objAttendanceEmpComment)
        {

            SMTPHelper mailSender = new SMTPHelper();
            EmailContent dbMailContent = CommonManager.GetMailContent((int)EmailContentType.TimeAttendanceCommentApprovedOrReject);
            if (dbMailContent != null)
            {
                //foreach (AttendanceEmpComment objAttendanceEmpComment in AttendanceEmpCommentList)
                //{
                string subject = dbMailContent.Subject.Replace("#Status#", "Rejected").Replace("#Date#", objAttendanceEmpComment.AttendanceDate.Value.ToShortDateString());
                string body = dbMailContent.Body.Replace("#Status#", "Rejected").Replace("#Date#", objAttendanceEmpComment.AttendanceDate.Value.ToShortDateString());
                body = body.Replace("#Reason#", "");

                EEmployee _Employeee = EmployeeManager.GetEmployeeById(objAttendanceEmpComment.EmpID.Value);
                if (string.IsNullOrEmpty(_Employeee.EAddresses[0].CIEmail))
                {
                    return;
                }

                bool isSendSuccess = SMTPHelper.SendAsyncMail(_Employeee.EAddresses[0].CIEmail, body, subject, "");
                // }
            }
        }

        protected void Store_ReadData(object sender, StoreReadDataEventArgs e)
        {
            DateTime? fromDate = null, toDate = null;
            int employeeId = -1, managerId = -1;

            if (txtFromDateFilter.SelectedDate != null && txtFromDateFilter.SelectedDate != new DateTime())
                fromDate = txtFromDateFilter.SelectedDate;

            if (txtToDateFilter.SelectedDate != null && txtToDateFilter.SelectedDate != new DateTime())
                toDate = txtToDateFilter.SelectedDate;

            if (fromDate != null && toDate == null)
            {
                NewMessage.ShowNormalMessage("To date is required for date filter.");
                txtToDateFilter.Focus();
                return;
            }

            if (toDate != null && fromDate == null)
            {
                NewMessage.ShowNormalMessage("From date is required for date filter.");
                txtFromDateFilter.Focus();
                return;
            }

            if (cmbEmpSearch.SelectedItem != null && cmbEmpSearch.SelectedItem.Value != null)
                employeeId = int.Parse(cmbEmpSearch.SelectedItem.Value);

            int totalRecords = 0;

            hdnSortBy.Text = (e.Sort[0].Property + " " + e.Sort[0].Direction).ToLower();

            int currentPage = e.Start / int.Parse(cmbPageSize.SelectedItem.Value);

            if (SessionManager.CurrentLoggedInEmployeeId != 0)
                managerId = SessionManager.CurrentLoggedInEmployeeId;

            List<GetAttendanceCommentRequestsForApprovalResult> list = AttendanceManager.GetAttendanceCommentRequestForApproval(employeeId, managerId, fromDate, toDate, currentPage, int.Parse(cmbPageSize.SelectedItem.Value), out totalRecords, int.Parse(cmbStatus.SelectedItem.Value)
                , hdnSortBy.Text);
            e.Total = totalRecords;
            Store1.DataSource = list;
            Store1.DataBind();


            


        }

    }
}