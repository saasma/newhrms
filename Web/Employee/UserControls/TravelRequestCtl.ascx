﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TravelRequestCtl.ascx.cs"
    Inherits="Web.Employee.UserControls.TravelRequestCtl" %>
<ext:Hidden runat="server" ID="hiddenValue" />
<style>
    .commentBlock
    {
        font-size: 13px;
        margin-top: 5px;
    }
    .nameBlock
    {
        font-size: 13px;
        color: #3399FF;
        font-style: italic;
    }
</style>
<script type="text/javascript">
var afterEdit = function()
        {
            var records = <%=gridAllowances.ClientID %>.getStore();
             var i = 0,
                length = records.data.length,
                total = 0,
                record;

            for (; i < length; ++i) {
                record = records.data.items[i];
                total += record.data.Quantity * record.data.Rate;
            }

            total = getFormattedAmount(total);
            <%=dispTotal.ClientID %>.setValue(total);
        }

        var beforeEdit = function(e1, e) {
            
            if(e.field=="Rate" && e.record.data.IsEditable!="true")
            {
                e.cancel = true;
                return;
            }
        
    };

    
</script>
<ext:Window ID="Window1" runat="server" AutoScroll="true" Title="Forward Travel Allowance"
    Icon="Application" Width="1020" Height="620" BodyPadding="5" Hidden="true" Modal="true">
    <Content>
        <div style="width: 350px; float: left; margin-top: 10px; margin-left: 10px;">
            <table id="albums1" width="400px;">
                <tr>
                    <td>
                        <table>
                            <tr>
                                <td valign="top">
                                    <ext:Image ID="image" ImageUrl="~/images/sample.jpg" runat="server" Width="120px"
                                        Height="120px" />
                                </td>
                                <td valign="top" style="padding-left: 10px;">
                                    <table>
                                        <tr>
                                            <td>
                                                <ext:Label ID="lblName" StyleSpec="font-weight:bold;color:#0099FF;font-size:16px;"
                                                    runat="server">
                                                </ext:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <ext:Label ID="lblBranch" runat="server">
                                                </ext:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <ext:Label ID="lblDepartment" Width="235px"  runat="server">
                                                </ext:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <ext:Label ID="lblDesignation" Width="235px"  runat="server">
                                                </ext:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <%--<ext:TextArea LabelSeparator="" ID="txtCommentSection" Height="25" runat="server">
                            </ext:TextArea>--%>
                        <div style="margin-top: 45px;">
                            <ext:Label runat="server" ID="lblCommentSection">
                            </ext:Label>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <ext:TextArea LabelSeparator="" ID="txtHRComment" FieldLabel="Your Comment *" LabelAlign="Top"
                            runat="server" Width="350" FieldStyle="height:30px!important">
                        </ext:TextArea>
                    </td>
                </tr>
            </table>
        </div>
        <div style="width: 600px; float: left; padding-left: 25px;">
            <div style="float: right; width: 90px; margin-top: 10px;">
                <ext:LinkButton ID="btnPrint" runat="server" Text="Print" OnClick="btnPrint_Click"
                    Hidden="true" Icon="Printer" AutoPostBack="true">
                </ext:LinkButton>
            </div>
            <table id="albums" cellspacing="0px">
                <tr style='display: none' id="rowRefNumber" runat="server">
                    <td colspan="1" style="margin-right: 25px; margin-left: 25px;">
                        <ext:TextField ID="txtRefNumber" Disabled="true" Width="200" runat="server" FieldLabel="Ref #"
                            LabelSeparator=" " LabelAlign="Top">
                        </ext:TextField>
                    </td>
                </tr>
                <tr style='display: none' id="rowDomestic" runat="server">
                    <td>
                        <table>
                            <tr>
                                <td style="margin-right: 25px; margin-left: 25px;">
                                    <h4 style='margin-bottom:2px;    font-size: 15px;'>
                                        Places to Travel</h4>
                                    <ext:Radio Name="domestic" runat="server" Checked="true" ID="radioDomestic" BoxLabel="Domestic" />
                                </td>
                                <td  style="padding-left: 15px;padding-top:36px;">
                                    <ext:Radio Name="domestic" runat="server" ID="radioInternaltional" BoxLabel="International" />
                                </td>
                            </tr>
                        </table>
                    </td>

                   
                </tr>
                <tr style='display: none' id="rowEditableLocation" runat="server">
                    <td colspan="5">
                        <ext:GridPanel Width="560" StyleSpec="margin-bottom:10px" ID="gridLocations" runat="server" Cls="itemgrid">
                            <Store>
                                <ext:Store ID="store4" runat="server">
                                    <Model>
                                        <ext:Model ID="Model2" Name="SettingModel" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="LocationPlace" Type="String" />
                                                <ext:ModelField Name="Country" Type="String" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                            <ColumnModel>
                                <Columns>
                                    <ext:RowNumbererColumn ID="RowNumbererColumn1" DataIndex="SequenceNo" runat="server"
                                        Width="35" />
                                    <ext:Column ID="Column3" Flex="1" runat="server" Text="Place" Sortable="false" MenuDisabled="true"
                                        Width="200" DataIndex="LocationPlace" Border="false">
                                    </ext:Column>
                                    <ext:Column ID="colTravelOrder" runat="server" Text="Country" Sortable="false" MenuDisabled="true"
                                        Width="150" DataIndex="Country" Border="false">
                                    </ext:Column>
                                </Columns>
                            </ColumnModel>
                        </ext:GridPanel>
                    </td>
                </tr>
                <tr>
                    <td colspan="1">
                        <ext:TextField LabelSeparator="" ID="txtPlaceToTravel" Width="200" runat="server"
                            FieldLabel="Place to Travel" LabelAlign="Top">
                        </ext:TextField>
                    </td>
                    <td colspan="1">
                        <ext:ComboBox LabelSeparator="" StyleSpec="margin-left:10px" Disabled="true" ID="cmbCountry"
                            runat="server" Width="200" FieldLabel="Country" LabelAlign="Top" QueryMode="Local"
                            DisplayField="CountryName" ValueField="CountryId">
                            <Store>
                                <ext:Store ID="store2" runat="server">
                                    <Fields>
                                        <ext:ModelField Name="CountryId" Type="String" />
                                        <ext:ModelField Name="CountryName" />
                                    </Fields>
                                </ext:Store>
                            </Store>
                            <DirectEvents>
                                <Change OnEvent="Country_Change">
                                </Change>
                            </DirectEvents>
                        </ext:ComboBox>
                    </td>
                </tr>
                <tr style="padding-bottom: 1em;">
                    <td colspan="5">
                        <ext:TextArea Height="70" LabelSeparator="" ID="txtPurposeOfTravel" Width="560" runat="server"
                            FieldLabel="Purpose of Travel" LabelAlign="Top">
                        </ext:TextArea>
                    </td>
                </tr>
                <tr style="padding-bottom: 1em;">
                    <td colspan="1">
                        <ext:DateField FieldLabel="Travelling from" Width="150" ID="txtFromDate" runat="server"
                            EmptyDate="" LabelAlign="Top" LabelSeparator="">
                            <DirectEvents>
                                <Change OnEvent="CheckInOutSelectChange">
                                </Change>
                            </DirectEvents>
                        </ext:DateField>
                    </td>
                    <td colspan="1">
                        <ext:DateField FieldLabel="Travelling to" Width="150" ID="txtToDate" runat="server"
                            LabelAlign="Top" EmptyDate="" LabelSeparator="">
                            <DirectEvents>
                                <Change OnEvent="CheckInOutSelectChange">
                                </Change>
                            </DirectEvents>
                        </ext:DateField>
                    </td>
                    <td colspan="1" style="padding-top: 5px;">
                        <ext:Label ID="TextArea4" Width="30" runat="server" Style="margin-right: 25px; margin-left: -35px;"
                            Text="=">
                        </ext:Label>
                    </td>
                    <td colspan="1" style="padding-right: 7px;">
                        <ext:NumberField LabelSeparator="" ID="txtDayCount" runat="server" Width="60" FieldLabel="Days"
                            LabelAlign="Top">
                        </ext:NumberField>
                    </td>
                    <td colspan="1">
                        <ext:NumberField LabelSeparator="" ID="txtNightCount" runat="server" Width="60" FieldLabel="Night"
                            LabelAlign="Top">
                        </ext:NumberField>
                    </td>
                </tr>
                <tr style="padding-bottom: 1em;">
                    <td colspan="1">
                        <ext:ComboBox ID="cmbExpensePaidBy" ForceSelection="true" LabelSeparator="" runat="server"
                            FieldLabel="Expense Paid by" LabelAlign="Top" QueryMode="Local" DisplayField="Name"
                            ValueField="Value">
                            <Store>
                                <ext:Store ID="storeExpensePaidBy" runat="server">
                                    <Fields>
                                        <ext:ModelField Name="Name" />
                                        <ext:ModelField Name="Value" Type="String" />
                                    </Fields>
                                </ext:Store>
                            </Store>
                        </ext:ComboBox>
                    </td>
                    <td colspan="1">
                        <ext:ComboBox ID="cmbTravelBy" ForceSelection="true" LabelSeparator="" runat="server"
                            FieldLabel="Travel by" LabelAlign="Top" QueryMode="Local" DisplayField="Name"
                            ValueField="Value">
                            <Store>
                                <ext:Store ID="storeTravelBy" runat="server">
                                    <Fields>
                                        <ext:ModelField Name="Name" />
                                        <ext:ModelField Name="Value" Type="String" />
                                    </Fields>
                                </ext:Store>
                            </Store>
                            <Listeners>
                                <Select Handler="if(this.getValue()==='5') {#{txtOtherName}.show();} else {#{txtOtherName}.hide();}" />
                            </Listeners>
                        </ext:ComboBox>
                    </td>
                    <td colspan="3" style='    padding-left: 20px;'>
                        <ext:TextField ID="txtOtherName" Width="120"  Hidden="true" runat="server" FieldLabel="Other Travel by"
                            LabelAlign="Top" LabelSeparator="">
                        </ext:TextField>
                    </td>
                    
                </tr>
                <tr>
                    <td>
                        <ext:TextField ID="txtRequestedAdvanceAmount" Disabled="true" runat="server" Width="150"
                            FieldLabel="Advance Request" LabelAlign="Top" LabelSeparator="">
                        </ext:TextField>
                    </td>
                    <td>
                        <ext:ComboBox ID="cmbLocations" ForceSelection="true" runat="server" Width="150"
                            FieldLabel="Location for Allowance *" LabelAlign="Top" LabelSeparator=" " QueryMode="Local"
                            DisplayField="LocationName" ValueField="LocationId" Disabled="true">
                            <Store>
                                <ext:Store ID="store1" runat="server">
                                    <Fields>
                                        <ext:ModelField Name="LocationId" Type="String" />
                                        <ext:ModelField Name="LocationName" />
                                    </Fields>
                                </ext:Store>
                            </Store>
                            <DirectEvents>
                                <Select OnEvent="cmbLocations_Change">
                                    <EventMask ShowMask="true" />
                                    <ExtraParams>
                                        <ext:Parameter Name="Values" Value="Ext.encode(#{gridAllowances}.getRowsValues({selectedOnly : false}))"
                                            Mode="Raw" />
                                    </ExtraParams>
                                </Select>
                            </DirectEvents>
                        </ext:ComboBox>
                    </td>
                </tr>
                <tr style="padding-bottom: 1em;">
                    <td colspan="5">
                        <ext:GridPanel StyleSpec="margin-top:15px;" Width="560" ID="gridAllowances" runat="server">
                            <Store>
                                <ext:Store ID="storeAllowances" runat="server">
                                    <Model>
                                        <ext:Model ID="Model1" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="LineId" Type="String" />
                                                <ext:ModelField Name="RequestId" Type="string" />
                                                <ext:ModelField Name="AllowanceId" Type="string" />
                                                <ext:ModelField Name="AllowanceName" Type="string" />
                                                <ext:ModelField Name="Quantity" Type="Float" />
                                                <ext:ModelField Name="Rate" Type="Float" />
                                                <ext:ModelField Name="Total" Type="Float" />
                                                <ext:ModelField Name="IsEditable" Type="String" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                            <Plugins>
                                <ext:CellEditing ID="CellEditing1" ClicksToEdit="1" runat="server">
                                    <Listeners>
                                        <Edit Fn="afterEdit" />
                                    </Listeners>
                                </ext:CellEditing>
                            </Plugins>
                            <ColumnModel>
                                <Columns>
                                    <ext:Column ID="Column1" Sortable="false" Flex="1" MenuDisabled="true" runat="server" Text="Allowances"
                                        Align="Left" DataIndex="AllowanceName" Width="240" />
                                    <ext:Column ID="ColumnQuantity" Sortable="false" MenuDisabled="true" runat="server" Text="Qty"
                                        Align="Center" DataIndex="Quantity" Width="90">
                                        <Editor>
                                            <ext:NumberField ID="txtQTY" SelectOnFocus="true" runat="server" AllowBlank="false" />
                                        </Editor>
                                    </ext:Column>
                                    <ext:Column ID="ColumnRate" Sortable="false" MenuDisabled="true" runat="server" Text="Rate"
                                        Align="Right" DataIndex="Rate" Width="110">
                                        <Editor>
                                            <ext:NumberField ID="txtRate" SelectOnFocus="true" runat="server" AllowBlank="false">
                                            </ext:NumberField>
                                        </Editor>                                    </ext:Column>
                                    <ext:Column Align="Right" runat="server" Width="110" ID="ColumnTotal" Text="Total" Sortable="false"
                                        Groupable="false" DataIndex="Total" CustomSummaryType="totalCost">
                                        <Renderer Handler="return Ext.util.Format.number((record.data.Quantity * record.data.Rate),'0,000.00');" />
                                    </ext:Column>
                                </Columns>
                            </ColumnModel>
                            <SelectionModel>
                                <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" Mode="Single" />
                            </SelectionModel>
                            <Listeners>
                                <BeforeEdit Fn="beforeEdit" />
                            </Listeners>
                        </ext:GridPanel>
                    </td>
                </tr>
                <tr>
                    <td colspan="5" style='padding-left: 350px'>
                        <ext:DisplayField FieldStyle="text-align:right" Width="200px" Text="0" LabelSeparator=""
                            FieldLabel="Total" ID="dispTotal" runat="server" />
                    </td>
                </tr>
                <tr id="rowAdvance" style="display: none">
                    <td colspan="5">
                        <div style="margin-top: -30px; margin-left: 50%;">
                            <ext:TextField ID="txtAdvanceAmount" Width="275" runat="server" FieldLabel="Advance Amount"
                                LabelWidth="150" LabelAlign="Left">
                            </ext:TextField>
                        </div>
                    </td>
                </tr>
            </table>
            <ext:LinkButton runat="server" ID="btnExtendShow" StyleSpec='margin-top:10px;' Text="Extend Arrival">
                <Listeners>
                    <Click Handler="#{windowExtend}.show();" />
                </Listeners>
            </ext:LinkButton>
            <table>
                <tr>
                    <td>
                        <ext:DateField Disabled="true" FieldLabel="Extended date" Width="150" ID="extensionDateValue"
                            runat="server" LabelAlign="Top" EmptyDate="" LabelSeparator="">
                        </ext:DateField>
                    </td>
                    <td style='padding-left: 10px;'>
                        <ext:TextField Disabled="true" ID="extensionReasonValue" LabelSeparator="" FieldLabel="Reason for extension"
                            LabelAlign="Top" runat="server" Width="400px">
                        </ext:TextField>
                    </td>
                </tr>
            </table>
        </div>
        <div style="clear: both">
        </div>
        <ext:ComboBox ForceSelection="true" Hidden="true" StyleSpec="margin-left:10px" ID="cmbForwardList"
            runat="server" FieldLabel="Forward To *" LabelAlign="Top" Width="200" LabelSeparator=" "
            QueryMode="Local" DisplayField="Text" ValueField="ID">
            <Store>
                <ext:Store ID="store3" runat="server">
                    <Fields>
                        <ext:ModelField Name="Text" />
                        <ext:ModelField Name="ID" Type="String" />
                    </Fields>
                </ext:Store>
            </Store>
        </ext:ComboBox>
        <div class="popupButtonDiv" runat="server" id="divButtons">
            <div style="margin-top: 10px">
            </div>
            <ext:Button StyleSpec="margin-left:10px" ID="btnSaveAndLater" Cls="btn btn-primary"
                Width="100" runat="server" Text="&nbsp;Forward&nbsp;" ValidationGroup="InsertUpdate">
                <DirectEvents>
                    <Click OnEvent="ButtonNext_Click">
                        <Confirmation ConfirmRequest="true" Message="Are you sure, you want to forward the Travel Request?" />
                        <EventMask ShowMask="true" />
                        <ExtraParams>
                            <ext:Parameter Name="AllowanceValues" Value="Ext.encode(#{gridAllowances}.getRowsValues({selectedOnly : false}))"
                                Mode="Raw" />
                        </ExtraParams>
                    </Click>
                </DirectEvents>
                <Listeners>
                    <Click Handler="valGroup = 'InsertUpdate'; return CheckValidation();">
                    </Click>
                </Listeners>
            </ext:Button>
            <ext:Button Hidden="true" StyleSpec="margin-left:10px" ID="btnAdvance" Cls="btn btn-primary"
                Width="120" runat="server" Text="&nbsp;Set Advance&nbsp;" ValidationGroup="InsertUpdateAdvance">
                <DirectEvents>
                    <Click OnEvent="btnAdvance_Click">
                        <Confirmation ConfirmRequest="true" Message="Are you sure, you want to set advance for the Travel Request?" />
                        <EventMask ShowMask="true" />
                    </Click>
                </DirectEvents>
                <Listeners>
                    <Click Handler="valGroup = 'InsertUpdateAdvance'; return CheckValidation();">
                    </Click>
                </Listeners>
            </ext:Button>
            <div class="btnFlatOr" id="cancelBlock">
                or</div>
            <ext:Button StyleSpec="margin-left:10px" ID="btnCancel1" Cls="btn btn-primary" Width="100"
                runat="server" Text="&nbsp;Cancel&nbsp;">
                <Listeners>
                    <Click Handler="#{Window1}.hide();">
                    </Click>
                </Listeners>
            </ext:Button>
        </div>
    </Content>
</ext:Window>
<ext:Window ID="windowExtend" runat="server" ButtonAlign="Left" Title="Extend Arrival"
    Icon="Application" Width="380" Height="320" BodyPadding="10" Hidden="true" Modal="true">
    <Content>
        <table class="fieldTable">
            <tr>
                <td>
                    <ext:DateField FieldLabel="Extended date" Width="150" ID="dateExtension" runat="server"
                        LabelAlign="Top" EmptyDate="" LabelSeparator="">
                    </ext:DateField>
                    <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator12" runat="server"
                        ValidationGroup="SaveAmountAdd" ControlToValidate="dateExtension" ErrorMessage="Date is required." />
                </td>
            </tr>
            <tr>
                <td>
                    <ext:TextArea ID="txtExtensionNotes" LabelSeparator="" FieldLabel="Reason for extension"
                        LabelAlign="Top" runat="server" Width="300px">
                    </ext:TextArea>
                    <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator13" runat="server"
                        ValidationGroup="SaveAmountAdd" ControlToValidate="txtExtensionNotes" ErrorMessage="Reason for extension is required." />
                </td>
            </tr>
        </table>
    </Content>
    <Buttons>
        <ext:Button runat="server" ID="btnExtend" Cls="btn btn-primary" Text="<i></i>Save"
            runat="server">
            <DirectEvents>
                <Click OnEvent="btnExtend_Click">
                    <Confirmation ConfirmRequest="true" Message="Confirm extend the arrival?" />
                    <EventMask ShowMask="true" />
                </Click>
            </DirectEvents>
            <Listeners>
                <Click Handler="valGroup = 'SaveAmountAdd'; return CheckValidation();">
                </Click>
            </Listeners>
        </ext:Button>
        <ext:LinkButton runat="server" Cls="btnFlatLeftGap" ID="LinkButton1" Text="<i></i>Cancel"
            runat="server">
            <Listeners>
                <Click Handler="#{windowExtend}.hide();">
                </Click>
            </Listeners>
        </ext:LinkButton>
    </Buttons>
</ext:Window>
<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtAdvanceAmount"
    ErrorMessage="Advance amount is required." Display="None" ValidationGroup="InsertUpdateAdvance">
</asp:RequiredFieldValidator>
<asp:CompareValidator ID="RequiredFieldValidator10" Type="Currency" Operator="GreaterThanEqual"
    ValueToCompare="0" runat="server" ControlToValidate="txtAdvanceAmount" ErrorMessage="Invalid advance amount."
    Display="None" ValidationGroup="InsertUpdateAdvance">
</asp:CompareValidator>
<asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtPlaceToTravel"
    ErrorMessage="Place to travel is required." Display="None" ValidationGroup="InsertUpdate">
</asp:RequiredFieldValidator>
<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtHRComment"
    ErrorMessage="Comment is required." Display="None" ValidationGroup="InsertUpdate">
</asp:RequiredFieldValidator>
<asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="cmbCountry"
    ErrorMessage="Country is required" Display="None" ValidationGroup="InsertUpdate">
</asp:RequiredFieldValidator>
<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtPurposeOfTravel"
    ErrorMessage="Purpose of Travel is required" E Display="None" ValidationGroup="InsertUpdate">
</asp:RequiredFieldValidator>
<asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtFromDate"
    ErrorMessage="From Date is required" Display="None" ValidationGroup="InsertUpdate">
</asp:RequiredFieldValidator>
<asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="txtToDate"
    ErrorMessage="To Date is required" Display="None" ValidationGroup="InsertUpdate">
</asp:RequiredFieldValidator>
<asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="cmbTravelBy"
    ErrorMessage="Travel is required" Display="None" ValidationGroup="InsertUpdate">
</asp:RequiredFieldValidator>
<asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ControlToValidate="cmbExpensePaidBy"
    ErrorMessage="Expense Paid by is required" Display="None" ValidationGroup="InsertUpdate">
</asp:RequiredFieldValidator>
<asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ControlToValidate="cmbLocations"
    ErrorMessage="Location is required" Display="None" ValidationGroup="InsertUpdateSettle">
</asp:RequiredFieldValidator>