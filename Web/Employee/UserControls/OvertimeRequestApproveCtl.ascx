﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OvertimeRequestApproveCtl.ascx.cs"
    Inherits="Web.Employee.UserControls.OvertimeRequestApproveCtl" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<%@ Register Assembly="DevExpress.Web.v16.1, Version=16.1.8.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dx" %>
<style type="text/css">
    body
    {
        background-color: #E8F1FF !important;
    }
    div.notify
    {
        width: inherit !important;
    }
    .marginal
    {
        margin-top: 10px !important;
    }
</style>
<script type="text/javascript">
    function ConfirmInstitutionDeletion() {
        if (ddllist.selectedIndex == -1) {
            alert('No Insurance company to delete.');
            clearUnload();
            return false;
        }
        if (confirm('Do you want to delete the insurance company?')) {
            clearUnload();
            return true;
        }
        else {
            return false;
        }
    }

    function DisplayInstitutionInTextBox(dropdown_id) {

    }


    function closePopup() {
//        if (hasImport)
//            alert('Test');
        window.opener.refreshEventList(window); ;
    }


    window.onunload = closePopup;




    function dateChange(value) {



    }

    function setRequestHour() {

        var calDate = <%=calDate.ClientID %>;
        var dateEnd = <%=dateEnd.ClientID %>;
        var TimePicker1 = <%=TimePicker1.ClientID %>;
        var TimePicker2 = <%=TimePicker2.ClientID %>;

        var value = CompanyX.direct.GetHour(calDate.getValue(),dateEnd.getValue(),TimePicker1.getValue(),TimePicker2.getValue(),
                            {
                                success: function (result) 
                                {
                                    <%= txtHour.ClientID %>.setValue(result);
                                    

                                }
                            }
                    );

    }

    function getHour(){
         var value = CompanyX.direct.GetGeneratedHour(<%=calDate.ClientID %>.getValue(),
                            {
                                success: function (result) 
                                {
                                    <%=txtActualHour.ClientID %>.setValue(result);
                                    if(<%=txtHour.ClientID %>.getValue() == '')
                                        <%=txtHour.ClientID %>.setValue(result);
                                }
                            }
                    );
    }
</script>
<asp:HiddenField ID="Hidden_RequestID" runat="server" />
<div style="display: none;">
    <%-- <asp:Button ID="btnEmpSelect" runat="server" OnClick="ddlEmployeeList_SelectionChange" />--%>
</div>
<div class="popupHeader">
    <span runat="server" id="header" style='margin-left: 20px; padding-top: 8px; display: block'>
        Overtime Request</span>
</div>
<div class="marginal">
    <ext:ResourceManager Namespace="CompanyX" ShowWarningOnAjaxFailure="false" ID="ResourceManager1" DisableViewState="false"
        runat="server" ScriptMode="Release" />
    <%--  <asp:Label ID="lblMsg" runat="server" Text="" BackColor="Yellow" ForeColor="Red"></asp:Label>--%>
    <table>
        <tr runat="server" id="row1">
            <%--<td colspan="4">
                <p style="margin-bottom: 3px; margin-top: 10px;">
                    Employee
                </p>
            </td>--%>
        </tr>
        <tr runat="server" id="row2">
            <td colspan="2">
                <%--<asp:TextBox Style="width: 200px; margin-right: 25px;" ID="txtEmpSearchText" onchange="if(this.value=='') clearEmp();"
                    runat="server"></asp:TextBox>
                <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtEmpSearchText"
                    WatermarkText="Employee Name" WatermarkCssClass="searchBoxText" />
                <cc1:AutoCompleteExtender EnableCaching="true" ID="AutoCompleteExtenderOrganization"
                    OnClientItemSelected="ACE_item_selected" runat="server" MinimumPrefixLength="2"
                    ServiceMethod="GetEmployeeNames" ServicePath="~/PayrollService.asmx" TargetControlID="txtEmpSearchText"
                    CompletionSetCount="10" CompletionInterval="250" CompletionListCssClass="AutoExtender"
                    CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight">
                </cc1:AutoCompleteExtender>--%>
                <ext:Store ID="storeEmployee" runat="server">
                    <Model>
                        <ext:Model ID="Model1" IDProperty="Value" runat="server">
                            <Fields>
                                <ext:ModelField Name="Value" Type="String" />
                                <ext:ModelField Name="Text" />
                            </Fields>
                        </ext:Model>
                    </Model>
                </ext:Store>
                <ext:ComboBox LabelSeparator="" ID="cboEmployee" runat="server" FieldLabel="Employee Name"
                    DisplayField="Text" ValueField="Value" Width="200" ForceSelection="true" StoreID="storeEmployee"
                    QueryMode="Local" LabelAlign="Top">
                    <DirectEvents>
                        <Select OnEvent="cboEmployee_Select">
                            <EventMask ShowMask="true" />
                        </Select>
                    </DirectEvents>
                </ext:ComboBox>
                <asp:RequiredFieldValidator ID="reqdEmployee" runat="server" ErrorMessage="Employee is required."
                    ControlToValidate="cboEmployee" ValidationGroup="SaveUpdate" Display="None" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <p style="margin-bottom: 3px; margin-top: 10px;">
                    Overtime type
                </p>
            </td>
        </tr>
        <tr>
             <td colspan="2">
                <asp:DropDownList DataTextField="Name" DataValueField="OvertimeTypeId" ID="ddlOvertimeType"
                    AppendDataBoundItems="true" runat="server" Style="width: 200px; margin-right: 25px;">
                    <asp:ListItem Text="" Value="-1" Selected="True" />
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <p style="margin-bottom: 3px; margin-top: 10px;">
                    Date
                </p>
            </td>
        </tr>
        <tr>
            <td>
                <ext:DateField ID="calDate" runat="server" Width="200px">
                    <Plugins>
                        <ext:GenericPlugin ID="HighlightDates1" runat="server" InstanceName="Ext.ux.DatePicker.HighlightWithNepDates" />
                    </Plugins>
                    <Listeners>
                        <Change Handler="setRequestHour();" />
                    </Listeners>
                </ext:DateField>
            </td>
            <td style="padding: 5px;">
                <ext:TimeField Width="100" StyleSpec="margin-left:10px" ID="TimePicker1" LabelSeparator=""
                    runat="server" Increment="1" Format="hh:mm tt">
                    <Listeners>
                        <Change Handler="setRequestHour();" />
                    </Listeners>
                </ext:TimeField>
                <ext:Button runat="server" Hidden="true" ID="btnGenerateHour1" Text="Generate OT Hour">
                    <DirectEvents>
                        <Click OnEvent="btnGenerateHour_Click">
                            <EventMask ShowMask="true" />
                        </Click>
                    </DirectEvents>
                   <%-- <Listeners>
                        <Click Handler="getHour();" />
                    </Listeners>--%>
                </ext:Button>
            </td>
        </tr>
        <tr runat="server" id="rowEndDateLabel">
            <td colspan="2">
                <p style="margin-bottom: 3px; margin-top: 10px;">
                    End Date
                </p>
            </td>
        </tr>
        <tr runat="server" id="rowEndDateValue">
            <td style="">
                <ext:DateField ID="dateEnd" runat="server" Width="200px">
                    <Plugins>
                        <ext:GenericPlugin ID="GenericPlugin2" runat="server" InstanceName="Ext.ux.DatePicker.HighlightWithNepDates" />
                    </Plugins>
                     <Listeners>
                        <Change Handler="setRequestHour();" />
                    </Listeners>
                </ext:DateField>
            </td>
            <td style="padding: 5px;">
                <ext:TimeField Width="100" StyleSpec="margin-left:10px" ID="TimePicker2" LabelSeparator=""
                    runat="server" Increment="1" Format="hh:mm tt">
                    <Listeners>
                        <Change Handler="setRequestHour();" />
                    </Listeners>
                </ext:TimeField>
            </td>
        </tr>
        <tr runat="server" id="rowActualHourLabel" style='display:none'>
            <td colspan="2">
                <p style="margin-bottom: 5px; margin-top: 15px;">
                    Actual hour
                </p>
            </td>
        </tr>
        <tr runat="server" id="rowActualHourValue" style='display:none'>
            <td colspan="2">
                <ext:TextField runat="server" Disabled="true" stylespec='background-color:lightgray;' ID="txtActualHour" ReadOnly="true" Width="100px"  />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <p style="margin-bottom: 5px; margin-top: 15px;">
                    Requested hour
                </p>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <ext:TextField runat="server" ID="txtHour" Width="100px"  />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <p style="margin-bottom: 3px; margin-top: 10px;">
                    Reason for Extra Work
                </p>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:TextBox ID="txtReason" runat="server" TextMode="multiline" Style="width: 410px;
                    height: 40px; margin-right: 25px;"></asp:TextBox>
            </td>
        </tr>
        <tr runat="server" id="rowManager">
            <%--<td colspan="0">
                <p style="margin-bottom: 3px; margin-top: 10px;">
                    Sent for Recommend to
                </p>
            </td>
            <td colspan="0">
                <p style="margin-bottom: 3px; margin-top: 10px;">
                    Sent for Approval to
                </p>
            </td>--%>
        </tr>
    </table>
    <table>
        <tr>
            <td>
                <%-- <asp:DropDownList DataTextField="Text" DataValueField="Value" ID="cmbRecommender"
                    runat="server" Style="width: 200px; margin-right: 25px;">
                    <asp:ListItem Text="--Select Recommender--" Value="-1" />
                </asp:DropDownList>--%>
                <ext:Store ID="storeR" runat="server">
                    <Model>
                        <ext:Model ID="Model2" IDProperty="Value" runat="server">
                            <Fields>
                                <ext:ModelField Name="Value" Type="String" />
                                <ext:ModelField Name="Text" />
                            </Fields>
                        </ext:Model>
                    </Model>
                </ext:Store>
                <ext:ComboBox LabelSeparator="" StyleSpec='padding-right: 10px;' ID="cmbRecommender" runat="server" FieldLabel=" Sent for Recommend to"
                    DisplayField="Text" ValueField="Value" Width="200" ForceSelection="true" StoreID="storeR"
                    QueryMode="Local" LabelAlign="Top">
                    <%-- <DirectEvents>
                        <Select OnEvent="cboEmployee_Select">
                            <EventMask ShowMask="true" />
                        </Select>
                    </DirectEvents>--%>
                </ext:ComboBox>
            </td>
            <td style="">
                <%--  <asp:DropDownList DataTextField="Text" DataValueField="Value" ID="cmbApproval" runat="server"
                    Style="width: 200px; margin-right: 25px;">
                    <asp:ListItem Text="--Select Approval--" Value="-1" />
                </asp:DropDownList>--%>
                <ext:Store ID="storeApp" runat="server">
                    <Model>
                        <ext:Model ID="Model3" IDProperty="Value" runat="server">
                            <Fields>
                                <ext:ModelField Name="Value" Type="String" />
                                <ext:ModelField Name="Text" />
                            </Fields>
                        </ext:Model>
                    </Model>
                </ext:Store>
                <ext:ComboBox LabelSeparator=""  ID="cmbApproval" StyleSpec='margin-left:10px' runat="server" FieldLabel=" Sent for Approval to"
                    DisplayField="Text" ValueField="Value" Width="200" ForceSelection="true" StoreID="storeApp"
                    QueryMode="Local" LabelAlign="Top">
                    <%-- <DirectEvents>
                        <Select OnEvent="cboEmployee_Select">
                            <EventMask ShowMask="true" />
                        </Select>
                    </DirectEvents>--%>
                </ext:ComboBox>
            </td>
        </tr>
    </table>
    <%-- <asp:RequiredFieldValidator runat="server" ID="reqdEmployee" ValidationGroup="SaveUpdate"
        Display="None" ErrorMessage="Employee is required." ControlToValidate="txtEmpSearchText"></asp:RequiredFieldValidator>--%>
    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator4" ValidationGroup="SaveUpdate"
        InitialValue="-1" Display="None" ErrorMessage="Overtime type is required." ControlToValidate="ddlOvertimeType"></asp:RequiredFieldValidator>
    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator2" ValidationGroup="SaveUpdate"
        Display="None" ErrorMessage="Reason is required" ControlToValidate="txtReason"></asp:RequiredFieldValidator>
    <asp:RequiredFieldValidator runat="server" ID="cmbRecommenderValidation" ValidationGroup="SaveUpdate"
        Display="None" ErrorMessage="Recommender is required" ControlToValidate="cmbRecommender"></asp:RequiredFieldValidator>
    <asp:RequiredFieldValidator runat="server" ID="cmbApprovalValidation" ValidationGroup="SaveUpdate"
        Display="None" ErrorMessage="Approval is required." ControlToValidate="cmbApproval"></asp:RequiredFieldValidator>
    <table>
        <tr>
            <td>
                <ext:Button runat="server" StyleSpec="margin-top:10px" Width="100" Cls="btn btn-primary"
                    ValidationGroup="SaveUpdatePopup" ID="btnSave" Text="<i></i>Save">
                    <DirectEvents>
                        <Click OnEvent="btnSave_Click">
                            <EventMask ShowMask="true" />
                        </Click>
                    </DirectEvents>
                    <Listeners>
                        <Click Handler="valGroup = 'SaveUpdate'; return CheckValidation();">
                        </Click>
                    </Listeners>
                </ext:Button>
                <ext:Button Visible="false" runat="server" StyleSpec="margin-top:10px" Width="100"
                    Cls="btn btn-primary" ValidationGroup="SaveUpdatePopup" ID="btnAssignOvertime"
                    Text="<i></i>Assign">
                    <DirectEvents>
                        <Click OnEvent="btnAssignOvertime_Click">
                            <EventMask ShowMask="true" />
                        </Click>
                    </DirectEvents>
                    <Listeners>
                        <Click Handler="valGroup = 'SaveUpdate'; return CheckValidation();">
                        </Click>
                    </Listeners>
                </ext:Button>
                <%--  <asp:Button Visible="false" ID="btnAssignOvertime" ValidationGroup="SaveUpdate" runat="server"
                    OnClientClick="valGroup='SaveUpdate';return CheckValidation();" Text="Assign"
                    Width="120" Style="margin-top: 25px;" CssClass="save" OnClick="btnAssignOvertime_Click" />--%>
            </td>
        </tr>
    </table>
</div>
