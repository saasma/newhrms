﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AssignAllowanceCtrl.ascx.cs"
    Inherits="Web.Employee.UserControls.AssignAllowanceCtrl" %>
<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<%@ Register Assembly="DevExpress.Web.v16.1, Version=16.1.8.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dx" %>
<script type="text/javascript">

        function closePopup() {

            window.opener.refreshEventList(window);

        }

        function ACE_item_selected(source, eventArgs) {
            var value = eventArgs.get_value();
            selEmpId = value;
            document.getElementById('<%= hdnEmployeeId.ClientID %>').value = value;
            var clickButton = document.getElementById('<%= btnEmpSelect.ClientID %>');
            clickButton.click();
        }
        function clearEmp()
        {
            <%= hdnEmployeeId.ClientID %>.setValue('');
        }
</script>
<ext:ResourceManager ID="ResourceManager1" DisableViewState="false" ScriptMode="Release"
    Namespace="" runat="server" />
<asp:HiddenField ID="Hidden_CounterTypePrev" runat="server" />
<asp:HiddenField ID="Hidden_FromPrev" runat="server" />
<asp:HiddenField ID="Hidden_ToPrev" runat="server" />
<asp:HiddenField ID="Hidden_DaysPrev" runat="server" />
<asp:HiddenField ID="hdnEmployeeId" runat="server" />
<div style="display: none;">
    <asp:Button ID="btnEmpSelect" runat="server" OnClick="ddlEmployeeList_SelectionChange" />
</div>
<div class="popupHeader">
    <span runat="server" id="header" style='margin-left: 20px; padding-top: 8px; display: block'>
        Assign Allowance</span>
</div>
<div class="marginal">
    <uc2:MsgCtl ID="divMsgCtl" EnableViewState="false" Hide="true" Width="490px" runat="server" />
    <uc2:WarningCtl ID="divWarningMsg" EnableViewState="false" Width="490px" Hide="true"
        runat="server" />
    <asp:HiddenField ID="Hidden_OldMinute" runat="server" />
    <asp:HiddenField ID="Hidden_RequestID" runat="server" />
    <table>
        <tbody>
            <tr>
                <td colspan="2">
                    Employee
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:TextBox Style="width: 200px; margin-right: 25px;" ID="txtEmpSearchText" onchange="if(this.value=='') clearEmp();"
                        runat="server"></asp:TextBox>
                    <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtEmpSearchText"
                        WatermarkText="Employee Name" WatermarkCssClass="searchBoxText" />
                    <cc1:AutoCompleteExtender EnableCaching="true" ID="AutoCompleteExtenderOrganization"
                        OnClientItemSelected="ACE_item_selected" runat="server" MinimumPrefixLength="2"
                        ServiceMethod="GetEmployeeNames" ServicePath="~/PayrollService.asmx" TargetControlID="txtEmpSearchText"
                        CompletionSetCount="10" CompletionInterval="250" CompletionListCssClass="AutoExtender"
                        CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight">
                    </cc1:AutoCompleteExtender>
                    <asp:RequiredFieldValidator ID="rfvEmployee" SetFocusOnError="true" ValidationGroup="SaveUpdate"
                        ControlToValidate="txtEmpSearchText" Display="None" ErrorMessage="Employee is required."
                        runat="server" />
                </td>
            </tr>
            <tr>
                <td colspan="2" style="padding-top: 10px;">
                    Allowance type *
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:DropDownList DataTextField="Name" DataValueField="EveningCounterTypeId" ID="ddlAllowanceType"
                        AppendDataBoundItems="true" runat="server" Style="width: 200px;">
                        <asp:ListItem Text="" Value="-1" Selected="True" />
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator runat="server" InitialValue="-1" ID="RequiredFieldValidator5"
                        ValidationGroup="SaveUpdate" Display="None" ErrorMessage="Allowance Type is required."
                        ControlToValidate="ddlAllowanceType"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <ext:DateField ID="txtFromDate" runat="server" FieldLabel="From Date *" LabelAlign="top"
                        LabelSeparator="" Width="200" AllowBlank="false">
                        <Plugins>
                            <ext:GenericPlugin ID="GenericPlugin2" runat="server" InstanceName="Ext.ux.DatePicker.HighlightWithNepDates" />
                        </Plugins>
                    </ext:DateField>
                    <asp:RequiredFieldValidator Enabled="true" Display="None" ID="valtxtPublicationName"
                        runat="server" ValidationGroup="SaveUpdate" ControlToValidate="txtFromDate" ErrorMessage="From date is required." />
                </td>
                <td style="padding-left: 10px">
                    <ext:DisplayField ID="txtFromDateNep" StyleSpec="margin-top:15px" runat="server"
                        FieldLabel=" " LabelAlign="top" LabelSeparator="" Width="200" />
                </td>
            </tr>
            <tr>
                <td>
                    <ext:DateField ID="txtToDate" runat="server" FieldLabel="To Date *" LabelAlign="top"
                        LabelSeparator="" Width="200" Hidden="false" AllowBlank="false">
                        <Plugins>
                            <ext:GenericPlugin ID="GenericPlugin1" runat="server" InstanceName="Ext.ux.DatePicker.HighlightWithNepDates" />
                        </Plugins>
                    </ext:DateField>
                    <asp:RequiredFieldValidator Enabled="true" Display="None" ID="RequiredFieldValidator111"
                        runat="server" ValidationGroup="SaveUpdate" ControlToValidate="txtToDate" ErrorMessage="To Date is required." />
                </td>
                <td style="padding-left: 10px">
                    <ext:DisplayField ID="txtToDateNep" StyleSpec="margin-top:15px" runat="server" FieldLabel=" "
                        LabelAlign="top" LabelSeparator="" Width="175" />
                </td>
            </tr>
            <tr>
                <td>
                    <ext:NumberField ID="txtDays" AllowDecimals="false" runat="server" FieldLabel="Days *"
                        LabelAlign="top" LabelSeparator=" " Width="200" Hidden="false" AllowBlank="false"
                        MinValue="0">
                    </ext:NumberField>
                    <asp:RequiredFieldValidator Enabled="true" Display="None" ID="RequiredFieldValidator11"
                        runat="server" ValidationGroup="SaveUpdate" ControlToValidate="txtDays" ErrorMessage="Day(s) is required." />
                </td>
            </tr>
            <tr>
                <td style="padding-top: 10px">
                    <strong>Reason</strong>
                </td>
                <td style="padding-top: 10px">
                    <%--<asp:Label runat="server" ID="lblReason"></asp:Label>--%>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:TextBox ID="txtReason" runat="server" Height="60" Width="200" TextMode="MultiLine"></asp:TextBox>
                </td>
            </tr>
            <tr runat="server" id="rowManager">
                <td colspan="0">
                    <p style="margin-bottom: 3px; margin-top: 10px;">
                        Sent for Recommend to
                    </p>
                </td>
                <td colspan="0">
                    <p style="margin-bottom: 3px; margin-top: 10px;margin-left:25px;">
                        Sent for Approval to
                    </p>
                </td>
            </tr>
    </table>
    <asp:UpdatePanel ID="UpdatePanel1" UpdateMode="Conditional" runat="server">
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnEmpSelect" />
            <asp:AsyncPostBackTrigger ControlID="btnUpdate" />
        </Triggers>
        <ContentTemplate>
            <table>
                <tr runat="server" id="rowManager2">
                    <td>
                        <asp:DropDownList DataTextField="Text" DataValueField="Value" ID="cmbRecommender"
                            runat="server" Style="width: 200px; margin-right: 25px;">
                            <asp:ListItem Text="--Select Recommender--" Value="-1" />
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:DropDownList DataTextField="Text" DataValueField="Value" ID="cmbApproval" runat="server"
                            Style="width: 200px; margin-right: 25px;">
                            <asp:ListItem Text="--Select Approval--" Value="-1" />
                        </asp:DropDownList>
                    </td>
                </tr>
                </tbody>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
    <table class="tbl">
        <tr>
            <td colspan="2">
                <asp:Button ID="btnUpdate" ValidationGroup="SaveUpdate" runat="server" OnClientClick="valGroup='SaveUpdate';  return CheckValidation(); "
                    Text="Assign" Width="150" Height="30" Style="margin-top: 25px;" OnClick="btnUpdate_Click"
                    CssClass="save" />
            </td>
            <td colspan="2">
            </td>
        </tr>
    </table>
</div>
