﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ENonMonitoryAwardCtrl.ascx.cs" Inherits="Web.Employee.UserControls.ENonMonitoryAwardCtrl" %>

<script type="text/javascript">

    var prepateNMAward = function (grid, toolbar, rowIndex, record) {
        var editDelBtn = toolbar.items.get(1);
        if (record.data.IsEditable == 0) {
            editDelBtn.setVisible(false);
        }

    };
    
    function reloadNMAGrid()
    {
        <%= btnReloadNMAGrid.ClientID %>.fireEvent('click');
    }

    function AddNonMAward()
    {
        addNMA();
    }

    function EditNonMA()
    {
        editNMA('AwardId=' + <%=hdnAwardId.ClientID %>.getValue());
    }

</script>

<ext:Hidden runat="server" ID="hdnAwardId">
</ext:Hidden>

<ext:LinkButton runat="server" Hidden="true" ID="btnReloadNMAGrid">
    <DirectEvents>
        <Click OnEvent="btnReloadNMAGrid_Click">
            <EventMask ShowMask="true" />
        </Click>
    </DirectEvents>
</ext:LinkButton>

<ext:GridPanel ID="gridNMAward" runat="server" Width="1000" Cls="itemgrid" Scroll="None">
                    <Store>
                        <ext:Store ID="storeNMAward" runat="server">
                            <Model>
                                <ext:Model ID="Model1" runat="server" IDProperty="NomineeId">
                                    <Fields>
                                        <ext:ModelField Name="AwardId" Type="String" />
                                        <ext:ModelField Name="AwardName" Type="string" />
                                        <ext:ModelField Name="AwardedBy" Type="string" />
                                        <ext:ModelField Name="AwardDate" Type="string" />
                                        <ext:ModelField Name="Description" Type="string" />
                                        <ext:ModelField Name="IsEditable" Type="Int" />
                                    </Fields>
                                </ext:Model>
                            </Model>
                        </ext:Store>
                    </Store>
                    <ColumnModel>
                        <Columns>                            
                            <ext:Column ID="Column1" Width="200" runat="server" Text="Award Name" DataIndex="AwardName" Wrap="true" />
                            <ext:Column ID="Column2" Width="200" runat="server" Text="Awarded By" DataIndex="AwardedBy" Wrap="true">
                            </ext:Column>
                            <ext:Column ID="Column6" Width="100" runat="server" Text="Award Date" DataIndex="AwardDate">
                            </ext:Column>
                            <ext:Column ID="Column3" Width="250" runat="server" Text="Description" DataIndex="Description" Wrap="true">
                            </ext:Column>
                          
                       <ext:CommandColumn ID="CommandColumn21" runat="server" Width="40">
                                <Commands>
                                    <ext:CommandSeparator />
                                    <ext:GridCommand ToolTip-Text="Edit" Icon="ApplicationEdit" CommandName="Edit" />
                                    
                                </Commands>
                                <DirectEvents>
                                    <Command OnEvent="gridNMAward_Command">
                                        <EventMask ShowMask="true">
                                        </EventMask>
                                        <ExtraParams>
                                            <ext:Parameter Name="ID" Value="record.data.AwardId" Mode="Raw">
                                            </ext:Parameter>
                                            <ext:Parameter Name="command" Value="command" Mode="Raw">
                                            </ext:Parameter>
                                        </ExtraParams>
                                    </Command>
                                </DirectEvents>
                            </ext:CommandColumn>
                            <ext:CommandColumn ID="CommandColumn2" runat="server" Width="40">
                                <Commands>
                                    <ext:CommandSeparator />
                                    <ext:GridCommand ToolTip-Text="Delete" Icon="ApplicationDelete"
                                        CommandName="Delete" />
                                </Commands>
                                <DirectEvents>
                                    <Command OnEvent="gridNMAward_Command">
                                        <EventMask ShowMask="true">
                                        </EventMask>
                                        <Confirmation ConfirmRequest="true" Message="Are you sure, you want to delete the record?" />
                                        <ExtraParams>
                                            <ext:Parameter Name="ID" Value="record.data.AwardId" Mode="Raw">
                                            </ext:Parameter>
                                            <ext:Parameter Name="command" Value="command" Mode="Raw">
                                            </ext:Parameter>
                                        </ExtraParams>
                                    </Command>
                                </DirectEvents>
                                <PrepareToolbar Fn="prepateNMAward" />
                            </ext:CommandColumn>
                             <ext:Column ID="Column4" Width="170" runat="server" Text="">
                            </ext:Column>
                        </Columns>
                    </ColumnModel>
                    <SelectionModel>
                        <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" Mode="Single" />
                    </SelectionModel>
                </ext:GridPanel>


 <ext:Button runat="server" Cls="btn btn-primary" Width="180" StyleSpec="margin-top:10px" Height="30"
            ID="btnAddNewLine" Text="<i></i>Add Non Monetary Award" OnClientClick="AddNonMAward(); return false;">
        </ext:Button>