﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EFamilyCtrl.ascx.cs"
    Inherits="Web.Employee.UserControls.EFamilyCtrl" %>
<script type="text/javascript">

      var CommandHandlerFamily = function(command, record){
            <%= hiddenValue.ClientID %>.setValue(record.data.FamilyId);
                if(command=="Edit")
                {
                    addFamilyMember('FamilyId=' + record.data.FamilyId);
                }
                else
                {
                    <%= btnDeleteLevel.ClientID %>.fireEvent('click');
                }

             }
        var dependentRenderer = function(value)
        {
            if(value=="true")
                return "Yes";
            return "No";
        }

        var prepareFamilyToolBar = function (grid, toolbar, rowIndex, record) {
            var editBtn = toolbar.items.get(1);
            var deleteBtn = toolbar.items.get(2);

             if(record.data.IsEditable != 1) {               
                deleteBtn.setVisible(false);              
            }
            
            //you can return false to cancel toolbar for this record
        };   
        
        function reloadFamilyGrid()
        {
            <%= btnReloadGrid.ClientID %>.fireEvent('click');
        }

        function AddFamilyMem()
        {
            addFamilyMember();
        }

        
</script>
<ext:Hidden runat="server" ID="hiddenValue" />
<ext:Button ID="btnDeleteLevel" runat="server" Hidden="true">
    <DirectEvents>
        <Click OnEvent="btnDeleteLevel_Click">
            <Confirmation ConfirmRequest="true" Message="Are you sure, you want to delete the family member?" />
            <EventMask ShowMask="true" />
        </Click>
    </DirectEvents>
</ext:Button>
<ext:LinkButton runat="server" Hidden="true" ID="btnReloadGrid">
    <DirectEvents>
        <Click OnEvent="btnReloadGrid_Click">
            <EventMask ShowMask="true" />
        </Click>
    </DirectEvents>
</ext:LinkButton>
<ext:GridPanel StyleSpec="margin-top:15px;" ID="GridLevels" runat="server" Cls="itemgrid"
    Scroll="None" Width="1000">
    <Store>
        <ext:Store ID="Store3" runat="server">
            <Model>
                <ext:Model ID="Model4" runat="server" IDProperty="FamilyId">
                    <Fields>
                        <ext:ModelField Name="FamilyId" Type="String" />
                        <ext:ModelField Name="Name" Type="string" />
                        <ext:ModelField Name="Relation" Type="string" />
                        <ext:ModelField Name="DateOfBirth" Type="string" />
                        <ext:ModelField Name="HasDependent" Type="string" />
                        <ext:ModelField Name="Occupation" Type="string" />
                        <ext:ModelField Name="ContactNumber" Type="string" />
                        <ext:ModelField Name="Remarks" Type="string" />
                        <ext:ModelField Name="SpecifiedDate" Type="string" />
                        <ext:ModelField Name="AgeOnSpecifiedSPDate" Type="string" />
                        <ext:ModelField Name="IsEditable" Type="Int" />
                    </Fields>
                </ext:Model>
            </Model>
        </ext:Store>
    </Store>
    <ColumnModel>
        <Columns>
            <ext:Column ID="Column8" Sortable="false" MenuDisabled="true" runat="server" Text="Relation" Wrap="true"
                Align="Left" Width="100" DataIndex="Relation" />
            <ext:Column ID="Column5" Sortable="false" MenuDisabled="true" runat="server" Text="Name" Wrap="true"
                Align="Left" Width="180" DataIndex="Name" />
            <ext:Column ID="Column6" Sortable="false" MenuDisabled="true" runat="server" Text="DOB"
                Width="80" Align="Center" DataIndex="DateOfBirth">
            </ext:Column>
            <ext:Column ID="Column1" Sortable="false" MenuDisabled="true" runat="server" Text="Dependent"
                Width="80" Align="Center" DataIndex="HasDependent">
                <Renderer Fn="dependentRenderer" />
            </ext:Column>
            <ext:Column ID="Column2" Sortable="false" MenuDisabled="true" runat="server" Text="Occupation" Wrap="true"
                Align="Left" Width="140" DataIndex="Occupation">
            </ext:Column>
            <ext:Column ID="Column3" Sortable="false" MenuDisabled="true" runat="server" Text="Contact Number" Wrap="true"
                Align="Left" Width="140" DataIndex="ContactNumber">
            </ext:Column>
            <ext:Column ID="Column7" Sortable="false" MenuDisabled="true" runat="server" Text="Remarks" Wrap="true"
                Align="Left" Width="200" DataIndex="Remarks">
            </ext:Column>
            <ext:CommandColumn ID="CommandColumn1" runat="server" Width="80" Text="Actions" Align="Center">
                <Commands>
                    <ext:CommandSeparator />
                    <ext:GridCommand ToolTip-Text="Edit" Icon="ApplicationEdit" CommandName="Edit" />
                    <ext:GridCommand  ToolTip-Text="Delete" Icon="ApplicationDelete"
                        CommandName="Delete" />
                </Commands>
                <Listeners>
                    <Command Handler="CommandHandlerFamily(command,record);" />
                </Listeners>
                <PrepareToolbar Fn="prepareFamilyToolBar" />
            </ext:CommandColumn>
        </Columns>
    </ColumnModel>
    <SelectionModel>
        <ext:RowSelectionModel ID="RowSelectionModel2" runat="server" Mode="Single" />
    </SelectionModel>
</ext:GridPanel>
<div class="buttonBlockSection">
    <ext:Button runat="server" Cls="btn btn-primary btn-sect" Width="150" StyleSpec="margin-top:10px"
        OnClientClick="AddFamilyMem();return false;" Height="30" ID="btnAddLevel" Text="<i></i>Add Family Member">
    </ext:Button>
</div>
