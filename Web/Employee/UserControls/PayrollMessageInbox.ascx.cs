﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Ext.Net;
using BLL;
using BLL.Manager;
using Web.Controls;

namespace Web.Employee.UserControls
{
    public partial class PayrollMessageInbox : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {

            }
            hdUserName.Text = SessionManager.UserName;
        }
        protected void btnMarkAsRead_Click(object sender, DirectEventArgs e)
        {
            RowSelectionModel sm = this.gridPayrollMessage.SelectionModel.Primary as RowSelectionModel;

            if (sm.SelectedRows.Count == 0)
                return;
            List<int> messagesId = new List<int>();
            MessageBoxConfig msgBoxConfig = new MessageBoxConfig();
            msgBoxConfig.Message = "Successfully mark as read.";
            msgBoxConfig.Handler = "ReloadPayrollMessageGrid();";
            msgBoxConfig.Buttons = MessageBox.Button.OK;
            msgBoxConfig.Width = 200;


            int rows = 0;
            foreach (SelectedRow row in sm.SelectedRows)
                messagesId.Add(int.Parse(row.RecordID));

            if (PayrollMessageManager.MarkAsRead(messagesId, out rows))
            {
                dfRowsAffected.SetValue(string.Concat(rows.ToString(), " row(s) affected."));
                X.ResourceManager.RegisterClientScriptBlock("sdfddsf", "ReloadPayrollMessageGrid();");
                UpdateUnreadMessageCounter();
            }
            else
            {
                dfRowsAffected.SetValue(string.Empty);
            }
        }

        [DirectMethod]
        public void FlagAsRead(int messageId)
        {
            List<int> messagesId = new List<int>();
            messagesId.Add(messageId);
            int rows = 0;
            PayrollMessageManager.MarkAsRead(messagesId, out rows);
            UpdateUnreadMessageCounter();
        }

        [DirectMethod]
        public void btnDirectDelete_Click()
        {
            RowSelectionModel sm = this.gridPayrollMessage.SelectionModel.Primary as RowSelectionModel;
            if (sm.SelectedRows.Count() == 0)
                return;
            List<int> messagesId = new List<int>();
            //MessageBoxConfig msgBoxConfig = new MessageBoxConfig();
            //msgBoxConfig.Message = "Successfully delete.";
            //msgBoxConfig.Handler = "ReloadPayrollMessageGrid();";
            //msgBoxConfig.Buttons = MessageBox.Button.OK;
            //msgBoxConfig.Width = 200;

            int rows = 0;
            foreach (SelectedRow row in sm.SelectedRows)
                messagesId.Add(int.Parse(row.RecordID));

            if (PayrollMessageManager.DeletePayrollMessageInBulk(messagesId, out rows))
            {
                dfRowsAffected.Text = String.Concat(rows.ToString(), " row(s) deleted.");
                X.ResourceManager.RegisterClientScriptBlock("sdfddsf", "ReloadPayrollMessageGrid();");
                UpdateUnreadMessageCounter();
            }
            else
            {
                dfRowsAffected.SetValue(string.Empty);
            }
        }

        public void UpdateUnreadMessageCounter()
        {
            int count = 0;
            count = (int)LeaveAttendanceManager.GetUnreadMessageByUserName(SessionManager.UserName);
            string st = count.ToString() == "0" ? "" : count.ToString();
            X.ResourceManager.RegisterClientScriptBlock("sdfsf", "loadMessage('" + st + "');");
        }

        [DirectMethod]
        public void DeletePoppedUpMessage(int messageId)
        {
            if (PayrollMessageManager.DeletePayrollMessage(messageId))
            {
                wMessageBody.Hide();
                dfRowsAffected.Text = "Your message has been successfully deleted.";
                X.Js.Call("ReloadPayrollMessageGrid");
            }
            else
            {
                NewMessage.ShowWarningMessage("Sorry! Couldn't delete the message. Try Again.");
                return;
            }
        }
    }
}