﻿using BLL;
using BLL.Manager;
using DAL;
using Ext.Net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Utils.Helper;

namespace Web.Employee.UserControls
{
    public partial class InsuranceClaim : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();

            }
        }

        protected void EnableMedicalType()
        {
            txtDetailsOfIllness.Show();
            txtConsultantName.Show();
            txtHostpitalName.Show();
            txtDoctorAddress.Show();
            txtDoctorName.Show();
            dfDiagnosisDate.Show();


            txtDetailsOfTreatment.Hide();
            txtDetailsOfAccident.Hide();
            txtLocationOfAccidents.Hide();
            tfttme.Hide();
            dfDateOfAccident.Hide();
        }

        protected void EnableAccidentalType()
        {
            txtDetailsOfIllness.Hide();
            txtConsultantName.Hide();
            txtHostpitalName.Hide();
            txtDoctorAddress.Hide();
            txtDoctorName.Hide();
            dfDiagnosisDate.Hide();


            txtDetailsOfTreatment.Show();
            txtDetailsOfAccident.Show();
            txtLocationOfAccidents.Show();
            tfttme.Show();
            dfDateOfAccident.Show();
        }

        public void ShowPopUp(string id)
        {
            ClearFields();
            if (string.IsNullOrEmpty(id))
            {

                hiddenID.Text = "";
                btnSave.Text = "Save";
                if (SessionManager.CurrentLoggedInEmployeeId == 0)
                {
                    lblEININo.Text = "";
                    lblEName.Text = "";
                }
                windowAddEditClaim.Show();
                return;
            }

            this.hiddenID.Text = id;


            btnSave.Text = "Update";



            if (hiddenID.Text.Trim() != null && hiddenID.Text.Trim() != "")
            {
                SCClaim obj = HRManager.getClaim(Convert.ToInt32(hiddenID.Text.Trim()));
                if (obj != null)
                {

                    if (!string.IsNullOrEmpty(obj.Status.ToString()))
                    {
                        cmbClaimType.SetValue(obj.ClaimType.ToString());
                    }
                    dfClaimDate.Text = obj.ClaimDate.ToString();


                    //to load emp details
                    EEmployee emp = new EmployeeManager().GetById(obj.EmployeeId);

                    lblEININo.Text = "Employee ID : " + emp.EmployeeId;
                    lblEName.Text = emp.Title + " " + emp.Name;

                    this.hdnEmployeeId.Text = emp.EmployeeId.ToString();

                    //GetFamilyMemberNameByRelation
                    List<HFamily> lst = CommonManager.GetAllActiveFamilyListByEmpID(obj.EmployeeId);
                    HFamily ob = new HFamily();
                    ob.FamilyId = 0;
                    ob.Name = emp.Name;
                    ob.Relation = "Self";
                    lst.Add(ob);
                    storecmbClaimOf.DataSource = lst;
                    storecmbClaimOf.DataBind();
                    //end



                    //cmbClaimOf.SelectedItem.Value = obj.ClaimOfFamilyId.ToString();
                    cmbClaimOf.SetValue(obj.ClaimOfFamilyId.ToString());

                    //cmbRelation.SelectedItem.Value = obj.RelationId.ToString();
                    cmbRelation.SetValue(obj.RelationId.ToString());



                    if (obj.ClaimType == (int)ClaimType.Medical)
                    {
                        EnableMedicalType();
                        if (obj.DiagnosisDate != null)
                            dfDiagnosisDate.Text = obj.DiagnosisDate.ToString();
                        txtDoctorName.Text = obj.DoctorName;
                        txtDoctorAddress.Text = obj.DoctorAddress;
                        txtHostpitalName.Text = obj.HospitalNameAddress;
                        txtConsultantName.Text = obj.ConsultantName;
                        txtDetailsOfIllness.Text = obj.Details;
                    }
                    else
                    {
                        EnableAccidentalType();
                        dfDateOfAccident.Text = obj.AccidentDateTime.ToString();

                        //tfttme.SetValue(Convert.ToDateTime(obj.AccidentDateTime).ToLongTimeString());
                        tfttme.SelectedTime = new TimeSpan(Convert.ToDateTime(obj.AccidentDateTime).Hour, Convert.ToDateTime(obj.AccidentDateTime).Minute, 0);

                        txtLocationOfAccidents.Text = obj.AccidentLocation;
                        txtDetailsOfAccident.Text = obj.Details;
                        txtDetailsOfTreatment.Text = obj.DetailsOfTreatment;

                    }
                    txtTotalClaim.Text = obj.TotalClaimAmount.ToString();


                }
                windowAddEditClaim.Show();
            }

        }

        public void ClearFields()
        {
            //hiddenID.Text = "";
            dfClaimDate.Clear();
            dfDiagnosisDate.Clear();
            txtDoctorName.Text = "";
            txtDoctorAddress.Text = "";
            txtHostpitalName.Text = "";
            txtConsultantName.Text = "";
            txtDetailsOfIllness.Text = "";
            txtTotalClaim.Clear();
            dfDateOfAccident.Clear();
            txtLocationOfAccidents.Text = "";
            txtDetailsOfAccident.Text = "";
            txtDetailsOfTreatment.Text = "";
            cmbClaimType.Clear();
            cmbClaimOf.Clear();
            cmbRelation.Clear();
        }

        //public void btnAddClaim_Click(object sender, DirectEventArgs e)
        //{
        //    ClearFields();    
        //    hiddenID.Text = "";
        //    btnSave.Text = "Save";
        //    windowAddEditClaim.Show();
        //}

        //public void popAddClaim(string id)
        //{
        //    ClearFields();
        //    hiddenID.Text = "";
        //    btnSave.Text = "Save";
        //    windowAddEditClaim.Show();
        //}

        public void btnSelectEmp_Click(object sender, DirectEventArgs e)
        {
            lblEININo.Text = "";
            lblEName.Text = "";
            if (!string.IsNullOrEmpty(cmbSearch.SelectedItem.Value))
            {
                int employeeId = int.Parse(cmbSearch.SelectedItem.Value);
                EEmployee emp = EmployeeManager.GetEmployeeById(employeeId);
                if (emp != null)
                {
                    lblEININo.Text = "Employee ID : " + emp.EmployeeId;
                    lblEName.Text = emp.Title + " " + emp.Name;

                    hdnEmployeeId.Text = employeeId.ToString();

                    //GetFamilyMemberNameByRelation
                    List<HFamily> lst = CommonManager.GetAllActiveFamilyListByEmpID(employeeId);
                    HFamily obj = new HFamily();
                    obj.FamilyId = 0;
                    obj.Name = emp.Name;
                    obj.Relation = "Self";
                    lst.Add(obj);
                    storecmbClaimOf.DataSource = lst;
                    storecmbClaimOf.DataBind();
                }
            }
            else
            {
                storecmbClaimOf.DataSource = "";
                storecmbClaimOf.DataBind();
            }
        }

        public void btnSave_Click(object sender, DirectEventArgs e)
        {


            DAL.SCClaim obj = new DAL.SCClaim();
            bool isInsert = string.IsNullOrEmpty(hiddenID.Text.Trim());
            if (!isInsert)
                obj.ClaimID = int.Parse(hiddenID.Text.Trim());

            if (cmbClaimType.SelectedItem != null && cmbClaimType.SelectedItem.Value != null)
                obj.ClaimType = int.Parse(cmbClaimType.SelectedItem.Value);


            if (SessionManager.CurrentLoggedInEmployeeId == 0 && hdnEmployeeId.RawValue != null)//nonadmin
            {
                if (hdnEmployeeId.Text == null || hdnEmployeeId.Text == "")
                {
                    NewMessage.ShowWarningMessage("Please select employee !");
                    return;
                }
                obj.EmployeeId = Convert.ToInt32(hdnEmployeeId.Text);
            }

            else
                obj.EmployeeId = SessionManager.CurrentLoggedInEmployeeId;



            if (!String.IsNullOrEmpty(dfClaimDate.Text))
                obj.ClaimDate = dfClaimDate.SelectedDate;
            else
            {
                NewMessage.ShowWarningMessage("Claim date is required !");
                return;
            }

            if (cmbClaimOf.SelectedItem != null && cmbClaimOf.SelectedItem.Index != -1)
                obj.ClaimOfFamilyId = int.Parse(cmbClaimOf.SelectedItem.Value);

            if (cmbRelation.SelectedItem != null && cmbRelation.SelectedItem.Index != -1)
                obj.RelationId = int.Parse(cmbRelation.SelectedItem.Value);

            if (!string.IsNullOrEmpty(txtTotalClaim.Text))
                obj.TotalClaimAmount = Convert.ToDecimal(txtTotalClaim.Text);

            if (obj.ClaimType != null && obj.ClaimType == (int)ClaimType.Medical)
            {
                if (!String.IsNullOrEmpty(dfDiagnosisDate.Text) && !dfDiagnosisDate.IsEmpty)
                    obj.DiagnosisDate = dfDiagnosisDate.SelectedDate;
                else
                {
                    NewMessage.ShowWarningMessage("Diagnosis date is required !");
                    return;
                }

                if (!String.IsNullOrEmpty(txtDoctorName.Text))
                    obj.DoctorName = txtDoctorName.Text;
                else
                {
                    NewMessage.ShowWarningMessage("Doctor name is required !");
                    return;
                }



                if (!String.IsNullOrEmpty(txtDoctorAddress.Text))
                    obj.DoctorAddress = txtDoctorAddress.Text;



                if (!String.IsNullOrEmpty(txtHostpitalName.Text))
                    obj.HospitalNameAddress = txtHostpitalName.Text;
                else
                {
                    NewMessage.ShowWarningMessage("Hospital name is required !");
                    return;
                }


                if (!String.IsNullOrEmpty(txtConsultantName.Text))
                    obj.ConsultantName = txtConsultantName.Text;
                else
                {
                    NewMessage.ShowWarningMessage("Consultant name is required !");
                    return;
                }


                if (!String.IsNullOrEmpty(txtDetailsOfIllness.Text))
                    obj.Details = txtDetailsOfIllness.Text;
                else
                {
                    NewMessage.ShowWarningMessage("Illness details is required !");
                    return;
                }

            }


            if (obj.ClaimType != null && obj.ClaimType == (int)ClaimType.Accidental)
            {
                DateTime? dt = null;
                if (!String.IsNullOrEmpty(dfDateOfAccident.Text) && !dfDateOfAccident.IsEmpty)
                {
                    dt = dfDateOfAccident.SelectedDate;//CommonManager.GetEngDate(dfDateOfAccident.Text, SessionManager.IsEnglish);
                    TimeSpan time = tfttme.SelectedTime;
                    obj.AccidentDateTime = dt + time;
                }
                else
                {
                    NewMessage.ShowWarningMessage("Date of Accident is required !");
                    return;
                }

                if (!String.IsNullOrEmpty(txtLocationOfAccidents.Text))
                    obj.AccidentLocation = txtLocationOfAccidents.Text;
                else
                {
                    NewMessage.ShowWarningMessage("Location of accident is required !");
                    return;
                }

                if (!String.IsNullOrEmpty(txtDetailsOfAccident.Text))
                    obj.Details = txtDetailsOfAccident.Text;
                else
                {
                    NewMessage.ShowWarningMessage("Accident details is required !");
                    return;
                }

                if (!String.IsNullOrEmpty(txtDetailsOfTreatment.Text))
                    obj.DetailsOfTreatment = txtDetailsOfTreatment.Text;
                else
                {
                    NewMessage.ShowWarningMessage("Tratment details is required !");
                    return;
                }

            }
            Status status = HRManager.InsertUpdateClaimRequest(obj, isInsert);
            if (status.IsSuccess)
            {
                hiddenID.Text = "";
                hdnEmployeeId.Text = "";
                windowAddEditClaim.Hide();
                if (isInsert)
                    NewMessage.ShowNormalMessage("Information saved successfully.", "searchList();");
                else
                    NewMessage.ShowNormalMessage("Information updated successfully.", "searchList();");
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }

        }
        public void Initialise()
        {
            btnSave.Visible = true;

            storeCmbClaimType.DataSource = ExtControlHelper.GetTextValues(typeof(ClaimType));
            storeCmbClaimType.DataBind();
            cmbClaimType.SetValue(ClaimType.Medical);


            FixedValueFamilyRelation fr = new FixedValueFamilyRelation();
            fr.ID = 0;
            fr.Name = "Self";
            List<FixedValueFamilyRelation> Lfr = CommonManager.GetRelationList();
            Lfr.Add(fr);
            storeRelation.DataSource = Lfr;
            storeRelation.DataBind();

            int empid;
            if (SessionManager.CurrentLoggedInEmployeeId != 0)//nonadmin
            {
                cmbSearch.Hide();
                empid = SessionManager.CurrentLoggedInEmployeeId;


                EEmployee emp = new EmployeeManager().GetById(empid);

                //txtStaffId.Text = emp.EmployeeId.ToString();
                //txtStaffName.Text = emp.Name;
                //txtInsurencePolicyNo.Text = "";

                lblEININo.Text = "Employee ID : " + emp.EmployeeId;
                lblEName.Text = emp.Title + " " + emp.Name;

                hdnEmployeeId.Text = emp.EmployeeId.ToString();

                //GetFamilyMemberNameByRelation
                List<HFamily> lst = CommonManager.GetAllActiveFamilyListByEmpID(empid);
                HFamily obj = new HFamily();
                obj.FamilyId = 0;
                obj.Name = emp.Name;
                obj.Relation = "Self";
                lst.Add(obj);
                storecmbClaimOf.DataSource = lst;
                storecmbClaimOf.DataBind();
            }

            //else
            //{
            //    btnSave.Visible = false;

            //    empid = Convert.ToInt32(hdnEmployeeId.Text.Trim());

            //}
        }
    }
}