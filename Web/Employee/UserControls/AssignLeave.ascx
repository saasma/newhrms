﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AssignLeave.ascx.cs"
    Inherits="Web.UserControls.AssignLeave" %>
<script type="text/javascript" src="<%= ResolveUrl("~/Employee/override.js?v=") + Web.Helper.WebHelper.Version %>"></script>
<script type="text/javascript" language="javascript">

    var cboEmployee = null;

    Ext.onReady(
        function()
        {
            cboEmployee = <%=cboEmployee.ClientID %>;
            //            if( typeof(CompanyX) != 'undefined')
            //             CompanyX.cmbEmployee.store = cboEmployee.getStore();
        }
    );    
    function ValidatorGetValue(id) {
        var control;
        var ctrlId = id.replace('CompanyX.','');
        control = window.document.getElementById(ctrlId);
        if (typeof (control.value) == "string") {

            return control.value;
        }
        return ValidatorGetValueRecursive(control);
    }

    var HideDisplayDate = function () {

        var chkHourlyLeave = <%=chkHourlyLeave.ClientID%>;
            var dtTo = <%=dtTo.ClientID%>;
            var cboHour = <%=cboHour.ClientID%>;
            var startTime = <%=startTime.ClientID %>;
            var endTime = <%=endTime.ClientID %>;

            if(chkHourlyLeave.getValue())
            {
                //dtTo.hide();
                cboHour.show();
                startTime.show();
                endTime.show();
            }
            else
            {
                dtTo.show();
                cboHour.hide();
                startTime.hide();
                endTime.hide();
            }
        };

        var HideDisplayControl = function(){
            var chkHourlyLeave = <%=chkHourlyLeave.ClientID%>;
            var dtTo = <%=dtTo.ClientID%>;
            var cboHour = <%=cboHour.ClientID%>;
            var chkIsHalfDay = <%=chkIsHalfDay.ClientID%>;
            var chkHalfDayType = <%= chkHalfDayType.ClientID  %>;
            if(chkIsHalfDay.getValue())
            {
                dtTo.hide();
                //cboHour.hide();
                chkHalfDayType.show();
            }
            else
            {
                dtTo.show();
                chkHalfDayType.hide();
                //cboHour.hide();
            }

        };

        var ReloadGrid = function(){
            
        }

        // Enable or Disable the button for Implementing the function such as:
        // btnApprove:  For Approving Leave;
        // btnDeny:     For Denying Leave;
        // btnReDraft:  For Redrafting Leave;
        // If current logged in employee is allowed to implement function then 
        //      Controls will be enabled
        // Else
        //      Controls will be disabled.
        //        var DisableEnableControls = function(){
        //            if(hdIsAllowedToApprove.getValue() == "false"){
        //                btnApprove.disable();
        //                btnDeny.disable();
        //                btnReDraft.disable();
        //                btnAssign.disable();
        //            }
        //            else{
        //                btnApprove.enable();
        //                btnDeny.enable();
        //                btnReDraft.enable();
        //                btnAssign.enable();
        //            }
        //        };

      
        var chkIsHalfDay =null;
        var storeLeaveType= null;
        var txtStartDate = null;
        var txtEndDate = null;
        var lblTotalDaysValue = null;

        var cmbLeaveType2;
        var    cmbLeaveType2;
        var     txtLeaveType1;
        var     txtLeaveType2;
        var    lblLeaveType1;
        var    lblLeaveType2;
        var    txtEndDate;
        var    lblTotalDaysValue;
        var    lblTotalDaysText;
        var   cmbAMPM;
        var chkIsHalfDay;
        var ulCompensatoryLeave =null;
        //        var cmbChildLeaves = null;
        Ext.onReady(function () {
            
            ulCompensatoryLeave = <%=ulCompensatoryLeave.ClientID %>;
            lblTotalDaysValue = <%=lblTotalDaysValue.ClientID %>;
            txtStartDate = <%=dtFrom.ClientID %>;
            txtEndDate = <%=dtTo.ClientID %>;
            chkIsHalfDay = <%= chkIsHalfDay.ClientID %>;
            storeLeaveType = <%= storeLeaveType.ClientID %>;
            btnReDraft = <%=btnReDraft.ClientID%>;
            btnDeny = <%=btnDeny.ClientID%>;
            btnApprove  = <%=btnApprove.ClientID%>;
            btnAssign = <%=btnAssign.ClientID%>;
            hdIsAllowedToApprove = <%=hdIsAllowedToApprove.ClientID%>;


            cmbLeaveType2 = <%=cboLeaveType2.ClientID %>;
            txtLeaveType1 = <%=txtLeaveType1.ClientID %>;
            txtLeaveType2 = <%=txtLeaveType2.ClientID %>;
            lblLeaveType1 = <%=lblLeaveType1.ClientID %>;
            lblLeaveType2 = <%=lblLeaveType2.ClientID %>;
            lblTotalDaysValue = <%=lblTotalDaysValue.ClientID %>;
            lblTotalDaysText = <%=lblTotalDaysText.ClientID %>;
            cmbAMPM = <%=chkHalfDayType.ClientID %>;
            chkIsHalfDay = <%=chkIsHalfDay.ClientID %>;
        });

        function clearTimeFields()
        {
            <%=startTime.ClientID %>.clearValue();
        <%=endTime.ClientID %>.clearValue();
    }

    function halfDayLeaveChange()
    {
        

        // if(cmbChildLeaves.hidden == false)
        {
            if(chkIsHalfDay.getValue()==true)
            {
                cmbAMPM.show();   
            }
            else
            {
                cmbAMPM.hide();   
            }
        }
        if( chkIsHalfDay.getValue()==true)
        {
            cmbLeaveType2.clearValue();
            cmbLeaveType2.hide();
            txtLeaveType1.hide();
            txtLeaveType2.hide();
            lblLeaveType1.hide();
            lblLeaveType2.hide();
            txtEndDate.hide();
            lblTotalDaysValue.hide();
            lblTotalDaysText.hide();
            cmbAMPM.show();
        }
        else
        {
            if( typeof(hasHourLeave) != 'undefined' && hasHourLeave)
            {
            }
            else
            {
                cmbLeaveType2.show();
                lblTotalDaysValue.show();
                lblTotalDaysText.show();
            }
            txtLeaveType1.show();
            txtLeaveType2.show();
            lblLeaveType1.show();
            lblLeaveType2.show();
            txtEndDate.show();
            
            cmbAMPM.hide();

            if(cmbLeaveType2.getValue()==null)
            {
                txtLeaveType1.hide();
                txtLeaveType2.hide();
                lblLeaveType1.hide();
                lblLeaveType2.hide();
            }
            else{
                txtLeaveType1.show();
                txtLeaveType2.show();
                lblLeaveType1.show();
                lblLeaveType2.show();
            }
        }

        // show for dish home only
        if(typeof(WhichCompany) != 'undefined' && WhichCompany == 27)
        {
            cmbLeaveType2.show();
        }
        else
        {
            cmbLeaveType2.hide();
        }
    }


    function BindEmployeeId()
    {
        var val = <%= cmbSubstitute.ClientID %>.getValue();
        <%= hdnEmployeeId.ClientID %>.setValue(val);
    }

</script>
<script type="text/javascript">


    var leaveTypeChanged = function (e1, e2) {

        var r = storeLeaveType.getAt(e2[0].index);
        if (r.data.IsHalfDayAllowed) {

            chkIsHalfDay.enable();
        }
        else {
            chkIsHalfDay.setValue(false);
            chkIsHalfDay.disable();
        }

        if (r.data.FreqOfAccrual == "Compensatory") {
            if (typeof (WhichCompany) != 'undefined' && WhichCompany == 34) {
            }
            else
                ulCompensatoryLeave.style.display = 'block';

            cmbLeaveType2.hide();
            txtLeaveType2.hide();
            lblLeaveType2.hide();
            cmbLeaveType2.clearValue();

            txtLeaveType1.hide();
            lblLeaveType1.hide();
        }
        else {
            ulCompensatoryLeave.style.display = 'none';

            if (typeof (hasHourLeave) != 'undefined' && hasHourLeave) {
            }
            else {
                // show for dish home only
                if(typeof(WhichCompany) != 'undefined' && WhichCompany == 27)
                {
                    cmbLeaveType2.show();
                }
                else
                {
                    cmbLeaveType2.hide();
                }
            }

        }

        //cmbChildLeaves.hide();
        if (typeof (WhichCompany) != 'undefined' && WhichCompany == 34) {
            cmbLeaveType2.hide();


        }


        setLeaveDays();

    }

    function setLeaveDays() {
        var from = txtStartDate.getRawValue()
        var to = txtEndDate.getRawValue()
        var leaveTypeId1 = <%=cboLeaveType.ClientID %>.getValue();
        var empId = <%=cboEmployee.ClientID %>.getValue();

        if( typeof(Ext.net.DirectMethods) != 'undefined')
        {
            var value = Ext.net.DirectMethods.GetDaysForLeave
            (from, to, leaveTypeId1,empId,
                {
                    success: function (result) {
                        lblTotalDaysValue.setValue(result);
                    }
                }
            );
        }
        else
        {
            var value = CompanyX.direct.GetDaysForLeave
            (from, to, leaveTypeId1,empId,
                {
                    success: function (result) {
                        lblTotalDaysValue.setValue(result);
                    }
                }
            );
        }
    }
    function datechange() {
        // if disabled do not change anything
        if (txtStartDate.readOnly == true)
            return;

        setLeaveDays();
        //        var start = txtStartDate.getRawValue();
        //        var end = txtEndDate.getRawValue();
        //        if (start != "" && end != "") {
        //            var date1 = new Date(start);
        //            var date2 = new Date(end);
        //            var one_day = 1000 * 60 * 60 * 24;
        //            var _Diff = Math.ceil((date2.getTime() - date1.getTime()) / (one_day));

        //            if (_Diff >= 0)
        //                _Diff += 1;

        //            if (isNaN(_Diff))
        //                _Diff = 0;

        //            lblTotalDaysValue.setValue(_Diff);
        //        }
    }

    function CheckValidationCtlCancel()
    {
        if( <%=txtReasonCancel.ClientID %>.getValue()=="")
        {
            Ext.Msg.show({title:"Warning",buttons:Ext.Msg.OK,icon:Ext.Msg.WARNING,msg:"Cancel Reason is required."});
            return false;
        }

       

        return true;
    }
</script>
<style type="text/css">
    .fieldTable1 > tbody > tr > td {
        padding: 2px !important;
    }

    .itemList {
        margin: 0px;
        padding: 0px;
    }

        .itemList li {
            float: left;
            list-style-type: none;
        }

    #ctl00_ContentPlaceHolder_Main_ctl00_txtComment-inputEl {
        height: 42px !important;
    }

    .textAreaHeight textarea {
        height: 38px !important;
    }
</style>
<ext:Hidden ID="txtFromDate" Text="" runat="server" />
<ext:Hidden ID="hdCompanyHasHourlyLeave" runat="server" />
<ext:Hidden ID="hdStartDateEng" runat="server" />
<ext:Hidden ID="hdEndDateEng" runat="server" />
<ext:Hidden ID="hdEmployeeId" Text="0" runat="server" />
<ext:Hidden ID="hdLeaveRequestId" Text="0" runat="server" />
<ext:Hidden ID="hdIsAllowedToApprove" runat="server" />
<ext:Hidden ID="hdnEmployeeId" runat="server" />
<ext:Store ID="storeLeaveType" runat="server">
    <Model>
        <ext:Model ID="Model2" runat="server" IDProperty="LeaveTypeId">
            <Fields>
                <ext:ModelField Name="LeaveTypeId" Type="String" />
                <ext:ModelField Name="Title" />
                <ext:ModelField Name="IsHalfDayAllowed" Type="Boolean" />
                <ext:ModelField Name="FreqOfAccrual" Type="String" />
                <ext:ModelField Name="IsParentGroupLeave" Type="Boolean" />
            </Fields>
        </ext:Model>
    </Model>
</ext:Store>
<ext:Window runat="server" Shadow="false" Modal="true" Hidden="true" Width="300"
    Height="200" Title="Forward to next Reviewer" ID="windowReview">
    <Content>
        <div style="padding: 10px">
            <table>
                <tr>
                    <td colspan="2">
                        <ext:ComboBox ID="cmbReviewerList" LabelAlign="Top" DisplayField="Text" ValueField="Value"
                            LabelSeparator="" ForceSelection="true" Width="200" runat="server" FieldLabel="Reviewer *">
                            <Store>
                                <ext:Store ID="store1" runat="server">
                                    <Model>
                                        <ext:Model ID="Model7" runat="server" IDProperty="Value">
                                            <Fields>
                                                <ext:ModelField Name="Value" Type="String" />
                                                <ext:ModelField Name="Text" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                            <SelectedItems>
                                <ext:ListItem Index="0" />
                            </SelectedItems>
                        </ext:ComboBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="Reviewer is required."
                            ControlToValidate="cmbReviewerList" ValidationGroup="ForwardForReview" Display="None" />
                    </td>
                </tr>
                <tr>
                    <td style="padding-top: 10px">
                        <ext:Button ID="btnForward" runat="server" Width="90" StyleSpec="margin-right:5px;"
                            Text="Forward">
                            <DirectEvents>
                                <Click OnEvent="btnForward_Click">
                                    <Confirmation ConfirmRequest="true" Message="Are you sure to forward the leave for Review?" />
                                    <EventMask ShowMask="true" />
                                </Click>
                            </DirectEvents>
                            <Listeners>
                                <Click Handler="valGroup='ForwardForReview';return CheckValidation();" />
                            </Listeners>
                        </ext:Button>
                    </td>
                    <td style="padding-top: 10px">
                        <ext:Button ID="Button2" runat="server" Width="90" StyleSpec="margin-right:5px;"
                            Text="Close">
                            <Listeners>
                                <Click Handler="#{windowReview}.hide();" />
                            </Listeners>
                        </ext:Button>
                    </td>
                </tr>
            </table>
        </div>
    </Content>
</ext:Window>
<ext:Window ID="wdoLeaveApproval" Layout="BorderLayout" BodyStyle="background-color:#F3F6FE"
    runat="server" Hidden="false" Padding="0" Height="520" Width="998px" Title="Assign Leave"
    Modal="false" X="-1000" Y="-1000" Resizable="true">
    <Listeners>
        <Hide Handler="typeof(clearAjaxHistory) != 'undefined' ? clearAjaxHistory() : '';" />
    </Listeners>
    <Items>
        <ext:Container StyleSpec="background-color: #F3F6FE;padding-top:10px" Region="North" runat="server">
            <Content>
                <table style="background-color: #F3F6FE;">
                    <tr>
                        <td valign="top" style="padding-left: 4px;">
                            <table class="fieldTable1" style="background-color: #F3F6FE;">
                                <tbody>
                                    <tr>
                                        <td colspan="2">
                                            <ext:Store ID="storeEmployee" runat="server">
                                                <Model>
                                                    <ext:Model ID="Model1" IDProperty="Value" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="Value" Type="String" />
                                                            <ext:ModelField Name="Text" />
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                            <ext:ComboBox LabelSeparator="" ReadOnlyCls="readonlyClass" ID="cboEmployee" runat="server"
                                                FieldLabel="Employee Name" DisplayField="Text" ValueField="Value" Width="350"
                                                ForceSelection="true" StoreID="storeEmployee" QueryMode="Local">
                                                <DirectEvents>
                                                    <Select OnEvent="cboEmployee_Select">
                                                        <EventMask ShowMask="true" />
                                                    </Select>
                                                </DirectEvents>
                                            </ext:ComboBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Employee is required."
                                                ControlToValidate="cboEmployee" ValidationGroup="AssignLeave" Display="None" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <ext:DisplayField LabelSeparator="" ID="lblDesignation"  runat="server" FieldLabel="Designation">
                                            </ext:DisplayField>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <ext:DisplayField LabelSeparator="" ID="lblBranch"  runat="server" FieldLabel="Branch">
                                            </ext:DisplayField>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <ul class="itemList">
                                                <li>
                                                    <ext:ComboBox ReadOnlyCls="readonlyClass" LabelSeparator="" ID="cboLeaveType" runat="server"
                                                        FieldLabel="Leave Type" DisplayField="Title" ValueField="LeaveTypeId" Width="280"
                                                        ForceSelection="true" StoreID="storeLeaveType" EmptyText="" Editable="false">
                                                        <Listeners>
                                                            <Select Fn="leaveTypeChanged" />
                                                        </Listeners>
                                                    </ext:ComboBox>
                                                </li>
                                                <li>
                                                    <ext:NumberField ReadOnlyCls="readonlyClass" MinValue="0" AllowDecimals="false" runat="server"
                                                        LabelSeparator="" LabelWidth="5" Width="60" Text="0" FieldLabel="&nbsp;" LabelAlign="Left"
                                                        ID="txtLeaveType1">
                                                    </ext:NumberField>
                                                </li>
                                                <li>
                                                    <ext:DisplayField runat="server" LabelSeparator="" LabelWidth="5" Width="50" Text="day"
                                                        FieldLabel="&nbsp;" LabelAlign="Left" ID="lblLeaveType1" />
                                                </li>
                                            </ul>
                                        </td>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Leave type is required."
                                            ControlToValidate="cboLeaveType" ValidationGroup="AssignLeave" Display="None" />
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <ul class="itemList">
                                                <li>
                                                    <ul class="itemList" runat="server" style="display: none; margin-left: 25px;" id="ulCompensatoryLeave">
                                                        <li style="margin-left: 80px;">
                                                            <ext:Radio ReadOnlyCls="readonlyClass" LabelSeparator="" LabelWidth="40" Name="Comensatory"
                                                                ID="compensatoryIsAdd" BoxLabel="Add" runat="server" />
                                                        </li>
                                                        <li style="padding-left: 15px">
                                                            <ext:Radio ReadOnlyCls="readonlyClass" LabelSeparator="" Checked="true" LabelWidth="40"
                                                                Name="Comensatory" ID="compensatoryIsDeduct" BoxLabel="Deduct" runat="server" />
                                                        </li>
                                                    </ul>
                                                    <ext:ComboBox ReadOnlyCls="readonlyClass" ID="cboLeaveType2" LabelSeparator="" runat="server"
                                                        FieldLabel="&nbsp;" DisplayField="Title" ValueField="LeaveTypeId" Width="280"
                                                        ForceSelection="true" StoreID="storeLeaveType" EmptyText="" Editable="false">
                                                        <Triggers>
                                                            <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                                                        </Triggers>
                                                        <Listeners>
                                                            <Select Handler="this.getTrigger(0).show();halfDayLeaveChange();" />
                                                            <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                                            <TriggerClick Handler="if (index == 0) { 
                                                       this.clearValue(); 
                                                       this.getTrigger(0).hide();
                                                   halfDayLeaveChange();}" />
                                                        </Listeners>
                                                    </ext:ComboBox>
                                                </li>
                                                <li>
                                                    <ext:NumberField ReadOnlyCls="readonlyClass" MinValue="0" AllowDecimals="false" runat="server"
                                                        LabelSeparator="" LabelWidth="5" Width="70" Text="0" FieldLabel="&nbsp;" LabelAlign="Left"
                                                        ID="txtLeaveType2" />
                                                </li>
                                                <li>
                                                    <ext:DisplayField runat="server" LabelSeparator="" LabelWidth="5" Width="50" Text="day"
                                                        FieldLabel="&nbsp;" LabelAlign="Left" ID="lblLeaveType2" />
                                                </li>
                                            </ul>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <ext:Checkbox ReadOnlyCls="readonlyClass" LabelSeparator="" ID="chkHourlyLeave" runat="server"
                                                FieldLabel="Hourly">
                                                <%-- FieldLabel="Is Hourly Leave">--%>
                                                <Listeners>
                                                    <Change Handler="HideDisplayDate();" />
                                                </Listeners>
                                            </ext:Checkbox>
                                        </td>
                                    </tr>
                                    <tr runat="server" id="rowHalfDay">
                                        <td runat="server" id="tdHalfDay">
                                            <ext:Checkbox ReadOnlyCls="readonlyClass" LabelSeparator="" ID="chkIsHalfDay" runat="server"
                                                FieldLabel="Half Day">
                                                <Listeners>
                                                    <Change Handler="halfDayLeaveChange();" />
                                                </Listeners>
                                            </ext:Checkbox>
                                        </td>
                                        <td>
                                            <ext:ComboBox ReadOnlyCls="readonlyClass" LabelSeparator="" Hidden="true" SelectedIndex="0"
                                                LabelWidth="100" ForceSelection="true" ID="chkHalfDayType" runat="server" Width="210"
                                                FieldLabel="Half Day Type">
                                                <Items>
                                                    <ext:ListItem Text="First half" Value="AM" />
                                                    <ext:ListItem Text="Second half" Value="PM" />
                                                </Items>
                                                <SelectedItems>
                                                    <ext:ListItem Index="0" />
                                                </SelectedItems>
                                            </ext:ComboBox>
                                        </td>
                                    </tr>
                                    <tr runat="server" id="rowHour">
                                        <td>
                                            <ext:SpinnerField ReadOnlyCls="readonlyClass" LabelSeparator="" FieldClass="spWidth"
                                                AllowDecimals="false" Text="1" ID="cboHour" Width="170" runat="server" FieldLabel="Hours"
                                                MinValue="1" />
                                        </td>
                                        <td style="padding-left: 5px; float: left">
                                            <table>
                                                <tr>
                                                    <td>
                                                        <ext:TimeField ID="startTime" StyleSpec="margin-top:-2!important" runat="server"
                                                            Increment="15" Width="95" SelectedTime="09:00">
                                                            <%--  <Listeners>
                                                                <Select Handler="#{hiddenStartTime}.setValue(#{startTime}.getValue())" />
                                                            </Listeners>--%>
                                                        </ext:TimeField>
                                                        <ext:Hidden ID="hiddenStartTime" runat="server" />
                                                    </td>
                                                    <td style="padding-left: 5px">
                                                        <ext:TimeField ID="endTime" runat="server" Increment="15" Width="95" SelectedTime="09:00">
                                                            <%--<Listeners>
                                                                <Select Handler="#{hiddenEndTime}.setValue(#{endTime}.getValue())" />
                                                            </Listeners>--%>
                                                        </ext:TimeField>
                                                        <ext:Hidden ID="hiddenEndTime" runat="server" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style='width: 205px'>
                                            <ext:DateField ReadOnlyCls="readonlyClass" ID="dtFrom" LabelSeparator="" runat="server"
                                                Vtype="daterange" FieldLabel="From" Width="210">
                                                <CustomConfig>
                                                    <ext:ConfigItem Name="endDateField" Value="#{dtApprovalDate}" Mode="Value" />
                                                </CustomConfig>
                                                <Listeners>
                                                    <Change Handler="datechange();" />
                                                </Listeners>
                                                <Plugins>
                                                    <ext:GenericPlugin ID="HighlightDates1" runat="server" InstanceName="Ext.ux.DatePicker.HighlightWithNepDates" />
                                                </Plugins>
                                            </ext:DateField>
                                        </td>
                                        <td>
                                            <ul class="itemList">
                                                <li>
                                                    <ext:DateField ReadOnlyCls="readonlyClass" ID="dtTo" runat="server" Vtype="daterange"
                                                        LabelWidth="15" LabelSeparator="" FieldLabel="To" Width="120">
                                                        <CustomConfig>
                                                            <ext:ConfigItem Name="endDateField" Value="#{dtApprovalDate}" Mode="Value" />
                                                        </CustomConfig>
                                                        <Listeners>
                                                            <Change Handler="datechange();" />
                                                        </Listeners>
                                                        <Plugins>
                                                            <ext:GenericPlugin ID="GenericPlugin1" runat="server" InstanceName="Ext.ux.DatePicker.HighlightWithNepDates" />
                                                        </Plugins>
                                                    </ext:DateField>
                                                </li>
                                                <li>
                                                    <ext:DisplayField runat="server" LabelSeparator="" LabelWidth="10" Width="30" Text="0"
                                                        FieldLabel="&nbsp;" LabelAlign="Left" ID="lblTotalDaysValue" />
                                                </li>
                                                <li>
                                                    <ext:DisplayField runat="server" LabelSeparator="" LabelWidth="0" Width="30" Text="day"
                                                        FieldLabel="&nbsp;" LabelAlign="Left" ID="lblTotalDaysText" />
                                                </li>
                                            </ul>
                                        </td>
                                    </tr>
                                    <tr runat="server" id="rowSpecialCase" visible="true">
                                        <td colspan="2">
                                            <table>
                                                <tr>
                                                    <td>
                                                        <ext:Checkbox ReadOnlyCls="readonlyClass" Hidden="false" ID="chkIsSpecialCase" LabelSeparator=""
                                                            runat="server" FieldLabel="Special case">
                                                        </ext:Checkbox>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <ext:TextField ReadOnlyCls="readonlyClass" ID="txtSpecialComment" FieldStyle="border:1px solid #a2b4c6;"
                                                runat="server" LabelAlign="Left" LabelSeparator="" FieldLabel="Note" Width="410" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <ext:TextArea ReadOnlyCls="readonlyClass" FieldStyle="border:1px solid #a2b4c6;"
                                                LabelSeparator="" ID="txtReason" runat="server" FieldLabel="Reason *" Width="410"  Height="45"/>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Reason is required."
                                                ControlToValidate="txtReason" ValidationGroup="AssignLeave" Display="None" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <ext:Store runat="server" ID="storeSearch" AutoLoad="false">
                                                <Proxy>
                                                    <ext:AjaxProxy Url="../../Handler/EmpSearch.ashx">
                                                        <ActionMethods Read="GET" />
                                                        <Reader>
                                                            <ext:JsonReader Root="plants" TotalProperty="total" />
                                                        </Reader>
                                                    </ext:AjaxProxy>
                                                </Proxy>
                                                <Model>
                                                    <ext:Model ID="Model9" IDProperty="Name" Name="ItemLineModel" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="Name" Type="String" />
                                                            <ext:ModelField Name="EmployeeId" Type="Int" />
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                            <ext:ComboBox ReadOnlyCls="readonlyClass" LabelSeparator="" ID="cmbSubstitute" FieldLabel="Substitute"
                                                LabelAlign="Left" runat="server" DisplayField="Name" ValueField="EmployeeId"
                                                StoreID="storeSearch" TypeAhead="false" Width="350" PageSize="9999" HideBaseTrigger="true"
                                                MinChars="1" TriggerAction="All" ForceSelection="true">
                                                <ListConfig LoadingText="Searching..." MinWidth="200" StyleSpec="border-top:1px solid #98c0f4;">
                                                    <ItemTpl ID="ItemTpl1" runat="server">
                                                        <Html>
                                                            <tpl>
                                                                <div class="search-item">
                                                                                <span>{Name}</span>  
                                                                 </div>
					                                    </tpl>
                                                        </Html>
                                                    </ItemTpl>
                                                </ListConfig>
                                                <Triggers>
                                                    <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                                                </Triggers>
                                                <Listeners>
                                                    <Select Handler="this.getTrigger(0).show();BindEmployeeId();" />
                                                    <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                                    <TriggerClick Handler="if (index == 0) { 
                                                               this.clearValue(); 
                                                               this.getTrigger(0).hide();
                                                           }" />
                                                </Listeners>
                                            </ext:ComboBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">

                                             <ext:DisplayField LabelSeparator="" ID="lblSubstituteInfo" LabelStyle="font-weight:bold"
                                                runat="server" LabelAlign="Left" FieldLabel="&nbsp" Text="">
                                            </ext:DisplayField>

                                        </td>

                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <ext:DisplayField LabelSeparator="" ID="lblReviewTo" LabelStyle="font-weight:bold"
                                                runat="server" LabelAlign="Left" FieldLabel="Review">
                                            </ext:DisplayField>
                                            <ext:ComboBox ID="cmbReviewTo" DisplayField="Text" ValueField="Value" Hidden="true"
                                                LabelSeparator="" QueryMode="Local" ForceSelection="true" Width="350" LabelStyle="font-weight:bold"
                                                LabelWidth="100" runat="server" FieldLabel="Review">
                                                <Store>
                                                    <ext:Store ID="store3" runat="server">
                                                        <Model>
                                                            <ext:Model ID="Model4" runat="server" IDProperty="Value">
                                                                <Fields>
                                                                    <ext:ModelField Name="Value" Type="String" />
                                                                    <ext:ModelField Name="Text" />
                                                                </Fields>
                                                            </ext:Model>
                                                        </Model>
                                                    </ext:Store>
                                                </Store>
                                                <SelectedItems>
                                                    <ext:ListItem Index="0" />
                                                </SelectedItems>
                                            </ext:ComboBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <ext:DisplayField LabelSeparator="" ID="lblApplyTo" LabelStyle="font-weight:bold"
                                                runat="server" LabelAlign="Left" FieldLabel="Approval">
                                            </ext:DisplayField>
                                            <ext:ComboBox ID="cmbApplyTo" DisplayField="Text" ValueField="Value" Hidden="true"
                                                LabelSeparator="" QueryMode="Local" ForceSelection="true" Width="350" LabelStyle="font-weight:bold"
                                                LabelWidth="100" runat="server" FieldLabel="Approval">
                                                <Store>
                                                    <ext:Store ID="store4" runat="server">
                                                        <Model>
                                                            <ext:Model ID="Model5" runat="server" IDProperty="Value">
                                                                <Fields>
                                                                    <ext:ModelField Name="Value" Type="String" />
                                                                    <ext:ModelField Name="Text" />
                                                                </Fields>
                                                            </ext:Model>
                                                        </Model>
                                                    </ext:Store>
                                                </Store>
                                                <SelectedItems>
                                                    <ext:ListItem Index="0" />
                                                </SelectedItems>
                                            </ext:ComboBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <%--<ext:TextField ID="txtComment" runat="server" FieldLabel="Comment" Width="480" Height="35" />--%>
                                            <ext:TextArea ReadOnlyCls="readonlyClass" LabelSeparator="" ID="txtComment" runat="server"
                                                FieldLabel="Your comment" Width="410" Cls="textAreaHeight" Height="25px" />
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="Comment is required."
                                                ControlToValidate="txtComment" ValidationGroup="AssignLeaveVal" Display="None" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <ext:DisplayField LabelSeparator="" ID="lblStatus" FieldStyle="font-weight:bold"
                                                StyleSpec="font-weight:bold;" runat="server" FieldLabel="Status">
                                            </ext:DisplayField>
                                        </td>
                                    </tr>


                                    <%--<tr>
                                        <td colspan="2" style="padding-top: 0px!important; color: #3892D3; font-weight: bold!important;">
                                            Leaves Pending Approval
                                        </td>
                                    </tr>--%>
                                </tbody>
                            </table>
                        </td>
                        <td valign="top">
                            <div style="padding-bottom: 10px; padding-left: 10px; clear: both; float: right; height: 360; background-color: #F3F6FE;"
                                id="divGrid" runat="server">
                            </div>
                            <table style="padding-top: 15px; margin-left: 10px; clear: both;">
                                <tr>
                                    <td>
                                        <ext:Button ID="btnRecommend" ToolTip="Recommend for Approval" Hidden="true" runat="server"
                                            Width="90" StyleSpec="margin-right:10px;" Text="Recommend">
                                            <DirectEvents>
                                                <Click OnEvent="btnRecommend_Click">
                                                    <Confirmation ConfirmRequest="true" Message="Are you sure to recommend the leave for Approval?" />
                                                    <EventMask ShowMask="true" />
                                                </Click>
                                            </DirectEvents>
                                        </ext:Button>
                                    </td>
                                    <td>
                                        <ext:Button ID="btnRecommendForReview" Hidden="true" ToolTip="Forward for Review"
                                            runat="server" Width="90" StyleSpec="margin-right:10px;" Text="Forward">
                                            <Listeners>
                                                <Click Handler="#{windowReview}.show();" />
                                            </Listeners>
                                        </ext:Button>
                                    </td>
                                    <td>
                                        <ext:Button ID="btnApprove" ToolTip="Approve the Leave" runat="server" Width="80"
                                            StyleSpec="margin-right:10px;" Text="Approve">
                                            <DirectEvents>
                                                <Click OnEvent="btnApprove_Click">
                                                    <Confirmation ConfirmRequest="true" Message="Confirm Approve the leave?" />
                                                    <EventMask ShowMask="true" />
                                                </Click>
                                            </DirectEvents>
                                        </ext:Button>
                                    </td>
                                    <td>
                                        <ext:Button ID="btnDeny" ToolTip="Deny the Leave" runat="server" Text="Deny" StyleSpec="margin-right:10px;"
                                            Width="80">
                                            <DirectEvents>
                                                <Click OnEvent="btnDeny_Click">
                                                    <Confirmation ConfirmRequest="true" Message="Confirm Deny the leave?" />
                                                    <EventMask ShowMask="true" />
                                                </Click>
                                            </DirectEvents>
                                        </ext:Button>
                                    </td>
                                    <td style="display: none">
                                        <ext:Button ID="btnReDraft" runat="server" Text="Re-Draft" StyleSpec="margin-right:10px;"
                                            Width="80">
                                            <DirectEvents>
                                                <Click OnEvent="btnReDraft_Click">
                                                    <Confirmation ConfirmRequest="true" Message="Confirm ReDraft the leave?" />
                                                    <EventMask ShowMask="true" />
                                                </Click>
                                            </DirectEvents>
                                            <Listeners>
                                                <Click Handler="valGroup='AssignLeaveVal';return CheckValidation();" />
                                            </Listeners>
                                        </ext:Button>
                                    </td>
                                    <%--<td>
                                        <ext:Button Hidden="true" ID="btnHRViewDelete" runat="server" Text="Delete" StyleSpec="margin-right:5px;"
                                            Width="80">
                                            <DirectEvents>
                                                <Click OnEvent="btnHRViewDelete_Click">
                                                    <Confirmation ConfirmRequest="true" Message="Confirm delete the leave?" />
                                                    <EventMask ShowMask="true" />
                                                </Click>
                                            </DirectEvents>
                                        </ext:Button>
                                    </td>--%>
                                </tr>
                                <tr>
                                    <td>
                                        <ext:Button ID="btnAssign" runat="server" Width="100" StyleSpec="margin-right:10px;"
                                            Text="Assign">
                                            <DirectEvents>
                                                <Click OnEvent="btnAssign_Click">
                                                    <Confirmation ConfirmRequest="true" Message="Confirm Assign the leave?" />
                                                    <EventMask ShowMask="true" />
                                                </Click>
                                            </DirectEvents>
                                            <Listeners>
                                                <Click Handler="valGroup='AssignLeave';return CheckValidation();" />
                                            </Listeners>
                                        </ext:Button>
                                    </td>
                                    <td>
                                        <ext:Button ID="btnCreateLeaveRequest" runat="server" Width="120" StyleSpec="margin-right:10px;"
                                            Text="Create Request">
                                            <DirectEvents>
                                                <Click OnEvent="btnCreateLeaveRequest_Click">
                                                    <Confirmation ConfirmRequest="true" Message="Confirm create leave request?" />
                                                    <EventMask ShowMask="true" />
                                                </Click>
                                            </DirectEvents>
                                            <Listeners>
                                                <Click Handler="valGroup='AssignLeave';return CheckValidation();" />
                                            </Listeners>
                                        </ext:Button>
                                    </td>
                                    <td>
                                        <ext:Button ID="btnActualCancel" ToolTip="Cancel the Approved Leave" runat="server"
                                            Width="100" StyleSpec="margin-right:10px;" Text="Cancel">
                                            <Menu>
                                                <ext:Menu runat="server">
                                                    <Items>
                                                        <ext:LinkButton ID="LinkButton1" ToolTip="Full Cancel" runat="server" Width="80"
                                                            StyleSpec="margin-right:10px;" Text="Full Cancel">
                                                            <DirectEvents>
                                                                <Click OnEvent="btnActualCancel_Click">
                                                                    <Confirmation ConfirmRequest="true" Message="Confirm Cancel the leave?" />
                                                                    <EventMask ShowMask="true" />
                                                                </Click>
                                                            </DirectEvents>
                                                            <Listeners>
                                                                <Click Handler="valGroup='AssignLeave';return CheckValidation();" />
                                                            </Listeners>
                                                        </ext:LinkButton>
                                                        <ext:LinkButton ID="btnPartialCancel" ToolTip="Partial Cancel" runat="server" Width="80"
                                                            StyleSpec="margin-right:10px;" Text="Partial Cancel">
                                                            <DirectEvents>
                                                                <Click OnEvent="btnPartialCancel_Click">

                                                                    <EventMask ShowMask="true" />
                                                                </Click>
                                                            </DirectEvents>
                                                        </ext:LinkButton>
                                                    </Items>
                                                </ext:Menu>
                                            </Menu>
                                        </ext:Button>
                                    </td>
                                    <td>
                                        <ext:Button ID="btnCancel" ToolTip="Close the Window" runat="server" Text="Close"
                                            StyleSpec="margin-right:5px;" Width="100">
                                            <Listeners>
                                                <Click Handler="#{wdoLeaveApproval}.hide();" />
                                            </Listeners>
                                        </ext:Button>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </Content>
        </ext:Container>
    </Items>
</ext:Window>
<asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="From date is required."
    ControlToValidate="dtFrom" ValidationGroup="AssignLeave" Display="None" />
<ext:Window runat="server" StyleSpec='z-index:99999999' Hidden="true" X="-1000" Y="-1000" ButtonAlign="Left"
    Title="Partial Cancel Request" ID="windowPartialCancelRequest" Modal="true"
    Width="510" Height="200">
    <Content>
        <div style="padding: 15px; padding-top: 5px;">
            <table class="tblDetails">
                <tr>
                    <td>
                        <ext:TextField LabelSeparator="" FieldLabel="Reason *" runat="server" ID="txtReasonCancel"
                            LabelWidth="75" Width="470" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <ul class="itemList" style='padding-top: 20px;'>
                            <li>
                                <ext:DateField LabelSeparator="" StyleSpec="200" FieldLabel="When *" runat="server"
                                    ID="txtStartDateCancel" LabelWidth="75" Width="180">
                                    <Plugins>
                                        <ext:GenericPlugin ID="GenericPlugin2" runat="server" InstanceName="Ext.ux.DatePicker.HighlightWithNepDates" />
                                    </Plugins>
                                </ext:DateField>
                            </li>
                            <li>
                                <ext:DateField FieldLabel="to" StyleSpec="margin-left:10px" LabelWidth="15" LabelSeparator=""
                                    runat="server" ID="txtEndDateCancel" Width="125">
                                    <Plugins>
                                        <ext:GenericPlugin ID="GenericPlugin3" runat="server" InstanceName="Ext.ux.DatePicker.HighlightWithNepDates" />
                                    </Plugins>
                                </ext:DateField>
                            </li>
                        </ul>
                    </td>
                </tr>
            </table>
        </div>
    </Content>
    <Buttons>
        <ext:Button ID="btnSavePartialCancel" Width="150" StyleSpec="margin-left:10px" Height="30"
            runat="server" Text="Save Cancellation" ValidationGroup="InsertUpdateCancel">
            <DirectEvents>
                <Click OnEvent="btnSavePartialCancel_Click">
                    <Confirmation ConfirmRequest="true" Message="Confirm cancel for the specified date?" />
                    <EventMask ShowMask="true" />
                </Click>
            </DirectEvents>
            <Listeners>
                <Click Handler="return CheckValidationCtlCancel();">
                </Click>
            </Listeners>
        </ext:Button>
        <ext:LinkButton ID="btnCloseCancel" StyleSpec="margin-left:10px;padding-top:5px;"
            Width="100" Height="30" runat="server" Text="Close" ValidationGroup="InsertUpdateCancel">
            <Listeners>
                <Click Handler="#{windowPartialCancelRequest}.hide();">
                </Click>
            </Listeners>
        </ext:LinkButton>
    </Buttons>
</ext:Window>
