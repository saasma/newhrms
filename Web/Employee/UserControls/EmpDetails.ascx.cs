﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using BLL.Manager;
using BLL;
using DAL;
using Utils.Helper;
using BLL.Entity;
using BLL.BO;
using Utils.Calendar;
using Utils;
using System.IO;
using Web.Helper;

namespace Web.Employee.UserControls
{
    public partial class EmpDetails : BaseUserControl
    {
        int employeeId = 0;

        public bool IsOtherEmployee
        {
            get;
            set;
        }

        protected void Page_Load(object sender, EventArgs e)
        {


            genderText.InnerHtml = string.Format(gender.InnerHtml, CommonManager.GetGenderName);
            HideEditLinks();
            if (Request.QueryString["id"] != null && Request.QueryString["id"].ToString() != "")
            {
                employeeId = int.Parse(Request.QueryString["id"].ToString());

                btnEdit.Visible = false;

                Setting setting = OvertimeManager.GetSetting();
                if (setting != null && setting.HideTeamHRDetails != null && setting.HideTeamHRDetails.Value)
                    Panel2.Hide();
            }
            else
                employeeId = SessionManager.CurrentLoggedInEmployeeId;

            // hide income list for manager if not nibl
            if (CommonManager.CompanySetting.WhichCompany != WhichCompany.NIBL &&
                SessionManager.CurrentLoggedInEmployeeId != 0
                && SessionManager.CurrentLoggedInEmployeeId != employeeId)
            {
                pnlIncomeList.Visible = false;
            }

            if (!X.IsAjaxRequest && !IsPostBack)
            {

                LoadEditData();

                if (CommonManager.CompanySetting.WhichCompany == WhichCompany.NIBL)
                    hdnCompanyName.Text = "nibl";
                else
                    hdnCompanyName.Text = "test";

                if (CommonManager.CompanySetting.WhichCompany == WhichCompany.Citizen)
                {
                    workingFor.Visible = false;
                    workingForImage.Visible = false;
                }
            }


        }

        private void LoadEditData()
        {
            if (IsOtherEmployee)
            {
                PanelDocument.Visible = false;
            }

            EEmployee emp = EmployeeManager.GetEmployeeById(employeeId);
            if (emp != null)
            {

                LoadBasicInfo(emp);
                LoadHireDetails(emp);
                LoadContactInfo(emp);
                LoadPersonalInfo(emp);
                LoadLevelGradeHistory(emp);

                levelGradeHistory.Visible = CommonManager.CompanySetting.HasLevelGradeSalary;

                actingHistory.Visible = CommonManager.CompanySetting.WhichCompany != WhichCompany.NIBL;

                family.HRef += emp.EmployeeId;
                prevEmployment.HRef += emp.EmployeeId;

                skill9.HRef += emp.EmployeeId;
                skill10.HRef += emp.EmployeeId;
                skill11.HRef += emp.EmployeeId;
                skill12.HRef += emp.EmployeeId;
                skill13.HRef += emp.EmployeeId;
                skill14.HRef += emp.EmployeeId;
                skill15.HRef += emp.EmployeeId;
                skill16.HRef += emp.EmployeeId;
                skill17.HRef += emp.EmployeeId;
                skill18.HRef += emp.EmployeeId;

                // nibl service history
                if (CommonManager.CompanySetting.WhichCompany == WhichCompany.NIBL)
                {
                    divStatusChange.Visible = false;
                    divBranchTransfer.Visible = false;

                    int total = 0;
                    // load service history
                    List<GetServiceHistoryNewResult> list = BranchManager.GetNewServiceHistory
              (0, 99999, ref total, "", employeeId, null, null, null);

                    gridServiceHistory.DataSource = list; //EmployeeManager.GetServiceHistoryList(employeeId, null, null, 0, 1000,null);
                    gridServiceHistory.DataBind();
                }
                else
                    divNIBLServiceHistory.Visible = false;





            }
        }


        protected void Tab_Change(object sender, DirectEventArgs e)
        {
            string TabName = hdnSelectedTabName.Text;
            LoadTabInfo(TabName.ToLower().Trim());
        }

        protected void LoadTabInfo(string TabName)
        {
            if (TabName == "pay")
            {
                ucEmpPayInfo.LoadInfo();
                ucEmpPayInsurannceCtl1.LoadInfo();
                EmpPayIncomeCtl1.LoadInfo();
            }

        }

        private void LoadContactInfo(EEmployee emp)
        {
            editContact.HRef += emp.EmployeeId;
            editAddress.HRef += emp.EmployeeId;


            if (emp.EAddresses.Count > 0)
            {
                // Address
                EAddress entity = emp.EAddresses[0];

                string present = "", permanent = "";
                WebHelper.GetAddressHTML(entity, ref present, ref permanent);
                addressTemporary.InnerHtml = present;
                addressPermanent.InnerHtml = permanent;


                if(!string.IsNullOrEmpty(entity.PSCitIssDis))
                {
                    rowCitizenIssueAddress.Visible = true;
                    spanCitizenIssueAddress.InnerHtml = entity.PSCitIssDis;
                }

                // Contact
                extNo.InnerHtml = entity.Extension;
                noPersonalNo.InnerHtml = entity.PersonalPhone;
                noPersonalMobile.InnerHtml = entity.PersonalMobile;
                contactEmergency.InnerHtml = entity.EmergencyName + (!string.IsNullOrEmpty(entity.EmergencyRelation) ? ", " + entity.EmergencyRelation : "");
                contactEmergencyNo.InnerHtml = entity.EmergencyPhone + (!string.IsNullOrEmpty(entity.EmergencyMobile) ? " / " + entity.EmergencyMobile : "");
            }
        }

        private void LoadBasicInfo(EEmployee emp)
        {

            title.InnerHtml = emp.Title + " " + emp.Name + " - " + (emp.EHumanResources.Count > 0 ? emp.EHumanResources[0].IdCardNo : "");

            // Designation/Position
            if (emp.EDesignation != null)
                positionDesignation.InnerHtml = emp.EDesignation.Name;
            // Group/Level
            if (emp.PEmployeeIncomes.Count > 0 && emp.PEmployeeIncomes[0].LevelId != null)
            {
                BLevel level = NewPayrollManager.GetLevelById(emp.PEmployeeIncomes[0].LevelId.Value);
                BLevel actingLevel = NewPayrollManager.GetActingLevelInTodayDate(emp.EmployeeId);
                if (level != null)
                    groupLevel.InnerHtml = level.BLevelGroup.Name + " (Level - " + level.Name +
                         (actingLevel == null ? "" : ", Acting Level - " + actingLevel.Name)
                        + ")";
            }
            else
                groupLevelImg.Visible = false;

            // Branch
            GetEmployeeCurrentBranchDepartmentResult currentBranchDep = EmployeeManager.GetEmployeeCurrentBranchDepartment(emp.EmployeeId);
            if (currentBranchDep != null)
            {
                branch.InnerHtml = currentBranchDep.Department + ", " + currentBranchDep.Branch;
            }

            // Contact
            if (emp.EAddresses.Count > 0)
            {
                EAddress entity = emp.EAddresses[0];
                // Mobile
                //if (EmployeeManager.IsBranchOrDepartmentOrRegionalHead(emp.EmployeeId))
                //{
                if (!string.IsNullOrEmpty(entity.CIMobileNo))
                {
                    contactMobile.InnerHtml = entity.CIMobileNo + " (O)";

                    if (!string.IsNullOrEmpty(entity.PersonalMobile))
                    {
                        contactMobile.InnerHtml += " " + entity.PersonalMobile + " (P)";
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(entity.PersonalMobile))
                    {
                        contactMobile.InnerHtml += entity.PersonalMobile + " (P)";
                    }
                }
                contactMobile.InnerHtml += "&nbsp;";
                //}
                //else
                //{
                //    contactMobile.Visible = false;
                //    imgMobile.Visible = false;
                //}

                // Phone
                if (!string.IsNullOrEmpty(entity.CIPhoneNo))
                    contactPhone.InnerHtml = entity.CIPhoneNo;
                if (!string.IsNullOrEmpty(entity.Extension))
                    contactPhone.InnerHtml += " - " + entity.Extension;
                if (contactPhone.InnerHtml != "")
                    contactPhone.InnerHtml += " (O)";

                if (!string.IsNullOrEmpty(entity.PersonalPhone))
                    contactPhone.InnerHtml += " " + entity.PersonalPhone + " (R)";

                contactPhone.InnerHtml += "&nbsp;";

                email.InnerHtml += string.Format("<a href='mailto:{0}'>{0}</a> ", entity.CIEmail);
            }

            // Working for since
            int years, months, days, hours;
            int minutes, seconds, milliseconds;
            ServicePeroid firstStatus = EmployeeManager.GetServicePeroidDate(emp.EmployeeId);
            if (firstStatus != null)
            {
                NewHelper.GetElapsedTime(firstStatus.FromDateEng, firstStatus.ToDateEng.Value, out years, out months, out days, out hours, out minutes, out seconds, out milliseconds);
                string text = "";
                if (years != 0)
                    text = " " + years + " years";
                if (months != 0)
                    text += " " + months + " months";
                if (days != 0)
                    text += " " + days + " days";

                workingFor.InnerHtml += text;

                workingFor.InnerHtml += ", Since " +
                    (IsEnglish ? firstStatus.FromDateEng.ToShortDateString() : firstStatus.FromDate + " (" + firstStatus.FromDateEng.ToShortDateString() + ")");

            }

            // Photo
            bool hasPhoto = false;
            if (emp.HHumanResources.Count > 0)
            {
                if (!string.IsNullOrEmpty(emp.HHumanResources[0].UrlPhotoThumbnail))
                {
                    image.ImageUrl = Page.ResolveUrl(Config.UploadLocation + "/" + emp.HHumanResources[0].UrlPhotoThumbnail);
                    if (File.Exists(Server.MapPath(image.ImageUrl)))
                        hasPhoto = true;
                }
            }
            if (!hasPhoto)
            {
                image.ImageUrl = Page.ResolveUrl(Web.Helper.WebHelper.GetEmployeeNoImage(emp.EmployeeId));
            }
        }

        private void LoadHireDetails(EEmployee emp)
        {

            //empPostionAndGrade.HRef += emp.EmployeeId;
            // Group/Level
            int? levelId = null;
            double grade = 0;
            NewHRManager.SetEmployeeJoiningLevelGrade(ref levelId, ref grade, emp.EmployeeId);
            if (levelId != null)
            {
                BLevel levelEntity = NewPayrollManager.GetLevelById(levelId.Value);
                level.InnerHtml = levelEntity.BLevelGroup.Name + " - " + levelEntity.Name;
                gradeDisplay.InnerHtml = grade.ToString();

                //designationChange.Visible = false;
                //salaryChange.Visible = false;
            }
            else
            {
                if (emp.EDesignation.BLevel != null)
                    level.InnerHtml = emp.EDesignation.BLevel.BLevelGroup.Name + " - " + emp.EDesignation.BLevel.Name;
                if (CommonManager.Setting.ShowGradeStep != null && CommonManager.Setting.ShowGradeStep.Value)
                {
                    if (emp.EGrade != null)
                        gradeDisplay.InnerHtml = emp.EGrade.Name;
                }
                else
                {
                    trGrade.Visible = false;
                }
                //divLevelGradeHistory.Visible = false;

            }


            if (emp.EHumanResources.Count > 0)
            {
                if (emp.EHumanResources[0].HireMethod != null)
                    hireMethod.InnerHtml = EmployeeManager.GetHireMethod(emp.EHumanResources[0].HireMethod.Value).HireMethodName;

            }

            ServicePeroid first = EmployeeManager.GetServicePeroidDate(emp.EmployeeId);
            if (first != null)
                hireDate.InnerHtml = first.FromDate;

            ECurrentStatus empLastStatus = new EmployeeManager().GetCurrentLastStatus(emp.EmployeeId);
            if (empLastStatus != null)
            {
                currentJobStatus.InnerHtml = empLastStatus.CurrentStatusText;

                if (!string.IsNullOrEmpty(empLastStatus.ToDate))
                    contractEndDate.InnerHtml = empLastStatus.ToDate;

            }
        }

        private void LoadPersonalInfo(EEmployee emp)
        {
            ein.InnerHtml = emp.EHumanResources.Count > 0 ? emp.EHumanResources[0].IdCardNo : "";
            gender.InnerHtml = new Gender().GetText(emp.Gender.Value);
            dob.InnerHtml = emp.DateOfBirth;
            maritalStatus.InnerHtml = emp.MaritalStatus;
            fatherName.InnerHtml = emp.FatherName;



            editPersonalInfo.HRef += emp.EmployeeId;
        }

        private void LoadLevelGradeHistory(EEmployee emp)
        {
            gridLevelGradeHistory.Store[0].DataSource = NewHRManager.GetLevelGradeHistory(emp.EmployeeId);
            gridLevelGradeHistory.Store[0].DataBind();
        }

        private void HideEditLinks()
        {
            editPersonalInfo.Visible = false;
            editAddress.Visible = false;
            editContact.Visible = false;
            health.Visible = false;
            family.Visible = false;
            prevEmployment.Visible = false;
            skill9.Visible = false;
            skill10.Visible = false;
            skill11.Visible = false;
            skill12.Visible = false;
            skill13.Visible = false;
            skill14.Visible = false;
            skill15.Visible = false;
            skill16.Visible = false;
        }
    }
}