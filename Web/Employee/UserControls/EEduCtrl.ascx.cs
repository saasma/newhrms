﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using BLL.BO;
using System.IO;
using Utils.Helper;

namespace Web.Employee.UserControls
{
    public partial class EEduCtrl : BaseUserControl
    {
        public bool ShowEmpty = false;

        private bool _isDisplayMode = false;
        public bool IsDisplayMode
        {
            set
            {
                if (value == true)
                {
                    _isDisplayMode = value;
                    btnAddNewLine.Visible = false;
                    CommandColumn21.Visible = false;
                    CommandColumn2.Visible = false;
                    GridEducation.Width = 920;
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                HideButtonAndColumn();
                CheckPermission();
                Initialise(0);
            }

            JavascriptHelper.AttachPopUpCode(Page, "addEducation", "EEduDetailsPopup.aspx", 720, 600);
            JavascriptHelper.AttachPopUpCode(Page, "editEducation", "EEduDetailsPopup.aspx", 720, 600);
        }
        public void HideButtonBlock()
        {
            btnAddNewLine.Visible = false;
        }
        public void Initialise(int employeeId)
        {
            int EmployeeID = GetEmployeeID();

            if (employeeId != 0)
                EmployeeID = employeeId;


            this.LoadEducationGrid(EmployeeID);


        }



        protected void EducationDownLoad(int ID)
        {

            //string contentType = "";
            HEducation doc = NewHRManager.GetEducationDetailsById(ID);

            string path = Server.MapPath(@"~/uploads/" + doc.FileLocation + doc.ServerFileName);
            string contentType = doc.FileType;
            string name = doc.UserFileName;

            byte[] bytes = File.ReadAllBytes(path);

            Context.Response.Clear();
            Context.Response.ClearHeaders();
            Context.Response.AppendHeader("Content-Disposition", "attachment; filename=\"" + name + "\"");
            Context.Response.ContentType = contentType;
            Context.Response.BinaryWrite(bytes);
            Context.Response.End();
            // Response.Redirect("~/DocumentHandler.ashx?ID=" + ID);
        }

        protected void LoadEducationGrid(int EmployeeID)
        {
            List<HEducation> _HEducation = NewHRManager.GetEducationByEmployeeID(EmployeeID);

            if (SessionManager.CurrentLoggedInEmployeeId == 0)
                _HEducation = _HEducation.Where(x => x.Status == (int)HRStatusEnum.Approved).ToList();

            if (Request.QueryString["ID"] != null)
            {
                if (int.Parse(Request.QueryString["ID"].ToString()) != SessionManager.CurrentLoggedInEmployeeId)
                    _HEducation = _HEducation.Where(x => x.Status == (int)HRStatusEnum.Approved).ToList();
            }

            this.StoreEducation.DataSource = _HEducation;
            this.StoreEducation.DataBind();

            if (ShowEmpty == false)
            {
                if (_isDisplayMode && _HEducation.Count <= 0)
                    GridEducation.Hide();
            }
            else
                GridEducation.Show();
        }
        protected int GetEmployeeID()
        {
            //Guid? AssetID = null;
            int EmpID = 0;
            if (!string.IsNullOrEmpty(Request.QueryString["ID"]))
            {
                EmpID = int.Parse(Request.QueryString["ID"]);
            }
            else
            {
                EmpID = SessionManager.CurrentLoggedInEmployeeId;
            }
            return EmpID;
        }
       

        private object[] EducationFillData
        {
            get
            {
                return new object[]
            {
                new object[] { "", "", "",""},
               
            };
            }
        }
        protected void GridEducation_Command(object sender, DirectEventArgs e)
        {
            string commandName = e.ExtraParams["command"];
            int EducationID = int.Parse(e.ExtraParams["ID"]);
            HEducation _HEducation = NewHRManager.GetEducationDetailsById(int.Parse(e.ExtraParams["ID"]));
            string Path = Server.MapPath(_HEducation.FileLocation) + _HEducation.ServerFileName;
            switch (commandName)
            {
                case "Delete":
                    this.DeleteEducationDocument(EducationID, Path);
                    break;
                case "DownLoad":
                    this.EducationDownLoad(EducationID);
                    break;
                case "Edit":
                    {
                        hdnEduID.Text = EducationID.ToString();
                        X.Js.Call("EditEdu");
                        break;
                    }
            }

        }
        protected void DeleteEducationDocument(int DocumentID, string path)
        {

            bool result = NewHRManager.DeleteEducationByID(DocumentID);
            if (result)
            {
                if (File.Exists(path))
                    File.Delete(path);
                //refresh grid
                NewMessage.ShowNormalMessage("Record deleted successfully.");
                this.LoadEducationGrid(this.GetEmployeeID());
            }

        }

       

        private void HideButtonAndColumn()
        {
            if (Request.QueryString["ID"] != null && SessionManager.CurrentLoggedInEmployeeId != 0)
            {
                if (SessionManager.CurrentLoggedInEmployeeId != int.Parse(Request.QueryString["ID"].ToString()))
                {
                    IsDisplayMode = true;

                }
            }
        }

        private void CheckPermission()
        {
            int employeeId = 0;
            if (Request.QueryString["ID"] != null && SessionManager.CurrentLoggedInEmployeeId != 0)
            {
                if (SessionManager.CurrentLoggedInEmployeeId != int.Parse(Request.QueryString["ID"].ToString()))
                {
                    employeeId = int.Parse(Request.QueryString["ID"]);
                    List<GetOtherEmployeeListForBranchDepartmentHeadResult> list = NewHRManager.GetOtherEmployeeListByEmpId(SessionManager.CurrentLoggedInEmployeeId);
                    bool found = false;
                    foreach (GetOtherEmployeeListForBranchDepartmentHeadResult obj in list)
                    {
                        if (obj.EmployeeId == employeeId)
                        {
                            found = true;
                            break;
                        }
                    }

                    if (found == false)
                    {
                        Response.Redirect("~/Employee/EmpNoPermissionPage.aspx");
                    }
                }
            }

        }

        protected void btnReloadEdu_Click(object sender, DirectEventArgs e)
        {
            int employeeId = 0;
            if (Request.QueryString["ID"] != null)
                employeeId = int.Parse(Request.QueryString["ID"].ToString());
            else
                employeeId = SessionManager.CurrentLoggedInEmployeeId;

            LoadEducationGrid(employeeId);
        }

        protected void btnChangeOrder_Click(object sender, DirectEventArgs e)
        {
            int sourceId = int.Parse(hdnSource.Text.Trim());
            int destId = int.Parse(hdnDest.Text.Trim());
            string mode = hdnDropMode.Text.Trim();

            NewHRManager.ChangeHEducationOrder(sourceId, destId, mode, GetEmployeeID());

            LoadEducationGrid(GetEmployeeID());
        }


    }
}