﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL.Entity;
using BLL;
using System.IO;
using Utils;
using Utils.Calendar;

namespace Web.Employee.UserControls
{
    public partial class EmpActivityCtrl : BaseUserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack && !X.IsAjaxRequest)
                Initialise();
        }

        private void Initialise()
        {
            List<ClientList> listClients = ListManager.GetAllClients();

            List<Clients> clients = new List<Clients>();
            foreach (var item in listClients)
            {
                clients.Add(new Clients() { ClientSoftwareId = item.ClientId, Name = item.ClientName });
            }
            storeClient.DataSource = clients;
            storeClient.DataBind();

            List<ActivityType> listActivityType = ListManager.GetActivityTypes();
            List<ActivityTypes> activities = new List<ActivityTypes>();
            foreach (var item in listActivityType)
            {
                activities.Add(new ActivityTypes() { ActivityType = item.ActivityTypeId, Name = item.Name });
            }

            storeType.DataSource = activities;
            storeType.DataBind();

            if (CommonManager.CompanySetting.WhichCompany == WhichCompany.Sana)
                tfClients.Hide();
        }


        private void Clear()
        {
            txtActivityDate.Text = "";
            int employeeId = SessionManager.CurrentLoggedInEmployeeId;
            EEmployee emp = EmployeeManager.GetEmployeeById(employeeId);
            bool hasPhoto = false;
            if (emp.HHumanResources.Count > 0)
            {
                if (!string.IsNullOrEmpty(emp.HHumanResources[0].UrlPhotoThumbnail))
                {
                    img.ImageUrl = Page.ResolveUrl(Config.UploadLocation + "/" + emp.HHumanResources[0].UrlPhotoThumbnail);
                    if (File.Exists(Server.MapPath(img.ImageUrl)))
                        hasPhoto = true;
                }
            }
            if (!hasPhoto)
            {
                img.ImageUrl = Page.ResolveUrl(Web.Helper.WebHelper.GetEmployeeNoImage(employeeId));

            }

            lblEmpName.Text = emp.Name;
            lblDesignation.Text = emp.EDesignation.Name;
            txtActivityDate.Enable();
            cmbActivityType.ClearValue();
            txtDetails.Clear();
            foreach (Tag tag in tfClients.Tags)
                tfClients.RemoveTag(tag.Value);

            txtWorkingHour.Clear();

            dfComments.Clear();
            txtActivityComment.Hide();
        }

        public void AddActivity()
        {
            Clear();
            hdnIsFromDashboard.Text = "false";
            X.Js.Call("hideCommetSection");
            txtActivityComment.Hide();
            hdnDetailId.Text = "";
            hdnPlanId.Text = "";
            txtActivityDate.SelectedDate = DateTime.Today;
            btnSaveAndNew.Show();
            btnSaveAndFinish.Show();
            WEmpActivity.Center();
            WEmpActivity.Show();
        }

        protected void btnSaveComment_Click(object sender, DirectEventArgs e)
        {
            Page.Validate("SaveActivityComment");
            if (Page.IsValid)
            {
                ActivityEmpComment obj = new ActivityEmpComment();
                obj.Comment = txtActivityComment.Text.Trim();
                obj.EmployeeId = SessionManager.CurrentLoggedInEmployeeId;
                obj.ActivityId = int.Parse(hdnDetailId.Text);

                if (obj.Comment.Length > 2000)
                {
                    NewMessage.ShowWarningMessage("Maxmium comment lenght is 2000 characters.");
                    txtActivityComment.Focus();
                    return;
                }

                Status status = NewHRManager.SaveUpdateEmployeeActivityComment(obj);
                if (status.IsSuccess)
                {
                    NewMessage.ShowNormalMessage("Comment saved successfully.", "searchList");

                    txtActivityComment.Clear();

                    LoadActivityCommentDetails(obj.ActivityId.Value, bool.Parse(hdnIsFromDashboard.Text));


                }
                else
                    NewMessage.ShowWarningMessage("Errror while saving comment.");
            }
        }

        protected void btnSaveAndNew_Click(object sender, DirectEventArgs e)
        {
            Page.Validate("SaveUpdActivity");
            if (Page.IsValid)
            {
                NewActivityDetail obj = new NewActivityDetail();

                if (!string.IsNullOrEmpty(hdnDetailId.Text))
                    obj.DetailId = int.Parse(hdnDetailId.Text);

                obj.EmployeeId = SessionManager.CurrentLoggedInEmployeeId;

                if (txtActivityDate.SelectedDate != null && txtActivityDate.SelectedDate != new DateTime())
                {
                    obj.DateEng = txtActivityDate.SelectedDate;
                    obj.Date = BLL.BaseBiz.GetAppropriateDate(obj.DateEng.Value);
                }
                else
                {
                    NewMessage.ShowWarningMessage("Date is required.");
                    txtActivityDate.Focus();
                    return;
                }

                if (obj.DateEng.Value.Date > DateTime.Today)
                {
                    NewMessage.ShowWarningMessage("Activity cannot be saved for future.");
                    return;
                }

                Setting setting = CalculationManager.GetSetting();
                if (setting != null && setting.EmployeeActvityThreshold != null)
                {
                    if (obj.DateEng.Value.Date < NewHRManager.GetEmpActivityThreshold())
                    {
                        NewMessage.ShowWarningMessage("Sorry employee activity threshold days has exceeded.");
                        return;
                    }
                }

                obj.ActivityType = int.Parse(cmbActivityType.SelectedItem.Value);

                foreach (Tag tag in tfClients.Tags)
                {
                    try
                    {
                        int.Parse(tag.Value);
                    }
                    catch (Exception ex)
                    {
                        NewMessage.ShowWarningMessage("Please select proper clients.");
                        tfClients.Focus();
                        return;
                    }
                    obj.ClientSoftwareIds += tag.Value + ",";
                }

                if (!string.IsNullOrEmpty(obj.ClientSoftwareIds))
                    obj.ClientSoftwareIds = obj.ClientSoftwareIds.TrimEnd(',');

                obj.Description = txtDetails.Text.Trim();

                obj.Status = (int)AtivityStatusEnum.Draft;
                obj.CreatedOn = DateTime.Now;

                if(!string.IsNullOrEmpty(txtWorkingHour.Text.Trim()))
                {
                    string[] workingHour = txtWorkingHour.Text.Split(':');
                    int hours = 0, minutes = 0;                   

                    try
                    {
                        hours = int.Parse(workingHour[0].ToString());

                        if(workingHour.Length > 1)
                            minutes = int.Parse(workingHour[1].ToString());

                        if (minutes > 59)
                        {
                            NewMessage.ShowWarningMessage("Invalid Hours:Minutes.");
                            txtWorkingHour.Focus();
                            return;
                        }
                    }
                    catch (Exception ex)
                    {
                        NewMessage.ShowWarningMessage("Invalid Hours:Minutes.");
                        txtWorkingHour.Focus();
                        return;
                    }

                    obj.ActivityDuration = new TimeSpan(hours, minutes, 0);

                    if (obj.ActivityDuration.Value.TotalMinutes <= 0)
                    {
                        NewMessage.ShowWarningMessage("Hours:Minutes is required.");
                        txtWorkingHour.Focus();
                        return;
                    }
                }
                else
                {
                    NewMessage.ShowWarningMessage("Hours:Minutes is required.");
                    txtWorkingHour.Focus();
                    return;
                }

                Status status = NewHRManager.SaveUpdateEmpActivityNew(obj);

                if (status.IsSuccess)
                {
                    if (!string.IsNullOrEmpty(hdnDetailId.Text))
                        NewMessage.ShowNormalMessage("Activity saved successfully.");
                    else
                        NewMessage.ShowNormalMessage("Activity udpated successfully.");

                    X.Js.Call("searchList");

                    if (sender == btnSaveAndNew)
                    {
                        Clear();
                        txtActivityDate.SelectedDate = DateTime.Today;
                    }
                    else
                        WEmpActivity.Close();
                }
                else
                    NewMessage.ShowWarningMessage(status.ErrorMessage);


            }
        }

        public void EditActivity(int detailId, bool isFromDashboard = false)
        {
            hdnIsFromDashboard.Text = isFromDashboard.ToString();
            Clear();
            hdnDetailId.Text = detailId.ToString();
            NewActivityDetail obj = NewHRManager.GetNewActivityDetailById(int.Parse(hdnDetailId.Text));
            if (obj != null)
            {
                txtActivityDate.SelectedDate = obj.DateEng.Value;
                txtActivityDate.Disable();

                cmbActivityType.SetValue(obj.ActivityType.Value.ToString());

                if (!string.IsNullOrEmpty(obj.ClientSoftwareIds))
                    tfClients.SetValue(obj.ClientSoftwareIds);

                txtDetails.Text = obj.Description;

                LoadActivityCommentDetails(obj.ActivityId, isFromDashboard);

                if (obj.Status == (int)AtivityStatusEnum.Draft)
                {
                    btnSaveAndNew.Show();
                    btnSaveAndFinish.Show();
                }
                else
                {
                    btnSaveAndNew.Hide();
                    btnSaveAndFinish.Hide();
                }


                

                if (obj.ActivityDuration != null)
                    txtWorkingHour.Text = string.Format("{0}:{1}", obj.ActivityDuration.Value.Hours.ToString(), obj.ActivityDuration.Value.Minutes.ToString());
                else
                    txtWorkingHour.Clear();

                LoadForDashboardOrOther(isFromDashboard);

                WEmpActivity.Center();
                WEmpActivity.Show();

            }
            else
            {
                NewMessage.ShowNormalMessage("Activity not found.");
            }
        }

        private void LoadForDashboardOrOther(bool isForDashboard)
        {
            if (isForDashboard)
            {
                txtActivityComment.Width = 404;
                txtActivityComment.StyleSpec = "margin:-4px;";
            }
            else
            {
                txtActivityComment.Width = 400;
            }
        }

        private void LoadActivityCommentDetails(int activityId, bool isFromDashboard)
        {
            List<ActivityComment> listActivityComments = NewHRManager.GetHRActivityComments(activityId);

            if (listActivityComments.Count > 0)
            {
                List<EmpActivityCommentCls> listComments = new List<EmpActivityCommentCls>();


                foreach (var item in listActivityComments)
                {
                    Guid commentedByUserID = item.CreatedBy.Value;
                    string commentBy = "";

                    UUser objUUser = UserManager.GetUserByUserID(commentedByUserID);
                    if (objUUser.UserMappedEmployeeId != null)
                    {
                        EEmployee createdUserEmp = EmployeeManager.GetEmployeeById(objUUser.UserMappedEmployeeId.Value);
                        commentBy = createdUserEmp.Name;
                    }
                    else
                        commentBy = (string.IsNullOrEmpty(objUUser.NameIfNotEmployee) ? objUUser.UserName : objUUser.NameIfNotEmployee);

                    commentBy = commentBy.Split(' ')[0].ToString();
                    

                    EmpActivityCommentCls objEmpActivityCommentCls = new EmpActivityCommentCls()
                    {
                        CommentBy = commentBy,
                        CommentedOn = item.CreatedOn.Value,
                        Comment = item.Comment
                    };

                    listComments.Add(objEmpActivityCommentCls);
                }


                List<ActivityEmpComment> listEmpActivityComments = NewHRManager.GetEmpActivityCommentsByActivityId(activityId);

                foreach (var item in listEmpActivityComments)
                {
                    string commentBy = "";

                    EEmployee emp = EmployeeManager.GetEmployeeById(item.EmployeeId.Value);
                    commentBy = emp.Name.Split(' ')[0].ToString();

                    EmpActivityCommentCls objEmpActivityCommentCls = new EmpActivityCommentCls()
                    {
                        CommentBy = commentBy,
                        CommentedOn = item.CreatedOn.Value,
                        Comment = item.Comment
                    };

                    listComments.Add(objEmpActivityCommentCls);
                }

                listComments = listComments.OrderByDescending(x => x.CommentedOn).ToList();

                foreach (var item in listComments)
                {
                    dfComments.Html += "<span style='color:#356FA2;'>" + item.CommentBy + "</span>" + "<span style='font-style: italic; margin-left:8px;'>" + item.CommentedOn.ToString("dd MMMMM hh:mm") + "</span>" + "<br/>" + "<span style='margin-top:5px !important;'>" + item.Comment + "</span>" + "<br/><br/>";
                }

                txtActivityComment.Show();

                btnSaveActivityComment.Show();

                if (isFromDashboard)
                {
                    txtActivityComment.StyleSpec = "margin:-4px;";
                    txtActivityComment.Width = 404;
                }
                else
                    txtActivityComment.Width = 400;

                X.Js.Call("showCommetSection");
            }
            else
            {
                dfComments.Clear();
                txtActivityComment.Hide();
                btnSaveActivityComment.Hide();
                X.Js.Call("hideCommetSection");

            }
        }

    }
}