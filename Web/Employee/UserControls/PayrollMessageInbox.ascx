﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PayrollMessageInbox.ascx.cs"
    Inherits="Web.Employee.UserControls.PayrollMessageInbox" %>
<script type="text/javascript">
        var dfBody = null;
        var clientid = null;
        var Hidden_PageSize = null;
        
        var hdMessageId =null;
        var wMessageBody = null;
        var dfSubject = null;
        var storePayrollMessage =null;

        var getRowClass = function (record) {
            if (record.data.IsRead) {
                return "dirty-row";
            }
            else{
                return "new-row";
            }
        };

        var GetGridRow = function (value, meta, record) {
         
            return "<a href='#' class='dirty-row' onclick=\"DisplayMessageBody('"+ record.data.MessageId +"');\" >" + value + "<a/>";
        };


        var deleteRows = function () {
            Ext.Msg.confirm('Delete Rows', 'Are you sure you want to delete selected message?', function (btn) {
                if (btn == 'yes') {
                    CompanyX.direct.<%=this.ClientID%>.btnDirectDelete_Click();
                    ReloadPayrollMessageGrid();
                }
            })
        };

        var ReloadPayrollMessageGrid = function () {
            
            //if( isMessageAlreadyViewed ==false)
            <%= storePayrollMessage.ClientID %>.reload();
            hdMessageId=null;
        };

        var isMessageAlreadyViewed = false;
        var DisplayMessageBody = function(messageId, subject){
            var data = <%= gridPayrollMessage.ClientID %>.store.getById(parseInt(messageId)).data;
            <%= hdMessageId.ClientID %>.setValue(messageId);
            <%= dfSubject.ClientID %>.setValue(data.Subject);
            <%= dfBody.ClientID %>.setValue(data.Body);
            CompanyX.direct.<%=this.ClientID%>.FlagAsRead(parseInt(messageId));
            <%= wMessageBody.ClientID %>.show();
            isMessageAlreadyViewed = data.IsRead;
            
            hdMessageId=messageId;
        };

        var deleteSingleRow = function(command, data){
            switch( command )
            {
                case "Delete":
                     Ext.Msg.confirm('Delete Row', 'Are you sure you want to delete the selected message?', function (btn) {
                        if (btn == 'yes') {
                            Ext.net.DirectMethods.btnDirectDeleteSingle_Click(data.MessageId);
                        }
                        return false;
                    })
                    break;
                default: 
                    break;
            }
            
        }
</script>
<script type="text/javascript">
      
</script>
<style type="text/css">
    .x-grid3-row-alt
    {
        background-color: #FFF;
    }
    
    .dirty-row
    {
        color: black;
    }
    
    .new-row, .new-row a
    {
        color: darkblue;
        background-color: #f1f8ff !important;
        font-weight: bold !important;
        font-size: 11px;
    }
    .divSubject
    {
        background: #E8F1FF;
        padding: 10px;
        border-bottom: 1px solid silver;
        border-top: 1px solid silver;
    }
    .divBody
    {
        background: #E8F1E9;
        padding: 10px;
        height: 500px;
        overflow: scroll;
    }
    .divSubject .x-form-display-field
    {
        font-weight: bold;
        font-size: 17px;
    }
    .x-btn-default-toolbar-small-icon-text-right .x-btn-inner
    {
        color: Red;
    }
    #Inbox1_gridPayrollMessage_header_hd-textEl{color:White;}
</style>
<ext:Hidden ID="hdUserName" runat="server" />
<ext:Hidden ID="Hidden_PageSize" runat="server" />
<div class="contentArea">
    <h3>
      &nbsp;</h3>
    <div style="clear:both"></div>
    <div >
        <ext:GridPanel ID="gridPayrollMessage" runat="server" StripeRows="true" Title="Messages"
            Height="580" AutoExpandColumn="Subject" Cls="gridtbl">
            <Store>
                <ext:Store ID="storePayrollMessage" runat="server">
                    <Proxy>
                        <ext:AjaxProxy Url="~/Handler/PayrollMessageHandler.ashx">
                            <ActionMethods Read="GET" />
                            <Reader>
                                <ext:JsonReader Root="data" TotalProperty="total" />
                            </Reader>
                        </ext:AjaxProxy>
                    </Proxy>
                    <AutoLoadParams>
                        <%-- <ext:Parameter Name="UserName" Mode="Raw" Value="#{hdUserName}.getValue()" />--%>
                        <ext:Parameter Name="start" Value="={0}" />
                        <ext:Parameter Name="limit" Value="={20}" />
                    </AutoLoadParams>
                    <Parameters>
                        <ext:StoreParameter Name="PageSize" Value="#{Hidden_PageSize}.getValue()" Mode="Raw" />
                        <ext:StoreParameter Name="UserName" Mode="Raw" Value="#{hdUserName}.getValue()" />
                    </Parameters>
                    <Model>
                        <ext:Model Root="Data" IDProperty="MessageId">
                            <Fields runat="server">
                                <ext:ModelField Name="MessageId" Type="Int" />
                                <ext:ModelField Name="Subject" Type="String" />
                                <ext:ModelField Name="Body" Type="String" />
                                <ext:ModelField Name="ReceivedTo" Type="String" />
                                <ext:ModelField Name="SendBy" Type="String" />
                                <ext:ModelField Name="SendByName" Type="String" />
                                <ext:ModelField Name="ReceivedDate" Type="String" />
                                <ext:ModelField Name="ReceivedDateEng" Type="Date" />
                                <ext:ModelField Name="IsRead" Type="Boolean" />
                                <ext:ModelField Name="Name" Type="String" />
                            </Fields>
                        </ext:Model>
                    </Model>
                </ext:Store>
            </Store>
            <TopBar>
                <ext:Toolbar ID="gridMessageTopbar" StyleSpec="margin-top:5px;margin-bottom:5px;" runat="server">
                    <Items>
                        <ext:Button ID="btnDelete" Text="Delete" Icon="Delete" runat="server" Width="120">
                            <%--<DirectEvents>
                                    <Click OnEvent="btnDelete_Click" >
                                        <EventMask ShowMask="true" Msg="Deleting..." />
                                    </Click>                                    
                                </DirectEvents>--%>
                            <Listeners>
                                <Click Handler="deleteRows();" />
                            </Listeners>
                        </ext:Button>
                        <ext:Button ID="btnMarkAsRead" Text="Mark as Read" Icon="Mail" runat="server" Width="120">
                            <DirectEvents>
                                <Click OnEvent="btnMarkAsRead_Click">
                                    <EventMask ShowMask="true" Msg="Processing..." />
                                </Click>
                            </DirectEvents>
                        </ext:Button>
                        <ext:DisplayField ID="dfRowsAffected" runat="server" Region="West" LabelAlign="Right" />
                    </Items>
                </ext:Toolbar>
            </TopBar>
            <ColumnModel ID="ColumnModel1" runat="server">
                <Columns>
                    <ext:Column ColumnID="Subject" MenuDisabled="true" Width="260" Sortable="false" Text="Subject"
                        DataIndex="Subject">
                        <Renderer Fn="GetGridRow" />
                    </ext:Column>
                    <ext:Column ColumnID="SendByName" MenuDisabled="true" Width="180" Sortable="false"
                        Text="From" DataIndex="SendByName" />
                    <%-- <ext:Column ColumnID="ReceivedDate" Width="150" MenuDisabled="true" Sortable="false"
                        Text="Received" DataIndex="ReceivedDateEng" />--%>
                    <ext:DateColumn ID="DateColumn1" Width="150" runat="server" Text="Received Date"
                        DataIndex="ReceivedDate">
                    </ext:DateColumn>
                    <ext:DateColumn ID="DateColumn2" Width="150" runat="server" Text="Time" DataIndex="ReceivedDateEng">
                        <Renderer Fn="Ext.util.Format.dateRenderer('h:m A')" />
                    </ext:DateColumn>
                    <ext:Column ColumnID="" Width="400" MenuDisabled="true" Sortable="false" Text=""
                        DataIndex="" />
                </Columns>
            </ColumnModel>
            <View>
                <ext:GridView ID="GridView1" runat="server">
                    <GetRowClass Fn="getRowClass" />
                </ext:GridView>
            </View>
            <SelectionModel>
                <ext:CheckboxSelectionModel ID="CheckboxSelectionModel1" Width="28" CheckOnly="true"
                    runat="server" />
            </SelectionModel>
            <BottomBar>
                <ext:PagingToolbar ID="PagingToolbar1" runat="server" PageSize="20" DisplayInfo="true"
                    DisplayMsg="Displaying message {0} - {1} of {2}" EmptyMsg="No message to display" />
            </BottomBar>
            <%-- <loadmask showmask="true" />--%>
            <Listeners>
                <%--<Command Handler="deleteSingleRow(command, record.data);" />--%>
                <%--<Command Handler="Ext.Msg.alert(command, record.data.MessageId);" />--%>
            </Listeners>
        </ext:GridPanel>
    </div>
</div>
<div style="clear:both">
</div>
<ext:Window ID="wMessageBody" runat="server" Title="Message Details" Icon="Application"
    Height="500px" Width="900px" Padding="5" Collapsible="true" Modal="true" Hidden="true">
    <Content>
        <ext:Panel ID="panelPopUP" runat="server" Padding="5" Width="890px" Height="440px">
            <TopBar>
                <ext:Toolbar ID="toolbarPopup" runat="server" Layout="HBoxLayout">
                    <Items>
                        <ext:Button ID="btnBackToMessage" runat="server" Text="Back To Messages" Icon="ArrowLeft"
                            IconAlign="Left">
                            <Listeners>
                                <Click Handler="#{wMessageBody}.hide();" />
                            </Listeners>
                        </ext:Button>
                        <ext:ToolbarFill runat="server">
                        </ext:ToolbarFill>
                        <ext:Button ID="btnDeletePopup" runat="server" Text="Delete" Icon="Delete" IconAlign="Right">
                            <Listeners>
                                <Click Handler="#{DirectMethods}.DeletePoppedUpMessage(#{hdMessageId}.getValue());" />
                            </Listeners>
                        </ext:Button>
                    </Items>
                </ext:Toolbar>
            </TopBar>
            <Content>
                <div style="margin: 0 0px;">
                    <div class="divSubject">
                        <ext:DisplayField ID="dfSubject" runat="server" />
                    </div>
                    <div class="divBody" style="background-color:Transparent">
                        <ext:Hidden ID="hdMessageId" runat="server" />
                        <ext:DisplayField ID="dfBody" runat="server" />
                    </div>
                </div>
            </Content>
        </ext:Panel>
    </Content>
    <Listeners>
        <Hide Handler="ReloadPayrollMessageGrid();" />
    </Listeners>
</ext:Window>
<div style="clear:both">
</div>
<ext:Window ID="wMessageBodyOld" runat="server" Title="Message Details" Icon="Application"
    Height="500px" Width="900px" Padding="5" Collapsible="true" Modal="true" Hidden="true">
    <Content>
        <ext:Panel ID="Panel2" runat="server" Padding="5" Height="440px">
            <TopBar>
                <ext:Toolbar ID="Toolbar1" runat="server" Layout="HBoxLayout">
                    <Items>
                        <ext:Button ID="btnClose" runat="server" Text="Close">
                            <Listeners>
                                <Click Handler="#{wMessageBody}.hide();" />
                            </Listeners>
                        </ext:Button>
                        <ext:DisplayField ID="dfSubjectOld" runat="server" />
                    </Items>
                </ext:Toolbar>
            </TopBar>
            <Content>
                <div style="border: 1px solid lightgray">
                    <ext:Hidden ID="hdMessageIdOld" runat="server" />
                    <ext:DisplayField ID="dfBodyOld" runat="server" />
                </div>
            </Content>
        </ext:Panel>
    </Content>
    <Listeners>
        <Hide Handler="ReloadPayrollMessageGrid();" />
    </Listeners>
</ext:Window>
