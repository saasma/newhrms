﻿<%@ Page Title="Company Resources" Language="C#" MasterPageFile="~/Master/Details.Master"
    AutoEventWireup="true" CodeBehind="AACompanyResources.aspx.cs" Inherits="Web.CP.AACompanyResources" %>

<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/PagingCtl.ascx" TagName="PagingCtl" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <script>
         function renderTitle(value, p, record) {
             return value ? Ext.String.format(
                '<a href="http://sencha.com/forum/showthread.php?t={1}" target="_blank">{0}</a>',
                value,
                record.data.threadid
            ) : '';
         }
    </script>
  

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent"  runat="server">
    <ext:ResourceManager ID="ResourceManager1" runat="server" />
    


    <div class="contentArea">
       
        <div class="attribute">
            
           
             </div>
    </div>
                <ext:Panel ID="RightView" runat="server"  MarginSpec="1 1 1 1" BodyCls="dbPanelBodyStyle"
                    FrameHeader="false" Layout="BorderLayout" Border="true" Height="600" style="Width:100em;" Region="Center" >
                    <Items>
                        <%--<ext:UserControlLoader ID="vendorDashboard" Region="Center" runat="server" Path="~/Accounting/UserControls/VendorDashboard.ascx" /> --%>
                        <ext:Panel ID="Panel_VendorTransactionList" Hidden="false" runat="server" FrameHeader="false"
                            Border="false" Region="Center" Title="Company Resources" Layout="BorderLayout" >
                            <TopBar>
                                <ext:Toolbar ID="Toolbar1" runat="server" Height="35">
                                    <Items>
                                       
                                    </Items>
                                </ext:Toolbar>
                            </TopBar>
                            <Items>
                               
                                <ext:TreePanel ID="TreePanel1" runat="server"
                                    Width="600"
                                    Height="400"           
                                    UseArrows="true"
                                    RootVisible="false"            
                                    Animate="true">
                                    <Store>
                                        <ext:TreeStore ID="TreeStore1" runat="server"  AutoLoad="true">
                                             <Model>
                                                 <ext:Model ID="Model1" runat="server" IDProperty="ResourceID">
                                                     <Fields>
                                                         <ext:ModelField Name="ResourceID" NullConvert="true" />
                                                         <ext:ModelField Name="Name" NullConvert="true" />
                                                         <ext:ModelField Name="ResourceFileName" NullConvert="true" />
                                                         <ext:ModelField Name="ResourceUrl" NullConvert="true" />
                                                         <ext:ModelField Name="ContentType"  />
                                                         <ext:ModelField Name="Size" NullConvert="true" />
                                                         <ext:ModelField Name="ResourceCategoryID" NullConvert="true" />
                                                         <ext:ModelField Name="ResourceCategoryName" NullConvert="true" />
                                                     </Fields>                             
                                                 </ext:Model>
                                            </Model>
                                        </ext:TreeStore>
                                    </Store>
                                    <%--<Plugins>
                                        <ext:BufferedRenderer ID="BufferedRenderer1" runat="server" />
                                    </Plugins>--%>
                                    <ColumnModel>
                                        <Columns>
                                            <ext:TreeColumn ID="TreeColumn1" runat="server" Text="ResourceCategoryName" Flex="2" DataIndex="ResourceCategoryName" />
                                            <ext:Column ID="Column1" runat="server" Text="File" Flex="1" DataIndex="Name" />
                                            <ext:Column ID="Column2" runat="server" Text="Title" Flex="2" DataIndex="ResourceFileName">
                                                <%--<Renderer Fn="renderTitle" />--%>
                                            </ext:Column>
                                            <ext:Column ID="Column3" runat="server" Text="ContentType" Flex="1" DataIndex="ContentType" />
                                            <ext:Column ID="Column4" runat="server" Text="Size" Flex="1" DataIndex="Size" />
                                        </Columns>
                                    </ColumnModel>
                                </ext:TreePanel>




                            </Items>
                            <BottomBar>
                               
                            </BottomBar>
                        </ext:Panel>
                    </Items>
                </ext:Panel>



</asp:Content>
