﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TimeAttendCtrl.ascx.cs"
    Inherits="Web.Employee.UserControls.TimeAttendCtrl" %>
<style type="text/css">
    .BlueCls
    {
        color: Blue;
    }
    .GreenCls
    {
        color: Green;
    }
    .RedCls
    {
        color: Red;
    }
    
</style>
<script type="text/javascript">

    var CommandHandler = function(value, command,record,p2) {
            
            if(command == 'View')
            { 
                 <%= hdnRequestId.ClientID %>.setValue(record.data.RequestID);
                 <%= btnView.ClientID %>.fireEvent('click');
            }
            else if(command == 'Approve')
            {
                <%= hdnRequestId.ClientID %>.setValue(record.data.RequestID);
                <%= btnApprove.ClientID %>.fireEvent('click');
            }
            else
            {
                <%= hdnRequestId.ClientID %>.setValue(record.data.RequestID);
                <%= btnReject.ClientID %>.fireEvent('click');
            }
       };


    var addNewRow = function (grid) {
            var newRow = new SettingModel();

            newRow.data.ControlID = "";

            var rowIndex = grid.getStore().data.items.length;

            grid.getStore().insert(rowIndex, newRow);
        }

        var RemoveItemLine = function (column, command, record, recordIndex, cellIndex) {
            var store = this.grid.store;
            store.remove(record);
        };

        var CalculateWorkHours = function(e1,e2,record)
        {
            var overNightShift = false;
            if(record.data.OvernightShift != null && record.data.OvernightShift == true)
            {
                overNightShift = true;
            }

            if(record.data.InTimeDT == null || record.data.OutTimeDT == null)
            {
                return;
            }

            if(record.data.InTimeDT != '' && record.data.OutTimeDT != '')
            {
                var strTime = Ext.util.Format.date(record.data.InTimeDT, 'g:i a')
                var eTime = Ext.util.Format.date(record.data.OutTimeDT, 'g:i a')
                
                var diffTime = calculateTotalMinutes(eTime,overNightShift,true) - calculateTotalMinutes(strTime, overNightShift, false)

                if(diffTime <= 0)
                {
                    alert('End time must be greater than Start time.');
                    return "";
                }


                return (Math.floor(diffTime/60) + ":" + (diffTime%60));
              
            }
        };

       

        function calculateTotalMinutes(time, overNightShift, endTime)
        {
            var parts = time.split(/ |:/);
            var totalHours = 0;

            if(parts[2] == 'pm')
            {
               if(parseInt(parts[0], 10) == 12)
               {
                    return (12 * 60 + parseInt(parts[1], 10));
               }
               else
               {
                    return (12 * 60 + parseInt(parts[0], 10) * 60 + parseInt(parts[1], 10));
               }
            }
            else
            {
                if(overNightShift == true && endTime == true)
                {
                    return (parseInt(parts[0], 10) * 60 + parseInt(parts[1], 10)) + 24*60;
                }
                else
                {
                    return (parseInt(parts[0], 10) * 60 + parseInt(parts[1], 10));
                }
            }
    
            //return (parts[2] == 'pm' * 12 * 60) + parseInt(parts[0], 10) * 60 + parseInt(parts[1], 10);
        }


        var EmpNameRender = function (value, metaData, record, rowIndex, colIndex, store) {
                    var r = <%= storeEmployees.ClientID %>.getById(value.toString().toLowerCase());

                    if (Ext.isEmpty(r)) {
                        return "";
                    }
                    return r.data.Text;
                };


  
    

    function searchListEL() {

             <%=gridAttRequest.ClientID %>.getStore().pageSize = <%=cmbPageSize.ClientID %>.getValue();
            <%=PagingToolbar1.ClientID %>.doRefresh();
        }

    
 var checkboxRenderer = function (v, p, record) {
            if(record.data.ShowHide == 0){            
                return "";
            }
            
          return '<div class="x-grid-row-checker">&nbsp;</div>'
        };

  
    var prepareCommand = function (grid, command, record, row) {
            // you can prepare group command
            if ((command.command == 'Approve' || command.command == 'Reject') && ( record.data.ShowHide == 0))
            {
                command.hidden = true;
                command.hideMode = 'visibility'; 
            }
        };

    function SelectEmp()
    {
        var employeeId = <%= cmbSearch.ClientID %>.getValue();
        <%= hdnEmployeeId.ClientID %>.setValue(employeeId);
        <%= btnSelectEmp.ClientID %>.fireEvent('click');
    }

    var onShow = function (toolTip, grid) {
            var view = grid.getView(),
                store = grid.getStore(),
                record = view.getRecord(view.findItemByChild(toolTip.triggerElement)),
                column = view.getHeaderByCell(toolTip.triggerElement),
                data = record.get(column.dataIndex);

            toolTip.update(data);
        };

    var onShowAssign = function (toolTip, grid) {
            var view = grid.getView(),
                store = grid.getStore(),
                record = view.getRecord(view.findItemByChild(toolTip.triggerElement)),
                column = view.getHeaderByCell(toolTip.triggerElement),
                data = record.get(column.dataIndex);

            if(column.dataIndex == 'InNote' || column.dataIndex == 'OutNote')
            {
                toolTip.update(data);
            }
            else
            {
                toolTip.update('');
            }   
        };
</script>
<ext:Hidden ID="hdnRequestId" runat="server" />
<ext:Hidden ID="hdnEmployeeId" runat="server" />
<ext:Hidden ID="hdnEmployeeIdFilter" runat="server" />
<ext:Hidden ID="Hidden1" runat="server" />
<ext:LinkButton ID="btnView" Hidden="true" runat="server">
    <DirectEvents>
        <Click OnEvent="btnView_Click">
            <EventMask ShowMask="true" />
        </Click>
    </DirectEvents>
</ext:LinkButton>
<ext:LinkButton ID="btnApprove" Hidden="true" runat="server">
    <DirectEvents>
        <Click OnEvent="btnApprove_Click">
            <EventMask ShowMask="true" />
            <Confirmation ConfirmRequest="true" Message="Are you sure, you want to approve the time request?" />
        </Click>
    </DirectEvents>
</ext:LinkButton>
<ext:LinkButton ID="btnReject" Hidden="true" runat="server">
    <DirectEvents>
        <Click OnEvent="btnReject_Click">
            <EventMask ShowMask="true" />
            <Confirmation ConfirmRequest="true" Message="Are you sure, you want to reject the time request?" />
        </Click>
    </DirectEvents>
</ext:LinkButton>
<ext:LinkButton ID="btnSelectEmp" Hidden="true" runat="server">
    <DirectEvents>
        <Click OnEvent="btnSelectEmp_Click">
            <EventMask ShowMask="true" />
        </Click>
    </DirectEvents>
</ext:LinkButton>
<ext:Store ID="storeEmployees" runat="server">
    <Model>
        <ext:Model ID="modelEmployees" IDProperty="Value" runat="server">
            <Fields>
                <ext:ModelField Name="Text" Type="String" />
                <ext:ModelField Name="Value" Type="String" />
            </Fields>
        </ext:Model>
    </Model>
</ext:Store>
<ext:Store runat="server" ID="storeSearch" AutoLoad="false">
    <Proxy>
        <ext:AjaxProxy Url="../../Handler/EmployeeSearch.ashx">
            <ActionMethods Read="GET" />
            <Reader>
                <ext:JsonReader Root="plants" TotalProperty="total" />
            </Reader>
        </ext:AjaxProxy>
    </Proxy>
    <Model>
        <ext:Model ID="Model2" IDProperty="Name" Name="ItemLineModel" runat="server">
            <Fields>
                <ext:ModelField Name="Name" Type="String" />
                <ext:ModelField Name="EmployeeId" Type="String" />
            </Fields>
        </ext:Model>
    </Model>
</ext:Store>
<ext:Button ID="btnAssignTimeAtten" runat="server" Cls="btn btn-save" Text="Assign Time Attendance"
    Width="180">
    <DirectEvents>
        <Click OnEvent="btnAssignTimeAtten_Click">
            <EventMask ShowMask="true" />
        </Click>
    </DirectEvents>
</ext:Button>
<table class="fieldTable firsttdskip" runat="server" id="tbl">
    <tr>
        <td>
            <ext:DateField LabelSeparator="" FieldLabel="From" runat="server" LabelAlign="Top"
                ID="txtFromDateFilter" Width="150">
                <Plugins>
                    <ext:GenericPlugin ID="GenericPlugin3" runat="server" InstanceName="Ext.ux.DatePicker.HighlightWithNepDates" />
                </Plugins>
            </ext:DateField>
        </td>
        <td>
            <ext:DateField LabelSeparator="" FieldLabel="To" runat="server" LabelAlign="Top"
                ID="txtToDateFilter" Width="150">
                <Plugins>
                    <ext:GenericPlugin ID="GenericPlugin4" runat="server" InstanceName="Ext.ux.DatePicker.HighlightWithNepDates" />
                </Plugins>
            </ext:DateField>
        </td>
        <td>
            <ext:Store runat="server" ID="storeEmpSearch" AutoLoad="false">
                <Proxy>
                    <ext:AjaxProxy Url="../../Handler/EmpSearchID.ashx">
                        <ActionMethods Read="GET" />
                        <Reader>
                            <ext:JsonReader Root="plants" TotalProperty="total" />
                        </Reader>
                    </ext:AjaxProxy>
                </Proxy>
                <Model>
                    <ext:Model ID="Model9" IDProperty="Name" Name="ItemLineModel" runat="server">
                        <Fields>
                            <ext:ModelField Name="Name" Type="String" />
                            <ext:ModelField Name="EmployeeId" Type="String" />
                        </Fields>
                    </ext:Model>
                </Model>
            </ext:Store>
            <ext:ComboBox LabelSeparator="" ID="cmbEmpSearch" FieldLabel="Search Employee" LabelWidth="70"
                LabelAlign="Top" runat="server" DisplayField="Name" ValueField="EmployeeId" StoreID="storeEmpSearch"
                TypeAhead="false" Width="180" PageSize="9999" HideBaseTrigger="true" MinChars="1"
                Hidden="true" TriggerAction="All" ForceSelection="true">
                <ListConfig LoadingText="Searching..." MinWidth="200" StyleSpec="border-top:1px solid #98c0f4;">
                    <ItemTpl ID="ItemTpl2" runat="server">
                        <Html>
                            <tpl>
                                                <div class="search-item">
                                                                <span>{Name}</span>  
                                                 </div>
					                    </tpl>
                        </Html>
                    </ItemTpl>
                </ListConfig>
                <Triggers>
                    <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                </Triggers>
                <Listeners>
                    <Select Handler="this.getTrigger(0).show();" />
                    <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                    <TriggerClick Handler="if (index == 0) { 
                                               this.clearValue(); 
                                               this.getTrigger(0).hide();
                                           }" />
                </Listeners>
            </ext:ComboBox>
        </td>
        <td>
            <ext:MultiCombo ID="cmbStatus" runat="server" Width="150" SelectOnFocus="true" Selectable="true"
                LabelAlign="Top" FieldLabel="Status" ValueField="Value" DisplayField="Text" ForceSelection="true"
                AllowBlank="false" LabelSeparator="">
                <Store>
                    <ext:Store ID="Store4" runat="server">
                        <Model>
                            <ext:Model ID="Model3" runat="server">
                                <Fields>
                                    <ext:ModelField Name="Value" Type="String" />
                                    <ext:ModelField Name="Text" Type="String" />
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
            </ext:MultiCombo>
        </td>
        <td style="padding-top: 20px">
            <div>
                <ext:Button Cls="btn btn-primary" runat="server" ID="Button1" Text="<i></i>Load">
                    <%--<DirectEvents>
                                    <Click OnEvent="btnLastMonth_Change">
                                        <EventMask ShowMask="true" />
                                    </Click>
                                </DirectEvents>--%>
                    <Listeners>
                        <Click Fn="searchListEL" />
                    </Listeners>
                </ext:Button>
            </div>
        </td>
    </tr>
</table>
<ext:GridPanel StyleSpec="margin-top:15px;" ID="gridAttRequest" runat="server" Cls="itemgrid"
    OnReadData="Store_ReadData" Scroll="None" Width="1060">
    <Store>
        <ext:Store ID="Store1" runat="server" AutoLoad="true" OnReadData="Store_ReadData"
            RemotePaging="true" RemoteSort="true">
            <Proxy>
                <ext:PageProxy />
            </Proxy>
            <Model>
                <ext:Model ID="Model4" runat="server" IDProperty="RequestID">
                    <Fields>
                        <ext:ModelField Name="RequestID" Type="String" />
                        <ext:ModelField Name="EmployeeId" Type="Int" />
                        <ext:ModelField Name="Name" Type="String" />
                        <ext:ModelField Name="Branch" Type="String" />
                        <ext:ModelField Name="StartDateEng" Type="Date" />
                        <ext:ModelField Name="EndDateEng" Type="Date" />
                        <ext:ModelField Name="WorkDays" Type="Int" />
                        <ext:ModelField Name="SubmittedOnEng" Type="Date" />
                        <ext:ModelField Name="IPAddress" Type="string" />
                        <ext:ModelField Name="Status" Type="Int" />
                        <ext:ModelField Name="StatusText" Type="string" />
                        <ext:ModelField Name="ShowHide" Type="Int" />
                    </Fields>
                </ext:Model>
            </Model>
        </ext:Store>
    </Store>
    <ColumnModel>
        <Columns>
            <ext:Column ID="Column4" Sortable="true" MenuDisabled="true" runat="server" Text="EIN"
                Width="60" Align="Center" DataIndex="EmployeeId">
            </ext:Column>
            <ext:Column ID="colEmployeeName" Sortable="true" MenuDisabled="true" runat="server"
                Text="Name" Width="160" Align="Left" DataIndex="Name">
            </ext:Column>
             <ext:Column ID="Column6" Sortable="true" MenuDisabled="true" runat="server"
                Text="Branch" Width="120" Align="Left" DataIndex="Branch">
            </ext:Column>
            <ext:DateColumn ID="colStartDate" runat="server" Align="Right" Text="From" Width="100"
                MenuDisabled="true" Sortable="true" Format="yyyy/MMM/dd" DataIndex="StartDateEng">
            </ext:DateColumn>
            <ext:DateColumn ID="colEndDate" runat="server" Align="Right" Text="To" Width="100"
                MenuDisabled="true" Sortable="true" Format="yyyy/MMM/dd" DataIndex="EndDateEng">
            </ext:DateColumn>
            <ext:Column ID="colWorkDays" Sortable="true" MenuDisabled="true" runat="server" Text="Work Days"
                Width="100" Align="Center" DataIndex="WorkDays">
            </ext:Column>
            <ext:DateColumn ID="colSubmittedOn" runat="server" Align="Right" Text="Submitted On"
                Width="100" MenuDisabled="true" Sortable="true" Format="yyyy/MMM/dd" DataIndex="SubmittedOnEng">
            </ext:DateColumn>
            <ext:Column ID="Column5" Sortable="true" MenuDisabled="true" runat="server" Text="Status"
                Width="100" Align="Center" DataIndex="StatusText">
            </ext:Column>
            <ext:ImageCommandColumn ID="ImageCommandColumn1" runat="server" Width="70" Text="Action"
                MenuDisabled="true" Sortable="false" Align="Center">
                <Commands>
                    <ext:ImageCommand CommandName="View" Text="View" Cls="BlueCls">
                        <ToolTip Text="View Time Request Lines." />
                    </ext:ImageCommand>
                </Commands>
                <Listeners>
                    <Command Fn="CommandHandler">
                    </Command>
                </Listeners>
            </ext:ImageCommandColumn>
            <ext:ImageCommandColumn ID="ImageCommandColumn2" runat="server" Width="70" Text="Action"
                MenuDisabled="true" Sortable="false" Align="Center">
                <Commands>
                    <ext:ImageCommand CommandName="Approve" Text="Approve" Cls="GreenCls">
                        <ToolTip Text="Approve" />
                    </ext:ImageCommand>
                </Commands>
                <Listeners>
                    <Command Fn="CommandHandler">
                    </Command>
                </Listeners>
                <PrepareCommand Fn="prepareCommand" />
            </ext:ImageCommandColumn>
            <ext:ImageCommandColumn ID="ImageCommandColumn3" runat="server" Width="70" Text="Action"
                MenuDisabled="true" Sortable="false" Align="Center">
                <Commands>
                    <ext:ImageCommand CommandName="Reject" Text="Reject" Cls="RedCls">
                        <ToolTip Text="Reject" />
                    </ext:ImageCommand>
                </Commands>
                <Listeners>
                    <Command Fn="CommandHandler">
                    </Command>
                </Listeners>
                <PrepareCommand Fn="prepareCommand" />
            </ext:ImageCommandColumn>
        </Columns>
    </ColumnModel>
    <SelectionModel>
        <ext:CheckboxSelectionModel ID="CheckboxSelectionModel1" runat="server" Mode="Multi">
            <CustomConfig>
                <ext:ConfigItem Name="renderer" Value="checkboxRenderer" Mode="Raw" />
            </CustomConfig>
            <Listeners>
                <BeforeSelect Handler="return (record.data.Status == 0 || record.data.Status == 3);" />
            </Listeners>
        </ext:CheckboxSelectionModel>
    </SelectionModel>
    <View>
        <ext:GridView ID="GridView1" runat="server" StripeRows="true" />
    </View>
    <BottomBar>
        <ext:PagingToolbar ID="PagingToolbar1" runat="server" StoreID="Store1" DisplayInfo="true">
            <Items>
                <ext:Label ID="lblPageSize" runat="server" Text="Page size:" />
                <ext:ToolbarSpacer ID="ToolbarSpacer_listItems" runat="server" Width="10" />
                <ext:ComboBox ID="cmbPageSize" runat="server" Width="80" SelectOnFocus="true" Selectable="true"
                    ValueField="Value" DisplayField="Text" ForceSelection="true" AllowBlank="false">
                    <Listeners>
                        <Select Handler="searchListEL()" />
                        <%--#{cmbPageSize}.pageSize = this.getValue();#{PagingToolbar1}.moveFirst();--%>
                    </Listeners>
                    <Items>
                        <ext:ListItem Value="50" Text="50" />
                        <ext:ListItem Value="100" Text="100" />
                        <ext:ListItem Value="200" Text="200" />
                    </Items>
                    <SelectedItems>
                        <ext:ListItem Index="0">
                        </ext:ListItem>
                    </SelectedItems>
                </ext:ComboBox>
            </Items>
        </ext:PagingToolbar>
    </BottomBar>
</ext:GridPanel>
<br />
<ext:Button ID="btnApproveAll" StyleSpec="float:left;margin-left:20px" runat="server"
    Cls="btn btn-success" Text="Approve" Width="120">
    <DirectEvents>
        <Click OnEvent="btnApproveAll_Click">
            <EventMask ShowMask="true" />
            <Confirmation ConfirmRequest="true" Message="Are you sure, you want to approve the time requests?" />
            <ExtraParams>
                <ext:Parameter Name="gridItems" Value="Ext.encode(#{gridAttRequest}.getRowsValues({selectedOnly:true}))"
                    Mode="Raw" />
            </ExtraParams>
        </Click>
    </DirectEvents>
</ext:Button>
<ext:Button runat="server" OnClick="btnExport_Click" StyleSpec="float:left;margin-left:20px"
    Cls="btn btn-info" AutoPostBack="true" ID="btnExport" Text="<i></i>Export To Excel">
</ext:Button>
<div style="clear: both">
</div>
<ext:Window ID="WAttendanceReq" runat="server" Title="Time Request Details" Icon="Application" CloseAction="Hide"
    Width="1060" Height="400" BodyPadding="5" Hidden="false" Modal="false" X="-1000"
    Y="-1000">
    <Content>
        <br />
       
                    <ext:GridPanel StyleSpec="margin-top:15px; margin-bottom:15px;" ID="gridAttTimeDetl" runat="server" Cls="itemgrid"
                        Scroll="None" Width="1020">
                        <Store>
                            <ext:Store ID="Store2" runat="server">
                                <Model>
                                    <ext:Model ID="Model1" runat="server" IDProperty="RequestLineID">
                                        <Fields>
                                            <ext:ModelField Name="RequestLineID" Type="String" />
                                            <ext:ModelField Name="DateEng" Type="String" />
                                            <ext:ModelField Name="DayName" Type="string" />
                                            <ext:ModelField Name="Description" Type="string" />
                                            <ext:ModelField Name="InTimeString" Type="string" />
                                            <ext:ModelField Name="InNote" Type="string" />
                                            <ext:ModelField Name="OutTimeString" Type="string" />
                                            <ext:ModelField Name="OutNote" Type="string" />
                                            <ext:ModelField Name="WorkHoursString" Type="string" />
                                            <ext:ModelField Name="SubmittedOnString" Type="String" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:DateColumn ID="colDate" runat="server" Align="Right" Text="Date" Width="100"
                                    MenuDisabled="true" Sortable="false" Format="yyyy/MM/dd" DataIndex="DateEng">
                                </ext:DateColumn>
                                <ext:Column ID="colDayName" Sortable="false" MenuDisabled="true" runat="server" Text="Day"
                                    Width="100" Align="Left" DataIndex="DayName">
                                </ext:Column>
                                <ext:Column ID="colDescription" Sortable="false" MenuDisabled="true" runat="server"
                                    Text="Description" Width="150" Align="Left" DataIndex="Description">
                                </ext:Column>
                                <ext:Column ID="colInTime" Sortable="false" MenuDisabled="true" runat="server" Text="In Time"
                                    Align="Right" Width="80" DataIndex="InTimeString">
                                </ext:Column>
                                <ext:Column ID="colInNote" Sortable="false" MenuDisabled="true" runat="server" Text="In Note"
                                    Align="Left" Width="150" DataIndex="InNote">
                                </ext:Column>
                                <ext:Column ID="colOutTime" Sortable="false" MenuDisabled="true" runat="server" Text="Out Time"
                                    Align="Right" Width="80" DataIndex="OutTimeString">
                                </ext:Column>
                                <ext:Column ID="colOutNote" Sortable="false" MenuDisabled="true" runat="server" Text="Out Note"
                                    Align="Left" Width="150" DataIndex="OutNote">
                                </ext:Column>
                                <ext:Column ID="Column7" Sortable="false" MenuDisabled="true" runat="server" Text="Requested On"
                                    Align="Left" Width="120" DataIndex="SubmittedOnString">
                                </ext:Column>
                                <ext:Column ID="colWorkHours" Sortable="false" MenuDisabled="true" runat="server"
                                    Text="Work Hours" Align="Center" Width="80" DataIndex="WorkHoursString">
                                </ext:Column>
                            </Columns>
                        </ColumnModel>
                        <SelectionModel>
                            <ext:RowSelectionModel ID="RowSelectionModel2" runat="server" Mode="Single" />
                        </SelectionModel>
                    </ext:GridPanel>
                    <ext:ToolTip ID="ToolTip1"
                        runat="server"
                        Target="={#{gridAttTimeDetl}.getView().el}"
                        Delegate=".x-grid-cell"
                        TrackMouse="true">
                        <Listeners>
                            <Show Handler="onShow(this, #{gridAttTimeDetl});" />
                        </Listeners>
                    </ext:ToolTip>
              
            <table>
            <tr>
                 <td style="width:115px;">
                    <ext:Button ID="btnViewReject" StyleSpec="float:left;margin-left:20px;" runat="server"
                        Cls="btn btn-warning" Text="Reject" Width="100">
                        <DirectEvents>
                            <Click OnEvent="btnReject_Click">
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                </td>
                <td style="width:135px;">
                    <ext:Button ID="btnViewApprove" StyleSpec="float:left;margin-left:20px" runat="server"
                        Cls="btn btn-success" Text="Approve" Width="120">
                        <DirectEvents>
                            <Click OnEvent="btnViewApprove_Click">
                                <EventMask ShowMask="true" />
                                <Confirmation ConfirmRequest="true" Message="Are you sure, you want to approve the time request?" />
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                </td>
                <td style="width:195px;">
                    <ext:Button ID="btnViewApproveAndProcessNext" StyleSpec="float:left;margin-left:20px" runat="server"
                        Cls="btn btn-success" Text="Approve and Process Next" Width="180">
                        <DirectEvents>
                            <Click OnEvent="btnViewApproveAndProcessNext_Click">
                                <EventMask ShowMask="true" />
                                <Confirmation ConfirmRequest="true" Message="Are you sure, you want to approve the time request and process next?" />
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                </td>
               
            </tr>
        </table>
        
        <br />
    </Content>
</ext:Window>
<ext:Window runat="server" Hidden="false" X="-1000" Y="-1000" Title="Assign Time Attendance"
    ID="winAssignTimeAttend" Modal="false" Width="1100" Height="650">
    <Content>
        <div style="padding: 15px; padding-top: 5px;">
            <table class="fieldTable" style="margin-left: 20px;">
                <tr>
                    <td style="width: 170px;">
                        <ext:DateField LabelSeparator="" FieldLabel="Start Date" runat="server" LabelAlign="Top"
                            ID="txtStartDateAdd" Width="150">
                            <Plugins>
                                <ext:GenericPlugin ID="GenericPlugin1" runat="server" InstanceName="Ext.ux.DatePicker.HighlightWithNepDates" />
                            </Plugins>
                        </ext:DateField>
                        <asp:RequiredFieldValidator runat="server" ID="rfvStartDateAdd" ValidationGroup="TimeAtt"
                            ControlToValidate="txtStartDateAdd" Display="None" ErrorMessage="Start date is required."></asp:RequiredFieldValidator>
                    </td>
                    <td style="width: 170px;">
                        <ext:DateField LabelSeparator="" FieldLabel="End Date" runat="server" LabelAlign="Top"
                            ID="txtEndDateAdd" Width="150">
                            <Plugins>
                                <ext:GenericPlugin ID="GenericPlugin2" runat="server" InstanceName="Ext.ux.DatePicker.HighlightWithNepDates" />
                            </Plugins>
                        </ext:DateField>
                        <asp:RequiredFieldValidator runat="server" ID="rfvEndateDateAdd" ValidationGroup="TimeAtt"
                            ControlToValidate="txtEndDateAdd" Display="None" ErrorMessage="End date is required."></asp:RequiredFieldValidator>
                    </td>
                    <td>
                        <ext:ComboBox LabelSeparator="" ID="cmbSearch" FieldLabel="Search Employee" LabelWidth="70"
                            LabelAlign="Top" runat="server" DisplayField="Name" ValueField="EmployeeId" StoreID="storeSearch"
                            TypeAhead="false" Width="180" PageSize="9999" HideBaseTrigger="true" MinChars="1"
                            TriggerAction="All" ForceSelection="true">
                            <ListConfig LoadingText="Searching..." MinWidth="200" StyleSpec="border-top:1px solid #98c0f4;">
                                <ItemTpl ID="ItemTpl1" runat="server">
                                    <Html>
                                        <tpl>
                                            <div class="search-item">
                                                            <span>{Name}</span>  
                                             </div>
					                </tpl>
                                    </Html>
                                </ItemTpl>
                            </ListConfig>
                            <Triggers>
                                <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                            </Triggers>
                            <Listeners>
                                <Select Handler="this.getTrigger(0).show(); SelectEmp();" />
                                <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                <TriggerClick Handler="if (index == 0) { 
                                                        this.clearValue(); 
                                                    this.getTrigger(0).hide(); triggerClickFn();}" />
                            </Listeners>
                        </ext:ComboBox>
                    </td>
                    <td style="width: 320px;">
                        <ext:TextField ID="txtInNoteAdd" runat="server" FieldStyle="border:1px solid rgb(173, 216, 230);"
                            FieldLabel="Note" Text="" LabelAlign="Top" LabelSeparator="" Width="300" />
                    </td>
                    <td>
                        <ext:Button ID="btnLoad" runat="server" StyleSpec="margin-top:17px;" Cls="btn btn-save"
                            Text="Load" Width="100">
                            <DirectEvents>
                                <Click OnEvent="btnLoad_Click">
                                    <EventMask ShowMask="true" />
                                </Click>
                            </DirectEvents>
                            <Listeners>
                                <Click Handler="valGroup = 'TimeAtt'; if(CheckValidation()) return ''; else return false;">
                                </Click>
                            </Listeners>
                        </ext:Button>
                    </td>
                </tr>
            </table>
            <table class="fieldTable" style="margin-left: 20px;">
                <tr>
                    <td>
                        <ext:GridPanel StyleSpec="margin-top:15px;" ID="gridOTList" runat="server" Cls="itemgrid"
                            Scroll="Vertical" Width="990" Height="380">
                            <Store>
                                <ext:Store ID="Store6" runat="server">
                                    <Model>
                                        <ext:Model ID="OTModel" Name="SettingModel" runat="server" IDProperty="SN">
                                            <Fields>
                                                <ext:ModelField Name="SN" Type="Int" />
                                                <ext:ModelField Name="DateEng" Type="Date" />
                                                <ext:ModelField Name="InTimeDT" Type="Date" />
                                                <ext:ModelField Name="OutTimeDT" Type="Date" />
                                                <ext:ModelField Name="InNote" Type="string" />
                                                <ext:ModelField Name="OutNote" Type="string" />
                                                <ext:ModelField Name="WorkHours" Type="string" />
                                                <ext:ModelField Name="OvernightShift" Type="Boolean" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                            <ColumnModel>
                                <Columns>
                                    <ext:DateColumn ID="DateColumn1" runat="server" Align="Right" Text="Date" Width="100"
                                        MenuDisabled="true" Sortable="false" Format="yyyy/MM/dd" DataIndex="DateEng">
                                        <Editor>
                                            <ext:DateField ID="dfDateAdd" runat="server" />
                                        </Editor>
                                    </ext:DateColumn>
                                    <ext:DateColumn ID="colInTime1" Sortable="false" MenuDisabled="true" runat="server"
                                        Text="Start Time" Align="Right" Width="100" DataIndex="InTimeDT" Format="hh:mm tt">
                                        <Editor>
                                            <ext:TimeField ID="TimeField1" runat="server" MinTime="00:00" MaxTime="23:59" Increment="1"
                                                Format="hh:mm tt" SelectedTime="08:00">
                                            </ext:TimeField>
                                        </Editor>
                                    </ext:DateColumn>
                                    <ext:Column ID="Column2" Sortable="false" MenuDisabled="true" runat="server" Text="In Note"
                                        Align="Left" Width="200" DataIndex="InNote">
                                        <Editor>
                                            <ext:TextField ID="txtInNote" runat="server">
                                            </ext:TextField>
                                        </Editor>
                                    </ext:Column>
                                    <%--<ext:CheckColumn ID="colOvernightShift" MenuDisabled="true" Sortable="false" Text="Overnight Shift" DataIndex="OvernightShift" runat="server" Width="120" Editable="true">                                                      
                                        </ext:CheckColumn>--%>
                                    <ext:DateColumn ID="colOutTime1" Sortable="false" MenuDisabled="true" runat="server"
                                        Text="End Time" Align="Right" Width="100" DataIndex="OutTimeDT" Format="hh:mm tt">
                                        <Editor>
                                            <ext:TimeField ID="TimeField2" runat="server" MinTime="00:00" MaxTime="23:59" Increment="1"
                                                Format="hh:mm tt" SelectedTime="08:00">
                                            </ext:TimeField>
                                        </Editor>
                                    </ext:DateColumn>
                                    <ext:Column ID="Column3" Sortable="false" MenuDisabled="true" runat="server" Text="Out Note"
                                        Align="Left" Width="200" DataIndex="OutNote">
                                        <Editor>
                                            <ext:TextField ID="TextField1" runat="server">
                                            </ext:TextField>
                                        </Editor>
                                    </ext:Column>
                                    <ext:Column ID="Column1" Sortable="false" MenuDisabled="true" runat="server" Text="Hours:Minutes"
                                        Align="Center" Width="120" DataIndex="WorkHours">
                                        <Renderer Fn="CalculateWorkHours" />
                                    </ext:Column>
                                    <ext:CommandColumn ID="CommandColumn1" runat="server" Sortable="false" MenuDisabled="true"
                                        Width="50" Align="Center">
                                        <Commands>
                                            <ext:GridCommand Icon="Delete" CommandName="Delete" Cls="deleteicon" />
                                        </Commands>
                                        <Listeners>
                                            <Command Fn="RemoveItemLine">
                                            </Command>
                                        </Listeners>
                                    </ext:CommandColumn>
                                </Columns>
                            </ColumnModel>
                            <SelectionModel>
                                <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" Mode="Single" />
                            </SelectionModel>
                            <Plugins>
                                <ext:CellEditing ID="CellEditing1" runat="server" ClicksToEdit="1">
                                </ext:CellEditing>
                            </Plugins>
                            <View>
                                <ext:GridView ID="GridView2" runat="server">
                                    <Plugins>
                                        <ext:GridDragDrop ID="GridDragDrop1" runat="server" DragGroup="ddGroup" DropGroup="ddGroup" />
                                    </Plugins>
                                </ext:GridView>
                            </View>
                        </ext:GridPanel>
                        <ext:ToolTip ID="ToolTip2"
                                runat="server"
                                Target="={#{gridOTList}.getView().el}"
                                Delegate=".x-grid-cell"
                                TrackMouse="true">
                                <Listeners>
                                    <Show Handler="onShowAssign(this, #{gridOTList});" />
                                </Listeners>
                            </ext:ToolTip>
                    </td>
                </tr>
            </table>
            <br />
            <table id="tblRecommenderApproval" runat="server" style="margin-left: 30px; display: none;">
                <tr>
                    <td style="width: 215px;" runat="server" id="tdRecommend">
                        <ext:ComboBox ID="cmbRecommender" runat="server" ValueField="Value" DisplayField="Text"
                            FieldLabel="Sent for Recommend to *" Width="200" LabelAlign="top" LabelSeparator=""
                            ForceSelection="true" QueryMode="Local">
                            <Store>
                                <ext:Store ID="Store5" runat="server">
                                    <Model>
                                        <ext:Model ID="Model5" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="Value" Type="String" />
                                                <ext:ModelField Name="Text" Type="String" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                            <Items>
                                <ext:ListItem Text="--Select Recommender--" Value="-1" />
                            </Items>
                            <SelectedItems>
                                <ext:ListItem Text="--Select Recommender--" Value="-1" />
                            </SelectedItems>
                        </ext:ComboBox>
                    </td>
                    <td>
                        <ext:ComboBox ID="cmbApproval" runat="server" ValueField="Value" DisplayField="Text"
                            FieldLabel="Sent for Approval to *" Width="200" LabelAlign="top" LabelSeparator=""
                            ForceSelection="true" QueryMode="Local">
                            <Store>
                                <ext:Store ID="Store3" runat="server">
                                    <Model>
                                        <ext:Model ID="Model6" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="Value" Type="String" />
                                                <ext:ModelField Name="Text" Type="String" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                            <Items>
                                <ext:ListItem Text="--Select Approval--" Value="-1" />
                            </Items>
                            <SelectedItems>
                                <ext:ListItem Text="--Select Approval--" Value="-1" />
                            </SelectedItems>
                        </ext:ComboBox>
                    </td>
                </tr>
            </table>
            <br />
            <div style="margin-left: 30px;">
                <ext:Button runat="server" ID="btnAssignTimeAtt" Cls="btn btn-primary" Text="<i></i>Assign">
                    <DirectEvents>
                        <Click OnEvent="btnAssignTimeAtt_Click">
                            <Confirmation ConfirmRequest="true" Message="Are you sure, you want to assign time attendance?">
                            </Confirmation>
                            <EventMask ShowMask="true" />
                            <ExtraParams>
                                <ext:Parameter Name="gridItems" Value="Ext.encode(#{gridOTList}.getRowsValues({ selectedOnly: false }))"
                                    Mode="Raw" />
                            </ExtraParams>
                        </Click>
                    </DirectEvents>
                    <Listeners>
                        <Click Handler="valGroup = 'TimeAtt';">
                        </Click>
                    </Listeners>
                </ext:Button>
            </div>
        </div>
    </Content>
</ext:Window>
<ext:Window runat="server" Hidden="false" X="-1000" Y="-1000" Title="Reject Time Attendance"
    ID="WReject" Modal="false" Width="380" Height="210">
    <Content>
        <table class="fieldTable" style="margin-left: 20px;">
            <tr>
                <td>
                    <ext:TextArea ID="txtRejectComment" runat="server" FieldLabel="Comment *" LabelSeparator=""
                        LabelAlign="Top" Rows="3" Width="300" Height="80" />
                    <asp:RequiredFieldValidator runat="server" ID="rfvRejectComment" ValidationGroup="TimeAttComment"
                        ControlToValidate="txtRejectComment" Display="None" ErrorMessage="Reject Comment is required."></asp:RequiredFieldValidator>
                </td>
            </tr>
        </table>
        <br />
        <div style="margin-left: 20px;">
            <ext:Button runat="server" ID="btnRejectWithComment" Cls="btn btn-primary" Text="<i></i>Reject">
                <DirectEvents>
                    <Click OnEvent="btnRejectWithComment_Click">
                        <EventMask ShowMask="true" />
                    </Click>
                </DirectEvents>
                <Listeners>
                    <Click Handler="valGroup = 'TimeAttComment'; if(CheckValidation()) return ''; else return false;">
                    </Click>
                </Listeners>
            </ext:Button>
        </div>
    </Content>
</ext:Window>
