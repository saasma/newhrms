﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EDrivingLicenseCtrl.ascx.cs"
    Inherits="Web.Employee.UserControls.EDrivingLicenseCtrl" %>
<ext:Hidden runat="server" ID="hdnDrivingLiscenceID">
</ext:Hidden>
<ext:Hidden runat="server" ID="hdnIsEmployee" />
<ext:LinkButton ID="btnLoadDrLicGrid" runat="server" Hidden="true">
    <DirectEvents>
        <Click OnEvent="btnLoadDrLicGrid_Click">
            <EventMask ShowMask="true" />
        </Click>
    </DirectEvents>
</ext:LinkButton>
<ext:GridPanel StyleSpec="margin-top:15px;" ID="GridDrivingLiscence" runat="server" Width="1000" Cls="itemgrid" Scroll="None">
    <Store>
        <ext:Store ID="StoreDrivingLiscence" runat="server">
            <Model>
                <ext:Model ID="Model1" runat="server" IDProperty="DrivingLicenceId">
                    <Fields>
                        <ext:ModelField Name="LiscenceTypeName" />
                        <ext:ModelField Name="DrivingLicenceNo" />
                        <ext:ModelField Name="IssuingCountry" />
                        <ext:ModelField Name="Status" Type="Int" />
                        <ext:ModelField Name="ServerFileName" Type="String" />
                    </Fields>
                </ext:Model>
            </Model>
        </ext:Store>
    </Store>
    <ColumnModel>
        <Columns>
            <ext:Column ID="Column1" runat="server" Text="Type" DataIndex="LiscenceTypeName" Wrap="true"
                Width="200" Flex="1" />
            <ext:Column ID="Column2" runat="server" Text="Liscence Number" DataIndex="DrivingLicenceNo" Wrap="true"
                Width="200">
            </ext:Column>
            <ext:Column ID="Column3" runat="server" Text="Issuing Country" DataIndex="IssuingCountry" Wrap="true"
                Width="200">
            </ext:Column>
            <ext:CommandColumn ID="CommandColumn1" runat="server" Width="40">
                <Commands>
                    <ext:CommandSeparator />
                    <ext:GridCommand Icon="PageWhitePut" CommandName="DownLoad">
                        <ToolTip Text="DownLoad" />
                    </ext:GridCommand>
                </Commands>
                <DirectEvents>
                    <Command OnEvent="GridDrivingLiscence_Command">
                        <ExtraParams>
                            <ext:Parameter Name="ID" Value="record.data.DrivingLicenceId" Mode="Raw">
                            </ext:Parameter>
                            <ext:Parameter Name="command" Value="command" Mode="Raw">
                            </ext:Parameter>
                        </ExtraParams>
                    </Command>
                </DirectEvents>
                <PrepareToolbar Fn="prepareDrivingLicenseDownload" />
            </ext:CommandColumn>
            <ext:CommandColumn ID="CommandColumn3" runat="server" Width="40">
                <Commands>
                    <ext:CommandSeparator />
                    <ext:GridCommand ToolTip-Text="Edit" Icon="ApplicationEdit" CommandName="Edit" />
                </Commands>
                <Listeners>
                    <Command Handler="CommandHandlerDrivLic(command,record);" />
                </Listeners>
            </ext:CommandColumn>
            <ext:CommandColumn ID="CommandColumn2" runat="server" Width="40">
                <Commands>
                    <ext:CommandSeparator />
                    <ext:GridCommand ToolTip-Text="Delete" Icon="ApplicationDelete"
                                        CommandName="Delete" />
                </Commands>
                <DirectEvents>
                    <Command OnEvent="GridDrivingLiscence_Command">
                        <ExtraParams>
                            <ext:Parameter Name="ID" Value="record.data.DrivingLicenceId" Mode="Raw">
                            </ext:Parameter>
                            <ext:Parameter Name="command" Value="command" Mode="Raw">
                            </ext:Parameter>
                        </ExtraParams>
                        <Confirmation ConfirmRequest="true" Message="Are you sure, you want to delete the record?" />
                    </Command>
                </DirectEvents>
                <PrepareToolbar Fn="prepareDrivingLic" />
            </ext:CommandColumn>
             <ext:Column ID="Column4" runat="server" Text=""
                Width="280">
            </ext:Column>
        </Columns>
    </ColumnModel>
    <SelectionModel>
        <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" Mode="Single" />
    </SelectionModel>
</ext:GridPanel>
<div class="buttonBlockSection">
    <ext:Button runat="server" Cls="btn btn-primary btn-sect" Width="150" StyleSpec="margin-top:10px"
        OnClientClick="AddDrivingLic();return false;" Height="30" ID="btnAddDrLic" Text="<i></i>Add New Line">
    </ext:Button>
</div>
<script type="text/javascript">

    var prepareDrivingLic= function (grid, toolbar, rowIndex, record) {
        var delBtn = toolbar.items.get(1);
        var IsEmployee = <%= hdnIsEmployee.ClientID %>.getValue();

        if (record.data.Status == 1 && IsEmployee == "1" ) {
            delBtn.setVisible(false);
        }

    } 

    var prepareDrivingLicenseDownload = function(grid, toolbar, rowIndex, record){
        var downloadBtn = toolbar.items.get(1);
        if(record.data.ServerFileName == null || record.data.ServerFileName == ''){
            downloadBtn.setVisible(false);     
        }
    }

    function reloadDrivLicGrid() {  
        <%= btnLoadDrLicGrid.ClientID %>.fireEvent('click');
    }

     function AddDrivingLic()
        {
            addDrivingLicense();
        }
    
    var CommandHandlerDrivLic = function(command, record){
            if(command=="Edit")
            {
                addDrivingLicense('DrivingLicenceId=' + record.data.DrivingLicenceId);
            }
                
        }

</script>
