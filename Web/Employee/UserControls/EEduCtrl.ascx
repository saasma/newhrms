﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EEduCtrl.ascx.cs" Inherits="Web.Employee.UserControls.EEduCtrl" %>


<script type="text/javascript">

     var prepareDownloadEdu = function(grid, toolbar, rowIndex, record){
        var downloadBtn = toolbar.items.get(0);
        if(record.data.ContainsFile == 0){  
            downloadBtn.setVisible(false);     
        }
    }
 
    var prepareEdu = function (grid, toolbar, rowIndex, record) {
        var editDelBtn = toolbar.items.get(1);
        if (record.data.IsEditable == 0) {
            editDelBtn.setVisible(false);
        }

    } 
   
   function reloadEdnGrid()
   {
        <%= btnReloadEdu.ClientID %>.fireEvent('click');
   }

      function processChangeOrder(e1,e2,e3,e4,e5)
        {
              var sourceId = e2.records[0].data.EductionId;
            <%=hdnSource.ClientID %>.setValue(sourceId);
            var educationId = e3.data.EductionId;
            <%=hdnDest.ClientID %>.setValue(educationId);
            <%=hdnDropMode.ClientID %>.setValue(e4);

            <%=btnChangeOrder.ClientID %>.fireEvent('click');
        }
        
    function AddEdu()
        {
            addEducation();
        }

    function EditEdu()
    {
        editEducation('EducationId='+ <%=hdnEduID.ClientID %>.getValue());
    }


</script>

<ext:Hidden runat="server" ID="hdnEduID">
</ext:Hidden>
<ext:Hidden runat="server" ID="hdnDropMode" />
<ext:Hidden runat="server" ID="hdnSource" />
<ext:Hidden runat="server" ID="hdnDest" />

<ext:LinkButton ID="btnReloadEdu" runat="server" Hidden="true">
    <DirectEvents>
        <Click OnEvent="btnReloadEdu_Click">
            <EventMask ShowMask="true" />
        </Click>
    </DirectEvents>
</ext:LinkButton>

<ext:LinkButton ID="btnChangeOrder" runat="server" Hidden="true">
    <DirectEvents>
        <Click OnEvent="btnChangeOrder_Click">
            <EventMask ShowMask="true" />
        </Click>
    </DirectEvents>
</ext:LinkButton>   
                
                <ext:GridPanel ID="GridEducation" runat="server" Width="1000" Cls="itemgrid" Scroll="None" EnableDragDrop="true"
                    DDGroup="ddGroup">
                    <Store>
                        <ext:Store ID="StoreEducation" runat="server">
                            <Model>
                                <ext:Model ID="Model1" runat="server" IDProperty="EductionId">
                                    <Fields>
                                        <ext:ModelField Name="Course" Type="String" />
                                        <ext:ModelField Name="LevelName" Type="string" />
                                        <ext:ModelField Name="Order" Type="string" />
                                        <ext:ModelField Name="College" Type="string" />
                                        <ext:ModelField Name="Institution" Type="string" />
                                        <ext:ModelField Name="Country" Type="string" />
                                        <ext:ModelField Name="Percentage" Type="string" />
                                        <ext:ModelField Name="PassedYear" Type="string" />
                                        <ext:ModelField Name="IsEditable" Type="Int" />
                                        <ext:ModelField Name="ContainsFile" Type="Int" />
                                        <ext:ModelField Name="DivisionName" Type="String" />
                                    </Fields>
                                </ext:Model>
                            </Model>
                        </ext:Store>
                    </Store>
                    <ColumnModel>
                        <Columns>
                             <ext:Column ID="Column3" Width="70"  runat="server" Text="Order" DataIndex="Order" Align="Center">
                            </ext:Column>
                            <ext:Column ID="Column1" Width="180" runat="server" Text="Course Name" DataIndex="Course" Wrap="true" />
                            <ext:Column ID="Column2" Width="150"  runat="server" Text="Level" DataIndex="LevelName" Wrap="true">
                            </ext:Column>
                           
                            <ext:Column ID="Column6" Width="180"  runat="server" Text="Institution" DataIndex="College" Wrap="true">
                            </ext:Column>
                            <ext:Column ID="Column81" runat="server" Width="100"  Text="Passed Year" DataIndex="PassedYear" Wrap="true">
                            </ext:Column>
                            <ext:Column ID="Column7" runat="server" Width="100"  Text="% / Grade" DataIndex="Percentage" Wrap="true">
                            </ext:Column>
                             <ext:Column ID="Column4" runat="server" Width="100"  Text="Division" DataIndex="DivisionName" Wrap="true">
                            </ext:Column>
                            <ext:CommandColumn ID="CommandColumn1" runat="server" Width="40">
                                <Commands>
                                    <ext:GridCommand Icon="PageWhitePut" CommandName="DownLoad">
                                        <ToolTip Text="DownLoad" />
                                    </ext:GridCommand>
                                </Commands>
                                <DirectEvents>
                                    <Command OnEvent="GridEducation_Command">
                                        <ExtraParams>
                                            <ext:Parameter Name="ID" Value="record.data.EductionId" Mode="Raw">
                                            </ext:Parameter>
                                            <ext:Parameter Name="command" Value="command" Mode="Raw">
                                            </ext:Parameter>
                                        </ExtraParams>
                                    </Command>
                                </DirectEvents>
                                <PrepareToolbar Fn="prepareDownloadEdu" />
                            </ext:CommandColumn>
                            <ext:CommandColumn ID="CommandColumn21" runat="server" Width="40">
                                <Commands>
                                    <ext:CommandSeparator />
                                    <ext:GridCommand ToolTip-Text="Edit" Icon="ApplicationEdit" CommandName="Edit" />
                                    
                                </Commands>
                                <DirectEvents>
                                    <Command OnEvent="GridEducation_Command">
                                        <EventMask ShowMask="true">
                                        </EventMask>
                                        <ExtraParams>
                                            <ext:Parameter Name="ID" Value="record.data.EductionId" Mode="Raw">
                                            </ext:Parameter>
                                            <ext:Parameter Name="command" Value="command" Mode="Raw">
                                            </ext:Parameter>
                                        </ExtraParams>
                                    </Command>
                                </DirectEvents>
                            </ext:CommandColumn>
                            <ext:CommandColumn ID="CommandColumn2" runat="server" Width="40">
                                <Commands>
                                    <ext:CommandSeparator />
                                    <ext:GridCommand ToolTip-Text="Delete" Icon="ApplicationDelete"
                                        CommandName="Delete" />
                                </Commands>
                                <DirectEvents>
                                    <Command OnEvent="GridEducation_Command">
                                        <EventMask ShowMask="true">
                                        </EventMask>
                                        <Confirmation ConfirmRequest="true" Message="Are you sure, you want to delete the record?" />
                                        <ExtraParams>
                                            <ext:Parameter Name="ID" Value="record.data.EductionId" Mode="Raw">
                                            </ext:Parameter>
                                            <ext:Parameter Name="command" Value="command" Mode="Raw">
                                            </ext:Parameter>
                                        </ExtraParams>
                                    </Command>
                                </DirectEvents>
                                <PrepareToolbar Fn="prepareEdu" />
                            </ext:CommandColumn>
                        </Columns>
                    </ColumnModel>
                    <SelectionModel>
                        <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" Mode="Single" />
                    </SelectionModel>
                     <View>
                        <ext:GridView ID="GridView1" runat="server">
                            <Plugins>
                                <ext:GridDragDrop ID="GridDragDrop1" runat="server" DragGroup="ddGroup" DropGroup="ddGroup" />
                            </Plugins>
                            <Listeners>
                                <BeforeItemClick Fn="processChangeOrder" />
                                <Drop Fn="processChangeOrder" />
                                <%-- <AfterRender Handler="this.plugins[0].dragZone.getDragText = getDragDropText;" />--%>
                                <%--  <Drop Handler="var dropOn = overModel ? ' ' + dropPosition + ' ' + overModel.get('Name') : ' on empty view'; 
                                               Ext.net.Notification.show({title:'Drag from right to left', html:'Dropped ' + data.records[0].get('Name') + dropOn});" />--%>
                            </Listeners>
                        </ext:GridView>
                    </View>
                </ext:GridPanel>
          
   
   
        <ext:Button runat="server" Cls="btn btn-primary" Width="120" StyleSpec="margin-top:10px" Height="30"
            ID="btnAddNewLine" Text="<i></i>Add Education" OnClientClick="AddEdu();return false;">          
        </ext:Button>
