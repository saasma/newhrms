﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="InsuranceClaim.ascx.cs" Inherits="Web.Employee.UserControls.InsuranceClaim" %>
<style type="text/css">
    .empName {
        font-size: 22px;
        color: #3892d3;
        font-weight: bold;
    }

    .titleDesign {
        font-size: 15px;
        color: #3892d3;
        text-decoration: underline;
        padding-top: 10px;
    }
</style>

<script type="text/javascript">

    var ClaimOfChanged = function () { 
        var r = this.getStore().getById(this.getValue()).data.Relation;
        try
        {
            var indexByName= <%=cmbRelation.ClientID %>.getStore().find('Name',r);
            var id=<%=cmbRelation.ClientID %>.getStore().getAt(indexByName).get('ID');
        }
        catch(err){
           
        }
        if(id)
            <%=cmbRelation.ClientID %>.setValue(id);
    }


    function ClaimTypeChanged(title) {
        if (title == "1")
        {   
            <%=txtDetailsOfIllness.ClientID %>.setVisible(true);
            <%=txtConsultantName.ClientID%>.setVisible(true);
            <%=txtHostpitalName.ClientID%>.setVisible(true);
            <%=txtDoctorAddress.ClientID%>.setVisible(true);
            <%=txtDoctorName.ClientID%>.setVisible(true);
            <%=dfDiagnosisDate.ClientID%>.setVisible(true);


            <%=txtDetailsOfTreatment.ClientID %>.setVisible(false);
            <%=txtDetailsOfAccident.ClientID%>.setVisible(false);
            <%=txtLocationOfAccidents.ClientID%>.setVisible(false);
            <%=tfttme.ClientID%>.setVisible(false);
            <%=dfDateOfAccident.ClientID%>.setVisible(false);
        }      
        else
        {
            <%=txtDetailsOfIllness.ClientID %>.setVisible(false);
            <%=txtConsultantName.ClientID%>.setVisible(false);
            <%=txtHostpitalName.ClientID%>.setVisible(false);
            <%=txtDoctorAddress.ClientID%>.setVisible(false);
            <%=txtDoctorName.ClientID%>.setVisible(false);
            <%=dfDiagnosisDate.ClientID%>.setVisible(false);

            <%=txtDetailsOfTreatment.ClientID %>.setVisible(true);
            <%=txtDetailsOfAccident.ClientID%>.setVisible(true);
            <%=txtLocationOfAccidents.ClientID%>.setVisible(true);
            <%=tfttme.ClientID%>.setVisible(true);
            <%=dfDateOfAccident.ClientID%>.setVisible(true);
        }
    }

    function EmpSelect()
    {
        <%=hdnEmployeeId.ClientID %>.setValue(<%=cmbSearch.ClientID %>.getValue());
        <%=btnSelectEmp.ClientID %>.fireEvent('click');
    }
 

</script>


<ext:Store ID="storeRelation" runat="server">
    <Model>
        <ext:Model ID="modelRelation" runat="server" IDProperty="ID">
            <Fields>
                <ext:ModelField Name="ID" Type="string" />
                <ext:ModelField Name="Name" Type="string" />
            </Fields>
        </ext:Model>
    </Model>
</ext:Store>
<ext:Hidden runat="server" ID="hiddenID" />
<ext:Hidden runat="server" ID="hdnEmployeeId" />

<ext:Store ID="storeCmbClaimType" runat="server">
    <Model>
        <ext:Model ID="modelCmbClaimType" runat="server" IDProperty="Value">
            <Fields>
                <ext:ModelField Name="Text" Type="string" />
                <ext:ModelField Name="Value" Type="string" />
            </Fields>
        </ext:Model>
    </Model>
</ext:Store>

<%--<ext:Button runat="server" MarginSpec="15 0 0 10" ID="btnAddClaim" Cls="btn btn-primary" Text="<i></i>Add New Claim">
    <Listeners>
        <Click Handler="#{hiddenClaimId}.setValue('');" />
    </Listeners>
    <DirectEvents>
        <Click OnEvent="btnAddClaim_Click">
            <EventMask ShowMask="true" />
        </Click>
    </DirectEvents>
</ext:Button>--%>

<ext:LinkButton ID="btnSelectEmp" runat="server" Hidden="true">
    <DirectEvents>
        <Click OnEvent="btnSelectEmp_Click">
            <EventMask ShowMask="true" />
        </Click>
    </DirectEvents>
</ext:LinkButton>


<ext:Window ID="windowAddEditClaim" ButtonAlign="Left" AutoScroll="true" runat="server"
    Title="Add/Update Claim" Hidden="true" Icon="ApplicationAdd" Width="630" Height="640" BodyPadding="5" Modal="true">
    <Content>
        <table class="fieldTable">
            <tr>
                <td>
                    <ext:Store runat="server" ID="storeSearch" AutoLoad="false">
                        <Proxy>
                            <ext:AjaxProxy Url="../../Handler/EmpSearchID.ashx">
                                <ActionMethods Read="GET" />
                                <Reader>
                                    <ext:JsonReader Root="plants" TotalProperty="total" />
                                </Reader>
                            </ext:AjaxProxy>
                        </Proxy>
                        <Model>
                            <ext:Model ID="Model3" IDProperty="Name" Name="ItemLineModel" runat="server">
                                <Fields>
                                    <ext:ModelField Name="Name" Type="String" />
                                    <ext:ModelField Name="EmployeeId" Type="String" />
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>

                    <ext:ComboBox LabelSeparator="" ID="cmbSearch" runat="server" DisplayField="Name"
                        FieldLabel="" LabelAlign="Top" ValueField="EmployeeId" EmptyText="Search employee" StoreID="storeSearch" TypeAhead="false" Width="200" HideBaseTrigger="true" MinChars="1" TriggerAction="All" ForceSelection="false">
                        <ListConfig LoadingText="Searching..." MinWidth="200" StyleSpec="border-top:1px solid #98c0f4;">
                            <ItemTpl ID="ItemTpl1" runat="server">
                                <Html>
                                    <tpl>
                                            <div class="search-item">
                                                            <span>{Name}</span>  
                                             </div>
					                </tpl>
                                </Html>
                            </ItemTpl>
                        </ListConfig>
                        <Triggers>
                            <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                        </Triggers>
                        <Listeners>
                            <Select Handler="this.getTrigger(0).show(); EmpSelect();" />
                            <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                            <TriggerClick Handler="if (index == 0) {  this.clearValue();  this.getTrigger(0).hide();EmpSelect();}" />
                        </Listeners>
                    </ext:ComboBox>
                </td>
            </tr>
            <tr>
                <td>
                    <ext:Label ID="lblEName" StyleSpec="font-size: 16px;font-weight: bold;" runat="server" />
                    <br />
                    <ext:Label ID="lblEININo" StyleSpec="font-weight: bold;" runat="server" Text="" />
                </td>
            </tr>

            <tr>

                <td>
                    <ext:ComboBox ID="cmbClaimType" runat="server" StoreID="storeCmbClaimType" ForceSelection="true" FieldLabel="Claim Type *" DisplayField="Text" ValueField="Value"
                        LabelAlign="Top" QueryMode="Local" LabelSeparator="">
                        <Listeners>
                            <Select Handler="ClaimTypeChanged(this.getValue());">
                            </Select>
                        </Listeners>
                    </ext:ComboBox>
                    <asp:RequiredFieldValidator Display="None" ID="rfvcmbClaimType" runat="server"
                        ValidationGroup="SaveUpdateClaimPopup" ControlToValidate="cmbClaimType" ErrorMessage="Claim Type is required." /></td>
                <td>
                    <ext:DateField ID="dfClaimDate" runat="server" FieldLabel="Claim Date *" LabelAlign="top" LabelSeparator="" Width="150">
                        <Plugins>
                            <ext:GenericPlugin ID="GenericPlugin2" runat="server" InstanceName="Ext.ux.DatePicker.HighlightWithNepDates" />
                        </Plugins>
                    </ext:DateField>
                    <asp:RequiredFieldValidator Enabled="true" Display="None" ID="rfvdfClaimDate" runat="server" ValidationGroup="SaveUpdateClaimPopup" ControlToValidate="dfClaimDate" ErrorMessage="Claim Date is required." />
                </td>
                <td></td>
            </tr>
            <tr>
                <td>
                    <ext:ComboBox ID="cmbClaimOf" runat="server" ForceSelection="true" FieldLabel="Claim Of *" Width="175" DisplayField="Name" ValueField="FamilyId" LabelAlign="Top" QueryMode="Local" LabelSeparator="">
                        <Store>
                            <ext:Store ID="storecmbClaimOf" runat="server">
                                <Model>
                                    <ext:Model ID="modelcmbClaimOf" runat="server" IDProperty="FamilyId">
                                        <Fields>
                                            <ext:ModelField Name="Name" Type="string" />
                                            <ext:ModelField Name="FamilyId" Type="string" />
                                            <ext:ModelField Name="Relation" Type="String" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <SelectedItems>
                            <ext:ListItem Value="0" />
                        </SelectedItems>
                        <Listeners>
                            <Select Fn="ClaimOfChanged">
                            </Select>
                        </Listeners>
                    </ext:ComboBox>
                    <asp:RequiredFieldValidator Display="None" ID="rfvcmbClaimOf" runat="server" ValidationGroup="SaveUpdateClaimPopup" ControlToValidate="cmbClaimOf" ErrorMessage="Claim Of is required." />
                </td>
                <td>
                    <ext:ComboBox ID="cmbRelation" Disabled="true" runat="server" ForceSelection="true" StoreID="storeRelation"
                        FieldLabel="Relation" DisplayField="Name" ValueField="ID" LabelAlign="Top" QueryMode="Local" LabelSeparator="">
                        <SelectedItems>
                            <ext:ListItem Value="0" />
                        </SelectedItems>
                    </ext:ComboBox>
                </td>
                <td></td>
            </tr>
            <tr>
                <td>
                    <ext:DateField ID="dfDiagnosisDate" EmptyDate="" PaddingSpec="15 0 0 0" runat="server" FieldLabel="Diagnosis Date" LabelAlign="top" LabelSeparator="" Width="175">
                        <Plugins>
                            <ext:GenericPlugin ID="GenericPlugin3" runat="server" InstanceName="Ext.ux.DatePicker.HighlightWithNepDates" />
                        </Plugins>
                    </ext:DateField>
                </td>
                <td>
                    <ext:TextField ID="txtDoctorName" PaddingSpec="15 0 0 0" runat="server" FieldLabel="Doctor's Name" LabelAlign="Top" LabelSeparator=""></ext:TextField>
                </td>
                <td>
                    <ext:TextField ID="txtDoctorAddress" PaddingSpec="15 0 0 0" runat="server" FieldLabel="Doctor's Address" LabelAlign="Top" LabelSeparator=""></ext:TextField>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <ext:TextField ID="txtHostpitalName" runat="server" FieldLabel="Hospital's Name & Address (if relevant)" Width="340" LabelAlign="Top" LabelSeparator=""></ext:TextField>
                </td>
                <td>
                    <ext:TextField ID="txtConsultantName" runat="server" FieldLabel="Consultant's Name" LabelAlign="Top" LabelSeparator=""></ext:TextField>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <ext:TextArea ID="txtDetailsOfIllness" runat="server" FieldLabel="Details of illness" LabelAlign="Top" LabelSeparator="" Width="520"></ext:TextArea>
                </td>
            </tr>
            <tr>
                <td>
                    <ext:DateField ID="dfDateOfAccident" EmptyDate="" Hidden="true" runat="server" FieldLabel="Date of Accident" LabelAlign="top" LabelSeparator="" Width="175">
                        <Plugins>
                            <ext:GenericPlugin ID="GenericPlugin4" runat="server" InstanceName="Ext.ux.DatePicker.HighlightWithNepDates" />
                        </Plugins>
                    </ext:DateField>
                </td>
                <td>
                    <ext:TimeField ID="tfttme" Hidden="true" FieldLabel="Time of Accident" LabelAlign="top" LabelSeparator="" runat="server" MinTime="00:00" MaxTime="23:50" Increment="10" SelectedTime="00:00" Format="hh:mm tt" />
                </td>
                <td>
                    <ext:TextField ID="txtLocationOfAccidents" Hidden="true" runat="server" FieldLabel="Location of Accident" LabelAlign="Top" LabelSeparator=""></ext:TextField>

                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <ext:TextArea ID="txtDetailsOfAccident" Hidden="true" runat="server" FieldLabel="Details of Accident" LabelAlign="Top" LabelSeparator="" Width="520"></ext:TextArea>
                </td>

            </tr>
            <tr>
                <td colspan="3">
                    <ext:TextArea ID="txtDetailsOfTreatment" Hidden="true" runat="server" FieldLabel="Details of Treatment Undertaken" LabelAlign="Top" LabelSeparator="" Width="520"></ext:TextArea>
                </td>
            </tr>
            <tr>
                <td>
                    <ext:NumberField ID="txtTotalClaim" AllowDecimals="true" MinValue="0" runat="server" FieldLabel="Total Claim (in Rs.) *" LabelAlign="Top" LabelSeparator=""></ext:NumberField>
                    <asp:RequiredFieldValidator Display="None" ID="rfvtxtTotalClaim" runat="server" ValidationGroup="SaveUpdateClaimPopup" ControlToValidate="txtTotalClaim" ErrorMessage="Claim amount is required." />
                </td>
            </tr>
        </table>
    </Content>
    <Buttons>
        <ext:Button runat="server" Cls="btn btn-primary" StyleSpec="margin-left:10px" ValidationGroup="SaveUpdateClaimPopup"
            ID="btnSave" Text="<i></i>Save">
            <DirectEvents>
                <Click OnEvent="btnSave_Click">
                    <EventMask ShowMask="true" />
                </Click>
            </DirectEvents>
            <Listeners>
                <Click Handler="valGroup = 'SaveUpdateClaimPopup'; return CheckValidation();">
                </Click>
            </Listeners>
        </ext:Button>
        <ext:LinkButton runat="server" StyleSpec="margin-left:10px;padding:0px;" Cls="btnFlatLeftGap" ID="LinkButton1"
            Text="<i></i>Cancel">
            <Listeners>
                <Click Handler="#{windowAddEditClaim}.hide();">
                </Click>
            </Listeners>
        </ext:LinkButton>
    </Buttons>
</ext:Window>

