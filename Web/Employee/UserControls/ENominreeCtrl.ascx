﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ENominreeCtrl.ascx.cs" Inherits="Web.Employee.UserControls.ENominreeCtrl" %>

<script type="text/javascript">

    var prepareHRNominee = function (grid, toolbar, rowIndex, record) {
        var editDelBtn = toolbar.items.get(1);
        if (record.data.IsEditable == 0) {
            editDelBtn.setVisible(false);
        }

    };

    function reloadNomineeGrid()
        {
            <%= btnReloadNomineeGrid.ClientID %>.fireEvent('click');
        }

    function AddNominee() {
        addNominees();
    }

    function EditNominee() {
        editNominees('NomineeId=' + <%=hdnNomineeId.ClientID %>.getValue());
    }
    
</script>

<ext:Hidden runat="server" ID="hdnNomineeId">
</ext:Hidden>

<ext:LinkButton runat="server" Hidden="true" ID="btnReloadNomineeGrid">
    <DirectEvents>
        <Click OnEvent="btnReloadNomineeGrid_Click">
            <EventMask ShowMask="true" />
        </Click>
    </DirectEvents>
</ext:LinkButton>

<ext:GridPanel ID="gridNominee" runat="server" Width="1000" Cls="itemgrid" Scroll="None">
                    <Store>
                        <ext:Store ID="storeNominee" runat="server">
                            <Model>
                                <ext:Model ID="Model1" runat="server" IDProperty="NomineeId">
                                    <Fields>
                                        <ext:ModelField Name="NomineeId" Type="String" />
                                        <ext:ModelField Name="SN" Type="string" />
                                        <ext:ModelField Name="NomineeName" Type="string" />
                                        <ext:ModelField Name="Relation" Type="string" />
                                        <ext:ModelField Name="EffectiveDate" Type="string" />
                                        <ext:ModelField Name="IsEditable" Type="Int" />
                                    </Fields>
                                </ext:Model>
                            </Model>
                        </ext:Store>
                    </Store>
                    <ColumnModel>
                        <Columns>
                            <ext:Column ID="Column3" Width="100" runat="server" Text="Nominee No" Align="Center" DataIndex="SN" />
                            <ext:Column ID="Column1" Width="200" runat="server" Text="Nominee Name" DataIndex="NomineeName" Wrap="true" />
                            <ext:Column ID="Column2" Width="150" runat="server" Text="Relation" DataIndex="Relation" Wrap="true">
                            </ext:Column>
                            <ext:Column ID="Column6" Width="100" runat="server" Text="EffectiveDate" DataIndex="EffectiveDate">
                            </ext:Column>
                          
                       <ext:CommandColumn ID="CommandColumn21" runat="server" Width="40">
                                <Commands>
                                    <ext:CommandSeparator />
                                    <ext:GridCommand ToolTip-Text="Edit" Icon="ApplicationEdit" CommandName="Edit" />
                                    
                                </Commands>
                                <DirectEvents>
                                    <Command OnEvent="gridNominee_Command">
                                        <EventMask ShowMask="true">
                                        </EventMask>
                                        <ExtraParams>
                                            <ext:Parameter Name="ID" Value="record.data.NomineeId" Mode="Raw">
                                            </ext:Parameter>
                                            <ext:Parameter Name="command" Value="command" Mode="Raw">
                                            </ext:Parameter>
                                        </ExtraParams>
                                    </Command>
                                </DirectEvents>
                            </ext:CommandColumn>
                            <ext:CommandColumn ID="CommandColumn2" runat="server" Width="40">
                                <Commands>
                                    <ext:CommandSeparator />
                                    <ext:GridCommand ToolTip-Text="Delete" Icon="ApplicationDelete"
                                        CommandName="Delete" />
                                </Commands>
                                <DirectEvents>
                                    <Command OnEvent="gridNominee_Command">
                                        <EventMask ShowMask="true">
                                        </EventMask>
                                        <Confirmation ConfirmRequest="true" Message="Are you sure, you want to delete the record?" />
                                        <ExtraParams>
                                            <ext:Parameter Name="ID" Value="record.data.NomineeId" Mode="Raw">
                                            </ext:Parameter>
                                            <ext:Parameter Name="command" Value="command" Mode="Raw">
                                            </ext:Parameter>
                                        </ExtraParams>
                                    </Command>
                                </DirectEvents>
                                <PrepareToolbar Fn="prepareHRNominee" />
                            </ext:CommandColumn>
                             <ext:Column ID="Column4" Width="370" runat="server" Text="">
                            </ext:Column>
                        </Columns>
                    </ColumnModel>
                    <SelectionModel>
                        <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" Mode="Single" />
                    </SelectionModel>
                </ext:GridPanel>


    <ext:Button runat="server" Cls="btn btn-primary" Width="120" StyleSpec="margin-top:10px" Height="30"
            ID="btnAddNewLine" Text="<i></i>Add Nominee" OnClientClick="AddNominee(); return false;">
        </ext:Button>