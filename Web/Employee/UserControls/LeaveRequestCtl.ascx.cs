﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Manager;
using BLL.BO;
using BLL;
using DAL;
using Ext.Net;
using BLL.Entity;
using System.Text;

namespace Web.Employee.UserControls
{
    public partial class LeaveRequestCtl : BLL.Base.BaseUserControl
    {

        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (CommonManager.Setting.HideReviewInLeaveRequest != null && CommonManager.Setting.HideReviewInLeaveRequest.Value)
            {
                cmbReviewTo.Hide();
                lblReviewTo.Hide();
            }

            X.Js.AddScript("WhichCompany=" + ((int)CommonManager.CompanySetting.WhichCompany).ToString() + ";");
            if (CommonManager.CompanySetting.WhichCompany == WhichCompany.NIBL)
            {
                liHalfDay.Style["display"] = "none";              
                
                chkIsSpecialCase.Show();
                compensatoryIsDeduct.Checked = true;
               // trCompensatory.Style["display"] = "none";
                //txtSpecialComment.Show();
                //linkSpecialCase.Show();
            }

            // hide second leave for other company, as for Dishhome they can take Sunday Leave with another half day then only reqd
            // for all cases hide it
            if (CommonManager.CompanySetting.WhichCompany != WhichCompany.DishHome)
            {
                cmbLeaveType2.Hide();
            }


            if (CommonManager.CalculationConstant.CompanyHasHourlyLeave != null
                && CommonManager.CalculationConstant.CompanyHasHourlyLeave.Value)
            {
                inTime.Show();
                outTime.Show();
                lblTotalDaysValue.Hide();
                lblTotalDaysText.Hide();
                txtEndDate.HideLabel = true;
                txtEndDate.LabelWidth = 0;
                txtEndDate.Width = new Unit(100);


                X.Js.AddScript("hasHourLeave=true;");


                chkHalfDay.Hide();
            }
            else
                txtHourlyLeaveHours.Visible = false;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                if (IsEnglish)
                    rowNepDate.Style["display"] = "none";


                //txtStartDate.Text = DateTime.Now.ToString("yyyy/MM/ddd");
            }

        }


        public void ShowRequestWindow()
        {
            eventWindow.Show();
            eventWindow.Center();
        }




        public void ShowDetails(List<GetLeaveListAsPerEmployeeResult> leaves, int leaveRequestID)
        {
            hdnLeaveRequestId.Text = leaveRequestID.ToString();
            DAL.LeaveRequest request = LeaveAttendanceManager.GetLeaveRequestById(leaveRequestID);

            if (request.IsCancelRequest != null && request.IsCancelRequest.Value)
            {
                btnCancelShowRequest_Click(null, null);
                return;
            }

            txtReason.Text = request.Reason;

            cmbLeaveType1.Store[0].ClearFilter();
            lblTotalDaysValue.Text = request.DaysOrHours.ToString();
            if (request.LeaveTypeId == 0)
                cmbLeaveType1.SetValue("-1");
            else
                cmbLeaveType1.SetValue(request.LeaveTypeId.ToString());
            if (request.LeaveTypeId2 != null)
            {
                if (request.Days1 != null)
                    txtLeaveType1.Number = request.Days1.Value;
                if (request.Days2 != null)
                    txtLeaveType2.Number = request.Days2.Value;
                if (request.LeaveTypeId2 == 0)
                    cmbLeaveType2.SetValue("-1");
                else
                    cmbLeaveType2.SetValue(request.LeaveTypeId2.ToString());


                txtLeaveType1.Show();
                txtLeaveType2.Show();
            }
            else
                cmbLeaveType2.ClearValue();

            if (request.IsSpecialCase != null && request.IsSpecialCase.Value)
            {
                chkIsSpecialCase.Checked = request.IsSpecialCase.Value;
                //txtSpecialComment.Text = request.SpecialComment;
            }

            //if (request.ChildLeaveTypeId != null)
            //    cmbChildLeaves.SetValue(request.ChildLeaveTypeId.ToString());
            //else
            //    cmbChildLeaves.ClearValue();

            txtStartDate.SelectedDate = request.FromDateEng.Value;
            txtEndDate.SelectedDate = request.ToDateEng.Value;

            if (request.LeaveTypeId != 0 &&
                new LeaveAttendanceManager().GetLeaveType(request.LeaveTypeId).FreqOfAccrual ==
                LeaveAccrue.COMPENSATORY)
            {
                if (CommonManager.CompanySetting.WhichCompany != WhichCompany.NIBL)
                    X.AddScript("trCompensatory.style.display = 'block';");
                if (request.CompensatorIsAddType != null)
                {
                    if (request.CompensatorIsAddType.Value)
                    {
                        compensatoryIsAdd.Checked = true;
                        compensatoryIsDeduct.Checked = false;
                    }
                    else
                    {
                        compensatoryIsAdd.Checked = false;
                        compensatoryIsDeduct.Checked = true;
                    }
                }
            }
            else
                X.AddScript("trCompensatory.style.display = 'none';");

            if (request.IsHalfDayLeave != null)
            {
                chkHalfDay.Checked = request.IsHalfDayLeave.Value;
                if (!string.IsNullOrEmpty(request.HalfDayType))
                    cmbAMPM.SetValue(request.HalfDayType);
            }
            lblStatus.Text = ((LeaveRequestStatusEnum)request.Status).ToString();

            // in multiple flow show, "Awaiting from : Name", in which employee has halt the flow
            if ((CommonManager.Setting.LeaveRequestSettingUsingPreDefinedList != null
                && CommonManager.Setting.LeaveRequestSettingUsingPreDefinedList.Value)
                ||
                (CommonManager.Setting.LeaveRequestSettngUsingOneToOne != null
                && CommonManager.Setting.LeaveRequestSettngUsingOneToOne.Value)
                )
            {
                // if recommended then halted from Approval
                if (request.Status == (int)LeaveRequestStatusEnum.Recommended
                    && request.SupervisorEmployeeIdInCaseOfSelection != null)
                {
                    lblStatus.Text += ",&nbsp;&nbsp; Awaiting from : " + EmployeeManager.GetEmployeeName(request.SupervisorEmployeeIdInCaseOfSelection.Value);
                }
                else if (request.Status == (int)LeaveRequestStatusEnum.Request
                    && request.RecommendEmployeeId != null)
                {
                    lblStatus.Text += ",&nbsp;&nbsp; Awaiting from : " + EmployeeManager.GetEmployeeName(request.RecommendEmployeeId.Value);
                }
            }


            if (CommonManager.CalculationConstant.CompanyHasHourlyLeave.Value)
            {
                txtHourlyLeaveHours.Number = request.DaysOrHours.Value;
                if (!string.IsNullOrEmpty(request.StartTime))
                    inTime.SelectedTime = new TimeSpan((Convert.ToDateTime(request.StartTime).Ticks));
                if (!string.IsNullOrEmpty(request.EndTime))
                    outTime.SelectedTime = new TimeSpan((Convert.ToDateTime(request.EndTime).Ticks));
            }


            if (request.SubstituteEmployeeId != null)
            {
                cmbSubstitute.GetStore().RemoveAll();
                cmbSubstitute.InsertItem(0, EmployeeManager.GetEmployeeName(request.SubstituteEmployeeId.Value), request.SubstituteEmployeeId.Value);
                cmbSubstitute.SelectedItem.Value = request.SubstituteEmployeeId.Value.ToString();
                cmbSubstitute.SelectedItem.Text = EmployeeManager.GetEmployeeName(request.SubstituteEmployeeId.Value);
                cmbSubstitute.UpdateSelectedItems();

                hdnEmployeeId.Text = request.SubstituteEmployeeId.ToString();
            }
            else
            {
                hdnEmployeeId.Text = "";
            }

            X.Js.AddScript("halfDayLeaveChange();");

            if (CommonManager.CompanySetting.LeaveRequestToOnlyOneSupervisor ||
                (CommonManager.Setting.LeaveRequestSettingUsingPreDefinedList != null && CommonManager.Setting.LeaveRequestSettingUsingPreDefinedList.Value)
                || (CommonManager.Setting.LeaveRequestSettngUsingOneToOne != null && CommonManager.Setting.LeaveRequestSettngUsingOneToOne.Value))
            {
                if (request.SupervisorEmployeeIdInCaseOfSelection != null)
                {
                    cmbApplyTo.Store[0].ClearFilter();
                    cmbApplyTo.SetValue(request.SupervisorEmployeeIdInCaseOfSelection.ToString());
                }
                if (request.RecommendEmployeeId != null)
                {
                    cmbReviewTo.Store[0].ClearFilter();
                    cmbReviewTo.SetValue(request.RecommendEmployeeId.ToString());

                }
            }
            eventWindow.Center();
            eventWindow.Show();
            btnSave.Hide();
            //btnUpdate.Show();
            btnCancel.Show();
            //lblOr.Show();

            if (((LeaveRequestStatusEnum)request.Status) == LeaveRequestStatusEnum.Request ||
                ((LeaveRequestStatusEnum)request.Status) == LeaveRequestStatusEnum.ReDraft)
            {
            }
            else
            {
                //btnUpdate.Hide();
                btnCancel.Hide();
                //lblOr.Hide();
            }

            // Show leave cancel for Recommend Status only
            if (request.Status == (int)(LeaveRequestStatusEnum.Recommended))
            {
                //lblOr.Show();
                btnActualCancel.Show();
            }
            else
                btnActualCancel.Hide();

            if (request.Status == (int)LeaveRequestStatusEnum.Approved && CommonManager
                .CompanySetting.WhichCompany == WhichCompany.NIBL &&
                LeaveRequestManager.IsCancelRequestNotCreatedForLeaveRequest(request.LeaveRequestId))
            {
                btnCancelRequest.Show();
            }
            else
                btnCancelRequest.Hide();

            //if (string.IsNullOrEmpty(txtSpecialComment.Text.Trim()))
            //    txtSpecialComment.Hide();
            //else
            //    txtSpecialComment.Show();

        }
        public void LoadDetails(List<GetLeaveListAsPerEmployeeResult> leaves)
        {
            // Set Apply To Employee
            if (CommonManager.CompanySetting.LeaveRequestToOnlyOneSupervisor ||
                (CommonManager.Setting.LeaveRequestSettingUsingPreDefinedList != null && CommonManager.Setting.LeaveRequestSettingUsingPreDefinedList.Value)
                || (CommonManager.Setting.LeaveRequestSettngUsingOneToOne != null && CommonManager.Setting.LeaveRequestSettngUsingOneToOne.Value))
            {
                List<TextValue> listReview = LeaveRequestManager.GetApplyToListForEmployee(SessionManager.CurrentLoggedInEmployeeId, false,PreDefindFlowType.Leave);
                List<TextValue> listApproval = LeaveRequestManager.GetApplyToListForEmployee(SessionManager.CurrentLoggedInEmployeeId, true, PreDefindFlowType.Leave);

                cmbApplyTo.Store[0].DataSource = listApproval;
                cmbApplyTo.Store[0].DataBind();

                cmbReviewTo.Store[0].DataSource = listReview;
                cmbReviewTo.Store[0].DataBind();

                //cmbReviewToCancel.Store[0].DataSource = listReview;
                //cmbReviewToCancel.Store[0].DataBind();

                //cmbApplyToCancel.Store[0].DataSource = listApproval;
                //cmbApplyToCancel.Store[0].DataBind();

                cmbApplyTo.Show();
                lblApplyTo.Hide();

                lblReviewTo.Hide();
                cmbReviewTo.Show();
                //lblReviewTo.Show();

                cmbApplyTo.SelectedItems.Add(new Ext.Net.ListItem { Index = 0 });
                cmbReviewTo.SelectedItems.Add(new Ext.Net.ListItem { Index = 0 });
                //cmbApplyTo.Show();
            }
            else
            {
                //cmbApplyTo.Hide();
                lblApplyTo.Show();
                string review = "", approval = "";
                LeaveRequestManager.GetApplyToForEmployee(SessionManager.CurrentLoggedInEmployeeId, ref review, ref approval);
                lblApplyTo.Text = approval;
                lblApplyToCancel.Text = approval;
                lblReviewTo.Text = review;
                if (CommonManager.CompanySetting.WhichCompany != WhichCompany.NIBL && !string.IsNullOrEmpty(review))
                    lblReviewTo.Show();
            }

            if(CommonManager.Setting.HideUnpaidLeaveInLeaveRequest != null && CommonManager.Setting.HideUnpaidLeaveInLeaveRequest.Value)
                cmbLeaveType1.Store[0].DataSource = leaves.Where(x => x.IsChildLeave == false).Where(x=>x.LeaveTypeId != -1).OrderBy(x => x.Title).ToList();
            else
                cmbLeaveType1.Store[0].DataSource = leaves.Where(x => x.IsChildLeave == false).OrderBy(x=>x.Title).ToList();

            cmbLeaveType1.Store[0].DataBind();

            //cmbChildLeaves.Store[0].DataSource = leaves.Where(x => x.IsChildLeave == true).ToList();
            //cmbChildLeaves.Store[0].DataBind();
            //cmbChildLeaves.Hide();

            //cmbLeaveType1.SelectedItems.Add(new Ext.Net.ListItem { Index = 0 });
            if (leaves[0].IsHalfDayAllowed == false)
            {
                cmbAMPM.Hide();
                chkHalfDay.Hide();
            }
            //if (leaves[0].IsParentGroupLeave)
            //    cmbChildLeaves.Show();

            cmbLeaveType2.Store[0].DataSource = leaves;
            cmbLeaveType2.Store[0].DataBind();

            if (leaves[0].LeaveTypeId != 0 && leaves[0].FreqOfAccrual == LeaveAccrue.COMPENSATORY)
            {
                //if (CommonManager.CompanySetting.WhichCompany != WhichCompany.NIBL)
                //    trCompensatory.Style["display"] = "block";
                cmbLeaveType2.Hide();
            }
            

            //EEmployee employee = new EmployeeManager().GetById(SessionManager.CurrentLoggedInEmployeeId);
            //cmbSubstitute.Store[0].DataSource = new EmployeeManager().GetEmployeesByBranch(employee.BranchId.Value, -1)
            //    .Where(x => x.EmployeeId != SessionManager.CurrentLoggedInEmployeeId).ToList();
            //cmbSubstitute.Store[0].DataBind();

        }

        #region "Cancel Request"
        protected void btnCancelShowRequest_Click(object sender, DirectEventArgs e)
        {
            DAL.LeaveRequest request = LeaveAttendanceManager.GetLeaveRequestById(int.Parse(hdnLeaveRequestId.Text));

            if (request.IsCancelRequest != null && request.IsCancelRequest.Value)
            {
                txtReasonCancel.Text = request.Reason;
            }

            cmbLeaveType1Cancel.Store[0].DataSource = LeaveAttendanceManager.GetAllLeaves(SessionManager.CurrentCompanyId);
            cmbLeaveType1Cancel.Store[0].DataBind();


            cmbLeaveType1Cancel.Store[0].ClearFilter();

            if (request.LeaveTypeId == 0)
                cmbLeaveType1Cancel.SetValue("-1");
            else
                cmbLeaveType1Cancel.SetValue(request.LeaveTypeId.ToString());

            //if (request.ChildLeaveTypeId != null)
            //    cmbLeaveType1Cancel.SetValue(request.ChildLeaveTypeId.ToString());


            txtStartDateCancel.SelectedDate = request.FromDateEng.Value;
            txtEndDateCancel.SelectedDate = request.ToDateEng.Value;

            if (request.LeaveTypeId != 0 &&
                new LeaveAttendanceManager().GetLeaveType(request.LeaveTypeId).FreqOfAccrual ==
                LeaveAccrue.COMPENSATORY)
            {
                if (CommonManager.CompanySetting.WhichCompany != WhichCompany.NIBL)
                    X.AddScript("trCompensatoryCancel.style.display = 'block';");

                if (request.CompensatorIsAddType != null)
                {
                    if (request.CompensatorIsAddType.Value)
                    {
                        compensatoryIsAddCancel.Checked = true;
                        compensatoryIsDeductCancel.Checked = false;
                    }
                    else
                    {
                        compensatoryIsAddCancel.Checked = false;
                        compensatoryIsDeductCancel.Checked = true;
                    }
                }
            }
            else
                X.AddScript("trCompensatoryCancel.style.display = 'none';");

            if (request.IsHalfDayLeave != null)
            {
                chkHalfDayCancel.Checked = request.IsHalfDayLeave.Value;
                if (!string.IsNullOrEmpty(request.HalfDayType))
                    cmbAMPMCancel.SetValue(request.HalfDayType);
            }



            if (request.IsCancelRequest != null && request.IsCancelRequest.Value)
            {
                btnSaveCancel.Hide();
                lblStatusCancel.Text = ((LeaveRequestStatusEnum)request.Status).ToString();
            }
            else
            {
                lblStatusCancel.Text = "";
                btnSaveCancel.Show();
            }
            

            // in multiple flow show, "Awaiting from : Name", in which employee has halt the flow
            if (
                (CommonManager.Setting.LeaveRequestSettingUsingPreDefinedList != null
                && CommonManager.Setting.LeaveRequestSettingUsingPreDefinedList.Value)
                ||
                  (CommonManager.Setting.LeaveRequestSettngUsingOneToOne != null
                && CommonManager.Setting.LeaveRequestSettngUsingOneToOne.Value)
                )
            {
                // if recommended then halted from Approval
                if (request.Status == (int)LeaveRequestStatusEnum.Recommended
                    && request.SupervisorEmployeeIdInCaseOfSelection != null)
                {
                    lblStatusCancel.Text += ",&nbsp;&nbsp; Awaiting from : " + EmployeeManager.GetEmployeeName(request.SupervisorEmployeeIdInCaseOfSelection.Value);
                }
                else if (request.Status == (int)LeaveRequestStatusEnum.Request
                    && request.RecommendEmployeeId != null)
                {
                    lblStatusCancel.Text += ",&nbsp;&nbsp; Awaiting from : " + EmployeeManager.GetEmployeeName(request.RecommendEmployeeId.Value);
                }
            }


          

            if (CommonManager.CompanySetting.LeaveRequestToOnlyOneSupervisor ||
                ((CommonManager.Setting.LeaveRequestSettingUsingPreDefinedList != null && CommonManager.Setting.LeaveRequestSettingUsingPreDefinedList.Value))
                ||
                  (CommonManager.Setting.LeaveRequestSettngUsingOneToOne != null && CommonManager.Setting.LeaveRequestSettngUsingOneToOne.Value)
                )
            {
                //if (request.SupervisorEmployeeIdInCaseOfSelection != null)
                //{
                //    cmbApplyToCancel.Store[0].ClearFilter();
                //    cmbApplyToCancel.SetValue(request.SupervisorEmployeeIdInCaseOfSelection.ToString());
                //}
                //if (request.RecommendEmployeeId != null)
                //{
                //    cmbReviewToCancel.Store[0].ClearFilter();
                //    cmbReviewToCancel.SetValue(request.RecommendEmployeeId.ToString());

                //}
            }
            windowCancelRequest.Center();
            windowCancelRequest.Show();

            //btnSaveCancel.Hide();
            //btnUpdate.Show();
            //btnCancel.Show();
            //lblOr.Show();


        }

     protected void btnDeleteCancel_Click(object sender, DirectEventArgs e)
     {

     }

     protected void btnSaveCancel_Click(object sender, DirectEventArgs e)
     {
         ResponseStatus status = new ResponseStatus();
         //DAL.LeaveRequest entity = new DAL.LeaveRequest();
         DAL.LeaveRequest leaveRequest = new DAL.LeaveRequest();
         //LeaveModel entity = new LeaveModel();


         //if (chkIsSpecialCase.Checked && string.IsNullOrEmpty(txtSpecialComment.Text.Trim()))
         //{
         //    NewMessage.ShowWarningMessage("Note is required for Special case.");
         //    return;
         //}

         leaveRequest.Reason = txtReasonCancel.Text.Trim();
         leaveRequest.Reason = HttpUtility.HtmlEncode(leaveRequest.Reason);
         leaveRequest.FromDate = txtStartDateCancel.Text.Trim();
         leaveRequest.ToDate = txtEndDateCancel.Text.Trim();
         leaveRequest.LeaveTypeId = int.Parse(cmbLeaveType1Cancel.SelectedItem.Value);
         //entity.IsSpecialCase = chkIsSpecialCase.Checked;
         //entity.SpecialComment = txtSpecialComment.Text.Trim();
         //leave.IsSpecialCase = chkIsSpecialCase.Checked;
         //leave.SpecialComment = txtSpecialComment.Text.Trim();
         leaveRequest.EmployeeId = SessionManager.CurrentLoggedInEmployeeId;


         if (leaveRequest.LeaveTypeId != -1
             && LeaveAttendanceManager.GetLeaveTypeById(leaveRequest.LeaveTypeId).FreqOfAccrual == LeaveAccrue.COMPENSATORY)
         {
             if (compensatoryIsAddCancel.Checked)
                 leaveRequest.CompensatorIsAddType = true;
             else if (compensatoryIsDeductCancel.Checked)
                 leaveRequest.CompensatorIsAddType = false;
         }

         //leaveRequest.DaysOrHours =( leaveRequest.ToDate - leaveRequest.FromDate)

         //leaveRequest.Days1 = txtLeaveType1.Number;
         //leaveRequest.Days2 = txtLeaveType2.Number;

         // set default days for Sunday leave
         //if (LeaveAttendanceManager.IsDishHomeSundayLeave(leaveRequest.LeaveTypeId) && leaveRequest.LeaveTypeId2 != 0)
         //{
         //    leaveRequest.Days1 = 2;
         //    leaveRequest.Days2 = 1;
         //}

         //if (cmbLeaveType2.SelectedItem != null && cmbLeaveType2.SelectedItem.Value != null)
         //    entity.LeaveTypeId2 = int.Parse(cmbLeaveType2.SelectedItem.Value);
         //if (cmbChildLeaves.SelectedItem != null && cmbChildLeaves.SelectedItem.Value != null)
         //    entity.ChildLeaveTypeId = int.Parse(cmbChildLeaves.SelectedItem.Value);
         leaveRequest.IsHalfDayLeave = chkHalfDayCancel.Checked;
         if (cmbAMPMCancel.SelectedItem != null && cmbAMPMCancel.SelectedItem.Value != null)
             leaveRequest.HalfDayType = cmbAMPMCancel.SelectedItem.Value;

         DateTime date = DateTime.Parse(leaveRequest.FromDate);
         leaveRequest.FromDateEng = new DateTime(date.Year, date.Month, date.Day);
         date = DateTime.Parse(leaveRequest.ToDate);
         leaveRequest.ToDateEng = new DateTime(date.Year, date.Month, date.Day);

         leaveRequest.IsHour = false;

         if (leaveRequest.IsHalfDayLeave.Value)
         {
             leaveRequest.ToDateEng = leaveRequest.FromDateEng;
             leaveRequest.DaysOrHours = (double)0.5;
             leaveRequest.LeaveTypeId2 = null;
             leaveRequest.Days2 = 0;

         }
         // If not Hour or Half day then it must be more than one day leave
         else
         {
             leaveRequest.DaysOrHours = (double)(leaveRequest.ToDateEng.Value - leaveRequest.FromDateEng.Value).Days + 1;
         }

         leaveRequest.FromDate = BLL.BaseBiz.GetAppropriateDate(leaveRequest.FromDateEng.Value);
         leaveRequest.ToDate = BLL.BaseBiz.GetAppropriateDate(leaveRequest.ToDateEng.Value);

         leaveRequest.Reason = leaveRequest.Reason.Trim();

         //if (isInsert)
         leaveRequest.CreatedOn = BLL.BaseBiz.GetCurrentDateAndTime();
         leaveRequest.ModifiedOn = BLL.BaseBiz.GetCurrentDateAndTime();
         leaveRequest.Status = (int)LeaveRequestStatusEnum.Request;

         leaveRequest.EditSequence = 1;
         //if (cmbSubstitute.SelectedItem != null && cmbSubstitute.SelectedItem.Value != null)
         //    entity.SubstituteEmployeeId = int.Parse(cmbSubstitute.SelectedItem.Value);


         //if (!string.IsNullOrEmpty(cmbSubstitute.Text))
         //{
         //    if (!string.IsNullOrEmpty(hdnEmployeeId.Text))
         //        entity.SubstituteEmployeeId = int.Parse(hdnEmployeeId.Text);
         //}

         //if (!string.IsNullOrEmpty(hdnLeaveRequestId.Text))
         {
             leaveRequest.IsCancelRequest = true;
             leaveRequest.LeaveRequestRef_Id = int.Parse(hdnLeaveRequestId.Text);
             //leave.LeaveRequestId = int.Parse(hdnLeaveRequestId.Text);
         }

         DAL.LeaveRequest originalLeaveRequest = LeaveAttendanceManager.GetLeaveRequestById(leaveRequest.LeaveRequestRef_Id.Value);
         if (originalLeaveRequest != null)
         {
             if (leaveRequest.FromDateEng < originalLeaveRequest.FromDateEng ||
                 leaveRequest.ToDateEng > originalLeaveRequest.ToDateEng)
             {
                 NewMessage.ShowWarningMessage("From and To date should lie within the approved leave.");
                 return;
             }
         }

         //if (CommonManager.CompanySetting.LeaveRequestToOnlyOneSupervisor ||
         //    ((CommonManager.Setting.LeaveRequestSettingUsingPreDefinedList != null && CommonManager.Setting.LeaveRequestSettingUsingPreDefinedList.Value)))
         //{
         //    if (cmbReviewToCancel.SelectedItem != null && cmbReviewToCancel.SelectedItem.Value != null)
         //        leaveRequest.RecommendEmployeeId = int.Parse(cmbReviewToCancel.SelectedItem.Value);

         //    if (cmbApplyToCancel.SelectedItem == null || cmbApplyToCancel.SelectedItem.Value == null)
         //    {
         //        NewMessage.ShowWarningMessage("Approval must be selected.");
         //        return;
         //    }
         //    if (cmbReviewToCancel.SelectedItem == null || cmbReviewToCancel.SelectedItem.Value == null)
         //    {
         //        NewMessage.ShowWarningMessage("Reviewer must be selected.");
         //        return;
         //    }


         //    leaveRequest.SupervisorEmployeeIdInCaseOfSelection = int.Parse(cmbApplyTo.SelectedItem.Value);

         //    // if no Review then set Approval emp in Review also
         //    if (CommonManager.Setting.HideReviewInLeaveRequest != null && CommonManager.Setting.HideReviewInLeaveRequest.Value)
         //        leaveRequest.RecommendEmployeeId = leaveRequest.SupervisorEmployeeIdInCaseOfSelection;

         //}
         //if (!string.IsNullOrEmpty(selectedSupervisor))
         //    leave.SupervisorEmployeeIdInCaseOfSelection = int.Parse(selectedSupervisor);




         //if (!string.IsNullOrEmpty(hdnLeaveRequestId.Text))
         //{
         //    leave.LeaveRequestId = int.Parse(hdnLeaveRequestId.Text);
         //}


         //if (status.IsSuccess == "true")
         {
             bool email = true;
             //leave.HalfDayType = halfDayType;
             LeaveAttendanceManager.SaveUpdateCancelRequest(leaveRequest, ref email);

             if (email)
             {
                    LeaveAttendanceManager.NotifyThroughMessageAndEmail(leaveRequest.LeaveRequestId, leaveRequest.IsHour,
                     leaveRequest.DaysOrHours.ToString(), leaveRequest.Reason, leaveRequest.FromDateEng.ToString(), leaveRequest.ToDateEng.ToString(),
                     ((LeaveRequestStatusEnum)leaveRequest.Status).ToString(), leaveRequest.SupervisorEmployeeIdInCaseOfSelection
                     , leaveRequest.RecommendEmployeeId, null);
             }

             windowCancelRequest.Hide();
             eventWindow.Hide();
             //if (!string.IsNullOrEmpty(hdnLeaveRequestId.Text))
             //    NewMessage.ShowNormalMessage("Leave request has been updated.");
             //else
             NewMessage.ShowNormalMessage("Cancel request has been created.");
             X.Js.AddScript("loadRequest();");
         }
         //else
         //{
         //    NewMessage.ShowWarningMessage(status.ErrorMessage);
         //}


         //return status;
     }

        #endregion 

   

        protected void btnShow_Click(object sender, DirectEventArgs e)
        {
            DAL.LeaveRequest request = LeaveAttendanceManager.GetLeaveRequestById(int.Parse(hdnLeaveRequestId.Text));

            if (request.IsCancelRequest != null && request.IsCancelRequest.Value)
            {
                btnCancelShowRequest_Click(sender, e);
                return;
            }

            txtReason.Text = request.Reason;
           
            cmbLeaveType1.Store[0].ClearFilter();
            lblTotalDaysValue.Text = request.DaysOrHours.ToString();
            if (request.LeaveTypeId == 0)
                cmbLeaveType1.SetValue("-1");
            else
                cmbLeaveType1.SetValue(request.LeaveTypeId.ToString());
            if (request.LeaveTypeId2 != null)
            {
                if (request.Days1 != null)
                    txtLeaveType1.Number = request.Days1.Value;
                if (request.Days2 != null)
                    txtLeaveType2.Number = request.Days2.Value;
                if (request.LeaveTypeId2 == 0)
                    cmbLeaveType2.SetValue("-1");
                else
                    cmbLeaveType2.SetValue(request.LeaveTypeId2.ToString());


                txtLeaveType1.Show();
                txtLeaveType2.Show();
            }
            else
                cmbLeaveType2.ClearValue();

            if (request.IsSpecialCase != null && request.IsSpecialCase.Value)
            {
                chkIsSpecialCase.Checked = request.IsSpecialCase.Value;
                //txtSpecialComment.Text = request.SpecialComment;
            }

            //if (request.ChildLeaveTypeId != null)
            //    cmbChildLeaves.SetValue(request.ChildLeaveTypeId.ToString());
            //else
            //    cmbChildLeaves.ClearValue();

            txtStartDate.SelectedDate = request.FromDateEng.Value;
            txtEndDate.SelectedDate = request.ToDateEng.Value;

            if (request.LeaveTypeId != 0 &&
                new LeaveAttendanceManager().GetLeaveType(request.LeaveTypeId).FreqOfAccrual ==
                LeaveAccrue.COMPENSATORY)
            {
                if (CommonManager.CompanySetting.WhichCompany != WhichCompany.NIBL)
                    X.AddScript("trCompensatory.style.display = 'block';");
                if (request.CompensatorIsAddType != null)
                {
                    if (request.CompensatorIsAddType.Value)
                    {
                        compensatoryIsAdd.Checked = true;
                        compensatoryIsDeduct.Checked = false;
                    }
                    else
                    {
                        compensatoryIsAdd.Checked = false;
                        compensatoryIsDeduct.Checked = true;
                    }
                }
            }
            else
                X.AddScript("trCompensatory.style.display = 'none';");

            if (request.IsHalfDayLeave != null)
            {
                chkHalfDay.Checked = request.IsHalfDayLeave.Value;
                if (!string.IsNullOrEmpty(request.HalfDayType))
                    cmbAMPM.SetValue(request.HalfDayType);
            }
            lblStatus.Text = ((LeaveRequestStatusEnum)request.Status).ToString();
            
            // in multiple flow show, "Awaiting from : Name", in which employee has halt the flow
            if( (CommonManager.Setting.LeaveRequestSettingUsingPreDefinedList != null 
                && CommonManager.Setting.LeaveRequestSettingUsingPreDefinedList.Value)
                ||
                (CommonManager.Setting.LeaveRequestSettngUsingOneToOne != null
                && CommonManager.Setting.LeaveRequestSettngUsingOneToOne.Value)
                )
            {
                // if recommended then halted from Approval
                if (request.Status == (int)LeaveRequestStatusEnum.Recommended 
                    && request.SupervisorEmployeeIdInCaseOfSelection != null)
                {
                    lblStatus.Text += ",&nbsp;&nbsp; Awaiting from : " + EmployeeManager.GetEmployeeName(request.SupervisorEmployeeIdInCaseOfSelection.Value);
                }
                else if (request.Status == (int)LeaveRequestStatusEnum.Request
                    && request.RecommendEmployeeId != null)
                {
                    lblStatus.Text += ",&nbsp;&nbsp; Awaiting from : " + EmployeeManager.GetEmployeeName(request.RecommendEmployeeId.Value);
                }
            }


            if (CommonManager.CalculationConstant.CompanyHasHourlyLeave.Value)
            {
                txtHourlyLeaveHours.Number = request.DaysOrHours.Value;
                if (!string.IsNullOrEmpty(request.StartTime))
                    inTime.SelectedTime = new TimeSpan((Convert.ToDateTime(request.StartTime).Ticks));
                if (!string.IsNullOrEmpty(request.EndTime))
                    outTime.SelectedTime = new TimeSpan((Convert.ToDateTime(request.EndTime).Ticks));
            }


            if (request.SubstituteEmployeeId != null)
            {
                cmbSubstitute.GetStore().RemoveAll();
                cmbSubstitute.InsertItem(0, EmployeeManager.GetEmployeeName(request.SubstituteEmployeeId.Value), request.SubstituteEmployeeId.Value);
                cmbSubstitute.SelectedItem.Value = request.SubstituteEmployeeId.Value.ToString();
                cmbSubstitute.SelectedItem.Text = EmployeeManager.GetEmployeeName(request.SubstituteEmployeeId.Value);
                cmbSubstitute.UpdateSelectedItems();

                hdnEmployeeId.Text = request.SubstituteEmployeeId.ToString();
            }
            else
            {
                hdnEmployeeId.Text = "";            
            }

            X.Js.AddScript("halfDayLeaveChange();");

            if (CommonManager.CompanySetting.LeaveRequestToOnlyOneSupervisor ||
                (CommonManager.Setting.LeaveRequestSettingUsingPreDefinedList != null && CommonManager.Setting.LeaveRequestSettingUsingPreDefinedList.Value)
                || (CommonManager.Setting.LeaveRequestSettngUsingOneToOne != null && CommonManager.Setting.LeaveRequestSettngUsingOneToOne.Value))
            {
                if (request.SupervisorEmployeeIdInCaseOfSelection != null)
                {
                    cmbApplyTo.Store[0].ClearFilter();
                    cmbApplyTo.SetValue(request.SupervisorEmployeeIdInCaseOfSelection.ToString());
                }
                if (request.RecommendEmployeeId != null)
                {
                    cmbReviewTo.Store[0].ClearFilter();
                    cmbReviewTo.SetValue(request.RecommendEmployeeId.ToString());
                   
                }
            }
            eventWindow.Center();
            eventWindow.Show();
            btnSave.Hide();
            //btnUpdate.Show();
            btnCancel.Show();
            //lblOr.Show();

            if (((LeaveRequestStatusEnum)request.Status) == LeaveRequestStatusEnum.Request ||
                ((LeaveRequestStatusEnum)request.Status) == LeaveRequestStatusEnum.ReDraft)
            {
            }
            else
            {
                //btnUpdate.Hide();
                btnCancel.Hide();
                //lblOr.Hide();
            }

            // Show leave cancel for Recommend Status only
            if (request.Status == (int)(LeaveRequestStatusEnum.Recommended))
            {
                //lblOr.Show();
                btnActualCancel.Show();
            }
            else
                btnActualCancel.Hide();

            if (request.Status == (int)LeaveRequestStatusEnum.Approved && CommonManager
                .CompanySetting.WhichCompany == WhichCompany.NIBL &&
                LeaveRequestManager.IsCancelRequestNotCreatedForLeaveRequest(request.LeaveRequestId))
            {
                btnCancelRequest.Show();
            }
            else
                btnCancelRequest.Hide();

            //if (string.IsNullOrEmpty(txtSpecialComment.Text.Trim()))
            //    txtSpecialComment.Hide();
            //else
            //    txtSpecialComment.Show();
        }

        protected void btnCancelRequest_Click(object sender, DirectEventArgs e)
        {

        }
        protected void btnSave_Click(object sender, DirectEventArgs e)
        {
            ResponseStatus status = new ResponseStatus();
            DAL.LeaveRequest entity = new DAL.LeaveRequest();
            DAL.LeaveRequest leave = new DAL.LeaveRequest();
            //LeaveModel entity = new LeaveModel();


            //if (chkIsSpecialCase.Checked && string.IsNullOrEmpty(txtSpecialComment.Text.Trim()))
            //{
            //    NewMessage.ShowWarningMessage("Note is required for Special case.");
            //    return;
            //}

            if (cmbLeaveType1.SelectedItem == null || cmbLeaveType1.SelectedItem.Value == null)
            {
                NewMessage.ShowWarningMessage("Leave type is required.");
                return;
            }

            entity.Reason = txtReason.Text.Trim();
            entity.Reason = HttpUtility.HtmlEncode(entity.Reason);
            entity.FromDate = txtStartDate.Text.Trim();
            entity.ToDate = txtEndDate.Text.Trim();
            entity.LeaveTypeId = int.Parse(cmbLeaveType1.SelectedItem.Value);
            entity.IsSpecialCase = chkIsSpecialCase.Checked;
            //entity.SpecialComment = txtSpecialComment.Text.Trim();
            leave.IsSpecialCase = chkIsSpecialCase.Checked;
            //leave.SpecialComment = txtSpecialComment.Text.Trim();


            if (entity.LeaveTypeId != -1 
                && LeaveAttendanceManager.GetLeaveTypeById(entity.LeaveTypeId).FreqOfAccrual == LeaveAccrue.COMPENSATORY)
            {
                if (compensatoryIsAdd.Checked)
                    leave.CompensatorIsAddType = true;
                else if (compensatoryIsDeduct.Checked)
                    leave.CompensatorIsAddType = false;
            }

            entity.Days1 = txtLeaveType1.Number;
            entity.Days2 = txtLeaveType2.Number;

            // set default days for Sunday leave
            if (LeaveAttendanceManager.IsDishHomeSundayLeave(entity.LeaveTypeId) && entity.LeaveTypeId2 != 0)
            {
                entity.Days1 = 2;
                entity.Days2 = 1;
            }

            if (cmbLeaveType2.SelectedItem != null && cmbLeaveType2.SelectedItem.Value != null)
                entity.LeaveTypeId2 = int.Parse(cmbLeaveType2.SelectedItem.Value);
            //if (cmbChildLeaves.SelectedItem != null && cmbChildLeaves.SelectedItem.Value != null)
            //    entity.ChildLeaveTypeId = int.Parse(cmbChildLeaves.SelectedItem.Value);
            entity.IsHalfDayLeave = chkHalfDay.Checked;
            if (cmbAMPM.SelectedItem != null && cmbAMPM.SelectedItem.Value != null)
                entity.HalfDayType = cmbAMPM.SelectedItem.Value;

           
            //if (cmbSubstitute.SelectedItem != null && cmbSubstitute.SelectedItem.Value != null)
            //    entity.SubstituteEmployeeId = int.Parse(cmbSubstitute.SelectedItem.Value);


            if (!string.IsNullOrEmpty(cmbSubstitute.Text))
            {
                if (!string.IsNullOrEmpty(hdnEmployeeId.Text))
                    entity.SubstituteEmployeeId = int.Parse(hdnEmployeeId.Text);
            }
            
            if (!string.IsNullOrEmpty(hdnLeaveRequestId.Text))
            {
                entity.LeaveRequestId = int.Parse(hdnLeaveRequestId.Text);
                leave.LeaveRequestId = int.Parse(hdnLeaveRequestId.Text);
            }

            // for hourly leave
            if (CommonManager.CalculationConstant.CompanyHasHourlyLeave != null &&
                CommonManager.CalculationConstant.CompanyHasHourlyLeave.Value)
            {
                double hour = 0;
                if (double.TryParse(txtHourlyLeaveHours.Text, out hour) == false)
                {
                    NewMessage.ShowWarningMessage("Invalid hour.");
                    return;
                }
                leave.IsHour = true;
                leave.DaysOrHours = hour;

                TimeSpan time;
                if (TimeSpan.TryParse(inTime.Text, out time) == false)
                {
                    NewMessage.ShowWarningMessage("Invalid in time.");
                    return;
                }
                leave.StartTime = Convert.ToDateTime(inTime.Text).ToString("hh:mm tt");
                if (TimeSpan.TryParse(outTime.Text, out time) == false)
                {
                    NewMessage.ShowWarningMessage("Invalid out time.");
                    return;
                }
                leave.EndTime = Convert.ToDateTime(outTime.Text).ToString("hh:mm tt");
            }

            // For Prabhu if Casual leave being taken in Friday and it is not half day then make it half day
            if (CommonManager.CompanySetting.WhichCompany == WhichCompany.Prabhu && entity.LeaveTypeId == 5 &&
                txtStartDate.SelectedDate == txtEndDate.SelectedDate && txtStartDate.SelectedDate.DayOfWeek == DayOfWeek.Friday
                && entity.IsHalfDayLeave == false)
            {
                entity.IsHalfDayLeave = true;
                entity.HalfDayType = "AM";
            }

            status = LeaveRequestManager.GetLeaveRequestFromCalendarEntity(entity, leave,true, SessionManager.CurrentLoggedInEmployeeId);

            leave.SubstituteEmployeeId = entity.SubstituteEmployeeId;

            if (CommonManager.CompanySetting.LeaveRequestToOnlyOneSupervisor ||
                (CommonManager.Setting.LeaveRequestSettingUsingPreDefinedList != null && CommonManager.Setting.LeaveRequestSettingUsingPreDefinedList.Value)
                || (CommonManager.Setting.LeaveRequestSettngUsingOneToOne != null && CommonManager.Setting.LeaveRequestSettngUsingOneToOne.Value))
            {
                if (cmbReviewTo.SelectedItem != null && cmbReviewTo.SelectedItem.Value != null)
                    leave.RecommendEmployeeId = int.Parse(cmbReviewTo.SelectedItem.Value);

                if (cmbApplyTo.SelectedItem == null || cmbApplyTo.SelectedItem.Value == null)
                {
                    NewMessage.ShowWarningMessage("Approval must be selected.");
                    return;
                }
                if (cmbReviewTo.SelectedItem == null || cmbReviewTo.SelectedItem.Value == null)
                {
                    NewMessage.ShowWarningMessage("Reviewer must be selected.");
                    return;
                }
                leave.SupervisorEmployeeIdInCaseOfSelection = int.Parse(cmbApplyTo.SelectedItem.Value);

                // if no Review then set Approval emp in Review also
                if (CommonManager.Setting.HideReviewInLeaveRequest != null && CommonManager.Setting.HideReviewInLeaveRequest.Value)
                    leave.RecommendEmployeeId = leave.SupervisorEmployeeIdInCaseOfSelection;
            }
            
            // if has Recommender and Approval drop down and Recommender selected is the employee himself then save the leave in Recommend status
            //  instead of Request
            if (leave.RecommendEmployeeId != null && leave.RecommendEmployeeId == SessionManager.CurrentLoggedInEmployeeId
                && cmbReviewTo.SelectedItem != null && cmbReviewTo.SelectedItem.Value != null)
                leave.Status = (int)LeaveRequestStatusEnum.Recommended;
            


            if (!string.IsNullOrEmpty(hdnLeaveRequestId.Text))
            {
                leave.LeaveRequestId = int.Parse(hdnLeaveRequestId.Text);
            }


            if (status.IsSuccess == "true")
            {
                bool email = true;
                //leave.HalfDayType = halfDayType;

                if(leave.ChildLeaveTypeId != null)
                    LeaveAttendanceManager.SaveUpdateParentLeaveRequest(leave, ref email);
                else
                    LeaveAttendanceManager.SaveUpdateLeaveRequest(leave,ref email);

                if (email)
                {
                    LeaveAttendanceManager.NotifyThroughMessageAndEmail(leave.LeaveRequestId, leave.IsHour,
                        leave.DaysOrHours.ToString(), leave.Reason, leave.FromDateEng.ToString(), leave.ToDateEng.ToString(), 
                        ((LeaveRequestStatusEnum)leave.Status).ToString(), 
                        leave.SupervisorEmployeeIdInCaseOfSelection
                        , leave.RecommendEmployeeId, null);
                }

                eventWindow.Hide();
                if (!string.IsNullOrEmpty(hdnLeaveRequestId.Text))
                    NewMessage.ShowNormalMessage("Leave request has been updated.");
                else
                    NewMessage.ShowNormalMessage("Leave request has been created.");
                X.Js.AddScript("loadRequest();");
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }


            //return status;
        }

        protected void btnCancel_Click(object sender, DirectEventArgs e)
        {

            ResponseStatus status = new ResponseStatus();// LeaveRequestManager.ValidateLeaveRequestDelete(leaveRequestId);
            int leaveRequestId = int.Parse(hdnLeaveRequestId.Text);
            DAL.LeaveRequest leave = LeaveAttendanceManager.GetLeaveRequestById(leaveRequestId);

            if (leave.EmployeeId != SessionManager.CurrentLoggedInEmployeeId)
            {
                status.ErrorMessage = "Other's leave can not be changed.";
                NewMessage.ShowWarningMessage(status.ErrorMessage);
                return;
            }

            if (leave.Status != (int)LeaveRequestStatusEnum.Request && leave.Status != (int)LeaveRequestStatusEnum.ReDraft)
            {
                status.ErrorMessage = "Only Request and ReDraft status leave can be cancelled.";
                NewMessage.ShowWarningMessage(status.ErrorMessage);
                return;
            }

            if (HttpContext.Current.User.Identity.IsAuthenticated == false)
            {
                status.ErrorMessage = "Not enough permission.";
                NewMessage.ShowWarningMessage(status.ErrorMessage);
                return;
            }

            //if (status.IsSuccess == "false")
            //    return status;

            if (LeaveRequestManager.DeleteSelfLeave(leaveRequestId))
            {
                status.IsSuccess = "true";

                X.Js.AddScript("loadRequest();");
                eventWindow.Hide();
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
                return;
            }

            

        }

        protected void btnActualCancel_Click(object sender, DirectEventArgs e)
        {

            ResponseStatus status = new ResponseStatus();// LeaveRequestManager.ValidateLeaveRequestDelete(leaveRequestId);
            int leaveRequestId = int.Parse(hdnLeaveRequestId.Text);
            DAL.LeaveRequest leave = LeaveAttendanceManager.GetLeaveRequestById(leaveRequestId);

            if (leave.EmployeeId != SessionManager.CurrentLoggedInEmployeeId)
            {
                status.ErrorMessage = "Other's leave can not be changed.";
                NewMessage.ShowWarningMessage(status.ErrorMessage);
                return;
            }

            if (leave.Status != (int)LeaveRequestStatusEnum.Recommended)
            {
                status.ErrorMessage = "Only Recommend status leave can be cancelled.";
                NewMessage.ShowWarningMessage(status.ErrorMessage);
                return;
            }

            if (HttpContext.Current.User.Identity.IsAuthenticated == false)
            {
                status.ErrorMessage = "Not enough permission.";
                NewMessage.ShowWarningMessage(status.ErrorMessage);
                return;
            }


            if (LeaveRequestManager.CancelRecommendedSelfLeave(leaveRequestId))
            {
                status.IsSuccess = "true";

                X.Js.AddScript("loadRequest();");
                eventWindow.Hide();
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
                return;
            }



        }
        protected void btnClear_Click(object sender, DirectEventArgs e)
        {
            txtReason.Text = "";
            cmbSubstitute.ClearValue();          
            cmbLeaveType2.ClearValue();
            txtLeaveType1.Number = 0;
            txtLeaveType2.Number = 0;
            hdnLeaveRequestId.Text = "";
            lblStatus.Text = "";
            btnSave.Show();
            //btnUpdate.Hide();
            btnCancel.Hide();
            //lblOr.Show();
            btnCancelRequest.Hide();
            //txtSpecialComment.Text="";
            //txtSpecialComment.Hide();
        }


        public void ShowLeaveRequestWindow(int leaveTypeId)
        {
            cmbLeaveType1.SelectedItem.Value = leaveTypeId.ToString();
            eventWindow.Center();
            eventWindow.Show();

        }

        //protected void btnUpdate_Click(object sender, DirectEventArgs e)
        //{
        //    ResponseStatus status = new ResponseStatus();
        //    DAL.LeaveRequest entity = new DAL.LeaveRequest();
        //    DAL.LeaveRequest leave = new DAL.LeaveRequest();
        //    //LeaveModel entity = new LeaveModel();

        //    entity.LeaveRequestId = int.Parse(hdnLeaveRequestId.Text);
        //    leave.LeaveRequestId = entity.LeaveRequestId;

        //    entity.Reason = txtReason.Text.Trim();
        //    entity.Reason = HttpUtility.HtmlEncode(entity.Reason);
        //    entity.FromDate = txtStartDate.Text.Trim();
        //    entity.ToDate = txtEndDate.Text.Trim();
        //    entity.LeaveTypeId = int.Parse(cmbLeaveType1.SelectedItem.Value);
        //    entity.Days1 = txtLeaveType1.Number;
        //    entity.Days2 = txtLeaveType2.Number;
        //    if (cmbLeaveType2.SelectedItem != null && cmbLeaveType2.SelectedItem.Value != null)
        //        entity.LeaveTypeId2 = int.Parse(cmbLeaveType2.SelectedItem.Value);
        //    entity.IsHalfDayLeave = chkHalfDay.Checked;
        //    if (cmbAMPM.SelectedItem != null && cmbAMPM.SelectedItem.Value != null)
        //        entity.HalfDayType = cmbAMPM.SelectedItem.Value;

        //    status = LeaveRequestManager.GetLeaveRequestFromCalendarEntity(entity, leave, false, SessionManager.CurrentLoggedInEmployeeId);



        //    //if (!string.IsNullOrEmpty(selectedSupervisor))
        //    //    leave.SupervisorEmployeeIdInCaseOfSelection = int.Parse(selectedSupervisor);

        //    if (status.IsSuccess == "true")
        //    {
        //        //leave.HalfDayType = halfDayType;
        //        LeaveAttendanceManager.SaveUpdateLeaveRequest(leave);

        //        LeaveRequestService.NotifyThroughMessageAndEmail(leave.LeaveRequestId, leave.IsHour,
        //            leave.DaysOrHours.ToString(), leave.Reason, leave.FromDateEng.ToString(), leave.ToDateEng.ToString(), ((LeaveRequestStatusEnum)leave.Status).ToString(), leave.SupervisorEmployeeIdInCaseOfSelection, leave.RecommendEmployeeId, null);

        //        X.Js.AddScript("loadRequest();");
        //        eventWindow.Hide();

        //    }
        //    else
        //    {
        //        NewMessage.ShowWarningMessage(status.ErrorMessage);
        //    }



        //    //return status;
        //}

    }
}