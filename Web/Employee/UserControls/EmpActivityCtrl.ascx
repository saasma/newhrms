﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EmpActivityCtrl.ascx.cs" Inherits="Web.Employee.UserControls.EmpActivityCtrl" %>



<ext:Hidden ID="hdnActivityId" runat="server" />
<ext:Hidden ID="hdnPlanId" runat="server" />
<ext:Hidden ID="hdnIsNotDateChange" runat="server" />
<ext:Hidden ID="hdnDetailId" runat="server" />
<ext:Hidden ID="hdnIsFromDashboard" runat="server" />


<ext:Store ID="storeClient" runat="server">
    <Model>
        <ext:Model ID="Model7" runat="server" IDProperty="ClientSoftwareId">
            <Fields>
                <ext:ModelField Name="ClientSoftwareId" Type="String" />
                <ext:ModelField Name="Name" />
            </Fields>
        </ext:Model>
    </Model>
</ext:Store>
<ext:Store ID="storeType" runat="server">
    <Model>
        <ext:Model ID="Model8" runat="server" IDProperty="ActivityType">
            <Fields>
                <ext:ModelField Name="ActivityType" Type="String"/>
                <ext:ModelField Name="Name"/>
            </Fields>
        </ext:Model>
    </Model>
</ext:Store>

<ext:Window ID="WEmpActivity" runat="server" Title="Daily Activity" Icon="Application" OverflowY="Scroll"
        Resizable="false" Width="1060" Height="460" BodyPadding="5" Hidden="true" Modal="true" ButtonAlign="Left">
        <Content>
            <table style="margin-left: 20px; margin-top: 20px;">
                <tr>
                    <td style="vertical-align:top;">
                        <table>
                            <tr>
                                <td colspan="3">
                                    <ext:DateField ID="txtActivityDate" runat="server" Width="120" LabelAlign="Top" FieldLabel="Date"
                                        LabelSeparator="">
                                        <Plugins>
                                            <ext:GenericPlugin ID="GenericPlugin4" runat="server" InstanceName="Ext.ux.DatePicker.HighlightWithNepDates" />
                                        </Plugins>
                                    </ext:DateField>
                                    <asp:RequiredFieldValidator Display="None" ID="rfvActivityDate" runat="server" ValidationGroup="SaveEmpActivity"
                                        ControlToValidate="txtActivityDate" ErrorMessage="Date is required." />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <ext:ComboBox DisplayField="Name" QueryMode="Local" ForceSelection="true" ValueField="ActivityType" LabelAlign="Top" FieldLabel="Activity Type"
                                        StoreID="storeType" ID="cmbActivityType" MinChars="2" runat="server" Width="200" LabelSeparator="" MarginSpec="10 0 0 0" >
                                        <Listeners>
                                            <BeforeQuery Handler="var q = queryEvent.query;if(q=='') return;
                                                queryEvent.query = new RegExp(q, 'ig');
                                                queryEvent.query.length = q.length;" />
                                        </Listeners>
                                    </ext:ComboBox>
                                    <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator3" runat="server" ValidationGroup="SaveEmpActivity"
                                        ControlToValidate="cmbActivityType" ErrorMessage="Activity Type is required." />
                                </td>
                                <td>
                                    <ext:TextField ID="txtWorkingHour" MaskRe="[0-9]|\:|%" runat="server" FieldLabel="Hours:Minutes" Text="" LabelAlign="Top" MarginSpec="10 0 0 0"
                                        LabelSeparator="" Width="120" />
                                </td>
                                <td style="width:200px;"></td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <ext:TagField ID="tfClients" QueryMode="Local" MinChars="2" StoreID="storeClient" DisplayField="Name" ValueField="ClientSoftwareId" runat="server" MarginSpec="20 0 0 0" Width="550" TypeAhead="true" FieldLabel="Any clients involved?" LabelSeparator="" LabelAlign="Top">
                                        <Listeners>
                                            <BeforeQuery Handler="var q = queryEvent.query;if(q=='') return;
                                                queryEvent.query = new RegExp(q, 'ig');
                                                queryEvent.query.length = q.length;" />
                                        </Listeners>
                                    </ext:TagField>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <ext:TextArea ID="txtDetails" FieldLabel="Write some details" runat="server" LabelAlign="Top" MarginSpec="10 0 0 0"
                                        LabelSeparator="" Width="550" Rows="7">
                                    </ext:TextArea>
                                    <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator5" runat="server" ValidationGroup="SaveEmpActivity"
                                        ControlToValidate="txtDetails" ErrorMessage="Write some details." />
                                </td>
                            </tr>
                        </table>
                    </td>


                    <td style="vertical-align:top;">
                        <table style="margin-left:20px;">
                            <tr>
                                <td style="width:110px;">
                                    <ext:Image ID="img" runat="server" Width="80" Height="94" />
                                </td>
                                <td style="vertical-align:bottom;">
                                        <ext:Label ID="lblEmpName" runat="server" StyleSpec="color:#356FA2; font-size:large;"  /><br />
                                        <ext:Label ID="lblDesignation" runat="server" />
                                </td>
                            </tr>

                            <tr class="spacerRowCls">
                                <td></td><td></td>
                            </tr>
                            </table>
                            
                                
                           <table runat="server" id="tblComments" style="margin-left:20px;">
                            <tr class="trHeaderCls">
                                <td colspan="2">Your Comment</td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <ext:TextArea ID="txtActivityComment" FieldLabel="" runat="server" LabelAlign="Top"
                                                LabelSeparator="" Width="400" Rows="4" >
                                            </ext:TextArea>
                                    <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator4" runat="server" ValidationGroup="SaveActivityComment"
                                                    ControlToValidate="txtActivityComment" ErrorMessage="Comment is required." />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <ext:Button runat="server" ID="btnSaveActivityComment" Cls="btn btn-primary" Text="<i></i>Send" MarginSpec="10 0 0 0" Hidden="true">
                                        <DirectEvents>
                                            <Click OnEvent="btnSaveComment_Click">
                                                <EventMask ShowMask="true" />
                                            </Click>
                                        </DirectEvents>
                                        <Listeners>
                                            <Click Handler="valGroup = 'SaveActivityComment'; if(CheckValidation()) return ''; else return false;">
                                            </Click>
                                        </Listeners>
                                    </ext:Button>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <ext:DisplayField ID="dfComments" runat="server" MarginSpec="20 0 0 0" />
                                </td>
                            </tr>


                           
                            </table>



                            <table>
                            
                            
                            
                            
                        </table>
                    </td>
                </tr>

            </table>
        </Content>
        <Buttons>
            <ext:Button runat="server" ID="btnSaveAndNew" Cls="btn btn-primary" Text="<i></i>Save and another" MarginSpec="0 0 0 20">
                    <DirectEvents>
                        <Click OnEvent="btnSaveAndNew_Click">
                            <EventMask ShowMask="true" />
                        </Click>
                    </DirectEvents>
                    <Listeners>
                        <Click Handler="valGroup = 'SaveEmpActivity'; if(CheckValidation()) return ''; else return false;">
                        </Click>
                    </Listeners>
                </ext:Button>
                
                <ext:Button runat="server" ID="btnSaveAndFinish" Cls="btn btn-primary" Text="<i></i>Save and finish" MarginSpec="0 0 0 20">
                    <DirectEvents>
                        <Click OnEvent="btnSaveAndNew_Click">
                            <EventMask ShowMask="true" />
                        </Click>
                    </DirectEvents>
                    <Listeners>
                        <Click Handler="valGroup = 'SaveEmpActivity'; if(CheckValidation()) return ''; else return false;">
                        </Click>
                    </Listeners>
                </ext:Button>
        </Buttons>
    </ext:Window>


    
<style type="text/css">
    
    .trHeaderCls td
        {
            background-color:#BDD7EE;
            color:#4077A7;
            padding:3px !important;
            margin-top:10px !important;
        }
        .trRowClass td
        {
            background-color:White;
            padding:3px !important;
        }
        .spacerRowCls
        {
            height:15px;
        }
    
</style>

<script type="text/javascript">

    function hideCommetSection() {
        document.getElementById("<%= tblComments.ClientID %>").style.display = "none";
    }
    function showCommetSection() {
        document.getElementById("<%= tblComments.ClientID %>").style.display = "block";
    }
</script>