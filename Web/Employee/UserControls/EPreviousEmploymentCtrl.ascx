﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EPreviousEmploymentCtrl.ascx.cs" Inherits="Web.Employee.UserControls.EPreviousEmploymentCtrl" %>


<script type="text/javascript">

     var prepareEmpHistory = function (grid, toolbar, rowIndex, record) {

         var btnEditDelete = toolbar.items.get(0);       

         if (record.data.IsEditable == 0) {
             btnEditDelete.setVisible(false);
         }

     }

     function reloadPrevEmpGrid() {
        <%= btnReloadPrevEmpGrid.ClientID %>.fireEvent('click');
     }

     function AddPrevEmp()
        {
            addPrevEmployment();
        }

    function editPrevEmploy()
    {
        editPrevEmp('PrevEmpId=' + <%=hdnPreviousEmploymentID.ClientID %>.getValue());
    }

</script>



<ext:Hidden runat="server" ID="hdnPreviousEmploymentID">
</ext:Hidden>

<ext:LinkButton ID="btnReloadPrevEmpGrid" runat="server" Hidden="true">
    <DirectEvents>
        <Click OnEvent="btnReloadPrevEmpGrid_Click">
            <EventMask ShowMask="true" />
        </Click>
    </DirectEvents>
</ext:LinkButton>

<table class="fieldTable firsttdskip">
    <tr>
        <td>
            <ext:GridPanel ID="GridPreviousEmployment" runat="server" Width="1000" Cls="itemgrid"
                Scroll="None">
                <Store>
                    <ext:Store ID="StorePreviousEmployment" runat="server">
                        <Model>
                            <ext:Model ID="Model1" runat="server" IDProperty="PrevEmploymentId">
                                <Fields>
                                    <ext:ModelField Name="Category" Type="String" />
                                    <ext:ModelField Name="Organization" Type="String" />
                                    <ext:ModelField Name="Place" Type="string" />
                                    <ext:ModelField Name="Position" Type="string" />
                                    <ext:ModelField Name="JobResponsibility" Type="string" />
                                    <ext:ModelField Name="IsEditable" Type="Int" />
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                <ColumnModel>
                    <Columns>
                        <ext:Column ID="Column5" Width="160" runat="server" Text="Experience Category" Wrap="true"
                            DataIndex="Category" />
                        <ext:Column ID="Column1" Width="180" runat="server" Text="Organization" DataIndex="Organization" Wrap="true" />
                        <ext:Column ID="Column2" runat="server" Text="Place" DataIndex="Place" Width="160" Wrap="true">
                        </ext:Column>
                        <ext:Column ID="Column3" runat="server" Text="Position" DataIndex="Position" Width="160" Wrap="true">
                        </ext:Column>
                        <ext:Column ID="Column4" runat="server" Text="Job Responsibility" DataIndex="JobResponsibility" Wrap="true"
                            Width="260">
                        </ext:Column>
                        <ext:CommandColumn ID="CommandColumn2" runat="server" Width="40">
                            <Commands>                            
                                <ext:GridCommand ToolTip-Text="Edit" Icon="ApplicationEdit" CommandName="Edit" />
                            </Commands>
                            <DirectEvents>
                                <Command OnEvent="GridPreviousEmployment_Command">
                                    <EventMask ShowMask="true">
                                    </EventMask>
                                    <ExtraParams>
                                        <ext:Parameter Name="ID" Value="record.data.PrevEmploymentId" Mode="Raw">
                                        </ext:Parameter>
                                        <ext:Parameter Name="command" Value="command" Mode="Raw">
                                        </ext:Parameter>
                                    </ExtraParams>
                                </Command>
                            </DirectEvents>
                        </ext:CommandColumn>
                        <ext:CommandColumn ID="CommandColumn1" runat="server" Width="40">
                            <Commands>
                                <ext:GridCommand ToolTip-Text="Delete" Icon="ApplicationDelete"
                                CommandName="Delete" />                             
                            </Commands>
                            <DirectEvents>
                                <Command OnEvent="GridPreviousEmployment_Command">
                                    <EventMask ShowMask="true">
                                    </EventMask>
                                    <Confirmation ConfirmRequest="true" Message="Are you sure, you want to delete the record?" />
                                    <ExtraParams>
                                        <ext:Parameter Name="ID" Value="record.data.PrevEmploymentId" Mode="Raw">
                                        </ext:Parameter>
                                        <ext:Parameter Name="command" Value="command" Mode="Raw">
                                        </ext:Parameter>
                                    </ExtraParams>
                                </Command>
                            </DirectEvents>
                            <PrepareToolbar Fn="prepareEmpHistory" />
                        </ext:CommandColumn>
                        
                    </Columns>
                </ColumnModel>
                <SelectionModel>
                    <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" Mode="Single" />
                </SelectionModel>
            </ext:GridPanel>
        </td>
    </tr>
</table>
<div class="buttonBlockSection">
    <ext:Button runat="server" ID="LinkButton1" Cls="btn btn-primary btn-sect" runat="server" Width="180" StyleSpec="margin-top:10px" Height="30"
         Text="<i></i>Add Previous Employment" OnClientClick="AddPrevEmp();return false;">      
    </ext:Button>
</div>