﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ETrainingCtrl.ascx.cs" Inherits="Web.Employee.UserControls.ETrainingCtrl" %>

<script type="text/javascript">
    var renderDuration = function (value, meta, record) {

        if (record.data.Duration > 0) {
            return record.data.Duration + " (" + record.data.DurationTypeName + ")";
        } else {

        }
    }

    var prepareDownloadTraining = function(grid, toolbar, rowIndex, record){
        var downloadBtn = toolbar.items.get(0);
        if(record.data.ServerFileName == null || record.data.ServerFileName == ''){
            downloadBtn.setVisible(false);
        }
    }

    var prepareDeleteTraininig = function (grid, toolbar, rowIndex, record) {
        
        var editDelBtn = toolbar.items.get(1);
        if (record.data.IsEditable == 0) {
            editDelBtn.setVisible(false);
        }

    }

    function reloadTrainingGrid() {
        <%= btnLoadTrGrid.ClientID %>.fireEvent('click');
    }

     function AddTraining()
        {
            addTrain();
        }

    function EditTraining()
    {
        editTrain('TrainingId=' + <%=hdnTrainingID.ClientID %>.getValue());
    }

</script>
<ext:Hidden runat="server" ID="hdnTrainingID">
</ext:Hidden>

<ext:LinkButton ID="btnLoadTrGrid" runat="server" Hidden="true">
    <DirectEvents>
        <Click OnEvent="btnLoadTrGrid_Click">
            <EventMask ShowMask="true" />
        </Click>
    </DirectEvents>
</ext:LinkButton>


<div class="widget-body">
    <table class="fieldTable firsttdskip">
        <tr>
            <td>
                <ext:GridPanel ID="GridTraining" runat="server" Width="1000" Cls="itemgrid" Scroll="None">
                    <Store>
                        <ext:Store ID="StoreTraining" runat="server">
                            <Model>
                                <ext:Model ID="Model1" runat="server" IDProperty="TrainingId">
                                    <Fields>
                                        <ext:ModelField Name="TrainingName" Type="string" />
                                        <ext:ModelField Name="TrainingTypeName" Type="String" />
                                        <ext:ModelField Name="InstitutionName" Type="string" />
                                        <ext:ModelField Name="Country" Type="string" />
                                        <ext:ModelField Name="Duration" Type="string" />
                                        <ext:ModelField Name="DurationTypeName" Type="string" />
                                        <ext:ModelField Name="TrainingFrom" Type="string" />
                                        <ext:ModelField Name="TrainingTo" Type="string" />
                                        <ext:ModelField Name="IsEditable" Type="Int" />
                                        <ext:ModelField Name="ServerFileName" Type="String" />
                                    </Fields>
                                </ext:Model>
                            </Model>
                        </ext:Store>
                    </Store>
                    <ColumnModel>
                        <Columns>
                            <ext:Column ID="Column1" runat="server" Text="Training Name" DataIndex="TrainingName" Width="200" Wrap="true"
                                Flex="1" />
                            <ext:Column ID="Column2" runat="server" Text="Training Type" DataIndex="TrainingTypeName" Width="160" Wrap="true">
                            </ext:Column>
                            <ext:Column ID="Column3" runat="server" Text="Institute" DataIndex="InstitutionName" Width="160" Wrap="true">
                            </ext:Column>
                            <ext:Column ID="Column4" runat="server" Text="Duration" DataIndex="Duration" Width="120" Wrap="true">
                                <Renderer Fn="renderDuration" />
                            </ext:Column>
                            <ext:Column ID="Column6" runat="server" Text="Start Date" DataIndex="TrainingFrom" Width="120">
                            </ext:Column>
                            <ext:Column ID="Column5" runat="server" Text="End Date" DataIndex="TrainingTo" Width="120">
                            </ext:Column>
                            <ext:CommandColumn ID="CommandColumn1" runat="server" Width="40">
                                <Commands>
                                    <ext:GridCommand Icon="PageWhitePut" CommandName="DownLoad" >
                                        <ToolTip Text="DownLoad" />
                                    </ext:GridCommand>
                                    
                                </Commands>
                                <DirectEvents>
                                    <Command OnEvent="GridTraining_Command">                                       
                                        <ExtraParams>
                                            <ext:Parameter Name="ID" Value="record.data.TrainingId" Mode="Raw">
                                            </ext:Parameter>
                                            <ext:Parameter Name="command" Value="command" Mode="Raw">
                                            </ext:Parameter>
                                        </ExtraParams>
                                    </Command>
                                </DirectEvents>
                                <PrepareToolbar Fn="prepareDownloadTraining" />
                            </ext:CommandColumn>
                            <ext:CommandColumn ID="CommandColumn2" runat="server" Width="40">
                                <Commands>
                                    <ext:CommandSeparator />
                                    <ext:GridCommand ToolTip-Text="Edit" Icon="ApplicationEdit" CommandName="Edit" />
                                </Commands>
                                <DirectEvents>
                                    <Command OnEvent="GridTraining_Command">                                        
                                        <EventMask ShowMask="true">
                                        </EventMask>
                                        <ExtraParams>
                                            <ext:Parameter Name="ID" Value="record.data.TrainingId" Mode="Raw">
                                            </ext:Parameter>
                                            <ext:Parameter Name="command" Value="command" Mode="Raw">
                                            </ext:Parameter>
                                        </ExtraParams>
                                    </Command>
                                </DirectEvents>
                            </ext:CommandColumn>
                            <ext:CommandColumn ID="CommandColumn3" runat="server" Width="40">
                                <Commands>
                                    <ext:CommandSeparator />
                                    <ext:GridCommand ToolTip-Text="Delete" Icon="ApplicationDelete"
                                        CommandName="Delete" />
                                </Commands>
                                <DirectEvents>
                                    <Command OnEvent="GridTraining_Command">                                        
                                        <EventMask ShowMask="true">
                                        </EventMask>
                                        <Confirmation ConfirmRequest="true" Message="Are you sure, you want to delete the record?" />
                                        <ExtraParams>
                                            <ext:Parameter Name="ID" Value="record.data.TrainingId" Mode="Raw">
                                            </ext:Parameter>
                                            <ext:Parameter Name="command" Value="command" Mode="Raw">
                                            </ext:Parameter>
                                        </ExtraParams>
                                    </Command>
                                </DirectEvents>
                                <PrepareToolbar Fn="prepareDeleteTraininig" />
                            </ext:CommandColumn>
                        </Columns>
                    </ColumnModel>
                    <SelectionModel>
                        <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" Mode="Single" />
                    </SelectionModel>
                </ext:GridPanel>
            </td>
        </tr>
    </table>
    <div class="buttonBlockSection">
        <ext:Button runat="server" Cls="btn btn-primary btn-sect" Width="120" StyleSpec="margin-top:0px" Height="30" ID="btnAddNewLine" Text="<i></i>Add Training" OnClientClick="AddTraining();return false;">
          
        </ext:Button>
    </div>
</div>