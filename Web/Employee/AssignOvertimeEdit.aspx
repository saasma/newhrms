﻿<%@ Page Title="Assign Overtime" Language="C#" MasterPageFile="~/Master/ForEmployeePopupPage.Master" AutoEventWireup="true" CodeBehind="AssignOvertimeEdit.aspx.cs" Inherits="Web.Employee.AssignOvertimeEdit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">

<script type="text/javascript">
    var skipLoadingCheck = true;


    var isSaved = false;

    function UpdateSavedStatus() {
        isSaved = true;
    }

    function closePopup() {
        window.close();
        window.opener.refreshWindow();
    }


</script>

<style type="text/css">
    .boldCls1
   {
       font-weight:bold;
       margin-left:20px;
       padding-left:8px;
       display:block;
   } 
   .boldCls
   {
       font-weight:bold;
   } 
</style>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="mainContent" runat="server">

<ext:ResourceManager ID="ResourceManager1"  DisableViewState="false" runat="server" />

<ext:Hidden runat="server" ID="Hidden_MinutesPrev" />
<ext:Hidden runat="server" ID="hdnRequestID" />


<div class="popupHeader" style="margin-top:-10px; height:50px; padding-top:10px; padding-left: 20px; color:White;">
        <h4>
            Assign Overtime</h4>
    </div>

<div class=" marginal" style='margin-top: 0px'>

     <br />

            <ext:Panel ID="pnlBorder" runat="server" StyleSpec="width:450px; height:40px; border:solid 1px red; margin-left:30px; display:none;" Hidden="true">
            <Items>
                <ext:Label ID="lblWarningMsg" StyleSpec="color:red; padding-top:10px; padding-left:10px;" runat="server" />
         </Items>
        </ext:Panel>
        <br />
                
            <ext:DisplayField ID="dfOvertime" FieldCls="boldCls1" runat="server" Text="Overtime approval for : " />           
     

        <table class="fieldTable" style="margin-left:20px;">
            <tr>
                <td>
                    <ext:DisplayField ID="dfDateTitle" runat="server" FieldCls="boldCls" Text="Date :"/>
                </td>
                <td>
                    <ext:DisplayField ID="dfDate" runat="server" StyleSpec="margin-bottom:10px;"/>
                </td>
            </tr>

            <tr>
                <td>
                    <ext:DisplayField ID="dfNameTitle" runat="server" FieldCls="boldCls" Text="Name :"/>
                </td>
                <td>
                    <ext:DisplayField ID="dfName" runat="server" StyleSpec="margin-bottom:10px;"/>
                </td>
            </tr>

            <tr>
                <td>
                    <ext:DisplayField ID="dfDescriptionTitle" runat="server" FieldCls="boldCls" Text="Description :"/>
                </td>
                <td>
                    <ext:DisplayField ID="dfDescription" runat="server" StyleSpec="margin-bottom:10px;"/>
                </td>
            </tr>

            <tr>
                    <td>
                        <ext:DisplayField ID="dfInTimeTitle" runat="server" FieldCls="boldCls" Text="In Time :" />
                    </td>
                    <td>
                        <ext:DisplayField ID="dfInTime" runat="server" StyleSpec="margin-bottom:10px;"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <ext:DisplayField ID="dfOutTimeTitle" runat="server" FieldCls="boldCls" Text="Out Time :" />
                    </td>
                    <td>
                        <ext:DisplayField ID="dfOutTime" runat="server" StyleSpec="margin-bottom:10px;"/>
                    </td>
                </tr>
        </table>

        <table class="fieldTable" style="margin-left:20px;">

            <tr>
                <td>
                    <ext:DisplayField ID="dfRequestedHourTitle" runat="server" FieldCls="boldCls" Text="Requested Hour :" />
                </td>
            </tr>
            <tr>
                <td>
                    <ext:TextField ID="txtRequestedHour" runat="server"  FieldLabel="" Width="100" /> 
                </td>
            </tr>
            <tr>
                <td>
                    <ext:DisplayField ID="dfApprovedForTitle" runat="server" FieldCls="boldCls" Text="Approve For :" />
                </td>
            </tr>
            <tr>
                <td>
                    Hour<br />
                    <ext:TextField ID="txtApprovedForHour" runat="server" FieldLabel="" Width="100" /> 
                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator6" ValidationGroup="ApproveOvertime"
                        ControlToValidate="txtApprovedForHour" Display="None" ErrorMessage="Approve hour is required."></asp:RequiredFieldValidator>
                </td>
                <td>
                    Minute<br />
                    <ext:TextField ID="txtApprovedForMin" runat="server" FieldLabel="" Width="100" /> 
                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator7" ValidationGroup="ApproveOvertime"
                        ControlToValidate="txtApprovedForMin" Display="None" ErrorMessage="Approve minute is required."></asp:RequiredFieldValidator>
                </td>
            </tr>
        </table>
        <br />

        <table class="fieldTable" style="margin-left:20px;">    
            <tr>
               <td style="width:400px;">
                        
                    <ext:Button runat="server" ID="btnApprove" Cls="btn btn-primary" StyleSpec="color:red;" Text="<i></i>Approve">
                                <DirectEvents>
                                    <Click OnEvent="btnApprove_Click">
                                        <EventMask ShowMask="true" />
                                        <Confirmation ConfirmRequest="true" Message="Are you sure you want to approve this request?" />
                                    </Click>
                                </DirectEvents>
                                <Listeners>
                                    <Click Handler="valGroup = 'ApproveOvertime'; if(CheckValidation()) return this.disable(); else return false;">
                                    </Click>
                                </Listeners>
                            </ext:Button>

                    <ext:Button runat="server" ID="btnRecommend" Cls="btn btn-primary" Text="<i></i>Recommend" Hidden="true">
                                <DirectEvents>
                                    <Click OnEvent="btnRecommend_Click">
                                        <EventMask ShowMask="true" />
                                        <Confirmation ConfirmRequest="true" Message="Are you sure you want to recommend this request?" />
                                    </Click>
                                </DirectEvents>
                                <Listeners>
                                    <Click Handler="valGroup = 'ApproveOvertime'; if(CheckValidation()) return this.disable(); else return false;">
                                    </Click>
                                </Listeners>
                            </ext:Button>
                        </td>
                        <td>

                    <ext:Button runat="server" ID="btnReject" Cls="btn btn-primary" Text="<i></i>Reject">
                                <DirectEvents>
                                    <Click OnEvent="btnReject_Click">
                                        <EventMask ShowMask="true" />
                                        <Confirmation ConfirmRequest="true" Message="Are you sure you want to Reject this request?" />
                                    </Click>
                                </DirectEvents>
                                <Listeners>                                    
                                </Listeners>
                            </ext:Button>
                            
                </td>
            </tr>            

        </table>

        <br />
      
            <ext:GridPanel ID="gridHistory" runat="server" Cls="itemgrid" StyleSpec="margin-left:25px;">
                <Store>
                    <ext:Store ID="Store3" runat="server">
                        <Model>
                            <ext:Model ID="Model3" runat="server" IDProperty="LineID">
                                <Fields>
                                    <ext:ModelField Name="LineID" Type="Int" />
                                    <ext:ModelField Name="ModifiedByName" Type="String" />
                                    <ext:ModelField Name="ModifiedOn" Type="string" />

                                    <ext:ModelField Name="Column1Before" Type="string" />
                                    <ext:ModelField Name="Column1After" Type="string" />
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                <ColumnModel>
                    <Columns>
                        <ext:Column ID="colModifiedByName" Sortable="false" MenuDisabled="true" runat="server" Text="Modified by"
                            Align="Left" Width="160" DataIndex="ModifiedByName" />
                        <ext:DateColumn ID="colModifiedOn" runat="server" Align="Left" Text="ModifiedOn" Width="100"
                            MenuDisabled="true" Sortable="false" Format="yyyy-MM-dd" DataIndex="ModifiedOn">
                        </ext:DateColumn>
                        <ext:Column ID="colColumn1Before" Sortable="false" MenuDisabled="true" runat="server" Text="Before"
                            Align="Left" Width="120" DataIndex="Column1Before" />
                        <ext:Column ID="colColumn1After" Sortable="false" MenuDisabled="true" runat="server" Text="After"
                            Align="Left" Width="120" DataIndex="Column1After" />
                    </Columns>
                </ColumnModel>
                <SelectionModel>
                    <ext:RowSelectionModel ID="RowSelectionModel2" runat="server" Mode="Single" />
                </SelectionModel>
            </ext:GridPanel>
</div>


</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
