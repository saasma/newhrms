﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL.Entity;
using BLL;
using System.IO;
using BLL.BO;
using Utils.Helper;
using Bll;


namespace Web.Employee
{
    public partial class OvertimeRequestAddPopup : BasePage
    {
        public bool HasImport
        {
            get
            {
                if (ViewState["import"] == null)
                    return false;
                return Convert.ToBoolean(ViewState["import"]);
            }
            set
            {
                ViewState["import"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest && !IsPostBack)
            {
                Initialize();
            }

            JavascriptHelper.AttachPopUpCode(Page, "overtimeImp", "EmployeeOTImport.aspx", 450, 500);
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "sdf", "var hasImport = " + (this.HasImport ? "true;" : "false;"), true);
        }

        protected void Initialize()
        {
            cmbOvertimeTypeAdd.GetStore().DataSource = OvertimeManager.GetOvertimeList();
            cmbOvertimeTypeAdd.GetStore().DataBind();

            Setting setting = OvertimeManager.GetSetting();

            if (setting.MultipleOvertimeRequestApproval != null && setting.MultipleOvertimeRequestApproval.Value)
            {             
                string review = "", approval = "";
                LeaveRequestManager.GetApplyToForEmployee(SessionManager.CurrentLoggedInEmployeeId, ref review, ref approval);
                lblApplyTo.Text = approval;
            }
        }

        protected void btnLoadGrid_Click(object sender, DirectEventArgs e)
        {
            if (Session["OTRequests"] != null)
            {
                List<OverTimeClsNew> list = (List<OverTimeClsNew>)Session["OTRequests"];

                if (list.Count > 0)
                {
                    DateTime dt = DateTime.Now;

                    gridOTList.GetStore().DataSource = list;
                    gridOTList.GetStore().DataBind();
                }

            }
        }


        protected void btnSaveOTAdd_Click(object sender, DirectEventArgs e)
        {
            if (string.IsNullOrEmpty(cmbOvertimeTypeAdd.Text))
            {
                NewMessage.ShowWarningMessage("Overtime type is required.");
                cmbOvertimeTypeAdd.Focus();
                return;
            }
            
            int approvalID1=0;
            int approvalID2 = 0;
            string approvalName1 = "";
            string approvalName2 = "";

            Setting setting = OvertimeManager.GetSetting();

            if (setting.MultipleOvertimeRequestApproval != null && setting.MultipleOvertimeRequestApproval.Value)
            {
                List<TextValue> listApproval = LeaveRequestManager.GetApplyToListForEmployee(SessionManager.CurrentLoggedInEmployeeId, true, PreDefindFlowType.Overtime);

                if (listApproval.Count > 0)
                {
                    if (listApproval.Count == 1)
                    {
                        approvalID1 = int.Parse(listApproval[0].Value);
                        approvalName1 = listApproval[0].Text;
                    }
                    else
                    {
                        approvalID1 = int.Parse(listApproval[0].Value);
                        approvalName1 = listApproval[0].Text;
                        approvalID2 = int.Parse(listApproval[1].Value);
                        approvalName2 = listApproval[1].Text;
                    }
                }
                else
                {
                    NewMessage.ShowWarningMessage("Approval is required.");
                    return;
                }
            }

            Page.Validate("SaveOTAdd");
            if (Page.IsValid)
            {
                string jsonItems = e.ExtraParams["gridItems"];
                List<OverTimeClsNew> timeRequestLines = JSON.Deserialize<List<OverTimeClsNew>>(jsonItems);

                if (timeRequestLines.Count == 0)
                {
                    NewMessage.ShowWarningMessage("Please add/import overtime requests.");
                    return;
                }

                List<OvertimeRequest> listOvertimeRequest = new List<OvertimeRequest>();
                int count = 1;

                foreach (var item in timeRequestLines)
                {
                    bool isEdit = false;
                    OvertimeRequest requestInstance = new OvertimeRequest();

                    requestInstance.OvertimeTypeId = int.Parse(cmbOvertimeTypeAdd.SelectedItem.Value);

                    TimeSpan tsStartTime, tsEndTime;
                    DateTime dt;

                    if (item.DateEng != null && item.DateEng != new DateTime())
                    {
                        try
                        {
                            dt = item.DateEng;
                        }
                        catch (Exception ex)
                        {
                            NewMessage.ShowWarningMessage("Valid Date is required for the row " + count.ToString());                            
                            return;
                        }
                    }
                    else
                    {
                        NewMessage.ShowWarningMessage("Date is required for the row " + count.ToString());
                        return;
                    }


                    requestInstance.Date = dt;
                    requestInstance.EmployeeID = SessionManager.CurrentLoggedInEmployeeId;

                    if (item.InTime != null && item.InTime != new DateTime())
                    {
                        tsStartTime = new TimeSpan(item.InTime.Hour, item.InTime.Minute, 0);
                        requestInstance.StartTime = new DateTime(dt.Year, dt.Month, dt.Day, tsStartTime.Hours, tsStartTime.Minutes, 0);
                    }
                    else
                    {
                        NewMessage.ShowWarningMessage("Start time is required for the row " + count.ToString());           
                        return;
                    }

                    if (item.OutTime != null && item.OutTime != new DateTime())
                    {
                        tsEndTime = new TimeSpan(item.OutTime.Hour, item.OutTime.Minute, 0);
                        requestInstance.EndTime = new DateTime(dt.Year, dt.Month, dt.Day, tsEndTime.Hours, tsEndTime.Minutes, 0);
                    }
                    else
                    {
                        NewMessage.ShowWarningMessage("End time is required for the row " + count.ToString());           
                        return;
                    }

                    if (string.IsNullOrEmpty(item.OutNote))
                    {
                        NewMessage.ShowWarningMessage("Reason is required for the row " + count.ToString());           
                        return;
                    }
                    requestInstance.Reason = item.OutNote;

                    requestInstance.Status = (int)OvertimeStatusEnum.Pending;

                    if (approvalID1 != 0)
                    {
                        requestInstance.ApprovalID = approvalID1;
                        requestInstance.ApprovalName = approvalName1;
                    }

                    if (approvalID2 != 0)
                    {
                        requestInstance.ApprovalID2 = approvalID2;
                        requestInstance.ApprovalName2 = approvalName2;
                    }
                    

                    ResponseStatus isSuccess = new ResponseStatus();

                    ResponseStatus isValidDate = OvertimeManager.isRequestedDateValid(requestInstance.EmployeeID, requestInstance.Date, requestInstance.StartTime, requestInstance.EndTime, isEdit);

                    if (!OvertimeManager.CanEmployeeRequestForOverTime(requestInstance.EmployeeID.Value, requestInstance.OvertimeTypeId.Value, requestInstance.Date.Value))
                    {
                        NewMessage.ShowWarningMessage("Employee is not eligible for " + cmbOvertimeTypeAdd.SelectedItem.Text + ".");
                        return;
                    }

                    int minimumOvertimeBuffer = OvertimeManager.GetOvertimeMinimumBufferTime(requestInstance.EmployeeID.Value, requestInstance.Date.Value);

                    if (minimumOvertimeBuffer != 0)
                    {
                        TimeSpan EndTimeSpan = OvertimeManager.getOfficeEndTime(SessionManager.CurrentLoggedInEmployeeId);

                        TimeSpan ts = tsStartTime - EndTimeSpan;

                        if (ts.TotalMinutes < minimumOvertimeBuffer)
                        {
                            NewMessage.ShowWarningMessage("You cannot apply overtime earlier than " + minimumOvertimeBuffer + " minutes from end of office time.");
                            return;
                        }
                    }
                    
                    requestInstance.NepaliDate = requestInstance.Date.Value.ToShortDateString().Replace('-', '/');

                    count++;

                    if (isValidDate.IsSuccessType)
                    {
                        listOvertimeRequest.Add(requestInstance);
                    }
                    else
                    {
                        NewMessage.ShowWarningMessage(isValidDate.ErrorMessage);
                        return;
                    }
                }

                Status status = OvertimeManager.SaveOvertimeRequestList(listOvertimeRequest);
                if (status.IsSuccess)
                {
                    NewMessage.ShowNormalMessage("Overtime requests saved successfully.","closePopup()");
                }
                else
                {
                    NewMessage.ShowWarningMessage(status.ErrorMessage);
                }
            }

        }

        protected void btnAddRow_Click(object sender, DirectEventArgs e)
        {
            string jsonItems = e.ExtraParams["gridItems"];
            List<OverTimeClsNew> overtimeClsNewList = JSON.Deserialize<List<OverTimeClsNew>>(jsonItems);
            int count = overtimeClsNewList.Count;

            if (overtimeClsNewList.Count == 0)
            {
                overtimeClsNewList.Add(new OverTimeClsNew() { });
            }
            else
            {
                OverTimeClsNew objNew = new OverTimeClsNew();
                OverTimeClsNew objLast = overtimeClsNewList[count - 1];
                objNew.SN = objLast.SN + 1;

                if(objLast.DateEng != null && objLast.DateEng != DateTime.MinValue)
                    objNew.DateEng = objLast.DateEng.AddDays(1);

                objNew.InTime = objLast.InTime;
                objNew.OutTime = objLast.OutTime;
                objNew.WorkHours = objLast.WorkHours;
                objNew.OutNote = objLast.OutNote;

                overtimeClsNewList.Add(objNew);
            }

            gridOTList.GetStore().DataSource = overtimeClsNewList;
            gridOTList.GetStore().DataBind();
        }
      


    }

    public class OverTimeClsNew
    {
        public int SN { get; set; }
        public DateTime DateEng { get; set; }
        public DateTime InTime { get; set; }
        public DateTime OutTime { get; set; }
        public string WorkHours { get; set; }
        public string OutNote { get; set; }
        public DateTime DATETIMEIN { get; set; }
        public DateTime DATETIMEOUT { get; set; }
    }

}