﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL;
using BLL.Manager;
using Web.ReportDataSetTableAdapters;
using Web.Master;
using BLL;
using Web.CP.Report.Templates.Pay.Detail;
using Utils;
using BLL.Base;
using System.IO;
using Ext.Net;
using System.Text;
using Utils.Helper;
using BLL.BO;
using Utils.Calendar;

namespace Web.Employee
{
    public class DateData { public string NepDate { get; set; } public string DaysCount { get; set; } }
    public partial class LeaveRequest : BasePage
    {
        LeaveAttendanceManager mgr = new LeaveAttendanceManager();
        protected void Page_Load(object sender, EventArgs e)
        {
            //if (CommonManager.CompanySetting.WhichCompany == WhichCompany.NIBL)
            //{
            //    Response.Redirect("LeaveRequestNew.aspx", true);
            //    return;
            //}
           
            if (!X.IsAjaxRequest)
            {
                // Change Days or Hours column name
                if (CommonManager.CalculationConstant.CompanyHasHourlyLeave.Value)
                {
                    ColumnDaysOrHours.Text = "Hour(s)";
                    ColumnDaysOrHours2.Text = "Hour(s)";
                }
                else
                {
                    ColumnDaysOrHours.Text = "Day(s)";
                    ColumnDaysOrHours2.Text = "Day(s)";
                }
                
                Initialise();
                LoadHourlyLeave();

                if (!string.IsNullOrEmpty(Request.QueryString["ReqLeaveTpyeId"]))
                {
                    LeaveCtrl.ShowLeaveRequestWindow(int.Parse(Request.QueryString["ReqLeaveTpyeId"]));
                }

                X.Js.AddScript("isHourlyLeaveCompany = " + CommonManager.CalculationConstant.CompanyHasHourlyLeave.ToString().ToLower() + ";");
            }

            
        }

        private void Initialise()
        {
            //set allowable dates in the Date Picker
            PayrollPeriod payrollPeriod = CommonManager.GetLastPayrollPeriod();
            if (payrollPeriod != null)
            {
               // hdStartDateEng.Text = DateTime.Now.ToShortDateString();
                //DatePicker1.MinDate =DateTime.Now;

                DateTime date = BLL.BaseBiz.GetCurrentDateAndTime();

                //CalendarPanel1.

                //Response.Write(date.ToShortDateString());

                hdStartDateEng.Text = date.AddDays(CommonManager.CompanySetting.LeaveRequestMinimumPastDays * -1).ToShortDateString();  //payrollPeriod.StartDateEng.Value.ToShortDateString();
                DatePicker1.MinDate = date.AddDays(CommonManager.CompanySetting.LeaveRequestMinimumPastDays * -1);

                // Next payroll is determied by "LeaveRequestValidMonths" setting value
                if (CommonManager.CompanySetting.LeaveRequestValidMonths > 0)
                {
                    DateTime maxDate = date.AddMonths(
                        CommonManager.CompanySetting.LeaveRequestValidMonths);
                    hdEndDateEng.Text = maxDate.ToShortDateString();
                    DatePicker1.MaxDate = maxDate;
                }
              //  lblCalendarTitle.Text = DatePicker1.MinDate.ToString();
            }
            else
            {
                DatePicker1.Disabled = true;
            }

            // Set  

            


            RegisterLegendColors();
            RegisterLeaveCalenderColorStyles();
            RegisterJS();
            List<GetLeaveListAsPerEmployeeResult> leaves = GetEmployeeLeaves();
            

            if (leaves != null)
            {
                divLeaveBalance.InnerHtml =
                    LeaveRequestManager.GetLeaveBalanceAsHTML(SessionManager.CurrentLoggedInEmployeeId, true, leaves);

                GetLeaveListAsPerEmployeeResult unpaidLeave = new GetLeaveListAsPerEmployeeResult();
                unpaidLeave.LeaveTypeId = -1;
                unpaidLeave.Title = Resources.Messages.UnpaidLeaveTitleForLeaveRequest;
                unpaidLeave.NewBalance = 0;
                unpaidLeave.IsHalfDayAllowed = true;
                leaves.Add(unpaidLeave);
                storeLeaves.DataSource = leaves;
                storeLeaves.DataBind();
          
                
            }

            LeaveCtrl.LoadDetails(leaves);
            


            X.Js.AddScript("isLeaveRequestPage = true;");
        }

        public List<GetLeaveListAsPerEmployeeResult> GetEmployeeLeaves()
        {
            PayrollPeriod payroll = CommonManager.GetLastPayrollPeriod();

            return
                LeaveAttendanceManager.GetAllLeavesForLeaveRequest(SessionManager.CurrentLoggedInEmployeeId, payroll.PayrollPeriodId, true);


        }

        void LoadHourlyLeave()
        {
            bool? HasCompanyHasHourlyLeave = CommonManager.CalculationConstant.CompanyHasHourlyLeave;            
            hdCompanyHasHourlyLeave.Text = HasCompanyHasHourlyLeave.ToString();        
            if (!(bool)HasCompanyHasHourlyLeave)
            {
                //chkIsHourlyLeave.Visible = false;
                //cboHourlyLeave.Visible = false;
                return;
            }
            else
            {               
                //chkIsHalfDay.Visible = false;                 
            }

            //int? CompanyLeaveHoursInADay = CommonManager.CalculationConstant.CompanyLeaveHoursInADay;
            //List<object> list = new List<object>();
            //if (CompanyLeaveHoursInADay != null)
            //{
            //    for (int i = 1; i <= CompanyLeaveHoursInADay; i++)
            //    {
            //        list.Add(new { Text = i.ToString(), Value = i.ToString() });
            //    }
            //}

            //this.storeHourlyLeave.DataSource = list;
            //this.storeHourlyLeave.DataBind();
            //this.cboHourlyLeave.SelectedIndex = 0;

            //startTime.Hidden = false;
            //endTime.Hidden = false;
        }

        [DirectMethod]
        

        void RegisterJS()
        {
            string code = string.Format("var LeaveRequestNegativeLeaveReqBalanceNotAllowed = {0};",
                (CommonManager.CompanySetting.LeaveRequestNegativeLeaveReqBalanceNotAllowed ? "true" : "false"));

            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "OtherInfo123", code, true);
        }

        void RegisterLegendColors()
        {
            StringBuilder str = new StringBuilder("\nlegends = new Array();");
           
            //add holiday legend color also
            str.AppendFormat("legends['{0}'] = '{1}';", HolidaysConstant.National_Holiday, HolidaysConstant.GetColor(HolidaysConstant.National_Holiday));
            str.AppendFormat("legends['{0}'] = '{1}';", HolidaysConstant.Caste_Holiday, HolidaysConstant.GetColor(HolidaysConstant.Caste_Holiday));
            str.AppendFormat("legends['{0}'] = '{1}';", HolidaysConstant.Weekly_Holiday, HolidaysConstant.GetColor(HolidaysConstant.Weekly_Holiday));
            str.AppendFormat("legends['{0}'] = '{1}';", HolidaysConstant.HalfWeekly_Holiday, HolidaysConstant.GetColor(HolidaysConstant.HalfWeekly_Holiday));
            str.AppendFormat("legends['{0}'] = '{1}';", HolidaysConstant.Female_Holiday, HolidaysConstant.GetColor(HolidaysConstant.Female_Holiday));

            X.ResourceManager.RegisterBeforeClientInitScript(str.ToString());

            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "LegendColorReg123", str.ToString(), true);
        }

        void RegisterLeaveCalenderColorStyles()
        {
            StringBuilder str = new StringBuilder();

            var store = (Ext.Net.CalendarStore)X.GetCtl("GroupStore1");


            List<DAL.LLeaveType> leaves = BLL.BaseBiz.PayrollDataContext.LLeaveTypes.ToList();


            //List<CalendarModel> data = new List<CalendarModel>();
            foreach (DAL.LLeaveType l in leaves)
            {
                //if (l.NewBalance != null)
                {
                    str.Append(GetLeaveCalendarStyle(l.LeaveTypeId, l.LegendColor));


                    CalendarModel g = new CalendarModel();
                    g.CalendarId = l.LeaveTypeId;
                    g.Title = l.Title;

                    store.Calendars.Add(g);

                    //CalendarPanel1.store

                    //CalendarPanel1.CalendarStore.Add(g);
                }

            }



            //add Unpaid leave
            CalendarModel unpaidLeaveGroup = new CalendarModel();
            unpaidLeaveGroup.CalendarId = -1;
            unpaidLeaveGroup.Title = Resources.Messages.UnpaidLeaveTitleForLeaveRequest;
            store.Calendars.Add(unpaidLeaveGroup);


            store.DataBind();

            str.Append(GetLeaveCalendarStyle(-1, Config.UPLColor));
            X.ResourceManager.RegisterClientStyleBlock("CalendarStyle", str.ToString());
        }


        /// <summary>
        /// Custom style for each group store item for the calendar
        /// if id is 0 which is unpaid leave does not accept color so for Unpaid Leave -1 is used
        /// </summary>
        /// <param name="leave"></param>
        /// <returns></returns>
        public string GetLeaveCalendarStyle(int id, string color)
        {
            //http://forums.ext.net/showthread.php?10566-CLOSED-CalendarPanel-GroupStore-and-CalendarId
            string style =
                @"
                    
            .ext-color-{0},
            .ext-ie .ext-color-{0}-ad,
            .ext-opera .ext-color-{0}-ad {{
                color: {1};
            }}
            .ext-cal-day-col .ext-color-{0},
            .ext-dd-drag-proxy .ext-color-{0},
            .ext-color-{0}-ad,
            .ext-color-{0}-ad .ext-cal-evm,
            .ext-color-{0} .ext-cal-picker-icon,
            .ext-color-{0}-x dl,
            .ext-color-{0}-x .ext-cal-evb {{
                background: {1};
            }}
            .ext-color-{0}-x .ext-cal-evb,
            .ext-color-{0}-x dl {{
                border-color: #7C3939;
            }}
                ";
            
            return string.Format(style, id, color);
        }

        protected void btnEdit_Click(object sender, DirectEventArgs e)
        {
            List<GetLeaveListAsPerEmployeeResult> leaves = GetEmployeeLeaves();
            if (leaves != null)
                LeaveCtrl.ShowDetails(leaves, int.Parse(hdnRequestID.Text));
        }

        [DirectMethod]
        public static DateData GetNepDate(string engDate)
        {
            DateTime date = Convert.ToDateTime(engDate);
            CustomDate nepDate = new CustomDate(date.Day, date.Month, date.Year, true);
            nepDate = CustomDate.ConvertEngToNep(nepDate);
            DateData data = new DateData();
            data.NepDate = nepDate.ToString();
            return data;
        }


        [DirectMethod]
        public static DateData GetDaysForLeave(bool isHour, bool isHalfDay, string fromdate, string todate,
            int leaveTypeId1, int? leaveTypeId2, int? childLeaveTypeId, int days1, bool? CompensatorIsAddType, string changedDate)
        {
            float daysCount = 0;
            DateData data = new DateData();

            DateTime from = Convert.ToDateTime(fromdate);
            DateTime? to = null;

            if (!string.IsNullOrEmpty(todate))
                to = Convert.ToDateTime(todate);
            else
                to = from;

            List<GetHolidaysForAttendenceResult> holiday = new HolidayManager()
                .GetMonthsHolidaysForAttendence(0, 0, SessionManager.CurrentCompanyId, 0, from, to,0).ToList();


            LeaveAttendanceManager.GetLeaveDetail(isHour, isHalfDay, from, to.Value, leaveTypeId1
                , leaveTypeId2,null, holiday, days1, CompensatorIsAddType, SessionManager.CurrentLoggedInEmployeeId, ref daysCount);

            if (!string.IsNullOrEmpty(changedDate))
            {
                DateTime date = Convert.ToDateTime(changedDate);
                CustomDate nepDate = new CustomDate(date.Day, date.Month, date.Year, true);
                nepDate = CustomDate.ConvertEngToNep(nepDate);
                
                data.NepDate = nepDate.ToString();
            }
            data.DaysCount = daysCount.ToString();
            return data;
        }

        [DirectMethod]
        public bool IsLeaveStatus_Request_Denied(int LeaveRequestId)
        {
            LeaveRequestStatusEnum request = LeaveAttendanceManager.GetLeaveRequestStatus(LeaveRequestId);
            if (request.Equals(LeaveRequestStatusEnum.ReDraft)  || request.Equals(LeaveRequestStatusEnum.Request))
                return true;
            return false;
        }
    }
}
