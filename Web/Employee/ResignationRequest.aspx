﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/EmployeeExt.Master" AutoEventWireup="true" 
    CodeBehind="ResignationRequest.aspx.cs" Inherits="Web.Employee.ResignationRequest" %>
<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">

     <style type="text/css">
        .panel1 {
            position: relative;
            left: 500px;
            top: 100px;
            width: 900px;
            position: relative;
            left: 500px;
            top: 100px;
            border: 1px solid black;
            box-shadow: 5px 5px 5px #888888;
        }

        .position {
            position: relative;
            margin-top: 5px;
            padding-left: 20px;
            padding-right: 20px;
        }

        .position1 {
            position: relative;
            margin-top: 10px;
        }

        #ctl04 {
            left: 35px !important;
        }

        #ctl06 {
            left: 160 px !important;
        }

        #ctl08 {
            left: 240px !important;
        }

        #ctl10 {
            left: 320px !important;
        }

        table, tr, td {
            padding: 5px;
        }

        #ctl14 {
            width: 120px !important;
        }

        #DateFieldDate {
            border-bottom-width: 2px;
        }
    </style>
    
     <script type="text/javascript">

        var getDaysCount = function () {
          
            <%= hdnDaysCount.ClientID %>.fireEvent('click');
        };
       
    </script>


</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="mainContent" runat="server">

 <div class="contentpanel">
    <ext:Button ID="hdnDaysCount" runat="server" Hidden="true">
        <DirectEvents>
            <Click OnEvent="btnDaysCount_Click">
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:Button>
   <ext:Panel
        ID="Panel"
        runat="server"
        Width="900"
        Height="500"
        Title="Resignation Request"
        BodyPadding="10">
        <Content>
            <div class="position">
                <table>
                    <tr>
                        <td>
                            <label><strong>Expected Date</strong></label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <ext:DateField
                                ID="ExpectedDate"
                                runat="server"
                                Vtype="daterange"
                               MinDate="<%# DateTime.Now %>"
                                AutoDataBind="true">
                                <Listeners>
                                    <Change Handler="getDaysCount();" />
                                </Listeners>
                            </ext:DateField>
                            <ext:Label runat="server" ID="lblDaysCount" Text="in 30 days" />
                        </td>
                        <br />
                        <br />
                    </tr>
                    <tr>
                        <td>
                            <label><strong>Reason for Resignation</strong></label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <ext:TextArea runat="server" ID="Reason" bottompadding="5px" Width="800" Height="200" />
                        </td>
                    </tr>
                </table>
            </div>

        </Content>
        <Buttons>
            <ext:Button ID="btnPost" runat="server" Text="Send" Height="50">
                <DirectEvents>
                    <Click OnEvent="Send_Click">
                        <EventMask ShowMask="true" />
                        <Confirmation ConfirmRequest="true" Message="Are you sure, you want to Forward the requests?" />
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
    </ext:Panel>




    <ext:Panel
        ID="Resigned"
        runat="server"
        Width="900"
        Height="500"
        Hidden="true"
        BodyPadding="10">
        <Content>
            <%-- <ext:Viewport runat="server">
                    <LayoutConfig>
                        <ext:VBoxLayoutConfig Align="Center" Pack="Center" />
                    </LayoutConfig>--%>
           
                        <ext:FormPanel
                            runat="server"
                            Width="500"
                            Height="290"
                            Frame="true"
                            Title="Resign Process"
                            BodyPadding="13">
                            <Items>
                                <ext:Label runat="server" Text="Your request has been sent." Icon="Accept" />
                            </Items>
                            <Items>
                                <ext:TextField
                                    ID="ExpectedDate1"
                                    ReadOnly="true"
                                    runat="server"
                                    FieldLabel="Request Date" />
                                <ext:TextField
                                    ID="Reason1"
                                    ReadOnly="true"
                                    runat="server"
                                    FieldLabel="Resignation reason"
                                    Height="70px"
                                    boarder="0px" />
                                <ext:TextField
                                    ID="Status"
                                    ReadOnly="true"
                                    runat="server"
                                    FieldLabel="Status" />
                            </Items>
                            <Items>
                                <ext:Button Cls="btn btn-primary" ID="CancelProcess" runat="server" Text="Cancel Process" >
                                    <DirectEvents>
                                        <Click OnEvent="Cancel_Click">
                                            <EventMask ShowMask="true" />
                                            <Confirmation ConfirmRequest="true" Message="Are you sure, you want to cancel your request?" />
                                        </Click>
                                    </DirectEvents>
                                </ext:Button>
                            </Items>
                        </ext:FormPanel>
                   
            <%--  </ext:Viewport>--%>
        </Content>
    </ext:Panel>
 </div>

</asp:Content>

