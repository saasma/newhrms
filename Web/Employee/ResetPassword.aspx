﻿<%@ Page Title="Reset Password" Language="C#" AutoEventWireup="true" CodeBehind="ResetPassword.aspx.cs"
    Inherits="Web.Employee.ResetPassword" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <link rel="stylesheet" href="../css/core.css?v=1" type="text/css" />
    <link href="../css/style.default.css" rel="stylesheet" />
    <style type="text/css">
        a
        {
            color: #FFFFFF;
        }
        a:hover
        {
            color: #FF5500;
        }
        
        .submit
        {
            text-align: center;
            font-weight: bold;
        }
        .invalid{border:0px;}
    </style>
    <script type="text/javascript">
        
    </script>
</head>
<body id="wrapper" style="background-color: white!important">
    <form id="form1" runat="server">
    <asp:HiddenField ID="hdnToken" runat="server" />
     <div id="login_header" style="background-color:#3B7DB5">
        <div class="header">
            <div class="login_head" style= 'height: 35px;padding-top: 10px; text-align: center;font-family: helvetica; font-size: 13px; text-transform: uppercase'>
             
            </div>
        </div>
    </div>
    <div id="sec_nav">
    </div>
    <div id="bodypart" style="min-height: 580px;background-color:white;'>
        <asp:Label ID="lblDemoMsg" runat="server" EnableViewState="true" CssClass="invalid"
            ForeColor="Red"></asp:Label>
        <section>
             <div class="logo text-center">
                        
                    </div>
           

            <div class="panel panel-signin" style='width:445px;border:1px solid #CCCCCC'>
                  
                <div class="panel-body">
                   
                
                   
                    <h4 class="text-center mb5">Please Change Your Password</h4>
                    <p class="text-center">Your password should have
            <br />
            - at least 6 characters and
            <br />
            - at least 1 uppercase letter and 1 number</p>
                  
                       <asp:Label ID="spanMessage" style='clear:inherit;' runat="server" EnableViewState="False" CssClass="invalid"
                ForeColor="Red"></asp:Label>
                    <div class="mb30"></div>
                    
                  
                        <div class="input-group mb15">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
                              <asp:TextBox TextMode="Password"  class="form-control" ID="txtNewPwd" runat="server" placeholder="New password" Width="300px"></asp:TextBox>
                                
                        </div><!-- input-group -->
                        <asp:RequiredFieldValidator ID="valReqdName" runat="server" ControlToValidate="txtNewPwd"
                                    Display="Dynamic" Text="Please type a new password." ValidationGroup="Change"></asp:RequiredFieldValidator>

                       <div class="input-group mb15" style='margin-top:10px;'>
                            <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
                              <asp:TextBox TextMode="Password" class="form-control" ID="txtConfirmPwd" runat="server" placeholder="Re-enter new password" Width="300px"></asp:TextBox>
                           
                        </div>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtConfirmPwd"
                                Display="Dynamic" ForeColor="Red" Text="Confirm password is required." ValidationGroup="Change"></asp:RequiredFieldValidator>
                            <asp:CompareValidator Display="Dynamic" ControlToValidate="txtConfirmPwd" ControlToCompare="txtNewPwd"
                                Type="String"  Operator="Equal" ValidationGroup="Change" ID="CompareValidator1"
                                runat="server" Text="Passwords do not match."></asp:CompareValidator>
                        <div class="clearfix">
                          

                            <div class="pull-right" style='padding-top:10px;    padding-right: 24px;'>
                               


                                <asp:Button ID="btnChange" CssClass="btn btn-success" ValidationGroup="Change" 
                        runat="server" Text="Change Password" OnClick="btnChange_Click" />

                            </div>
                        </div>                      
                    
                    
                </div><!-- panel-body -->
              <div class="panel-footer">
                 
                  <a id="linkForgotPwd" style='color:#428bca;text-align:center;' class="btn-block" runat="server" href="~/Default.aspx" >
                                Login</a>
                </div><!-- panel-footer -->

            </div><!-- panel -->
            
        </section>
    </div>
    </form>
    <div style="position: fixed; color: #FFFFFF; left: 0px; bottom: 0px; height: 45px;
        width: 100%; text-align: center; padding-bottom: 10px; padding-top: 0px; background-color: #355D81;
        border-top: thin #82B3FF outset;">
        &nbsp;<br />
        <a href="http://rigonepal.com">Rigo Technologies </a></strong> &nbsp;<br />
        &nbsp;<br />
    </div>
    <%--    <script>
        var $buoop = { vs: { i: 9, f: 25, o: 17, s: 6, c: 30 } }; 
        function $buo_f() {
            var e = document.createElement("script");
            e.src = "scripts/update.js";
            document.body.appendChild(e);
        };
        try { document.addEventListener("DOMContentLoaded", $buo_f, false) }
        catch (e) { window.attachEvent("onload", $buo_f) }
</script> --%>
</body>
<style type="text/css">
    #bodypart
    {
        background-color: white !important;
    }
    #wrapper
    {
        background-color: #428BCA !important;
    }
</style>
</html>
