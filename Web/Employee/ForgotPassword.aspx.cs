﻿using System;
using System.Collections.Generic;
using System.Linq;
using BLL.Manager;
using System.Web;
using Utils.Security;
using Utils.Helper;
namespace Web.Employee
{
    public partial class ForgotPassword : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }




        //protected void btnLogin_Click(object sender, EventArgs e)
        //{
        //    if (Page.IsValid)
        //    {
        //        var email = txtEmail.Text.Trim();
        //        var isUserExists = true;
        //        var user = UserManager.GetEmployeeUserByEmail(email, ref isUserExists);

        //        if (isUserExists == false)
        //        {
        //            lblMsg.Text = "Email doesn't exists.";
        //        }
        //        else
        //        {
        //            if (user == null)
        //            {
        //                lblMsg.Text = "User doesn't exists.";
        //            }
        //            else
        //            {
        //                var errorMsg =  UserManager.SendNewPasswordInMail(user.UserName);
        //                if (errorMsg == string.Empty)
        //                {
        //                    txtEmail.Text = string.Empty;
        //                    lblDemoMsg.Text = "New password sent in the email.";
        //                }
        //                else
        //                {
        //                    lblMsg.Text = "Error : " + errorMsg;
        //                }
        //            }
        //        }
        //    }
        //}

        public string GetClientIP()
        {
            HttpRequest currentRequest = HttpContext.Current.Request;
            string ipAddress = currentRequest.ServerVariables["HTTP_X_FORWARDED_FOR"];

            if (ipAddress == null || ipAddress.ToLower() == "unknown")
                ipAddress = currentRequest.ServerVariables["REMOTE_ADDR"];

            return ipAddress;
        }
        protected void btnLogin_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                var email = txtEmail.Text.Trim();
                var isUserExists = true;
                var user = UserManager.GetEmployeeUserByEmail(email, ref isUserExists);


                lblMsg.Style.Add("display", "block");

                if (isUserExists == false)
                {
                    lblMsg.Text = "Email does not exists.";
                }
                else
                {
                    if (user == null)
                    {
                        lblMsg.Text = "User does not exists.";
                    }
                    else
                    {
                        string body = "";

                        Guid obj = Guid.NewGuid();
                        string objEncrypt = new EncryptorDecryptor().Encrypt(obj.ToString());

                        string name = user.UserName;

                        if (user.EmployeeId != null && user.EmployeeId != 0 && user.EmployeeId != -1)
                        {
                            string empName = EmployeeManager.GetEmployeeName(user.EmployeeId.Value);
                            if (!string.IsNullOrEmpty(empName))
                                name = empName;
                        }

                        body = "Dear " + name + ", " + "<br/><br/>" +
                            "You (or someone pretending to be you) requested that your password be reset." + "<br/>" +
                            "If you didn't make this request, then ignore this email; no changes have been made." + "<br/>" +

                            "If you did make the request, then click " + String.Format("<a href=\"{0}\">here</a>", HttpContext.Current.Request.Url.ToString().ToLower().Replace("forgotpassword", "ResetPassword") + "?Token=" + Server.UrlEncode(objEncrypt)) +
                            " to reset your password." +
                            "<br/>" +
                            "<br/>" +
                            "Regards," + "<br/>" + "HR";

                        string msg = UserManager.SendForgotPasswordLinkInMail(email, body, user.UserName, obj,GetClientIP());

                        if (msg == string.Empty)
                        {
                            txtEmail.Text = string.Empty;
                            lblMsg.Text = "Please check your email to reset your password.";
                        }
                        else
                        {
                            lblMsg.Text = "Error : " + msg;
                        }
                    }
                }
            }
        }
    }
}
