﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EmpAttTimeCommentRequestList.aspx.cs"
    Inherits="Web.Employee.EmpAttTimeCommentRequestList" %>

<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/FooterCtl.ascx" TagName="FooterCtl" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/HeaderCtl.ascx" TagName="HeaderCtl" TagPrefix="uc3" %>
<%@ Register Src="~/UserControls/LogoutCtl.ascx" TagName="LogoutCtl" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/TopMenu.ascx" TagName="TopMenu" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head id="Head1" runat="server">
    <meta http-equiv="x-ua-compatible" content="IE=8">
    <title>Late Attendance Request</title>
    <link id="Link5" runat="server" rel="stylesheet" href="~/payrollmenu_files/mbcsmbmcp.css"
        type="text/css" />
    <link id="Link1" rel="stylesheet" type="text/css" href="<%= ResolveUrl("~/css/core.css?v=") + Web.Helper.WebHelper.Version %>" />
    <link id="Link2" rel="Stylesheet" type="text/css" href="<%= ResolveUrl("~/Styles/calendar/calendar.css?v=") + Web.Helper.WebHelper.Version %>" />
    <link id="Link4" runat="server" rel="stylesheet" href="~/Styles/design.css" />
    <ext:ResourcePlaceHolder ID="ResourcePlaceHolder1" runat="server" Mode="Script" />
    <style type="text/css">
        .details
        {
            padding-top: 10px;
        }
        .details tr td
        {
            padding-top: 4px;
        }
        .leavetype .leaveTitle
        {
            width: 128px;
        }
        
        .btn
        {
            -moz-border-radius: 3px;
            -webkit-border-radius: 3px;
            border-radius: 3px;
            line-height: 21px;
            -moz-transition: all 0.2s ease-out 0s;
            -webkit-transition: all 0.2s ease-out 0s;
            transition: all 0.2s ease-out 0s;
            padding: 8px 15px;
            border-width: 0;
        }
        
        .icon-delete
        {
            background-image: url("../images/delet.png") !important;
        }
        
        .x-form
        {
            padding: 10px 10px 2px !important;
        }
        .x-form-item
        {
            margin-bottom: 6px;
        }
        .x-form-item label.x-form-item-label
        {
            padding-top: 1px;
        }
        .topgap
        {
            padding-top: 5px;
        }
        thead td, th
        {
            border: 0px solid;
        }
        
        
        
        
        #CompanyX.chkHalfDayType_Container .x-form-item-label
        {
            padding-top: 3px !important;
        }
        
        .x-form-spinner-splitter
        {
            visibility: hidden !important;
        }
        #mbmcpebul_table
        {
            margin-top: 0px !important;
        }
        .bodypart
        {
            margin: 0 auto !important;
        }
        /*hide calendar icon*/
        .ext-cal-ic-rem
        {
            display: none;
        }
        
        
        .itemList
        {
            margin: 0px;
            padding: 0px;
        }
        .x-window-body-default
        {
            background: #E8F1FF;
        }
    </style>
    <script type="text/javascript" src="<%= ResolveUrl("~/Employee/override.js?v=") + Web.Helper.WebHelper.Version %>"></script>
    <script type="text/javascript">

     var currentMonth = null;
     var eventWindow = null;
     var txtStartDate = null;
     var txtEndDate = null;
     var btnShow = null;
     var holidayList = null; 
     var hdnRequestID = null;

     Ext.onReady(
        function () {

            eventWindow = <%=eventWindow.ClientID %>;
            hdnRequestID = <%=hdnRequestID.ClientID %>;

        });

        var CommandHandler = function(value, command,record,p2) {
            <%= hdnRequestID.ClientID %>.setValue(record.data.LineID);
            if(command == 'Edit')
            { 
                 <%= btnShow.ClientID %>.fireEvent('click');
            }
            else if(command == 'Delete')
            {
                <%= btnDelete.ClientID %>.fireEvent('click');
            }
       };

       var RemoveItemLine = function (column, command, record, recordIndex, cellIndex) {
            var store = this.grid.store;
            store.remove(record);
        };

      
    </script>
</head>
<body style="margin: 0px">
    <script type="text/javascript" src="<%= ResolveUrl("~/Employee/override.js?v=") + Web.Helper.WebHelper.Version %>"></script>
    <form id="Form1" runat="server">
    <ext:ResourceManager ID="ResourceManager1" runat="server" DisableViewState="true"
        ShowWarningOnAjaxFailure="false" ScriptMode="Release" Namespace="CompanyX" />
    <uc3:HeaderCtl ID="HeaderCtl1" runat="server" />
    <ext:Hidden ID="hdnRequestID" runat="server" Cls="hidden-inputs" />
    <ext:LinkButton ID="btnShow" runat="server" Hidden="true" Cls="hidden-inputs">
        <DirectEvents>
            <Click OnEvent="btnShow_Click">
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:LinkButton>
    <ext:LinkButton ID="btnDelete" runat="server" Hidden="true" Cls="hidden-inputs">
        <DirectEvents>
            <Click OnEvent="btnDelete_Click">
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:LinkButton>
    <div id="bodypart">
        <div class="bodypart">
            <h3 style='margin-top: 20px; margin-bottom: 0px; font-size: 16px;'>
                Late Attendance Request :</h3>
            <h4 style='margin-top: 5px; margin-bottom: 5px;'>
                Note : In case of Late Check In, or Late Check Out only
            </h4>
            <div style="clear: both">
            </div>
            <table>
                <tr>
                    <td valign="top">
                        <ext:Button ID="btnAssignTimeAasdftten" runat="server" Cls="btn btn-save" Text="New Request">
                            <DirectEvents>
                                <Click OnEvent="ShowWindow">
                                    <EventMask ShowMask="true" />
                                </Click>
                            </DirectEvents>
                        </ext:Button>
                    </td>
                    <td valign="top">
                    </td>
                </tr>
            </table>
            <ext:GridPanel StyleSpec="margin-top:15px;" ID="gridPending" runat="server" Cls="itemgrid"
                Scroll="None" Width="1100">
                <Store>
                    <ext:Store ID="Store2" runat="server">
                        <Model>
                            <ext:Model ID="Model1" runat="server" IDProperty="LineID">
                                <Fields>
                                    <ext:ModelField Name="LineID" Type="String" />
                                    <ext:ModelField Name="EmpID" />
                                    <ext:ModelField Name="Name" Type="String" />
                                    <ext:ModelField Name="AttendanceDate" Type="Date" />
                                    <ext:ModelField Name="InTime" Type="Date" />
                                    <ext:ModelField Name="InComment" Type="string" />
                                    <ext:ModelField Name="OutTime" Type="Date" />
                                    <ext:ModelField Name="OutComment" Type="string" />
                                    <ext:ModelField Name="StatusName" Type="string" />
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                <ColumnModel>
                    <Columns>
                        <%--<ext:Column ID="adsfa" Sortable="false" MenuDisabled="true" runat="server" Text="EIN"
                            Width="100" Align="Center" DataIndex="EmpID">
                        </ext:Column>
                        <ext:Column ID="Column1" Sortable="false" MenuDisabled="true" runat="server" Text="Name"
                            Width="100" Align="Center" DataIndex="Name">
                        </ext:Column>--%>
                        <ext:DateColumn ID="colSubmittedOn" runat="server" Align="Right" Text="Date" Width="100"
                            MenuDisabled="true" Sortable="false" Format="dd MMM yyyy" DataIndex="AttendanceDate">
                        </ext:DateColumn>
                        <ext:DateColumn ID="dColumn2" Sortable="false" MenuDisabled="true" runat="server"
                            Text="In Time" Width="100" Align="Center" DataIndex="InTime" Format="hh:mm tt">
                        </ext:DateColumn>
                        <ext:Column ID="Column3" Sortable="false" MenuDisabled="true" runat="server" Text="In Comment"
                            Width="250" Align="Left" DataIndex="InComment">
                        </ext:Column>
                        <ext:DateColumn ID="Column4" Sortable="false" MenuDisabled="true" runat="server"
                            Text="Out Time" Width="100" Align="Center" DataIndex="OutTime" Format="hh:mm tt">
                        </ext:DateColumn>
                        <ext:Column ID="Column5" Sortable="false" MenuDisabled="true" runat="server" Text="Out Comment"
                            Width="250" Align="Left" DataIndex="OutComment">
                        </ext:Column>
                        <ext:Column ID="colStatusName" Sortable="false" MenuDisabled="true" runat="server"
                            Text="Status" Width="100" Align="Left" DataIndex="StatusName">
                        </ext:Column>
                        <ext:ImageCommandColumn ID="ImageCommandColumn2" runat="server" Width="70" Text=""
                            MenuDisabled="true" Sortable="false" Align="Center">
                            <Commands>
                                <ext:ImageCommand CommandName="Edit" Text="Edit" Cls="GreenCls">
                                    <ToolTip Text="Edit Attendance Request Lines" />
                                </ext:ImageCommand>
                            </Commands>
                            <Listeners>
                                <Command Fn="CommandHandler">
                                </Command>
                            </Listeners>
                        </ext:ImageCommandColumn>
                        <ext:ImageCommandColumn ID="ImageCommandColumn3" runat="server" Width="70" Text=""
                            MenuDisabled="true" Sortable="false" Align="Center">
                            <Commands>
                                <ext:ImageCommand CommandName="Delete" Text="Delete" Cls="RedCls">
                                    <ToolTip Text="Delete" />
                                </ext:ImageCommand>
                            </Commands>
                            <Listeners>
                                <Command Fn="CommandHandler">
                                </Command>
                            </Listeners>
                        </ext:ImageCommandColumn>
                    </Columns>
                </ColumnModel>
                <SelectionModel>
                    <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" Mode="Single" />
                </SelectionModel>
            </ext:GridPanel>
        </div>
        <div style="margin-top: 15px; font-size: 15px; margin-left: 50px;">
        </div>
    </div>
    <ext:Window runat="server" Hidden="false" X="-1000" Y="-1000" Title="Comment Request"
        ID="eventWindow" Modal="false" Width="700" Height="350">
        <Content>
            <div style="padding: 15px; padding-top: 5px;">
                <table>
                    <tr>
                        <td style="width: 200">
                            <ext:DateField LabelSeparator="" FieldLabel="Date" runat="server" LabelAlign="Top"
                                ID="txtStartDateAdd" Width="150">
                                <Plugins>
                                    <ext:GenericPlugin ID="HighlightDates1" runat="server" InstanceName="Ext.ux.DatePicker.HighlightWithNepDates" />
                                </Plugins>
                            </ext:DateField>
                            <asp:RequiredFieldValidator runat="server" ID="rfvStartDateAdd" ValidationGroup="TimeAtt"
                                ControlToValidate="txtStartDateAdd" Display="None" ErrorMessage="Start date is required."></asp:RequiredFieldValidator>
                        </td>
                        <td>
                            <ext:Button ID="btnLoad" runat="server" StyleSpec="margin-top:15px;" Cls="btn btn-save"
                                Text="Load Time" Width="100" Height="30">
                                <DirectEvents>
                                    <Click OnEvent="btnLoad_Click">
                                        <EventMask ShowMask="true" />
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                        </td>
                    </tr>
                </table>
                <table>
                    <tr>
                        <td>
                            <ext:TextField ID="txtInTime" runat="server" LabelAlign="Top" FieldLabel="InTime"
                                Disabled="true" LabelSeparator="">
                            </ext:TextField>
                        </td>
                        <td>
                            <ext:TextField ID="txtOutTime" runat="server" LabelAlign="Top" FieldLabel="OutTime"
                                Disabled="true" LabelSeparator="">
                            </ext:TextField>
                        </td>
                    </tr>
                </table>
                <table>
                    <tr>
                        <td>
                            <ext:TextArea ID="txtInComment" runat="server" LabelAlign="Top" Width="300" FieldLabel="In Comment"
                                Rows="2" LabelSeparator="">
                            </ext:TextArea>
                        </td>
                        <td>
                            <ext:TextArea ID="txtOutComment" runat="server" LabelAlign="Top" Width="300" FieldLabel="Out Comment"
                                Rows="2" LabelSeparator="">
                            </ext:TextArea>
                        </td>
                    </tr>
                </table>
                <table class="tblDetails">
                    <tr>
                        <td>
                            <ext:DisplayField ID="lblApplyTo" LabelSeparator="" LabelStyle="font-weight:bold"
                                LabelWidth="75" runat="server" FieldLabel="Approval">
                            </ext:DisplayField>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <ext:Button runat="server" Cls="btn btn-primary btn-sect" Width="150" StyleSpec="margin-top:10px"
                                Height="30" ID="btnSend" Text="<i></i>Send For Approval">
                                <DirectEvents>
                                    <Click OnEvent="btnSend_Click">
                                        <Confirmation ConfirmRequest="true" Message="Confirm send the request?" />
                                        <EventMask ShowMask="true" />
                                    </Click>
                                </DirectEvents>
                                <Listeners>
                                    <Click Handler="valGroup = 'TimeAtt';">
                                    </Click>
                                </Listeners>
                            </ext:Button>
                        </td>
                    </tr>
                </table>
            </div>
        </Content>
    </ext:Window>
    <uc1:FooterCtl ID="FooterCtl1" runat="server" />
    </form>
</body>
</html>
