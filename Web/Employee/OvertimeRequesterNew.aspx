﻿<%@ Page Title="Overtime Request" Language="C#" MasterPageFile="~/Master/EmployeeExt.Master" AutoEventWireup="true" CodeBehind="OvertimeRequesterNew.aspx.cs" Inherits="Web.Employee.OvertimeRequesterNew" %>

<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">

<style type="text/css">

.x-progress-text
   {
       background-color:White;
   }
   


</style>

<script type="text/javascript">

    var CommandHandler = function(command, record){
            <%= hdnRequestID.ClientID %>.setValue(record.data.RequestID);
            <%= hdnEmployeeId.ClientID %>.setValue(record.data.EmployeeId);
                if(command=="Edit")
                {
                    <%= btnEdit.ClientID %>.fireEvent('click');
                }

           }


      var addNewRow = function (grid) {
            var newRow = new SettingModel();

            newRow.data.ControlID = "";

            var rowIndex = grid.getStore().data.items.length;

            //gridProjectSplitter.stopEditing();
            grid.getStore().insert(rowIndex, newRow);
        }

        var RemoveItemLine = function (column, command, record, recordIndex, cellIndex) {
            var store = this.grid.store;
            store.remove(record);
        };

        var CalculateWorkHours = function(e1,e2,record)
        {
            if(record.data.InTimeString != '' && record.data.OutTimeString != '')
            {
                var strTime = Ext.util.Format.date(record.data.InTimeString, 'g:i a')
                var eTime = Ext.util.Format.date(record.data.OutTimeString, 'g:i a')
                var diffTime = calculateTotalMinutes(eTime) - calculateTotalMinutes(strTime)
                if(diffTime <= 0)
                {
                    alert('Out time must be greater than In time.');
                    record.data.OutTime = "";
                    return "";
                }

                return (Math.floor(diffTime/60) + ":" + (diffTime%60));
              
            }
        };


        function calculateTotalMinutes(time)
        {
            var parts = time.split(/ |:/);
            var totalHours = 0;

            if(parts[2] == 'pm')
            {
               if(parseInt(parts[0], 10) == 12)
               {
                    return (12 * 60 + parseInt(parts[1], 10));
               }
               else
               {
                    return (12 * 60 + parseInt(parts[0], 10) * 60 + parseInt(parts[1], 10));
               }
            }
            else
            {
                return (parseInt(parts[0], 10) * 60 + parseInt(parts[1], 10));
            }
    
            //return (parts[2] == 'pm' * 12 * 60) + parseInt(parts[0], 10) * 60 + parseInt(parts[1], 10);
        }


        function ImportPopup()
        {
            var RecommenderId = <%=cmbRecommenderAdd.ClientID %>.getValue();
            var ApprovalId = <%=cmbApprovalAdd.ClientID %>.getValue();
            var OvertimeTypeId = <%=cmbOvertimeTypeAdd.ClientID %>.getValue();

            overtimeImp('RId=' + RecommenderId + '&AId=' + ApprovalId + '&OId=' + OvertimeTypeId);
        }

        function refreshWindow() {
            <%=btnLoadRef.ClientID %>.fireEvent('click');
        }

</script>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="mainContent" runat="server">


<div class="contentArea" style="margin-top:10px">



<ext:Hidden runat="server" ID="hdnRequestID" />
<ext:Hidden runat="server" ID="hdnEmployeeId" />

<ext:LinkButton runat="server" Hidden="true" ID="btnLoadRef">
    <DirectEvents>
        <Click OnEvent="btnLoadRef_Click">
            <EventMask ShowMask="true" />
        </Click>
    </DirectEvents>
</ext:LinkButton>

<ext:LinkButton runat="server" Hidden="true" ID="btnEdit">
    <DirectEvents>
        <Click OnEvent="btnEdit_Click">
            <EventMask ShowMask="true" />
        </Click>
    </DirectEvents>
</ext:LinkButton>

        <div class="attribute" style="padding:10px">
            
            <table>
                <tr>
                    <td rowspan="2">                        
                        <ext:Button ID="btnNewRequest" runat="server" Cls="btn btn-save" Text="New Request" Width="130">
                            <DirectEvents>
                                <Click OnEvent="btnNewRequest_Click">                               
                                    <EventMask ShowMask="true" />                              
                                </Click>
                            </DirectEvents>
                        </ext:Button>     
                    </td>
                    
                    <td style="padding-left:10px;">

                        <ext:ComboBox ID="cmbType" runat="server" Width="200" LabelWidth="40"
                            FieldLabel="Show" LabelAlign="Left" LabelSeparator="" ForceSelection="true"
                            QueryMode="Local">
                            <Items>
                                <ext:ListItem Text="All" Value="-1" />
                                <ext:ListItem Text="This Week" Value="1" />
                                <ext:ListItem Text="This Month" Value="2" />
                                <ext:ListItem Text="Last Month" Value="3" />
                                <ext:ListItem Text="This Year" Value="4" />
                            </Items>      
                            <SelectedItems>
                                <ext:ListItem Text="This Week" Value="1" />
                            </SelectedItems>                   
                        </ext:ComboBox>

                    </td>
                    <td valign="top" style="padding-bottom: 8px;padding-left:15px;">                      
                        <ext:Button ID="btnLoad" runat="server" Cls="btn btn-save" Text="Load" Width="130">
                            <DirectEvents>
                                <Click OnEvent="btnLoad_Click">                               
                                    <EventMask ShowMask="true" />                              
                                </Click>
                            </DirectEvents>
                        </ext:Button>     
                    </td>
                </tr>
            </table>
           
        </div>
        <div class="clear">

            <ext:GridPanel StyleSpec="margin-top:15px;" ID="gridOverTimeReq" runat="server" Cls="itemgrid" Scroll="None">
            <Store>
                <ext:Store ID="Store1" runat="server" OnReadData="MyData_Refresh" PageSize="10">
                    <Model>
                        <ext:Model ID="Model6" runat="server" IDProperty="RequestID">
                            <Fields>
                                <ext:ModelField Name="RequestID" Type="String" />
                                <ext:ModelField Name="EmployeeID" Type="String" />
                                <ext:ModelField Name="Date" Type="Date" />
                                <ext:ModelField Name="StartTime" Type="string" />                                
                                <ext:ModelField Name="EndTime" Type="string" />
                                <ext:ModelField Name="CheckInTime" Type="string" />
                                <ext:ModelField Name="CheckOutTime" Type="string" />                                                               
                                <ext:ModelField Name="DurationModified" Type="string" />

                                <ext:ModelField Name="ApprovedTime" Type="string" />
                                <ext:ModelField Name="Reason" Type="string" />
                                <ext:ModelField Name="StatusModified" Type="string" />
                            </Fields>
                        </ext:Model>
                    </Model>
                </ext:Store>
            </Store>
            <ColumnModel>
                <Columns>
                    <ext:Column ID="Column25" Sortable="false" MenuDisabled="true" runat="server" Text="RequestID" Visible="false"
                        Align="Left" Width="150" DataIndex="RequestID" />
                    <ext:Column ID="Column1" Sortable="false" MenuDisabled="true" runat="server" Text="EmployeeID" Visible="false"
                        Align="Left" Width="150" DataIndex="EmployeeID" />

                    <ext:DateColumn ID="colDate" runat="server" Align="Left" Text="Date" Width="100"
                        MenuDisabled="true" Sortable="false" Format="yyyy-MMM-dd" DataIndex="Date">
                    </ext:DateColumn>
                    <ext:Column ID="colStartTime" Sortable="false" MenuDisabled="true" runat="server" Text="Start Time" Width="100"
                        Align="Left" DataIndex="StartTime">
                    </ext:Column>
                    <ext:Column ID="colEndTime" Sortable="false" MenuDisabled="true" runat="server" Text="End Time" Width="80"
                        Align="Left" DataIndex="EndTime">
                    </ext:Column>
                    <ext:Column ID="colCheckInTime" Sortable="false" MenuDisabled="true" runat="server" Text="In Time"
                        Align="Left" Width="100" DataIndex="CheckInTime">
                    </ext:Column>
                    <ext:Column ID="colCheckOutTime" Sortable="false" MenuDisabled="true" runat="server" Text="Out Time"
                        Align="Left" Width="100" DataIndex="CheckOutTime">
                    </ext:Column>

                    <ext:Column ID="colDurationModified" Sortable="false" MenuDisabled="true" runat="server" Text="Duration"
                        Align="Left" Width="100" DataIndex="DurationModified">
                    </ext:Column>
                    <ext:Column ID="colApprovedTime" Sortable="false" MenuDisabled="true" runat="server" Text="Approved Time"
                        Align="Left" Width="100" DataIndex="ApprovedTime">
                    </ext:Column>
                    <ext:Column ID="colReason" Sortable="false" MenuDisabled="true" runat="server" Text="Reason"
                        Align="Left" Width="250" DataIndex="Reason">
                    </ext:Column>

                     <ext:Column ID="colStatusModified" Sortable="false" MenuDisabled="true" runat="server" Text="Status"
                        Align="Left" Width="100" DataIndex="StatusModified">
                    </ext:Column>
                   
                    <ext:CommandColumn ID="CommandColumn3" runat="server" Width="75" Text="Actions" Align="Center">
                        <Commands>
                            <ext:CommandSeparator />
                            <ext:GridCommand Cls="editGridButton" ToolTip-Text="Edit"  Text="<i class='fa fa-pencil'></i>" CommandName="Edit" />
                        </Commands>
                        <Listeners>
                            <Command Handler="CommandHandler(command,record);" />
                        </Listeners>
                       <%-- <PrepareToolbar Fn="prepareActivity" />--%>
                    </ext:CommandColumn>
                </Columns>
            </ColumnModel>
            <SelectionModel>
                <ext:RowSelectionModel ID="RowSelectionModel5" runat="server" Mode="Single" />
            </SelectionModel>
            <View>
                <ext:GridView ID="GridView1" runat="server" StripeRows="true" />                   
            </View>            
            <BottomBar>
                 <ext:PagingToolbar ID="PagingToolbar1" runat="server">
                    <Items>
                        <ext:Label ID="Label1" runat="server" Text="Page size:" />
                        <ext:ToolbarSpacer ID="ToolbarSpacer1" runat="server" Width="10" />
                        <ext:ComboBox ID="ComboBox1" runat="server" Width="80">
                            <Items>
                                <ext:ListItem Text="20" />
                                <ext:ListItem Text="30" />
                                <ext:ListItem Text="50" />
                                <ext:ListItem Text="100" />
                            </Items>
                            <SelectedItems>
                                <ext:ListItem Value="20" />
                            </SelectedItems>
                            <Listeners>
                                <Select Handler="#{gridOverTimeReq}.store.pageSize = parseInt(this.getValue(), 20); #{gridOverTimeReq}.store.reload();" />
                            </Listeners>
                        </ext:ComboBox>
                    </Items>
                    <Plugins>
                        <ext:ProgressBarPager ID="ProgressBarPager1" runat="server" />
                    </Plugins>
                </ext:PagingToolbar>
            </BottomBar>
        </ext:GridPanel>

         

        </div>
        <div class="buttonsDiv">
           <%-- <asp:LinkButton ID="LinkButton1" runat="server" Text="Export to Excel" OnClick="btnExport_Click"
                CssClass=" excel marginRight" Style="float: right;" />--%>

            <ext:Button runat="server" AutoPostBack="true" OnClick="btnExport_Click" 
                            ID="btnExport"  Text="<i></i>Export to Excel">
                        </ext:Button>
        </div>
    </div>


     <ext:Window ID="WOvertimeReq" runat="server" Title="Add/Edit Overtime Request" Icon="Application"
        Width="550" Height="510" BodyPadding="5" Hidden="true" Modal="true">
        <Content>
        <ext:Panel ID="pnlBorder" runat="server" StyleSpec="width:450px; height:40px; border:solid 1px red; margin-left:30px; display:none;" Hidden="true">
            <Items>
                <ext:Label ID="lblWarningMsg" StyleSpec="color:red; padding-top:10px; padding-left:10px;" runat="server" />
         </Items>
        </ext:Panel>
        
            <table class="fieldTable" style="margin-left:20px;">

            <tr>
                <td>
                    <ext:ComboBox ID="cmbOvertimeType" runat="server" ValueField="OvertimeTypeId" DisplayField="Name" FieldLabel="Overtime Type *" Width="200"
                            LabelAlign="top" LabelSeparator="" ForceSelection="true" QueryMode="Local">
                            <Store>
                                <ext:Store ID="Store2" runat="server">
                                    <Model>
                                        <ext:Model ID="Model1" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="OvertimeTypeId" Type="String" />
                                                <ext:ModelField Name="Name" Type="String" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                        </ext:ComboBox>
                </td>
            </tr>
            <tr>
                <td>
                    <ext:DateField ID="txtDate" runat="server" FieldLabel="Date *" LabelSeparator="" Width="200" LabelAlign="Top" />
                </td>
            </tr>
            <tr>
                <td>
                     <ext:TimeField  ID="tfExtraWorkStartAt"  runat="server"  MinTime="6:00" MaxTime="22:00" Increment="1" Width="200" SelectedTime="09:00"
                            Format="hh:mm tt" FieldLabel="Extra Work Start at *" LabelAlign="Top" LabelSeparator="">
                        </ext:TimeField> 
                </td>
                <td>
                     <ext:TimeField  ID="tfExtraWorkEndAt"  runat="server"  MinTime="6:00" MaxTime="22:00" Increment="1" Width="200" SelectedTime="09:00"
                            Format="hh:mm tt" FieldLabel="Extra Work Ended at *" LabelAlign="Top" LabelSeparator="">
                        </ext:TimeField>
                </td>
                
            </tr>
            <tr>
                <td colspan="2">
                    <ext:TextArea ID="txtReason" runat="server" FieldLabel="Reason for Extra Work *" LabelSeparator=""
                            LabelAlign="Top" Rows="3" Width="410" Height="80" />
                </td>
            </tr>
            <tr>
                <td>
                    <ext:ComboBox ID="cmbRecommender" runat="server" ValueField="Value" DisplayField="Text" FieldLabel="Sent for Recommend to *" Width="200"
                            LabelAlign="top" LabelSeparator="" ForceSelection="true" QueryMode="Local">
                            <Store>
                                <ext:Store ID="Store4" runat="server">
                                    <Model>
                                        <ext:Model ID="Model4" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="Value" Type="String" />
                                                <ext:ModelField Name="Text" Type="String" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                            <Items>
                                <ext:ListItem Text="--Select Recommender--" Value="-1" />
                            </Items>
                            <SelectedItems>
                                <ext:ListItem Text="--Select Recommender--" Value="-1" />
                            </SelectedItems>
                        </ext:ComboBox>
                </td>
                <td>
                    <ext:ComboBox ID="cmbApproval" runat="server" ValueField="Value" DisplayField="Text" FieldLabel="Sent for Approval to *" Width="200"
                            LabelAlign="top" LabelSeparator="" ForceSelection="true" QueryMode="Local">
                            <Store>
                                <ext:Store ID="Store5" runat="server">
                                    <Model>
                                        <ext:Model ID="Model5" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="Value" Type="String" />
                                                <ext:ModelField Name="Text" Type="String" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                            <Items>
                                <ext:ListItem Text="--Select Recommender--" Value="-1" />
                            </Items>
                            <SelectedItems>
                                <ext:ListItem Text="--Select Recommender--" Value="-1" />
                            </SelectedItems>
                        </ext:ComboBox>
                </td>
            </tr>

            <tr>
               <td valign="bottom" colspan="2">
                        <div class="popupButtonDiv">
                    <ext:Button runat="server" ID="btnSave" Cls="btn btn-primary" Text="<i></i>Save">
                                <DirectEvents>
                                    <Click OnEvent="btnSave_Click">
                                        <EventMask ShowMask="true" />
                                    </Click>
                                </DirectEvents>
                                <Listeners>
                                    <Click Handler="valGroup = 'SaveOvertimeReq'; if(CheckValidation()) return this.disable(); else return false;">
                                    </Click>
                                </Listeners>
                            </ext:Button>
                    <ext:Button runat="server" ID="btnAssignOvertime" Cls="btn btn-primary" Text="<i></i>Assign" Hidden="true">
                                <DirectEvents>
                                    <%--<Click OnEvent="btnAssignOvertime_Click">
                                        <EventMask ShowMask="true" />
                                    </Click>--%>
                                </DirectEvents>
                                <Listeners>
                                    <Click Handler="valGroup = 'SaveOvertimeReq'; if(CheckValidation()) return this.disable(); else return false;">
                                    </Click>
                                </Listeners>
                            </ext:Button>
                            </div>
                </td>
            </tr>
        

            </table>

        <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator5" ValidationGroup="SaveUpdate"
            Display="None" ErrorMessage="Overtime type is required." ControlToValidate="cmbOvertimeType"></asp:RequiredFieldValidator>
          
        <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator4" ValidationGroup="SaveOvertimeReq"
            Display="None" ErrorMessage="Date is required." ControlToValidate="txtDate"></asp:RequiredFieldValidator>
            
        <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator2" ValidationGroup="SaveOvertimeReq"
            Display="None" ErrorMessage="Reason is required" ControlToValidate="txtReason"></asp:RequiredFieldValidator>

        <asp:CompareValidator ID="CompareValidator2" runat="server" ControlToValidate="cmbRecommender" ValueToCompare="0" ValidationGroup="SaveOvertimeReq"
            Operator="GreaterThan" Display="None" ErrorMessage="Recommender is required." ></asp:CompareValidator>

        <asp:CompareValidator ID="CompareValidator3" runat="server" ControlToValidate="cmbApproval" ValueToCompare="0" ValidationGroup="SaveOvertimeReq"
            Operator="GreaterThan" Display="None" ErrorMessage="Approval is required." ></asp:CompareValidator>

        </Content>
    </ext:Window>


    <ext:Window ID="WOvertimeAdd" runat="server" Title="Add Overtime Request" Icon="Application"
        Width="740" Height="580" BodyPadding="5" Hidden="true" Modal="true">
        <Content>

             <table class="fieldTable" style="margin-left:20px;">

            <tr>
                <td style="width:350px;">
                    <ext:ComboBox ID="cmbOvertimeTypeAdd" runat="server" ValueField="OvertimeTypeId" DisplayField="Name" FieldLabel="Overtime Type *" Width="200"
                            LabelAlign="top" LabelSeparator="" ForceSelection="true" QueryMode="Local">
                            <Store>
                                <ext:Store ID="Store3" runat="server">
                                    <Model>
                                        <ext:Model ID="Model2" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="OvertimeTypeId" Type="String" />
                                                <ext:ModelField Name="Name" Type="String" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                        </ext:ComboBox>
                        <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ValidationGroup="SaveOTAdd"
                            Display="None" ErrorMessage="Overtime type is required." ControlToValidate="cmbOvertimeTypeAdd"></asp:RequiredFieldValidator>
                        <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator3" ValidationGroup="OvertimeImport"
                            Display="None" ErrorMessage="Overtime type is required." ControlToValidate="cmbOvertimeTypeAdd"></asp:RequiredFieldValidator>
                </td>

                <td>                     

                    <ext:LinkButton runat="server" StyleSpec="padding:0px;" ID="btnImport" Cls="btn btn-primary"
                                        OnClientClick="ImportPopup();return false;" Text="<i></i>Excel import">
                                        <Listeners>
                                            <Click Handler="valGroup = 'OvertimeImport'; if(CheckValidation()) return this.disable(); else return false;">
                                            </Click>
                                        </Listeners>

                                    </ext:LinkButton>
                </td>
            </tr>
            </table>

            <table class="fieldTable" style="margin-left:20px;">
            <tr>
                <td>
                    <ext:GridPanel StyleSpec="margin-top:15px;" ID="gridOTList" runat="server" Cls="itemgrid"
                                Scroll="None" Width="650">
                                <Store>
                                    <ext:Store ID="Store6" runat="server">
                                        <Model>
                                            <ext:Model ID="OTModel" Name="SettingModel" runat="server" IDProperty="SN">
                                                <Fields>
                                                    <ext:ModelField Name="SN" Type="Int" />
                                                    <ext:ModelField Name="DateEng" Type="Date"/>
                                                    <ext:ModelField Name="InTimeString" Type="string" />
                                                    <ext:ModelField Name="OutTimeString" Type="string" />                                                    
                                                    <ext:ModelField Name="WorkHours" Type="string" />
                                                    <ext:ModelField Name="OutNote" Type="string" />
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <ColumnModel>
                                    <Columns>
                                        <ext:DateColumn ID="DateColumn1" runat="server" Align="Right" Text="Date" Width="100"
                                            MenuDisabled="true" Sortable="false" Format="yyyy-MM-dd" DataIndex="DateEng">
                                              <Editor>
                                                <ext:DateField ID="dfDateAdd" runat="server" />
                                              </Editor>
                                        </ext:DateColumn>
                                        <ext:Column ID="colInTime" Sortable="false" MenuDisabled="true" runat="server" Text="Start Time"
                                            Align="Right" Width="100" DataIndex="InTimeString">
                                            <Renderer Handler="return Ext.util.Format.date(value, 'g:i a')" />
                                            <Editor>
                                                <ext:TimeField ID="tfInTime" runat="server" MinTime="06:00" MaxTime="22:00" Increment="1"
                                                    SelectedTime="08:00" Format="hh:mm tt">
                                                </ext:TimeField>
                                            </Editor>
                                        </ext:Column>
                                        <ext:Column ID="colOutTime" Sortable="false" MenuDisabled="true" runat="server" Text="End Time"
                                            Align="Right" Width="100" DataIndex="OutTimeString">
                                            <Renderer Handler="return Ext.util.Format.date(value, 'g:i a'); " />
                                            <Editor>
                                                <ext:TimeField ID="tfOutTime" runat="server" MinTime="06:00" MaxTime="22:00" Increment="1"
                                                    SelectedTime="08:00" Format="hh:mm tt">
                                                </ext:TimeField>
                                            </Editor>
                                        </ext:Column>
                                        <ext:Column ID="colWorkHours" Sortable="false" MenuDisabled="true" runat="server"
                                            Text="Hours:Minutes" Align="Center" Width="120" DataIndex="WorkHours">
                                            <Renderer Fn="CalculateWorkHours" />
                                        </ext:Column>

                                        
                                        <ext:Column ID="colReasonAdd" Sortable="false" MenuDisabled="true" runat="server" Text="Out Note"
                                            Align="Left" Width="200" DataIndex="OutNote">
                                            <Editor>
                                                <ext:TextField ID="txtOutNote" runat="server">
                                                </ext:TextField>
                                            </Editor>
                                        </ext:Column>

                                        <ext:CommandColumn ID="CommandColumn1" runat="server" Sortable="false" MenuDisabled="true"
                                            Width="30" Align="Center">
                                            <Commands>
                                                <ext:GridCommand Icon="Delete" CommandName="Delete" Cls="deleteicon" />
                                            </Commands>
                                            <Listeners>
                                                <Command Fn="RemoveItemLine">
                                                </Command>

                                              <%--  <Command Handler="PositionCommandHandler(command,record);" />--%>

                                            </Listeners>

                                        </ext:CommandColumn>

                                    </Columns>
                                </ColumnModel>
                                <SelectionModel>
                                    <ext:RowSelectionModel ID="RowSelectionModel2" runat="server" Mode="Single" />
                                </SelectionModel>
                                <Plugins>
                                    <ext:CellEditing ID="CellEditing1" runat="server" ClicksToEdit="1">
                                    </ext:CellEditing>
                                </Plugins>
                                <View>
                                    <ext:GridView ID="GridView2" runat="server">
                                        <Plugins>
                                            <ext:GridDragDrop ID="GridDragDrop1" runat="server" DragGroup="ddGroup" DropGroup="ddGroup" />
                                        </Plugins>
                                      
                                    </ext:GridView>
                                </View>
                            </ext:GridPanel>
                </td>
            </tr>

            <tr>
                <td>

                    <ext:Button ID="btnAddRow" runat="server" Cls="btn btn-save" Text="Add New Row" Width="130">
                        <Listeners>
                            <Click Handler="addNewRow(#{gridOTList});" />
                        </Listeners>
                    </ext:Button>
                </td>
                <td></td>
            </tr>
            </table>
            
            <table class="fieldTable" style="margin-left:20px;">
            <tr>
                <td style="width:300px;">
                    <ext:ComboBox ID="cmbRecommenderAdd" runat="server" ValueField="Value" DisplayField="Text" FieldLabel="Sent for Recommend to *" Width="200"
                            LabelAlign="top" LabelSeparator="" ForceSelection="true" QueryMode="Local">
                            <Store>
                                <ext:Store ID="Store7" runat="server">
                                    <Model>
                                        <ext:Model ID="Model7" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="Value" Type="String" />
                                                <ext:ModelField Name="Text" Type="String" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                            <Items>
                                <ext:ListItem Text="--Select Recommender--" Value="-1" />
                            </Items>
                            <SelectedItems>
                                <ext:ListItem Text="--Select Recommender--" Value="-1" />
                            </SelectedItems>
                        </ext:ComboBox>

                        <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="cmbRecommenderAdd" ValueToCompare="0" ValidationGroup="SaveOTAdd"
                            Operator="GreaterThan" Display="None" ErrorMessage="Recommender is required." ></asp:CompareValidator>
                        <asp:CompareValidator ID="CompareValidator5" runat="server" ControlToValidate="cmbRecommenderAdd" ValueToCompare="0" ValidationGroup="OvertimeImport"
                            Operator="GreaterThan" Display="None" ErrorMessage="Recommender is required." ></asp:CompareValidator>

                </td>
                <td>
                    <ext:ComboBox ID="cmbApprovalAdd" runat="server" ValueField="Value" DisplayField="Text" FieldLabel="Sent for Approval to *" Width="200"
                            LabelAlign="top" LabelSeparator="" ForceSelection="true" QueryMode="Local">
                            <Store>
                                <ext:Store ID="Store8" runat="server">
                                    <Model>
                                        <ext:Model ID="Model8" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="Value" Type="String" />
                                                <ext:ModelField Name="Text" Type="String" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                            <Items>
                                <ext:ListItem Text="--Select Recommender--" Value="-1" />
                            </Items>
                            <SelectedItems>
                                <ext:ListItem Text="--Select Recommender--" Value="-1" />
                            </SelectedItems>
                        </ext:ComboBox>
                        
                        <asp:CompareValidator ID="CompareValidator4" runat="server" ControlToValidate="cmbApprovalAdd" ValueToCompare="0" ValidationGroup="SaveOTAdd"
                            Operator="GreaterThan" Display="None" ErrorMessage="Approval is required." ></asp:CompareValidator>
                        <asp:CompareValidator ID="CompareValidator6" runat="server" ControlToValidate="cmbApprovalAdd" ValueToCompare="0" ValidationGroup="OvertimeImport"
                            Operator="GreaterThan" Display="None" ErrorMessage="Approval is required." ></asp:CompareValidator>
                </td>
            </tr>

            <tr>
               <td valign="bottom" colspan="2">
                        <div class="popupButtonDiv">
                             <ext:Button runat="server" ID="btnSaveOTAdd" Cls="btn btn-primary" Text="<i></i>Save">
                                <DirectEvents>
                                    <Click OnEvent="btnSaveOTAdd_Click">
                                        <EventMask ShowMask="true" />
                                        <ExtraParams>
                                            <ext:Parameter Name="gridItems" Value="Ext.encode(#{gridOTList}.getRowsValues({ selectedOnly: false }))"
                                                Mode="Raw" />
                                        </ExtraParams>
                                    </Click>
                                </DirectEvents>
                                <Listeners>
                                    <Click Handler="valGroup = 'SaveOTAdd'; if(CheckValidation()) return this.disable(); else return false;">
                                    </Click>
                                </Listeners>
                            </ext:Button>


                            </div>
                </td>
            </tr>

            </table>

        </Content>
    </ext:Window>
</asp:Content>
