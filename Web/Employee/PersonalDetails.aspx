﻿<%@ Page Title="Personal Details" Language="C#" MasterPageFile="~/Master/EmployeeMS.Master" AutoEventWireup="true"
    CodeBehind="PersonalDetails.aspx.cs" Inherits="Web.Employee.PersonalDetails" %>

<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .details
        {
            padding-top: 10px;
        }
        .details tr td
        {
            padding-top: 4px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
    <div class="contentArea">
        <uc2:WarningCtl ID="divWarningMsg" EnableViewState="false" Hide="true" runat="server" />
        <table>
            <tr>
                <td valign="top" style='padding-top: 5px'>
                    <asp:Image ID="image" runat="server" Width="300px" Height="300px" />
                </td>
                <td style="width: 20px">
                </td>
                <td valign="top" style="padding-top: 20px; padding-left: 13px">
                    <asp:Label ID="lblName" Font-Size="Large" Style="padding-left: 2px;" runat="server"
                        Text=""></asp:Label>
                    <br />
                    <asp:Label ID="lblDesignation" Font-Size="Medium" Style="padding-left: 2px;" runat="server"
                        Text=""></asp:Label>
                    <table class="details">
                        <tr runat="server" id="rowGrade">
                            <td class="fieldHeader">
                                Grade
                            </td>
                            <td>
                                <asp:Label ID="lblGrade" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="fieldHeader">
                                Date of Joining
                            </td>
                            <td>
                                <asp:Label ID="lblDOJ" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td class="fieldHeader">
                                Marital Status
                            </td>
                            <td>
                                <asp:Label ID="lblMaritalStatus" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="fieldHeader">
                                Tax Status
                            </td>
                            <td>
                                <asp:Label ID="lblTaxStatus" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td class="fieldHeader">
                                Address
                            </td>
                            <td>
                                <asp:Label ID="lblAddress" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td class="fieldHeader">
                                Phone / Mobile
                            </td>
                            <td>
                                <asp:Label ID="lblPhoneNo" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <%--<tr>
                            <td class="fieldHeader">
                                Mobile
                            </td>
                            <td>
                                <asp:Label ID="lblMobile" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>--%>
                        <tr>
                            <td class="fieldHeader">
                                Email
                            </td>
                            <td>
                                <asp:Label ID="lblEmail" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <table runat="server" id="payTable">
            <tr>
                <td valign="top">
                   
                    <div style="border: 1px solid #10B0EA; margin-top: 10px;">
                        <asp:GridView ID="gvwPayInformation" Width="300px" CssClass="tableLightColor" GridLines="None"
                            runat="server" AutoGenerateColumns="false">
                            <Columns>
                                <asp:TemplateField ItemStyle-Width="146px" HeaderStyle-HorizontalAlign="Left" HeaderText="Pay Information">
                                    <ItemTemplate>
                                        <%# Eval("Title") %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Amount" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right">
                                    <ItemTemplate>
                                        <%# ( Eval("Value"))%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <RowStyle CssClass="odd" />
                            <AlternatingRowStyle CssClass="even" />
                        </asp:GridView>
                    </div>
                    <div style="border: 1px solid #10B0EA; margin-top: 10px;">
                        <asp:GridView ID="gvwDeductions" Width="300px" CssClass="tableLightColor" GridLines="None"
                            runat="server" AutoGenerateColumns="false">
                            <Columns>
                                <asp:TemplateField  ItemStyle-Width="146px" HeaderStyle-HorizontalAlign="Left" HeaderText="Deductions">
                                    <ItemTemplate>
                                        <%# Eval("Title") %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Amount" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right">
                                    <ItemTemplate>
                                        <%# GetCurrency( Eval("Amount"))%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <RowStyle CssClass="odd" />
                            <AlternatingRowStyle CssClass="even" />
                        </asp:GridView>
                    </div>
                    <div style="border: 1px solid #10B0EA; margin-top: 10px;">
                        <table style='width: 300px; clear: both' class="tableLightColor">
                            <tr class="even">
                                <th style="width:146px" >
                                    &nbsp;
                                </th>
                                <th style='text-align: right'>
                                </th>
                            </tr>
                            <tr class="odd">
                                <td style="width:146px" >
                                    CIT
                                </td>
                                <td style='text-align: right'>
                                    <asp:Label ID="lblCIT" runat="server" />
                                </td>
                            </tr>
                            <tr class="even">
                                <td>
                                    Insurance
                                </td>
                                <td style='text-align: right'>
                                    <asp:Label ID="lblInsurance" runat="server" />
                                </td>
                            </tr>
                            <tr class="odd" runat="server" id="rowMedicalTax">
                                <td>
                                    Medical Tax Credit
                                </td>
                                <td style='text-align: right'>
                                    <asp:Label ID="lblMedicalTaxCredit" runat="server" />
                                </td>
                            </tr>
                            <tr class="even">
                                <td>
                                    Tax Paid this year
                                </td>
                                <td style='text-align: right'>
                                    <asp:Label ID="lblTaxPaidThisYear" runat="server" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
                <td valign="top" style='padding-left: 40px'>
                     <div style="border: 1px solid #10B0EA; margin-top: 10px;">
                       
                        <asp:GridView Style='clear: both;' ID="gvwStatusSummary" Width="300px" CssClass="tableLightColor"
                            GridLines="None" runat="server" AutoGenerateColumns="false">
                            <Columns>
                                <asp:TemplateField ItemStyle-Width="160" HeaderText="Status Summary" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <%# GetStatus(Eval("CurrentStatus")) %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField HeaderText="" DataField="FromDate" />
                                <asp:BoundField HeaderText="" DataField="ToDateValue" />
                            </Columns>
                            <RowStyle CssClass="odd" />
                            <AlternatingRowStyle CssClass="even" />
                        </asp:GridView>
                    </div>
                    <%--  <h3>
                        Loans</h3>--%>
                    <asp:Repeater ID="rptLoan" runat="server">
                        <ItemTemplate>
                            <div style="border: 1px solid #10B0EA; margin-top: 10px;">
                                <table style='border: 0px' id="gvwLoan" width="300px" class="tableLightColor">
                                    <tr>
                                        <th colspan="2" style="text-align: left">
                                            <%# Eval("PDeduction.Title") %>
                                        </th>
                                    </tr>
                                    <tr class="even">
                                        <td>
                                            Amount
                                        </td>
                                        <td style='text-align: right'>
                                            <%#GetCurrency( Eval("AdvanceLoanAmount"))%>
                                        </td>
                                    </tr>
                                    <tr class="odd">
                                        <td>
                                            Taken on
                                        </td>
                                        <td style='text-align: right'>
                                            <%# Eval("TakenOn")%>
                                        </td>
                                    </tr>
                                    <%--<tr class="even">
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>--%>
                                    <tr class="even">
                                        <td>
                                            Interest Rate
                                        </td>
                                        <td style='text-align: right'>
                                            <%# Eval("InterestRate")%>
                                        </td>
                                    </tr>
                                    <tr class="odd">
                                        <td>
                                            Monthly Installment
                                        </td>
                                        <td style='text-align: right'>
                                            <%#GetCurrency( Eval("InstallmentAmount"))%>
                                        </td>
                                    </tr>
                                    <tr class="even">
                                        <td>
                                            No of Installments
                                        </td>
                                        <td style='text-align: right'>
                                            <%# Eval("NoOfInstallments")%>
                                        </td>
                                    </tr>
                                    <tr class="odd">
                                        <td>
                                            Installment Paid
                                        </td>
                                        <td style='text-align: right'>
                                            <%# GetCurrency(Eval("Paid"))%>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                    <asp:Repeater ID="rptAdvance" runat="server">
                        <ItemTemplate>
                            <div style="border: 1px solid #10B0EA; margin-top: 10px;">
                                <table style='border: 0px' width="300px" class="tableLightColor">
                                    <tr>
                                        <th colspan="2" style="text-align: left">
                                            <%# Eval("PDeduction.Title") %>
                                        </th>
                                    </tr>
                                    <tr class="even">
                                        <td>
                                            Amount
                                        </td>
                                        <td style='text-align: right'>
                                            <%#GetCurrency( Eval("AdvanceLoanAmount"))%>
                                        </td>
                                    </tr>
                                    <tr class="odd">
                                        <td>
                                            Taken on
                                        </td>
                                        <td style='text-align: right'>
                                            <%# Eval("TakenOn")%>
                                        </td>
                                    </tr>
                                    <%--<tr class="even">
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>--%>
                                    <tr class="even">
                                        <td>
                                            Monthly Deduction
                                        </td>
                                        <td style='text-align: right'>
                                            <%#GetCurrency( Eval("InstallmentAmount"))%>
                                        </td>
                                    </tr>
                                    <tr class="odd">
                                        <td>
                                            Amount Paid
                                        </td>
                                        <td style='text-align: right'>
                                            <%#GetCurrency(Eval("Paid"))%>
                                        </td>
                                    </tr>
                                    <tr class="even">
                                        <td>
                                            Amount Remaining
                                        </td>
                                        <td style='text-align: right'>
                                            <%#GetCurrency( Convert.ToDecimal(Eval("AdvanceLoanAmount")) - Convert.ToDecimal( Eval("Paid")))%>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                </td>
                <td valign="top" style='padding-left: 0px'>
                    <%--  <h3>
                        Advances</h3>--%>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
