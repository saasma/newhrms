﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL;
using BLL.Manager;
using Web.ReportDataSetTableAdapters;
using Web.Master;
using BLL;
using Web.CP.Report.Templates.Pay.Detail;
using Utils;
using BLL.Base;
using System.IO;
using Ext.Net;
using System.Text;
using Utils.Helper;
using BLL.BO;
using Utils.Calendar;

namespace Web.Employee
{
    public partial class EmpAttTimeRequestList : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialize();                
            }
        }

        private void Initialize()
        {
            if (CommonManager.CompanySetting.WhichCompany != WhichCompany.NIBL)
                notes.Visible = false;
            else
            {
                Response.Redirect("~/Employee/DashboardNew.aspx");
                return;
            }

            Setting setting = OvertimeManager.GetSetting();
            bool timeRequestTeamUsingRecommendApproval = false;
            if (setting.TimeRequestTeamUsingRecommendApproval != null && setting.TimeRequestTeamUsingRecommendApproval.Value)
                timeRequestTeamUsingRecommendApproval = true;

            // used for sana kisan and may be prabhu like client
            if (timeRequestTeamUsingRecommendApproval)
            {
                trApplyTo.Visible = false;
                trRecApp.Visible = true;
                BindRecommenderAndApprovalCombos();

                if (setting.HideReviewInLeaveRequest != null && setting.HideReviewInLeaveRequest.Value)
                {
                    cmbRecommender.Hide();
                    tdRecommend.Style["display"] = "none";
                }
            }
            else
            {
                trApplyTo.Visible = true;
                trRecApp.Visible = false;
                panelRecommended.Hidden = true;
            }


            if (setting.MultipleTimeRequestApproval != null && setting.MultipleTimeRequestApproval.Value && !timeRequestTeamUsingRecommendApproval)
            {
                lblApplyTo.Show();
                string review = "", approval = "";
                LeaveRequestManager.GetApplyToForEmployee(SessionManager.CurrentLoggedInEmployeeId, ref review, ref approval);
                lblApplyTo.Text = approval;
            }
            else
            {
                lblApplyTo.Hide();
            }


            if (CommonManager.CompanySetting.WhichCompany == WhichCompany.NIBL)
            {
                colInTime1.Editor.Clear();
                colOutTime1.Editor.Clear();
            }

            BindCalendar();
            BindGrids();
        }

        private void BindRecommenderAndApprovalCombos()
        {
            List<TextValue> listReview = LeaveRequestManager.GetApplyToListForEmployee(SessionManager.CurrentLoggedInEmployeeId, false, PreDefindFlowType.Leave);

            List<TextValue> listApproval = LeaveRequestManager.GetApplyToListForEmployee(SessionManager.CurrentLoggedInEmployeeId, true, PreDefindFlowType.Leave);

            cmbRecommender.GetStore().DataSource = listReview;
            cmbRecommender.GetStore().DataBind();

            cmbApproval.GetStore().DataSource = listApproval;
            cmbApproval.GetStore().DataBind();
        }

        private void BindGrids()
        {
            List<TimeRequest> list = AttendanceManager.GetTimeRequestsByEmployeeId(SessionManager.CurrentLoggedInEmployeeId);
            gridPending.GetStore().DataSource = list.Where(x => x.Status == (int)AttendanceRequestStatusEnum.Saved).ToList();
            gridPending.GetStore().DataBind();

            gridApproved.GetStore().DataSource = list.Where(x => x.Status == (int)AttendanceRequestStatusEnum.Approved).ToList();
            gridApproved.GetStore().DataBind();

            gridRejected.GetStore().DataSource = list.Where(x => x.Status == (int)AttendanceRequestStatusEnum.Rejected).ToList();
            gridRejected.GetStore().DataBind();

            gridRecommended.GetStore().DataSource = list.Where(x => x.Status == (int)AttendanceRequestStatusEnum.Recommended).ToList();
            gridRecommended.GetStore().DataBind();
        }              

        protected void btnShow_Click(object sender, DirectEventArgs e)
        {
            Clear();            

         

           //when p.note!="";
            if (!string.IsNullOrEmpty(hdnRequestID.Text.Trim()))
            {
              

                int requestID = int.Parse(hdnRequestID.Text.Trim());
                List<TimeRequestLine> list = AttendanceManager.GetTimeRequestLinesByRequestId(requestID);
                if (list.Count > 0)
                {
                    // clearing grid 


                    gridAttRequest.GetStore().DataSource = list;
                    gridAttRequest.GetStore().DataBind();

                    txtStartDateAdd.SelectedDate = list[0].DateEng.Value;
                    txtEndDateAdd.SelectedDate = list[list.Count - 1].DateEng.Value;

                    TimeRequest obj = AttendanceManager.GetTimeRequestByRequestID(requestID);

                    if (obj.Status.Value != 0)
                    {
                        btnSend.Hide();
                        //eventWindow.Height = (list.Count * 25) + 270;
                    }
                    else
                    {
                        btnSend.Show();
                        //eventWindow.Height = (list.Count * 25) + 300;
                    }

                    if (obj.RecommenderEmployeeId != null)
                        cmbRecommender.SetValue(obj.RecommenderEmployeeId.ToString());
                    if (obj.ApprovalEmployeeId != null)
                        cmbApproval.SetValue(obj.ApprovalEmployeeId.ToString());
                }
                else
                {
                    gridAttRequest.GetStore().DataSource = new List<TimeRequestLine>();
                    gridAttRequest.GetStore().DataBind();
                }
               
            } //when p.note="";
            else
            {
                DateTime startDate = DateTime.Parse(txtStartDate.Text.Trim());
                DateTime endDate = DateTime.Parse(txtEndDate.Text.Trim());

                //if (startDate.Date > DateTime.Now.Date || endDate.Date > DateTime.Now.Date)
                //{
                //    NewMessage.ShowWarningMessage("You cannot request attendance in the future.");
                //    return;
                //}

                if (AttendanceManager.CheckTimeRequestIsSaved(startDate, endDate, SessionManager.CurrentLoggedInEmployeeId))
                {
                    NewMessage.ShowWarningMessage("Time request is already saved for the period.");
                    return;
                }

                string message = AttendanceManager.CheckEmpTimeAttRequestThreshold(startDate, endDate);
                if (message != "")
                {
                    NewMessage.ShowWarningMessage(message);
                    return;
                }

                List<TimeRequestLine> list = AttendanceManager.GetTimeRequestLineForDateRange(startDate, endDate, "", SessionManager.CurrentLoggedInEmployeeId);
                gridAttRequest.GetStore().DataSource = list;
                gridAttRequest.GetStore().DataBind();

                txtStartDateAdd.SelectedDate = list[0].DateEng.Value;
                txtEndDateAdd.SelectedDate = list[list.Count - 1].DateEng.Value;

                btnSend.Show();
                //eventWindow.Height = (list.Count * 25) + 330;
            }

            eventWindow.Center();
            eventWindow.Show();            
        }

        private void BindCalendar()
        {
            RegisterLegendColors();
            RegisterTrainingCalendarColorStyles();
        }


        void RegisterLegendColors()
        {
            StringBuilder str = new StringBuilder("\nlegends = new Array();");

            //add holiday legend color also
            //str.AppendFormat("legends['{0}'] = '{1}';", "", "gray");
            str.AppendFormat("legends['{0}'] = '{1}';", "P", "orange");
            str.AppendFormat("legends['{0}'] = '{1}';", "Request", "blue");
            str.AppendFormat("legends['{0}'] = '{1}';", "Approved", "green");
            str.AppendFormat("legends['{0}'] = '{1}';", "Rejected", "red");

            X.ResourceManager.RegisterBeforeClientInitScript(str.ToString());

            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "LegendColorReg123", str.ToString(), true);
        }


        void RegisterTrainingCalendarColorStyles()
        {
            StringBuilder str = new StringBuilder();

            var store = (Ext.Net.CalendarStore)X.GetCtl("GroupStore1");

            //CalendarModel type1 = new CalendarModel();
            //type1.CalendarId = -1;
            //type1.Title = "No";
            //str.Append(GetTrainingCalendarStyle(-1, "gray"));
            //store.Calendars.Add(type1);

            CalendarModel type2 = new CalendarModel();
            type2.CalendarId = 1;
            type2.Title = "P";
            str.Append(GetTrainingCalendarStyle(1, "orange"));
            store.Calendars.Add(type2);

            CalendarModel type3 = new CalendarModel();
            type3.CalendarId = 2;
            type3.Title = "Requested";
            str.Append(GetTrainingCalendarStyle(2, "blue"));
            store.Calendars.Add(type3);

            CalendarModel type4 = new CalendarModel();
            type4.CalendarId = 3;
            type4.Title = "Approved";
            str.Append(GetTrainingCalendarStyle(3, "green"));
            store.Calendars.Add(type4);

            CalendarModel type5 = new CalendarModel();
            type5.CalendarId = 4;
            type5.Title = "Rejected";
            str.Append(GetTrainingCalendarStyle(4, "red"));
            store.Calendars.Add(type5);

            store.DataBind();

            X.ResourceManager.RegisterClientStyleBlock("CalendarStyle", str.ToString());
        }

        public string GetTrainingCalendarStyle(int id, string color)
        {
            //http://forums.ext.net/showthread.php?10566-CLOSED-CalendarPanel-GroupStore-and-CalendarId
            string style =
                @"
                    
             .ext-color-{0},
            .ext-ie .ext-color-{0}-ad,
            .ext-opera .ext-color-{0}-ad {{
                color: {1};
            }}
            .ext-cal-day-col .ext-color-{0},
            .ext-dd-drag-proxy .ext-color-{0},
            .ext-color-{0}-ad,
            .ext-color-{0}-ad .ext-cal-evm,
            .ext-color-{0} .ext-cal-picker-icon,
            .ext-color-{0}-x dl,
            .ext-color-{0}-x .ext-cal-evb {{
                background: {1};
            }}
            .ext-color-{0}-x .ext-cal-evb,
            .ext-color-{0}-x dl {{
                border-color: #7C3939;
            }}
                ";

            return string.Format(style, id, color);
        }

        protected void lnkAssign_Click(object sender, DirectEventArgs e)
        {
            txtStartDateAdd.Text = "";
            txtEndDateAdd.Text = "";

            gridAttRequest.GetStore().DataSource = new List<TimeRequestLine>();
            gridAttRequest.GetStore().DataBind();

            hdnRequestID.Text = "";

            eventWindow.Center();
            eventWindow.Show();
        }

        protected void btnSend_Click(object sender, DirectEventArgs e)
        {
            string entryLineJson = e.ExtraParams["gridItems"];

            List<TimeRequestLine> timeRequestList = JSON.Deserialize<List<TimeRequestLine>>(entryLineJson);
            List<TimeRequestLine> newTimeRequestList = new List<TimeRequestLine>();

            int workDaysCount = 0;

            int row = 0;

            foreach (var item in timeRequestList)
            {
                TimeRequestLine obj = new TimeRequestLine();
                obj.DateEng = item.DateEng;
                obj.DateName = BLL.BaseBiz.GetAppropriateDate(obj.DateEng.Value);
                obj.OvernightShift = item.OvernightShift;

               
              

                TimeSpan tsStartTime, tsEndTime;

                obj.Description = item.Description;

                if (string.IsNullOrEmpty(obj.Description))
                    workDaysCount++;

                row++;

                if (item.InTimeDT != null && item.InTimeDT != new DateTime())
                {
                    tsStartTime = new TimeSpan(item.InTimeDT.Value.Hour, item.InTimeDT.Value.Minute, 0);                    
                    obj.InTime = tsStartTime;
                }

                if (item.OutTimeDT != null && item.OutTimeDT != new DateTime())
                {
                    tsEndTime = new TimeSpan(item.OutTimeDT.Value.Hour, item.OutTimeDT.Value.Minute, 0);
                    obj.OutTime = tsEndTime;
                }

                if (obj.InTime == null && obj.OutTime == null)
                {
                    NewMessage.ShowWarningMessage("Start time or end time is required for row " + row.ToString());
                    return;
                }

                if (!string.IsNullOrEmpty(item.InNote))
                    obj.InNote = item.InNote;
                else 
                {
                    if (obj.InTime != null)
                    {
                        NewMessage.ShowWarningMessage("In Note is required for row " + row.ToString());
                        return;
                    }
                }

                if (!string.IsNullOrEmpty(item.OutNote))
                    obj.OutNote = item.OutNote;
                else 
                {
                    if (obj.OutTime != null)
                    {
                        NewMessage.ShowWarningMessage("Out Note is required for row " + row.ToString());
                        return;
                    }
                }

                if ((item.InTimeDT != null && item.InTimeDT != new DateTime()) && (item.OutTimeDT != null && item.OutTimeDT != new DateTime()))
                {
                    if (obj.OutTime.Value <= obj.InTime.Value && obj.OvernightShift == false)
                    {
                        NewMessage.ShowWarningMessage("Out time must be greater than In time for row " + row.ToString());
                        return;
                    }

                    TimeSpan tsOutTime = new TimeSpan();
                    if (item.OvernightShift.Value)
                    {
                        tsOutTime = obj.OutTime.Value.Add(new TimeSpan(24, 0, 0));
                    }
                    else
                        tsOutTime = obj.OutTime.Value;

                    TimeSpan totalWorkHourTS = tsOutTime - obj.InTime.Value;
                    double time = 0;

                    if (item.OvernightShift.Value)
                    {
                        time = double.Parse((totalWorkHourTS.Hours).ToString() + "." + totalWorkHourTS.Minutes.ToString());
                        obj.WorkHours = time;
                    }
                    else
                    {
                        time = double.Parse(totalWorkHourTS.Hours.ToString() + "." + totalWorkHourTS.Minutes.ToString());
                        obj.WorkHours = time;
                    }

                }

                // validate date
                if (obj.DateEng != null)
                {
                    if (obj.DateEng.Value.Date > DateTime.Now.Date)
                    {
                        NewMessage.ShowWarningMessage("Request can not be made in future date for row " + row.ToString());
                        return;
                    }
                }

                // for wdn do not allow for future time also
                if (CommonManager.CompanySetting.WhichCompany == WhichCompany.WDN)
                {
                    if (obj.InTime != null && obj.DateEng != null  && obj.DateEng.Value.Date == DateTime.Now.Date)
                    {
                        TimeSpan time = obj.InTime.Value;
                        DateTime now = DateTime.Now;
                        TimeSpan currentTime = DateTime.Now.TimeOfDay;

                        if (time > currentTime)
                        {
                            NewMessage.ShowWarningMessage("Request can not be made in future time as current time is only " +
                                   now.ToString("hh:mm tt") + " for row " + row.ToString());
                            return;
                        }
                    }

                    if (obj.OutTime != null && obj.DateEng != null && obj.DateEng.Value.Date == DateTime.Now.Date)
                    {
                        TimeSpan time = obj.OutTime.Value;
                        DateTime now = DateTime.Now;
                        TimeSpan currentTime = DateTime.Now.TimeOfDay;

                        if (time > currentTime)
                        {
                            NewMessage.ShowWarningMessage("Request can not be made in future time as current time is only " +
                                   now.ToString("hh:mm tt") + " for row " + row.ToString());
                            return;
                        }
                    }
                }

                newTimeRequestList.Add(obj);

            }

            TimeRequest objTimeRequest = new TimeRequest();

            if (!string.IsNullOrEmpty(hdnRequestID.Text.Trim()))
                objTimeRequest.RequestID = int.Parse(hdnRequestID.Text.Trim());

            objTimeRequest.EmployeeId = SessionManager.CurrentLoggedInEmployeeId;
            objTimeRequest.StartDateEng = newTimeRequestList[0].DateEng;
            objTimeRequest.StartDate = newTimeRequestList[0].DateName;
            objTimeRequest.EndDateEng = newTimeRequestList[newTimeRequestList.Count - 1].DateEng;
            objTimeRequest.EndDate = newTimeRequestList[newTimeRequestList.Count - 1].DateName;
            objTimeRequest.TimeRequestLines.AddRange(newTimeRequestList);
            objTimeRequest.WorkDays = workDaysCount;
            objTimeRequest.SubmittedOnEng = System.DateTime.Now;
            objTimeRequest.SubmittedOn = BLL.BaseBiz.GetAppropriateDate(objTimeRequest.SubmittedOnEng.Value);
            objTimeRequest.Status = (int)AttendanceRequestStatusEnum.Saved;
            objTimeRequest.IPAddress = HttpContext.Current.Request.UserHostAddress;

            if (txtStartDateAdd.SelectedDate != new DateTime())
            {
                if (!IsDateInBetweenStartEndDate(objTimeRequest.StartDateEng.Value))
                {
                    NewMessage.ShowWarningMessage("Date must be between " + txtStartDateAdd.SelectedDate.ToShortDateString() + " and " + txtEndDateAdd.SelectedDate.ToShortDateString()
                       + ". Please reload the list by clicking Load button.");
                    return;
                }

                if (!IsDateInBetweenStartEndDate(objTimeRequest.EndDateEng.Value))
                {
                    NewMessage.ShowWarningMessage("Date must be between " + txtStartDateAdd.SelectedDate.ToShortDateString() + " and " + txtEndDateAdd.SelectedDate.ToShortDateString()
                        + ". Please reload the list by clicking Load button.");
                    return;
                }
            }

            Setting setting = OvertimeManager.GetSetting();
            bool timeRequestTeamUsingRecommendApproval = false;
            if (setting.TimeRequestTeamUsingRecommendApproval != null && setting.TimeRequestTeamUsingRecommendApproval.Value)
                timeRequestTeamUsingRecommendApproval = true;

            if (timeRequestTeamUsingRecommendApproval)
            {
                bool hideRecommender = setting.HideReviewInLeaveRequest != null && setting.HideReviewInLeaveRequest.Value;

                if (hideRecommender == false)
                {
                    if (cmbRecommender.SelectedItem != null && cmbRecommender.SelectedItem.Value != null && cmbRecommender.SelectedItem.Value != "-1")
                        objTimeRequest.RecommenderEmployeeId = int.Parse(cmbRecommender.SelectedItem.Value);
                    else
                    {
                        NewMessage.ShowWarningMessage("Recommender is required.");
                        cmbRecommender.Focus();
                        return;
                    }
                }

                if (cmbApproval.SelectedItem != null && cmbApproval.SelectedItem.Value != null && cmbApproval.SelectedItem.Value != "-1")
                    objTimeRequest.ApprovalEmployeeId = int.Parse(cmbApproval.SelectedItem.Value);
                else
                {
                    NewMessage.ShowWarningMessage("Approval is required.");
                    cmbApproval.Focus();
                    return;
                }

                if (hideRecommender)
                {
                    objTimeRequest.RecommenderEmployeeId = objTimeRequest.ApprovalEmployeeId;
                }

                // if requesting and recommender selection is same then set status to Recommended instead of Request
                if(objTimeRequest.RecommenderEmployeeId==SessionManager.CurrentLoggedInEmployeeId)
                    objTimeRequest.Status = (int)AttendanceRequestStatusEnum.Recommended;
            }
            else
            {
                if (setting.MultipleTimeRequestApproval != null && setting.MultipleTimeRequestApproval.Value)
                {
                    List<TextValue> listApproval = LeaveRequestManager.GetApplyToListForEmployee(SessionManager.CurrentLoggedInEmployeeId, true, PreDefindFlowType.Overtime);

                    if (listApproval.Count > 0)
                    {
                        if (listApproval.Count == 1)
                            objTimeRequest.ApprovalId1 = int.Parse(listApproval[0].Value);
                        else
                        {
                            objTimeRequest.ApprovalId1 = int.Parse(listApproval[0].Value);
                            objTimeRequest.ApprovalId2 = int.Parse(listApproval[1].Value);
                        }

                        // remove self approval from the list
                        if (objTimeRequest.ApprovalId1 == SessionManager.CurrentLoggedInEmployeeId)
                            objTimeRequest.ApprovalId1 = null;

                        if (objTimeRequest.ApprovalId2 == SessionManager.CurrentLoggedInEmployeeId)
                            objTimeRequest.ApprovalId2 = null;

                    }
                    else
                    {
                        NewMessage.ShowWarningMessage("Approval is required.");
                        return;
                    }
                }
                else
                {
                    NewMessage.ShowWarningMessage("Approval is required.");
                    return;
                }
            }


            Status status = AttendanceManager.SaveTimeRequest(objTimeRequest, timeRequestTeamUsingRecommendApproval);
            if (status.IsSuccess)
            {
                NewMessage.ShowNormalMessage("Time request saved successfully.");
                eventWindow.Close();
                CalendarPanel1.Update(); 
                BindGrids();
            }
            else
                NewMessage.ShowWarningMessage(status.ErrorMessage);

        }


        protected void btnDelete_Click(object sender, DirectEventArgs e)
        {
            int requestID = int.Parse(hdnRequestID.Text.Trim());

            Status status = AttendanceManager.DeleteTimeRequest(requestID);
            if (status.IsSuccess)
            {
                NewMessage.ShowNormalMessage("Time request deleted successfully.");
                BindGrids();
                CalendarPanel1.Update();
                
            }
            else
                NewMessage.ShowWarningMessage(status.ErrorMessage);
        }

        protected void btnLoad_Click(object sender, DirectEventArgs e)
        {
            DateTime startDate = new DateTime();
            DateTime endDate = new DateTime();

            if (txtStartDateAdd.SelectedDate != new DateTime())
                startDate = txtStartDateAdd.SelectedDate;
            else
            {
                NewMessage.ShowWarningMessage("Start date is required.");
                return;
            }

            if (txtEndDateAdd.SelectedDate != new DateTime())
                endDate = txtEndDateAdd.SelectedDate;
            else
            {
                NewMessage.ShowWarningMessage("End date is required.");
                return;
            }

            //if (startDate > DateTime.Now.Date || endDate.Date > DateTime.Now.Date)
            //{
            //    NewMessage.ShowWarningMessage("You cannot request attendance in the future.");
            //    return;
            //}

            //if (AttendanceManager.CheckTimeRequestIsSaved(startDate, endDate, SessionManager.CurrentLoggedInEmployeeId))
            //{
            //    NewMessage.ShowWarningMessage("Time request is already saved for the period.");
            //    return;
            //}

            string message = AttendanceManager.CheckEmpTimeAttRequestThreshold(startDate, endDate);
            if (message != "")
            {
                NewMessage.ShowWarningMessage(message);
                return;
            }

            List<TimeRequestLine> list = AttendanceManager.GetTimeRequestLineForDateRange(startDate, endDate, txtInNoteAdd.Text.Trim(), SessionManager.CurrentLoggedInEmployeeId);

            string gridItemsJSON = e.ExtraParams["gridItems"];
            List<TimeRequestLine> listGrid = JSON.Deserialize<List<TimeRequestLine>>(gridItemsJSON);

            foreach (var item in list)
            {
                foreach (var itemGrid in listGrid)
                {
                    if (itemGrid.DateEng.Value == item.DateEng.Value)
                    {
                        item.InTimeDT = itemGrid.InTimeDT;

                        if(!string.IsNullOrEmpty(itemGrid.InNote))
                            item.InNote = itemGrid.InNote;
                        item.OutTimeDT = itemGrid.OutTimeDT;

                        if(!string.IsNullOrEmpty(itemGrid.OutNote))
                            item.OutNote = itemGrid.OutNote;
                    }
                }
            }

            gridAttRequest.GetStore().DataSource = list;
            gridAttRequest.GetStore().DataBind();

            btnSend.Show();
            //eventWindow.Height = (list.Count * 25) + 300;


            eventWindow.Center();
            eventWindow.Show();



        }

        private bool IsDateInBetweenStartEndDate(DateTime date)
        {
            if ( date.Date >= txtStartDateAdd.SelectedDate.Date && date.Date <= txtEndDateAdd.SelectedDate.Date)
                return true;

            return false;
        }

        private void Clear()
        {
            txtStartDateAdd.Text = "";
            txtEndDateAdd.Text = "";
            txtInNoteAdd.Text = "";
        }
    
    }
}