﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using BLL;
using System.Data;
using System.IO;
using BLL.Manager;
using DAL;
using BLL.Entity;
using System.Data.OleDb;

namespace Web.Employee
{
    public partial class EmployeeOTImport : BasePage
    {
        static int RecommenderId, ApprovalId, overtimeTypeId;

        public bool HasImport
        {
            get
            {
                if (ViewState["import"] == null)
                    return false;
                return Convert.ToBoolean(ViewState["import"]);
            }
            set
            {
                ViewState["import"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!string.IsNullOrEmpty(Request.QueryString["OvertimeType"]))
                    hdnOverType.Text = Request.QueryString["OvertimeType"];
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            //Page.ClientScript.RegisterOnSubmitStatement(this.GetType(), "sdfdsffd", "window.onunload = null;");
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "sdf", "var hasImport = " + (this.HasImport ? "true;" : "false;"), true);
        }




        protected void btnExport_Click(object sender, EventArgs e)
        {
            string template = ("~/App_Data/ExcelTemplate/EmpOvertimeImp.xls");

            ExcelGenerator.ExportEmpOvertime(template, hdnOverType.Text + ".xls");

        }


        protected void btnImport_Click(object sender, EventArgs e)
        {
            string template = Server.MapPath("~/App_Data/ExcelTemplate/" + Guid.NewGuid() + ".xls");

            List<OverTimeClsNew> list = new List<OverTimeClsNew>();

            if (fupUpload.HasFile && fupUpload.FileName.ToLower().Contains(".xls"))
            {
                fupUpload.SaveAs(template);

                DataTable datatable = ExcelGenerator.ImportFromExcel(template, "select * from [Sheet1$]");

                if (datatable == null)
                {
                    divWarningMsg.InnerHtml = Resources.Messages.HPL_Allowance_ImportFail;
                    divWarningMsg.Hide = false;
                    File.Delete(template);
                    return;
                }
                
                int importedCount = 0;

                try
                {
                    int rowNumber = 0;

                    foreach (DataRow row in datatable.Rows)
                    {                      

                        OverTimeClsNew item = new OverTimeClsNew();
                        item.SN = rowNumber;

                        rowNumber += 1;

                        importedCount += 1;
                        DateTime date = new DateTime();
                        TimeSpan tsStartTime;

                        if (row["Date"] != DBNull.Value && row["Date"].ToString() != "")
                        {
                            try
                            {
                                date = Convert.ToDateTime(row["Date"].ToString());

                                item.DateEng = date;
                            }
                            catch
                            {

                                divWarningMsg.InnerHtml = "Date is invalid for the row : " + rowNumber ;
                                divWarningMsg.Hide = false;
                                return;
                            }
                        }
                        else
                        {
                            divWarningMsg.InnerHtml = "Date is required for the row : " + rowNumber;
                            divWarningMsg.Hide = false;
                            return;
                        }

                        if (row["Start Time"] != DBNull.Value && row["Start Time"].ToString() != "")
                        {
                            try
                            {
                                
                                //string startTime = row["Start Time"].ToString();
                                //string[] splInTime = startTime.Split(':');
                                //int hoursStart = int.Parse(splInTime[0]);
                                //int minutesStart = int.Parse(splInTime[1]);

                                //item.InTime = new DateTime(date.Year, date.Month, date.Day, hoursStart, minutesStart, 0);

                                //tsStartTime = new TimeSpan(hoursStart, minutesStart, 0);

                                DateTime dtStart = DateTime.Parse(row["Start Time"].ToString());
                                int hoursStart = dtStart.Hour;
                                int minutesStart = dtStart.Minute;
                                item.InTime = new DateTime(date.Year, date.Month, date.Day, hoursStart, minutesStart, 0);
                                tsStartTime = new TimeSpan(hoursStart, minutesStart, 0);
                            }
                            catch
                            {

                                divWarningMsg.InnerHtml = "Start Time is invalid for the row : " + rowNumber;
                                divWarningMsg.Hide = false;
                                return;
                            }
                        }
                        else
                        {
                            divWarningMsg.InnerHtml = "Start Time is required for the row : " + rowNumber;
                            divWarningMsg.Hide = false;
                            return;
                        }

                        if (row["End Time"] != DBNull.Value && row["End Time"].ToString() != "")
                        {
                            try
                            {
                                //string startTime = row["End Time"].ToString();
                                //string[] splInTime = startTime.Split(':');
                                //int hoursStart = int.Parse(splInTime[0]);
                                //int minutesStart = int.Parse(splInTime[1]);

                                //item.OutTime = new DateTime(date.Year, date.Month, date.Day, hoursStart, minutesStart, 0);

                                DateTime dtEnd = DateTime.Parse(row["End Time"].ToString());
                                int hoursStart = dtEnd.Hour;
                                int minutesStart = dtEnd.Minute;
                                item.OutTime = new DateTime(date.Year, date.Month, date.Day, hoursStart, minutesStart, 0);                                

                            }
                            catch
                            {

                                divWarningMsg.InnerHtml = "End Time is invalid for the row : " + rowNumber;
                                divWarningMsg.Hide = false;
                                return;
                            }
                        }
                        else
                        {
                            divWarningMsg.InnerHtml = "End Time is required for the row : " + rowNumber;
                            divWarningMsg.Hide = false;
                            return;
                        }

                        if (row["Reason"] != DBNull.Value && row["Reason"].ToString() != "")
                        {
                            item.OutNote = row["Reason"].ToString();
                        }
                        else
                        {
                            divWarningMsg.InnerHtml = "Reason is required for the row : " + rowNumber;
                            divWarningMsg.Hide = false;
                            return;
                        }

                        list.Add(item);

                    }

                    if(list.Count > 0)
                    {
                        Session["OTRequests"] = list;
                        divMsgCtl.InnerHtml = "Overtime imported successfully.";
                        divMsgCtl.Hide = false;

                        this.HasImport = true;
                    }
                    else
                    {
                        divWarningMsg.InnerHtml = "Error while importing overtime.";
                        divWarningMsg.Hide = false;
                    }


                }

                    //sheet name changed error
                catch (OleDbException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_SheetNameError);
                    divWarningMsg.Hide = false;
                }
                catch (ArgumentException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_ColumnNameChangedError);
                    divWarningMsg.Hide = false;
                }
            }
        }
    }
}