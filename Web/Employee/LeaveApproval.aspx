﻿<%@ Page Title="Leave Approval" Language="C#" EnableViewState="false" AutoEventWireup="true"
    CodeBehind="LeaveApproval.aspx.cs" Inherits="Web.Employee.LeaveApproval" %>

<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/FooterCtl.ascx" TagName="FooterCtl" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/HeaderCtl.ascx" TagName="HeaderCtl" TagPrefix="uc3" %>
<%@ Register Src="~/UserControls/LogoutCtl.ascx" TagName="LogoutCtl" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/TopMenu.ascx" TagName="TopMenu" TagPrefix="uc2" %>
<%--<%@ Register Src="~/Employee/UserControls/LeaveApproval.ascx" TagName="LA" TagPrefix="uc4" %>--%>
<%@ Register Src="~/Employee/UserControls/AssignLeave.ascx" TagName="AssingLeave"
    TagPrefix="uc5" %>
<!DOCTYPE html>
<html>
<head runat="server">
    <title>Leave Approval</title>
    <meta http-equiv="X-UA-Compatible" content="IE=8" />
    <link id="Link5" runat="server" rel="stylesheet" href="~/payrollmenu_files/mbcsmbmcp.css"
        type="text/css" />
    <link id="Link1" rel="stylesheet" type="text/css" href="<%= ResolveUrl("~/css/core.css?v=") + Web.Helper.WebHelper.Version %>" />
    <link id="Link2" rel="Stylesheet" type="text/css" href="<%= ResolveUrl("~/Styles/calendar/calendar.css?v=") + Web.Helper.WebHelper.Version %>" />
    <link id="Link4" runat="server" rel="stylesheet" href="~/Styles/design.css" />
    <ext:ResourcePlaceHolder ID="ResourcePlaceHolder2" Mode="Script" runat="server" />
    <ext:ResourcePlaceHolder ID="ResourcePlaceHolder1" Mode="Style" runat="server" />
    <style type="text/css">
        a.x-datepicker-eventdate
        {
            border: 1px solid #a3bad9;
            padding: 1px; /* background-color: Cyan;*/
        }
        
        .icon-delete
        {
            background-image: url("../images/delet.png") !important;
        }
        .link_assignleave
        {
            /*background-color: #FeFeFe;*/
            border-top: 1px solid #eee;
            border-left: 1px solid #eee;
            border-right: 1px solid #ddd;
            border-bottom: 1px solid #ddd;
            float: left;
            margin-bottom: 5px;
            padding: 5px 15px;
        }
        thead td, th
        {
            border: 0px solid;
        }
        .ext-cal-evr
        {
            padding: 1px 1px 1px 5px;
        }
        .x-form-spinner-splitter
        {
            display: none !important;
        }
        .spWidth
        {
            width: 117 !important;
        }
        #CompanyX.ctl27_cboHour_Container, #ext-gen156
        {
            width: 120px !important;
        }
        #mbmcpebul_table
        {
            margin-top: 0px !important;
        }
        
        .bodypart
        {
            margin: 0 auto !important;
        }
        .x-panel-header-text-container-default-framed
        {
            color: Black;
            font-weight: normal;
        }
        /*grid cell padding*/
        .x-grid-cell-inner
        {
            padding: 4px 0px 3px 5px;
        }
        /*hide calendar icon*/
        .ext-cal-ic-rem
        {
            display: none;
        }
        .readonlyClass input
        {
            background-color: #F5F5F5 !important;
        }
    </style>
    <script type="text/javascript" src="<%= ResolveUrl("~/Employee/common.js?v=") + Web.Helper.WebHelper.Version %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/Employee/override.js?v=") + Web.Helper.WebHelper.Version %>"></script>
    <link href="<%=ResolveUrl("~/bootstrap/css/bootstrap.css?v=") + Web.Helper.WebHelper.Version %>"
        rel="stylesheet" />
    <script type="text/javascript">
     

        var currentMonth = null, currentYear = null;
        var holidayList = null; var datePickerChanged = false;
        var hasHourlyLeave = false;
        var btnApprove = null;
        var chkIsHalfDay = null;
        var hdFromDateEng= null;
        function getChkAllDay() {
            try {
                return CompanyX.EventEditWindow1.items.itemAt(0).items.items[1].items.items[5];
            }
            catch (eee) {
                return undefined;
            }
        }


        function getHolidayObject(date) {
            var str = (date.getMonth() + 1) + "/" + date.getDate() + "/" + date.getFullYear();
            for (var i = 0; i < holidayList.length; i++) {
                if (holidayList[i].DateEngText == str) {
                    return holidayList[i];
                }
            }
            return null;
        }

        Ext.onReady(function () {
            hdFromDateEng = <%= hdFromDateEng.ClientID %>;
            reloadGrids();

            CompanyX.storeLeaveApproval.reload();
            CompanyX.storePendingRequest.reload();

            if (window.location.hash.toString() != "") {
                var hash = window.location.hash.toString().replace("#", "");
                if (hash != null) {
                    MoreHandler(hash);
                }
            }


        });

        var loadHoliday = function (month, year) {
            Ext.net.Mask.show();
            Ext.net.DirectMethod.request({
                url: "../LeaveRequestService.asmx/GetHolidays",
                cleanRequest: true,
                json: true,
                params: {
                    month: month,
                    year: year
                },
                success: function (result) {
                    holidayList = result;
                    //Ext.Msg.alert("Json Message", result);
                    CompanyX.DatePicker1.highlightDates(result);
                    Ext.net.Mask.hide();

                    //find the range case
                    for (i = 0; i < result.length; i++) {
                        if (result[i].Id == 0) {
                            CompanyX.lblCalendarTitle.setText(result[i].Abbreviation);
                            break;
                        }
                    }
                },
                failure: function (result) { Ext.net.Mask.hide(); }
            });
        };

        var calRender = function (e1, e2, e3, e4, e5) {
        };

        //method that will be called when item selected changes for Item in the GridPanel
        var leaveRenderer = function (value) {

            var r = CompanyX.storeLeaves.getById(value);

            if (Ext.isEmpty(r)) {
                return "";
            }

            return r.data.Title;
        };


        function changeEventWindow(window) {
            //change text 
            window.get(0).form.items.items[0].fieldLabel = "Reason";
            window.get(0).form.items.items[2].fieldLabel = "Leave type";

            //hide edit details link button
            window.fbar.items.items[0].hide();

        };

        function changeEventWindowAfterRender(window) {
            chkIsHalfDay = Ext.get('chkIsHalfDay').dom;

            if (hasHourlyLeave) {
                //Hide all day check box
                chkIsHalfDay.parentNode.parentNode.parentNode.parentNode.style.display = "none";
            }
            else {

                //hide  "All day" check box
                document.getElementsByName('date-range-allday')[0].style.display = "none";
                //hide "All day" check box label just the side of the checkbox
                document.getElementsByName('date-range-allday')[0].nextSibling.style.display = "none";

                //hide td of start time
                Ext.get('date-range-start-time').dom.parentNode.parentNode.style.display = 'none';
                Ext.get('date-range-start-time').hide();
                Ext.get('date-range-end-time').dom.parentNode.parentNode.style.display = 'none';
                Ext.get('date-range-end-time').hide();
            }
        }

        //        function deleteHandler(record) {
        //            if (confirm("Are your sure, you want to cancel the Approved leave?")) {
        //                var leaveRequestId = record.data.LeaveRequestId;

        //                Ext.net.Mask.show();
        //                Ext.net.DirectMethod.request({
        //                    url: "../LeaveRequestService.asmx/CancelledApprovedLeaveRequest",
        //                    cleanRequest: true,
        //                    json: true,
        //                    params: {
        //                        leaveRequestId: leaveRequestId
        //                    },
        //                    success: function (result) {
        //                        if (result.IsSuccess.toString() == "true") {

        //                            CompanyX.storeLeaveApproval.reload();
        //                            CompanyX.EventStore1.reload();
        //                        }
        //                        Ext.net.Mask.hide();
        //                    },
        //                    failure: function (result) { Ext.net.Mask.hide(); }
        //                });
        //                //CompanyX.storeLeaveApproval.reload();
        //                //CompanyX.EventStore1.reload();
        //            }
        //        }

        function MoreHandler(leaveRequestId) {
            //            var ApplyTo = record.data.ApplyTo.toString();
            //            //var ApplyToId = record.data.ApplyToId.toString();
            //            // var CC1Id = record.data.CC1Id.toString();
            //            var CC1Name = record.data.CC1Name.toString();
            //            //var CC2Id = record.data.CC2Id.toString();
            //            var CC2Name = record.data.CC2Name.toString();
            //            //var CreatedOn = record.data.CreatedOn.toString();
            //            var DaysOrHours = record.data.DaysOrHours.toString();
            //            //var DepartmentName = record.data.DepartmentName.toString();
            //            var EmployeeId = record.data.EmployeeId.toString();
            //            var EmployeeName = record.data.EmployeeName.toString();
            //            var FromDate = record.data.FromDate.toString();
            //            var FromDateEng = record.data.FromDateEng.toString();
            //            var IsHour = record.data.IsHour.toString();
            //            var LeaveRequestId = record.data.LeaveRequestId.toString();
            //            var LeaveTypeId = record.data.LeaveTypeId.toString();
            //            //alert(LeaveTypeId);
            //            //var Range = record.data.Range.toString();
            //            var Reason = record.data.Reason.toString();
            //            var Title = record.data.Title.toString();
            //            var ToDate = record.data.ToDate.toString();
            //            var ToDateEng = record.data.ToDateEng.toString();
            //Ext.net.Mask.show(this);


            //add in ajax history
            Ext.History.add(leaveRequestId);
            <%=HiddenLeaveRequestId.ClientID %>.setValue(leaveRequestId);

            //call to clear time as during postback time value giving error
            clearTimeFields();
            //Ext.net.DirectMethods.ClickMore(leaveRequestId);

            <%=btn.ClientID %>.fireEvent('click');
        }
        var ClickApprove1 = function (grid) {
            if (grid.selModel.selections.items.length > 0) {
                MoreHandler(grid.selModel.selections.items[0].data.LeaveRequestId)
            }
            //            CompanyX.btnApprove.fireEvent('click');
        }

        var attachToolTip = function () {

            var reg = /[a-z\ ]?CalendarPanel1\-[a-z\-]+\-evt\-(\d+)/,
                               cls = Ext.fly(this.triggerElement).dom.className,
                               eventId = reg.exec(cls)[1],
                               recordIndex = CompanyX.EventStore1.find('EventId', parseInt(eventId)),
                               eventData = (CompanyX.EventStore1.getAt(recordIndex) == 'undefined' ? null : CompanyX.EventStore1.getAt(recordIndex).data);
            this.body.dom.innerHTML = (eventData == null ? '' : Ext.encode(eventData.Title));
        }

        var prepareToolbar = function (e1, toolbar, e3, record) {

            if (record.data.Status == 'Approved' && record.data.IsAttendanceSaved == false) {
            }
            else {
                toolbar.hide();
            }
        }

        function clearAjaxHistory() {
            Ext.History.add('');
        }
        //        Ext.History.on('change', function (token) {
        //            
        //            alert(token);
        //            if (token) {
        //                //                var parts = token.split(tokenDelimiter);
        //                //                var tabPanel = Ext.getCmp(parts[0]);
        //                //                var tabId = parts[1];

        //                //                tabPanel.show();
        //                //                tabPanel.setActiveTab(tabId);
        //            } else {
        //                // This is the initial default state.  Necessary if you navigate starting from the
        //                // page without any existing history token params and go back to the start state.
        //                //                tp.setActiveTab(0);
        //                //                tp.getItem(0).setActiveTab(0);
        //            }
        //        });

    </script>
</head>
<body style="margin: 0px">
    <form runat="server">
    <ext:ResourceManager ID="ResourceManager1" runat="server" DisableViewState="false"
        ShowWarningOnAjaxFailure="false" ScriptMode="Release" Namespace="CompanyX" />
    <ext:Store ID="storeEmployee1" runat="server">
        <Model>
            <ext:Model ID="Model11" IDProperty="Value" runat="server">
                <Fields>
                    <ext:ModelField Name="Value" Type="String" />
                    <ext:ModelField Name="Text" />
                </Fields>
            </ext:Model>
        </Model>
    </ext:Store>
    <uc3:HeaderCtl ID="HeaderCtl1" runat="server" />
    <ext:Hidden ID="hdFromDateEng" runat="server" />
    <ext:Hidden ID="hdStatus" runat="server" />
    <ext:Hidden ID="hdCompanyId" runat="server" />
    <ext:Hidden ID="HiddenLeaveRequestId" runat="server" />
    <ext:Button Hidden="true" runat="server" ID="btn">
        <DirectEvents>
            <Click OnEvent="btn_Click">
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:Button>
    <div id="bodypart">
        <div class="bodypart">
            <ext:Store runat="server" ID="storeLeaves">
                <Model>
                    <ext:Model IDProperty="LeaveTypeId" runat="server">
                        <Fields>
                            <ext:ModelField Name="LeaveTypeId" Type="Int" />
                            <ext:ModelField Name="Title" Type="String" />
                        </Fields>
                    </ext:Model>
                </Model>
            </ext:Store>
            <ext:ToolTip ID="toolTipForCalendar" runat="server" Target="={#{CalendarPanel1}.body}"
                Delegate=".ext-cal-evt" Calendar="={#{CalendarPanel1}}">
                <Listeners>
                    <Show Fn="attachToolTip" />
                </Listeners>
            </ext:ToolTip>
            <h3 style='margin-top: 10px; margin-bottom: 0px'>
                Leave Approval</h3>
            <table style='clear: both'>
                <tr>
                    <td colspan="1">
                        <ext:Label runat="server" Height="30" ID="lblCalendarTitle" Text="" />
                    </td>
                    <td class="link_assignleave" style="margin-bottom: 0px; padding: 0px!important; border: 0px!important"
                        colspan="2">
                        <table cellpadding="0" cellspacing="0" border="0" style="margin-bottom: 7px;">
                            <tr>
                                <td style="padding-bottom: 4px; padding-left: 10px;">
                                    <ext:LinkButton ID="lnkAssignLeave" runat="server" Text="Assign Leave">
                                        <DirectEvents>
                                            <Click OnEvent="lnkAssignLeave_Click">
                                                <EventMask ShowMask="true" />
                                            </Click>
                                        </DirectEvents>
                                        <Listeners>
                                            <Click Handler="clearTimeFields();" />
                                        </Listeners>
                                        <%-- <Listeners>
                                <Click Handler="#{storeLeaveOnDay}.reload();" />
                            </Listeners>--%>
                                    </ext:LinkButton>
                                </td>
                                <td style="padding-left: 20px">
                                    <ext:ComboBox Width="240" LabelWidth="100" ID="cboLeaveStatus" SelectedIndex="0"
                                        runat="server" FieldLabel="Filter by Status" LabelSeparator="">
                                        <Items>
                                            <ext:ListItem Text="All" Value="0" />
                                            <ext:ListItem Text="Request" Value="1" />
                                            <ext:ListItem Text="Recommended" Value="6" />
                                            <ext:ListItem Text="Approved" Value="2" />
                                        </Items>
                                        <Listeners>
                                            <Select Handler="CompanyX.EventStore1.reload();" />
                                        </Listeners>
                                        <SelectedItems>
                                            <ext:ListItem Index="0" />
                                        </SelectedItems>
                                    </ext:ComboBox>
                                </td>
                                <td style="padding-left: 20px">
                                    <ext:MultiCombo Width="240" LabelSeparator="" SelectionMode="All" LabelWidth="50"
                                        ID="cmbBranch" DisplayField="Name" ValueField="BranchId" EmptyText="Select Branch"
                                        runat="server" FieldLabel="Branch">
                                        <Store>
                                            <ext:Store ID="Store1" runat="server">
                                                <Model>
                                                    <ext:Model IDProperty="BranchId" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="BranchId" />
                                                            <ext:ModelField Name="Name" />
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <Listeners>
                                            <Select Handler="reloadGrids();" />
                                        </Listeners>
                                    </ext:MultiCombo>
                                </td>
                                <td style="padding-left: 20px;">
                                    <ext:ComboBox Width="260" LabelWidth="60" ID="cmbEmployee" DisplayField="Text" ValueField="Value"
                                        StoreID="storeEmployee1" QueryMode="Local" AllowBlank="true" EmptyText="Select Employee"
                                        runat="server" FieldLabel="Employee">
                                        <Triggers>
                                            <ext:FieldTrigger Icon="Clear" Qtip="Remove selected" />
                                        </Triggers>
                                        <Listeners>
                                            <Select Handler="reloadGridsAfterApproval();" />
                                            <TriggerClick Handler="this.clearValue();reloadGridsAfterApproval();" />
                                        </Listeners>
                                    </ext:ComboBox>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <ext:DatePicker ID="DatePicker1" runat="server" StyleSpec='StyleSpec="border:1px solid #A3BAD9;'
                            Width="219">
                            <Plugins>
                                <ext:GenericPlugin ID="HighlightDates1" runat="server" InstanceName="Ext.ux.DatePicker.HighlightDates" />
                            </Plugins>
                            <Listeners>
                                <Select Fn="CompanyX.setStartDate" Scope="CompanyX" />
                            </Listeners>
                        </ext:DatePicker>
                    </td>
                    <td valign="top" colspan="2" align="left" style="padding-left: 10px">
                        <ext:CalendarPanel ShowNavBar="false" ShowDayView="false" ShowWeekView="false" ID="CalendarPanel1"
                            HeaderAsText="true" Header="true" Height="415" Width="972" runat="server" ActiveIndex="2"
                            Border="true" Margins="0">
                            <CalendarStore ID="GroupStore1" runat="server">
                            </CalendarStore>
                            <%--<loadmask showmask="true" />--%>
                            <EventStore ID="EventStore1" runat="server" NoMappings="true">
                                <Proxy>
                                    <ext:AjaxProxy Url="../LeaveRequestService.asmx/GetLeaveListForApproval" Json="true">
                                        <ActionMethods Read="POST" />
                                        <Reader>
                                            <ext:JsonReader Root="d" />
                                        </Reader>
                                    </ext:AjaxProxy>
                                </Proxy>
                                <Parameters>
                                    <ext:StoreParameter Name="leaveRequestFrom" Value="0" />
                                    <ext:StoreParameter Name="Status" Value="#{cboLeaveStatus}.getValue()" Mode="Raw" />
                                    <ext:StoreParameter Name="Branch" Value="#{cmbBranch}.getSelectedValues().toString()"
                                        Mode="Raw" />
                                    <ext:StoreParameter Name="Employee" Value="#{cmbEmployee}.getValue()" Mode="Raw" />
                                    <ext:StoreParameter Name="StartDate" Mode="Raw" Value="CompanyX.CalendarPanel1.layout.activeItem.lastRenderStart" />
                                    <ext:StoreParameter Name="EndDate" Mode="Raw" Value="CompanyX.CalendarPanel1.layout.activeItem.lastRenderEnd" />
                                </Parameters>
                                <Mappings>
                                    <ext:ModelField Name="StartDate" Type="Date" DateFormat="M$" />
                                    <ext:ModelField Name="EndDate" Type="Date" DateFormat="M$" />
                                </Mappings>
                                <Listeners>
                                    <BeforeLoad Handler="Ext.net.Mask.show();" />
                                    <Load Handler="Ext.net.Mask.hide();" />
                                </Listeners>
                            </EventStore>
                            <%-- <DayView Hidden="true" Disabled="true">
                            </DayView>
                            <WeekView Hidden="true" Disabled="true">
                            </WeekView>--%>
                            <MonthView ID="MonthView1" runat="server" ShowHeader="true" ShowWeekLinks="false"
                                ShowWeekNumbers="true">
                            </MonthView>
                            <Listeners>
                                <DayClick Fn="CompanyX.approvalDayClick" Scope="CompanyX" />
                                <ViewChange Fn="CompanyX.viewChange" Scope="CompanyX" />
                                <EventClick Fn="CompanyX.record.show" Scope="CompanyX" />
                                <RangeSelect Fn="CompanyX.RangeSelection" Scope="CompanyX" />
                                <%--<EventMove Handler="return false;" />--%>
                                <EventResize Fn="CompanyX.record.resize" Scope="CompanyX" />
                                <EventDelete Fn="CompanyX.record.remove" />
                            </Listeners>
                        </ext:CalendarPanel>
                    </td>
                </tr>
            </table>
            <ext:TabPanel ID="Panel2" runat="server" MinHeight="500" AutoHeight="true" StyleSpec="padding-top:20px"
                AutoWidth="true" DefaultBorder="false">
                <Items>
                    <ext:GridPanel ID="gridPendingRequest" StyleSpec="" MinHeight="400" Border="true"
                        StripeRows="true" Cls="gridtbl" Header="true" runat="server" AutoHeight="true"
                        Title="Leave Pending Approval">
                        <Store>
                            <ext:Store runat="server" ID="storePendingRequest" AutoLoad="false">
                                <Proxy>
                                    <ext:AjaxProxy Url="~/LeaveRequestService.asmx/GetLeaveByDateApproval" Json="true">
                                        <ActionMethods Read="POST" />
                                        <Reader>
                                            <ext:JsonReader Root="d.Data" TotalProperty="d.TotalRecords" />
                                        </Reader>
                                    </ext:AjaxProxy>
                                </Proxy>
                                <Model>
                                    <ext:Model runat="server" Root="d.Data" TotalProperty="d.TotalRecords" IDProperty="LeaveRequestId">
                                        <Fields>
                                            <ext:ModelField Name="EmployeeName" Type="String" />
                                            <ext:ModelField Name="EmployeeId" Type="String" />
                                            <ext:ModelField Name="DepartmentName" Type="String" />
                                            <ext:ModelField Name="LeaveRequestId" Type="String" />
                                            <ext:ModelField Name="LeaveTypeId" Type="String" />
                                            <ext:ModelField Name="Title" Type="String" />
                                            <ext:ModelField Name="IsHour" Type="String" />
                                            <ext:ModelField Name="Range" Type="String" />
                                            <ext:ModelField Name="DaysOrHours" Type="String" />
                                            <ext:ModelField Name="Reason" Type="String" />
                                            <ext:ModelField Name="From" Type="String" />
                                            <ext:ModelField Name="To" Type="String" />
                                            <ext:ModelField Name="Created" Type="String" />
                                            <ext:ModelField Name="Status" Type="string" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                                <AutoLoadParams>
                                    <ext:Parameter Name="start" Value="={0}" />
                                    <ext:Parameter Name="limit" Value="50" />
                                </AutoLoadParams>
                                <Parameters>
                                    <ext:StoreParameter Name="start" Value="={0}" />
                                    <ext:StoreParameter Name="limit" Value="50" />
                                    <ext:StoreParameter Name="FromDateEng" Value="CompanyX.DatePicker1.activeDate.toDateString()"
                                        Mode="Raw" />
                                    <ext:StoreParameter Name="leaveRequestEmployeeId" Value="-1" Mode="Value" />
                                    <ext:StoreParameter Name="Status" Value="1-6" Mode="Value" />
                                    <ext:StoreParameter Name="companyId" Value="#{hdCompanyId}.getValue()" Mode="Raw" />
                                    <ext:StoreParameter Name="isYearly" Value="false" Mode="Raw" />
                                    <ext:StoreParameter Name="ExcludeLeaveRequestId" Value="-1" Mode="Value" />
                                    <ext:StoreParameter Name="Branch" Value="#{cmbBranch}.getSelectedValues().toString()"
                                        Mode="Raw" />
                                    <ext:StoreParameter Name="Employee" Value="#{cmbEmployee}.getValue()" Mode="Raw" />
                                </Parameters>
                            </ext:Store>
                        </Store>
                        <View>
                        </View>
                        <ColumnModel runat="server" ID="columnmodle1">
                            <Columns>
                                <ext:Column Sortable="false" Align="Center" MenuDisabled="true" Header="EIN" Width="35"
                                    DataIndex="EmployeeId" />
                                <ext:Column Sortable="false" MenuDisabled="true" Header="Name" Width="130" DataIndex="EmployeeName" />
                                <ext:Column Sortable="false" MenuDisabled="true" Header="Department" Width="130"
                                    DataIndex="DepartmentName" />
                                <ext:Column Sortable="false" MenuDisabled="true" Header="Leave Type" Width="100"
                                    DataIndex="Title" />
                                <ext:Column Sortable="false" MenuDisabled="true" Text="From" Width="100" DataIndex="From" />
                                <ext:Column ID="DateColumn1" runat="server" Text="Date" DataIndex="To" MenuDisabled="false"
                                    Sortable="true" Align="Left" Width="100" />
                                <ext:Column ID="ColumnDaysorHours1" Sortable="false" MenuDisabled="true" Align="Center"
                                    Text="Days" Width="60" DataIndex="DaysOrHours" />
                                <ext:Column Sortable="false" MenuDisabled="true" Header="Reason" Width="270" DataIndex="Reason" />

                                <ext:Column ID="Column1" runat="server" Sortable="false" MenuDisabled="true" Header="Status"
                                    Width="90" DataIndex="Status" />

                                <ext:Column Sortable="false" MenuDisabled="true" Align="Center" Header="Date" Width="90"
                                    DataIndex="Created" />
                                <ext:CommandColumn Width="60" ButtonAlign="Center" Hidden="false">
                                    <Commands>
                                        <ext:GridCommand Icon="ArrowRight" CommandName="More" Text="more  " />
                                    </Commands>
                                    <Listeners>
                                        <Command Handler="MoreHandler(record.data.LeaveRequestId)" />
                                        <%--<rowdblclic fn="ClickApprove1" />--%>
                                    </Listeners>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar2" runat="server" PageSize="50" DisplayInfo="true"
                                DisplayMsg="Displaying Leaves {0} - {1} of {2}" EmptyMsg="No record to display" />
                        </BottomBar>
                        <SelectionModel>
                            <ext:CheckboxSelectionModel ID="RowSelectionModel1" runat="server" Width="30" />
                        </SelectionModel>
                        <%--<loadmask showmask="true" />--%>
                    </ext:GridPanel>
                    <ext:GridPanel StyleSpec="" ID="gridLeaveBalance" MinHeight="400" Border="true" Cls="gridtbl"
                        StripeRows="true" Header="true" runat="server" AutoHeight="true" Title="Leave History">
                        <Store>
                            <ext:Store runat="server" RemoteSort="true" ID="storeLeaveApproval" AutoLoad="false">
                                <Proxy>
                                    <ext:AjaxProxy Json="true" Url="~/LeaveRequestService.asmx/GetLeaveByDateApproval">
                                        <ActionMethods Read="POST" />
                                        <Reader>
                                            <ext:JsonReader Root="d.Data" TotalProperty="d.TotalRecords" />
                                        </Reader>
                                    </ext:AjaxProxy>
                                </Proxy>
                                <AutoLoadParams>
                                    <ext:Parameter Name="start" Value="={0}" />
                                    <ext:Parameter Name="limit" Value="50" />
                                </AutoLoadParams>
                                <Parameters>
                                    <ext:StoreParameter Name="start" Value="={0}" />
                                    <ext:StoreParameter Name="limit" Value="50" />
                                    <%-- Raw can contain js code, Value is direct value to be passed to the serve--%>
                                    <ext:StoreParameter Name="FromDateEng" Value="CompanyX.DatePicker1.activeDate.toDateString()"
                                        Mode="Raw" />
                                    <ext:StoreParameter Name="Status" Value="2-3-5" Mode="Value" />
                                    <ext:StoreParameter Name="leaveRequestEmployeeId" Value="-1" Mode="Value" />
                                    <ext:StoreParameter Name="companyId" Value="#{hdCompanyId}.getValue()" Mode="Raw" />
                                    <ext:StoreParameter Name="ExcludeLeaveRequestId" Value="-1" Mode="Value" />
                                    <ext:StoreParameter Name="Branch" Value="#{cmbBranch}.getSelectedValues().toString()"
                                        Mode="Raw" />
                                    <ext:StoreParameter Name="Employee" Value="#{cmbEmployee}.getValue()" Mode="Raw" />
                                </Parameters>
                                <Model>
                                    <ext:Model runat="server">
                                        <Fields>
                                            <ext:ModelField Name="EmployeeName" Type="String" />
                                            <ext:ModelField Name="EmployeeId" Type="String" />
                                            <ext:ModelField Name="LeaveRequestId" Type="String" />
                                            <ext:ModelField Name="Title" Type="String" />
                                            <ext:ModelField Name="IsHour" Type="String" />
                                            <ext:ModelField Name="From" Type="String" />
                                            <ext:ModelField Name="To" Type="String" />
                                            <ext:ModelField Name="DaysOrHours" Type="String" />
                                            <ext:ModelField Name="Reason" Type="String" />
                                            <ext:ModelField Name="Status" Type="String" />
                                            <ext:ModelField Name="IsAttendanceSaved" Type="Boolean" />
                                            <ext:ModelField Name="ApprovedBy" Type="String" />
                                            <ext:ModelField Name="Comment" Type="String" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <SelectionModel>
                            <ext:RowSelectionModel ID="RowSelectionModel3" runat="server">
                            </ext:RowSelectionModel>
                        </SelectionModel>
                        <ColumnModel runat="server" ID="ColumnModel2">
                            <Columns>
                                <ext:Column runat="server" Sortable="false" MenuDisabled="true" Header="Name" Width="150"
                                    DataIndex="EmployeeName" />
                                <ext:Column runat="server" Sortable="false" MenuDisabled="true" Header="Leave" Width="120"
                                    DataIndex="Title" />
                                <ext:Column Sortable="false" MenuDisabled="true" Text="From" Width="100" DataIndex="From" />
                                <ext:Column ID="Column2" runat="server" Text="Date" DataIndex="To" MenuDisabled="false"
                                    Sortable="true" Align="Left" Width="100" />
                                <ext:Column ID="ColumnDaysorHours2" runat="server" Sortable="false" MenuDisabled="true"
                                    Align="Center" Text="Days" Width="55" DataIndex="DaysOrHours" />
                                <ext:Column runat="server" Sortable="false" MenuDisabled="true" Header="Verified by"
                                    Width="110" DataIndex="ApprovedBy" />
                                <ext:Column runat="server" Sortable="false" MenuDisabled="true" Header="Status" Width="80"
                                    DataIndex="Status" />
                                <ext:Column runat="server" Sortable="false" MenuDisabled="true" Header="Reason" Width="210"
                                    DataIndex="Reason" />
                                <ext:Column runat="server" Sortable="false" MenuDisabled="true" Header="Comment"
                                    Width="200" DataIndex="Comment" />
                                <ext:CommandColumn Width="60" ButtonAlign="Center" Hidden="false">
                                    <Commands>
                                        <ext:GridCommand Icon="ArrowRight" CommandName="More" Text="more  " />
                                    </Commands>
                                    <Listeners>
                                        <Command Handler="MoreHandler(record.data.LeaveRequestId)" />
                                        <%--<rowdblclic fn="ClickApprove1" />--%>
                                    </Listeners>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="pagingOne" runat="server" PageSize="50" DisplayInfo="true"
                                DisplayMsg="Displaying  Leaves {0} - {1} of {2}" EmptyMsg="No record to display" />
                        </BottomBar>
                        <%-- <loadmask showmask="true" />--%>
                    </ext:GridPanel>
                </Items>
            </ext:TabPanel>
            <ext:Button Width="120px" Visible="false" StyleSpec="margin-top:10px" Height="30px"
                runat="server" ID="btnApprove" runat="server" Text="Approve">
                <DirectEvents>
                    <Click OnEvent="btnApprove_Click">
                        <Confirmation ConfirmRequest="true" Message="Are you sure, you want to approve the selected leaves?" />
                        <EventMask ShowMask="true" />
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Hidden ID="hdStartDate" runat="server" />
            <ext:Hidden ID="hdEndDate" runat="server" />
        </div>
    </div>
    <uc1:FooterCtl ID="FooterCtl1" runat="server" />
    <%--    <uc4:LA Id="LA1" runat="server" />--%>
    <uc5:AssingLeave Id="AssignLeave" runat="server" />
    </form>
</body>
</html>
