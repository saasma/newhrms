﻿<%@ Page Title="Overtime Request" Language="C#" MasterPageFile="~/Master/ForEmployeePopupPage.Master" AutoEventWireup="true" CodeBehind="OvertimeRequestEditPopup.aspx.cs" Inherits="Web.Employee.OvertimeRequestEditPopup" %>

<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">

<script type="text/javascript">

    var isSaved = false;

    var skipLoadingCheck = true;

    function closePopup() {
        window.close();
        window.opener.refreshWindow();
    }


    </script>


</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="mainContent" runat="server">

<ext:ResourceManager ID="ResourceManager1" runat="server" />
    
<div class="popupHeader" style="margin-top:-10px; height:50px; padding-top:10px; padding-left: 20px; color:White;">
        <h4>
            Overtime Request</h4>
    </div>

    <div class=" marginal" style='margin-top: 0px'>
      
      <ext:Hidden ID="hdnRequestID" runat="server" />
       
    
     <ext:Panel ID="pnlBorder" runat="server" StyleSpec="width:450px; height:40px; border:solid 1px red; margin-left:30px; display:none; margin-top:10px;" Hidden="true">
            <Items>
                <ext:Label ID="lblWarningMsg" StyleSpec="color:red; padding-top:10px; padding-left:10px;" runat="server" />
         </Items>
        </ext:Panel>
        
            <table class="fieldTable" style="margin-left:20px;">

            <tr>
                <td>
                    <ext:ComboBox ID="cmbOvertimeType" runat="server" ValueField="OvertimeTypeId" DisplayField="Name" FieldLabel="Overtime Type *" Width="200"
                            LabelAlign="top" LabelSeparator="" ForceSelection="true" QueryMode="Local">
                            <Store>
                                <ext:Store ID="Store2" runat="server">
                                    <Model>
                                        <ext:Model ID="Model1" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="OvertimeTypeId" Type="String" />
                                                <ext:ModelField Name="Name" Type="String" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                        </ext:ComboBox>
                </td>
            </tr>
            <tr>
                <td>
                    <ext:DateField ID="txtDate" runat="server" FieldLabel="Date *" LabelSeparator="" Width="200" LabelAlign="Top" Format="yyyy/MM/dd" />
                </td>
            </tr>
            <tr>
                <td>
                     <ext:TimeField  ID="tfExtraWorkStartAt"  runat="server"  MinTime="6:00" MaxTime="22:00" Increment="1" Width="200" SelectedTime="09:00"
                            Format="hh:mm tt" FieldLabel="Extra Work Start at *" LabelAlign="Top" LabelSeparator="">
                        </ext:TimeField> 
                </td>
                <td>
                     <ext:TimeField  ID="tfExtraWorkEndAt"  runat="server"  MinTime="6:00" MaxTime="22:00" Increment="1" Width="200" SelectedTime="09:00"
                            Format="hh:mm tt" FieldLabel="Extra Work Ended at *" LabelAlign="Top" LabelSeparator="">
                        </ext:TimeField>
                </td>
                
            </tr>
            <tr>
                <td colspan="2">
                    <ext:TextArea ID="txtReason" runat="server" FieldLabel="Reason for Extra Work *" LabelSeparator=""
                            LabelAlign="Top" Rows="3" Width="410" Height="80" />
                </td>
            </tr>

             <tr>
                <td>
                    <ext:DisplayField ID="lblApplyTo" LabelSeparator="" LabelStyle="font-weight:bold"
                                LabelWidth="75" runat="server" FieldLabel="Approval">
                            </ext:DisplayField>
                </td>
            </tr>

            <tr>
               <td valign="bottom" colspan="2">
                        <div class="popupButtonDiv">
                    <ext:Button runat="server" ID="btnSave" Cls="btn btn-primary" Text="<i></i>Save">
                                <DirectEvents>
                                    <Click OnEvent="btnSave_Click">
                                        <EventMask ShowMask="true" />
                                    </Click>
                                </DirectEvents>
                                <Listeners>
                                    <Click Handler="valGroup = 'SaveOvertimeReq'; if(CheckValidation()) return this.disable(); else return false;">
                                    </Click>
                                </Listeners>
                            </ext:Button>
                            </div>
                    
                </td>
            </tr>
        

            </table>

        <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator5" ValidationGroup="SaveOvertimeReq"
            Display="None" ErrorMessage="Overtime type is required." ControlToValidate="cmbOvertimeType"></asp:RequiredFieldValidator>
          
        <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator4" ValidationGroup="SaveOvertimeReq"
            Display="None" ErrorMessage="Date is required." ControlToValidate="txtDate"></asp:RequiredFieldValidator>
            
        <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator2" ValidationGroup="SaveOvertimeReq"
            Display="None" ErrorMessage="Reason is required" ControlToValidate="txtReason"></asp:RequiredFieldValidator>

    </div>
</asp:Content>



<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
