﻿<%@ Page Title="Assign Overtime" Language="C#" MasterPageFile="~/Master/ForEmployeePopupPage.Master" AutoEventWireup="true" CodeBehind="AssignOvertimePopup.aspx.cs" Inherits="Web.Employee.AssignOvertimePopup" %>

<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript" src="<%= ResolveUrl("~/Employee/override.js?v=") + Web.Helper.WebHelper.Version %>"></script>

<script type="text/javascript">

    var addNewRow = function (grid) {
            var newRow = new SettingModel();

            newRow.data.ControlID = "";

            var rowIndex = grid.getStore().data.items.length;

            //gridProjectSplitter.stopEditing();
            grid.getStore().insert(rowIndex, newRow);
        }

        var RemoveItemLine = function (column, command, record, recordIndex, cellIndex) {
            var store = this.grid.store;
            store.remove(record);
        };

        var CalculateWorkHours = function(e1,e2,record)
        {
            if(record.data.InTime == null || record.data.OutTime == null)
            {
                return;
            }

            if(record.data.InTime != '' && record.data.OutTime != '')
            {
                var strTime = Ext.util.Format.date(record.data.InTime, 'g:i a')
                var eTime = Ext.util.Format.date(record.data.OutTime, 'g:i a')
                var diffTime = calculateTotalMinutes(eTime) - calculateTotalMinutes(strTime)
                if(diffTime <= 0)
                {
                    alert('End time must be greater than Start time.');
                    record.data.OutTime = "";
                    return "";
                }


                return (Math.floor(diffTime/60) + ":" + (diffTime%60));
              
            }
        };

       

        function calculateTotalMinutes(time)
        {
            var parts = time.split(/ |:/);
            var totalHours = 0;

            if(parts[2] == 'pm')
            {
               if(parseInt(parts[0], 10) == 12)
               {
                    return (12 * 60 + parseInt(parts[1], 10));
               }
               else
               {
                    return (12 * 60 + parseInt(parts[0], 10) * 60 + parseInt(parts[1], 10));
               }
            }
            else
            {
                return (parseInt(parts[0], 10) * 60 + parseInt(parts[1], 10));
            }
    
            //return (parts[2] == 'pm' * 12 * 60) + parseInt(parts[0], 10) * 60 + parseInt(parts[1], 10);
        }


        var EmpNameRender = function (value, metaData, record, rowIndex, colIndex, store) {
                    var r = <%= storeEmployees.ClientID %>.getById(value.toString().toLowerCase());

                    if (Ext.isEmpty(r)) {
                        return "";
                    }
                    return r.data.Text;
                };


    function ImportPopup()
    {
        var val = <%=cmbOvertimeType.ClientID %>.getValue();
        if(val == null || val == '')
        {
            alert('Please select overtime type.');
            return;
        }
        overtimeImp('OvertimeType=' + <%=cmbOvertimeType.ClientID %>.getRawValue());
    }

    var isSaved = false;

    function UpdateSavedStatus()
     {
        isSaved = true;
     }

    function refreshWindow()
    {
        <%=btnLoadGrid.ClientID %>.fireEvent('click');
    }
    
     var skipLoadingCheck = true;

    function closePopup() {
       window.close();
      window.opener.refreshWindow();
    }
    

</script>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="mainContent" runat="server">

<ext:ResourceManager ID="ResourceManager1" runat="server" />

<ext:LinkButton ID="btnLoadGrid" runat="server" Hidden="true">
    <DirectEvents>
        <Click OnEvent="btnLoadGrid_Click">
            <EventMask ShowMask="true" />
        </Click>
    </DirectEvents>
</ext:LinkButton>


 <ext:Store ID="storeEmployees" runat="server">
    <Model>
        <ext:Model ID="modelEmployees" IDProperty="Value" runat="server">
            <Fields>
                <ext:ModelField Name="Text" Type="String" />
                <ext:ModelField Name="Value" Type="String" />
            </Fields>
        </ext:Model>
    </Model>
</ext:Store>

<div class="popupHeader" style="margin-top:-10px; height:50px; padding-top:10px; padding-left: 20px; color:White;">
        <h4>
            Assign Overtime</h4>
    </div>

<div class=" marginal" style='margin-top: 0px'>

      <table class="fieldTable" style="margin-left:20px;">

            <tr>
                <td style="width:350px;">
                    <ext:ComboBox ID="cmbOvertimeType" runat="server" ValueField="OvertimeTypeId" DisplayField="Name" FieldLabel="Overtime Type *" Width="200"
                            LabelAlign="top" LabelSeparator="" ForceSelection="true" QueryMode="Local">
                            <Store>
                                <ext:Store ID="Store2" runat="server">
                                    <Model>
                                        <ext:Model ID="Model2" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="OvertimeTypeId" Type="String" />
                                                <ext:ModelField Name="Name" Type="String" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>                            
                        </ext:ComboBox>

                        <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ValidationGroup="AssignOvertime"
                            Display="None" ErrorMessage="Overtime type is required." ControlToValidate="cmbOvertimeType"></asp:RequiredFieldValidator>
                </td>

                <td>                     
                <%--Style='text-decoration: underline'
                    <ext:LinkButton runat="server" StyleSpec="padding:0px;" ID="btnImport" Cls="btn btn-primary"
                                        OnClientClick="ImportPopup();return false;" Text="<i></i>Excel import">                                      
                                    </ext:LinkButton>--%>

                <ext:LinkButton runat="server" StyleSpec="padding:0px;text-decoration: underline; font-size:20px;" ID="btnImport" 
                                        OnClientClick="ImportPopup();return false;" Text="<i></i>Import from Excel">                                      
                                    </ext:LinkButton>
                </td>
            </tr>
            </table>



             <table class="fieldTable" style="margin-left:20px;">
            <tr>
                <td>
                    <ext:GridPanel StyleSpec="margin-top:15px;" ID="gridOTList" runat="server" Cls="itemgrid"
                                Scroll="None" Width="850">
                                <Store>
                                    <ext:Store ID="Store6" runat="server">
                                        <Model>
                                            <ext:Model ID="OTModel" Name="SettingModel" runat="server" IDProperty="SN">
                                                <Fields>
                                                    <ext:ModelField Name="SN" Type="Int" />
                                                    <ext:ModelField Name="DateEng" Type="Date"/>        
                                                    <ext:ModelField Name="Value" Type="String" />
                                                    <ext:ModelField Name="Text" Type="String" />                                           
                                                    <ext:ModelField Name="InTime" Type="Date"/>
                                                    <ext:ModelField Name="OutTime" Type="Date" />                                             
                                                    <ext:ModelField Name="WorkHours" Type="string" />
                                                    <ext:ModelField Name="OutNote" Type="string" />
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <ColumnModel>
                                    <Columns>
                                        <ext:DateColumn ID="DateColumn1" runat="server" Align="Right" Text="Date" Width="100"
                                            MenuDisabled="true" Sortable="false" Format="yyyy-MM-dd" DataIndex="DateEng">
                                              <Editor>
                                                <ext:DateField  runat="server"
                                                    ID="dfDateAdd">
                                                    <Plugins>
                                                        <ext:GenericPlugin ID="HighlightDates1" runat="server" InstanceName="Ext.ux.DatePicker.HighlightWithNepDates" />
                                                    </Plugins>
                                                </ext:DateField>
                                              </Editor>
                                        </ext:DateColumn>                          

                                         <ext:Column ID="colEmployeeNameAdd" Sortable="false" MenuDisabled="true" runat="server" Text="Employee Name"
                                            Align="Left" Width="200" DataIndex="Value">
                                            <Renderer Fn="EmpNameRender" />
                                            <Editor>
                                                <ext:ComboBox ID="cmbEmployeeAdd" runat="server" DisplayField="Text" ValueField="Value" Width="200" StoreID="storeEmployees"
                                                   ForceSelection="true" QueryMode="Local">                                                    
                                                    <Items>
                                                        <ext:ListItem Text="--Select Employee--" Value="-1" />
                                                    </Items>
                                                    <SelectedItems>
                                                        <ext:ListItem Text="--Select Employee--" Value="-1" />
                                                    </SelectedItems>                            
                                                </ext:ComboBox>
                                            </Editor>
                                        </ext:Column>

                                        <ext:DateColumn ID="colInTime1" Sortable="false" MenuDisabled="true" runat="server" Text="Start Time"
                                            Align="Right" Width="100" DataIndex="InTime" Format="HH:mm">
                                            <Editor>
                                                <ext:TimeField ID="tfInTime" runat="server" MinTime="06:00" MaxTime="23:59" Increment="1" Format="HH:mm"
                                                    SelectedTime="08:00" >
                                                </ext:TimeField>
                                            </Editor>
                                           
                                        </ext:DateColumn>

                                        <ext:DateColumn ID="colOutTime1" Sortable="false" MenuDisabled="true" runat="server" Text="End Time"
                                            Align="Right" Width="100" DataIndex="OutTime" Format="HH:mm">                                                                                                                 
                                            <Editor>
                                                <ext:TimeField ID="tfOutTime" runat="server" MinTime="06:00" MaxTime="23:59" Increment="1" Format="HH:mm"
                                                    SelectedTime="08:00" >
                                                </ext:TimeField>
                                            </Editor>
                                           
                                        </ext:DateColumn>

                                        <ext:Column ID="colWorkHours" Sortable="false" MenuDisabled="true" runat="server"
                                            Text="Hours:Minutes" Align="Center" Width="120" DataIndex="WorkHours">
                                            <Renderer Fn="CalculateWorkHours" />
                                        </ext:Column>

                                        
                                        <ext:Column ID="colReasonAdd" Sortable="false" MenuDisabled="true" runat="server" Text="Reason"
                                            Align="Left" Width="200" DataIndex="OutNote">
                                            <Editor>
                                                <ext:TextField ID="txtOutNote" runat="server">
                                                </ext:TextField>
                                            </Editor>
                                        </ext:Column>

                                        <ext:CommandColumn ID="CommandColumn1" runat="server" Sortable="false" MenuDisabled="true"
                                            Width="30" Align="Center">
                                            <Commands>
                                                <ext:GridCommand Icon="Delete" CommandName="Delete" Cls="deleteicon" />
                                            </Commands>
                                            <Listeners>
                                                <Command Fn="RemoveItemLine">
                                                </Command>

                                            </Listeners>

                                        </ext:CommandColumn>

                                    </Columns>
                                </ColumnModel>
                                <SelectionModel>
                                    <ext:RowSelectionModel ID="RowSelectionModel2" runat="server" Mode="Single" />
                                </SelectionModel>
                                <Plugins>
                                    <ext:CellEditing ID="CellEditing1" runat="server" ClicksToEdit="1">
                                    </ext:CellEditing>
                                </Plugins>
                                <View>
                                    <ext:GridView ID="GridView2" runat="server">
                                        <Plugins>
                                            <ext:GridDragDrop ID="GridDragDrop1" runat="server" DragGroup="ddGroup" DropGroup="ddGroup" />
                                        </Plugins>
                                      
                                    </ext:GridView>
                                </View>
                            </ext:GridPanel>
                </td>
            </tr>

            <tr>
                <td>

                    <ext:Button ID="btnAddRow" runat="server" Cls="btn btn-save" Text="Add New Row" Width="130">
                        <Listeners>
                            <Click Handler="addNewRow(#{gridOTList});" />
                        </Listeners>
                    </ext:Button>
                </td>
                <td></td>
            </tr>
            </table>


              <table class="fieldTable" style="margin-left:20px;">

            <tr>
               <td valign="bottom" colspan="2">
                        <div class="popupButtonDiv">                            

                            <ext:Button runat="server" ID="btnAssignOvertime" Cls="btn btn-primary" Text="<i></i>Assign">
                                <DirectEvents>
                                    <Click OnEvent="btnAssignOvertime_Click">
                                        <EventMask ShowMask="true" />
                                        <ExtraParams>
                                            <ext:Parameter Name="gridItems" Value="Ext.encode(#{gridOTList}.getRowsValues({ selectedOnly: false }))"
                                                Mode="Raw" />
                                        </ExtraParams>
                                    </Click>
                                </DirectEvents>
                                <Listeners>
                                    <Click Handler="valGroup = 'AssignOvertime';">
                                    </Click>
                                </Listeners>
                            </ext:Button>      

                            </div>
                </td>
            </tr>

            </table>


</div>

</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
