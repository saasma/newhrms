﻿<%@ Page Title="Notice List" Language="C#" MasterPageFile="~/Master/EmployeeMS.Master"
    AutoEventWireup="true" CodeBehind="NoticeList.aspx.cs" Inherits="Web.Employee.NoticeList" %>

<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .details tr td
        {
            padding-top: 4px;
        }
        .leaveList
        {
            list-style-type: none; /*border: 2px solid #BFBFBF;
            background-color: #D0CECE;*/
            display: inline-block;
            padding: 10px;
        }
        .leaveList li
        {
            border: 1px solid #DEDEDE;
            background-color: #F2F2F2;
            float: left;
            padding: 5px;
            padding-left: 8px;
            padding-right: 8px;
            text-align: center;
        }
        
        .blockTitle
        {
            color: #0C6E92;
            text-transform: uppercase;
            font-size: 14px;
            font-weight: bold;
            padding-left: 2px;
        }
        .blockText
        {
            color: #305496;
        }
        .blockBottomBorder
        {
            border-bottom: 2px solid #D9D9D9;
            padding-bottom: 1px;
        }
        .weekBlock
        {
            background-color: #DEDEDE;
            display: inline-block;
            padding: 4px;
            margin-top: 10px;
            padding-left: 8px;
            padding-right: 50px;
            color: #333333;
            margin-left: 90px;
        }
        
        
        .submitAttendance
        {
            color: #ffffff;
            font-size: 14px;
            background: url('images/clock.png');
            background-position: left;
            background-repeat: no-repeat;
            background-color: #5CB85C;
            padding: 10px 5px 10px 20px;
            text-decoration: none;
            width: 100px;
            margin-right: 5px;
            margin-left: 5px;
            padding-left: 32px;
        }
        
        .submitAttendance:hover
        {
            color: #ffffff;
            font-size: 14px;
            background: url('images/clock.png');
            background-position: left;
            background-repeat: no-repeat;
            background-color: #71DB8E;
            padding: 10px 5px 10px 20px;
            text-decoration: none;
            width: 100px;
            margin-right: 5px;
            margin-left: 5px;
            padding-left: 32px;
        }
        
        
        .scheduleLeave
        {
            color: #ffffff;
            font-size: 14px;
            background: url('images/plane.png');
            background-position: left;
            background-repeat: no-repeat;
            background-color: #3498db;
            padding: 10px 5px 10px 20px;
            text-decoration: none;
            width: 100px;
            margin-right: 5px;
            margin-left: 5px;
            padding-left: 30px;
        }
        
        .scheduleLeave:hover
        {
            color: #ffffff;
            font-size: 14px;
            background: url('images/plane.png');
            background-position: left;
            background-repeat: no-repeat;
            background-color: #3cb0fd;
            padding: 10px 5px 10px 20px;
            text-decoration: none;
            width: 100px;
            margin-right: 5px;
            margin-left: 5px;
            padding-left: 30px;
        }
        
        .widget .widget-body.list ul li
        {
            line-height: inherit !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
    <ext:ResourceManager ID="ResourceManager1" runat="server" />
    <table>
        <tr>
            <td>
                <h2 style="margin-top: 25px;">
                    Notice Board
                </h2>
            </td>
        </tr>
        <tr>
            <td>
                <div style="margin-top: 15px;">
                    <div style="">
                        <ext:GridPanel ID="gridNoticeList" runat="server" Width="1200">
                            <Store>
                                <ext:Store runat="server" ID="storeNoticeList">
                                    <Model>
                                        <ext:Model>
                                            <Fields>
                                                <ext:ModelField Name="NoticeID" />
                                                <ext:ModelField Name="Title" />
                                                <ext:ModelField Name="PublishDate" Type="Date" />
                                                <ext:ModelField Name="FileName" />
                                                <ext:ModelField Name="URL" />
                                                <ext:ModelField Name="StatusStr" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                            <ColumnModel>
                                <Columns>
                                    <ext:DateColumn ID="DateColumn1" runat="server" Text="Publish Date" Width="100" DataIndex="PublishDate" />
                                    <ext:Column ID="Column1" runat="server" Text="Notice Title" Width="300" Flex="1"
                                        DataIndex="Title" />
                                    <%--<ext:Column ID="Column2"  runat="server" Text="Attachment" Width="200" DataIndex="FileName" />--%>
                                    <%--<ext:Column ID="Column3"  runat="server" Text="Status" Width="100" DataIndex="StatusStr" />--%>
                                    <ext:TemplateColumn ID="TemplateColumn1" runat="server" Text="Attachment" Width="200"
                                        DataIndex="NoticeID" TemplateString='<a href="../NoticeFileDownloader.ashx?id={URL}"> {FileName}</a>' />
                                    <ext:TemplateColumn ID="TemplateColumn3" runat="server" Text="Action" Width="100"
                                        DataIndex="NoticeID" TemplateString='<a href="NoticeDetail.aspx?id={NoticeID}"> View Detail</a>' />
                                </Columns>
                            </ColumnModel>
                        </ext:GridPanel>
                    </div>
                </div>
            </td>
        </tr>
    </table>
</asp:Content>
