﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL;
using BLL.Manager;
using Web.ReportDataSetTableAdapters;
using Web.Master;
using BLL;
using Web.CP.Report.Templates.Pay.Detail;
using Utils;
using BLL.Base;
using System.IO;

namespace Web.Employee
{
    public partial class PersonalDetails : BasePage
    {
      
        protected void Page_Load(object sender, EventArgs e)
        {


            if (CommonManager.CompanySetting.WhichCompany == WhichCompany.AceTravels)
            {
                Response.Redirect("~/Employee/EmployeeAAA/PersonalDetails.aspx");
            }

            if (!IsPostBack)
            {
                Initialise();
            }


            if (SessionManager.IsLDAPLogin == false)
            {
                // If not Active Directory then only show the Message
                UUser user = SessionManager.User;
                if (user != null && user.IsInitialPasswordChanged != null && !user.IsInitialPasswordChanged.Value)
                {
                    divWarningMsg.InnerHtml = Resources.Messages.ChangeDefaultPasswordMsg;
                    divWarningMsg.Hide = false;
                }
            }
        }

        private void Initialise()
        {
            CommonManager mgr = new CommonManager();

			// for all
            //if (CommonManager.CompanySetting.IsD2 == true || CommonManager.CompanySetting.WhichCompany==WhichCompany.PSI)
            {
                rowMedicalTax.Visible = false;
                rowGrade.Visible = false;
                payTable.Visible = false;
            }

            int empId = SessionManager.CurrentLoggedInEmployeeId;
            EEmployee emp = EmployeeManager.GetEmployeeById(empId);

            if (emp == null)
                return;

            bool hasPhoto = false;
            if (emp.HHumanResources.Count > 0)
            {
                if (!string.IsNullOrEmpty(emp.HHumanResources[0].UrlPhotoThumbnail))
                {
                    image.ImageUrl = Page.ResolveUrl(Config.UploadLocation + "/" + emp.HHumanResources[0].UrlPhotoThumbnail);
                    if (File.Exists(Server.MapPath(image.ImageUrl)))
                        hasPhoto = true;
                }
            }
            if (!hasPhoto)
            {
                image.ImageUrl = Page.ResolveUrl(Web.Helper.WebHelper.GetEmployeeNoImage(emp.EmployeeId));
                   
            }

            lblName.Text = emp.Title + " " + emp.Name;
            lblDesignation.Text = mgr.GetDesignationById(emp.DesignationId.Value).Name;
            if (emp.GradeIdLatest != null)
                lblGrade.Text = CommonManager.GetGradeById(emp.GradeIdLatest.Value).Name;


            lblDOJ.Text = emp.ECurrentStatus.OrderBy(x => x.FromDateEng).First().FromDate;
            lblPhoneNo.Text = emp.EAddresses[0].CIPhoneNo;
            //lblMobile.Text = emp.EAddresses[0].CIMobileNo;
            lblEmail.Text = emp.EAddresses[0].CIEmail;


            lblAddress.Text =
                emp.EAddresses[0].PSLocality;

            lblMaritalStatus.Text = emp.MaritalStatus;
            lblTaxStatus.Text =
                EmployeeManager.IsEmployeeSingle(emp.EmployeeId) ? "Single  " : "Couple  ";
            lblTaxStatus.Text += emp.IsHandicapped.Value ? CommonManager.GetHandicappedName : "";


            //status summary table
            EmployeeManager.SetStatusToDate(emp.ECurrentStatus);
            gvwStatusSummary.DataSource = emp.ECurrentStatus;
            gvwStatusSummary.DataBind();

            try
            {

                DisplayIncomeDeduction(emp);
                DisplayLoan(emp);
                DisplayOtherDetails(emp);
            }
            catch (Exception exp2)
            {
                Log.log("From Personal details display", exp2);
            }
        }

        void SetCIT(EEmployee emp, List<EmployeeIncomeListResult> incomeList)
        {
            PayManager mgr = new PayManager();

            OptimumPFAndCIT optimumPFAndCIT = PayManager.GetOptimumPFAndCIT(emp.EmployeeId);
            if (optimumPFAndCIT == null)
            {
                if (emp.EFundDetails[0].CITIsRate.HasValue && emp.EFundDetails[0].CITIsRate.Value)
                {
                    List<CITIncome> citIncomes = PayManager.GetCITIncomes();
                    // If not CITIncome set, then only use for Basic Salary
                    if (citIncomes.Count == 0)
                    {
                        decimal? basicAmount = incomeList.Where(x => x.IsBasicIncome == true).Select(x => x.Amount).SingleOrDefault();
                        if (basicAmount != null && basicAmount != 0)
                        {
                            lblCIT.Text = GetCurrency((emp.EFundDetails[0].CITRate / 100) * ((double)basicAmount));
                        }
                    }
                    else
                    {
                        decimal sum = 0;
                        PayrollPeriod payrollPeriod = CommonManager.GetLastPayrollPeriod();
                        if (payrollPeriod != null)
                        {
                            foreach (CITIncome entity in citIncomes)
                            {
                                PIncome income = new PayManager().GetIncomeById(entity.IncomeId.Value);
                                PEmployeeIncome empIncome = new PayManager().GetEmployeeIncome(emp.EmployeeId, entity.IncomeId.Value);

                                if (income == null || empIncome == null || income.IsEnabled == false || empIncome.IsEnabled == false)
                                    continue;

                                CalcGetIncomeValueResult result = PayManager.GetCalcuatedForIncome(emp.EmployeeId, entity.IncomeId.Value, payrollPeriod.PayrollPeriodId);
                                if (result != null)
                                {
                                    sum += (
                                        (result.AmountWithAdjustment == null ? 0 : result.AmountWithAdjustment.Value) +
                                        (result.PFIncome == null ? 0 : result.PFIncome.Value) +
                                        (result.RetrospectAmount == null ? 0 : result.RetrospectAmount.Value)
                                        );

                                    lblCIT.Text = GetCurrency((emp.EFundDetails[0].CITRate / 100) * ((double)sum));
                                }
                            }
                        }
                    }
                }
                else
                    lblCIT.Text = GetCurrency(emp.EFundDetails[0].CITAmount);
            }
            else
            {
                lblCIT.Text = GetCurrency(optimumPFAndCIT.AdjustedCIT);
            }
        }

        //display details like cit, insurance ...
        private void DisplayOtherDetails(EEmployee emp)
        {
            

            if (emp.IIndividualInsurances.Count > 0)
            {
                if (emp.IIndividualInsurances[0].Premium != null)
                    lblInsurance.Text = GetCurrency(emp.IIndividualInsurances[0].Premium.Value);
            }
            
                

            lblMedicalTaxCredit.Text = GetCurrency(TaxManager.GetAdjustableAmount(emp.EmployeeId));

            //total tax paid in current fiscal year
            PayrollPeriod lastPayroll =   CommonManager.GetLastPayrollPeriod();
            bool reading = false;
            int startingPayrollId = 0, endingPayrollId = 0;

            if (lastPayroll != null)
            {
                CalculationManager.GenerateForPastIncome(lastPayroll.PayrollPeriodId, ref reading, ref startingPayrollId, ref endingPayrollId);

                //if zero then start & ending are equal
                if (startingPayrollId == 0)
                    startingPayrollId = lastPayroll.PayrollPeriodId;

                lblTaxPaidThisYear.Text = GetCurrency(TaxManager.GetTotalTaxPaid(startingPayrollId, lastPayroll.PayrollPeriodId, emp.EmployeeId));
            }
        }

        public void DisplayIncomeDeduction(EEmployee emp)
        {
            decimal? incomePf = 0, deductionPf = 0;
            List<EmployeeIncomeListResult> incomeList =
                 PayManager.GetEmployeeIncomeListWithoutAnyDeduction(emp.EmployeeId,ref incomePf,ref deductionPf).ToList();

            SetCIT(emp, incomeList);

            gvwPayInformation.DataSource = incomeList;
            gvwPayInformation.DataBind();

            List<EmployeeDeductionListResult> deductions = PayManager.GetEmployeeDeductionListForPersonalDetails(emp.EmployeeId);

            if (deductionPf != null)
            {
                EmployeeDeductionListResult pf = new EmployeeDeductionListResult();
                pf.Title = "PF";
                pf.Amount = deductionPf ;

                deductions.Add(pf);

            }

            gvwDeductions.DataSource = deductions;
            gvwDeductions.DataBind();
        }

        public void DisplayLoan(EEmployee emp)
        {
            rptLoan.DataSource = PayManager.GetEmployeeLoanForPersonalDetails(emp.EmployeeId);
            rptLoan.DataBind();

            rptAdvance.DataSource = PayManager.GetEmployeeAdvanceForPersonalDetails(emp.EmployeeId);
            rptAdvance.DataBind();

        }

        public string GetStatus(object value)
        {

            return JobStatus.GetValueForDisplay((int)value);

        }
    }
}
