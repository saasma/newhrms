﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL;
using BLL.Manager;
using Web.ReportDataSetTableAdapters;
using Web.Master;
using BLL;
using Web.CP.Report.Templates.Pay.Detail;
using Utils;
using BLL.Base;
using System.IO;

namespace Web.Employee
{
    public enum CITType
    {
        NoCIT = 1,
        OptimumCIT = 2,
        CITAmount = 3,
        CITRate = 4
    }
    public partial class CITRequest : BasePage
    {

        protected void Page_PreRender(object sender, EventArgs e)
        {

            CITType type = (CITType)int.Parse(ddlType.SelectedValue);

            rowOptimum.Style["display"] = "none";
            rowRate.Style["display"] = "none";

            rowAmount.Style["display"] = "none";


            if (type == CITType.OptimumCIT)
            {
                rowOptimum.Style["display"] = "";
            }
            else if (type == CITType.CITAmount)
            {
                rowAmount.Style["display"] = "";
            }
            else if (type == CITType.CITRate)
            {
                rowRate.Style["display"] = "";
            }
        }
      
        protected void Page_Load(object sender, EventArgs e)
        {


           


            if (!IsPostBack)
            {
                Initialise();
            }


         

        }

        private void Initialise()
        {

            SetCIT();

        }

        void SetCIT()
        {
            EEmployee emp = new EmployeeManager().GetById(SessionManager.CurrentLoggedInEmployeeId);
            if (emp == null)
                return;

            
            decimal? incomePf = 0, deductionPf = 0;
            List<EmployeeIncomeListResult> incomeList =
                 PayManager.GetEmployeeIncomeListWithoutAnyDeduction(emp.EmployeeId, ref incomePf, ref deductionPf).ToList();


            int lastPeriodId = CommonManager.GetLastPayrollPeriod().PayrollPeriodId;

            int total = 0;
            List<GetEmployeeListForOptimumPFAndCITResult> optimum
                = PayManager.GetEmployeeListForOptimum(lastPeriodId, 0, 0, 10, ref total, -1, -1, emp.Name);
            foreach (GetEmployeeListForOptimumPFAndCITResult item in optimum)
            {
                if (item.EID == SessionManager.CurrentLoggedInEmployeeId)
                {
                    lblOptimumAmount.Text = GetCurrency(item.AdjustedCIT);
                }
            }



            PayManager mgr = new PayManager();
            bool hasCIT = false;

            OptimumPFAndCIT optimumPFAndCIT = PayManager.GetOptimumPFAndCIT(emp.EmployeeId);
            if (optimumPFAndCIT == null)
            {
                if (emp.EFundDetails[0].CITIsRate.HasValue && emp.EFundDetails[0].CITIsRate.Value)
                {
                    List<CITIncome> citIncomes = PayManager.GetCITIncomes();
                    // If not CITIncome set, then only use for Basic Salary
                    if (citIncomes.Count == 0)
                    {
                        decimal? basicAmount = incomeList.Where(x => x.IsBasicIncome == true).Select(x => x.Amount).SingleOrDefault();
                        if (basicAmount != null && basicAmount != 0)
                        {
                            hasCIT = true;
                            lblCITType.Text = "Rate - " + emp.EFundDetails[0].CITRate + "%";
                            lblCITAmount.Text = GetCurrency((emp.EFundDetails[0].CITRate / 100) * ((double)basicAmount));
                        }
                    }
                    else
                    {
                        decimal sum = 0;
                        PayrollPeriod payrollPeriod = CommonManager.GetLastPayrollPeriod();
                        if (payrollPeriod != null)
                        {
                            foreach (CITIncome entity in citIncomes)
                            {
                                PIncome income = new PayManager().GetIncomeById(entity.IncomeId.Value);
                                PEmployeeIncome empIncome = new PayManager().GetEmployeeIncome(emp.EmployeeId, entity.IncomeId.Value);

                                if (income == null || empIncome == null || income.IsEnabled == false || empIncome.IsEnabled == false)
                                    continue;

                                CalcGetIncomeValueResult result = PayManager.GetCalcuatedForIncome(emp.EmployeeId, entity.IncomeId.Value, payrollPeriod.PayrollPeriodId);
                                if (result != null)
                                {

                                    

                                    sum += (
                                        (result.AmountWithAdjustment == null ? 0 : result.AmountWithAdjustment.Value) +
                                        (result.PFIncome == null ? 0 : result.PFIncome.Value) +
                                        (result.RetrospectAmount == null ? 0 : result.RetrospectAmount.Value)
                                        );

                                    if (emp.EFundDetails[0].CITRate != null && emp.EFundDetails[0].CITRate != 0)
                                    {
                                        hasCIT = true;
                                        lblCITType.Text = "Rate - " + emp.EFundDetails[0].CITRate + "%";

                                        lblCITAmount.Text = GetCurrency((emp.EFundDetails[0].CITRate / 100) * ((double)sum));
                                    }
                                }
                            }
                        }
                    }
                }
                else if(emp.EFundDetails[0].CITAmount != null && emp.EFundDetails[0].CITAmount.Value!= 0)
                {
                    hasCIT = true;
                    lblCITType.Text = "Fixed Amount";
                    lblCITAmount.Text = GetCurrency(emp.EFundDetails[0].CITAmount);
                }
            }
            else
            {
                hasCIT = true;
                lblCITType.Text = "Optimum CIT";
                lblCITAmount.Text = GetCurrency(optimumPFAndCIT.AdjustedCIT);
            }


            if (hasCIT == false)
            {
                lblCITType.Text = "No CIT";
            }

            CITChangeRequest request = EmployeeManager.GetLastChangeRequest(SessionManager.CurrentLoggedInEmployeeId);

            if (request != null)
            {
                ddlType.SelectedValue = request.ChangeType.ToString();
                if (request.ChangeType == (int)CITType.CITAmount)
                    txtAmount.Text = GetCurrency(request.AmountOrRate);
                if (request.ChangeType == (int)CITType.CITRate)
                    txtRate.Text = GetCurrency(request.AmountOrRate);
            }

            // for d2 no fixed amount, only rate
            if (CommonManager.CompanySetting.IsD2)
            {
                ddlType.Items.RemoveAt(2);
            }
        }


        public void btnSaveAndSend_Click(object sender, EventArgs e)
        {

            CITChangeRequest request = new CITChangeRequest();
            request.ChangeType =int.Parse(ddlType.SelectedValue);
            if(request.ChangeType==(int)CITType.CITAmount)
            {
                 request.AmountOrRate = decimal.Parse(txtAmount.Text.Trim());
            }
            if (request.ChangeType == (int)CITType.CITRate)
            {
                request.AmountOrRate = decimal.Parse(txtRate.Text.Trim());
            }

            request.CreatedOn = DateTime.Now;
            request.EmployeeId = SessionManager.CurrentLoggedInEmployeeId;
            request.Status = (int)CITRequestStatus.SaveAndSend;

            EmployeeManager.SaveCITRequest(request);

            msgInfo.InnerHtml = "CIT change request sent.";
            msgInfo.Hide = false;
        }

    }
}
