<%@ Page Title="Approve Overtime" Language="C#" MasterPageFile="~/Master/EmployeeMS.Master"
    AutoEventWireup="true" CodeBehind="AApproveOvertime.aspx.cs" Inherits="Web.Employee.AApproveOvertime" %>

<%@ Import Namespace="BLL.Manager" %>
<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/PagingCtl.ascx" TagName="PagingCtl" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        var loadingButtonWhenSubmitting = false;
        var skipLoadingCheck = true;
        function addSkillSetToEmployeePopup(requestid) {
            var theText = requestid.getAttribute("value");
            positionHistoryPopup("isPopup=true&reqid=" + theText);
        }

        function refreshEventList(popupWindow) {
            if (typeof (popupWindow) != 'undefined')
                popupWindow.close();

            __doPostBack('<%=btnLoad.UniqueID %>', '');

        }

        function assignOvertime() {

            assignovertimePopup("isPopup=true&assign=true");
        }


        function selectDeselectAll(chk) {
            $('input[type=checkbox]').each(
                    function (index) {
                        if (this.id.indexOf('chkDelete') > 0)
                            this.checked = chk.checked;
                    }
                );
        }
   
    </script>
    <style type="text/css">
        .tableLightColor th
        {
            font-weight: normal !important;
        }
        .table td
        {
            background-color: White;
        }
        .x-form-date-trigger
        {
            margin-left: -5px !important;
        }
    </style>
    <script type="text/javascript" src="<%= ResolveUrl("~/Employee/override.js?v=") + Web.Helper.WebHelper.Version %>"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
    <ext:ResourceManager ID="ResourceManager1" DisableViewState="false" runat="server"
        ScriptMode="Release" />
    <div class="contentArea" style="margin-top: 10px">
        <div class="attribute" style="padding: 10px">
            <table>
                <tr>
                    <td>
                    </td>
                    <td style="padding-left: 10px;">
                        Period
                    </td>
                    <td style="padding-left: 15px;">
                        From
                    </td>
                    <td style="padding-left: 15px;">
                        To
                    </td>
                    <td style="padding-left: 10px;">
                        Branch
                    </td>
                    <td style="padding-left: 10px;">
                        Status
                    </td>
                    <td style="padding-left: 10px;">
                        Overtime Type
                    </td>
                    <td style="padding-left: 10px;">
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:LinkButton ID="btnOvertimRequest" OnClientClick="assignOvertime(); return false;"
                            CssClass="save" Style="height: 16px; width: 120px" runat="server" Text="Assign Overtime" />
                    </td>
                    <td style="padding-left: 10px;">
                        <asp:DropDownList ID="ddlType" runat="server">
                            <asp:ListItem Value="-1" Text="All" Selected="True" />
                            <asp:ListItem Value="1">This Week</asp:ListItem>
                            <asp:ListItem Value="2">This Month</asp:ListItem>
                            <asp:ListItem Value="3">Last Month</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td style="padding-left: 10px;">
                        <ext:DateField ID="calFrom" runat="server" Width="120px">
                            <Plugins>
                                <ext:GenericPlugin ID="HighlightDates1" runat="server" InstanceName="Ext.ux.DatePicker.HighlightWithNepDates" />
                            </Plugins>
                        </ext:DateField>
                    </td>
                    <td style="padding-left: 10px;">
                        <ext:DateField ID="calTo" runat="server" Width="120px">
                            <Plugins>
                                <ext:GenericPlugin ID="GenericPlugin1" runat="server" InstanceName="Ext.ux.DatePicker.HighlightWithNepDates" />
                            </Plugins>
                        </ext:DateField>
                    </td>
                    <td style="padding-left: 10px;">
                        <asp:DropDownList DataTextField="Name" DataValueField="BranchId" ID="ddlBranch" AppendDataBoundItems="true"
                            runat="server" Style="width: 150px; margin-right: 15px;">
                            <asp:ListItem Text="All" Value="-1" Selected="True" />
                        </asp:DropDownList>
                    </td>
                    <td style="padding-left: 10px;">
                        <asp:DropDownList ID="ddlStatus" runat="server">
                            <asp:ListItem Value="-1">All</asp:ListItem>
                            <asp:ListItem Value="0">Pending</asp:ListItem>
                            <asp:ListItem Value="1">Recommended</asp:ListItem>
                            <asp:ListItem Value="-2" Selected="True">Pending / Recommended</asp:ListItem>
                            <asp:ListItem Value="2">Approved</asp:ListItem>
                            <asp:ListItem Value="3">Rejected</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td style="padding-left: 10px;">
                        <asp:DropDownList DataTextField="Name" DataValueField="OvertimeTypeId" ID="ddlOvertimeType"
                            AppendDataBoundItems="true" runat="server" Style="width: 180px; margin-right: 25px;">
                            <asp:ListItem Text="All" Value="-1" Selected="True" />
                        </asp:DropDownList>
                    </td>
                    <td valign="top" style="padding-left: 10px;">
                        <asp:Button ID="btnLoad" CssClass="load" OnClientClick="valGroup='AEEmployee';return CheckValidation();"
                            OnClick="btnLoad_Click" runat="server" Text="Load" />
                    </td>
                </tr>
            </table>
            <%--</ContentTemplate>
            </asp:UpdatePanel>--%>
        </div>
        <uc2:MsgCtl ID="divMsgCtl" EnableViewState="false" Hide="true" Width="390px" runat="server" />
        <uc2:WarningCtl ID="divWarningMsg" EnableViewState="false" Hide="true" runat="server" />
        <div class="clear">
            <cc2:EmptyDisplayGridView CssClass="table table-primary mb30 table-bordered table-hover"
                UseAccessibleHeader="true" ShowHeaderWhenEmpty="True" ID="gvw" runat="server"
                DataKeyNames="RequestID" AutoGenerateColumns="False" CellPadding="4" GridLines="None"
                ShowFooterWhenEmpty="False" OnRowCreated="gvwEmployees_RowCreated" PagerStyle-CssClass="defaultPagingBar"
                PagerStyle-HorizontalAlign="Center" OnPageIndexChanged="gvwRoles_PageIndexChanged"
                OnPageIndexChanging="gvwRoles_PageIndexChanging">
                <RowStyle BackColor="#E3EAEB" />
                <Columns>
                    <asp:TemplateField HeaderText="Select" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                        <HeaderTemplate>
                            <asp:CheckBox ID="chk12Delete1" onclick="selectDeselectAll(this)" runat="server" />
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:CheckBox Visible='<%# CanRecommendOrApprove(int.Parse(Eval("Status").ToString()),int.Parse(Eval("EmployeeID").ToString()))%>'
                                ID="chkDelete" runat="server" />
                        </ItemTemplate>
                    </asp:TemplateField>
                
                    <asp:BoundField DataField="OvertimeType" HeaderStyle-Width="170px" HeaderStyle-HorizontalAlign="Left"
                        HeaderText="Overtime Type"></asp:BoundField>
                    <asp:BoundField DataField="Date" DataFormatString="{0:yyyy-MMM-dd}" HeaderStyle-Width="110"
                        ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Left" HeaderText="Date">
                    </asp:BoundField>
                    <asp:BoundField DataField="EmployeeName" HeaderStyle-Width="200px" HeaderStyle-HorizontalAlign="Left"
                        HeaderText="Employee Name"></asp:BoundField>
                    <asp:BoundField DataField="StartTime" HeaderStyle-Width="85px" HeaderStyle-HorizontalAlign="Left"
                        HeaderText="Start"></asp:BoundField>
                    <asp:BoundField DataField="EndTime" HeaderStyle-Width="85px" HeaderStyle-HorizontalAlign="Left"
                        HeaderText="End"></asp:BoundField>
                    <asp:BoundField DataField="DurationModified" HeaderStyle-Width="100px" HeaderStyle-HorizontalAlign="Left"
                        HeaderText="Requested Hour"></asp:BoundField>
                    <asp:BoundField DataField="ApprovedTime" HeaderStyle-Width="100px" HeaderStyle-HorizontalAlign="Center"
                        ItemStyle-HorizontalAlign="Center" HeaderText="Approved Hour"></asp:BoundField>
                    <asp:BoundField DataField="CheckInTime" HeaderStyle-Width="85px" HeaderStyle-HorizontalAlign="Left"
                        HeaderText="Device In"></asp:BoundField>
                    <asp:BoundField DataField="CheckOutTime" HeaderStyle-Width="85px" HeaderStyle-HorizontalAlign="Left"
                        HeaderText="Device Out"></asp:BoundField>
                    <%-- <asp:BoundField DataField="Position" HeaderText="Position"></asp:BoundField>--%>
                    <asp:BoundField DataField="Reason" HeaderStyle-Width="300px" HeaderStyle-HorizontalAlign="Left"
                        HeaderText="Reason"></asp:BoundField>
                    <%--  <asp:BoundField DataField="SupervisorName" Visible="false" HeaderStyle-Width="120px"
                        HeaderStyle-HorizontalAlign="Left" HeaderText="Supervisor Name"></asp:BoundField>--%>
                    <asp:BoundField DataField="StatusModified" HeaderStyle-Width="70px" HeaderStyle-HorizontalAlign="Left"
                        HeaderText="Status"></asp:BoundField>
                    <%--<asp:TemplateField HeaderStyle-Width="120px" HeaderStyle-HorizontalAlign="Left"   HeaderText="">
                        <ItemTemplate>
                               <%# "<a  href='#'  onclick='addSkillSetToEmployeePopup("  + Eval("RequestID")  + ")'>Action</a>"%>
                        </ItemTemplate>
                    </asp:TemplateField>--%>
                    <asp:TemplateField HeaderStyle-Width="50" HeaderStyle-HorizontalAlign="Left" HeaderText="">
                        <ItemTemplate>
                            <asp:HyperLink NavigateUrl="javascript:void(0)" value='<%# Eval("RequestID") %>'
                                onclick='<%# "addSkillSetToEmployeePopup(this);" %>' runat="server" Text="Action"
                                ID="btnAction">
                            </asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <%-- <%# "<a  href='#'  onclick='addSkillSetToEmployeePopup("+\x27 + Eval("RequestID")+ \x27 + ")'>Action</a>"%>--%>
                <PagerSettings Mode="NumericFirstLast" NextPageText="Next �" PreviousPageText="� Prev"
                    FirstPageText="First" LastPageText="Last" />
                <PagerStyle CssClass="defaultPagingBar" />
                <%--    <RowStyle CssClass="odd" />
                <AlternatingRowStyle CssClass="even" />--%>
                <EmptyDataTemplate>
                    <b>No list. </b>
                </EmptyDataTemplate>
            </cc2:EmptyDisplayGridView>
            <uc1:PagingCtl ID="pagintCtl" OnNextRecord="btnNext_Click" OnPrevRecord="btnPrevious_Click"
                OnDropDownShowRecordsChanged="ddlRecords_SelectedIndexChanged" runat="server" />
        </div>
        <div class="buttonsDiv">
            <asp:Button ID="btnRecommendOrApprove" Style="float: left; margin-left: 10px;" CssClass="createbtns"
                OnClick="btnRecommendOrApprove_Click" Visible="true" Width="125" Height="32px"
                CausesValidation="false" runat="server" OnClientClick="return confirm('Are you srue you want to Forward the selected requests?');"
                Text="Recommend" />
            <asp:LinkButton ID="LinkButton1" runat="server" Text="Export to Excel" OnClick="btnExport_Click"
                CssClass=" excel marginRight" Style="float: right;" />
        </div>
    </div>
</asp:Content>
