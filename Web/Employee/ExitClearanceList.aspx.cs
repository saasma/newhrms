﻿using BLL.Manager;
using Ext.Net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Web.Employee
{
    public partial class ExitClearanceList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Store_ReadData(object sender, StoreReadDataEventArgs e)
        {
            Store3.DataSource = CommonManager.GetAssignedResignationReq();
            Store3.DataBind();
        }
    }
}