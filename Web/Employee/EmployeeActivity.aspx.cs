﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL.Entity;
using BLL;
using System.IO;
using Utils;
using Utils.Calendar;

namespace Web.Employee
{
    public partial class EmployeeActivity : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();
            }
        }

        private void Initialise()
        {
            List<ClientList> listClients = ListManager.GetAllClients();

            List<Clients> clients = new List<Clients>();
            foreach (var item in listClients)
            {
                clients.Add(new Clients() { ClientSoftwareId = item.ClientId, Name = item.ClientName});
            }
            storeClient.DataSource = clients;
            storeClient.DataBind();

            List<ActivityType> listActivityType = ListManager.GetActivityTypes();
            List<ActivityTypes> activities = new List<ActivityTypes>();
            foreach (var item in listActivityType)
            {
                activities.Add(new ActivityTypes() { ActivityType = item.ActivityTypeId, Name= item.Name });
            }

            storeType.DataSource = activities;
            storeType.DataBind();

            if (CommonManager.CompanySetting.WhichCompany == WhichCompany.Sana)
            {
                ColClients.Hide();
                gridActivity.Width = 855;
            }
        }

        private void BindGrid()
        {
            List<NewActivityDetail> list = new List<NewActivityDetail>();

            EmployeePlan obj = NewHRManager.GetEmployeePlanByDate(DateTime.Now.Date , SessionManager.CurrentLoggedInEmployeeId);
            if (obj != null)
            {
                hdnPlanId.Text = obj.PlanId.ToString();

                List<EmployeePlanDetail> listEPD = obj.EmployeePlanDetails.ToList();

                int i = 1;
                foreach (var item in listEPD)
                {
                    NewActivityDetail objNewActivity = new NewActivityDetail()
                    {
                        SN = i,
                        Description = item.Description
                    };

                    i++;

                    list.Add(objNewActivity);
                }

                if (list.Count < 5)
                {
                    for (int j = i; j <= 5; j++)
                    {
                        NewActivityDetail objNA = new NewActivityDetail()
                        {
                            SN = j
                        };

                        list.Add(objNA);
                    }
                }
            }
            else
            {

                for (int i = 1; i <= 5; i++)
                {
                    NewActivityDetail objDetail = new NewActivityDetail()
                    {
                        SN = i,
                        Description = ""
                    };
                    list.Add(objDetail);
                }
            }

            gridActivityAdd.Store[0].DataSource = list;
            gridActivityAdd.Store[0].DataBind();

        }

        protected void btnAdd_Click(object sender, DirectEventArgs e)
        {
            EmpActCtrl1.AddActivity();

            //Clear();

            //hdnDetailId.Text = "";
            //hdnPlanId.Text = "";
            //txtActivityDate.SelectedDate = DateTime.Today;
            //btnSaveAndNew.Show();
            //btnSaveAndFinish.Show();
            //WEmpActivity.Center();
            //WEmpActivity.Show();
        }

        protected void txtDate_Change(object sender, DirectEventArgs e)
        {
            if (!string.IsNullOrEmpty(hdnIsNotDateChange.Text) && hdnIsNotDateChange.Text == "1")
                return;

            if (txtDate.SelectedDate != null && txtDate.SelectedDate != new DateTime())
            {             
                EmployeePlan obj = NewHRManager.GetEmployeePlanByDate(txtDate.SelectedDate, SessionManager.CurrentLoggedInEmployeeId);
                if (obj != null)
                {
                    hdnPlanId.Text = obj.PlanId.ToString();

                    List<NewActivityDetail> listNewActivityDetail = new List<NewActivityDetail>();
                    List<EmployeePlanDetail> list = obj.EmployeePlanDetails.ToList();

                    int i = 1;
                    foreach (var item in list)
                    {
                        NewActivityDetail objNewActivity = new NewActivityDetail()
                        {
                            SN = i,
                            Description = item.Description
                        };

                        i++;

                        listNewActivityDetail.Add(objNewActivity);
                    }

                    if (list.Count < 5)
                    {
                        for (int j = i; j <= 5; j++)
                        {
                            NewActivityDetail objNA = new NewActivityDetail()
                            {
                                SN = j
                            };

                            listNewActivityDetail.Add(objNA);
                        }
                    }

                    gridActivityAdd.Store[0].DataSource = listNewActivityDetail;
                    gridActivityAdd.Store[0].DataBind();
                }
                else
                {
                    BindEmptyPlanGrid();
                }                
            }
            else
            {
                BindEmptyPlanGrid();
            }
        }

        private void BindEmptyPlanGrid()
        {
            List<NewActivityDetail> listNewActivityDetail = new List<NewActivityDetail>();

            for (int j = 1; j <= 5; j++)
            {
                NewActivityDetail objNA = new NewActivityDetail()
                {
                    SN = j
                };

                listNewActivityDetail.Add(objNA);
            }

            gridActivityAdd.Store[0].DataSource = listNewActivityDetail;
            gridActivityAdd.Store[0].DataBind();
        }

        protected void btnSave_Click(object sender, DirectEventArgs e)
        {
            Page.Validate("SaveUpdActivity");
            if (Page.IsValid)
            {
                NewActivity obj = new NewActivity();

                if (!string.IsNullOrEmpty(hdnDetailId.Text))
                    obj.ActivityId = int.Parse(hdnDetailId.Text);

                if (!string.IsNullOrEmpty(hdnPlanId.Text))
                    obj.PlanId = int.Parse(hdnPlanId.Text);

                obj.EmployeeId = SessionManager.CurrentLoggedInEmployeeId;

                if (txtDate.SelectedDate != null && txtDate.SelectedDate != new DateTime())
                {
                    obj.DateEng = txtDate.SelectedDate;
                    obj.Date = BLL.BaseBiz.GetAppropriateDate(obj.DateEng.Value);
                }
                else
                {
                    NewMessage.ShowWarningMessage("Date is required.");
                    txtDate.Focus();
                    return;
                }

                string gridItemsJSON = e.ExtraParams["gridItems"];
                List<NewActivityDetail> list = JSON.Deserialize<List<NewActivityDetail>>(gridItemsJSON);

                List<NewActivityDetail> resultNewActivityDetailList = new List<NewActivityDetail>();

                foreach (var item in list)
                {
                    if (item.ClientSoftwareId != null && item.ClientSoftwareId != 0)
                    {
                        NewActivityDetail objDetail = new NewActivityDetail();
                        objDetail.ClientSoftwareId = item.ClientSoftwareId;

                        if (item.ActivityType == null)
                        {
                            NewMessage.ShowWarningMessage(string.Format("Type is required in row {0}.", item.SN));
                            return;
                        }

                        objDetail.ActivityType = item.ActivityType;

                        if (item.StartTimeDate == null || item.StartTimeDate == new DateTime())
                        {
                            NewMessage.ShowWarningMessage(string.Format("Start Time is required in row {0}.", item.SN));
                            return;
                        }

                        objDetail.StartTime = new TimeSpan(item.StartTimeDate.Hour, item.StartTimeDate.Minute, 0);

                        if (!string.IsNullOrEmpty(item.DurationTime))
                        {
                            
                            int hours = 0;
                            int minutes = 0;

                            string[] duration = item.DurationTime.Split(':');
                            if (duration.Count() > 1)
                            {
                                if (!int.TryParse(duration[0], out hours))
                                {
                                    NewMessage.ShowWarningMessage(string.Format("Invalid duration in row {0}.", item.SN));
                                    return;
                                }

                                if (!int.TryParse(duration[1], out minutes))
                                {
                                    NewMessage.ShowWarningMessage(string.Format("Invalid duration in row {0}.", item.SN));
                                    return;
                                }

                                if (minutes >= 60)
                                {
                                    NewMessage.ShowWarningMessage(string.Format("Invalid duration in row {0}.", item.SN));
                                    return;
                                }
                            }
                            else
                            {
                                if (!int.TryParse(duration[0], out hours))
                                {
                                    NewMessage.ShowWarningMessage(string.Format("Invalid duration in row {0}.", item.SN));
                                    return;
                                }
                            }

                            objDetail.EndTime = objDetail.StartTime.Value.Add(new TimeSpan(hours, minutes, 0));

                            if (objDetail.EndTime.Value.TotalHours >= 24)
                            {
                                NewMessage.ShowWarningMessage(string.Format("Invalid duration in row {0}.", item.SN));
                                return;
                            }

                            TimeSpan ts = objDetail.EndTime.Value - objDetail.StartTime.Value;
                            objDetail.Duration = double.Parse((ts.Hours).ToString() + "." + ts.Minutes.ToString());
                        }
                        else
                        {
                            NewMessage.ShowWarningMessage(string.Format("Duration is required in row {0}.", item.SN));
                            return;
                        }

                        if (!string.IsNullOrEmpty(item.Representative))
                            objDetail.Representative = item.Representative;

                        if (!string.IsNullOrEmpty(item.Description))
                            objDetail.Description = item.Description;

                        resultNewActivityDetailList.Add(objDetail);

                    }
                }

                if (resultNewActivityDetailList.Count == 0)
                {
                    NewMessage.ShowWarningMessage("Please add activity to be saved.");
                    return;
                }

                //obj.NewActivityDetails.AddRange(resultNewActivityDetailList);

                obj.Status = (int)AtivityStatusEnum.Draft;

                Status status = NewHRManager.SaveUpdateEmpActivity(obj);
                if (status.IsSuccess)
                {
                    if(!string.IsNullOrEmpty(hdnDetailId.Text))
                        NewMessage.ShowNormalMessage("Activity saved successfully.");
                    else
                        NewMessage.ShowNormalMessage("Activity udpated successfully.");

                    X.Js.Call("searchList");
                    WActivity.Close();
                }
                else
                    NewMessage.ShowWarningMessage(status.ErrorMessage);
            }
        }

        protected void btnEdit_Click(object sender, DirectEventArgs e)
        {
            EmpActCtrl1.EditActivity(int.Parse(hdnDetailId.Text));
        }

        protected void btnDelete_Click(object sender, DirectEventArgs e)
        {
            int detailId = int.Parse(hdnDetailId.Text);
            Status status = NewHRManager.DeleteNewActivityDetail(detailId);
            if (status.IsSuccess)
            {
                NewMessage.ShowNormalMessage("Activity deleted successfully.");
                X.Js.Call("searchList");
            }
            else
                NewMessage.ShowWarningMessage(status.ErrorMessage);
        }

        protected void Store_ReadData(object sender, StoreReadDataEventArgs e)
        {
            int totalRecords = 0;
            int clientId = -1;
            int activityTypeId = -1;
            DateTime? startDate = null, endDate = null;

            if (cmbClientFilter.SelectedItem != null && cmbClientFilter.SelectedItem.Value != null)
                clientId = int.Parse(cmbClientFilter.SelectedItem.Value);

            if (cmbTypeFilter.SelectedItem != null && cmbTypeFilter.SelectedItem.Value != null)
                activityTypeId = int.Parse(cmbTypeFilter.SelectedItem.Value);            

            int type = 0;
            bool all = false;

            if (TabPanel1.ActiveTabIndex == 0)
                type = 1;
            else if (TabPanel1.ActiveTabIndex == 1)
                type = 2;
            else if (TabPanel1.ActiveTabIndex == 2)
                type = 3;
            else if (TabPanel1.ActiveTabIndex == 3)
            {
                type = -1;
                all = true;

                if (txtFromDateFilter.SelectedDate != new DateTime())
                {
                    startDate = txtFromDateFilter.SelectedDate;

                    if (txtToDateFilter.SelectedDate == new DateTime())
                    {
                        NewMessage.ShowWarningMessage("To Date is required for date filter.");
                        txtToDateFilter.Focus();
                        return;
                    }
                }

                if (txtToDateFilter.SelectedDate != new DateTime())
                {
                    endDate = txtToDateFilter.SelectedDate;

                    if (txtFromDateFilter.SelectedDate == new DateTime())
                    {
                        NewMessage.ShowWarningMessage("From Date is required for date filter.");
                        txtFromDateFilter.Focus();
                        return;
                    }
                }
            }

            int pageSize = int.Parse(cmbPageSize.SelectedItem.Value); ;
            List<GetActivityListForEmployeeResult> list = NewHRManager.GetActivityForEmployee(e.Start / pageSize, pageSize, SessionManager.CurrentLoggedInEmployeeId, clientId, activityTypeId, type, startDate, endDate, all);

            if (list.Count > 0)
                totalRecords = list[0].TotalRows.Value;

            e.Total = totalRecords;

                storeActivity.DataSource = list;
            storeActivity.DataBind();

        }

        //[DirectMethod]
        //public static string GetGrid(Dictionary<string, string> parameters)
        //{
        //    int activityId = int.Parse(parameters["id"]);

        //    NewActivity obj = NewHRManager.GetNewActivityById(activityId);
        //    if (obj != null)
        //    {
        //        List<object> data = new List<object>();

        //        int i = 0;
        //        DateTime date = obj.DateEng.Value;

        //        foreach (var item in obj.NewActivityDetails)
        //        {
        //            i++;
        //            item.SN = i;
        //            item.StartTimeDate = new DateTime(date.Year, date.Month, date.Day, item.StartTime.Value.Hours, item.StartTime.Value.Minutes, 0);

        //            if(item.EndTime != null)
        //                item.EndTimeDate = new DateTime(date.Year, date.Month, date.Day, item.EndTime.Value.Hours, item.EndTime.Value.Minutes, 0);

        //            if (item.Duration != null)
        //            {
        //                string[] splString = item.Duration.Value.ToString().Split('.');
        //                if (splString.Count() > 1)
        //                    item.DurationTime = splString[0] + ":" + splString[1].PadLeft(2, '0');
        //                else
        //                    item.DurationTime = splString[0] + ":" + "00";
        //            }

        //            item.ClientName = ListManager.GetClientById(item.ClientSoftwareId.Value).ClientName;
        //            item.ActivityName = ListManager.GetActivityTypeById(item.ActivityType.Value).Name;

        //            data.Add(new {SN = i, Client = item.ClientName, ActType = item.ActivityName, StartTimeDate = item.StartTimeDate.ToString("HH:mm tt"),
        //                          DurationTime = item.DurationTime,
        //                          Representative = item.Representative,
        //                          Description = item.Description
        //            });
        //        }

        //        int count = obj.NewActivityDetails.Count();
        //        int height = 42 + 42 * count;
        //        int width = 1030;

        //        GridPanel grid = new GridPanel
        //        {
        //            Width = width,
        //            Scroll = ScrollMode.Vertical,
        //            Height = height,
        //            EnableColumnHide = false,
        //            Store = 
        //        { 
        //            new Store 
        //            { 
        //                Model = {
        //                    new Model {
        //                        IDProperty = "SN",
        //                        Fields = 
        //                        {
        //                            new ModelField("SN"),
        //                            new ModelField("Client"),
        //                            new ModelField("ActType"),
        //                            new ModelField("StartTimeDate"),
        //                            new ModelField("DurationTime"),
        //                            new ModelField("Representative"),
        //                            new ModelField("Description")
        //                        }
        //                    }
        //                },
        //                DataSource = data
        //            }
        //        },
        //            ColumnModel =
        //            {
        //                Columns = 
        //            { 
        //                new Column { Text = "SN", DataIndex = "SN", Width=10, Sortable = false, MenuDisabled = true, Hidden=true },
        //                new Column { Text = "Client", DataIndex = "Client", Width=150, Sortable = false, MenuDisabled = true },
        //                new Column { Text = "Type", DataIndex = "ActType", Width=150, Sortable = false, MenuDisabled = true },
        //                new Column { Text = "Start Time", DataIndex = "StartTimeDate", Width=90, Sortable = false, MenuDisabled = true, Align = Alignment.Center },
        //                new Column { Text = "Duration(h:m)", DataIndex = "DurationTime", Width=90, Sortable = false, MenuDisabled = true, Align = Alignment.Center },
        //                new Column { Text = "Person", DataIndex = "Representative", Width=200, Sortable = false, MenuDisabled = true, Wrap = true },
        //                new Column { Text = "Description", DataIndex = "Description", Width=350, Sortable = false, MenuDisabled = true, Wrap = true  }
        //            }
        //            }
        //        };

        //        return ComponentLoader.ToConfig(grid);
        //    }
        //    else
        //        return "";
        //}

        protected void btnView_Click(object sender, DirectEventArgs e)
        {
            int activityId = int.Parse(hdnDetailId.Text);
            NewActivity obj = NewHRManager.GetNewActivityById(activityId);

            if (obj != null)
            {
                txtDateView.SelectedDate = obj.DateEng.Value;
                txtDateView.Disable();

                int employeeId = obj.EmployeeId.Value;
                EEmployee emp = EmployeeManager.GetEmployeeById(employeeId);
                bool hasPhoto = false;
                if (emp.HHumanResources.Count > 0)
                {
                    if (!string.IsNullOrEmpty(emp.HHumanResources[0].UrlPhotoThumbnail))
                    {
                        Image1.ImageUrl = Page.ResolveUrl(Config.UploadLocation + "/" + emp.HHumanResources[0].UrlPhotoThumbnail);
                        if (File.Exists(Server.MapPath(img.ImageUrl)))
                            hasPhoto = true;
                    }
                }
                if (!hasPhoto)
                {
                    Image1.ImageUrl = Page.ResolveUrl(Web.Helper.WebHelper.GetEmployeeNoImage(employeeId));
                }

                dfNameView.Text = emp.Name;

                DateTime date = obj.DateEng.Value;
                int i = 0;
                //foreach (var item in obj.NewActivityDetails)
                //{
                //    i++;
                //    item.SN = i;
                //    item.StartTimeDate = new DateTime(date.Year, date.Month, date.Day, item.StartTime.Value.Hours, item.StartTime.Value.Minutes, 0);

                //    if (item.EndTime != null)
                //    {
                //        TimeSpan tsDiff = item.EndTime.Value - item.StartTime.Value;
                //        item.DurationTime = tsDiff.Hours + ":" + tsDiff.Minutes.ToString().PadLeft(2, '0');
                //    }
                    
                //    item.ActivityName = ListManager.GetActivityTypeById(item.ActivityType.Value).Name;
                //    item.ClientName = ListManager.GetClientById(item.ClientSoftwareId.Value).ClientName;
                //}

                //gridActivityView.Store[0].DataSource = obj.NewActivityDetails;
                //gridActivityView.Store[0].DataBind();

                List<ActivityComment> listActivityComments = new List<ActivityComment>();//obj.ActivityComments.ToList();
                foreach (var item in listActivityComments)
                {
                    item.Name = EmployeeManager.GetEmployeeById(UserManager.GetUserByUserID(item.CreatedBy.Value).UserMappedEmployeeId.Value).Name;
                }

                gridComments.Store[0].DataSource = listActivityComments;
                gridComments.Store[0].DataBind();

                if (listActivityComments.Count > 0)
                {
                    gridComments.Show();

                    ActivityEmpComment objActivityEmpComment = NewHRManager.GetActivityEmpComment(obj.ActivityId, SessionManager.CurrentLoggedInEmployeeId);
                    if (objActivityEmpComment != null)
                        txtEmpComment.Text = objActivityEmpComment.Comment;

                    txtEmpComment.Show();
                    btnSaveComment.Show();
                }
                else
                {
                    gridComments.Hide();
                    txtEmpComment.Hide();
                    btnSaveComment.Hide();
                }


                WActivityView.Center();
                WActivityView.Show();
            }
        }

        protected void btnSaveComment_Click(object sender, DirectEventArgs e)
        {
            Page.Validate("SaveActivityComment");
            if (Page.IsValid)
            {
                ActivityEmpComment obj = new ActivityEmpComment();
                obj.Comment = txtEmpComment.Text.Trim();
                obj.EmployeeId = SessionManager.CurrentLoggedInEmployeeId;
                obj.ActivityId = int.Parse(hdnDetailId.Text);

                if (obj.Comment.Length > 2000)
                {
                    NewMessage.ShowWarningMessage("Maximum comment lenght is 2000.");
                    txtEmpComment.Focus();
                    return;
                }

                Status status = NewHRManager.SaveUpdateEmployeeActivityComment(obj);
                if (status.IsSuccess)
                    NewMessage.ShowNormalMessage("Comment saved successfully.", "searchList");
                else
                    NewMessage.ShowWarningMessage("Errror while saving comment.");
            }
        }

       
    }

    public class Clients
    {
        public int ClientSoftwareId { get; set; }
        public string Name { get; set; }
    }

    public class ActivityTypes
    {
        public int ActivityType { get; set; }
        public string Name { get; set; }
    }
}