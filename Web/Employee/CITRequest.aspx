﻿<%@ Page Title="CIT Request" Language="C#" MasterPageFile="~/Master/EmployeeMS.Master"
    AutoEventWireup="true" CodeBehind="CITRequest.aspx.cs" Inherits="Web.Employee.CITRequest" %>

<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="InfoMsgCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .details
        {
            padding-top: 10px;
        }
        .details tr td
        {
            padding-top: 4px;
        }
        strong
        {
            margin-top: 10px;
            display: block;
        }
    </style>
    <script type="text/javascript">

        function typeChange(value) {
            var rowOptimum = document.getElementById('<%= rowOptimum.ClientID %>');
            var rowRate = document.getElementById('<%= rowRate.ClientID %>');
            var rowAmount = document.getElementById('<%= rowAmount.ClientID %>');

            rowOptimum.style.display = 'none';
            rowRate.style.display = 'none';
            rowAmount.style.display = 'none';

            // opt
            if (value == "2") {
                rowOptimum.style.display = '';
            }
            // rate
            else if (value == "3") {
                rowAmount.style.display = '';
            }
            // amount
            else if (value == "4") {
                rowRate.style.display = '';
            }
        }
    
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
    <div class="contentArea">
        <h3>
            CIT Change Request</h3>
        <uc2:InfoMsgCtl runat="server" Hide="true" EnableViewState="false" ID="msgInfo" />
        <uc2:WarningCtl ID="divWarningMsg" EnableViewState="false" Hide="true" runat="server" />
        <table style='width: 300px; clear: both' class="tableLightColor">
            <tr class="even">
                <th style="width: 146px;text-align:left">
                    Current CIT
                </th>
                <th style='text-align: right'>
                    Amount
                </th>
            </tr>
            <tr class="odd">
                <td style="width: 146px">
                    <asp:Label ID="lblCITType" runat="server" />
                </td>
                <td style='text-align: right'>
                    <asp:Label ID="lblCITAmount" runat="server" />
                </td>
            </tr>
        </table>
        <strong style="margin-top: 30px">How you want to contribute to CIT</strong><br />
        <asp:DropDownList ID="ddlType" runat="server" onchange="typeChange(this.value)" Width="158px">
            <asp:ListItem Text="--Select CIT Type--" Value="-1" />
            <asp:ListItem Text="No CIT" Value="1" />
            <%--<asp:ListItem Text="Optimum CIT" Value="2" />--%>
            <asp:ListItem Text="Fixed Amount" Value="3" />
            <%--<asp:ListItem Text="Rate" Value="4" />--%>
          
        </asp:DropDownList>
          <asp:RequiredFieldValidator Display="None" runat="server" ErrorMessage="CIT Type is required."
                        ID="RequiredFieldValidator4" ValidationGroup="SaveUpdate" InitialValue="-1" ControlToValidate="ddlType"></asp:RequiredFieldValidator>
        <table style='margin-top: 10px;'>
            <tr runat="server" id="rowOptimum" style="display: none;">
                <td style="padding-bottom: 10px;">
                    <strong>Optimum CIT Amount</strong>
                </td>
                <td>
                    <asp:Label runat="server" ID="lblOptimumAmount" />
                </td>
            </tr>
            <tr runat="server" id="rowRate" style="display: none">
                <td>
                    <strong>Rate</strong>
                </td>
                <td>
                    <asp:TextBox ID="txtRate" runat="server" />
                    <asp:RequiredFieldValidator Display="None" runat="server" ErrorMessage="Rate is required."
                        ID="RequiredFieldValidator2" ValidationGroup="SaveUpdate" ControlToValidate="txtRate"></asp:RequiredFieldValidator>
                    <asp:RangeValidator MinimumValue="0" Type="Double" Display="None" MaximumValue="100"
                        runat="server" ErrorMessage="Invalid rate range." ID="RequiredFieldValidator1"
                        ValidationGroup="SaveUpdate" ControlToValidate="txtRate"></asp:RangeValidator>
                </td>
            </tr>
            <tr runat="server" id="rowAmount" style="display: none">
                <td>
                    <strong>Fixed Amount</strong>
                </td>
                <td>
                    <asp:TextBox ID="txtAmount" Height="28px" runat="server" />
                    <asp:RequiredFieldValidator Display="None" runat="server" ErrorMessage="Amount is required."
                        ID="RequiredFieldValidator3" ValidationGroup="SaveUpdate" ControlToValidate="txtAmount"></asp:RequiredFieldValidator>
                    <asp:CompareValidator Display="None" Type="Currency" Operator="DataTypeCheck" runat="server"
                        ErrorMessage="Invalid amount." ID="RangeValidator1" ValidationGroup="SaveUpdate"
                        ControlToValidate="txtRate"></asp:CompareValidator>
                </td>
            </tr>
        </table>
        <div id="btnDiv" class="buttonsDiv" style="float: left; margin-top: 20px;" runat="server">
            <asp:Button Text="Save and Send" Width="120px" ID="btnSaveAndSend" Style='float: left;
                margin-left: 10px;' CssClass="update" OnClientClick='valGroup="SaveUpdate";return CheckValidation();'
                runat="server" OnClick="btnSaveAndSend_Click" />
        </div>
    </div>
</asp:Content>
