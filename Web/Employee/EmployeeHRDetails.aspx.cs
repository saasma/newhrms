using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using BLL;
using BLL.Manager;
using Utils.Web;
using Utils.Helper;
using DAL;
using BLL.Base;
using System.Collections.Generic;
using Web.CP.Report;
using Utils;
using System.IO;

namespace Web.Employee
{
    public partial class EmployeeHRDetails : BasePage
    {
        CommonManager commonMgr = new CommonManager();
        HRManager hrMgr = new HRManager();
        EEmployee employee = null;
        EmployeeManager empMgr = new EmployeeManager();

        public int GetEmployeeId()
        {
            return
                SessionManager.CurrentLoggedInEmployeeId;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            RegisterEmployeeIDInScript(0);
            LoadDynamicLists();

            if (!IsPostBack)
            {
                Initialise();

                if (CommonManager.CompanySetting.IsD2)
                    rowCaste.Visible = false;
            }

            AttachJSCode();
            ucHRDetails.DisplayDocuments(GetEmployeeId());
          //  SetPagePermission();
        }
      
        void RegisterEmployeeIDInScript(int empId)
        {
            empId = GetEmployeeId();

            if (empId != 0)
            {
                employee = EmployeeManager.GetEmployeeById(empId);
                if (employee != null)
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "EmpIDKey", "\nEmployeeId=" + empId + ";", true);
                //else
                //{
                //    JavascriptHelper.DisplayClientMsg("Employee with the specified Id does not exists.", Page, "window.location='ManageEmployee.aspx';");
                //}
            }
        }

        void AttachJSCode()
        {
            

            JavascriptHelper.AttachPopUpCode(Page, "castePopup", "PP/ManageCaste.aspx", 486, 230);

            JavascriptHelper.AttachPopUpCode(Page, "skillSetPopup", "PP/ManageSkillSet.aspx", 700, 660);

            JavascriptHelper.AttachNonDialogPopUpCode(Page, "addSkillSetToEmployee", "../CP/PP/AddSkillSetToEmployee.aspx", 700, 660);
        }

        void LoadSkillSets()
        {
            //chkListSkillSets.DataSource = new CommonManager().GetAllSkillSet();
            //chkListSkillSets.DataBind();
        }

        protected void dob_Changed(object sender, EventArgs e)
        {
            if (sender == dobBS)
                calDOB.IsEnglishCalendar = false;
            else
                calDOB.IsEnglishCalendar = true;
        }

        void Initialise()
        {
            int employeeId = GetEmployeeId();
            if (employee == null)
                return;

            calDOB.IsEnglishCalendar = IsEnglish;

            LoadSkillSets();

            txtName.Text = employee.Name;

            ddlBloodGroups.DataSource = commonMgr.GetAllBloodGroups();
            ddlBloodGroups.DataBind();

            ddlQualification.DataSource = commonMgr.GetAllQualifications();
            ddlQualification.DataBind();

            HHumanResource entity = hrMgr.GetHumanResource(employeeId);
            List<SkillSetEmployee> skillSets = CommonManager.GetEmployeeSkillSet(employeeId);
            EAddress add = null;
            if (entity != null)
            {

                Process(ref entity,  ref add);
                ucHRDetails.Process(entity, true);
            }
            ucEducation.DisplayEducation(employeeId);
            ucActivity.DisplayCurriculum(employeeId);
            ucFamily.DisplayFamily(employeeId);
            ucDiscipline.DisplayDiscipline(employeeId);
            // ucEvaluation.DisplayEvaluation(employeeId);
            ucAccident.DisplayAccident(employeeId);
            ucTraining.DisplayTraining(employeeId);
            ucSpecialEvent.DisplaySpecialEvent(employeeId);
            ucPreviousEmployment.DisplayPreviousEmployment(employeeId);
            divInnerSkillSet.InnerHtml = CommonManager.GetEmployeeSkillSetHTML(employeeId);
            EAddress empAddress = null;
            if (employee != null && employee.EAddresses.Count > 0)
            {
                empAddress = employee.EAddresses[0];

                empAddressAndContact.Process(empAddress);

                //if AD/BS selected
                if (employee.IsEnglishDOB.HasValue)
                {
                    calDOB.IsEnglishCalendar = employee.IsEnglishDOB.Value;
                }
                else
                {
                    calDOB.IsEnglishCalendar = IsEnglish;
                }

                if (calDOB.IsEnglishCalendar)
                {
                    dobAD.Checked = true;
                    dobBS.Checked = false;
                }
                else
                {
                    dobAD.Checked = false;
                    dobBS.Checked = true;
                }

                //emp.DateOfBirth = calDOB.SelectedDate.ToString();

                if (string.IsNullOrEmpty(employee.DateOfBirth) == false)
                    calDOB.SetSelectedDate(employee.DateOfBirth, calDOB.IsEnglishCalendar);
            }

          
        }

        void LoadDynamicLists()
        {
            LoadCaste();
        }

        void LoadCaste()
        {
            string casteId = Request.Form[ddlCastes.UniqueID];  
            ddlCastes.DataSource = commonMgr.GetAllCastes();
            ddlCastes.DataBind();
            if (casteId != null)
                UIHelper.SetSelectedInDropDown(ddlCastes, casteId);      
        }

        void Process(ref HHumanResource entity,ref EAddress empAddress)
        {
            if (entity == null)
            {
                entity = new HHumanResource();

                if (ddlBloodGroups.SelectedValue != "-1")
                    entity.BloodGroup = ddlBloodGroups.SelectedItem.Text;
                else
                    entity.BloodGroup = null;

                if (ddlCastes.SelectedValue == "-1")
                    entity.CasteId = null;
                else
                    entity.CasteId = int.Parse(ddlCastes.SelectedValue);

                if (ddlQualification.SelectedValue == "-1")
                    entity.Qualification = null;
                else
                    entity.Qualification = ddlQualification.SelectedItem.Text;


                //employeeSkillSetList = new List<SkillSetEmployee>();
                //foreach (ListItem item in chkListSkillSets.Items)
                //{
                //    if (item.Selected)
                //    {
                //        employeeSkillSetList.Add(
                //            new SkillSetEmployee { EmployeeId= employee.EmployeeId,SkillSetId = int.Parse(item.Value) });
                //    }
                //}


                entity.Languages = txtLanguages.Text.Trim();

                ucHRDetails.Process(entity, false);

                empAddress = empAddressAndContact.Process(null);
            }
            else
            {
                UIHelper.SetSelectedInDropDownFromText(ddlBloodGroups, entity.BloodGroup);
                UIHelper.SetSelectedInDropDown(ddlCastes, entity.CasteId);
                UIHelper.SetSelectedInDropDownFromText(ddlQualification, entity.Qualification);
                //txtMajorSubjects.Text = entity.MajorSubjects;
                txtLanguages.Text = entity.Languages;

                ucHRDetails.Process(entity, true);

                //foreach (SkillSetEmployee skillItem in employeeSkillSetList)
                //{
                //    ListItem item = chkListSkillSets.Items.FindByValue(skillItem.SkillSetId.ToString());
                //    if (item != null)
                //        item.Selected = true;
                //}
            }               
            
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            Web.CP.Report.Templates.HR.ReportOtherEmployeeDetails report1 = new Web.CP.Report.Templates.HR.ReportOtherEmployeeDetails();
            OtherEmployeeDetails page = new OtherEmployeeDetails();

            EEmployee emp = EmployeeManager.GetEmployeeById(SessionManager.CurrentLoggedInEmployeeId);


            try
            {
                page.SetGeneralInfo(emp, report1);
                page.SetAddress(emp, report1);
                page.SetHRInformation(emp, report1);
                page.SetQualification(emp, report1);
                page.SetIncomes(emp, report1);
            }
            catch (Exception exp1)
            {
                Log.log("Other emp details error for employee id : " + emp.EmployeeId, exp1);
            }


            using (MemoryStream stream = new MemoryStream())
            {
                report1.ExportToPdf(stream);
                HttpContext.Current.Response.Clear();
                HttpContext.Current.Response.ContentType = "application/pdf";
                HttpContext.Current.Response.AppendHeader("Content-Type", "application/pdf");
                HttpContext.Current.Response.AddHeader("Content-Length", stream.ToArray().Length.ToString());
                HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment; filename=HRDetails" + ".pdf");
                HttpContext.Current.Response.OutputStream.Write(stream.ToArray(), 0, stream.ToArray().Length);
                HttpContext.Current.Response.End();
            }

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            HHumanResource entity = null;
            //List<SkillSetEmployee> skillSets = null;
            EAddress address = null;
            Process(ref entity, ref address);

            entity.EmployeeId = GetEmployeeId();

            bool IsEnglishDOB; string DateOfBirth;
            if (dobAD.Checked)
                IsEnglishDOB = true;
            else
                IsEnglishDOB = false;
            DateOfBirth = calDOB.SelectedDate.ToString();

            hrMgr.SaveOrUpdateEmployeeHRDetails(entity, address, IsEnglishDOB, DateOfBirth);

            divInnerSkillSet.InnerHtml = CommonManager.GetEmployeeSkillSetHTML(GetEmployeeId());

            msgInfo.InnerHtml = "HR details saved.";
            msgInfo.Hide = false;
            
        }
    }
}
