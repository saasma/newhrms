﻿<%@ Page Title="Employee Plan" Language="C#" MasterPageFile="~/Master/EmployeeExt.Master"
    AutoEventWireup="true" CodeBehind="EmpPlan.aspx.cs" Inherits="Web.Employee.EmpPlan" %>

<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">

    
      var CommandHandler = function(command, record){
            <%= hdnPlanId.ClientID %>.setValue(record.data.PlanId);
                if(command=="Edit")
                {
                    <%= btnEdit.ClientID %>.fireEvent('click');
                }
                else
                {
                    <%= btnDelete.ClientID %>.fireEvent('click');
                }

           }

           var prepareActivity = function (grid, toolbar, rowIndex, record) {
                var editButton = toolbar.items.get(1);
                var deleteButton = toolbar.items.get(3);
                if (record.data.IsEditable == 0) {
                    editButton.setVisible(false);
                    deleteButton.setVisible(false);
                }

            } 


        function searchList() {
            <%=gridPlanList.ClientID %>.getStore().pageSize = <%=cmbPageSize.ClientID %>.getValue();
            <%=PagingToolbar1.ClientID %>.doRefresh();
        }

        var RemoveItemLine = function (column, command, record, recordIndex, cellIndex) {
            var store = this.grid.store;
            store.remove(record);
        };

        window.addEventListener('keydown', function(e) {
          var key = e.keyCode || e.which;
          if (key == 8 /*BACKSPACE*/ || key == 46/*DELETE*/) {
            var len=window.location.href.length;
            if(window.location.href[len-1]!='#') window.location.href += "#";
          }
        },false);

    </script>
    <style type="text/css">
        .bottomBlock
        {
            clear: both;
            padding: 0 0 10px 10px;
            background: #D7E7FF;
            float: left;
            width: 1160px;
            margin-left: -5px;
            margin-top: 20px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="mainContent" runat="server">
    <script type="text/javascript" src="<%= ResolveUrl("~/Employee/override.js?v=") + Web.Helper.WebHelper.Version %>"></script>
    <ext:Hidden ID="hdnPlanId" runat="server" />
    <ext:LinkButton runat="server" Hidden="true" ID="btnEdit">
        <DirectEvents>
            <Click OnEvent="btnEdit_Click">
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:LinkButton>
    <ext:Button ID="btnDelete" runat="server" Hidden="true">
        <DirectEvents>
            <Click OnEvent="btnDelete_Click">
                <Confirmation ConfirmRequest="true" Message="Are you sure, you want to delete the record?" />
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:Button>
    <div class="innerLR1">
        <h3 style="margin-top: 10px">
            My Daily Plans
        </h3>
        <br />
        <table>
            <tr>
                <td style="width: 140px;">
                    <ext:DateField ID="txtFromDateFilter" runat="server" Width="120px" LabelAlign="Top"
                        FieldLabel="From Date" LabelSeparator="">
                        <Plugins>
                            <ext:GenericPlugin ID="HighlightDates1" runat="server" InstanceName="Ext.ux.DatePicker.HighlightWithNepDates" />
                        </Plugins>
                    </ext:DateField>
                </td>
                <td style="width: 140px;">
                    <ext:DateField ID="txtToDateFilter" runat="server" Width="120px" LabelAlign="Top"
                        FieldLabel="To Date" LabelSeparator="">
                        <Plugins>
                            <ext:GenericPlugin ID="GenericPlugin1" runat="server" InstanceName="Ext.ux.DatePicker.HighlightWithNepDates" />
                        </Plugins>
                    </ext:DateField>
                </td>
                <td style="padding-top: 12px;">
                    <ext:Button runat="server" ID="btnLoad" Cls="btn btn-primary" Text="<i></i>Load">
                        <Listeners>
                            <Click Fn="searchList">
                            </Click>
                        </Listeners>
                    </ext:Button>
                </td>
            </tr>
        </table>
        <br />
        <ext:Button runat="server" ID="btnAdd" Cls="btn btn-success" Text="<i></i>Add New Plan">
            <DirectEvents>
                <Click OnEvent="btnAdd_Click">
                    <EventMask ShowMask="true" />
                </Click>
            </DirectEvents>
        </ext:Button>
        <br />
        <ext:TabPanel ID="TabPanel1" runat="server" Width="1100">
            <Items>
                <ext:Panel ID="panelThisWeek" runat="server" Title="This Week">
                    <Content>
                    </Content>
                </ext:Panel>
                <ext:Panel ID="panelLastWeek" runat="server" Title="Last Week">
                    <Content>
                    </Content>
                </ext:Panel>
                <ext:Panel ID="panelLastMonth" runat="server" Title="This Month">
                    <Content>
                    </Content>
                </ext:Panel>
                <ext:Panel ID="panelAll" runat="server" Title="All">
                    <Content>
                    </Content>
                </ext:Panel>
            </Items>
            <Listeners>
                <TabChange Fn="searchList" />
            </Listeners>
        </ext:TabPanel>
        <ext:GridPanel StyleSpec="margin-top:15px;" ID="gridPlanList" runat="server" Cls="itemgrid"
            Scroll="None" Width="1050">
            <Store>
                <ext:Store ID="storePlanList" runat="server" AutoLoad="true" OnReadData="Store_ReadData"
                    RemotePaging="true" RemoteSort="true">
                    <Proxy>
                        <ext:PageProxy />
                    </Proxy>
                    <Model>
                        <ext:Model ID="Model6" runat="server" IDProperty="PlanId">
                            <Fields>
                                <ext:ModelField Name="PlanId" Type="String" />
                                <ext:ModelField Name="DateEng" Type="Date" />
                                <ext:ModelField Name="IsEditable" Type="Int" />
                                <ext:ModelField Name="StatusName" Type="String" />
                                <ext:ModelField Name="DetailCount" Type="Int" />
                            </Fields>
                        </ext:Model>
                    </Model>
                </ext:Store>
            </Store>
            <ColumnModel>
                <Columns>
                    <ext:DateColumn ID="DateColumn4" runat="server" Align="Left" Text="Date" Width="150"
                        MenuDisabled="true" Sortable="false" Format="yyyy/MM/dd" DataIndex="DateEng">
                    </ext:DateColumn>
                    <ext:Column ID="Column19" Sortable="false" MenuDisabled="true" runat="server" Text="No of Plans"
                        Width="700" Align="Center" DataIndex="DetailCount">
                    </ext:Column>
                    <ext:CommandColumn ID="CommandColumn5" runat="server" Width="200" Text="Action" Align="Center">
                        <Commands>
                            <ext:CommandSeparator />
                            <ext:GridCommand Cls="editGridButton" ToolTip-Text="Edit" Text="<i class='fa fa-pencil'></i>"
                                CommandName="Edit" />
                            <ext:CommandSeparator />
                            <ext:GridCommand Cls="editGridButton" ToolTip-Text="Delete" Text="<i class='fa fa-trash-o'></i>"
                                CommandName="Delete" />
                        </Commands>
                        <Listeners>
                            <Command Handler="CommandHandler(command,record);" />
                        </Listeners>
                        <PrepareToolbar Fn="prepareActivity" />
                    </ext:CommandColumn>
                </Columns>
            </ColumnModel>
            <SelectionModel>
                <ext:RowSelectionModel ID="RowSelectionModel5" runat="server" Mode="Single" />
            </SelectionModel>
            <Plugins>
                <ext:RowExpander ID="RowExpander1" runat="server">
                    <Loader ID="Loader1" runat="server" DirectMethod="#{DirectMethods}.GetGrid" Mode="Component">
                        <LoadMask ShowMask="true" />
                        <Params>
                            <ext:Parameter Name="id" Value="this.record.getId()" Mode="Raw" />
                        </Params>
                    </Loader>
                </ext:RowExpander>
            </Plugins>
            <BottomBar>
                <ext:PagingToolbar Height="40" ID="PagingToolbar1" runat="server" StoreID="storePlanList"
                    DisplayInfo="true">
                    <Items>
                        <ext:Label ID="lblPageSize" runat="server" Text="Page size:" />
                        <ext:ToolbarSpacer ID="ToolbarSpacer_listItems" runat="server" Width="10" />
                        <ext:ComboBox ID="cmbPageSize" runat="server" Width="80" SelectOnFocus="true" Selectable="true"
                            ValueField="Value" DisplayField="Text" ForceSelection="true" AllowBlank="false">
                            <Listeners>
                                <Select Handler="searchList()" />
                            </Listeners>
                            <Items>
                                <ext:ListItem Value="20" Text="20" />
                                <ext:ListItem Value="50" Text="50" />
                                <ext:ListItem Value="100" Text="100" />
                            </Items>
                            <SelectedItems>
                                <ext:ListItem Index="0">
                                </ext:ListItem>
                            </SelectedItems>
                        </ext:ComboBox>
                    </Items>
                </ext:PagingToolbar>
            </BottomBar>
        </ext:GridPanel>
        <br />
    </div>
    <ext:Window ID="wPlan" runat="server" Title="My Plan" Icon="Application" Resizable="false"
        Width="1160" Height="360" BodyPadding="5" Hidden="true" Modal="true">
        <Content>
            <table style="margin-left: 20px; margin-top: 20px;">
                <tr>
                    <td style="width: 850px;">
                        <ext:DateField ID="txtDate" runat="server" Width="120" LabelAlign="Top" FieldLabel=""
                            LabelSeparator="">
                            <Plugins>
                                <ext:GenericPlugin ID="GenericPlugin2" runat="server" InstanceName="Ext.ux.DatePicker.HighlightWithNepDates" />
                            </Plugins>
                        </ext:DateField>
                        <asp:RequiredFieldValidator Display="None" ID="rfvDateCV" runat="server" ValidationGroup="SaveUpdPlan"
                            ControlToValidate="txtDate" ErrorMessage="Date is required." />
                    </td>
                    <td>
                        <ext:Image ID="img" runat="server" Width="30" Height="30" Cls="img-circle" />
                    </td>
                    <td>
                        <ext:DisplayField ID="dfName" runat="server" />
                    </td>
                </tr>
            </table>
            <ext:GridPanel StyleSpec="margin-top:15px; margin-left:20px;" ID="gridPlan" runat="server"
                Cls="itemgrid" Scroll="Vertical" Width="1100">
                <Store>
                    <ext:Store ID="storePlan" runat="server">
                        <Model>
                            <ext:Model ID="modelPlan" Name="SettingModel" runat="server" IDProperty="SN">
                                <Fields>
                                    <ext:ModelField Name="SN" Type="Int" />
                                    <ext:ModelField Name="Description" Type="String" />
                                    <ext:ModelField Name="TotalTime" Type="String" />
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                <ColumnModel>
                    <Columns>
                        <ext:Column ID="colSN" Sortable="false" MenuDisabled="true" runat="server" Text="SN"
                            Align="Center" Width="50" DataIndex="SN">
                        </ext:Column>
                        <ext:Column ID="colDescription" Sortable="false" MenuDisabled="true" runat="server" Wrap="true"
                            Text="Description" Align="Left" Width="800" DataIndex="Description">
                            <Editor>
                                <ext:TextField ID="txtDesciption" runat="server">
                                </ext:TextField>
                            </Editor>
                        </ext:Column>
                        <ext:Column ID="colTime" Sortable="false" MenuDisabled="true" runat="server"
                            Text="Time" Align="Left" Width="200" DataIndex="TotalTime">
                            <Editor>
                                <ext:TextField ID="TextField1" runat="server">
                                </ext:TextField>
                            </Editor>
                        </ext:Column>
                        <ext:CommandColumn ID="CommandColumn1" runat="server" Sortable="false" MenuDisabled="true"
                            Width="50" Align="Center">
                            <Commands>
                                <ext:CommandSeparator />
                                <ext:GridCommand Icon="Delete" CommandName="Delete" Cls="deleteicon" />
                            </Commands>
                            <Listeners>
                                <Command Fn="RemoveItemLine">
                                </Command>
                            </Listeners>
                        </ext:CommandColumn>
                    </Columns>
                </ColumnModel>
                <SelectionModel>
                    <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" Mode="Single" />
                </SelectionModel>
                <Plugins>
                    <ext:CellEditing ID="CellEditing1" runat="server" ClicksToEdit="1">
                    </ext:CellEditing>
                </Plugins>
            </ext:GridPanel>
            <div class="popupButtonDiv bottomBlock" style="padding-left: 35px;">
                <ext:Button runat="server" ID="btnSave" Cls="btn btn-primary" Text="<i></i>Save">
                    <DirectEvents>
                        <Click OnEvent="btnSave_Click">
                            <EventMask ShowMask="true" />
                            <ExtraParams>
                                <ext:Parameter Name="gridItems" Value="Ext.encode(#{gridPlan}.getRowsValues({ selectedOnly: false }))"
                                    Mode="Raw" />
                            </ExtraParams>
                        </Click>
                    </DirectEvents>
                    <Listeners>
                        <Click Handler="valGroup = 'SaveUpdPlan'; if(CheckValidation()) return ''; else return false;">
                        </Click>
                    </Listeners>
                </ext:Button>
                <div class="btnFlatOr">
                </div>
                <ext:Button runat="server" ID="btnCancel" Cls="btn btn-default" Text="<i></i>Cancel">
                    <Listeners>
                        <Click Handler="#{wPlan}.hide();" />
                    </Listeners>
                </ext:Button>
            </div>
        </Content>
    </ext:Window>
</asp:Content>
