﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL;
using BLL.Manager;
using Web.ReportDataSetTableAdapters;
using Web.Master;
using BLL;
using Web.CP.Report.Templates.Pay.Detail;
using DevExpress.XtraPrinting;
using Utils;
using BLL.Base;
using System.IO;
using Utils.Calendar;
using BLL.BO;

namespace Web.Employee
{
    public partial class PaySlip : BasePage
    {
      
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                List<PayrollPeriodBO> periodList = CommonManager.GetAllYear(SessionManager.CurrentCompanyId);

                ddlPayrollPeriod.Items.Clear();
                ddlPayrollPeriod.Items.AddRange(CommonManager.AppendMiddleTaxPaidList(periodList, true, true).ToArray());

                if (periodList.Count > 0)
                {
                    ddlPayrollPeriod.SelectedIndex = ddlPayrollPeriod.Items.Count - 1;
                }
            }

            if (IsPostBack)
                LoadReport();


        }

        protected void btnLoad_Click(object sender, EventArgs e)
        {

            //HttpContext.Current.Items["PaySlipIncomeList"] = null;
            //HttpContext.Current.Items["PaySlipDeductionList"] = null;
            //HttpContext.Current.Items["PaySlipAttendanceList"] = null;
            //HttpContext.Current.Items["PaySlipLeaveList"] = null;

            //LoadReport();
        }

        public int[] PayrollPeriodValue
        {
            get
            {


                if (string.IsNullOrEmpty(ddlPayrollPeriod.SelectedValue))
                    return null;
                int period = 0, type = 0, addOnId = 0;

                SplitPeriod(ddlPayrollPeriod.SelectedValue, ref period, ref type, ref addOnId);
                return new int[] { period, type, addOnId };



            }
        }
        void SplitPeriod(string value, ref int payrollPeriodId, ref int periodType, ref int addOnId)
        {
            string[] values = value.Split(new char[] { ':' });
            payrollPeriodId = int.Parse(values[0]);
            periodType = int.Parse(values[1]);

            if (values.Length >= 3 && int.Parse(values[1]) == (int)PayrollPeriodType.NewAddOn)
            {
                // assign add on id
                //payrollPeriodId = int.Parse(values[2]);
                periodType = 4;
                addOnId = int.Parse(values[2]);
            }
        }
        protected void LoadReport()
        {

            if (!IsPostBack)
                return;


            int[] values = PayrollPeriodValue;
            if (values == null || values.Length < 2)
                return;

            PayrollPeriod payrollPeriod = CommonManager.GetPayrollPeriod(values[0]);

            if (payrollPeriod == null)
                return;


            // Hide payslip if Hidden settings exists
            // Hide payslip if Hidden settings exists
            if (CommonManager.GetLastPayrollPeriod().PayrollPeriodId == payrollPeriod.PayrollPeriodId
                && CommonManager.Setting.HidePaySlip != null && CommonManager.Setting.HidePaySlip.Value)
            {
                return;
            }

            if (CommonManager.Setting.HideAllMonthPayslip != null && CommonManager.Setting.HideAllMonthPayslip.Value)
            {
                return;
            }

            //if (HttpContext.Current.Items["PaySlipIncomeList"] == null)
            {


                Report_Pay_PaySlipDetailTableAdapter mainAdapter =
                new Report_Pay_PaySlipDetailTableAdapter();

                bool readingSumForMiddleFiscalYearStartedReqd = false;
                int startingPayrollPeriodId = 0, endingPayrollPeriodId = 0;
                PayrollPeriod startingMonthParollPeriodInFiscalYear =
                   CalculationManager.GenerateForPastIncome(
                        payrollPeriod.PayrollPeriodId, ref readingSumForMiddleFiscalYearStartedReqd,
                        ref startingPayrollPeriodId, ref endingPayrollPeriodId);


                ReportPaySlip mainReport = new ReportPaySlip();



                mainReport.labelMonth.Text = string.Format("{0} ({1} {2})", payrollPeriod.Name,
                                                            DateHelper.GetMonthsForOtherPayrollPeriod(payrollPeriod.Month,
                                                                                                      IsEnglish)
                                                                                                      , DateManager.GetOtherYearForPayrollPeriod(payrollPeriod, IsEnglish));

                mainReport.labelCompanyName.Text = new CompanyManager().GetById(SessionManager.CurrentCompanyId).Name;
                string logoLoc = ReportManager.PaySlipLogoAbsolutePath;
                if (File.Exists(logoLoc))
                {
                    mainReport.logo.Image = new System.Drawing.Bitmap(logoLoc);
                    mainReport.logo.Visible = true;
                }


                BLL.BaseBiz.SetConnectionPwd(mainAdapter.Connection);
                ReportDataSet.Report_Pay_PaySlipDetailDataTable mainTable =
                    mainAdapter.GetData(payrollPeriod.Month, payrollPeriod.Year, SessionManager.CurrentCompanyId, SessionManager.CurrentLoggedInEmployeeId, "", "", values[1], values[2], -1);



                #region "Income/Deduction/Atte List"


                // Code to retrieve all emp once & save the DataTable in the request Cache so that multiple db request can be prevented
                // due to large no of employees
                Report_Pay_PaySlipDetail_EmpHeaderTableAdapter incomeHeaderAdapter = new Report_Pay_PaySlipDetail_EmpHeaderTableAdapter();
                BLL.BaseBiz.SetConnectionPwd(incomeHeaderAdapter.Connection);

                Report_Pay_PaySlip_AttendanceTableAdapter attendacDetailAdapter = new Report_Pay_PaySlip_AttendanceTableAdapter();
                BLL.BaseBiz.SetConnectionPwd(attendacDetailAdapter.Connection);

                ReportDataSet.Report_Pay_PaySlipDetail_EmpHeaderDataTable incomesTable = incomeHeaderAdapter.GetData(payrollPeriod.Month, payrollPeriod.Year,
                    SessionManager.CurrentCompanyId, SessionManager.CurrentLoggedInEmployeeId, true, null, startingPayrollPeriodId, endingPayrollPeriodId, values[1], values[2]);
                ReportDataSet.Report_Pay_PaySlipDetail_EmpHeaderDataTable deductionTable = incomeHeaderAdapter.GetData(payrollPeriod.Month, payrollPeriod.Year,
                    SessionManager.CurrentCompanyId, SessionManager.CurrentLoggedInEmployeeId, false, null, startingPayrollPeriodId, endingPayrollPeriodId, values[1], values[2]);

                ReportDataSet.Report_Pay_PaySlip_AttendanceDataTable attendanceTable =
                        attendacDetailAdapter.GetData(payrollPeriod.Month, payrollPeriod.Year, SessionManager.CurrentCompanyId, null, SessionManager.CurrentLoggedInEmployeeId.ToString());

                try
                {
                    //to prevent db timeout due to 100 connection full
                    mainAdapter.Connection.Close();
                    incomeHeaderAdapter.Connection.Close();
                    attendacDetailAdapter.Connection.Close();
                }
                catch (Exception exp)
                {
                    Log.log("Error", exp);
                }
                HttpContext.Current.Items["PaySlipIncomeList"] = incomesTable;
                HttpContext.Current.Items["PaySlipDeductionList"] = deductionTable;
                HttpContext.Current.Items["PaySlipAttendanceList"] = attendanceTable;

                mainReport.Month = payrollPeriod.Month;
                mainReport.Year = payrollPeriod.Year.Value;
                mainReport.IsIncome = true;
                mainReport.CompanyId = SessionManager.CurrentCompanyId;

                mainReport.DataSource = mainTable;
                mainReport.DataMember = "Report";

               

               this.rptViewer.Report = mainReport;
                this.ReportToolbar1.ReportViewer = this.rptViewer;
            }

            #endregion


            //if (CommonManager.CompanySetting.IsD2 == false)
            //if(Page.IsCallback)
            {
               

                //report.DisplayReport(mainReport);
            }
            //else
            //{
            //    mainReportD2.Month = payrollPeriod.Month;
            //    mainReportD2.Year = payrollPeriod.Year.Value;
            //    mainReportD2.IsIncome = true;
            //    mainReportD2.CompanyId = SessionManager.CurrentCompanyId;

            //    mainReportD2.DataSource = mainTable;
            //    mainReportD2.DataMember = "Report";
            //    //report.DisplayReport(mainReportD2);

            //    this.rptViewer.Report = mainReportD2;
            //    this.ReportToolbar1.ReportViewer = this.rptViewer;

            //}


        }
    }
}
