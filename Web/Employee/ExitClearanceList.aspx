﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/EmployeeExt.Master" AutoEventWireup="true" 
    CodeBehind="ExitClearanceList.aspx.cs" Inherits="Web.Employee.ExitClearanceList" %>
<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">

    <style type="text/css">
        .holiday, .holiday a, .holiday td {
            color: #469146;
            background-color: #F0FFF0 !important;
        }

        .leave, .leave a, .leave td {
            color: #D95CA9;
            background-color: #FFF0F5 !important;
        }

        .weeklyholiday, .weeklyholiday a, .weeklyholiday td {
            color: #8F8F1A;
            background-color: #FAFAD2 !important;
        }
    </style>
    <style type="text/css">
        .x-grid-row, .x-grid-data-row {
            height: 25px;
        }
    </style>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="mainContent" runat="server">
    <script type="text/javascript">
         function showWindow(command){
            if(command=="Form")
            {
                <%= ClearanceForm.ClientID %>.show();
            }
        }

    </script>

     
        <div class="innerLR">
            <h3>Resignation Requests</h3>
            <br />
            
            <ext:GridPanel ID="gridReqList" runat="server" Header="true" AutoScroll="true" Width="650">
                <Store>
                    <ext:Store ID="Store3" runat="server" OnReadData="Store_ReadData" RemoteSort="true"
                        PageSize="20" GroupField="EmployeeName">
                        <Proxy>
                            <ext:PageProxy />
                        </Proxy>
                        <Sorters>
                            <ext:DataSorter Direction="ASC" Property="EmployeeName" />
                        </Sorters>

                        <Model>
                            <ext:Model ID="Model1" runat="server" IDProperty="RequestID">
                                <Fields>
                                    <ext:ModelField Name="ID" />
                                    <ext:ModelField Name="EmployeeName" />
                                    <ext:ModelField Name="ExpectedDate" />
                                    <ext:ModelField Name="DaysCount" />
                                    <ext:ModelField Name="Action" />
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>


                <ColumnModel ID="ColumnModel2" runat="server">
                    <Columns>
                        <ext:RowNumbererColumn runat="server" Text="SN" Width="40" Align="Center"/>
                        <ext:Column ID="colEId" Locked="true" runat="server" Text="Employee Name" Width="130" DataIndex="EmployeeName"
                             Sortable="true" MenuDisabled="false">
                        </ext:Column>
                        <ext:DateColumn ID="Column10" Format="yyyy-MMM-dd" runat="server" Text="Expected Date"
                            Width="100" DataIndex="ExpectedDate" Sortable="true" MenuDisabled="false">
                        </ext:DateColumn>
                        <ext:Column ID="Column11" runat="server" Text="Days Count" Width="100"
                            DataIndex="DaysCount" Sortable="true" MenuDisabled="false">
                        </ext:Column>
                        <%--<ext:CommandColumn runat="server" Text="Action" Width="120">
                            <Commands>
                                <ext:GridCommand>
                                    <Menu EnableScrolling="false">
                                        <Items>
                                            <ext:MenuCommand Text="View" CommandName="View" />
                                            <ext:MenuCommand Text="Start Exist Process" CommandName="Start" />
                                            <ext:MenuCommand Text="Reject" CommandName="Reject" />
                                        </Items>
                                    </Menu>
                                </ext:GridCommand>
                            </Commands>
                            <PrepareToolbar Fn="prepare" />
                            <Listeners>
                                <Command Handler="command == 'Start' ? #{StartExitProcess}.show() : #{Reject}.show(); setValue(record);" />
                                <Command Handler="showWindow(command); setValue(record); " />
                            </Listeners>
                        </ext:CommandColumn>--%>
                        <ext:CommandColumn runat="server" Text="Action" Width="260">
                            <Commands>
                                <ext:GridCommand CommandName="View" Text="View" />
                                <ext:GridCommand CommandName="Form" Text="Form" />
                            </Commands>
                           
                            <Listeners>
                                <Command Handler="showWindow(command); setValue(record); " />
                            </Listeners>
                        </ext:CommandColumn>
                    </Columns>
                </ColumnModel>
                <Listeners>
                    <SelectionChange Handler="document.getElementById('spanText').innerHTML = this.getSelectionModel().selected.items.length + ' requests selected';" />
                </Listeners>
                <%-- <SelectionModel>
                    <ext:CheckboxSelectionModel ID="CheckboxSelectionModel1" runat="server" Mode="Simple">
                        <Listeners>
                            <BeforeSelect Fn="gridRowBeforeSelect" />
                        </Listeners>
                        <Renderer Fn="renderSelect">
                        </Renderer>
                    </ext:CheckboxSelectionModel>
                </SelectionModel>--%>
                <View>
                    <ext:GridView ID="GridView1" EnableTextSelection="true" runat="server">
                        <Listeners>
                        </Listeners>
                    </ext:GridView>
                </View>
                <BottomBar>
                    <ext:PagingToolbar ID="PagingToolbar1" runat="server" StoreID="storeEmpList" DisplayInfo="true">
                        <Items>
                            <ext:Label ID="lblPageSize" runat="server" Text="Page size:" />
                            <ext:ToolbarSpacer ID="ToolbarSpacer_listItems" runat="server" Width="10" />
                            <ext:ComboBox ID="cmbPageSize" runat="server" Width="80" SelectOnFocus="true" Selectable="true"
                                ValueField="Value" DisplayField="Text" ForceSelection="true" AllowBlank="false">
                                <Listeners>
                                    <Select Handler="#{Store3}.pageSize = this.getValue();searchList()" />
                                </Listeners>
                                <Items>
                                    <ext:ListItem Value="20" Text="20" />
                                    <ext:ListItem Value="50" Text="50" />
                                </Items>
                                <SelectedItems>
                                    <ext:ListItem Index="0">
                                    </ext:ListItem>
                                </SelectedItems>
                            </ext:ComboBox>
                        </Items>
                    </ext:PagingToolbar>
                </BottomBar>
            </ext:GridPanel>
            <br />
        </div>  


    <ext:Window
        ID="ClearanceForm"
        runat="server"
        Title="Clearance Form"
        Icon="Application"
        Width="950"
        Height="500"
        Closable="true"
        BodyStyle="background-color: #fff;"
        BodyPadding="5"
        Hidden="true"
        Modal="true">
        <Content>
            <div>
                <div class="panel">
                    <content>
                    <div class="position">
                        <label>
                            <p><strong>You are about to start employee clearnernce. </strong></p>                           
                        </label>
                        <br />
                        
                </content>
                </div>
            </div>
        </Content>
    </ext:Window>


</asp:Content>