﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL;
using BLL.Manager;
using Web.ReportDataSetTableAdapters;
using Web.Master;
using BLL;
using Web.CP.Report.Templates.Pay.Detail;
using Utils;
using BLL.Base;
using System.IO;
using Utils.Calendar;
using BLL.BO;
using Ext.Net;
using System.Text;
using BLL.Entity;
using Utils.Helper;

namespace Web.Employee
{
    public partial class DashboardReviewed : BasePage
    {

        public string EmpSearchNavigation()
        {
            return ResolveUrl("~/Employee/HR/EmployeeSearchDetail.aspx");
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            //if (CommonManager.CompanySetting.WhichCompany == WhichCompany.AceTravels)
            //{
            //    Response.Redirect("~/Employee/EmployeeAAA/PersonalDetails.aspx");
            //}


            if (CommonManager.CompanySetting.WhichCompany == WhichCompany.NIBL)
            {
                Response.Redirect("DashboardNew.aspx");
            }

            if (!IsPostBack && ! X.IsAjaxRequest)
            {
                Initialise();
                RegisterLegendColors();

                ResourceManager1.RegisterIcon(Icon.Information);

                CheckPwdChangedBeforePwdChangingDays();
                ShowPasswordChangedMessage();

                CheckIn();
                CheckOutPopup();
                TimeAttRejectedComment();

                LoadHPLDutySchedule();
            }

            JavascriptHelper.AttachPopUpCode(Page, "overtimePopup", "AApproveOvertimePopup.aspx", 475, 500);
            JavascriptHelper.AttachNonDialogPopUpCode(Page, "allowActionPopup", "AllowanceApprovePopup.aspx", 600, 550);
        }

        private void LoadHPLDutySchedule()
        {
            // show hpl duty list to only Dr Sandip (49)
            if (CommonManager.CompanySetting.WhichCompany == WhichCompany.HPL && SessionManager.CurrentLoggedInEmployeeId == 49)
            {
              
                int columnWidth = 70;

                Model SiteModel = (this.gridSchedule.Store[0].Model[0] as Model);
                string columnId = "Item_{0}";

                int weekNumber = AttendanceManager.GetWeekNumber(DateTime.Today);

                List<HPLDutyPeriod> listPeriods = listPeriods = NewHRManager.GetHPLDutyPeriodsForEmployeeDashboardOfSandip();

                int listcount = listPeriods.Count;

                int k = 0;

                foreach (var item in listPeriods)
                {
                    string gridIndexIdMonth = string.Format(columnId, ++k);

                    ModelField field = new ModelField(gridIndexIdMonth, ModelFieldType.String);
                    field.UseNull = true;
                    SiteModel.Fields.Add(field);

                    Column column = new Column();
                    column.Text = item.MonthName;
                    column.Sortable = false;
                    column.Draggable = false;
                    column.MenuDisabled = true;

                    Column column1 = new Column();
                    column1.Text = item.PeriodName;
                    column1.Sortable = false;
                    column1.Draggable = false;
                    column1.MenuDisabled = true;

                    column.Columns.Add(column1);

                    Column dataColumn = new Column { DataIndex = gridIndexIdMonth, Align = Alignment.Center, Text = "WK " + item.WeekNo.ToString().PadLeft(2, '0'), Width = columnWidth, MenuDisabled = true, Sortable = false, Draggable = false };
                    dataColumn.Renderer.Fn = "myRenderer1";
                    column1.Columns.Add(dataColumn);

                    gridSchedule.ColumnModel.Columns.Add(column);

                }

                List<int> employeeIds = NewHRManager.GetScheduleEmployeesByYearForDashboard(DateTime.Today.Year);

                object[] data = new object[employeeIds.Count];

                int sn = 0, l = 2;
                int row = 0;
                foreach (int employeeId in employeeIds)
                {
                    sn++;
                    object[] rowData = new object[listcount + 3];
                    rowData[0] = sn.ToString();
                    rowData[1] = employeeId.ToString();
                    rowData[2] = EmployeeManager.GetEmployeeById(employeeId).Name;
                    l = 2;
                    foreach (var item in listPeriods)
                    {
                        HPLDutySchedule obj = NewHRManager.GetEmployeeDutyScheduleForPeriod(employeeId, item.PeriodId);
                        if (obj != null)
                        {
                            if (obj.Value != null && obj.Value.Value)
                                rowData[++l] = "On";
                            else
                                rowData[++l] = "Off";
                        }
                        else
                            rowData[++l] = null;
                    }

                    //Add a Row
                    data[row++] = rowData;

                }

                gridSchedule.Store[0].DataSource = data;
                gridSchedule.Store[0].DataBind();
            }
            else
            {
                gridSchedule.Visible = false;
                divHPLDuty.Visible = false;
            }
        }
        

        void SetDate()
        {
            try
            {
                DateTime date = BLL.BaseBiz.GetCurrentDateAndTime();
                //set dates
                lblDay.Text = date.DayOfWeek.ToString();

                lblEngDate.Text = date.ToString("dd MMMM, yyyy");


                //CultureInfo culture = CultureInfo.GetCultureInfo("ne-NP");
                CustomDate nepalidate = DateManager.GetTodayNepaliDate();

                string[] yearNos = new string[4];
                yearNos[0] = nepalidate.Year.ToString()[0].ToString();
                yearNos[1] = nepalidate.Year.ToString()[1].ToString();
                yearNos[2] = nepalidate.Year.ToString()[2].ToString();
                yearNos[3] = nepalidate.Year.ToString()[3].ToString();

                string[] dayNos = new string[2];
                dayNos[0] = "";
                dayNos[1] = "";


                dayNos[0] = nepalidate.Day.ToString()[0].ToString();

                lblNepDate.Text = Resources.NepaliCalendar.ResourceManager.GetString("N" + dayNos[0].ToString());
                if (nepalidate.Day.ToString().Length >= 2)
                {
                    dayNos[1] = nepalidate.Day.ToString()[1].ToString();
                    lblNepDate.Text += Resources.NepaliCalendar.ResourceManager.GetString("N" + dayNos[1].ToString());
                }

                lblNepDate.Text += " " + Resources.NepaliCalendar.ResourceManager.GetString(DateHelper.GetMonthName(nepalidate.Month, false))
                    + ", " +

                    Resources.NepaliCalendar.ResourceManager.GetString("N" + yearNos[0]) +
                    Resources.NepaliCalendar.ResourceManager.GetString("N" + yearNos[1]) +
                    Resources.NepaliCalendar.ResourceManager.GetString("N" + yearNos[2]) +
                    Resources.NepaliCalendar.ResourceManager.GetString("N" + yearNos[3])
                    ;
            }
            catch (Exception exp1) { }
        }

        //void BindLeaveList(int employeeId)
        //{
        //    PayrollPeriod lastPayroll = CommonManager.GetLastPayrollPeriod();

        //    List<TextValue> leaveList = new List<TextValue>();

        //    string days = "days";
        //    if (CommonManager.CalculationConstant.CompanyHasHourlyLeave.Value)
        //        days = "hours";

        //    if (lastPayroll != null)
        //    {
        //        List<GetLeaveListAsPerEmployeeResult> list = LeaveAttendanceManager
        //            .GetAllLeavesForLeaveRequest(employeeId, lastPayroll.PayrollPeriodId, true)
        //            .Where(x => x.IsParentGroupLeave == false).ToList();

        //        //  List<GetEmployeeLeaveBalanceResult> balanceList = LeaveAttendanceManager.GetEmployeeLeaveBalance(employeeId);
        //        foreach (GetLeaveListAsPerEmployeeResult balance in list)
        //        {
        //            //if (balance.FreqOfAccrual != LeaveAccrue.COMPENSATORY)
        //            {
        //                TextValue leave = new TextValue();
        //                leave.Text
        //                    =string.Format("{0}<br/>{1} {2} left", balance.Title, string.Format("{0:0.##}", balance.NewBalance),days
        //                   );

        //                leaveList.Add(leave);
        //            }
        //        }

        //    }
        //    rptLeaveList.DataSource = leaveList;
        //    rptLeaveList.DataBind();
        //}

        void RegisterLegendColors()
        {
            StringBuilder str = new StringBuilder("\nlegends = new Array();");

            //add holiday legend color also
            str.AppendFormat("legends['{0}'] = '{1}';", HolidaysConstant.National_Holiday, HolidaysConstant.GetColor(HolidaysConstant.National_Holiday));
            str.AppendFormat("legends['{0}'] = '{1}';", HolidaysConstant.Caste_Holiday, HolidaysConstant.GetColor(HolidaysConstant.Caste_Holiday));
            str.AppendFormat("legends['{0}'] = '{1}';", HolidaysConstant.Weekly_Holiday, HolidaysConstant.GetColor(HolidaysConstant.Weekly_Holiday));
            str.AppendFormat("legends['{0}'] = '{1}';", HolidaysConstant.Female_Holiday, HolidaysConstant.GetColor(HolidaysConstant.Female_Holiday));

            X.ResourceManager.RegisterBeforeClientInitScript(str.ToString());

            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "LegendColorReg123", str.ToString(), true);
        }
       
        private void Initialise()
        {


            /*
            HolidayManager holidayMgr = new HolidayManager();
            storeMYTimesheet.DataSource = holidayMgr.GetHolidayList();
            storeMYTimesheet.DataBind();
            */

            List<AttendanceReportResult> atteList = new List<AttendanceReportResult>();
            atteList = AttendanceManager.GetAttendanceOfDateRange(SessionManager.CurrentLoggedInEmployeeId, 0, DateTime.Now.Date.AddDays(-6), DateTime.Now.Date, 0
                , 0, 99999);

            foreach (var item in atteList)
            {
                DateTime date = item.DateEng.Value;
                if (date.Date == CommonManager.GetCurrentDateAndTime().Date)
                {
                    item.ActualDate = "Today";
                }
                else
                {
                    if (date != null)
                        item.ActualDate = date.ToString("MMM d");//BLL.BaseBiz.GetAppropriateDate(date);
                }
                //if (item.InTime != null)
                //    item.RefinedInRemarks = item.InTime.Value.ToShortTimeString();
                //if (item.OutTime != null)
                //    item.RefinedOutRemarks = item.OutTime.Value.ToShortTimeString();
            }
            gvwWeekTimes.DataSource = atteList;
            gvwWeekTimes.DataBind();

            LoadLeavePendingApproval();
            try
            {

                DateTime todayDate = new DateTime();
                todayDate = CommonManager.GetCurrentDateAndTime();
                HolidayClass service = new HolidayClass();
                //LeaveRequestService service = new LeaveRequestService();
                List<GetHolidaysForAttendenceResult> bindList = new List<GetHolidaysForAttendenceResult>();

                //DateTime FromDate = new DateTime();
                DateTime ToDate = new DateTime();
                DateTime TodayDate = new DateTime();

                TodayDate = CommonManager.GetCurrentDateAndTime();

                //FromDate = new DateTime(todayDate.Year, 1, 1);
                ToDate = TodayDate.AddDays(32);

                bindList = service.GetHolidays(TodayDate, ToDate);
                bindList = bindList.Where(x => x.IsWeekly != true && x.Type != 0).ToList();


                List<NoticeBoard> noticeBoard = new List<NoticeBoard>();
                noticeBoard = NoticeManager.getAllNotice((int)NoticeBoardStatusEnum.Published);
                noticeBoard = noticeBoard
                                .OrderByDescending(x => x.PublishDate)
                                .Where(y => y.ExpiryDate == null || y.ExpiryDate >= CommonManager.GetCurrentDateAndTime())
                                .Take(7)
                                .ToList();
                //noticeBoard = noticeBoard.Take(7).ToList();
                storeNoticeBoard.DataSource = noticeBoard;
                storeNoticeBoard.DataBind();

                if (noticeBoard.Count <= 0)
                    gridNoticeBoard.Hide();

                spanNotices.InnerText = noticeBoard.Count.ToString();

                List<GetHolidaysForAttendenceResult> list = new List<GetHolidaysForAttendenceResult>();

                foreach (var item in bindList)
                {
                    if (item.DateEng != null)
                    {
                        item.EngDateLongForDashboard = item.DateEng.Value.ToString("ddd dd MMM yyyy");
                        int days = (item.DateEng.Value.Date - CommonManager.GetCurrentDateAndTime().Date).Days;
                        if (days < 0)
                            continue;

                        list.Add(item);

                        item.DaysLeftForDashboard = "In " + days.ToString() + " Days";
                        item.NepDateLongForDashboard = item.DateEngText;

                        CustomDate nepalidate = new CustomDate(item.DateEng.Value.Day, item.DateEng.Value.Month, item.DateEng.Value.Year, true); //DateManager.GetTodayNepaliDate();
                        nepalidate = CustomDate.ConvertEngToNep(nepalidate);

                        string[] yearNos = new string[4];
                        yearNos[0] = nepalidate.Year.ToString()[0].ToString();
                        yearNos[1] = nepalidate.Year.ToString()[1].ToString();
                        yearNos[2] = nepalidate.Year.ToString()[2].ToString();
                        yearNos[3] = nepalidate.Year.ToString()[3].ToString();

                        string[] dayNos = new string[2];
                        dayNos[0] = "";
                        dayNos[1] = "";


                        dayNos[0] = nepalidate.Day.ToString()[0].ToString();

                        item.NepDateLongForDashboard = Resources.NepaliCalendar.ResourceManager.GetString("N" + dayNos[0].ToString());
                        if (nepalidate.Day.ToString().Length >= 2)
                        {
                            dayNos[1] = nepalidate.Day.ToString()[1].ToString();
                            item.NepDateLongForDashboard += Resources.NepaliCalendar.ResourceManager.GetString("N" + dayNos[1].ToString());
                        }

                        item.NepDateLongForDashboard += " " + Resources.NepaliCalendar.ResourceManager.GetString(DateHelper.GetMonthName(nepalidate.Month, false))
                            + ", " +

                            Resources.NepaliCalendar.ResourceManager.GetString("N" + yearNos[0]) +
                            Resources.NepaliCalendar.ResourceManager.GetString("N" + yearNos[1]) +
                            Resources.NepaliCalendar.ResourceManager.GetString("N" + yearNos[2]) +
                            Resources.NepaliCalendar.ResourceManager.GetString("N" + yearNos[3])
                            ;

                    }
                }


                storeMYTimesheet.DataSource = list;
                storeMYTimesheet.DataBind();


            }
            catch (Exception exp)
            {
                Log.log("Employee dashboard error", exp);
            }

            CommonManager mgr = new CommonManager();

            int empId = SessionManager.CurrentLoggedInEmployeeId;
            EEmployee emp = EmployeeManager.GetEmployeeById(empId);

            if (emp == null)
                return;

            bool hasPhoto = false;
            if (emp.HHumanResources.Count > 0)
            {
                if (!string.IsNullOrEmpty(emp.HHumanResources[0].UrlPhotoThumbnail))
                {
                    image.ImageUrl = Page.ResolveUrl(Config.UploadLocation + "/" + emp.HHumanResources[0].UrlPhotoThumbnail);
                    if (File.Exists(Server.MapPath(image.ImageUrl)))
                        hasPhoto = true;
                }
            }
            if (!hasPhoto)
            {
                image.ImageUrl = Page.ResolveUrl(Web.Helper.WebHelper.GetEmployeeNoImage(empId));

            }


            

            welcome.InnerHtml =
                string.Format(welcome.InnerHtml,emp.Title + " " + emp.FirstName);

            SetDate();

            LoadTimeRequestPendingApproval();
            LoadOvertimeAllowancePendingReq();
            LoadOnLeaveEmployees();
            BindEmpLeaveGrid();
        }

        private void CheckPwdChangedBeforePwdChangingDays()
        {
            int days = 0;
            if (!IsPwdChangedBeforePwdChangingDays(SessionManager.UserName, ref days))
            {
                Setting objSetting = UserManager.GetSettingForPasswordChangeRule();
                if (objSetting.PasswordChangeType == (int)PasswordChangeTypeEnum.RecommendPwdChange)
                {
                    WPasswordNotification.Show();
                    return;
                }
            }
        }

        private void ShowPasswordChangedMessage()
        {
            if (Session["PwdChanged"] != null && Session["PwdChanged"].ToString() == "1")
            {
                Session["PwdChanged"] = "0";
                NewMessage.ShowNormalPopup("Your Password has been changed.");
            }
        }


        private void CheckIn()
        {
            Setting setting = OvertimeManager.GetSetting();
            if (setting != null && setting.ShowCheckInCheckOutPopup != null && setting.ShowCheckInCheckOutPopup.Value)
            {
                bool? isManualAtte = new AttendanceManager().IsManualAttendancePossible(SessionManager.CurrentLoggedInEmployeeId);
                
                if (isManualAtte != null && isManualAtte.Value)
                {
                    if (!AttendanceManager.IsCheckedInOrCheckedOut((int)ChkINOUTEnum.ChkIn))
                    {
                        int loggedInCount = AttendanceManager.GetLoggedInCountForUser();
                        ChangeUserActivity obj = AttendanceManager.GetLastChangeUserActivityForEmployee();
                        if (obj != null && loggedInCount == 1)
                        {
                            hdnLoggedInTime.Text = obj.DateEng.Value.ToString();
                            dfLoginDateTime.Text = string.Format("{0} | {1}", obj.DateEng.Value.ToString("h:mm tt"), obj.DateEng.Value.ToString("dddd, dd MMMM yyyy"));
                            dfMessage.Text = "This is your first login today. Do you want to Check In?";
                            btnCheckIn.Visible = true;
                            btnCheckOut.Visible = false;
                            btnCheckInCancel.Visible = true;
                            btnCheckOutCancel.Visible = false;
                            WCheckInOut.Visible = true;
                            WCheckInOut.Center();
                            WCheckInOut.Show();
                        }
                    }
                }
            }
            
        }

        protected void btnCheckIn_Click(object sender, DirectEventArgs e)
        {
            DAL.AttendanceMapping map = BLL.BaseBiz.PayrollDataContext.AttendanceMappings
                   .SingleOrDefault(x => x.EmployeeId == SessionManager.CurrentLoggedInEmployeeId);

            if (map == null)
            {
                Notification.Show(new NotificationConfig
                {
                    Title = "",
                    Icon = Icon.Information,
                    Html = "Mapping does not exists"
                });
                return;
            }

            AttendanceCheckInCheckOut chkInOut = new AttendanceCheckInCheckOut();
            chkInOut.ChkInOutID = Guid.NewGuid();
            chkInOut.DeviceID = map.DeviceId;
            chkInOut.AuthenticationType = (byte)TimeAttendanceAuthenticationType.SelfManualEntryFromSoftware; 

            if(sender == btnCheckIn)
                chkInOut.InOutMode = (int)ChkINOUTEnum.ChkIn;
            else
                chkInOut.InOutMode = (int)ChkINOUTEnum.ChkOut;

            chkInOut.IPAddress = HttpContext.Current.Request.UserHostAddress;
            chkInOut.ManualVisibleTime = DateTime.Parse(hdnLoggedInTime.Text).ToShortTimeString();

            DateTime chkinoutDate = DateTime.Parse(hdnLoggedInTime.Text);
            DateTime chkinoutTime = DateTime.Parse(hdnLoggedInTime.Text);

            chkInOut.DateTime = new DateTime(chkinoutDate.Year, chkinoutDate.Month, chkinoutDate.Day, chkinoutTime.Hour, chkinoutTime.Minute, chkinoutTime.Second);
            chkInOut.Date = chkInOut.DateTime.Date;

            chkInOut.ModificationRemarks = "Manual Attendance";

            AttendanceCheckInCheckOutComment comment = new AttendanceCheckInCheckOutComment();

            comment.AttendanceDate = chkinoutDate;

            if (sender == btnCheckIn)
                comment.InOutMode = (int)ChkINOUTEnum.ChkIn;
            else
                comment.InOutMode = (int)ChkINOUTEnum.ChkOut;

            if(!string.IsNullOrEmpty(txtNote.Text.Trim()))
                comment.Note = txtNote.Text.Trim();
            comment.EmpID = SessionManager.CurrentLoggedInEmployeeId;

            Status status = AttendanceManager.InsertCheckInAndComments(chkInOut, comment);
            if (status.IsSuccess)
            {
                if (sender == btnCheckOut)
                {
                    Session.Abandon();
                    System.Web.Security.FormsAuthentication.SignOut();
                    Response.Redirect("~/Default.aspx", true);
                }

                WCheckInOut.Close();
                Notification.Show(new NotificationConfig
                {
                    Title = "",
                    Icon = Icon.Information,
                    Html = "Your Time has been Recorded"
                });

                if (NewHRManager.RedirectToEmployeePlan())
                    Response.Redirect("~/Employee/EmpPlan.aspx");
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }
        }

        private void CheckOutPopup()
        {
            if (Session["logOut"] != null)
            {
                Session["logOut"] = null;

                DateTime now = DateTime.Now;

                hdnLoggedInTime.Text = now.ToString();
                dfLoginDateTime.Text = string.Format("{0} | {1}", now.ToString("h:mm tt"), now.ToString("dddd, dd MMMM yyyy"));
                dfMessage.Text = "All done. Do you want to Check Out?";
                btnCheckIn.Visible = false;
                btnCheckOut.Visible = true;
                btnCheckInCancel.Visible = false;
                btnCheckOutCancel.Visible = true;
                WCheckInOut.Title = "Check Out";
                WCheckInOut.Closable = false;
                WCheckInOut.Visible = true;
                WCheckInOut.Center();
                WCheckInOut.Show();                
            }
        }

        protected void btnCheckInCancel_Click(object sender, DirectEventArgs e)
        {
            if (sender == btnCheckInCancel)
            {
                WCheckInOut.Close();
                return;
            }

            Session.Abandon();
            System.Web.Security.FormsAuthentication.SignOut();
            Response.Redirect("~/Default.aspx", true);
        }

        private void TimeAttRejectedComment()
        {
            TimeRequest obj = AttendanceManager.GetRejectedTimeRequestForEmployee();
            if (obj != null && !string.IsNullOrEmpty(obj.RejectComment))
            {
                lblTimeAttRejectedComment.Text = string.Format("Your time attendance rejected : {0}.", obj.RejectComment);
                divTimeAttRejectComment.Visible = true;
            }
        }

        protected void btnSaveComment_Save(object sender, DirectEventArgs e)
        {

            ActivityEmpComment obj = new ActivityEmpComment();

            if (string.IsNullOrEmpty(txtActivityComment.Text.Trim()))
            {
                NewMessage.ShowWarningMessage("Comment is required.");
                txtActivityComment.Focus();
                return;
            }

            obj.Comment = txtActivityComment.Text.Trim();
            obj.EmployeeId = SessionManager.CurrentLoggedInEmployeeId;
            obj.ActivityId = int.Parse(hdnActivityId.Text);

            Status status = NewHRManager.SaveUpdateEmployeeActivityComment(obj);
            if (status.IsSuccess)
            {
                NewMessage.ShowNormalMessage("Comment saved successfully.");
                WComment.Close();
            }
            else
                NewMessage.ShowWarningMessage("Errror while saving comment.");

        }

        #region Leave Request
        protected void LoadLeavePendingApproval()
        {
            List<DAL.GetLeaveByDateApprovalResult> list = LeaveAttendanceManager.GetLeaveByDateApproval("1-6", SessionManager.CurrentCompany.CompanyId, -1, 0, 5, -1, -1, "", "0", SessionManager.CurrentLoggedInEmployeeId);

            DateTime fromDate, toDate;
            foreach (GetLeaveByDateApprovalResult item in list)
            {
                fromDate = item.FromDate.Value;
                toDate = item.ToDate.Value;

                if (fromDate.Year != toDate.Year)
                    item.Range = string.Format("{0}-{1}", fromDate.ToString("MMM d, yyyy"), toDate.ToString("MMM d,yyyy"));
                else if (fromDate.Month != toDate.Month)
                    item.Range = string.Format("{0}-{1}, {2}", fromDate.ToString("MMM d"), toDate.ToString("MMM d"), fromDate.Year.ToString());
                else if (fromDate.Date != toDate.Date)
                    item.Range = string.Format("{0}-{1}, {2}", fromDate.ToString("MMM d"), toDate.Day.ToString(), fromDate.Year.ToString());
                else
                    item.Range = fromDate.ToString("MMM d, yyyy");
            }

            gridPendingRequest.GetStore().DataSource = list;
            gridPendingRequest.GetStore().DataBind();
            spnLeaveRequestCount.InnerText = list.Count.ToString();

            LoadOnLeaveEmployees();
        }

        protected void btnViewLeaveReq_Click(object sender, DirectEventArgs e)
        {
            if (string.IsNullOrEmpty(hdnLeaveRequestId.Text))
            {
                NewMessage.ShowWarningMessage("Please select leave request.");
                return;
            }

            int requestId = int.Parse(hdnLeaveRequestId.Text);

            DAL.LeaveRequest leave = LeaveAttendanceManager.GetLeaveRequestById(requestId);
            if (leave != null)
            {
                if (LeaveRequestManager.CanCurrentEmployeeChangeLeaveRequestStatus(SessionManager.CurrentLoggedInEmployeeId, leave) == false)
                {
                    X.MessageBox.Show(
                   new MessageBoxConfig
                   {
                       Message = "Not enough permission to view the leave details.",
                       Buttons = MessageBox.Button.OK,
                       Title = "Warning",
                       Icon = MessageBox.Icon.WARNING,
                       MinWidth = 300
                   });
                    return;
                }
            }

            this.AssignLeave.ClearField();
            if (this.AssignLeave.LoadSelectedData(requestId) == false)
            {
                X.MessageBox.Show(
                 new MessageBoxConfig
                 {
                     Message = "Leave is disabled for this employee,can not be changed.",
                     Buttons = MessageBox.Button.OK,
                     Title = "Warning",
                     Icon = MessageBox.Icon.WARNING,
                     MinWidth = 300
                 });

                Ext.Net.Mask m = new Mask();
                m.Hide();
                return;
            }

            this.AssignLeave.AssignLeaveWindow.Center();
            this.AssignLeave.AssignLeaveWindow.Show();
        }

        protected void btnApproveLeaveReq_Click(object sender, DirectEventArgs e)
        {
            if (string.IsNullOrEmpty(hdnLeaveRequestId.Text))
            {
                NewMessage.ShowWarningMessage("Please select leave request.");
                return;
            }

            int requestId = int.Parse(hdnLeaveRequestId.Text);

            DAL.LeaveRequest dbLeaveRequest = LeaveAttendanceManager.GetLeaveRequestById(requestId);

            List<int> leaveRequestIds = new List<int>();
            leaveRequestIds.Add(requestId);
            int count = 0;

            ResponseStatus status = LeaveAttendanceManager.ApproveSelectedLeaveRequests(leaveRequestIds, ref count);
            

            if (status.IsSuccessType == true)
            {
                X.MessageBox.Show(
                       new MessageBoxConfig
                       {
                           Message = "Leave Approved.",
                           Buttons = MessageBox.Button.OK,
                           Title = "Information",
                           Icon = MessageBox.Icon.INFO,
                           MinWidth = 300,
                           Handler = "reloadGridsAfterApproval();"
                       });

                LoadLeavePendingApproval();

                LeaveAttendanceManager.NotifyThroughMessageAndEmail(dbLeaveRequest.LeaveRequestId, dbLeaveRequest.IsHour,
                dbLeaveRequest.DaysOrHours.ToString(), dbLeaveRequest.Reason, dbLeaveRequest.FromDateEng.ToString(), dbLeaveRequest.ToDateEng.ToString(), ((LeaveRequestStatusEnum)dbLeaveRequest.Status).ToString()
                , dbLeaveRequest.SupervisorEmployeeIdInCaseOfSelection, dbLeaveRequest.RecommendEmployeeId, null);
            }
            else
            {
                BLL.Helper.ShowExtWarningMessage(status.ErrorMessage);
            }
        }

        protected void btnDenyLeaveReq_Click(object sender, DirectEventArgs e)
        {
            int requestId = int.Parse(hdnLeaveRequestId.Value.ToString());
            Status status = LeaveAttendanceManager.ProcessLeaveLikeForDeny(requestId, LeaveRequestStatusEnum.Denied, "");

            DAL.LeaveRequest leaveRequest = LeaveAttendanceManager.GetLeaveRequestById(requestId);

            if (status.IsSuccess == true)
            {
                X.MessageBox.Show(
                       new MessageBoxConfig
                       {
                           Message = "Leave Denied.",
                           Buttons = MessageBox.Button.OK,
                           Title = "Information",
                           Icon = MessageBox.Icon.INFO,
                           MinWidth = 300,
                           Handler = "reloadGridsAfterApproval();"
                       });

                LoadLeavePendingApproval();
                LeaveAttendanceManager.NotifyThroughMessageAndEmail(Resources.Messages.LeaveDeny, leaveRequest, leaveRequest.EmployeeId, leaveRequest.FromDateEng.ToString(), leaveRequest.ToDateEng.ToString(), "denied");
            }
            else
            {
                BLL.Helper.ShowExtWarningMessage(status.ErrorMessage);
            }
        }

        protected void btnLeaveReqReload_Click(object sender, DirectEventArgs e)
        {
            LoadLeavePendingApproval();
        }
        #endregion

        #region Time Request
        protected void LoadTimeRequestPendingApproval()
        {
            int totalRecords = 0;
            List<GetTimeRequestsForApprovalResult> list = AttendanceManager.GetTimeRequestForApproval(-1, SessionManager.CurrentLoggedInEmployeeId, null, null, 0, 999999, out totalRecords, "0,3");

            bool inRequest = false, outRequest = false;
            string reason = "";
            DateTime fromDate, toDate;

            foreach (var item in list)
            {
                TimeRequest obj = AttendanceManager.GetTimeRequestByRequestID(item.RequestID);

                inRequest = false;
                outRequest = false;
                reason = "";

                fromDate = item.StartDateEng.Value;
                toDate = item.EndDateEng.Value;

                if (fromDate.Year != toDate.Year)
                    item.DateRange = string.Format("{0}-{1}", fromDate.ToString("MMM d, yyyy"), toDate.ToString("MMM d,yyyy"));
                else if (fromDate.Month != toDate.Month)
                    item.DateRange = string.Format("{0}-{1}, {2}", fromDate.ToString("MMM d"), toDate.ToString("MMM d"), fromDate.Year.ToString());
                else if (fromDate.Date != toDate.Date)
                    item.DateRange = string.Format("{0}-{1}, {2}", fromDate.ToString("MMM d"), toDate.Day.ToString(), fromDate.Year.ToString());
                else
                    item.DateRange = fromDate.ToString("MMM d, yyyy");

                foreach (var line in obj.TimeRequestLines)
                {
                    if (line.InTime != null)
                    {
                        inRequest = true;
                        reason += line.InNote + ",";
                    }

                    if (line.OutTime != null)
                    {
                        outRequest = true;
                        reason += line.OutNote;
                    }
                }

                item.Reason = reason.TrimEnd(',');

                if (inRequest && outRequest)
                    item.ReqType = "In, Out";
                else if (inRequest)
                    item.ReqType = "In";
                else
                    item.ReqType = "Out";
            }

            gridTimeRequest.Store[0].DataSource = list;
            gridTimeRequest.Store[0].DataBind();

            spanTimeRequestCount.InnerText = list.Count.ToString();        
        }

        protected void btnViewTimeReq_Click(object sender, DirectEventArgs e)
        {
            if (string.IsNullOrEmpty(hdnTimeRequestId.Text))
            {
                NewMessage.ShowWarningMessage("Please select time request.");
                return;
            }

            int requestId = int.Parse(hdnTimeRequestId.Text);
            TimeReqCtrl1.LoadWindow(requestId);
        }

        protected void btnApproveTimeReq_Click(object sender, DirectEventArgs e)
        {
            if (string.IsNullOrEmpty(hdnTimeRequestId.Text))
            {
                NewMessage.ShowWarningMessage("Please select time request.");
                return;
            }

            int requestId = int.Parse(hdnTimeRequestId.Text);

            List<TimeRequest> list = new List<TimeRequest>();
            TimeRequest dbObj = AttendanceManager.GetTimeRequestByRequestID(requestId);
            list.Add(dbObj);

            Status status = AttendanceManager.ApproveAttRequestTime(list, false, false);
            if (status.IsSuccess)
            {
                NewMessage.ShowNormalMessage("Time request approved successfully.", "reloadTimeReqGrid()");

                LoadTimeRequestPendingApproval();
                TimeReqCtrl1.SendApproveMail(list);
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }
        }

        protected void btnRejectTimeReq_Click(object sender, DirectEventArgs e)
        {
            if (string.IsNullOrEmpty(hdnTimeRequestId.Text))
            {
                NewMessage.ShowWarningMessage("Please select time request.");
                return;
            }

            int requestId = int.Parse(hdnTimeRequestId.Text);
            TimeReqCtrl1.LoadRejectWindow(requestId);
        }

        protected void btnReloadTimeReqGrid_Click(object sender, DirectEventArgs e)
        {
            LoadTimeRequestPendingApproval();
        }
        #endregion

        private void LoadOvertimeAllowancePendingReq()
        {
            int totalRecords = 0;

            List<OTAllowanceCls> listOTAllowance = new List<OTAllowanceCls>();

            List<GetOvertimeRequestForManagerResult> listOT = OvertimeManager.GetLeaveRequestsForManager(-1, -2, 1, 999999, -1, -1,
                null, null, ref totalRecords);

            foreach (var item in listOT)
            {
                OTAllowanceCls obj = new OTAllowanceCls();
                obj.Id = item.RequestID;
                obj.Name = item.EmployeeName;
                obj.Type = item.OvertimeType;
                obj.DateString = item.Date.Value.ToString("MMM d, yyyy");
                obj.DateOrder = item.Date.Value;
                obj.Days = item.DurationModified;
                obj.StatusText = item.StatusModified;
                obj.OTAllowanceType = "ot";
                listOTAllowance.Add(obj);
            }

            List<GetEveningCounterRequestForManagerResult> listAllowance = OvertimeManager.GetEveningCounterForManager
                (-1, -2, 1, 999999, ref totalRecords, null, null, "", -1, -1);

            DateTime fromDate, toDate;

            foreach (var item in listAllowance)
            {
                OTAllowanceCls obj = new OTAllowanceCls();
                obj.Id = item.RequestID;
                obj.Name = item.EmployeeName;
                obj.Type = item.AllowanceType;

                fromDate = item.StartDate.Value;
                toDate = item.EndTime.Value;

                obj.DateOrder = item.StartDate.Value;
                if (fromDate.Year != toDate.Year)
                    obj.DateString = string.Format("{0}-{1}", fromDate.ToString("MMM d, yyyy"), toDate.ToString("MMM d,yyyy"));
                else if (fromDate.Month != toDate.Month)
                    obj.DateString = string.Format("{0}-{1}, {2}", fromDate.ToString("MMM d"), toDate.ToString("MMM d"), fromDate.Year.ToString());
                else if (fromDate.Date != toDate.Date)
                    obj.DateString = string.Format("{0}-{1}, {2}", fromDate.ToString("MMM d"), toDate.Day.ToString(), fromDate.Year.ToString());
                else
                    obj.DateString = fromDate.ToString("MMM d, yyyy");

                obj.Days = item.Duration.ToString();

                obj.StatusText = ((EveningCounterStatusEnum)item.Status.Value).ToString();
                obj.OTAllowanceType = "al";
                listOTAllowance.Add(obj);
            }

            gridOvertimeAllowance.Store[0].DataSource = listOTAllowance.OrderByDescending(x => x.DateOrder).ToList();
            gridOvertimeAllowance.Store[0].DataBind();

            spanOvertimeAllowanceReq.InnerText = listOTAllowance.Count.ToString();           
        }

        protected void btnApproveOTAllow_Click(object sender, DirectEventArgs e)
        {
            if (string.IsNullOrEmpty(hdnOTAllowReqId.Text))
            {
                NewMessage.ShowWarningMessage("Pleas select the request for approval.");
                return;
            }

            int requestId = int.Parse(hdnOTAllowReqId.Text);
            if (hdnOTAllowType.Text == "ot")
            {
                OvertimeRequest request = OvertimeManager.getRequestByID(hdnOTAllowReqId.Text);

                OvertimeRequest requestInstance = new OvertimeRequest();
                requestInstance.OvertimeRequestID = requestId;
                requestInstance.ApprovedTimeInMinute = request.ApprovedTimeInMinute;
                double totalHour = TimeSpan.FromMinutes(request.ApprovedTimeInMinute.Value).TotalHours;

                if (totalHour >= 24)
                {
                    NewMessage.ShowWarningMessage("Approval hour should be under 24 hrs, current total hour is "
                         + totalHour.ToString("n2") + ".");
                    return;
                }

                requestInstance.ApprovedTime = request.ApprovedTime;
                requestInstance.Status = (int)OvertimeStatusEnum.Approved;

                List<RequestEditHistory> edits = new List<RequestEditHistory>();

                Status status = OvertimeManager.ApproveRequest(requestInstance, edits);
                if (status.IsSuccess)
                {
                    NewMessage.ShowNormalMessage("Overtime has been approved.");
                    LoadOvertimeAllowancePendingReq();
                }
                else
                {
                    NewMessage.ShowWarningMessage(status.ErrorMessage);
                }
            }
            else if (hdnOTAllowType.Text == "al")
            {
                EveningCounterRequest request = AllowanceManager.getEveningCounterRequestByID(requestId);

                EveningCounterRequest requestInstance = new EveningCounterRequest();
                requestInstance.CounterRequestID = requestId;
                requestInstance.StartDate = request.StartDate;
                requestInstance.EndDate = request.EndDate;
                requestInstance.Days = request.Days;

                List<RequestEditHistory> edits = new List<RequestEditHistory>();

                requestInstance.Status = (int)EveningCounterStatusEnum.Approved;
                Status status = AllowanceManager.ApproveRequest(requestInstance, edits);
                if (status.IsSuccess)
                {
                    NewMessage.ShowNormalMessage("Allowance has been approved.");
                    LoadOvertimeAllowancePendingReq();
                }
                else
                    NewMessage.ShowWarningMessage(status.ErrorMessage);
            }


        }

        protected void btnRejectOTAllow_Click(object sender, DirectEventArgs e)
        {
            if (string.IsNullOrEmpty(hdnOTAllowReqId.Text))
            {
                NewMessage.ShowWarningMessage("Pleas select the request for approval.");
                return;
            }

            int requestId = int.Parse(hdnOTAllowReqId.Text);
            if (hdnOTAllowType.Text == "ot")
            {
                bool isSuccess = OvertimeManager.RejectRequest(requestId);
                if (isSuccess)
                {
                    NewMessage.ShowNormalMessage("Overtime has been rejected.");
                    LoadOvertimeAllowancePendingReq();
                }
                else
                    NewMessage.ShowWarningMessage("Error while rejecting overtime request.");
            }
            else if (hdnOTAllowType.Text == "al")
            {
                bool isSuccess = AllowanceManager.RejectRequest(requestId);
                if (isSuccess)
                {
                    NewMessage.ShowNormalMessage("Allowance has been rejected.");
                    LoadOvertimeAllowancePendingReq();
                }
            }
        }

        protected void btnReloadOTAllowGrid_Click(object sender, DirectEventArgs e)
        {
            LoadOvertimeAllowancePendingReq();
        }

        private void LoadOnLeaveEmployees()
        {
            List<GetLeaveByDateApprovalResult> list = LeaveAttendanceManager
                .GetLeaveByDateApproval(((int)LeaveRequestStatusEnum.Approved).ToString(),
                SessionManager.CurrentCompanyId, -1, 0, int.MaxValue, -1, -1, "", "0", SessionManager.CurrentLoggedInEmployeeId);

            List<EmpOnLeave> listEmployeeOnLeave = new List<EmpOnLeave>();

            DateTime today = DateTime.Today;
            foreach (var item in list)
            {
                if (item.FromDate.Value.Date >= today.Date && item.ToDate.Value.Date <= today.Date)
                {
                    if (item.IsHour == null || item.IsHour.Value == false)
                    {
                        EmpOnLeave obj = new EmpOnLeave();
                        obj.EmployeeId = item.EmployeeId;
                        obj.Name = item.EmployeeName;
                        obj.UrlPhoto = GetEmployeePhotoUrl(obj.EmployeeId);
                        listEmployeeOnLeave.Add(obj);
                    }
                }
            }

            storeAbsentEmployee.DataSource = listEmployeeOnLeave.Distinct().OrderBy(x => x.Name);
            storeAbsentEmployee.DataBind();

        }


        public string GetEmployeePhotoUrl(int employeeId)
        {
            string photoUrl = "";

            photoUrl = BLL.BaseBiz.PayrollDataContext.HHumanResources.SingleOrDefault(x => x.EmployeeId == employeeId).UrlPhotoThumbnail;
            if (string.IsNullOrEmpty(photoUrl))
            {
                string title = BLL.BaseBiz.PayrollDataContext.EEmployees.SingleOrDefault(x => x.EmployeeId == employeeId).Title;

                if (!string.IsNullOrEmpty(title))
                {
                    if (title.ToLower() == "mr")
                        photoUrl = "../images/male.png";
                    else
                        photoUrl = "../images/female.png";
                }
                else
                    photoUrl = "../images/female.png";
            }

            return photoUrl;
        }

        private void BindEmpLeaveGrid()
        {
            PayrollPeriod payroll = CommonManager.GetLastPayrollPeriod();
            gridLeaveRequest.Store[0].DataSource = LeaveAttendanceManager.GetAllLeavesForLeaveRequest(SessionManager.CurrentLoggedInEmployeeId, payroll.PayrollPeriodId, true);
            gridLeaveRequest.Store[0].DataBind();
        }

    }

    public class OTAllowanceCls
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public string DateString { get; set; }
        public DateTime DateOrder { get; set; }
        public string Days { get; set; }
        public string StatusText { get; set; }
        public string OTAllowanceType { get; set; }
    }
    public class EmpOnLeave
    {
        public int EmployeeId { get; set; }
        public string Name { get; set; }
        public string UrlPhoto { get; set; }
    }
}
