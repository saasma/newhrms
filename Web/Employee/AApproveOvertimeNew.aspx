﻿<%@ Page Title="Approve Overtime" Language="C#" MasterPageFile="~/Master/EmployeeExt.Master" AutoEventWireup="true" CodeBehind="AApproveOvertimeNew.aspx.cs" Inherits="Web.Employee.AApproveOvertimeNew" %>

<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript" src="<%= ResolveUrl("~/Employee/override.js?v=") + Web.Helper.WebHelper.Version %>"></script>
    
<style type="text/css">
    
   .boldCls1
   {
       font-weight:bold;
       margin-left:20px;
       padding-left:8px;
       display:block;
   } 
   .boldCls
   {
       font-weight:bold;
   } 
   
   .x-progress-text
   {
       background-color:White;
   }
 
</style>

<script type="text/javascript">

    var CommandHandler = function(command, record){
            <%= hdnRequestID.ClientID %>.setValue(record.data.RequestID);
            <%= hdnEmployeeId.ClientID %>.setValue(record.data.EmployeeId);
                if(command=="Edit")
                {
                    <%= btnEdit.ClientID %>.fireEvent('click');
                }

           };


 var checkboxRenderer = function (v, p, record) {
            if(record.data.CanRecOrAppRec == false){            
                return "";
            }
            
          return '<div class="x-grid-row-checker">&nbsp;</div>'
        };


var addNewRow = function (grid) {
            var newRow = new SettingModel();

            newRow.data.ControlID = "";

            var rowIndex = grid.getStore().data.items.length;

            //gridProjectSplitter.stopEditing();
            grid.getStore().insert(rowIndex, newRow);
        }

        var RemoveItemLine = function (column, command, record, recordIndex, cellIndex) {
            var store = this.grid.store;
            store.remove(record);
        };

        var CalculateWorkHours = function(e1,e2,record)
        {
            if(record.data.InTime == null || record.data.OutTime == null)
            {
                return;
            }

            if(record.data.InTime != '' && record.data.OutTime != '')
            {
                var strTime = Ext.util.Format.date(record.data.InTime, 'g:i a')
                var eTime = Ext.util.Format.date(record.data.OutTime, 'g:i a')
                var diffTime = calculateTotalMinutes(eTime) - calculateTotalMinutes(strTime)
                if(diffTime <= 0)
                {
                    alert('End time must be greater than Start time.');
                    record.data.OutTime = "";
                    return "";
                }


                return (Math.floor(diffTime/60) + ":" + (diffTime%60));
              
            }
        };

       

        function calculateTotalMinutes(time)
        {
            var parts = time.split(/ |:/);
            var totalHours = 0;

            if(parts[2] == 'pm')
            {
               if(parseInt(parts[0], 10) == 12)
               {
                    return (12 * 60 + parseInt(parts[1], 10));
               }
               else
               {
                    return (12 * 60 + parseInt(parts[0], 10) * 60 + parseInt(parts[1], 10));
               }
            }
            else
            {
                return (parseInt(parts[0], 10) * 60 + parseInt(parts[1], 10));
            }
    
            //return (parts[2] == 'pm' * 12 * 60) + parseInt(parts[0], 10) * 60 + parseInt(parts[1], 10);
        }


        function AssignOTImportPopup()
        {
            var val = <%=cmbOvertimeType.ClientID %>.getValue();
            if(val == null || val == '')
            {
                alert('Please select overtime type.');
                return;
            }
            assignovertimeImp('OvertimeType=' + <%=cmbOvertimeType.ClientID %>.getRawValue());
        }

       var isSaved = false;

    function UpdateSavedStatus()
     {
        isSaved = true;
     }

    function refreshWindow()
    {
        <%=btnLoadGrid.ClientID %>.fireEvent('click');
    }
    
     var skipLoadingCheck = true;

    function closePopup() {
       window.close();
      window.opener.refreshWindow();
    }

    var EmpNameRender = function (value, metaData, record, rowIndex, colIndex, store) {
                    var r = <%= storeEmployees.ClientID %>.getById(value.toString().toLowerCase());

                    if (Ext.isEmpty(r)) {
                        return "";
                    }
                    return r.data.Text;
                };


</script>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="mainContent" runat="server">

<div class="contentArea" style="margin-top:10px">



<ext:Hidden runat="server" ID="hdnRequestID" />
<ext:Hidden runat="server" ID="hdnEmployeeId" />
<ext:Hidden runat="server" ID="Hidden_MinutesPrev" />

<ext:LinkButton ID="btnLoadGrid" runat="server" Hidden="true">
    <DirectEvents>
        <Click OnEvent="btnLoadGrid_Click">
            <EventMask ShowMask="true" />
        </Click>
    </DirectEvents>
</ext:LinkButton>

<ext:LinkButton runat="server" Hidden="true" ID="btnEdit">
    <DirectEvents>
        <Click OnEvent="btnEdit_Click">
            <EventMask ShowMask="true" />
        </Click>
    </DirectEvents>
</ext:LinkButton>

 <ext:Store ID="storeEmployees" runat="server">
    <Model>
        <ext:Model ID="modelEmployees" IDProperty="Value" runat="server">
            <Fields>
                <ext:ModelField Name="Text" Type="String" />
                <ext:ModelField Name="Value" Type="String" />
            </Fields>
        </ext:Model>
    </Model>
</ext:Store>

        <div class="attribute" style="padding:10px">
            
            <table>
                <tr>
                    <td rowspan="2">                        
                        <ext:Button ID="btnAssignOT" runat="server" Cls="btn btn-save" Text="Assign Overtime" Width="130">
                            <DirectEvents>
                                <Click OnEvent="btnAssignOT_Click">                               
                                    <EventMask ShowMask="true" />                              
                                </Click>
                            </DirectEvents>
                        </ext:Button>     
                    </td>
                    
                    <td style="padding-left:10px;">

                        <ext:ComboBox ID="cmbType" runat="server" Width="150" LabelWidth="40"
                            FieldLabel="Period" LabelAlign="Left" LabelSeparator="" ForceSelection="true"
                            QueryMode="Local">
                            <Items>
                                <ext:ListItem Text="All" Value="-1" />
                                <ext:ListItem Text="This Week" Value="1" />
                                <ext:ListItem Text="This Month" Value="2" />
                                <ext:ListItem Text="Last Month" Value="3" />
                            </Items>      
                            <SelectedItems>
                                <ext:ListItem Text="All" Value="-1" />
                            </SelectedItems>                   
                        </ext:ComboBox>

                    </td>

                    <td style="padding-left:5px;">

                        <ext:ComboBox ID="cmbStatus" runat="server" Width="150" LabelWidth="40"
                            FieldLabel="" LabelAlign="Left" LabelSeparator="" ForceSelection="true"
                            QueryMode="Local">
                            <Items>
                                <ext:ListItem Text="All" Value="-1" />
                                <ext:ListItem Text="Pending" Value="0" />
                                <ext:ListItem Text="Recommended" Value="1" />
                                <ext:ListItem Text="Approved" Value="2" />
                                <ext:ListItem Text="Rejected" Value="3" />
                            </Items>      
                            <SelectedItems>
                                <ext:ListItem Text="Pending" Value="0" />
                            </SelectedItems>                   
                        </ext:ComboBox>
                    </td>

                    <td valign="top" style="padding-bottom: 8px;padding-left:15px;">                      
                        <ext:Button ID="btnLoad" runat="server" Cls="btn btn-save" Text="Load" Width="130">
                            <DirectEvents>
                                <Click OnEvent="btnLoad_Click">                               
                                    <EventMask ShowMask="true" />                              
                                </Click>
                            </DirectEvents>
                        </ext:Button>     
                    </td>
                </tr>
            </table>
           
        </div>
        <div class="clear">

            <ext:GridPanel StyleSpec="margin-top:15px;" ID="gridOverTimeReq" runat="server" Cls="itemgrid" Scroll="None">
            <Store>
                <ext:Store ID="Store1" runat="server" OnReadData="MyData_Refresh" PageSize="10">
                    <Model>
                        <ext:Model ID="Model6" runat="server" IDProperty="RequestID">
                            <Fields>
                                <ext:ModelField Name="RequestID" Type="String" />
                                <ext:ModelField Name="EmployeeID" Type="String" />                                
                                <ext:ModelField Name="EmployeeName" Type="String" />
                                <ext:ModelField Name="Date" Type="String" />
                                <ext:ModelField Name="StartTime" Type="string" />                                
                                <ext:ModelField Name="EndTime" Type="string" />
                                <ext:ModelField Name="CheckInTime" Type="string" />
                                <ext:ModelField Name="CheckOutTime" Type="string" />                                                               
                                <ext:ModelField Name="DurationModified" Type="string" />
                                <ext:ModelField Name="ApprovedTime" Type="string" />
                                <ext:ModelField Name="Reason" Type="string" />
                                <ext:ModelField Name="StatusModified" Type="string" />
                                <ext:ModelField Name="CanRecOrAppRec" Type="Boolean" />
                            </Fields>
                        </ext:Model>
                    </Model>
                </ext:Store>
            </Store>
            <ColumnModel>
                <Columns>
                    <ext:Column ID="Column25" Sortable="false" MenuDisabled="true" runat="server" Text="RequestID" Visible="false"
                        Align="Left" Width="150" DataIndex="RequestID" />
                    <ext:Column ID="Column1" Sortable="false" MenuDisabled="true" runat="server" Text="EmployeeID" Visible="false"
                        Align="Left" Width="150" DataIndex="EmployeeID" />

                    <ext:Column ID="colDate1" runat="server" Align="Left" Text="Date" Width="100"
                        MenuDisabled="true" Sortable="false" DataIndex="Date">
                    </ext:Column>

                    <ext:Column ID="colEmployeeName" Sortable="false" MenuDisabled="true" runat="server" Text="Employee Name"
                        Align="Left" Width="200" DataIndex="EmployeeName" />

                    <ext:Column ID="colStartTime" Sortable="false" MenuDisabled="true" runat="server" Text="Start Time" Width="80"
                        Align="Left" DataIndex="StartTime">
                    </ext:Column>
                    <ext:Column ID="colEndTime" Sortable="false" MenuDisabled="true" runat="server" Text="End Time" Width="80"
                        Align="Left" DataIndex="EndTime">
                    </ext:Column>
                    <ext:Column ID="colCheckInTime" Sortable="false" MenuDisabled="true" runat="server" Text="In Time"
                        Align="Left" Width="80" DataIndex="CheckInTime">
                    </ext:Column>
                    <ext:Column ID="colCheckOutTime" Sortable="false" MenuDisabled="true" runat="server" Text="Out Time"
                        Align="Left" Width="80" DataIndex="CheckOutTime">
                    </ext:Column>

                    <ext:Column ID="colDurationModified" Sortable="false" MenuDisabled="true" runat="server" Text="Duration"
                        Align="Left" Width="100" DataIndex="DurationModified">
                    </ext:Column>
                    <ext:Column ID="colApprovedTime" Sortable="false" MenuDisabled="true" runat="server" Text="Approved Time"
                        Align="Left" Width="100" DataIndex="ApprovedTime">
                    </ext:Column>
                    <ext:Column ID="colReason" Sortable="false" MenuDisabled="true" runat="server" Text="Reason"
                        Align="Left" Width="200" DataIndex="Reason">
                    </ext:Column>

                     <ext:Column ID="colStatusModified" Sortable="false" MenuDisabled="true" runat="server" Text="Status"
                        Align="Left" Width="100" DataIndex="StatusModified">
                    </ext:Column>
                   
                    <ext:CommandColumn ID="CommandColumn3" runat="server" Width="75" Text="Actions" Align="Center">
                        <Commands>
                            <ext:CommandSeparator />
                            <ext:GridCommand Cls="editGridButton" ToolTip-Text="Edit"  Text="<i class='fa fa-pencil'></i>" CommandName="Edit" />
                        </Commands>
                        <Listeners>
                            <Command Handler="CommandHandler(command,record);" />
                        </Listeners>
                       
                    </ext:CommandColumn>
                </Columns>
            </ColumnModel>
            <SelectionModel>
                <ext:CheckboxSelectionModel ID="CheckboxSelectionModel1" runat="server">
                    <CustomConfig>
                        <ext:ConfigItem Name="renderer" Value="checkboxRenderer" Mode="Raw" />
                    </CustomConfig>
                    <Listeners>
                        <BeforeSelect Handler="return record.data.CanRecOrAppRec == true;" />
                    </Listeners>
                </ext:CheckboxSelectionModel>
            </SelectionModel>       
            <View>
                <ext:GridView ID="GridView1" runat="server" StripeRows="true" />                   
            </View>            
            <BottomBar>
                 <ext:PagingToolbar ID="PagingToolbar1" runat="server">
                    <Items>
                        <ext:Label ID="Label1" runat="server" Text="Page size:" />
                        <ext:ToolbarSpacer ID="ToolbarSpacer1" runat="server" Width="10" />
                        <ext:ComboBox ID="ComboBox1" runat="server" Width="80">
                            <Items>
                                <ext:ListItem Text="20" />
                                <ext:ListItem Text="30" />
                                <ext:ListItem Text="50" />
                                <ext:ListItem Text="100" />
                            </Items>
                            <SelectedItems>
                                <ext:ListItem Value="20" />
                            </SelectedItems>
                            <Listeners>
                                <Select Handler="#{gridOverTimeReq}.store.pageSize = parseInt(this.getValue(), 20); #{gridOverTimeReq}.store.reload();" />
                            </Listeners>
                        </ext:ComboBox>
                    </Items>
                    <Plugins>
                        <ext:ProgressBarPager ID="ProgressBarPager1" runat="server" />
                    </Plugins>
                </ext:PagingToolbar>
            </BottomBar>
        </ext:GridPanel>

         

        </div>
        <div class="buttonsDiv">           

                 <ext:Button runat="server" StyleSpec="float:left;" ID="btnRecommendOrApprove"  Text="<i></i>Recommend">
                           <DirectEvents>
                            <Click OnEvent="btnRecommendOrApprove_Click">
                                <EventMask ShowMask="true" />
                                <Confirmation ConfirmRequest="true" Message="Are you srue you want to Forward the selected requests?" />
                                <ExtraParams>
                                    <ext:Parameter Name="gridItems" Value="Ext.encode(#{gridOverTimeReq}.getRowsValues({selectedOnly:true}))"
                                        Mode="Raw" />
                                </ExtraParams>
                            </Click>
                    </DirectEvents>
                        </ext:Button>

            <ext:Button runat="server" AutoPostBack="true" OnClick="btnExport_Click" 
                            ID="btnExport"  Text="<i></i>Export to Excel">
                        </ext:Button>
        </div>
    </div>


    <ext:Window ID="WAssignOvertime" runat="server" Title="Assign Overtime" Icon="Application"
        Width="935" MinHeight="510" BodyPadding="5" Hidden="true" Modal="true" OverflowY="Scroll">
        <Content>
        <br />

        <uc2:MsgCtl ID="divMsgCtl" EnableViewState="false" Hide="true" Width="390px" runat="server" />
        <uc2:WarningCtl ID="divWarningMsg" EnableViewState="false" Hide="true" runat="server" />

            <table class="fieldTable" style="margin-left:20px;">

            <tr>
                <td>
                    <ext:ComboBox ID="cmbOvertimeType" runat="server" ValueField="OvertimeTypeId" DisplayField="Name" FieldLabel="Overtime Type *" Width="200"
                            LabelAlign="top" LabelSeparator="" ForceSelection="true" QueryMode="Local">
                            <Store>
                                <ext:Store ID="Store2" runat="server">
                                    <Model>
                                        <ext:Model ID="Model1" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="OvertimeTypeId" Type="String" />
                                                <ext:ModelField Name="Name" Type="String" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>                            
                        </ext:ComboBox>

                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ValidationGroup="AssignOTPop"
                         Display="None" ErrorMessage="Overtime type is required." ControlToValidate="cmbOvertimeType"></asp:RequiredFieldValidator>
                </td>
                <td>

                    <ext:LinkButton runat="server" StyleSpec="padding:0px;text-decoration: underline; font-size:14px;" ID="btnImport"
                                        OnClientClick="AssignOTImportPopup();return false;" Text="<i></i>Import from Excel">
                                        <Listeners>
                                            <Click Handler="valGroup = 'AssignOTPop';">
                                            </Click>
                                        </Listeners>

                                    </ext:LinkButton>
                </td>
            </tr>
           
           <tr>
            <td colspan="2">
                  <ext:GridPanel StyleSpec="margin-top:15px;" ID="gridOTList" runat="server" Cls="itemgrid"
                                Scroll="None" Width="850">
                                <Store>
                                    <ext:Store ID="Store7" runat="server">
                                        <Model>
                                            <ext:Model ID="OTModel" Name="SettingModel" runat="server" IDProperty="SN">
                                                <Fields>
                                                    <ext:ModelField Name="SN" Type="Int" />
                                                    <ext:ModelField Name="DateEng" Type="Date"/>        
                                                    <ext:ModelField Name="Value" Type="String" />
                                                    <ext:ModelField Name="Text" Type="String" />                                           
                                                    <ext:ModelField Name="InTime" Type="Date"/>
                                                    <ext:ModelField Name="OutTime" Type="Date" />                                             
                                                    <ext:ModelField Name="WorkHours" Type="string" />
                                                    <ext:ModelField Name="OutNote" Type="string" />
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <ColumnModel>
                                    <Columns>
                                        <ext:DateColumn ID="DateColumn1" runat="server" Align="Right" Text="Date" Width="100"
                                            MenuDisabled="true" Sortable="false" Format="yyyy-MM-dd" DataIndex="DateEng">
                                              <Editor>
                                                <ext:DateField  runat="server"
                                                    ID="dfDateAdd">
                                                    <Plugins>
                                                        <ext:GenericPlugin ID="HighlightDates1" runat="server" InstanceName="Ext.ux.DatePicker.HighlightWithNepDates" />
                                                    </Plugins>
                                                </ext:DateField>
                                              </Editor>
                                        </ext:DateColumn>                          

                                         <ext:Column ID="colEmployeeNameAdd" Sortable="false" MenuDisabled="true" runat="server" Text="Employee Name"
                                            Align="Left" Width="200" DataIndex="Value">
                                            <Renderer Fn="EmpNameRender" />
                                            <Editor>
                                                <ext:ComboBox ID="cmbEmployeeAdd" runat="server" DisplayField="Text" ValueField="Value" Width="200" StoreID="storeEmployees"
                                                   ForceSelection="true" QueryMode="Local">                                                    
                                                    <Items>
                                                        <ext:ListItem Text="--Select Employee--" Value="-1" />
                                                    </Items>
                                                    <SelectedItems>
                                                        <ext:ListItem Text="--Select Employee--" Value="-1" />
                                                    </SelectedItems>                            
                                                </ext:ComboBox>
                                            </Editor>
                                        </ext:Column>

                                        <ext:DateColumn ID="colInTime1" Sortable="false" MenuDisabled="true" runat="server" Text="Start Time"
                                            Align="Right" Width="100" DataIndex="InTime" Format="HH:mm">
                                            <Editor>
                                                <ext:TimeField ID="tfInTime" runat="server" MinTime="06:00" MaxTime="23:59" Increment="1" Format="HH:mm"
                                                    SelectedTime="08:00" >
                                                </ext:TimeField>
                                            </Editor>
                                           
                                        </ext:DateColumn>

                                        <ext:DateColumn ID="colOutTime1" Sortable="false" MenuDisabled="true" runat="server" Text="End Time"
                                            Align="Right" Width="100" DataIndex="OutTime" Format="HH:mm">                                                                                                                 
                                            <Editor>
                                                <ext:TimeField ID="tfOutTime" runat="server" MinTime="06:00" MaxTime="23:59" Increment="1" Format="HH:mm"
                                                    SelectedTime="08:00" >
                                                </ext:TimeField>
                                            </Editor>
                                           
                                        </ext:DateColumn>

                                        <ext:Column ID="colWorkHours" Sortable="false" MenuDisabled="true" runat="server"
                                            Text="Hours:Minutes" Align="Center" Width="120" DataIndex="WorkHours">
                                            <Renderer Fn="CalculateWorkHours" />
                                        </ext:Column>

                                        
                                        <ext:Column ID="colReasonAdd" Sortable="false" MenuDisabled="true" runat="server" Text="Reason"
                                            Align="Left" Width="200" DataIndex="OutNote">
                                            <Editor>
                                                <ext:TextField ID="txtOutNote" runat="server">
                                                </ext:TextField>
                                            </Editor>
                                        </ext:Column>

                                        <ext:CommandColumn ID="CommandColumn1" runat="server" Sortable="false" MenuDisabled="true"
                                            Width="30" Align="Center">
                                            <Commands>
                                                <ext:GridCommand Icon="Delete" CommandName="Delete" Cls="deleteicon" />
                                            </Commands>
                                            <Listeners>
                                                <Command Fn="RemoveItemLine">
                                                </Command>

                                            </Listeners>

                                        </ext:CommandColumn>

                                    </Columns>
                                </ColumnModel>
                                <SelectionModel>
                                    <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" Mode="Single" />
                                </SelectionModel>
                                <Plugins>
                                    <ext:CellEditing ID="CellEditing1" runat="server" ClicksToEdit="1">
                                    </ext:CellEditing>
                                </Plugins>
                                <View>
                                    <ext:GridView ID="GridView2" runat="server">
                                        <Plugins>
                                            <ext:GridDragDrop ID="GridDragDrop1" runat="server" DragGroup="ddGroup" DropGroup="ddGroup" />
                                        </Plugins>
                                      
                                    </ext:GridView>
                                </View>
                            </ext:GridPanel>

            </td>
           </tr>
           <tr>
            <td colspan="2">
                 <ext:Button ID="btnAddRow" runat="server" Cls="btn btn-save" Text="Add New Row" Width="130">
                                <Listeners>
                                    <Click Handler="addNewRow(#{gridOTList});" />
                                </Listeners>
                            </ext:Button>
            </td>
           </tr>
          
             <tr>
                <td colspan="2">
                    <div class="popupButtonDiv">                            

                            <ext:Button runat="server" ID="btnAssignOvertime" Cls="btn btn-primary" Text="<i></i>Assign">
                                <DirectEvents>
                                    <Click OnEvent="btnAssignOvertime_Click">
                                        <EventMask ShowMask="true" />
                                        <ExtraParams>
                                            <ext:Parameter Name="gridItems" Value="Ext.encode(#{gridOTList}.getRowsValues({ selectedOnly: false }))"
                                                Mode="Raw" />
                                        </ExtraParams>
                                    </Click>
                                </DirectEvents>
                                <Listeners>
                                    <Click Handler="valGroup = 'SaveAssignOvertime';">
                                    </Click>
                                </Listeners>
                            </ext:Button>      

                            </div>    
                </td>
             </tr>              

                            

                  

         </table>

        <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator5" ValidationGroup="SaveAssignOvertime"
            Display="None" ErrorMessage="Overtime type is required." ControlToValidate="cmbOvertimeType"></asp:RequiredFieldValidator>

        </Content>
    </ext:Window>


     <ext:Window ID="WApproveOvertime" runat="server" Title="Overtime Approval" Icon="Application"
        Width="550" Height="650" BodyPadding="5" Hidden="true" Modal="true">
        <Content>
        <br />

            <ext:Panel ID="pnlBorder" runat="server" StyleSpec="width:450px; height:40px; border:solid 1px red; margin-left:30px; display:none;" Hidden="true">
            <Items>
                <ext:Label ID="lblWarningMsg" StyleSpec="color:red; padding-top:10px; padding-left:10px;" runat="server" />
         </Items>
        </ext:Panel>
        <br />

         <div class="popupHeader">
            <ext:DisplayField ID="dfOvertime" FieldCls="boldCls1" runat="server" Text="Overtime approval for : " />           
        </div>

        <table class="fieldTable" style="margin-left:20px;">
            <tr>
                <td>
                    <ext:DisplayField ID="dfDateTitle" runat="server" FieldCls="boldCls" Text="Date :"/>
                </td>
                <td>
                    <ext:DisplayField ID="dfDate" runat="server" StyleSpec="margin-bottom:10px;"/>
                </td>
            </tr>

            <tr>
                <td>
                    <ext:DisplayField ID="dfNameTitle" runat="server" FieldCls="boldCls" Text="Name :"/>
                </td>
                <td>
                    <ext:DisplayField ID="dfName" runat="server" StyleSpec="margin-bottom:10px;"/>
                </td>
            </tr>

            <tr>
                <td>
                    <ext:DisplayField ID="dfDescriptionTitle" runat="server" FieldCls="boldCls" Text="Description :"/>
                </td>
                <td>
                    <ext:DisplayField ID="dfDescription" runat="server" StyleSpec="margin-bottom:10px;"/>
                </td>
            </tr>

            <tr>
                    <td>
                        <ext:DisplayField ID="dfInTimeTitle" runat="server" FieldCls="boldCls" Text="In Time :" />
                    </td>
                    <td>
                        <ext:DisplayField ID="dfInTime" runat="server" StyleSpec="margin-bottom:10px;"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <ext:DisplayField ID="dfOutTimeTitle" runat="server" FieldCls="boldCls" Text="Out Time :" />
                    </td>
                    <td>
                        <ext:DisplayField ID="dfOutTime" runat="server" StyleSpec="margin-bottom:10px;"/>
                    </td>
                </tr>
        </table>

        <table class="fieldTable" style="margin-left:20px;">

            <tr>
                <td>
                    <ext:DisplayField ID="dfRequestedHourTitle" runat="server" FieldCls="boldCls" Text="Requested Hour :" />
                </td>
            </tr>
            <tr>
                <td>
                    <ext:TextField ID="txtRequestedHour" runat="server"  FieldLabel="" Width="100" /> 
                </td>
            </tr>
            <tr>
                <td>
                    <ext:DisplayField ID="dfApprovedForTitle" runat="server" FieldCls="boldCls" Text="Approve For :" />
                </td>
            </tr>
            <tr>
                <td>
                    Hour<br />
                    <ext:TextField ID="txtApprovedForHour" runat="server" FieldLabel="" Width="100" /> 
                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator6" ValidationGroup="ApproveOvertime"
                        ControlToValidate="txtApprovedForHour" Display="None" ErrorMessage="Approve hour is required."></asp:RequiredFieldValidator>
                </td>
                <td>
                    Minute<br />
                    <ext:TextField ID="txtApprovedForMin" runat="server" FieldLabel="" Width="100" /> 
                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator7" ValidationGroup="ApproveOvertime"
                        ControlToValidate="txtApprovedForMin" Display="None" ErrorMessage="Approve minute is required."></asp:RequiredFieldValidator>
                </td>
            </tr>
        </table>
        <br />

        <table class="fieldTable" style="margin-left:20px;">    
            <tr>
               <td style="width:400px;">
                        
                    <ext:Button runat="server" ID="btnApprove" Cls="btn btn-primary" StyleSpec="color:red;" Text="<i></i>Approve">
                                <DirectEvents>
                                    <Click OnEvent="btnApprove_Click">
                                        <EventMask ShowMask="true" />
                                        <Confirmation ConfirmRequest="true" Message="Are you sure you want to approve this request?" />
                                    </Click>
                                </DirectEvents>
                                <Listeners>
                                    <Click Handler="valGroup = 'ApproveOvertime'; if(CheckValidation()) return this.disable(); else return false;">
                                    </Click>
                                </Listeners>
                            </ext:Button>

                    <ext:Button runat="server" ID="btnRecommend" Cls="btn btn-primary" Text="<i></i>Recommend">
                                <DirectEvents>
                                    <Click OnEvent="btnRecommend_Click">
                                        <EventMask ShowMask="true" />
                                        <Confirmation ConfirmRequest="true" Message="Are you sure you want to recommend this request?" />
                                    </Click>
                                </DirectEvents>
                                <Listeners>
                                    <Click Handler="valGroup = 'ApproveOvertime'; if(CheckValidation()) return this.disable(); else return false;">
                                    </Click>
                                </Listeners>
                            </ext:Button>
                        </td>
                        <td>

                    <ext:Button runat="server" ID="btnReject" Cls="btn btn-primary" Text="<i></i>Reject">
                                <DirectEvents>
                                    <Click OnEvent="btnReject_Click">
                                        <EventMask ShowMask="true" />
                                        <Confirmation ConfirmRequest="true" Message="Are you sure you want to Reject this request?" />
                                    </Click>
                                </DirectEvents>
                                <Listeners>                                    
                                </Listeners>
                            </ext:Button>
                            
                </td>
            </tr>            

        </table>

        <br />
      
            <ext:GridPanel ID="gridHistory" runat="server" Cls="itemgrid" StyleSpec="margin-left:25px;">
                <Store>
                    <ext:Store ID="Store3" runat="server">
                        <Model>
                            <ext:Model ID="Model3" runat="server" IDProperty="LineID">
                                <Fields>
                                    <ext:ModelField Name="LineID" Type="Int" />
                                    <ext:ModelField Name="ModifiedByName" Type="String" />
                                    <ext:ModelField Name="ModifiedOn" Type="string" />

                                    <ext:ModelField Name="Column1Before" Type="string" />
                                    <ext:ModelField Name="Column1After" Type="string" />
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                <ColumnModel>
                    <Columns>
                        <ext:Column ID="colModifiedByName" Sortable="false" MenuDisabled="true" runat="server" Text="Modified by"
                            Align="Left" Width="160" DataIndex="ModifiedByName" />
                        <ext:DateColumn ID="colModifiedOn" runat="server" Align="Left" Text="ModifiedOn" Width="100"
                            MenuDisabled="true" Sortable="false" Format="yyyy-MM-dd" DataIndex="ModifiedOn">
                        </ext:DateColumn>
                        <ext:Column ID="colColumn1Before" Sortable="false" MenuDisabled="true" runat="server" Text="Before"
                            Align="Left" Width="120" DataIndex="Column1Before" />
                        <ext:Column ID="colColumn1After" Sortable="false" MenuDisabled="true" runat="server" Text="After"
                            Align="Left" Width="120" DataIndex="Column1After" />
                    </Columns>
                </ColumnModel>
                <SelectionModel>
                    <ext:RowSelectionModel ID="RowSelectionModel2" runat="server" Mode="Single" />
                </SelectionModel>
            </ext:GridPanel>
       




         </Content>
    </ext:Window>


</asp:Content>
