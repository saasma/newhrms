﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using Utils.Helper;
using Utils.Web;
using BLL.Base;
using DAL;
using Utils.Calendar;
using BLL.BO;

namespace Web.Employee
{
    public partial class AllowanceApprove : BasePage
    {


        //private int _tempCurrentPage;
        //private int _tempCount;
        private PayManager payMgr = new PayManager();

        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (!IsPostBack)
            {

                List<Branch> branches = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId); ;
                //if (branches.Count == 1)
                //{

                //    tdBranch2.Visible = false;
                //    tdBranch.Visible = false;
                //    ddlBranch.SelectedIndex = 1;

                //}
                ddlBranch.DataSource = branches;
                ddlBranch.DataBind();
               // ddlBranch.SelectedIndex = 1;

                Initialise();

                if (LeaveAttendanceManager.IsEmployeeAllowedToApproveLeave(SessionManager.CurrentLoggedInEmployeeId))
                    btnRecommendOrApprove.Text = "Approve";

            }
            JavascriptHelper.AttachNonDialogPopUpCode(Page, "assignAllowancePopup", "AllowanceAssignPopup.aspx", 475, 550);
            JavascriptHelper.AttachNonDialogPopUpCode(Page, "positionHistoryPopup", "AllowanceApprovePopup.aspx", 600, 550);
        
        }

        public bool CanRecommendOrApprove(int status, int employeeId)
        {
            if (status == (int)OvertimeStatusEnum.Recommended &&
                LeaveRequestManager.CanApprove(employeeId, SessionManager.CurrentLoggedInEmployeeId,PreDefindFlowType.Overtime))
                return true;

            if (status == 0)
                return true;

            return false;
        }


        protected void btnRecommendOrApprove_Click(object sender, EventArgs e)
        {

            bool employeeAdded = false;
            List<EveningCounterRequest> requestList = new List<EveningCounterRequest>();
            foreach (GridViewRow row in gvw.Rows)
            {


                EveningCounterRequest request = new EveningCounterRequest();
                int RequestID = int.Parse(gvw.DataKeys[row.RowIndex]["RequestID"].ToString());


                if (row.FindControl("chkDelete") == null)
                    continue;

                CheckBox chkDelete = (CheckBox)row.FindControl("chkDelete");

                if (chkDelete.Checked)
                {
                    employeeAdded = true;

                    request.CounterRequestID = RequestID;
                    requestList.Add(request);
                }
            }

            int count = 0;
            if (AllowanceManager.RecommendOrApprove(requestList, out count))
            {
                LoadEmployees();

                

                divMsgCtl.InnerHtml = count + " Allowance has been forwarded.";
                divMsgCtl.Hide = false;
            }

        }


        void Initialise()
        {

            // if has Overtime approval permission then set default status filter to Approve
            //if (LeaveAttendanceManager.IsEmployeeAllowedToApproveOvertime())
            //{
            //    ddlStatus.SelectedValue = "-1";
            //}

            List<KeyValue> statues = new JobStatus().GetMembers();

            ddlAllowanceType.DataSource = AllowanceManager.GetAllEveningCounterList()
                .Where(x => x.DoNotShowInEmployeePortal == false || x.DoNotShowInEmployeePortal == null).ToList();
            ddlAllowanceType.DataBind();

            LoadEmployees();
        }
        protected void btnLoad_Click(object sender, EventArgs e)
        {
        
            LoadEmployees();
        }
        protected void gvwEmployees_RowCreated(object sender, GridViewRowEventArgs e)
        {
            //if (e.Row.RowType == DataControlRowType.DataRow)
            //{
            //    e.Row.Attributes.Add("onmouseover",
            //    "this.originalClass=this.className;this.className='selected'");

            //    e.Row.Attributes.Add("onmouseout",
            //    "this.className=this.originalClass;");
            //}

        }
        protected void LoadEmployees()
        {
            int type=1;
            int status = -1;


            if( ddlType.SelectedItem != null)
                type = int.Parse(ddlType.SelectedValue);

            if (ddlStatus.SelectedItem != null)
                status = int.Parse(ddlStatus.SelectedValue);

            DateTime? start = null;
            DateTime? end = null;

            DateTime date;
            if (ddlType.SelectedValue == "-1")
            {
                if (DateTime.TryParse(dateFrom.Text, out date))
                    start = dateFrom.SelectedDate;
                if (DateTime.TryParse(dateTo.Text, out date))
                    end = dateTo.SelectedDate;
            }

            if (start != null && start == DateTime.MinValue)
                start = null;

            if (end != null && end == DateTime.MinValue)
                end = null;

           

            int BranchID = -1;

            if (ddlBranch.SelectedIndex != 0)
            {
                BranchID = int.Parse(ddlBranch.SelectedValue);
            }


            int totalRecords = 0;

            List<GetEveningCounterRequestForManagerResult> list = OvertimeManager.GetEveningCounterForManager
                (type, status, pagintCtl.CurrentPage, int.Parse(pagintCtl.DDLRecords.SelectedValue)
                , ref totalRecords, start, end, txtEmpSearchText.Text.Trim(), BranchID, int.Parse(ddlAllowanceType.SelectedValue));

            foreach (var item in list)
            {
                if (item.Status != null)
                {
                    item.StatusModified = ((EveningCounterStatusEnum)item.Status.Value).ToString();
                }

                if (item.StartDate != null)
                {
                    DateTime startDate = item.StartDate.Value;
                    CustomDate cdStartDate = new CustomDate(startDate.Day, startDate.Month, startDate.Year, true);
                    CustomDate cdNepaliDate = CustomDate.ConvertEngToNep(cdStartDate);
                    item.StartDateNep = DateHelper.GetMonthName(cdNepaliDate.Month, false) + " " + cdNepaliDate.Day.ToString();
                }

                if (item.EndTime != null)
                {
                    DateTime endDate = item.EndTime.Value;
                    CustomDate cdEndDate = new CustomDate(endDate.Day, endDate.Month, endDate.Year, true);
                    CustomDate cdNepaliDate = CustomDate.ConvertEngToNep(cdEndDate);
                    item.EndDateNep = DateHelper.GetMonthName(cdNepaliDate.Month, false) + " " + cdNepaliDate.Day.ToString();
                }
            }
            

            gvw.DataSource = list;
            gvw.DataBind();


            pagintCtl.UpdatePagingBar(totalRecords);

            
        }

        public string GetEngOrNepDate(object value)
        {
            if (value == null)
                return "";

            DateTime date = Convert.ToDateTime(value);

            CustomDate c = new CustomDate(date.Day, date.Month, date.Year, true);

            if (IsEnglish)
                return c.ToString();

            else
                return CustomDate.ConvertEngToNep(c).ToString();
        }

       
        protected void btnExport_Click(object sender, EventArgs e)
        {
            GridViewExportUtil.Export("Allowane List.xls", gvw);
        }   

        

        protected void txtEmployeeName_TextChanged(object sender, EventArgs e)
        {
            
        }

        protected void gvwRoles_PageIndexChanged(object sender, EventArgs e)
        {

        }

        protected void gvwRoles_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {

            LoadEmployees();
            //Clear();
        }

        protected void btnNext_Click()
        {
            pagintCtl.CurrentPage += 1;
            LoadEmployees();
        }



        protected void btnPrevious_Click()
        {
            pagintCtl.CurrentPage -= 1;
            LoadEmployees();
        }

        protected void ddlRecords_SelectedIndexChanged()
        {
            pagintCtl.CurrentPage = 1;
            LoadEmployees();
        }

        


    }
}
