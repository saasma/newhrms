﻿<%@ Page Title="Approve Overtime" Language="C#" MasterPageFile="~/Master/EmployeeExt.Master" AutoEventWireup="true" CodeBehind="AApproveOvertimeNewNew.aspx.cs" Inherits="Web.Employee.AApproveOvertimeNewNew" %>

<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">

<style type="text/css">
    
   .boldCls1
   {
       font-weight:bold;
       margin-left:20px;
       padding-left:8px;
       display:block;
   } 
   .boldCls
   {
       font-weight:bold;
   } 
   
   .x-progress-text
   {
       background-color:White;
   }
 
</style>


<script type="text/javascript">

    var CommandHandler = function(command, record){
            
            if(command=="Edit")
            {
                overtimeEdit('RId=' + record.data.RequestID);
            }

    };

    var checkboxRenderer = function (v, p, record) {
            if(record.data.CanRecOrAppRec == false){            
                return "";
            }
            
          return '<div class="x-grid-row-checker">&nbsp;</div>'
        };

      function AssignOvertimePopup()
        {
            overtimeAdd();
        }

    function refreshWindow() {
            <%=btnLoadRef.ClientID %>.fireEvent('click');
        }

</script>


</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="mainContent" runat="server">

<ext:LinkButton runat="server" Hidden="true" ID="btnLoadRef">
    <DirectEvents>
        <Click OnEvent="btnLoadRef_Click">
            <EventMask ShowMask="true" />
        </Click>
    </DirectEvents>
</ext:LinkButton>


<div class="contentArea" style="margin-top:10px">


    <div class="attribute" style="padding:10px">
            
            <table>
                <tr>
                    <td rowspan="2">  
                        
                        <ext:Button ID="btnAssignOT" runat="server" Cls="btn btn-save" Text="Assign Overtime" Width="130" OnClientClick="AssignOvertimePopup();return false;">
                        </ext:Button> 
                         
                    </td>
                    
                    <td style="padding-left:10px;">

                        <ext:ComboBox ID="cmbType" runat="server" Width="150" LabelWidth="40"
                            FieldLabel="Period" LabelAlign="Left" LabelSeparator="" ForceSelection="true"
                            QueryMode="Local">
                            <Items>
                                <ext:ListItem Text="All" Value="-1" />
                                <ext:ListItem Text="This Week" Value="1" />
                                <ext:ListItem Text="This Month" Value="2" />
                                <ext:ListItem Text="Last Month" Value="3" />
                            </Items>      
                            <SelectedItems>
                                <ext:ListItem Text="All" Value="-1" />
                            </SelectedItems>                   
                        </ext:ComboBox>

                    </td>

                    <td style="padding-left:5px;">

                        <ext:ComboBox ID="cmbStatus" runat="server" Width="150" LabelWidth="40"
                            FieldLabel="" LabelAlign="Left" LabelSeparator="" ForceSelection="true"
                            QueryMode="Local">
                            <Items>
                                <ext:ListItem Text="All" Value="-1" />
                                <ext:ListItem Text="Pending" Value="0" />
                                <%--<ext:ListItem Text="Recommended" Value="1" />--%>
                                <ext:ListItem Text="Approved" Value="2" />
                                <ext:ListItem Text="Rejected" Value="3" />
                            </Items>      
                            <SelectedItems>
                                <ext:ListItem Text="Pending" Value="0" />
                            </SelectedItems>                   
                        </ext:ComboBox>
                    </td>

                    <td valign="top" style="padding-bottom: 8px;padding-left:15px;">                      
                        <ext:Button ID="btnLoad" runat="server" Cls="btn btn-save" Text="Load" Width="130">
                            <DirectEvents>
                                <Click OnEvent="btnLoad_Click">                               
                                    <EventMask ShowMask="true" />                              
                                </Click>
                            </DirectEvents>
                        </ext:Button>     
                    </td>
                </tr>
            </table>
           
        </div>
        <div class="clear">

            <ext:GridPanel StyleSpec="margin-top:15px;" ID="gridOverTimeReq" runat="server" Cls="itemgrid" Scroll="None">
            <Store>
                <ext:Store ID="Store1" runat="server" OnReadData="MyData_Refresh" PageSize="10">
                    <Model>
                        <ext:Model ID="Model6" runat="server" IDProperty="RequestID">
                            <Fields>
                                <ext:ModelField Name="RequestID" Type="String" />
                                <ext:ModelField Name="EmployeeID" Type="String" />                                
                                <ext:ModelField Name="EmployeeName" Type="String" />
                                <ext:ModelField Name="Date" Type="String" />
                                <ext:ModelField Name="StartTime" Type="string" />                                
                                <ext:ModelField Name="EndTime" Type="string" />
                                <ext:ModelField Name="CheckInTime" Type="string" />
                                <ext:ModelField Name="CheckOutTime" Type="string" />                                                               
                                <ext:ModelField Name="DurationModified" Type="string" />
                                <ext:ModelField Name="ApprovedTime" Type="string" />
                                <ext:ModelField Name="Reason" Type="string" />
                                <ext:ModelField Name="StatusModified" Type="string" />
                                <ext:ModelField Name="CanRecOrAppRec" Type="Boolean" />
                            </Fields>
                        </ext:Model>
                    </Model>
                </ext:Store>
            </Store>
            <ColumnModel>
                <Columns>
                    <ext:Column ID="Column25" Sortable="false" MenuDisabled="true" runat="server" Text="RequestID" Visible="false"
                        Align="Left" Width="150" DataIndex="RequestID" />
                    <ext:Column ID="Column1" Sortable="false" MenuDisabled="true" runat="server" Text="EmployeeID" Visible="false"
                        Align="Left" Width="150" DataIndex="EmployeeID" />

                    <ext:Column ID="colDate" runat="server" Align="Left" Text="Date" Width="100"
                        MenuDisabled="true" Sortable="false" DataIndex="Date">
                    </ext:Column>

                    <ext:Column ID="colEmployeeName" Sortable="false" MenuDisabled="true" runat="server" Text="Employee Name"
                        Align="Left" Width="200" DataIndex="EmployeeName" />

                    <ext:Column ID="colStartTime" Sortable="false" MenuDisabled="true" runat="server" Text="Start Time" Width="80"
                        Align="Left" DataIndex="StartTime">
                    </ext:Column>
                    <ext:Column ID="colEndTime" Sortable="false" MenuDisabled="true" runat="server" Text="End Time" Width="80"
                        Align="Left" DataIndex="EndTime">
                    </ext:Column>
                    <ext:Column ID="colCheckInTime" Sortable="false" MenuDisabled="true" runat="server" Text="In Time"
                        Align="Left" Width="80" DataIndex="CheckInTime">
                    </ext:Column>
                    <ext:Column ID="colCheckOutTime" Sortable="false" MenuDisabled="true" runat="server" Text="Out Time"
                        Align="Left" Width="80" DataIndex="CheckOutTime">
                    </ext:Column>

                    <ext:Column ID="colDurationModified" Sortable="false" MenuDisabled="true" runat="server" Text="Duration"
                        Align="Left" Width="100" DataIndex="DurationModified">
                    </ext:Column>
                    <ext:Column ID="colApprovedTime" Sortable="false" MenuDisabled="true" runat="server" Text="Approved Time"
                        Align="Left" Width="100" DataIndex="ApprovedTime">
                    </ext:Column>
                    <ext:Column ID="colReason" Sortable="false" MenuDisabled="true" runat="server" Text="Reason"
                        Align="Left" Width="200" DataIndex="Reason">
                    </ext:Column>

                     <ext:Column ID="colStatusModified" Sortable="false" MenuDisabled="true" runat="server" Text="Status"
                        Align="Left" Width="100" DataIndex="StatusModified">
                    </ext:Column>
                   
                    <ext:CommandColumn ID="CommandColumn3" runat="server" Width="75" Text="Actions" Align="Center">
                        <Commands>
                            <ext:CommandSeparator />
                            <ext:GridCommand ToolTip-Text="Edit" Icon="ApplicationEdit" CommandName="Edit" />
                        </Commands>
                        <Listeners>
                            <Command Handler="CommandHandler(command,record);" />
                        </Listeners>
                       
                    </ext:CommandColumn>
                </Columns>
            </ColumnModel>
            <SelectionModel>
                <ext:CheckboxSelectionModel ID="CheckboxSelectionModel1" runat="server">
                    <CustomConfig>
                        <ext:ConfigItem Name="renderer" Value="checkboxRenderer" Mode="Raw" />
                    </CustomConfig>
                    <Listeners>
                        <BeforeSelect Handler="return record.data.CanRecOrAppRec == true;" />
                    </Listeners>
                </ext:CheckboxSelectionModel>
            </SelectionModel>       
            <View>
                <ext:GridView ID="GridView1" runat="server" StripeRows="true" />                   
            </View>            
            <BottomBar>
                 <ext:PagingToolbar ID="PagingToolbar1" runat="server">
                    <Items>
                        <ext:Label ID="Label1" runat="server" Text="Page size:" />
                        <ext:ToolbarSpacer ID="ToolbarSpacer1" runat="server" Width="10" />
                        <ext:ComboBox ID="ComboBox1" runat="server" Width="80">
                            <Items>
                                <ext:ListItem Text="20" />
                                <ext:ListItem Text="30" />
                                <ext:ListItem Text="50" />
                                <ext:ListItem Text="100" />
                            </Items>
                            <SelectedItems>
                                <ext:ListItem Value="20" />
                            </SelectedItems>
                            <Listeners>
                                <Select Handler="#{gridOverTimeReq}.store.pageSize = parseInt(this.getValue(), 20); #{gridOverTimeReq}.store.reload();" />
                            </Listeners>
                        </ext:ComboBox>
                    </Items>
                    <Plugins>
                        <ext:ProgressBarPager ID="ProgressBarPager1" runat="server" />
                    </Plugins>
                </ext:PagingToolbar>
            </BottomBar>
        </ext:GridPanel>

         

        </div>
        <div class="buttonsDiv">           

                 <ext:Button runat="server" StyleSpec="float:left;" ID="btnRecommendOrApprove"  Text="<i></i>Recommend">
                           <DirectEvents>
                            <Click OnEvent="btnRecommendOrApprove_Click">
                                <EventMask ShowMask="true" />
                                <Confirmation ConfirmRequest="true" Message="Are you srue you want to Forward the selected requests?" />
                                <ExtraParams>
                                    <ext:Parameter Name="gridItems" Value="Ext.encode(#{gridOverTimeReq}.getRowsValues({selectedOnly:true}))"
                                        Mode="Raw" />
                                </ExtraParams>
                            </Click>
                    </DirectEvents>
                        </ext:Button>

            <ext:Button runat="server" AutoPostBack="true" OnClick="btnExport_Click" 
                            ID="btnExport"  Text="<i></i>Export to Excel">
                        </ext:Button>
        </div>


</div>

</asp:Content>
