<%@ Page MaintainScrollPositionOnPostback="true" Title="Payroll Message" Language="C#"
    AutoEventWireup="true" CodeBehind="PayrollMessage.aspx.cs" Inherits="Web.Employee.PayrollMessage" %>

<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Employee/UserControls/PayrollMessageInbox.ascx" TagName="Inbox"
    TagPrefix="uc1" %>
<%@ Register Src="~/Controls/FooterCtl.ascx" TagName="FooterCtl" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/HeaderCtl.ascx" TagName="HeaderCtl" TagPrefix="uc3" %>
<!DOCTYPE html>
<html>
<head runat="server" >
     <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Leave Approval</title>
  <link id="Link5" runat="server" rel="stylesheet" href="~/payrollmenu_files/mbcsmbmcp.css" type="text/css" />
  
    <link id="Link1" runat="server" rel="stylesheet" type="text/css" href="~/css/core.css" />
    <link id="Link2" runat="server" rel="Stylesheet" type="text/css" href="~/Styles/calendar/calendar.css" />
  
     <link id="Link4" runat="server" rel="stylesheet" href="~/Styles/design.css" />

         
    <style type="text/css">
               #mbmcpebul_table{margin-top:0px!important;}
        .details
        {
            padding-top: 10px;
        }
        .details tr td
        {
            padding-top: 4px;
        }
        
        a.x-datepicker-eventdate
        {
            border: 1px solid #a3bad9;
            padding: 1px; /* background-color: Cyan;*/
        }
        
        .icon-delete
        {
            background-image: url("../images/delet.png") !important;
        }
        .bodypart{margin: 0 auto!important;}
    </style>

    <%--<script type="text/javascript">
        Ext.onReady(function () {

            var queryPart = window.location.search;
            var queryValue;
            if (queryPart == "") {
                queryValue = null;
            }
            else {
                queryValue = queryPart.substring(queryPart.indexOf("=") + 1);
            }
            console.log(queryValue);
        });
    </script>--%>
</head>
<body style="margin:0px">
    <form id="Form1" runat="server">
    <ext:ResourceManager ID="ResourceManager1" DisableViewState="false"  runat="server" ShowWarningOnAjaxFailure="false"  ScriptMode="Release" Namespace="CompanyX" />
    <uc3:HeaderCtl ID="HeaderCtl1" runat="server" />
    <div id="bodypart">
        <div class="bodypart">
            <ext:Hidden ID="hdUserName" runat="server" />
            <ext:Hidden ID="hdBody" runat="server" />
            <uc1:Inbox ID="Inbox1" runat="server" />
        </div>
    </div>
    <uc1:FooterCtl ID="FooterCtl1" runat="server" />
    </form>
</body>
</html>
