﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Manager;
using DAL;
using Utils.Helper;
using Utils.Web;
using System.Text;
using BLL;
using System.Text.RegularExpressions;
using Utils.Calendar;

namespace Web.Employee
{

    
    public partial class AApproveOvertimePopup : System.Web.UI.Page
    {
        InsuranceManager insMgr = new InsuranceManager();
        List<IInsuranceInstitution> source = new List<IInsuranceInstitution>();
        public bool IsDisplayedAsPopup
        {
            get
            {
                if (Request.QueryString["isPopup"] != null)
                    return true;
                return false;
            }
        }

        public bool HasImport
        {
            get
            {
                if (ViewState["import"] == null)
                    return false;
                return Convert.ToBoolean(ViewState["import"]);
            }
            set
            {
                ViewState["import"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                Initialise();

        }

        void Initialise()
        {
            if (OvertimeManager.IsRequestAutoGroupType)
            {
                txtMinute.Visible = false;
                colMin.Visible = false;
            }

            Hidden_RequestID.Value = Request.QueryString["reqid"];
            Load();
        }

        new void Load()
        {
            string hr;
            string min;
            string hrmin;
            string employeename;

            
            OvertimeRequest request = new OvertimeRequest();
            request = OvertimeManager.getRequestByID(Hidden_RequestID.Value);

            if (request != null)
            {
                gridHistory.DataSource = AllowanceManager.GetAllowanceHistory(request.OvertimeRequestID, (int)RequestHistoryTypeEnum.Overtime);
                gridHistory.DataBind();
            }


            if (OvertimeManager.IsRequestAutoGroupType)
            {
            }
            else
                min = (request.StartTime.Value.Subtract(request.EndTime.Value).TotalMinutes).ToString();
            employeename = EmployeeManager.GetEmployeeById(request.EmployeeID.Value).Name;

            spanOvertime.InnerHtml += " " + employeename;

            //lblDate.Text = request.Date.Value.ToShortDateString();

            DateTime requestDate = request.Date.Value;
            CustomDate cd = new CustomDate(requestDate.Day, requestDate.Month, requestDate.Year, true);

            CustomDate cdNepaliDate = CustomDate.ConvertEngToNep(cd);

            lblDate.Text = DateHelper.GetMonthName(request.Date.Value.Month, true) + " " + request.Date.Value.Day.ToString() + "," + requestDate.Year.ToString() + " (" + DateHelper.GetMonthName(cdNepaliDate.Month, false) + " " + cdNepaliDate.Day.ToString() + ")";

            lblName.Text = employeename;
            lblReason.Text = request.Reason;
            DateTime? date = null;
            if (AttendanceManager.GetEmployeeCheckInOutTime(request.EmployeeID.Value, request.Date.Value, "CheckIn") != null)
            {
                date = AttendanceManager.GetEmployeeCheckInOutTime(request.EmployeeID.Value, request.Date.Value, "CheckIn");
                if (date != null)
                    lblInTime.Text = date.Value.ToShortTimeString();
            }

            if (AttendanceManager.GetEmployeeCheckInOutTime(request.EmployeeID.Value, request.Date.Value, "CheckOut") != null)
            {
                date = AttendanceManager.GetEmployeeCheckInOutTime(request.EmployeeID.Value, request.Date.Value, "CheckOut");
                if (date != null)
                    lblOutTime.Text = date.Value.ToShortTimeString();
            }

            
           
            int minute = 0;

            if (request.RequestedTimeInMin != null)
                minute = request.RequestedTimeInMin.Value;
            
            hrmin = new AttendanceManager().MinuteToHourMinuteConverter(minute);
            txtRequested.Text = hrmin;
            Hidden_MinutesPrev.Value = txtRequested.Text;


            hr = hrmin.Split(':').First();
            min = hrmin.Split(':').Last();


            if (request.ApprovedTimeInMinute != null)
            {
                var span = System.TimeSpan.FromMinutes(request.ApprovedTimeInMinute.Value);
                var hours = ((int)span.TotalHours).ToString();
                var minutes = span.Minutes.ToString();

                txtHour.Text = hours;
                txtMinute.Text = minutes;
            }
            else
            {
                txtHour.Text = Regex.Match(hr, @"\d+").Value;
                txtMinute.Text = Regex.Match(min, @"\d+").Value;
            }

            if (request.Status == (int)OvertimeStatusEnum.Pending)
            {
                btnRecommend.Visible = true;
                btnApprove.Visible = false;

                if (request.RecommendedBy == request.ApprovalID)
                {
                    btnRecommend.Visible = false;
                    btnApprove.Visible = true;
                }
            }
            else if (request.Status == (int)OvertimeStatusEnum.Recommended)
            {
                btnRecommend.Visible = false;
                btnApprove.Visible = true;

                
            }

            if (request.Status == (int)OvertimeStatusEnum.Pending && request.RecommendedBy==SessionManager.CurrentLoggedInEmployeeId)
            {
            }
            else if ((request.Status == (int)OvertimeStatusEnum.Forwarded || request.Status==(int)OvertimeStatusEnum.Rejected)
                ||
                (request.RecommendedBy == SessionManager.CurrentLoggedInEmployeeId && request.ApprovalID != SessionManager.CurrentLoggedInEmployeeId) 
                ||
                (request.Status==(int)OvertimeStatusEnum.Approved)
                )
            {

                divWarningMsg.InnerHtml = "Status has been changed. This request cannot be edited.";
                divWarningMsg.Hide = false;

                txtHour.Enabled = false;
                txtMinute.Enabled = false;
                btnApprove.Visible = false;
                btnRecommend.Visible = false;
                btnReject.Visible = false;
            }

        }

       

        protected void Page_PreRender(object sender, EventArgs e)
        {
            //Page.ClientScript.RegisterOnSubmitStatement(this.GetType(), "sdfdsffd", "window.onunload = null;");
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "sdf", "var hasImport = " + (this.HasImport ? "true;" : "false;"), true);


            //output all as js array to be updatable in parent window
            //if (IsDisplayedAsPopup && source != null)
            //{
            //    //Page.ClientScript.
            //    StringBuilder str = new StringBuilder("");
            //    bool first = true;
            //    foreach (IInsuranceInstitution obj in source)
            //    {
            //        if (first == false)
            //            str.Append(",");
            //        str.Append("'" + obj.Id + "$$" + obj.InstitutionName + "'");
            //        first = false;
            //    }
            //    Page.ClientScript.RegisterArrayDeclaration("texts", str.ToString());
            //}
        }

        protected void btnApprove_Click(object sender, EventArgs e)
        {
             Page.Validate("SaveUpdate");
             if (Page.IsValid)
             {
                 OvertimeRequest requestInstance = new OvertimeRequest();

                 requestInstance.OvertimeRequestID = int.Parse(Hidden_RequestID.Value);
                 requestInstance.ApprovedTimeInMinute = (int.Parse(txtHour.Text) * 60) + int.Parse(txtMinute.Text);

                 double totalHour = TimeSpan.FromMinutes(requestInstance.ApprovedTimeInMinute.Value).TotalHours;

                 if (totalHour >= 24)
                 {
                     JavascriptHelper.DisplayClientMsg("Approval hour should be under 24 hrs, current total hour is "
                          + totalHour.ToString("n2") + ".",this.Page);
                     return;
                 }

                 requestInstance.ApprovedTime = txtHour.Text + ":" + txtMinute.Text + "";
                 requestInstance.Status = (int)OvertimeStatusEnum.Approved;


                 List<RequestEditHistory> edits = new List<RequestEditHistory>();
                 RequestEditHistory edit = new RequestEditHistory();
                 if (Hidden_MinutesPrev.Value.Trim().ToLower() != requestInstance.ApprovedTimeInMinute.Value.ToString().Trim().ToLower())
                 {
                     edit = new RequestEditHistory();
                     edit.Column1Before = Hidden_MinutesPrev.Value;
                     edit.Column1After = requestInstance.ApprovedTime;
                     edits.Add(edit);
                     Hidden_MinutesPrev.Value = requestInstance.ApprovedTime;
                 }

                 Status status = OvertimeManager.ApproveRequest(requestInstance, edits);
                 if (status.IsSuccess)
                 {
                     this.HasImport = true;
                     JavascriptHelper.DisplayClientMsg("Overtime has been approved.", this, "closePopup();\n");
                 }
                 else
                 {
                     divWarningMsg.InnerHtml = status.ErrorMessage;
                     divWarningMsg.Hide = false;
                 }
                  
             }
             else
             {
                 JavascriptHelper.DisplayClientMsg("Validation Failed", this);
             }
        }
        protected void btnRecommend_Click(object sender, EventArgs e)
        {
            Page.Validate("SaveUpdate");
            if (Page.IsValid)
            {
                OvertimeRequest requestInstance = new OvertimeRequest();

                requestInstance.OvertimeRequestID = int.Parse(Hidden_RequestID.Value);
                requestInstance.ApprovedTimeInMinute = (int.Parse(txtHour.Text) * 60) + int.Parse(txtMinute.Text);
                requestInstance.ApprovedTime = txtHour.Text + ":" + txtMinute.Text + "";

                double totalHour = TimeSpan.FromMinutes(requestInstance.ApprovedTimeInMinute.Value).TotalHours;
                if (totalHour >= 24)
                {
                    JavascriptHelper.DisplayClientMsg("Approval hour should be under 24 hrs, current total hour is "
                         + totalHour.ToString("n2") + ".", this.Page);
                    return;
                }

                requestInstance.Status = (int)OvertimeStatusEnum.Recommended;

                List<RequestEditHistory> edits = new List<RequestEditHistory>();
                RequestEditHistory edit = new RequestEditHistory();
                if (Hidden_MinutesPrev.Value.Trim().ToLower() != requestInstance.ApprovedTimeInMinute.Value.ToString().Trim().ToLower())
                {
                    edit = new RequestEditHistory();
                    edit.Column1Before = Hidden_MinutesPrev.Value;
                    edit.Column1After = requestInstance.ApprovedTime;
                    edits.Add(edit);
                    Hidden_MinutesPrev.Value = requestInstance.ApprovedTime;
                }

                Status status = OvertimeManager.RecommendRequest(requestInstance, edits);
                if (status.IsSuccess)
                {
                    this.HasImport = true;
                    JavascriptHelper.DisplayClientMsg("Overtime has been recommended.", this, "closePopup();\n");
                }
                else
                {
                    divWarningMsg.InnerHtml = status.ErrorMessage;
                    divWarningMsg.Hide = false;
                }

            }
            else
            {
                JavascriptHelper.DisplayClientMsg("Validation Failed", this);
            }
        }
        protected void btnReject_Click(object sender, EventArgs e)
        {
            int requestId = int.Parse(Hidden_RequestID.Value);
            bool isSuccess = OvertimeManager.RejectRequest(requestId); 
            
            if (isSuccess)
            {
                this.HasImport = true;  
                JavascriptHelper.DisplayClientMsg("Overtime has been rejected.", this, "closePopup();\n");
            }

        }


       

    }
}
