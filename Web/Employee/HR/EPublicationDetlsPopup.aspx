﻿<%@ Page Title="Publication Details" Language="C#" MasterPageFile="~/Master/ForEmployeePopupPage.Master" AutoEventWireup="true" CodeBehind="EPublicationDetlsPopup.aspx.cs" Inherits="Web.Employee.HR.EPublicationDetlsPopup" %>

<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">


<script type="text/javascript">

    var skipLoadingCheck = true;


    function closePopup() {
        window.close();
        window.opener.reloadPublicGrid();
    }

</script>
 <style type="text/css">
        input[type=text], textarea, select
        {
            border: 1px solid #A2B4C6;
        }
        textarea
        {
            height: 50px !important;
        }
    </style>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="mainContent" runat="server">

<ext:ResourceManager ID="ResourceManager1"   Namespace=""  ScriptMode="Release" runat="server" />

<ext:Hidden runat="server" ID="hdnPublicationID" />

<div class="popupHeader">
        <h4 style="margin-top:0px; color:White; margin-left:20px;">
            Publication Details</h4>
    </div>



<div class="" style='margin-top: 0px'>

    <table class="fieldTable" style="margin-left:20px;">  
        <tr>
                <td colspan="2">
                    <ext:ComboBox ID="cmbPublicationType" runat="server" ValueField="PublicationTypeID" DisplayField="PublicationTypeName"
                        Width="180" FieldLabel="Publication Type" LabelAlign="top" LabelSeparator="" ForceSelection="true"
                        QueryMode="Local">
                        <%--<Items>
                            <ext:ListItem Text="Book" Value="1" />
                            <ext:ListItem Text="Research" Value="2" />
                            <ext:ListItem Text="Article" Value="3" />
                            <ext:ListItem Text="Report" Value="4" />
                        </Items>--%>
                         <Store>
                            <ext:Store ID="Store1" runat="server">
                                <Model>
                                    <ext:Model ID="Model2" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PublicationTypeID" Type="String" />
                                            <ext:ModelField Name="PublicationTypeName" Type="String" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                    </ext:ComboBox>
                    <asp:RequiredFieldValidator Display="None" ID="valcmbPublicationType" runat="server"
                        ValidationGroup="PublicationSaveUpdate" ControlToValidate="cmbPublicationType"
                        ErrorMessage="Publication Type is required." />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <ext:TextField ID="txtPublicationName" runat="server" FieldLabel="Publication Name"
                        LabelAlign="top" LabelSeparator="" Width="377" />
                    <asp:RequiredFieldValidator Display="None" ID="valtxtPublicationName" runat="server"
                        ValidationGroup="PublicationSaveUpdate" ControlToValidate="txtPublicationName"
                        ErrorMessage="Publication Name is required." />
                </td>
            </tr>
            <tr>
                <td>
                    <ext:TextField ID="txtPublisher" Width="180" runat="server" FieldLabel="Publisher"
                        LabelAlign="top" LabelSeparator="" />
                    <asp:RequiredFieldValidator Display="None" ID="valtxtPublisher" runat="server" ValidationGroup="PublicationSaveUpdate"
                        ControlToValidate="txtPublisher" ErrorMessage="Publihser is required." />
                </td>
                <td>
                    <ext:ComboBox ID="cmbCountry" Width="180" runat="server" ValueField="CountryName"
                        DisplayField="CountryName" FieldLabel="Country" LabelAlign="top" LabelSeparator=""
                        ForceSelection="true" QueryMode="Local">
                        <Store>
                            <ext:Store ID="Store2" runat="server">
                                <Model>
                                    <ext:Model ID="Model4" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="CountryName" />
                                            <ext:ModelField Name="CountryName" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                    </ext:ComboBox>
                    <asp:RequiredFieldValidator Display="None" ID="valcmbCountry" runat="server" ValidationGroup="PublicationSaveUpdate"
                        ControlToValidate="cmbCountry" ErrorMessage="Country is required." />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <ext:TextField ID="txtYear" Width="180" runat="server" FieldLabel="Year" LabelAlign="top"
                        LabelSeparator="" MaskRe="[0-9]|\.|%" />
                    <asp:RequiredFieldValidator Display="None" ID="valtxtYear" runat="server" ValidationGroup="PublicationSaveUpdate"
                        ControlToValidate="txtYear" ErrorMessage="Year is required." />
                </td>
            </tr>
            <tr>
                <td valign="bottom" colspan="2">
                    <div class="popupButtonDiv">
                        <ext:Button runat="server" Cls="btn btn-primary" ID="btnSave"  Text="<i></i>Save">
                            <DirectEvents>
                                <Click OnEvent="btnSave_Click">
                                    <EventMask ShowMask="true" />
                                </Click>
                            </DirectEvents>
                            <Listeners>
                                <Click Handler="valGroup = 'PublicationSaveUpdate'; if(CheckValidation()) return this.disable(); else return false;">
                                </Click>
                            </Listeners>
                        </ext:Button>
                        <div class="btnFlatOr">
                            or
                        </div>
                        <ext:LinkButton runat="server" StyleSpec="padding:0px;" ID="LinkButton2" Cls="btnFlatLeftGap"
                            Text="<i></i>Cancel" OnClientClick="window.close();">
                        </ext:LinkButton>
                    </div>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
