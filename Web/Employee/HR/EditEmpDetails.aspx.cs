﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ext.Net;
using BLL.Base;
using BLL.Manager;
using DAL;
using BLL;
using System.IO;
using Utils;
using Web.Helper;
using Utils.Helper;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace Web.Employee.HR
{
    public partial class EditEmpDetails : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                LoadPhotograph();



                CheckPermission();
                Initialise();

                LoadEditData();

                LoadBasicInfo();
            }
        }

        private void LoadBasicInfo()
        {

            EEmployee emp = new EmployeeManager().GetById(SessionManager.CurrentLoggedInEmployeeId);

            title.InnerHtml = emp.Title + " " + emp.Name + " - " + emp.EmployeeId;

            // Designation/Position
            if (emp.EDesignation != null)
                positionDesignation.InnerHtml = emp.EDesignation.Name;
            // Group/Level
            if (emp.PEmployeeIncomes.Count > 0 && emp.PEmployeeIncomes[0].LevelId != null)
            {
                BLevel level = NewPayrollManager.GetLevelById(emp.PEmployeeIncomes[0].LevelId.Value);
                BLevel actingLevel = NewPayrollManager.GetActingLevelInTodayDate(emp.EmployeeId);
                if (level != null)
                    groupLevel.InnerHtml = level.BLevelGroup.Name + " (Level - " + level.Name +
                         (actingLevel == null ? "" : ", Acting Level - " + actingLevel.Name)
                        + ")";
            }
            else
                groupLevelImg.Visible = false;

            // Branch
            GetEmployeeCurrentBranchDepartmentResult currentBranchDep = EmployeeManager.GetEmployeeCurrentBranchDepartment(emp.EmployeeId);
            if (currentBranchDep != null)
            {
                branch.InnerHtml = currentBranchDep.Department + ", " + currentBranchDep.Branch;
            }

            // Contact
            if (emp.EAddresses.Count > 0)
            {
                EAddress entity = emp.EAddresses[0];
                // Mobile
                //if (EmployeeManager.IsBranchOrDepartmentOrRegionalHead(emp.EmployeeId))
                //{
                if (!string.IsNullOrEmpty(entity.CIMobileNo))
                {
                    contactMobile.InnerHtml = entity.CIMobileNo + " (O)";

                    if (!string.IsNullOrEmpty(entity.PersonalMobile))
                    {
                        contactMobile.InnerHtml += " " + entity.PersonalMobile + " (P)";
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(entity.PersonalMobile))
                    {
                        contactMobile.InnerHtml += entity.PersonalMobile + " (P)";
                    }
                }
                contactMobile.InnerHtml += "&nbsp;";
                //}
                //else
                //{
                //    contactMobile.Visible = false;
                //    imgMobile.Visible = false;
                //}

                // Phone
                if (!string.IsNullOrEmpty(entity.CIPhoneNo))
                    contactPhone.InnerHtml = entity.CIPhoneNo;
                if (!string.IsNullOrEmpty(entity.Extension))
                    contactPhone.InnerHtml += " - " + entity.Extension;
                if (contactPhone.InnerHtml != "")
                    contactPhone.InnerHtml += " (O)";

                if (!string.IsNullOrEmpty(entity.PersonalPhone))
                    contactPhone.InnerHtml += " " + entity.PersonalPhone + " (R)";

                contactPhone.InnerHtml += "&nbsp;";

                email.InnerHtml += string.Format("<a href='mailto:{0}'>{0}</a> ", entity.CIEmail);
            }

            // Working for since
            int years, months, days, hours;
            int minutes, seconds, milliseconds;
            ServicePeroid firstStatus = EmployeeManager.GetServicePeroidDate(emp.EmployeeId);
            if (firstStatus != null)
            {
                NewHelper.GetElapsedTime(firstStatus.FromDateEng, firstStatus.ToDateEng.Value, out years, out months, out days, out hours, out minutes, out seconds, out milliseconds);
                string text = "";
                if (years != 0)
                    text = " " + years + " years";
                if (months != 0)
                    text += " " + months + " months";
                if (days != 0)
                    text += " " + days + " days";

                workingFor.InnerHtml += text;

                workingFor.InnerHtml += ", Since " +
                    (IsEnglish ? firstStatus.FromDateEng.ToShortDateString() : firstStatus.FromDate + " (" + firstStatus.FromDateEng.ToShortDateString() + ")");

            }

            // Photo
            bool hasPhoto = false;
            if (emp.HHumanResources.Count > 0)
            {
                if (!string.IsNullOrEmpty(emp.HHumanResources[0].UrlPhotoThumbnail))
                {
                    image.ImageUrl = Page.ResolveUrl(Config.UploadLocation + "/" + emp.HHumanResources[0].UrlPhotoThumbnail);
                    if (File.Exists(Server.MapPath(image.ImageUrl)))
                        hasPhoto = true;
                }
            }
            if (!hasPhoto)
            {
                image.ImageUrl = Page.ResolveUrl(Web.Helper.WebHelper.GetEmployeeNoImage(emp.EmployeeId));
            }
        }



        public void Initialise()
        {
            if (CommonManager.CompanySetting.WhichCompany == WhichCompany.Citizen)
            {
                workingFor.Visible = false;
                workingForImage.Visible = false;
            }

            Hidden_CustomerID.Text = SessionManager.CurrentLoggedInEmployeeId.ToString();

            CommonManager comManager = new CommonManager();

            cmbCountryPerm.Store[0].DataSource = comManager.GetAllCountries();
            cmbCountryPerm.Store[0].DataBind();

            cmbCountryTemp.Store[0].DataSource = comManager.GetAllCountries();
            cmbCountryTemp.Store[0].DataBind();

            cmbZonePerm.Store[0].DataSource = comManager.GetAllZones();
            cmbZonePerm.Store[0].DataBind();

            cmbZoneTemp.Store[0].DataSource = comManager.GetAllZones();
            cmbZoneTemp.Store[0].DataBind();

            LoadEditContact();

            if (CommonManager.CompanySetting.WhichCompany == WhichCompany.NIBL)
                txtPhoneOfficial.FieldLabel = "Direct Line";

        }

        private void LoadEditContact()
        {
            TempEAddress objTempAddress = NewHRManager.GetTempEAddressByEmployeeId(SessionManager.CurrentLoggedInEmployeeId);
            if (objTempAddress != null)
            {
                EAddress objEAddress = NewHRManager.GetAddressByID(SessionManager.CurrentLoggedInEmployeeId);
                if (objEAddress != null && objEAddress.CIEmail != null)
                    txtEmailOfficial.Text = objEAddress.CIEmail;

                txtPhoneOfficial.Text = objTempAddress.CIPhoneNo;
                txtExtentionOfficial.Text = objTempAddress.Extension;
                txtMobileOfficial.Text = objTempAddress.CIMobileNo;

                txtEmailPersonal.Text = objTempAddress.PersonalEmail;
                txtMobilePersonal.Text = objTempAddress.PersonalMobile;
                txtPhonePersonal.Text = objTempAddress.PersonalPhone;

                txtRelationEmergency.Text = objTempAddress.EmergencyRelation;
                txtNameEmergency.Text = objTempAddress.EmergencyName;
                txtPhoneEmergency.Text = objTempAddress.EmergencyPhone;
                txtMobileEmergency.Text = objTempAddress.EmergencyMobile;

                //if (objTempAddress.CreatedOn != null)
                //    btnNext.Text = "Update";
            }
            else
            {

                EAddress eAddress = new EAddress();
                eAddress = NewHRManager.GetAddressByID(SessionManager.CurrentLoggedInEmployeeId);
                if (eAddress != null)
                {
                    txtEmailOfficial.Text = eAddress.CIEmail;
                    txtPhoneOfficial.Text = eAddress.CIPhoneNo;
                    txtExtentionOfficial.Text = eAddress.Extension;
                    txtMobileOfficial.Text = eAddress.CIMobileNo;

                    txtEmailPersonal.Text = eAddress.PersonalEmail;
                    txtMobilePersonal.Text = eAddress.PersonalMobile;
                    txtPhonePersonal.Text = eAddress.PersonalPhone;

                    txtRelationEmergency.Text = eAddress.EmergencyRelation;
                    txtNameEmergency.Text = eAddress.EmergencyName;
                    txtPhoneEmergency.Text = eAddress.EmergencyPhone;
                    txtMobileEmergency.Text = eAddress.EmergencyMobile;

                    //if (eAddress.CreatedOn != null)
                    //    btnNext.Text = "Update";

                }
            }
        }
        protected void ButtonNext_Click(object sender, DirectEventArgs e)
        {
            TempEAddress address = new TempEAddress();
            int EmpID;
            if (cmbCountryPerm.SelectedItem.Index != -1)
            {
                address.PECountryId = int.Parse(cmbCountryPerm.SelectedItem.Value);
            }

            if (cmbZonePerm.SelectedItem.Index != -1)
            {
                address.PEZoneId = int.Parse(cmbZonePerm.SelectedItem.Value);
            }

            if (cmbDistrictPerm.SelectedItem.Index != -1)
            {
                address.PEDistrictId = int.Parse(cmbDistrictPerm.SelectedItem.Value);
            }

            address.PEVDCMuncipality = cmbVDCPerm.Text;
            address.PEStreet = txtStreetColonyPerm.Text;
            address.PEWardNo = txtWardNoPerm.Text;
            address.PEHouseNo = txtHouseNoPerm.Text;
            address.PEState = txtStatePerm.Text.Trim();
            address.PEZipCode = txtZipCodePerm.Text.Trim();
            address.PELocality = txtLocalityPerm.Text.Trim();


            if (cmbCountryTemp.SelectedItem.Index != -1)
            {
                address.PSCountryId = int.Parse(cmbCountryTemp.SelectedItem.Value);
            }
            if (cmbZoneTemp.SelectedItem.Index != -1)
            {
                address.PSZoneId = int.Parse(cmbZoneTemp.SelectedItem.Value);
            }
            if (cmbDistrictTemp.SelectedItem.Index != -1)
            {
                address.PSDistrictId = int.Parse(cmbDistrictTemp.SelectedItem.Value);
            }
            address.PSVDCMuncipality = cmbVDCTemp.Text;
            address.PSStreet = txtStreetColonyTemp.Text;
            address.PSWardNo = txtWardNoTemp.Text;
            address.PSHouseNo = txtHouseNoTemp.Text;
            address.PSState = txtStateTemp.Text.Trim();
            address.PSZipCode = txtZipCodeTemp.Text.Trim();
            address.PSLocality = txtLocalityTemp.Text.Trim();
            address.PSCitIssDis = txtCitIssuseDist.Text.Trim();

            EmpID = SessionManager.CurrentLoggedInEmployeeId;

            // Contact section
            address.CIPhoneNo = txtPhoneOfficial.Text;
            address.Extension = txtExtentionOfficial.Text;
            address.CIMobileNo = txtMobileOfficial.Text;

            address.PersonalEmail = txtEmailPersonal.Text;
            address.PersonalMobile = txtMobilePersonal.Text;
            address.PersonalPhone = txtPhonePersonal.Text;

            address.EmergencyRelation = txtRelationEmergency.Text;
            address.EmergencyName = txtNameEmergency.Text;
            address.EmergencyPhone = txtPhoneEmergency.Text;
            address.EmergencyMobile = txtMobileEmergency.Text;

            address.EmployeeId = SessionManager.CurrentLoggedInEmployeeId;

            Status respStatus;
            respStatus = NewHRManager.InsertUpdateEmployeeAddress(address);
            if (respStatus.IsSuccess)
            {
                //btnNext.Text = "Update";
                NewMessage.ShowNormalMessage("Address change request has been sent to HR.");
                //Response.Redirect("Address.aspx?id=" + EmpID.ToString());
            }

        }

        protected void CopyButton_Click(object sender, DirectEventArgs e)
        {
            CommonManager comManager = new CommonManager();
            if (cmbCountryPerm.SelectedItem != null && cmbCountryPerm.SelectedItem.Value != null)
                cmbCountryTemp.SetValue(int.Parse(cmbCountryPerm.SelectedItem.Value));
            //cmbCountryTemp.SelectedItem.Value = cmbCountryPerm.SelectedItem.Value;


            if (cmbZonePerm.SelectedItem != null && cmbZonePerm.SelectedItem.Value != null)
            {
                cmbZoneTemp.SetValue(int.Parse(cmbZonePerm.SelectedItem.Value));
                //cmbZoneTemp.SelectedItem.Value = cmbZonePerm.SelectedItem.Value;
                if (cmbDistrictPerm.SelectedItem != null && cmbDistrictPerm.SelectedItem.Value != null)
                {
                    cmbDistrictTemp.Clear();
                    cmbDistrictTemp.Store[0].DataSource = comManager.GetAllDistricts(int.Parse(cmbZonePerm.SelectedItem.Value));
                    cmbDistrictTemp.Store[0].DataBind();
                    //cmbDistrictTemp.SelectedItem.Value = cmbDistrictPerm.SelectedItem.Value;
                    cmbDistrictTemp.SetValue(int.Parse(cmbDistrictPerm.SelectedItem.Value));
                }
            }


            cmbVDCTemp.Text = cmbVDCPerm.Text;
            txtStreetColonyTemp.Text = txtStreetColonyPerm.Text;
            txtWardNoTemp.Text = txtWardNoPerm.Text;
            txtHouseNoTemp.Text = txtHouseNoPerm.Text;
            txtStateTemp.Text = txtStatePerm.Text;
            txtZipCodeTemp.Text = txtZipCodePerm.Text;
            txtLocalityTemp.Text = txtLocalityPerm.Text;
        }

        protected void ZoneChangeTemp(object sender, DirectEventArgs e)
        {
            CommonManager comManager = new CommonManager();
            cmbDistrictTemp.Clear();
            cmbDistrictTemp.Store[0].DataSource = comManager.GetAllDistricts(int.Parse(cmbZoneTemp.SelectedItem.Value));
            cmbDistrictTemp.Store[0].DataBind();

        }
        protected void ZoneChangePerm(object sender, DirectEventArgs e)
        {
            CommonManager comManager = new CommonManager();
            cmbDistrictPerm.Clear();
            cmbDistrictPerm.Store[0].DataSource = comManager.GetAllDistricts(int.Parse(cmbZonePerm.SelectedItem.Value));
            cmbDistrictPerm.Store[0].DataBind();
        }



        public void LoadEditData()
        {
            TempEAddress objTempAddress = NewHRManager.GetTempEAddressByEmployeeId(SessionManager.CurrentLoggedInEmployeeId);
            if (objTempAddress != null)
            {
                if (objTempAddress.PECountryId != null)
                    ExtControlHelper.ComboBoxSetSelected(objTempAddress.PECountryId, cmbCountryPerm);
                if (objTempAddress.PEZoneId != null)
                {
                    ExtControlHelper.ComboBoxSetSelected(objTempAddress.PEZoneId, cmbZonePerm);

                    CommonManager comManager = new CommonManager();
                    cmbDistrictPerm.Clear();
                    cmbDistrictPerm.Store[0].DataSource = comManager.GetAllDistricts(objTempAddress.PEZoneId.Value);
                    cmbDistrictPerm.Store[0].DataBind();
                }

                if (objTempAddress.PEDistrictId != null)
                    ExtControlHelper.ComboBoxSetSelected(objTempAddress.PEDistrictId, cmbDistrictPerm);

                if (objTempAddress.PSCountryId != null)
                    ExtControlHelper.ComboBoxSetSelected(objTempAddress.PSCountryId, cmbCountryTemp);
                if (objTempAddress.PSZoneId != null)
                {
                    ExtControlHelper.ComboBoxSetSelected(objTempAddress.PSZoneId, cmbZoneTemp);


                    CommonManager comManager = new CommonManager();
                    cmbDistrictTemp.Clear();
                    cmbDistrictTemp.Store[0].DataSource = comManager.GetAllDistricts(objTempAddress.PSZoneId.Value);
                    cmbDistrictTemp.Store[0].DataBind();
                }
                if (objTempAddress.PSDistrictId != null)
                    ExtControlHelper.ComboBoxSetSelected(objTempAddress.PSDistrictId, cmbDistrictTemp);

                cmbVDCPerm.Text = objTempAddress.PEVDCMuncipality;
                txtStreetColonyPerm.Text = objTempAddress.PEStreet;
                txtWardNoPerm.Text = objTempAddress.PEWardNo;
                txtHouseNoPerm.Text = objTempAddress.PEHouseNo;
                txtStatePerm.Text = objTempAddress.PEState;
                txtZipCodePerm.Text = objTempAddress.PEZipCode;
                txtLocalityPerm.Text = objTempAddress.PELocality;
                txtCitIssuseDist.Text = objTempAddress.PSCitIssDis;

                cmbVDCTemp.Text = objTempAddress.PSVDCMuncipality;
                txtStreetColonyTemp.Text = objTempAddress.PSStreet;
                txtWardNoTemp.Text = objTempAddress.PSWardNo;
                txtHouseNoTemp.Text = objTempAddress.PSHouseNo;
                txtStateTemp.Text = objTempAddress.PSState;
                txtZipCodeTemp.Text = objTempAddress.PSZipCode;
                txtLocalityTemp.Text = objTempAddress.PSLocality;

                //btnNext.Text = "Update";

            }
            else
            {

                EAddress eAddress = new EAddress();
                eAddress = NewHRManager.GetAddressByID(SessionManager.CurrentLoggedInEmployeeId);
                if (eAddress != null)
                {
                    if (eAddress.PECountryId != null && eAddress.PECountryId != -1)
                        ExtControlHelper.ComboBoxSetSelected(eAddress.PECountryId, cmbCountryPerm);
                    if (eAddress.PEZoneId != null && eAddress.PEZoneId != -1)
                    {
                        ExtControlHelper.ComboBoxSetSelected(eAddress.PEZoneId, cmbZonePerm);

                        CommonManager comManager = new CommonManager();
                        cmbDistrictPerm.Clear();
                        cmbDistrictPerm.Store[0].DataSource = comManager.GetAllDistricts(eAddress.PEZoneId.Value);
                        cmbDistrictPerm.Store[0].DataBind();
                    }

                    if (eAddress.PEDistrictId != null && eAddress.PEDistrictId != -1)
                        ExtControlHelper.ComboBoxSetSelected(eAddress.PEDistrictId, cmbDistrictPerm);

                    if (eAddress.PSCountryId != null && eAddress.PSCountryId != -1)
                        ExtControlHelper.ComboBoxSetSelected(eAddress.PSCountryId, cmbCountryTemp);
                    if (eAddress.PSZoneId != null && eAddress.PSZoneId != -1)
                    {
                        ExtControlHelper.ComboBoxSetSelected(eAddress.PSZoneId, cmbZoneTemp);


                        CommonManager comManager = new CommonManager();
                        cmbDistrictTemp.Clear();
                        cmbDistrictTemp.Store[0].DataSource = comManager.GetAllDistricts(eAddress.PSZoneId.Value);
                        cmbDistrictTemp.Store[0].DataBind();
                    }
                    if (eAddress.PSDistrictId != null && eAddress.PSDistrictId != -1)
                        ExtControlHelper.ComboBoxSetSelected(eAddress.PSDistrictId, cmbDistrictTemp);

                    cmbVDCPerm.Text = eAddress.PEVDCMuncipality;
                    txtStreetColonyPerm.Text = eAddress.PEStreet;
                    txtWardNoPerm.Text = eAddress.PEWardNo;
                    txtHouseNoPerm.Text = eAddress.PEHouseNo;
                    txtStatePerm.Text = eAddress.PEState;
                    txtZipCodePerm.Text = eAddress.PEZipCode;
                    txtLocalityPerm.Text = eAddress.PELocality;

                    cmbVDCTemp.Text = eAddress.PSVDCMuncipality;
                    txtStreetColonyTemp.Text = eAddress.PSStreet;
                    txtWardNoTemp.Text = eAddress.PSWardNo;
                    txtHouseNoTemp.Text = eAddress.PSHouseNo;
                    txtStateTemp.Text = eAddress.PSState;
                    txtZipCodeTemp.Text = eAddress.PSZipCode;
                    txtLocalityTemp.Text = eAddress.PSLocality;
                    txtCitIssuseDist.Text = eAddress.PSCitIssDis;

                    //if (eAddress.CreatedOn != null)
                    //    btnNext.Text = "Update";

                }
            }
        }



        private void CheckPermission()
        {
            int employeeId = 0;
            if (Request.QueryString["ID"] != null && SessionManager.CurrentLoggedInEmployeeId != 0)
            {
                if (SessionManager.CurrentLoggedInEmployeeId != int.Parse(Request.QueryString["ID"].ToString()))
                {
                    employeeId = int.Parse(Request.QueryString["ID"]);
                    List<GetOtherEmployeeListForBranchDepartmentHeadResult> list = NewHRManager.GetOtherEmployeeListByEmpId(SessionManager.CurrentLoggedInEmployeeId);
                    bool found = false;
                    foreach (GetOtherEmployeeListForBranchDepartmentHeadResult obj in list)
                    {
                        if (obj.EmployeeId == employeeId)
                        {
                            found = true;
                            break;
                        }
                    }

                    if (found == false)
                    {
                        Response.Redirect("~/Employee/EmpNoPermissionPage.aspx");
                    }
                }
            }

        }

        protected void LoadPhotograph()
        {
            if (!X.IsAjaxRequest)
            {
                NewHRManager _NewHRManager = new NewHRManager();
                int employeeId = SessionManager.CurrentLoggedInEmployeeId;
                HHumanResource _HHumanResource = _NewHRManager.GetHumanResource(employeeId);
                if ((_HHumanResource != null && !string.IsNullOrEmpty(_HHumanResource.tempUrlPhoto)))
                {
                    string filename = "";

                    if (_HHumanResource.tempUrlPhoto != null)
                    {
                        filename = "../Uploads/" + _HHumanResource.tempUrlPhoto;

                        //btnUpload.Hidden = true;
                    }
                    //else
                    //    filename = "../Uploads/" + _HHumanResource.tempUrlPhoto;
                    ImgEmployee.ImageUrl = filename;
                }

            }
        }

        protected void btnUpload_Click(object sender, DirectEventArgs e)
        {
            int employeeId = SessionManager.CurrentLoggedInEmployeeId;
            if (fup.HasFile)
            {
                string ext = Path.GetExtension(fup.FileName);
                string guidFileName = Guid.NewGuid().ToString() + ext;
                string saveLocation = Path.Combine(Server.MapPath(Config.UploadLocation), guidFileName);
                string thumbnailLocation = Path.Combine(Server.MapPath(Config.UploadLocation), Guid.NewGuid().ToString() + ext);

                if (IsImage(ext) == false)
                {
                    NewMessage.ShowWarningMessage("Invalid image type.");
                    return;
                }
                fup.PostedFile.SaveAs(saveLocation);
                string filename = "";

                try
                {
                    WebHelper.CreateThumbnail(saveLocation, 200, 200, thumbnailLocation);
                    thumbnailLocation = Path.GetFileName(thumbnailLocation);
                }
                catch { }

                try
                {
                    Status status = NewHRManager.SaveUpdateEmployeePhotograph(employeeId, guidFileName, thumbnailLocation);

                    if (status.IsSuccess)
                    {
                        filename = "../Uploads/" + guidFileName;
                        NewMessage.ShowNormalMessage("Photo added.");
                        ImgEmployee.ImageUrl = filename;

                        X.Js.Call("OpenImagePopup");
                    }
                    else
                        NewMessage.ShowWarningMessage("Error while saving photograph.");

                }
                catch (Exception exp1)
                {
                    JavascriptHelper.DisplayClientMsg(exp1.Message, Page);
                }

            }
        }

        public static bool IsImage(string pExtension)
        {
            if ((pExtension.ToLower() == ".jpg" | pExtension.ToLower() == ".gif" | pExtension.ToLower() == ".bmp" | pExtension.ToLower() == ".jpeg" | pExtension.ToLower() == ".png"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        protected void btnView1_Click(object sender, EventArgs e)
        {
            NewHRManager _NewHRManager = new NewHRManager();
            HHumanResource _HHumanResource = _NewHRManager.GetHumanResource(SessionManager.CurrentLoggedInEmployeeId);
            if (_HHumanResource != null && !string.IsNullOrEmpty(_HHumanResource.tempUrlPhoto))
            {
                string filename = "../Uploads/" + _HHumanResource.tempUrlPhoto;
                Image1.ImageUrl = filename;
                X.Js.Call("myFunction");
            }

        }


        protected void btnCropImage_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(_xField.Value) || string.IsNullOrEmpty(_yField.Value) || string.IsNullOrEmpty(_widthField.Value) || string.IsNullOrEmpty(_heightField.Value))
                return;
            var x = int.Parse(_xField.Value);
            var y = int.Parse(_yField.Value);
            var width = int.Parse(_widthField.Value);
            var height = int.Parse(_heightField.Value);

            if (height == 0 || width == 0)
                return;

            string filename = "";
            string newFileName = "";

            NewHRManager _NewHRManager = new NewHRManager();

            string saveLocation = "";
            string guidFileName = "";
            string thumbnailLocation = "";

            int employeeId = SessionManager.CurrentLoggedInEmployeeId;

            HHumanResource _HHumanResource = _NewHRManager.GetHumanResource(employeeId);
            if (_HHumanResource != null && !string.IsNullOrEmpty(_HHumanResource.tempUrlPhoto))
            {
                filename = @"~/uploads/" + _HHumanResource.tempUrlPhoto;


                string[] splFileName = _HHumanResource.tempUrlPhoto.Split('.');
                guidFileName = Guid.NewGuid().ToString() + "." + splFileName[1].ToString();

                saveLocation = Path.Combine(Server.MapPath(Config.UploadLocation), guidFileName);

                newFileName = @"~/uploads/" + guidFileName;
                thumbnailLocation = Path.Combine(Server.MapPath(Config.UploadLocation), Guid.NewGuid().ToString() + "." + splFileName[1].ToString());

            }

            using (var photo = System.Drawing.Image.FromFile(Server.MapPath(filename)))
            {
                using (var result = new Bitmap(width, height, photo.PixelFormat))
                {
                    //result.SetResolution(
                    //        photo.HorizontalResolution,
                    //        photo.VerticalResolution);

                    result.SetResolution(200, 200);

                    Bitmap bmpNew = new Bitmap(result);
                    result.Dispose();


                    using (var g = Graphics.FromImage(bmpNew))
                    {
                        g.InterpolationMode =
                             InterpolationMode.HighQualityBicubic;
                        g.DrawImage(photo,
                             new Rectangle(0, 0, width, height),
                             new Rectangle(x, y, width, height),
                             GraphicsUnit.Pixel);


                        photo.Dispose();
                        g.Dispose();

                        bmpNew.Height.Equals(height);
                        bmpNew.Width.Equals(width);

                        bmpNew.Save(Server.MapPath(newFileName));



                        NewHRManager mgr = new NewHRManager();
                        //UploadType type = (UploadType)1;

                        try
                        {

                            WebHelper.CreateThumbnail(saveLocation, 200, 200, thumbnailLocation);
                            thumbnailLocation = Path.GetFileName(thumbnailLocation);
                        }
                        catch { }

                        NewHRManager.SaveUpdateEmployeePhotograph(SessionManager.CurrentLoggedInEmployeeId, guidFileName, thumbnailLocation);

                        X.Js.Call("AfterSaveCall");

                    }
                }
            }


        }


        protected void btnSaveCall_Click(object sender, EventArgs e)
        {
            NewMessage.ShowNormalMessage("Photograph cropped successfully.");
        }

    }
}
