﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using BLL.BO;
using System.IO;
using Utils.Helper;
using Utils.Security;
using Utils;
using Web.Helper;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace Web.Employee.HR
{
    public partial class EmpPhotograph : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!X.IsAjaxRequest)
            {
                NewHRManager _NewHRManager = new NewHRManager();
                int employeeId = SessionManager.CurrentLoggedInEmployeeId;
                HHumanResource _HHumanResource = _NewHRManager.GetHumanResource(employeeId);
                if ((_HHumanResource != null && !string.IsNullOrEmpty(_HHumanResource.UrlPhoto)) || (_HHumanResource != null && !string.IsNullOrEmpty(_HHumanResource.tempUrlPhoto)))
                {
                    string filename = "";

                    if (_HHumanResource.UrlPhoto != null)
                    {
                        filename = "../../Uploads/" + _HHumanResource.UrlPhoto;
                        btnUpload.Hidden = true;
                    }
                    else
                        filename = "../../Uploads/" + _HHumanResource.tempUrlPhoto;
                    ImgEmployee.ImageUrl = filename;
                    Image1.ImageUrl = filename;

                    if (!string.IsNullOrEmpty(_HHumanResource.tempUrlPhoto))
                        btnCrop.Hidden = false;
                }

                if (Session["Redirect"] != null && Session["Redirect"].ToString() == "1")
                {
                    Session["Redirect"] = "0";
                    X.Js.Call("myFunction");
                }

            }
        }

        protected void btnUpload_Click(object sender, EventArgs e)
        {
            int employeeId = SessionManager.CurrentLoggedInEmployeeId;
            if (fup.HasFile)
            {
                string ext = Path.GetExtension(fup.FileName);
                string guidFileName = Guid.NewGuid().ToString() + ext;
                string saveLocation = Path.Combine(Server.MapPath(Config.UploadLocation), guidFileName);
                string thumbnailLocation = Path.Combine(Server.MapPath(Config.UploadLocation), Guid.NewGuid().ToString() + ext);

                if (IsImage(ext) == false)
                {
                    NewMessage.ShowWarningMessage("Invalid image type.");
                    return;
                }
                fup.PostedFile.SaveAs(saveLocation);
                string filename = "";

                try
                {
                    WebHelper.CreateThumbnail(saveLocation, 200, 200, thumbnailLocation);
                    thumbnailLocation = Path.GetFileName(thumbnailLocation);
                }
                catch { }

                try
                {
                    Status status = NewHRManager.SaveUpdateEmployeePhotograph(employeeId, guidFileName, thumbnailLocation);

                    if (status.IsSuccess)
                    {
                        filename = "../Uploads/" + guidFileName;

                        Session["Redirect"] = "1";
                        Response.Redirect("~/Employee/hr/EmpPhotograph.aspx");
                    }
                    else
                        NewMessage.ShowWarningMessage("Error while saving photograph.");

                }
                catch (Exception exp1)
                {
                    JavascriptHelper.DisplayClientMsg(exp1.Message, Page);
                }
             
            }
        }

        public static bool IsImage(string pExtension)
        {
            if ((pExtension.ToLower() == ".jpg" | pExtension.ToLower() == ".gif" | pExtension.ToLower() == ".bmp" | pExtension.ToLower() == ".jpeg" | pExtension.ToLower() == ".png"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        protected void btnCropImage_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(_xField.Value) || string.IsNullOrEmpty(_yField.Value) || string.IsNullOrEmpty(_widthField.Value) || string.IsNullOrEmpty(_heightField.Value))
                return;
            var x = int.Parse(_xField.Value);
            var y = int.Parse(_yField.Value);
            var width = int.Parse(_widthField.Value);
            var height = int.Parse(_heightField.Value);

            if (height == 0 || width == 0)
                return;

            string filename = "";
            string newFileName = "";

            NewHRManager _NewHRManager = new NewHRManager();

            string saveLocation = "";
            string guidFileName = "";
            string thumbnailLocation = "";

            HHumanResource _HHumanResource = _NewHRManager.GetHumanResource(SessionManager.CurrentLoggedInEmployeeId);
            if (_HHumanResource != null && !string.IsNullOrEmpty(_HHumanResource.tempUrlPhoto))
            {
                filename = "../../Uploads/" + _HHumanResource.tempUrlPhoto;


                string[] splFileName = _HHumanResource.tempUrlPhoto.Split('.');
                guidFileName = Guid.NewGuid().ToString() + "." + splFileName[1].ToString();

                saveLocation = Path.Combine(Server.MapPath(Config.UploadLocation), guidFileName);

                newFileName = "../../Uploads/" + guidFileName;
                thumbnailLocation = Path.Combine(Server.MapPath(Config.UploadLocation), Guid.NewGuid().ToString() + "." + splFileName[1].ToString());

            }

            using (var photo =
                  System.Drawing.Image.FromFile(Server.MapPath(filename)))
            {
                using (var result =
                      new Bitmap(width, height, photo.PixelFormat))
                {
                    //result.SetResolution(
                    //        photo.HorizontalResolution,
                    //        photo.VerticalResolution);

                    result.SetResolution(200, 200);

                    Bitmap bmpNew = new Bitmap(result);
                    result.Dispose();


                    using (var g = Graphics.FromImage(bmpNew))
                    {
                        g.InterpolationMode =
                             InterpolationMode.HighQualityBicubic;
                        g.DrawImage(photo,
                             new Rectangle(0, 0, width, height),
                             new Rectangle(x, y, width, height),
                             GraphicsUnit.Pixel);


                        photo.Dispose();
                        g.Dispose();

                        bmpNew.Height.Equals(height);
                        bmpNew.Width.Equals(width);
                       
                        bmpNew.Save(Server.MapPath(newFileName));

                        try
                        {

                            WebHelper.CreateThumbnail(saveLocation, 200, 200, thumbnailLocation);
                            thumbnailLocation = Path.GetFileName(thumbnailLocation);
                        }
                        catch { }

                        NewHRManager.SaveUpdateEmployeePhotograph(SessionManager.CurrentLoggedInEmployeeId, guidFileName, thumbnailLocation);

                        ImgEmployee.ImageUrl = newFileName;


                    }
                }
            }


            Response.Redirect("~/Employee/hr/EmpPhotograph.aspx");
        }

    }
}