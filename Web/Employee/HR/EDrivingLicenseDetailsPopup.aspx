﻿<%@ Page Title="Driving License Details" Language="C#" MasterPageFile="~/Master/ForEmployeePopupPage.Master" AutoEventWireup="true" CodeBehind="EDrivingLicenseDetailsPopup.aspx.cs" Inherits="Web.Employee.HR.EDrivingLicenseDetailsPopup" %>

<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">

<script type="text/javascript">

    var skipLoadingCheck = true;

    function closePopup() {
        window.close();
        window.opener.reloadDrivLicGrid();
    }

  
</script>
    <style type="text/css">
        input[type=text], textarea, select
        {
            border: 1px solid #A2B4C6;
        }
        textarea
        {
            height: 50px !important;
        }
    </style>
<script type="text/javascript">
    function afterRender() { }
</script>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="mainContent" runat="server">

<ext:ResourceManager ID="ResourceManager1"  Namespace=""  ScriptMode="Release" runat="server" />

<ext:Hidden runat="server" ID="hdnDrivingLiscenceID">
</ext:Hidden>

<div class="popupHeader">
        <h4 style="margin-top:0px; color:White; margin-left:20px;">
            Driving License Details</h4>
    </div>



<div class="" style='margin-top: 0px'>

    <table class="fieldTable" style="margin-left:20px;">   
        <tr>
                <td>
                    <span style="padding-bottom: 50px; font-weight: bold">Driving Licence Type</span>
                    <ext:Checkbox ID="chkTwoWheeler" runat="server" FieldLabel="Two Wheeler" LabelAlign="Left"
                        LabelSeparator="">
                    </ext:Checkbox>
                    <ext:Checkbox ID="chkFourWheeler" runat="server" FieldLabel="Four Wheeler" LabelAlign="Left"
                        LabelSeparator="">
                    </ext:Checkbox>
                    <ext:Checkbox ID="chkHeavy" runat="server" FieldLabel="Heavy Vehicle" LabelAlign="Left" LabelSeparator="">
                    </ext:Checkbox>
                    <%--  <ext:MultiCombo  runat="server" Width="260" ID="cmbLiscenceType" LabelAlign="Top" LabelSeparator="" FieldLabel="Type">
                                    <Items>
                                        <ext:ListItem Text="Two Wheeler" Value="1" />
                                        <ext:ListItem Text="Four Wheeler" Value="2" />
                                        <ext:ListItem Text="Heavy" Value="3" />
                                    </Items>
                                </ext:MultiCombo>--%>
                </td>
            </tr>
            <tr>
                <td>
                    <ext:TextField ID="txtDrivingLiscenceNumber" runat="server" FieldLabel="Driving Licence Number"
                        Width="180px" LabelAlign="top" LabelSeparator="" />
                    <asp:RequiredFieldValidator Display="None" ID="valtxtDrivingLiscenceNumber" runat="server"
                        ValidationGroup="DrivingLiscenceSaveUpdate" ControlToValidate="txtDrivingLiscenceNumber"
                        ErrorMessage="Please enter Driving Liscence Number." />
                </td>
            </tr>
            <tr>
                <td style="padding-top: 5px;">
                    <ext:ComboBox ID="cmbIssuingCountry" Width="180px" runat="server" ValueField="CountryName"
                        DisplayField="CountryName" FieldLabel="Issuing Country" LabelAlign="top" LabelSeparator=""
                        ForceSelection="true" QueryMode="Local">
                        <Store>
                            <ext:Store ID="Store2" runat="server">
                                <Model>
                                    <ext:Model ID="Model4" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="CountryName" />
                                            <ext:ModelField Name="CountryName" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                    </ext:ComboBox>
                    <asp:RequiredFieldValidator Display="None" ID="valcmbIssuingCountry" runat="server"
                        ValidationGroup="DrivingLiscenceSaveUpdate" ControlToValidate="cmbIssuingCountry"
                        ErrorMessage="Issuing Country is required." />
                </td>
            </tr>
            <tr>
                <td>
                    <ext:FileUploadField ID="FileDrivingLiscenceDocumentUpload" runat="server" Width="200"
                        Icon="Attach" FieldLabel="&nbsp;" LabelAlign="Top" LabelSeparator="" />
                    <%--<span style="color: #ACACAC; padding-top: 20px;">*(only doc,pdf,txt,docx,rar)</span>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ValidationExpression="^.*\.(doc|DOC|pdf|PDF|txt|TXT|docx|DOCX|rar|RAR)$"
                        runat="server" ValidationGroup="DrivingLiscenceSaveUpdate" Display="None" ControlToValidate="FileDrivingLiscenceDocumentUpload"
                        ErrorMessage="Invalid file format."></asp:RegularExpressionValidator>--%>
                </td>
                <td valign="bottom">
                    <table>
                        <tr>
                            <td>
                                <ext:Label ID="lblUploadedFile" runat="server" Hidden="true">
                                </ext:Label>
                            </td>
                            <td>
                                <ext:LinkButton Icon="Delete" runat="server" ID="lnkDeleteFile" OnDirectClick="lnkDeleteFile_Click"
                                    Text="Delete file" Hidden="true">
                                    <DirectEvents>
                                        <Click OnEvent="lnkDeleteFile_Click">
                                            <Confirmation ConfirmRequest="true" Message="Do you want to delete the file?" />
                                            <EventMask ShowMask="true" />
                                        </Click>
                                    </DirectEvents>
                                </ext:LinkButton>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td valign="bottom" colspan="2">
                    <div class="popupButtonDiv">
                        <ext:Button runat="server"  ID="btnSave" Cls="btn btn-primary" Text="<i></i>Save">
                            <DirectEvents>
                                <Click OnEvent="btnSave_Click">
                                    <EventMask ShowMask="true" />
                                </Click>
                            </DirectEvents>
                            <Listeners>
                                <Click Handler="valGroup = 'DrivingLiscenceSaveUpdate'; return CheckValidation();">
                                </Click>
                            </Listeners>
                        </ext:Button>
                        <div class="btnFlatOr">
                            or</div>
                        <ext:LinkButton runat="server" StyleSpec="padding:0px;" ID="LinkButton2" Cls="btnFlatLeftGap" OnClientClick="window.close();"
                            Text="<i></i>Cancel">
                        </ext:LinkButton>
                    </div>
                </td>
            </tr>
    </table>
</div>

</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
