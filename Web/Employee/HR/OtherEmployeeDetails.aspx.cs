﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;

namespace Web.Employee.HR
{
    public partial class OtherEmployeeDetails : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                CheckPermission();
            }
        }

        private void CheckPermission()
        {
            int employeeId = 0;
            if (Request.QueryString["ID"] != null && SessionManager.CurrentLoggedInEmployeeId != 0)
            {
                if (SessionManager.CurrentLoggedInEmployeeId != int.Parse(Request.QueryString["ID"].ToString()))
                {
                    employeeId = int.Parse(Request.QueryString["ID"]);
                    List<GetOtherEmployeeListForBranchDepartmentHeadResult> list = NewHRManager.GetOtherEmployeeListByEmpId(SessionManager.CurrentLoggedInEmployeeId);
                    bool found = false;
                    foreach (GetOtherEmployeeListForBranchDepartmentHeadResult obj in list)
                    {
                        if (obj.EmployeeId == employeeId)
                        {
                            found = true;
                            break;
                        }
                    }

                    if (found == false)
                    {
                        Response.Redirect("~/Employee/EmpNoPermissionPage.aspx");
                    }
                }
            }

        }
    }
}