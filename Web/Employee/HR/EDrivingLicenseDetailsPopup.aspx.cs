﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using BLL.BO;
using System.IO;
namespace Web.Employee.HR
{
    public partial class EDrivingLicenseDetailsPopup : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();

                if (!string.IsNullOrEmpty(Request.QueryString["DrivingLicenceId"]))
                {
                    hdnDrivingLiscenceID.Text = Request.QueryString["DrivingLicenceId"];
                    editDrivingLiscence(int.Parse(hdnDrivingLiscenceID.Text));
                }
            }
        }
        protected void Initialise()
        {
            int EmployeeID = GetEmployeeID();
            CommonManager _CommonManager = new CommonManager();
            cmbIssuingCountry.Store[0].DataSource = _CommonManager.GetAllCountries();
            cmbIssuingCountry.Store[0].DataBind();

        }
        protected void btnSave_Click(object sender, DirectEventArgs e)
        {
            this.SaveFormData();
        }
        private void SaveFormData()
        {

            Status myStatus = new Status();
            HDrivingLicence _HDrivingLiscence = new HDrivingLicence();
            _HDrivingLiscence.EmployeeId = this.GetEmployeeID();
            bool isSave = bool.Parse(this.hdnDrivingLiscenceID.Text == "" ? "true" : "false");
            if (!isSave)
                _HDrivingLiscence.DrivingLicenceId = int.Parse(this.hdnDrivingLiscenceID.Text);

            var LiscenceTypeName = "";

            if (chkTwoWheeler.Checked)
            {

                if (LiscenceTypeName == "")
                    LiscenceTypeName += "Two Wheeler";
                else
                    LiscenceTypeName += "," + "Two Wheeler";
            }

            if (chkFourWheeler.Checked)
            {

                if (LiscenceTypeName == "")
                    LiscenceTypeName += "Four Wheeler";
                else
                    LiscenceTypeName += "," + "Four Wheeler";
            }

            if (chkHeavy.Checked)
            {

                if (LiscenceTypeName == "")
                    LiscenceTypeName += "Heavy";
                else
                    LiscenceTypeName += "," + "Heavy";
            }

            if (string.IsNullOrEmpty(LiscenceTypeName))
            {
                NewMessage.ShowWarningMessage("Select Driving Licence Type.");
                return;
            }


            if (!string.IsNullOrEmpty(LiscenceTypeName))
            {
                //  _HDrivingLiscence.LiscenceTypeID = LiscenceTypevalues;
                _HDrivingLiscence.LiscenceTypeName = LiscenceTypeName;
            }

            _HDrivingLiscence.DrivingLicenceNo = txtDrivingLiscenceNumber.Text.Trim();
            _HDrivingLiscence.IssuingCountry = cmbIssuingCountry.SelectedItem.Text;

            //file upload section
            string UserFileName = this.FileDrivingLiscenceDocumentUpload.FileName;

            string ext = Path.GetExtension(UserFileName);

            if (ext.ToLower() == ".exe")
            {
                FileDrivingLiscenceDocumentUpload.Reset();
                FileDrivingLiscenceDocumentUpload.Focus();
                NewMessage.ShowWarningMessage("Please upload proper file.");
                return;
            }

            string ServerFileName = Guid.NewGuid().ToString() + ext;
            string relativePath = @"~/uploads/" + ServerFileName;
            if (this.UploadFileDrivingLiscence(relativePath))
            {
                double fileSize = FileDrivingLiscenceDocumentUpload.PostedFile.ContentLength;

                _HDrivingLiscence.FileFormat = Path.GetExtension(this.FileDrivingLiscenceDocumentUpload.FileName).Replace(".", "").Trim();
                _HDrivingLiscence.FileType = this.FileDrivingLiscenceDocumentUpload.PostedFile.ContentType;
                _HDrivingLiscence.FileLocation = @"../Uploads/";
                _HDrivingLiscence.ServerFileName = ServerFileName;
                _HDrivingLiscence.UserFileName = UserFileName;
                _HDrivingLiscence.Size = this.ToSizeString(fileSize);

            }

            if (SessionManager.CurrentLoggedInEmployeeId == 0)
                _HDrivingLiscence.Status = 1;
            else
                _HDrivingLiscence.Status = 0;

            myStatus = NewHRManager.Instance.InsertUpdateDrivingLicence(_HDrivingLiscence, isSave);

            if (myStatus.IsSuccess)
            {
                if (isSave)
                    NewMessage.ShowNormalMessage("Record Saved Successfully.", "closePopup()");
                else
                    NewMessage.ShowNormalMessage("Record Updated Successfully.", "closePopup()");
            }
            else
            {
                NewMessage.ShowWarningMessage(myStatus.ErrorMessage);
            }
        }

        protected string ToSizeString(double bytes)
        {

            string[] SizeSuffixes = { "bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB" };
            int mag = (int)Math.Log(bytes, 1024);
            decimal adjustedSize = (decimal)bytes / (1 << (mag * 10));
            return string.Format("{0:n1} {1}", adjustedSize, SizeSuffixes[mag]);
        }
        protected void DrivingLiscenceDownLoad(int ID)
        {

            //string contentType = "";
            HDrivingLicence doc = NewHRManager.GetDrivingLicenceDetailsById(ID);

            string path = Context.Server.MapPath(doc.FileLocation + doc.ServerFileName);
            string contentType = doc.FileType;
            string name = doc.UserFileName + "." + doc.FileFormat;

            byte[] bytes = File.ReadAllBytes(path);

            Context.Response.Clear();
            Context.Response.ClearHeaders();
            Context.Response.AppendHeader("Content-Disposition", "attachment; filename=\"" + name + "\"");
            Context.Response.ContentType = contentType;
            Context.Response.BinaryWrite(bytes);
            Context.Response.End();
            // Response.Redirect("~/DocumentHandler.ashx?ID=" + ID);
        }
        protected bool UploadFileDrivingLiscence(string relativePath)
        {

            if (this.FileDrivingLiscenceDocumentUpload.HasFile)
            {

                int fileSize = FileDrivingLiscenceDocumentUpload.PostedFile.ContentLength;
                this.FileDrivingLiscenceDocumentUpload.PostedFile.SaveAs(Server.MapPath(relativePath));
                return true;
            }
            else
                return false;

        }

        protected int GetEmployeeID()
        {
            //Guid? AssetID = null;
            int EmpID = 0;
            if (!string.IsNullOrEmpty(Request.QueryString["ID"]))
            {
                EmpID = int.Parse(Request.QueryString["ID"]);
            }
            else
                EmpID = SessionManager.CurrentLoggedInEmployeeId;

            return EmpID;
        }
        protected void btnAddNewLine_Click(object sender, DirectEventArgs e)
        {
            this.ClearFields();


        }
        protected void lnkDeleteFile_Click(object sender, DirectEventArgs e)
        {
            int DrivingLiscenceID = int.Parse(hdnDrivingLiscenceID.Text);
            Status myStatus = new Status();

            HDrivingLicence _HDrivingLiscence = NewHRManager.GetDrivingLicenceDetailsById(DrivingLiscenceID);
            string path = Server.MapPath(@"~/uploads/" + _HDrivingLiscence.ServerFileName); 
            myStatus = NewHRManager.Instance.DeleteDrivingLicenceFile(DrivingLiscenceID);
            if (myStatus.IsSuccess)
            {
                if (File.Exists(path))
                {
                    File.Delete(path);
                    lnkDeleteFile.Hide();
                    lblUploadedFile.Hide();
                    NewMessage.ShowNormalMessage("file deleted successfully");
                    //refresh grid
                    //this.LoadDrivingLiscenceGrid(this.GetEmployeeID());

                }
            }



        }
        private object[] DrivingLiscenceFillData
        {
            get
            {
                return new object[]
            {
                new object[] { "", "", "",""},
               
            };
            }
        }

        protected void DeleteDrivingLiscence(int ID, string path)
        {

            bool result = NewHRManager.DeleteDrivingLicenceByID(ID);
            if (result)
            {
                if (File.Exists(path))
                    File.Delete(path);
                //refresh grid
                NewMessage.ShowNormalMessage("Record deleted successfully.");
                //this.LoadDrivingLiscenceGrid(this.GetEmployeeID());
            }

        }
        public void ClearFields()
        {
            this.hdnDrivingLiscenceID.Text = "";
            txtDrivingLiscenceNumber.Text = "";
            cmbIssuingCountry.Value = "";
            chkTwoWheeler.Checked = false;
            chkFourWheeler.Checked = false;
            chkHeavy.Checked = false;
            lblUploadedFile.Hide();
            lblUploadedFile.Text = "";
            lnkDeleteFile.Hide();
            btnSave.Text = Resources.Messages.Save;
            FileDrivingLiscenceDocumentUpload.Reset();
        }
        public void editDrivingLiscence(int DrivingLiscenceID)
        {
            HDrivingLicence _HDrivingLiscence = NewHRManager.GetDrivingLicenceDetailsById(DrivingLiscenceID);
            this.hdnDrivingLiscenceID.Text = DrivingLiscenceID.ToString();
            txtDrivingLiscenceNumber.Text = _HDrivingLiscence.DrivingLicenceNo;
            cmbIssuingCountry.SelectedItem.Value = _HDrivingLiscence.IssuingCountry.ToString();
            if (_HDrivingLiscence.LiscenceTypeName.Contains("Two Wheeler") == true)
                chkTwoWheeler.Checked = true;
            else
                chkTwoWheeler.Checked = false;

            if (_HDrivingLiscence.LiscenceTypeName.Contains("Four Wheeler") == true)
                chkFourWheeler.Checked = true;
            else
                chkFourWheeler.Checked = false;

            if (_HDrivingLiscence.LiscenceTypeName.Contains("Heavy") == true)
                chkHeavy.Checked = true;
            else
                chkHeavy.Checked = false;

            if (!string.IsNullOrEmpty(_HDrivingLiscence.UserFileName))
            {
                lblUploadedFile.Show();
                lblUploadedFile.Text = _HDrivingLiscence.UserFileName;
                lnkDeleteFile.Show();
            }
            else
            {
                lblUploadedFile.Hide();
                lblUploadedFile.Text = "";
                lnkDeleteFile.Hide();
            }
            btnSave.Text = Resources.Messages.Update;
        }
        public string GetComboSelection(Ext.Net.ComboBox cmb)
        {
            if (cmb.SelectedItem == null || cmb.SelectedItem.Value == null)
                return null;

            return cmb.SelectedItem.Value;

        }
        public string GetComboSelectionText(Ext.Net.ComboBox cmb)
        {
            if (cmb.SelectedItem == null || cmb.SelectedItem.Value == null)
                return null;

            return cmb.SelectedItem.Text;
        }

    }
}