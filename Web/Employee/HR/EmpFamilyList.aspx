﻿<%@ Page Title="Family List" Language="C#" MasterPageFile="~/Master/EmployeeExt.Master" AutoEventWireup="true" CodeBehind="EmpFamilyList.aspx.cs" Inherits="Web.Employee.HR.EmpFamilyList" %>
<%@ Register Src="~/NewHR/UserControls/FamilyCtl.ascx" TagName="FamilyCtl" TagPrefix="uc1" %>

<%@ Register Src="~/NewHR/UserControls/UFamilyCtrl.ascx" TagName="UFamilyCtrl" TagPrefix="ucF" %>



<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="mainContent" runat="server">

 <div class="separator bottom">
    </div>
 
    <div class="innerLR">
        <div class="widget">
            <div class="widget-head">
                <h4 class="heading">
                    Family</h4>
            </div>
            <div class="widget-body">
               <%-- <uc1:FamilyCtl Id="familyCtrl" runat="server" />--%>
                <ucF:UFamilyCtrl Id="UFamilyCtrl" runat="server" />
            </div>
        </div>
    </div>

</asp:Content>
