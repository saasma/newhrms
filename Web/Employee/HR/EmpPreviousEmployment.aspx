﻿<%@ Page Title="Previous Employment" Language="C#" MasterPageFile="~/Master/EmployeeExt.Master" AutoEventWireup="true" CodeBehind="EmpPreviousEmployment.aspx.cs" Inherits="Web.Employee.HR.EmpPreviousEmployment" %>
<%@ Register Src="~/NewHR/UserControls/EmploymentHistoryCtl.ascx" TagName="EmploymentHistoryCtl" TagPrefix="ucEmploymentHistoryCtl" %>

<%@ Register Src="~/NewHR/UserControls/EmploymentCtrl.ascx" TagName="UEmploymentCtrl"
    TagPrefix="ucEmploymentCtrl" %>

<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="mainContent" runat="server">


 <div class="separator bottom">
    </div>
 
    <div class="innerLR">
        <div class="widget">
            <div class="widget-head">
                <h4 class="heading">
                    Previous Employment</h4>
            </div>
            <div class="widget-body">
                <%--<ucEmploymentHistoryCtl:EmploymentHistoryCtl Id="ucEmploymentHistoryCtl1" runat="server" />--%>
                <ucEmploymentCtrl:UEmploymentCtrl Id="ucEmploymentHistoryCtl1" runat="server" />
            </div>
        </div>
    </div>

</asp:Content>
