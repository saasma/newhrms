﻿<%@ Page Title="Family Details" Language="C#" MasterPageFile="~/Master/ForEmployeePopupPage.Master"
    AutoEventWireup="true" CodeBehind="EFamilyDetailsPopup.aspx.cs" Inherits="Web.Employee.HR.EFamilyDetailsPopup" %>

<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
    <style type="text/css">
        input[type=text], textarea, select
        {
            border: 1px solid #A2B4C6;
        }
        textarea
        {
            height: 50px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">

        //        function setDetaultMonth() {
        //            var element = document.getElementById('dateMonth');
        //            if (element) {
        //            var value = <%=hdnCurrentMonth.ClientID %>.getValue();
        //            if(value)
        //            {
        //                element.value = value;
        //                monthYearChange(null, element);
        //             }
        //            }
        //          }

        //         Ext.onReady(function () {
        //             setTimeout(setDetaultMonth, 2000);
        //  });

        var skipLoadingCheck = true;
        function closePopup() {
            window.close();
            window.opener.reloadFamilyGrid();
        }

  
    </script>
    <script type="text/javascript">
        function afterRender() { }
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="mainContent" runat="server">
    <ext:ResourceManager ID="ResourceManager1" Namespace="" ScriptMode="Release" runat="server" />
    <ext:Hidden runat="server" ID="hiddenValue" />
    <ext:Hidden runat="server" ID="hdnCurrentMonth" />
    <div class="popupHeader">
        <h4 style="margin-top: 0px; color: White; margin-left: 0px;">
            Family Details</h4>
    </div>
    <div class="" style='margin-top: 20px'>
        <table class="fieldTable" style="margin-left: 0px;">
            <tr>
                <td>
                    <ext:TextField ID="txtName" LabelSeparator="" runat="server" FieldLabel="Name *"
                        Width="200" LabelAlign="Top">
                    </ext:TextField>
                    <asp:RequiredFieldValidator Display="None" ID="valcmbFaculty" runat="server" ValidationGroup="SaveUpdateLevel"
                        ControlToValidate="txtName" ErrorMessage="Name is required." />
                </td>
                <td>
                    <ext:ComboBox ID="cmbRelation" runat="server" ValueField="ID" DisplayField="Name"
                        FieldLabel="Relation *" LabelAlign="top" LabelSeparator="" ForceSelection="true"
                        QueryMode="Local">
                        <Store>
                            <ext:Store ID="Store1" runat="server">
                                <Model>
                                    <ext:Model ID="Model3" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="ID" Type="String" />
                                            <ext:ModelField Name="Name" Type="String" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                    </ext:ComboBox>
                    <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator1" runat="server"
                        ValidationGroup="SaveUpdateLevel" ControlToValidate="cmbRelation" ErrorMessage="Relation is required." />
                </td>
            </tr>
            <tr>
                <td>
                    <pr:CalendarExtControl Width="180px" FieldLabel="Date Of Birth" ID="txtDOB" runat="server"
                        LabelAlign="Top" LabelSeparator="">
                    </pr:CalendarExtControl>
                    <%--   <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator2" runat="server"
                        ValidationGroup="SaveUpdateLevel" ControlToValidate="txtDOB" ErrorMessage="DOB is required." />--%>
                </td>
                <td>
                    <ext:Checkbox Width="180px" BoxLabelAlign="After" BoxLabel="Dependent" ID="chkHasDependent"
                        runat="server" LabelSeparator="" />
                </td>
            </tr>
            <tr style="display: none;">
                <td>
                    <pr:CalendarExtControl Width="180px" FieldLabel="Specified Date *" ID="txtSPDate"
                        runat="server" LabelAlign="Top" LabelSeparator="" />
                    <%--<asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator4" runat="server"
                        ValidationGroup="SaveUpdateLevel" ControlToValidate="txtSPDate" ErrorMessage="Specified Date is required." />--%>
                </td>
                <td>
                    <ext:TextField ID="txtAgeOnSPDate" LabelSeparator="" runat="server" FieldLabel="Age on Specified Date"
                        Width="200" LabelAlign="Top">
                    </ext:TextField>
                    <%-- <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator5" runat="server"
                                ValidationGroup="SaveUpdateLevel" ControlToValidate="txtAgeOnSPDate" ErrorMessage="Age on Specified Date is required." />--%>
                </td>
            </tr>
            <tr>
                <td>
                    <ext:ComboBox ID="cmbOccupation" runat="server" ValueField="OccupationId" DisplayField="Occupation"
                        FieldLabel="Occupation" LabelAlign="top" LabelSeparator="" ForceSelection="true"
                        QueryMode="Local">
                        <Store>
                            <ext:Store ID="storeOccupation" runat="server">
                                <Model>
                                    <ext:Model ID="Model1" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="OccupationId" Type="String" />
                                            <ext:ModelField Name="Occupation" Type="String" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                    </ext:ComboBox>
                </td>
                <td>
                    <ext:TextField ID="txtContactNumber" FieldLabel="Contact Number " runat="server"
                        LabelAlign="Top" Width="152" LabelSeparator="">
                    </ext:TextField>
                    <asp:RegularExpressionValidator ID="valRegPhone" runat="server" ControlToValidate="txtContactNumber"
                        ErrorMessage="Contact number is invalid." ValidationGroup="SaveUpdateLevel" Display="None"></asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <ext:TextArea ID="txtRemarks" Width="362" Rows="4" LabelSeparator="" runat="server"
                        FieldLabel="Note " LabelAlign="Top">
                    </ext:TextArea>
                </td>
            </tr>
            <tr>
                <td valign="bottom" colspan="2">
                    <div class="popupButtonDiv">
                        <ext:Button runat="server" ID="btnLevelSaveUpdate" Cls="btn btn-primary" Text="<i></i>Save">
                            <DirectEvents>
                                <Click OnEvent="btnLevelSaveUpdate_Click">
                                    <EventMask ShowMask="true" />
                                </Click>
                            </DirectEvents>
                            <Listeners>
                                <Click Handler="valGroup = 'SaveUpdateLevel'; if(CheckValidation()) return this.disable(); else return false;">
                                </Click>
                            </Listeners>
                        </ext:Button>
                        <div class="btnFlatOr">
                            or</div>
                        <ext:LinkButton ID="LinkButton1" StyleSpec="padding:0px;" runat="server" Text="Cancel"
                            OnClientClick="window.close();" Cls="btnFlatLeftGap">
                        </ext:LinkButton>
                    </div>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
