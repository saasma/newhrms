﻿<%@ Page Title="Nominee Details" Language="C#" MasterPageFile="~/Master/ForEmployeePopupPage.Master" AutoEventWireup="true" CodeBehind="ENomineeDetlsPopup.aspx.cs" Inherits="Web.Employee.HR.ENomineeDetlsPopup" %>

<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">

<script type="text/javascript">

    var skipLoadingCheck = true;


    function closePopup() {
        window.close();
        window.opener.reloadNomineeGrid();
    }
</script>
 <style type="text/css">
        input[type=text], textarea, select
        {
            border: 1px solid #A2B4C6;
        }
        textarea
        {
            height: 50px !important;
        }
    </style>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="mainContent" runat="server">

<ext:ResourceManager ID="ResourceManager1"  Namespace=""  ScriptMode="Release"  runat="server" />

<ext:Hidden runat="server" ID="hdnNomineeId" />

<div class="popupHeader">
         <h4 style="margin-top:0px; color:White; margin-left:20px;">
            Nominee Details</h4>
    </div>


<div class="" style='margin-top: 0px'>

    <table class="fieldTable" style="margin-left:20px;"> 
        <tr>
                <td colspan="2">
                    <ext:TextField Width="330" ID="txtNomineeName" runat="server" FieldLabel="Nominee Name *"
                        LabelAlign="top" LabelSeparator="" />
                    <asp:RequiredFieldValidator Display="None" ID="rfvNomineeName" runat="server"
                        ValidationGroup="SaveUpdateNominee" ControlToValidate="txtNomineeName" ErrorMessage="Nominee Name is required." />
                </td>
               
            </tr>
            <tr>
                <td>
                    <ext:ComboBox ID="cmbRelation" runat="server" ValueField="ID" DisplayField="Name" FieldLabel="Relation *" Width="160"
                        LabelAlign="top" LabelSeparator="" ForceSelection="true" QueryMode="Local">
                        <Store>
                            <ext:Store ID="Store1" runat="server">
                                <Model>
                                    <ext:Model ID="Model3" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="ID" Type="String" />
                                            <ext:ModelField Name="Name" Type="String" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                    </ext:ComboBox>                   
                    <asp:RequiredFieldValidator Display="None" ID="rfvRelation" runat="server"
                        ValidationGroup="SaveUpdateNominee" ControlToValidate="cmbRelation" ErrorMessage="Relation is required." />
                </td>
                <td>
                    <pr:CalendarExtControl ID="txtEffectiveDate" LabelSeparator="" runat="server" FieldLabel="Effective Date *"
                        Width="160" LabelAlign="Top">
                    </pr:CalendarExtControl>    
                     <asp:RequiredFieldValidator Display="None" ID="rfvEffectiveDate" runat="server"
                        ValidationGroup="SaveUpdateNominee" ControlToValidate="txtEffectiveDate" ErrorMessage="Effective Date is required." />               
                </td>
            </tr>
          
             <td colspan="2">
                    <ext:TextArea ID="txtRemarks" Width="330" runat="server" FieldLabel="Remarks" LabelAlign="Top"
                        LabelSeparator="" />
                </td>

    
            <tr>
                <td valign="bottom" colspan="2">
                    <div class="popupButtonDiv">
                        <ext:Button runat="server" Cls="btn btn-primary" ID="btnSave" Text="<i></i>Save">
                            <DirectEvents>
                                <Click OnEvent="btnSave_Click">
                                    <EventMask ShowMask="true" />
                                </Click>
                            </DirectEvents>
                            <Listeners>
                                <Click Handler="valGroup = 'SaveUpdateNominee';  if(CheckValidation()) return this.disable(); else return false;">
                                </Click>
                            </Listeners>
                        </ext:Button>
                        <div class="btnFlatOr">
                            or</div>
                        <ext:LinkButton runat="server" StyleSpec="padding:0px;" ID="LinkButton2" Cls="btnFlatLeftGap"
                            Text="<i></i>Cancel" OnClientClick="window.close();">
                        </ext:LinkButton>
                    </div>
                </td>
            </tr>
        </table>

</div>


</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
