﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL.Entity;
using BLL;
using Utils;
using System.IO;
using Ext.Net.Utilities;
using Web.Helper;

namespace Web.Employee.HR
{
    public partial class EmployeeSearchDetail : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();

                foreach (Portlet portlet in ControlUtils.FindControls<Portlet>(this.Page))
                {
                    portlet.BodyPadding = 5;
                    portlet.CloseAction = CloseAction.Hide;
                }

                foreach (Portlet portlet in ControlUtils.FindControls<Portlet>(this.Page))
                {
                    portlet.DirectEvents.Hide.Event += Portlet_Hide;
                    portlet.DirectEvents.Hide.EventMask.ShowMask = true;
                    portlet.DirectEvents.Hide.EventMask.MinDelay = 500;

                    portlet.DirectEvents.Hide.ExtraParams.Add(new Ext.Net.Parameter("ID", portlet.ClientID));
                }

                if (CommonManager.CompanySetting.WhichCompany != WhichCompany.Sangrila)
                    PortalColumnSignature.Hide();
                if (CommonManager.CompanySetting.WhichCompany == WhichCompany.Citizen)
                {
                    PortalAddress.Visible = false;
                }
            }

        }



        protected void Portlet_Hide(object sender, DirectEventArgs e)
        {
            X.Msg.Alert("Status", e.ExtraParams["ID"] + " Hidden").Show();
        }


        private void Initialise()
        {
            if (!string.IsNullOrEmpty(Request.QueryString["ID"]))
            {
                int employeeId = int.Parse(Request.QueryString["ID"]);
                block.Style["display"] = "block";
                LoadEmployeeData(employeeId);
            }
        }

        private void LoadEmployeeData(int employeeId)
        {
            EEmployee emp = new EmployeeManager().GetById(employeeId);

            title.InnerHtml = "";

            branch.InnerHtml = "";
            email.InnerHtml = "";
            contactMobile.InnerHtml = "";
            contactPhone.InnerHtml = "";

            title.InnerHtml = emp.Title + " " + emp.Name;

            //Designation
            //if (emp.EDesignation != null)
            //    designation.InnerHtml = emp.EDesignation.Name;

            // Branch
            GetEmployeeCurrentBranchDepartmentResult currentBranchDep = EmployeeManager.GetEmployeeCurrentBranchDepartment(emp.EmployeeId);
            if (currentBranchDep != null)
            {
                branch.InnerHtml = currentBranchDep.Department + ", " + currentBranchDep.Branch;
            }

            // Contact
            if (emp.EAddresses.Count > 0)
            {
                EAddress entity = emp.EAddresses[0];

                // Mobile

                if (!string.IsNullOrEmpty(entity.CIMobileNo))
                {
                    contactMobile.InnerHtml = entity.CIMobileNo + " (O)";

                    if (!string.IsNullOrEmpty(entity.PersonalMobile))
                    {
                        contactMobile.InnerHtml += " " + entity.PersonalMobile + " (P)";
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(entity.PersonalMobile))
                    {
                        contactMobile.InnerHtml += entity.PersonalMobile + " (P)";
                    }
                }


                // Phone
                if (!string.IsNullOrEmpty(entity.CIPhoneNo))
                    contactPhone.InnerHtml = entity.CIPhoneNo;
                if (!string.IsNullOrEmpty(entity.Extension))
                    contactPhone.InnerHtml += " - " + entity.Extension;
                if (contactPhone.InnerHtml != "")
                    contactPhone.InnerHtml += " (O)";

                if (!string.IsNullOrEmpty(entity.PersonalPhone))
                    contactPhone.InnerHtml += " " + entity.PersonalPhone + " (R)";


                email.InnerHtml += string.Format("<a href='mailto:{0}'>{0}</a> ", entity.CIEmail);
            }

            // Photo
            bool hasPhoto = false;
            if (emp.HHumanResources.Count > 0)
            {
                if (!string.IsNullOrEmpty(emp.HHumanResources[0].UrlPhotoThumbnail))
                {
                    image.ImageUrl = Page.ResolveUrl(Config.UploadLocation + "/" + emp.HHumanResources[0].UrlPhotoThumbnail);
                    if (File.Exists(Server.MapPath(image.ImageUrl)))
                        hasPhoto = true;
                }
            }
            if (!hasPhoto)
            {
                image.ImageUrl = Page.ResolveUrl(Web.Helper.WebHelper.GetEmployeeNoImage(emp.EmployeeId));
            }

            NewHRManager _NewHRManager = new NewHRManager();
            HHumanResource _HHumanResource = _NewHRManager.GetHumanResource(employeeId);
            if (_HHumanResource != null && !string.IsNullOrEmpty(_HHumanResource.UrlSignature))
            {
                string filename = "../Uploads/" + _HHumanResource.UrlSignature;
                imgSignature.ImageUrl = filename;
                signatureLink.NavigateUrl = imgSignature.ImageUrl;
            }


            if (emp.EAddresses != null)
            {
                EAddress entity = emp.EAddresses[0];

                string present = "", permanent = "";
                WebHelper.GetAddressHTML(entity, ref present, ref permanent);
                addressTemporary.Html = present;
                addressPermanent.Html = permanent;




            }
        }

    }
}