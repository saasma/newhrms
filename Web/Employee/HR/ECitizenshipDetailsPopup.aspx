﻿<%@ Page Title="Citizenship Details" Language="C#" MasterPageFile="~/Master/ForEmployeePopupPage.Master"
    AutoEventWireup="true" CodeBehind="ECitizenshipDetailsPopup.aspx.cs" Inherits="Web.Employee.HR.ECitizenshipDetailsPopup" %>

<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">

        var skipLoadingCheck = true;

        function closePopup() {
            window.close();
            window.opener.reloadCtznGrid();
        }
  
    </script>
    <style type="text/css">
        input[type=text], textarea, select
        {
            border: 1px solid #A2B4C6;
        }
        textarea
        {
            height: 50px !important;
        }
    </style>
    <script type="text/javascript">
        function afterRender() { }
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="mainContent" runat="server">
    <ext:ResourceManager ID="ResourceManager1" Namespace="" ScriptMode="Release" runat="server" />
    <ext:Hidden runat="server" ID="hdnCitizenshipID">
    </ext:Hidden>
    <div class="popupHeader">
        <h4 style="margin-top: 0px; color: White; margin-left: 20px;">
            Citizenship Details</h4>
    </div>
    <div class="" style='margin-top: 0px'>
        <table class="fieldTable" style="margin-left: 20px;">
            <tr>
                <td>
                    <ext:ComboBox ID="cmbNationality" Width="180px" runat="server" ValueField="Nationality"
                        DisplayField="Nationality" FieldLabel="Nationality" LabelAlign="top" LabelSeparator=""
                        ForceSelection="true" QueryMode="Local">
                        <%--<Items>
                            <ext:ListItem Text="Nepali" Value="Nepali" />
                            <ext:ListItem Text="Indian" Value="Indian" />
                        </Items>--%>
                        <Store>
                            <ext:Store ID="Store2" runat="server">
                                <Model>
                                    <ext:Model ID="Model4" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="Nationality" Type="String" />
                                            <ext:ModelField Name="Nationality" Type="String" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                    </ext:ComboBox>
                    <asp:RequiredFieldValidator Display="None" ID="valcmbNationality" runat="server"
                        ValidationGroup="CitizenshipSaveUpdate" ControlToValidate="cmbNationality" ErrorMessage="Please choose a Nationality." />
                </td>
            </tr>
            <tr>
                <td valign="top">
                    <ext:TextField ID="txtCitizenshipNumber" Width="180px" runat="server" FieldLabel="Citizenship Number"
                        LabelAlign="top" LabelSeparator="" />
                    <asp:RequiredFieldValidator Display="None" ID="valtxtCitizenshipNumber" runat="server"
                        ValidationGroup="CitizenshipSaveUpdate" ControlToValidate="txtCitizenshipNumber"
                        ErrorMessage="Citizenship Number is required." />
                </td>
            </tr>
            <tr>
                <td>
                    <%--  <ext:DateField ID="calIssueDate" runat="server" Width="150" LabelAlign="Top" FieldLabel="Issue Date"
                                                LabelSeparator="">
                                            </ext:DateField>--%>
                    <pr:CalendarExtControl FieldLabel="Issue Date" Width="180px" ID="calIssueDate" runat="server"
                        LabelSeparator="" LabelAlign="Top" />
                    <asp:RequiredFieldValidator Display="None" ID="valcalIssueDate" runat="server" ValidationGroup="CitizenshipSaveUpdate"
                        ControlToValidate="calIssueDate" ErrorMessage="Please enter citizenship Issue Date." />
                </td>
                <td style="padding-top: 5px">
                    <ext:TextField ID="txtPlace" Width="180px" runat="server" FieldLabel="Place" LabelAlign="top"
                        LabelSeparator="" />
                    <asp:RequiredFieldValidator Display="None" ID="valtxtPlace" runat="server" ValidationGroup="CitizenshipSaveUpdate"
                        ControlToValidate="txtPlace" ErrorMessage="Please type the Place." />
                </td>
            </tr>
            <tr>
                <td style="padding-top: 5px;" valign="top">
                    <ext:FileUploadField ID="FileCitizenshipDocumentUpload" runat="server" Width="180px"
                        Icon="Attach" FieldLabel="&nbsp;" LabelAlign="Top" LabelSeparator="" />
                    <%-- <span style="color: #ACACAC; padding-top: 20px;">*(only doc,pdf,txt,docx,rar)</span>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ValidationExpression="^.*\.(doc|DOC|pdf|PDF|txt|TXT|docx|DOCX|rar|RAR)$"
                        runat="server" ValidationGroup="CitizenshipSaveUpdate" Display="None" ControlToValidate="FileCitizenshipDocumentUpload"
                        ErrorMessage="Invalid file format."></asp:RegularExpressionValidator>--%>
                </td>
                <td>
                    <table style="margin-top: 15px">
                        <tr>
                            <td>
                                <ext:Label ID="lblUploadedFile" runat="server" Hidden="true">
                                </ext:Label>
                            </td>
                            <td>
                                <ext:LinkButton Icon="Delete" runat="server" ID="lnkDeleteFile" OnDirectClick="lnkDeleteFile_Click"
                                    Text="Delete file" Hidden="true">
                                    <DirectEvents>
                                        <Click OnEvent="lnkDeleteFile_Click">
                                            <EventMask ShowMask="true" />
                                        </Click>
                                    </DirectEvents>
                                </ext:LinkButton>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td valign="bottom" colspan="2">
                    <div class="popupButtonDiv">
                        <ext:Button runat="server" ID="btnSave" Cls="btn btn-primary" Text="<i></i>Save">
                            <DirectEvents>
                                <Click OnEvent="btnSave_Click">
                                    <EventMask ShowMask="true" />
                                </Click>
                            </DirectEvents>
                            <Listeners>
                                <Click Handler="valGroup = 'CitizenshipSaveUpdate'; return CheckValidation();">
                                </Click>
                            </Listeners>
                        </ext:Button>
                        <div class="btnFlatOr">
                            or</div>
                        <ext:LinkButton ID="LinkButton2" StyleSpec="padding:0px;" runat="server" Text="Cancel"
                            OnClientClick="window.close();" Cls="btnFlatLeftGap">
                        </ext:LinkButton>
                    </div>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
