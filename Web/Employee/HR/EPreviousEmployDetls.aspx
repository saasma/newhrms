﻿<%@ Page Title="Previous Employment Details" Language="C#" MasterPageFile="~/Master/ForEmployeePopupPage.Master"
    AutoEventWireup="true" CodeBehind="EPreviousEmployDetls.aspx.cs" Inherits="Web.Employee.HR.EPreviousEmployDetls" %>

<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        
    </style>
    <script type="text/javascript">


        var skipLoadingCheck = true;

        function closePopup() {
            window.close();
            window.opener.reloadPrevEmpGrid();
        }

    

    </script>
    <style type="text/css">
        input[type=text], textarea, select
        {
            border: 1px solid #A2B4C6;
        }
        textarea
        {
            height: 50px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="mainContent" runat="server">
    <ext:ResourceManager ID="ResourceManager1" runat="server" />
    <ext:Hidden runat="server" ID="hdnPreviousEmploymentID" />
    <div class="popupHeader">
        <h4 style="margin-top: 0px; color: White; margin-left: 20px;">
            Previous Employment Details</h4>
    </div>
    <div>
        <div style="margin-left:20px">
            <ext:ComboBox ID="cmbExpCategory" runat="server" ValueField="CategoryID" DisplayField="Name" 
                FieldLabel="Experience Category" LabelAlign="top" LabelSeparator="" ForceSelection="true"
                QueryMode="Local">
                <Store>
                    <ext:Store ID="Store1" runat="server">
                        <Model>
                            <ext:Model ID="Model2" runat="server">
                                <Fields>
                                    <ext:ModelField Name="CategoryID" Type="String" />
                                    <ext:ModelField Name="Name" Type="String" />
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
            </ext:ComboBox>
        </div>
        <table style="margin-left: 20px;">
            <tr>
                <td>
                    <ext:TextField ID="txtOrganization" Width="180px" runat="server" FieldLabel="Organization *"
                        LabelAlign="top" LabelSeparator="" />
                    <asp:RequiredFieldValidator Display="None" ID="valtxtOrganization" runat="server"
                        ValidationGroup="EmploymentHistorySaveUpdate" ControlToValidate="txtOrganization"
                        ErrorMessage="Please enter the Organization." />
                </td>
                <td style="padding-top: 5px">
                    <ext:TextField ID="txtPlace" Width="180px" runat="server" FieldLabel="Place *" LabelAlign="top"
                        LabelSeparator="" />
                    <asp:RequiredFieldValidator Display="None" ID="valtxtPlace" runat="server" ValidationGroup="EmploymentHistorySaveUpdate"
                        ControlToValidate="txtPlace" ErrorMessage="Please type a Place." />
                </td>
            </tr>
            <tr>
                <td style="padding-top: 5px">
                    <ext:TextField ID="txtPosition" Width="180px" runat="server" FieldLabel="Position *"
                        LabelAlign="top" LabelSeparator="" />
                    <asp:RequiredFieldValidator Display="None" ID="valtxtPosition" runat="server" ValidationGroup="EmploymentHistorySaveUpdate"
                        ControlToValidate="txtPosition" ErrorMessage="Please enter Position." />
                </td>
                <td style="padding-top: 5px">
                    <ext:TextField ID="txtJobResponsibility" Width="180px" runat="server" FieldLabel="Job Responsibility *"
                        LabelAlign="top" LabelSeparator="" />
                    <asp:RequiredFieldValidator Display="None" ID="valtxtJobResponsibility" runat="server"
                        ValidationGroup="EmploymentHistorySaveUpdate" ControlToValidate="txtJobResponsibility"
                        ErrorMessage="Please type the Job Responsibility." />
                </td>
            </tr>
            <tr>
                <td>
                    <pr:CalendarExtControl FieldLabel="From " Width="180px" ID="calFrom" runat="server"
                        LabelSeparator="" LabelAlign="Top" />
                    <%--<asp:RequiredFieldValidator Display="None" ID="valcalFrom" runat="server" ValidationGroup="EmploymentHistorySaveUpdate"
                        ControlToValidate="calFrom" ErrorMessage="Please enter From Date." />--%>
                </td>
                <td>
                    <pr:CalendarExtControl FieldLabel="To" Width="180px" ID="calTo" runat="server" LabelSeparator=""
                        LabelAlign="Top" />
                    <%--<asp:RequiredFieldValidator Display="None" ID="valcalTo" runat="server" ValidationGroup="EmploymentHistorySaveUpdate"
                        ControlToValidate="calTo" ErrorMessage="Please enter To Date." />--%>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <ext:TextField ID="txtReasonforLeaving" runat="server" FieldLabel="Reason for Leaving *"
                        Width="375" LabelAlign="top" LabelSeparator="" />
                    <asp:RequiredFieldValidator Display="None" ID="valtxtReasonforLeaving" runat="server"
                        ValidationGroup="EmploymentHistorySaveUpdate" ControlToValidate="txtReasonforLeaving"
                        ErrorMessage="Please type Reason for Leaving." />
            </tr>
            <tr>
                <td colspan="2">
                    <ext:TextArea ID="txtNote" runat="server" FieldStyle="height:50px!important" Width="375px"
                        FieldLabel="Note" LabelAlign="Top" LabelSeparator="">
                    </ext:TextArea>
            </tr>
            <tr>
                <td valign="bottom" colspan="2">
                    <div class="popupButtonDiv">
                        <ext:Button runat="server" ID="btnSave" Cls="btn btn-primary" Text="<i></i>Save">
                            <DirectEvents>
                                <Click OnEvent="btnSave_Click">
                                    <EventMask ShowMask="true" />
                                </Click>
                            </DirectEvents>
                            <Listeners>
                                <Click Handler="valGroup = 'EmploymentHistorySaveUpdate'; if(CheckValidation()) return this.disable(); else return false;">
                                </Click>
                            </Listeners>
                        </ext:Button>
                        <div class="btnFlatOr">
                            or</div>
                        <ext:LinkButton runat="server" StyleSpec="padding:0px;" ID="LinkButton2" Cls="btnFlatLeftGap"
                            OnClientClick="window.close();" Text="<i></i>Cancel">
                        </ext:LinkButton>
                    </div>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
