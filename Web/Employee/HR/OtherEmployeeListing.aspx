﻿<%@ Page Title="Employee List" Language="C#" MasterPageFile="~/Master/EmployeeExt.Master" AutoEventWireup="true" CodeBehind="OtherEmployeeListing.aspx.cs" Inherits="Web.Employee.HR.OtherEmployeeListing" %>
<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">

<script type="text/javascript">

    var CommandHandler = function (command, record) {

        if (command == 'View') {
            window.location = 'OtherEmployeeDetails.aspx?id=' + record.data.EmployeeId;
        }

    };


</script>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="mainContent" runat="server">

<div class="innerLR1">
        

        <h2 style="margin-top:10px">
            Employee List
        </h2>
        <br />

            <ext:GridPanel runat="server" ID="gridOtherEmpList" Width="780" Cls="itemgrid">
               <Store>
                        <ext:Store ID="storeOtherEmpList" runat="server">
                            <Model>
                                <ext:Model ID="Model1" runat="server" IDProperty="EmployeeId">
                                    <Fields>
                                        <ext:ModelField Name="EmployeeId" Type="string" />
                                        <ext:ModelField Name="Name" Type="string" />
                                        <ext:ModelField Name="Branch" Type="string" />
                                        
                                        <ext:ModelField Name="Department" Type="string" />
                                        <ext:ModelField Name="Designation" Type="String" />
                                        <ext:ModelField Name="IdCardNo" Type="string" />                                        
                                    </Fields>
                                </ext:Model>
                            </Model>
                        </ext:Store>
                    </Store>
                <ColumnModel>
                        <Columns>
                            <ext:Column ID="colEmployeeId" Sortable="false" MenuDisabled="true" runat="server" Text="EIN"
                                Align="Left" Width="40" DataIndex="EmployeeId" />

                             <ext:Column ID="colEmployeeName" Sortable="false" MenuDisabled="true" runat="server" Text="Employee Name"
                                Align="Left" Width="180" DataIndex="Name" />

                            <ext:Column ID="colBranch" Sortable="false" MenuDisabled="true" runat="server" Text="Branch"
                                Align="Left" Width="140" DataIndex="Branch" />

                            <ext:Column ID="colDepartment" Width="100" Sortable="false" MenuDisabled="true" runat="server" 
                                Text="Department" Align="Left" DataIndex="Department">                               
                            </ext:Column>

                            <ext:Column ID="colDesignation" Width="100" Sortable="false" MenuDisabled="true" runat="server" Text="Designation"
                                 Align="Left" DataIndex="Designation">                               
                            </ext:Column>
                          
                             <ext:Column ID="colIdCardNo" Width="150" Sortable="false" MenuDisabled="true" runat="server" Text="Id Card No"
                                 Align="Left" DataIndex="IdCardNo">                               
                            </ext:Column>
                           
                            <ext:CommandColumn ID="CommandColumn1" runat="server" Width="70" Sortable="false" MenuDisabled="true" Text="Actions" Align="Center">
                                <Commands>
                                    <ext:CommandSeparator />
                                    <ext:GridCommand Text="<i></i>View"   CommandName="View" ToolTip-Text="View" />                                   
                                </Commands>                               

                                <Listeners>
                                    <Command Handler="CommandHandler(command,record);" />
                                    
                                </Listeners>                              
                               
                            </ext:CommandColumn>

                        </Columns>
                    </ColumnModel>
                    <SelectionModel>
                        <ext:RowSelectionModel ID="RowSelectionModel2111" runat="server" Mode="Single" />
                    </SelectionModel>

                </ext:GridPanel>
 
        
    </div>

</asp:Content>
