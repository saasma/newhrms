﻿<%@ Page Title="Extra Activity Details" Language="C#" MasterPageFile="~/Master/ForEmployeePopupPage.Master" AutoEventWireup="true" CodeBehind="EExtraActivityDetlsPopup.aspx.cs" Inherits="Web.Employee.HR.EExtraActivityDetlsPopup" %>

<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">

<script type="text/javascript">

    var skipLoadingCheck = true;


    function closePopup() {
        window.close();
        window.opener.reloadActivityGrid();
    }

</script>

 <style type="text/css">
        input[type=text], textarea, select
        {
            border: 1px solid #A2B4C6;
        }
        textarea
        {
            height: 50px !important;
        }
    </style>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="mainContent" runat="server">

<ext:ResourceManager ID="ResourceManager1"  Namespace=""  ScriptMode="Release"  runat="server" />

<ext:Hidden runat="server" ID="hdnCurricularId" />

<div class="popupHeader">
         <h4 style="margin-top:0px; color:White; margin-left:20px;">
            Extra Activity Details</h4>
    </div>

<div class="" style='margin-top: 0px'>

    <table class="fieldTable" style="margin-left:20px;">  
            <tr>
                <td colspan="2">
                    <ext:TextField Width="330" ID="txtActivityName" runat="server" FieldLabel="Activity Name *"
                        LabelAlign="top" LabelSeparator="" />
                    <asp:RequiredFieldValidator Display="None" ID="rfvActivityName" runat="server"
                        ValidationGroup="SaveUpdateActivity" ControlToValidate="txtActivityName" ErrorMessage="Activity Name is required." />
                </td>
               
            </tr>
            <tr>
                <td colspan="2">
                    <ext:TextField Width="330" ID="txtAward" runat="server" FieldLabel="Award"
                        LabelAlign="top" LabelSeparator="" />
                </td>
           
            </tr>
            <tr>
                <td>
                    <ext:TextField ID="txtYear" Width="160" runat="server" FieldLabel="Year *" MaskRe="/[0-9]/"
                        LabelAlign="top" LabelSeparator="" />
                    <asp:RequiredFieldValidator Display="None" ID="rfvYear" runat="server"
                        ValidationGroup="SaveUpdateActivity" ControlToValidate="txtYear"
                        ErrorMessage="Year is required." />
                 
                </td>
                <td>
                     <ext:TextField Width="160" ID="txtProficiency" runat="server" FieldLabel="Proficiency"
                        LabelAlign="top" LabelSeparator="" />     
                </td>
            </tr>
             <td colspan="2">
                    <ext:TextArea ID="txtRemarks" Width="330" runat="server" FieldLabel="Remarks" LabelAlign="Top"
                        LabelSeparator="" />
                </td>

    
            <tr>
                <td valign="bottom" colspan="2">
                    <div class="popupButtonDiv">
                        <ext:Button runat="server" Cls="btn btn-primary" ID="btnSave" Text="<i></i>Save">
                            <DirectEvents>
                                <Click OnEvent="btnSave_Click">
                                    <EventMask ShowMask="true" />
                                </Click>
                            </DirectEvents>
                            <Listeners>
                                <Click Handler="valGroup = 'SaveUpdateActivity';  if(CheckValidation()) return this.disable(); else return false;">
                                </Click>
                            </Listeners>
                        </ext:Button>
                        <div class="btnFlatOr">
                            or</div>
                        <ext:LinkButton runat="server" StyleSpec="padding:0px;" ID="LinkButton2" Cls="btnFlatLeftGap"
                            Text="<i></i>Cancel" OnClientClick="window.close();">
                        </ext:LinkButton>
                    </div>
                </td>
            </tr>
        </table>

</div>

</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
