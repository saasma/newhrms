﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using BLL.BO;
using System.IO;

namespace Web.Employee.HR
{
    public partial class EPublicationDetlsPopup : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();
            }
        }

        protected void Initialise()
        {
            int EmployeeID = GetEmployeeID();
            //Load Data
            if (EmployeeID > 0)
            {
                CommonManager _CommonManager = new CommonManager();
                cmbCountry.Store[0].DataSource = _CommonManager.GetAllCountries();
                cmbCountry.Store[0].DataBind();

                cmbPublicationType.Store[0].DataSource = CommonManager.GetPublicationTypeList();
                cmbPublicationType.Store[0].DataBind();
            }

            if (!string.IsNullOrEmpty(Request.QueryString["PubId"]))
            {
                hdnPublicationID.Text = Request.QueryString["PubId"];
                editPublication(int.Parse(hdnPublicationID.Text));
            }
        }

        protected void btnSave_Click(object sender, DirectEventArgs e)
        {
            this.SaveFormData();
        }

        private void SaveFormData()
        {
            Status myStatus = new Status();
            HPublication _HPublication = new HPublication();
            _HPublication.EmployeeId = this.GetEmployeeID();
            bool isSave = bool.Parse(this.hdnPublicationID.Text == "" ? "true" : "false");
            if (!isSave)
                _HPublication.PublicationId = int.Parse(this.hdnPublicationID.Text);

            _HPublication.PublicationName = txtPublicationName.Text.Trim();

            if (!string.IsNullOrEmpty(cmbPublicationType.SelectedItem.Text))
            {
                _HPublication.PublicationTypeName = cmbPublicationType.SelectedItem.Text;
                _HPublication.PublicationTypeID = int.Parse(cmbPublicationType.SelectedItem.Value);
            }

            if (!string.IsNullOrEmpty(cmbCountry.SelectedItem.Text))
            {

                _HPublication.Country = cmbCountry.SelectedItem.Text;

            }

            _HPublication.Publisher = txtPublisher.Text.Trim();
            if (!string.IsNullOrEmpty(txtYear.Text.Trim()))
                _HPublication.Year = int.Parse(txtYear.Text.Trim());


            myStatus = NewHRManager.Instance.InsertUpdatePublication(_HPublication, isSave);

            if (myStatus.IsSuccess)
            {
                if (isSave)
                    NewMessage.ShowNormalMessage("Record Saved Successfully.", "closePopup()");
                else
                    NewMessage.ShowNormalMessage("Record Updated Successfully.", "closePopup()");

                btnSave.Hide();
            }
            else
                NewMessage.ShowWarningMessage(myStatus.ErrorMessage);
        }

        protected int GetEmployeeID()
        {
            //Guid? AssetID = null;
            int EmpID = 0;
            if (!string.IsNullOrEmpty(Request.QueryString["ID"]))
            {
                EmpID = int.Parse(Request.QueryString["ID"]);
            }
            else
            {
                EmpID = SessionManager.CurrentLoggedInEmployeeId;
            }
            return EmpID;
        }

        private object[] PublicationFillData
        {
            get
            {
                return new object[]
            {
                new object[] { "", "", "",""},
               
            };
            }
        }

        private void ClearFields()
        {
            this.hdnPublicationID.Text = "";
            txtPublicationName.Text = "";
            txtPublisher.Text = "";
            cmbCountry.Value = "";
            txtYear.Text = "";
            cmbPublicationType.Value = "";
            btnSave.Text = Resources.Messages.Save;
        }

        private void editPublication(int PublicationID)
        {
            HPublication _HPublication = NewHRManager.GetPublicationDetailsById(PublicationID);
            this.hdnPublicationID.Text = PublicationID.ToString();
            txtPublicationName.Text = _HPublication.PublicationName;

            //cmbPublicationType.Store[0].ClearFilter();
            if (_HPublication.PublicationTypeID != null)
                cmbPublicationType.SelectedItem.Value = _HPublication.PublicationTypeID.ToString();
            else
                cmbPublicationType.ClearValue();

            txtPublisher.Text = _HPublication.Publisher;

            cmbCountry.SelectedItem.Value = _HPublication.Country;

            txtYear.Text = _HPublication.Year.ToString();
            btnSave.Text = Resources.Messages.Update;
        }

        public void HideButtons()
        {
            btnSave.Hidden = true;
            LinkButton2.Hidden = true;
        }


    }
}