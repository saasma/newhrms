﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using BLL.BO;
using System.IO;

namespace Web.Employee.HR
{
    public partial class EPreviousEmployDetls : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();
            }
        }

        protected void Initialise()
        {
            cmbExpCategory.Store[0].DataSource = NewHRManager.GetAllExperienceCategory();
            cmbExpCategory.Store[0].DataBind();

            ClearFields();

            int EmployeeID = GetEmployeeID();

            if(!string.IsNullOrEmpty(Request.QueryString["PrevEmpId"]))
            {
                hdnPreviousEmploymentID.Text  = Request.QueryString["PrevEmpId"];
                editPreviousEmployment(int.Parse(hdnPreviousEmploymentID.Text));
            }
        }

        protected void btnSave_Click(object sender, DirectEventArgs e)
        {
            this.SaveFormData();
        }

        private void SaveFormData()
        {

            Status myStatus = new Status();
            HPreviousEmployment _HPreviousEmployment = new HPreviousEmployment();
            _HPreviousEmployment.EmployeeId = this.GetEmployeeID();
            bool isSave = bool.Parse(this.hdnPreviousEmploymentID.Text == "" ? "true" : "false");
            if (!isSave)
                _HPreviousEmployment.PrevEmploymentId = int.Parse(this.hdnPreviousEmploymentID.Text);

            if (cmbExpCategory.SelectedItem != null && cmbExpCategory.SelectedItem.Value != null)
                _HPreviousEmployment.CategoryID = int.Parse(cmbExpCategory.SelectedItem.Value);

            _HPreviousEmployment.Organization = txtOrganization.Text.Trim();
            _HPreviousEmployment.Place = txtPlace.Text.Trim();
            _HPreviousEmployment.Position = txtPosition.Text.Trim();
            _HPreviousEmployment.JobResponsibility = txtJobResponsibility.Text.Trim();
            if (!string.IsNullOrEmpty(calFrom.Text.Trim()))
            {
                _HPreviousEmployment.JoinDate = calFrom.Text.Trim();
                _HPreviousEmployment.JoinDateEng = BLL.BaseBiz.GetEngDate(_HPreviousEmployment.JoinDate, IsEnglish);
            }
            if (!string.IsNullOrEmpty(calTo.Text.Trim()))
            {
                _HPreviousEmployment.ResignDate = calTo.Text.Trim();
                _HPreviousEmployment.ResignDateEng = BLL.BaseBiz.GetEngDate(_HPreviousEmployment.ResignDate, IsEnglish);
            }
            _HPreviousEmployment.ReasonForLeaving = txtReasonforLeaving.Text.Trim();
            _HPreviousEmployment.Notes = txtNote.Text.Trim();

            myStatus = NewHRManager.Instance.InsertUpdatePreviousEmployment(_HPreviousEmployment, isSave);

            if (myStatus.IsSuccess)
            {              
                if (isSave)
                    NewMessage.ShowNormalMessage("Record Saved Successfully.", "closePopup()");
                else
                    NewMessage.ShowNormalMessage("Record Updated Successfully.", "closePopup()");
            }
            else
                NewMessage.ShowWarningMessage(myStatus.ErrorMessage);
        }

        protected int GetEmployeeID()
        {
            int EmpID = 0;
            if (!string.IsNullOrEmpty(Request.QueryString["ID"]))
            {
                EmpID = int.Parse(Request.QueryString["ID"]);
            }
            else
            {
                EmpID = SessionManager.CurrentLoggedInEmployeeId;
            }
            return EmpID;
        }

        public void ClearFields()
        {
            this.hdnPreviousEmploymentID.Text = "";
            cmbExpCategory.Text = "";
            txtOrganization.Text = "";
            txtPlace.Text = "";
            txtPosition.Text = "";
            txtJobResponsibility.Text = "";
            calFrom.Text = "";
            calTo.Text = "";
            txtReasonforLeaving.Text = "";
            txtNote.Text = "";
            btnSave.Text = Resources.Messages.Save;
        }

        private void editPreviousEmployment(int PreviousEmploymentID)
        {
            HPreviousEmployment _HPreviousEmployment = NewHRManager.GetPreviousEmploymentDetailsById(PreviousEmploymentID);
            this.hdnPreviousEmploymentID.Text = PreviousEmploymentID.ToString();

            txtOrganization.Text = _HPreviousEmployment.Organization;
            txtPlace.Text = _HPreviousEmployment.Place;
            txtPosition.Text = _HPreviousEmployment.Position;
            txtJobResponsibility.Text = _HPreviousEmployment.JobResponsibility;
            calFrom.Text = _HPreviousEmployment.JoinDate;
            calTo.Text = _HPreviousEmployment.ResignDate;
            txtReasonforLeaving.Text = _HPreviousEmployment.ReasonForLeaving;
            if (_HPreviousEmployment.CategoryID != null)
                cmbExpCategory.SelectedItem.Value = _HPreviousEmployment.CategoryID.ToString();
            txtNote.Text = _HPreviousEmployment.Notes;
            btnSave.Text = Resources.Messages.Update;
        }

        public void HideButtons()
        {
            btnSave.Hidden = true;
            LinkButton2.Hidden = true;
        }


    }
}