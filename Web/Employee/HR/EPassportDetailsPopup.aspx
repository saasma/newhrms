﻿<%@ Page Title="Passport Details" Language="C#" MasterPageFile="~/Master/ForEmployeePopupPage.Master" AutoEventWireup="true" CodeBehind="EPassportDetailsPopup.aspx.cs" Inherits="Web.Employee.HR.EPassportDetailsPopup" %>

<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">

<script type="text/javascript">

    var skipLoadingCheck = true;

    function closePassPopup() {
        window.close();
        window.opener.reloadPassportGrid();
    }
  
</script>
<script type="text/javascript">
    function afterRender() { }
</script>
 <style type="text/css">
        input[type=text], textarea, select
        {
            border: 1px solid #A2B4C6;
        }
        textarea
        {
            height: 50px !important;
        }
    </style>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="mainContent" runat="server">

<ext:ResourceManager ID="ResourceManager1"  Namespace=""  ScriptMode="Release" runat="server" />

<ext:Hidden runat="server" ID="hdnPassportID" />

<div class="popupHeader">
        <h4 style="margin-top:0px; color:White; margin-left:20px;">
            Passport Details</h4>
    </div>



<div class="" style='margin-top: 0px'>

    <table class="fieldTable" style="margin-left:20px;">   
        <tr>
                <td>
                    <ext:TextField ID="txtPassportNumber" Width="180px" runat="server" FieldLabel="Passport Number"
                        LabelAlign="top" LabelSeparator="" />
                    <asp:RequiredFieldValidator Display="None" ID="valtxtPassportNumber" runat="server"
                        ValidationGroup="PassportSaveUpdate" ControlToValidate="txtPassportNumber" ErrorMessage="Please enter Passport Number." />
                </td>
            </tr>
            <tr>
                <td>
                    <pr:CalendarExtControl FieldLabel="Issue Date" Width="180px" ID="calIssueDate" runat="server"
                        LabelSeparator="" LabelAlign="Top" />
                    <asp:RequiredFieldValidator Display="None" ID="valcalIssueDate" runat="server" ValidationGroup="PassportSaveUpdate"
                        ControlToValidate="calIssueDate" ErrorMessage="Please enter passport Issue Date." />
                </td>
                <td style="padding-top: 5px">
                    <pr:CalendarExtControl FieldLabel="Valid Upto" Width="180px" ID="calValidUpto" runat="server"
                        LabelSeparator="" LabelAlign="Top" />
                    <asp:RequiredFieldValidator Display="None" ID="valcalValidUpto" runat="server" ValidationGroup="PassportSaveUpdate"
                        ControlToValidate="calValidUpto" ErrorMessage="Please check Valid Upto." />
                </td>
            </tr>
            <tr>
                <td>
                    <ext:FileUploadField ID="FilePassportDocumentUpload" runat="server" Width="200" Icon="Attach"
                        FieldLabel="&nbsp;" LabelAlign="Top" LabelSeparator="" />
                </td>
                <td valign="bottom">
                    <table>
                        <tr>
                            <td>
                                <ext:Label ID="lblUploadedFile" runat="server" Hidden="true">
                                </ext:Label>
                            </td>
                            <td>
                                <ext:LinkButton Icon="Delete" runat="server" ID="lnkDeleteFile" OnDirectClick="lnkDeleteFile_Click"
                                    Text="Delete file" Hidden="true">
                                    <DirectEvents>
                                        <Click OnEvent="lnkDeleteFile_Click">
                                         <Confirmation ConfirmRequest="true" Message="Do you want to delete the file?" />
                                            <EventMask ShowMask="true" />
                                        </Click>
                                    </DirectEvents>
                                </ext:LinkButton>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>

            <tr>
                <td valign="bottom" colspan="2">
                    <div class="popupButtonDiv">
                        <ext:Button runat="server" ID="btnSave" Cls="btn btn-primary" Text="<i></i>Save"
                            >
                            <DirectEvents>
                                <Click OnEvent="btnSave_Click">
                                    <EventMask ShowMask="true" />
                                </Click>
                            </DirectEvents>
                            <Listeners>
                                <Click Handler="valGroup = 'PassportSaveUpdate'; return CheckValidation();">
                                </Click>
                            </Listeners>
                        </ext:Button>
                        <div class="btnFlatOr">
                            or</div>

                        <ext:LinkButton ID="LinkButton1" StyleSpec="padding:0px;" runat="server" Text="Cancel" OnClientClick="window.close();"
                                        Cls="btnFlatLeftGap">
                                    </ext:LinkButton>                       
                    </div>
                </td>
            </tr>
           
    </table>
</div>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
