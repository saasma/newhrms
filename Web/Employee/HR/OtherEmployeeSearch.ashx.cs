﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;

namespace Web.Employee.HR
{
    /// <summary>
    /// Summary description for OtherEmployeeSearch
    /// </summary>
    public class OtherEmployeeSearch : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {

            if (HttpContext.Current.User.Identity.IsAuthenticated == false )
            {
                context.Response.Write("");
                context.Response.End();
                return;
            }

            context.Response.ContentType = "application/json";
            int start = 0;
            int pagesize = 50;
            int EmployeeId = 0;
            string empName = string.Empty;

            if (!string.IsNullOrEmpty(context.Request["start"]))
                start = Convert.ToInt32(context.Request["start"].ToString());

            if (!string.IsNullOrEmpty(context.Request["PageSize"]))
                pagesize = Convert.ToInt32(context.Request["PageSize"]);

            if (!string.IsNullOrEmpty(context.Request["EmpName"]))
                empName = context.Request["EmpName"].ToString();
            else
                return;

            if(!string.IsNullOrEmpty(context.Request["EmpId"]))
                EmployeeId = Convert.ToInt32(context.Request["EmpId"].ToString());

            if (!string.IsNullOrEmpty(empName))
            {
                if (empName.Contains('-'))
                {
                    string[] split = empName.Split('-');
                    empName = split[0].ToString().Substring(0, split[0].ToString().Length - 1);                   
                }
            }

            int totalRecords = 0;

            Paging<GetEmployeeSearchListResult> list = null;
            List<GetEmployeeSearchListResult> resultset = NewHRManager.GetEmployeeSearchList(start, pagesize, empName, EmployeeId, ref totalRecords);
            if (resultset.Count > 0)
                list = new Paging<GetEmployeeSearchListResult>(resultset, Convert.ToInt32(resultset[0].TotalRows));
            else
                list = new Paging<GetEmployeeSearchListResult>(resultset, 0);

            context.Response.Write(JSON.Serialize(list));
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}