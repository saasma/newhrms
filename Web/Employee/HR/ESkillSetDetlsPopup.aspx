﻿<%@ Page Title="Skill Set Details" Language="C#" MasterPageFile="~/Master/ForEmployeePopupPage.Master"
    AutoEventWireup="true" CodeBehind="ESkillSetDetlsPopup.aspx.cs" Inherits="Web.Employee.HR.ESkillSetDetlsPopup" %>

<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">

        var skipLoadingCheck = true;

        function closePopup() {
            window.close();
            window.opener.reloadSkillSetGrid();
        }


    </script>
    <style type="text/css">
        input[type=text], textarea, select
        {
            border: 1px solid #A2B4C6;
        }
        textarea
        {
            height: 50px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="mainContent" runat="server">
    <ext:ResourceManager ID="ResourceManager1" Namespace="" ScriptMode="Release" runat="server" />
    <ext:Hidden runat="server" ID="hdnSkillSetsID" />
    <div class="popupHeader">
        <h4 style="margin-top: 0px; color: White; margin-left: 20px;">
            Skill Set Details</h4>
    </div>
    <div style="margin-left: 30px;">
        <ext:ComboBox ID="cmbSkill" runat="server" ValueField="SkillSetId" DisplayField="Name"
            FieldLabel="Skill" LabelAlign="top" LabelSeparator="" QueryMode="Local">
            <Store>
                <ext:Store ID="Store2" runat="server">
                    <Model>
                        <ext:Model ID="Model4" runat="server">
                            <Fields>
                                <ext:ModelField Name="SkillSetId" />
                                <ext:ModelField Name="Name" />
                            </Fields>
                        </ext:Model>
                    </Model>
                </ext:Store>
            </Store>
        </ext:ComboBox>
    </div>
    <div style="margin-left: 30px;">
        <ext:TextField ID="txtNewSkill" runat="server" FieldLabel="Add a new skill" LabelAlign="top"
            Width="200" LabelSeparator="" />
    </div>
    <div class="" style='margin-top: 0px'>
        <table class="fieldTable" style="margin-left: 20px;">
            <tr>
                <td>
                    <ext:Checkbox ID="chkAddtoSkillPool" runat="server" FieldLabel="Add to Skill Pool"
                        LabelSeparator="">
                    </ext:Checkbox>
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <table class="fieldTable firsttdskip">
                        <tr>
                            <td>
                                <ext:ComboBox ID="cmbLevelOfExpertise" runat="server" ValueField="LevelID" DisplayField="Name"
                                    Width="150" FieldLabel="Level of Expertise" LabelAlign="top" LabelSeparator=""
                                    ForceSelection="true" QueryMode="Local">
                                    <Store>
                                        <ext:Store ID="Store1" runat="server">
                                            <Model>
                                                <ext:Model ID="Model2" runat="server">
                                                    <Fields>
                                                        <ext:ModelField Name="LevelID" />
                                                        <ext:ModelField Name="Name" />
                                                    </Fields>
                                                </ext:Model>
                                            </Model>
                                        </ext:Store>
                                    </Store>
                                </ext:ComboBox>
                                <%-- <asp:RequiredFieldValidator Display="None" ID="valcmbLevelOfExpertise" runat="server"
                                    ValidationGroup="SkillSetsSaveUpdate" ControlToValidate="cmbLevelOfExpertise"
                                    ErrorMessage="Level of Expertise is required." />--%>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <ext:TextArea ID="txtNote" runat="server" FieldLabel="Notes" LabelSeparator="" LabelAlign="Top"
                        Rows="3" Cols="50" />
                </td>
            </tr>
            <tr>
                <td valign="bottom" colspan="2">
                    <div class="popupButtonDiv">
                        <ext:Button runat="server" Cls="btn btn-primary" ID="btnSave" Text="<i></i>Save">
                            <DirectEvents>
                                <Click OnEvent="btnSave_Click">
                                    <EventMask ShowMask="true" />
                                </Click>
                            </DirectEvents>
                            <Listeners>
                                <Click Handler="valGroup = 'SkillSetsSaveUpdate'; if(CheckValidation()) return this.disable(); else return false;">
                                </Click>
                            </Listeners>
                        </ext:Button>
                        <div class="btnFlatOr">
                            or</div>
                        <ext:LinkButton runat="server" StyleSpec="padding:0px;" ID="LinkButton2" Cls="btnFlatLeftGap"
                            OnClientClick="window.close();" Text="<i></i>Cancel">
                        </ext:LinkButton>
                    </div>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
