﻿<%@ Page Title="Hobby Details" Language="C#" MasterPageFile="~/Master/ForEmployeePopupPage.Master"
    AutoEventWireup="true" CodeBehind="EHobbyDetlsPopup.aspx.cs" Inherits="Web.Employee.HR.EHobbyDetlsPopup" %>

<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">

        var skipLoadingCheck = true;

        function closePopup() {
            window.close();
            window.opener.reloadHobbyGrid();
        }

    </script>
    <style type="text/css">
        input[type=text], textarea, select
        {
            border: 1px solid #A2B4C6;
        }
        textarea
        {
            height: 50px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="mainContent" runat="server">
    <ext:ResourceManager ID="ResourceManager1" Namespace="" ScriptMode="Release" runat="server" />
    <ext:Hidden runat="server" ID="hdnHobbyId" />
    <div class="popupHeader">
        <h4 style="margin-top: 0px; color: White; margin-left: 20px;">
            Hobby Details</h4>
    </div>
    <div class="" style='margin-top: 0px'>
        <table class="" style="margin-left: 20px;position:relative;">
            <tr>
                <td style="width:200px">
                    <ext:ComboBox ID="cmbHobby" runat="server" ValueField="HobbyTypeId" DisplayField="HobbyName" Width="180" LabelWidth="60"
                        FieldLabel="Hobby *" LabelAlign="Top" LabelSeparator="" ForceSelection="true"
                        QueryMode="Local">
                        <Store>
                            <ext:Store ID="Store1" runat="server">
                                <Model>
                                    <ext:Model ID="Model3" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="HobbyTypeId" Type="String" />
                                            <ext:ModelField Name="HobbyName" Type="String" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                    </ext:ComboBox>
                    <asp:RequiredFieldValidator Display="None" ID="rfvHobby" runat="server" ValidationGroup="SaveUpdateHobby"
                        ControlToValidate="cmbHobby" ErrorMessage="Hobby is required." />
                </td>
                <td>
                    <pr:CalendarExtControl ID="txtInvolvedFrom" LabelSeparator="" runat="server" FieldLabel="Involved From Date"
                        Width="120" LabelAlign="Top">
                    </pr:CalendarExtControl>
                </td>
            </tr>
            <tr>
            <td colspan="2">
                <ext:TextArea ID="txtDescription" Width="330" runat="server" FieldLabel="Description"
                    LabelAlign="Top" LabelSeparator="" />
            </td>
            </tr>
            <tr>
                <td valign="bottom" colspan="2">
                    <div class="popupButtonDiv">
                        <ext:Button runat="server" Cls="btn btn-primary" ID="btnSave" Text="<i></i>Save">
                            <DirectEvents>
                                <Click OnEvent="btnSave_Click">
                                    <EventMask ShowMask="true" />
                                </Click>
                            </DirectEvents>
                            <Listeners>
                                <Click Handler="valGroup = 'SaveUpdateHobby';  if(CheckValidation()) return this.disable(); else return false;">
                                </Click>
                            </Listeners>
                        </ext:Button>
                        <div class="btnFlatOr">
                            or</div>
                        <ext:LinkButton runat="server" StyleSpec="padding:0px;" ID="LinkButton2" Cls="btnFlatLeftGap"
                            Text="<i></i>Cancel" OnClientClick="window.close();">
                        </ext:LinkButton>
                    </div>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
