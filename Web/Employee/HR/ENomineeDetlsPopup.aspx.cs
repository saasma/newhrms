﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL;
using BLL.Manager;
using BLL;
using BLL.BO;
using System.IO;
using BLL.Base;
using Ext.Net;


namespace Web.Employee.HR
{
    public partial class ENomineeDetlsPopup : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();
            }
        }

        private void Initialise()
        {
            ClearFields();
            BindRelatoinCombo();

            if (!string.IsNullOrEmpty(Request.QueryString["NomineeId"]))
            {
                hdnNomineeId.Text = Request.QueryString["NomineeId"];
                EditHrNominee(int.Parse(hdnNomineeId.Text));
            }
        }

        private void ClearFields()
        {
            txtNomineeName.Text = "";
            cmbRelation.ClearValue();
            txtEffectiveDate.Text = "";
            txtEffectiveDate.ReadOnly = false;
            txtRemarks.Text = "";
            btnSave.Text = Resources.Messages.Save;
        }

        private void BindRelatoinCombo()
        {
            cmbRelation.Store[0].DataSource = ListManager.GetFamilyRelations();
            cmbRelation.Store[0].DataBind();
        }

        protected void btnSave_Click(object sender, DirectEventArgs e)
        {
            Page.Validate("SaveUpdateNominee");
            if (Page.IsValid)
            {
                HrNominee obj = new HrNominee();
                if (!string.IsNullOrEmpty(hdnNomineeId.Text))
                    obj.NomineeId = int.Parse(hdnNomineeId.Text);

                obj.EmployeeId = GetEmployeeID();
                obj.NomineeName = txtNomineeName.Text.Trim();
                obj.RelationId = int.Parse(cmbRelation.SelectedItem.Value);
                obj.EffectiveDate = txtEffectiveDate.Text.Trim();
                obj.EffectiveDateEng = BLL.BaseBiz.GetEngDate(obj.EffectiveDate, IsEnglish);

                if (!string.IsNullOrEmpty(txtRemarks.Text.Trim()))
                    obj.Remarks = txtRemarks.Text.Trim();

                Status status = NewHRManager.SaveUpdateHrNominee(obj);
                if (status.IsSuccess)
                {
                    if (string.IsNullOrEmpty(hdnNomineeId.Text))
                        NewMessage.ShowNormalMessage("Record saved successfully.", "closePopup()");
                    else
                        NewMessage.ShowNormalMessage("Record updated successfully.", "closePopup()");

                    btnSave.Hide();
                }
                else
                    NewMessage.ShowWarningMessage(status.ErrorMessage);
            }

        }

        protected int GetEmployeeID()
        {
            int EmpID = 0;
            if (!string.IsNullOrEmpty(Request.QueryString["ID"]))
            {
                EmpID = int.Parse(Request.QueryString["ID"]);
            }
            else
            {
                EmpID = SessionManager.CurrentLoggedInEmployeeId;
            }
            return EmpID;
        }

        private void EditHrNominee(int nomineeId)
        {
            HrNominee obj = NewHRManager.GetHrNomineeByNomineeId(nomineeId);
            if (obj != null)
            {
                txtNomineeName.Text = obj.NomineeName;

                if (obj.RelationId != null)
                    cmbRelation.SelectedItem.Value = obj.RelationId.Value.ToString();
                else
                    cmbRelation.ClearValue();

                txtEffectiveDate.Text = obj.EffectiveDate;
                txtEffectiveDate.ReadOnly = true;
                if (obj.Remarks != null)
                    txtRemarks.Text = obj.Remarks;

                hdnNomineeId.Text = obj.NomineeId.ToString();
                btnSave.Text = Resources.Messages.Update;
            }

        }

    }
}