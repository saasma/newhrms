﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using System.IO;
using Utils;
using Web.Helper;
using Utils.Calendar;
using BLL.BO;

namespace Web.Employee.HR
{
    public partial class EFamilyDetailsPopup : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();
            }
        }

        public void Initialise()
        {
            //if(Request.Browser.Type.ToUpper().Contains("IE"))
            //{
            //    //for datepicker overriding in IE
            //    DateTime CurrentDate = BLL.BaseBiz.GetCurrentDateAndTime();
            //    CustomDate nepDate = new CustomDate(CurrentDate.Day, CurrentDate.Month, CurrentDate.Year, false);
            //    nepDate = CustomDate.ConvertEngToNep(nepDate);
            //    DateEngToNep _DateEngToNep = GetEngToNepDate(CurrentDate.ToString());
            //    int month = nepDate.Month;
            //    hdnCurrentMonth.Text = month.ToString();
            //    //------------------------------------
            //}
            
            valRegPhone.ValidationExpression = Config.PhoneRegExp;
            cmbRelation.Store[0].DataSource = CommonManager.GetRelationList();
            cmbRelation.Store[0].DataBind();

            cmbOccupation.Store[0].DataSource = ListManager.GetAllOccuations();
            cmbOccupation.Store[0].DataBind();
            LoadLevels();

            ClearLevelFields();

            if (!string.IsNullOrEmpty(Request.QueryString["FamilyId"]))
                LoadEditData();

        }

        private void LoadLevels()
        {
            int employeeId = 0;           

            if (Request.QueryString["id"] == null)
            {
                employeeId = SessionManager.CurrentLoggedInEmployeeId;
            }
            else
            {
                employeeId = int.Parse(Request.QueryString["id"]);
            }

        }


        public void txtDOB_Blur(object sender, DirectEventArgs e)
        {
            if (!string.IsNullOrEmpty(txtDOB.Text.Trim()))
            {
                int years, months, days, hours, minutes, seconds, milliseconds;
                NewHelper.GetElapsedTime(GetEngDate(txtDOB.Text.Trim()),
                    BLL.BaseBiz.GetCurrentDateAndTime(), out years, out months, out days, out hours, out minutes, out seconds, out milliseconds);

                txtAgeOnSPDate.Text = string.Format(" {0} years {1} months {2} days",
                    years, months, days);
            }
        }

        public void ClearLevelFields()
        {
            hiddenValue.Text = "";
            txtName.Text = "";
            cmbRelation.ClearValue();
            txtRemarks.Text = "";
            txtDOB.Text = "";
            chkHasDependent.Checked = false;
            txtSPDate.Text = "";
            txtAgeOnSPDate.Text = "";
        }

        private void LoadEditData()
        {
            hiddenValue.Text = Request.QueryString["FamilyId"];
            int familyId = int.Parse(hiddenValue.Text);

            HFamily entity = NewPayrollManager.GetFamilyMemberById(familyId);

            hiddenValue.Text = familyId.ToString();

            txtName.Text = entity.Name;
            if (entity.RelationID != null)
                cmbRelation.SelectedItem.Value = entity.RelationID.Value.ToString();
            else
                cmbRelation.ClearValue();
            txtRemarks.Text = entity.Remarks;
            txtDOB.Text = entity.DateOfBirth;
            if (entity.HasDependent != null)
                chkHasDependent.Checked = entity.HasDependent.Value;
            else
                chkHasDependent.Checked = false;
            //txtSPDate.Text = entity.SpecifiedDate;
            txtAgeOnSPDate.Text = entity.AgeOnSpecifiedSPDate;

            cmbOccupation.Store[0].ClearFilter();
            if (entity.OccupationId != null)
                cmbOccupation.SelectedItem.Value = entity.OccupationId.Value.ToString();
            else
                cmbOccupation.ClearValue();

            if (entity.ContactNumber != null)
                txtContactNumber.Text = entity.ContactNumber;

        }


        protected void btnLevelSaveUpdate_Click(object sender, DirectEventArgs e)
        {
            Page.Validate("InsertUpdateLevel");
            if (Page.IsValid)
            {
                HFamily hFamily = new HFamily();
                int EmpID;
                int FamilyID;
                bool isInsert = true;
                hFamily.Name = txtName.Text;
                hFamily.Relation = cmbRelation.SelectedItem.Text;
                hFamily.RelationID = int.Parse(cmbRelation.SelectedItem.Value);
                hFamily.Remarks = txtRemarks.Text;
                hFamily.DateOfBirth = txtDOB.Text;
                if (!string.IsNullOrEmpty(hFamily.DateOfBirth))
                    hFamily.DateOfBirthEng = GetEngDate(hFamily.DateOfBirth);
                hFamily.HasDependent = false;
                if (chkHasDependent.Checked)
                    hFamily.HasDependent = true;
                //hFamily.SpecifiedDate = txtSPDate.Text;
                hFamily.AgeOnSpecifiedSPDate = txtAgeOnSPDate.Text;

                if (cmbOccupation.SelectedItem != null && cmbOccupation.SelectedItem.Value != null)
                    hFamily.OccupationId = int.Parse(cmbOccupation.SelectedItem.Value);

                if (!string.IsNullOrEmpty(txtContactNumber.Text.Trim()))
                    hFamily.ContactNumber = txtContactNumber.Text.Trim();

                if (Request.QueryString["id"] == null)
                    EmpID = SessionManager.CurrentLoggedInEmployeeId;
                else
                    EmpID = int.Parse(Request.QueryString["id"]);


                hFamily.EmployeeId = EmpID;

                if (!string.IsNullOrEmpty(hiddenValue.Text))
                {
                    hFamily.FamilyId = int.Parse(hiddenValue.Text.Trim());
                    isInsert = false;
                }


                Status status = NewHRManager.InsertUpdateFamily(hFamily, isInsert);
                if (status.IsSuccess)
                {
                    if (isInsert)
                        NewMessage.ShowNormalMessage("Record Saved Successfully.", "closePopup()");
                    else
                        NewMessage.ShowNormalMessage("Record Updated Successfully.", "closePopup()");
                }
                else
                {
                    NewMessage.ShowWarningMessage(status.ErrorMessage);
                }

            }
        }

        public void HideButtons()
        {
            btnLevelSaveUpdate.Hidden = true;
            LinkButton1.Hidden = true;
        }

    }
}