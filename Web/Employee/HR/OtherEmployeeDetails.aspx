﻿<%@ Page Title="Employee Details" Language="C#" MasterPageFile="~/Master/EmployeeExt.Master"
    AutoEventWireup="true" CodeBehind="OtherEmployeeDetails.aspx.cs" Inherits="Web.Employee.HR.OtherEmployeeDetails" %>

<%@ Register Src="~/Employee/UserControls/EmpDetails.ascx" TagName="EmpDetails" TagPrefix="uc" %>
<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="mainContent" runat="server">
    <div class="innerLR">
        <div class="separator bottom">
        </div>
        <uc:EmpDetails id="ucEmpDetails" runat="server" IsOtherEmployee="true" />
    </div>
</asp:Content>
