﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL;
using BLL.Manager;
using BLL;
using BLL.BO;
using System.IO;
using BLL.Base;
using Ext.Net;
using Utils.Helper;

namespace Web.Employee.HR
{
    public partial class EHobbyDetlsPopup : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();
            }
        }

        private void Initialise()
        {
            ClearFields();
            BindHobbyCombo();

            if (!string.IsNullOrEmpty(Request.QueryString["HobbyId"]))
            {
                hdnHobbyId.Text = Request.QueryString["HobbyId"];
                EditEHobby(int.Parse(hdnHobbyId.Text));
            }
        }

        private void BindHobbyCombo()
        {
            cmbHobby.Store[0].DataSource = ListManager.GetHobbyTpyeList();
            cmbHobby.Store[0].DataBind();
        }

        private void ClearFields()
        {
            cmbHobby.Text = "";
            txtInvolvedFrom.Text = "";
            txtDescription.Text = "";
            btnSave.Text = Resources.Messages.Save;
        }

        protected void btnSave_Click(object sender, DirectEventArgs e)
        {
            Page.Validate("SaveUpdateHobby");
            if (Page.IsValid)
            {
                EHobby obj = new EHobby();

                if (!string.IsNullOrEmpty(hdnHobbyId.Text))
                    obj.HobbyId = int.Parse(hdnHobbyId.Text);

                obj.EmployeeId = GetEmployeeID();
                obj.HobbyTypeId = int.Parse(cmbHobby.SelectedItem.Value);

                if (!string.IsNullOrEmpty(txtInvolvedFrom.Text.Trim()))
                {
                    obj.InvolvedFrom = txtInvolvedFrom.Text.Trim();
                    obj.InvolvedFromEng = BLL.BaseBiz.GetEngDate(obj.InvolvedFrom, IsEnglish);
                }

                if (!string.IsNullOrEmpty(txtDescription.Text.Trim()))
                    obj.Description = txtDescription.Text.Trim();

                Status status = NewHRManager.SaveUpdateEHobby(obj);
                if (status.IsSuccess)
                {
                    if (string.IsNullOrEmpty(hdnHobbyId.Text))
                        NewMessage.ShowNormalMessage("Record saved successfully.", "closePopup()");
                    else
                        NewMessage.ShowNormalMessage("Record updated successfully.", "closePopup()");

                    btnSave.Hide();
                }
                else
                    NewMessage.ShowWarningMessage(status.ErrorMessage);
            }

        }

        protected int GetEmployeeID()
        {
            int EmpID = 0;
            if (!string.IsNullOrEmpty(Request.QueryString["ID"]))
            {
                EmpID = int.Parse(Request.QueryString["ID"]);
            }
            else
            {
                EmpID = SessionManager.CurrentLoggedInEmployeeId;
            }
            return EmpID;
        }

        private void EditEHobby(int hobbyId)
        {
            EHobby obj = NewHRManager.GetEHobbyById(hobbyId);
            if (obj != null)
            {
                cmbHobby.SelectedItem.Value = obj.HobbyTypeId.Value.ToString();

                if (!string.IsNullOrEmpty(obj.InvolvedFrom))
                    txtInvolvedFrom.Text = obj.InvolvedFrom;

                if (obj.Description != null)
                    txtDescription.Text = obj.Description;

                hdnHobbyId.Text = obj.HobbyId.ToString();
                btnSave.Text = Resources.Messages.Update;
            }

        }




    }
}