﻿<%@ Page Title="Employee Search" Language="C#" MasterPageFile="~/Master/EmployeeExt.Master"
    AutoEventWireup="true" CodeBehind="EmpSearchEmployee.aspx.cs" Inherits="Web.Employee.HR.EmpSearchEmployee" %>


<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/PagingCtl.ascx" TagName="PagingCtl" TagPrefix="uc1" %> 

<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <style>
        .menu
        {
            height: 50px;
        }
        
        .RowTable
        {
            margin-left: 0px;
            clear: both;
        }
        
        .menu ul
        {
            list-style: none;
            margin-left: 5px;
        }
        
        .menu li
        {
            display: inline;
            float: left;
            width: 200px;
            margin-left: -5px;
        }
        
        .AutoExtender
        {
            z-index: 999999 !important;
        }
        .items
        {
            padding-left: 20px;
        }
        .items span
        {
            display: block;
            font-family: 'Open Sans' ,sans-serif;
            padding-bottom: 2px;
        }
        .items img
        {
            float: left;
            width: 18px;
            height: 18px;
            margin-right: 10px;
        }
        
        .tdClass
        {
            padding-bottom: 10px;
        }
        </style>
    <script type="text/javascript">


        var CommandHandler = function (command, record) {
            
            if(command == 'View')
            { 
                 window.location = 'EmployeeSearchDetail.aspx?Id=' + record.data.EmployeeId;
            }           
        };

        function searchList() {
            <%=PagingToolbar1.ClientID %>.doRefresh();
        }

        function cmbSelected(empId)
        {
            <%= hdnEmpId.ClientID %>.setValue(empId);
            searchList();
            return;
        }

    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="mainContent" runat="server">
   
    
    <asp:ScriptManager ID="scriptM" runat="server" />

    <ext:Hidden ID="hdnEmpId" runat="server" />

    <div class="innerLR1">
        <div class="separator bottom">
        </div>
        <div class="widget" style="float: left; width: 100%;">
            <div class="widget-head">
                <h4 class="heading">
                    Search Employee</h4>
            </div>
            <div class="widget-body" style="padding-top: 10px;">
                         
                 <table>
                 <tr>
                    <td style="width:270px;">Type Name Search</td>
                    <td></td>
                 </tr>
                 <tr>
                    <td>                    
                         <ext:Store runat="server" ID="storeSearch" AutoLoad="false">
                            <Proxy>
                                <ext:AjaxProxy Url="../HR/EmpSearch.ashx">
                                    <ActionMethods Read="GET" />
                                    <Reader>
                                        <ext:JsonReader Root="plants" TotalProperty="total" />
                                    </Reader>
                                </ext:AjaxProxy>
                            </Proxy>
                            <Model>
                                <ext:Model ID="Model2" IDProperty="Name" Name="ItemLineModel" runat="server">
                                    <Fields>
                                        <ext:ModelField Name="Name" Type="String" />
                                        <ext:ModelField Name="EmployeeId" Type="String" />
                                    </Fields>
                                </ext:Model>
                            </Model>
                        </ext:Store>
                        <ext:ComboBox LabelSeparator="" ID="cmbSearch"  LabelWidth="70" 
                          runat="server" DisplayField="Name" ValueField="EmployeeId" StoreID="storeSearch"
                            TypeAhead="false" Width="250" PageSize="9999" HideBaseTrigger="true" MinChars="2"
                            TriggerAction="All" >
                            <ListConfig LoadingText="Searching..." MinWidth="200" StyleSpec="border-top:1px solid #98c0f4;">
                                <ItemTpl ID="ItemTpl1" runat="server">
                                    <Html>
                                        <tpl>
                                            <div class="search-item">
                                                            <span>{Name}</span>  
                                             </div>
					                </tpl>
                                    </Html>
                                </ItemTpl>
                            </ListConfig>
                            <Triggers>
                                <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                            </Triggers>
                            <Listeners>
                                <Select Handler="this.getTrigger(0).show(); searchList();" />
                                <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                <TriggerClick Handler="if (index == 0) { 
                                           this.clearValue(); 
                                           this.getTrigger(0).hide();
                                       }" />
                            </Listeners>
                            
                        </ext:ComboBox>
                    </td>

                    <td>
                        <%--<ext:LinkButton runat="server" StyleSpec="padding:0px;margin-top:20px;" ID="btnLoad"
                            Cls="btnFlat" BaseCls="btnFlat" Text="<i></i>Load" >                           
                           <DirectEvents>
                            <Click OnEvent="btnLoad_Click">
                                <EventMask ShowMask="true" />
                            </Click>
                           </DirectEvents>
                        </ext:LinkButton>    --%>  
                    </td>

                   
                   
                 </tr>
                 </table>

                <br />

        <div class="clear" style="width: 1140px;margin-top:10px;">
         

         <table>
        <tr>
            <td>
                <ext:GridPanel ID="gridEmployeeSearch" runat="server"  Cls="itemgrid" Width="920" Scroll="None">
                    <Store>
                        <ext:Store ID="Store3" runat="server" AutoLoad="true" PageSize="50">
                            <Proxy>
                                <ext:AjaxProxy Json="true" Url="../HR/OtherEmployeeSearch.ashx">
                                    <ActionMethods Read="GET" />
                                    <Reader>
                                        <ext:JsonReader Root="data" TotalProperty="total" />
                                    </Reader>
                                </ext:AjaxProxy>
                            </Proxy>
                            <AutoLoadParams>
                                <ext:Parameter Name="start" Value="0" Mode="Raw" />                               
                            </AutoLoadParams>
                            <Parameters>                              
                                <ext:StoreParameter Name="EmpName" Value="#{cmbSearch}.getRawValue()" Mode="Raw" ApplyMode="Always" /> 
                                <ext:StoreParameter Name="EmpId" Value="#{cmbSearch}.getValue()" Mode="Raw" ApplyMode="Always" />                                                         
                            </Parameters>
                            <Model>
                                <ext:Model ID="Model1" runat="server" IDProperty="EmployeeId">
                                    <Fields>
                                        <ext:ModelField Name="EmployeeId" Type="String" />
                                        <ext:ModelField Name="Name" Type="string" />
                                        <ext:ModelField Name="Position" Type="string" />
                                        <ext:ModelField Name="Branch" Type="string" />
                                        <ext:ModelField Name="Department" Type="string" />                                      
                                    </Fields>
                                </ext:Model>
                            </Model>
                        </ext:Store>
                    </Store>
                    <ColumnModel ID="ColumnModel2" runat="server">
		                <Columns>                 

                          <ext:Column ID="colEId" runat="server" Text="EIN" Width="80" DataIndex="EmployeeId" Align="Center" Sortable="false" MenuDisabled="true">
                            </ext:Column>
                        
                            <ext:Column ID="Column1" runat="server" Text="Employee Name" Width="250" DataIndex="Name" Sortable="false" MenuDisabled="true" Align="Left">
                            </ext:Column>

                            <ext:Column ID="Column2" runat="server" Text="Position" Width="150" DataIndex="Position" Align="Left" Sortable="false" MenuDisabled="true">
                            </ext:Column>

                            <ext:Column ID="Column3" runat="server" Text="Branch" Width="150" DataIndex="Branch" Align="Left" Sortable="false" MenuDisabled="true">
                            </ext:Column>

                            <ext:Column ID="Column4" runat="server" Text="Department" Width="150" DataIndex="Department" Align="Left" Sortable="false" MenuDisabled="true">
                            </ext:Column>

                            <ext:CommandColumn ID="CommandColumn1" runat="server" Width="100" Sortable="false" MenuDisabled="true" Text="Actions" Align="Center">
                              <Commands>
                                    <ext:CommandSeparator />
                                    <ext:GridCommand Text="<i></i>View Details"   CommandName="View" ToolTip-Text="View" />                           
                                   
                                </Commands>                                

                                <Listeners>
                                    <Command Handler="CommandHandler(command,record);" />                                    
                                </Listeners>                          

                            </ext:CommandColumn>
                        </Columns>
                    </ColumnModel>
         
                    <SelectionModel>
                        <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" Mode="Single" />
                    </SelectionModel>
                   <BottomBar>
                        <ext:PagingToolbar ID="PagingToolbar1" runat="server" PageSize="50" DisplayInfo="true"
                            DisplayMsg="Displaying Time Sheet {0} - {1} of {2}" EmptyMsg="No Records to display">
                            <Items>
                                <ext:ComboBox Hidden="true" runat="server" SelectedIndex="0" Width="80" ID="cmbPageSize">
                                    <Items>
                                        <ext:ListItem Text="20" Value="20" />
                                        <ext:ListItem Text="30" Value="30" />
                                        <ext:ListItem Text="50" Value="50" />
                                    </Items>
                                    <Listeners>
                                        <Select Handler="#{PagingToolbar1}.pageSize = parseInt( '50'); searchList();" />
                                    </Listeners>
                                </ext:ComboBox>
                            </Items>
                        </ext:PagingToolbar>
                    </BottomBar>       
                </ext:GridPanel>
            </td>
        </tr>
        </table>


        </div>
             
            </div>
        </div>
    </div>
</asp:Content>
