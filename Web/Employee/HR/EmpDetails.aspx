﻿<%@ Page Title="Employee Details"  Language="C#" EnableViewState="false" MasterPageFile="~/Master/EmployeeExt.Master"
    AutoEventWireup="true" CodeBehind="EmpDetails.aspx.cs" Inherits="Web.Employee.HR.EmpDetails" %>

<%@ Register Src="~/Employee/UserControls/EmpDetails.ascx" TagName="EmpDetails" TagPrefix="uc" %>
<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="mainContent" runat="server">
    <div class="innerLR">
        <div class="separator bottom">
        </div>
        <uc:EmpDetails id="ucEmpDetails" runat="server" />
    </div>
</asp:Content>
