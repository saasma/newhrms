﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL;
using BLL.Manager;
using BLL;
using BLL.BO;
using System.IO;
using BLL.Base;
using Ext.Net;
namespace Web.Employee.HR
{
    public partial class EExtraActivityDetlsPopup : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialize();
            }
        }

        private void Initialize()
        {
            ClearFields();

            if (!string.IsNullOrEmpty(Request.QueryString["ActivityId"]))
            {
                hdnCurricularId.Text = Request.QueryString["ActivityId"];
                EditExtraActivity(int.Parse(hdnCurricularId.Text));
            }
        }

        private void ClearFields()
        {
            txtActivityName.Text = "";
            txtProficiency.Text = "";
            txtAward.Text = "";
            txtYear.Text = "";
            txtRemarks.Text = "";
        }

        protected int GetEmployeeID()
        {
            int EmpID = 0;
            if (!string.IsNullOrEmpty(Request.QueryString["ID"]))
            {
                EmpID = int.Parse(Request.QueryString["ID"]);
            }
            else
            {
                EmpID = SessionManager.CurrentLoggedInEmployeeId;
            }
            return EmpID;
        }

        protected void btnSave_Click(object sender, DirectEventArgs e)
        {
            Page.Validate("SaveUpdateActivity");
            if (Page.IsValid)
            {
                HExtraActivity obj = new HExtraActivity();

                if (!string.IsNullOrEmpty(hdnCurricularId.Text))
                    obj.CurricularId = int.Parse(hdnCurricularId.Text);

                obj.EmployeeId = GetEmployeeID();

                obj.AcitivityName = txtActivityName.Text;

                if (!string.IsNullOrEmpty(txtProficiency.Text.Trim()))
                    obj.Proficiency = txtProficiency.Text.Trim();

                if (!string.IsNullOrEmpty(txtAward.Text.Trim()))
                    obj.Award = txtAward.Text.Trim();

                if (!string.IsNullOrEmpty(txtYear.Text.Trim()))
                    obj.Year = txtYear.Text.Trim();

                if (!string.IsNullOrEmpty(txtRemarks.Text.Trim()))
                    obj.Remarks = txtYear.Text.Trim();

                Status status = NewHRManager.SaveUpdateExtraActivity(obj);
                if (status.IsSuccess)
                {

                    if (!string.IsNullOrEmpty(hdnCurricularId.Text))
                        NewMessage.ShowNormalMessage("Record updated successfully.", "closePopup()");
                    else
                        NewMessage.ShowNormalMessage("Record saved successfully.", "closePopup()");

                    btnSave.Hide();
                }
                else
                    NewMessage.ShowWarningMessage(status.ErrorMessage);
            }

        }

        private void EditExtraActivity(int curricularId)
        {
            hdnCurricularId.Text = curricularId.ToString();
            HExtraActivity obj = NewHRManager.GetExtraActivityById(curricularId);
            if (obj != null)
            {
                txtActivityName.Text = obj.AcitivityName;
                if (obj.Proficiency != null)
                    txtProficiency.Text = obj.Proficiency;

                if (obj.Award != null)
                    txtAward.Text = obj.Award;

                if (obj.Year != null)
                    txtYear.Text = obj.Year;

                if (obj.Remarks != null)
                    txtRemarks.Text = obj.Remarks;

                btnSave.Text = Resources.Messages.Update;
            }
        }



    }
}