﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL.Entity;
using BLL;

namespace Web.Employee.HR
{
    public partial class OtherEmployeeListing : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();
            }
        }

        private void Initialise()
        {
            int employeeId = SessionManager.CurrentLoggedInEmployeeId;
            gridOtherEmpList.GetStore().DataSource = NewHRManager.GetOtherEmployeeListByEmpId(employeeId);
            gridOtherEmpList.GetStore().DataBind();


            // for ncc hide other employee details
            if (CommonManager.CompanySetting.WhichCompany == WhichCompany.NCC)
                CommandColumn1.Visible = false;
        }
    }
}