﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BLL;
using BLL.Manager;
using System.Collections.Generic;
using DAL;
using Ext.Net;

namespace Web.Employee.HR
{
    /// <summary>
    /// Summary description for EmpSearch
    /// </summary>
    public class EmpSearch : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {

            if (HttpContext.Current.User.Identity.IsAuthenticated == false )
            {
                context.Response.Write("");
                context.Response.End();
                return;
            }

            context.Response.ContentType = "text/plain";
            string query = context.Request["query"];

            if (query == null)
                return;

            query = query.Trim();

            bool isEIN = false;
            int eid = 0;
            if (int.TryParse(query, out eid))
            {
                isEIN = true;
            }

            EmployeeManager mgr = new EmployeeManager();

            List<string> suggestions = new List<string>();

            string customRoleDepartmentList = null;
            if (SessionManager.IsCustomRole)
                customRoleDepartmentList = SessionManager.CustomRoleDeparmentIDList;

            List<GetPrefixedEmpNamesResult> list = mgr.GetPrefixedEmplyeeNamesWithIDForHr(isEIN, eid, query.ToLower(), customRoleDepartmentList, false);

            //foreach (GetPrefixedEmpNamesResult dr in list)
            //{
            //    suggestions.Add(AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem(dr.Name, dr.EmployeeId.ToString()));

            //}
            //return suggestions.ToArray();

            context.Response.Write(string.Format("{{total:{1},'plants':{0}}}", JSON.Serialize(list), list.Count));

        }


        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}