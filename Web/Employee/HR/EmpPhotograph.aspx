﻿<%@ Page Title="Identification" Language="C#" MasterPageFile="~/Master/EmployeeExt.Master"
    AutoEventWireup="true" CodeBehind="EmpPhotograph.aspx.cs" Inherits="Web.Employee.HR.EmpPhotograph" %>

<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
    <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.Jcrop.js") %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.Jcrop.min.js") %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery-ui-1.10.3.min.js") %>"></script>
    <link id="Link1" rel="stylesheet" type="text/css" href="<%= ResolveUrl("~/css/jquery.Jcrop.css") %>" />
    <link id="Link2" rel="stylesheet" type="text/css" href="<%= ResolveUrl("~/css/jquery.Jcrop.min.css")  %>" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .ui-dialog-titlebar-close
        {
            visibility: hidden;
        }
    </style>
    <script type="text/javascript">
        function myFunction() {
            //some code here
            $("#popupdiv").dialog({
                modal: true,
                draggable: false,
                resizable: false,
                width: 'auto',
                height: 'auto',
                position: ['center', 'center'],
                show: 'blind',
                hide: 'blind',
                dialogClass: 'ui-dialog-osx',
                buttons: {
                    "Close": function () {
                        $(this).dialog("close");
                    }
                }
            });
        }
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="mainContent" runat="server">
    <h3>
        Photograph
    </h3>
    <br />
    <table>
        <tr>
            <td style="padding-left: 20px; padding-top: 10px;">
                <div style="border: 1px solid grey; width: 180px; height: 160px;">
                    <ext:Image ID="ImgEmployee" runat="Server" Width="180" Height="160">
                    </ext:Image>
                </div>
            </td>
        </tr>
        <tr>
            <td style="padding-left: 20px; padding-top: 10px;">
                <ext:FileUploadField ID="fup" runat="server" ButtonText="Browse" Icon="Add" Width="300"
                    Visible="true" ButtonOffset="1">
                </ext:FileUploadField>
                <asp:RequiredFieldValidator ValidationGroup="File" ControlToValidate="fup" Display="None"
                    Text="Please select a file." ID="RequiredFieldValidator1" runat="server" ErrorMessage="Please select a file."></asp:RequiredFieldValidator>
            </td>
            <td style="padding-left: 20px; padding-top: 10px;">
                <ext:Button runat="server" Text="Upload" ID="btnUpload" Cls="bluebutton left">
                    <DirectEvents>
                        <Click OnEvent="btnUpload_Click">
                            <EventMask ShowMask="true" />
                        </Click>
                    </DirectEvents>
                    <Listeners>
                        <Click Handler="valGroup= 'File'; return CheckValidation();">
                        </Click>
                    </Listeners>
                </ext:Button>
            </td>
        </tr>
    </table>
    <br />
    <input type="button" id="btnclick" value="Crop Photograph" style="background-color: #428BCA;
        color: White; visibility: hidden;" />
    <div id="popupdiv" title="" style="display: none; background-color: Silver;">
        <asp:Image ID="Image1" runat="server" />
        <br />
        <ext:Button runat="server" Text="Crop" ID="btnCrop" Cls="bluebutton left" Hidden="true">
            <DirectEvents>
                <Click OnEvent="btnCropImage_Click">
                    <EventMask ShowMask="true" />
                </Click>
            </DirectEvents>
            <Listeners>
            </Listeners>
        </ext:Button>
    </div>
    <input type="hidden" runat="server" id="_xField" />
    <input type="hidden" runat="server" id="_yField" />
    <input type="hidden" runat="server" id="_widthField" />
    <input type="hidden" runat="server" id="_heightField" />
    <script type="text/javascript">
        $(function () {
            $('#btnclick').click(function () {
                $("#popupdiv").dialog({
                    modal: true,
                    draggable: false,
                    resizable: false,
                    width: 'auto',
                    height: 'auto',
                    position: ['center', 'center'],
                    show: 'blind',
                    hide: 'blind',
                    dialogClass: 'ui-dialog-osx',
                    buttons: {
                        "Close": function () {
                            $(this).dialog("close");
                        }
                    }
                });

            });


        })
    </script>
    <script type="text/javascript">

        function hideDiv() {
            document.getElementById('popupdiv').style.display = "none";
        }

        var editorID = '<%= Image1.ClientID %>';

        jQuery(function () {
            jQuery('#' + editorID).Jcrop({
                onChange: showCoords,
                onSelect: showCoords,
                aspectRatio: 1
            });
        });

        function showCoords(c) {

            var xField = document.getElementById('<%= _xField.ClientID %>');
            var yField = document.getElementById('<%= _yField.ClientID %>');
            var widthField = document.getElementById('<%= _widthField.ClientID %>');
            var heightField = document.getElementById('<%= _heightField.ClientID %>');

            xField.value = c.x;
            yField.value = c.y;
            widthField.value = c.w;
            heightField.value = c.h;
        }



        function showPopUp() {
            $("#popupdiv").dialog({
                title: "jQuery Popup from Server Side",

                modal: true
            });
        }
  
    
    </script>
</asp:Content>
