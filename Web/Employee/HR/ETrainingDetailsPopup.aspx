﻿<%@ Page Title="Training Details" Language="C#" MasterPageFile="~/Master/ForEmployeePopupPage.Master"
    AutoEventWireup="true" CodeBehind="ETrainingDetailsPopup.aspx.cs" Inherits="Web.Employee.HR.ETrainingDetailsPopup" %>

<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">

        var skipLoadingCheck = true;

        function closePopup() {
            window.close();
            window.opener.reloadTrainingGrid();
        }

    </script>
    <style type="text/css">
        input[type=text], textarea, select
        {
            border: 1px solid #A2B4C6;
        }
        textarea
        {
            height: 50px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="mainContent" runat="server">
    <ext:ResourceManager ID="ResourceManager1" Namespace="" ScriptMode="Release" runat="server" />
    <ext:Hidden runat="server" ID="hdnTrainingID">
    </ext:Hidden>
    <div class="popupHeader">
        <h4 style="margin-top: 0px; color: White; margin-left: 20px;">
            Training Details</h4>
    </div>
    <div class=" marginal" style='margin-top: 0px'>
        <table class="fieldTable" style="margin-left: 20px;">
            <tr>
                <td>
                    <ext:ComboBox ID="cmbTrainingType" runat="server" Width="180px" ValueField="TrainingTypeId"
                        DisplayField="TrainingTypeName" FieldLabel="Training Type *" LabelAlign="top"
                        LabelSeparator="" ForceSelection="true" QueryMode="Local">
                        <%--<Items>
                            <ext:ListItem Text="Organization Sponcered" Value="1" />
                            <ext:ListItem Text="Third Paty Sponcered" Value="2" />
                            <ext:ListItem Text="Self Funded" Value="3" />
                        </Items>--%>
                        <Store>
                            <ext:Store ID="Store3" runat="server">
                                <Model>
                                    <ext:Model ID="Model3" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="TrainingTypeId" Type="String" />
                                            <ext:ModelField Name="TrainingTypeName" Type="String" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                    </ext:ComboBox>
                    <asp:RequiredFieldValidator Display="None" ID="valcmbTrainingType" runat="server"
                        ValidationGroup="TrainingSaveUpdate" ControlToValidate="cmbTrainingType" ErrorMessage="Training Type is required." />
                </td>
            </tr>
            <tr>
                <td colspan='2'>
                    <ext:TextField ID="txtTrainingName" Width="340px" runat="server" FieldLabel="Training Name *"
                        LabelAlign="top" LabelSeparator="" />
                    <asp:RequiredFieldValidator Display="None" ID="valtxtTrainingName" runat="server"
                        ValidationGroup="TrainingSaveUpdate" ControlToValidate="txtTrainingName" ErrorMessage="Training Name is required." />
                </td>
                <td>
                </td>
                <td>
                    <ext:FileUploadField ID="FileTrainingDocumentUpload" runat="server" Width="200" Icon="Attach"
                        FieldLabel="&nbsp;" LabelAlign="Top" LabelSeparator="" />
                    <%--<span style="color: #ACACAC; padding-top: 20px;">*(only doc,pdf,txt,docx,rar,xlsx,xls)</span>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ValidationExpression="^.*\.(doc|DOC|pdf|PDF|txt|TXT|docx|DOCX|rar|RAR|xlsx|xls)$"
                        runat="server" ValidationGroup="TrainingSaveUpdate" Display="None" ControlToValidate="FileTrainingDocumentUpload"
                        ErrorMessage="Invalid file format."></asp:RegularExpressionValidator>--%><br />
                    <ext:LinkButton runat="server" ID="lnkFileName" OnDirectClick="lnkFileName_Click"
                        Text="" Hidden="true">
                        <DirectEvents>
                            <Click OnEvent="lnkFileName_Click">
                            </Click>
                        </DirectEvents>
                    </ext:LinkButton>
                    <ext:Label ID="lblUploadedFile" runat="server" Hidden="true">
                    </ext:Label>
                    <ext:LinkButton Icon="Delete" runat="server" ID="lnkDeleteFile" OnDirectClick="lnkDeleteFile_Click"
                        Text="Delete file" Hidden="true">
                        <DirectEvents>
                            <Click OnEvent="lnkDeleteFile_Click">
                                <EventMask ShowMask="true" />
                                <Confirmation ConfirmRequest="true" Message="Are you sure, you want to delete the file?" />
                            </Click>
                        </DirectEvents>
                    </ext:LinkButton>
                </td>
            </tr>
            <tr>
                <td colspan='2'>
                    <ext:TextField ID="txtResourcePerson" Width="340px" runat="server" FieldLabel="Resource Person"
                        LabelAlign="top" LabelSeparator="" />
                </td>
            </tr>
            <tr>
                <td>
                    <ext:TextField Width="180px" ID="txtInstitute" runat="server" FieldLabel="Institute"
                        LabelAlign="top" LabelSeparator="" />
                    <%-- <asp:RequiredFieldValidator Display="None" ID="valtxtInstitute" runat="server" ValidationGroup="TrainingSaveUpdate"
                        ControlToValidate="txtInstitute" ErrorMessage="Institute is required." />--%>
                </td>
                <td>
                    <ext:ComboBox Width="150px" ID="cmbCountry" runat="server" ValueField="CountryName"
                        DisplayField="CountryName" FieldLabel="Country" LabelAlign="top" LabelSeparator=""
                        ForceSelection="true" QueryMode="Local">
                        <Store>
                            <ext:Store ID="Store2" runat="server">
                                <Model>
                                    <ext:Model ID="Model4" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="CountryName" />
                                            <ext:ModelField Name="CountryName" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                    </ext:ComboBox>
                    <%--  <asp:RequiredFieldValidator Display="None" ID="valcmbCountry" runat="server" ValidationGroup="TrainingSaveUpdate"
                        ControlToValidate="cmbCountry" ErrorMessage="Country is required." />--%>
                </td>
            </tr>
            <tr>
                <td colspan="4">
                    <table class="fieldTable firsttdskip">
                        <tr>
                            <td>
                                <ext:TextField ID="txtDuration" runat="server" FieldLabel="Duration" LabelAlign="top"
                                    Width="85" LabelSeparator="" MaskRe="[0-9]|\.|%" />
                                <%-- <asp:RequiredFieldValidator Display="None" ID="valtxtDuration" runat="server" ValidationGroup="TrainingSaveUpdate"
                                    ControlToValidate="txtDuration" ErrorMessage="Duration  is required." />--%>
                            </td>
                            <td>
                                <ext:ComboBox ID="cmbDurationType" runat="server" ValueField="Value" DisplayField="Text"
                                    Width="80" FieldLabel="&nbsp;" LabelAlign="top" LabelSeparator="" ForceSelection="true"
                                    QueryMode="Local">
                                    <Items>
                                        <ext:ListItem Text="Year" Value="1" />
                                        <ext:ListItem Text="Month" Value="2" />
                                        <ext:ListItem Text="Week" Value="3" />
                                        <ext:ListItem Text="Day" Value="4" />
                                    </Items>
                                    <%-- <Store>
                                            <ext:Store ID="Store1" runat="server">
                                                <Model>
                                                    <ext:Model ID="Model2" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="Value" />
                                                            <ext:ModelField Name="Text" />
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>--%>
                                </ext:ComboBox>
                                <%--  <asp:RequiredFieldValidator Display="None" ID="valcmbDurationType" runat="server"
                                    ValidationGroup="TrainingSaveUpdate" ControlToValidate="cmbDurationType" ErrorMessage="Duration Type is required." />--%>
                            </td>
                            <td>
                                <pr:CalendarExtControl FieldLabel="Start Date" Width="150px" ID="calStartDate" runat="server"
                                    LabelSeparator="" LabelAlign="Top" />
                                <%-- <asp:RequiredFieldValidator Display="None" ID="valcalStartDate" runat="server" ValidationGroup="TrainingSaveUpdate"
                                    ControlToValidate="calStartDate" ErrorMessage="Start Date is required." />--%>
                            </td>
                            <td>
                                <pr:CalendarExtControl FieldLabel="End Date" Width="150px" ID="calEndDate" runat="server"
                                    LabelSeparator="" LabelAlign="Top" />
                                <%--<asp:RequiredFieldValidator Display="None" ID="valCalEndDate" runat="server" ValidationGroup="TrainingSaveUpdate"
                                    ControlToValidate="calEndDate" ErrorMessage="End Date is required." />--%>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <ext:TextField ID="txtCostOfTraining" Width="180px" runat="server" FieldLabel="Cost Of Training"
                        LabelAlign="top" LabelSeparator="" MaskRe="[0-9]|\.|%" />
                </td>
                <td>
                    <ext:TextField ID="txtTotalCost" Width="180px" runat="server" FieldLabel="Total Cost"
                        LabelAlign="top" LabelSeparator="" MaskRe="[0-9]|\.|%" />
                </td>
            </tr>
            <tr>
                <td colspan="4">
                    <table class="fieldTable" style="position:relative">
                        <tr>
                            <td style="padding-left: 0px!important">
                                <ext:ComboBox ID="cmbBond" runat="server" ValueField="Value" DisplayField="Text" 
                                    FieldLabel="Bond" Width="85" LabelAlign="top" LabelSeparator="" ForceSelection="true"
                                    QueryMode="Local">
                                    <Items>
                                        <ext:ListItem Text="Yes" Value="True" />
                                        <ext:ListItem Text="No" Value="False" />
                                    </Items>
                                    <%-- <Store>
                                            <ext:Store ID="Store4" runat="server">
                                                <Model>
                                                    <ext:Model ID="Model5" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="Value" />
                                                            <ext:ModelField Name="Text" />
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>--%>
                                    <DirectEvents>
                                        <Select OnEvent="cmbBond_Change">
                                            <EventMask ShowMask="true" />
                                        </Select>
                                    </DirectEvents>
                                </ext:ComboBox>
                                <%-- <asp:RequiredFieldValidator Display="None" ID="valcmbBond" runat="server" ValidationGroup="TrainingSaveUpdate"
                                    ControlToValidate="cmbBond" ErrorMessage="Bond is required." />--%>
                            </td>
                            <td>
                                <ext:TextField Width="80" ID="txtBondPeriod" runat="server" FieldLabel="Bond Period"
                                    Hidden="true" LabelAlign="top" LabelSeparator="" MaskRe="[0-9]|\.|%" />
                            </td>
                            <td>
                                <ext:ComboBox ID="cmbBondType" runat="server" ValueField="Value" DisplayField="Text" Width="100"
                                    Hidden="true" FieldLabel="&nbsp;" LabelAlign="top" LabelSeparator="" ForceSelection="true"
                                    QueryMode="Local">
                                    <Items>
                                        <ext:ListItem Text="Years" Value="1" />
                                        <ext:ListItem Text="Months" Value="2" />
                                    </Items>
                                </ext:ComboBox>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <ext:TextArea ID="txtNote" runat="server" FieldLabel="Notes" LabelSeparator="" LabelAlign="Top"
                        Rows="3" Cols="50" />
                </td>
            </tr>
            <tr>
                <td valign="bottom" colspan="2">
                    <div class="popupButtonDiv">
                        <ext:Button runat="server" Cls="btn btn-primary" ID="btnSave" Text="<i></i>Save"
                            runat="server">
                            <DirectEvents>
                                <Click OnEvent="btnSave_Click">
                                    <EventMask ShowMask="true" />
                                </Click>
                            </DirectEvents>
                            <Listeners>
                                <Click Handler="valGroup = 'TrainingSaveUpdate'; if(CheckValidation()) return this.disable(); else return false;">
                                </Click>
                            </Listeners>
                        </ext:Button>
                        <div id="divOr" runat="server" class="btnFlatOr">
                            or</div>
                        <ext:LinkButton runat="server" StyleSpec="padding:0px;" ID="LinkButton2" Cls="btnFlatLeftGap"
                            OnClientClick="window.close();" Text="<i></i>Cancel">
                        </ext:LinkButton>
                    </div>
                </td>
            </tr>
        </table>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
