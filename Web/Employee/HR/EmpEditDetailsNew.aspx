﻿<%@ Page Title="Edit Employee Details" Language="C#" MasterPageFile="~/Master/EmployeeExt.Master" AutoEventWireup="true" CodeBehind="EmpEditDetailsNew.aspx.cs" Inherits="Web.Employee.HR.EmpEditDetailsNew" %>

<%@ Register Src="~/Employee/UserControls/EFamilyCtrl.ascx" TagName="EFamilyCtrl" TagPrefix="ucEFC" %>
<%@ Register Src="~/Employee/UserControls/EPreviousEmploymentCtrl.ascx" TagName="EPrevEm" TagPrefix="ucPEm" %>
<%@ Register Src="~/Employee/UserControls/EEduCtrl.ascx" TagName="EEduCtrl" TagPrefix="ucEEdu" %>
<%@ Register Src="~/Employee/UserControls/ETrainingCtrl.ascx" TagName="ETrainCtrl" TagPrefix="ucETr" %>
<%@ Register Src="~/Employee/UserControls/ESeminarCtrl.ascx" TagName="ESeminarCtrl" TagPrefix="ucSem" %>

<%@ Register Src="~/Employee/UserControls/ESkillSetCtrl.ascx" TagName="ESkillSestCtrl" TagPrefix="ucSS" %>
<%@ Register Src="~/Employee/UserControls/ELanguageCtrl.ascx" TagName="ELangCtrl" TagPrefix="ucLang" %>
<%@ Register Src="~/Employee/UserControls/EPublicationCtrl.ascx" TagName="EPubliCtrl" TagPrefix="ucPub" %>
<%@ Register Src="~/Employee/UserControls/EExtraActivitiesCtrl.ascx" TagName="EExtraActivity" TagPrefix="ucEA" %>

<%@ Register Src="~/Employee/UserControls/ENominreeCtrl.ascx" TagName="ENomineeCtrl" TagPrefix="ucEN" %>
<%@ Register Src="~/Employee/UserControls/ENonMonitoryAwardCtrl.ascx" TagName="ENMACtrl" TagPrefix="ucENMA" %>
<%@ Register Src="~/Employee/UserControls/EHobbyCtrl.ascx" TagName="EHobCtrl" TagPrefix="ucEH" %>
<%@ Register Src="~/Employee/UserControls/ECitizenshipCtrl.ascx" TagName="ECtznship" TagPrefix="ucECTzn" %>
<%@ Register Src="~/Employee/UserControls/EDrivingLicenseCtrl.ascx" TagName="EDriLic" TagPrefix="ucDrLic" %>
<%@ Register Src="~/Employee/UserControls/EPassportCtrl.ascx" TagName="EPassport" TagPrefix="ucEPass" %>
<%@ Register Src="~/Employee/UserControls/EDocumentCtrl.ascx" TagName="DocCtrl" TagPrefix="ucDoc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
<script type="text/javascript">
    function afterrender() { }
</script>
<style type="text/css">
        .panel-default
        {
            padding: 10px;
        }
    </style>
    <style type="text/css">
        .items
        {
            padding-left: 20px;
        }
        .items span
        {
            
            display: block;
            font-family: 'Open Sans' ,sans-serif;
            padding-bottom: 2px;
        }
        .items img
        {
            float:left;
            width:18px;height:18px;
            margin-right: 10px;
        }
     
        
        /*ext grid to nomal grid*/


     
        #content
        {
            background: white;
        }

        }
        .accordion-edit:hover, .accordion-edit
        {
            font-weight: normal;
        }
        a:hover
        {
            
            font-weight: normal;
        }
        .accordion {
margin-bottom: 5px;

}

.itemgrid
{
    border:1px solid;
}


   </style>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="mainContent" runat="server">

<div class="separator bottom" style="margin-top: 20px">
    </div>

<h3>
        Your Profile Details</h3>

     <div style="margin-bottom: 10px">
        <asp:Image ID="image" CssClass="left" runat="server" ImageUrl="~/images/sample.jpg"
            Width="173px" Height="170px" />
        <div class="left items">
            <h3 runat="server" class="heading" style="color: #214B71; margin: 0; margin-top: 2px;
                margin-bottom: 3px;" id="title">
            </h3>
            <span runat="server" style="font-size: 15px; font-weight: bold" id="positionDesignation">
            </span>
            <img runat="server" id="groupLevelImg" src="../../Styles/images/emp_position.png"
                style="margin-top: 8px;" />
            <span runat="server" style="padding-top: 10px;" id="groupLevel"></span>
            <img src="../../Styles/images/emp_br_dept.png" />
            <span runat="server" id="branch"></span>
            <img runat="server" id="imgMobile" src="../../Styles/images/emp__phone.png" />
            <span runat="server" id="contactMobile"></span>
            <img src="../../Styles/images/telephone.png" />
            <span runat="server" id="contactPhone"></span>
            <img src="../../Styles/images/emp_email.png" />
            <span runat="server" id="email">&nbsp;</span>
            <img src="../../Styles/images/emp_work.png" runat="server" id="workingForImage" />
            <span runat="server" id="workingFor" style="width: 500px"></span>
        </div>
        <div style="clear: both">
        </div>
    </div>
    <br />

<ext:Hidden ID="Hidden_CustomerID" runat="server">
    </ext:Hidden>


    <ext:TabPanel ID="Panel2" runat="server" MinHeight="500" AutoHeight="true" StyleSpec="padding-top:10px"
        AutoWidth="true" DefaultBorder="false">
        <Items>

            <ext:Panel ID="Panel1" Title="Family" runat="server">
                <Content>
                    <div class="panel panel-default">
                        <ucEFC:EFamilyCtrl Id="FamilyCtrl" runat="server" />
                        
                    </div>
                </Content>
            </ext:Panel>

            <ext:Panel ID="Panel3" Title="Previous Employment" runat="server">
                <Content>
                    <div class="panel panel-default">
                        <ucPEm:EPrevEm Id="ucPrevEmploy" runat="server" />
                    </div>
                </Content>
            </ext:Panel>

            <ext:Panel ID="Panel4" Title="Education and Training" runat="server" MinHeight="1100" AutoHeight="true" OverflowY="Auto">
                <Content>
                    <div class="panel panel-default">
                        <div id="divEducation">
                            <div class="widget">
                                <div class="widget-head">
                                    <span><i> Note: Drag and drop to reorder the education list.</i></span>
                                    <h4 class="heading">
                                        Education</h4>
                                </div>
                                <ucEEdu:EEduCtrl id="ECtrl" runat="server" />
                            </div>
                        </div>


                        <div id="divTraining">
                            <div class="widget">
                                <div class="widget-head">
                                    <h4 class="heading">
                                        Training</h4>
                                </div>
                                <ucETr:ETrainCtrl id="UcETrainCtrl" runat="server" />
                            </div>
                        </div>

                        <div id="divSeminar">
                            <div class="widget">
                                <div class="widget-head">
                                    <h4 class="heading">
                                        Seminar</h4>
                                </div>
                                <ucSem:ESeminarCtrl id="SeminarCtl1" runat="server" />
                            </div>
                        </div>

                        <div id="divSkill">
                            <div class="widget">
                                <div class="widget-head">
                                    <h4 class="heading">
                                        Skill Sets</h4>
                                </div>
                                <ucSS:ESkillSestCtrl id="SkillSetsCtl1" runat="server" />
                            </div>
                        </div>

                        <div id="divLanguageSets">
                            <div class="widget">
                                <div class="widget-head">
                                    <h4 class="heading">
                                        Language</h4>
                                </div>
                                <ucLang:ELangCtrl id="LanguageSetsCtl1" runat="server" />
                            </div>
                        </div>

                        <div id="divPublication">
                            <div class="widget">
                                <div class="widget-head">
                                    <h4 class="heading">
                                        Publication</h4>
                                </div>
                                <ucPub:EPubliCtrl id="PublicationCtl1" runat="server" />
                            </div>
                        </div>

                        <div id="divExtraActivities">
                            <div class="widget">
                                <div class="widget-head">
                                    <h4 class="heading">
                                        Extra Activities</h4>
                                </div>
                                <ucEA:EExtraActivity Id="ExtAct" runat="server" />
                            </div>
                        </div>
                    </div>
                </Content>
            </ext:Panel>

            <ext:Panel ID="Panel5" Title="Photograph" runat="server">
                <Content>
                    <div class="panel panel-default">
                        <table>
                            <tr>
                                <td style="padding-left: 20px; padding-top: 10px;">
                                    <div style="border: 1px solid grey; width: 180px; height: 160px;">
                                        <ext:Image ID="ImgEmployee" runat="Server" Width="180" Height="160">
                                        </ext:Image>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding-left: 20px; padding-top: 10px;">
                                    <ext:FileUploadField ID="fup" runat="server" ButtonText="Browse Photo" Icon="Add"
                                        Width="300" Visible="true" ButtonOffset="1">
                                    </ext:FileUploadField>
                                    <asp:RequiredFieldValidator ValidationGroup="File" ControlToValidate="fup" Display="None"
                                        Text="Please select a file." ID="RequiredFieldValidator1" runat="server" ErrorMessage="Please select a file."></asp:RequiredFieldValidator>
                                </td>
                                <td style="padding-left: 20px; padding-top: 10px;">
                                    <ext:Button runat="server" Text="Upload" ID="btnUpload" Cls="bluebutton left">
                                        <DirectEvents>
                                            <Click OnEvent="btnUpload_Click">
                                                <EventMask ShowMask="true" />
                                            </Click>
                                        </DirectEvents>
                                        <Listeners>
                                            <Click Handler="valGroup= 'File'; return CheckValidation();">
                                            </Click>
                                        </Listeners>
                                    </ext:Button>
                                </td>
                            </tr>
                        </table>
                    </div>
                </Content>
            </ext:Panel>

            <ext:Panel ID="Panel6" Title="Address" runat="server">
                <Content>
                    <div class="panel panel-default">
                        <div class="panel panel-primary" style="float: left; width: 48%;">
                            <div class="panel-heading" style="padding-top: 4px!Important; padding-bottom: 4px!important;">
                                <h3 class="panel-title">
                                    Permanent Address</h3>
                            </div>
                            <div class="panel-body">
                                <table>
                                    <tr>
                                        <td colspan="2">
                                            <ext:ComboBox ID="cmbCountryPerm" FieldLabel="Country " runat="server" LabelAlign="Top"
                                                 Width="180" LabelSeparator="" QueryMode="Local" DisplayField="CountryName"
                                                ValueField="CountryId">
                                                <Store>
                                                    <ext:Store ID="store2" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="CountryId" />
                                                            <ext:ModelField Name="CountryName" />
                                                        </Fields>
                                                    </ext:Store>
                                                </Store>
                                            </ext:ComboBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 250px;">
                                            <ext:ComboBox ID="cmbZonePerm" FieldLabel="Zone " runat="server" LabelAlign="Top"
                                               DisplayField="Zone" Width="180" LabelSeparator="" QueryMode="Local"
                                                ValueField="ZoneId">
                                                <Store>
                                                    <ext:Store ID="Store1" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="Zone" />
                                                            <ext:ModelField Name="ZoneId" />
                                                        </Fields>
                                                    </ext:Store>
                                                </Store>
                                                <DirectEvents>
                                                    <Select OnEvent="ZoneChangePerm">
                                                    </Select>
                                                </DirectEvents>
                                            </ext:ComboBox>
                                        </td>
                                        <td>
                                            <ext:ComboBox ID="cmbDistrictPerm" FieldLabel="District " runat="server" LabelAlign="Top"
                                                Width="180" LabelSeparator="" QueryMode="Local" DisplayField="District"
                                                ValueField="DistrictId">
                                                <Store>
                                                    <ext:Store ID="storeGender" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="District" />
                                                            <ext:ModelField Name="DistrictId" />
                                                        </Fields>
                                                    </ext:Store>
                                                </Store>
                                            </ext:ComboBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 250px;">
                                            <ext:TextField ID="cmbVDCPerm" FieldStyle="border:1px solid lightgray" FieldLabel="VDC / Municipality " runat="server" Width="180"
                                               LabelSeparator="" LabelAlign="Top">
                                            </ext:TextField>
                                        </td>
                                        <td>
                                            <ext:TextField ID="txtStreetColonyPerm" FieldStyle="border:1px solid lightgray"  FieldLabel="Street / Colony " runat="server"
                                             LabelAlign="Top" Width="180" LabelSeparator="">
                                            </ext:TextField>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <ext:TextField ID="txtWardNoPerm" FieldStyle="border:1px solid lightgray"  FieldLabel="Ward No. " runat="server" LabelAlign="Top"
                                               Width="180" LabelSeparator="">
                                            </ext:TextField>
                                        </td>
                                        <td>
                                            <ext:TextField ID="txtHouseNoPerm" FieldStyle="border:1px solid lightgray"  FieldLabel="House No. " runat="server" LabelAlign="Top"
                                                 Width="180" LabelSeparator="">
                                            </ext:TextField>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <ext:TextArea ID="txtLocalityPerm" FieldStyle="border:1px solid lightgray"  FieldLabel="Locality" runat="server" LabelAlign="Top"
                                               LabelSeparator="" Height="60px" Width="430">
                                            </ext:TextArea>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <ext:TextField ID="txtStatePerm" FieldStyle="border:1px solid lightgray"  FieldLabel="State  " runat="server" LabelAlign="Top"
                                                 Width="180" LabelSeparator="">
                                            </ext:TextField>
                                        </td>
                                        <td>
                                            <ext:TextField ID="txtZipCodePerm" FieldStyle="border:1px solid lightgray"  FieldLabel="Zip Code " runat="server" LabelAlign="Top"
                                               Width="180" LabelSeparator="">
                                            </ext:TextField>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="widget" style="float: left; margin-left: 20px; width: 49%;">
                            <div class="panel panel-primary">
                                <div class="panel-heading" style="padding-top: 4px!Important; padding-bottom: 4px!important;">
                                    <h3 class="panel-title">
                                        Present Address</h3>
                                </div>
                                <div class="panel-body">
                                    <table>
                                        <tr>
                                            <td>
                                            </td>
                                            <td>
                                                <ext:LinkButton Icon="ArrowRight" runat="server" ID="btnCopyAbove" Text="Copy">
                                                    <DirectEvents>
                                                        <Click OnEvent="CopyButton_Click">
                                                            <EventMask ShowMask="true" />
                                                        </Click>
                                                    </DirectEvents>
                                                </ext:LinkButton>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 250px;">
                                                <ext:ComboBox ID="cmbCountryTemp" FieldLabel="Country " runat="server" LabelAlign="Top"
                                                    Width="180" LabelSeparator="" QueryMode="Local" DisplayField="CountryName" ValueField="CountryId">
                                                    <Store>
                                                        <ext:Store ID="store4" runat="server">
                                                            <Fields>
                                                                <ext:ModelField Name="CountryId" />
                                                                <ext:ModelField Name="CountryName" />
                                                            </Fields>
                                                        </ext:Store>
                                                    </Store>
                                                </ext:ComboBox>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 250px;">
                                                <ext:ComboBox ID="cmbZoneTemp" FieldLabel="Zone " runat="server" LabelAlign="Top"
                                                    DisplayField="Zone" Width="180" LabelSeparator="" QueryMode="Local" ValueField="ZoneId">
                                                    <Store>
                                                        <ext:Store ID="Store5" runat="server">
                                                            <Fields>
                                                                <ext:ModelField Name="Zone" />
                                                                <ext:ModelField Name="ZoneId" />
                                                            </Fields>
                                                        </ext:Store>
                                                    </Store>
                                                    <DirectEvents>
                                                        <Select OnEvent="ZoneChangeTemp">
                                                        </Select>
                                                    </DirectEvents>
                                                </ext:ComboBox>
                                            </td>
                                            <td>
                                                <ext:ComboBox ID="cmbDistrictTemp" FieldLabel="District " runat="server" LabelAlign="Top"
                                                    Width="180" LabelSeparator="" QueryMode="Local" DisplayField="District" ValueField="DistrictId">
                                                    <Store>
                                                        <ext:Store ID="store6" runat="server">
                                                            <Fields>
                                                                <ext:ModelField Name="DistrictId" />
                                                                <ext:ModelField Name="District" />
                                                            </Fields>
                                                        </ext:Store>
                                                    </Store>
                                                </ext:ComboBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <ext:TextField ID="cmbVDCTemp" FieldStyle="border:1px solid lightgray"  FieldLabel="VDC / Municipality " runat="server" LabelAlign="Top"
                                                    Width="180" LabelSeparator="">
                                                </ext:TextField>
                                            </td>
                                            <td>
                                                <ext:TextField ID="txtStreetColonyTemp" FieldStyle="border:1px solid lightgray"  FieldLabel="Street / Colony " runat="server"
                                                    LabelAlign="Top" Width="180" LabelSeparator="">
                                                </ext:TextField>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 250px;">
                                                <ext:TextField ID="txtWardNoTemp" FieldStyle="border:1px solid lightgray"  FieldLabel="Ward No. " runat="server" LabelAlign="Top"
                                                    Width="180" LabelSeparator="">
                                                </ext:TextField>
                                            </td>
                                            <td>
                                                <ext:TextField ID="txtHouseNoTemp" FieldStyle="border:1px solid lightgray"  FieldLabel="House No. " runat="server" LabelAlign="Top"
                                                    Width="180" LabelSeparator="">
                                                </ext:TextField>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <ext:TextArea ID="txtLocalityTemp"  Height="60px" FieldStyle="border:1px solid lightgray"  FieldLabel="Locality " runat="server" LabelAlign="Top"
                                                    Width="430" LabelSeparator="">
                                                </ext:TextArea>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 250px;">
                                                <ext:TextField ID="txtStateTemp" FieldStyle="border:1px solid lightgray"  FieldLabel="State " runat="server" LabelAlign="Top"
                                                    Width="180" LabelSeparator="">
                                                </ext:TextField>
                                            </td>
                                            <td>
                                                <ext:TextField ID="txtZipCodeTemp" FieldStyle="border:1px solid lightgray"  FieldLabel="Zip Code " runat="server" LabelAlign="Top"
                                                    Width="180" LabelSeparator="">
                                                </ext:TextField>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-primary" style="float: left;">
                            <div class="panel-heading" style="padding-top: 4px!Important; padding-bottom: 4px!important;">
                                <h3 class="panel-title">
                                    Contact Information</h3>
                            </div>
                            <div class="panel-body">
                                <table>
                                    <tr>
                                        <td>
                                            <h5 style="width: 250px;">
                                                OFFICIAL CONTACT</h5>
                                        </td>
                                        <td>
                                            <h5 style="width: 250px;">
                                                PERSONAL CONTACT</h5>
                                        </td>
                                        <td>
                                            <h5 style="width: 250px;">
                                                EMERGENCY CONTACT</h5>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <ext:TextField ID="txtEmailOfficial" FieldStyle="border:1px solid lightgray"  FieldLabel="Email Address  " runat="server"
                                                Disabled="true" LabelAlign="Top" TabIndex='1' Width="180" LabelSeparator="">
                                            </ext:TextField>
                                        </td>
                                        <td>
                                            <ext:TextField TabIndex='5' FieldStyle="border:1px solid lightgray"  ID="txtEmailPersonal" FieldLabel="Email Address   " runat="server"
                                                LabelAlign="Top" Width="180" LabelSeparator="">
                                            </ext:TextField>
                                        </td>
                                        <td>
                                            <ext:TextField TabIndex='8' FieldStyle="border:1px solid lightgray"  ID="txtNameEmergency" FieldLabel="Contact Name   " runat="server"
                                                LabelAlign="Top" Width="180" LabelSeparator="">
                                            </ext:TextField>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <ext:TextField TabIndex='2' FieldStyle="border:1px solid lightgray"  ID="txtPhoneOfficial" FieldLabel="Phone Number " runat="server"
                                              LabelAlign="Top" Width="180" LabelSeparator="">
                                            </ext:TextField>
                                        </td>
                                        <td>
                                            <ext:TextField TabIndex='6' FieldStyle="border:1px solid lightgray"  ID="txtPhonePersonal" FieldLabel="Phone Number" runat="server"
                                                LabelAlign="Top" Width="180" LabelSeparator="">
                                            </ext:TextField>
                                        </td>
                                        <td>
                                            <ext:TextField TabIndex='9' FieldStyle="border:1px solid lightgray"  ID="txtRelationEmergency" FieldLabel="Relation" runat="server"
                                                LabelAlign="Top" Width="180" LabelSeparator="">
                                            </ext:TextField>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <ext:TextField TabIndex='3' FieldStyle="border:1px solid lightgray"  ID="txtExtentionOfficial" FieldLabel="Extention  "
                                                runat="server" LabelAlign="Top" Width="180" LabelSeparator="">
                                            </ext:TextField>
                                        </td>
                                        <td>
                                            <ext:TextField TabIndex='7' FieldStyle="border:1px solid lightgray"  ID="txtMobilePersonal" FieldLabel="Mobile Number " runat="server"
                                                LabelAlign="Top" Width="180" LabelSeparator="">
                                            </ext:TextField>
                                        </td>
                                        <td>
                                            <ext:TextField TabIndex='10' FieldStyle="border:1px solid lightgray"  ID="txtPhoneEmergency" FieldLabel="Phone  " runat="server"
                                                LabelAlign="Top" Width="180" LabelSeparator="">
                                            </ext:TextField>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <ext:TextField TabIndex='4' FieldStyle="border:1px solid lightgray"  ID="txtMobileOfficial" FieldLabel="Mobile Number " runat="server"
                                             LabelAlign="Top" Width="180" LabelSeparator="">
                                            </ext:TextField>
                                        </td>
                                        <td>
                                            <ext:TextField TabIndex='11' FieldStyle="border:1px solid lightgray"  ID="txtMobileEmergency" FieldLabel="Mobile Number "
                                                runat="server" LabelAlign="Top" Width="180" LabelSeparator="">
                                            </ext:TextField>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div id="buttonBlock" runat="server">
                                                <ext:Button ID="btnNext" runat="server" Cls="btn btn-primary" Width="100px" Text="Save"
                                                    StyleSpec="margin-top:20px;" Height="35">
                                                    <DirectEvents>
                                                        <Click OnEvent="ButtonNext_Click">
                                                            <EventMask ShowMask="true" />
                                                        </Click>
                                                    </DirectEvents>
                                                </ext:Button>
                                            </div>
                                        </td>
                                        <td>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div style="clear: both">
                        </div>
                    </div>
                </Content>
            </ext:Panel>

            <ext:Panel ID="Panel8" Title="Identification" runat="server">
                <Content>
                     <div class="panel panel-default">

                      <div id="divCitizenship">
                            <div class="widget">
                                <div class="widget-head">
                                    <h4 class="heading">
                                        Citizenship</h4>
                                </div>
                                <ucECTzn:ECtznship Id="ucCitizenshipCtl1" runat="server" />
                            </div>
                        </div>

                        <div id="div2">
                            <div class="widget">
                                <div class="widget-head">
                                    <h4 class="heading">
                                        Driving License</h4>
                                </div>
                                <ucDrLic:EDriLic Id="DrivingLiscenceCtl1" runat="server" />
                            </div>
                        </div>

                         <div id="div3">
                            <div class="widget">
                                <div class="widget-head">
                                    <h4 class="heading">
                                        Passport</h4>
                                </div>
                                <ucEPass:EPassport Id="PassportCtl1" runat="server" />
                            </div>
                        </div>

                    </div>
               
                </Content>
                
            </ext:Panel>

            <ext:Panel ID="Panel9" Title="Document" runat="server">
                <Content>
                    <div class="panel panel-default">
                      <div id="div4">
                            <div class="widget">
                                <div class="widget-head">
                                    <h4 class="heading">
                                        </h4>
                                </div>
                                <ucDoc:DocCtrl Id="ucDocument" runat="server" />
                            </div>
                        </div>

                    </div>
                </Content>
                
            </ext:Panel>

            <ext:Panel ID="Panel7" Title="Others" runat="server">
                <Content>
                   <div class="panel panel-default">
                    <div id="divHrNominee">
                        <div class="widget">
                            <div class="widget-head">
                                <h4 class="heading">
                                    Nominees</h4>
                            </div>
                            <ucEN:ENomineeCtrl Id="HrNom" runat="server" />
                        </div>
                    </div>

                    <div id="divNonMonetaryAward">
                        <div class="widget">
                            <div class="widget-head">
                                <h4 class="heading">
                                    Non Monetary Award</h4>
                            </div>
                            <ucENMA:ENMACtrl Id="NMACtrl" runat="server" />
                        </div>
                    </div>

                    <div id="divHobby">
                        <div class="widget">
                            <div class="widget-head">
                                <h4 class="heading">
                                    Hobby</h4>
                            </div>
                            <ucEH:EHobCtrl Id="EHobbyControl" runat="server" />
                        </div>
                    </div>
                </div>
                </Content>
            </ext:Panel>

        </Items>
    </ext:TabPanel>



</asp:Content>
