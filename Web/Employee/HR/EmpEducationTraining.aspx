﻿<%@ Page Title="Education and Skills" Language="C#" MasterPageFile="~/Master/EmployeeExt.Master"
    AutoEventWireup="true" CodeBehind="EmpEducationTraining.aspx.cs" Inherits="Web.Employee.HR.EmpEducationTraining" %>

<%@ Register Src="~/NewHR/UserControls/EducationCtl.ascx" TagName="EducationCtl"
    TagPrefix="ucEducation" %>
<%@ Register Src="~/NewHR/UserControls/TrainingCtl.ascx" TagName="TrainingCtl" TagPrefix="ucTraining" %>
<%@ Register Src="~/NewHR/UserControls/SeminarCtl.ascx" TagName="SeminarCtl" TagPrefix="ucSeminar" %>
<%@ Register Src="~/NewHR/UserControls/SkillSetsCtl.ascx" TagName="SkillSetsCtl"
    TagPrefix="ucSkillSets" %>
<%@ Register Src="~/NewHR/UserControls/PublicationCtl.ascx" TagName="PublicationCtl"
    TagPrefix="ucPublication" %>
<%@ Register Src="~/NewHR/UserControls/LanguageSetsCtl.ascx" TagName="LanguageSetsCtl"
    TagPrefix="ucLanguageSets" %>
<%@ Register Src="~/NewHR/UserControls/EduCtrlTest.ascx" TagName="ECtrl" TagPrefix="ucE" %>
<%@ Register Src="~/NewHR/UserControls/UTrainingCtrl.ascx" TagName="UCUTrainingCtrl"
    TagPrefix="UUTrainingCtrl" %>
<%@ Register Src="~/NewHR/UserControls/USeminarCtrl.ascx" TagName="UCUSeminarCtrl"
    TagPrefix="uUSeminarCtrl" %>
<%@ Register Src="~/NewHR/UserControls/USkillSetCtrl.ascx" TagName="UCUSkillSetCtrl"
    TagPrefix="uUSkillSetCtrl" %>
<%@ Register Src="~/NewHR/UserControls/ULanguageCtrl.ascx" TagName="UULanguageCtrl"
    TagPrefix="ucLanguageSetsCtrl" %>
<%@ Register Src="~/NewHR/UserControls/UPublicationCtrl.ascx" TagName="UUPublicationCtrl"
    TagPrefix="ucUPublicationCtrl" %>
<%@ Register Src="~/NewHR/UserControls/ExtraActivitiesCtrl.ascx" TagName="ExtraActivity" TagPrefix="EA" %>
<%@ Register Src="~/NewHR/UserControls/NomineeCtrl.ascx" TagName="HrNominee" TagPrefix="HrN" %>

<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="mainContent" runat="server">
    <div class="separator bottom">
    </div>
    <div class="heading-buttons" style="margin-top: 20px; margin-bottom: -3px;">
    </div>
    <div class="separator bottom">
    </div>
    <div class="innerLR">
    </div>
</asp:Content>
