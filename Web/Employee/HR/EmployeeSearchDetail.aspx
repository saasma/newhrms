﻿<%@ Page Title="Employee Search" Language="C#" MasterPageFile="~/Master/EmployeeExt.Master"
    AutoEventWireup="true" CodeBehind="EmployeeSearchDetail.aspx.cs" Inherits="Web.Employee.HR.EmployeeSearchDetail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <style>
        .menu
        {
            height: 50px;
        }
        
        .RowTable
        {
            margin-left: 0px;
            clear: both;
        }
        
        .menu ul
        {
            list-style: none;
            margin-left: 5px;
        }
        
        .menu li
        {
            display: inline;
            float: left;
            width: 200px;
            margin-left: -5px;
        }
        
        .AutoExtender
        {
            z-index: 999999 !important;
        }
        .items
        {
            padding-left: 20px;
        }
        .items span
        {
            display: block;
            font-family: 'Open Sans' ,sans-serif;
            padding-bottom: 2px;
        }
        .items img
        {
            float: left;
            width: 18px;
            height: 18px;
            margin-right: 10px;
        }
        
        .tdClass
        {
            padding-bottom: 5px;
        }
        .x-panel-body-default
        {
            background: inherit;
        }
    </style>
    <style>
        .x-column-padding
        {
            padding: 10px 0px 10px 10px;
        }
        
        .x-column-padding1
        {
            padding: 10px;
        }
        
        .x-column-padding2
        {
            padding: 10px;
            width: 145px;
        }
        
        .x-panel .x-panel-header-default-framed-top
        {
            border: 1px solid #157FCC;
        }
        .x-panel .x-panel-header-default-framed-top
        {
            border-bottom: 1px solid #157FCC !important;
        }
        .x-panel-default-framed
        {
            -webkit-border-radius: 0px;
            border-radius: 0px;
            border-width: 0px;
        }
        .x-panel-body-default-framed
        {
            border-color: #157fcc !important;
        }
    </style>
    <script type="text/javascript">
        function searchEmp(value) {
            window.location = 'EmployeeSearchDetail.aspx?ID=' + value;
        }
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="mainContent" runat="server">
    <div class="innerLR1">
        <div class="separator bottom">
        </div>
        <h2>
            Employee Search</h2>
        <div style="margin-top: 10px; margin-bottom: 15px;">
            <ext:Store runat="server" ID="storeSearch" AutoLoad="false">
                <Proxy>
                    <ext:AjaxProxy Url="../HR/EmpSearch.ashx">
                        <ActionMethods Read="GET" />
                        <Reader>
                            <ext:JsonReader Root="plants" TotalProperty="total" />
                        </Reader>
                    </ext:AjaxProxy>
                </Proxy>
                <Model>
                    <ext:Model ID="Model2" IDProperty="Name" Name="ItemLineModel" runat="server">
                        <Fields>
                            <ext:ModelField Name="Name" Type="String" />
                            <ext:ModelField Name="EmployeeId" Type="String" />
                        </Fields>
                    </ext:Model>
                </Model>
            </ext:Store>
            <ext:ComboBox LabelSeparator="" EmptyText="Search by Name" ForceSelection="true"
                ID="cmbSearch" LabelWidth="70" runat="server" DisplayField="Name" ValueField="EmployeeId"
                StoreID="storeSearch" TypeAhead="false" Width="250" PageSize="9999" HideBaseTrigger="true"
                MinChars="2" TriggerAction="All">
                <ListConfig LoadingText="Searching..." MinWidth="200" StyleSpec="border-top:1px solid #98c0f4;">
                    <ItemTpl ID="ItemTpl1" runat="server">
                        <Html>
                            <tpl>
                                            <div class="search-item">
                                                            <span>{Name}</span>  
                                             </div>
					                </tpl>
                        </Html>
                    </ItemTpl>
                </ListConfig>
                <Triggers>
                    <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                </Triggers>
                <Listeners>
                    <Select Handler="this.getTrigger(0).show(); searchEmp(this.getValue());" />
                    <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                    <TriggerClick Handler="if (index == 0) { 
                                           this.clearValue(); 
                                           this.getTrigger(0).hide();
                                       }" />
                </Listeners>
            </ext:ComboBox>
        </div>
        <div class="widget" style="width: 100%; display: none" id="block" runat="server">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <!-- panel-btns -->
                    <h3 class="panel-title">
                        Employee Details</h3>
                </div>
                <div class="panel-body">
                    <table>
                        <tr>
                            <td style="width: 800px">
                                <table>
                                    <tr>
                                        <td style="width: 500px">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding-left: 12px">
                                            <%--<asp:Image ID="image" CssClass="left" runat="server" ImageUrl="~/images/sample.jpg"
                                                Width="143px" Height="140px" />--%>
                                            <ext:Image ID="image" Cls="left" runat="server" ImageUrl="~/images/sample.jpg" Width="153px"
                                                Height="150px" />
                                            <div class="left items">
                                                <h3 runat="server" class="heading" style="color: #214B71; margin: 0; margin-top: 2px;
                                                    margin-bottom: 3px;" id="title">
                                                </h3>
                                                <table>
                                                    <tr>
                                                        <td class="tdClass">
                                                            <img src="../../Styles/images/emp_br_dept.png" />
                                                        </td>
                                                        <td class="tdClass">
                                                            <span runat="server" id="branch"></span>
                                                        </td>
                                                    </tr>
                                                    <tr runat="server" id="rowMobile">
                                                        <td class="tdClass">
                                                            <img src="../../Styles/images/emp__phone.png" />
                                                        </td>
                                                        <td class="tdClass">
                                                            <span runat="server" id="contactMobile">&nbsp;</span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="tdClass">
                                                            <img src="../../Styles/images/telephone.png" />
                                                        </td>
                                                        <td class="tdClass">
                                                            <span runat="server" id="contactPhone">&nbsp;</span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="tdClass">
                                                            <img src="../../Styles/images/emp_email.png" />
                                                        </td>
                                                        <td class="tdClass">
                                                            <span runat="server" id="email">&nbsp;</span>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td>
                            </td>
                        </tr>
                    </table>
                    <ext:Portal ID="PortalAddress" runat="server" Border="false">
                        <Items>
                            <ext:PortalColumn ID="PortalColumn1" runat="server" Cls="x-column-padding">
                                <Items>
                                    <ext:Portlet StyleSpec="border:1px solid #157FCC;padding:5px;" ID="Portlet1" runat="server"
                                        Title="Present Address" Icon="Accept">
                                        <Items>
                                            <ext:Label ID="addressTemporary" runat="server" StyleSpec="font-size:12px;" />
                                        </Items>
                                    </ext:Portlet>
                                </Items>
                            </ext:PortalColumn>
                            <ext:PortalColumn ID="PortalColumn2" runat="server" Cls="x-column-padding">
                                <Items>
                                    <ext:Portlet StyleSpec="border:1px solid #157FCC;padding:5px;" ID="Portlet2" runat="server"
                                        Title="Permanent Address" Icon="Accept">
                                        <Items>
                                            <ext:Label ID="addressPermanent" runat="server" StyleSpec="font-size:12px;" />
                                        </Items>
                                    </ext:Portlet>
                                </Items>
                            </ext:PortalColumn>
                        </Items>
                    </ext:Portal>
                    <ext:Portal ID="Portal2" runat="server" Border="false">
                        <Items>
                            <ext:PortalColumn ID="PortalColumnSignature" runat="server" Cls="x-column-padding2">
                                <Items>
                                    <ext:Portlet StyleSpec="border:1px solid #157FCC;padding:5px;" ID="Portlet4" runat="server"
                                        Title="Signature" Icon="Accept" Width="150" Height="220">
                                        <Items>
                                            <ext:Panel Layout="BorderLayout" runat="server">
                                                <Items>
                                                    <ext:HyperLink Region="North" Text="Show Full Signature" runat="server" ID="signatureLink">
                                                    </ext:HyperLink>
                                                    <ext:Container Region="Center" runat="server">
                                                        <Content>
                                                            <ext:Image ID="imgSignature" Region="Center" Height="200" Width="200" Cls="left"
                                                                runat="server" ImageUrl="../../Styles/images/noSignature.png">
                                                            </ext:Image>
                                                        </Content>
                                                    </ext:Container>
                                                </Items>
                                            </ext:Panel>
                                        </Items>
                                    </ext:Portlet>
                                </Items>
                            </ext:PortalColumn>
                            <ext:PortalColumn ID="PortalColumn4" Hidden="true" runat="server" Cls="x-column-padding1">
                                <Items>
                                </Items>
                            </ext:PortalColumn>
                        </Items>
                    </ext:Portal>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
