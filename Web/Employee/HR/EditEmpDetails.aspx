﻿<%@ Page Title="Edit Employee Details" Language="C#" MasterPageFile="~/Master/EmployeeExt.Master"
    AutoEventWireup="true" CodeBehind="EditEmpDetails.aspx.cs" Inherits="Web.Employee.HR.EditEmpDetails" %>


<%@ Register Src="~/NewHR/UserControls/UFamilyCtrl.ascx" TagName="UFamilyCtrl" TagPrefix="ucF" %>
<%@ Register Src="~/NewHR/UserControls/EmploymentCtrl.ascx" TagName="UEmploymentCtrl"
    TagPrefix="ucEmploymentCtrl" %>
<%@ Register Src="~/NewHR/UserControls/EducationCtl.ascx" TagName="EducationCtl"
    TagPrefix="ucEducation" %>
<%@ Register Src="~/NewHR/UserControls/TrainingCtl.ascx" TagName="TrainingCtl" TagPrefix="ucTraining" %>
<%@ Register Src="~/NewHR/UserControls/SeminarCtl.ascx" TagName="SeminarCtl" TagPrefix="ucSeminar" %>
<%@ Register Src="~/NewHR/UserControls/SkillSetsCtl.ascx" TagName="SkillSetsCtl"
    TagPrefix="ucSkillSets" %>
<%@ Register Src="~/NewHR/UserControls/PublicationCtl.ascx" TagName="PublicationCtl"
    TagPrefix="ucPublication" %>
<%@ Register Src="~/NewHR/UserControls/LanguageSetsCtl.ascx" TagName="LanguageSetsCtl"
    TagPrefix="ucLanguageSets" %>
<%@ Register Src="~/NewHR/UserControls/EduCtrlTest.ascx" TagName="ECtrl" TagPrefix="ucE" %>
<%@ Register Src="~/NewHR/UserControls/UTrainingCtrl.ascx" TagName="UCUTrainingCtrl"
    TagPrefix="UUTrainingCtrl" %>
<%@ Register Src="~/NewHR/UserControls/USeminarCtrl.ascx" TagName="UCUSeminarCtrl"
    TagPrefix="uUSeminarCtrl" %>
<%@ Register Src="~/NewHR/UserControls/USkillSetCtrl.ascx" TagName="UCUSkillSetCtrl"
    TagPrefix="uUSkillSetCtrl" %>
<%@ Register Src="~/NewHR/UserControls/ULanguageCtrl.ascx" TagName="UULanguageCtrl"
    TagPrefix="ucLanguageSetsCtrl" %>
<%@ Register Src="~/NewHR/UserControls/UPublicationCtrl.ascx" TagName="UUPublicationCtrl"
    TagPrefix="ucUPublicationCtrl" %>
<%@ Register Src="~/NewHR/UserControls/ExtraActivitiesCtrl.ascx" TagName="ExtraActivity" TagPrefix="EA" %>
<%@ Register Src="~/NewHR/UserControls/NomineeCtrl.ascx" TagName="HrNominee" TagPrefix="HrN" %>
<%@ Register Src="~/NewHR/UserControls/NonMonetaryAwardCtrl.ascx" TagName="NMAward" TagPrefix="NMA" %>
<%@ Register Src="~/NewHR/UserControls/HobbyCtrl.ascx" TagName="EHobbyControl" TagPrefix="EHC" %>
<%@ Register Src="~/NewHR/UserControls/UCitizenshipCtl.ascx" TagName="CitizenshipCtl"
    TagPrefix="ucCitizenshipCtl" %>
<%@ Register Src="~/NewHR/UserControls/UDrivingLiscenceCtl.ascx" TagName="DrivingLiscenceCtl"
    TagPrefix="ucDrivingLiscenceCtl" %>
<%@ Register Src="~/NewHR/UserControls/UPassportCtrl.ascx" TagName="PassportCtl" TagPrefix="ucPassportCtl" %>
<%@ Register Src="~/NewHR/UserControls/DocumentCtrl.ascx" TagName="DocCtrl" TagPrefix="ucDoc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript" src="../../js/jquery.Jcrop.js"></script>
    <script type="text/javascript" src="../../js/jquery.Jcrop.min.js"></script>
    <script type="text/javascript" src="../../js/jquery-ui-1.10.3.min.js"></script>
    <link id="Link1" rel="stylesheet" type="text/css" href="../../css/jquery.Jcrop.css" />
    <link id="Link2" rel="stylesheet" type="text/css" href="../../css/jquery.Jcrop.min.css" />

    <style type="text/css">
        .panel-default
        {
            padding: 10px;
        }
    </style>
    <style type="text/css">
        .items
        {
            padding-left: 20px;
        }
        .items span
        {
            
            display: block;
            font-family: 'Open Sans' ,sans-serif;
            padding-bottom: 2px;
        }
        .items img
        {
            float:left;
            width:18px;height:18px;
            
margin-right: 10px;
        }
        
     
        
        /*ext grid to nomal grid*/


     
        #content
        {
            background: white;
        }

        }
        .accordion-edit:hover, .accordion-edit
        {
            font-weight: normal;
        }
        a:hover
        {
            
            font-weight: normal;
        }
        .accordion {
margin-bottom: 5px;

}

    .ui-dialog-titlebar-close
    {
        visibility: hidden;
    }

   </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="mainContent" runat="server">
    
    <div class="separator bottom" style="margin-top: 20px">
    </div>
    <h3>
        Your Profile Details</h3>
    <div style="margin-bottom: 10px">
        <asp:Image ID="image" CssClass="left" runat="server" ImageUrl="~/images/sample.jpg"
            Width="173px" Height="170px" />
        <div class="left items">
            <h3 runat="server" class="heading" style="color: #214B71; margin: 0; margin-top: 2px;
                margin-bottom: 3px;" id="title">
            </h3>
            <span runat="server" style="font-size: 15px; font-weight: bold" id="positionDesignation">
            </span>
            <img runat="server" id="groupLevelImg" src="../../Styles/images/emp_position.png"
                style="margin-top: 8px;" />
            <span runat="server" style="padding-top: 10px;" id="groupLevel"></span>
            <img src="../../Styles/images/emp_br_dept.png" />
            <span runat="server" id="branch"></span>
            <img runat="server" id="imgMobile" src="../../Styles/images/emp__phone.png" />
            <span runat="server" id="contactMobile"></span>
            <img src="../../Styles/images/telephone.png" />
            <span runat="server" id="contactPhone"></span>
            <img src="../../Styles/images/emp_email.png" />
            <span runat="server" id="email">&nbsp;</span>
            <img src="../../Styles/images/emp_work.png" runat="server" id="workingForImage" />
            <span runat="server" id="workingFor" style="width: 500px"></span>
        </div>
        <div style="clear: both">
        </div>
    </div>
    <br />
    <ext:Hidden ID="Hidden_CustomerID" runat="server">
    </ext:Hidden>
    <ext:TabPanel ID="Panel2" runat="server" MinHeight="500" AutoHeight="true"  StyleSpec="padding-top:10px" Layout="AutoLayout"
        AutoWidth="true" DefaultBorder="false">
        <Items>
            <ext:Panel ID="Panel1" Title="Family" runat="server">
                <Content>
                    <div class="panel panel-default">
                        <ucF:UFamilyCtrl Id="UFamilyCtrl" runat="server" />
                    </div>
                </Content>
            </ext:Panel>
            <ext:Panel ID="Panel3" Title="Previous Employment" runat="server">
                <Content>
                    <div class="panel panel-default">
                        <ucEmploymentCtrl:UEmploymentCtrl Id="ucEmploymentHistoryCtl1" runat="server" />
                    </div>
                </Content>
            </ext:Panel>
            <ext:Panel ID="Panel4" Title="Education and Training" runat="server" MinHeight="1100" AutoHeight="true" OverflowY="Auto">
                <Content>
                    <div class="panel panel-default">
                        <div id="divEducation">
                            <div class="widget">
                                <div class="widget-head">
                                    <span><i> Note: Drag and drop to reorder the education list.</i></span>
                                    <h4 class="heading">
                                        Education</h4>
                                </div>
                                <%--<ucEducation:EducationCtl Id="EducationCtl" runat="server" />--%>
                                <ucE:ectrl id="ECtrl" runat="server" />
                            </div>
                        </div>
                        <div id="divTraining">
                            <div class="widget">
                                <div class="widget-head">
                                    <h4 class="heading">
                                        Training</h4>
                                </div>
                                <%--<ucTraining:TrainingCtl Id="TrainingCtl1" runat="server" />--%>
                                <UUTrainingCtrl:ucutrainingctrl id="UCUTrainingCtrl" runat="server" />
                            </div>
                        </div>
                        <div id="divSeminar">
                            <div class="widget">
                                <div class="widget-head">
                                    <h4 class="heading">
                                        Seminar</h4>
                                </div>
                                <%--<ucSeminar:SeminarCtl Id="SeminarCtl1" runat="server" />--%>
                                <uUSeminarCtrl:ucuseminarctrl id="SeminarCtl1" runat="server" />
                            </div>
                        </div>
                        <div id="divSkill">
                            <div class="widget">
                                <div class="widget-head">
                                    <h4 class="heading">
                                        Skill Sets</h4>
                                </div>
                                <%--<ucSkillSets:SkillSetsCtl Id="SkillSetsCtl1" runat="server" />--%>
                                <uUSkillSetCtrl:ucuskillsetctrl id="SkillSetsCtl1" runat="server" />
                            </div>
                        </div>
                        <div id="divLanguageSets">
                            <div class="widget">
                                <div class="widget-head">
                                    <h4 class="heading">
                                        Language</h4>
                                </div>
                                <%--<ucLanguageSets:LanguageSetsCtl Id="LanguageSetsCtl1" runat="server" />--%>
                                <ucLanguageSetsCtrl:uulanguagectrl id="LanguageSetsCtl1" runat="server" />
                            </div>
                        </div>
                        <div id="divPublication">
                            <div class="widget">
                                <div class="widget-head">
                                    <h4 class="heading">
                                        Publication</h4>
                                </div>
                                <%--<ucPublication:PublicationCtl Id="PublicationCtl1" runat="server" />--%>
                                <ucUPublicationCtrl:uupublicationctrl id="PublicationCtl1" runat="server" />
                            </div>
                        </div>

                          <div id="divExtraActivities">
                            <div class="widget">
                                <div class="widget-head">
                                    <h4 class="heading">
                                        Extra Activities</h4>
                                </div>
                                <EA:ExtraActivity Id="ExtAct" runat="server" />
                            </div>
                        </div>

                        

                       

                    </div>
                </Content>
            </ext:Panel>
            <ext:Panel ID="Panel5" Title="Photograph" runat="server">
                <Content>
                    <div class="panel panel-default">
                        <table>
                            <tr>
                                <td style="padding-left: 20px; padding-top: 10px;">
                                    <div style="width: 180px; height: 180px;">
                                        <ext:Image ID="ImgEmployee" runat="Server" Width="180" Height="180">
                                        </ext:Image>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding-left: 20px; padding-top: 10px;">
                                    <ext:FileUploadField ID="fup" runat="server" ButtonText="Browse Photo" Icon="Add"
                                        Width="300" Visible="true" ButtonOffset="1">
                                        <DirectEvents>
                                            <Change OnEvent="btnUpload_Click" />
                                        </DirectEvents>
                                    </ext:FileUploadField>
                                  
                                </td>
                                
                            </tr>
                        </table>
                    </div>
                </Content>
            </ext:Panel>
            <ext:Panel ID="Panel6" Title="Address" runat="server">
                <Content>
                    <div class="panel panel-default">
                        <div class="panel panel-primary" style="float: left; width: 48%;">
                            <div class="panel-heading" style="padding-top: 4px!Important; padding-bottom: 4px!important;">
                                <h3 class="panel-title">
                                    Permanent Address</h3>
                            </div>
                            <div class="panel-body">
                                <table>
                                    <tr>
                                        <td colspan="2">
                                            <ext:ComboBox ID="cmbCountryPerm" FieldLabel="Country " runat="server" LabelAlign="Top"
                                                 Width="180" LabelSeparator="" QueryMode="Local" DisplayField="CountryName"
                                                ValueField="CountryId">
                                                <Store>
                                                    <ext:Store ID="store2" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="CountryId" />
                                                            <ext:ModelField Name="CountryName" />
                                                        </Fields>
                                                    </ext:Store>
                                                </Store>
                                            </ext:ComboBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 250px;">
                                            <ext:ComboBox ID="cmbZonePerm" FieldLabel="Zone " runat="server" LabelAlign="Top"
                                               DisplayField="Zone" Width="180" LabelSeparator="" QueryMode="Local"
                                                ValueField="ZoneId">
                                                <Store>
                                                    <ext:Store ID="Store1" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="Zone" />
                                                            <ext:ModelField Name="ZoneId" />
                                                        </Fields>
                                                    </ext:Store>
                                                </Store>
                                                <DirectEvents>
                                                    <Select OnEvent="ZoneChangePerm">
                                                    </Select>
                                                </DirectEvents>
                                            </ext:ComboBox>
                                        </td>
                                        <td>
                                            <ext:ComboBox ID="cmbDistrictPerm" FieldLabel="District " runat="server" LabelAlign="Top"
                                                Width="180" LabelSeparator="" QueryMode="Local" DisplayField="District"
                                                ValueField="DistrictId">
                                                <Store>
                                                    <ext:Store ID="storeGender" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="District" />
                                                            <ext:ModelField Name="DistrictId" />
                                                        </Fields>
                                                    </ext:Store>
                                                </Store>
                                            </ext:ComboBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 250px;">
                                            <ext:TextField ID="cmbVDCPerm" FieldLabel="VDC / Municipality " runat="server" Width="180"
                                               LabelSeparator="" LabelAlign="Top">
                                            </ext:TextField>
                                        </td>
                                        <td>
                                            <ext:TextField ID="txtStreetColonyPerm" FieldLabel="Street / Colony " runat="server"
                                             LabelAlign="Top" Width="180" LabelSeparator="">
                                            </ext:TextField>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <ext:TextField ID="txtWardNoPerm" FieldLabel="Ward No. " runat="server" LabelAlign="Top"
                                               Width="180" LabelSeparator="">
                                            </ext:TextField>
                                        </td>
                                        <td>
                                            <ext:TextField ID="txtHouseNoPerm" FieldLabel="House No. " runat="server" LabelAlign="Top"
                                                 Width="180" LabelSeparator="">
                                            </ext:TextField>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <ext:TextArea ID="txtLocalityPerm" FieldLabel="Locality" runat="server" LabelAlign="Top"
                                               LabelSeparator="" Width="430">
                                            </ext:TextArea>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <ext:TextField ID="txtStatePerm" FieldLabel="State  " runat="server" LabelAlign="Top"
                                                 Width="180" LabelSeparator="">
                                            </ext:TextField>
                                        </td>
                                        <td>
                                            <ext:TextField ID="txtZipCodePerm" FieldLabel="Zip Code " runat="server" LabelAlign="Top"
                                               Width="180" LabelSeparator="">
                                            </ext:TextField>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="widget" style="float: left; margin-left: 20px; width: 49%;">
                            <div class="panel panel-primary">
                                <div class="panel-heading" style="padding-top: 4px!Important; padding-bottom: 4px!important;">
                                    <h3 class="panel-title">
                                        Present Address</h3>
                                </div>
                                <div class="panel-body">
                                    <table>
                                        <tr>
                                            <td>
                                            </td>
                                            <td>
                                                <ext:LinkButton Icon="ArrowRight" runat="server" ID="btnCopyAbove" Text="Copy">
                                                    <DirectEvents>
                                                        <Click OnEvent="CopyButton_Click">
                                                            <EventMask ShowMask="true" />
                                                        </Click>
                                                    </DirectEvents>
                                                </ext:LinkButton>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 250px;">
                                                <ext:ComboBox ID="cmbCountryTemp" FieldLabel="Country " runat="server" LabelAlign="Top"
                                                    Width="180" LabelSeparator="" QueryMode="Local" DisplayField="CountryName" ValueField="CountryId">
                                                    <Store>
                                                        <ext:Store ID="store4" runat="server">
                                                            <Fields>
                                                                <ext:ModelField Name="CountryId" />
                                                                <ext:ModelField Name="CountryName" />
                                                            </Fields>
                                                        </ext:Store>
                                                    </Store>
                                                </ext:ComboBox>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 250px;">
                                                <ext:ComboBox ID="cmbZoneTemp" FieldLabel="Zone " runat="server" LabelAlign="Top"
                                                    DisplayField="Zone" Width="180" LabelSeparator="" QueryMode="Local" ValueField="ZoneId">
                                                    <Store>
                                                        <ext:Store ID="Store5" runat="server">
                                                            <Fields>
                                                                <ext:ModelField Name="Zone" />
                                                                <ext:ModelField Name="ZoneId" />
                                                            </Fields>
                                                        </ext:Store>
                                                    </Store>
                                                    <DirectEvents>
                                                        <Select OnEvent="ZoneChangeTemp">
                                                        </Select>
                                                    </DirectEvents>
                                                </ext:ComboBox>
                                            </td>
                                            <td>
                                                <ext:ComboBox ID="cmbDistrictTemp" FieldLabel="District " runat="server" LabelAlign="Top"
                                                    Width="180" LabelSeparator="" QueryMode="Local" DisplayField="District" ValueField="DistrictId">
                                                    <Store>
                                                        <ext:Store ID="store6" runat="server">
                                                            <Fields>
                                                                <ext:ModelField Name="DistrictId" />
                                                                <ext:ModelField Name="District" />
                                                            </Fields>
                                                        </ext:Store>
                                                    </Store>
                                                </ext:ComboBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <ext:TextField ID="cmbVDCTemp" FieldLabel="VDC / Municipality " runat="server" LabelAlign="Top"
                                                    Width="180" LabelSeparator="">
                                                </ext:TextField>
                                            </td>
                                            <td>
                                                <ext:TextField ID="txtStreetColonyTemp" FieldLabel="Street / Colony " runat="server"
                                                    LabelAlign="Top" Width="180" LabelSeparator="">
                                                </ext:TextField>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 250px;">
                                                <ext:TextField ID="txtWardNoTemp" FieldLabel="Ward No. " runat="server" LabelAlign="Top"
                                                    Width="180" LabelSeparator="">
                                                </ext:TextField>
                                            </td>
                                            <td>
                                                <ext:TextField ID="txtHouseNoTemp" FieldLabel="House No. " runat="server" LabelAlign="Top"
                                                    Width="180" LabelSeparator="">
                                                </ext:TextField>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <ext:TextArea ID="txtLocalityTemp" FieldLabel="Locality " runat="server" LabelAlign="Top"
                                                    Width="430" LabelSeparator="">
                                                </ext:TextArea>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 250px;">
                                                <ext:TextField ID="txtStateTemp" FieldLabel="State " runat="server" LabelAlign="Top"
                                                    Width="180" LabelSeparator="">
                                                </ext:TextField>
                                            </td>
                                            <td>
                                                <ext:TextField ID="txtZipCodeTemp" FieldLabel="Zip Code " runat="server" LabelAlign="Top"
                                                    Width="180" LabelSeparator="">
                                                </ext:TextField>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                 <ext:TextField ID="txtCitIssuseDist" FieldLabel="Citizenship Issue Address " runat="server" LabelAlign="Top"
                                                    Width="180" LabelSeparator="">
                                                </ext:TextField>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-primary" style="float: left;">
                            <div class="panel-heading" style="padding-top: 4px!Important; padding-bottom: 4px!important;">
                                <h3 class="panel-title">
                                    Contact Information</h3>
                            </div>
                            <div class="panel-body">
                                <table>
                                    <tr>
                                        <td>
                                            <h5 style="width: 250px;">
                                                OFFICIAL CONTACT</h5>
                                        </td>
                                        <td>
                                            <h5 style="width: 250px;">
                                                PERSONAL CONTACT</h5>
                                        </td>
                                        <td>
                                            <h5 style="width: 250px;">
                                                EMERGENCY CONTACT</h5>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <ext:TextField ID="txtEmailOfficial" FieldLabel="Email Address  " runat="server"
                                                Disabled="true" LabelAlign="Top" TabIndex='1' Width="180" LabelSeparator="">
                                            </ext:TextField>
                                        </td>
                                        <td>
                                            <ext:TextField TabIndex='5' ID="txtEmailPersonal" FieldLabel="Email Address   " runat="server"
                                                LabelAlign="Top" Width="180" LabelSeparator="">
                                            </ext:TextField>
                                        </td>
                                        <td>
                                            <ext:TextField TabIndex='8' ID="txtNameEmergency" FieldLabel="Contact Name   " runat="server"
                                                LabelAlign="Top" Width="180" LabelSeparator="">
                                            </ext:TextField>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <ext:TextField TabIndex='2' ID="txtPhoneOfficial" FieldLabel="Phone Number " runat="server"
                                              LabelAlign="Top" Width="180" LabelSeparator="">
                                            </ext:TextField>
                                        </td>
                                        <td>
                                            <ext:TextField TabIndex='6' ID="txtPhonePersonal" FieldLabel="Phone Number" runat="server"
                                                LabelAlign="Top" Width="180" LabelSeparator="">
                                            </ext:TextField>
                                        </td>
                                        <td>
                                            <ext:TextField TabIndex='9' ID="txtRelationEmergency" FieldLabel="Relation" runat="server"
                                                LabelAlign="Top" Width="180" LabelSeparator="">
                                            </ext:TextField>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <ext:TextField TabIndex='3' ID="txtExtentionOfficial" FieldLabel="Extention  "
                                                runat="server" LabelAlign="Top" Width="180" LabelSeparator="">
                                            </ext:TextField>
                                        </td>
                                        <td>
                                            <ext:TextField TabIndex='7' ID="txtMobilePersonal" FieldLabel="Mobile Number " runat="server"
                                                LabelAlign="Top" Width="180" LabelSeparator="">
                                            </ext:TextField>
                                        </td>
                                        <td>
                                            <ext:TextField TabIndex='10' ID="txtPhoneEmergency" FieldLabel="Phone  " runat="server"
                                                LabelAlign="Top" Width="180" LabelSeparator="">
                                            </ext:TextField>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <ext:TextField TabIndex='4' ID="txtMobileOfficial" FieldLabel="Mobile Number " runat="server"
                                             LabelAlign="Top" Width="180" LabelSeparator="">
                                            </ext:TextField>
                                        </td>
                                        <td>
                                            <ext:TextField TabIndex='11' ID="txtMobileEmergency" FieldLabel="Mobile Number "
                                                runat="server" LabelAlign="Top" Width="180" LabelSeparator="">
                                            </ext:TextField>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div id="buttonBlock" runat="server">
                                                <ext:Button ID="btnNext" runat="server" Cls="btn btn-primary" Width="100px" Text="Save"
                                                    StyleSpec="margin-top:20px;" Height="35">
                                                    <DirectEvents>
                                                        <Click OnEvent="ButtonNext_Click">
                                                            <EventMask ShowMask="true" />
                                                        </Click>
                                                    </DirectEvents>
                                                </ext:Button>
                                            </div>
                                        </td>
                                        <td>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div style="clear: both">
                        </div>
                    </div>
                </Content>
            </ext:Panel>

            <ext:Panel ID="Panel8" Title="Identification" runat="server">
                <Content>
                 <div class="panel panel-default">
                      <div id="div1">
                            <div class="widget">
                                <div class="widget-head">
                                    <h4 class="heading">
                                        Citizenship</h4>
                                </div>
                                <ucCitizenshipCtl:CitizenshipCtl Id="ucCitizenshipCtl1" runat="server" />
                            </div>
                        </div>

                        <div id="div2">
                            <div class="widget">
                                <div class="widget-head">
                                    <h4 class="heading">
                                        Driving License</h4>
                                </div>
                                <ucDrivingLiscenceCtl:DrivingLiscenceCtl Id="DrivingLiscenceCtl1" runat="server" />
                            </div>
                        </div>

                         <div id="div3">
                            <div class="widget">
                                <div class="widget-head">
                                    <h4 class="heading">
                                        Passport</h4>
                                </div>
                                <ucPassportCtl:PassportCtl Id="PassportCtl1" runat="server" />
                            </div>
                        </div>

                        </div>
                </Content>
                
            </ext:Panel>

            <ext:Panel ID="Panel9" Title="Document" runat="server">
                <Content>
                    
                    <div class="panel panel-default">
                        <ucDoc:DocCtrl Id="ucDocument" runat="server" />
                    </div>                      
                        
              

                </Content>
                
            </ext:Panel>

            <ext:Panel ID="Panel7" Title="Others" runat="server">
                <Content>
                 <div class="panel panel-default">
                      <div id="divHrNominee">
                            <div class="widget">
                                <div class="widget-head">
                                    <h4 class="heading">
                                        Nominees</h4>
                                </div>
                                <HrN:HrNominee Id="HrNom" runat="server" />
                            </div>
                        </div>

                        <div id="divNonMonetaryAward">
                            <div class="widget">
                                <div class="widget-head">
                                    <h4 class="heading">
                                        Non Monetary Award</h4>
                                </div>
                                <NMA:NMAward Id="NMACtrl" runat="server" />
                            </div>
                        </div>

                         <div id="divHobby">
                            <div class="widget">
                                <div class="widget-head">
                                    <h4 class="heading">
                                        Hobby</h4>
                                </div>
                                <EHC:EHobbyControl Id="EHobbyControl" runat="server" />
                            </div>
                        </div>

                        </div>
                </Content>
                
            </ext:Panel>

        </Items>
           
    </ext:TabPanel>

      <div style="display:none;">
        <asp:Button ID="btnView1" OnClick="btnView1_Click" Text="text" runat="server" />
    </div>

     <div style="display:none;">
        <asp:Button ID="btnSaveCall" OnClick="btnSaveCall_Click" Text="text" runat="server" />
    </div>

    <div id="popupdiv" title="" style="display: none; background-color: Silver;">
    <asp:Image ID="Image1" runat="server" />
    <br />
    <ext:Button runat="server" Text="Crop" ID="btnCrop" Cls="bluebutton left">
        <DirectEvents>
            <Click OnEvent="btnCropImage_Click">
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
        <Listeners>
        </Listeners>
    </ext:Button>
</div>


<input type="hidden" runat="server" id="_xField" />
<input type="hidden" runat="server" id="_yField" />
<input type="hidden" runat="server" id="_widthField" />
<input type="hidden" runat="server" id="_heightField" />

  <script type="text/javascript">


      function myFunction() {
          //some code here
          $("#popupdiv").dialog({
              modal: true,
              draggable: false,
              resizable: false,
              width: 'auto',
              height: 'auto',
              position: ['center', 'center'],
              show: 'blind',
              hide: 'blind',
              dialogClass: 'ui-dialog-osx',
              buttons: {
                  "Close": function () {
                      $(this).dialog("close");
                  }
              }
          });
      }

      function OpenImagePopup() {
          document.getElementById("<%=btnView1.ClientID %>").click();
      }

      function AfterSaveCall() {
          document.getElementById("<%= btnSaveCall.ClientID %>").click();
      }

      function hideDiv() {
          document.getElementById('popupdiv').style.display = "none";
      }

      var editorID = '<%= Image1.ClientID %>';

      jQuery(function () {
          jQuery('#' + editorID).Jcrop({
              onChange: showCoords,
              onSelect: showCoords,
              aspectRatio: 1
          });
      });

      function showCoords(c) {

          var xField = document.getElementById('<%= _xField.ClientID %>');
          var yField = document.getElementById('<%= _yField.ClientID %>');
          var widthField = document.getElementById('<%= _widthField.ClientID %>');
          var heightField = document.getElementById('<%= _heightField.ClientID %>');

          xField.value = c.x;
          yField.value = c.y;
          widthField.value = c.w;
          heightField.value = c.h;
      }

    
    </script>


</asp:Content>
