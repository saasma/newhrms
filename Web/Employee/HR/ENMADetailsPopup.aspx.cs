﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL;
using BLL.Manager;
using BLL;
using BLL.BO;
using System.IO;
using BLL.Base;
using Ext.Net;


namespace Web.Employee.HR
{
    public partial class ENMADetailsPopup : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();
            }
        }

        private void Initialise()
        {
            ClearFields();

            if(!string.IsNullOrEmpty(Request.QueryString["AwardId"]))
            {
                hdnAwardId.Text = Request.QueryString["AwardId"];
                EditNMAward(int.Parse(hdnAwardId.Text));
            }
        }

        private void ClearFields()
        {
            txtAwardName.Text = "";
            txtAwardedBy.Text = "";
            txtAwardedDate.Text = "";
            txtDescription.Text = "";
            btnSave.Text = Resources.Messages.Save;
        }

        protected void btnSave_Click(object sender, DirectEventArgs e)
        {
            Page.Validate("SaveUpdateNMAward");
            if (Page.IsValid)
            {
                NonMonetaryAward obj = new NonMonetaryAward();
                if (!string.IsNullOrEmpty(hdnAwardId.Text))
                    obj.AwardId = int.Parse(hdnAwardId.Text);

                obj.EmployeeId = GetEmployeeID();
                obj.AwardName = txtAwardName.Text.Trim();

                if (!string.IsNullOrEmpty(txtAwardedBy.Text.Trim()))
                    obj.AwardedBy = txtAwardedBy.Text.Trim();

                if (!string.IsNullOrEmpty(txtAwardedDate.Text.Trim()))
                {
                    obj.AwardDate = txtAwardedDate.Text.Trim();
                    obj.AwardDateEng = BLL.BaseBiz.GetEngDate(obj.AwardDate, IsEnglish);
                }

                if (!string.IsNullOrEmpty(txtDescription.Text.Trim()))
                    obj.Description = txtDescription.Text.Trim();

                Status status = NewHRManager.SaveUpdateNMAward(obj);
                if (status.IsSuccess)
                {
                    if (string.IsNullOrEmpty(hdnAwardId.Text))
                        NewMessage.ShowNormalMessage("Record saved successfully.", "closePopup()");
                    else
                        NewMessage.ShowNormalMessage("Record updated successfully.", "closePopup()");

                    btnSave.Hide();
                }
                else
                    NewMessage.ShowWarningMessage(status.ErrorMessage);
            }

        }

        protected int GetEmployeeID()
        {
            int EmpID = 0;
            if (!string.IsNullOrEmpty(Request.QueryString["ID"]))
            {
                EmpID = int.Parse(Request.QueryString["ID"]);
            }
            else
            {
                EmpID = SessionManager.CurrentLoggedInEmployeeId;
            }
            return EmpID;
        }

        private void EditNMAward(int awardId)
        {
            NonMonetaryAward obj = NewHRManager.GetNonMonetaryAwardById(awardId);
            if (obj != null)
            {
                txtAwardName.Text = obj.AwardName;

                if (!string.IsNullOrEmpty(obj.AwardedBy))
                    txtAwardedBy.Text = obj.AwardedBy;

                if (!string.IsNullOrEmpty(obj.AwardDate))
                    txtAwardedDate.Text = obj.AwardDate;

                if (obj.Description != null)
                    txtDescription.Text = obj.Description;

                hdnAwardId.Text = obj.AwardId.ToString();
                btnSave.Text = Resources.Messages.Update;
            }
        }

    }
}