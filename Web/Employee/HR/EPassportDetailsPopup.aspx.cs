﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using BLL.BO;
using System.IO;

namespace Web.Employee.HR
{
    public partial class EPassportDetailsPopup : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();

                if (!string.IsNullOrEmpty(Request.QueryString["PassportID"]))
                {
                    hdnPassportID.Text = Request.QueryString["PassportID"];
                    editPassport(int.Parse(hdnPassportID.Text));
                }
            }
        }

        protected void Initialise()
        {
            int EmployeeID = GetEmployeeID();
        }

        protected void btnSave_Click(object sender, DirectEventArgs e)
        {
            this.SaveFormData();
        }

        private void SaveFormData()
        {

            Status myStatus = new Status();
            HPassport _HPassport = new HPassport();
            _HPassport.EmployeeId = this.GetEmployeeID();
            bool isSave = bool.Parse(this.hdnPassportID.Text == "" ? "true" : "false");
            if (!isSave)
                _HPassport.PassportId = int.Parse(this.hdnPassportID.Text);

            _HPassport.PassportNo = txtPassportNumber.Text.Trim();
            if (!string.IsNullOrEmpty(calIssueDate.Text.Trim()))
            {
                _HPassport.IssuingDate = calIssueDate.Text.Trim();
                _HPassport.IssuingDateEng = BLL.BaseBiz.GetEngDate(_HPassport.IssuingDate, IsEnglish);
            }
            if (!string.IsNullOrEmpty(calValidUpto.Text.Trim()))
            {
                _HPassport.ValidUpto = calValidUpto.Text.Trim();
                _HPassport.ValidUptoEng = BLL.BaseBiz.GetEngDate(_HPassport.ValidUpto, IsEnglish);
            }

            //file upload section
            string UserFileName = this.FilePassportDocumentUpload.FileName;

            string ext = Path.GetExtension(UserFileName);

            if (ext.ToLower() == ".exe")
            {
                FilePassportDocumentUpload.Reset();
                FilePassportDocumentUpload.Focus();
                NewMessage.ShowWarningMessage("Please upload proper file.");
                return;
            }

            string ServerFileName = Guid.NewGuid().ToString() + ext;
            string relativePath = @"~/uploads/" + ServerFileName;
            if (this.UploadFilePassport(relativePath))
            {
                double fileSize = FilePassportDocumentUpload.PostedFile.ContentLength;

                _HPassport.FileFormat = Path.GetExtension(this.FilePassportDocumentUpload.FileName).Replace(".", "").Trim();
                _HPassport.FileType = this.FilePassportDocumentUpload.PostedFile.ContentType;
                _HPassport.FileLocation = @"../Uploads/";
                _HPassport.ServerFileName = ServerFileName;
                _HPassport.UserFileName = UserFileName;
                _HPassport.Size = this.ToSizeString(fileSize);

            }

            if (SessionManager.CurrentLoggedInEmployeeId == 0)
                _HPassport.Status = 1;
            else
                _HPassport.Status = 0;

            myStatus = NewHRManager.Instance.InsertUpdatePassport(_HPassport, isSave);

            if (myStatus.IsSuccess)
            {
                if (isSave)
                    NewMessage.ShowNormalMessage("Record Saved Successfully.", "closePassPopup()");
                else
                    NewMessage.ShowNormalMessage("Record Updated Successfully.", "closePassPopup()");
            }
            else
            {
                NewMessage.ShowWarningMessage(myStatus.ErrorMessage);
            }
        }

        protected string ToSizeString(double bytes)
        {

            string[] SizeSuffixes = { "bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB" };
            int mag = (int)Math.Log(bytes, 1024);
            decimal adjustedSize = (decimal)bytes / (1 << (mag * 10));
            return string.Format("{0:n1} {1}", adjustedSize, SizeSuffixes[mag]);
        }


        protected bool UploadFilePassport(string relativePath)
        {

            if (this.FilePassportDocumentUpload.HasFile)
            {

                int fileSize = FilePassportDocumentUpload.PostedFile.ContentLength;
                this.FilePassportDocumentUpload.PostedFile.SaveAs(Server.MapPath(relativePath));
                return true;
            }
            else
                return false;

        }

        protected int GetEmployeeID()
        {
            //Guid? AssetID = null;
            int EmpID = 0;
            if (!string.IsNullOrEmpty(Request.QueryString["ID"]))
            {
                EmpID = int.Parse(Request.QueryString["ID"]);
            }
            else
                EmpID = SessionManager.CurrentLoggedInEmployeeId;

            return EmpID;
        }

        protected void btnAddNewLine_Click(object sender, DirectEventArgs e)
        {
            this.ClearFields();
        }

        protected void lnkDeleteFile_Click(object sender, DirectEventArgs e)
        {
            int PassportID = int.Parse(hdnPassportID.Text);
            Status myStatus = new Status();

            HPassport _HPassport = NewHRManager.GetPassportDetailsById(PassportID);
            string path = Server.MapPath(@"~/uploads/" + _HPassport.ServerFileName); 
            myStatus = NewHRManager.Instance.DeletePassportFile(PassportID);
            if (myStatus.IsSuccess)
            {
                if (File.Exists(path))
                {
                    File.Delete(path);
                    lnkDeleteFile.Hide();
                    lblUploadedFile.Hide();
                    NewMessage.ShowNormalMessage("file deleted successfully");
                    //refresh grid
                    // this.LoadPassportGrid(this.GetEmployeeID());

                }
            }



        }

        private object[] PassportFillData
        {
            get
            {
                return new object[]
            {
                new object[] { "", "", "",""},
               
            };
            }
        }

        public void ClearFields()
        {
            this.hdnPassportID.Text = "";
            txtPassportNumber.Text = "";
            calValidUpto.Text = "";
            calIssueDate.Text = "";
            lblUploadedFile.Hide();
            lblUploadedFile.Text = "";
            lnkDeleteFile.Hide();
            btnSave.Text = Resources.Messages.Save;
            FilePassportDocumentUpload.Reset();
        }

        public void editPassport(int PassportID)
        {
            HPassport _HPassport = NewHRManager.GetPassportDetailsById(PassportID);
            this.hdnPassportID.Text = PassportID.ToString();
            txtPassportNumber.Text = _HPassport.PassportNo;
            calIssueDate.Text = _HPassport.IssuingDate;
            calValidUpto.Text = _HPassport.ValidUpto;

            if (!string.IsNullOrEmpty(_HPassport.UserFileName))
            {
                lblUploadedFile.Show();
                lblUploadedFile.Text = _HPassport.UserFileName;
                lnkDeleteFile.Show();
            }
            else
            {
                lblUploadedFile.Hide();
                lblUploadedFile.Text = "";
                lnkDeleteFile.Hide();
            }
            btnSave.Text = Resources.Messages.Update;
        }

        public void HideButtons()
        {
            btnSave.Hide();
            LinkButton1.Hide();
        }

    }
}