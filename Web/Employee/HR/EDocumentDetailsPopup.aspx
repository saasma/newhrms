﻿<%@ Page Title="Document Details" Language="C#" MasterPageFile="~/Master/ForEmployeePopupPage.Master" AutoEventWireup="true" CodeBehind="EDocumentDetailsPopup.aspx.cs" Inherits="Web.Employee.HR.EDocumentDetailsPopup" %>

<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">

<script type="text/javascript">

    var skipLoadingCheck = true;

    function closeDocPopup() {
        window.close();
        window.opener.reloadDocumentGrid();
    }
  
</script>
<script type="text/javascript">
    function afterRender() { }
</script>
 <style type="text/css">
        input[type=text], textarea, select
        {
            border: 1px solid #A2B4C6;
        }
        textarea
        {
            height: 50px !important;
        }
    </style>
</asp:Content>
   
<asp:Content ID="Content3" ContentPlaceHolderID="mainContent" runat="server">

<ext:ResourceManager ID="ResourceManager1"  Namespace=""  ScriptMode="Release" runat="server" />

<ext:Hidden runat="server" ID="hiddenValue" />

<div class="popupHeader">
        <h4 style="margin-top:0px; color:White; margin-left:20px;">
            Document Details</h4>
    </div>



<div class="" style='margin-top: 0px'>

    <table class="fieldTable" style="margin-left:20px;">   
        <tr style="height: 30px;">
                                    <td style='width: 100px!important'>
                                        Type
                                    </td>
                                    <td>
                                        <ext:ComboBox ForceSelection="true" DisplayField="DocumentTypeName" ValueField="DocumentTypeId" QueryMode="Local"
                                            Width="300" ID="cmbType" runat="server">
                                            <Store>
                                                <ext:Store ID="Store1" runat="server">
                                                    <Model>
                                                        <ext:Model ID="Model1" runat="server">
                                                            <Fields>
                                                                <ext:ModelField Name="DocumentTypeId" Type="String" />
                                                                <ext:ModelField Name="DocumentTypeName" Type="string" />
                                                            </Fields>
                                                        </ext:Model>
                                                    </Model>
                                                </ext:Store>
                                            </Store>
                                            <SelectedItems>
                                                <ext:ListItem Index="0" />
                                            </SelectedItems>
                                        </ext:ComboBox>
                                    </td>
                                </tr>

                                <tr id="rowDesc" runat="server">
                                    <td style='width: 60px!important; vertical-align: top'>
                                        Description
                                    </td>
                                    <td>
                                        <%--<asp:TextBox Width="200px"  Rows="3" ID="txtDesc" runat="server" TextMode="MultiLine" />--%>
                                        <ext:TextArea Width="300" Height="50" ID="txtDesc" runat="server">
                                        </ext:TextArea>
                                    </td>
                                </tr>
                                <tr style="height: 50px;">
                                    <td style='width: 100px!important'>
                                        Select File
                                    </td>
                                    <td>
                                        <%--<asp:FileUpload ID="fup" Width="300px" CssClass="file" runat="server" />--%>
                                        <ext:FileUploadField ID="fup" runat="server" Width="300" Icon="Attach" />
                                        <asp:RequiredFieldValidator ValidationGroup="SaveDocument" ControlToValidate="fup"
                                            Display="None" ID="RequiredFieldValidator1" runat="server" ErrorMessage="Please select a file."></asp:RequiredFieldValidator>
                                    </td>
                                </tr>                                
                             
                                <tr style="width: 300px;">
                                    <td valign="bottom" colspan="2">
                                        <div class="popupButtonDiv">
                                            <ext:Button runat="server" ID="btnPositionSaveUpdate" Cls="btn btn-primary" Text="<i></i>Save"
                                                runat="server">
                                                <DirectEvents>
                                                    <Click OnEvent="btnLevelSaveUpdate_Click">
                                                        <EventMask ShowMask="true" />
                                                    </Click>
                                                </DirectEvents>
                                                <Listeners>
                                                    <Click Handler="valGroup = 'SaveDocument';  if(CheckValidation()) return this.disable(); else return false;">
                                                    </Click>
                                                </Listeners>
                                            </ext:Button>
                                            <div class="btnFlatOr">
                                                or
                                            </div>
                                            <ext:LinkButton runat="server" Cls="btnFlatLeftGap" StyleSpec="padding:0px;" ID="LinkButton4" OnClientClick="window.close();"
                                                Text="<i></i>Cancel" >
                                            </ext:LinkButton>
                                        </div>
                                    </td>
                                </tr>

    </table>
</div>

</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
