﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL.Entity;
using BLL;
using Utils;
using System.IO;

namespace Web.Employee.HR
{
    public partial class EmpSearchEmployee : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                gridEmployeeSearch.GetStore().DataSource = new string[] { };
                gridEmployeeSearch.GetStore().DataBind();
            }
        }

        protected void btnLoad_Click(object sender, DirectEventArgs e)
        {
            if (string.IsNullOrEmpty(hdnEmpId.Text))
            {
                if (cmbSearch.Text.Length < 3)
                {
                    NewMessage.ShowWarningMessage("Please type at least three characters to search.");
                    return;
                }
            }

            PagingToolbar1.DoRefresh();

        }
    }

}
