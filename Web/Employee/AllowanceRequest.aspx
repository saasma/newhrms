﻿<%@ Page Title="My Allowance Request List" Language="C#" MasterPageFile="~/Master/EmployeeMS.Master"
    AutoEventWireup="true" CodeBehind="AllowanceRequest.aspx.cs" Inherits="Web.Employee.AllowanceRequest" %>

<%--<%@ Register Src="~/NewHR/UserControls/EmployeeDetailsCtl.ascx" TagName="EmployeeDetailsCtl"
    TagPrefix="uc1" %>--%>
<%@ Register Src="~/Controls/ContentHeader.ascx" TagName="ContentHeader" TagPrefix="C" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">

        var CommandHandler1 = function(command, record){
            <%= hiddenValue.ClientID %>.setValue(record.data.CounterRequestID);
                if(command=="Edit")
                {
                    <%= btnEditLevel.ClientID %>.fireEvent('click');
                }
                else if(command=="Detail")
                {
                     <%= btnDetailLevel.ClientID %>.fireEvent('click');
                }
                else
                {
                
                    <%= btnDeleteLevel.ClientID %>.fireEvent('click');
                }

             }
             
            function typeChange(value)
            {
                //based on
                if(value == "1")
                {
                    document.getElementById("ctl00_ContentPlaceHolder_Main_rowPercent1").style.display = "block";
                    document.getElementById("ctl00_ContentPlaceHolder_Main_rowPercent2").style.visibility = "visible";

                    document.getElementById("ctl00_ContentPlaceHolder_Main_rowFixed").style.display = "none";
                }
                else
                {
                    document.getElementById("ctl00_ContentPlaceHolder_Main_rowPercent1").style.display = "none";
                    document.getElementById("ctl00_ContentPlaceHolder_Main_rowPercent2").style.visibility = "hidden";

                    document.getElementById("ctl00_ContentPlaceHolder_Main_rowFixed").style.display = "block";
                }
            }


            function searchList() {
            <%=PagingToolbar1.ClientID %>.doRefresh();
        }
             
    </script>
    <script src="override.js" type="text/javascript"></script>
    <link href="../css/newDesignChange.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .x-tree-icon
        {
            display: none !important;
        }
        .hideLeftBlockCssInPage
        {
            margin: 0px !important;
            padding-left: 20px !important;
        }
        #menu
        {
            display: none;
        }
        
        .x-form-textarea, textarea
        {
            height: 40px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
    <C:ContentHeader runat="server" />
    <ext:Hidden runat="server" ID="hiddenValue" />
    <ext:LinkButton runat="server" Hidden="true" ID="btnEditLevel" runat="server">
        <DirectEvents>
            <Click OnEvent="btnEditLevel_Click">
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:LinkButton>
    <ext:LinkButton runat="server" Hidden="true" ID="btnDetailLevel" runat="server">
        <DirectEvents>
            <Click OnEvent="btnDetailLevel_Click">
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:LinkButton>
    <ext:Button ID="btnDeleteLevel" runat="server" Hidden="true">
        <DirectEvents>
            <Click OnEvent="btnDeleteLevel_Click">
                <Confirmation ConfirmRequest="true" Message="Are you sure, you want to delete the request?" />
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:Button>
    <h4>
        My Allowance Requests</h4>
    <%--<uc1:EmployeeDetailsCtl Id="EmployeeDetailsCtl1" runat="server" />--%>
    <table>
        <tr>
            <td valign="bottom">
                <ext:Button runat="server" ID="btnAddNewLine" Cls="btn btn-primary" Text="<i></i>Request Allowance">
                    <Listeners>
                        <Click Handler="#{hiddenValue}.setValue('');#{windowAddEditOvertime}.show();" />
                    </Listeners>
                </ext:Button>
            </td>
            <td style="padding-left: 20px;">
                <ext:ComboBox runat="server" SelectedIndex="0" Width="120" ID="cmbType" ForceSelection="true"
                    FieldLabel="Period" LabelAlign="Top" DisplayField="Text" ValueField="Value" QueryMode="Local"
                    LabelSeparator="">
                    <Items>
                        <ext:ListItem Value="-1" Text="All" />
                        <ext:ListItem Value="1" Text="This Week">
                        </ext:ListItem>
                        <ext:ListItem Value="2" Text="This Month">
                        </ext:ListItem>
                        <ext:ListItem Value="3" Text="Last Month">
                        </ext:ListItem>
                        <ext:ListItem Value="4" Text="This Year">
                        </ext:ListItem>
                    </Items>
                    <SelectedItems>
                        <ext:ListItem Value="-1" Text="All">
                        </ext:ListItem>
                    </SelectedItems>
                    <Listeners>
                        <Select Handler="searchList();" />
                    </Listeners>
                </ext:ComboBox>
            </td>
            <td style="padding-left: 20px;">
                <ext:ComboBox runat="server" Width="120" ID="cmbStatus" ForceSelection="true" FieldLabel="Status"
                    LabelAlign="Top" DisplayField="Text" ValueField="Value" QueryMode="Local" LabelSeparator="">
                    <Items>
                        <ext:ListItem Value="-1" Text="All">
                        </ext:ListItem>
                        <ext:ListItem Value="0" Text="Pending">
                        </ext:ListItem>
                        <ext:ListItem Value="1" Text="Recommended">
                        </ext:ListItem>
                        <ext:ListItem Value="2" Text="Approved">
                        </ext:ListItem>
                        <ext:ListItem Value="3" Text="Rejected">
                        </ext:ListItem>
                    </Items>
                    <SelectedItems>
                        <ext:ListItem Value="-1" Text="All">
                        </ext:ListItem>
                    </SelectedItems>
                    <Listeners>
                        <Select Handler="searchList();" />
                    </Listeners>
                </ext:ComboBox>
            </td>
            <td style="padding-left: 20px;">
                <ext:ComboBox ID="cmbCounterTypeFilter" runat="server" ForceSelection="true" FieldLabel="Allowance Type"
                    LabelAlign="Top" DisplayField="Name" ValueField="EveningCounterTypeId" Width="200"
                    QueryMode="Local" LabelSeparator="">
                    <Store>
                        <ext:Store ID="storeCounterTypeFilter" runat="server">
                            <Model>
                                <ext:Model ID="model3" runat="server" IDProperty="EveningCounterTypeId">
                                    <Fields>
                                        <ext:ModelField Name="EveningCounterTypeId" Type="string" />
                                        <ext:ModelField Name="Name" Type="string" />
                                    </Fields>
                                </ext:Model>
                            </Model>
                        </ext:Store>
                    </Store>
                    <Listeners>
                        <Select Handler="searchList();" />
                    </Listeners>
                </ext:ComboBox>
            </td>
            <td valign="bottom" style="padding-left: 20px;">
                <ext:Button runat="server" ID="btnExport" Cls="btn btn-primary" Text="<i></i>Export to Excel"
                    OnClick="btnExport_Click" AutoPostBack="true">
                </ext:Button>
            </td>
        </tr>
    </table>
    <ext:GridPanel StyleSpec="margin-top:15px;" ID="GridLevels" runat="server" Cls="itemgrid">
        <Store>
            <ext:Store ID="Store3" runat="server" AutoLoad="true" PageSize="50">
                <Proxy>
                    <ext:AjaxProxy Json="true" Url="../Handler/AllowanceRequestForEmpHandler.ashx">
                        <ActionMethods Read="GET" />
                        <Reader>
                            <ext:JsonReader Root="data" TotalProperty="total" />
                        </Reader>
                    </ext:AjaxProxy>
                </Proxy>
                <AutoLoadParams>
                    <ext:Parameter Name="start" Value="0" Mode="Raw" />
                </AutoLoadParams>
                <Parameters>
                    <ext:StoreParameter Name="Period" Value="#{cmbType}.getValue()" Mode="Raw" ApplyMode="Always" />
                    <ext:StoreParameter Name="Status" Value="#{cmbStatus}.getValue()" Mode="Raw" ApplyMode="Always" />
                    <ext:StoreParameter Name="CounterTypeId" Value="#{cmbCounterTypeFilter}.getValue()"
                        Mode="Raw" ApplyMode="Always" />
                </Parameters>
                <Model>
                    <ext:Model ID="Model4" runat="server" IDProperty="CounterRequestID">
                        <Fields>
                            <ext:ModelField Name="CounterRequestID" Type="string" />
                            <ext:ModelField Name="StartDate" Type="Date" />
                            <ext:ModelField Name="EndDate" Type="Date" />
                            <ext:ModelField Name="Reason" Type="String" />
                            <ext:ModelField Name="StatusStr" Type="String" />
                            <ext:ModelField Name="EveningCounterTypeStr" Type="String" />
                            <ext:ModelField Name="Days" Type="String" />
                        </Fields>
                    </ext:Model>
                </Model>
            </ext:Store>
        </Store>
        <ColumnModel>
            <Columns>
                <ext:DateColumn ID="fromDateColumn" Sortable="false" MenuDisabled="true" runat="server"
                    Text="From Date" Format="yyyy-MMM-dd" Width="120" Align="Left" DataIndex="StartDate" />
                <ext:DateColumn ID="toDateColumn" Sortable="false" MenuDisabled="true" runat="server"
                    Text="To Date" Format="yyyy-MMM-dd" Width="120" Align="Left" DataIndex="EndDate" />
                <ext:Column ID="ColumnDaysOrHours" Sortable="false" MenuDisabled="true" runat="server"
                    Text="Units" Width="80" Align="Left" DataIndex="Days" />
                <ext:Column ID="Column1" Sortable="false" MenuDisabled="true" runat="server" Text="Allowance Type"
                    Width="215" Align="Left" DataIndex="EveningCounterTypeStr" />
                <ext:Column ID="Column3" Sortable="false" MenuDisabled="true" runat="server" Text="Status"
                    Width="125" Align="Left" DataIndex="StatusStr" />
                <ext:Column ID="Column2" Sortable="false" MenuDisabled="true" runat="server" Text="Reason"
                    Width="450" Align="Left" DataIndex="Reason">
                </ext:Column>
                <ext:CommandColumn runat="server" Text="Actions" Align="Center" Width="80">
                    <Commands>
                        <ext:GridCommand Cls="extGridImageIconCls" Text="<i></i>" Icon="pencil" CommandName="Edit" />
                        <ext:GridCommand Cls="extGridImageIconCls" Text="<i></i>" Icon="Cancel" CommandName="Delete" />
                    </Commands>
                    <Listeners>
                        <Command Handler="CommandHandler1(command,record);" />
                    </Listeners>
                </ext:CommandColumn>
            </Columns>
        </ColumnModel>
        <SelectionModel>
            <ext:RowSelectionModel ID="RowSelectionModel2" runat="server" Mode="Single" />
        </SelectionModel>
        <BottomBar>
            <ext:PagingToolbar ID="PagingToolbar1" runat="server" PageSize="50" DisplayInfo="true"
                DisplayMsg="Displaying Requests {0} - {1} of {2}" EmptyMsg="No Records to display">
                <Items>
                    <ext:ComboBox Hidden="true" runat="server" SelectedIndex="0" Width="80" ID="cmbPageSize">
                        <Items>
                            <ext:ListItem Text="20" Value="20" />
                            <ext:ListItem Text="30" Value="30" />
                            <ext:ListItem Text="50" Value="50" />
                        </Items>
                        <Listeners>
                            <Select Handler="#{PagingToolbar1}.pageSize = parseInt( '50'); searchList();" />
                        </Listeners>
                    </ext:ComboBox>
                </Items>
            </ext:PagingToolbar>
        </BottomBar>
    </ext:GridPanel>
    <div class="buttonBlock">
    </div>
    <ext:Window ID="windowAddEditOvertime" ButtonAlign="Left" AutoScroll="true" runat="server"
        Title="Allowance Request" Icon="Application" Width="425" Height="500" BodyPadding="5"
        Hidden="true" Modal="true">
        <Content>
            <table class="fieldTable">
                <%-- <tr>
                    <td colspan="2">
                        <ext:ComboBox ID="cmbCounterType" runat="server" ForceSelection="true" FieldLabel="Allowance Type *"
                            LabelAlign="Top" DisplayField="Name" ValueField="EveningCounterTypeId" Width="368"
                            QueryMode="Local" LabelSeparator="">
                            <Store>
                                <ext:Store ID="storeCounterType" runat="server">
                                    <Model>
                                        <ext:Model ID="modelCounterType" runat="server" IDProperty="EveningCounterTypeId">
                                            <Fields>
                                                <ext:ModelField Name="EveningCounterTypeId" Type="string" />
                                                <ext:ModelField Name="Name" Type="string" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                        </ext:ComboBox>
                        <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator2" runat="server"
                            ValidationGroup="SaveUpdatePopup" ControlToValidate="cmbCounterType" ErrorMessage="Allowance Type is required." />
                    </td>
                </tr>--%>
                <tr>
                    <tr>
                        <td colspan="2">
                            <ext:ComboBox ID="cmbCounterType" runat="server" ForceSelection="true" FieldLabel="Allowance Type *"
                                LabelAlign="Top" DisplayField="Name" ValueField="EveningCounterTypeId" Width="368"
                                QueryMode="Local" LabelSeparator="">
                                <Store>
                                    <ext:Store ID="storeCounterType" runat="server">
                                        <Model>
                                            <ext:Model ID="modelCounterType" runat="server" IDProperty="EveningCounterTypeId">
                                                <Fields>
                                                    <ext:ModelField Name="EveningCounterTypeId" Type="string" />
                                                    <ext:ModelField Name="Name" Type="string" />
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                            </ext:ComboBox>
                            <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator2" runat="server"
                                ValidationGroup="SaveUpdatePopup" ControlToValidate="cmbCounterType" ErrorMessage="Allowance Type is required." />
                        </td>
                    </tr>
                    <td>
                        <ext:DateField ID="txtFromDate" runat="server" FieldLabel="From Date *" LabelAlign="top"
                            LabelSeparator="" Width="175" AllowBlank="false">
                            <DirectEvents>
                                <Select OnEvent="Date_Change">
                                </Select>
                            </DirectEvents>
                            <Plugins>
                                <ext:GenericPlugin ID="GenericPlugin1" runat="server" InstanceName="Ext.ux.DatePicker.HighlightWithNepDates" />
                            </Plugins>
                        </ext:DateField>
                        <asp:RequiredFieldValidator Enabled="true" Display="None" ID="valtxtPublicationName"
                            runat="server" ValidationGroup="SaveUpdatePopup" ControlToValidate="txtFromDate"
                            ErrorMessage="From date is required." />
                    </td>
                    <td>
                        <ext:DisplayField ID="txtFromDateNep" StyleSpec="margin-top:15px" runat="server"
                            FieldLabel=" " LabelAlign="top" LabelSeparator="" Width="175" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <ext:DateField ID="txtToDate" runat="server" FieldLabel="To Date *" LabelAlign="top"
                            LabelSeparator="" Width="175" Hidden="false" AllowBlank="false">
                            <DirectEvents>
                                <Select OnEvent="Date_Change">
                                </Select>
                            </DirectEvents>
                            <Plugins>
                                <ext:GenericPlugin ID="HighlightDates1" runat="server" InstanceName="Ext.ux.DatePicker.HighlightWithNepDates" />
                            </Plugins>
                        </ext:DateField>
                        <asp:RequiredFieldValidator Enabled="true" Display="None" ID="RequiredFieldValidator1"
                            runat="server" ValidationGroup="SaveUpdatePopup" ControlToValidate="txtToDate"
                            ErrorMessage="To Date is required." />
                    </td>
                    <td>
                        <ext:DisplayField ID="txtToDateNep" StyleSpec="margin-top:15px" runat="server" FieldLabel=" "
                            LabelAlign="top" LabelSeparator="" Width="175" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <ext:NumberField ID="txtDays" AllowDecimals="false" runat="server" FieldLabel="Units *"
                            LabelAlign="top" LabelSeparator=" " Width="175" Hidden="false" AllowBlank="false"
                            MinValue="0">
                        </ext:NumberField>
                        <asp:RequiredFieldValidator Enabled="true" Display="None" ID="RequiredFieldValidator5"
                            runat="server" ValidationGroup="SaveUpdatePopup" ControlToValidate="txtDays"
                            ErrorMessage="Days/Hours is required." />
                    </td>
                    <%--<td>
                        <div style="margin-top: 10px; padding-top: 10px;">
                            Actual Worked days between the two Dates</div>
                    </td>--%>
                </tr>
                <tr>
                    <td colspan="2">
                        <ext:TextArea ID="txtReason" LabelSeparator="" runat="server" FieldLabel="Reason"
                            LabelAlign="Top" Width="368" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <ext:ComboBox ID="cmbRecomender" runat="server" FieldLabel="Send for Recomendation to *"
                            LabelAlign="Top" DisplayField="Text" ForceSelection="true" ValueField="Value"
                            Width="175" QueryMode="Local" LabelSeparator="">
                            <Store>
                                <ext:Store ID="storeRecomender" runat="server">
                                    <Model>
                                        <ext:Model ID="model1" runat="server" IDProperty="Value">
                                            <Fields>
                                                <ext:ModelField Name="Value" Type="string" />
                                                <ext:ModelField Name="Text" Type="string" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                        </ext:ComboBox>
                        <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator3" runat="server"
                            ValidationGroup="SaveUpdatePopup" ControlToValidate="cmbRecomender" ErrorMessage="Recommender is required." />
                    </td>
                    <td>
                        <ext:ComboBox ID="cmbApprover" runat="server" FieldLabel="Send for Approval to *"
                            LabelAlign="Top" DisplayField="Text" ForceSelection="true" ValueField="Value"
                            Width="175" QueryMode="Local" LabelSeparator="">
                            <Store>
                                <ext:Store ID="storeApprover" runat="server">
                                    <Model>
                                        <ext:Model ID="model2" runat="server" IDProperty="Value">
                                            <Fields>
                                                <ext:ModelField Name="Value" Type="string" />
                                                <ext:ModelField Name="Text" Type="string" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                        </ext:ComboBox>
                        <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator4" runat="server"
                            ValidationGroup="SaveUpdatePopup" ControlToValidate="cmbApprover" ErrorMessage="Approval is required." />
                    </td>
                </tr>
            </table>
        </Content>
        <Buttons>
            <ext:Button runat="server" Cls="btn btn-primary" StyleSpec="margin-left:10px" ValidationGroup="SaveUpdatePopup"
                ID="btnOvertimeType" Text="<i></i>Save">
                <DirectEvents>
                    <Click OnEvent="btnOvertimeType_Click">
                        <EventMask ShowMask="true" />
                    </Click>
                </DirectEvents>
                <Listeners>
                    <Click Handler="valGroup = 'SaveUpdatePopup'; return CheckValidation();">
                    </Click>
                </Listeners>
            </ext:Button>
            <ext:LinkButton runat="server" StyleSpec="margin-left:10px;padding:0px;" Cls="btnFlatLeftGap" ID="LinkButton5"
                Text="<i></i>Cancel">
                <Listeners>
                    <Click Handler="#{windowAddEditOvertime}.hide();">
                    </Click>
                </Listeners>
            </ext:LinkButton>
        </Buttons>
    </ext:Window>
    <br />
</asp:Content>
