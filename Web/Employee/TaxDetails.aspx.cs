﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


using BLL;
using DevExpress.XtraReports.UI;
using Web.CP.Report.Templates.HR;
using Web.ReportDataSetTableAdapters;
using Utils.Calendar;
using BLL.Base;
using DAL;
using BLL.Manager;
using System.Data;

namespace Web.CP.Report
{
    public partial class TaxDetails1 : BasePage
    {
        protected  void LoadReportHandler()
        {        
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //report.Filter.PayrollFrom = false;
            report.Filter.PayrollTo = false;
            report.Filter.Branch = false;
            report.Filter.Department = false;
            //report.Filter.Employee = true;
            report.Filter.CostCode = false;
            report.Filter.Program = false;
            // if (!IsPostBack || CommonManager.CompanySetting.RemoveStyleWhileExportingReport)

            if (IsPostBack)
                LoadReport();

            report.Filter.Employee = false;
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                report.Filter.SelectLastPayroll();
            }
        }

        public ReportDataSet.TaxCalculationDetailsDataTable GetDataTable(string data,EEmployee emp)
        {
            
            string[] rows = data.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);

            ReportDataSet.TaxCalculationDetailsDataTable table = new ReportDataSet.TaxCalculationDetailsDataTable();

            foreach (string strRow in rows)
            {
                string[] values = strRow.Split(new char[] { ':' });

                ReportDataSet.TaxCalculationDetailsRow row = table.NewTaxCalculationDetailsRow();

                row.Header = values[0];

                if (values.Length <= 1)
                    continue;

                if (!string.IsNullOrEmpty(values[1]))

                    row["Amount"] = decimal.Parse(values[1]);

                if (values.Length >= 3)
                    row.Note = values[2];

                //try
                //{

                int rowType = 0;
                if (values.Length >= 4 && int.TryParse(values[3], out rowType))
                    row.RowType = int.Parse(values[3]);
                //}
                //catch { }
                if (emp != null)
                {
                    row.EmployeeId = "I No : " + (emp.EHumanResources.Count > 0 ? emp.EHumanResources[0].IdCardNo : "");
                    row.Name = "Name : " + emp.Name;
                }

                table.AddTaxCalculationDetailsRow(row);
            }

            // Move Yearly Income Forecast to upper            
            //for (int i = 0; i < table.Rows.Count; i++)
            //{
            //    if ((table.Rows[i] as ReportDataSet.TaxCalculationDetailsRow).Header == "Past Regular Income Amount")
            //    {
            //        ReportDataSet.TaxCalculationDetailsRow row = table.Rows[i] as ReportDataSet.TaxCalculationDetailsRow;
            //        ReportDataSet.TaxCalculationDetailsRow newRow = table.NewTaxCalculationDetailsRow();
            //        newRow.Header = row.Header;
            //        newRow.Amount = row.Amount;
            //        newRow.Note = row.Note;
            //        newRow.RowType = row.RowType;
            //        table.Rows.RemoveAt(i);
            //        table.Rows.InsertAt(newRow, i - 2);
            //        break;
            //    }
            //}

            //for (int i = 0; i < table.Rows.Count; i++)
            //{
            //    if ((table.Rows[i] as ReportDataSet.TaxCalculationDetailsRow).Header == "Yearly Income Forecast")
            //    {
            //        ReportDataSet.TaxCalculationDetailsRow row = table.Rows[i] as ReportDataSet.TaxCalculationDetailsRow;
            //        ReportDataSet.TaxCalculationDetailsRow newRow = table.NewTaxCalculationDetailsRow();
            //        newRow.Header = row.Header;
            //        newRow.Amount = row.Amount;
            //        newRow.Note = row.Note;
            //        newRow.RowType = row.RowType;
            //        table.Rows.RemoveAt(i);
            //        table.Rows.InsertAt(newRow, i - 2);
            //        break;
            //    }
            //}

           

            return table;

        }

        protected void LoadReport()
        {

            if (!IsPostBack)
                return;


            PayrollPeriod payrollPeriod = CommonManager.GetPayrollPeriod(report.Filter.StartDate.Month,
                                                                         report.Filter.StartDate.Year);
            if( payrollPeriod ==null)
                return;

            //if (report.Filter.EmployeeId == 0)
            //{
            //    Utils.Helper.JavascriptHelper.DisplayClientMsg("Employee selection is required.", this);
            //    return;
            //}

            // Hide payslip if Hidden settings exists
            // Hide payslip if Hidden settings exists
            if (CommonManager.GetLastPayrollPeriod().PayrollPeriodId == payrollPeriod.PayrollPeriodId
                && CommonManager.Setting.HidePaySlip != null && CommonManager.Setting.HidePaySlip.Value)
            {
                return;
            }

            if (CommonManager.Setting.HideAllMonthPayslip != null && CommonManager.Setting.HideAllMonthPayslip.Value)
            {
                return;
            }



            Report.Templates.Pay.Detail.ReportTaxCalculationDetails mainReport = new Report.Templates.Pay.Detail.ReportTaxCalculationDetails();


            if (CommonManager.CompanySetting.IsD2 == false)
            {
                mainReport.labelTitle.Text += string.Format(" for {0} ({1} {2})", payrollPeriod.Name,
                                                           DateHelper.GetMonthsForOtherPayrollPeriod(payrollPeriod.Month,
                                                                                                     IsEnglish)
                                                                                                     , DateManager.GetOtherYearForPayrollPeriod(payrollPeriod, IsEnglish));
               
            }
            else
            {
                mainReport.labelTitle.Text += string.Format(" for {0} ({1} {2})", payrollPeriod.Name,
                                                         DateHelper.GetMonthsForOtherPayrollPeriod(payrollPeriod.Month,
                                                                                                   IsEnglish)
                                                                                                   , DateManager.GetOtherYearForPayrollPeriod(payrollPeriod, IsEnglish));
            }

            string data = CalculationManager.GetSalaryCalcForTaxDetails(SessionManager.CurrentLoggedInEmployeeId,
                SessionManager.CurrentCompanyId, payrollPeriod.PayrollPeriodId);

            EEmployee employee = EmployeeManager.GetEmployeeById(SessionManager.CurrentLoggedInEmployeeId);

            string sex="";
            if( employee.Gender==0)
                sex="Female";
            else if(employee.Gender ==1 )   
                sex="Male";
            else
                sex = "Third Gender";

            // Append additional information
            data =  string.Format(";Sex::{0}:2", sex) +
                    string.Format(";Tax Status (M/S)::{0}:2", employee.MaritalStatus) + data;

            // replace CIT name in tax details with other Retirement fundname
            if (!string.IsNullOrEmpty(SessionManager.CurrentCompany.OtherFundAbbreviation))
            {
                string name = SessionManager.CurrentCompany.OtherFundAbbreviation;
                data = data.Replace("CIT Deduction", name + " Deduction");
                data = data.Replace("CIT Settlement", name + " Settlement");
                data = data.Replace("Provisional CIT", "Provisional " + name);
                data = data.Replace("Sum of CIT And PF", "Sum of " + name + " And PF");
            }

            ReportDataSet.TaxCalculationDetailsDataTable mainTable = GetDataTable(data, employee);


            //mainReport.Month = payrollPeriod.Month;
            //mainReport.Year = payrollPeriod.Year.Value;
            //mainReport.IsIncome = true;
            //mainReport.CompanyId = SessionManager.CurrentCompanyId;
           
            mainReport.DataSource = mainTable;
            mainReport.DataMember = "Report";
           
            report.DisplayReport(mainReport);

            if(ReportHelper.IsReportExportState())
            {
                this.report.clearCache = true;
            }
        }
    }
}
