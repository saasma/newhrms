﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL.Entity;
using BLL;
using System.IO;
using BLL.BO;
using Utils.Helper;
using Bll;
using System.Text.RegularExpressions;

namespace Web.Employee
{
    public partial class AssignOvertimeEdit : BasePage
    {
        public bool HasImport
        {
            get
            {
                if (ViewState["import"] == null)
                    return false;
                return Convert.ToBoolean(ViewState["import"]);
            }
            set
            {
                ViewState["import"] = value;
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "sdf", "var hasImport = " + (this.HasImport ? "true;" : "false;"), true);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest && !IsPostBack)
            {
                Initialize();
            }
        }

        private void Initialize()
        {
            if (!string.IsNullOrEmpty(Request.QueryString["RId"]))
            {
                hdnRequestID.Text = Request.QueryString["RId"];
                LoadEditData();
            }
        }

        private void LoadEditData()
        {
            string hr;
            string min;
            string hrmin;
            string employeename;

            btnReject.Show();

            OvertimeRequest request = new OvertimeRequest();
            request = OvertimeManager.getRequestByID(hdnRequestID.Text);

            if (request != null)
            {
                gridHistory.GetStore().DataSource = AllowanceManager.GetAllowanceHistory(request.OvertimeRequestID, (int)RequestHistoryTypeEnum.Overtime);
                gridHistory.DataBind();
            }

            min = (request.StartTime.Value.Subtract(request.EndTime.Value).TotalMinutes).ToString();
            employeename = EmployeeManager.GetEmployeeById(request.EmployeeID.Value).Name;

            dfOvertime.Text += " " + employeename;

            dfDate.Text = request.Date.Value.ToShortDateString();
            dfName.Text = employeename;
            dfDescription.Text = request.Reason;

            DateTime? date = null;
            if (AttendanceManager.GetEmployeeCheckInOutTime(request.EmployeeID.Value, request.Date.Value, "CheckIn") != null)
            {
                date = AttendanceManager.GetEmployeeCheckInOutTime(request.EmployeeID.Value, request.Date.Value, "CheckIn");
                if (date != null)
                    dfInTime.Text = date.Value.ToShortTimeString();
            }

            if (AttendanceManager.GetEmployeeCheckInOutTime(request.EmployeeID.Value, request.Date.Value, "CheckOut") != null)
            {
                date = AttendanceManager.GetEmployeeCheckInOutTime(request.EmployeeID.Value, request.Date.Value, "CheckOut");
                if (date != null)
                    dfOutTime.Text = date.Value.ToShortTimeString();
            }


            int minute = Math.Abs(int.Parse(min));

            hrmin = new AttendanceManager().MinuteToHourMinuteConverter(minute);
            txtRequestedHour.Text = hrmin;
            txtRequestedHour.Disable();
            Hidden_MinutesPrev.Text = txtRequestedHour.Text;

            hr = hrmin.Split(':').First();
            min = hrmin.Split(':').Last();

            if (request.ApprovedTimeInMinute != null)
            {
                var span = System.TimeSpan.FromMinutes(request.ApprovedTimeInMinute.Value);
                var hours = ((int)span.TotalHours).ToString();
                var minutes = span.Minutes.ToString();

                txtApprovedForHour.Text = hours;
                txtApprovedForMin.Text = minutes;
            }
            else
            {
                txtApprovedForHour.Text = Regex.Match(hr, @"\d+").Value;
                txtApprovedForMin.Text = Regex.Match(min, @"\d+").Value;
            }

            if (request.Status == (int)OvertimeStatusEnum.Pending && request.RecommendedBy == SessionManager.CurrentLoggedInEmployeeId)
            {
            }
            else if ((request.Status == (int)OvertimeStatusEnum.Forwarded || request.Status == (int)OvertimeStatusEnum.Rejected)
                ||
                (request.RecommendedBy == SessionManager.CurrentLoggedInEmployeeId && request.ApprovalID != SessionManager.CurrentLoggedInEmployeeId)
                ||
                (request.Status == (int)OvertimeStatusEnum.Approved)
                )
            {

                pnlBorder.Show();
                lblWarningMsg.Text = "Status has been changed. This request cannot be edited.";
                lblWarningMsg.Show();

                txtApprovedForHour.Disable();
                txtApprovedForMin.Disable();
                btnApprove.Hide();
                btnReject.Hide();
            }
        }

        protected void btnApprove_Click(object sender, DirectEventArgs e)
        {
            Page.Validate("ApproveOvertime");
            if (Page.IsValid)
            {
                OvertimeRequest requestInstance = new OvertimeRequest();

                requestInstance.OvertimeRequestID = int.Parse(hdnRequestID.Text);
                requestInstance.ApprovedTimeInMinute = (int.Parse(txtApprovedForHour.Text) * 60) + int.Parse(txtApprovedForMin.Text);
                requestInstance.ApprovedTime = txtApprovedForHour.Text + " Hr : " + txtApprovedForMin.Text + " Min";
                requestInstance.Status = (int)OvertimeStatusEnum.Approved;

                List<RequestEditHistory> edits = new List<RequestEditHistory>();
                RequestEditHistory edit = new RequestEditHistory();
                if (Hidden_MinutesPrev.Text.Trim().ToLower() != requestInstance.ApprovedTimeInMinute.Value.ToString().Trim().ToLower())
                {
                    edit = new RequestEditHistory();
                    edit.Column1Before = Hidden_MinutesPrev.Text;
                    edit.Column1After = requestInstance.ApprovedTime;
                    edits.Add(edit);
                    Hidden_MinutesPrev.Value = requestInstance.ApprovedTime;
                }

                Status status = OvertimeManager.ApproveRequest(requestInstance, edits);
                if (status.IsSuccess)
                {
                    NewMessage.ShowNormalMessage("Overtime has been approved.", "closePopup()");
                }
                else
                {
                    NewMessage.ShowWarningMessage(status.ErrorMessage);
                }
            }
            else
            {
                NewMessage.ShowWarningMessage("Validation Failed");
            }
        }

        protected void btnRecommend_Click(object sender, DirectEventArgs e)
        {
            Page.Validate("ApproveOvertime");
            if (Page.IsValid)
            {
                OvertimeRequest requestInstance = new OvertimeRequest();

                requestInstance.OvertimeRequestID = int.Parse(hdnRequestID.Text);
                requestInstance.ApprovedTimeInMinute = (int.Parse(txtApprovedForHour.Text) * 60) + int.Parse(txtApprovedForMin.Text);
                requestInstance.ApprovedTime = txtApprovedForHour.Text + " Hr : " + txtApprovedForMin.Text + " Min";
                requestInstance.Status = (int)OvertimeStatusEnum.Recommended;

                List<RequestEditHistory> edits = new List<RequestEditHistory>();
                RequestEditHistory edit = new RequestEditHistory();
                if (Hidden_MinutesPrev.Text.Trim().ToLower() != requestInstance.ApprovedTimeInMinute.Value.ToString().Trim().ToLower())
                {
                    edit = new RequestEditHistory();
                    edit.Column1Before = Hidden_MinutesPrev.Text;
                    edit.Column1After = requestInstance.ApprovedTime;
                    edits.Add(edit);
                    Hidden_MinutesPrev.Value = requestInstance.ApprovedTime;
                }

                Status status = OvertimeManager.RecommendRequest(requestInstance, edits);
                if (status.IsSuccess)
                {
                    NewMessage.ShowNormalMessage("Overtime has been recommended.", "closePopup()");
                }
                else
                {
                    NewMessage.ShowWarningMessage(status.ErrorMessage);
                }
            }
            else
            {
                NewMessage.ShowWarningMessage("Validation Failed");
            }
        }

        protected void btnReject_Click(object sender, DirectEventArgs e)
        {
            int requestId = int.Parse(hdnRequestID.Text);
            bool isSuccess = OvertimeManager.RejectRequest(requestId);

            if (isSuccess)
            {
                NewMessage.ShowNormalMessage("Overtime has been rejected.", "closePopup()");
            }
        }

        private void HideButtons()
        {
            btnApprove.Hide();
            btnReject.Hide();
            HasImport = true;
        }


    }
}