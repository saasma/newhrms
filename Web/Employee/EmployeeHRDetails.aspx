<%@ Page Language="C#" Title="HR Details" EnableEventValidation="false" ValidateRequest="false"
    AutoEventWireup="true" MasterPageFile="~/Master/EmployeeMS.Master" CodeBehind="EmployeeHRDetails.aspx.cs"
    Inherits="Web.Employee.EmployeeHRDetails" %>

<%@ Register Src="~/UserControls/EmployeeAddressAndContact.ascx" TagName="EmpAdressContact"
    TagPrefix="uc7" %>
<%@ Register Src="~/UserControls/HR/HRDetailsCtl.ascx" TagName="hrdetailsctl" TagPrefix="uc2" %>
<%@ Register Src="~/UserControls/HR/MGEduction.ascx" TagName="MGEduction" TagPrefix="uc1" %>
<%@ Register Src="../UserControls/HR/MGTraining.ascx" TagName="MGTraining" TagPrefix="uc2" %>
<%@ Register Src="../UserControls/HR/MGActivities.ascx" TagName="MGActivities" TagPrefix="uc3" %>
<%@ Register Src="../UserControls/HR/MGFamily.ascx" TagName="MGFamily" TagPrefix="uc4" %>
<%@ Register Src="../UserControls/HR/MGDiscipline.ascx" TagName="MGDiscipline" TagPrefix="uc5" %>
<%@ Register Src="../UserControls/HR/MGEvaluation.ascx" TagName="MGEvaluation" TagPrefix="uc6" %>
<%@ Register Src="../UserControls/HR/HRDetailsCtl.ascx" TagName="HRDetailsCtl" TagPrefix="uc7" %>
<%@ Register Src="../UserControls/HR/MGAccident.ascx" TagName="MGAccident" TagPrefix="uc8" %>
<%@ Register Src="../UserControls/HR/MGSpecialEvent.ascx" TagName="MGSpecialEvent"
    TagPrefix="uc9" %>
<%@ Register Src="../UserControls/HR/MGPreviousEmployment.ascx" TagName="MGPreviousEmployment"
    TagPrefix="uc10" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="InfoMsgCtl" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../_assets1/themes/yui/tabs.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">

        function castePopupCall() {
            var ret = castePopup("isPopup=true");
            if (typeof (ret) != 'undefined') {
                ChangeDropDownList('<%= ddlCastes.ClientID %>', ret);
            }
        }

        function addSkillSetToEmployeePopup() {
            var employeeId = (typeof (EmployeeId) == 'undefined' ? 0 : parseInt(EmployeeId));
            var ret = addSkillSetToEmployee("isPopup=true&EmployeeId=" + employeeId);
        }
        function skillSetPopupCall() {
            var ret = skillSetPopup("isPopup=true");

        }
        function onChangeZone(ddl) {
            var ddlDistrictId = ddl.id.replace("Zones", "Districts");
            Web.PayrollService.GetDistrictByZone(ddl.value, onChangeZoneCallback, null, ddlDistrictId);
        }

        function onChangeZoneCallback(result, ddlDistrictId) {


            var list = result;
            document.getElementById(ddlDistrictId).options.length = 1;
            for (i = 0; i < list.length; i++) {
                var menu = list[i];
                var htmlContent = String.format("<option value='{0}'>{1}</option>", menu.Key, menu.Value);
                $('#' + ddlDistrictId).append(htmlContent);
            }

        }

        function ReloadEmployeeSkillSet(popupWindow, values) {
            popupWindow.close();
            if (typeof (values) != 'undefined') {
                document.getElementById('<%= divInnerSkillSet.ClientID %>').innerHTML = values;
            }
        }

        function parentReloadCallbackFunction1(popupWindow, values) {
            popupWindow.close();
            if (typeof (values) != 'undefined') {
                ChangeDropDownList('<%= ddlCastes.ClientID %>', values);
            }
        }
        var tab = '<%= tab.ClientID %>';
        //displays validation message for tab content
        function setTabFocus(valId, ctl, errormsg) {
            var id = valId.substr(valId.length - 1, 1);
            var tabCtl = $find(tab);
            if (isNaN(parseInt(id)) == false) {
                tabCtl.set_activeTabIndex(parseInt(id));
            }
            if (typeof (errormsg) != 'undefined') {
                alert(errormsg);

                if ($('#' + ctl).is(":visible"))
                    document.getElementById(ctl).focus();
            }
        }
    </script>
    <style type="text/css">
        .hrTabFieldset legend
        {
            font-size: 13px;
        }
        .hrTabFieldset
        {
            padding: 10px;
        }
        .hrTabFieldset a
        {
            margin-top: 10px;
        }
        .tableLightColor td
        {
            overflow: hidden;
            width: 100px;
        }
        .tableLightColor th
        {
            width: 100px;
        }
        .tableLightColor th
        {
            text-align: left;
        }
        #containerDIV td
        {
            vertical-align: top !important;
            padding: 2px !important;
            height: 10px !important;
            margin: 2px !important;
        }
        #containerDIV tr
        {
            padding: 0px !important;
            height: 10px !important;
            margin: 0px !important;
        }
        .rightContents
        {
            width: 560px !important;
        }
        .skillsetList li
        {
            padding-bottom: 3px;
        }
        #ctl00_mainContent_ctl03_tabHRDetails_body
        {
            height: 375px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
    <asp:Panel ID="container" runat="server" DefaultButton="Button1">
        <div class="contentArea">
            <uc2:InfoMsgCtl runat="server" Hide="true" EnableViewState="false" ID="msgInfo" />
            <div class="leftContents" style="width: 300px">
                <div class="content_title">
                    Title</div>
                <div class="content_box" style='height: 370px'>
                    <table cellpadding="0" cellspacing="0" width="400px" class="hr">
                        <tr>
                            <td class="fieldHeader" width="100px">
                                <label class="emp_label">
                                    Name</label>
                            </td>
                            <td>
                                <asp:TextBox Enabled="false" Style="padding-left: 3px" ID="txtName" Width="200px"
                                    runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="fieldHeader">
                                <label class="emp_label">
                                    Blood group</label>
                            </td>
                            <td>
                                <asp:DropDownList AppendDataBoundItems="true" Width="203px" ID="ddlBloodGroups" DataTextField="BloodGroupName"
                                    DataValueField="Id" runat="server">
                                    <asp:ListItem Text="--Select blood group--" Value="-1"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr runat="server" id="rowCaste">
                            <td class="fieldHeader">
                                <label class="emp_label">
                                    Caste/Group</label>
                            </td>
                            <td>
                                <asp:DropDownList AppendDataBoundItems="true" Width="203px" EnableViewState="false"
                                    ID="ddlCastes" DataTextField="CasteName" DataValueField="CasteId" runat="server">
                                    <asp:ListItem Text="--Select caste--" Value="-1"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <%--<td style="width: 200px; vertical-align: top">
                                <input onclick="castePopupCall()" id="btnAddCaste" runat="server" type="button" class="browse" />
                            </td>--%>
                        </tr>
                        <tr>
                            <td class="fieldHeader">
                                <label class="emp_label">
                                    Qualification</label>
                            </td>
                            <td>
                                <asp:DropDownList AppendDataBoundItems="true" Width="203px" ID="ddlQualification"
                                    DataTextField="Name" DataValueField="QualificationId" runat="server">
                                    <asp:ListItem Text="--Select qualification--" Value="-1"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="fieldHeader" valign="top">
                                <label class="emp_label">
                                    Skill Sets</label>
                            </td>
                            <td>
                                <div id='divSkillSet' runat="server" style="overflow-y: scroll; height: 138px; width: 200px;
                                    border: 1px solid #C1D6F3; padding: 0px">
                                    <ul id="divInnerSkillSet" runat="server" style="margin-left: -18px; margin-top: 4px"
                                        class="skillsetList">
                                    </ul>
                                </div>
                            </td>
                            <td style="width: 200px; vertical-align: top">
                                <input onclick="addSkillSetToEmployeePopup()" id="btnAddEmployeeSkillSet" title="Add Skill Set"
                                    runat="server" type="button" class="browse" />
                            </td>
                        </tr>
                        <tr>
                            <td class="fieldHeader">
                                <label class="emp_label" valign="top">
                                    Languages</label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtLanguages" Width="200px" TextMode="MultiLine" Rows="2" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="fieldHeader">
                                <label class="emp_label" valign="top">
                                    Date of Birth</label>
                            </td>
                            <td>
                                <My:Calendar Id="calDOB" runat="server" />
                            </td>
                            <td>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:RadioButton ID="dobBS" runat="server" Width="40px" Text="BS" GroupName="DOB"
                                                AutoPostBack="true" OnCheckedChanged="dob_Changed" />
                                        </td>
                                        <td>
                                            <asp:RadioButton ID="dobAD" runat="server" Width="40px" Text="AD" GroupName="DOB"
                                                AutoPostBack="true" OnCheckedChanged="dob_Changed" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="rightContents">
                <uc2:hrdetailsctl ID="ucHRDetails" DivHeight="377" IsEmployeePage="true" runat="server" />
            </div>
        </div>
        <div style="clear: both">
        </div>
        <p>
        </p>
        <p>
        </p>
        <cc1:TabContainer ID="tab" runat="server" Style="margin-left: 11px; margin-right: 11px"
            CssClass="yui" ActiveTabIndex="0">
            <cc1:TabPanel runat="server" ID="tabAddress1">
                <HeaderTemplate>
                    <asp:Label runat="server" ID="tabHeader1" Text="Address & Contact info" />
                </HeaderTemplate>
                <ContentTemplate>
                    <uc7:EmpAdressContact ID="empAddressAndContact" runat="server" />
                    <br style='clear: both' />
                </ContentTemplate>
            </cc1:TabPanel>
            <cc1:TabPanel runat="server" ID="tabEmpEducation">
                <HeaderTemplate>
                    <asp:Label runat="server" ID="tabHeader2" Text=" Education" />
                </HeaderTemplate>
                <ContentTemplate>
                    <uc1:MGEduction ID="ucEducation" runat="server" />
                </ContentTemplate>
            </cc1:TabPanel>
            <cc1:TabPanel runat="server" ID="tabEmpTraining">
                <HeaderTemplate>
                    <asp:Label runat="server" ID="tabHeader3" Text="Training" />
                </HeaderTemplate>
                <ContentTemplate>
                    <uc2:MGTraining ID="ucTraining" runat="server" />
                </ContentTemplate>
            </cc1:TabPanel>
            <cc1:TabPanel runat="server" ID="tabEmpActivities">
                <HeaderTemplate>
                    <asp:Label runat="server" ID="tabHeader4" Text="Extra activities" />
                </HeaderTemplate>
                <ContentTemplate>
                    <uc3:MGActivities ID="ucActivity" runat="server" />
                </ContentTemplate>
            </cc1:TabPanel>
            <cc1:TabPanel runat="server" ID="tabEmpFamily">
                <HeaderTemplate>
                    <asp:Label runat="server" ID="tabHeader5" Text="Family" />
                </HeaderTemplate>
                <ContentTemplate>
                    <uc4:MGFamily ID="ucFamily" runat="server" />
                </ContentTemplate>
            </cc1:TabPanel>
            <cc1:TabPanel runat="server" ID="tabEmpDiscipline">
                <HeaderTemplate>
                    <asp:Label runat="server" ID="tabHeader6" Text="Disciplinary action" />
                </HeaderTemplate>
                <ContentTemplate>
                    <uc5:MGDiscipline ID="ucDiscipline" runat="server" />
                </ContentTemplate>
            </cc1:TabPanel>
            <%-- <cc1:TabPanel runat="server" ID="tabEmpEvaluation">
            <HeaderTemplate>
                <asp:Label runat="server" ID="Label1" Text="Evaluation" />
            </HeaderTemplate>
            <ContentTemplate>
                <uc6:MGEvaluation ID="ucEvaluation" runat="server" />
            </ContentTemplate>
        </cc1:TabPanel>--%>
            <cc1:TabPanel runat="server" ID="tabEmpAccident">
                <HeaderTemplate>
                    <asp:Label runat="server" ID="Label2" Text="Accident" />
                </HeaderTemplate>
                <ContentTemplate>
                    <uc8:MGAccident ID="ucAccident" runat="server" />
                </ContentTemplate>
            </cc1:TabPanel>
            <cc1:TabPanel runat="server" ID="tabEmpSpecialEvent">
                <HeaderTemplate>
                    <asp:Label runat="server" ID="Label3" Text="Special events" />
                </HeaderTemplate>
                <ContentTemplate>
                    <uc9:MGSpecialEvent ID="ucSpecialEvent" runat="server" />
                </ContentTemplate>
            </cc1:TabPanel>
            <cc1:TabPanel runat="server" ID="tabPreviousEmployment">
                <HeaderTemplate>
                    <asp:Label runat="server" ID="Label4" Text="Previous employment" />
                </HeaderTemplate>
                <ContentTemplate>
                    <uc10:MGPreviousEmployment ID="ucPreviousEmployment" runat="server" />
                </ContentTemplate>
            </cc1:TabPanel>
        </cc1:TabContainer>
        <div class="buttonsDiv" style="width: 1175px; margin-right: 10px;">
            <asp:Button ID="Button1" Style="float: right!important" OnClientClick="hasTabControl = true;valGroup='AEEmployee';if( CheckValidation()) return true;;"
                runat="server" Text="Update" CssClass="save" ValidationGroup="AEEmployee" OnClick="btnSave_Click" />

                 <asp:Button ID="btnExport" Style="float: right!important" 
                runat="server" Text="Export" CssClass="update"  OnClick="btnExport_Click" />
        </div>
    </asp:Panel>
</asp:Content>
