﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using DAL;
using BLL.Base;
using Utils.Web;
using Utils.Helper;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Text;
using Utils.Calendar;
using Utils;
using BLL.BO;

namespace Web.Employee.TADA
{
    public partial class TADAClaimExcelPopup : BasePage
    {
        private bool? _IsEnglishDate = null;
        public bool ImportIsEnglishDate
        {
            get
            {
                return this.IsEnglish;
                //if (_IsEnglishDate != null)
                //    return _IsEnglishDate.Value;

                //_IsEnglishDate = Config.ImportDateIsEnglish;

                //if (_IsEnglishDate == null)
                //    _IsEnglishDate = IsEnglish;

                //return _IsEnglishDate.Value;

            }
        }
        private string _DateFormat = ""; 
        public string ImportDateFormat
        {
            get
            {
                if (_DateFormat != "")
                    return _DateFormat;
                _DateFormat = Config.ImportDateFormat;

                return _DateFormat;
            }
        }

        CommonManager mgr = new CommonManager();

        public bool HasImport
        {
            get
            {
                if (ViewState["import"] == null)
                    return false;
                return Convert.ToBoolean(ViewState["import"]);
            }
            set
            {
                ViewState["import"] = value;
            }
        }

        public void ProcessSectionLevelPermission()
        {
            if (SessionManager.IsCustomRole)
            {
                UUser user = UserManager.GetUserByUserName(Page.User.Identity.Name);
                int currentPageId = UserManager.GetPageIdByPageUrl("employee.aspx");

                bool permissionExists = false;
                List<UPermissionSection> sections = UserManager.GetAccessibleSectionsForPage(user.RoleId.Value, currentPageId);

                foreach (UPermissionSection section in sections)
                {
                    if (section.PageSectionId == 2)
                    {
                        permissionExists = true;
                    }
                }

                if (permissionExists == false)
                {
                    Response.Write("Not Enough Permission.");
                    Response.End();
                    return;
                }

            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {

            ProcessSectionLevelPermission();
            //Page.ClientScript.RegisterOnSubmitStatement(this.GetType(), "sdfdsffd", "window.onunload = null;");
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "sdf", "var hasImport = " + (this.HasImport ? "true;" : "false;"), true);
        }


        protected void btnExport_Click(object sender, EventArgs e)
        {

            int month = int.Parse(Request.QueryString["month"]);
            int year = int.Parse(Request.QueryString["year"]);

            int totalDays =  DateHelper.GetTotalDaysInTheMonth(year, month, false);

            List<DHTADAClaimLine> claimLine = new List<DHTADAClaimLine>();
            DHTADAClaimLine claimInst = new DHTADAClaimLine();
            for (int i = 1; i <= totalDays; i++)
            {
                claimInst = new DHTADAClaimLine();
                claimInst.Date = year + "/" + month + "/" + i;
               
                claimInst.LineDate = GetEngDate(claimInst.Date);
                
                claimInst = TADAManager.FillClaimDataForExcel(claimInst, month, year, SessionManager.CurrentLoggedInEmployeeId);
                claimInst.Date = DateHelper.GetMonthName(month,IsEnglish) + " - " +  i.ToString();

                /*
                claimInst.Description = "";
                claimInst.LocationFrom = "";
                claimInst.LocationTo = "";
                claimInst.RequestedAllowance1 = 0;
                claimInst.RequestedAllowance2 = 0;
                claimInst.RequestedAllowance3 = 0;
                claimInst.RequestedAllowance4 = 0;
                claimInst.RequestedAllowance5 = 0;
                claimInst.RequestedAllowance6 = 0;
                claimInst.RequestedAllowance7 = 0;
                claimInst.RequestedAllowance8 = 0;
                claimInst.Total = 0;
                 */

                claimLine.Add(claimInst);
            }

              

            string template = ("~/App_Data/ExcelTemplate/TADAClaim.xlsx");

            
           ExcelGenerator.WriteTADAClaimExportExcel(template,claimLine);

        }

        public EEmployee SetImportError(string msg, EEmployee emp,int row)
        {
            emp.IsValidImport = false;
            emp.ImportErrorMsg += "<br>" + msg + " for the employee \"" + (emp.Name == null ? "" : emp.Name) + "\" for the row " + (row+1) + ".";
            return emp;
        }

        public EEmployee SetImportErrorWithoutAppend(string msg, EEmployee emp, int row)
        {
            emp.IsValidImport = false;
            emp.ImportErrorMsg += "<br>" + msg + "\" for the row " + (row + 1) + ".";
            return emp;
        }

        public string GetAvailableStatuses(List<KeyValue> statusList)
        {
            string str = "";
            foreach (KeyValue val in statusList)
            {
                if (str == "")
                    str = val.Value;
                else
                    str += ", " + val.Value;
            }
            return str;
        }


      



        protected void btnImport_Click(object sender, EventArgs e)
        {
            string template = Server.MapPath("~/App_Data/ExcelTemplate/" + Guid.NewGuid() + ".xls");


            if (fupUpload.HasFile)// && fupUpload.FileName.ToLower().Contains(".xls"))
            {
                fupUpload.SaveAs(template);



                try
                {
                    DataTable datatable = ExcelGenerator.ImportFromExcel(template, "select * from [TADAClaim$]");

                    if (datatable == null)
                    {
                        divWarningMsg.InnerHtml = Resources.Messages.HPL_Allowance_ImportFail;
                        divWarningMsg.Hide = false;
                        File.Delete(template);
                        return;
                    }

                    List<DHTADAClaimLine> claimLine = new List<DHTADAClaimLine>();
                    DHTADAClaimLine claimInst = new DHTADAClaimLine();
                    DHTADAClaim claim = new DHTADAClaim();
                    decimal amount;

                    claim.Month = int.Parse(Request.QueryString["month"]);
                    claim.Year = int.Parse(Request.QueryString["year"]);

                    DateTime startDate = new CustomDate(1, claim.Month, claim.Year, IsEnglish).EnglishDate;
                    CustomDate endCustomDate = new CustomDate(DateHelper.GetTotalDaysInTheMonth(claim.Year, claim.Month, IsEnglish),
                        claim.Month, claim.Year, IsEnglish);
                    DateTime endDate = endCustomDate.EnglishDate;

                    decimal total=0;
                    int rowIndex  =1;
                    foreach (DataRow row in datatable.Rows)
                    {
                        claimInst = new DHTADAClaimLine();
                        DateTime date;
                        ++rowIndex;
                        //claimInst.Date = row[0].ToString();
                        if (DateTime.TryParse(row[1].ToString(),out date)==false)
                        {
                            divWarningMsg.InnerHtml = "Invalid date \"" + row[1].ToString() + "\" in the row " + (rowIndex);
                            divWarningMsg.Hide = false;
                            return;
                        }
                        claimInst.LineDate = DateTime.Parse(row[1].ToString());
                        claimInst.Date = BLL.BaseBiz.GetAppropriateDate(claimInst.LineDate);
                        claimInst.LocationFrom = row[2].ToString();
                        claimInst.LocationTo = row[3].ToString();
                        claimInst.Description = row[4].ToString();


                        if (claimInst.LineDate >= startDate && claimInst.LineDate <= endDate) { }
                        else
                        {
                            divWarningMsg.InnerHtml = "Date \""  +row[1].ToString()+ "\"" + " should lie between " +startDate.ToShortDateString() + " and " + endDate.ToShortDateString() + " for the row " + rowIndex + ".";
                            divWarningMsg.Hide = false;
                            return;
                        }

                        if (claimLine.Any(x => x.LineDate == claimInst.LineDate))
                        {
                            divWarningMsg.InnerHtml = "Data already contains for the date " + row[1].ToString()+ " for the row " + (rowIndex) + ".";
                            divWarningMsg.Hide = false;
                            return;
                        }

                        if (!string.IsNullOrEmpty(row[5].ToString()) && decimal.TryParse(row[5].ToString(),out amount) == false)
                        {
                            divWarningMsg.InnerHtml = "Invalid amount value in column \"Local Daily\" for the row " + (rowIndex) + ".";
                            divWarningMsg.Hide = false;
                            return;
                        }
                        if (!string.IsNullOrEmpty(row[6].ToString()) && decimal.TryParse(row[6].ToString(), out amount) == false)
                        {
                            divWarningMsg.InnerHtml = "Invalid amount value in column \"UpCountry Daily\" for the row " + (rowIndex) + ".";
                            divWarningMsg.Hide = false;
                            return;
                        }
                        if (!string.IsNullOrEmpty(row[7].ToString()) && decimal.TryParse(row[7].ToString(), out amount) == false)
                        {
                            divWarningMsg.InnerHtml = "Invalid amount value in column \"Fooding\" for the row " + (rowIndex) + ".";
                            divWarningMsg.Hide = false;
                            return;
                        }
                        if (!string.IsNullOrEmpty(row[8].ToString()) && decimal.TryParse(row[8].ToString(), out amount) == false)
                        {
                            divWarningMsg.InnerHtml = "Invalid amount value in column \"Transportation\" for the row " + (rowIndex) + ".";
                            divWarningMsg.Hide = false;
                            return;
                        }
                        if (!string.IsNullOrEmpty(row[9].ToString()) && decimal.TryParse(row[9].ToString(), out amount) == false)
                        {
                            divWarningMsg.InnerHtml = "Invalid amount value in column \"Feul\" for the row " + (rowIndex) + ".";
                            divWarningMsg.Hide = false;
                            return;
                        }
                        if (!string.IsNullOrEmpty(row[10].ToString()) && decimal.TryParse(row[10].ToString(), out amount) == false)
                        {
                            divWarningMsg.InnerHtml = "Invalid amount value in column \"Maintainence\" for the row " + (rowIndex) + ".";
                            divWarningMsg.Hide = false;
                            return;
                        }
                        if (!string.IsNullOrEmpty(row[11].ToString()) && decimal.TryParse(row[11].ToString(), out amount) == false)
                        {
                            divWarningMsg.InnerHtml = "Invalid amount value in column \"Communication\" for the row " + (rowIndex) + ".";
                            divWarningMsg.Hide = false;
                            return;
                        }
                        if (!string.IsNullOrEmpty(row[12].ToString()) && decimal.TryParse(row[12].ToString(), out amount) == false)
                        {
                            divWarningMsg.InnerHtml = "Invalid amount value in column \"Other Allowance\" for the row " + (rowIndex) + ".";
                            divWarningMsg.Hide = false;
                            return;
                        }

                        claimInst.RequestedAllowance1 = row[5].ToString() == "" ? 0 : decimal.Parse(row[5].ToString());
                        claimInst.RequestedAllowance2 = row[6].ToString() == "" ? 0 : decimal.Parse(row[6].ToString());
                        claimInst.RequestedAllowance3 = row[7].ToString() == "" ? 0 : decimal.Parse(row[7].ToString());
                        claimInst.RequestedAllowance4 = row[8].ToString() == "" ? 0 : decimal.Parse(row[8].ToString());
                        claimInst.RequestedAllowance5 = row[9].ToString() == "" ? 0 : decimal.Parse(row[9].ToString());
                        claimInst.RequestedAllowance6 = row[10].ToString() == "" ? 0 : decimal.Parse(row[10].ToString());
                        claimInst.RequestedAllowance7 = row[11].ToString() == "" ? 0 : decimal.Parse(row[11].ToString());
                        claimInst.RequestedAllowance8 = row[12].ToString() == "" ? 0 : decimal.Parse(row[12].ToString());

                        claimInst.Allowance1 = claimInst.RequestedAllowance1;
                        claimInst.Allowance2 = claimInst.RequestedAllowance2;
                        claimInst.Allowance3 = claimInst.RequestedAllowance3;
                        claimInst.Allowance4 = claimInst.RequestedAllowance4;
                        claimInst.Allowance5 = claimInst.RequestedAllowance5;
                        claimInst.Allowance6 = claimInst.RequestedAllowance6;
                        claimInst.Allowance7 = claimInst.RequestedAllowance7;
                        claimInst.Allowance8 = claimInst.RequestedAllowance8;


                        claimInst.Total = claimInst.RequestedAllowance1.Value + claimInst.RequestedAllowance2.Value + claimInst.RequestedAllowance3.Value +
                                      claimInst.RequestedAllowance4.Value + claimInst.RequestedAllowance5.Value + claimInst.RequestedAllowance6.Value +
                                      claimInst.RequestedAllowance7.Value + claimInst.RequestedAllowance8.Value;
                        total = total + claimInst.Total.Value;

                        claimLine.Add(claimInst);
                    }


                    claim.EmployeeID = SessionManager.CurrentLoggedInEmployeeId;
                    claim.Date = CommonManager.GetCurrentDateAndTime();
                    
                    claim.CreatedBy = SessionManager.CurrentLoggedInUserID;
                    claim.CreatedOn = CommonManager.GetCurrentDateAndTime();
                    claim.Status = (int)TADAStatusEnum.Saved;
                    claim.RequestedTotal = total;
                    claim.Total = total;
                    claim.DHTADAClaimLines.AddRange(claimLine);
                    Status status = TADAManager.InsertTADAFromExcel(claim);

                    if (status.IsSuccess==false)
                    {
                        divWarningMsg.InnerHtml = status.ErrorMessage;
                        divWarningMsg.Hide = false;
                    }
                    else
                    {
                        this.HasImport = true;

                        divMsgCtl.InnerHtml = string.Format("TADA Imported for given Month");
                        divMsgCtl.Hide = false;
                    }
                }

                catch (Exception exp)
                {
                    //divMsgCtl.Text = exp.ToString();
                    divWarningMsg.InnerHtml = "Error occured : Please confirm any columns has not been changed or sheet name has not been changed after export.";
                    divWarningMsg.Hide = false;
                    Log.log("TADA saving error for " + SessionManager.UserName, exp);

                }


            }


        }

    }
}

   


