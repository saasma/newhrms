﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using Utils.Helper;
using Utils.Web;
using BLL.Base;
using DAL;
using Utils.Calendar;
using BLL.BO;

namespace Web.Employee
{
    public partial class TADAList : BasePage
    {


        private int _tempCurrentPage;
        private int _tempCount;
        private PayManager payMgr = new PayManager();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Initialise();
            }
       
        }

     

        void Initialise()
        {
            List<KeyValue> statues = new JobStatus().GetMembers();
            LoadEmployees();
        }
        protected void btnLoad_Click(object sender, EventArgs e)
        {
        
            LoadEmployees();
        }
        protected void gvwEmployees_RowCreated(object sender, GridViewRowEventArgs e)
        {

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover",
                "this.originalClass=this.className;this.className='selected'");

                e.Row.Attributes.Add("onmouseout",
                "this.className=this.originalClass;");


            }

        }
        public void LoadEmployees()
        {
           


            gvw.DataSource = TADAManager.GetSelfClaimList();
            gvw.DataBind();

        }

        public void LoadGridRemotely()
        {
            gvw.DataSource = TADAManager.GetSelfClaimList();
            gvw.DataBind();
        }


        public string GetEngOrNepDate(object value)
        {
            if (value == null)
                return "";

            DateTime date = Convert.ToDateTime(value);

            CustomDate c = new CustomDate(date.Day, date.Month, date.Year, true);

            if (IsEnglish)
                return c.ToString();

            else
                return CustomDate.ConvertEngToNep(c).ToString();
        }

       
        protected void btnExport_Click(object sender, EventArgs e)
        {
            GridViewExportUtil.Export("TADA List.xls", gvw);
        }

        //protected void btnPostToSalary_Click(object sender, EventArgs e)
        //{

        //    foreach (GridViewRow row in gvw.Rows)
        //    {


        //        int payrollPeriodId = (int)gvw.DataKeys[row.RowIndex]["PayrollPeriodId"];

        //        PayManager.PostOvertimePay(payrollPeriodId);
        //        msgCtl.InnerHtml = Resources.Messages.OvertimePostedMessage;
        //        msgCtl.Hide = false;
        //        break;
        //    }


        //}
        protected void txtEmployeeName_TextChanged(object sender, EventArgs e)
        {
            

        }

        protected void gvw_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "DeleteData")
            {
                int requestId = int.Parse(e.CommandArgument.ToString());

                Status status = TADAManager.DeleteTADA(requestId);

                if (status.IsSuccess)
                {
                    LoadEmployees();
                }
                else
                {
                    NewMessage.ShowWarningMessage(status.ErrorMessage);
                }
            }
        }

      
        public bool IsDeleteVisible(object status)
        {
            TADAStatusEnum stat = (TADAStatusEnum)Convert.ToInt32(status);
            if (stat == TADAStatusEnum.Saved)
                return true;

            return false;
        }
       

    }
}
