﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using System.Text;
using BLL.Manager;
using Utils.Helper;
using DAL;
using System.Drawing;
using BLL.Base;
using Ext.Net;
using Utils;
using System.IO;
using Utils.Calendar;
using BLL.BO;
using Utils.Web;


namespace Web.CP
{
    public partial class TADAClaim : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Initialise();
            }

            JavascriptHelper.AttachNonDialogPopUpCode(Page, "importPopup", "TADAClaimExcelPopup.aspx", 450, 500);
        }


        protected void btnImport_Click(object sender, DirectEventArgs e)
        {
            string month;
            string year;
            month = calMonth.SelectedDate.Month.ToString();
            year = calMonth.SelectedDate.Year.ToString();

            string url = "TADAClaimExcelPopup.aspx?month="+month+"&year="+year;
            string script = string.Format("window.open('" + url + "', 'Import', 'toolbar=no,directories=no,status=no,menubar=no,scrollbars=no,resizable=no,modal=yes,center=yes');");


            X.Js.AddScript(script);
        }


        void Initialise()
        {

            calMonth.IsEnglishCalendar = this.IsEnglish;
            calMonth.SelectTodayDate();

            storeAllowances.DataSource = new List<DHTADAClaimLine> { new DHTADAClaimLine{ Date="Total",IsTotal=true } };

            System.Web.UI.WebControls.ListItem itemRec = cmbRecommender.Items[0];
            System.Web.UI.WebControls.ListItem itemApr = cmbApproval.Items[0];

            List<TextValue> listReview = LeaveRequestManager.GetApplyToListForEmployee(SessionManager.CurrentLoggedInEmployeeId, false, PreDefindFlowType.Overtime);
            List<TextValue> listApproval = LeaveRequestManager.GetApplyToListForEmployee(SessionManager.CurrentLoggedInEmployeeId, true, PreDefindFlowType.Overtime);

            cmbRecommender.DataSource = listReview;
            cmbRecommender.DataBind();

            cmbApproval.DataSource = listApproval;
            cmbApproval.DataBind();

            cmbRecommender.Items.Insert(0, itemRec);
            cmbApproval.Items.Insert(0, itemApr);


            if (!string.IsNullOrEmpty(Request.QueryString["ID"]))
            {
                calMonth.Enabled = false;
                Hidden_QuerryStringID.Text = Request.QueryString["ID"];
                LoadEditData();
            }


           
        }

        private void LoadEditData()
        {


            DHTADAClaim loadRequest = new DHTADAClaim();
            loadRequest = TADAManager.GetTADAClaim(int.Parse(Hidden_QuerryStringID.Text));

            if (loadRequest.Status != (int)TADAStatusEnum.Saved)
                X.AddScript("isEditMode = true;");

            CustomDate date = new CustomDate(1, loadRequest.Month, loadRequest.Year, IsEnglish);
            calMonth.SetSelectedDate(date.ToString(), IsEnglish);
            UIHelper.SetSelectedInDropDown(cmbRecommender, loadRequest.RecommendedBy);
            UIHelper.SetSelectedInDropDown(cmbApproval, loadRequest.ApprovalID);


            if (loadRequest.Status != (int)TADAStatusEnum.Saved)
            {
                SetWarning(lblMsg, ((TADAStatusEnum)loadRequest.Status).ToString() +   " TADA can not be changed.");
                btnAddNewLine.Hide();
                btnSaveAndLater.Hide();
                btnSaveAndSend.Hide();
            }

            if (loadRequest.Status == (int)TADAStatusEnum.Rejected)
                LinkButton1.Visible = false;
            DisplayGrid(loadRequest);
            //cmbTravelBy.SelectedItem.Value = loadRequest.TravelBy.Value.ToString();
         


            //dispTotal.Text = GetCurrency(loadRequest.Total);




        }

        public void btnLoad_Click(object sender, DirectEventArgs e)
        {
            DHTADAClaim loadRequest = new DHTADAClaim();
            loadRequest = TADAManager.GetTADAClaim(SessionManager.CurrentLoggedInEmployeeId,
                calMonth.SelectedDate.Month, calMonth.SelectedDate.Year);
            if (loadRequest != null)
                DisplayGrid(loadRequest);

        }
        private void DisplayGrid(DHTADAClaim loadRequest)
        {

            List<DHTADAClaimLine> lines = loadRequest.DHTADAClaimLines.OrderBy(x => x.LineDate).ToList();
            DHTADAClaimLine total = new DHTADAClaimLine();
            total.Allowance1 = lines.Sum(x => Convert.ToDecimal(x.Allowance1));
            total.Allowance2 = lines.Sum(x => Convert.ToDecimal(x.Allowance2));
            total.Allowance3 = lines.Sum(x => Convert.ToDecimal(x.Allowance3));
            total.Allowance4 = lines.Sum(x => Convert.ToDecimal(x.Allowance4));
            total.Allowance5 = lines.Sum(x => Convert.ToDecimal(x.Allowance5));
            total.Allowance6 = lines.Sum(x => Convert.ToDecimal(x.Allowance6));
            total.Allowance7 = lines.Sum(x => Convert.ToDecimal(x.Allowance7));
            total.Allowance8 = lines.Sum(x => Convert.ToDecimal(x.Allowance8));
            total.Total = lines.Sum(x => Convert.ToDecimal(x.Total));
            total.IsTotal = true;
            total.Date = "Total";

            lines.Add(total);

            gridAllowances.Store[0].DataSource = lines;
            gridAllowances.Store[0].DataBind();
        }

        protected void Country_Change(object sender, DirectEventArgs e)
        {
            

        }

       

        //protected void LoadAllowanceGrid(int countryID)
        //{
        //    List<TARequestLine> lines = new List<TARequestLine>();
        //    int? RequestID = null;
        //    if (!string.IsNullOrEmpty(Hidden_QuerryStringID.Text))
        //    {
        //        RequestID = int.Parse(Hidden_QuerryStringID.Text);
        //    }
        //    lines = TravelAllowanceManager.getAllowanceByEmployeeID(SessionManager.CurrentLoggedInEmployeeId, RequestID, countryID).Where(x => x.UnitType == null).ToList();
        //    storeAllowances.DataSource = lines;
        //    storeAllowances.DataBind();

        //}


        protected void LoadEmployeeInfo()
        {
            int EmployeeID = SessionManager.CurrentLoggedInEmployeeId;

           
        }


        protected void ButtonNext_Click(object sender, DirectEventArgs e)
        {

            Page.Validate("InsertUpdate");
            if (Page.IsValid)
            {
                Ext.Net.Button btn = (Ext.Net.Button)sender;
                DHTADAClaim request = new DHTADAClaim();
                


                bool isEdit;
                isEdit = !string.IsNullOrEmpty(Hidden_QuerryStringID.Text);
                if (isEdit)
                {
                    request.RequestID = int.Parse(Hidden_QuerryStringID.Text);
                }
                request.Status = (int)TADAStatusEnum.Saved ;               
                if (sender == btnSaveAndSend)
                {
                    request.Status = (int)TADAStatusEnum.Pending;                   
                }
                request.EmployeeID = SessionManager.CurrentLoggedInEmployeeId;
                request.Month = calMonth.SelectedDate.Month;
                request.Year = calMonth.SelectedDate.Year;
                CustomDate customDate = new CustomDate(1,request.Month,request.Year,IsEnglish);
                request.Date = customDate.EnglishDate;
                request.ApprovalID = int.Parse(cmbApproval.SelectedItem.Value);
                request.ApprovalName = cmbApproval.SelectedItem.Text;
                request.RecommendedBy = int.Parse(cmbRecommender.SelectedItem.Value);

                DateTime startDate = request.Date;
                CustomDate endCustomDate = new CustomDate(DateHelper.GetTotalDaysInTheMonth(request.Year, request.Month, IsEnglish),
                    request.Month, request.Year, IsEnglish);
                DateTime endDate = endCustomDate.EnglishDate;

                List<DHTADAClaimLine> lines = new List<DHTADAClaimLine>();
                List<DHTADAClaimLine> actualLines = new List<DHTADAClaimLine>();

                string json = e.ExtraParams["Values"];
                if (string.IsNullOrEmpty(json))
                {
                    return;
                }

                lines = JSON.Deserialize<List<DHTADAClaimLine>>(json);


                int row =1;
                foreach (DHTADAClaimLine line in lines)
                {
                    if (line.IsTotal == false)
                    {
                        if (string.IsNullOrEmpty(line.Date))
                        {
                            SetError(lblMsg, "Date is required for the row " + (row) + ".");
                            return;
                        }
                        line.LineDate = GetEngDate(line.Date);

                        if (line.LineDate >= startDate && line.LineDate <= endDate) { }
                        else
                        {
                            SetError(lblMsg, line.Date + " should lie between " + customDate.ToString() + " and " + endCustomDate.ToString() + " for the row " + row + ".");
                            return;
                        }

                        if (actualLines.Any(x => x.LineDate == line.LineDate))
                        {
                            SetError(lblMsg, "Data already contains for the date " + line.Date + " for the row " + (row) + ".");
                            return;
                        }

                        line.RequestedAllowance1 = line.Allowance1;
                        line.RequestedAllowance2 = line.Allowance2;
                        line.RequestedAllowance3 = line.Allowance3;
                        line.RequestedAllowance4 = line.Allowance4;

                        line.RequestedAllowance5 = line.Allowance5;
                        line.RequestedAllowance6 = line.Allowance6;
                        line.RequestedAllowance7 = line.Allowance7;
                        line.RequestedAllowance8 = line.Allowance8;

                        actualLines.Add(line);
                    }
                    row += 1;
                }

                request.Total = actualLines.Sum(x => x.Total).Value;

                Status status = TADAManager.InsertUpdateTADA(request, actualLines);
                if (status.IsSuccess)
                {

                    Hidden_QuerryStringID.Text = request.RequestID.ToString();

                    if (request.Status != (int)TADAStatusEnum.Saved)
                    {
                        btnSaveAndLater.Hide();
                        btnSaveAndSend.Hide();
                    }

                   

                    {
                        if (request.Status == (int)TADAStatusEnum.Saved)
                            SetMessage(lblMsg, "TADA has been saved.");
                        else
                            SetMessage(lblMsg, "TADA has been saved & forwarded.");
                    }
                }
                else
                {
                    SetWarning(lblMsg, status.ErrorMessage);
                    
                }

            }
           


        }

    }
}

