﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using Utils.Helper;
using Utils.Web;
using BLL.Base;
using DAL;
using Utils.Calendar;
using BLL.BO;

namespace Web.Employee
{
    public partial class TADAReviewList : BasePage
    {


        private int _tempCurrentPage;
        private int _tempCount;
        private PayManager payMgr = new PayManager();


        protected void Page_PreRender(object sender, EventArgs e)
        {
            calMonth.Enabled = chkHasDate.Checked;

        }
        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (!IsPostBack)
            {
              
                Initialise();

                
            }
            JavascriptHelper.AttachNonDialogPopUpCode(Page, "assignovertimePopup", "OvertimeRequesterPopup.aspx", 475, 550);
            JavascriptHelper.AttachNonDialogPopUpCode(Page, "positionHistoryPopup", "AApproveOvertimePopup.aspx", 475, 500);

            JavascriptHelper.AttachEnableDisablingJSCode(chkHasDate, calMonth.ClientID + "_date", false);
            
        }

     

        void Initialise()
        {
            calMonth.IsEnglishCalendar = this.IsEnglish;
            calMonth.SelectTodayDate();

            calMonth.Enabled = chkHasDate.Checked;

            // if has Overtime approval permission then set default status filter to Approve
            if (LeaveAttendanceManager.IsEmployeeAllowedToApproveLeave())
            {
                ddlStatus.SelectedValue = "1";
            }

            List<KeyValue> statues = new JobStatus().GetMembers();
            LoadEmployees();
        }
        protected void btnLoad_Click(object sender, EventArgs e)
        {
        
            LoadEmployees();
        }
        protected void gvwEmployees_RowCreated(object sender, GridViewRowEventArgs e)
        {

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover",
                "this.originalClass=this.className;this.className='selected'");

                e.Row.Attributes.Add("onmouseout",
                "this.className=this.originalClass;");


            }

        }
        protected void LoadEmployees()
        {
            //int type=1;
            int status = -1;


           

            if (ddlStatus.SelectedItem != null)
                status = int.Parse(ddlStatus.SelectedValue);

            gvw.DataSource = TADAManager.GetTADAListForReview( status,chkHasDate.Checked,calMonth.SelectedDate.Month,calMonth.SelectedDate.Year);
            gvw.DataBind();

            
        }

        public string GetEngOrNepDate(object value)
        {
            if (value == null)
                return "";

            DateTime date = Convert.ToDateTime(value);

            CustomDate c = new CustomDate(date.Day, date.Month, date.Year, true);

            if (IsEnglish)
                return c.ToString();

            else
                return CustomDate.ConvertEngToNep(c).ToString();
        }

       
        protected void btnExport_Click(object sender, EventArgs e)
        {
            GridViewExportUtil.Export("TADAList.xls", gvw);
        }

        //protected void btnPostToSalary_Click(object sender, EventArgs e)
        //{

        //    foreach (GridViewRow row in gvw.Rows)
        //    {


        //        int payrollPeriodId = (int)gvw.DataKeys[row.RowIndex]["PayrollPeriodId"];

        //        PayManager.PostOvertimePay(payrollPeriodId);
        //        msgCtl.InnerHtml = Resources.Messages.OvertimePostedMessage;
        //        msgCtl.Hide = false;
        //        break;
        //    }


        //}
        protected void txtEmployeeName_TextChanged(object sender, EventArgs e)
        {
            

        }

      
       

    }
}
