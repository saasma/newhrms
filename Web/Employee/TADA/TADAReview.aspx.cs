﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using System.Text;
using BLL.Manager;
using Utils.Helper;
using DAL;
using System.Drawing;
using BLL.Base;
using Ext.Net;
using Utils;
using System.IO;
using Utils.Calendar;
using BLL.BO;
using Utils.Web;


namespace Web.CP
{
    public partial class TADAReview : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Initialise();
            }
        }


        void Initialise()
        {
            calMonth.IsEnglishCalendar = this.IsEnglish;
            calMonth.SelectTodayDate();

            storeAllowances.DataSource = new List<DHTADAClaimLine> { new DHTADAClaimLine { Date = "Total", IsTotal = true } };




            if (!string.IsNullOrEmpty(Request.QueryString["ID"]))
            {
                Hidden_QuerryStringID.Text = Request.QueryString["ID"];
                LoadEditData();
            }



        }

        private void LoadEditData()
        {


            DHTADAClaim loadRequest = new DHTADAClaim();
            loadRequest = TADAManager.GetTADAClaim(int.Parse(Hidden_QuerryStringID.Text));

            if (loadRequest.RecommendedBy != null)
            {
                EEmployee eemp = new EEmployee();
                eemp = new EmployeeManager().GetById(loadRequest.RecommendedBy.Value);

                if (eemp != null)
                    lblRecommender.Text = eemp.Name;
                else
                    lblRecommender.Text = "admin";
            }
            if (loadRequest.ApprovalID != null)
            {
                EEmployee eemp = new EEmployee();
                eemp = new EmployeeManager().GetById(loadRequest.ApprovalID.Value);

                if (eemp != null)
                    lblApproval.Text = eemp.Name;
                else
                    lblApproval.Text = "admin";
            }
            title.InnerHtml += new EmployeeManager().GetById(loadRequest.EmployeeID).Name;
            

            if (loadRequest.Status == (int)TADAStatusEnum.Pending)
            {
                foreach (DHTADAClaimLine item in loadRequest.DHTADAClaimLines)
                {
                    item.RequestedAllowance1 = item.Allowance1;
                    item.RequestedAllowance2 = item.Allowance2;
                    item.RequestedAllowance3 = item.Allowance3;
                    item.RequestedAllowance4 = item.Allowance4;

                    item.RequestedAllowance5 = item.Allowance5;
                    item.RequestedAllowance6 = item.Allowance6;
                    item.RequestedAllowance7 = item.Allowance7;
                    item.RequestedAllowance8 = item.Allowance8;
                }
            }

            CustomDate date = new CustomDate(1, loadRequest.Month, loadRequest.Year, IsEnglish);
            calMonth.SetSelectedDate(date.ToString(), IsEnglish);



            List<DHTADAClaimLine> lines = loadRequest.DHTADAClaimLines.OrderBy(x => x.LineDate).ToList();
            DHTADAClaimLine total = new DHTADAClaimLine();
            total.Allowance1 = lines.Sum(x => Convert.ToDecimal(x.Allowance1));
            total.Allowance2 = lines.Sum(x => Convert.ToDecimal(x.Allowance2));
            total.Allowance3 = lines.Sum(x => Convert.ToDecimal(x.Allowance3));
            total.Allowance4 = lines.Sum(x => Convert.ToDecimal(x.Allowance4));
            total.Allowance5 = lines.Sum(x => Convert.ToDecimal(x.Allowance5));
            total.Allowance6 = lines.Sum(x => Convert.ToDecimal(x.Allowance6));
            total.Allowance7 = lines.Sum(x => Convert.ToDecimal(x.Allowance7));
            total.Allowance8 = lines.Sum(x => Convert.ToDecimal(x.Allowance8));
            total.Total = lines.Sum(x => Convert.ToDecimal(x.Total));
            total.IsTotal = true;
            total.Date = "Total";

            lines.Add(total);

            gridAllowances.Store[0].DataSource = lines;
            gridAllowances.Store[0].DataBind();
            //cmbTravelBy.SelectedItem.Value = loadRequest.TravelBy.Value.ToString();



            if (loadRequest.Status == (int)TADAStatusEnum.Pending)
            {
                btnRecommend.Visible = true;
                btnApprove.Visible = false;
                btnReject.Visible = true;

                if (loadRequest.RecommendedBy == loadRequest.ApprovalID)
                {
                    btnRecommend.Visible = false;
                    btnApprove.Visible = true;
                }
            }
            else if (loadRequest.Status == (int)TADAStatusEnum.Recommended)
            {
                btnRecommend.Visible = false;
                btnApprove.Visible = true;
                btnReject.Visible = true;
            }

            if (loadRequest.Status == (int)TADAStatusEnum.Pending && loadRequest.RecommendedBy == SessionManager.CurrentLoggedInEmployeeId)
            {
            }
            else if ((loadRequest.Status == (int)TADAStatusEnum.Forwarded || loadRequest.Status == (int)TADAStatusEnum.Rejected)
                ||
                (loadRequest.RecommendedBy == SessionManager.CurrentLoggedInEmployeeId && loadRequest.ApprovalID != SessionManager.CurrentLoggedInEmployeeId)
                ||
                (loadRequest.Status == (int)TADAStatusEnum.Approved)
                )
            {

                SetMessage(lblMsg, "Status has been changed. This request cannot be edited.");


                btnApprove.Visible = false;
                btnRecommend.Visible = false;
                btnReject.Visible = false;

            }



        }

        protected void Country_Change(object sender, DirectEventArgs e)
        {


        }



        //protected void LoadAllowanceGrid(int countryID)
        //{
        //    List<TARequestLine> lines = new List<TARequestLine>();
        //    int? RequestID = null;
        //    if (!string.IsNullOrEmpty(Hidden_QuerryStringID.Text))
        //    {
        //        RequestID = int.Parse(Hidden_QuerryStringID.Text);
        //    }
        //    lines = TravelAllowanceManager.getAllowanceByEmployeeID(SessionManager.CurrentLoggedInEmployeeId, RequestID, countryID).Where(x => x.UnitType == null).ToList();
        //    storeAllowances.DataSource = lines;
        //    storeAllowances.DataBind();

        //}


        protected void LoadEmployeeInfo()
        {
            int EmployeeID = SessionManager.CurrentLoggedInEmployeeId;


        }


        protected void btnRecommend_Click(object sender, DirectEventArgs e)
        {

            Page.Validate("InsertUpdate");
            if (Page.IsValid)
            {
                Ext.Net.Button btn = (Ext.Net.Button)sender;
                DHTADAClaim request = new DHTADAClaim();





                request.RequestID = int.Parse(Hidden_QuerryStringID.Text);


                request.Status = (int)TADAStatusEnum.Recommended;
                if (sender == btnApprove)
                {
                    request.Status = (int)TADAStatusEnum.Approved;
                }

                request.Month = calMonth.SelectedDate.Month;
                request.Year = calMonth.SelectedDate.Year;
                CustomDate customDate = new CustomDate(1, request.Month, request.Year, IsEnglish);
                request.Date = customDate.EnglishDate;



                DateTime startDate = request.Date;
                CustomDate endCustomDate = new CustomDate(DateHelper.GetTotalDaysInTheMonth(request.Year, request.Month, IsEnglish),
                    request.Month, request.Year, IsEnglish);
                DateTime endDate = endCustomDate.EnglishDate;

                List<DHTADAClaimLine> lines = new List<DHTADAClaimLine>();
                List<DHTADAClaimLine> actualLines = new List<DHTADAClaimLine>();

                string json = e.ExtraParams["Values"];
                if (string.IsNullOrEmpty(json))
                {
                    return;
                }

                lines = JSON.Deserialize<List<DHTADAClaimLine>>(json);


                int row = 1;
                foreach (DHTADAClaimLine line in lines)
                {
                    if (line.IsTotal == false)
                    {
                        if (string.IsNullOrEmpty(line.Date))
                        {
                            SetError(lblMsg, "Date is required for the row " + (row) + ".");
                            return;
                        }
                        line.LineDate = GetEngDate(line.Date);

                        if (line.LineDate >= startDate && line.LineDate <= endDate) { }
                        else
                        {
                            SetError(lblMsg, line.Date + " should lie between " + customDate.ToString() + " and " + endCustomDate.ToString() + " for the row " + row + ".");
                            return;
                        }

                        actualLines.Add(line);
                    }
                    row += 1;
                }

                request.Total = actualLines.Sum(x => x.Total).Value;
                request.DHTADAClaimLines.AddRange(actualLines);

                Status status = TADAManager.RecommendApproveRequest(request);
                if (status.IsSuccess)
                {

                    Hidden_QuerryStringID.Text = request.RequestID.ToString();


                    if (request.Status == (int)TADAStatusEnum.Saved)
                        SetMessage(lblMsg, "TADA has been saved.");
                    else
                        SetMessage(lblMsg, "TADA has been saved & forwarded.");


                }
                else
                {
                    SetWarning(lblMsg, status.ErrorMessage);

                }

            }


            if (sender == btnRecommend)
                btnRecommend.Hide();
            else
                btnApprove.Hide();
            btnReject.Hide();
        }

        protected void btnReject_Click(object sender, DirectEventArgs e)
        {
            int requestID = int.Parse(Hidden_QuerryStringID.Text);

            Status status = TADAManager.ManagerRejectRequest(requestID);
            if (status.IsSuccess)
            {

                //Hidden_QuerryStringID.Text = request.RequestID.ToString();

                SetMessage(lblMsg, "TADA has been rejected.");

            }
            else
            {
                SetWarning(lblMsg, status.ErrorMessage);

            }

            btnApprove.Hide();
            btnReject.Hide();
            btnRecommend.Hide();
        }

    }
}




