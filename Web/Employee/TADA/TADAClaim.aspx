﻿<%@ Page Title="TADA Claim" Language="C#" MasterPageFile="~/Master/EmployeeExt.Master"
    AutoEventWireup="true" CodeBehind="TADAClaim.aspx.cs" Inherits="Web.CP.TADAClaim" %>

<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/PagingCtl.ascx" TagName="PagingCtl" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        
        function importPopupProcess(btn) {
            
            var month;
            var year;
            //month = 4;
            //year =2071;
            //month = document.getElementById('<%= calMonth.ClientID %>');
            var calMonth = '<%= calMonth.ClientID %>';
            month = parseInt(document.getElementById(calMonth + "_datem").value);
            year = parseInt(document.getElementById(calMonth + "_datey").value);

            var ret = importPopup("month=" + month +"&year="+year);

            return false;
        }

        var refreshWindow = function()
        {
            <%=btnLoad.ClientID %>.fireEvent('click');
        }

     
        function refreshGridViewForDateInGrid()
        {

        dateDataRecord.set('Date',currentlySelectedDate);
        currentlySelectedDate = "";
        dateDataRecord.commit();
        //dateDataRecord.commit();
        //.RequiredByDate = currentlySelectedDate;
       // dateDataRecord.commit();
            //Store_RequisitionSlipLine.reload();
        }

        var dateDataRecord = null;
        var afterEdit = function (e1, e2, e3, e4) {
            //if date column then assign date from here
            if (e2.field == "Date") {
                dateDataRecord = e2.record;
                isDateColumnChanged = true;
                //e2.record.data.RequiredByDate = currentlySelectedDate;
            }
           
            e2.record.commit();

            setTotal(e2.record);
            
        }

        var setTotal = function (currentRow) {
             
             var records = <%=gridAllowances.ClientID %>.getStore();
             var i = 0,
                length = records.data.length,
                total = 0,
                grandTotal = 0,
                record;

            var totalRecord =  records.data.items[records.data.items.length-1];
            totalRecord.data.Allowance1 = 0;
            totalRecord.data.Allowance2 = 0;
            totalRecord.data.Allowance3 = 0;
            totalRecord.data.Allowance4 = 0;
            totalRecord.data.Allowance5 = 0;
            totalRecord.data.Allowance6 = 0;
            totalRecord.data.Allowance7 = 0;
            totalRecord.data.Allowance8 = 0;

            for (; i < length; ++i) {
                record = records.data.items[i];
                
                if(record.data.IsTotal==false)
                {

                    totalRecord.data.Allowance1 +=  record.data.Allowance1;
                    totalRecord.data.Allowance2 +=  record.data.Allowance2;
                    totalRecord.data.Allowance3 +=  record.data.Allowance3;
                    totalRecord.data.Allowance4 +=  record.data.Allowance4;
                    totalRecord.data.Allowance5 +=  record.data.Allowance5;
                    totalRecord.data.Allowance6 +=  record.data.Allowance6;
                    totalRecord.data.Allowance7 +=  record.data.Allowance7;
                    totalRecord.data.Allowance8 +=  record.data.Allowance8;

                    total = record.data.Allowance1 + record.data.Allowance2+ record.data.Allowance3+ record.data.Allowance4
                            + record.data.Allowance5+ record.data.Allowance6+ record.data.Allowance7+ record.data.Allowance8;

                    record.data.Total = total;
                    grandTotal += total;
                }
            }
            
            
            totalRecord.data.Total = totalRecord.data.Allowance1 + totalRecord.data.Allowance2+ totalRecord.data.Allowance3+ totalRecord.data.Allowance4
                            + totalRecord.data.Allowance5+ totalRecord.data.Allowance6+ totalRecord.data.Allowance7+ totalRecord.data.Allowance8;

            totalRecord.commit();

            if(currentRow != null)
                currentRow.commit();

            total = getFormattedAmount(grandTotal);
          
        }
       
        var beforeEdit = function(e1,e) 
        {
            if(e.record.data.IsTotal==true)
            {
                 e.cancel = true;
                   return;
            }
         }

        var onDeleteCommand = function (column, command, record, recordIndex, cellIndex) {
            if(record.data.IsTotal==true)
                return;
            var store = this.grid.store;
            store.remove(record);
            setTotal(null);
        };

        var getFormattedAmount1= function(value,e1,e2,e3,e4)
        {
            var data = e2.data;
            value1 = getFormattedAmount(value);
            var returnValue="";

            if(parseFloat(value)==0)
                returnValue = "";
            else
                returnValue = value1;
            
           
            if(data.IsTotal==false && typeof(isEditMode) != 'undefined')
            {
                if(e1.columnIndex == 4)
                {
                    if(data.Allowance1 != data.RequestedAllowance1)
                        return '<span class="changeClass" title="Requested Amount : ' + getFormattedAmount(data.RequestedAllowance1) + '">' + (returnValue=='' ? '0' : returnValue) +'</span>';
                }
                else if(e1.columnIndex == 5)
                {
                    if(data.Allowance2 != data.RequestedAllowance2)
                        return '<span class="changeClass" title="Requested Amount : ' + getFormattedAmount(data.RequestedAllowance2) + '">' + (returnValue=='' ? '0' : returnValue) +'</span>';
                }
                else if(e1.columnIndex == 6)
                {
                    if(data.Allowance3 != data.RequestedAllowance3)
                        return '<span class="changeClass" title="Requested Amount : ' + getFormattedAmount(data.RequestedAllowance3) + '">' + (returnValue=='' ? '0' : returnValue) +'</span>';
                }
                else if(e1.columnIndex == 7)
                {
                    if(data.Allowance4 != data.RequestedAllowance4)
                        return '<span class="changeClass" title="Requested Amount : ' + getFormattedAmount(data.RequestedAllowance4) + '">' + (returnValue=='' ? '0' : returnValue) +'</span>';
                }
                else if(e1.columnIndex == 8)
                {
                    if(data.Allowance5 != data.RequestedAllowance5)
                        return '<span class="changeClass" title="Requested Amount : ' + getFormattedAmount(data.RequestedAllowance5) + '">' + (returnValue=='' ? '0' : returnValue) +'</span>';
                }
                else if(e1.columnIndex == 9)
                {
                    if(data.Allowance6 != data.RequestedAllowance6)
                        return '<span class="changeClass" title="Requested Amount : ' + getFormattedAmount(data.RequestedAllowance6) + '">' + (returnValue=='' ? '0' : returnValue) +'</span>';
                }
                else if(e1.columnIndex == 10)
                {
                    if(data.Allowance7 != data.RequestedAllowance7)
                        return '<span class="changeClass" title="Requested Amount : ' + getFormattedAmount(data.RequestedAllowance7) + '">' + (returnValue=='' ? '0' : returnValue) +'</span>';
                }
                else if(e1.columnIndex == 11)
                {
                    if(data.Allowance8 != data.RequestedAllowance8)
                        return '<span class="changeClass" title="Requested Amount : ' + getFormattedAmount(data.RequestedAllowance8) + '">' + (returnValue=='' ? '0' : returnValue) +'</span>';
                }
            }
            return returnValue;
        }
    </script>
    <style type="text/css">
        .bodypart
        {
            margin: 0 auto !important;
        }
        .tableTD > tbody > tr > td
        {
            padding-top: 15px;
        }
        .changeClass
        {
            color: Red;
            display: block;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
    <ext:Hidden runat="server" ID="Hidden_QuerryStringID">
    </ext:Hidden>
    <div class="contentArea">
        <ext:Button ID="btnLoad" Hidden="true" runat="server" Text="Import" >
            <DirectEvents>
                <Click OnEvent="btnLoad_Click">
                    <EventMask ShowMask="true" />
                </Click>
            </DirectEvents>
        </ext:Button>
        <h3 style="margin-top: 0px">
            TADA Claim
        </h3>
        <ext:Label ID="lblMsg" runat="server" />
        <table style="padding-top: 5px; padding-bottom: 5px; clear: both;">
            <tr>
                <td>
                    <strong>Month</strong>
                </td>
                <td>
                    <My:Calendar Id="calMonth" IsSkipDay="true" runat="server" />
                </td>
                <td style="padding-left: 15px">
                    <strong>Recommender</strong>
                </td>
                <td>
                    <asp:DropDownList DataTextField="Text" DataValueField="Value" ID="cmbRecommender"
                        runat="server" Style="width: 200px; margin-right: 25px;">
                        <asp:ListItem Text="--Select Recommender--" Value="-1" />
                    </asp:DropDownList>
                </td>
                <td style="padding-left: 15px">
                    <strong>Approval</strong>
                </td>
                <td>
                    <asp:DropDownList DataTextField="Text" DataValueField="Value" ID="cmbApproval" runat="server"
                        Style="width: 200px; margin-right: 25px;">
                        <asp:ListItem Text="--Select Approval--" Value="-1" />
                    </asp:DropDownList>
                </td>
                <td>
                    <ext:Button ID="btnImport" runat="server" Text="Import" Hidden="true">
                        <DirectEvents>
                            <Click OnEvent="btnImport_Click">
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                </td>
                <td>
                    <asp:LinkButton ID="LinkButton1" Style='text-decoration: underline' runat="server"
                        Text="Import" OnClientClick="importPopupProcess();return false;" />
                </td>
            </tr>
        </table>
        <div>
            <ext:GridPanel StyleSpec="margin-top:15px;" ID="gridAllowances" runat="server" Width="1370"
                Cls="itemgrid">
                <Store>
                    <ext:Store ID="storeAllowances" runat="server">
                        <Model>
                            <ext:Model ID="Model4" runat="server" Name="ModelLine">
                                <Fields>
                                    <ext:ModelField Name="Date" Type="String" />
                                    <ext:ModelField Name="LocationFrom" Type="string" />
                                    <ext:ModelField Name="LocationTo" Type="string" />
                                    <ext:ModelField Name="Description" Type="string" />
                                    <ext:ModelField Name="RequestedAllowance1" Type="Float" />
                                    <ext:ModelField Name="RequestedAllowance2" Type="Float" />
                                    <ext:ModelField Name="RequestedAllowance3" Type="Float" />
                                    <ext:ModelField Name="RequestedAllowance4" Type="Float" />
                                    <ext:ModelField Name="RequestedAllowance5" Type="Float" />
                                    <ext:ModelField Name="RequestedAllowance6" Type="Float" />
                                    <ext:ModelField Name="RequestedAllowance7" Type="Float" />
                                    <ext:ModelField Name="RequestedAllowance8" Type="Float" />
                                    <ext:ModelField Name="Allowance1" Type="Float" />
                                    <ext:ModelField Name="Allowance2" Type="Float" />
                                    <ext:ModelField Name="Allowance3" Type="Float" />
                                    <ext:ModelField Name="Allowance4" Type="Float" />
                                    <ext:ModelField Name="Allowance5" Type="Float" />
                                    <ext:ModelField Name="Allowance6" Type="Float" />
                                    <ext:ModelField Name="Allowance7" Type="Float" />
                                    <ext:ModelField Name="Allowance8" Type="Float" />
                                    <ext:ModelField Name="Total" Type="Float" />
                                    <ext:ModelField Name="IsTotal" Type="Boolean" />
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                <Listeners>
                    <BeforeEdit Fn="beforeEdit" />
                </Listeners>
                <Plugins>
                    <ext:CellEditing ID="CellEditing1" ClicksToEdit="1" runat="server">
                        <Listeners>
                            <Edit Fn="afterEdit" />
                        </Listeners>
                    </ext:CellEditing>
                </Plugins>
                <ColumnModel>
                    <Columns>
                        <ext:Column ID="Column5" Sortable="false" MenuDisabled="true" runat="server" Text="Date"
                            Align="Left" DataIndex="Date" Width="100">
                            <Editor>
                                <pr:CalendarExtControl ID="txtDOB" runat="server" LabelAlign="Top" LabelSeparator="">
                                </pr:CalendarExtControl>
                            </Editor>
                        </ext:Column>
                        <ext:Column ID="Column1" Sortable="false" MenuDisabled="true" runat="server" Text="From"
                            Align="Left" DataIndex="LocationFrom" Width="120">
                            <Editor>
                                <ext:TextField ID="txtQTY" SelectOnFocus="true" runat="server" AllowBlank="false">
                                </ext:TextField>
                            </Editor>
                        </ext:Column>
                        <ext:Column ID="Column3" Sortable="false" MenuDisabled="true" runat="server" Text="To"
                            Align="Left" DataIndex="LocationTo" Width="120">
                            <Editor>
                                <ext:TextField ID="TextField1" SelectOnFocus="true" runat="server" AllowBlank="false">
                                </ext:TextField>
                            </Editor>
                        </ext:Column>
                        <ext:Column ID="Column4" Sortable="false" MenuDisabled="true" runat="server" Text="Description"
                            Align="Left" DataIndex="Description" Width="150">
                            <Editor>
                                <ext:TextField ID="TextField2" SelectOnFocus="true" runat="server" AllowBlank="false">
                                </ext:TextField>
                            </Editor>
                        </ext:Column>
                        <ext:Column ID="Column2" Sortable="false" MenuDisabled="true" runat="server" Text="Local-Daily"
                            Align="Right" DataIndex="Allowance1" Width="90">
                            <Renderer Fn="getFormattedAmount1" />
                            <Editor>
                                <ext:TextField ID="TextField3" MaskRe="[0-9]|\.|%" StyleSpec="text-align:right" SelectOnFocus="true"
                                    runat="server" AllowBlank="false">
                                </ext:TextField>
                            </Editor>
                        </ext:Column>
                        <ext:Column ID="Column6" Sortable="false" MenuDisabled="true" runat="server" Text="Upcountry-Daily"
                            Align="Right" DataIndex="Allowance2" Width="110">
                            <Renderer Fn="getFormattedAmount1" />
                            <Editor>
                                <ext:TextField ID="TextField4" MaskRe="[0-9]|\.|%" StyleSpec="text-align:right" SelectOnFocus="true"
                                    runat="server" AllowBlank="false">
                                </ext:TextField>
                            </Editor>
                        </ext:Column>
                        <ext:Column ID="Column7" Sortable="false" MenuDisabled="true" runat="server" Text="Fooding"
                            Align="Right" DataIndex="Allowance3" Width="80">
                            <Renderer Fn="getFormattedAmount1" />
                            <Editor>
                                <ext:TextField ID="TextField5" MaskRe="[0-9]|\.|%" StyleSpec="text-align:right" SelectOnFocus="true"
                                    runat="server" AllowBlank="false">
                                </ext:TextField>
                            </Editor>
                        </ext:Column>
                        <ext:Column ID="Column8" Sortable="false" MenuDisabled="true" runat="server" Text="Transportation"
                            Align="Right" DataIndex="Allowance4" Width="100">
                            <Renderer Fn="getFormattedAmount1" />
                            <Editor>
                                <ext:TextField ID="TextField6" MaskRe="[0-9]|\.|%" StyleSpec="text-align:right" SelectOnFocus="true"
                                    runat="server" AllowBlank="false">
                                </ext:TextField>
                            </Editor>
                        </ext:Column>
                        <ext:Column ID="Column9" Sortable="false" MenuDisabled="true" runat="server" Text="Fuel"
                            Align="Right" DataIndex="Allowance5" Width="80">
                            <Renderer Fn="getFormattedAmount1" />
                            <Editor>
                                <ext:TextField ID="TextField7" MaskRe="[0-9]|\.|%" StyleSpec="text-align:right" SelectOnFocus="true"
                                    runat="server" AllowBlank="false">
                                </ext:TextField>
                            </Editor>
                        </ext:Column>
                        <ext:Column ID="Column10" Sortable="false" MenuDisabled="true" runat="server" Text="Maintainance"
                            Align="Right" DataIndex="Allowance6" Width="95">
                            <Renderer Fn="getFormattedAmount1" />
                            <Editor>
                                <ext:TextField ID="TextField8" MaskRe="[0-9]|\.|%" StyleSpec="text-align:right" SelectOnFocus="true"
                                    runat="server" AllowBlank="false">
                                </ext:TextField>
                            </Editor>
                        </ext:Column>
                        <ext:Column ID="Column11" Sortable="false" MenuDisabled="true" runat="server" Text="Communication"
                            Align="Right" DataIndex="Allowance7" Width="105">
                            <Renderer Fn="getFormattedAmount1" />
                            <Editor>
                                <ext:TextField ID="TextField9" MaskRe="[0-9]|\.|%" StyleSpec="text-align:right" SelectOnFocus="true"
                                    runat="server" AllowBlank="false">
                                </ext:TextField>
                            </Editor>
                        </ext:Column>
                        <ext:Column ID="Column12" Sortable="false" MenuDisabled="true" runat="server" Text="Other Allowance"
                            Align="Right" DataIndex="Allowance8" Width="110">
                            <Renderer Fn="getFormattedAmount1" />
                            <Editor>
                                <ext:TextField ID="TextField10" MaskRe="[0-9]|\.|%" StyleSpec="text-align:right"
                                    SelectOnFocus="true" runat="server" AllowBlank="false">
                                </ext:TextField>
                            </Editor>
                        </ext:Column>
                        <ext:Column runat="server" Align="Right" Width="90" ID="Total" Text="Total" Sortable="false"
                            Groupable="false" DataIndex="Total">
                            <Renderer Fn="getFormattedAmount1" />
                        </ext:Column>
                        <ext:CommandColumn ID="CommandColumn_Delete" runat="server" Width="22" Sortable="false"
                            MenuDisabled="true" Align="Center">
                            <Commands>
                                <ext:GridCommand Icon="Delete" CommandName="Delete" Cls="deleteicon" />
                            </Commands>
                            <Listeners>
                                <Command Fn="onDeleteCommand">
                                </Command>
                            </Listeners>
                        </ext:CommandColumn>
                    </Columns>
                </ColumnModel>
                <SelectionModel>
                    <ext:CellSelectionModel ID="RowSelectionModel21" runat="server" Mode="Single" />
                </SelectionModel>
            </ext:GridPanel>
            <table>
                <tr>
                    <td style="padding-top: 15px;">
                        <ext:Button ID="btnAddNewLine" Width="150" runat="server" Text="&nbsp;&nbsp;Add New Line&nbsp;&nbsp;"
                            ValidationGroup="InsertUpdate">
                            <Listeners>
                                <Click Handler="var newRow = new ModelLine();var rowIndex = #{gridAllowances}.getStore().data.items.length;#{gridAllowances}.getStore().insert(rowIndex-1, newRow);">
                                </Click>
                            </Listeners>
                        </ext:Button>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div style="height: 5px;">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="1" style="padding-top: 20px">
                        <ext:Button ID="btnSaveAndLater" Width="150" Height="30" runat="server" Text="&nbsp;&nbsp;Save and Finish Later&nbsp;&nbsp;"
                            ValidationGroup="InsertUpdate">
                            <DirectEvents>
                                <Click OnEvent="ButtonNext_Click">
                                    <EventMask ShowMask="true" />
                                    <ExtraParams>
                                        <ext:Parameter Name="Values" Value="Ext.encode(#{gridAllowances}.getRowsValues({selectedOnly : false}))"
                                            Mode="Raw" />
                                    </ExtraParams>
                                </Click>
                            </DirectEvents>
                            <Listeners>
                                <Click Handler="valGroup = 'InsertUpdate'; return CheckValidation();">
                                </Click>
                            </Listeners>
                        </ext:Button>
                    </td>
                    <td colspan="1" style="padding-top: 20px; padding-left: 10px;">
                        <ext:Button ID="btnSaveAndSend" Width="150" Height="30" runat="server" Text="&nbsp;&nbsp;Save and Send &nbsp;&nbsp;"
                            ValidationGroup="InsertUpdate">
                            <DirectEvents>
                                <Click OnEvent="ButtonNext_Click">
                                    <Confirmation ConfirmRequest="true" Message="Are you sure, you want to save and forward the TADA?" />
                                    <EventMask ShowMask="true" />
                                    <ExtraParams>
                                        <ext:Parameter Name="Values" Value="Ext.encode(#{gridAllowances}.getRowsValues({selectedOnly : false}))"
                                            Mode="Raw" />
                                    </ExtraParams>
                                </Click>
                            </DirectEvents>
                            <Listeners>
                                <Click Handler="valGroup = 'InsertUpdate'; return CheckValidation();">
                                </Click>
                            </Listeners>
                        </ext:Button>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator3" InitialValue="-1"
        ValidationGroup="InsertUpdate" Display="None" ErrorMessage="Recommender is required"
        ControlToValidate="cmbRecommender"></asp:RequiredFieldValidator>
    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" InitialValue="-1"
        ValidationGroup="InsertUpdate" Display="None" ErrorMessage="Approval is required."
        ControlToValidate="cmbApproval"></asp:RequiredFieldValidator>
</asp:Content>
