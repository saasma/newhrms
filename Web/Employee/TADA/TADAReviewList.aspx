<%@ Page Title="TADA Review List" Language="C#" MasterPageFile="~/Master/EmployeeMS.Master"
    AutoEventWireup="true" CodeBehind="TADAReviewList.aspx.cs" Inherits="Web.Employee.TADAReviewList" %>

<%@ Import Namespace="BLL.Manager" %>
<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/PagingCtl.ascx" TagName="PagingCtl" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    
    <script type="text/javascript">
        var loadingButtonWhenSubmitting = false;
        var skipLoadingCheck = true;
        function addSkillSetToEmployeePopup(requestid) {
            var theText = requestid.getAttribute("value");
            positionHistoryPopup("isPopup=true&reqid=" + theText);
        }

        function refreshEventList(popupWindow) {
            if (typeof (popupWindow) != 'undefined')
                popupWindow.close();

            __doPostBack('<%=btnLoad.UniqueID %>', '');

        }

        function assignOvertime() {

            assignovertimePopup("isPopup=true&assign=true");
        }
   
    </script>
    <style type="text/css">
        .tableLightColor a:hover
        {
            color: #048FC2;
            text-decoration: none;
        }
        .attribute
        {
            width: inherit;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
    <div class="contentArea">
        <h3>
            TADA Review List</h3>
        <div class="attribute" style="padding:10px">
            <table>
                <tr>
                    <td>
                        Month
                        <asp:CheckBox ID="chkHasDate" runat="server" />
                    </td>
                    <td>
                        Status
                    </td>
                </tr>
                <tr>
                    <td>
                        <My:Calendar Id="calMonth" IsSkipDay="true" runat="server" />
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlStatus" runat="server">
                            <asp:ListItem Value="-1">All</asp:ListItem>
                            <asp:ListItem Value="0" Selected="True">Pending</asp:ListItem>
                            <asp:ListItem Value="1">Recommended</asp:ListItem>
                            <asp:ListItem Value="2">Approved</asp:ListItem>
                            <asp:ListItem Value="3">Rejected</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td>
                    </td>
                    <td valign="top">
                        <asp:Button ID="btnLoad" CssClass="load" OnClientClick="valGroup='AEEmployee';return CheckValidation();"
                            OnClick="btnLoad_Click" runat="server" Text="Load" />
                    </td>
                    <td rowspan="2" style="padding-left: 700px">
                    </td>
                </tr>
            </table>
            <%--</ContentTemplate>
            </asp:UpdatePanel>--%>
        </div>
        <div class="clear">
            <cc2:EmptyDisplayGridView CssClass="tableLightColor" UseAccessibleHeader="true" ShowHeaderWhenEmpty="True"
                ID="gvw" runat="server" DataKeyNames="" AutoGenerateColumns="False" CellPadding="4"
                GridLines="None" ShowFooterWhenEmpty="False" OnRowCreated="gvwEmployees_RowCreated">
                <RowStyle BackColor="#E3EAEB" />
                <Columns>
                    <asp:BoundField DataField="EmployeeName" HeaderStyle-Width="110px" HeaderStyle-HorizontalAlign="Left"
                        HeaderText="Employee"></asp:BoundField>
                    <asp:BoundField DataField="MonthText" HeaderStyle-Width="110px" HeaderStyle-HorizontalAlign="Left"
                        HeaderText="Month"></asp:BoundField>
                    <%--<asp:BoundField DataField="Name" HeaderStyle-Width="180px" HeaderStyle-HorizontalAlign="Left" HeaderText="Name"></asp:BoundField>--%>
                     <asp:BoundField DataField="RequestedTotal" DataFormatString="{0:N2}" HeaderStyle-HorizontalAlign="Right"
                        ItemStyle-HorizontalAlign="Right" HeaderStyle-Width="120px" HeaderText="Claimed Amount">
                    </asp:BoundField>
                     <asp:BoundField DataField="Total" DataFormatString="{0:N2}" HeaderStyle-HorizontalAlign="Right"
                        ItemStyle-HorizontalAlign="Right" HeaderStyle-Width="120px" HeaderText="Approved Amount">
                    </asp:BoundField>
                    <asp:BoundField DataField="StatusText" HeaderStyle-Width="120px" HeaderStyle-HorizontalAlign="Left"
                        HeaderText="Status"></asp:BoundField>
                    <asp:TemplateField HeaderStyle-Width="80px" HeaderStyle-HorizontalAlign="Left" HeaderText="">
                        <ItemTemplate>
                            <asp:HyperLink NavigateUrl='<%# "TADAReview.aspx?ID=" +  Eval("RequestID")%>' runat="server"
                                Text="View" ID="btnAction">
                            </asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <RowStyle CssClass="odd" />
                <AlternatingRowStyle CssClass="even" />
                <EmptyDataTemplate>
                    <b>No list. </b>
                </EmptyDataTemplate>
            </cc2:EmptyDisplayGridView>
        </div>
        <div class="buttonsDiv">
            <asp:Button ID="btnExport" CssClass="save" OnClick="btnExport_Click" Visible="true"
                CausesValidation="false" runat="server" Text="Export" />
        </div>
    </div>
</asp:Content>
