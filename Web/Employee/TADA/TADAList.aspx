<%@ Page Title="TADA List" Language="C#" MasterPageFile="~/Master/EmployeeMS.Master"
    AutoEventWireup="true" CodeBehind="TADAList.aspx.cs" Inherits="Web.Employee.TADAList" %>

<%@ Import Namespace="BLL.Manager" %>
<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/PagingCtl.ascx" TagName="PagingCtl" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    
    <script type="text/javascript">
        var loadingButtonWhenSubmitting = false;
        var skipLoadingCheck = true;
        function addSkillSetToEmployeePopup() {
            positionHistoryPopup("isPopup=true");
        }

        function addSkillSetToEmployeePopup1(requestid) {
            var theText = requestid.getAttribute("value");
            positionHistoryPopup("isPopup=true&reqid=" + theText);
        }


        function refreshEventList(popupWindow) {
            if (typeof (popupWindow) != 'undefined')
                popupWindow.close();




        }
        
       
    </script>
    <style type="text/css">
        .tableLightColor a:hover
        {
            color: #048FC2;
            text-decoration: none;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
    <div class="contentArea">
        <h3 style="margin-top: 0px">
            Your TADA Claim List
        </h3>
        <%--  <div class="attribute" style="float: inherit; width: inherit;">
          

            <table>
                <tr>
                    <td>
                        Type
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td valign="top" style="padding-bottom: 8px">
                        <asp:Button ID="btnLoad" CssClass="load" OnClientClick="valGroup='AEEmployee';return CheckValidation();"
                            OnClick="btnLoad_Click" runat="server" Text="Load" />
                    </td>
                </tr>
            </table>
            

        </div>--%>
        <div class="clear">
            <cc2:EmptyDisplayGridView CssClass="tableLightColor" UseAccessibleHeader="true" ShowHeaderWhenEmpty="True"
                ID="gvw" runat="server" DataKeyNames="RequestID" 
                AutoGenerateColumns="False" CellPadding="4"
                GridLines="None" ShowFooterWhenEmpty="False" 
                OnRowCreated="gvwEmployees_RowCreated" onrowcommand="gvw_RowCommand">
                <RowStyle BackColor="#E3EAEB" />
                <Columns>
                    <asp:BoundField DataField="MonthText" HeaderStyle-Width="110px" HeaderStyle-HorizontalAlign="Left"
                        HeaderText="Month"></asp:BoundField>
                    <%--<asp:BoundField DataField="Name" HeaderStyle-Width="180px" HeaderStyle-HorizontalAlign="Left" HeaderText="Name"></asp:BoundField>--%>
                    <asp:BoundField DataField="RequestedTotal" DataFormatString="{0:N2}" HeaderStyle-HorizontalAlign="Right"
                        ItemStyle-HorizontalAlign="Right" HeaderStyle-Width="120px" HeaderText="Claimed Amount">
                    </asp:BoundField>
                    <asp:BoundField DataField="Total" DataFormatString="{0:N2}" HeaderStyle-HorizontalAlign="Right"
                        ItemStyle-HorizontalAlign="Right" HeaderStyle-Width="120px" HeaderText="Approved Amount">
                    </asp:BoundField>
                    <asp:BoundField DataField="StatusText" HeaderStyle-Width="120px" HeaderStyle-HorizontalAlign="Left"
                        HeaderText="Status"></asp:BoundField>
                    <asp:TemplateField HeaderStyle-Width="80px" HeaderStyle-HorizontalAlign="Left" HeaderText="">
                        <ItemTemplate>
                            <asp:HyperLink NavigateUrl='<%# "TADAClaim.aspx?ID=" +  Eval("RequestID")%>' runat="server"
                                Text="View" ID="btnAction1">
                            </asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-Width="80px" HeaderStyle-HorizontalAlign="Left" HeaderText="">
                        <ItemTemplate>
                            <asp:LinkButton Visible='<%# IsDeleteVisible(Eval("Status")) %>' CommandArgument='<%# Eval("RequestID") %>' OnClientClick="return confirm('Confirm delete the TADA?');" CommandName="DeleteData"
                                runat="server" Text="Delete" ID="btnAction2">
                            </asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <RowStyle CssClass="odd" />
                <AlternatingRowStyle CssClass="even" />
                <EmptyDataTemplate>
                    <b>No list. </b>
                </EmptyDataTemplate>
            </cc2:EmptyDisplayGridView>
        </div>
        <div class="buttonsDiv">
            <asp:Button ID="btnNew" CssClass="update" Style="margin-left: 10px; float: left;"
                OnClientClick="window.location = 'TADAClaim.aspx';return false;" Visible="true"
                runat="server" Text="New TADA" />
            <asp:Button ID="btnExport" CssClass="save" OnClick="btnExport_Click" Visible="true"
                CausesValidation="false" runat="server" Text="Export" />
        </div>
    </div>
</asp:Content>
