<%@ Page Title="Approve Allowance" Language="C#" MasterPageFile="~/Master/EmployeeMS.Master"
    AutoEventWireup="true" CodeBehind="AllowanceApprove.aspx.cs" Inherits="Web.Employee.AllowanceApprove" %>

<%@ Import Namespace="BLL.Manager" %>
<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/PagingCtl.ascx" TagName="PagingCtl" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        var loadingButtonWhenSubmitting = false;
        var skipLoadingCheck = true;
        function addSkillSetToEmployeePopup(requestid) {
            var theText = requestid.getAttribute("value");
            positionHistoryPopup("isPopup=true&reqid=" + theText);
        }

        function refreshEventList(popupWindow) {
            if (typeof (popupWindow) != 'undefined')
                popupWindow.close();

            __doPostBack('<%=btnLoad.UniqueID %>', '');

        }

        function assignAllowance() {

            assignAllowancePopup("isPopup=true&assign=true");
        }


        function selectDeselectAll(chk) {
            $('input[type=checkbox]').each(
                    function (index) {
                        if (this.id.indexOf('chkDelete') > 0)
                            this.checked = chk.checked;
                    }
                );
        }
   
    </script>
    <script src="override.js" type="text/javascript"></script>
    <style type="text/css">
        .tableLightColor th
        {
            font-weight: normal !important;
        }
        
        .table td
        {
            background-color: White;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
    <ext:ResourceManager ID="ResourceManager1" DisableViewState="false" runat="server"
        ScriptMode="Release" />
    <div class="contentArea" style="margin-top: 10px">
        <div class="attribute" style="padding: 10px">
            <table>
                <tr>
                    <td>
                        Period
                    </td>
                    <td style="padding-left: 5px; display: none;">
                        Search Employee
                    </td>
                    <td style="padding-left: 10px">
                        From
                    </td>
                    <td style="padding-left: 10px">
                        To
                    </td>
                    <td id="tdBranch" runat="server">
                        Branch
                    </td>
                    <td>
                        Status
                    </td>
                    <td>
                        Allowance Type
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:DropDownList ID="ddlType" runat="server">
                            <asp:ListItem Value="-1" Text="All" Selected="True" />
                            <asp:ListItem Value="1">This Week</asp:ListItem>
                            <asp:ListItem Value="2">This Month</asp:ListItem>
                            <asp:ListItem Value="3">Last Month</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td style="padding-left: 5px; display: none;">
                        <asp:TextBox Width="160px" ID="txtEmpSearchText" runat="server"></asp:TextBox>
                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtEmpSearchText"
                            WatermarkText="Employee Name" WatermarkCssClass="searchBoxText" />
                        <cc1:AutoCompleteExtender EnableCaching="true" ID="AutoCompleteExtenderOrganization"
                            runat="server" MinimumPrefixLength="2" ServiceMethod="GetEmployeeNames" ServicePath="~/PayrollService.asmx"
                            TargetControlID="txtEmpSearchText" CompletionSetCount="10" CompletionInterval="250"
                            CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList"
                            CompletionListHighlightedItemCssClass="AutoExtenderHighlight">
                        </cc1:AutoCompleteExtender>
                    </td>
                    <td runat="server" id="t2">
                        <ext:DateField ID="dateFrom" runat="server">
                            <Plugins>
                                <ext:GenericPlugin ID="GenericPlugin1" runat="server" InstanceName="Ext.ux.DatePicker.HighlightWithNepDates" />
                            </Plugins>
                        </ext:DateField>
                    </td>
                    <td runat="server" id="t3">
                        <ext:DateField ID="dateTo" runat="server">
                            <Plugins>
                                <ext:GenericPlugin ID="GenericPlugin2" runat="server" InstanceName="Ext.ux.DatePicker.HighlightWithNepDates" />
                            </Plugins>
                        </ext:DateField>
                    </td>
                    <td id="tdBranch2" runat="server">
                        <asp:DropDownList ID="ddlBranch" AutoPostBack="false" AppendDataBoundItems="true"
                            DataTextField="Name" DataValueField="BranchId" runat="server" Width="150px">
                            <asp:ListItem Text="--Select Branch--" Value="-1" Selected="True" />
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlStatus" runat="server">
                            <asp:ListItem Value="-1">All</asp:ListItem>
                            <asp:ListItem Value="0">Pending</asp:ListItem>
                            <asp:ListItem Value="1">Recommended</asp:ListItem>
                            <asp:ListItem Value="-2" Selected="True">Pending / Recommended</asp:ListItem>
                            <asp:ListItem Value="2">Approved</asp:ListItem>
                            <asp:ListItem Value="3">Rejected</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlAllowanceType" AutoPostBack="false" AppendDataBoundItems="true"
                            DataTextField="Name" DataValueField="EveningCounterTypeId" runat="server" Width="150px">
                            <asp:ListItem Text="All" Value="-1" Selected="True" />
                        </asp:DropDownList>
                    </td>
                    <td valign="top">
                        <asp:Button ID="btnLoad" CssClass="load" OnClientClick="valGroup='AEEmployee';return CheckValidation();"
                            OnClick="btnLoad_Click" runat="server" Text="Load" />
                    </td>
                </tr>
            </table>
            <%--</ContentTemplate>
            </asp:UpdatePanel>--%>
            <br />
            <asp:LinkButton ID="btnAssignAllowance" OnClientClick="assignAllowance(); return false;"
                CssClass="save" Style="height: 16px; width: 130px" runat="server" Text="Assign Allowance" />
        </div>
        <uc2:MsgCtl ID="divMsgCtl" EnableViewState="false" Hide="true" Width="390px" runat="server" />
        <uc2:WarningCtl ID="divWarningMsg" EnableViewState="false" Hide="true" runat="server" />
        <div class="clear">
            <cc2:EmptyDisplayGridView CssClass="table table-primary mb30 table-bordered table-hover"
                PagerStyle-CssClass="defaultPagingBar" UseAccessibleHeader="true" ShowHeaderWhenEmpty="True"
                ID="gvw" runat="server" DataKeyNames="RequestID" AutoGenerateColumns="False"
                PagerStyle-HorizontalAlign="Center" OnPageIndexChanged="gvwRoles_PageIndexChanged"
                OnPageIndexChanging="gvwRoles_PageIndexChanging" CellPadding="4" GridLines="None"
                ShowFooterWhenEmpty="False" OnRowCreated="gvwEmployees_RowCreated">
                <RowStyle BackColor="#E3EAEB" />
                <Columns>
                    <asp:TemplateField HeaderText="Select" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                        <HeaderTemplate>
                            <asp:CheckBox ID="chk12Delete1" onclick="selectDeselectAll(this)" runat="server" />
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:CheckBox Visible='<%# CanRecommendOrApprove(int.Parse(Eval("Status").ToString()),int.Parse(Eval("EmployeeID").ToString()))%>'
                                ID="chkDelete" runat="server" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="EmployeeName" HeaderStyle-Width="150px" HeaderStyle-HorizontalAlign="Left"
                        HeaderText="Employee Name"></asp:BoundField>
                    <asp:BoundField DataField="AllowanceType" HeaderStyle-Width="150px" HeaderStyle-HorizontalAlign="Left"
                        HeaderText="Allowance"></asp:BoundField>
                    <asp:BoundField DataField="Branch" HeaderStyle-Width="120px" HeaderStyle-HorizontalAlign="Left"
                        HeaderText="Branch"></asp:BoundField>
                    <%--<asp:BoundField DataField="Name" HeaderStyle-Width="180px" HeaderStyle-HorizontalAlign="Left" HeaderText="Name"></asp:BoundField>--%>
                    <asp:BoundField DataField="StartTime" DataFormatString="{0:yyyy-MMM-dd}" HeaderStyle-Width="100px"
                        ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Left" HeaderText="Start Date">
                    </asp:BoundField>
                    <asp:BoundField DataField="EndTime" DataFormatString="{0:yyyy-MMM-dd}" HeaderStyle-Width="100px"
                        ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Left" HeaderText="End Date">
                    </asp:BoundField>
                    <asp:BoundField DataField="StartDateNep" DataFormatString="{0:M-dd-yyyy}" HeaderStyle-Width="100px"
                        HeaderStyle-HorizontalAlign="Right" HeaderText="Start Miti"></asp:BoundField>
                    <asp:BoundField DataField="EndDateNep" DataFormatString="{0:M-dd-yyyy}" HeaderStyle-Width="100px"
                        HeaderStyle-HorizontalAlign="Right" HeaderText="End Miti"></asp:BoundField>
                    <%-- <asp:BoundField DataField="Position" HeaderText="Position"></asp:BoundField>--%>
                    <asp:BoundField DataField="Duration" HeaderStyle-Width="80px" HeaderStyle-HorizontalAlign="Right"
                        HeaderText="Units"></asp:BoundField>
                    <asp:BoundField DataField="Reason" HeaderStyle-Width="300px" HeaderStyle-HorizontalAlign="Left"
                        HeaderText="Reason"></asp:BoundField>
                    <%--  <asp:BoundField DataField="SupervisorName" Visible="false" HeaderStyle-Width="120px"
                        HeaderStyle-HorizontalAlign="Left" HeaderText="Supervisor Name"></asp:BoundField>--%>
                    <asp:BoundField DataField="StatusModified" HeaderStyle-Width="100px" HeaderStyle-HorizontalAlign="Left"
                        HeaderText="Status"></asp:BoundField>
                    <%--<asp:TemplateField HeaderStyle-Width="120px" HeaderStyle-HorizontalAlign="Left"   HeaderText="">
                        <ItemTemplate>
                               <%# "<a  href='#'  onclick='addSkillSetToEmployeePopup("  + Eval("RequestID")  + ")'>Action</a>"%>
                        </ItemTemplate>
                    </asp:TemplateField>--%>
                    <asp:TemplateField HeaderStyle-Width="120px" HeaderStyle-HorizontalAlign="Left" HeaderText="">
                        <ItemTemplate>
                            <asp:HyperLink NavigateUrl="javascript:void(0)" value='<%# Eval("RequestID") %>'
                                onclick='<%# "addSkillSetToEmployeePopup(this);" %>' runat="server" Text="Action"
                                ID="btnAction">
                            </asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <PagerSettings Mode="NumericFirstLast" NextPageText="Next �" PreviousPageText="� Prev"
                    FirstPageText="First" LastPageText="Last" />
                <%-- <%# "<a  href='#'  onclick='addSkillSetToEmployeePopup("+\x27 + Eval("RequestID")+ \x27 + ")'>Action</a>"%>--%>
                <%--   <RowStyle CssClass="odd" />
                <AlternatingRowStyle CssClass="even" />
                <PagerStyle CssClass="defaultPagingBar" />--%>
                <EmptyDataTemplate>
                    <b>No list. </b>
                </EmptyDataTemplate>
            </cc2:EmptyDisplayGridView>
            <uc1:PagingCtl ID="pagintCtl" OnNextRecord="btnNext_Click" OnPrevRecord="btnPrevious_Click"
                OnDropDownShowRecordsChanged="ddlRecords_SelectedIndexChanged" runat="server" />
        </div>
        <div class="buttonsDiv">
            <asp:Button ID="btnRecommendOrApprove" Style="float: left; margin-left: 10px;" CssClass="createbtns"
                OnClick="btnRecommendOrApprove_Click" Visible="true" Width="125" Height="32px"
                CausesValidation="false" runat="server" OnClientClick="return confirm('Are you srue you want to Forward the selected requests?');"
                Text="Recommend" />
            <asp:LinkButton ID="LinkButton1" runat="server" Text="Export to Excel" OnClick="btnExport_Click"
                CssClass=" excel marginRight" Style="float: right;" />
        </div>
    </div>
</asp:Content>
