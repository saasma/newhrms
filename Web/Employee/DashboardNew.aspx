﻿<%@ Page Title="Dashboard" Language="C#" MasterPageFile="~/Master/EmployeeMS.Master"
    AutoEventWireup="true" CodeBehind="DashboardNew.aspx.cs" Inherits="Web.Employee.DashboardNew" %>

<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="<%=ResolveUrl("~/bootstrap/css/bootstrap.css?v=") + Web.Helper.WebHelper.Version %>"
        rel="stylesheet" />
    <link rel="stylesheet" href="<%=ResolveUrl("~/theme/css/style.min.css?v=") + Web.Helper.WebHelper.Version %>" />
    <style type="text/css">
        .leaveList
        {
            list-style-type: none; /*border: 2px solid #BFBFBF;
            background-color: #D0CECE;*/
            display: inline-block;
            padding: 10px;
        }
        .leaveList li
        {
            /* border: 1px solid #DEDEDE;*/
            background-color: #F2F2F2;
            float: left;
            padding: 5px;
            padding-left: 8px;
            padding-right: 8px;
            text-align: center;
            margin-bottom: 3px;
            margin-right: 3px;
        }
        .panel-title a:hover
        {
        }
        .MenuDashboardList li span a:hover, .MenuDashboardList li:hover
        {
            background-color: #5BC0DE !important;
        }
        .MenuDashboardList li
        {
            /* border: 1px solid #DEDEDE;*/
            display: block;
            width: 200px;
            margin-bottom: 3px;
            margin-right: 3px;
            text-align: center;
            background: #3498DB;
            font-size: 12px;
            font-family: 'Arial' , sans-serif;
            color: White !important;
            white-space: nowrap;
            box-sizing: border-box;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            float: left;
            padding: 5px;
            padding-left: 8px;
            padding-right: 8px;
        }
        
        #ctl00_mainContent_DatePickerWithHolidayDisplay-footerEl
        {
            display: none;
        }
        
        #ulDiv a
        {
            color: White;
            text-decoration: none;
        }
        
        .MenuDashboardList li
        {
        }
        
        .MenuDashboardList li:nth-child(4n + 1)
        {
            clear: left;
        }
        
        .blockTitle
        {
            color: #428BCA; /*text-transform: uppercase;*/
            font-size: 14px;
            font-weight: bold;
            padding-left: 2px;
        }
        .x-grid-body
        {
            border-top-color: white;
        }
        .blockText
        {
            color: #305496;
        }
        .blockBottomBorder
        {
            border-bottom: 1px solid #D9E7F3;
            padding-bottom: 1px;
        }
        .weekBlock
        {
            background-color: #DEDEDE;
            display: inline-block;
            padding: 4px;
            margin-top: 10px;
            padding-left: 8px;
            padding-right: 50px;
            color: #333333; /*margin-left: 90px;*/
        }
        .col-md-6
        {
            width: 100% !important;
        }
        
        #tableLink table td
        {
            background-color: #9DC3E6;
            display: inline-block;
            padding: 10px;
            margin-top: 4px; /* padding-left: 8px; */ /* padding-right: 50px; */
            color: #333333;
            margin-left: 10px;
            width: 153px;
            height: 33px;
            vertical-align: center; /* text-align: center; */
            vertical-align: middle;
            font-weight: bold !important;
        }
        
        
        .widget .widget-body.list ul li
        {
            line-height: inherit !important;
        }
        td
        {
            padding: 2px;
        }
        
        .panel-body
        {
            padding: 10px !important;
        }
        .widget .widget-head .heading.glyphicons
        {
            padding: 0 0 0 5px;
        }
        .panel-heading
        {
            padding: 10px 20px 10px 5px !important;
        }
        
        .col-md-6 .panel, .col-md-6 .panel-body
        {
        }
        .col-md-6 .panel-heading
        {
            background-color: #bcc1c4;
        }
        .col-md-6 .panel-title
        {
            color: #41464d;
            text-shadow: 1px 1px 1px rgba(255,255,255,1);
        }
        .x-monthpicker-item
        {
            margin-top: 0px !important;
        }
    </style>
    <script type="text/javascript">
        

    </script>
    <script type="text/javascript">

        var currentMonth = null;
        ///// Override DatePicker & is triggered when month or date selection changes
        Ext.DatePicker.prototype.update = function (date, forceRefresh) {

            var me = this,
            active = me.activeDate;
            if (me.rendered) {
                me.activeDate = date;
                if (!forceRefresh && active && me.el && active.getMonth() == date.getMonth() && active.getFullYear() == date.getFullYear()) {
                    me.selectedUpdate(date, active);
                } else {
                    me.fullUpdate(date, active);
                }
            }


            if (this.id == 'ctl00_mainContent_DatePickerWithHolidayDisplay') {

                if (currentMonth == null) {
                    currentMonth = date.getMonth();
                    currentYear = date.getFullYear();
                    loadHoliday(date.getMonth(), date.getFullYear());

                }
                else if (currentMonth != date.getMonth() || currentYear != date.getFullYear()) {
                    currentMonth = date.getMonth();
                    currentYear = date.getFullYear();
                    loadHoliday(date.getMonth(), date.getFullYear());

                }
            }

            return me;

        };



        var loadHoliday = function (month, year) {

        };


    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
    <ext:ResourceManager ID="ResourceManager1" runat="server" DisableViewState="true"
        ShowWarningOnAjaxFailure="false" ScriptMode="Release" Namespace="CompanyX" />
    <div class="contentArea">
        <uc2:WarningCtl ID="divWarningMsg" EnableViewState="false" Hide="true" runat="server" />
        <table>
            <tr>
                <td valign="top">
                    <div style="margin-top: 15px">
                        <table>
                            <tr>
                                <td valign="top">
                                    <div style="height: 250px; overflow-y: hidden">
                                        <asp:Image ID="image" runat="server" Width="200px" />
                                    </div>
                                    <div class="widget" runat="server" id="Div1" visible="true" style="width: 200px;
                                        margin-top: 10px; border-color: #4E5154;">
                                        <div class="widget-head" style="background-color: #4E5154">
                                            <h4 class="heading glyphicons history">
                                                <i></i>My Leave Balances</h4>
                                            <a href="AApproveOvertime.aspx" class="details pull-right"></a>
                                        </div>
                                        <div class="widget-body list">
                                            <asp:Repeater runat="server" ID="rptLeaveList">
                                                <ItemTemplate>
                                                    <ul>
                                                        <li runat="server" id="Li1"><span style="float: left">
                                                            <%#Eval("Text") %></span> <span style="float: right; color: #4E5154" runat="server"
                                                                id="Span1" class="count">
                                                                <%#Eval("Value") %></span> </li>
                                                    </ul>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </div>
                                    </div>
                                </td>
                                <td style="padding-left: 20px;">
                                    <table>
                                        <tr>
                                            <td valign="top">
                                                <div>
                                                    <span id="welcome" style="color: #44546A; padding-left: 30px; font-size: 22px; font-weight: bold"
                                                        runat="server">Welcome {0},</span>
                                                    <br />
                                                    <br />
                                                    <asp:Label Style="text-align: center; font-size: 16px; color: #339900; line-height: 20px;
                                                        padding-left: 30px" ID="lblDay" runat="server" Text=""></asp:Label>
                                                    <asp:Label Style="text-align: center; font-size: 16px; color: #FF3300; line-height: 20px;
                                                        padding-left: 15px" ID="lblNepDate" runat="server" Text=""></asp:Label>
                                                    <asp:Label Style="text-align: center; font-size: 16px; color: #336699; line-height: 20px;
                                                        padding-left: 15px" ID="lblEngDate" runat="server" Text=""></asp:Label>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                    <br />
                                    <div style="clear: both" id="tableLink">
                                        <div style="margin-top: 10px;">
                                            <ul id="ulDiv" class="MenuDashboardList" style="margin-left: 0px">
                                                <asp:Repeater runat="server" ID="DashboardMnuRepeater">
                                                    <ItemTemplate>
                                                        <li runat="server" id="Li1" style="font-weight: normal; font-size: 13px; width: 160px;
                                                            background-color: #3EB2DB"><span style="display: block">
                                                                <%#Eval("Text") %></span> </li>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </ul>
                                        </div>
                                    </div>
                                    <div style="padding-top: 10px; clear: both" id="divMessages">
                                        <div class="col-md-6" style="padding-left: 0px; padding-right: 0px;">
                                            <div class="panel-group" id="accordion2" style="">
                                                <!-- panel -->
                                                <div class="panel panel-primary" style="clear: both; border-radius: 0px; border: 1px solid #EBCCD1!important;
                                                    margin-bottom: 20px;">
                                                    <div class="panel-heading" style="clear: both; border-radius: 0px; background-color: #F2DEDE;">
                                                        <div style="width: 630px" class="panel-title">
                                                            <span><a href="NoticeList.aspx">Notice</a></span> <span style="float: right"><a href="NoticeList.aspx">View All</a></span>
                                                        </div>
                                                    </div>
                                                    <div id="Div2" class="panel-collapse collapse in">
                                                        <div class="panel-body" style="border-top-color: #EBCCD1">
                                                            <div>
                                                                <div style="padding-left: 0px; font-size: 13px;">
                                                                    <ext:GridPanel ID="gridNoticeBoard" StyleSpec="border-top:0px;" runat="server" Header="false"
                                                                        HideHeaders="true">
                                                                        <Store>
                                                                            <ext:Store ID="storeNoticeBoard" runat="server">
                                                                                <Model>
                                                                                    <ext:Model ID="model1" runat="server">
                                                                                        <Fields>
                                                                                            <ext:ModelField Name="NoticeID" />
                                                                                            <ext:ModelField Name="Title" />
                                                                                            <ext:ModelField Name="PublishedDateStr" />
                                                                                            <ext:ModelField Name="FileName" />
                                                                                            <ext:ModelField Name="URL" />
                                                                                            <ext:ModelField Name="StatusStr" />
                                                                                        </Fields>
                                                                                    </ext:Model>
                                                                                </Model>
                                                                            </ext:Store>
                                                                        </Store>
                                                                        <ColumnModel>
                                                                            <Columns>
                                                                                <ext:TemplateColumn ID="TemplateColumn2" runat="server" MenuDisabled="true" Header="Template"
                                                                                    Width="480">
                                                                                    <Template ID="Template2" runat="server" Width="480">
                                                                                        <Html>
                                                                                            <tpl for=".">
                                                               
                                                                                                    <table style="">
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <div style="width:80px;">
                                                                                                                    <%--{PublishedDateStr:date("d-m-Y")}--%>
                                                                                                                    {PublishedDateStr}
                                                                                                                </div>
                                                                                                            </td>
                                                                                                            <td>
                                                                                                                <div style="width:380px;">
                                                                                                                    {Title}
                                                                                                                </div>
                                                                                                            </td>
                                                                                                           
                                                                                                        </tr>

                                                                                                    </table>

                                                                                             </tpl>
                                                                                        </Html>
                                                                                    </Template>
                                                                                </ext:TemplateColumn>
                                                                                <ext:TemplateColumn ID="TemplateColumn4" runat="server" Text="Attachment" Width="50"
                                                                                    DataIndex="NoticeID" TemplateString='<tpl for="."><tpl if="FileName.trim().length!=0"><a href="../NoticeFileDownloader.ashx?id={URL}"> <img src="../images/paperclip.png" width="15" height="15" title="{FileName}"/></a></tpl></tpl>' />
                                                                                <ext:TemplateColumn ID="TemplateColumn3" runat="server" Text="Action" Width="100"
                                                                                    DataIndex="NoticeID" TemplateString='<a href="NoticeDetail.aspx?id={NoticeID}"> View Detail</a>' />
                                                                            </Columns>
                                                                        </ColumnModel>
                                                                    </ext:GridPanel>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- panel -->
                                                <div class="panel panel-primary" style="border-radius: 0px; border: 1px solid #EBCCD1!important;
                                                    margin-bottom: 20px;">
                                                   <%-- <div class="panel-heading" style="border-radius: 0px; background-color: #F2DEDE;">
                                                        <h4 class="panel-title" style="padding-top: 5px;">
                                                            <a data-toggle="collapse" style="font-weight: normal;" class="collapsed" data-parent="#accordion2"
                                                                href="#collapseTwo2">Messages for you </a>
                                                        </h4>
                                                    </div>--%>

                                                     <div class="panel-heading" style="clear: both; border-radius: 0px; background-color: #F2DEDE;">
                                                        <div style="width: 630px" class="panel-title">
                                                            <span>Messages for you</span>
                                                        </div>
                                                    </div>

                                                    <div id="collapseTwo2" class="panel-collapse collapse in">
                                                        <div class="panel-body" style="border-top-color: #EBCCD1">
                                                            <div>
                                                                <div style="padding-left: 0px; font-size: 13px;">
                                                                    <asp:Repeater runat="server" ID="rptMessages">
                                                                        <HeaderTemplate>
                                                                            <table>
                                                                        </HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <tr>
                                                                                <td>
                                                                                    <span style="font-weight: bold">
                                                                                        <%# Eval("SendByName")%></span> Says
                                                                                </td>
                                                                                <td>
                                                                                    &#9658;
                                                                                    <%# Eval("Subject")%>
                                                                                </td>
                                                                            </tr>
                                                                        </ItemTemplate>
                                                                        <FooterTemplate>
                                                                            </table>
                                                                        </FooterTemplate>
                                                                    </asp:Repeater>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- panel -->
                                                <div class="panel panel-primary" style="border-radius: 0px; margin-bottom: 20px;
                                                    border: 1px solid #DDDDDD;">
                                                    <div class="panel-heading" style="height: 30px; border-radius: 0px; background-color: #EEEEEE;">
                                                        <h4 class="panel-title" style="padding-top: 0px;">
                                                            <a data-toggle="collapse" style="font-weight: normal;" class="collapsed" data-parent="#accordion2"
                                                                href="#collapseFour3">Upcoming leaves </a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapseFour3" class="panel-collapse collapse in">
                                                        <div class="panel-body" style="border-top-color: #DDDDDD">
                                                            <div class="weekBlock">
                                                                THIS WEEK
                                                            </div>
                                                            <div style="padding-left: 0px; font-size: 13px;">
                                                                <asp:Repeater runat="server" ID="rptThisWeek">
                                                                    <HeaderTemplate>
                                                                        <table>
                                                                    </HeaderTemplate>
                                                                    <ItemTemplate>
                                                                        <tr>
                                                                            <td>
                                                                                <span style="font-weight: bold">
                                                                                    <%# Eval("Text") %></span>
                                                                            </td>
                                                                            <td>
                                                                                &#9658;
                                                                                <%# Eval("Value") %>
                                                                                <%#  Eval("Other")%>
                                                                            </td>
                                                                        </tr>
                                                                    </ItemTemplate>
                                                                    <FooterTemplate>
                                                                        </table>
                                                                    </FooterTemplate>
                                                                </asp:Repeater>
                                                            </div>
                                                            <div class="weekBlock">
                                                                NEXT 30 DAYS</div>
                                                            <div style="padding-left: 0px; font-size: 13px;">
                                                                <asp:Repeater runat="server" ID="rpt30Days">
                                                                    <HeaderTemplate>
                                                                        <table>
                                                                    </HeaderTemplate>
                                                                    <ItemTemplate>
                                                                        <tr>
                                                                            <td>
                                                                                <span style="font-weight: bold">
                                                                                    <%# Eval("Text") %></span>
                                                                            </td>
                                                                            <td>
                                                                                &#9658;
                                                                                <%# Eval("Value") %>
                                                                                <%# Eval("Other")%>
                                                                            </td>
                                                                        </tr>
                                                                    </ItemTemplate>
                                                                    <FooterTemplate>
                                                                        </table>
                                                                    </FooterTemplate>
                                                                </asp:Repeater>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- panel -->
                                            </div>
                                            <!-- panel-group -->
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div>
                    </div>
                </td>
                <td style="width: 20px">
                    &nbsp;
                </td>
                <td valign="top" style="width: 274px; padding-top: 20px;">

                    
                    <div>
                        <div class="widget" runat="server" id="Div3" visible="true" style="clear: both; margin-right: 0px;
                            width: 274px; margin-top: 10px; border-color: #4E5154;">
                            <div class="widget-head" style="background-color: #4E5154">
                                <h4 class="heading glyphicons history">
                                    <i></i>My Times This Week</h4>
                            </div>
                            <div style="clear: both">
                            </div>
                            <asp:GridView CssClass="tableLightColor" ID="gvwWeekTimes" runat="server" ShowHeader="false"
                                AutoGenerateColumns="false" GridLines="None" PagerStyle-CssClass="defaultPagingBar"
                                PagerStyle-HorizontalAlign="Center">
                                <RowStyle BackColor="#E3EAEB" />
                                <Columns>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:Label runat="server" Width="40px" Text='<%# Eval("ActualDate") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:Label ID="Label1" runat="server" Width="90px" Text='<%# Eval("DayType") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:Label ID="Label1" runat="server" Width="50px" Text='<%# Eval("RefinedInRemarks") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:Label ID="Label1" runat="server" Width="50px" Text='<%# Eval("RefinedOutRemarks") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <RowStyle CssClass="odd" />
                                <AlternatingRowStyle CssClass="even" />
                                <EmptyDataTemplate>
                                    <b>No list. </b>
                                </EmptyDataTemplate>
                            </asp:GridView>
                        </div>
                        <div style="margin-top: 20px;">
                            <img src="images/plane.png" width="25" height="20" />
                            <span class="blockTitle">Upcoming Holidays</span> <span style="padding-left: 20px"
                                class="blockText"><a href="AllHolidayReport.aspx">View All</a></span>
                        </div>
                        <ext:GridPanel EmptyText="There are no immediate holidays." ID="GridPanelMYTimesheet"
                            runat="server" Width="280" Header="false" HideHeaders="true">
                            <Store>
                                <ext:Store ID="storeMYTimesheet" runat="server">
                                    <Model>
                                        <ext:Model ID="model2" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="EngDateLongForDashboard" Type="String">
                                                </ext:ModelField>
                                                <ext:ModelField Name="Id" Type="String">
                                                </ext:ModelField>
                                                <ext:ModelField Name="Name" Type="String">
                                                </ext:ModelField>
                                                <ext:ModelField Name="NepDateLongForDashboard" Type="String">
                                                </ext:ModelField>
                                                <ext:ModelField Name="DaysLeftForDashboard" Type="String">
                                                </ext:ModelField>
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                            <ColumnModel>
                                <Columns>
                                    <ext:TemplateColumn ID="TemplateColumn1" runat="server" MenuDisabled="true" Header="Template"
                                        Width="245">
                                        <Template ID="Template1" runat="server" Width="245">
                                            <Html>
                                                <tpl for=".">
                                                               
                                                                <table style="width:245px;">
                                                                    <tr>
                                                                        <td>
                                                                            <div style="width:120px;">
                                                                                {EngDateLongForDashboard}
                                                                            </div>
                                                                        </td>
                                                                        <td>
                                                                            <div title='{Name}' style="width:175px;">
                                                                                {Name}
                                                                            </div>
                                                                        </td>
                                                                    </tr>

                                                                    <tr>
                                                                        <td>
                                                                            <div style="width:120px;">
                                                                                {NepDateLongForDashboard}
                                                                            </div>
                                                                        </td>
                                                                        <td>
                                                                            <div style="width:120px;">
                                                                                {DaysLeftForDashboard}
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                </table>

                                                         </tpl>
                                            </Html>
                                        </Template>
                                    </ext:TemplateColumn>
                                </Columns>
                            </ColumnModel>
                        </ext:GridPanel>
                    </div>
                    <div class="widget" runat="server" id="blockOvertime" visible="true">
                        <div class="widget-head">
                            <h4 class="heading glyphicons history">
                                <i></i>Overtime</h4>
                            <a href="AApproveOvertimeNewNew.aspx" class="details pull-right">View All</a>
                        </div>
                        <div class="widget-body list">
                            <ul>
                                <li runat="server" id="overtimeRecommendLi"><span style="float: left">Pending</span>
                                    <span style="float: right" runat="server" id="overtimeRecommend" class="count">0</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="widget" runat="server" id="blockEveningCounter" visible="false">
                        <div class="widget-head">
                            <h4 class="heading glyphicons history">
                                <i></i>Allowance</h4>
                            <a href="AllowanceApprove.aspx" class="details pull-right">View All</a>
                        </div>
                        <div class="widget-body list">
                            <ul>
                                <li runat="server" id="eveningCounterRecommnededLi"><span>Pending</span> <span runat="server"
                                    id="eveningCounterRecommneded" class="count">0</span> </li>
                                <li runat="server" id="eveningCounterApproveLi"><span>Recommended</span> <span class="count"
                                    runat="server" id="eveningCounterApprove">0</span> </li>
                            </ul>
                        </div>
                    </div>
                    <div class="widget" runat="server" id="blockTADA" visible="false">
                        <div class="widget-head">
                            <h4 class="heading glyphicons history">
                                <i></i>TADA</h4>
                            <a href="TADA/TADAReviewList.aspx" class="details pull-right">View All</a>
                        </div>
                        <div class="widget-body list">
                            <ul>
                                <li runat="server" id="tadaRecommendLi"><span>Pending</span> <span runat="server"
                                    id="tadaRecommend" class="count">0</span> </li>
                                <li runat="server" id="tadaApproveLi"><span>Recommended</span> <span class="count"
                                    runat="server" id="tadaApprove">0</span> </li>
                            </ul>
                        </div>
                    </div>
                    <div class="widget" runat="server" id="blockLeave" visible="false">
                        <div class="widget-head">
                            <h4 class="heading glyphicons history">
                                <i></i>Leave</h4>
                            <a href="LeaveApproval.aspx" class="details pull-right">View All</a>
                        </div>
                        <div class="widget-body list">
                            <ul>
                                <li runat="server" id="LeaveRecommendLi"><span style="float: left">Pending</span> <span
                                    runat="server" style="float: right" id="LeaveRecommend" class="count">0</span>
                                </li>
                                <li runat="server" id="LeaveApproveLi" visible="false"><span style="float: left">Recommended</span>
                                    <span class="count" style="float: right" runat="server" id="LeaveApprove">0</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="widget" runat="server" id="blockTimeSheet" visible="false">
                        <div class="widget-head">
                            <h4 class="heading glyphicons history">
                                <i></i>Timesheet</h4>
                            <a href="TimeSheet/MyTimeSheetList.aspx" class="details pull-right">View All</a>
                        </div>
                        <div class="widget-body list">
                            <ul>
                                <li runat="server" id="TimesheetRecommendLi"><span>Pending</span> <span runat="server"
                                    id="TimesheetRecommend" class="count">0</span> </li>
                                <li runat="server" id="TimesheetAppproveLi"><span>Recommended</span> <span class="count"
                                    runat="server" id="TimesheetApprove">0</span> </li>
                            </ul>
                        </div>
                    </div>
                    <div class="widget" runat="server" id="blockExpenseClaim" visible="false">
                        <div class="widget-head">
                            <h4 class="heading glyphicons history">
                                <i></i>Expense Claim</h4>
                            <a href="#" class="details pull-right">View All</a>
                        </div>
                        <div class="widget-body list">
                            <ul>
                                <li runat="server" id="Li6"><span>Pending</span> <span runat="server" id="Span6"
                                    class="count">0</span> </li>
                                <li runat="server" id="Li7"><span>Recommended</span> <span class="count" runat="server"
                                    id="Span7">0</span> </li>
                            </ul>
                        </div>
                    </div>
                    <%--<div class="widget" runat="server" id="blockTimeAttendance" visible="true">
                        <div class="widget-head">
                            <h4 class="heading glyphicons history">
                                <i></i>Time Attendance</h4>
                            <a href="ApproveTimeAttendance.aspx" class="details pull-right">View All</a>
                        </div>
                        <div class="widget-body list">
                            <ul>
                                <li runat="server" id="timeAttendRecommendedLi"><span style="float: left">Pending</span>
                                    <span style="float: right" runat="server" id="timeAttendRecommended" class="count">0</span>
                                </li>
                            </ul>
                        </div>
                    </div>--%>
                    <div class="widget" runat="server" id="blockComment" visible="true">
                        <div class="widget-head">
                            <h4 class="heading glyphicons history">
                                <i></i>Late Attendance Request</h4>
                            <a href="ApproveTimeAttendanceComment.aspx" class="details pull-right">View All</a>
                        </div>
                        <div class="widget-body list">
                            <ul>
                                <li runat="server" id="Li2"><span style="float: left">Pending</span> <span style="float: right"
                                    runat="server" id="commentCount" class="count">0</span> </li>
                            </ul>
                        </div>
                    </div>
                </td>
            </tr>
        </table>
    </div>
    <ext:Window ID="WPasswordNotification" runat="server" Title="Password Change Notification"
        Icon="None" Height="150" Width="300" BodyPadding="5" Hidden="true" Modal="true">
        <Content>
            <div style="background-color: White; height: 150px; width: 300px;">
                <br />
                <p style="margin-left: 20px; padding-bottom: 5px;">
                    <b>Your password has expired.</b></p>
                <b><a style="margin-left: 20px; text-decoration: underline;" href="ChangePassword.aspx">
                    Click here</a> to change your password.</b>
            </div>
        </Content>
    </ext:Window>
    <script type="text/javascript">
        var stub = "Your Password has been changed.";
        function ShowMsg() {
            Ext.net.Notification.show({
                iconCls: 'icon-information',
                pinEvent: 'click',
                html: 'Your Password has been changed.',
                title: 'Title'
            });
        }
     
  

    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
