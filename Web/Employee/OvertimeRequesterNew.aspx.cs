﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL.Entity;
using BLL;
using System.IO;
using BLL.BO;
using Utils.Helper;
using Bll;

namespace Web.Employee
{
    public partial class OvertimeRequesterNew : BasePage
    {
       

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest && !IsPostBack)
            {
                Initialize();
            }

            JavascriptHelper.AttachNonDialogPopUpCode(Page, "overtimeImp", "EmployeeOTImport.aspx", 450, 500);
        }

        protected void Initialize()
        {
            cmbOvertimeType.GetStore().DataSource = OvertimeManager.GetOvertimeList();
            cmbOvertimeType.GetStore().DataBind();

            cmbOvertimeTypeAdd.GetStore().DataSource = OvertimeManager.GetOvertimeList();
            cmbOvertimeTypeAdd.GetStore().DataBind();

            List<TextValue> listReview = LeaveRequestManager.GetApplyToListForEmployee(SessionManager.CurrentLoggedInEmployeeId, false, PreDefindFlowType.Overtime);

            List<TextValue> listApproval = LeaveRequestManager.GetApplyToListForEmployee(SessionManager.CurrentLoggedInEmployeeId, true, PreDefindFlowType.Overtime);

            cmbRecommender.GetStore().DataSource = listReview;
            cmbRecommender.GetStore().DataBind();

            cmbRecommenderAdd.GetStore().DataSource = listReview;
            cmbRecommenderAdd.GetStore().DataBind();

            cmbApproval.GetStore().DataSource = listApproval;
            cmbApproval.GetStore().DataBind();

            cmbApprovalAdd.GetStore().DataSource = listApproval;
            cmbApprovalAdd.GetStore().DataBind(); 

            cmbType.SetValue("1");

            LoadEmployees();
        }

        private void Clear()
        {
            cmbOvertimeType.Clear();
            txtDate.Text = "";

            TimeSpan EndTimeSpan = OvertimeManager.getOfficeEndTime(SessionManager.CurrentLoggedInEmployeeId);
            DateTime EndTime = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, EndTimeSpan.Hours, EndTimeSpan.Minutes, EndTimeSpan.Seconds);
            DateTime dt = DateTime.Now;
            TimeSpan tsNow = new TimeSpan(dt.Hour, dt.Minute, dt.Second);

            tfExtraWorkStartAt.Value = EndTimeSpan;
            tfExtraWorkEndAt.Value = tsNow;
            tfExtraWorkStartAt.Enable();
            tfExtraWorkEndAt.Enable();
            txtDate.Enable();
            txtReason.Enable();

            cmbRecommender.SetValue("-1");
            cmbApproval.SetValue("-1");

            txtReason.Text = "";
            btnSave.Text = "Save";
            pnlBorder.Hide();
        }

        public void LoadEmployees()
        {
            int type = 1;


            if (!string.IsNullOrEmpty(cmbType.SelectedItem.Text))
                type = int.Parse(cmbType.SelectedItem.Value);

            int totalRecords = 0;

            List<GetOvertimeRequestByIDResult> list = OvertimeManager.GetLeaveRequestsByEmpID(SessionManager.CurrentLoggedInEmployeeId, type, 1, 99999,-1, -1, ref totalRecords);

            gridOverTimeReq.GetStore().DataSource = list;
            gridOverTimeReq.GetStore().DataBind();
        }

        protected void MyData_Refresh(object sender, StoreReadDataEventArgs e)
        {
            LoadEmployees();
        }

        protected void btnNewRequest_Click(object sender, DirectEventArgs e)
        {
            WOvertimeAdd.Show();
            return;

            //Clear();
            //hdnRequestID.Text = "";
            //WOvertimeReq.Show();
        }

        protected void btnLoad_Click(object sender, DirectEventArgs e)
        {
            LoadEmployees();
        }

        protected void btnEdit_Click(object sender, DirectEventArgs e)
        {
            OvertimeRequest requestInstance = new OvertimeRequest();
            requestInstance = OvertimeManager.getRequestByID(hdnRequestID.Text.Trim());
            if (requestInstance.Status != 0)
            {
                tfExtraWorkStartAt.Disable();
                tfExtraWorkEndAt.Disable();

                txtReason.Enabled = false;
                btnSave.Enabled = false;
                txtDate.Disable();
                btnSave.Hide();
                txtReason.Disable();

                pnlBorder.Show();
                lblWarningMsg.Text = "Status has been changed. This request cannot be edited.";
                lblWarningMsg.Show();
            }

            txtDate.Disable();
            btnSave.Text = "Update";

            DateTime startDateTime = requestInstance.StartTime.Value;
            DateTime endDateTime = requestInstance.EndTime.Value;

            TimeSpan tsStart = new TimeSpan(startDateTime.Hour, startDateTime.Minute, startDateTime.Second);
            TimeSpan tsEnd = new TimeSpan(endDateTime.Hour, endDateTime.Minute, endDateTime.Second);

            tfExtraWorkStartAt.Value = tsStart;
            tfExtraWorkEndAt.Value = tsEnd;

            txtReason.Text = requestInstance.Reason;

            cmbRecommender.SetValue(requestInstance.RecommendedBy.ToString());
            cmbApproval.SetValue(requestInstance.ApprovalID.ToString());

            if (requestInstance.OvertimeTypeId != null)
                cmbOvertimeType.SetValue(requestInstance.OvertimeTypeId.Value.ToString());

            txtDate.SelectedDate = requestInstance.Date.Value;

            WOvertimeReq.Show();
        }

        protected void btnSave_Click(object sender, DirectEventArgs e)
        {
            Page.Validate("SaveOvertimeReq");
            if (Page.IsValid)
            {
                bool isEdit = false;
                OvertimeRequest requestInstance = new OvertimeRequest();
                if (!string.IsNullOrEmpty(hdnRequestID.Text.Trim()))
                {
                    requestInstance.OvertimeRequestID = int.Parse(hdnRequestID.Text.Trim());
                    isEdit = true;
                }

                requestInstance.OvertimeTypeId = int.Parse(cmbOvertimeType.SelectedItem.Value);

                DateTime dt = txtDate.SelectedDate;
                requestInstance.Date = dt;
                requestInstance.EmployeeID = SessionManager.CurrentLoggedInEmployeeId;

                requestInstance.StartTime = new DateTime(dt.Year, dt.Month, dt.Day, tfExtraWorkStartAt.SelectedTime.Hours, tfExtraWorkStartAt.SelectedTime.Minutes, tfExtraWorkStartAt.SelectedTime.Seconds);
                requestInstance.EndTime = new DateTime(dt.Year, dt.Month, dt.Day, tfExtraWorkEndAt.SelectedTime.Hours, tfExtraWorkEndAt.SelectedTime.Minutes, tfExtraWorkEndAt.SelectedTime.Seconds);
                requestInstance.Reason = txtReason.Text;
                requestInstance.Status = (int)OvertimeStatusEnum.Pending;
                requestInstance.ApprovalID = int.Parse(cmbApproval.SelectedItem.Value);
                requestInstance.ApprovalName = cmbApproval.SelectedItem.Text;
                requestInstance.RecommendedBy = int.Parse(cmbRecommender.SelectedItem.Value);
                ResponseStatus isSuccess = new ResponseStatus();

                ResponseStatus isValidDate = OvertimeManager.isRequestedDateValid(requestInstance.EmployeeID, requestInstance.Date, requestInstance.StartTime, requestInstance.EndTime, isEdit);

                if (!OvertimeManager.CanEmployeeRequestForOverTime(requestInstance.EmployeeID.Value, requestInstance.OvertimeTypeId.Value, requestInstance.Date.Value))
                {
                    NewMessage.ShowWarningMessage("Employee is not eligible for " + cmbOvertimeType.SelectedItem.Text + ".");
                    return;
                }

                int minimumOvertimeBuffer = OvertimeManager.GetOvertimeMinimumBufferTime(requestInstance.EmployeeID.Value, requestInstance.Date.Value);

                if (minimumOvertimeBuffer != 0)
                {
                    TimeSpan EndTimeSpan = OvertimeManager.getOfficeEndTime(SessionManager.CurrentLoggedInEmployeeId);

                    TimeSpan ts = tfExtraWorkStartAt.SelectedTime - EndTimeSpan;

                    if (ts.TotalMinutes < minimumOvertimeBuffer)
                    {
                        NewMessage.ShowWarningMessage("Minimum buffer time after regual shift is " + minimumOvertimeBuffer + " minutes.");
                        tfExtraWorkStartAt.Focus();
                        return;
                    }
                }
                

                requestInstance.NepaliDate = txtDate.SelectedDate.ToShortDateString().Replace('-', '/');

                if (isValidDate.IsSuccessType)
                {                 
                    isSuccess = OvertimeManager.InsertUpdateRequest(requestInstance);
                    if (isSuccess.IsSuccessType)
                    {
                        if (isEdit == false)
                            NewMessage.ShowNormalMessage("Overtime request has been saved.");
                        else
                            NewMessage.ShowNormalMessage("Overtime request has been updated.");

                        WOvertimeReq.Close();

                        LoadEmployees();
                    }
                    else
                    {
                        NewMessage.ShowWarningMessage(isSuccess.ErrorMessage);
                    }
                }
                else
                {
                    NewMessage.ShowWarningMessage(isValidDate.ErrorMessage);
                }
            }
            else
            {
                NewMessage.ShowWarningMessage("Validation fail.");
            }
            
        }

        public void btnExport_Click(object sender, EventArgs e)
        {
            int type = 1;

            if (!string.IsNullOrEmpty(cmbType.SelectedItem.Text))
                type = int.Parse(cmbType.SelectedItem.Value);

            int totalRecords = 0;

            List<GetOvertimeRequestByIDResult> list = OvertimeManager.GetLeaveRequestsByEmpID(SessionManager.CurrentLoggedInEmployeeId, type, 1, 99999, -1, -1, ref totalRecords);

            Dictionary<string, string> title = new Dictionary<string, string>();
            title["Overtime Request Report"] = "";

            ExcelHelper.ExportToExcel("Overtime Request", list,
                new List<String>() { "RequestID", "EmployeeID", "SupervisorName", "SupervisorID", "Status", "Duration", "TotalRows", "RowNumber" },
                new List<String>() { },
                new Dictionary<string, string>() { { "StartTime", "Start Time" }, { "EndTime", "End Time" }, { "CheckInTime", "In Time" }, { "CheckOutTime", "Out Time" }, { "DurationModified", "Duration" }, { "ApprovedTime", "Approved Time" },{"StatusModified","Status"} },
                new List<string>() { },
                new Dictionary<string, string>() { },
                new List<string> { "Date", "StartTime", "EndTime", "CheckInTime", "CheckOutTime", "DurationModified", "ApprovedTime", "Reason", "StatusModified"});
        }

        protected void btnSaveOTAdd_Click(object sender, DirectEventArgs e)
        {

            Page.Validate("SaveOTAdd");
            if (Page.IsValid)
            {
                string jsonItems = e.ExtraParams["gridItems"];
                List<OverTimeCls> timeRequestLines = JSON.Deserialize<List<OverTimeCls>>(jsonItems);

                List<OvertimeRequest> listOvertimeRequest = new List<OvertimeRequest>();

                foreach (var item in timeRequestLines)
                {
                    bool isEdit = false;
                    OvertimeRequest requestInstance = new OvertimeRequest();

                    requestInstance.OvertimeTypeId = int.Parse(cmbOvertimeTypeAdd.SelectedItem.Value);

                    TimeSpan tsStartTime, tsEndTime;
                    DateTime dt;

                    if (item.DateEng != null)
                    {
                        try
                        {
                            dt = item.DateEng;
                        }
                        catch (Exception ex)
                        {
                            NewMessage.ShowWarningMessage("Valid Date is required.");
                            return;
                        }
                    }
                    else
                        continue;


                    requestInstance.Date = dt;
                    requestInstance.EmployeeID = SessionManager.CurrentLoggedInEmployeeId;

                    if (!string.IsNullOrEmpty(item.InTimeString))
                    {
                        try
                        {

                            string[] splInTime = item.InTimeString.Split(' ');

                            if (splInTime.ToList().Count > 3)
                                tsStartTime = TimeSpan.Parse(splInTime[4].ToString());
                            else
                            {
                                tsStartTime = TimeSpan.Parse(splInTime[1].ToString());
                                if (item.InTimeString.ToLower().Contains("pm"))
                                {
                                    TimeSpan addHour = TimeSpan.FromHours(12);
                                    tsStartTime = tsStartTime.Add(addHour);
                                }
                            }


                            requestInstance.StartTime = new DateTime(dt.Year, dt.Month, dt.Day, tsStartTime.Hours, tsStartTime.Minutes, 0);

                        }
                        catch (Exception ex)
                        {
                            NewMessage.ShowWarningMessage("Valid Start time is required.");
                            return;
                        }
                    }
                    else
                    {
                        NewMessage.ShowWarningMessage("Start time is required.");
                        return;
                    }


                    if (!string.IsNullOrEmpty(item.OutTimeString))
                    {
                        try
                        {
                            string[] splitOutTime = item.OutTimeString.Split(' ');
                            if (splitOutTime.ToList().Count > 3)
                                tsEndTime = TimeSpan.Parse(splitOutTime[4].ToString());
                            else
                            {
                                tsEndTime = TimeSpan.Parse(splitOutTime[1].ToString());
                                if (item.OutTimeString.ToLower().Contains("pm"))
                                {
                                    TimeSpan addHour = TimeSpan.FromHours(12);
                                    tsEndTime = tsEndTime.Add(addHour);
                                }
                            }

                            requestInstance.EndTime = new DateTime(dt.Year, dt.Month, dt.Day, tsEndTime.Hours, tsEndTime.Minutes, 0);
                        }
                        catch (Exception ex)
                        {
                            NewMessage.ShowWarningMessage("Valid Out time is required.");
                            return;
                        }
                    }
                    else
                    {
                        NewMessage.ShowWarningMessage("End time is required.");
                        return;
                    }

                    if (string.IsNullOrEmpty(item.OutNote))
                    {
                        NewMessage.ShowWarningMessage("Reason is required.");
                        return;
                    }
                    requestInstance.Reason = item.OutNote;

                    requestInstance.Status = (int)OvertimeStatusEnum.Pending;
                    requestInstance.ApprovalID = int.Parse(cmbApprovalAdd.SelectedItem.Value);
                    requestInstance.ApprovalName = cmbApprovalAdd.SelectedItem.Text;
                    requestInstance.RecommendedBy = int.Parse(cmbRecommenderAdd.SelectedItem.Value);
                    ResponseStatus isSuccess = new ResponseStatus();

                    ResponseStatus isValidDate = OvertimeManager.isRequestedDateValid(requestInstance.EmployeeID, requestInstance.Date, requestInstance.StartTime, requestInstance.EndTime, isEdit);

                    if (!OvertimeManager.CanEmployeeRequestForOverTime(requestInstance.EmployeeID.Value, requestInstance.OvertimeTypeId.Value, requestInstance.Date.Value))
                    {
                        NewMessage.ShowWarningMessage("Employee is not eligible for " + cmbOvertimeTypeAdd.SelectedItem.Text + ".");
                        return;
                    }

                    int minimumOvertimeBuffer = OvertimeManager.GetOvertimeMinimumBufferTime(requestInstance.EmployeeID.Value, requestInstance.Date.Value);

                    if (minimumOvertimeBuffer != 0)
                    {
                        TimeSpan EndTimeSpan = OvertimeManager.getOfficeEndTime(SessionManager.CurrentLoggedInEmployeeId);

                        TimeSpan ts = tsStartTime - EndTimeSpan;

                        if (ts.TotalMinutes < minimumOvertimeBuffer)
                        {
                            //NewMessage.ShowWarningMessage("Minimum buffer time after regual shift is " + minimumOvertimeBuffer + " minutes.");
                            NewMessage.ShowWarningMessage("You cannot apply overtime earlier than " + minimumOvertimeBuffer + " minutes from end of office time.");
                            return;
                        }
                    }


                    requestInstance.NepaliDate = requestInstance.Date.Value.ToShortDateString().Replace('-', '/');

                    if (isValidDate.IsSuccessType)
                    {
                        listOvertimeRequest.Add(requestInstance);
                    }
                    else
                    {
                        NewMessage.ShowWarningMessage(isValidDate.ErrorMessage);
                        return;
                    }
                }

                Status status = OvertimeManager.SaveOvertimeRequestList(listOvertimeRequest);
                if (status.IsSuccess)
                {
                    WOvertimeAdd.Close();
                    NewMessage.ShowNormalMessage("Overtime requests saved successfully.");
                    LoadEmployees();
                }
                else
                {
                    NewMessage.ShowWarningMessage(status.ErrorMessage);
                }
            }

        }

        protected void btnLoadRef_Click(object sender, DirectEventArgs e)
        {
            WOvertimeAdd.Close();
            LoadEmployees();
        }

        
       
    }

    public class OverTimeCls
    {
        public int SN { get; set; }
        public DateTime DateEng { get; set; }
        public string InTimeString { get; set; }
        public string OutTimeString { get; set; }
        public string WorkHours { get; set; }
        public string OutNote { get; set; }
    }
}