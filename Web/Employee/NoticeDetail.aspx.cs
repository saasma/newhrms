﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL;
using BLL.Manager;
using Web.ReportDataSetTableAdapters;
using Web.Master;
using BLL;
using Web.CP.Report.Templates.Pay.Detail;
using Utils;
using BLL.Base;
using System.IO;
using Utils.Calendar;
using BLL.BO;
using Ext.Net;

namespace Web.Employee
{
    public partial class NoticeDetail : BasePage
    {
      
        protected void Page_Load(object sender, EventArgs e)
        {



            if (!IsPostBack && ! X.IsAjaxRequest)
            {
                Initialise();
            }


            
        }


        private void Initialise()
        {
            LoadEditData();
        }

        void LoadEditData()
        {
            int NoticeID = int.Parse(Request.QueryString["id"]);

            NoticeBoard notice = NoticeManager.getNoticeByID(NoticeID);

            if (notice != null)
            {
                txtTitle.Text = notice.Title;
                
                if(notice.PublishDate!=null)
                {
                    if(notice.PublishDate.Value.Date == CommonManager.GetCurrentDateAndTime().Date)
                    {
                        txtPublishedDate.Text = "Today";

                    }
                    else 
                    {
                        txtPublishedDate.Text = (CommonManager.GetCurrentDateAndTime().Date - notice.PublishDate.Value.Date).TotalDays + " Days ago";

                    }
                }

                if (notice.CategoryID != null)
                {
                    txtCategory.Text = NoticeManager.GetNoticeCategoryByID(notice.CategoryID).Name+" Notice";
                }

                txtBody.Text = notice.Body;

                if (!string.IsNullOrEmpty(notice.FileName) && !string.IsNullOrEmpty(notice.URL))
                {
                    btnFile.Text = notice.FileName;
                    HiddenFileName.Text = notice.FileName;
                    HiddenFileURL.Text = notice.URL;
                }
                else
                {
                    btnFile.Visible = false;
                    imageAttach.Visible = false;
                }

            }
        }

        public void DownloadFile_Click(object sender, DirectEventArgs e)
        {
            
                if (!string.IsNullOrEmpty(HiddenFileURL.Text))
                    Response.Redirect("~/NoticeFileDownloader.ashx?id=" + System.IO.Path.GetFileNameWithoutExtension(HiddenFileURL.Text).ToString());
                else
                    NewMessage.ShowWarningMessage("This file does not exist.");
           
        }
        
        
    }
}
