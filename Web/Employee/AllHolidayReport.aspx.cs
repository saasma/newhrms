﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL;
using BLL.Manager;
using Web.ReportDataSetTableAdapters;
using Web.Master;
using BLL;
using Web.CP.Report.Templates.Pay.Detail;
using Utils;
using BLL.Base;
using System.IO;
using Utils.Calendar;
using BLL.BO;
using Ext.Net;

namespace Web.Employee
{
    public partial class AllHolidayReport : BasePage
    {
      
        protected void Page_Load(object sender, EventArgs e)
        {



            if (!IsPostBack && ! X.IsAjaxRequest)
            {
                Initialise();
            }


            
        }


        private void Initialise()
        {

/*            DateTime todayDate = new DateTime();
            todayDate = CommonManager.GetCurrentDateAndTime();
            HolidayClass service = new HolidayClass();
            List<GetHolidaysForAttendenceResult> bindList = new List<GetHolidaysForAttendenceResult>();
            DateTime FromDate = new DateTime();
            DateTime ToDate = new DateTime();
            DateTime TodayDate = new DateTime();

            TodayDate = CommonManager.GetCurrentDateAndTime();

            FromDate = new DateTime(todayDate.Year, 1, 1);
            ToDate = new DateTime(todayDate.Year, 12, DateTime.DaysInMonth(todayDate.Year, 12), 23, 59, 59);

            bindList = service.GetHolidays(FromDate, ToDate);
            bindList = bindList.Where(x => x.IsWeekly != true && x.Type != 0).ToList();
            foreach (var item in bindList)
            {
                if (item.DateEng != null)
                {
                    item.EngDateLongForDashboard = item.DateEng.Value.ToLongDateString();
                    item.DaysLeftForDashboard = "In " + (item.DateEng.Value.Date - CommonManager.GetCurrentDateAndTime().Date).Days.ToString() + " Days";
                    item.NepDateLongForDashboard = item.DateEngText;

                    CustomDate nepalidate = new CustomDate(item.DateEng.Value.Day, item.DateEng.Value.Month, item.DateEng.Value.Year, true); //DateManager.GetTodayNepaliDate();
                    nepalidate = CustomDate.ConvertEngToNep(nepalidate);

                    string[] yearNos = new string[4];
                    yearNos[0] = nepalidate.Year.ToString()[0].ToString();
                    yearNos[1] = nepalidate.Year.ToString()[1].ToString();
                    yearNos[2] = nepalidate.Year.ToString()[2].ToString();
                    yearNos[3] = nepalidate.Year.ToString()[3].ToString();

                    string[] dayNos = new string[2];
                    dayNos[0] = "";
                    dayNos[1] = "";


                    dayNos[0] = nepalidate.Day.ToString()[0].ToString();

                    item.NepDateLongForDashboard = Resources.NepaliCalendar.ResourceManager.GetString("N" + dayNos[0].ToString());
                    if (nepalidate.Day.ToString().Length >= 2)
                    {
                        dayNos[1] = nepalidate.Day.ToString()[1].ToString();
                        item.NepDateLongForDashboard += Resources.NepaliCalendar.ResourceManager.GetString("N" + dayNos[1].ToString());
                    }

                    item.NepDateLongForDashboard += " " + Resources.NepaliCalendar.ResourceManager.GetString(DateHelper.GetMonthName(nepalidate.Month, false))
                        + ", " +

                        Resources.NepaliCalendar.ResourceManager.GetString("N" + yearNos[0]) +
                        Resources.NepaliCalendar.ResourceManager.GetString("N" + yearNos[1]) +
                        Resources.NepaliCalendar.ResourceManager.GetString("N" + yearNos[2]) +
                        Resources.NepaliCalendar.ResourceManager.GetString("N" + yearNos[3])
                        ;

                }
            }


            storeMYTimesheet.DataSource = bindList;
            storeMYTimesheet.DataBind();
            */


            try
            {

                DateTime todayDate = new DateTime();
                todayDate = CommonManager.GetCurrentDateAndTime();
                HolidayClass service = new HolidayClass();
                List<GetHolidaysForAttendenceResult> bindList = new List<GetHolidaysForAttendenceResult>();

                DateTime FromDate = new DateTime();
                DateTime ToDate = new DateTime();
                DateTime TodayDate = new DateTime();

                TodayDate = CommonManager.GetCurrentDateAndTime();

                FromDate = new DateTime(todayDate.Year, 1, 1);
                ToDate = new DateTime(todayDate.Year + 10, 12, DateTime.DaysInMonth(todayDate.Year, 12), 23, 59, 59);

                bindList = service.GetHolidays(FromDate, ToDate);
                bindList = bindList.Where(x => x.IsWeekly != true && x.Type != 0).ToList();
               

                List<GetHolidaysForAttendenceResult> list = new List<GetHolidaysForAttendenceResult>();

                foreach (var item in bindList)
                {
                    if (item.DateEng != null)
                    {
                        item.EngDateLongForDashboard = item.DateEng.Value.ToString("ddd dd MMM yyyy");
                        int days = (item.DateEng.Value.Date - CommonManager.GetCurrentDateAndTime().Date).Days;
                        if (days < 0)
                            continue;

                        list.Add(item);

                        item.DaysLeftForDashboard = "In " + days.ToString() + " Days";
                        item.NepDateLongForDashboard = item.DateEngText;

                        CustomDate nepalidate = new CustomDate(item.DateEng.Value.Day, item.DateEng.Value.Month, item.DateEng.Value.Year, true); //DateManager.GetTodayNepaliDate();
                        nepalidate = CustomDate.ConvertEngToNep(nepalidate);

                        string[] yearNos = new string[4];
                        yearNos[0] = nepalidate.Year.ToString()[0].ToString();
                        yearNos[1] = nepalidate.Year.ToString()[1].ToString();
                        yearNos[2] = nepalidate.Year.ToString()[2].ToString();
                        yearNos[3] = nepalidate.Year.ToString()[3].ToString();

                        string[] dayNos = new string[2];
                        dayNos[0] = "";
                        dayNos[1] = "";


                        dayNos[0] = nepalidate.Day.ToString()[0].ToString();

                        item.NepDateLongForDashboard = Resources.NepaliCalendar.ResourceManager.GetString("N" + dayNos[0].ToString());
                        if (nepalidate.Day.ToString().Length >= 2)
                        {
                            dayNos[1] = nepalidate.Day.ToString()[1].ToString();
                            item.NepDateLongForDashboard += Resources.NepaliCalendar.ResourceManager.GetString("N" + dayNos[1].ToString());
                        }

                        item.NepDateLongForDashboard += " " + Resources.NepaliCalendar.ResourceManager.GetString(DateHelper.GetMonthName(nepalidate.Month, false))
                            + ", " +

                            Resources.NepaliCalendar.ResourceManager.GetString("N" + yearNos[0]) +
                            Resources.NepaliCalendar.ResourceManager.GetString("N" + yearNos[1]) +
                            Resources.NepaliCalendar.ResourceManager.GetString("N" + yearNos[2]) +
                            Resources.NepaliCalendar.ResourceManager.GetString("N" + yearNos[3])
                            ;

                    }
                }


                storeMYTimesheet.DataSource = list;
                storeMYTimesheet.DataBind();

            }
            catch (Exception exp)
            {
                Log.log("Employee dashboard error", exp);
            }


            CommonManager mgr = new CommonManager();

            int empId = SessionManager.CurrentLoggedInEmployeeId;
            EEmployee emp = EmployeeManager.GetEmployeeById(empId);

            if (emp == null)
                return;

           

        }

        
     
    }
}
