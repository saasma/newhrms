﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL.Entity;
using BLL;
using System.IO;
using BLL.BO;
using Utils.Helper;
using Bll;
using System.Text.RegularExpressions;

namespace Web.Employee
{
    public partial class AApproveOvertimeNew : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {          

            if (!X.IsAjaxRequest && !IsPostBack)
            {
                Initialize();

                if (LeaveAttendanceManager.IsEmployeeAllowedToApproveLeave(SessionManager.CurrentLoggedInEmployeeId))
                    btnRecommendOrApprove.Text = "Approve";
            }

            JavascriptHelper.AttachNonDialogPopUpCode(Page, "assignovertimeImp", "SupOTImport.aspx", 450, 500);
        }

        private void Initialize()
        {
            cmbOvertimeType.GetStore().DataSource = OvertimeManager.GetOvertimeList();
            cmbOvertimeType.GetStore().DataBind();            

            cmbEmployeeAdd.GetStore().DataSource = LeaveAttendanceManager.GetEmployeeListForLeaveAssign();
            cmbEmployeeAdd.GetStore().DataBind();

            if (LeaveAttendanceManager.IsEmployeeAllowedToApproveOvertime())
            {
                cmbStatus.SelectedItem.Value = "1";
            }

            LoadGrid();

        }

        private void Clear()
        {
            cmbOvertimeType.SelectedItem.Text = "";
            cmbOvertimeType.Clear();
        }

        public bool CanRecommendOrApprove(int status, int employeeId)
        {
            if (status == (int)OvertimeStatusEnum.Recommended &&
                LeaveRequestManager.CanApprove(employeeId, SessionManager.CurrentLoggedInEmployeeId, PreDefindFlowType.Overtime))
                return true;

            if (status == 0)
                return true;


            return false;
        }

        protected void LoadGrid()
        {
            int type = 1;
            int status = -1;

            if (!string.IsNullOrEmpty(cmbType.SelectedItem.Text))
                type = int.Parse(cmbType.SelectedItem.Value);

            if (!string.IsNullOrEmpty(cmbStatus.SelectedItem.Text))
                status = int.Parse(cmbStatus.SelectedItem.Value);

            int totalRecords = 0;

            List<GetOvertimeRequestForManagerNewResult> list = new List<GetOvertimeRequestForManagerNewResult>();
            list = OvertimeManager.GetLeaveRequestsForManagerNew(type, status, 1, 999999, ref totalRecords);

            //CanRecommendOrApprove(int status, int employeeId)
            foreach (var item in list)
            {
                item.CanRecOrAppRec = CanRecommendOrApprove(item.Status.Value, item.EmployeeID.Value);
            }

            gridOverTimeReq.GetStore().DataSource = list;
            gridOverTimeReq.GetStore().DataBind();
        }

        


        protected void btnLoad_Click(object sender, DirectEventArgs e)
        {
            LoadGrid();
        }

        protected void MyData_Refresh(object sender, StoreReadDataEventArgs e)
        {
            LoadGrid();
        }

        //Assign OverTime
        protected void btnAssignOT_Click(object sender, DirectEventArgs e)
        {
            Clear();
            hdnRequestID.Text = "";
            gridOTList.Store[0].DataSource = new List<OverTimeClsWithEmp>();
            gridOTList.Store[0].DataBind();
            WAssignOvertime.Center();
            WAssignOvertime.Show();          
        }

        protected void btnLoadGrid_Click(object sender, DirectEventArgs e)
        {
            if (Session["AssignOTImp"] != null)
            {
                List<OverTimeClsWithEmp> list = (List<OverTimeClsWithEmp>)Session["AssignOTImp"];

                if (list.Count > 0)
                {
                    DateTime dt = DateTime.Now;

                    gridOTList.GetStore().DataSource = list;
                    gridOTList.GetStore().DataBind();
                }

            }
        }

        //Save Assign overtime
        protected void btnAssignOvertime_Click(object sender, DirectEventArgs e)
        {
            if (string.IsNullOrEmpty(cmbOvertimeType.Text))
            {
                NewMessage.ShowWarningMessage("Overtime type id required.");
                btnAssignOvertime.Enable();
                return;
            }

            Page.Validate("SaveAssignOvertime");
            if (Page.IsValid)
            {
                int overtimeTpyeId = int.Parse(cmbOvertimeType.SelectedItem.Value);

                string jsonItems = e.ExtraParams["gridItems"];
                List<OverTimeClsWithEmp> timeRequestLines = JSON.Deserialize<List<OverTimeClsWithEmp>>(jsonItems);

                if (timeRequestLines.Count == 0)
                {
                    NewMessage.ShowWarningMessage("Please add/import overtime lists to be assigned.");
                    return;
                }

                List<OvertimeRequest> list = new List<OvertimeRequest>();
                int count = 1;

                TextValue objTextValue = new TextValue() { Text = EmployeeManager.GetEmployeeById(SessionManager.CurrentLoggedInEmployeeId).Name, Value = SessionManager.CurrentLoggedInEmployeeId.ToString() };

                foreach (var item in timeRequestLines)
                {
                    OvertimeRequest obj = new OvertimeRequest();

                    bool isEdit = false;
                    OvertimeRequest requestInstance = new OvertimeRequest();

                    requestInstance.OvertimeTypeId = overtimeTpyeId;

                    TimeSpan tsStartTime, tsEndTime;
                    DateTime dt;

                    if (item.DateEng != null && item.DateEng != new DateTime())
                    {
                        try
                        {
                            dt = item.DateEng;
                        }
                        catch (Exception ex)
                        {
                            NewMessage.ShowWarningMessage("Valid Date is required for the row " + count.ToString());
                            return;
                        }
                    }
                    else
                    {
                        NewMessage.ShowWarningMessage("Date is required for the row " + count.ToString());
                        return;
                    }


                    requestInstance.Date = dt;
                    if (!string.IsNullOrEmpty(item.Value))
                        requestInstance.EmployeeID = int.Parse(item.Value);
                    else
                    {
                        NewMessage.ShowWarningMessage("Please select employee for the row " + count.ToString());
                        return;
                    }

                    if (item.InTime != null && item.InTime != new DateTime())
                    {
                        tsStartTime = new TimeSpan(item.InTime.Hour, item.InTime.Minute, 0);
                        requestInstance.StartTime = new DateTime(dt.Year, dt.Month, dt.Day, tsStartTime.Hours, tsStartTime.Minutes, 0);
                    }
                    else
                    {
                        NewMessage.ShowWarningMessage("Start time is required for the row " + count.ToString());
                        return;
                    }

                    if (item.OutTime != null && item.OutTime != new DateTime())
                    {
                        tsEndTime = new TimeSpan(item.OutTime.Hour, item.OutTime.Minute, 0);
                        requestInstance.EndTime = new DateTime(dt.Year, dt.Month, dt.Day, tsEndTime.Hours, tsEndTime.Minutes, 0);
                    }
                    else
                    {
                        NewMessage.ShowWarningMessage("End time is required for the row " + count.ToString());
                        return;
                    }

                    if (string.IsNullOrEmpty(item.OutNote))
                    {
                        NewMessage.ShowWarningMessage("Reason is required for the row " + count.ToString());
                        return;
                    }
                    requestInstance.Reason = item.OutNote;


                    List<TextValue> listReview = LeaveRequestManager.GetApplyToListForEmployee(requestInstance.EmployeeID.Value, false, PreDefindFlowType.Overtime);
                    List<TextValue> listApproval = LeaveRequestManager.GetApplyToListForEmployee(requestInstance.EmployeeID.Value, true, PreDefindFlowType.Overtime);

                    if (listApproval.Count > 0)
                    {
                        foreach (var approvalItem in listApproval)
                        {
                            if (approvalItem.Value == objTextValue.Value)
                            {
                                requestInstance.Status = (int)OvertimeStatusEnum.Approved;
                                requestInstance.ApprovalID = SessionManager.CurrentLoggedInEmployeeId;
                                requestInstance.ApprovalName = EmployeeManager.GetEmployeeById(SessionManager.CurrentLoggedInEmployeeId).Name;
                                requestInstance.ApprovedOn = DateTime.Now;
                                requestInstance.ApprovedTime = DateTime.Now.ToShortTimeString();
                            
                            }
                        }

                        
                    }

                    if (listReview.Count > 0 && listApproval.Count == 0)
                    {
                        foreach (var recommenderItem in listReview)
                        {
                            if (recommenderItem.Value == objTextValue.Value)
                            {
                                requestInstance.Status = (int)OvertimeStatusEnum.Recommended;
                                requestInstance.RecommendedBy = SessionManager.CurrentLoggedInEmployeeId;
                                requestInstance.RecommendedOn = DateTime.Now;
                            }
                        }

                    }

                    

                    ResponseStatus isSuccess = new ResponseStatus();

                    ResponseStatus isValidDate = OvertimeManager.isRequestedDateValid(requestInstance.EmployeeID, requestInstance.Date, requestInstance.StartTime, requestInstance.EndTime, isEdit);

                    if (!OvertimeManager.CanEmployeeRequestForOverTime(requestInstance.EmployeeID.Value, requestInstance.OvertimeTypeId.Value, requestInstance.Date.Value))
                    {
                        NewMessage.ShowWarningMessage("Employee is not eligible for " + cmbOvertimeType.SelectedItem.Text + " for the row " + count.ToString());
                        return;
                    }

                    int minimumOvertimeBuffer = OvertimeManager.GetOvertimeMinimumBufferTime(requestInstance.EmployeeID.Value, requestInstance.Date.Value);

                    if (minimumOvertimeBuffer != 0)
                    {
                        TimeSpan EndTimeSpan = OvertimeManager.getOfficeEndTime(SessionManager.CurrentLoggedInEmployeeId);

                        TimeSpan ts = tsStartTime - EndTimeSpan;

                        if (ts.TotalMinutes < minimumOvertimeBuffer)
                        {
                            NewMessage.ShowWarningMessage("You cannot assign overtime earlier than " + minimumOvertimeBuffer + " minutes from end of office time for the row " + count.ToString());
                            return;
                        }
                    }

                    requestInstance.NepaliDate = requestInstance.Date.Value.ToShortDateString().Replace('-', '/');

                    count++;

                    if (isValidDate.IsSuccessType)
                    {
                        list.Add(requestInstance);
                    }
                    else
                    {
                        NewMessage.ShowWarningMessage(isValidDate.ErrorMessage);
                        return;
                    }
                }

                Status status = OvertimeManager.AssignOvertimes(list);
                if (status.IsSuccess)
                {
                    NewMessage.ShowNormalMessage("Overtime requests assigned successfully.");
                    WAssignOvertime.Close();
                    LoadGrid();
                }
                else
                {
                    NewMessage.ShowWarningMessage(status.ErrorMessage);
                }

            }
        }



        //AApprove
        protected void btnEdit_Click(object sender, DirectEventArgs e)
        {
            //code from AAOvertimePopup

            string hr;
            string min;
            string hrmin;
            string employeename;

            btnReject.Show();

            OvertimeRequest request = new OvertimeRequest();
            request = OvertimeManager.getRequestByID(hdnRequestID.Text);

            if (request != null)
            {
                gridHistory.GetStore().DataSource = AllowanceManager.GetAllowanceHistory(request.OvertimeRequestID, (int)RequestHistoryTypeEnum.Overtime);
                gridHistory.DataBind();
            }

            min = (request.StartTime.Value.Subtract(request.EndTime.Value).TotalMinutes).ToString();
            employeename = EmployeeManager.GetEmployeeById(request.EmployeeID.Value).Name;

            dfOvertime.Text += " " + employeename;

            dfDate.Text = request.Date.Value.ToShortDateString();
            dfName.Text = employeename;
            dfDescription.Text = request.Reason;

            DateTime? date = null;
            if (AttendanceManager.GetEmployeeCheckInOutTime(request.EmployeeID.Value, request.Date.Value, "CheckIn") != null)
            {
                date = AttendanceManager.GetEmployeeCheckInOutTime(request.EmployeeID.Value, request.Date.Value, "CheckIn");
                if (date != null)
                    dfInTime.Text = date.Value.ToShortTimeString();
            }

            if (AttendanceManager.GetEmployeeCheckInOutTime(request.EmployeeID.Value, request.Date.Value, "CheckOut") != null)
            {
                date = AttendanceManager.GetEmployeeCheckInOutTime(request.EmployeeID.Value, request.Date.Value, "CheckOut");
                if (date != null)
                    dfOutTime.Text = date.Value.ToShortTimeString();
            }


            int minute = Math.Abs(int.Parse(min));

            hrmin = new AttendanceManager().MinuteToHourMinuteConverter(minute);
            txtRequestedHour.Text = hrmin;
            Hidden_MinutesPrev.Text = txtRequestedHour.Text;

            hr = hrmin.Split(':').First();
            min = hrmin.Split(':').Last();

            if (request.ApprovedTimeInMinute != null)
            {
                var span = System.TimeSpan.FromMinutes(request.ApprovedTimeInMinute.Value);
                var hours = ((int)span.TotalHours).ToString();
                var minutes = span.Minutes.ToString();

                txtApprovedForHour.Text = hours;
                txtApprovedForMin.Text = minutes;
            }
            else
            {
                txtApprovedForHour.Text = Regex.Match(hr, @"\d+").Value;
                txtApprovedForMin.Text = Regex.Match(min, @"\d+").Value;
            }

            if (request.Status == (int)OvertimeStatusEnum.Pending)
            {
                btnRecommend.Show(); 
                btnApprove.Hide(); 

                if (request.RecommendedBy == request.ApprovalID)
                {
                    btnRecommend.Hide();  
                    btnApprove.Show(); 
                }
            }
            else if (request.Status == (int)OvertimeStatusEnum.Recommended)
            {
                btnRecommend.Hide(); 
                btnApprove.Show();
            }

            if (request.Status == (int)OvertimeStatusEnum.Pending && request.RecommendedBy == SessionManager.CurrentLoggedInEmployeeId)
            {
            }
            else if ((request.Status == (int)OvertimeStatusEnum.Forwarded || request.Status == (int)OvertimeStatusEnum.Rejected)
                ||
                (request.RecommendedBy == SessionManager.CurrentLoggedInEmployeeId && request.ApprovalID != SessionManager.CurrentLoggedInEmployeeId)
                ||
                (request.Status == (int)OvertimeStatusEnum.Approved)
                )
            {

                pnlBorder.Show();
                lblWarningMsg.Text = "Status has been changed. This request cannot be edited.";
                lblWarningMsg.Show();

                txtApprovedForHour.Disable();
                txtApprovedForMin.Disable(); 
                btnApprove.Hide();
                btnRecommend.Hide();
                btnReject.Hide();
            }

            WApproveOvertime.Show();
        }
        
        protected void btnRecommendOrApprove_Click(object sender, DirectEventArgs e)
        {
            string gridItemsJson = e.ExtraParams["gridItems"];
            List<GetOvertimeRequestForManagerResult> list = JSON.Deserialize<List<GetOvertimeRequestForManagerResult>>(gridItemsJson);

            if (list.Count == 0)
                return;

            List<OvertimeRequest> requestList = new List<OvertimeRequest>();

            foreach (var item in list)
            {
                OvertimeRequest request = new OvertimeRequest();
                int RequestID = item.RequestID;

                request.OvertimeRequestID = RequestID;
                requestList.Add(request);
            }

            int count = 0;
            if (OvertimeManager.RecommendOrApprove(requestList, out count))
            {
                LoadGrid();

                NewMessage.ShowNormalMessage(count + " overtime has been forwarded.");
            }
        }

        

        //Approve OverTime
        protected void btnApprove_Click(object sender, DirectEventArgs e)
        {
            Page.Validate("ApproveOvertime");
            if (Page.IsValid)
            {
                OvertimeRequest requestInstance = new OvertimeRequest();

                requestInstance.OvertimeRequestID = int.Parse(hdnRequestID.Text);
                requestInstance.ApprovedTimeInMinute = (int.Parse(txtApprovedForHour.Text) * 60) + int.Parse(txtApprovedForMin.Text);
                requestInstance.ApprovedTime = txtApprovedForHour.Text + " Hr : " + txtApprovedForMin.Text + " Min";
                requestInstance.Status = (int)OvertimeStatusEnum.Approved;

                List<RequestEditHistory> edits = new List<RequestEditHistory>();
                RequestEditHistory edit = new RequestEditHistory();
                if (Hidden_MinutesPrev.Text.Trim().ToLower() != requestInstance.ApprovedTimeInMinute.Value.ToString().Trim().ToLower())
                {
                    edit = new RequestEditHistory();
                    edit.Column1Before = Hidden_MinutesPrev.Text;
                    edit.Column1After = requestInstance.ApprovedTime;
                    edits.Add(edit);
                    Hidden_MinutesPrev.Value = requestInstance.ApprovedTime;
                }

                Status status = OvertimeManager.ApproveRequest(requestInstance, edits);
                if (status.IsSuccess)
                {
                    WApproveOvertime.Close();
                    NewMessage.ShowNormalMessage("Overtime has been approved.");
                }
                else
                {
                    NewMessage.ShowWarningMessage(status.ErrorMessage);
                }
            }
            else
            {
                NewMessage.ShowWarningMessage("Validation Failed");
            }
        }

        protected void btnRecommend_Click(object sender, DirectEventArgs e)
        {
            Page.Validate("ApproveOvertime");
            if (Page.IsValid)
            {
                OvertimeRequest requestInstance = new OvertimeRequest();

                requestInstance.OvertimeRequestID = int.Parse(hdnRequestID.Text);
                requestInstance.ApprovedTimeInMinute = (int.Parse(txtApprovedForHour.Text) * 60) + int.Parse(txtApprovedForMin.Text);
                requestInstance.ApprovedTime = txtApprovedForHour.Text + " Hr : " + txtApprovedForMin.Text + " Min";
                requestInstance.Status = (int)OvertimeStatusEnum.Recommended;

                List<RequestEditHistory> edits = new List<RequestEditHistory>();
                RequestEditHistory edit = new RequestEditHistory();
                if (Hidden_MinutesPrev.Text.Trim().ToLower() != requestInstance.ApprovedTimeInMinute.Value.ToString().Trim().ToLower())
                {
                    edit = new RequestEditHistory();
                    edit.Column1Before = Hidden_MinutesPrev.Text;
                    edit.Column1After = requestInstance.ApprovedTime;
                    edits.Add(edit);
                    Hidden_MinutesPrev.Value = requestInstance.ApprovedTime;
                }

                Status status = OvertimeManager.RecommendRequest(requestInstance, edits);
                if (status.IsSuccess)
                {
                    NewMessage.ShowNormalMessage("Overtime has been recommended.");
                    WApproveOvertime.Close();
                }
                else
                {
                    NewMessage.ShowWarningMessage(status.ErrorMessage);
                }
            }
            else
            {
                NewMessage.ShowWarningMessage("Validation Failed");
            }
        }

        protected void btnReject_Click(object sender, DirectEventArgs e)
        {
            int requestId = int.Parse(hdnRequestID.Text);
            bool isSuccess = OvertimeManager.RejectRequest(requestId);

            if (isSuccess)
            {
                WApproveOvertime.Close();
                NewMessage.ShowNormalMessage("Overtime has been rejected.");
            }
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            int type = 1;
            int status = -1;

            if (!string.IsNullOrEmpty(cmbType.SelectedItem.Text))
                type = int.Parse(cmbType.SelectedItem.Value);

            if (!string.IsNullOrEmpty(cmbStatus.SelectedItem.Text))
                status = int.Parse(cmbStatus.SelectedItem.Value);

            int totalRecords = 0;

            List<GetOvertimeRequestForManagerNewResult> list = new List<GetOvertimeRequestForManagerNewResult>();
            list = OvertimeManager.GetLeaveRequestsForManagerNew(type, status, 1, 999999, ref totalRecords);
            
            Dictionary<string, string> title = new Dictionary<string, string>();
            title["Overtime List Report"] = "";

            ExcelHelper.ExportToExcel("Overtime List", list,
                new List<String>() { "RequestID", "EmployeeID", "CanRecOrAppRec", "SupervisorName", "SupervisorID", "Recommender", "Status", "Duration", "TotalRows", "RowNumber" },
                new List<String>() { },
                new Dictionary<string, string>() { {"EmployeeName", "Employee Name"}, {"StartTime", "Start Time" }, { "EndTime", "End Time" }, { "CheckInTime", "In Time" }, { "CheckOutTime", "Out Time" }, { "DurationModified", "Duration" }, { "ApprovedTime", "Approved Time" }, { "StatusModified", "Status" } },
                new List<string>() { },
                new Dictionary<string, string>() { },
                new List<string> { "Date", "EmployeeName", "StartTime", "EndTime", "CheckInTime", "CheckOutTime", "DurationModified", "ApprovedTime", "Reason", "StatusModified" });
        }


    }

    public class SupOvertimeClass
    {
        public int SN { get; set; }
        public DateTime DateEng { get; set; }
        public string Value { get; set; }
        public string Text { get; set; }
        public string InTimeString { get; set; }
        public string OutTimeString { get; set; }
        public string WorkHours { get; set; }
        public string OutNote { get; set; }
    }


}