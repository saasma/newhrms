﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using BLL;
using System.Data;
using System.IO;
using BLL.Manager;
using DAL;
using BLL.Entity;
using System.Data.OleDb;
using BLL.BO;

namespace Web.Employee
{
    public partial class SupOTImport : BasePage
    {
        

        public bool HasImport
        {
            get
            {
                if (ViewState["import"] == null)
                    return false;
                return Convert.ToBoolean(ViewState["import"]);
            }
            set
            {
                ViewState["import"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!string.IsNullOrEmpty(Request.QueryString["OvertimeType"]))
                    hdnOverType.Text = Request.QueryString["OvertimeType"];
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            //Page.ClientScript.RegisterOnSubmitStatement(this.GetType(), "sdfdsffd", "window.onunload = null;");
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "sdf", "var hasImport = " + (this.HasImport ? "true;" : "false;"), true);
        }




        protected void btnExport_Click(object sender, EventArgs e)
        {
            string template = ("~/App_Data/ExcelTemplate/SupOTExcel.xls");

            ExcelGenerator.ExportSupOvertime(template, hdnOverType.Text + ".xls");

        }


        protected void btnImport_Click(object sender, EventArgs e)
        {
            string template = Server.MapPath("~/App_Data/ExcelTemplate/" + Guid.NewGuid() + ".xls");


            List<TextValue> listTextValue = LeaveAttendanceManager.GetEmployeeListForLeaveAssignWithID();

            List<OverTimeClsWithEmp> list = new List<OverTimeClsWithEmp>();

            if (fupUpload.HasFile && fupUpload.FileName.ToLower().Contains(".xls"))
            {
                fupUpload.SaveAs(template);

                DataTable datatable = ExcelGenerator.ImportFromExcel(template, "select * from [Sheet1$]");

                if (datatable == null)
                {
                    divWarningMsg.InnerHtml = Resources.Messages.HPL_Allowance_ImportFail;
                    divWarningMsg.Hide = false;
                    File.Delete(template);
                    return;
                }
                
                int importedCount = 0;

                try
                {
                    int rowNumber = 0;

                    EEmployee objEmp = new EEmployee();

                    foreach (DataRow row in datatable.Rows)
                    {
                        OverTimeClsWithEmp objOT = new OverTimeClsWithEmp();
                        objOT.SN = rowNumber;

                        rowNumber += 1;


                        if (row["Employee"] != DBNull.Value && row["Employee"].ToString() != "")
                        {
                            try
                            {
                                objOT.Value = listTextValue.SingleOrDefault(x => x.Text.ToLower().Trim() == row["Employee"].ToString().ToLower().Trim()).Value;
                                objOT.Text = row["Employee"].ToString().Trim().Split('-')[0].Trim();
                            }
                            catch (Exception ex)
                            {
                                divWarningMsg.InnerHtml = "Please select proper employee for the row : " + rowNumber;
                                divWarningMsg.Hide = false;
                                return;
                            }
                            
                        }
                        else
                        {
                            divWarningMsg.InnerHtml = "Employee is required for the row : " + rowNumber;
                            divWarningMsg.Hide = false;
                            return;
                        }

                        importedCount += 1;

                        DateTime date = new DateTime();
                        TimeSpan tsStartTime;

                        if (row["Date"] != DBNull.Value && row["Date"].ToString() != "")
                        {
                            try
                            {
                                date = Convert.ToDateTime(row["Date"].ToString());
                                objOT.DateEng = date;
                            }
                            catch
                            {

                                divWarningMsg.InnerHtml = "Date is invalid for the row : " + rowNumber;
                                divWarningMsg.Hide = false;
                                return;
                            }
                        }
                        else
                        {
                            divWarningMsg.InnerHtml = "Date is required for the row : " + rowNumber;
                            divWarningMsg.Hide = false;
                            return;
                        }

                        if (row["Start Time"] != DBNull.Value && row["Start Time"].ToString() != "")
                        {
                            try
                            {
                                //string startTime = row["Start Time"].ToString();
                                //string[] splInTime = startTime.Split(':');
                                //int hoursStart = int.Parse(splInTime[0]);
                                //int minutesStart = int.Parse(splInTime[1]);

                                //objOT.InTime = new DateTime(date.Year, date.Month, date.Day, hoursStart, minutesStart, 0);
                                //tsStartTime = new TimeSpan(hoursStart, minutesStart, 0);

                                DateTime dtStart = DateTime.Parse(row["Start Time"].ToString());
                                int hoursStart = dtStart.Hour;
                                int minutesStart = dtStart.Minute;
                                objOT.InTime = new DateTime(date.Year, date.Month, date.Day, hoursStart, minutesStart, 0);
                                tsStartTime = new TimeSpan(hoursStart, minutesStart, 0);

                            }
                            catch
                            {

                                divWarningMsg.InnerHtml = "Start Time is invalid for the row : " + rowNumber;
                                divWarningMsg.Hide = false;
                                return;
                            }
                        }
                        else
                        {
                            divWarningMsg.InnerHtml = "Start Time is required for the row : " + rowNumber;
                            divWarningMsg.Hide = false;
                            return;
                        }

                        if (row["End Time"] != DBNull.Value && row["End Time"].ToString() != "")
                        {
                            try
                            {
                                //string startTime = row["End Time"].ToString();
                                //string[] splInTime = startTime.Split(':');
                                //int hoursStart = int.Parse(splInTime[0]);
                                //int minutesStart = int.Parse(splInTime[1]);

                                //objOT.OutTime = new DateTime(date.Year, date.Month, date.Day, hoursStart, minutesStart, 0);


                                DateTime dtEnd = DateTime.Parse(row["End Time"].ToString());
                                int hoursStart = dtEnd.Hour;
                                int minutesStart = dtEnd.Minute;
                                objOT.OutTime = new DateTime(date.Year, date.Month, date.Day, hoursStart, minutesStart, 0); 

                            }
                            catch
                            {

                                divWarningMsg.InnerHtml = "End Time is invalid for the row : " + rowNumber;
                                divWarningMsg.Hide = false;
                                return;
                            }
                        }
                        else
                        {
                            divWarningMsg.InnerHtml = "End Time is required for the row : " + rowNumber;
                            divWarningMsg.Hide = false;
                            return;
                        }

                        if (row["Reason"] != DBNull.Value && row["Reason"].ToString() != "")
                        {
                            objOT.OutNote = row["Reason"].ToString();
                        }
                        else
                        {
                            divWarningMsg.InnerHtml = "Reason is required for the row : " + rowNumber;
                            divWarningMsg.Hide = false;
                            return;
                        }

                        list.Add(objOT);

                    }

                    if (list.Count > 0)
                    {
                        Session["AssignOTImp"] = list;
                        divMsgCtl.InnerHtml = "Overtime imported successfully.";
                        divMsgCtl.Hide = false;

                        this.HasImport = true;
                    }
                    else
                    {
                        divWarningMsg.InnerHtml = "Error while importing overtime assign.";
                        divWarningMsg.Hide = false;
                    }


                }

                    //sheet name changed error
                catch (OleDbException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_SheetNameError);
                    divWarningMsg.Hide = false;
                }
                catch (ArgumentException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_ColumnNameChangedError);
                    divWarningMsg.Hide = false;
                }
            }
        }
    }
}