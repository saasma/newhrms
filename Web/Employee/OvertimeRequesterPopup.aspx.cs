﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Manager;
using DAL;
using Utils.Helper;
using Utils.Web;
using System.Text;
using BLL;
using BLL.Entity;
using BLL.Base;
using BLL.BO;
using Ext.Net;

namespace Web.Employee
{
    public partial class OvertimeRequesterPopup : BasePage
    {

        [DirectMethod]
      

        
        public static string GetHour(string startDate, string endDate, string startTime1, string endTime1)
        {
            startDate = startDate.Replace("\"", "");
            endDate = endDate.Replace("\"", "");
            startTime1 = startTime1.Replace("\"", "");
            endTime1 = endTime1.Replace("\"", "");

            DateTime date;
            if (DateTime.TryParse(startDate, out date) == false)
                return "";
            if (DateTime.TryParse(endDate, out date) == false)
                return "";
            if (DateTime.TryParse(startTime1, out date) == false)
                return "";
            if (DateTime.TryParse(endTime1, out date) == false)
                return "";

            DateTime start = Convert.ToDateTime(startDate);
            DateTime end = Convert.ToDateTime(endDate);
            DateTime startTime = Convert.ToDateTime(startTime1);
            DateTime endTime = Convert.ToDateTime(endTime1);

            DateTime otstart = new DateTime(start.Year, start.Month, start.Day, startTime.Hour, startTime.Minute, 0);
            DateTime otend = new DateTime(end.Year, end.Month, end.Day, endTime.Hour, endTime.Minute, 0);

            TimeSpan diff = (otend - otstart);
            return (int)diff.TotalHours + ":" + diff.Minutes;

        }
     
    }
}