﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL;
using BLL.Manager;
using Web.ReportDataSetTableAdapters;
using Web.Master;
using BLL;
using Web.CP.Report.Templates.Pay.Detail;
using Utils;
using BLL.Base;
using System.IO;
using Utils.Calendar;
using BLL.BO;
using Ext.Net;

namespace Web.Employee
{
    public partial class NoticeList : BasePage
    {
      
        protected void Page_Load(object sender, EventArgs e)
        {



            if (!IsPostBack && ! X.IsAjaxRequest)
            {
                Initialise();
            }


            
        }


        private void Initialise()
        {
            LoadGrid(1);
        }


        public void LoadGrid(int status)
        {
            //StatusStr
            List<NoticeBoard> noticeList = new List<NoticeBoard>();
            noticeList = NoticeManager.getAllNotice(status);
            noticeList = noticeList.OrderByDescending(x => x.PublishDate).Where(y => y.ExpiryDate == null || y.ExpiryDate >= CommonManager.GetCurrentDateAndTime() ).ToList();
                
            storeNoticeList.DataSource = noticeList;
            storeNoticeList.DataBind();
        }
        

    }
}
