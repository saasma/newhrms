﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using BLL.BO;
using System.IO;
using Utils.Calendar;
using Utils.Helper;

namespace Web.NewHR
{
    public partial class MyOvertimeList : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                List<OvertimePeriod> list = OvertimeManager.GetOverTimPeriod();
                cmbOverTimePeriod.GetStore().DataSource = list;
                cmbOverTimePeriod.GetStore().DataBind();


                if (list.Count > 0)
                {
                    ExtControlHelper.ComboBoxSetSelected(list[list.Count - 1].OvertimeID.ToString(), cmbOverTimePeriod);
                    cmbOverTimePeriod_Select(null, null);
                }
               
                
            }
        }

        public void cmbOverTimePeriod_Select(object sender, DirectEventArgs e)
        {
            int period = int.Parse(cmbOverTimePeriod.SelectedItem.Value);
            OvertimePeriod ot = OvertimeManager.GetOverTimPeriodById(period);
            title.Html = string.Format("<h3> Overtime List for {0} to {1}</h3>",ot.StartDate.ToString("dd MMM yyyy")
                , ot.EndDate.ToString("dd MMM yyyy"));

            X.Js.AddScript("searchList();");

           
        }

     
    }
}
