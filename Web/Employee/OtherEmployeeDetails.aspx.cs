﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//using Web.CP.Report.Templates;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using Web.CP.Report.Templates.HR;
using Web.ReportDataSetTableAdapters;
using DevExpress.XtraReports.Web;
using System.IO;

using BLL;
using DevExpress.XtraReports.UI;
using Web.ReportDataSetTableAdapters;
using Utils.Calendar;
using BLL.Base;
using BLL.Manager;
using DAL;
using Utils;
using Web.UserControls;

namespace Web.CP.Report
{
    public partial class OtherEmployeeDetails : BasePage
    {

        //protected void Page_Load(object sender, EventArgs e)
        //{
        //    LoadReport();
        //}

        //public ReportToolbar ReportToolbar
        //{
        //    get
        //    {
        //        return ReportToolbar1;
        //    }

        //}

        
        //private bool ClearCache = false;
        public bool clearCache
        {
            get
            {
                if (ViewState["ClearCache"] == null)
                    return false;
                return bool.Parse(ViewState["ClearCache"].ToString());
            }
            set
            {
                ViewState["ClearCache"] = value;
            }
        }

        public Unit ViewerPaddingLeft
        {
            get
            {
                return this.rptViewer.Paddings.PaddingLeft;
            }
            set
            {
                this.rptViewer.Paddings.PaddingLeft = value;
            }

        }

        public void SelectLastPayrollAndDisableDropDown()
        {
            SelectLastPayrollAndDisableDropDown();
        }
     

        //public ReportViewer ReportViewer
        //{
        //    get
        //    {
        //        return rptViewer;
        //    }
        //}

       // public event LoadReport ReloadReport;


       

        //public event CompanySpecificExport CompanyExport;


    
        //public void DisplayReport(DevExpress.XtraReports.UI.XtraReport xtraReport)
        //{




        //    this.rptViewer.Report = xtraReport;
        //    this.ReportToolbar1.ReportViewer = this.rptViewer;

        //}

       
        protected void ReportViewer1_CacheReportDocument(object sender, CacheReportDocumentEventArgs e)
        {
            return;
            ///won't work for export so commented
            //get page name as key
            //e.Key = this.Page.ToString();// Guid.NewGuid().ToString();
            //Page.Session[e.Key] = e.SaveDocumentToMemoryStream();
            //this.clearCache = false;

        }
        protected void ReportViewer1_RestoreReportDocumentFromCache(object sender, RestoreReportDocumentFromCacheEventArgs e)
        {
            return;
            ///won't work for export so commented

            if (!this.clearCache)
            {
                Stream stream = Page.Session[e.Key] as Stream;
                if (stream != null)
                    e.RestoreDocumentFromStream(stream);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
           
            //LoadReport();

            if (!IsPostBack)
            {
                List<GetEmployeeListForDepartmentHeaderManagerResult> list = EmployeeManager.GetEmpListForDepartmentHeadOrManagerView();

                cmbEmpList.DataSource = list;
                cmbEmpList.DataBind();

                rptEmployeeList.DataSource = list;
                rptEmployeeList.DataBind();

            }
            LoadReport();
        }
        protected void Page_PreRender(object sender, EventArgs e)
        {
            //if (!IsPostBack)
            //    report.Filter.SelectLastPayroll();
        }

        protected void btnLoad_Click(object sender, EventArgs e)
        {
            LoadReport();
        }
        public void btnEmployee_Command(object sender, CommandEventArgs e)
        {
            int empId = Convert.ToInt32(e.CommandArgument);
            //cmbEmpList.SelectedItem = new DevExpress.Web.ASPxEditors.ListEditItem("", empId);
            //cmbEmpList.SelectedItem.Value = empId;
            cmbEmpList.Value = empId;
            cmbEmpList.Text = ((LinkButton)sender).Text;
            LoadReport();
        }
        protected void LoadReport()
        {

            int empId = 0;

            if (cmbEmpList.SelectedItem != null && cmbEmpList.SelectedItem.Value!= null)
            {
                empId = int.Parse(cmbEmpList.SelectedItem.Value.ToString());
            }

            EEmployee emp = EmployeeManager.GetEmployeeById(empId);

            //int empId = SessionManager.CurrentLoggedInEmployeeId;  //report.Filter.EmployeeId;

            if (empId == 0 || emp == null)
            {
                this.rptViewer.Report = null;
                this.ReportToolbar1.ReportViewer = this.rptViewer;
                return;
            }
           


            Report.Templates.HR.ReportOtherEmployeeDetails report1 = new Report.Templates.HR.ReportOtherEmployeeDetails();

            try
            {
                SetGeneralInfo(emp, report1);
                SetAddress(emp, report1);
                SetHRInformation(emp, report1);
                SetQualification(emp, report1);
                SetIncomes(emp, report1);
            }
            catch (Exception exp1)
            {
                Log.log("Other emp details error for employee id : " + emp.EmployeeId, exp1);
            }

            report1.DataMember = "Report";

            this.rptViewer.Report = report1;
            this.ReportToolbar1.ReportViewer = this.rptViewer;

            //report.DisplayReport(report1);

        }

        public void SetIncomes(EEmployee emp, Report.Templates.HR.ReportOtherEmployeeDetails rpt)
        {
            decimal? incomePf = 0, deductionPf = 0;
            List<EmployeeIncomeListResult> incomeList =
                 PayManager.GetEmployeeIncomeListWithoutAnyDeduction(emp.EmployeeId, ref incomePf, ref deductionPf).ToList();

            string values = "";

            foreach (EmployeeIncomeListResult item in incomeList)
            {
                if (values == "")
                    values = item.Title + " : " + item.Value;
                else
                    values += "\n" +  item.Title + " : " + item.Value;
            }
            rpt.lblIncomes.Text = values;

            GetSalaryIncrementsTableAdapter adap = new GetSalaryIncrementsTableAdapter();
            BLL.BaseBiz.SetConnectionPwd(adap.Connection);

            HEducationTableAdapter eductaionAdap = new HEducationTableAdapter();
            HTrainingTableAdapter trainingAdap = new HTrainingTableAdapter();
            HPreviousEmploymentTableAdapter empAdap = new HPreviousEmploymentTableAdapter();
            HDocumentTableAdapter docAdap = new HDocumentTableAdapter();
            BLL.BaseBiz.SetConnectionPwd(eductaionAdap.Connection);
            BLL.BaseBiz.SetConnectionPwd(trainingAdap.Connection);
            BLL.BaseBiz.SetConnectionPwd(empAdap.Connection);
            BLL.BaseBiz.SetConnectionPwd(docAdap.Connection);

            rpt.subReportSalaryIncrement.ReportSource.DataSource = adap.GetData(emp.EmployeeId);
            rpt.subReportSalaryIncrement.ReportSource.DataMember = "Report";

            // Education
            rpt.subreportEducation.ReportSource.DataSource = (eductaionAdap).GetData(emp.EmployeeId);
            rpt.subreportEducation.ReportSource.DataMember = "Report";

            // Trainings
            rpt.subreportTraining.ReportSource.DataSource = (trainingAdap).GetData(emp.EmployeeId);
            rpt.subreportTraining.ReportSource.DataMember = "Report";

            // Prev Employee
            rpt.subreportPrevEmployee.ReportSource.DataSource = (empAdap).GetData(emp.EmployeeId);
            rpt.subreportPrevEmployee.ReportSource.DataMember = "Report";


            //HDocumentTableAdapter adapDocs = new HDocumentTableAdapter();
            ReportDataSet.HDocumentDataTable docTable = docAdap.GetData(emp.EmployeeId);
            foreach (ReportDataSet.HDocumentRow row in docTable.Rows)
            {                

                  row.Url = "~/DocumentHandler.ashx?id=" + System.IO.Path.GetFileNameWithoutExtension(row.Url).ToString();
            }
            rpt.subReportDocuments.ReportSource.DataSource = docTable;
            rpt.subReportDocuments.ReportSource.DataMember = "Report";
        }

        public void SetQualification(EEmployee emp, Report.Templates.HR.ReportOtherEmployeeDetails rpt)
        {
            if (emp.HHumanResources.Count > 0)
                rpt.lblQualification.Text = string.Format(rpt.lblQualification.Text, emp.HHumanResources[0].Qualification);
            else
                rpt.lblQualification.Text = "";
            rpt.lblSkillSetList.Text = CommonManager.GetEmployeeSkillSetHTMLForReport(emp.EmployeeId);
        }

        // Return the number of years, months, days, hours, minutes, seconds,
        // and milliseconds you need to add to from_date to get to_date.
        public void GetElapsedTime(DateTime from_date, DateTime to_date,
            out int years, out int months, out int days, out int hours,
            out int minutes, out int seconds, out int milliseconds)
        {
            // If from_date > to_date, switch them around.
            if (from_date > to_date)
            {
                GetElapsedTime(to_date, from_date,
                    out years, out months, out days, out hours,
                    out minutes, out seconds, out milliseconds);
                years = -years;
                months = -months;
                days = -days;
                hours = -hours;
                minutes = -minutes;
                seconds = -seconds;
                milliseconds = -milliseconds;
            }
            else
            {
                // Handle the years.
                years = to_date.Year - from_date.Year;

                // See if we went too far.
                DateTime test_date = from_date.AddMonths(12 * years);
                if (test_date > to_date)
                {
                    years--;
                    test_date = from_date.AddMonths(12 * years);
                }

                // Add months until we go too far.
                months = 0;
                while (test_date <= to_date)
                {
                    months++;
                    test_date = from_date.AddMonths(12 * years + months);
                }
                months--;

                // Subtract to see how many more days,
                // hours, minutes, etc. we need.
                from_date = from_date.AddMonths(12 * years + months);
                TimeSpan remainder = to_date - from_date;
                days = remainder.Days;
                hours = remainder.Hours;
                minutes = remainder.Minutes;
                seconds = remainder.Seconds;
                milliseconds = remainder.Milliseconds;
            }
        }

        public void SetHRInformation(EEmployee emp, Report.Templates.HR.ReportOtherEmployeeDetails rpt)
        {

            ServicePeroid firstStatus = EmployeeManager.GetServicePeroidDate(emp.EmployeeId);
            //DateTime from_date,  to_date;
             int years,   months,   days,   hours;
             int minutes,   seconds, milliseconds;



            GetElapsedTime(firstStatus.FromDateEng,BLL.BaseBiz.GetCurrentDateAndTime(),out years,  out months,out days,out hours,out minutes,out seconds,out milliseconds);

            rpt.lblEmployeeFor.Text = string.Format(rpt.lblEmployeeFor.Text, years, months, days);
            rpt.lblJoinedOn.Text = string.Format(rpt.lblJoinedOn.Text, firstStatus.FromDateEng.ToShortDateString());

            int? currentStatusId = EmployeeManager.GetEmployeeCurrentStatus(emp.EmployeeId);
            //rpt.lblCurrentStatus.Text = "";
            if (currentStatusId != null)
                rpt.lblCurrentStatus.Text = string.Format(rpt.lblCurrentStatus.Text, JobStatus.GetStatusForDisplay(currentStatusId));


            string values = "";
            List<ECurrentStatus> statusList = new EmployeeManager().GetCurrentStatuses(emp.EmployeeId);
            foreach (ECurrentStatus status in statusList)
            {
                if (values == "")
                    values = JobStatus.GetStatusForDisplay(currentStatusId) + " on " + status.FromDateEng.ToShortDateString();
                else
                    values += "\n" + JobStatus.GetStatusForDisplay(currentStatusId) + " on " + status.FromDateEng.ToShortDateString();
            }
            rpt.lblStatuses.Text = values;


            if (emp.HHumanResources.Count > 0)
            {
                rpt.lblBloodGroup.Text = string.Format(rpt.lblBloodGroup.Text, emp.HHumanResources[0].BloodGroup);
                rpt.lblPassportNo.Text = string.Format(rpt.lblPassportNo.Text, emp.HHumanResources[0].PassportNo);
                rpt.lblDriverLicense.Text = string.Format(rpt.lblDriverLicense.Text, emp.HHumanResources[0].DrivingLicenseNo);
                CustomDate date = CustomDate.GetCustomDateFromString(emp.HHumanResources[0].PassportValidUpto, true);

                if (string.IsNullOrEmpty(emp.HHumanResources[0].PassportNo) == false)
                    rpt.lblPassportValidUpto.Text = string.Format(rpt.lblPassportValidUpto.Text, date.EnglishDate.ToShortDateString());
                else
                    rpt.lblPassportValidUpto.Text = string.Format(rpt.lblPassportValidUpto.Text, " ");
            }
            else
            {
                rpt.lblBloodGroup.Text = "";
                rpt.lblPassportNo.Text = "";
                rpt.lblDriverLicense.Text = "";
                rpt.lblPassportValidUpto.Text = "";
            }
        }
        public void SetAddress(EEmployee emp, Report.Templates.HR.ReportOtherEmployeeDetails rpt)
        {
            // Present Address
            rpt.lblPresentLocality.Text = emp.EAddresses[0].PSLocality;
            rpt.lblPresentZone.Text = "";
            if (emp.EAddresses[0].PSZoneId != null && emp.EAddresses[0].PSZoneId != -1)
                rpt.lblPresentZone.Text = CommonManager.GetZoneById(emp.EAddresses[0].PSZoneId.Value).Zone;
            rpt.lblPresentDistrict.Text = "";
            if (emp.EAddresses[0].PSDistrictId != null && emp.EAddresses[0].PSDistrictId != -1)
                rpt.lblPresentDistrict.Text = CommonManager.GetDistrictById(emp.EAddresses[0].PSDistrictId.Value).District;

            // Permanent Address
            rpt.lblPermanentLocality.Text = emp.EAddresses[0].PELocality;
            rpt.lblPermanentZone.Text = "";
            if (emp.EAddresses[0].PEZoneId != null && emp.EAddresses[0].PEZoneId != -1)
                rpt.lblPermanentZone.Text = CommonManager.GetZoneById(emp.EAddresses[0].PEZoneId.Value).Zone;
            rpt.lblPermanentDistrict.Text = "";
            if (emp.EAddresses[0].PEDistrictId != null && emp.EAddresses[0].PEDistrictId != -1)
                rpt.lblPermanentDistrict.Text = CommonManager.GetDistrictById(emp.EAddresses[0].PEDistrictId.Value).District;
            rpt.lblPermanentCountry.Text = "";
            if (emp.EAddresses[0].PECountryId != null && emp.EAddresses[0].PECountryId != -1)
                rpt.lblPermanentCountry.Text = CommonManager.GetCountryById(emp.EAddresses[0].PECountryId.Value).CountryName;

            // Official Email
            rpt.lblOfficialEmail.Text = emp.EAddresses[0].CIEmail;
            rpt.lblOfficalPhone.Text = emp.EAddresses[0].CIPhoneNo;
            rpt.lblOfficialExt.Text = emp.EAddresses[0].Extension;

            // Personal Contact
            rpt.lblPersonalEmail.Text = emp.EAddresses[0].PersonalEmail;
            rpt.lblPersonalPhone.Text = emp.EAddresses[0].PersonalPhone;
            rpt.lblPersonalMobile.Text = emp.EAddresses[0].CIMobileNo;

            // Emergency Contact
            rpt.lblEmergencyName.Text = emp.EAddresses[0].EmergencyName;
            rpt.lblEmergencyRelation.Text = emp.EAddresses[0].EmergencyRelation;
            rpt.lblEmergencyPhone.Text = emp.EAddresses[0].CIEmergencyNo;
            rpt.lblEmergencyMobile.Text = emp.EAddresses[0].EmergencyMobile;

        }
        public void SetGeneralInfo(EEmployee emp, Report.Templates.HR.ReportOtherEmployeeDetails rpt)
        {
            rpt.lblName.Text = emp.Title + " " + emp.Name;
            CustomDate date = null; string value = "";
            try
            {
                // DOB
                if (this.IsEnglish && emp.DateOfBirth != null)
                {
                    date = CustomDate.GetCustomDateFromString(emp.DateOfBirth,true);
                    if (date.Year == CalendarCtl.StartEngYear)
                        date = null;
                }
                else if( emp.DateOfBirth != null)
                {
                    if (emp.IsEnglishDOB != null && emp.IsEnglishDOB.Value)
                    {
                        date = CustomDate.GetCustomDateFromString(emp.DateOfBirth, true);
                        if (date.Year == CalendarCtl.StartEngYear)
                            date = null;
                    }
                    else
                    {
                        date = CustomDate.GetCustomDateFromString(emp.DateOfBirth, false);
                        if (date.Year == CalendarCtl.StartNepYear)
                            date = null;
                    }
                }

                if (date != null)
                    rpt.lblAge.Text = string.Format(rpt.lblAge.Text, GetAge(date.EnglishDate));
                else
                    rpt.lblAge.Text = "";
            }
            catch (Exception exp1)
            {
                Log.log("Other emp details error for employee id : " + emp.EmployeeId, exp1);
            }

            rpt.lblStatus.Text = emp.MaritalStatus;
            rpt.lblINo.Text = string.Format(rpt.lblINo.Text, GetValue(emp.EHumanResources[0].IdCardNo));
            rpt.lblDepartment.Text = string.Format(rpt.lblDepartment.Text, GetValue(emp.Department.Name));

                 LeaveProjectEmployee leave = LeaveRequestManager.GetLeaveProjectForEmployee(emp.EmployeeId);
                if (leave != null)
                {
                    rpt.lblTeam.Text = string.Format(rpt.lblTeam.Text, leave.LeaveProject.Name);
                    //ddlLeaveProject.SelectedValue = leave.LeaveProjectId.ToString();
                }
                else
                    rpt.lblTeam.Text = string.Format(rpt.lblTeam.Text, "");


           // if(emp.EHumanResources[0].lea

            value = "";
            if (emp.SubDepartmentId != null && emp.SubDepartmentId != -1)
            {
                value = new DepartmentManager().GetSubDepartmentById(emp.SubDepartmentId.Value).Name;
            }
            rpt.lblSubDepartment.Text = string.Format(rpt.lblSubDepartment.Text, GetValue(value));
            rpt.lblDesignation.Text = string.Format(rpt.lblDesignation.Text, GetValue(emp.EDesignation.Name));
            value = "";
            //if (emp.TierId != null && emp.TierId != -1)
            //{
            //    value = new CommonManager().GetTierById(emp.TierId.Value).Name;
            //}
            //rpt.lblTier.Text = string.Format(rpt.lblTier.Text, GetValue(value));

            if (emp.HHumanResources.Count > 0 && !string.IsNullOrEmpty(emp.HHumanResources[0].UrlPhotoThumbnail))
                rpt.pic.ImageUrl = Config.UploadLocation + "/" + emp.HHumanResources[0].UrlPhotoThumbnail;

            
        }


        public string GetValue(string value)
        {
            if (value == null)
                return "";
            return value.ToString();
        }

        public int GetAge(DateTime bday)
        {
            DateTime today = DateTime.Today;
            int age = today.Year - bday.Year;
            if (bday > today.AddYears(-age)) age--;

            return age;
        }
    }
}
