﻿<%@ Page Language="C#" Title="Overtime Request" AutoEventWireup="true" MasterPageFile="~/Master/ForEmployeePopupPage.Master"
    CodeBehind="OvertimeRequesterPopup.aspx.cs" Inherits="Web.Employee.OvertimeRequesterPopup" %>

<%@ Register Src="UserControls/OvertimeRequestApproveCtl.ascx" TagName="OvertimeRequestApproveCtl"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" src="<%= ResolveUrl("~/Employee/override.js?v=") + Web.Helper.WebHelper.Version %>"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
    <uc1:OvertimeRequestApproveCtl ID="OvertimeRequestApproveCtl1" runat="server" />
</asp:Content>
