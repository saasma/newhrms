﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL.Entity;
using BLL;
using System.IO;
using BLL.BO;
using Utils.Helper;
using Bll;
using System.Text.RegularExpressions;

namespace Web.Employee
{
    public partial class AApproveOvertimeNewNew : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest && !IsPostBack)
            {
                Initialize();

                if (LeaveAttendanceManager.IsEmployeeAllowedToApproveLeave(SessionManager.CurrentLoggedInEmployeeId))
                    btnRecommendOrApprove.Text = "Approve";
            }

            JavascriptHelper.AttachPopUpCode(Page, "overtimeAdd", "AssignOvertimePopup.aspx", 920, 500);
            JavascriptHelper.AttachPopUpCode(Page, "overtimeEdit", "AssignOvertimeEdit.aspx", 700, 650);
        }

        private void Initialize()
        {
            LoadEmployees();
        }

        protected void LoadEmployees()
        {
            int type = 1;
            int status = -1;

            if (!string.IsNullOrEmpty(cmbType.SelectedItem.Text))
                type = int.Parse(cmbType.SelectedItem.Value);

            if (!string.IsNullOrEmpty(cmbStatus.SelectedItem.Text))
                status = int.Parse(cmbStatus.SelectedItem.Value);

            int totalRecords = 0;

            List<GetOvertimeRequestForManagerNewResult> list = new List<GetOvertimeRequestForManagerNewResult>();
            list = OvertimeManager.GetLeaveRequestsForManagerNew(type, status, 1, 999999, ref totalRecords);

            //CanRecommendOrApprove(int status, int employeeId)
            foreach (var item in list)
            {
                item.CanRecOrAppRec = CanRecommendOrApprove(item.Status.Value, item.EmployeeID.Value);
            }

            gridOverTimeReq.GetStore().DataSource = list;
            gridOverTimeReq.GetStore().DataBind();
        }

        public bool CanRecommendOrApprove(int status, int employeeId)
        {
            if (status == (int)OvertimeStatusEnum.Recommended &&
                LeaveRequestManager.CanApprove(employeeId, SessionManager.CurrentLoggedInEmployeeId, PreDefindFlowType.Overtime))
                return true;

            if (status == 0)
                return true;


            return false;
        }

        protected void btnLoad_Click(object sender, DirectEventArgs e)
        {
            LoadEmployees();
        }

        protected void MyData_Refresh(object sender, StoreReadDataEventArgs e)
        {
            LoadEmployees();
        }

        protected void btnRecommendOrApprove_Click(object sender, DirectEventArgs e)
        {
            string gridItemsJson = e.ExtraParams["gridItems"];
            List<GetOvertimeRequestForManagerResult> list = JSON.Deserialize<List<GetOvertimeRequestForManagerResult>>(gridItemsJson);

            if (list.Count == 0)
                return;

            List<OvertimeRequest> requestList = new List<OvertimeRequest>();

            foreach (var item in list)
            {
                OvertimeRequest request = new OvertimeRequest();
                int RequestID = item.RequestID;

                request.OvertimeRequestID = RequestID;
                requestList.Add(request);
            }

            int count = 0;
            if (OvertimeManager.RecommendOrApprove(requestList, out count))
            {
                LoadEmployees();

                NewMessage.ShowNormalMessage(count + " overtime has been forwarded.");
            }
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            int type = 1;
            int status = -1;

            if (!string.IsNullOrEmpty(cmbType.SelectedItem.Text))
                type = int.Parse(cmbType.SelectedItem.Value);

            if (!string.IsNullOrEmpty(cmbStatus.SelectedItem.Text))
                status = int.Parse(cmbStatus.SelectedItem.Value);

            int totalRecords = 0;

            List<GetOvertimeRequestForManagerNewResult> list = new List<GetOvertimeRequestForManagerNewResult>();
            list = OvertimeManager.GetLeaveRequestsForManagerNew(type, status, 1, 999999, ref totalRecords);

            Dictionary<string, string> title = new Dictionary<string, string>();
            title["Overtime List Report"] = "";

            ExcelHelper.ExportToExcel("Overtime List", list,
                new List<String>() { "RequestID", "EmployeeID", "CanRecOrAppRec", "SupervisorName", "SupervisorID", "Recommender", "Status", "Duration", "TotalRows", "RowNumber" },
                new List<String>() { },
                new Dictionary<string, string>() { { "EmployeeName", "Employee Name" }, { "StartTime", "Start Time" }, { "EndTime", "End Time" }, { "CheckInTime", "In Time" }, { "CheckOutTime", "Out Time" }, { "DurationModified", "Duration" }, { "ApprovedTime", "Approved Time" }, { "StatusModified", "Status" } },
                new List<string>() { },
                new Dictionary<string, string>() { },
                new List<string> { "Date", "EmployeeName", "StartTime", "EndTime", "CheckInTime", "CheckOutTime", "DurationModified", "ApprovedTime", "Reason", "StatusModified" });
        }

        protected void btnLoadRef_Click(object sender, DirectEventArgs e)
        {
            LoadEmployees();
        }


    }
}