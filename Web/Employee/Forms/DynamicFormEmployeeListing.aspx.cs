﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using System.Xml;

namespace Web.Employee.Forms
{
    public partial class DynamicFormEmployeeListing : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();
            }
        }

        private void Initialise()
        {
            cmbForm.Store[0].DataSource = DynamicFormManager.GetAllPublishedDynamicForms();
            cmbForm.Store[0].DataBind();            
        }

        public void btnLoad_Click(object sender, DirectEventArgs e)
        {
            if (string.IsNullOrEmpty(cmbForm.SelectedItem.Value))
                return;
            gvDynamicFormEmployee.GetStore().DataSource = DynamicFormManager.GetAllDynamicFormEmployeeByUser(SessionManager.CurrentLoggedInEmployeeId, int.Parse(cmbForm.SelectedItem.Value));
            gvDynamicFormEmployee.GetStore().DataBind();
        }

        protected void btnDelete_Click(object sender, DirectEventArgs e)
        {
            
        }

        protected void btnEdit_Click(object sender, DirectEventArgs e)
        {
            int formId = int.Parse(hiddenValue.Text.Trim());
            int formEmpId = int.Parse(hiddenValueFormEmpId.Text.Trim());
            Response.Redirect(string.Format("~/Employee/Forms/EmployeeDynamicForm.aspx?formId={0}&formEmpId={1}", formId.ToString(), formEmpId.ToString()));
        }
    }
}