﻿<%@ Page Title="Employee Dynamic Form" Language="C#" MasterPageFile="~/Master/EmployeeExt.Master" AutoEventWireup="true" CodeBehind="EmployeeDynamicForm.aspx.cs" Inherits="Web.Employee.Forms.EmployeeDynamicForm" %>
<%@ Register Src="~/DynamicForms/UserControls/DynamicFormEmp.ascx" TagName="EmpForm" TagPrefix="ucDFE" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">

<ucDFE:EmpForm Id="DynamicFormEmpCtrl" runat="server" />

</asp:Content>

