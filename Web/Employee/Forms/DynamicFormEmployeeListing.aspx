﻿<%@ Page Title="Dynamic Form Employee List" Language="C#" MasterPageFile="~/Master/EmployeeExt.Master" AutoEventWireup="true" CodeBehind="DynamicFormEmployeeListing.aspx.cs" Inherits="Web.Employee.Forms.DynamicFormEmployeeListing" %>
<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">

<script type="text/javascript">

        var CommandHandler1 = function(command, record){
            <%= hiddenValue.ClientID %>.setValue(record.data.FormID);
            <%= hiddenValueFormEmpId.ClientID %>.setValue(record.data.DFEID);
                if(command=="Edit")
                {
                    <%= btnEdit.ClientID %>.fireEvent('click');
                }
                else
                {
                    <%= btnDelete.ClientID %>.fireEvent('click');
                }

             }
             
        
    </script>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="mainContent" runat="server">

<ext:Hidden runat="server" ID="hiddenValue" />
<ext:Hidden runat="server" ID="hiddenValueFormEmpId"></ext:Hidden>
    <ext:LinkButton runat="server" Hidden="true" ID="btnEdit">
        <DirectEvents>
            <Click OnEvent="btnEdit_Click">
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:LinkButton>
    <ext:Button ID="btnDelete" runat="server" Hidden="true">
        <DirectEvents>
            <Click OnEvent="btnDelete_Click">
                <Confirmation ConfirmRequest="true" Message="Are you sure, you want to delete the employee form?" />
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:Button>

    <div class="separator bottom">
    </div>

        <div class="innerLR">
   
        <div class="widget">
            <div class="widget-head">
                <h4 class="heading">
                    Your Dynamic Form Employee List</h4>
            </div>

            <div class="widget-body">

                <table class="fieldTable firsttdskip">
                <tr>
                    
                    <td>
                        <ext:ComboBox FieldLabel="Form Name" ID="cmbForm" Width="180px" runat="server" ValueField="FormID"
                            DisplayField="FormName" LabelAlign="Top" LabelSeparator="" ForceSelection="true"
                            QueryMode="Local">
                            <Store>
                                <ext:Store ID="Store2" runat="server">
                                    <Model>
                                        <ext:Model ID="Model2" IDProperty="StepID" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="FormID" />
                                                <ext:ModelField Name="FormName" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                            <%--<DirectEvents>
                                <Select OnEvent="FormName_Change">
                                    <EventMask ShowMask="true" />
                                </Select>
                            </DirectEvents>--%>
                        </ext:ComboBox>
                    </td>

                    
                    <td style="padding-top: 20px">
                        <ext:LinkButton runat="server" StyleSpec="padding:0px;margin-top:20px;" ID="btnLoad"
                            Cls="btnFlat" BaseCls="btnFlat" Text="<i></i>Load" runat="server">
                            <DirectEvents>
                                <Click OnEvent="btnLoad_Click">
                                    <EventMask ShowMask="true" />
                                </Click>
                            </DirectEvents>
                        </ext:LinkButton>
                    </td>
                </tr>
            </table>

            </div>


            <div class="widget-body">
                <ext:GridPanel StyleSpec="margin-top:15px;" ID="gvDynamicFormEmployee" runat="server" Cls="itemgrid">
                    <Store>
                        <ext:Store ID="storeDynamicFormEmployee" runat="server">
                            <Model>
                                <ext:Model ID="Model4" runat="server" IDProperty="DFEID">
                                    <Fields>
                                        <ext:ModelField Name="FormID" Type="Int" />
                                        <ext:ModelField Name="DFEID" Type="Int" />
                                        <ext:ModelField Name="DateEng" Type="String" />
                                        <ext:ModelField Name="StatusName" Type="String" />
                                    </Fields>
                                </ext:Model>
                            </Model>
                        </ext:Store>
                    </Store>
                    <ColumnModel>
                        <Columns>

                            <ext:Column ID="Column1" Sortable="false" MenuDisabled="true" runat="server" Text="Form Id"
                                Width="100" Align="Left" DataIndex="FormID" />

                            <ext:Column ID="colFormEmployeeId" Sortable="false" MenuDisabled="true" runat="server" Text="DFEID"
                                Width="100" Align="Left" DataIndex="DFEID" />

                             <ext:DateColumn ID="DateColumn1" runat="server" Text="Date" Sortable="true"
                                DataIndex="DateEng" Width="86" Align="Left" Format="MM/dd/yyyy" />
                           

                            <ext:Column ID="Column3" Sortable="false" MenuDisabled="true" runat="server" Text="Status"
                                Width="150" Align="Left" DataIndex="StatusName" />

                            <ext:CommandColumn ID="CommandColumn1" runat="server" Text="Actions" Align="Center" Width="100">
                                <Commands>
                                    <ext:GridCommand Text="<i></i>" Cls="btn-action glyphicons pencil btn-success" CommandName="Edit" ToolTip-Text="Edit" />
                                   
                                </Commands>
                                <Listeners>
                                    <Command Handler="CommandHandler1(command,record);" />
                                </Listeners>
                            </ext:CommandColumn>
                        </Columns>
                    </ColumnModel>
                    <SelectionModel>
                        <ext:RowSelectionModel ID="RowSelectionModel2" runat="server" Mode="Single" />
                    </SelectionModel>
                </ext:GridPanel>

                
            </div>
        </div>
        
    </div>
    <br />



</asp:Content>
