﻿<%@ Page Title="Employee Dynamic Form List" Language="C#" MasterPageFile="~/Master/EmployeeExt.Master" AutoEventWireup="true" CodeBehind="EmployeeDynamicFormList.aspx.cs" Inherits="Web.Employee.Forms.EmployeeDynamicFormList" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<script type="text/javascript">

        var CommandHandler1 = function(command, record){
            <%= hiddenValue.ClientID %>.setValue(record.data.FormID);
            <%= hiddenValueFormEmpId.ClientID %>.setValue(record.data.DFEID);
                if(command=="Edit")
                {
                    <%= btnEdit.ClientID %>.fireEvent('click');
                }
                else
                {
                    <%= btnDelete.ClientID %>.fireEvent('click');
                }

             }
             
        
    </script>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">


<ext:Hidden runat="server" ID="hiddenValue" />
<ext:Hidden runat="server" ID="hiddenValueFormEmpId"></ext:Hidden>
    <ext:LinkButton runat="server" Hidden="true" ID="btnEdit">
        <DirectEvents>
            <Click OnEvent="btnEdit_Click">
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:LinkButton>
    <ext:Button ID="btnDelete" runat="server" Hidden="true">
        <DirectEvents>
            <Click OnEvent="btnDelete_Click">
                <Confirmation ConfirmRequest="true" Message="Are you sure, you want to delete the employee form?" />
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:Button>

    <div class="separator bottom">
    </div>

    <div class="innerLR">
   
        <div class="widget">
            <div class="widget-head">
                <h4 class="heading">
                    <asp:Label ID="lblFormName" Text="" runat="server" />
                    <%--Dynamic Employee Form List--%></h4>
            </div>
            <div class="widget-body">
                <ext:GridPanel StyleSpec="margin-top:15px;" ID="gvDynamicFormEmployee" runat="server" Cls="itemgrid">
                    <Store>
                        <ext:Store ID="storeDynamicFormEmployee" runat="server">
                            <Model>
                                <ext:Model ID="Model4" runat="server" IDProperty="DFEID">
                                    <Fields>
                                        <ext:ModelField Name="DFEID" Type="Int" />
                                        <ext:ModelField Name="FormID" Type="Int" />
                                        <ext:ModelField Name="Date" Type="String" />
                                        <ext:ModelField Name="StatusName" Type="String" />
                                    </Fields>
                                </ext:Model>
                            </Model>
                        </ext:Store>
                    </Store>
                    <ColumnModel>
                        <Columns>
                            <ext:Column ID="colFormEmployeeId" Sortable="false" MenuDisabled="true" runat="server" Text="Id"
                                Width="100" Align="Left" DataIndex="DFEID" />

                            <ext:Column ID="Column1" Sortable="false" MenuDisabled="true" runat="server" Text="Form Id" Visible="false"
                                Width="100" Align="Left" DataIndex="FormID" />

                            <ext:Column ID="Column2" Sortable="false" MenuDisabled="true" runat="server" Text="Date"
                                Width="100" Align="Left" DataIndex="Date" />

                            <ext:Column ID="Column3" Sortable="false" MenuDisabled="true" runat="server" Text="Status"
                                Width="150" Align="Left" DataIndex="StatusName" />

                            <ext:CommandColumn ID="CommandColumn1" runat="server" Text="Actions" Align="Center" Width="100">
                                <Commands>
                                    <ext:GridCommand Text="<i></i>" Cls="btn-action glyphicons pencil btn-success" CommandName="Edit" ToolTip-Text="Edit" />
                                   
                                </Commands>
                                <Listeners>
                                    <Command Handler="CommandHandler1(command,record);" />
                                </Listeners>
                            </ext:CommandColumn>
                        </Columns>
                    </ColumnModel>
                    <SelectionModel>
                        <ext:RowSelectionModel ID="RowSelectionModel2" runat="server" Mode="Single" />
                    </SelectionModel>
                </ext:GridPanel>

                <div class="buttonBlock">
                    <ext:LinkButton runat="server" StyleSpec="padding:0px;" ID="btnAdd" Cls="btnFlat"
                        BaseCls="btnFlat" >
                        <DirectEvents>
                            <Click OnEvent="btnAdd_Click">
                                <EventMask ShowMask="true" />
                            </Click>
                        </DirectEvents>
                    </ext:LinkButton>
                </div>
                <%--Text="<i></i>Add New Dynamic Form"--%>
            </div>
        </div>
        
    </div>
    <br />


</asp:Content>
