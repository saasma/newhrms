﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;

namespace Web.Employee.Forms
{
    public partial class EmployeeFormListing : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindDynamicForms();
            }
        }

        private void BindDynamicForms()
        {
            //gvFormsList.DataSource = DynamicFormManager.GetDynamicFormsForEmp();
            gvFormsList.DataSource = DynamicFormManager.GetDynamicFormPublishList(SessionManager.CurrentLoggedInEmployeeId);
            gvFormsList.DataBind();
        }

        protected void gvFormsList_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "View")
            {
                Response.Redirect("~/Employee/Forms/EmployeeDynamicFormList.aspx?formId=" + e.CommandArgument.ToString());
            }
        }
    }
}