﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using System.Xml;

namespace Web.Employee.Forms
{
    public partial class EmployeeDynamicFormList : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                LoadDynamicFormEmployees();
                lblFormName.Text = DynamicFormManager.GetDynamicFormNameUsingFormId(int.Parse(Request.QueryString["formId"].ToString())) + " Employee Form List";
                btnAdd.Text = "Add New " + DynamicFormManager.GetDynamicFormNameUsingFormId(int.Parse(Request.QueryString["formId"].ToString()));
            }
        }

        private void LoadDynamicFormEmployees()
        {
            if (Request.QueryString["formId"] != null)
            {
                gvDynamicFormEmployee.GetStore().DataSource = DynamicFormManager.GetDynamicFormEmpoyeeList(int.Parse(Request.QueryString["formId"].ToString()), SessionManager.CurrentLoggedInEmployeeId);
                gvDynamicFormEmployee.GetStore().DataBind();
            }
            else
            {
                gvDynamicFormEmployee.GetStore().DataSource = new string[] { };
                gvDynamicFormEmployee.DataBind();
            }
            
        }

        protected void btnDelete_Click(object sender, DirectEventArgs e)
        {
            int formEmpId = int.Parse(hiddenValueFormEmpId.Text.Trim());
            Status status = DynamicFormManager.DeleteDynamicFormEmployee(formEmpId);
            if (status.IsSuccess)
            {
                LoadDynamicFormEmployees();
                NewMessage.ShowNormalMessage("Dynamic Form Employee deleted");
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }
        }

        protected void btnEdit_Click(object sender, DirectEventArgs e)
        {
            int formId = int.Parse(hiddenValue.Text.Trim());
            int formEmpId = int.Parse(hiddenValueFormEmpId.Text.Trim());
            Response.Redirect(string.Format("~/Employee/Forms/EmployeeDynamicForm.aspx?formId={0}&formEmpId={1}", formId.ToString(), formEmpId.ToString()));
        }

        protected void btnAdd_Click(object sender, DirectEventArgs e)
        {
            Response.Redirect("~/Employee/Forms/EmployeeDynamicForm.aspx?formId="+Request.QueryString["formId"].ToString());
        }
    }
}