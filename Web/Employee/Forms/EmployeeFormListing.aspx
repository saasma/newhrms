﻿<%@ Page Title="Dynamic Form List" Language="C#" MasterPageFile="~/Master/EmployeeExt.Master" AutoEventWireup="true" CodeBehind="EmployeeFormListing.aspx.cs" Inherits="Web.Employee.Forms.EmployeeFormListing" %>
<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="mainContent" runat="server">


<div class="separator bottom">
    </div>


    <div class="innerLR">
   
        <div class="widget">
            <div class="widget-head">
                <h4 class="heading">
                    Dynamic Form List
            </div>

             <div class="widget-body">

                <table class="fieldTable firsttdskip">
            
                    <tr>
                        <td>
                            <asp:GridView ID="gvFormsList" DataKeyNames="FormId" 
                                            AutoGenerateColumns="false" runat="server" onrowcommand="gvFormsList_RowCommand" >
                                            <Columns>
                                                <asp:TemplateField HeaderText="Form Name">
                                                <HeaderStyle Width="300px" HorizontalAlign="Left" />
                                                    <ItemTemplate>
                                    
                                                        <asp:LinkButton ID="lnkName"  CommandArgument='<%#Eval("FormId") %>' 
                                                            Text='<%#Eval("Name") %>' runat="server" CommandName="View"
                                                            ></asp:LinkButton>
                                                    </ItemTemplate>
                                                    <ItemStyle Width="300px" />
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                        </td>
                    </tr>

                </table>

            </div>


            
        </div>
        
    </div>
    <br />







</asp:Content>
