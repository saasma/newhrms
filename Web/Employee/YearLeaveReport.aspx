﻿<%@ Page Title="Yearly Leave Report" Language="C#" MasterPageFile="~/Master/EmployeeExt.Master"
    AutoEventWireup="true" CodeBehind="YearLeaveReport.aspx.cs" Inherits="Web.Employee.YearLeaveReport" %>

<%@ Register Src="~/newhr/UserControls/YearlyLeaveReportCtl.ascx" TagName="YearlyLeaveReportCtl"
    TagPrefix="uc" %>
<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="mainContent" runat="server">
    <div class="contentpanel" style="margin-top: 0px !important; padding-top: 0px !important;">
        <div class="innerLR">
            <h4>
                Yearly Leave Report
            </h4>
            <uc:YearlyLeaveReportCtl Id="YearlyLeaveReportCtl2" PageView="ManagerEmployee" runat="server" />
        </div>
    </div>
</asp:Content>
