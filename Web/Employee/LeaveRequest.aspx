﻿<%@ Page Title="Leave Request" Language="C#" EnableViewState="false" AutoEventWireup="true"
    CodeBehind="LeaveRequest.aspx.cs" Inherits="Web.Employee.LeaveRequest" %>

<%@ Register Src="~/Employee/UserControls/LeaveRequestCtl.ascx" TagName="LeaveCtl"
    TagPrefix="uc2" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/FooterCtl.ascx" TagName="FooterCtl" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/HeaderCtl.ascx" TagName="HeaderCtl" TagPrefix="uc3" %>
<%@ Register Src="~/UserControls/LogoutCtl.ascx" TagName="LogoutCtl" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/TopMenu.ascx" TagName="TopMenu" TagPrefix="uc2" %>
<!DOCTYPE html>
<html>
<head runat="server">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Leave Request</title>
    <link id="Link5" runat="server" rel="stylesheet" href="~/payrollmenu_files/mbcsmbmcp.css"
        type="text/css" />
    <link id="Link1" rel="stylesheet" type="text/css" href="<%= ResolveUrl("~/css/core.css?v=") + Web.Helper.WebHelper.Version %>" />
    <link id="Link2" rel="Stylesheet" type="text/css" href="<%= ResolveUrl("~/Styles/calendar/calendar.css?v=") + Web.Helper.WebHelper.Version %>" />
    <ext:ResourcePlaceHolder ID="ResourcePlaceHolder1" runat="server" Mode="Script" />
    <style type="text/css">
        .details
        {
            padding-top: 10px;
        }
        .details tr td
        {
            padding-top: 4px;
        }
        .leavetype .leaveTitle
        {
            width: 128px;
        }
        a.x-datepicker-eventdate
        {
            border: 1px solid #a3bad9;
            padding: 1px; /* background-color: Cyan;*/
        }
        
        
        .icon-delete
        {
            background-image: url("../images/delet.png") !important;
        }
        
        .x-form
        {
            padding: 10px 10px 2px !important;
        }
        .x-form-item
        {
            margin-bottom: 6px;
        }
        .x-form-item label.x-form-item-label
        {
            padding-top: 1px;
        }
        .topgap
        {
            padding-top: 5px;
        }
        thead td, th
        {
            border: 0px solid;
        }
        
        #CompanyX.startTime_Container .x-form-field-wrap
        {
            margin-top:-5px!important;
        }
        
        <%--half day type--%>
        #CompanyX.chkHalfDayType_Container .x-form-item-label
        {
            padding-top:3px!important;
        }

 .itemList {
  margin: 0px;padding:0px;
}
.x-window-body-default {
  background: #E8F1FF;
}
.x-toolbar-footer 
{
    background: #dfeaf2;
    border-top:1px solid lightgray!important;
}
        
        .x-form-spinner-splitter{visibility:hidden!important;}
        #mbmcpebul_table{margin-top:0px!important;}
       .bodypart {
            margin: 0 auto!important;}
           /*hide calendar icon*/
.ext-cal-ic-rem {display:none;}
    </style>
    <script type="text/javascript" src="<%= ResolveUrl("~/Employee/common.js?v=") + Web.Helper.WebHelper.Version %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/Employee/override.js?v=") + Web.Helper.WebHelper.Version %>"></script>
    <script type="text/javascript">

//        window.addEventListener('keydown', function (e) { (e.keyCode == 27 && e.preventDefault()) });
        var currentMonth = null, currentYear = null;
        var holidayList = null; var datePickerChanged = false;
        var hasHourlyLeave = false;
        var chkHalfDayType = null;
        var chkIsHalfDay = null;
        var DatePicker1 = null;
        //var chkIsHourlyLeave = null;
        var isHourlyLeaveCompany = false;

       
        var EventEditWindow1 = null;
        Ext.onReady(function () {
           
            //            window.get(0).form.items.items[2].on('click', function (btn, e, eOpts) {
            //                console.log(btn, e, eOpts);
            //            });
            DatePicker1 = <%=DatePicker1.ClientID %>;

            
        });

        function getChkAllDay() {
            try {
                return CompanyX.EventEditWindow1.items.itemAt(0).items.items[1].items.items[5];
            }
            catch (eee) {
                return undefined;
            }
        }

        var loadHoliday = function (month, year) {
            Ext.net.Mask.show();
            Ext.net.DirectMethod.request({
                url: "../LeaveRequestService.asmx/GetHolidays",
                cleanRequest: true,
                json: true,
                params: {
                    month: month,
                    year: year
                },
                success: function (result) {
                    holidayList = result;
                    //Ext.Msg.alert("Json Message", result);
                    CompanyX.DatePicker1.highlightDates(result);
                    Ext.net.Mask.hide();

                    //find the range case
                    for (i = 0; i < result.length; i++) {
                        if (result[i].Id == 0) {
                            CompanyX.lblCalendarTitle.setText(result[i].Abbreviation);
                            break;
                        }
                    }

                },
                failure: function (result) { Ext.net.Mask.hide(); }
            });
        };

        var calRender = function (e1, e2, e3, e4, e5) {
        }

        //method that will be called when item selected changes for Item in the GridPanel
        var leaveRenderer = function (value) {

            var r = CompanyX.storeLeaves.getById(value);

            if (parseInt(value) == 0)
                return "Unpaid Leave";

            if (Ext.isEmpty(r)) {
                return "";
            }



            return r.data.Title;
        }

        function changeEventWindow(window) {
            //change text
            var hdIsHalfDayLeave = CompanyX.hdIsHalfDayLeave;
            var chkIsHalfDay = CompanyX.chkIsHalfDay;

            

            if (CompanyX.storeLeaves.data.items.length <= 0)
                return;

            var isHalfDayAllowed = CompanyX.storeLeaves.data.items[0].data.IsHalfDayAllowed;
            hdIsHalfDayLeave.setValue(isHalfDayAllowed);

            CompanyX.EventEditWindow1.titleField.setFieldLabel('Reason');
            document.getElementById('calendarpicker-1093-labelEl').innerHTML = "Leave:";


            //Show/Hide IsHalfDay as per Combo LeaveType selection
            CompanyX.EventEditWindow1.calendarField.on('select', function (box, record, index) {
                showHideHalfDayCheckbox(record[0].index);
            });



            //hide edit details link button
            //window.fbar.items.items[0].hide();
            if (chkIsHalfDay == undefined)
                return;
            if (isHalfDayAllowed)
                chkIsHalfDay.show();
            else
                chkIsHalfDay.hide();
        }

        function showHideHalfDayCheckbox(index) {


            var r = CompanyX.storeLeaves.getAt(index);
            if (r.data.IsHalfDayAllowed) {
                //hide td of start time
//                if (isHourlyLeaveCompany == false) {
//                    Ext.get('date-range-end-time').dom.parentNode.parentNode.style.display = 'none';
//                    Ext.get('date-range-end-time').hide();
//                }

                if (CompanyX.chkIsHalfDay != undefined)
                    CompanyX.chkIsHalfDay.show();

            }
            else {
                //hide td of start time
                if (isHourlyLeaveCompany == false) {
//                    Ext.get('date-range-end-time').dom.parentNode.parentNode.style.display = 'none';
//                    Ext.get('date-range-end-time').hide();
                }

                if (CompanyX.chkIsHalfDay != undefined) {
                    CompanyX.chkIsHalfDay.setValue(false);
                    CompanyX.chkIsHalfDay.hide();
                }
            }
        }

        function changeEventWindowAfterRender(window) {
            //chkIsHalfDay = Ext.get('chkIsHalfDay').dom;
            //chkIsHalfDay.parentNode.parentNode.parentNode.parentNode.style.display = "none";

            //            CompanyX.EventEditWindow1.items.itemAt(0).items.items[1].items.items[3].setValue('9:00 AM');
            //            CompanyX.EventEditWindow1.items.itemAt(0).items.items[1].items.items[4].setValue('9:00 AM');
            changeEventWindow(window);

            if (isHourlyLeaveCompany == false) {

                Ext.select('.x-toolbar-left-row').hide();
                //hide  "All day" check box
                CompanyX.EventEditWindow1.dateRangeField.items.items[5].hide();
                //hide "All day" check box label just the side of the checkbox
                //document.getElementsByName('date-range-allday')[0].nextSibling.style.display = "none";

                //hide td of start time
                 CompanyX.EventEditWindow1.dateRangeField.items.items[1].hide();
                 CompanyX.EventEditWindow1.dateRangeField.items.items[3].hide();
                 // make end date left same after hiding time fields
                // CompanyX.EventEditWindow1.dateRangeField.items.items[4].el.dom.style.left = '120px';
            }
//            else {
//                //Show/Hide All Day/hourl leave as per Combo LeaveType selection
//                getChkAllDay().getEl().dom.onchange = function () { //  n('select', function (box, record, index) {
//                    ShowHideControl();
//                };
//            }


            Ext.get('tblink').hide();
           
            //Ext.get('button-1099-btnInnerEl').innerHTML="Close";
            //rename Delete to Cancel
            //Ext.get('delete-btn').dom.getElementsByTagName("button")[0].innerHTML = "Cancel";
            CompanyX.EventEditWindow1.deleteButton.setText("Cancel");
            //rename Cancel to Close
            document.getElementById('button-1099-btnInnerEl').innerHTML = "Close";
            //Ext.get('button-1099-btnEl').dom.getElementsByTagName("button")[0].innerHTML = "Close";
            
        }

        
        var EditCommandHandler = function(record) {
            <%=hdnRequestID.ClientID %>.setValue(record.data.LeaveRequestId);
             <%= btnEdit.ClientID %>.fireEvent('click');
            };

        function deleteHandler(record) {
            if (confirm("Cancel the leave request?")) {
                var leaveRequestId = record.data.LeaveRequestId;
                Ext.net.Mask.show(this);
                Ext.net.DirectMethod.request({
                    url: "../LeaveRequestService.asmx/DeleteLeaveRequest",
                    cleanRequest: true,
                    json: true,
                    params: {
                        leaveRequestId: leaveRequestId
                    },
                    success: function (result) {
                        if (result.IsSuccess.toString() == "true") {
                            holidayList = result;
                            CompanyX.storePendingRequest.reload();
                            CompanyX.EventStore1.reload();
                            CompanyX.storeApprovedRequest.reload();
                        }
                        else{
                            Ext.Msg.show({
                                title: 'Message', //<- the dialog's title  
                                msg: result.ErrorMessage, //<- the message  
                                buttons: Ext.Msg.OK, //<- YES and NO buttons  
                                icon: Ext.Msg.WARNING, // <- error icon  
                                minWidth: 300
                            });
                        }
                        Ext.net.Mask.hide();
                    },
                    failure: function (result) { Ext.net.Mask.hide(); }
                });
            }
        }

        function chkHalfDayRender() {

            

            if (CompanyX.chkIsHalfDay.getValue()) {
                //to 
                CompanyX.EventEditWindow1.dateRangeField.items.items[2].hide();
                CompanyX.EventEditWindow1.dateRangeField.items.items[4].hide();
//                Ext.get('date-range-end-date').dom.parentNode.parentNode.style.display = 'none';
//                Ext.get('date-range-end-date').hide();
                CompanyX.chkHalfDayType.show();
            }
            else {
               CompanyX.EventEditWindow1.dateRangeField.items.items[2].show();
                CompanyX.EventEditWindow1.dateRangeField.items.items[4].show();
//                Ext.get('date-range-end-date').dom.parentNode.parentNode.style.display = 'table';
//                Ext.get('date-range-end-date').show();
                CompanyX.chkHalfDayType.hide();
            }
        }

        function ShowHideControl() {



            if (getChkAllDay().getValue()) {
                //Ext.get('date-range-to-label').hide();
                //Ext.get('date-range-end-date').dom.parentNode.parentNode.style.display = 'none';
                //Ext.get('date-range-end-date').hide();
                CompanyX.cboHourlyLeave.show();
                //CompanyX.EventEditWindow1.items.itemAt(0).items.items[1].items.items[1].hide();
                //document.getElementById('divHourly').style.display = "";
            }
            else {
                Ext.get('date-range-to-label').show();
                Ext.get('date-range-end-date').dom.parentNode.parentNode.style.display = 'table';
                Ext.get('date-range-end-date').show();

                CompanyX.EventEditWindow1.items.itemAt(0).items.items[1].items.items[1].show();
                //                if (isHourlyLeaveCompany == false)
                //                    document.getElementById('divHourly').style.display = "none";
            }

            if (isHourlyLeaveCompany) {
                if (getChkAllDay().checked == false) {
                    CompanyX.cboHourlyLeave.show();
                    //document.getElementById('divHourly').style.display = "";
                }
                else {
                    //document.getElementById('divHourly').style.display = "none";
                    CompanyX.cboHourlyLeave.hide();
                }
            }
        }

        var attachToolTip = function () {

            var reg = /[a-z\ ]?CalendarPanel1\-[a-z\-]+\-evt\-(\d+)/,
                               cls = Ext.fly(this.triggerElement).dom.className,
                               eventId = reg.exec(cls)[1],
                               recordIndex = CompanyX.EventStore1.find('EventId', parseInt(eventId)),
                               eventData = (CompanyX.EventStore1.getAt(recordIndex) == 'undefined' ? null : CompanyX.EventStore1.getAt(recordIndex).data);

            this.body.dom.innerHTML = (eventData == null ? '' : Ext.encode(eventData.Title));
        }
        //onmousewheel
//       CompanyX.DatePicker1.el.dom.addEventListener('mousewheel',function(event){
//            return false;
//        });



    </script>
</head>
<body style="margin: 0px">
    <form runat="server">
    <ext:Hidden ID="hdCompanyHasHourlyLeave" runat="server" />
    <ext:Hidden ID="hdIsHalfDayLeave" runat="server" />
    <ext:Hidden ID="hdStartDateEng" runat="server" />
    <ext:Hidden ID="hdEndDateEng" runat="server" />
    <ext:ResourceManager ID="ResourceManager1" runat="server" DisableViewState="false"
        ShowWarningOnAjaxFailure="false" ScriptMode="Release" Namespace="CompanyX" />
    <uc3:HeaderCtl ID="HeaderCtl1" runat="server" />
    <ext:Hidden ID="hdnRequestID" runat="server">
    </ext:Hidden>
    <ext:LinkButton ID="btnEdit" runat="server" Hidden="true" Cls="hidden-inputs">
        <DirectEvents>
            <Click OnEvent="btnEdit_Click">
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:LinkButton>
    <div id="bodypart">
        <div class="bodypart">
            <ext:Store runat="server" ID="storeLeaves">
                <Model>
                    <ext:Model runat="server" IDProperty="LeaveTypeId">
                        <Fields>
                            <ext:ModelField Name="LeaveTypeId" Type="Int" />
                            <ext:ModelField Name="Title" Type="String" />
                            <ext:ModelField Name="IsHalfDayAllowed" Type="Boolean" />
                            <ext:ModelField Name="NewBalance" Type="Float" />
                            <ext:ModelField Name="FreqOfAccrual" Type="String" />
                        </Fields>
                    </ext:Model>
                </Model>
            </ext:Store>
            <ext:ToolTip ID="toolTipForCalendar" CloseAction="Hide" runat="server" Target="={#{CalendarPanel1}.body}"
                Delegate=".ext-cal-evt" Calendar="={#{CalendarPanel1}}">
                <Listeners>
                    <Show Fn="attachToolTip" />
                </Listeners>
            </ext:ToolTip>
            <h3 style='margin-top: 10px; margin-bottom: 0px; font-size: 16px;'>
                Leave Request</h3>
            <div style="clear: both">
            </div>
            <table>
                <tr>
                    <td colspan="3">
                        <ext:Label runat="server" Height="30" ID="lblCalendarTitle" Text="&nbsp;" />
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <ext:DatePicker ID="DatePicker1" runat="server" Width="219" StyleSpec="border:1px solid #A3BAD9;">
                            <Listeners>
                                <Select Fn="CompanyX.setStartDate" Scope="CompanyX" />
                            </Listeners>
                            <Plugins>
                                <ext:GenericPlugin ID="HighlightDates1" runat="server" InstanceName="Ext.ux.DatePicker.HighlightDates" />
                            </Plugins>
                        </ext:DatePicker>
                        <div runat="server" id="divLeaveBalance" style="padding-top: 10px">
                        </div>
                    </td>
                    <td valign="top" style="padding-left: 10px">
                        <div style="padding-bottom: 5px;">
                            Click or select the date range for leave apply</div>
                        <ext:CalendarPanel ShowNavBar="false" ID="CalendarPanel1" Width="972" HeaderAsText="true"
                            Header="true" Height="415" runat="server" ActiveIndex="2" Border="true" Margins="0">
                            <CalendarStore ID="GroupStore1" runat="server">
                            </CalendarStore>
                            <EventStore ID="EventStore1" runat="server" NoMappings="true">
                                <Proxy>
                                    <ext:AjaxProxy Url="../LeaveRequestService.asmx/GetEvents" Json="true">
                                        <ActionMethods Read="POST" />
                                        <Reader>
                                            <ext:JsonReader Root="d" />
                                        </Reader>
                                    </ext:AjaxProxy>
                                </Proxy>
                                <Mappings>
                                    <ext:ModelField Name="StartDate" Type="Date" DateFormat="M$" />
                                    <ext:ModelField Name="EndDate" Type="Date" DateFormat="M$" />
                                </Mappings>
                                <Parameters>
                                    <ext:StoreParameter Name="leaveRequestFrom" Value="1" />
                                    <ext:StoreParameter Name="StartDate" Mode="Raw" Value="CompanyX.CalendarPanel1.layout.activeItem.lastRenderStart" />
                                    <ext:StoreParameter Name="EndDate" Mode="Raw" Value="CompanyX.CalendarPanel1.layout.activeItem.lastRenderEnd" />
                                </Parameters>
                                <Listeners>
                                    <BeforeLoad Handler="Ext.net.Mask.show();" />
                                    <Load Handler="Ext.net.Mask.hide();" />
                                </Listeners>
                            </EventStore>
                            <DayView Hidden="true" Disabled="true" runat="server">
                            </DayView>
                            <WeekView Hidden="true" Disabled="true" runat="server">
                            </WeekView>
                            <MonthView ID="MonthView1" ShowHeader="true" runat="server" ShowWeekLinks="false"
                                ShowWeekNumbers="false">
                            </MonthView>
                            <Listeners>
                                <ViewChange Fn="CompanyX.viewChange" Scope="CompanyX" />
                                <EventClick Fn="CompanyX.record.show" Scope="CompanyX" />
                                <DayClick Fn="CompanyX.dayClick" Scope="CompanyX" />
                                <RangeSelect Fn="CompanyX.rangeSelect" Scope="CompanyX" />
                                <EventMove Handler="return false;" />
                                <EventResize Fn="CompanyX.record.resize" Scope="CompanyX" />
                                <EventDelete Fn="CompanyX.record.remove" />
                            </Listeners>
                        </ext:CalendarPanel>
                    </td>
                    <td valign="top">
                    </td>
                </tr>
            </table>
            <%--  </Items>
    </ext:Viewport>--%>
            <ext:TabPanel ID="Panel2" runat="server" MinHeight="500" AutoHeight="true" StyleSpec="padding-top:20px"
                AutoWidth="true" DefaultBorder="false">
                <Items>
                    <ext:GridPanel ID="gridPendingRequest" Height="500" StyleSpec="margin-top:5px;border:1px solid #157fcc"
                        Cls="gridtbl" Header="false" runat="server" AutoHeight="true" Title="Leaves Pending Approval">
                        <Store>
                            <ext:Store runat="server" ID="storePendingRequest">
                                <Model>
                                    <ext:Model runat="server" IDProperty="LeaveRequestId" ID="ctl838">
                                        <Fields>
                                            <ext:ModelField Name="LeaveRequestId" Type="Int" />
                                            <ext:ModelField Name="LeaveTypeId" Type="Int" />
                                            <ext:ModelField Name="FromDate" Type="string" />
                                            <ext:ModelField Name="ToDate" Type="string" />
                                            <ext:ModelField Name="DaysOrHours" Type="Float" />
                                            <ext:ModelField Name="Status" Type="String" />
                                            <ext:ModelField Name="Reason" Type="String" />
                                            <ext:ModelField Name="Comment" Type="String" />
                                            <ext:ModelField Name="LeaveName" Type="String" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                                <AutoLoadParams>
                                    <ext:Parameter Name="start" Value="={0}" />
                                    <ext:Parameter Name="limit" Value="={10}" />
                                </AutoLoadParams>
                                <Proxy>
                                    <ext:AjaxProxy Url="../LeaveRequestService.asmx/GetEmployeePendingLeaveRequestList"
                                        Json="true">
                                        <ActionMethods Read="POST" />
                                        <Reader>
                                            <ext:JsonReader Root="d.Data" TotalProperty="d.TotalRecords" />
                                        </Reader>
                                    </ext:AjaxProxy>
                                </Proxy>
                            </ext:Store>
                        </Store>
                        <View>
                        </View>
                        <%--<loadmask showmask="true" />--%>
                        <SelectionModel>
                            <ext:RowSelectionModel ID="RowSelectionModel1" runat="server">
                            </ext:RowSelectionModel>
                        </SelectionModel>
                        <ColumnModel runat="server" ID="columnmodle1">
                            <Columns>
                                <ext:DateColumn Sortable="false" MenuDisabled="true" Text="From" Width="80" DataIndex="FromDate"
                                    Format="yyyy/MM/dd">
                                </ext:DateColumn>
                                <ext:DateColumn Sortable="false" MenuDisabled="true" Header="To" Width="80" DataIndex="ToDate"
                                    Format="yyyy/MM/dd">
                                </ext:DateColumn>
                                <ext:Column Sortable="false" MenuDisabled="true" Header="Leave Name" Width="115"
                                    DataIndex="LeaveName">
                                    <%--  <Renderer Fn="leaveRenderer" />--%>
                                </ext:Column>
                                <ext:Column ID="ColumnDaysOrHours" Sortable="false" MenuDisabled="true" Text="Days"
                                    Width="75" DataIndex="DaysOrHours">
                                </ext:Column>
                                <ext:Column Sortable="false" MenuDisabled="true" Header="Status" Width="100" DataIndex="Status">
                                </ext:Column>
                                <ext:Column Sortable="false" MenuDisabled="true" Header="Reason" Width="250" DataIndex="Reason">
                                </ext:Column>
                                <ext:Column Sortable="false" MenuDisabled="true" Header="Comment" Width="430" DataIndex="Comment">
                                </ext:Column>
                                <ext:CommandColumn Width="30" ButtonAlign="Center">
                                    <Commands>
                                        <ext:GridCommand Icon="Delete" ToolTip-Text="Cancel" CommandName="Delete">
                                            <ToolTip Text="Delete" />
                                        </ext:GridCommand>
                                    </Commands>
                                    <Listeners>
                                        <Command Handler="deleteHandler(record)" />
                                    </Listeners>
                                </ext:CommandColumn>
                                <ext:CommandColumn Width="30" ButtonAlign="Center" runat="server">
                                    <Commands>
                                        <ext:GridCommand Icon="ApplicationEdit" ToolTip-Text="Edit" CommandName="Edit">
                                            <ToolTip Text="Edit" />
                                        </ext:GridCommand>
                                    </Commands>
                                    <Listeners>
                                        <Command Handler="EditCommandHandler(record)" />
                                    </Listeners>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <%--  <Listeners>
                    <command handler="deleteHandler(record)" />
                </Listeners>--%>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar2" Height="34px" runat="server" PageSize="10"
                                DisplayInfo="true" DisplayMsg="Displaying record {0} - {1} of {2}" EmptyMsg="No record to display." />
                        </BottomBar>
                    </ext:GridPanel>
                    <ext:GridPanel StyleSpec="margin-top:5px" ID="gridApprovedRequest" Border="true"
                        Cls="gridtbl" Header="false" runat="server" AutoHeight="true" Title="Leave History">
                        <Store>
                            <ext:Store runat="server" ID="storeApprovedRequest" PageSize="50">
                                <AutoLoadParams>
                                    <ext:Parameter Name="start" Value="={0}" />
                                    <ext:Parameter Name="limit" Value="={50}" />
                                </AutoLoadParams>
                                <Model>
                                    <ext:Model runat="server" ID="ctl651">
                                        <Fields>
                                            <ext:ModelField Name="LeaveRequestId" Type="Int" />
                                            <ext:ModelField Name="LeaveTypeId" Type="Int" />
                                            <ext:ModelField Name="FromDate" Type="string" />
                                            <ext:ModelField Name="ToDate" Type="string" />
                                            <ext:ModelField Name="DaysOrHours" Type="Float" />
                                            <ext:ModelField Name="ApprovedBy" Type="String" />
                                            <ext:ModelField Name="Comment" Type="String" />
                                            <ext:ModelField Name="Status" Type="String" />
                                            <ext:ModelField Name="LeaveName" Type="String" />
                                            <ext:ModelField Name="Reason" Type="String" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                                <Proxy>
                                    <ext:AjaxProxy Url="../LeaveRequestService.asmx/GetEmployeeApprovedLeaveRequestList"
                                        Json="true">
                                        <ActionMethods Read="POST" />
                                        <Reader>
                                            <ext:JsonReader Root="d.Data" TotalProperty="d.TotalRecords" />
                                        </Reader>
                                    </ext:AjaxProxy>
                                </Proxy>
                            </ext:Store>
                        </Store>
                        <View>
                        </View>
                        <%--<loadmask showmask="true" />--%>
                        <SelectionModel>
                            <ext:RowSelectionModel ID="RowSelectionModel2" runat="server">
                            </ext:RowSelectionModel>
                        </SelectionModel>
                        <ColumnModel runat="server" ID="ColumnModel1">
                            <Columns>
                                <ext:DateColumn ID="DateColumn1" runat="server" Sortable="false" Text="From" Width="100"
                                    DataIndex="FromDate" Format="yyyy/MM/dd">
                                </ext:DateColumn>
                                <ext:DateColumn ID="DateColumn2" runat="server" Sortable="false" Text="To" Width="100"
                                    DataIndex="ToDate" Format="yyyy/MM/dd">
                                </ext:DateColumn>
                                <ext:Column ID="Column1" runat="server" Sortable="false" Text="Leave Type" Width="120"
                                    DataIndex="LeaveName">
                                    <%-- <Renderer Fn="leaveRenderer" />--%>
                                </ext:Column>
                                <ext:Column ID="ColumnDaysOrHours2" runat="server" Sortable="false" Text="Days/Hours"
                                    Width="85" DataIndex="DaysOrHours">
                                </ext:Column>
                                <ext:Column ID="Column3" runat="server" Sortable="false" Text="Verified By" Width="125"
                                    DataIndex="ApprovedBy" />
                                <ext:Column ID="Column4" runat="server" Sortable="false" Text="Status" Width="100"
                                    DataIndex="Status" />
                                <ext:Column ID="Column5" runat="server" Sortable="false" Text="Reason" Width="150"
                                    DataIndex="Reason">
                                </ext:Column>
                                <ext:Column ID="Column6" runat="server" Sortable="false" Text="Comment" Width="400"
                                    DataIndex="Comment">
                                </ext:Column>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar1" Height="34px" runat="server" PageSize="10"
                                DisplayInfo="true" DisplayMsg="Displaying leaves {0} - {1} of {2}" EmptyMsg="No leave history to display." />
                        </BottomBar>
                    </ext:GridPanel>
                </Items>
            </ext:TabPanel>
            <uc2:LeaveCtl Id="LeaveCtrl" runat="server" />
        </div>
    </div>
    <uc1:FooterCtl ID="FooterCtl1" runat="server" />
    </form>
</body>
</html>
