﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL.Entity;
using BLL;
using System.IO;

namespace Web.Employee
{
    public partial class EmpActivity : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();
            }
        }

        private void Initialise()
        {
            CleintVisitCombo();
            BindSoftwareNameCombo();
            BindSoftwareDocCombo();
            BindClientVisitRSCombo();

            BindClientVisitGrid();
            BindSoftwareTestingGrid();
            BindDocumentationGrid();
            BindRemoteSupportGrid();
            BindOtherGrid();
        }

        private void ClearClientVisit()
        {
            cmbClientNameCV.Text = "";
            txtDateCV.Text = "";
            tfStartTimeCV.Text = "09:00";
            tfEndTimeCV.Text = "09:00";
            txtRepresentativeCV.Text = "";
            txtIssueCV.Text = "";
            txtResultCV.Text = "";
            txtNextPlannedVisitCV.Text = "";
        }

        private void ClearSoftwareTesting()
        {
            cmbSoftwareST.Text = "";
            txtDateST.Text = "";
            tfStartTimeST.Text = "09:00";
            tfEndTimeST.Text = "09:00";
            txtDescriptionST.Text = "";
            txtResultST.Text = "";
            fileUploadST.Text = "";
            cmbProblemStatusST.Text = "";
            lnkDeleteFile.Hide();
            lnkFileName.Hide();
        }

        private void ClearDocumentation()
        {
            cmbSoftwareDoc.Text = "";
            txtDateDoc.Text = "";
            tfStartTimeDoc.Text = "09:00";
            tfEndTimeDoc.Text = "09:00";
            txtModuleDocumentedDoc.Text = "";
            txtDescriptionDoc.Text = "";
        }

        private void ClearRemoteSupport()
        {
            cmbClientRS.Text = "";
            txtDateRS.Text = "";
            tfStartTimeRS.Text = "09:00";
            tfEndTimeRS.Text = "09:00";
            txtIssueRS.Text = "";
            txtResultRS.Text = "";
            txtNextPlannedVisitDateRS.Text = "";
            cmbSupportModeRS.Text = "";
        }

        private void ClearOther()
        {
            txtWorkType.Text = "";
            txtDateOther.Text = "";
            tfStartTimeOther.Text = "09:00";
            tfEndTimeOther.Text = "09:00";
            txtDescriptionOther.Text = "";
        }

        private void CleintVisitCombo()
        {
            cmbClientNameCV.Store[0].DataSource = ListManager.GetAllClients();
            cmbClientNameCV.Store[0].DataBind();
        }

        private void BindSoftwareNameCombo()
        {
            cmbSoftwareST.Store[0].DataSource = ListManager.GetAllSoftware();
            cmbSoftwareST.Store[0].DataBind();
        }

        private void BindSoftwareDocCombo()
        {
            cmbSoftwareDoc.Store[0].DataSource = ListManager.GetAllSoftware();
            cmbSoftwareDoc.Store[0].DataBind();
        }

        private void BindClientVisitRSCombo()
        {
            cmbClientRS.Store[0].DataSource = ListManager.GetAllClients();
            cmbClientRS.Store[0].DataBind();
        }

        protected void btnAddClientVisit_Click(object sender, DirectEventArgs e)
        {
            hdnActivityId.Text = "";
            ClearClientVisit();
            WClientVisit.Show();
        }
        
        protected void btnSoftwareTesting_Click(object sender, DirectEventArgs e)
        {
            hdnActivityId.Text = "";
            ClearSoftwareTesting();
            WSoftwareTesting.Show();
        }

        protected void btnAddDocumentation_Click(object sender, DirectEventArgs e)
        {
            hdnActivityId.Text = "";
            ClearDocumentation();
            WDocumentation.Show();
        }

        protected void btnNewRemoteSupport_Click(object sender, DirectEventArgs e)
        {
            hdnActivityId.Text = "";
            ClearRemoteSupport();
            WRemoteSupport.Show();
        }

        protected void btnAddNewOther_Click(object sender, DirectEventArgs e)
        {
            hdnActivityId.Text = "";
            ClearOther();
            WOther.Show();
        }

        private void BindClientVisitGrid()
        {
            List<NewActivity> list = NewHRManager.GetNewActivityList((int)ActivityTypeEnum.ClientVisit);

            if (!string.IsNullOrEmpty(txtDateFilter.Text.Trim()))
            {
                DateTime date = BLL.BaseBiz.GetEngDate(txtDateFilter.Text.Trim(), IsEnglish);
                list = list.Where(x => x.DateEng >= date).ToList();
            }

            gridClientVisit.GetStore().DataSource = list;
            gridClientVisit.GetStore().DataBind();
        }

        private void BindSoftwareTestingGrid()
        {
            List<NewActivity> list = NewHRManager.GetNewActivityList((int)ActivityTypeEnum.SoftwareTesting);

            if (!string.IsNullOrEmpty(txtDateFilter.Text.Trim()))
            {
                DateTime date = BLL.BaseBiz.GetEngDate(txtDateFilter.Text.Trim(), IsEnglish);
                list = list.Where(x => x.DateEng > date).ToList();
            }

            gridSoftwareTesting.GetStore().DataSource = list;
            gridSoftwareTesting.GetStore().DataBind();
        }

        private void BindDocumentationGrid()
        {
            List<NewActivity> list = NewHRManager.GetNewActivityList((int)ActivityTypeEnum.Documentation);

            if (!string.IsNullOrEmpty(txtDateFilter.Text.Trim()))
            {
                DateTime date = BLL.BaseBiz.GetEngDate(txtDateFilter.Text.Trim(), IsEnglish);
                list = list.Where(x => x.DateEng > date).ToList();
            }

            gridDocumentation.GetStore().DataSource = list;
            gridDocumentation.GetStore().DataBind();
        }

        private void BindRemoteSupportGrid()
        {
            List<NewActivity> list = NewHRManager.GetNewActivityList((int)ActivityTypeEnum.RemoteSupport);

            if (!string.IsNullOrEmpty(txtDateFilter.Text.Trim()))
            {
                DateTime date = BLL.BaseBiz.GetEngDate(txtDateFilter.Text.Trim(), IsEnglish);
                list = list.Where(x => x.DateEng > date).ToList();
            }

            gridRemoteSupport.GetStore().DataSource = list;
            gridRemoteSupport.GetStore().DataBind();
        }

        private void BindOtherGrid()
        {
            List<NewActivity> list = NewHRManager.GetNewActivityList((int)ActivityTypeEnum.Other);

            if (!string.IsNullOrEmpty(txtDateFilter.Text.Trim()))
            {
                DateTime date = BLL.BaseBiz.GetEngDate(txtDateFilter.Text.Trim(), IsEnglish);
                list = list.Where(x => x.DateEng > date).ToList();
            }

            gridOther.GetStore().DataSource = list;
            gridOther.GetStore().DataBind();
        }

        protected void btnSaveCV_Click(object sender, DirectEventArgs e)
        {
            Page.Validate("SaveClientVisit");
            if (Page.IsValid)
            {
                NewActivity obj = new NewActivity();

                if (!string.IsNullOrEmpty(hdnActivityId.Text))
                    obj.ActivityId = int.Parse(hdnActivityId.Text);
                obj.EmployeeId = SessionManager.CurrentLoggedInEmployeeId;

                obj.ActivityType = (int)ActivityTypeEnum.ClientVisit;
                obj.ClientSoftwareId = int.Parse(cmbClientNameCV.SelectedItem.Value);
                
                obj.Date = txtDateCV.Text.Trim();
                obj.DateEng = BLL.BaseBiz.GetEngDate(obj.Date, IsEnglish);

                if (!string.IsNullOrEmpty(tfStartTimeCV.Text))
                    obj.StartTime = TimeSpan.Parse(tfStartTimeCV.Text);

                if (!string.IsNullOrEmpty(tfEndTimeCV.Text))
                    obj.EndTime = TimeSpan.Parse(tfEndTimeCV.Text);

                double startTime = 0, endTime = 0;

                if (tfEndTimeCV.SelectedTime.TotalHours == 9)
                {
                    NewMessage.ShowWarningMessage("Please select end time.");
                    return;
                }

                startTime = tfStartTimeCV.SelectedTime.TotalHours;
                endTime = tfEndTimeCV.SelectedTime.TotalHours;

                if (startTime > endTime)
                {
                    NewMessage.ShowWarningMessage("Start time cannot be greater than end time.");
                    return;
                }

                obj.Duration = endTime - startTime;

                if(!string.IsNullOrEmpty(txtRepresentativeCV.Text.Trim()))
                    obj.Representative = txtRepresentativeCV.Text.Trim();

                if (!string.IsNullOrEmpty(txtIssueCV.Text.Trim()))
                    obj.Issue = txtIssueCV.Text.Trim();

                if (!string.IsNullOrEmpty(txtResultCV.Text.Trim()))
                    obj.Result = txtResultCV.Text.Trim();

                if (!string.IsNullOrEmpty(txtNextPlannedVisitCV.Text.Trim()))
                {
                    obj.NextPlannedVisit = txtNextPlannedVisitCV.Text.Trim();
                    obj.NextPlannedVisitEng = BLL.BaseBiz.GetEngDate(obj.NextPlannedVisit, IsEnglish);

                    if (obj.DateEng > obj.NextPlannedVisitEng)
                    {
                        NewMessage.ShowWarningMessage("Date cannot be greater than next planned visit date.");
                        return;
                    }
                }

                obj.Status = (int)AtivityStatusEnum.Draft;
                
                Status status = NewHRManager.SaveUpdateClientVisit(obj);
                if (status.IsSuccess)
                {
                    NewMessage.ShowNormalMessage("Client Visit is successfully saved.");
                    BindClientVisitGrid();
                    WClientVisit.Close();
                }
                else
                    NewMessage.ShowWarningMessage(status.ErrorMessage);
            }
        }

        protected void btnSaveST_Click(object sender, DirectEventArgs e)
        {
            Page.Validate("SaveSoftwareTest");
            if (Page.IsValid)
            {
                NewActivity obj = new NewActivity();

                if (!string.IsNullOrEmpty(hdnActivityId.Text))
                    obj.ActivityId = int.Parse(hdnActivityId.Text);
                obj.EmployeeId = SessionManager.CurrentLoggedInEmployeeId;

                obj.ActivityType = (int)ActivityTypeEnum.SoftwareTesting;

                obj.ClientSoftwareId = int.Parse(cmbSoftwareST.SelectedItem.Value);

                obj.Date = txtDateST.Text.Trim();
                obj.DateEng = BLL.BaseBiz.GetEngDate(obj.Date, IsEnglish);

                if (!string.IsNullOrEmpty(tfStartTimeST.Text))
                    obj.StartTime = TimeSpan.Parse(tfStartTimeST.Text);

                if (!string.IsNullOrEmpty(tfEndTimeST.Text))
                    obj.EndTime = TimeSpan.Parse(tfEndTimeST.Text);

                double startTime = 0, endTime = 0;

                if (tfEndTimeST.SelectedTime.TotalHours == 9)
                {
                    NewMessage.ShowWarningMessage("Please select end time.");
                    return;
                }

                startTime = tfStartTimeST.SelectedTime.TotalHours;
                endTime = tfEndTimeST.SelectedTime.TotalHours;

                if (startTime > endTime)
                {
                    NewMessage.ShowWarningMessage("Start time cannot be greater than end time.");
                    return;
                }

                obj.Duration = endTime - startTime;

                if (!string.IsNullOrEmpty(txtDescriptionST.Text.Trim()))
                    obj.Description = txtDescriptionST.Text.Trim();

                if (!string.IsNullOrEmpty(txtResultST.Text.Trim()))
                    obj.Result = txtResultST.Text.Trim();

                if(!string.IsNullOrEmpty(cmbProblemStatusST.SelectedItem.Text))
                    obj.Issue = cmbProblemStatusST.SelectedItem.Text.Trim();

                //file upload section
                string UserFileName = this.fileUploadST.FileName;
                string ServerFileName = Guid.NewGuid().ToString() + Path.GetExtension(UserFileName);

                string relativePath = @"~/uploads/" + ServerFileName;

                if (this.UploadFile(relativePath))
                {
                    obj.FileFormat = Path.GetExtension(this.fileUploadST.FileName).Replace(".", "").Trim();
                    obj.FileType = this.fileUploadST.PostedFile.ContentType;
                    obj.FileLocation = @"../Uploads/";
                    obj.ServerFileName = ServerFileName;
                    obj.UserFileName = UserFileName;

                }

                obj.Status = (int)AtivityStatusEnum.Draft;

                Status status = NewHRManager.SaveUpdateSoftwareTesting(obj);
                if (status.IsSuccess)
                {
                    NewMessage.ShowNormalMessage("Software Testing is successfully saved.");
                    BindSoftwareTestingGrid();
                    WSoftwareTesting.Close();
                }
                else
                    NewMessage.ShowWarningMessage(status.ErrorMessage);
            }
        }

        protected bool UploadFile(string relativePath)
        {

            if (this.fileUploadST.HasFile)
            {

                int fileSize = fileUploadST.PostedFile.ContentLength;
                this.fileUploadST.PostedFile.SaveAs(Server.MapPath(relativePath));
                return true;
            }
            else
                return false;

        }

        protected void btnSaveDoc_Click(object sender, DirectEventArgs e)
        {
            Page.Validate("SaveDocumentation");
            if (Page.IsValid)
            {
                NewActivity obj = new NewActivity();

                if (!string.IsNullOrEmpty(hdnActivityId.Text))
                    obj.ActivityId = int.Parse(hdnActivityId.Text);
                obj.EmployeeId = SessionManager.CurrentLoggedInEmployeeId;

                obj.ActivityType = (int)ActivityTypeEnum.Documentation;

                obj.ClientSoftwareId = int.Parse(cmbSoftwareDoc.SelectedItem.Value);

                obj.Date = txtDateDoc.Text.Trim();
                obj.DateEng = BLL.BaseBiz.GetEngDate(obj.Date, IsEnglish);

                if (!string.IsNullOrEmpty(tfStartTimeDoc.Text))
                    obj.StartTime = TimeSpan.Parse(tfStartTimeDoc.Text);

                if (!string.IsNullOrEmpty(tfEndTimeDoc.Text))
                    obj.EndTime = TimeSpan.Parse(tfEndTimeDoc.Text);

                double startTime = 0, endTime = 0;

                if (tfEndTimeDoc.SelectedTime.TotalHours == 9)
                {
                    NewMessage.ShowWarningMessage("Please select end time.");
                    return;
                }

                startTime = tfStartTimeDoc.SelectedTime.TotalHours;
                endTime = tfEndTimeDoc.SelectedTime.TotalHours;

                if (startTime > endTime)
                {
                    NewMessage.ShowWarningMessage("Start time cannot be greater than end time.");
                    return;
                }

                obj.Duration = endTime - startTime;

                if (!string.IsNullOrEmpty(txtModuleDocumentedDoc.Text.Trim()))
                    obj.Issue = txtModuleDocumentedDoc.Text.Trim();

                if (!string.IsNullOrEmpty(txtDescriptionDoc.Text.Trim()))
                    obj.Description = txtDescriptionDoc.Text.Trim();

                obj.Status = (int)AtivityStatusEnum.Draft;

                Status status = NewHRManager.SaveUpdateDocumentation(obj);
                if (status.IsSuccess)
                {
                    NewMessage.ShowNormalMessage("Documentation is successfully saved.");
                    BindDocumentationGrid();
                    WDocumentation.Close();
                }
                else
                    NewMessage.ShowWarningMessage(status.ErrorMessage);
            }
        }

        protected void btnSaveRS_Click(object sender, DirectEventArgs e)
        {
            Page.Validate("SaveRemoteSupport");
            if (Page.IsValid)
            {
                NewActivity obj = new NewActivity();

                if (!string.IsNullOrEmpty(hdnActivityId.Text))
                    obj.ActivityId = int.Parse(hdnActivityId.Text);
                obj.EmployeeId = SessionManager.CurrentLoggedInEmployeeId;

                obj.ActivityType = (int)ActivityTypeEnum.RemoteSupport;

                obj.ClientSoftwareId = int.Parse(cmbClientRS.SelectedItem.Value);

                obj.Date = txtDateRS.Text.Trim();
                obj.DateEng = BLL.BaseBiz.GetEngDate(obj.Date, IsEnglish);

                if (!string.IsNullOrEmpty(tfStartTimeRS.Text))
                    obj.StartTime = TimeSpan.Parse(tfStartTimeRS.Text);

                if (!string.IsNullOrEmpty(tfEndTimeRS.Text))
                    obj.EndTime = TimeSpan.Parse(tfEndTimeRS.Text);

                double startTime = 0, endTime = 0;

                if (tfEndTimeRS.SelectedTime.TotalHours == 9)
                {
                    NewMessage.ShowWarningMessage("Please select end time.");
                    return;
                }

                startTime = tfStartTimeRS.SelectedTime.TotalHours;
                endTime = tfEndTimeRS.SelectedTime.TotalHours;

                if (startTime > endTime)
                {
                    NewMessage.ShowWarningMessage("Start time cannot be greater than end time.");
                    return;
                }

                obj.Duration = endTime - startTime;

                if(!string.IsNullOrEmpty(cmbSupportModeRS.SelectedItem.Text))
                    obj.Description = cmbSupportModeRS.SelectedItem.Text.Trim();

                if (!string.IsNullOrEmpty(txtIssueRS.Text.Trim()))
                    obj.Issue = txtIssueRS.Text.Trim();

                if (!string.IsNullOrEmpty(txtResultRS.Text.Trim()))
                    obj.Result = txtResultRS.Text.Trim();

                if (!string.IsNullOrEmpty(txtNextPlannedVisitDateRS.Text.Trim()))
                {
                    obj.NextPlannedVisit = txtNextPlannedVisitDateRS.Text.Trim();
                    obj.NextPlannedVisitEng = BLL.BaseBiz.GetEngDate(obj.NextPlannedVisit, IsEnglish);

                    if (obj.DateEng > obj.NextPlannedVisitEng)
                    {
                        NewMessage.ShowWarningMessage("Date cannot be greater than next planned visit date.");
                        return;
                    }
                }

                obj.Status = (int)AtivityStatusEnum.Draft;

                Status status = NewHRManager.SaveUpdateRemoteSupport(obj);
                if (status.IsSuccess)
                {
                    NewMessage.ShowNormalMessage("Remote Support is successfully saved.");
                    BindRemoteSupportGrid();
                    WRemoteSupport.Close();
                }
                else
                    NewMessage.ShowWarningMessage(status.ErrorMessage);
            }
        }

        protected void btnSaveOther_Click(object sender, DirectEventArgs e)
        {
            Page.Validate("SaveOther");
            if (Page.IsValid)
            {
                NewActivity obj = new NewActivity();

                if (!string.IsNullOrEmpty(hdnActivityId.Text))
                    obj.ActivityId = int.Parse(hdnActivityId.Text);
                obj.EmployeeId = SessionManager.CurrentLoggedInEmployeeId;

                obj.ActivityType = (int)ActivityTypeEnum.Other;
                obj.Name = txtWorkType.Text.Trim();
                obj.Date = txtDateOther.Text.Trim();
                obj.DateEng = BLL.BaseBiz.GetEngDate(obj.Date, IsEnglish);

                if (!string.IsNullOrEmpty(tfStartTimeOther.Text))
                    obj.StartTime = TimeSpan.Parse(tfStartTimeOther.Text);

                if (!string.IsNullOrEmpty(tfEndTimeOther.Text))
                    obj.EndTime = TimeSpan.Parse(tfEndTimeOther.Text);

                double startTime = 0, endTime = 0;

                if (tfEndTimeOther.SelectedTime.TotalHours == 9)
                {
                    NewMessage.ShowWarningMessage("Please select end time.");
                    return;
                }

                startTime = tfStartTimeOther.SelectedTime.TotalHours;
                endTime = tfEndTimeOther.SelectedTime.TotalHours;

                if (startTime > endTime)
                {
                    NewMessage.ShowWarningMessage("Start time cannot be greater than end time.");
                    return;
                }

                obj.Duration = endTime - startTime;

                if (!string.IsNullOrEmpty(txtDescriptionOther.Text.Trim()))
                    obj.Description = txtDescriptionOther.Text.Trim();              

                obj.Status = (int)AtivityStatusEnum.Draft;

                Status status = NewHRManager.SaveUpdateOther(obj);
                if (status.IsSuccess)
                {
                    NewMessage.ShowNormalMessage("Record is successfully saved.");
                    BindOtherGrid();
                    WOther.Close();
                }
                else
                    NewMessage.ShowWarningMessage(status.ErrorMessage);
            }
        }

        protected void btnEdit_Click(object sender, DirectEventArgs e)
        {
            int activityId = int.Parse(hdnActivityId.Text);
            int activityType = int.Parse(hdnAcitivityType.Text);
            NewActivity obj = NewHRManager.GetNewActivityById(activityId);
            if (obj == null)
                return;

            switch (activityType)
            {
                case ((int)ActivityTypeEnum.ClientVisit):
                    LoadClientVisitData(obj);
                    break;

                case ((int)ActivityTypeEnum.SoftwareTesting):
                    LoadSoftwareTestingData(obj);
                    break;

                case ((int)ActivityTypeEnum.Documentation):
                    LoadDocumentationData(obj);
                    break;

                case ((int)ActivityTypeEnum.RemoteSupport):
                    LoadRemoteSupportData(obj);
                    break;

                case ((int)ActivityTypeEnum.Other):
                    LoadOtherData(obj);
                    break;
            }
        }

        private void LoadClientVisitData(NewActivity obj)
        {
            ClearClientVisit();
            cmbClientNameCV.ClearValue();
            cmbClientNameCV.SetValue(obj.ClientSoftwareId.ToString());
            txtDateCV.Text = obj.Date;
            tfStartTimeCV.Value = obj.StartTime;
            tfEndTimeCV.Value = obj.EndTime;
            txtRepresentativeCV.Text = obj.Representative;
            txtIssueCV.Text = obj.Issue;
            txtResultCV.Text = obj.Result;
            txtNextPlannedVisitCV.Text = obj.NextPlannedVisit;
            WClientVisit.Show();
        }

        private void LoadSoftwareTestingData(NewActivity obj)
        {
            ClearSoftwareTesting();

            cmbSoftwareST.SetValue(obj.ClientSoftwareId.ToString());

            txtDateST.Text = obj.Date;
            tfStartTimeST.Value = obj.StartTime;
            tfEndTimeST.Value = obj.EndTime;
            txtDescriptionST.Text = obj.Description;
            txtResultST.Text = obj.Result;

            if(!string.IsNullOrEmpty(obj.Issue))
                cmbProblemStatusST.SetValue(obj.Issue);

            if (!string.IsNullOrEmpty(obj.UserFileName))
            {
                lnkFileName.Show();
                lnkFileName.Text = obj.UserFileName;
                lnkDeleteFile.Show();
            }
            else
            {
                lnkFileName.Hide();
                lnkFileName.Text = "";
                lnkDeleteFile.Hide();
            }

            WSoftwareTesting.Show();
        }

        private void LoadDocumentationData(NewActivity obj)
        {
            ClearDocumentation();
            cmbSoftwareDoc.SetValue(obj.ClientSoftwareId.ToString());

            txtDateDoc.Text = obj.Date;
            tfStartTimeDoc.Value = obj.StartTime;
            tfEndTimeDoc.Value = obj.EndTime;
            txtModuleDocumentedDoc.Text = obj.Issue;
            txtDescriptionDoc.Text = obj.Description;
            WDocumentation.Show();
        }

        private void LoadRemoteSupportData(NewActivity obj)
        {
            ClearRemoteSupport();

            cmbClientRS.SetValue(obj.ClientSoftwareId.ToString());

            txtDateRS.Text = obj.Date;
            tfStartTimeRS.Value = obj.StartTime;
            tfEndTimeRS.Value = obj.EndTime;

            if(!string.IsNullOrEmpty(obj.Description))
                cmbSupportModeRS.SelectedItem.Text = obj.Description;

            txtIssueRS.Text = obj.Issue;
            txtResultRS.Text = obj.Result;
            txtNextPlannedVisitDateRS.Text = obj.NextPlannedVisit;
            WRemoteSupport.Show();
        }

        private void LoadOtherData(NewActivity obj)
        {
            ClearOther();
            txtWorkType.Text = obj.Name;
            txtDateOther.Text = obj.Date;
            tfStartTimeOther.Value = obj.StartTime;
            tfEndTimeOther.Value = obj.EndTime;
            txtDescriptionOther.Text = obj.Description;
            WOther.Show();
        }


        protected void btnDelete_Click(object sender, DirectEventArgs e)
        {
            int activityId = int.Parse(hdnActivityId.Text);
            int activityType = int.Parse(hdnAcitivityType.Text);


            NewActivity obj = NewHRManager.GetNewActivityById(activityId);

            string path = Context.Server.MapPath(obj.FileLocation + obj.ServerFileName);


            Status status = NewHRManager.DeleteNewActivityDetail(activityId);
            if (status.IsSuccess)
            {
                if (activityType == (int)ActivityTypeEnum.ClientVisit)
                    BindClientVisitGrid();
                else if (activityType == (int)ActivityTypeEnum.SoftwareTesting)
                {
                    BindSoftwareTestingGrid();

                    if (File.Exists(path))
                    {
                        File.Delete(path);
                    }

                }
                else if (activityType == (int)ActivityTypeEnum.Documentation)
                    BindDocumentationGrid();
                else if (activityType == (int)ActivityTypeEnum.RemoteSupport)
                    BindRemoteSupportGrid();
                else
                    BindOtherGrid();

                NewMessage.ShowNormalMessage("Record deleted successfully.");
            }
            else
                NewMessage.ShowWarningMessage(status.ErrorMessage);
        }

        protected void lnkFileName_Click(object sender, DirectEventArgs e)
        {
            int activityId = int.Parse(hdnActivityId.Text);
            DownloadFile(activityId);
        }

        protected void DownloadFile(int activityId)
        {
            NewActivity obj = NewHRManager.GetNewActivityById(activityId);

            string path = Context.Server.MapPath(obj.FileLocation + obj.ServerFileName);
            string contentType = obj.FileType;
            string name = obj.UserFileName + "." + obj.FileFormat;

            byte[] bytes = File.ReadAllBytes(path);

            Context.Response.Clear();
            Context.Response.ClearHeaders();
            Context.Response.AppendHeader("Content-Disposition", "attachment; filename=\"" + name + "\"");
            Context.Response.ContentType = contentType;
            Context.Response.BinaryWrite(bytes);
            Context.Response.End();
        }

        protected void lnkDeleteFile_Click(object sender, DirectEventArgs e)
        {
            int activityId = int.Parse(hdnActivityId.Text);

            Status status = new Status();
            NewActivity obj = NewHRManager.GetNewActivityById(activityId);

            string path = Context.Server.MapPath(obj.FileLocation + obj.ServerFileName);
            status = NewHRManager.DeleteSoftwareTestingFile(activityId);
            if (status.IsSuccess)
            {
                if (File.Exists(path))
                {
                    File.Delete(path);
                    lnkDeleteFile.Hide();
                    lnkFileName.Hide();
                    NewMessage.ShowNormalMessage("File deleted successfully.");
                }
            }
        }

        protected void btnLoad_Click(object sender, DirectEventArgs e)
        {
            BindClientVisitGrid();
            BindSoftwareTestingGrid();
            BindDocumentationGrid();
            BindRemoteSupportGrid();
            BindOtherGrid();
        }

        protected void btnDownload_Click(object sendeer, DirectEventArgs e)
        {
            int activityId = int.Parse(hdnActivityId.Text);
            DownloadFile(activityId);
        }
    }
}