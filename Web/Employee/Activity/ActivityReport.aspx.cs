﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ext.Net;
using DAL;
using Bll;
using BLL.Manager;
using BLL;
using System.IO;


namespace Web.Activity
{
    public partial class ActivityReport : System.Web.UI.Page
    {

        //private int empId;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                //this.storeActivity.DataSource = this.Data;
                //this.storeActivity.DataBind();
                //this.resourceManager1.RegisterIcon(Icon.Eye);
                //this.resourceManager1.RegisterIcon(Icon.ApplicationEdit);

                ResourceManager resourceMaster = ResourceManager.GetInstance();
                resourceMaster.RegisterIcon(Icon.PageGo);
                resourceMaster.RegisterIcon(Icon.PageEdit);

                Initialize();
            }
        }

        protected void btnDelete_Click(object sender, DirectEventArgs e)
        {
            int ActivityID = Convert.ToInt32(this.hiddenValue.Text.Trim());

            Status status = ActivityManager.DeleteActivity(ActivityID);

            if (status.IsSuccess)
            {
                LoadActivities();
            }
            else
                NewMessage.ShowWarningMessage(status.ErrorMessage);

        }

        protected void btnView_Click(object s, DirectEventArgs e)
        {
            int ActivityID = Convert.ToInt32(this.hiddenValue.Text.Trim());
            LoadWindowData(ActivityID);
            winActivity.Show();
        }

        #region Methods...
        private void Initialize()
        {
            LoadActivities();
        }

        private void LoadActivities()
        {

            this.storeActivity.DataSource = ActivityManager.GetAllUnreadActivities(SessionManager.CurrentLoggedInEmployeeId);
            this.storeActivity.DataBind();
        }

        private void LoadWindowData(int ActivityID)
        {

            // Bind Employee Details in bold
            BindEmployeeDetails();

            DAL.Activiti entityActivity = ActivityManager.GetActivityFromID(ActivityID);

            string activityDate = entityActivity.ActivityDateEng.Value.ToShortDateString();
            lblActivityDate.Text = activityDate;
            lblSubmissionDate.Text = entityActivity.SubmissionDate.Value.ToShortDateString();

            txtDetails.Text = entityActivity.Details;


            List<ActivityLearning> lstLearnings = ActivityManager.GetLearningsFromID(ActivityID);
            lblLearnings.Text = "";
            foreach (ActivityLearning learning in lstLearnings)
            {
                lblLearnings.AppendLine("\u2022 " + learning.Text);
                //lblLearnings.AppendLine(Environment.NewLine);
            }
                        
            List<ActivityAchvement> lstAchievements = ActivityManager.GetAchievementsFromID(ActivityID);
            lblAchievements.Text = "";
            foreach (ActivityAchvement achievement in lstAchievements)
            {
                lblAchievements.AppendLine("\u2022 "+achievement.Text);

                //lblAchievements.Html += "<ul><li> " + achievement.Text + "</li></ul>";
            }

            string fileName = entityActivity.AttachmentName;
            if (!string.IsNullOrEmpty(fileName))
            {
                btnAttachment.Enable();
                btnAttachment.Text = fileName;
            }
            else
            {
                btnAttachment.Text = " NONE";
                btnAttachment.Disable();
            }
        }

        private void BindEmployeeDetails()
        {
            EEmployee entityEmp = ActivityManager.GetEmployee(SessionManager.CurrentLoggedInEmployeeId);

            int branchId = Convert.ToInt32(entityEmp.BranchId);
            int designationId = Convert.ToInt32(entityEmp.DesignationId);

            Branch entityBranch = ActivityManager.GetBranch(branchId);
            EDesignation entityDesignation = ActivityManager.GetDesignation(designationId);

            lblEmpName.Text = entityEmp.Name;
            lblEmpBranch.Text = entityBranch.Name;
            lblEmpPosition.Text = entityDesignation.Name;
        }

        protected void btnAttachment_Click(object s, DirectEventArgs e)
        {
            int ActivityID = Convert.ToInt32(hiddenValue.Text.Trim());
            string queryFile = ActivityManager.GetAttachmentURLFromID(ActivityID);
            string originalName = ActivityManager.GetAttachmentFromID(ActivityID);

            string filePath = Server.MapPath(@"~/Uploads/" + queryFile);
            originalName = originalName.Trim();

            //FileInfo file = new FileInfo(filePath);
            //if (file.Exists)
            //{
            //    Response.Clear();
            //    Response.AddHeader("Content-Disposition", "attachment; filename=" + originalName);
            //    Response.AddHeader("Content-Length", file.Length.ToString());
            //    Response.ContentType = Utils.Helper.Util.GetMimeType(Path.GetExtension(filePath));//"application/octet-stream";
            //    Context.Response.WriteFile(file.FullName);
            //    Response.End();
            //}
            //else
            //{
            //    NewMessage.ShowWarningMessage("This file does not exist.");
            //}

            FileInfo file = new FileInfo(filePath);
            if (file.Exists && !string.IsNullOrEmpty(filePath) && !string.IsNullOrEmpty(originalName))
            {
                //File file = new File();


                
                
                    //write file 
                    HttpContext.Current.Response.ClearHeaders();
                    HttpContext.Current.Response.ContentType = Utils.Helper.Util.GetMimeType(Path.GetExtension(filePath));
                    HttpContext.Current.Response.AddHeader("Content-Disposition", String.Format("attachment;filename=\"{0}\"", originalName));

                    HttpContext.Current.Response.WriteFile(file.FullName);

                    HttpContext.Current.Response.End();
                
            }


        }

        protected void RefreshGrid(object s, StoreReadDataEventArgs e)
        {
            this.LoadActivities();
        }
        #endregion
    }
}