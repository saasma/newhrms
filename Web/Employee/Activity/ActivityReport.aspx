﻿<%@ Page Title="Activity Report" Language="C#" MasterPageFile="~/Master/EmployeeExt.Master"
    AutoEventWireup="true" CodeBehind="ActivityReport.aspx.cs" Inherits="Web.Activity.ActivityReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .bodyContainer
        {
            background-color: #E8F1FF;
            padding: 20px 0 20px 30px;
            margin: 20px;
            border: 1px solid #BFD9FB;
            border-radius: 3px;
            width: 900px;
            display: block;
            float: left;
        }
        
        .grids
        {
            padding: 0 0 0 0;
            border: 1px solid silver;
            border-radius: 2px;
            width: 900px;
            margin-bottom: 5px;
            height: auto;
        }
        #lblAttachment
        {
            border: 2px solid #ccc;
            padding: 5px 10px;
            background: #eee;
            margin: 5px;
            float: none;
        }
        .bodypart
        {
            margin: 0 auto !important;
        }
        p
        {
            margin: 10px 0;
        }
        
        .windowContentWrapper td
        {
            vertical-align: middle;
            margin-right: 0;
            padding: 10px 10px 10px 0;
        }
        .windowContentWrapper table
        {
        }
        .x-window-body-default
        {
            background: #E8F1FF;
        }
        .windowContentWrapper
        {
            margin: 0;
            padding: 5px;
        }
        .bolds
        {
            padding: 0 100px 0 0;
        }
        .bolds .x-label-value
        {
            color: Gray;
            font-size: large;
            font-weight: bold;
        }
        .boldLabels
        {
            font-weight: bold;
            font-size: 15px;
        }
        .hr
        {
            margin-bottom: 5px;
            display: block;
            border-bottom: 1px solid #6495ED;
        }
        
        .darkOrange
        {
            margin-left: 5px;
            color: #ff8c00;
        }
    </style>
    <script type="text/javascript">

        Ext.onReady(function() {
            window.name='winActivities';
        });
        

         var prepareDelete = function (grid, toolbar, rowIndex, record) {
            var cmdButton = toolbar.items.getAt(0);

//            Ext.net.ResourceMgr.registerIcon(["Eye"]);
//            Ext.net.ResourceMgr.registerIcon(["ApplicationEdit"]);

            if (record.data.Status ==3) {
                cmdButton.hide();
            }
            else {
                cmdButton.setText("Delete");
                cmdButton.setIconCls("icon-pagedelete");
                cmdButton.command = "Delete";
                cmdButton.setWidth(75);
            }
            //toolbar.doLayout();
        };

        var prepare = function (grid, toolbar, rowIndex, record) {
            var cmdButton = toolbar.items.getAt(0);

//            Ext.net.ResourceMgr.registerIcon(["Eye"]);
//            Ext.net.ResourceMgr.registerIcon(["ApplicationEdit"]);

            if (record.data.Status == 2 || record.data.Status ==3) {
                cmdButton.setText("View");
                cmdButton.setIconCls("icon-pagego");
                cmdButton.command = "view";
                cmdButton.setWidth(75);
            }
            else {
                cmdButton.setText("Edit");
                cmdButton.setIconCls("icon-pageedit");
                cmdButton.command = "edit";
                cmdButton.setWidth(75);
            }
            //toolbar.doLayout();
        };

        var activityID;

        

        var CommandHandler = function (command, record) {
            activityID = record.data.ActivityID;
            <%= hiddenValue.ClientID %>.setValue(activityID);
            if(command == "edit") {
                window.open("DailyActivityReport.aspx?id="+activityID,"winActivityReport");
            }
            else {
                <%= btnView.ClientID %>.fireEvent('click');
            }
        };

        var CommandHandlerDelete = function (command, record) {
            activityID = record.data.ActivityID;
            <%= hiddenValue.ClientID %>.setValue(activityID);
            
                <%= btnDelete.ClientID %>.fireEvent('click');
            
        };

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
    <div style="margin-top: 10px">
    </div>
    <%--<ext:ResourceManager ID="resourceManager1" runat="server" />--%>
    <div class="contentArea">
        <ext:Hidden ID="hiddenValue" runat="server" />
        <ext:LinkButton ID="btnView" runat="server" Hidden="true">
            <DirectEvents>
                <Click OnEvent="btnView_Click">
                    <EventMask ShowMask="true" />
                </Click>
            </DirectEvents>
        </ext:LinkButton>
        <ext:LinkButton ID="btnDelete" runat="server" Hidden="true">
            <DirectEvents>
                <Click OnEvent="btnDelete_Click">
                    <Confirmation ConfirmRequest="true" Message="Are you sure, you want to delete the activity?" />
                    <EventMask ShowMask="true" />
                </Click>
            </DirectEvents>
        </ext:LinkButton>
        <ext:Store ID="storeActivity" runat="server" PageSize="50" OnReadData="RefreshGrid">
            <Model>
                <ext:Model ID="modelActivity" runat="server">
                    <Fields>
                        <ext:ModelField Name="ActivityDateEng" Type="Date" />
                        <ext:ModelField Name="SharedWith" Type="String" />
                        <ext:ModelField Name="SharedDate" Type="Date" DateFormat="M/d/Y" />
                        <ext:ModelField Name="Statuss" Type="String" />
                        <ext:ModelField Name="Status" Type="Int" />
                        <ext:ModelField Name="ActivityID" Type="Int" />
                    </Fields>
                </ext:Model>
            </Model>
        </ext:Store>
        <div>
            <ext:Button ID="btnNewActivityReport" runat="server" Text="New Activity Report" OnClientClick="window.location = 'DailyActivityReport.aspx';"
                Cls="btn btn-primary">
            </ext:Button>
        </div>
        <div>
            <p>
                Your reports yet to be read.</p>
            <div class="grids">
                <ext:GridPanel ID="gridActivity" runat="server" StoreID="storeActivity" AutoScroll="true"
                    MinHeight="400">
                    <ColumnModel>
                        <Columns>
                            <%--<ext:Column ID="colActivityDate" runat="server" Flex="1" Text="Activity Date" MenuDisabled="true" Sortable="false" DataIndex="ActivityDate" Align="Left">
                        </ext:Column>--%>
                            <ext:DateColumn ID="colDate" runat="server" Width="300" Text="Activity Date" MenuDisabled="true"
                                Sortable="false" DataIndex="ActivityDateEng" Align="Left" Format="dddd - dd MMMM"
                                Draggable="false" Resizable="false">
                            </ext:DateColumn>
                            <ext:Column ID="colSharedWith" Hidden="true" runat="server" Width="200" Text="Shared With"
                                MenuDisabled="true" Sortable="false" DataIndex="SharedWith" Align="Left" Draggable="false"
                                Resizable="false">
                            </ext:Column>
                            <ext:Column ID="colSharedDate" Hidden="true" runat="server" Width="100" Text="Shared Date"
                                MenuDisabled="true" Sortable="false" DataIndex="SharedDate" Align="Left" Draggable="false"
                                Resizable="false">
                            </ext:Column>
                            <ext:Column ID="colStatus" runat="server" Width="300" Text="Status" MenuDisabled="true"
                                Sortable="false" DataIndex="Statuss" Align="Left" Draggable="false" Resizable="false">
                            </ext:Column>
                            <ext:CommandColumn ID="cmdCol1" runat="server" Width="100" Align="Left" Draggable="false"
                                Sortable="false" MenuDisabled="true" Resizable="false">
                                <Commands>
                                    <ext:GridCommand>
                                    </ext:GridCommand>
                                </Commands>
                                <PrepareToolbar Fn="prepare" />
                                <Listeners>
                                    <Command Handler="CommandHandler(command, record);" />
                                </Listeners>
                            </ext:CommandColumn>
                            <ext:CommandColumn ID="CommandColumn1" runat="server" Width="150" Align="Left" Draggable="false"
                                Sortable="false" MenuDisabled="true" Resizable="false">
                                <Commands>
                                    <ext:GridCommand>
                                    </ext:GridCommand>
                                </Commands>
                                <PrepareToolbar Fn="prepareDelete" />
                                <Listeners>
                                    <Command Handler="CommandHandlerDelete(command, record);" />
                                </Listeners>
                            </ext:CommandColumn>
                        </Columns>
                    </ColumnModel>
                    <BottomBar>
                        <ext:PagingToolbar ID="pagingActivity" runat="server">
                        </ext:PagingToolbar>
                    </BottomBar>
                </ext:GridPanel>
            </div>
        </div>
    </div>
    <div>
        <ext:Window ID="winActivity" MinWidth="840" MinHeight="500" Title="Daily Activity Report"
            Closable="true" Resizable="false" Hidden="true" runat="server" AutoScroll="true">
            <Content>
                <div class="windowContentWrapper" style="padding: 10px; padding-left: 15px;">
                    <table>
                        <tr>
                            <td colspan="2">
                                <span class="bolds">
                                    <ext:Label ID="lblEmpName" runat="server" Text="">
                                    </ext:Label>
                                </span><span class="bolds">
                                    <ext:Label ID="lblEmpPosition" runat="server" Text="">
                                    </ext:Label>
                                </span><span class="bolds">
                                    <ext:Label ID="lblEmpBranch" runat="server" Text="">
                                    </ext:Label>
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <span class="boldLabels">Activity Date:</span>
                                <ext:Label ID="lblActivityDate" runat="server" />
                            </td>
                            <td>
                                <span class="boldLabels">Submission Date:</span>
                                <ext:Label ID="lblSubmissionDate" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <span class="boldLabels hr" style="color: #6495ED;">Key Learnings</span>
                                <div style="margin-left: 15px;">
                                    <ext:Label ID="lblLearnings" runat="server" Text="[Learnings]">
                                    </ext:Label>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <span class="boldLabels hr" style="color: #6495ED;">Key Achievements</span>
                                <div style="margin-left: 15px;">
                                    <ext:Label ID="lblAchievements" runat="server" Text="[Achievements]">
                                    </ext:Label>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <span class="boldLabels" style="color: #6495ED;">Details</span>
                                <div style="background-color: #ffffff; padding: 10px; border: 1px solid silver; max-height: 150px;
                                    overflow-x: scroll;">
                                    <ext:TextArea ID="txtDetails" runat="server" Width="770px" MinHeight="0" AutoScroll="true"
                                        Height="20px">
                                    </ext:TextArea>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <div style="display: flex;">
                                    <span class="boldLabels">Attachment:</span>
                                    <ext:LinkButton ID="btnAttachment" runat="server" Cls="darkOrange">
                                        <DirectEvents>
                                            <Click OnEvent="btnAttachment_Click" IsUpload="true" Success="Ext.net.DirectMethods.Download({ isUpload : true });">
                                                <EventMask ShowMask="false" />
                                            </Click>
                                        </DirectEvents>
                                    </ext:LinkButton>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <ext:Button ID="btnCancel" runat="server" Text="Cancel" Cls="btnFlatLeftGap" BaseCls="btnFlatLeftGap">
                                    <Listeners>
                                        <Click Handler="#{winActivity}.hide();" />
                                    </Listeners>
                                </ext:Button>
                            </td>
                        </tr>
                    </table>
                </div>
            </Content>
        </ext:Window>
    </div>
</asp:Content>
