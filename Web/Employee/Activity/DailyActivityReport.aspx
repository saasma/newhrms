﻿<%@ Page Title="Daily Activity Report" Language="C#" MasterPageFile="~/Master/EmployeeExt.Master"
    AutoEventWireup="true" CodeBehind="DailyActivityReport.aspx.cs" Inherits="Web.Activity.DailyActivityReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        p
        {
            padding: 5px;
            margin: 0;
        }
        
        p span
        {
            color: #808080;
            font-style: italic;
            margin-left: 15px;
        }
        
        .buttons
        {
            display: inline;
            position: relative;
            float: left;
            margin-right: 30px;
        }
        
        hr
        {
            width: 800px;
            margin-left: 0px;
        }
        .bodyContainer
        {
            background-color: #E8F1FF;
            padding: 20px 0 20px 30px;
            margin: 20px;
            border: 1px solid #BFD9FB;
            border-radius: 3px;
            width: 800px;
            display: block;
            float: left;
        }
        
        .components
        {
            margin: 20px 0;
        }
        .grids
        {
            padding: 0 0 0 0;
            border: 1px solid silver;
            border-radius: 2px;
            width: 800px;
            margin-bottom: 5px;
            height: auto;
        }
        #fiButtonMsg
        {
            border: 2px solid #ccc;
            padding: 5px 10px;
            background: #eee;
            float: none;
            width: 250px;
            overflow: hidden;
            text-overflow: ellipsis;
            white-space: nowrap;
            display: none;
        }
        .bodypart
        {
            margin: 0 auto !important;
        }
        
        .lblAttachment
        {
            font-weight: bold;
        }
        #divUpload
        {
            padding: 5px 10px;
            float: none;
            width: 400px;
            overflow: hidden;
            text-overflow: ellipsis;
            white-space: nowrap;
            display: inline;
            margin: 2px 0 0 10px;
        }
        .right
        {
            float: right;
        }
        .left
        {
            float: left;
            width: 90%;
            overflow: hidden;
            text-overflow: ellipsis;
            white-space: nowrap;
            padding: 2px;
        }
        .btnFlat .x-form-file-btn
        {
            background: transparent;
            background-color: transparent;
            padding: 10px 32px;
            background-image: none;
            height: 36px;
            width: 100%;
        }
        .btnFlat .x-form-file-btn .x-btn-inner
        {
            color: #ffffff;
        }
        .dateField
        {
            position: relative;
            margin-left: 0;
        }
    </style>
    <script type="text/javascript">

        var showFile = function (fb, v) {
            if (v) {
                var el = Ext.get('divUpload');
                
                var btnAttachment = <%= btnAttachment.ClientID %>;
                btnAttachment.setText(v);
                btnAttachment.show();

                if (!el.isVisible()) {
                    el.slideIn('t', {
                        duration: .0,
                        easing: 'easeIn',
                        callback: function () {
                            el.highlight();
                        }
                    });
                } else {
                    el.highlight();
                }
            }
        };

        var afterEdit = function (e1, e2) {
            e2.record.commit();
        };

        var closeWindow = function () {
            if (confirm("Are you sure you want to close the window?")) {
                 window.open('','_self','').close();
            }
        };

        var resetFileUpload = function () { 
            if (confirm("Are you sure you want remove the file?")) {
               
            
                var upload1 = <%= upload1.ClientID %>;
                upload1.reset();
                hideUploadDiv();

                var hiddenResetBool = <%= hiddenResetBool.ClientID %>;
                hiddenResetBool.setValue(1);
            }
        };

        var hideUploadDiv = function() {
            var el = Ext.get('divUpload');
            el.hide();
        };

        var refreshParentAndClose = function() {
            window.opener.location.reload();
            window.close();
        };

        var afterSend = function() {
            alert("Activity sent for verification.");
            if(window.opener==null)
            {
                window.open('ActivityReport.aspx','_self');
            }
            else
            {
                if(window.opener.name=='winActivityReport')
                {
                    window.open('ActivityReport.aspx','_self');
                }
                else
                {
                    refreshParentAndClose();
                }
            }
        };
    Ext.onReady(function(){
        CKEDITOR.config.toolbar = [
   ['Styles','Format','Font','FontSize'],
   
   ['Bold','Italic','Underline','StrikeThrough','-','Undo','Redo','-','Cut','Copy','Paste','Find','Replace','-','Outdent','Indent','-'],
   '/',
   ['NumberedList','BulletedList','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
   ['Image','Table','-','Link','TextColor','BGColor']
] ;
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
    <%-- <ext:ResourceManager runat="server" />--%>
    <ext:Hidden ID="hiddenResetBool" runat="server" />
    <ext:Hidden ID="hiddenID" runat="server" />
    <div class="contentArea">
        <ext:Store ID="storeLearnings" runat="server">
            <Model>
                <ext:Model runat="server" ID="modelLearnings" Name="learningItem">
                    <Fields>
                        <ext:ModelField Name="Text" Type="String" />
                    </Fields>
                </ext:Model>
            </Model>
        </ext:Store>
        <ext:Store ID="storeAchivements" runat="server">
            <Model>
                <ext:Model ID="modelAchivements" runat="server" Name="achivementItem">
                    <Fields>
                        <ext:ModelField Name="Text" Type="String" />
                    </Fields>
                </ext:Model>
            </Model>
        </ext:Store>
        <div>
            <div style="height: 50px;">
                <h3>
                    Daily Activity Report</h3>
            </div>
            <div>
                <table>
                    <tr>
                        <td>
                            <div style="">
                                <ext:DateField ID="dateActivity" runat="server" FieldLabel="Activity Date *" LabelAlign="Top"
                                    LabelSeparator="" Width="160px">
                                </ext:DateField>
                            </div>
                        </td>
                        <td style="padding-left: 20px">
                            <div>
                                <ext:ComboBox ID="cmbApplyTo" DisplayField="Text" LabelAlign="Top" ValueField="Value"
                                    LabelSeparator="" ForceSelection="true" Width="220" LabelWidth="75" runat="server"
                                    FieldLabel="Apply to *">
                                    <Store>
                                        <ext:Store ID="store4" runat="server">
                                            <Model>
                                                <ext:Model ID="Model5" runat="server" IDProperty="Value">
                                                    <Fields>
                                                        <ext:ModelField Name="Value" Type="String" />
                                                        <ext:ModelField Name="Text" />
                                                    </Fields>
                                                </ext:Model>
                                            </Model>
                                        </ext:Store>
                                    </Store>
                                </ext:ComboBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Apply to is required."
                                    ControlToValidate="cmbApplyTo" ValidationGroup="AssignActivity" Display="None" />
                            </div>
                        </td>
                    </tr>
                </table>
                <div class="components">
                    <div class="grids">
                        <p>
                            Key Learnings of the Day <span>Share what you learned today</span></p>
                        <ext:GridPanel ID="gridLearnings" Width="800px" MinHeight="0" AutoScroll="true" Title="Key Learnings of the Day"
                            StoreID="storeLearnings" runat="server" Header="false">
                            <ColumnModel>
                                <Columns>
                                    <ext:RowNumbererColumn ID="RowNumbererColumn1" Width="25px" runat="server" Align="Left">
                                    </ext:RowNumbererColumn>
                                    <ext:Column ID="Column1" Flex="1" DataIndex="Text" Text="Key Learnings" runat="server">
                                        <Editor>
                                            <ext:TextField ID="txtLearning" runat="server">
                                            </ext:TextField>
                                        </Editor>
                                    </ext:Column>
                                    <ext:CommandColumn ID="CommandColumn1" runat="server" Width="22px" Align="Left">
                                        <Commands>
                                            <ext:GridCommand Icon="Delete" CommandName="Delete" ToolTip-Text="Remove">
                                            </ext:GridCommand>
                                        </Commands>
                                        <Listeners>
                                            <Command Handler="if(command=='Delete') { #{storeLearnings}.remove(record); }" />
                                        </Listeners>
                                    </ext:CommandColumn>
                                </Columns>
                            </ColumnModel>
                            <SelectionModel>
                                <ext:CellSelectionModel ID="cellSelectionLearning" runat="server">
                                </ext:CellSelectionModel>
                            </SelectionModel>
                            <Plugins>
                                <ext:CellEditing ID="cellEditingLearning" runat="server" ClicksToEdit="1">
                                    <Listeners>
                                        <Edit Fn="afterEdit" />
                                    </Listeners>
                                </ext:CellEditing>
                            </Plugins>
                        </ext:GridPanel>
                    </div>
                    <div style="clear: both; margin-top: 20px;">
                    </div>
                    <ext:Button ID="btnAddLearning" runat="server" Text="Add New Row" Cls="btn btn-primary">
                        <Listeners>
                            <Click Handler="var newRow = new learningItem(); var rowIndex = #{storeLearnings}.data.items.length; #{storeLearnings}.insert(rowIndex, newRow);" />
                        </Listeners>
                    </ext:Button>
                </div>
                <div class="components">
                    <div class="grids">
                        <p>
                            Key Achievements of the Day <span>Share what you did today</span></p>
                        <ext:GridPanel ID="gridAchivements" Width="800px" MinHeight="0" AutoScroll="true"
                            Title="Key Achievements of the Day" StoreID="storeAchivements" runat="server"
                            Header="false">
                            <ColumnModel>
                                <Columns>
                                    <ext:RowNumbererColumn ID="RowNumbererColumn2" runat="server" Width="25px" Align="Left">
                                    </ext:RowNumbererColumn>
                                    <ext:Column ID="Column2" Flex="1" Text="Key Achievements" Align="Left" DataIndex="Text"
                                        runat="server">
                                        <Editor>
                                            <ext:TextField ID="txtAchivement" runat="server">
                                            </ext:TextField>
                                        </Editor>
                                    </ext:Column>
                                    <ext:CommandColumn ID="CommandColumn2" runat="server" Align="Left" Width="22px">
                                        <Commands>
                                            <ext:GridCommand Icon="Delete" CommandName="Delete" ToolTip-Text="Remove">
                                            </ext:GridCommand>
                                        </Commands>
                                        <Listeners>
                                            <Command Handler="if(command=='Delete') { #{storeAchivements}.remove(record); }" />
                                        </Listeners>
                                    </ext:CommandColumn>
                                </Columns>
                            </ColumnModel>
                            <SelectionModel>
                                <ext:CellSelectionModel ID="cellSelectionAchivement" runat="server">
                                </ext:CellSelectionModel>
                            </SelectionModel>
                            <Plugins>
                                <ext:CellEditing ID="cellEditingAchivement" runat="server" ClicksToEdit="1">
                                    <Listeners>
                                        <Edit Fn="afterEdit" />
                                    </Listeners>
                                </ext:CellEditing>
                            </Plugins>
                        </ext:GridPanel>
                    </div>
                    <div style="clear: both; margin-top: 20px;">
                    </div>
                    <ext:Button ID="btnAddAchivement" runat="server" Text="Add New Row" Cls="btn btn-primary">
                        <Listeners>
                            <Click Handler="var newRow = new achivementItem(); var rowIndex = #{storeAchivements}.data.items.length; #{storeAchivements}.insert(rowIndex, newRow);" />
                        </Listeners>
                    </ext:Button>
                </div>
                <div class="components">
                    <div class="grids">
                        <p>
                            Would you like to add more details! <span>if you like, you can copy and paste from Word.</span></p>
                        <CKEditor:CKEditorControl ExtraPlugins="autogrow" ID="CKEditor1" Width="800px" Height="100px"
                            runat="server" BasePath="~/ckeditor/">
                        </CKEditor:CKEditorControl>
                    </div>
                </div>
                <div style="clear: both; margin-top: 30px;">
                </div>
                <div style="padding-bottom: 15px;">
                    <div style="display: block">
                        Select a file to upload</div>
                    <ext:FileUploadField ID="upload1" StyleSpec="cursor:pointer" runat="server" Text="Attach File"
                        ButtonOnly="true" Width="160px">
                        <Listeners>
                            <Change Fn="showFile" />
                        </Listeners>
                    </ext:FileUploadField>
                    <div id="divUpload">
                        <table>
                            <tr>
                                <td>
                                    <ext:LinkButton ID="btnAttachment" runat="server" Text="[Attachment]">
                                        <DirectEvents>
                                            <Click OnEvent="btnAttachment_Click" IsUpload="true" Success="Ext.net.DirectMethods.Download({ isUpload : true });">
                                                <EventMask ShowMask="false" />
                                            </Click>
                                        </DirectEvents>
                                    </ext:LinkButton>
                                </td>
                                <td>
                                    <ext:Button ID="btnRemoveFile" runat="server" Icon="Cross" ToolTip="Remove File"
                                        Width="25px">
                                        <Listeners>
                                            <Click Fn="resetFileUpload" />
                                        </Listeners>
                                    </ext:Button>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <ext:ComboBox ID="cmbShareWith" runat="server" Width="300px" FieldLabel="Share Your Activities With"
                    LabelAlign="Left" LabelSeparator="" EmptyText="Select One..." ForceSelection="true"
                    Cls="buttons" Hidden="true">
                    <Items>
                        <ext:ListItem Text="Person 1">
                        </ext:ListItem>
                        <ext:ListItem Text="Person 2">
                        </ext:ListItem>
                        <ext:ListItem Text="Person 3">
                        </ext:ListItem>
                    </Items>
                </ext:ComboBox>
            </div>
            <div class="buttonBlock buttonLeft">
                <%--<ext:Button ID="btnSaveAndFinishLater" runat="server" Text="Save and Finish Later"
                    Cls="btnFlat" BaseCls="btnFlat">
                    <Listeners>
                        <Click Handler="valGroup='AssignActivity'; return CheckValidation();">
                        </Click>
                    </Listeners>
                    <DirectEvents>
                        <Click OnEvent="btnSaveAndFinishLater_Click">
                            <EventMask ShowMask="true" />
                            <ExtraParams>
                                <ext:Parameter Name="LearningExtraParam" Value="Ext.encode(#{gridLearnings}.getRowsValues({selectedOnly: false}))"
                                    Mode="Raw" />
                                <ext:Parameter Name="AchievementExtraParam" Value="Ext.encode(#{gridAchivements}.getRowsValues({selectedOnly: false}))"
                                    Mode="Raw" />
                            </ExtraParams>
                        </Click>
                    </DirectEvents>
                </ext:Button>
                <div style="width: 20px; display: block;">
                    &nbsp;</div>--%>
                <ext:Button ID="btnSaveAndSend" runat="server" Text="Save and Send" Cls="btn btn-primary"
                    Width="160px">
                    <Listeners>
                        <Click Handler="valGroup='AssignActivity'; return CheckValidation();">
                        </Click>
                    </Listeners>
                    <DirectEvents>
                        <Click OnEvent="btnSaveAndSend_Click">
                            <EventMask ShowMask="true" />
                            <ExtraParams>
                                <ext:Parameter Name="LearningExtraParam" Value="Ext.encode(#{gridLearnings}.getRowsValues({selectedOnly: false}))"
                                    Mode="Raw" />
                                <ext:Parameter Name="AchievementExtraParam" Value="Ext.encode(#{gridAchivements}.getRowsValues({selectedOnly: false}))"
                                    Mode="Raw" />
                            </ExtraParams>
                        </Click>
                    </DirectEvents>
                </ext:Button>
                &nbsp;&nbsp;
                <div class="btnFlatOr">
                    or
                </div>
                <div style="margin-top: 6px;">
                <ext:LinkButton ID="btnCancel" runat="server" StyleSpec="" Text="Cancel"
                    Cls="btnFlatLeftGap" BaseCls="btnFlatLeftGap" Width="160px">
                    <Listeners>
                        <Click Handler="window.location = 'ActivityReport.aspx'" />
                    </Listeners>
                </ext:LinkButton></div>
            </div>
        </div>
    </div>
</asp:Content>
