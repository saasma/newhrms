﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ext.Net;
using BLL;
using DAL;
using BLL.Manager;
using System.IO;
using BLL.BO;

namespace Web.Activity
{
    public partial class DailyActivityReport : System.Web.UI.Page
    {

        //enum S
        //{ 
        //    READ = 1,
        //    PLEASEWAIT=2
        //};

        #region Variables

        //private  bool isSend;
        //private  bool isInsert;
        //private  int activityID;

        public bool IsInsert
        {
            set { }

            get
            {
                if (string.IsNullOrEmpty(hiddenID.Text)
                    || hiddenID.Text == "0")
                    return true;

                return false;
            }
        }

        #endregion

        #region Properties...
        private object[] DataLearnings
        {
            get
            {
                return new object[] {
                    new object[] {"This is item 1"},
                    new object[] {"This is item 2"},
                    new object[] {"This is item 3"},
                    new object[] {"This is item 4"},
                    new object[] {"This is item 5"}
                };
            }
        }

        private object[] DataAchivements
        {
            get
            {
                return new object[] {
                    new object[] {"This is item 1"},
                    new object[] {"This is item 2"},
                    new object[] {"This is item 3"}
                };
            }
        }
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {

                List<TextValue> listReview = LeaveRequestManager.GetApplyToListForEmployee(SessionManager.CurrentLoggedInEmployeeId, false, PreDefindFlowType.Leave);
                List<TextValue> listApproval = LeaveRequestManager.GetApplyToListForEmployee(SessionManager.CurrentLoggedInEmployeeId, true, PreDefindFlowType.Leave);
                listApproval.AddRange(listReview);
                cmbApplyTo.Store[0].DataSource = listApproval.OrderBy(x => x.Text).ToList();
                cmbApplyTo.Store[0].DataBind();


                if (!Request.QueryString.HasKeys())
                {
                    InitializeNewActivity();
                    //isInsert = true;
                }   
                else
                {
                    //activityID = Convert.ToInt32(Request.QueryString["id"]);
                    hiddenID.Text = Request.QueryString["id"];
                    dateActivity.Disable(true);
                    DAL.Activiti entity = ActivityManager.GetActivityFromID(int.Parse( hiddenID.Text ));

                    if (entity.Status != (int)DARStatusEnum.Draft)
                    {
                        upload1.Hide();
                        //btnSaveAndFinishLater.Hide();
                        btnSaveAndSend.Hide();
                        btnRemoveFile.Hide();
                    }

                    if (entity.EmployeeId != SessionManager.CurrentLoggedInEmployeeId)
                    {
                        Response.Write("Invalid Request.");
                        return;
                    }
                    //isInsert = false;
                    BindData(entity);
                    hiddenResetBool.Value = 0;
                }
            }
        }

        protected void btnSaveAndFinishLater_Click(object s, DirectEventArgs e)
        {
            //isSend = false;
            InsertUpdateData(false, e);
            //RefreshPage();
        }

        protected void btnSaveAndSend_Click(object s, DirectEventArgs e)
        {

            //isSend = true;
            InsertUpdateData(true, e);
        }

        protected void btnAttachment_Click(object s, DirectEventArgs e)
        {
            if (hiddenID.Text != "" && hiddenID.Text != "0")
            {
                string queryFile = ActivityManager.GetAttachmentURLFromID(int.Parse(hiddenID.Text));
                string originalName = ActivityManager.GetAttachmentFromID(int.Parse(hiddenID.Text));

                string filePath = Server.MapPath(@"~/Uploads/" + queryFile);

                FileInfo file = new FileInfo(filePath);
                if (file.Exists)
                {
                    //Response.Clear();
                    //Response.AddHeader("Content-Disposition", "attachment; filename=" + originalName);
                    //Response.AddHeader("Content-Length", file.Length.ToString());
                    //Response.ContentType = "application/octet-stream";
                    //Context.Response.WriteFile(file.FullName);
                    //Response.End();

                    //write file 
                    HttpContext.Current.Response.ClearHeaders();
                    HttpContext.Current.Response.ContentType = Utils.Helper.Util.GetMimeType(Path.GetExtension(filePath));
                    HttpContext.Current.Response.AddHeader("Content-Disposition", String.Format("attachment;filename=\"{0}\"", originalName));

                    HttpContext.Current.Response.WriteFile(file.FullName);

                    HttpContext.Current.Response.End();
                }
                else
                {
                    NewMessage.ShowWarningMessage("File has not been uploaded, please save the document first.");
                }
            }
            else
                NewMessage.ShowWarningMessage("File has not been uploaded, please save the document first.");
        }

        #region Methods...
        public static bool IsNullOrWhiteSpace(string value)
        {
            if (value != null)
            {
                for (int i = 0; i < value.Length; i++)
                {
                    if (!char.IsWhiteSpace(value[i]))
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        private void RefreshPage()
        {
            Response.Redirect(Request.RawUrl);
        }

        private void InitializeNewActivity()
        {
            DefineEmptyGrids();
            dateActivity.Text = DateTime.Now.ToShortDateString();

            X.Js.Call("hideUploadDiv");
        }

        private void DefineEmptyGrids()
        {
            this.storeLearnings.DataSource = GetEmptyLearningLines();
            this.storeLearnings.DataBind();

            this.storeAchivements.DataSource = GetEmptyAchievementLines();
            this.storeAchivements.DataBind();
        }

        private List<ActivityLearning> GetEmptyLearningLines()
        {
            List<ActivityLearning> emptyLearningLines = new List<ActivityLearning>();
            for (int i = 0; i < 1; i++)
            {
                emptyLearningLines.Add(new ActivityLearning { });
            }

            return emptyLearningLines;
        }

        private List<ActivityAchvement> GetEmptyAchievementLines()
        {
            List<ActivityAchvement> emptyAchievementLines = new List<ActivityAchvement>();
            for (int i = 0; i < 1; i++)
            {
                emptyAchievementLines.Add(new ActivityAchvement { });
            }

            return emptyAchievementLines;
        }

        private void BindData(DAL.Activiti entity)
        {
            int activityID = entity.ActivityID;

            dateActivity.SelectedDate = Convert.ToDateTime(entity.ActivityDateEng);

            if (entity.ApplyToVerifyEmployeeId != null)
                ExtControlHelper.ComboBoxSetSelected(entity.ApplyToVerifyEmployeeId.ToString(), cmbApplyTo);

            this.storeLearnings.DataSource = ActivityManager.GetLearningsFromID(activityID);
            this.storeLearnings.DataBind();

            this.storeAchivements.DataSource = ActivityManager.GetAchievementsFromID(activityID);
            this.storeAchivements.DataBind();

            //this.htmlEditor1.Text = entity.Details;
            this.CKEditor1.Text = entity.Details;

            string attachedFileName = entity.AttachmentName;

            if (!string.IsNullOrEmpty(attachedFileName))
            {
                btnAttachment.Text = attachedFileName;
            }
            else
            {
                X.Js.Call("hideUploadDiv");
            }
        }

        private void InsertUpdateData(bool isSend, DirectEventArgs e)
        {

            if (dateActivity.IsEmpty)
            {
                NewMessage.ShowWarningMessage("Date is required.");
                dateActivity.Focus();
                return;
            }
            else if (this.IsInsert)
            { 
                // check if the empId and date matches for a particular activity
                int empId = SessionManager.CurrentLoggedInEmployeeId;
                DateTime actDate = dateActivity.SelectedDate;

                bool isExist = ActivityManager.CheckIfAlreadyExists(empId, actDate);

                if (isExist)
                {
                    NewMessage.ShowWarningMessage("The activity for this date already exists.");
                    dateActivity.Focus();
                    return;
                }
            }

            // validate if exists 1. key learning or 2. key achievement or 3. Details or 4. file upload
            


            DAL.Activiti entity = new DAL.Activiti();

            entity.ActivityDateEng = Convert.ToDateTime(dateActivity.Text);
           //entity.Details = htmlEditor1.Text;
            entity.Details = CKEditor1.Text;
            entity.EmployeeId = SessionManager.CurrentLoggedInEmployeeId;
            entity.SubmissionDate = DateTime.Now;
            entity.ApplyToVerifyEmployeeId = int.Parse(cmbApplyTo.SelectedItem.Value);


            if (!this.IsInsert)
            {
                entity.ActivityID = int.Parse(hiddenID.Text);

                DAL.Activiti dbActivity = ActivityManager.GetActivityFromID(entity.ActivityID);

                if (upload1.HasFile)
                {
                    entity.AttachmentName = upload1.PostedFile.FileName;

                    // file upload section
                    string userFileName = entity.AttachmentName;
                    string serverFileName = Guid.NewGuid().ToString() + Path.GetExtension(userFileName);
                    string absolutePath = Server.MapPath(@"~/Uploads/" + serverFileName);
                    upload1.PostedFile.SaveAs(absolutePath);
                    entity.AttachmentUrl = serverFileName;
                }
                else
                {
                    if (int.Parse(hiddenResetBool.Text.Trim())==0)
                    {
                        // if user hasn't cleared the upload field, but the button shows file.
                        entity.AttachmentName = dbActivity.AttachmentName;
                        entity.AttachmentUrl = dbActivity.AttachmentUrl;
                    }
                    else
                    {
                        // if user has cleared the upload field
                        entity.AttachmentName = null;
                        entity.AttachmentUrl = null;
                    }
                }
            }

            if (isSend)
            {
                entity.Status = Convert.ToInt32(DARStatusEnum.Pending);
            }
            else
            {
                entity.Status = Convert.ToInt32(DARStatusEnum.Draft);
            }

            // saving the uploaded file
            if (upload1.HasFile && this.IsInsert)
            {
                entity.AttachmentName = upload1.PostedFile.FileName;

                // file upload section
                string userFileName = entity.AttachmentName;
                string serverFileName = Guid.NewGuid().ToString() + Path.GetExtension(userFileName);
                string absolutePath = Server.MapPath(@"~/Uploads/" + serverFileName);
                upload1.PostedFile.SaveAs(absolutePath);
                entity.AttachmentUrl = serverFileName;
            }

            List<ActivityLearning> learningLines = new List<ActivityLearning>();
            List<ActivityAchvement> achievementLines = new List<ActivityAchvement>();

            string jsonLearning = e.ExtraParams["LearningExtraParam"];
            string jsonAchievement = e.ExtraParams["AchievementExtraParam"];

            if (string.IsNullOrEmpty(jsonLearning) && string.IsNullOrEmpty(jsonAchievement))
                return;

            learningLines = JSON.Deserialize<List<ActivityLearning>>(jsonLearning);
            achievementLines = JSON.Deserialize<List<ActivityAchvement>>(jsonAchievement);

            // making lists and adding line items to the list to avoid InvalidOperationException
            List<ActivityLearning> lstLearningsToRemove = new List<ActivityLearning>();
            List<ActivityAchvement> lstAchievementsToRemove = new List<ActivityAchvement>();

            // adding the empty row references to the list
            foreach (var line in learningLines)
            {
                // if the line is empty add it to the list to remove
                if (IsNullOrWhiteSpace(line.Text))
                {
                    lstLearningsToRemove.Add(line);
                }
            }

            foreach (var line in achievementLines)
            {
                // if the line is empty, add it to the list to remove
                if (IsNullOrWhiteSpace(line.Text))
                {
                    lstAchievementsToRemove.Add(line);
                }
            }

            // Now remove the empty lines
            foreach (var line in lstLearningsToRemove)
            {
                learningLines.Remove(line);
            }

            foreach (var line in lstAchievementsToRemove)
            {
                achievementLines.Remove(line);
            }

            if (this.IsInsert)
            {
                if (learningLines.Count <= 0 && achievementLines.Count <= 0
                    && (string.IsNullOrEmpty(entity.Details.Trim()) || entity.Details.Trim().Equals("<p>&nbsp;&nbsp;</p>\r\n")) && upload1.HasFile == false)
                {
                    NewMessage.ShowWarningMessage("Some value is required in Key learnings or Key achievements or More details or file has to be uploaded.");
                    dateActivity.Focus();
                    return;
                }

            }

            Status status = ActivityManager.InsertUpdateActivity(entity, learningLines, achievementLines, this.IsInsert);

            if (status.IsSuccess && isSend)
            {

                hiddenID.Text = entity.ActivityID.ToString();
                NewMessage.ShowNormalMessage("Activity sent for verification.");
                upload1.Hide();
                //btnSaveAndFinishLater.Hide();
                btnSaveAndSend.Hide();
                btnRemoveFile.Hide();
            }
            else if (status.IsSuccess && !isSend)
            {
                NewMessage.ShowNormalMessage("Activity saved as a draft successfully.");
                //isInsert = false;
                hiddenID.Text = ActivityManager.GetActivityIdFromDate(dateActivity.SelectedDate).ToString();
                hiddenResetBool.Value = 0;
                // = entity.ActivityID.ToString();
            }
            else
            {
                NewMessage.ShowWarningMessage("Error occurred while saving activity.\nPlease try again.");
            }
        }
        #endregion

    }
}   