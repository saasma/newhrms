﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using Utils.Helper;
using Utils.Web;
using BLL.Base;
using DAL;
using Utils.Calendar;
using BLL.BO;

namespace Web.Employee
{
    public partial class OvertimeRequester : BasePage
    {


        //private int _tempCurrentPage;
        //private int _tempCount;
        private PayManager payMgr = new PayManager();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Initialise();
            }
            JavascriptHelper.AttachPopUpCode(Page, "positionHistoryPopup", "OvertimeRequesterPopup.aspx", 475, 500);
        }

     

        void Initialise()
        {
            List<KeyValue> statues = new JobStatus().GetMembers();

            ddlOvertimeType.DataSource = OvertimeManager.GetOvertimeList();
            ddlOvertimeType.DataBind();

            if (OvertimeManager.IsRequestAutoGroupType)
            {
                gvw.Columns[4].Visible = false;
                gvw.Columns[5].Visible = false;
            }

            LoadEmployees();
        }
        protected void btnLoad_Click(object sender, EventArgs e)
        {
        
            LoadEmployees();
        }
        protected void gvwEmployees_RowCreated(object sender, GridViewRowEventArgs e)
        {

            //if (e.Row.RowType == DataControlRowType.DataRow)
            //{
            //    e.Row.Attributes.Add("onmouseover",
            //    "this.originalClass=this.className;this.className='selected'");

            //    e.Row.Attributes.Add("onmouseout",
            //    "this.className=this.originalClass;");


            //}

        }
        public void LoadEmployees()
        {
            int type=1;


            if (ddlType.SelectedItem != null)
                type = int.Parse(ddlType.SelectedValue);
                

            //gvw.DataSource = OvertimeManager.GetLeaveRequestsByEmpID(SessionManager.CurrentLoggedInEmployeeId, type);
            //gvw.DataBind();
            ///////////////////////////////////////
            int totalRecords = 0;

            List<GetOvertimeRequestByIDResult> list = OvertimeManager.GetLeaveRequestsByEmpID(SessionManager.CurrentLoggedInEmployeeId, type, pagintCtl.CurrentPage, int.Parse(pagintCtl.DDLRecords.SelectedValue), int.Parse(ddlStatus.SelectedValue), int.Parse(ddlOvertimeType.SelectedValue), ref totalRecords);
            //List<GetOvertimeRequestForManagerResult> list = OvertimeManager.GetLeaveRequestsForManager(type, pagintCtl.CurrentPage, int.Parse(pagintCtl.DDLRecords.SelectedValue), ref totalRecords);
            
            gvw.DataSource = list;
            gvw.DataBind();



            pagintCtl.UpdatePagingBar(totalRecords);

        }

        public void LoadGridRemotely()
        {
            int totalRecords = 0;

            gvw.DataSource = OvertimeManager.GetLeaveRequestsByEmpID(SessionManager.CurrentLoggedInEmployeeId, 1, 0, 9999, int.Parse(ddlStatus.SelectedValue), int.Parse(ddlOvertimeType.SelectedValue), ref totalRecords);
            gvw.DataBind();
            pagintCtl.UpdatePagingBar(totalRecords);

        }


        public string GetEngOrNepDate(object value)
        {
            if (value == null)
                return "";

            DateTime date = Convert.ToDateTime(value);

            CustomDate c = new CustomDate(date.Day, date.Month, date.Year, true);

            if (IsEnglish)
                return c.ToString();

            else
                return CustomDate.ConvertEngToNep(c).ToString();
        }

       
        protected void btnExport_Click(object sender, EventArgs e)
        {
            GridViewExportUtil.Export("OvertimeList.xls", gvw);
        }

        //protected void btnPostToSalary_Click(object sender, EventArgs e)
        //{

        //    foreach (GridViewRow row in gvw.Rows)
        //    {


        //        int payrollPeriodId = (int)gvw.DataKeys[row.RowIndex]["PayrollPeriodId"];

        //        PayManager.PostOvertimePay(payrollPeriodId);
        //        msgCtl.InnerHtml = Resources.Messages.OvertimePostedMessage;
        //        msgCtl.Hide = false;
        //        break;
        //    }


        //}
        protected void txtEmployeeName_TextChanged(object sender, EventArgs e)
        {
            

        }



        protected void gvwRoles_PageIndexChanged(object sender, EventArgs e)
        {

        }

        protected void gvwRoles_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {

            LoadEmployees();
            //Clear();
        }

        protected void btnNext_Click()
        {
            pagintCtl.CurrentPage += 1;
            LoadEmployees();
        }



        protected void btnPrevious_Click()
        {
            pagintCtl.CurrentPage -= 1;
            LoadEmployees();
        }

        protected void ddlRecords_SelectedIndexChanged()
        {
            pagintCtl.CurrentPage = 1;
            LoadEmployees();
        }

        public bool IsInSaveStatus(int status)
        {
            if (status == (int)OvertimeStatusEnum.Pending)
                return true;
            else
                return false;
        }

        protected void gvw_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            int overtimeRequestId = int.Parse(gvw.DataKeys[e.RowIndex]["RequestID"].ToString());

            Status status = OvertimeManager.DeleteOvertimeRequest(overtimeRequestId);
            if (status.IsSuccess)
            {
                JavascriptHelper.DisplayClientMsg("Record deleted successfully.", this.Page);
                LoadEmployees();
            }
            else 
            {
                JavascriptHelper.DisplayClientMsg(status.ErrorMessage, this.Page);
            }

        }

      
       

    }
}
