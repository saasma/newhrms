<%@ Page Title="Attendance Details" Language="C#" MaintainScrollPositionOnPostback="true"
    MasterPageFile="~/Master/EmployeeExt.Master" AutoEventWireup="true" CodeBehind="AttendanceDetails.aspx.cs"
    Inherits="Web.CP.Report.AttendanceDetails" %>

<%@ Register Src="~/Attendance/UserControl/EmployeeAtteCtl.ascx" TagName="ReportContainer"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" ScriptMode="Release">
        <Services>
            <asp:ServiceReference Path="~/PayrollService.asmx" />
        </Services>
    </asp:ScriptManager>
    <div style="margin-top: 10px">
        <h4>
            Attendance Details Report
        </h4>
    </div>
    <uc1:ReportContainer runat="server" PageType="Self" OnReloadReport="LoadReport" Id="report" />
</asp:Content>
