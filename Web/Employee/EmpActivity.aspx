﻿<%@ Page Title="Employee Acitivty" Language="C#" MasterPageFile="~/Master/EmployeeExt.Master" AutoEventWireup="true" CodeBehind="EmpActivity.aspx.cs" Inherits="Web.Employee.EmpActivity" %>

<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">

<style type="text/css">


.x-panel-header-text-default {
    color:White;
   
}

</style>

<script type="text/javascript">

    
      var CommandHandler = function(command, record){
            <%= hdnActivityId.ClientID %>.setValue(record.data.ActivityId);
            <%= hdnAcitivityType.ClientID %>.setValue(record.data.ActivityType);
                if(command=="Edit")
                {
                    <%= btnEdit.ClientID %>.fireEvent('click');
                }
                else
                {
                    <%= btnDelete.ClientID %>.fireEvent('click');
                }

           }


           var prepareActivity = function (grid, toolbar, rowIndex, record) {
                var editButton = toolbar.items.get(1);
                var deleteButton = toolbar.items.get(2);
                if (record.data.IsEditable == 0) {
                    editButton.setVisible(false);
                    deleteButton.setVisible(false);
                }

            } 

        var prepareDownloadActivity = function(grid, toolbar, rowIndex, record){
            var downloadBtn = toolbar.items.get(1);
            if(record.data.ContainsFile == 0){  
                downloadBtn.setVisible(false);     
            }
        }
   
        var CommandDownload = function(command, record){
            <%= hdnActivityId.ClientID %>.setValue(record.data.ActivityId);
            <%= hdnAcitivityType.ClientID %>.setValue(record.data.ActivityType);
                
            <%= btnDownload.ClientID %>.fireEvent('click');
           }

</script>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="mainContent" runat="server">

<ext:Hidden runat="server" ID="hdnActivityId" />
<ext:Hidden runat="server" ID="hdnAcitivityType" />
<ext:LinkButton runat="server" Hidden="true" ID="btnEdit">
    <DirectEvents>
        <Click OnEvent="btnEdit_Click">
            <EventMask ShowMask="true" />
        </Click>
    </DirectEvents>
</ext:LinkButton>
<ext:Button ID="btnDelete" runat="server" Hidden="true">
    <DirectEvents>
        <Click OnEvent="btnDelete_Click">
            <Confirmation ConfirmRequest="true" Message="Are you sure, you want to delete the record?" />
            <EventMask ShowMask="true" />
        </Click>
    </DirectEvents>
</ext:Button>
<ext:LinkButton ID="btnDownload" runat="server" Hidden="true">
    <DirectEvents>
        <Click OnEvent="btnDownload_Click">
        </Click>
    </DirectEvents>
</ext:LinkButton>

<div class="innerLR1">
        

        <h3 style="margin-top:10px">
            My Activities
        </h3>
        <br />

        <table>
            <tr>
                <td style="width:230px;">
                    <pr:CalendarExtControl Width="180" FieldLabel="Date Filter" ID="txtDateFilter" runat="server"
                        LabelAlign="Top" LabelSeparator="">                     
                    </pr:CalendarExtControl>
                </td>
                <td style="padding-top:13px;">
                    <ext:Button ID="btnLoad" runat="server" Cls="btn btn-save" Text="Load" Width="120">
                        <DirectEvents>
                            <Click OnEvent="btnLoad_Click">                               
                                <EventMask ShowMask="true" />                              
                            </Click>
                        </DirectEvents>
                    </ext:Button>                       
                </td>
            </tr>
            
        </table>


        <br />

        <ext:GridPanel StyleSpec="margin-top:15px;" ID="gridClientVisit" runat="server" Cls="itemgrid" Scroll="None" Title="Client Visit" >
            <Store>
                <ext:Store ID="Store1" runat="server">
                    <Model>
                        <ext:Model ID="Model4" runat="server" IDProperty="ActivityId">
                            <Fields>
                                <ext:ModelField Name="ActivityId" Type="String" />
                                <ext:ModelField Name="ActivityType" Type="String" />
                                <ext:ModelField Name="ClientName" Type="string" />
                                <ext:ModelField Name="DateEng" Type="Date" />
                                <ext:ModelField Name="StartTime" Type="string" />
                                <ext:ModelField Name="EndTime" Type="string" />
                                <ext:ModelField Name="DurationTime" Type="string" />
                                <ext:ModelField Name="Representative" Type="string" />
                                <ext:ModelField Name="Issue" Type="string" />
                                <ext:ModelField Name="Result" Type="string" />
                                <ext:ModelField Name="NextPlannedVisitEng" Type="Date" />
                                <ext:ModelField Name="IsEditable" Type="Int" />
                            </Fields>
                        </ext:Model>
                    </Model>
                </ext:Store>
            </Store>
            <ColumnModel>
                <Columns>
                    <ext:Column ID="colClientName" Sortable="false" MenuDisabled="true" runat="server" Text="Client Name"
                        Align="Left" Width="150" DataIndex="ClientName" />
                    <ext:DateColumn ID="colDate" runat="server" Align="Right" Text="Date" Width="100"
                        MenuDisabled="true" Sortable="false" Format="yyyy-MM-dd" DataIndex="DateEng">
                    </ext:DateColumn>
                    <ext:Column ID="colStart" Sortable="false" MenuDisabled="true" runat="server" Text="Start" Width="80"
                        Align="Right" DataIndex="StartTime">
                    </ext:Column>
                    <ext:Column ID="colEndTime" Sortable="false" MenuDisabled="true" runat="server" Text="End" Width="80"
                        Align="Right" DataIndex="EndTime">
                    </ext:Column>
                    <ext:Column ID="Column10" Sortable="false" MenuDisabled="true" runat="server" Text="Duration"
                        Align="Center" Width="70" DataIndex="DurationTime">
                    </ext:Column>
                    <ext:Column ID="Column7" Sortable="false" MenuDisabled="true" runat="server" Text="Representative"
                        Align="Left" Width="150" DataIndex="Representative">
                    </ext:Column>
                     <ext:Column ID="Column1" Sortable="false" MenuDisabled="true" runat="server" Text="Issue"
                        Align="Left" Width="185" DataIndex="Issue">
                    </ext:Column>
                    <ext:Column ID="Column2" Sortable="false" MenuDisabled="true" runat="server" Text="Result"
                        Align="Left" Width="185" DataIndex="Result">
                    </ext:Column>
                    <ext:DateColumn ID="DateColumn1" runat="server" Align="Right" Text="Next Planned Visit" Width="120"
                        MenuDisabled="true" Sortable="false" Format="yyyy-MM-dd" DataIndex="NextPlannedVisitEng">
                    </ext:DateColumn>
                    <ext:CommandColumn ID="CommandColumn1" runat="server" Width="75" Text="Actions" Align="Center">
                        <Commands>
                            <ext:CommandSeparator />
                            <ext:GridCommand Cls="editGridButton" ToolTip-Text="Edit"  Text="<i class='fa fa-pencil'></i>" CommandName="Edit" />
                            <ext:GridCommand Cls="editGridButton" ToolTip-Text="Delete"  Text="<i class='fa fa-trash-o'></i>"
                                CommandName="Delete" />   
                        </Commands>
                        <Listeners>
                            <Command Handler="CommandHandler(command,record);" />
                        </Listeners>
                        <PrepareToolbar Fn="prepareActivity" />
                        
                    </ext:CommandColumn>
                </Columns>
            </ColumnModel>
            <SelectionModel>
                <ext:RowSelectionModel ID="RowSelectionModel2" runat="server" Mode="Single" />
            </SelectionModel>
        </ext:GridPanel>

        <br />

        <ext:Button runat="server" Cls="btn btn-primary btn-sect" Width="150" StyleSpec="margin-top:10px"
                Height="30" ID="btnAddClientVisit" Text="<i></i>Add New Line">
            <DirectEvents>
                <Click OnEvent="btnAddClientVisit_Click">
                    <EventMask ShowMask="true" />
                </Click>
            </DirectEvents>
        </ext:Button>
        
        <br />

            <ext:GridPanel StyleSpec="margin-top:15px;" ID="gridSoftwareTesting" runat="server" Cls="itemgrid" Scroll="None" Title="Software Testing">
            <Store>
                <ext:Store ID="Store2" runat="server">
                    <Model>
                        <ext:Model ID="Model1" runat="server" IDProperty="ActivityId">
                            <Fields>
                                <ext:ModelField Name="ActivityId" Type="String" />
                                <ext:ModelField Name="ActivityType" Type="String" />
                                <ext:ModelField Name="SoftwareName" Type="string" />
                                <ext:ModelField Name="DateEng" Type="Date" />
                                <ext:ModelField Name="StartTime" Type="string" />
                                <ext:ModelField Name="EndTime" Type="string" />
                                <ext:ModelField Name="DurationTime" Type="string" />
                                <ext:ModelField Name="Description" Type="string" />
                                <ext:ModelField Name="Result" Type="string" />
                                <ext:ModelField Name="Issue" Type="string" />                                
                                <ext:ModelField Name="IsEditable" Type="Int" />
                                <ext:ModelField Name="ContainsFile" Type="Int" />
                            </Fields>
                        </ext:Model>
                    </Model>
                </ext:Store>
            </Store>
            <ColumnModel>
                <Columns>
                    <ext:Column ID="Column3" Sortable="false" MenuDisabled="true" runat="server" Text="Software Name"
                        Align="Left" Width="150" DataIndex="SoftwareName" />
                    <ext:DateColumn ID="DateColumn2" runat="server" Align="Right" Text="Date" Width="100"
                        MenuDisabled="true" Sortable="false" Format="yyyy-MM-dd" DataIndex="DateEng">
                    </ext:DateColumn>
                    <ext:Column ID="Column4" Sortable="false" MenuDisabled="true" runat="server" Text="Start" Width="80"
                        Align="Right" DataIndex="StartTime">
                    </ext:Column>
                    <ext:Column ID="Column5" Sortable="false" MenuDisabled="true" runat="server" Text="End" Width="80"
                        Align="Right" DataIndex="EndTime">
                    </ext:Column>
                    <ext:Column ID="Column11" Sortable="false" MenuDisabled="true" runat="server" Text="Duration"
                        Align="Center" Width="70" DataIndex="DurationTime">
                    </ext:Column>
                    <ext:Column ID="Column6" Sortable="false" MenuDisabled="true" runat="server" Text="Module Tested"
                        Align="Left" Width="200" DataIndex="Description">
                    </ext:Column>
                     <ext:Column ID="Column8" Sortable="false" MenuDisabled="true" runat="server" Text="Result"
                        Align="Left" Width="200" DataIndex="Result">
                    </ext:Column>
                    <ext:Column ID="Column9" Sortable="false" MenuDisabled="true" runat="server" Text="Issue"
                        Align="Left" Width="200" DataIndex="Issue">
                    </ext:Column>
                    
                    <ext:CommandColumn ID="CCSTDownload" runat="server" Width="40" Sortable="false"
                            MenuDisabled="true" Align="Center">
                            <Commands>
                                <ext:CommandSeparator />
                                <ext:GridCommand Icon="PageWhitePut" CommandName="Download" ToolTip-Text="Download attachment" />
                            </Commands>
                            <PrepareToolbar Fn="prepareDownloadActivity" />
                            <Listeners>
                                <Command Handler="CommandDownload(command,record);" />
                            </Listeners>
                        </ext:CommandColumn>
                   
                    <ext:CommandColumn ID="CCST" runat="server" Width="75" Text="Actions" Align="Center">
                        <Commands>
                            <ext:CommandSeparator />
                            <ext:GridCommand Cls="editGridButton" ToolTip-Text="Edit"  Text="<i class='fa fa-pencil'></i>" CommandName="Edit" />
                            <ext:GridCommand Cls="editGridButton" ToolTip-Text="Delete"  Text="<i class='fa fa-trash-o'></i>"
                                CommandName="Delete" />   
                        </Commands>
                        <Listeners>
                            <Command Handler="CommandHandler(command,record);" />
                        </Listeners>
                         <PrepareToolbar Fn="prepareActivity" />
                    </ext:CommandColumn>
                </Columns>
            </ColumnModel>
            <SelectionModel>
                <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" Mode="Single" />
            </SelectionModel>
        </ext:GridPanel>

        <br />

        <ext:Button runat="server" Cls="btn btn-primary btn-sect" Width="150" StyleSpec="margin-top:10px"
                Height="30" ID="btnSoftwareTesting" Text="<i></i>Add New Line">
            <DirectEvents>
                <Click OnEvent="btnSoftwareTesting_Click">
                    <EventMask ShowMask="true" />
                </Click>
            </DirectEvents>
        </ext:Button>
        
        <br />

           <ext:GridPanel StyleSpec="margin-top:15px;" ID="gridDocumentation" runat="server" Cls="itemgrid" Scroll="None" Title="Documentation">
            <Store>
                <ext:Store ID="Store4" runat="server">
                    <Model>
                        <ext:Model ID="Model2" runat="server" IDProperty="ActivityId">
                            <Fields>
                                <ext:ModelField Name="ActivityId" Type="String" />
                                <ext:ModelField Name="ActivityType" Type="String" />
                                <ext:ModelField Name="SoftwareName" Type="string" />
                                <ext:ModelField Name="DateEng" Type="Date" />
                                <ext:ModelField Name="StartTime" Type="string" />
                                <ext:ModelField Name="EndTime" Type="string" />
                                <ext:ModelField Name="DurationTime" Type="string" />
                                <ext:ModelField Name="Issue" Type="string" />     
                                <ext:ModelField Name="Description" Type="string" />
                                <ext:ModelField Name="IsEditable" Type="Int" />
                            </Fields>
                        </ext:Model>
                    </Model>
                </ext:Store>
            </Store>
            <ColumnModel>
                <Columns>
                    <ext:Column ID="Column12" Sortable="false" MenuDisabled="true" runat="server" Text="Software Name"
                        Align="Left" Width="150" DataIndex="SoftwareName" />
                    <ext:DateColumn ID="DateColumn3" runat="server" Align="Right" Text="Date" Width="100"
                        MenuDisabled="true" Sortable="false" Format="yyyy-MM-dd" DataIndex="DateEng">
                    </ext:DateColumn>
                    <ext:Column ID="Column13" Sortable="false" MenuDisabled="true" runat="server" Text="Start" Width="80"
                        Align="Right" DataIndex="StartTime">
                    </ext:Column>
                    <ext:Column ID="Column14" Sortable="false" MenuDisabled="true" runat="server" Text="End" Width="80"
                        Align="Right" DataIndex="EndTime">
                    </ext:Column>
                    <ext:Column ID="Column15" Sortable="false" MenuDisabled="true" runat="server" Text="Duration"
                        Align="Center" Width="70" DataIndex="DurationTime">
                    </ext:Column>
                    <ext:Column ID="Column16" Sortable="false" MenuDisabled="true" runat="server" Text="Module Documented"
                        Align="Left" Width="200" DataIndex="Issue">
                    </ext:Column>
                     <ext:Column ID="Column17" Sortable="false" MenuDisabled="true" runat="server" Text="Description"
                        Align="Left" Width="300" DataIndex="Description">
                    </ext:Column>                    
                   
                    <ext:CommandColumn ID="CCDocumentation" runat="server" Width="75" Text="Actions" Align="Center">
                        <Commands>
                            <ext:CommandSeparator />
                            <ext:GridCommand Cls="editGridButton" ToolTip-Text="Edit"  Text="<i class='fa fa-pencil'></i>" CommandName="Edit" />
                            <ext:GridCommand Cls="editGridButton" ToolTip-Text="Delete"  Text="<i class='fa fa-trash-o'></i>"
                                CommandName="Delete" />   
                        </Commands>
                        <Listeners>
                            <Command Handler="CommandHandler(command,record);" />
                        </Listeners>
                        <PrepareToolbar Fn="prepareActivity" />
                    </ext:CommandColumn>
                </Columns>
            </ColumnModel>
            <SelectionModel>
                <ext:RowSelectionModel ID="RowSelectionModel3" runat="server" Mode="Single" />
            </SelectionModel>
        </ext:GridPanel>

        <br />

        <ext:Button runat="server" Cls="btn btn-primary btn-sect" Width="150" StyleSpec="margin-top:10px"
                Height="30" ID="btnAddDocumentation" Text="<i></i>Add New Line">
            <DirectEvents>
                <Click OnEvent="btnAddDocumentation_Click">
                    <EventMask ShowMask="true" />
                </Click>
            </DirectEvents>
        </ext:Button>
        
        <br />

          <ext:GridPanel StyleSpec="margin-top:15px;" ID="gridRemoteSupport" runat="server" Cls="itemgrid" Scroll="None" Title="Remote Support">
            <Store>
                <ext:Store ID="Store5" runat="server">
                    <Model>
                        <ext:Model ID="Model5" runat="server" IDProperty="ActivityId">
                            <Fields>
                                <ext:ModelField Name="ActivityId" Type="String" />
                                <ext:ModelField Name="ActivityType" Type="String" />
                                <ext:ModelField Name="ClientName" Type="string" />
                                <ext:ModelField Name="DateEng" Type="Date" />
                                <ext:ModelField Name="StartTime" Type="string" />
                                <ext:ModelField Name="EndTime" Type="string" />
                                <ext:ModelField Name="DurationTime" Type="string" />
                                <ext:ModelField Name="Description" Type="string" />
                                <ext:ModelField Name="Issue" Type="string" />     
                                <ext:ModelField Name="Result" Type="string" />   
                                <ext:ModelField Name="NextPlannedVisitEng" Type="Date" />
                                <ext:ModelField Name="IsEditable" Type="Int" />
                            </Fields>
                        </ext:Model>
                    </Model>
                </ext:Store>
            </Store>
            <ColumnModel>
                <Columns>
                    <ext:Column ID="Column18" Sortable="false" MenuDisabled="true" runat="server" Text="Client Name"
                        Align="Left" Width="150" DataIndex="ClientName" />
                    <ext:DateColumn ID="DateColumn4" runat="server" Align="Right" Text="Date" Width="100"
                        MenuDisabled="true" Sortable="false" Format="yyyy-MM-dd" DataIndex="DateEng">
                    </ext:DateColumn>
                    <ext:Column ID="Column19" Sortable="false" MenuDisabled="true" runat="server" Text="Start" Width="80"
                        Align="Right" DataIndex="StartTime">
                    </ext:Column>
                    <ext:Column ID="Column20" Sortable="false" MenuDisabled="true" runat="server" Text="End" Width="80"
                        Align="Right" DataIndex="EndTime">
                    </ext:Column>
                    <ext:Column ID="Column21" Sortable="false" MenuDisabled="true" runat="server" Text="Duration"
                        Align="Center" Width="70" DataIndex="DurationTime">
                    </ext:Column>
                    <ext:Column ID="Column22" Sortable="false" MenuDisabled="true" runat="server" Text="Support Mode"
                        Align="Left" Width="200" DataIndex="Description">
                    </ext:Column>
                     <ext:Column ID="Column23" Sortable="false" MenuDisabled="true" runat="server" Text="Issue"
                        Align="Left" Width="150" DataIndex="Issue">
                    </ext:Column>   
                     <ext:Column ID="Column24" Sortable="false" MenuDisabled="true" runat="server" Text="Result"
                        Align="Left" Width="150" DataIndex="Result">
                    </ext:Column>        
                    <ext:DateColumn ID="DateColumn5" runat="server" Align="Right" Text="Next Planned Visit" Width="120"
                        MenuDisabled="true" Sortable="false" Format="yyyy-MM-dd" DataIndex="NextPlannedVisitEng">
                    </ext:DateColumn>            
                   
                    <ext:CommandColumn ID="CommandColumn2" runat="server" Width="75" Text="Actions" Align="Center">
                        <Commands>
                            <ext:CommandSeparator />
                            <ext:GridCommand Cls="editGridButton" ToolTip-Text="Edit"  Text="<i class='fa fa-pencil'></i>" CommandName="Edit" />
                            <ext:GridCommand Cls="editGridButton" ToolTip-Text="Delete"  Text="<i class='fa fa-trash-o'></i>"
                                CommandName="Delete" />   
                        </Commands>
                        <Listeners>
                            <Command Handler="CommandHandler(command,record);" />
                        </Listeners>
                        <PrepareToolbar Fn="prepareActivity" />
                    </ext:CommandColumn>
                </Columns>
            </ColumnModel>
            <SelectionModel>
                <ext:RowSelectionModel ID="RowSelectionModel4" runat="server" Mode="Single" />
            </SelectionModel>
        </ext:GridPanel>

        <br />

        <ext:Button runat="server" Cls="btn btn-primary btn-sect" Width="150" StyleSpec="margin-top:10px"
                Height="30" ID="btnNewRemoteSupport" Text="<i></i>Add New Line">
            <DirectEvents>
                <Click OnEvent="btnNewRemoteSupport_Click">
                    <EventMask ShowMask="true" />
                </Click>
            </DirectEvents>
        </ext:Button>
        
        <br />
 
         <ext:GridPanel StyleSpec="margin-top:15px;" ID="gridOther" runat="server" Cls="itemgrid" Scroll="None" Title="Other">
            <Store>
                <ext:Store ID="Store6" runat="server">
                    <Model>
                        <ext:Model ID="Model6" runat="server" IDProperty="ActivityId">
                            <Fields>
                                <ext:ModelField Name="ActivityId" Type="String" />
                                <ext:ModelField Name="ActivityType" Type="String" />
                                <ext:ModelField Name="Name" Type="string" />
                                <ext:ModelField Name="DateEng" Type="Date" />
                                <ext:ModelField Name="StartTime" Type="string" />
                                <ext:ModelField Name="EndTime" Type="string" />
                                <ext:ModelField Name="DurationTime" Type="string" />
                                <ext:ModelField Name="Description" Type="string" />                               
                                <ext:ModelField Name="IsEditable" Type="Int" />
                            </Fields>
                        </ext:Model>
                    </Model>
                </ext:Store>
            </Store>
            <ColumnModel>
                <Columns>
                    <ext:Column ID="Column25" Sortable="false" MenuDisabled="true" runat="server" Text="Work Type"
                        Align="Left" Width="150" DataIndex="Name" />
                    <ext:DateColumn ID="DateColumn6" runat="server" Align="Right" Text="Date" Width="100"
                        MenuDisabled="true" Sortable="false" Format="yyyy-MM-dd" DataIndex="DateEng">
                    </ext:DateColumn>
                    <ext:Column ID="Column26" Sortable="false" MenuDisabled="true" runat="server" Text="Start" Width="80"
                        Align="Right" DataIndex="StartTime">
                    </ext:Column>
                    <ext:Column ID="Column27" Sortable="false" MenuDisabled="true" runat="server" Text="End" Width="80"
                        Align="Right" DataIndex="EndTime">
                    </ext:Column>
                    <ext:Column ID="Column28" Sortable="false" MenuDisabled="true" runat="server" Text="Duration"
                        Align="Center" Width="70" DataIndex="DurationTime">
                    </ext:Column>
                    <ext:Column ID="Column29" Sortable="false" MenuDisabled="true" runat="server" Text="Description"
                        Align="Left" Width="200" DataIndex="Description">
                    </ext:Column>
                   
                    <ext:CommandColumn ID="CommandColumn3" runat="server" Width="75" Text="Actions" Align="Center">
                        <Commands>
                            <ext:CommandSeparator />
                            <ext:GridCommand Cls="editGridButton" ToolTip-Text="Edit"  Text="<i class='fa fa-pencil'></i>" CommandName="Edit" />
                            <ext:GridCommand Cls="editGridButton" ToolTip-Text="Delete"  Text="<i class='fa fa-trash-o'></i>"
                                CommandName="Delete" />   
                        </Commands>
                        <Listeners>
                            <Command Handler="CommandHandler(command,record);" />
                        </Listeners>
                        <PrepareToolbar Fn="prepareActivity" />
                    </ext:CommandColumn>
                </Columns>
            </ColumnModel>
            <SelectionModel>
                <ext:RowSelectionModel ID="RowSelectionModel5" runat="server" Mode="Single" />
            </SelectionModel>
        </ext:GridPanel>

        <br />

        <ext:Button runat="server" Cls="btn btn-primary btn-sect" Width="150" StyleSpec="margin-top:10px"
                Height="30" ID="btnAddNewOther" Text="<i></i>Add New Line">
            <DirectEvents>
                <Click OnEvent="btnAddNewOther_Click">
                    <EventMask ShowMask="true" />
                </Click>
            </DirectEvents>
        </ext:Button>
        
        <br />
        
    </div>


    <ext:Window ID="WClientVisit" runat="server" Title="Add/Edit Client Visit" Icon="Application"
        Width="550" Height="510" BodyPadding="5" Hidden="true" Modal="true">
        <Content>

            <table class="fieldTable" style="margin-left:20px;">
                
                <tr>
                     <td>
                        <pr:CalendarExtControl Width="200" FieldLabel="Date *" ID="txtDateCV" runat="server"
                            LabelAlign="Top" LabelSeparator="">                     
                        </pr:CalendarExtControl>
                        <asp:RequiredFieldValidator Display="None" ID="rfvDateCV" runat="server" ValidationGroup="SaveClientVisit"
                            ControlToValidate="txtDateCV" ErrorMessage="Date is required." />
                    </td>
                    <td></td>
                </tr>

                <tr>
                    <td>
                        <ext:ComboBox ID="cmbClientNameCV" runat="server" ValueField="ClientId" DisplayField="ClientName" FieldLabel="Client Name *" Width="200"
                            LabelAlign="top" LabelSeparator="" ForceSelection="true" QueryMode="Local">
                            <Store>
                                <ext:Store ID="Store3" runat="server">
                                    <Model>
                                        <ext:Model ID="Model3" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="ClientId" Type="String" />
                                                <ext:ModelField Name="ClientName" Type="String" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                        </ext:ComboBox>
                        <asp:RequiredFieldValidator Display="None" ID="rfvNameCV" runat="server" ValidationGroup="SaveClientVisit"
                            ControlToValidate="cmbClientNameCV" ErrorMessage="Name is required." />
                    </td>
                   <td></td>
                </tr>

                <tr>
                    <td>
                        <ext:TimeField  ID="tfStartTimeCV"  runat="server"  MinTime="9:00" MaxTime="18:00" Increment="60" Width="200" SelectedTime="09:00"
                            Format="hh:mm tt" FieldLabel="Start Time" LabelAlign="Top" LabelSeparator="">
                        </ext:TimeField>
                    </td>
                    <td>
                         <ext:TimeField  ID="tfEndTimeCV"  runat="server"  MinTime="9:00" MaxTime="18:00" Increment="60" Width="200" SelectedTime="09:00"
                            Format="hh:mm tt" FieldLabel="End Time" LabelAlign="Top" LabelSeparator="">  
                        </ext:TimeField>
                    </td>
                </tr>

                <tr>
                    <td>
                        <ext:TextField ID="txtRepresentativeCV" LabelSeparator="" runat="server" FieldLabel="Representative"
                            Width="200" LabelAlign="Top">
                        </ext:TextField>
                    </td>
                    <td>    
                        <pr:CalendarExtControl Width="200" FieldLabel="Next Planned Visit" ID="txtNextPlannedVisitCV" runat="server"
                            LabelAlign="Top" LabelSeparator="">                     
                        </pr:CalendarExtControl>
                        
                    </td>
                </tr>

                <tr>
                    <td colspan="2">
                        <ext:TextArea ID="txtIssueCV" runat="server" FieldLabel="Issue" LabelSeparator=""
                            LabelAlign="Top" Rows="3" Width="410" Height="80" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">                       
                        <ext:TextArea ID="txtResultCV" runat="server" FieldLabel="Result" LabelSeparator=""
                            LabelAlign="Top" Rows="3" Width="410" Height="80" />
                    </td>
                    
                </tr>

                <tr>
                    <td valign="bottom" colspan="2">
                        <div class="popupButtonDiv">
                            <ext:Button runat="server" ID="btnSaveCV" Cls="btn btn-primary" Text="<i></i>Save">
                                <DirectEvents>
                                    <Click OnEvent="btnSaveCV_Click">
                                        <EventMask ShowMask="true" />
                                    </Click>
                                </DirectEvents>
                                <Listeners>
                                    <Click Handler="valGroup = 'SaveClientVisit'; if(CheckValidation()) return this.disable(); else return false;">
                                    </Click>
                                </Listeners>
                            </ext:Button>
                            <div class="btnFlatOr">
                                or</div>
                            <ext:LinkButton ID="LinkButton2" StyleSpec="padding:0px;" runat="server" Text="Cancel"
                                Cls="btnFlatLeftGap">
                                <Listeners>
                                    <Click Handler="#{WClientVisit}.hide();" />
                                </Listeners>
                            </ext:LinkButton>
                       
                        </div>
                    </td>
                </tr>

            </table>
        </Content>
    </ext:Window>

    <ext:Window ID="WSoftwareTesting" runat="server" Title="Add/Edit Software Testing" Icon="Application"
        Width="550" Height="600" BodyPadding="5" Hidden="true" Modal="true">
        <Content>

            <table class="fieldTable" style="margin-left:20px;">

                <tr>
                    <td>
                        <pr:CalendarExtControl Width="200" FieldLabel="Date *" ID="txtDateST" runat="server"
                            LabelAlign="Top" LabelSeparator="">                     
                        </pr:CalendarExtControl>
                        <asp:RequiredFieldValidator Display="None" ID="rfvDateST" runat="server" ValidationGroup="SaveSoftwareTest"
                            ControlToValidate="txtDateST" ErrorMessage="Date is required." />
                    </td>
                    <td></td>
                </tr>

                <tr>
                    <td>
                        <ext:ComboBox ID="cmbSoftwareST" runat="server" ValueField="SoftwareId" DisplayField="SoftwareName" FieldLabel="Software Name *" Width="200"
                            LabelAlign="top" LabelSeparator="" ForceSelection="true" QueryMode="Local">
                            <Store>
                                <ext:Store ID="Store7" runat="server">
                                    <Model>
                                        <ext:Model ID="Model7" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="SoftwareId" Type="String" />
                                                <ext:ModelField Name="SoftwareName" Type="String" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                        </ext:ComboBox>

                        <asp:RequiredFieldValidator Display="None" ID="rfvName" runat="server" ValidationGroup="SaveSoftwareTest"
                            ControlToValidate="cmbSoftwareST" ErrorMessage="Software Name is required." />
                    </td>
                    <td></td>
                </tr>

                <tr>
                    <td>
                        <ext:TimeField  ID="tfStartTimeST"  runat="server"  MinTime="9:00" MaxTime="18:00" Increment="60" Width="200" SelectedTime="09:00"
                            Format="hh:mm tt" FieldLabel="Start Time" LabelAlign="Top" LabelSeparator="">                                           
                        </ext:TimeField>
                    </td>
                    <td>
                         <ext:TimeField  ID="tfEndTimeST"  runat="server"  MinTime="9:00" MaxTime="18:00" Increment="60" Width="200" SelectedTime="09:00"
                            Format="hh:mm tt" FieldLabel="End Time" LabelAlign="Top" LabelSeparator="">                                           
                        </ext:TimeField>
                    </td>
                </tr>

                <tr>
                    <td colspan="2">
                        <ext:TextArea ID="txtDescriptionST" runat="server" FieldLabel="Module Tested" LabelSeparator=""
                            LabelAlign="Top" Rows="3" Width="410" Height="80" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <ext:TextArea ID="txtResultST" runat="server" FieldLabel="Test Result" LabelSeparator=""
                            LabelAlign="Top" Rows="3" Width="410" Height="80" />
                    </td>
                </tr>

                <tr>
                    <td>                        
                    <ext:ComboBox ID="cmbProblemStatusST" runat="server" Width="200"
                        FieldLabel="Problem Status" LabelAlign="top" LabelSeparator="" ForceSelection="true"
                        QueryMode="Local">
                        <Items>
                            <ext:ListItem Text="Issue Created" Value="Issue Created" />
                            <ext:ListItem Text="Resoloved" Value="Resoloved" />
                            <ext:ListItem Text="Need Discussion" Value="Need Discussion" />
                        </Items>                         
                    </ext:ComboBox>
                    </td>
                    <td>
                       
                    </td>
                </tr>
               </table>

              <table class="fieldTable" style="margin-left:20px;">
                <tr>
                    <td colspan="2" style="padding-left:10px;">
                         <ext:FileUploadField ID="fileUploadST" runat="server" Width="200" Icon="Attach"
                            FieldLabel="File" LabelAlign="Top" LabelSeparator="" />
                        <asp:RegularExpressionValidator ID="revfileUploadST" ValidationExpression="^.*\.(doc|DOC|pdf|PDF|txt|TXT|docx|DOCX|rar|RAR)$"
                            runat="server" ValidationGroup="SaveSoftwareTest" Display="None" ControlToValidate="fileUploadST"
                            ErrorMessage="Invalid file format."></asp:RegularExpressionValidator>
                        <ext:LinkButton runat="server" ID="lnkFileName" OnDirectClick="lnkFileName_Click"
                            Text="" Hidden="true">
                            <DirectEvents>
                                <Click OnEvent="lnkFileName_Click">                                
                                </Click>
                            </DirectEvents>
                        </ext:LinkButton>

                        <ext:LinkButton Icon="Delete" runat="server" ID="lnkDeleteFile" OnDirectClick="lnkDeleteFile_Click"
                            Text="Delete file" Hidden="true">
                            <DirectEvents>
                                <Click OnEvent="lnkDeleteFile_Click">
                                    <EventMask ShowMask="true" />
                                    <Confirmation ConfirmRequest="true" Message="Are you sure, you want to delete the file?" />
                                </Click>
                            </DirectEvents>
                        </ext:LinkButton>
                    </td>
                </tr>

                <tr>
                    <td valign="bottom" colspan="2">
                        <div class="popupButtonDiv">
                            <ext:Button runat="server" ID="btnSaveST" Cls="btn btn-primary" Text="<i></i>Save">
                                <DirectEvents>
                                    <Click OnEvent="btnSaveST_Click">
                                        <EventMask ShowMask="true" />
                                    </Click>
                                </DirectEvents>
                                <Listeners>
                                    <Click Handler="valGroup = 'SaveSoftwareTest'; if(CheckValidation()) return this.disable(); else return false;">
                                    </Click>
                                </Listeners>
                            </ext:Button>
                            <div class="btnFlatOr">
                                or</div>
                            <ext:LinkButton ID="LinkButton1" StyleSpec="padding:0px;" runat="server" Text="Cancel"
                                Cls="btnFlatLeftGap">
                                <Listeners>
                                    <Click Handler="#{WSoftwareTesting}.hide();" />
                                </Listeners>
                            </ext:LinkButton>
                       
                        </div>
                    </td>
                </tr>

            </table>
        </Content>
    </ext:Window>

    <ext:Window ID="WDocumentation" runat="server" Title="Add/Edit Documentation" Icon="Application"
        Width="550" Height="420" BodyPadding="5" Hidden="true" Modal="true">
        <Content>

            <table class="fieldTable" style="margin-left:20px;">
                <tr>
                    <td>
                        <pr:CalendarExtControl Width="200" FieldLabel="Date *" ID="txtDateDoc" runat="server"
                            LabelAlign="Top" LabelSeparator="">                     
                        </pr:CalendarExtControl>
                        <asp:RequiredFieldValidator Display="None" ID="rfvDateDoc" runat="server" ValidationGroup="SaveDocumentation"
                            ControlToValidate="txtDateDoc" ErrorMessage="Date is required." />
                    </td>
                    <td></td>
                </tr>

                <tr>
                    <td>
                        <ext:ComboBox ID="cmbSoftwareDoc" runat="server" ValueField="SoftwareId" Width="200" DisplayField="SoftwareName" FieldLabel="Software Name *"
                            LabelAlign="top" LabelSeparator="" ForceSelection="true" QueryMode="Local">
                            <Store>
                                <ext:Store ID="Store8" runat="server">
                                    <Model>
                                        <ext:Model ID="Model8" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="SoftwareId" Type="String" />
                                                <ext:ModelField Name="SoftwareName" Type="String" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                        </ext:ComboBox>
                        
                        <asp:RequiredFieldValidator Display="None" ID="rfvNameDoc" runat="server" ValidationGroup="SaveDocumentation"
                            ControlToValidate="cmbSoftwareDoc" ErrorMessage="Software Name is required." />
                    </td>
                    <td></td>
                    
                </tr>

                <tr>
                    <td>
                        <ext:TimeField  ID="tfStartTimeDoc"  runat="server"  MinTime="9:00" MaxTime="18:00" Increment="60" Width="200" SelectedTime="09:00"
                            Format="hh:mm tt" FieldLabel="Start Time" LabelAlign="Top" LabelSeparator="">                                           
                        </ext:TimeField>
                    </td>
                    <td>
                         <ext:TimeField  ID="tfEndTimeDoc"  runat="server"  MinTime="9:00" MaxTime="18:00" Increment="60" Width="200" SelectedTime="09:00"
                            Format="hh:mm tt" FieldLabel="End Time" LabelAlign="Top" LabelSeparator="">                                           
                        </ext:TimeField>
                    </td>
                </tr>

                <tr>
                    <td>
                        <ext:TextField ID="txtModuleDocumentedDoc" LabelSeparator="" runat="server" FieldLabel="Module Documented"
                            Width="200" LabelAlign="Top">
                        </ext:TextField>
                    </td>
                    <td>
                        
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <ext:TextArea ID="txtDescriptionDoc" runat="server" FieldLabel="Description" LabelSeparator=""
                            LabelAlign="Top" Rows="3" Width="410" Height="80" />
                    </td>
                </tr>

                <tr>
                    <td valign="bottom" colspan="2">
                        <div class="popupButtonDiv">
                            <ext:Button runat="server" ID="btnSaveDoc" Cls="btn btn-primary" Text="<i></i>Save">
                                <DirectEvents>
                                    <Click OnEvent="btnSaveDoc_Click">
                                        <EventMask ShowMask="true" />
                                    </Click>
                                </DirectEvents>
                                <Listeners>
                                    <Click Handler="valGroup = 'SaveDocumentation'; if(CheckValidation()) return this.disable(); else return false;">
                                    </Click>
                                </Listeners>
                            </ext:Button>
                            <div class="btnFlatOr">
                                or</div>
                            <ext:LinkButton ID="LinkButton3" StyleSpec="padding:0px;" runat="server" Text="Cancel"
                                Cls="btnFlatLeftGap">
                                <Listeners>
                                    <Click Handler="#{WDocumentation}.hide();" />
                                </Listeners>
                            </ext:LinkButton>
                       
                        </div>
                    </td>
                </tr>

            </table>
        </Content>
    </ext:Window>

    <ext:Window ID="WRemoteSupport" runat="server" Title="Add/Edit Remote Support" Icon="Application"
        Width="550" Height="400" BodyPadding="5" Hidden="true" Modal="true">
        <Content>

            <table class="fieldTable" style="margin-left:20px;">

                <tr>
                    <td>
                        <pr:CalendarExtControl Width="200" FieldLabel="Date *" ID="txtDateRS" runat="server"
                            LabelAlign="Top" LabelSeparator="">                     
                        </pr:CalendarExtControl>
                        <asp:RequiredFieldValidator Display="None" ID="rfvDateRS" runat="server" ValidationGroup="SaveRemoteSupport"
                            ControlToValidate="txtDateRS" ErrorMessage="Date is required." />
                    </td>
                    <td></td>
                </tr>

                <tr>
                    <td>
                        <ext:ComboBox ID="cmbClientRS" runat="server" ValueField="ClientId" DisplayField="ClientName" FieldLabel="Client Name *" Width="200"
                            LabelAlign="top" LabelSeparator="" ForceSelection="true" QueryMode="Local">
                            <Store>
                                <ext:Store ID="Store9" runat="server">
                                    <Model>
                                        <ext:Model ID="Model9" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="ClientId" Type="String" />
                                                <ext:ModelField Name="ClientName" Type="String" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                        </ext:ComboBox>

                        <asp:RequiredFieldValidator Display="None" ID="rfvNameRS" runat="server" ValidationGroup="SaveRemoteSupport"
                            ControlToValidate="cmbClientRS" ErrorMessage="Client Name is required." />
                    </td>
                    <td></td>
                </tr>

                <tr>
                    <td>
                        <ext:TimeField  ID="tfStartTimeRS"  runat="server"  MinTime="9:00" MaxTime="18:00" Increment="60" Width="200" SelectedTime="09:00"
                            Format="hh:mm tt" FieldLabel="Start Time" LabelAlign="Top" LabelSeparator="">                                           
                        </ext:TimeField>
                    </td>
                    <td>
                         <ext:TimeField  ID="tfEndTimeRS"  runat="server"  MinTime="9:00" MaxTime="18:00" Increment="60" Width="200" SelectedTime="09:00"
                            Format="hh:mm tt" FieldLabel="End Time" LabelAlign="Top" LabelSeparator="">                                           
                        </ext:TimeField>
                    </td>
                </tr>

                <tr>
                    <td>
                        <ext:ComboBox ID="cmbSupportModeRS" runat="server" Width="200"
                            FieldLabel="Support Mode" LabelAlign="top" LabelSeparator="" ForceSelection="true"
                            QueryMode="Local">
                            <Items>
                                <ext:ListItem Text="Phone" Value="Phone" />
                                <ext:ListItem Text="Screen Share" Value="Screen Share" />
                                <ext:ListItem Text="Online" Value="Online" />
                            </Items>                         
                        </ext:ComboBox>
                    </td>
                    <td>
                        <ext:TextField ID="txtIssueRS" LabelSeparator="" runat="server" FieldLabel="Issue"
                            Width="200" LabelAlign="Top">
                        </ext:TextField>
                    </td>
                </tr>

                <tr>
                    <td>
                        <ext:TextField ID="txtResultRS" LabelSeparator="" runat="server" FieldLabel="Result"
                            Width="200" LabelAlign="Top">
                        </ext:TextField>
                    </td>
                    <td>
                        <pr:CalendarExtControl Width="200" FieldLabel="Next Planned Visit" ID="txtNextPlannedVisitDateRS" runat="server"
                            LabelAlign="Top" LabelSeparator="">                     
                        </pr:CalendarExtControl>
                    </td>
                </tr>

                <tr>
                    <td valign="bottom" colspan="2">
                        <div class="popupButtonDiv">
                            <ext:Button runat="server" ID="btnSaveRS" Cls="btn btn-primary" Text="<i></i>Save">
                                <DirectEvents>
                                    <Click OnEvent="btnSaveRS_Click">
                                        <EventMask ShowMask="true" />
                                    </Click>
                                </DirectEvents>
                                <Listeners>
                                    <Click Handler="valGroup = 'SaveRemoteSupport'; if(CheckValidation()) return this.disable(); else return false;">
                                    </Click>
                                </Listeners>
                            </ext:Button>
                            <div class="btnFlatOr">
                                or</div>
                            <ext:LinkButton ID="LinkButton4" StyleSpec="padding:0px;" runat="server" Text="Cancel"
                                Cls="btnFlatLeftGap">
                                <Listeners>
                                    <Click Handler="#{WRemoteSupport}.hide();" />
                                </Listeners>
                            </ext:LinkButton>
                       
                        </div>
                    </td>
                </tr>

            </table>
        </Content>
    </ext:Window>

    <ext:Window ID="WOther" runat="server" Title="Add/Edit Other" Icon="Application"
        Width="550" Height="400" BodyPadding="5" Hidden="true" Modal="true">
        <Content>

            <table class="fieldTable" style="margin-left:20px;">
                
                <tr>
                    <td>
                         <pr:CalendarExtControl Width="200" FieldLabel="Date *" ID="txtDateOther" runat="server"
                            LabelAlign="Top" LabelSeparator="">                     
                        </pr:CalendarExtControl>
                        <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator2" runat="server" ValidationGroup="SaveOther"
                            ControlToValidate="txtDateOther" ErrorMessage="Date is required." />
                    </td>
                    <td></td>
                </tr>

                <tr>
                    <td>
                        <ext:TextField ID="txtWorkType" LabelSeparator="" runat="server" FieldLabel="Work Type *"
                            Width="200" LabelAlign="Top">
                        </ext:TextField>
                        <asp:RequiredFieldValidator Display="None" ID="rfvWorkType" runat="server" ValidationGroup="SaveOther"
                            ControlToValidate="txtWorkType" ErrorMessage="Work Type is required." />
                    </td>
                    <td>
                       
                    </td>
                </tr>

                <tr>
                    <td>
                        <ext:TimeField  ID="tfStartTimeOther"  runat="server"  MinTime="9:00" MaxTime="18:00" Increment="60" Width="200" SelectedTime="09:00"
                            Format="hh:mm tt" FieldLabel="Start Time" LabelAlign="Top" LabelSeparator="">                                           
                        </ext:TimeField>
                    </td>
                    <td>
                         <ext:TimeField  ID="tfEndTimeOther"  runat="server"  MinTime="9:00" MaxTime="18:00" Increment="60" Width="200" SelectedTime="09:00"
                            Format="hh:mm tt" FieldLabel="End Time" LabelAlign="Top" LabelSeparator="">                                           
                        </ext:TimeField>
                    </td>
                </tr>

                <tr>
                    <td colspan="2">
                        <ext:TextArea ID="txtDescriptionOther" runat="server" FieldLabel="Description" LabelSeparator=""
                            LabelAlign="Top" Rows="3" Width="410" Height="80" />
                    </td>
                </tr>

                <tr>
                    <td valign="bottom" colspan="2">
                        <div class="popupButtonDiv">
                            <ext:Button runat="server" ID="btnSaveOther" Cls="btn btn-primary" Text="<i></i>Save">
                                <DirectEvents>
                                    <Click OnEvent="btnSaveOther_Click">
                                        <EventMask ShowMask="true" />
                                    </Click>
                                </DirectEvents>
                                <Listeners>
                                    <Click Handler="valGroup = 'SaveOther'; if(CheckValidation()) return this.disable(); else return false;">
                                    </Click>
                                </Listeners>
                            </ext:Button>
                            <div class="btnFlatOr">
                                or</div>
                            <ext:LinkButton ID="LinkButton5" StyleSpec="padding:0px;" runat="server" Text="Cancel"
                                Cls="btnFlatLeftGap">
                                <Listeners>
                                    <Click Handler="#{WOther}.hide();" />
                                </Listeners>
                            </ext:LinkButton>
                       
                        </div>
                    </td>
                </tr>

            </table>
        </Content>
    </ext:Window>

</asp:Content>
