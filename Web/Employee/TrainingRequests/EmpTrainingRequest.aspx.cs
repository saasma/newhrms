﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL.Entity;
using BLL;
using Utils.Calendar;

namespace Web.Employee.TrainingRequests
{
    public partial class EmpTrainingRequest : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();
            }
        }

        private void Initialise()
        {
            ShowEvents();

            BindTrainingRequest();
            BindTrainingRequestHistory();
            BindMyTrainingRequests();
        }
       
        private void ClearFields()
        {
            txtNotes.Text = "";
            dispTrainingName.Html = "";
            dispHostedBy.Html = "";
            dispResourcePerson.Html = "";
            dispTrainingDetails.Html = "";
            btnSave.Text = "Request Participation";
        }

        private void BindMyTrainingRequests()
        {
            gridMyTrainingRequests.GetStore().DataSource = TrainingManager.GetTrainingReqByEmployeeId(SessionManager.CurrentLoggedInEmployeeId);
            gridMyTrainingRequests.GetStore().DataBind();
        }

        private void ShowEvents()
        {
            List<Training> list = new List<Training>();
            int listCount = 0;
            int day = 0;
            int month = System.DateTime.Now.Month;
            int year = System.DateTime.Now.Year;
            int nextMonth = 0,  nextYear = 0;

            dispThisMonthTitle.Html = "";
            dispThisMonthDetails.Html = "";
            dispNearFutureDetails.Html = "";
            dispNextMonthTitle.Html = "";
            dispNextMonthDetails.Html = "";
            dispMyTrainingPartReq.Html = "";

            string monthName = DateHelper.GetMonthName(month, true);

            //current month
            list = TrainingManager.GetPublishedTrainingByMonth(month, year);
            listCount = list.Count;

            //dispThisMonthTitle.Html = monthName + " " + year.ToString() + "{Current Month}";
            dispThisMonthTitle.Html = "This Month - " + monthName + " " + year.ToString();

            foreach (var item in list)
            {
                dispThisMonthDetails.Html += item.Name + ", " + item.FromDate.Value.Day + "-" + item.ToDate.Value.Day + ", " + item.Days + " days, " + ((TrainingTypeEnum)item.Type.Value).ToString() + (item.LeftSeats == 0 ? "" : "  (" + item.LeftSeats.ToString() + ")") + "   <a class='clsCursorHand' onclick='return DetailsClick(" + item.TrainingID.ToString() + ");'> Details</a>" + "<br>";
            }
            if (list.Count == 0)
                dispThisMonthTitle.Hidden = true;
         
            list.Clear();

            list = TrainingManager.GetLatestTrainingList();
            listCount = list.Count;
            foreach (var item in list)
            {
                dispNearFutureDetails.Html += item.Name + ", " + item.FromDate.Value.Day + "-" + item.ToDate.Value.Day + ", " + item.Days + " days, " + ((TrainingTypeEnum)item.Type.Value).ToString() + (item.LeftSeats == 0 ? "" : "  (" + item.LeftSeats.ToString() + ")") + "   <a class='clsCursorHand' onclick='return DetailsClick(" + item.TrainingID.ToString() + ");'> Details</a>" + "<br>";
            }
            if (list.Count == 0)
                dispNearFuture.Hidden = true;

            //next month
            if (month == 12)
            {
                nextMonth = 1;
                nextYear = year + 1;
            }
            else
            {
                nextMonth = month + 1;
                nextYear = year + 1;
            }

            monthName = DateHelper.GetMonthName(nextMonth, true);

            list.Clear();
            list = TrainingManager.GetPublishedTrainingByMonth(nextMonth, year);
            listCount = list.Count;
            
            //dispNextMonthTitle.Html = monthName + " " + year.ToString() + "{Next Month}";
            dispNextMonthTitle.Html = "Next Month - " + monthName + " " + year.ToString();

            foreach (var item in list)
            {
                dispNextMonthDetails.Html += item.Name + ", " + item.FromDate.Value.Day + "-" + item.ToDate.Value.Day + ", " + item.Days + " days, " + ((TrainingTypeEnum)item.Type.Value).ToString() + (item.LeftSeats == 0 ? "" : "  (" + item.LeftSeats.ToString() + ")") + "   <a class='clsCursorHand' onclick='return DetailsClick(" + item.TrainingID.ToString() + ");'> Details</a>" + "<br>";
            }
            if (list.Count == 0)
                dispNextMonthTitle.Hidden = true;

            list.Clear();
            list = TrainingManager.GetTrainingRequestForEmployee(SessionManager.CurrentLoggedInEmployeeId);
            foreach (var item in list)
            {
                //dispMyTrainingPartReq
                dispMyTrainingPartReq.Html += item.Name + ", " + item.FromDate.Value.Day + "-" + item.ToDate.Value.Day + ", " + item.Days + " days, " + ((TrainingTypeEnum)item.Type.Value).ToString() +  "   <input type='button' title='Test' />" + "<br>";
            }
        }

        private void BindTrainingRequest()
        {          
            gridTrainingRequest.GetStore().DataSource = TrainingManager.GetTrainingRequestsByEmpId(SessionManager.CurrentLoggedInEmployeeId);
            gridTrainingRequest.GetStore().DataBind();
        }

        private void BindTrainingRequestHistory()
        {
            DateTime? dateFilter = null;
            if (!string.IsNullOrEmpty(txtDateFilter.Text.Trim()))
                dateFilter = DateTime.Parse(txtDateFilter.Text.Trim());

            storeTrainingReqHistory.DataSource = TrainingManager.GetTrainingRequestHistoryByEmpId(SessionManager.CurrentLoggedInEmployeeId, dateFilter);
            storeTrainingReqHistory.DataBind();
        }

        protected void btnAddNew_Click(object sender, DirectEventArgs e)
        {
            ClearFields();
            windowTrainSave.Show();
        }

        protected void btnTrainng_Click(object sender, DirectEventArgs e)
        {
            ClearFields();
            int trainingId = int.Parse(hdnTrainingId.Text);

            BindTrainingDetails(trainingId);
        }

        private void BindTrainingDetails(int trainingId)
        {
            Training obj = TrainingManager.GetTrainingById(trainingId);
            if (obj != null)
            {
                dispTrainingName.Html = obj.Name;

                //dispTrainingDetails.Html = DateHelper.GetMonthName(obj.FromDate.Value.Month, true) + " " + obj.FromDate.Value.Day.ToString() + "-" + obj.ToDate.Value.Day.ToString() + "," + obj.FromDate.Value.Year.ToString() + " " + obj.Days.ToString() + " days from " + obj.StartTime + " to " + obj.EndTime;
                dispTrainingDetails.Html = DateHelper.GetMonthName(obj.FromDate.Value.Month, true) + " " + obj.FromDate.Value.Day.ToString() + "-" + obj.ToDate.Value.Day.ToString() + "," + obj.FromDate.Value.Year.ToString() + ", " + obj.Days.ToString() + " days" ;//from " + GetAppropriateTime(obj.StartTime.Value) + " to " + GetAppropriateTime(obj.EndTime.Value);

                dispHostedBy.Html = obj.Host;
                dispResourcePerson.Html = obj.ResourcePerson;
                BindTrainingLines(trainingId);
              

            }
            windowTrainSave.Show();
        }

        private void BindTrainingLines(int trainingId)
        {
            List<TrainingPlanningLine> _listTrainingList = TrainingManager.GetTrainingLines(trainingId);
            foreach (TrainingPlanningLine item in _listTrainingList)
            {
                if (item.StartTime != null)
                {
                    DateTime inTimeDateTime = new DateTime(item.DateEng.Value.Year, item.DateEng.Value.Month, item.DateEng.Value.Day, item.StartTime.Value.Hours, item.StartTime.Value.Minutes, item.StartTime.Value.Seconds);
                    //  item.InTimeString = inTimeDateTime.ToString();
                    item.InTimeDT = inTimeDateTime;
                }

                if (item.EndTime != null)
                {
                    DateTime outTimeDateTime = new DateTime(item.DateEng.Value.Year, item.DateEng.Value.Month, item.DateEng.Value.Day, item.EndTime.Value.Hours, item.EndTime.Value.Minutes, item.EndTime.Value.Seconds);
                    //  item.OutTimeString = outTimeDateTime.ToString();
                    item.OutTimeDT = outTimeDateTime;
                }
            }

            StoreTrainingLines.DataSource = _listTrainingList;
            StoreTrainingLines.DataBind();
        }

        private string GetAppropriateTime(TimeSpan ts)
        {
            if (ts.TotalHours < 12)
                return ts.TotalHours.ToString() + " AM";
            else if (ts.TotalHours == 12)
                return ts.TotalHours.ToString() + " PM";
            else
                return (ts.TotalHours - 12).ToString() + " PM";
        }

        protected void btnEdit_Click(object sender, DirectEventArgs e)
        {
            int trainingRequestId = int.Parse(hdnTrainingRequestId.Text);

            int trainingId = 0;
            TrainingRequest obj = TrainingManager.GetTrainingRequestById(trainingRequestId);
            trainingId = obj.TrainingID.Value;
            BindTrainingDetails(trainingId);

            if (obj.Notes != null)
                txtNotes.Text = obj.Notes;

            btnSave.Text = "Update Participation";
        }
               

        protected void btnSave_Click(object sender, DirectEventArgs e)
        {
            Page.Validate("SaveTrainingReq");
            if (Page.IsValid)
            {
                TrainingRequest obj = new TrainingRequest();

                if (!string.IsNullOrEmpty(hdnTrainingRequestId.Text))
                    obj.TrainingRequestId = int.Parse(hdnTrainingRequestId.Text);

                obj.TrainingID = int.Parse(hdnTrainingId.Text);

                obj.RequestedDate = System.DateTime.Now;
                
                if(!string.IsNullOrEmpty(txtNotes.Text.Trim()))
                    obj.Notes=txtNotes.Text.Trim();

                obj.EmployeeId = SessionManager.CurrentLoggedInEmployeeId;
                obj.Status = (int)TrainingRequestStatusEnum.Pending;

                Status status = TrainingManager.SaveUpdateTrainingRequest(obj);
                if (status.IsSuccess)
                {
                    if(!string.IsNullOrEmpty(hdnTrainingRequestId.Text))
                        NewMessage.ShowNormalMessage("Training Request updated successfully.");
                    else
                        NewMessage.ShowNormalMessage("Training Request saved successfully.");
                    BindTrainingRequest();
                    BindTrainingRequestHistory();
                    BindMyTrainingRequests();
                    ShowEvents();
                    ClearFields();
                    hdnTrainingId.Text = "";
                    hdnTrainingRequestId.Text = "";
                    windowTrainSave.Close();
                }
                else
                {
                    NewMessage.ShowWarningMessage(status.ErrorMessage);
                }
            }
        }        

        protected void btnDelete_Click(object sender, DirectEventArgs e)
        {
            int trainingRequestId = int.Parse(hdnTrainingRequestId.Text);
            Status status = TrainingManager.DeleteTrainingRequest(trainingRequestId);
            if (status.IsSuccess)
            {
                NewMessage.ShowNormalMessage("Training Request deleted successfully.");
                BindTrainingRequest();
                BindTrainingRequestHistory();
                ShowEvents();
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }
        }

        protected void BindMyTROnRead(object sender, StoreReadDataEventArgs e)
        {
            BindMyTrainingRequests();
        }

        protected void BindTrOnRead(object sender, StoreReadDataEventArgs e)
        {
            BindTrainingRequest();
        }

        protected void BindTRHistory(object sender, StoreReadDataEventArgs e)
        {
            BindTrainingRequestHistory();
        }

        protected void btnLoad_Click(object sender, DirectEventArgs e)
        {
            if (string.IsNullOrEmpty(txtDateFilter.Text.Trim()))
                return;

            BindTrainingRequestHistory();
        }

    }
}