﻿<%@ Page Title="Training Request" Language="C#" MasterPageFile="~/Master/EmployeeExt.Master" AutoEventWireup="true" CodeBehind="EmpTrainingRequest.aspx.cs" Inherits="Web.Employee.TrainingRequests.EmpTrainingRequest" %>

<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">

<style type="text/css">

.blockBottomBorder
{
    border-bottom: 4px solid #EAF3FA;
    padding-bottom: 1px;
    width:350px;
}
.blockTitle
{
    font-size: 14px;
    padding-left: 2px;
    margin-bottom:5px;
}
.blockTraining
{
    width:500px;
    background-color :#EAF3FA;
    margin-top:5px;
    height:30px;
    padding-top:7px;
}
.textCls
{
    color:#47759E;
    padding-top:3px;
    font-weight:bold;
}

.blockBottomBorder1
{
    border-bottom: 4px solid #EAF3FA;
    width:1148px;
}
     
.pending-row{
	color:#A8552C;
} 
.approved-row
{
    color:#308530;
}
.houseful-row
{
    color:#5B5C58;
}
.waiting-row
{
    color:#003399;
}
.clsCursorHand
{
    cursor:pointer;
}

 #tblBookedInfo td
        {
            padding: 5px;
            color: White;
        }
        
         .blockTrainingName
        {
            width: 350px;
            background-color: #EAF3FA;
            margin-top: 5px;
            padding: 5px 5px 5px 0px;
            border-bottom: 1px solid #428BCA;
        }
        
</style>



<script type="text/javascript">
        function DetailsClick(TrainingId)
        {        
         
            <%= hdnTrainingId.ClientID %>.setValue(TrainingId);
            
                <%= btnTrainng.ClientID %>.fireEvent('click');
        }

        var CommandHandler = function(command, record){
            <%= hdnTrainingRequestId.ClientID %>.setValue(record.data.TrainingRequestId);
            <%= hdnTrainingId.ClientID %>.setValue(record.data.TrainingID);
            
                if(command=="Edit")
                {
                    <%= btnEdit.ClientID %>.fireEvent('click');
                }
                else if(command=="Delete")
                {
                    <%= btnDelete.ClientID %>.fireEvent('click');
                }
               
             };

           var getRowClass = function (record) {
                if (record.data.Status == 1) {
                    return "pending-row";
                }                
                else if (record.data.Status == 2) {
                    return "approved-row";
                }
                else if (record.data.Status == 3) {
                    return "houseful-row";
                }
                else{
                    return "waiting-row";
                }
            }

</script>


</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="mainContent" runat="server">

<ext:Hidden ID="hdnTrainingId" runat="server" />
<ext:Hidden ID="hdnTrainingRequestId" runat="server" />

<ext:LinkButton ID="btnTrainng" runat="server" Hidden="true">
        <DirectEvents>
            <Click OnEvent="btnTrainng_Click">
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:LinkButton>


<ext:LinkButton ID="btnEdit" runat="server" Hidden="true">
        <DirectEvents>
            <Click OnEvent="btnEdit_Click">
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:LinkButton>

    <ext:LinkButton ID="btnDelete" runat="server" Hidden="true">
        <DirectEvents>
            <Click OnEvent="btnDelete_Click">
                <EventMask ShowMask="true" />
                <Confirmation ConfirmRequest="true" Message="Are you sure, you want to delete the training." />
            </Click>
        </DirectEvents>
    </ext:LinkButton>

    

<div class="innerLR">
    
    <h4 class="heading">
         <div class="blockBottomBorder1">Training Home</div></h4>
        <div style="padding-bottom: 15px">
            <table>
                               
            </table>
        </div>
        
       <table>
            <tr>
                <td valign="top" style="width:650px;">
                
                     <div class="blockTraining">
                    <span class="blockTitle" style="color:#47759E;">Available Trainings</span> </div>             

                    <div class="blockTraining"> <ext:DisplayField ID="dispThisMonthTitle" FieldCls="textCls"  runat="server" /></div>
                     <ext:DisplayField ID="dispThisMonthDetails"  runat="server" />
                     <br />
                     <div class="blockTraining"><ext:DisplayField ID="dispNextMonthTitle" runat="server" FieldCls="textCls" /></div>
                     <ext:DisplayField ID="dispNextMonthDetails"  runat="server" />
                     <br />
                      <div class="blockTraining"><ext:DisplayField ID="dispNearFuture" Text="Near Future" runat="server" FieldCls="textCls" /></div>
                     <ext:DisplayField ID="dispNearFutureDetails" runat="server" />
                    
                </td>
                

                 <td valign="top">
                
                     <div class="blockTraining">
                    <span class="blockTitle" style="color:#47759E;">My Training Participation Request</span> </div>             
                                       
                     <ext:DisplayField ID="dispMyTrainingPartReq"  runat="server" Hidden="true" />   
                     
                     <br />
                     
                     <ext:Store ID="storeMyTrainingRequests" runat="server" OnReadData="BindMyTROnRead" PageSize="20">
                                 <Model>
                                    <ext:Model ID="model2" runat="server" IDProperty="TrainingRequestId">
                                        <Fields>
                                            <ext:ModelField Name="Name" Type="String" />
                                            <ext:ModelField Name="Branch" Type="String" />
                                            <ext:ModelField Name="Department" Type="String" />                                            
                                            <ext:ModelField Name="TrainingType" Type="String" />
                                            <ext:ModelField Name="ApprovedByName" Type="String" />
                                            <ext:ModelField Name="Status" Type="Int" />
                                        </Fields>
                                    </ext:Model>
                                 </Model>
                           </ext:Store>
                            <ext:GridPanel ID="gridMyTrainingRequests" runat="server" StoreID="storeMyTrainingRequests" MinHeight="0" Width="500" >
                            <ColumnModel>
                                   <Columns>
                                        <ext:Column ID="Column4" Width="200" runat="server" DataIndex="Name" MenuDisabled="true" Align="Left" HideTitleEl="true">
                                        </ext:Column>

                                        <ext:Column ID="Column5" Width="70" runat="server" DataIndex="Branch" MenuDisabled="true" Align="Left" HideTitleEl="true">
                                        </ext:Column>  
                                        
                                        <ext:Column ID="Column6" Width="70" runat="server" DataIndex="Department" MenuDisabled="true" Align="Left" HideTitleEl="true">
                                        </ext:Column>   
                                        
                                        <ext:Column ID="Column7" Width="70" runat="server" DataIndex="TrainingType" MenuDisabled="true" Align="Left" HideTitleEl="true">
                                        </ext:Column>
                                        
                                        <ext:Column ID="Column8" Width="90" runat="server" DataIndex="ApprovedByName" MenuDisabled="true" Align="Left" HideTitleEl="true">
                                        </ext:Column>                                

                                    </Columns>                                  
                                     
                                     </ColumnModel>

                                     <View>
                                        <ext:GridView ID="GridView1" runat="server">
                                            <GetRowClass Fn="getRowClass" />                       
                                        </ext:GridView>
                                    </View>

                                      <SelectionModel>
                                        <ext:RowSelectionModel ID="RowSelectionModel3" runat="server" Mode="Single" />
                                      </SelectionModel>
                                      <BottomBar>
                                            <ext:PagingToolbar ID="paging1" runat="server">
                                            </ext:PagingToolbar>
                                      </BottomBar>
                            </ext:GridPanel>  
                            
                            <br />
                            
                              
                     <div class="blockTraining">
                    <span class="blockTitle" style="color:#47759E;">Additional Training Request</span> </div>  
               
                </td>


                 
             
            </tr>
       </table>

       <br />

          <br />

        <ext:TabPanel ID="TabPanel1" runat="server" MinHeight="500">
                <Items>
                    <ext:Panel ID="Panel1" runat="server" Title="Training Events">
                        <Content>
                            
                            <br />                          
                            <br />

                            
                           <ext:Store ID="storeTrainingReq" runat="server" OnReadData="BindTrOnRead" PageSize="20">
                                 <Model>
                                    <ext:Model ID="modelTrainingReq" runat="server" IDProperty="TrainingRequestId">
                                        <Fields>
                                            <ext:ModelField Name="TrainingID" Type="String" />
                                            <ext:ModelField Name="Name" Type="String" />
                                            <ext:ModelField Name="FromDate" Type="String" />
                                            <ext:ModelField Name="ToDate" Type="String" />
                                            <ext:ModelField Name="Days" Type="String" />
                                        </Fields>
                                    </ext:Model>
                                 </Model>
                           </ext:Store>
                            <ext:GridPanel ID="gridTrainingRequest" runat="server" StoreID="storeTrainingReq" MinHeight="0" Width="640">
                            <ColumnModel>
                                   <Columns>
                                        <ext:Column ID="colName" Width="300" runat="server" Text="Training Name" DataIndex="Name" MenuDisabled="true" Align="Left">
                                        </ext:Column>
                                        <ext:DateColumn ID="colFromDate" Width="100" runat="server" Text="From" DataIndex="FromDate" Format="yyyy-MM-dd" Sortable="false" MenuDisabled="true"></ext:DateColumn>
                                        <ext:DateColumn ID="DateColumn1" Width="100" runat="server" Text="To" DataIndex="ToDate" Format="yyyy-MM-dd" Sortable="false" MenuDisabled="true"></ext:DateColumn>

                                         <ext:Column ID="Column3" Width="70" runat="server" Text="Days" DataIndex="Days" Sortable="false" MenuDisabled="true" Align="Center">
                                        </ext:Column>                                        

                                        <ext:CommandColumn ID="CommandColumn3" runat="server" Width="80" MenuDisabled="true" Sortable="false">
                                            <Commands>
                                                <ext:GridCommand Text="Action">
                                                    <Menu EnableScrolling="false">
                                                    <Items>
                                                        <ext:MenuCommand Text="Edit" CommandName="Edit" />        
                                                        <ext:MenuCommand Text="Delete" CommandName="Delete" /> 
                                                    </Items>
                                                    </Menu>
                                                                        
                                                </ext:GridCommand>

                                            </Commands>
                                            <Listeners>
                                                <Command Handler="CommandHandler(command, record);" />
                                            </Listeners>
                                        </ext:CommandColumn>

                                    </Columns>
                            
                                     </ColumnModel>
                                      <SelectionModel>
                                        <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" Mode="Single" />
                                      </SelectionModel>
                                      <BottomBar>
                                            <ext:PagingToolbar ID="PagingToolbar1" runat="server">
                                            </ext:PagingToolbar>
                                      </BottomBar>
                            </ext:GridPanel> 
                      
                        </Content>
                    </ext:Panel>
                    <ext:Panel ID="Panel2" runat="server" Title="Event History">
                        <Content>

                            <br />
                             <table>
                                <tr>
                                    <td style="width:140px;">
                                        <ext:DateField ID="txtDateFilter" runat="server" Width="120" ></ext:DateField>
                                    </td>
                                    <td>
                                        <ext:Button ID="btnLoad" runat="server" Cls="btn btn-primary" Text="Load">
                                            <DirectEvents>
                                                <Click OnEvent="btnLoad_Click">
                                                    <EventMask ShowMask="true" />
                                                </Click>
                                            </DirectEvents>
                                        </ext:Button>
                                    </td>
                                </tr>
                            </table>
                           
                            <br />
                             <ext:Store ID="storeTrainingReqHistory" runat="server" OnReadData="BindTRHistory" PageSize="20">
                                <Model>
                                    <ext:Model ID="model1" runat="server" IDProperty="TrainingRequestId">
                                          <Fields>
                                            <ext:ModelField Name="TrainingID" Type="String" />
                                            <ext:ModelField Name="Name" Type="String" />
                                            <ext:ModelField Name="FromDate" Type="String" />
                                            <ext:ModelField Name="ToDate" Type="String" />
                                            <ext:ModelField Name="Days" Type="String" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>

                         <ext:GridPanel ID="gridTrainingRequestHistory" runat="server" StoreID="storeTrainingReqHistory" MinHeight="0" Width="640">
                             <ColumnModel>
                                   <Columns>
                                        <ext:Column ID="Column1" Width="300" runat="server" Text="Training Name" DataIndex="Name" MenuDisabled="true" Align="Left">
                                        </ext:Column>
                                        <ext:DateColumn ID="DateColumn2" Width="100" runat="server" Text="From" DataIndex="FromDate" Format="yyyy-MM-dd" Sortable="false" MenuDisabled="true"></ext:DateColumn>
                                        <ext:DateColumn ID="DateColumn3" Width="100" runat="server" Text="To" DataIndex="ToDate" Format="yyyy-MM-dd" Sortable="false" MenuDisabled="true"></ext:DateColumn>

                                         <ext:Column ID="Column2" Width="70" runat="server" Text="Days" DataIndex="Days" Sortable="false" MenuDisabled="true" Align="Center">
                                        </ext:Column>

                                        <ext:CommandColumn ID="CommandColumn1" runat="server" Width="80" MenuDisabled="true" Sortable="false">
                                            <Commands>
                                                <ext:GridCommand Text="Action">
                                                    <Menu EnableScrolling="false">
                                                    <Items>
                                                        <ext:MenuCommand Text="Edit" CommandName="Edit" />        
                                                        <ext:MenuCommand Text="Delete" CommandName="Delete" /> 
                                                    </Items>
                                                    </Menu>
                                                                        
                                                </ext:GridCommand>

                                            </Commands>
                                            <Listeners>
                                                <Command Handler="CommandHandler(command, record);" />
                                            </Listeners>
                                        </ext:CommandColumn>

                                    </Columns>
                            
                                     </ColumnModel>
                                      <SelectionModel>
                                        <ext:RowSelectionModel ID="RowSelectionModel2" runat="server" Mode="Single" />
                                      </SelectionModel>
                                      <BottomBar>
                                            <ext:PagingToolbar ID="PagingToolbar2" runat="server">
                                            </ext:PagingToolbar>
                                      </BottomBar>
                            </ext:GridPanel> 
                           
                        </Content>
                    </ext:Panel>
                   
                </Items>
            </ext:TabPanel>

</div>


<ext:Window ID="windowTrainSave" runat="server" Title="Training Participation Request" Icon="None"
        Height="510" Width="730" BodyPadding="5" Hidden="true" Modal="false">
        <Content>
            <table class="fieldTable">
                
                <tr>
                    <td>
                       <ext:DisplayField ID="dispTrainingName" runat="server" style="color:#47759E; font-size:15px; font-weight:bold;"/>
                   
                        <ext:DisplayField ID="dispTrainingDetails" runat="server" style="color:#47759E;"></ext:DisplayField>
                    </td>
                </tr>   
                <tr>
                    <td  style="height:40px;">
                        Hosted By:
                        <ext:DisplayField ID="dispHostedBy" runat="server" style="color:#47759E;"/>
                    </td>
                </tr>  
                <tr> 
                    <td  style="height:40px;">
                        Resource Person:
                        <ext:DisplayField ID="dispResourcePerson" runat="server" style="color:#47759E;"/>
                    </td>   
                </tr>
                <tr>
                <td>
                  <ext:GridPanel StyleSpec="margin-top:5px; border-color: transparent;" ID="gridTrainingLines"
                            runat="server" Cls="itemgrid" Width="700" AutoScroll="true"  Height="175" >
                            <Store>
                                <ext:Store ID="StoreTrainingLines" runat="server">
                                    <Model>
                                        <ext:Model ID="Model3" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="LineID" />
                                                <ext:ModelField Name="DateEng" Type="Date" />
                                                <ext:ModelField Name="Day" Type="string" />
                                                <ext:ModelField Name="InTimeDT" Type="Date" />
                                                <ext:ModelField Name="OutTimeDT" Type="Date" />
                                                <ext:ModelField Name="Hours" />
                                                <ext:ModelField Name="IsTraining" />
                                                <ext:ModelField Name="ResourcePerson" Type="string" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                            <ColumnModel>
                                <Columns>
                                    <ext:DateColumn ID="colDate" runat="server" Align="Right" Text="Date" Width="100" 
                                        MenuDisabled="true" Sortable="false" Format="yyyy/MM/dd" DataIndex="DateEng">
                                    </ext:DateColumn>
                                    <ext:Column ID="Column24" Sortable="false" MenuDisabled="true" runat="server" Text="Day" 
                                        Width="90" Align="Left" DataIndex="Day">
                                    </ext:Column>
                                    <ext:CheckColumn ID="CheckColumn1" runat="server" Text="Is Training?" DataIndex="IsTraining"
                                        StopSelection="false" Editable="true" Width="80">
                                    </ext:CheckColumn>
                                    <ext:DateColumn ID="colInTime1" Sortable="false" MenuDisabled="true" runat="server"
                                        Text="Start Time" Align="Right" Width="70" DataIndex="InTimeDT" Format="hh:mm tt">
                                      
                                    </ext:DateColumn>
                                    <ext:DateColumn ID="colOutTime1" Sortable="false" MenuDisabled="true" runat="server"
                                        Text="End Time" Align="Right" Width="70" DataIndex="OutTimeDT" Format="hh:mm tt">
                                       
                                    </ext:DateColumn>
                                    <ext:Column ID="colWorkHours" Sortable="false" MenuDisabled="true" runat="server"
                                        Text="Hours" Align="Center" Width="70" DataIndex="Hours">
                                    </ext:Column>
                                    <ext:Column ID="asdf" Sortable="false" MenuDisabled="true" runat="server" Text="Resource Person"
                                        Align="Left" Width="200" DataIndex="ResourcePerson">
                                    </ext:Column>
                                </Columns>
                            </ColumnModel>
                        </ext:GridPanel>
                </td>
                </tr>
                                

                <tr>
                    <td>
                        <ext:TextArea ID="txtNotes" runat="server" Height="70" Width="400" FieldLabel="Notes" LabelAlign="Top" LabelSeparator="" />
                    </td>
                </tr>

                <tr>
                    <td colspan="2">
                       <table>
                            <tr>
                                <td style="padding-right:20px;">
                                    <ext:Button runat="server" Cls="btn btn-primary" StyleSpec="margin-top:20px;" ID="btnSave" Text="<i></i>Request Participation">
                                        <DirectEvents>
                                            <Click OnEvent="btnSave_Click">
                                                <EventMask ShowMask="true" />
                                            </Click>
                                        </DirectEvents>
                                        <Listeners>
                                            <Click Handler="valGroup = 'SaveTrainingReq'; return CheckValidation();">
                                            </Click>
                                        </Listeners>
                                    </ext:Button>
                                </td>
                                <td style="padding-top:20px;">
                                    <ext:LinkButton runat="server" StyleSpec="padding:0px;" ID="LinkButton1" Cls="btnFlat"
                                        BaseCls="btnFlat" Text="<i></i>Cancel">
                                        <Listeners>
                                            <Click Handler="#{windowTrainSave}.hide();">
                                            </Click>
                                        </Listeners>
                                    </ext:LinkButton>
                                </td>
                            </tr>
                       </table>
                            
                          

                            
                       
                    </td>
                </tr>
            </table>
        </Content>
    </ext:Window>


</asp:Content>
