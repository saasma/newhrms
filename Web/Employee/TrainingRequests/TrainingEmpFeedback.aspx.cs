﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL.Entity;
using BLL;
using Utils.Calendar;
using Newtonsoft.Json.Linq;

namespace Web.Employee.TrainingRequests
{
    public partial class TrainingEmpFeedback : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialize();
            }
        }

        private void Initialize()
        {
            BindTrainingCombo();
            
           
        }

        protected void Store_ReadData(object sender, StoreReadDataEventArgs e)
        {
            int totalRecords = 0;
            int TrainingID = -1;
            int EmpID = -1;
            

            if (!string.IsNullOrEmpty(cmbTraining.SelectedItem.Value))
                TrainingID = int.Parse(cmbTraining.SelectedItem.Value);

            int pageSize = int.Parse(cmbPageSize.SelectedItem.Value);

            EmpID = SessionManager.CurrentLoggedInEmployeeId;

            List<GetTrainingFeedbackOfEmployeeResult> list = TrainingManager.GetTrainingFeedbackOfEmployee(e.Start / pageSize, pageSize, EmpID, TrainingID);

            if (list.Count > 0)
                totalRecords = list[0].TotalRows.Value;

            e.Total = totalRecords;

            storeTrainingEmp.DataSource = list;
            storeTrainingEmp.DataBind();

        }

        public void btnDetail_Click(object sender,DirectEventArgs e)
        {

            int trainingId = int.Parse(hdnTrainingId.Text);

            if(TrainingManager.IsTrainingAccessibleByEmployee(trainingId,SessionManager.CurrentLoggedInEmployeeId)==false)
            {
                NewMessage.ShowWarningMessage("Training is not accessible.");
                return;
            }

            Training obj = TrainingManager.GetTrainingById(trainingId);
            if (obj != null)
            {
                dispTrainingName2.Html = obj.Name;

                //dispTrainingDetails.Html = DateHelper.GetMonthName(obj.FromDate.Value.Month, true) + " " + obj.FromDate.Value.Day.ToString() + "-" + obj.ToDate.Value.Day.ToString() + "," + obj.FromDate.Value.Year.ToString() + " " + obj.Days.ToString() + " days from " + obj.StartTime + " to " + obj.EndTime;

                int TotalDay = 0;
                BindTrainingLines(trainingId, obj.ResourcePerson, out TotalDay);


                dispTrainingDetails.Html = DateHelper.GetMonthName(obj.FromDate.Value.Month, true) + " " + obj.FromDate.Value.Day.ToString() + "-" + obj.ToDate.Value.Day.ToString() + "," + obj.FromDate.Value.Year.ToString() + ", " + TotalDay.ToString() + " days"; //from " + GetAppropriateTime(obj.StartTime.Value) + " to " + GetAppropriateTime(obj.EndTime.Value);
                dispVenue.Html = obj.Venue;
                dispNotes.Html = obj.Notes;
                dispHostedBy.Html = obj.Host;
                dispResourcePerson.Html = obj.ResourcePerson;

               

            }
            windowTrainingDetails.Center();
            windowTrainingDetails.Show();
        }
        private void BindTrainingLines(int trainingId, string RP, out int TotalTrainingDays)
        {
            List<TrainingPlanningLine> _listTrainingList = TrainingManager.GetTrainingLines(trainingId);
            List<TrainingPlanningLine> _listTrainingLineForTotal = _listTrainingList.Where(x => x.IsTraining == true).ToList();

            foreach (TrainingPlanningLine item in _listTrainingList)
            {
                if (item.StartTime != null)
                {
                    DateTime inTimeDateTime = new DateTime(item.DateEng.Value.Year, item.DateEng.Value.Month, item.DateEng.Value.Day, item.StartTime.Value.Hours, item.StartTime.Value.Minutes, item.StartTime.Value.Seconds);
                    //  item.InTimeString = inTimeDateTime.ToString();
                    item.InTimeDT = inTimeDateTime;
                }

                if (item.EndTime != null)
                {
                    DateTime outTimeDateTime = new DateTime(item.DateEng.Value.Year, item.DateEng.Value.Month, item.DateEng.Value.Day, item.EndTime.Value.Hours, item.EndTime.Value.Minutes, item.EndTime.Value.Seconds);
                    //  item.OutTimeString = outTimeDateTime.ToString();
                    item.OutTimeDT = outTimeDateTime;
                }

                //Display RP

                if (!string.IsNullOrEmpty(item.ResourcePerson))
                {
                    List<string> RPIds = item.ResourcePerson.Split(',').Distinct().ToList();
                    string ResourcePersons = "";
                    //getting Resoucre Persons
                    foreach (string Id in RPIds)
                    {
                        string Name = TrainingManager.GetResourePersons().ToList().SingleOrDefault(x => x.ResourcePersonId == int.Parse(Id)).Name;
                        if (!string.IsNullOrEmpty(ResourcePersons))
                            ResourcePersons = ResourcePersons + "," + Name;
                        else
                            ResourcePersons = Name;
                    }
                    item.ResourcePerson = ResourcePersons;
                }




                //-----------------
            }

            int TotalHours = 0, TotalMins = 0;
            foreach (TrainingPlanningLine element in _listTrainingLineForTotal)
            {
                if (element.Hours != null)
                {
                    string[] str = element.Hours.Split(':').ToArray();
                    TotalHours = TotalHours + int.Parse(str[0]);
                    TotalMins = TotalMins + int.Parse(str[1]);
                }
            }

            if (_listTrainingLineForTotal.Count > 0)

                lblDaysTotal.Text = _listTrainingLineForTotal.Count() + " Days";
            if (RP != null)
                lblTotalRP.Text = RP.Split(',').ToList().Count() + " resource persons";

            string strTime = "";
            if (TotalHours == 0 && TotalMins != 0)
                strTime = TotalMins + " Mins";

            if (TotalMins == 0 && TotalHours != 0)
                strTime = TotalHours + " Hrs";

            if (TotalHours != 0 && TotalMins != 0)
                strTime = TotalHours + " Hrs  " + TotalMins + " Mins";

            lblTotalHours.Text = strTime;
            TotalTrainingDays = _listTrainingLineForTotal.Count;
            StoreTrainingLines.DataSource = _listTrainingList;
            StoreTrainingLines.DataBind();
        }
        protected void lnkFeedback_Click(object sender, DirectEventArgs e)
        {
            ClearFeedbackWindow();
            windowFeedback.Show();
            BindcomboRatingText();
            GetFeedBackInfo();
            
        }

        protected void ClearFeedbackWindow()
        {
            txtNotes.Clear();
            txtValueAfterTraining.Clear();
            txtValueBeforeTraining.Clear();
            dispTrainingName.Clear();
            cmbRating.ClearValue();
        }

        protected void GetFeedBackInfo()
        {
            int TrainingID = 0;
            int EmployeeID = 0;

            if (!string.IsNullOrEmpty(hdnTrainingId.Text))
            {
                TrainingID = int.Parse(hdnTrainingId.Text);

                Training obj = TrainingManager.GetTrainingById(TrainingID);
                if (obj != null)
                {
                    dispTrainingName.Html = obj.Name;
                }

                if (!string.IsNullOrEmpty(hdnEmployeeId.Text))
                    EmployeeID = int.Parse(hdnEmployeeId.Text);


                if (TrainingID == 0 || EmployeeID == 0)
                    return;

                TrainingRating dbTrainingRating = TrainingManager.GetTrainingRatingOfEmployeeByTraining(TrainingID, EmployeeID);
                if (dbTrainingRating != null)
                {
                    if (dbTrainingRating.ScoreBeforeTraining != null)
                        txtValueBeforeTraining.Text = dbTrainingRating.ScoreBeforeTraining.ToString();

                    if (dbTrainingRating.ScoreAfterTraining != null)
                        txtValueAfterTraining.Text = dbTrainingRating.ScoreAfterTraining.ToString();

                    if (dbTrainingRating.MyRatingRef_Id != null)
                        cmbRating.SetValue(dbTrainingRating.MyRatingRef_Id);

                    if (!string.IsNullOrEmpty(dbTrainingRating.Note))
                        txtNotes.Text = dbTrainingRating.Note;
                }

            }
        }


        protected void btnSavefeedback_Click(object sender, DirectEventArgs e)
        {
            Page.Validate("SaveTrainingReq");

            if (Page.IsValid)
            {
                TrainingRating obj = new TrainingRating();

                if (!string.IsNullOrEmpty(hdnTrainingId.Text))
                    obj.TrainingID = int.Parse(hdnTrainingId.Text);

                if (!string.IsNullOrEmpty(hdnEmployeeId.Text))
                    obj.EmployeeId = int.Parse(hdnEmployeeId.Text);

                if (!string.IsNullOrEmpty(cmbRating.SelectedItem.Value))
                    obj.MyRatingRef_Id = int.Parse(cmbRating.SelectedItem.Value);

                if (!string.IsNullOrEmpty(txtValueBeforeTraining.Text.Trim()))
                    obj.ScoreBeforeTraining = int.Parse(txtValueBeforeTraining.Text.Trim());

                if (!string.IsNullOrEmpty(txtValueAfterTraining.Text.Trim()))
                    obj.ScoreAfterTraining = int.Parse(txtValueAfterTraining.Text.Trim());

                if (!string.IsNullOrEmpty(txtNotes.Text.Trim()))
                    obj.Note = txtNotes.Text.Trim();

                Status status = TrainingManager.SaveUpdateTrainingFeedback(obj);
                if (status.IsSuccess)
                {
                    NewMessage.ShowNormalMessage("Information has been updated successfully.");
                    windowFeedback.Close();
                    gridTrainingEmp.GetStore().Reload();
                }
                else
                {
                    NewMessage.ShowWarningMessage(status.ErrorMessage);
                }
            }
        }    
        
       
        private void BindTrainingCombo()
        {
            List<Training> _list = TrainingManager.GetAllTrainings();
            cmbTraining.GetStore().DataSource = _list;
            cmbTraining.GetStore().DataBind();
        }

        private void BindcomboRatingText()
        {
            List<TrainingRatingText> _list = TrainingManager.GetTrainingRatingText();
            cmbRating.GetStore().DataSource = _list;
            cmbRating.GetStore().DataBind();
        }

  



    }
}