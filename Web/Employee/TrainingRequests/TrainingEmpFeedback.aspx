﻿<%@ Page Title=" My Trainings" Language="C#" MasterPageFile="~/Master/EmployeeExt.Master"
    AutoEventWireup="true" CodeBehind="TrainingEmpFeedback.aspx.cs" Inherits="Web.Employee.TrainingRequests.TrainingEmpFeedback" %>

<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">

      var CommandHandler = function(command, record){

            <%= hdnEmployeeId.ClientID %>.setValue(record.data.EmployeeID);
            <%= hdnTrainingId.ClientID %>.setValue(record.data.TrainingId);

             if(command=="feedback")
                {
                    <%= lnkFeedback.ClientID %>.fireEvent('click');
             }
             else if(command=='Detail')
                 <%= btnDetail.ClientID %>.fireEvent('click');
           }


       function searchList() {
            <%=gridTrainingEmp.ClientID %>.getStore().pageSize = <%=cmbPageSize.ClientID %>.getValue();
            <%=PagingToolbar1.ClientID %>.doRefresh();
        }
        var getRowClassTrainingLn = function (record) {
            if (record.data.IsTraining==false) {
                return "commentCls";
            }
            else{
                return "new-row";
            }
        };
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="mainContent" runat="server">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    My Trainings
                </h4>
            </div>
        </div>
    </div>
    <ext:LinkButton ID="lnkFeedback" runat="server" Hidden="true">
        <DirectEvents>
            <Click OnEvent="lnkFeedback_Click">
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:LinkButton>
     <ext:LinkButton ID="btnDetail" runat="server" Hidden="true">
        <DirectEvents>
            <Click OnEvent="btnDetail_Click">
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:LinkButton>
    <div class="contentpanel">
        <ext:Hidden ID="hdnTrainingId" runat="server" />
        <ext:Hidden ID="hdnEmployeeId" runat="server" />
        <ext:Hidden ID="hdnLineID" runat="server" />
        <ext:Hidden ID="hdnStartTime" runat="server" />
        <ext:Hidden ID="hdnEndTime" runat="server" />
        <ext:Hidden ID="hdnDate" runat="server" />
        <ext:Hidden ID="hdnTotalDays" runat="server" />
        <table class="fieldTable" style="margin-top:0px;display:none">
            <tr>
                <td style="padding-left:0px">
                    <ext:ComboBox ID="cmbTraining" runat="server" Width="200" FieldLabel="Training" LabelAlign="Top"
                        LabelSeparator="" QueryMode="Local" DisplayField="Name" ValueField="TrainingID">
                        <Store>
                            <ext:Store ID="store1" runat="server">
                                <Fields>
                                    <ext:ModelField Name="TrainingID" />
                                    <ext:ModelField Name="Name" />
                                </Fields>
                            </ext:Store>
                        </Store>
                  
                    <Triggers>
                                <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                            </Triggers>
                            <Listeners>
                                <Select Handler="this.getTrigger(0).show();" />
                                <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                <TriggerClick Handler="if (index == 0) { 
                                           this.clearValue(); 
                                           this.getTrigger(0).hide();
                                       }" />
                            </Listeners>

                              </ext:ComboBox>
                </td>
               
                <td style="padding-top: 22px">
                    <ext:Button runat="server" ID="btnLoad" Cls="btn btn-primary" Text="Load">
                        <Listeners>
                            <Click Fn="searchList">
                            </Click>
                        </Listeners>
                    </ext:Button>
                </td>
            </tr>
            <%--  <tr>
                <td colspan="3">
                    <ext:DisplayField ID="dfTrainingName" FieldCls="clsBold" runat="server" Width="300" />
                </td>
            </tr>--%>
        </table>
        <ext:GridPanel ID="gridTrainingEmp" runat="server" MinHeight="0" Width="1070" StyleSpec="margin-top:10px">
            <Store>
                <ext:Store ID="storeTrainingEmp" runat="server" AutoLoad="true" OnReadData="Store_ReadData"
                    PageSize="50" RemotePaging="true" RemoteSort="true">
                    <Proxy>
                        <ext:PageProxy />
                    </Proxy>
                    <Model>
                        <ext:Model ID="Model2" runat="server" IDProperty="SN">
                            <Fields>
                                <ext:ModelField Name="EmployeeID" />
                                <ext:ModelField Name="TrainingId" />
                                <ext:ModelField Name="Name" />
                                <ext:ModelField Name="Days" />
                                <ext:ModelField Name="FromDate" />
                                <ext:ModelField Name="ToDate" />
                                <ext:ModelField Name="Host" />
                                <ext:ModelField Name="RatingText" />
                                <ext:ModelField Name="ScoreBeforeTraining" />
                                <ext:ModelField Name="ScoreAfterTraining" />
                                <ext:ModelField Name="RowNumber" />
                            </Fields>
                        </ext:Model>
                    </Model>
                </ext:Store>
            </Store>
            <ColumnModel ID="ColumnModel1" runat="server">
                <Columns>
                    <ext:RowNumbererColumn ID="Column11" runat="server" Text="SN" Width="40" 
                        MenuDisabled="true" Sortable="false">
                    </ext:RowNumbererColumn>
                    <ext:Column ID="colEmployeeName" runat="server" Text="Name" Width="200" DataIndex="Name"
                        MenuDisabled="true" Sortable="false">
                    </ext:Column>
                    <ext:Column ID="Column2" runat="server" Text="Days" Width="50" DataIndex="Days" MenuDisabled="true"
                        Sortable="false">
                    </ext:Column>
                    <ext:DateColumn ID="colFromDate" Width="100" runat="server" Text="From" DataIndex="FromDate"
                        Format="yyyy-MM-dd" Sortable="false" MenuDisabled="true">
                    </ext:DateColumn>
                    <ext:DateColumn ID="DateColumn1" Width="100" runat="server" Text="To" DataIndex="ToDate"
                        Format="yyyy-MM-dd" Sortable="false" MenuDisabled="true">
                    </ext:DateColumn>
                    <ext:Column ID="Column3" runat="server" Text="Provided by" Width="150" DataIndex="Host" Flex="1"
                        MenuDisabled="true" Sortable="false">
                    </ext:Column>
                    <ext:Column ID="Column4" runat="server" Text="My Rating" Width="100" DataIndex="RatingText"
                        MenuDisabled="true" Sortable="false">
                    </ext:Column>
                    <ext:Column ID="Column5" runat="server" Text="Skill Level - before" Width="115" DataIndex="ScoreBeforeTraining"
                        MenuDisabled="true" Sortable="false">
                    </ext:Column>
                    <ext:Column ID="Column6" runat="server" Text="Skill Level - after" Width="115" DataIndex="ScoreAfterTraining"
                        MenuDisabled="true" Sortable="false">
                    </ext:Column>
                    <ext:CommandColumn ID="CommandColumn3" runat="server" Width="90" MenuDisabled="true"
                        Sortable="false">
                        <Commands>
                            <ext:GridCommand Text="Feedback" Icon="FlagGrey" ToolTip-Text="Provide feedback for the training" CommandName="feedback">
                            </ext:GridCommand>
                        </Commands>
                        <Listeners>
                            <Command Handler="CommandHandler(command, record);" />
                        </Listeners>
                    </ext:CommandColumn>
                    <ext:CommandColumn ID="CommandColumn1" runat="server" Width="30" MenuDisabled="true"
                        Sortable="false">
                        <Commands>
                            <ext:GridCommand  Icon="ApplicationViewDetail" ToolTip-Text="View training details" CommandName="Detail">
                            </ext:GridCommand>
                        </Commands>
                        <Listeners>
                            <Command Handler="CommandHandler(command, record);" />
                        </Listeners>
                    </ext:CommandColumn>
                </Columns>
            </ColumnModel>
            <SelectionModel>
                <ext:RowSelectionModel ID="RowSelectionModel3" runat="server" Mode="Single" />
            </SelectionModel>
            <BottomBar>
                <ext:PagingToolbar Height="40" ID="PagingToolbar1" runat="server" StoreID="storeTrainingEmp"
                    DisplayInfo="true">
                    <Items>
                        <ext:Label ID="lblPageSize" runat="server" Text="Page size:" />
                        <ext:ToolbarSpacer ID="ToolbarSpacer_listItems" runat="server" Width="10" />
                        <ext:ComboBox ID="cmbPageSize" runat="server" Width="80" SelectOnFocus="true" Selectable="true"
                            ValueField="Value" DisplayField="Text" ForceSelection="true" AllowBlank="false">
                            <Listeners>
                                <Select Handler="searchList()" />
                                <%--#{cmbPageSize}.pageSize = this.getValue();#{PagingToolbar1}.moveFirst();--%>
                            </Listeners>
                            <Items>
                                <ext:ListItem Value="20" Text="20" />
                                <ext:ListItem Value="50" Text="50" />
                                <ext:ListItem Value="100" Text="100" />
                                
                            </Items>
                            <SelectedItems>
                                <ext:ListItem Index="1">
                                </ext:ListItem>
                            </SelectedItems>
                        </ext:ComboBox>
                    </Items>
                </ext:PagingToolbar>
            </BottomBar>
        </ext:GridPanel>
    </div>
     <ext:Window ID="windowTrainingDetails" runat="server" Title="Training Details"
        ButtonAlign="Left" AutoScroll="true" Icon="None" Height="550" Width="900" BodyPadding="5"
        Hidden="true" Modal="false">
        <Content>
            <div>
                <table class="fieldTable" border="0">
                    <tr>
                        <td valign="top">
                            <ext:DisplayField ID="dispTrainingName2" runat="server" Style="color: #47759E; font-size: 15px;
                                font-weight: bold;" />
                            <ext:DisplayField ID="dispTrainingDetails" runat="server" Style="color: #47759E;">
                            </ext:DisplayField>
                            <div style="    margin-top: 10px;">
                              Venue
                            <ext:DisplayField ID="dispVenue" Text="" runat="server" Style="color: #47759E;" /></div>
                        </td>
                        
                       

                    </tr>
                    <tr>
                        <td style="height: 40px;">
                            Hosted by
                            <ext:DisplayField ID="dispHostedBy" runat="server" Style="color: #47759E;" />
                        </td>
                        

                    </tr>
                    <tr>
                        <td style="height: 40px;" colspan="4">
                            Resource Person:
                            <ext:DisplayField ID="dispResourcePerson" runat="server" Style="color: #47759E;" />
                        </td>
                    </tr>
                    <tr>
                        <td style="height: 40px;" colspan="4">
                            Note:
                            <ext:DisplayField ID="dispNotes" runat="server" Style="color: #47759E;" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <ext:GridPanel StyleSpec="margin-top:5px; border-color: transparent;" ID="gridTrainingLines"
                                runat="server" Cls="itemgrid" Width="840" MinHeight="150" AutoScroll="true">
                                <Store>
                                    <ext:Store ID="StoreTrainingLines" runat="server">
                                        <Model>
                                            <ext:Model ID="Model11" runat="server">
                                                <Fields>
                                                    <ext:ModelField Name="LineID" />
                                                    <ext:ModelField Name="DateEng" Type="Date" />
                                                    <ext:ModelField Name="Day" Type="string" />
                                                    <ext:ModelField Name="InTimeDT" Type="Date" />
                                                    <ext:ModelField Name="OutTimeDT" Type="Date" />
                                                    <ext:ModelField Name="Hours" />
                                                    <ext:ModelField Name="IsTraining" />
                                                    <ext:ModelField Name="ResourcePerson" />
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <ColumnModel>
                                    <Columns>
                                        <ext:DateColumn ID="colDate" runat="server" Align="Left" Text="Date" Width="80" MenuDisabled="true"
                                            Sortable="false" Format="yyyy/MM/dd" DataIndex="DateEng">
                                        </ext:DateColumn>
                                        <ext:Column ID="Column45" Sortable="false" MenuDisabled="true" runat="server" Text="Day"
                                            Width="90" Align="Left" DataIndex="Day">
                                        </ext:Column>
                                        <ext:Column ID="asdfas" runat="server" Text="" Sortable="false" MenuDisabled="true"
                                            Width="50" DataIndex="IsTraining" Border="false">
                                           
                                        </ext:Column>
                                        <ext:DateColumn ID="colInTime1" Sortable="false" MenuDisabled="true" runat="server"
                                            Text="Start" Align="Left" Width="95" DataIndex="InTimeDT" Format="hh:mm tt">
                                        </ext:DateColumn>
                                        <ext:DateColumn ID="colOutTime1" Sortable="false" MenuDisabled="true" runat="server"
                                            Text="End" Align="Left" Width="95" DataIndex="OutTimeDT" Format="hh:mm tt">
                                        </ext:DateColumn>
                                        <ext:Column ID="colWorkHours" Sortable="false" MenuDisabled="true" runat="server"
                                            Text="Hours" Align="Left" Width="100" DataIndex="Hours">
                                        </ext:Column>
                                        <ext:TagColumn ID="TagColumn1" runat="server" Text="Resource Person(s)" DataIndex="ResourcePerson"
                                            Flex="1">
                                        </ext:TagColumn>
                                    </Columns>
                                </ColumnModel>
                                <DockedItems>
                                    <ext:FieldContainer ID="Container1" runat="server" Dock="Bottom" Layout="HBoxLayout"
                                        StyleSpec="margin-top:2px;">
                                        <Defaults>
                                            <ext:Parameter Name="height" Value="32" />
                                        </Defaults>
                                        <Items>
                                            <ext:DisplayField ID="lblDaysTotal" Cls="total-field" runat="server" Width="80" />
                                            <ext:DisplayField ID="DisplayField2" Text="" Cls="total-field" runat="server" Width="90" />
                                            <ext:DisplayField ID="DisplayField3" Text="" Cls="total-field" runat="server" Width="50" />
                                            <ext:DisplayField ID="DisplayField4" Text="" Cls="total-field" runat="server" Width="95" />
                                            <ext:DisplayField ID="DisplayField5" Text="" Cls="total-field" runat="server" Width="95" />
                                            <ext:DisplayField ID="lblTotalHours" Cls="total-field" runat="server" Width="100" />
                                            <ext:DisplayField ID="lblTotalRP" Cls="total-field" runat="server" Width="550" />
                                        </Items>
                                    </ext:FieldContainer>
                                </DockedItems>
                                <View>
                                    <ext:GridView ID="GridView2" runat="server" StripeRows="false">
                                        <GetRowClass Fn="getRowClassTrainingLn" />
                                    </ext:GridView>
                                </View>
                            </ext:GridPanel>
                        </td>
                    </tr>
                </table>
            </div>
         
        </Content>
        <Buttons>
            <ext:LinkButton runat="server" StyleSpec="padding:0px;" ID="btnClose" Cls="btnFlat"
                BaseCls="btnFlat" Text="<i></i>Close">
                <Listeners>
                    <Click Handler="#{windowTrainingDetails}.hide();">
                    </Click>
                </Listeners>
            </ext:LinkButton>
        </Buttons>
    </ext:Window>
    <ext:Window ID="windowFeedback" runat="server" Title="Training Feedback" Icon="None"
        Height="400" Width="400" BodyPadding="5" Hidden="true" Modal="false">
        <Content>
            <table class="fieldTable">
                <tr>
                    <td colspan="2">
                        <ext:DisplayField ID="dispTrainingName" runat="server" Style="color: #47759E; font-size: 15px;
                            font-weight: bold;" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <span>My skill level(in a scale of 5)</span>
                    </td>
                </tr>
                <tr>
                    <td>
                        <ext:NumberField ID="txtValueBeforeTraining" MinValue="0" MaxValue="5" runat="server"
                            LabelAlign="Top" LabelSeparator="" FieldLabel="Before the training *">
                        </ext:NumberField>
                        <asp:RequiredFieldValidator runat="server" ID="val1" ControlToValidate="txtValueBeforeTraining" ValidationGroup="SaveTrainingReq"
                            Display="None" ErrorMessage="Before training value is required." />
                    </td>
                    <td>
                        <ext:NumberField ID="txtValueAfterTraining" MinValue="0" MaxValue="5" runat="server"
                            LabelAlign="Top" LabelSeparator="" FieldLabel="After the training *">
                        </ext:NumberField>
                         <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="txtValueBeforeTraining" ValidationGroup="SaveTrainingReq"
                            Display="None" ErrorMessage="After training value is required." />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <ext:ComboBox ID="cmbRating" ForceSelection="true" runat="server" Width="150" FieldLabel="My Rating *" LabelAlign="Top"
                            LabelSeparator="" QueryMode="Local" DisplayField="RatingName" ValueField="RatingID">
                            <Store>
                                <ext:Store ID="store2" runat="server">
                                    <Fields>
                                        <ext:ModelField Name="RatingID" />
                                        <ext:ModelField Name="RatingName" />
                                    </Fields>
                                </ext:Store>
                            </Store>
                        </ext:ComboBox>
                        <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator2" ControlToValidate="cmbRating" ValidationGroup="SaveTrainingReq"
                            Display="None" ErrorMessage="My Rating is required." />
                    </td>
                    <td>
                </tr>
                <tr>
                    <td colspan="2">
                        <ext:TextArea ID="txtNotes" runat="server" Height="80" Width="314" FieldLabel="Additional remarks"
                            LabelAlign="Top" LabelSeparator="" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <table>
                            <tr>
                                <td style="padding-right: 20px;">
                                    <ext:Button runat="server" Cls="btn btn-primary" StyleSpec="margin-top:20px;" ID="btnSave"
                                        Text="<i></i>Save">
                                        <DirectEvents>
                                            <Click OnEvent="btnSavefeedback_Click">
                                                <EventMask ShowMask="true" />
                                            </Click>
                                        </DirectEvents>
                                        <Listeners>
                                            <Click Handler="valGroup = 'SaveTrainingReq'; return CheckValidation();">
                                            </Click>
                                        </Listeners>
                                    </ext:Button>
                                </td>
                                <td style="padding-top: 20px;">
                                    <ext:LinkButton runat="server" StyleSpec="padding:0px;" ID="LinkButton1" Cls="btnFlat"
                                        BaseCls="btnFlat" Text="<i></i>Cancel">
                                        <Listeners>
                                            <Click Handler="#{windowFeedback}.hide();">
                                            </Click>
                                        </Listeners>
                                    </ext:LinkButton>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </Content>
    </ext:Window>
</asp:Content>

