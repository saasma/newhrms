﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL;
using BLL.Manager;
using Web.ReportDataSetTableAdapters;
using Web.Master;
using BLL;
using Web.CP.Report.Templates.Pay.Detail;
using Utils;
using BLL.Base;
using System.IO;
using Ext.Net;
using System.Text;
using Utils.Helper;
using System.Web.Security;
using BLL.Entity;

namespace Web.Employee
{
    public partial class LeaveApproval : BasePage
    {
        LeaveAttendanceManager mgr = new LeaveAttendanceManager();

        protected void Page_PreRender(object sender, EventArgs e)
        {
            // Check if QueryString contains LeaveRequest Id to be viewed
            if (!X.IsAjaxRequest)
            {
                if (Request.QueryString["Data"] != null && Request.QueryString["Data"] != "")
                {
                    int leaveRequestId = 0;
                    if (int.TryParse(Request.QueryString["Data"], out leaveRequestId))
                    {
                        DAL.LeaveRequest leave = LeaveAttendanceManager.GetLeaveRequestById(leaveRequestId);
                        if (leave != null)
                        {
                            if (LeaveRequestManager.CanCurrentEmployeeChangeLeaveRequestStatus(SessionManager.CurrentLoggedInEmployeeId, leave))
                            {
                                X.Js.Call("MoreHandler", leaveRequestId);
                                //ClickMore(leaveRequestId);
                            }
                        }
                    }
                }
            }
        }

        [DirectMethod]
        public static string GetDaysForLeave(string fromdate, string todate, int leaveTypeId1,int? empId)
        {
            float daysCount = 0;

            if (empId == null || empId == -1)
                empId = 0;

            DateTime from = Convert.ToDateTime(fromdate);
            DateTime? to = null;

            if (!string.IsNullOrEmpty(todate))
                to = Convert.ToDateTime(todate);
            else
                to = from;

            if (CommonManager.CalculationConstant.CompanyHasHourlyLeave != null && CommonManager.CalculationConstant.CompanyHasHourlyLeave.Value)
                return (to.Value - from).Days.ToString();

            List<GetHolidaysForAttendenceResult> holiday = new HolidayManager()
                .GetMonthsHolidaysForAttendence(0, 0, SessionManager.CurrentCompanyId, 0, from, to,empId).ToList();


            LeaveAttendanceManager.GetLeaveDetail(false, false, from, to.Value, leaveTypeId1
                , null, null, holiday, 0, false, empId.Value, ref daysCount);

            return daysCount.ToString();
        }

        public void btnApprove_Click(object sender, DirectEventArgs e)
        {
            RowSelectionModel sm = this.gridPendingRequest.SelectionModel.Primary as RowSelectionModel;

            if (sm.SelectedRows.Count == 0)
                return;

            List<int> leaveRequestIDList = new List<int>();

            foreach (SelectedRow row in sm.SelectedRows)
                leaveRequestIDList.Add(int.Parse(row.RecordID));

            int count = 0;
            ResponseStatus status = LeaveAttendanceManager.ApproveSelectedLeaveRequests(leaveRequestIDList, ref count);

            if (status.IsSuccessType)
            {
                X.MessageBox.Show(
                 new MessageBoxConfig
                 {
                     Message = count + " leave(s) have been approved.",
                     Buttons = MessageBox.Button.OK,
                     Title = "Message",
                     Icon = MessageBox.Icon.INFO,
                     MinWidth = 300,
                     Handler = "reloadGridsAfterApproval();"
                 });
            }
            else
            {
                X.MessageBox.Show(
                 new MessageBoxConfig
                 {
                     Message = status.ErrorMessage,
                     Buttons = MessageBox.Button.OK,
                     Title = "Warning",
                     Icon = MessageBox.Icon.WARNING,
                     MinWidth = 300
                 });
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (!X.IsAjaxRequest)
            {
                //if emp allow to assign leave
                // Change Days or Hours column name
                if (CommonManager.CalculationConstant.CompanyHasHourlyLeave.Value)
                {
                    ColumnDaysorHours1.Text = "Hours";
                    ColumnDaysorHours2.Text = "Hours";
                }
                else
                {
                    ColumnDaysorHours1.Text = "Days";
                    ColumnDaysorHours2.Text = "Days";
                }

                // check if Employee has Approval authority
                if (User.Identity.IsAuthenticated &&  !LeaveAttendanceManager.IsValidEmployeeForApproval())
                {
                    Response.Redirect(FormsAuthentication.LoginUrl);
                    return;
                }

                Initialise();
                DateTime date = BLL.BaseBiz.GetCurrentDateAndTime();
                hdFromDateEng.Text = date.ToShortDateString();
                hdCompanyId.Text = SessionManager.CurrentCompanyId.ToString();


                List<Branch> branchList = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId);
                cmbBranch.Store[0].DataSource = branchList;
                cmbBranch.Store[0].DataBind();

                if (branchList.Count <= 1)
                    cmbBranch.Hide();


                if (CommonManager.CompanySetting.WhichCompany == WhichCompany.NIBL)
                {
                    btnApprove.Visible = true;
                    cboLeaveStatus.Items.RemoveAt(2);
                }
                else if (CommonManager.CompanySetting.WhichCompany == WhichCompany.Civil)
                    btnApprove.Visible = true;
            }

         
        }

        //void DisableButtonsForCCIfNotAllowToApprove()
        //{
        //    bool isAllowedToApprove = LeaveAttendanceManager.IsAllowedToApproveLeave(SessionManager.CurrentLoggedInEmployeeId);
        //    if (!isAllowedToApprove)
        //    {
        //        //btnApprove.Hide();
        //        //btnDeny.Hide();
        //        //btnReDraft.Hide();
        //        gridLeaveBalance.ColumnModel.Columns.RemoveAt(gridLeaveBalance.ColumnModel.Columns.Count - 1);
        //    }
        //}

        private void Initialise()
        {
            //DisableButtonsForCCIfNotAllowToApprove();

            storeEmployee1.DataSource = LeaveAttendanceManager.GetEmployeeListForLeaveAssign();
            storeEmployee1.DataBind();

            //set allowable dates in the Date Picker
            PayrollPeriod payrollPeriod = CommonManager.GetLastPayrollPeriod();
            if (payrollPeriod != null)
            {
                DateTime date = BLL.BaseBiz.GetCurrentDateAndTime();
                this.AssignLeave.SetStartDate = date.AddDays(CommonManager.CompanySetting.LeaveApprovalMinimumPastDays * -1).ToShortDateString();  //payrollPeriod.StartDateEng.Value.ToShortDateString();
                DatePicker1.MinDate = date.AddDays(CommonManager.CompanySetting.LeaveApprovalMinimumPastDays * -1);

                this.AssignLeave.SetMinDate(DatePicker1.MinDate);

                // Next payroll is determied by "LeaveRequestValidMonths" setting value
                if (CommonManager.CompanySetting.LeaveRequestValidMonths > 0)
                {
                    DateTime maxDate = date.AddMonths(
                        CommonManager.CompanySetting.LeaveRequestValidMonths);
                        //LeaveRequestManager.GetMonthsFromStartDate(payrollPeriod.StartDateEng.Value,
                        //CommonManager.CompanySetting.LeaveRequestValidMonths);

                    this.AssignLeave.SetEndDate = maxDate.ToShortDateString();
                    DatePicker1.MaxDate = maxDate;
                    this.AssignLeave.SetMaxDate(maxDate);
                }

            }
            else
            {
                DatePicker1.Disabled = true;
            }

                     


            //X.ResourceManager.RegisterClientStyleBlock(
            RegisterLegendColors();
            RegisterLeaveCalenderColorStyles();

       

            List<LLeaveType> leaves = LeaveAttendanceManager.GetAllLeaves(SessionManager.CurrentCompanyId);
            LLeaveType unpaidLeave = new LLeaveType();
            unpaidLeave.LeaveTypeId = -1;
            unpaidLeave.IsHalfDayAllowed = true;
            unpaidLeave.Title = Resources.Messages.UnpaidLeaveTitleForLeaveRequest;
            leaves.Add(unpaidLeave);

            storeLeaves.DataSource = leaves;
            storeLeaves.DataBind();



            //gridLeaveBalance.Store[0].DataSource = LeaveAttendanceManager.GetEmployeeLeaveBalance(SessionManager.CurrentLoggedInEmployeeId,false);
            //gridLeaveBalance.Store[0].DataBind();
        }


        void RegisterLegendColors()
        {
            StringBuilder str = new StringBuilder("\nlegends = new Array();");
          
            //add holiday legend color also
            str.AppendFormat("legends['{0}'] = '{1}';", HolidaysConstant.National_Holiday, HolidaysConstant.GetColor(HolidaysConstant.National_Holiday));
            str.AppendFormat("legends['{0}'] = '{1}';", HolidaysConstant.Caste_Holiday, HolidaysConstant.GetColor(HolidaysConstant.Caste_Holiday));
            str.AppendFormat("legends['{0}'] = '{1}';", HolidaysConstant.Weekly_Holiday, HolidaysConstant.GetColor(HolidaysConstant.Weekly_Holiday));
            str.AppendFormat("legends['{0}'] = '{1}';", HolidaysConstant.HalfWeekly_Holiday, HolidaysConstant.GetColor(HolidaysConstant.HalfWeekly_Holiday));
            str.AppendFormat("legends['{0}'] = '{1}';", HolidaysConstant.Female_Holiday, HolidaysConstant.GetColor(HolidaysConstant.Female_Holiday));

            X.ResourceManager.RegisterBeforeClientInitScript(str.ToString());

            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "LegendColorReg123", str.ToString(), true);
        }

        void RegisterLeaveCalenderColorStyles()
        {
            StringBuilder str = new StringBuilder();

            List<LLeaveType> leaveList = LeaveAttendanceManager.GetLeaveListByCompany(SessionManager.CurrentCompanyId);

   

            foreach (LLeaveType leave in leaveList)
            {
                LLeaveType leaveType = mgr.GetLeaveType(leave.LeaveTypeId);

                if (leaveType != null)
                {
                    str.Append(GetLeaveCalendarStyle(leaveType.LeaveTypeId, leaveType.LegendColor));
                    CalendarModel  g = new CalendarModel ();
                    g.CalendarId = leaveType.LeaveTypeId;
                    g.Title = leaveType.Title;

                    CalendarPanel1.CalendarStore.Add(g);
                }
            }



            //add Unpaid leave
            CalendarModel unpaidLeaveGroup = new CalendarModel();
            unpaidLeaveGroup.CalendarId = -1;
            unpaidLeaveGroup.Title = Resources.Messages.UnpaidLeaveTitleForLeaveRequest;
            CalendarPanel1.CalendarStore.Add(unpaidLeaveGroup);
            str.Append(GetLeaveCalendarStyle(-1, Config.UPLColor));

            X.ResourceManager.RegisterClientStyleBlock("CalendarStyle", str.ToString());


        }


        /// <summary>
        /// Custom style for each group store item for the calendar
        /// </summary>
        /// <param name="leave"></param>
        /// <returns></returns>
        public string GetLeaveCalendarStyle(int id, string color)
        {
            //http://forums.ext.net/showthread.php?10566-CLOSED-CalendarPanel-GroupStore-and-CalendarId
            string style =
                @"
                    
            .ext-color-{0},
            .ext-ie .ext-color-{0}-ad,
            .ext-opera .ext-color-{0}-ad {{
                color: {1};
            }}
            .ext-cal-day-col .ext-color-{0},
            .ext-dd-drag-proxy .ext-color-{0},
            .ext-color-{0}-ad,
            .ext-color-{0}-ad .ext-cal-evm,
            .ext-color-{0} .ext-cal-picker-icon,
            .ext-color-{0}-x dl,
            .ext-color-{0}-x .ext-cal-evb {{
                background: {1};
            }}
            .ext-color-{0}-x .ext-cal-evb,
            .ext-color-{0}-x dl {{
                border-color: #7C3939;
            }}
                ";

            return string.Format(style, id, color);
        }


        //protected void btnApprove_Click(object sender, DirectEventArgs e)
        //{
        //    X.ResourceManager.RegisterClientScriptBlock("two2", "DisableControls();");
        //    string json = e.ExtraParams["Values"];
        //    Dictionary<string, string>[] companies = JSON.Deserialize<Dictionary<string, string>[]>(json);
        //    if (companies.Count().Equals(0)) return;
        //    Dictionary<string, string> row = companies[0];
          
        //    this.LA1.LeaveRequestId = int.Parse(row["LeaveRequestId"]);
        //    this.LA1.LoadData(row);
        //    this.LA1.ButtonDeny.Hide();
        //    this.LA1.ButtonReDraft.Hide();
        //    this.LA1.ButtonApprove.Show();
        //    this.LA1.EnableControl();
        //    this.LA1.DisableControls();
        //    this.LA1.ApprovalWindow.Center();
        //    this.LA1.ApprovalWindow.Title = "Approve Leave";
        //    this.LA1.ApprovalWindow.Show();
        //    X.ResourceManager.RegisterClientScriptBlock("one1", "DisableControls();");
        //}

        //protected void btnDeny_Click(object sender, DirectEventArgs e)
        //{
        //    X.ResourceManager.RegisterClientScriptBlock("two2", "DisableControls();");
        //    string json = e.ExtraParams["Values"];
        //    Dictionary<string, string>[] companies = JSON.Deserialize<Dictionary<string, string>[]>(json);
        //    if (companies.Count().Equals(0)) return;
        //    Dictionary<string, string> row = companies[0];

        //    this.LA1.LeaveRequestId = int.Parse(row["LeaveRequestId"]);
        //    this.LA1.LoadData(row);
        //    this.LA1.ButtonDeny.Show();
        //    this.LA1.ButtonReDraft.Hide();
        //    this.LA1.ButtonApprove.Hide();
        //    this.LA1.EnableControl();
        //    this.LA1.DisableControls();
        //    this.LA1.ApprovalWindow.Title = "Deny Leave";
        //    this.LA1.ApprovalWindow.Center();
        //    this.LA1.ApprovalWindow.Show();
           
        //}

        //protected void btnReDraft_Click(object sender, DirectEventArgs e)
        //{
        //    X.ResourceManager.RegisterClientScriptBlock("three3", "DisableControls();");
        //    string json = e.ExtraParams["Values"];
        //    Dictionary<string, string>[] companies = JSON.Deserialize<Dictionary<string, string>[]>(json);
        //    if (companies.Count().Equals(0)) return;
        //    Dictionary<string, string> row = companies[0];

        //    this.LA1.LeaveRequestId = int.Parse(row["LeaveRequestId"]);
        //    this.LA1.LoadData(row);
        //    this.LA1.ButtonDeny.Hide();
        //    this.LA1.ButtonReDraft.Show();
        //    this.LA1.ButtonApprove.Hide();
        //    this.LA1.EnableControl();
        //    this.LA1.DisableControls();
        //    this.LA1.ApprovalWindow.Title = "ReDraft Leave";
        //    this.LA1.ApprovalWindow.Center();
        //    this.LA1.ApprovalWindow.Show();
            
        //}

        protected void lnkAssignLeave_Click(object sender, DirectEventArgs e)
        {
            //this.AssignLeave.CurrentDate = hdFromDateEng.Text;
            this.AssignLeave.ClearField();
            this.AssignLeave.AssignLeaveWindow.Center();
            this.AssignLeave.AssignLeaveWindow.Show();
            this.AssignLeave.AssignLeaveWindow.Title = "Assign Leave";
            // this.AssignLeave.LoadData(true);
            //this.AssignLeave.Initialize();



            this.AssignLeave.ShowForAssignLeave();


            AssignLeave.LoadCombo(0, "");

        }

        public void btn_Click(object sender, DirectEventArgs e)
        {
            ClickMore(HiddenLeaveRequestId.Text.Trim());
        }
        [DirectMethod]
        public void ClickMore(string LeaveRequestId)
        {
            int leaveId = 0;

            if (int.TryParse(LeaveRequestId, out leaveId) == false)
                return;

            if (leaveId == 0)
                return;

             DAL.LeaveRequest leave = LeaveAttendanceManager.GetLeaveRequestById(leaveId);
            if (leave != null)
            {
                if (LeaveRequestManager.CanCurrentEmployeeChangeLeaveRequestStatus(SessionManager.CurrentLoggedInEmployeeId, leave) == false)
                {
                    X.MessageBox.Show(
                   new MessageBoxConfig
                   {
                       Message = "Not enough permission to view the leave details.",
                       Buttons = MessageBox.Button.OK,
                       Title = "Warning",
                       Icon = MessageBox.Icon.WARNING,
                       MinWidth = 300
                   });
                    return;
                }
            }

           


            this.AssignLeave.ClearField();
            if (this.AssignLeave.LoadSelectedData(leaveId) == false)
            {
                X.MessageBox.Show(
                 new MessageBoxConfig
                 {
                     Message = "Leave is disabled for this employee,can not be changed.",
                     Buttons = MessageBox.Button.OK,
                     Title = "Warning",
                     Icon = MessageBox.Icon.WARNING,
                     MinWidth = 300
                 });

                Ext.Net.Mask m = new Mask();
                m.Hide();
                return;
            }


            this.AssignLeave.AssignLeaveWindow.Center();
            this.AssignLeave.AssignLeaveWindow.Show();
            //  this.AssignLeave.LoadData(false);
            

        }

        [DirectMethod]
        public void setDate(string date)
        {
            hdFromDateEng.Text = Convert.ToDateTime(date).ToShortDateString();
        }

        
    }
}
