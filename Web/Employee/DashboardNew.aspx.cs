﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL;
using BLL.Manager;
using Web.ReportDataSetTableAdapters;
using Web.Master;
using BLL;
using Web.CP.Report.Templates.Pay.Detail;
using Utils;
using BLL.Base;
using System.IO;
using Utils.Calendar;
using BLL.BO;
using Ext.Net;
using System.Text;
using System.Xml;

namespace Web.Employee
{
    public partial class DashboardNew : BasePage
    {

        public string EmpSearchNavigation()
        {
            return ResolveUrl("~/Employee/HR/EmployeeSearchDetail.aspx");
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            //if (CommonManager.CompanySetting.WhichCompany == WhichCompany.AceTravels)
            //{
            //    Response.Redirect("~/Employee/EmployeeAAA/PersonalDetails.aspx");
            //}

            if (!IsPostBack && ! X.IsAjaxRequest)
            {
                Initialise();
                RegisterLegendColors();
                LoadDashBoardMenus();
                ResourceManager1.RegisterIcon(Icon.Information);

                CheckPwdChangedBeforePwdChangingDays();
                ShowPasswordChangedMessage();

            }            

        }


        private void LoadDashBoardMenus()
        {

            XmlDocument doc = GetEmployeeDashDoardMenuXml();

            //node.AppendChild(newNode);

            XmlNodeList rootNodes =
             (((System.Xml.XmlDocument)((System.Xml.XmlNode)(doc))).ChildNodes);
            rootNodes = rootNodes[1].ChildNodes;

            List<TextValue> _Menus = new List<TextValue>();
            for (int i = 0; i < rootNodes.Count; i++)
            {

                XmlElement rootMenu = rootNodes[i] as XmlElement;

                //if root menu item selected
                if (rootMenu.Attributes["url"] != null)
                {
                    TextValue _TextValue = new TextValue();

                    string txtValue = "<a style='display:block' href=" + rootMenu.Attributes["url"].Value.ToString()+ ">" + rootMenu.Attributes["title"].Value.ToString()+  "</a>";
                    _TextValue.Text = (txtValue);
                    _Menus.Add(_TextValue);
                }
            }

            DashboardMnuRepeater.DataSource = _Menus;
            DashboardMnuRepeater.DataBind();


            //if (SessionManager.CurrentLoggedInEmployeeId != 0)
            //{
            //    //load root menus
            //    //DashboardMnuRepeater.DataSource = rootNodes;
            //    //DashboardMnuRepeater.DataBind();
            //}
        }


        public static XmlDocument GetEmployeeDashDoardMenuXml()
        {
            if (MyCache.GetFromGlobalCache("EmployeeDashBoardMenu") != null)
                return MyCache.GetFromGlobalCache("EmployeeDashBoardMenu") as XmlDocument;

            XmlDocument doc = new XmlDocument();
            doc.Load(HttpContext.Current.Server.MapPath(CommonManager.GetEmployeeMenuDashBoardFileName()));
            MyCache.SaveToGlobalCache("EmployeeDashBoardMenu", doc, MyCache.ExpirationType.Sliding, MyCache.veryLongTime);

            return doc;
        }


        void SetDate()
        {
            try
            {
                DateTime date = BLL.BaseBiz.GetCurrentDateAndTime();
                //set dates
                lblDay.Text = date.DayOfWeek.ToString();

                lblEngDate.Text = date.ToString("dd MMMM, yyyy");


                //CultureInfo culture = CultureInfo.GetCultureInfo("ne-NP");
                CustomDate nepalidate = DateManager.GetTodayNepaliDate();

                string[] yearNos = new string[4];
                yearNos[0] = nepalidate.Year.ToString()[0].ToString();
                yearNos[1] = nepalidate.Year.ToString()[1].ToString();
                yearNos[2] = nepalidate.Year.ToString()[2].ToString();
                yearNos[3] = nepalidate.Year.ToString()[3].ToString();

                string[] dayNos = new string[2];
                dayNos[0] = "";
                dayNos[1] = "";


                dayNos[0] = nepalidate.Day.ToString()[0].ToString();

                lblNepDate.Text = Resources.NepaliCalendar.ResourceManager.GetString("N" + dayNos[0].ToString());
                if (nepalidate.Day.ToString().Length >= 2)
                {
                    dayNos[1] = nepalidate.Day.ToString()[1].ToString();
                    lblNepDate.Text += Resources.NepaliCalendar.ResourceManager.GetString("N" + dayNos[1].ToString());
                }

                lblNepDate.Text += " " + Resources.NepaliCalendar.ResourceManager.GetString(DateHelper.GetMonthName(nepalidate.Month, false))
                    + ", " +

                    Resources.NepaliCalendar.ResourceManager.GetString("N" + yearNos[0]) +
                    Resources.NepaliCalendar.ResourceManager.GetString("N" + yearNos[1]) +
                    Resources.NepaliCalendar.ResourceManager.GetString("N" + yearNos[2]) +
                    Resources.NepaliCalendar.ResourceManager.GetString("N" + yearNos[3])
                    ;
            }
            catch (Exception exp1) { }
        }

        void BindLeaveList(int employeeId)
        {
            PayrollPeriod lastPayroll = CommonManager.GetLastPayrollPeriod();

            List<TextValue> leaveList = new List<TextValue>();

            string days = "days";
            if (CommonManager.CalculationConstant.CompanyHasHourlyLeave.Value)
                days = "hours";

            if (lastPayroll != null)
            {
                List<GetLeaveListAsPerEmployeeResult> list = LeaveAttendanceManager
                    .GetAllLeavesForLeaveRequest(employeeId, lastPayroll.PayrollPeriodId, true)//.Where(x => x.IsParentGroupLeave == false)
                    .ToList();

                // combine child in to parent
                //foreach (GetLeaveListAsPerEmployeeResult item in list)
                for (int i = 0; i < list.Count; i++)
                {
                    GetLeaveListAsPerEmployeeResult item = list[i];
                    if (item.IsParentGroupLeave)
                    {
                        for (int j = 0; j < list.Count; j++)
                        {
                            if (list[j].ParentLeaveTypeId != null && list[j].ParentLeaveTypeId == item.LeaveTypeId)
                            {
                                if (item.NewBalance == null)
                                    item.NewBalance = 0;
                                if (list[j].NewBalance != null)
                                    item.NewBalance += list[j].NewBalance;
                            }
                        }
                    }
                }

                list = list.Where(x => x.IsChildLeave == false).OrderBy(x => x.Title).ToList();

                //  List<GetEmployeeLeaveBalanceResult> balanceList = LeaveAttendanceManager.GetEmployeeLeaveBalance(employeeId);
                foreach (GetLeaveListAsPerEmployeeResult balance in list)
                {
                    if (balance.FreqOfAccrual != LeaveAccrue.COMPENSATORY)
                    {
                        TextValue leave = new TextValue();
                        leave.Text

                            = string.Format("{0}", balance.Title);
                        leave.Value = balance.NewBalance.ToString();

                        leaveList.Add(leave);
                    }
                }

            }
            rptLeaveList.DataSource = leaveList;
            rptLeaveList.DataBind();
        }


        

        void RegisterLegendColors()
        {
            StringBuilder str = new StringBuilder("\nlegends = new Array();");

            //add holiday legend color also
            str.AppendFormat("legends['{0}'] = '{1}';", HolidaysConstant.National_Holiday, HolidaysConstant.GetColor(HolidaysConstant.National_Holiday));
            str.AppendFormat("legends['{0}'] = '{1}';", HolidaysConstant.Caste_Holiday, HolidaysConstant.GetColor(HolidaysConstant.Caste_Holiday));
            str.AppendFormat("legends['{0}'] = '{1}';", HolidaysConstant.Weekly_Holiday, HolidaysConstant.GetColor(HolidaysConstant.Weekly_Holiday));
            str.AppendFormat("legends['{0}'] = '{1}';", HolidaysConstant.Female_Holiday, HolidaysConstant.GetColor(HolidaysConstant.Female_Holiday));

            X.ResourceManager.RegisterBeforeClientInitScript(str.ToString());

            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "LegendColorReg123", str.ToString(), true);
        }

        void BindOtherRequestedLeave(int employeeId)
        {


            //DateTime date = BLL.BaseBiz.GetCurrentDateAndTime();
            //DateTime minDate = date.AddDays(CommonManager.CompanySetting.LeaveApprovalMinimumPastDays * -1);

            


           
            List<NoticeBoard> noticeBoard = new List<NoticeBoard>();
            noticeBoard = NoticeManager.getAllNotice((int)NoticeBoardStatusEnum.Published);
            noticeBoard = noticeBoard
                            .OrderBy(x => x.PublishDate)
                            .Where(y => y.ExpiryDate == null || y.ExpiryDate >= CommonManager.GetCurrentDateAndTime())
                            .Take(7)
                            .ToList();
            noticeBoard = noticeBoard.Take(7).ToList();
            storeNoticeBoard.DataSource = noticeBoard;
            storeNoticeBoard.DataBind();

            if (noticeBoard.Count <= 0)
                gridNoticeBoard.Hide();
        }
        private void Initialise()
        {


            /*
            HolidayManager holidayMgr = new HolidayManager();
            storeMYTimesheet.DataSource = holidayMgr.GetHolidayList();
            storeMYTimesheet.DataBind();
            */

            List<AttendanceReportResult> atteList = new List<AttendanceReportResult>();
            atteList = AttendanceManager.GetAttendanceOfDateRange(SessionManager.CurrentLoggedInEmployeeId, 0, DateTime.Now.Date.AddDays(-6), DateTime.Now.Date, 0
                ,0,99999);

            foreach (var item in atteList)
            {
                DateTime date = item.DateEng.Value;
                if (date.Date == CommonManager.GetCurrentDateAndTime().Date)
                {
                    item.ActualDate = "Today";
                }
                else
                {
                    if (date != null)
                        item.ActualDate = date.ToString("MMM d");//BLL.BaseBiz.GetAppropriateDate(date);
                }
                //if (item.InTime != null)
                //    item.RefinedInRemarks = item.InTime.Value.ToShortTimeString();
                //if (item.OutTime != null)
                //    item.RefinedOutRemarks = item.OutTime.Value.ToShortTimeString();
            }
            gvwWeekTimes.DataSource = atteList;
            gvwWeekTimes.DataBind();

            try
            {

                DateTime todayDate = new DateTime();
                todayDate = CommonManager.GetCurrentDateAndTime();
                HolidayClass service = new HolidayClass();
                //LeaveRequestService service = new LeaveRequestService();
                List<GetHolidaysForAttendenceResult> bindList = new List<GetHolidaysForAttendenceResult>();

                //DateTime FromDate = new DateTime();
                DateTime ToDate = new DateTime();
                DateTime TodayDate = new DateTime();

                TodayDate = CommonManager.GetCurrentDateAndTime();

                //FromDate = new DateTime(todayDate.Year, 1, 1);
                ToDate = TodayDate.AddDays(32);

                bindList = service.GetHolidays(TodayDate, ToDate);
                bindList = bindList.Where(x => x.IsWeekly != true && x.Type != 0).ToList();


                //List<NoticeBoard> noticeBoard = new List<NoticeBoard>();
                //noticeBoard = NoticeManager.getAllNotice((int)NoticeBoardStatusEnum.Published);
                //noticeBoard = noticeBoard
                //                .OrderBy(x => x.PublishDate)
                //                .Where(y => y.ExpiryDate == null || y.ExpiryDate >= CommonManager.GetCurrentDateAndTime())
                //                .Take(7)
                //                .ToList();
                ////noticeBoard = noticeBoard.Take(7).ToList();
                //storeNoticeBoard.DataSource = noticeBoard;
                //storeNoticeBoard.DataBind();

                //if (noticeBoard.Count <= 0)
                //    gridNoticeBoard.Hide();


                List<GetHolidaysForAttendenceResult> list = new List<GetHolidaysForAttendenceResult>();

                foreach (var item in bindList)
                {
                    if (item.DateEng != null)
                    {
                        item.EngDateLongForDashboard = item.DateEng.Value.ToString("ddd dd MMM yyyy");
                        int days = (item.DateEng.Value.Date - CommonManager.GetCurrentDateAndTime().Date).Days;
                        if (days < 0)
                            continue;

                        list.Add(item);

                        item.DaysLeftForDashboard = "In " + days.ToString() + " Days";
                        item.NepDateLongForDashboard = item.DateEngText;

                        CustomDate nepalidate = new CustomDate(item.DateEng.Value.Day, item.DateEng.Value.Month, item.DateEng.Value.Year, true); //DateManager.GetTodayNepaliDate();
                        nepalidate = CustomDate.ConvertEngToNep(nepalidate);

                        string[] yearNos = new string[4];
                        yearNos[0] = nepalidate.Year.ToString()[0].ToString();
                        yearNos[1] = nepalidate.Year.ToString()[1].ToString();
                        yearNos[2] = nepalidate.Year.ToString()[2].ToString();
                        yearNos[3] = nepalidate.Year.ToString()[3].ToString();

                        string[] dayNos = new string[2];
                        dayNos[0] = "";
                        dayNos[1] = "";


                        dayNos[0] = nepalidate.Day.ToString()[0].ToString();

                        item.NepDateLongForDashboard = Resources.NepaliCalendar.ResourceManager.GetString("N" + dayNos[0].ToString());
                        if (nepalidate.Day.ToString().Length >= 2)
                        {
                            dayNos[1] = nepalidate.Day.ToString()[1].ToString();
                            item.NepDateLongForDashboard += Resources.NepaliCalendar.ResourceManager.GetString("N" + dayNos[1].ToString());
                        }

                        item.NepDateLongForDashboard += " " + Resources.NepaliCalendar.ResourceManager.GetString(DateHelper.GetMonthName(nepalidate.Month, false))
                            + ", " +

                            Resources.NepaliCalendar.ResourceManager.GetString("N" + yearNos[0]) +
                            Resources.NepaliCalendar.ResourceManager.GetString("N" + yearNos[1]) +
                            Resources.NepaliCalendar.ResourceManager.GetString("N" + yearNos[2]) +
                            Resources.NepaliCalendar.ResourceManager.GetString("N" + yearNos[3])
                            ;

                    }
                }


                storeMYTimesheet.DataSource = list;
                storeMYTimesheet.DataBind();


            }
            catch (Exception exp)
            {
                Log.log("Employee dashboard error", exp);
            }

            CommonManager mgr = new CommonManager();

            int empId = SessionManager.CurrentLoggedInEmployeeId;
            //EEmployee emp = EmployeeManager.GetEmployeeById(empId);

            //if (emp == null)
            //    return;

            bool hasPhoto = false;
            string urlPhoto = EmployeeManager.GetEmployeePhotoThumbnail(empId);

            if (!string.IsNullOrEmpty(urlPhoto))
            {
                image.ImageUrl = Page.ResolveUrl(Config.UploadLocation + "/" + urlPhoto);
                if (File.Exists(Server.MapPath(image.ImageUrl)))
                    hasPhoto = true;
            }
            
            if (!hasPhoto)
            {
                image.ImageUrl = Page.ResolveUrl(Web.Helper.WebHelper.GetEmployeeNoImage(empId));

            }


            

            welcome.InnerHtml =
                string.Format(welcome.InnerHtml, EmployeeManager.GetEmployeeTitleAndFirstName(empId));

            SetDate();

            BindLeaveList(empId);

            BindOtherRequestedLeave(empId);

            DateTime now = DateTime.Now;

            // message
            //rptMessages.DataSource = PayrollMessageManager.GetPayrollMessageByUserName(
            //    SessionManager.User.UserName, 0, 50).Data
            //    .Where(x => (x.IsRead==false || x.DueDateEng>now) 
            //        && x.Subject.Contains("Leave Request From") == false &&
            //        x.Subject.Contains("Leave Approved By") == false)
            //        .OrderByDescending(x => x.ReceivedDateEng).ToList();                
            //rptMessages.DataBind();

            // This Week
            rptThisWeek.DataSource = DashboardManager.GetEmployeeThisWeekList();
            rptThisWeek.DataBind();

            // Next 30 days
            // This Week
            rpt30Days.DataSource = DashboardManager.Next30DaysAfterThisWeekList();
            rpt30Days.DataBind();

            LoadBlocks();
        }

        //private void BindMessages()
        //{
        //    storeMessages.DataSource = PayrollMessageManager.GetPayrollMessageByUserName(SessionManager.User.UserName, 0, 50).Data;
        //    storeMessages.DataBind();

        //    //X.Js.Call("filterGrid");
        //}

        //protected void ReloadMessageGrid(object s, StoreReadDataEventArgs e)
        //{
        //    BindMessages();
        //}


        public void LoadBlocks()
        {
            LoadOvertime();
            LoadTADA();
            LoadLeave();
            LoadTimeSheet();
            LoadEveningCounter();
            LoadTimeAttendance();
            LoadComment();
        }

        public void LoadOvertime()
        {
            int? pendingForRecommend = 0, recommendForApproval = 0, approvedForForward = 0;

            OvertimeManager.SetOvertimeCount(ref pendingForRecommend, ref recommendForApproval, ref approvedForForward);

            if (pendingForRecommend > 0)
            {
                blockOvertime.Visible = true;

                if (pendingForRecommend > 0)
                {
                    overtimeRecommend.InnerHtml = pendingForRecommend.ToString();
                }
                else
                    overtimeRecommendLi.Visible = false;

            }
            else
                blockOvertime.Visible = false;

        }
        public void LoadEveningCounter()
        {
            int? pendingForRecommend = 0, recommendForApproval = 0, approvedForForward = 0;

            OvertimeManager.SetEveningCounterCount(ref pendingForRecommend, ref recommendForApproval, ref approvedForForward);

            if (pendingForRecommend > 0 || recommendForApproval > 0)
            {
                blockEveningCounter.Visible = true;

                if (pendingForRecommend > 0)
                {
                    eveningCounterRecommneded.InnerHtml = pendingForRecommend.ToString();
                }
                else
                    eveningCounterRecommnededLi.Visible = false;

                if (recommendForApproval > 0)
                {
                    eveningCounterApprove.InnerHtml = recommendForApproval.ToString();
                }
                else
                    eveningCounterApproveLi.Visible = false;
            }
            else
                blockEveningCounter.Visible = false;

        }
        public void LoadTADA()
        {
            int? pendingForRecommend = 0, recommendForApproval = 0, approvedForForward = 0;

            OvertimeManager.SetTADACount(ref pendingForRecommend, ref recommendForApproval, ref approvedForForward);

            if (pendingForRecommend > 0 || recommendForApproval > 0)
            {
                blockTADA.Visible = true;

                if (pendingForRecommend > 0)
                {
                    tadaRecommend.InnerHtml = pendingForRecommend.ToString();
                }
                else
                    tadaRecommendLi.Visible = false;

                if (recommendForApproval > 0)
                {
                    tadaApprove.InnerHtml = recommendForApproval.ToString();
                }
                else
                    tadaApproveLi.Visible = false;
            }

        }



        public void LoadLeave()
        {

            if (CommonManager.CompanySetting.WhichCompany == WhichCompany.NIBL
                && LeaveAttendanceManager.IsEmployeeAllowedToApproveLeave(SessionManager.CurrentLoggedInEmployeeId))
            {
                //List<GetLeaveByDateApprovalResult> list = LeaveAttendanceManager
                //    .GetLeaveByDateApproval(((int)LeaveRequestStatusEnum.Request
                //        + "-" + (int)LeaveRequestStatusEnum.Recommended).ToString(),
                //    SessionManager.CurrentCompanyId, -1, 0, 50, -1, -1, "", "0");

                int count = LeaveAttendanceManager.GetLeaveByDateApprovalCount(((int)LeaveRequestStatusEnum.Request
                        + "-" + (int)LeaveRequestStatusEnum.Recommended).ToString());

                if (count > 0)
                {
                    blockLeave.Visible = true;


                    LeaveRecommend.InnerHtml = count.ToString();


                }
                else
                    blockLeave.Visible = false;
            }
        }


        public void LoadTimeSheet()
        {
            int? pendingForRecommend = 0, recommendForApproval = 0, approvedForForward = 0;

            NewTimeSheetManager.SetTimeSheetCount(ref pendingForRecommend, ref recommendForApproval, ref approvedForForward);

            if (pendingForRecommend > 0 || recommendForApproval > 0)
            {
                blockTimeSheet.Visible = true;

                if (pendingForRecommend > 0)
                {
                    TimesheetRecommend.InnerHtml = pendingForRecommend.ToString();
                }
                else
                    TimesheetRecommendLi.Visible = false;

                if (recommendForApproval > 0)
                {
                    TimesheetApprove.InnerHtml = recommendForApproval.ToString();
                }
                else
                    TimesheetAppproveLi.Visible = false;
            }

        }


        


        private void CheckPwdChangedBeforePwdChangingDays()
        {
            int days = 0;
            if (!IsPwdChangedBeforePwdChangingDays(SessionManager.UserName, ref days))
            {
                Setting objSetting = UserManager.GetSettingForPasswordChangeRule();
                if (objSetting.PasswordChangeType == (int)PasswordChangeTypeEnum.RecommendPwdChange)
                {
                    WPasswordNotification.Show();
                    return;
                }

                //string res = "Password is not changed from the last " + days.ToString() + " days!";
                //ScriptManager.RegisterStartupScript(this, GetType(), "alertMessage", "alert('" + res + "');", true);
                
            }
        }

        private void ShowPasswordChangedMessage()
        {
            if (Session["PwdChanged"] != null && Session["PwdChanged"].ToString() == "1")
            {
                Session["PwdChanged"] = "0";
                NewMessage.ShowNormalPopup("Your Password has been changed.");
                //Page.ClientScript.RegisterStartupScript(this.GetType(), "test", "javascript:ShowMsg();", true);
            }
        }

        public void LoadTimeAttendance()
        {
            // not time requst for approval now
            //List<TimeRequest> list = AttendanceManager.GetTimeRequestsForApprovalById();

            //if (list.Count > 0)
            //{
            //    blockTimeAttendance.Visible = true;
            //    timeAttendRecommended.InnerHtml = list.Count().ToString();
            //}
            //else
            //    blockTimeAttendance.Visible = false;
        }


        public void LoadComment()
        {
            // not time requst for approval now
            List<AttendanceEmpComment> list = AttendanceManager.GetCommentRequest();

            if (list.Count > 0)
            {
                blockComment.Visible = true;
                commentCount.InnerHtml = list.Count().ToString();
            }
            else
                blockComment.Visible = false;
        }
    }
}


