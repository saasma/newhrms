﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using System.Xml;
using Utils.Calendar;

namespace Web.Appraisal
{
    public partial class YourAppraisal : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();
            }
        }

        public void Initialise()
        {

            List<AppraisalPeriod> list = AppraisalManager.GetAllPeriods().ToList();
            
            foreach (var item in list)
            {
                item.Start = string.Format("{0} ({1} {2} - {3} {4})", item.Name, item.StartDate.Value.Year, DateHelper.GetMonthShortName(item.StartDate.Value.Month, true),
                    item.EndDate.Value.Year, DateHelper.GetMonthShortName(item.EndDate.Value.Month, true));
            }

            AppraisalPeriod obj = new AppraisalPeriod() { Start = "All", PeriodId = -1, Closed= true };
            list.Insert(0, obj);
            cmbPeriod.Store[0].DataSource = list;
            cmbPeriod.Store[0].DataBind();
            
            List<AppraisalPeriod> listOpenOnly = list.Where(x => x.Closed == null || x.Closed == false).ToList();
            foreach (var item in listOpenOnly)
                cmbPeriod.SelectedItems.Add(new Ext.Net.ListItem(item.PeriodId.ToString()));

        }
        
        private void LoadLevels()
        {
            PagingToolbar1.DoRefresh();
            //GridYourAppraisals.GetStore().DataSource = AppraisalManager.GetYourAppraisalList();
            //GridYourAppraisals.GetStore().DataBind();
        }


        
        public void ClearLevelFields()
        {
            hiddenValue.Text = "";
            //txtName.Text = "";
            //txtDescription.Text = "";
        }

      
        protected void btnEditLevel_Click(object sender, DirectEventArgs e)
        {

            TARequest request = new TARequest();
            request = TravelAllowanceManager.getRequestByID(int.Parse(hiddenValue.Text));
            if (request.Status == (int)FlowStepEnum.Step2 || request.Status == null)
            {
                Response.Redirect("NewtravelRequest.aspx?id="+request.RequestID.ToString());
            }
            else
            {
                X.Msg.Alert("Warning", "This Request Cant be modifed, its already been forwarded.").Show();
            }

        }


        
 
    }
}

