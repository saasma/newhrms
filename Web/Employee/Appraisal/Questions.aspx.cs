﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using System.Xml;
using BLL.BO;

namespace Web.Appraisal
{
    public partial class Questions : BasePage
    {


        protected override void OnInit(EventArgs e)
        {

            Page.RegisterRequiresControlState(this);
            base.OnPreInit(e);

            //Page.RegisterRequiresControlState(this);
            //base.OnInit(e);
            Initialise();

        }

       

        public int GetFormID()
        {
            if (string.IsNullOrEmpty(Request.QueryString["fid"]))
                return 0;
            return int.Parse(Request.QueryString["fid"]);
        }
        public int GetEmployeeFormID()
        {
            if (string.IsNullOrEmpty(Request.QueryString["efid"]))
                return 0;
            return int.Parse(Request.QueryString["efid"]);
        }


        public void LoadSupervisorFloatingScore(AppraisalEmployeeForm empForm)
        {
            //decimal scoreActivity = Convert.ToDecimal(empForm.SupervisorActivityTotalScore);
         
            //decimal scoreCompetency = Convert.ToDecimal(empForm.SupervisorCompetencyTotalScore);
           

            //decimal scoreQuestion = Convert.ToDecimal(empForm.SupervisorQuestionTotalScore);
            //lblSupervisorQuestion.Text = scoreQuestion.ToString("N2") + "%";
            //if (lblSupervisorQuestion.Text == "0.00%")
            //    lblSupervisorQuestion.Text = "-";

            //decimal totalScore = scoreActivity + scoreCompetency + scoreQuestion;
           
            //string grade, gradeName;
            //AppraisalManager.GetMegaGradeForPercent(totalScore, out grade, out gradeName);
           
            //lblSupervisorGradeName.Text = gradeName;
        }

        public void Initialise()
        {

            CompetencyBlock1.AppraisalFormID = GetFormID();


            int pageSize = 0;

            if (!string.IsNullOrEmpty(Request.Form[ddlRecords.UniqueID]))
                pageSize = int.Parse(Request.Form[ddlRecords.UniqueID]);
            else
                pageSize = int.Parse(ddlRecords.SelectedValue);


      
            List<AppraisalDynamicFormQuestionnaire> questionList = AppraisalManager.GetFormQuestionnaire(GetFormID());
            int totalRecords = questionList.Count;
            int currentPage = _tempCurrentPage - 1;

           
            //pagintCtl.UpdatePagingBar(totalRecords);

           
            //CompetencyBlock1.LoadCompetencyDetails();
            CompetencyBlock1.SetQuestionTitle();
            CompetencyBlock1.LoadQuestionnaireDetails(currentPage, pageSize);


            UpdatePagingBar(totalRecords);


            AppraisalEmployeeForm empForm = AppraisalManager.GetEmployeeAppraisaleForm(GetEmployeeFormID());

            if (empForm != null)
            {
                //if (empForm.Status != (int)AppraisalStatus.SaveAndSend)
                //{
                //    floatingDiv.Visible = false;
                //}
                //else
                //    floatingDiv.Visible = true;

                LoadSupervisorFloatingScore(empForm);
            }

            if (empForm.EmployeeId == SessionManager.CurrentLoggedInEmployeeId)
            {
                if (empForm.Status != (int)AppraisalStatus.Saved)
                {
                    btnSave.Visible = false;
                }
                else
                {

                }
            }
            else
            {

                if (empForm.Status != (int)AppraisalStatus.SaveAndSend)
                {
                    btnSave.Visible = false;
                }
            }


        }

        private void LoadLevels()
        {



        }



        public void ClearLevelFields()
        {


        }



        protected void btnSave_Click(object sender, EventArgs e)
        {

            if (btnSave.Visible)
            {
                List<AppraisalValue> questionList = CompetencyBlock1.GetEmployeeQuestionList();

                AppraisalManager.SaveUpdateEmployeeQuestion(GetEmployeeFormID(),
                    questionList);

                divMsgCtl.InnerHtml = "Appraisal saved.";
                divMsgCtl.Hide = false;
            }

            if (sender == btnNext)
            {
                _tempCurrentPage += 1;
                //_tempCurrentPage = int.Parse(pagintCtl.LabelCurrentPage.Text);
                //_tempCurrentPage += 1;
                CompetencyBlock1.ClearQuestionBlock();
                Initialise();
            }
            if (sender == btnNextOtherPage)
            {
                AppraisalEmployeeForm empForm = AppraisalManager.GetEmployeeAppraisaleForm(GetEmployeeFormID());

                List<AppraisalDynamicFormQuestionnaire> formquestionList = AppraisalManager.GetFormQuestionnaire(GetFormID());
                int totalRecords = formquestionList.Count;
                int totalPages = (int)CalculateTotalPages(totalRecords);

                _tempCurrentPage += 1;

                if (_tempCurrentPage <= totalPages)
                {

                    //_tempCurrentPage = int.Parse(pagintCtl.LabelCurrentPage.Text);
                    //_tempCurrentPage += 1;
                    CompetencyBlock1.ClearQuestionBlock();
                    Initialise();
                }
                else
                {
                    if (empForm.Status == (int)AppraisalStatus.Saved)
                        Response.Redirect(string.Format("EmployeeSignature.aspx?fid={0}&efid={1}",
                        GetFormID(), GetEmployeeFormID()));
                    else
                    {
                        AppraisalForm form = AppraisalManager.getFormInstanceByID(GetFormID());
                        if (form.HideSupervisorReview != null && form.HideSupervisorReview.Value)
                            Response.Redirect(string.Format("EmployeeSignature.aspx?fid={0}&efid={1}",
                            GetFormID(), GetEmployeeFormID()));
                        else
                            Response.Redirect(string.Format("ReviewList.aspx?fid={0}&efid={1}",
                            GetFormID(), GetEmployeeFormID()));
                    }
                }
            }

            AppraisalEmployeeForm empForm1 = AppraisalManager.GetEmployeeAppraisaleForm(GetEmployeeFormID());
            if (empForm1 != null)
                LoadSupervisorFloatingScore(empForm1);
        }


        #region "Paging"
        private int _tempCurrentPage = 1;
        private int _tempCount;

        

        protected override void LoadControlState(object savedState)
        {
            object[] rgState = (object[])savedState;
            base.LoadControlState(rgState[0]);
            _tempCurrentPage = (int)rgState[1];

            CompetencyBlock1.ClearQuestionBlock();
            Initialise();
        }
        protected override object SaveControlState()
        {
            object[] rgState = new object[3];
            rgState[0] = base.SaveControlState();
            rgState[1] = _tempCurrentPage;
            return rgState;
        }



        protected void btnPrevious_Click(object sender, EventArgs e)
         {
             _tempCurrentPage -= 1;
            //_tempCurrentPage = int.Parse(pagintCtl.LabelCurrentPage.Text);
            //_tempCurrentPage -= 1;
            CompetencyBlock1.ClearQuestionBlock();
            Initialise();
        }



        //protected void btnNext_Click(object sender, EventArgs e)
        //{
        //    _tempCurrentPage += 1;
        //    //_tempCurrentPage = int.Parse(pagintCtl.LabelCurrentPage.Text);
        //    //_tempCurrentPage += 1;
        //    CompetencyBlock1.ClearQuestionBlock();
        //    Initialise();
        //}

       

        
        protected void ddlRecords_SelectedIndexChanged(object sender, EventArgs e)
        {

            //SessionManager.EmployeeDisplayRecodsPerPage = int.Parse(pagintCtl.DDLRecords.SelectedValue);
            //_tempCurrentPage = 1;
            _tempCurrentPage = 1;
            CompetencyBlock1.ClearQuestionBlock();
            Initialise();
        }

        protected void ddlPageNumber_SelectedIndexChanged(object sender, EventArgs e)
        {


            _tempCurrentPage = int.Parse(ddlPageNumber.SelectedValue);
            CompetencyBlock1.ClearQuestionBlock();
            Initialise();


        }

        public void EnableDisablePreviousButton(bool enable)
        {
            if (enable)
            {
                btnPrevious.Enabled = true;
                btnPrevious.CssClass = "";
            }
            else
            {
                btnPrevious.Enabled = false;
                btnPrevious.CssClass = "PaginationBarDisableNextPrev";
            }
        }

        public void EnableDisableNextButton(bool enable)
        {
            if (enable)
            {
                btnNext.Enabled = true;
                btnNext.CssClass = "";
            }
            else
            {
                btnNext.Enabled = false;
                btnNext.CssClass = "PaginationBarDisableNextPrev";
            }
        }
        public DropDownList DDLPageNumber
        {
            get
            {
                return ddlPageNumber;
            }
        }
        public void UpdatePagingBar(int totalRecords)
        {
            if (totalRecords <= 0)
            {
                this.Visible = false;
            }
            else
            {
                this.Visible = true;
            }
            int totalPages = (int)CalculateTotalPages(totalRecords);
            lblCurrentPage.Text = _tempCurrentPage.ToString();

            lblTotalPage.Text = (totalPages).ToString();

            this.lblTotalRecords.Text = string.Format(" ({0})", totalRecords);

            // Update Pge numbers
            if (ddlPageNumber.Items.Count != totalPages)
            {
                ddlPageNumber.Items.Clear();
                for (int i = 1; i <= totalPages; i++)
                {
                    ddlPageNumber.Items.Add(i.ToString());
                }
            }
            if (this.ddlPageNumber.Items.FindByValue(_tempCurrentPage.ToString()) != null)
                this.ddlPageNumber.SelectedValue = _tempCurrentPage.ToString();


            if (_tempCurrentPage <= 1)
            {
                this.EnableDisablePreviousButton(false);
            }
            else
            {
                this.EnableDisablePreviousButton(true);
            }

            if (_tempCurrentPage < totalPages)
                this.EnableDisableNextButton(true);
            //pagintCtl.ButtonNext.Enabled = true;
            else
                this.EnableDisableNextButton(false);
        }

        private int CalculateTotalPages(double totalRows)
        {
            int totalPages = (int)Math.Ceiling(totalRows / int.Parse(this.ddlRecords.SelectedValue));

            return totalPages;
        }


        #endregion

    }
}