﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using System.Xml;
using BLL.BO;

namespace Web.Appraisal
{
    public partial class CoreValues : BasePage
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            //if (!IsPostBack)
            {
                Initialise();

            }
        }
        public int GetFormID()
        {
            if (string.IsNullOrEmpty(Request.QueryString["fid"]))
                return 0;
            return int.Parse(Request.QueryString["fid"]);
        }
        public int GetEmployeeFormID()
        {
            if (string.IsNullOrEmpty(Request.QueryString["efid"]))
                return 0;
            return int.Parse(Request.QueryString["efid"]);
        }
        public void Initialise()
        {

            CompetencyBlock1.AppraisalFormID = GetFormID();
            CompetencyBlock1.LoadCompetencyDetails();
            //CompetencyBlock1.LoadQuestionnaireDetails();
            AppraisalEmployeeForm empForm = AppraisalManager.GetEmployeeAppraisaleForm(GetEmployeeFormID());
            //if (empForm != null)
            //{
            //    if (empForm.Status >= (int)AppraisalStatus.SupervisorManagerCommented)
            //        btnSave.Visible = false;
            //}

//            btnNext.OnClientClick = string.Format("window.location='Questions.aspx?fid={0}&efid={1}';return false;",
//GetFormID(), GetEmployeeFormID());

            if (empForm.EmployeeId == SessionManager.CurrentLoggedInEmployeeId)
            {
                if (empForm.Status != (int)AppraisalStatus.Saved)
                {
                    btnSave.Visible = false;
                }
                else
                {
                    
                }
            }
            else
            {
               
                if (empForm.Status != (int)AppraisalStatus.SaveAndSend)
                {
                    btnSave.Visible = false;
                }
            }

        }

        private void LoadLevels()
        {



        }



        public void ClearLevelFields()
        {


        }



        protected void btnSave_Click(object sender, EventArgs e)
        {


            if (btnSave.Visible)
            {
                List<AppraisalValue> competencyList = CompetencyBlock1.GetEmployeeCompetencyList();

                AppraisalManager.SaveUpdateEmployeeCompetency(GetEmployeeFormID(),
                    competencyList);

                divMsgCtl.InnerHtml = "Appraisal saved.";
                divMsgCtl.Hide = false;
            }


            if (sender == btnNext)
            {

                if (AppraisalManager.HasTargetForForm(GetFormID()))
                    Response.Redirect(string.Format("TargetList.aspx?fid={0}&efid={1}",
                        GetFormID(), GetEmployeeFormID()), true);
                else
                    Response.Redirect(string.Format("Questions.aspx?fid={0}&efid={1}",
                        GetFormID(), GetEmployeeFormID()), true);

            }

        }








    }
}