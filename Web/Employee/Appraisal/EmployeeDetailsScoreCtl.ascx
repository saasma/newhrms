﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EmployeeDetailsScoreCtl.ascx.cs"
    Inherits="Web.Employee.Appraisal.EmployeeDetailsScoreCtl" %>
<%@ Register Src="~/NewHR/UserControls/EducationCtl.ascx" TagName="EducationCtl"
    TagPrefix="ucEducation" %>
<style type="text/css">
    .headerOtherRow td
    {
        background-color: lightgray !important;
        background: lightgray !important;
    }
    .table-primary thead th
    {
        background: #47759e;
    }
    .ratingTable td
    {
        font-size: 11px !important;
    }
    .ratingTable strong
    {
        font-size: 11px !important;
        font-weight: bold !important;
    }
    .overridetd thead tr td, .overridetd tbody tr td
    {
        padding: 0px !important;
        padding-left: 5px !important;
        line-height: 2 !important;
    }
    .overridetd tbody tr td span
    {
        line-height: 1 !important;
    }
    .header
    {
        font-weight: bolder;
        color: #1D74A2;
    }
</style>
<hr style="margin: 10px; margin-left: 0px;" />

<table>
    <tr>
        <td style="vertical-align:top">
            <%-- <table style="width: auto" class="ratingTable table table-bordered table-condensed table-striped table-primary">
                <thead>
                    <tr>
                        <th colspan="3" runat="server" id="Th1">
                            Employee Self Rating
                        </th>
                    </tr>
                </thead>
            </table>--%>
            <asp:GridView CssClass="table table-primary mb30 table-bordered table-hover overridetd"
                UseAccessibleHeader="true" ShowHeaderWhenEmpty="True" ID="gridEmployee" runat="server"
                AutoGenerateColumns="False" CellPadding="0" GridLines="None" AllowSorting="false"
                ShowFooterWhenEmpty="False" OnRowCreated="gridEmployee_RowCreated">
                <Columns>
                    <asp:TemplateField HeaderText="Section" HeaderStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <asp:Label ID="lblName" CssClass='<%# Convert.ToBoolean( Eval("IsParent")) ? "header" : "" %>'
                                runat="server" Width="250" Text='<%# Eval("Name") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Points" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <asp:Label ID="lblAssginedTarget" runat="server" Width="60" Text='<%# Convert.ToBoolean(Eval("ShowEmptyInPointColumn").ToString()) ? "" :  Convert.ToBoolean(Eval("IsTextRow").ToString()) ?  Eval("PointsValue"): Eval("Points") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Score" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <asp:Label ID="lblAssginedTarget" runat="server" Width="80" Text='<%# Convert.ToBoolean(Eval("ShowEmptyInScoreColumn").ToString()) ? "" : Convert.ToBoolean(Eval("IsTextRow").ToString()) ?  Eval("PercentageValue"): Eval("Percentage") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </td>
        <td style="padding-left: 25px;vertical-align:top">
           <%-- <table style="width: auto" class="ratingTable table table-bordered table-condensed table-striped table-primary">
                <thead>
                    <tr>
                        <th colspan="3" runat="server" id="Th2">
                            Supervisor Rating
                        </th>
                    </tr>
                </thead>
            </table>--%>
            <asp:GridView CssClass="table table-primary mb30 table-bordered table-hover overridetd"
                UseAccessibleHeader="true" ShowHeaderWhenEmpty="True" ID="gridSupervisor" runat="server"
                AutoGenerateColumns="False" CellPadding="0" GridLines="None" AllowSorting="false"
                ShowFooterWhenEmpty="False" OnRowCreated="gridEmployee_RowCreated">
                <Columns>
                    <asp:TemplateField HeaderText="Section" HeaderStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <asp:Label ID="lblName" CssClass='<%# Convert.ToBoolean( Eval("IsParent")) ? "header" : "" %>'
                                runat="server" Width="250" Text='<%# Eval("Name") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                     <asp:TemplateField HeaderText="Points" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <asp:Label ID="lblAssginedTarget" runat="server" Width="60" Text='<%# Convert.ToBoolean(Eval("ShowEmptyInPointColumn").ToString()) ? "" : Convert.ToBoolean(Eval("IsTextRow").ToString()) ?  Eval("PointsValue"): Eval("Points") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Score" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <asp:Label ID="lblAssginedTarget" runat="server" Width="80" Text='<%# Convert.ToBoolean(Eval("ShowEmptyInScoreColumn").ToString()) ? "" : Convert.ToBoolean(Eval("IsTextRow").ToString()) ?  Eval("PercentageValue"): Eval("Percentage") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </td>
    </tr>
</table>
<hr style="margin: 10px; margin-left: 0px;" />
