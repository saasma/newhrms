﻿<%@ Page Title="Your Appraisal" Language="C#" MasterPageFile="~/Master/EmployeeExt.Master"
    AutoEventWireup="true" CodeBehind="YourAppraisal.aspx.cs" Inherits="Web.Appraisal.YourAppraisal" %>

<%--<%@ Register Src="~/NewHR/UserControls/EmployeeDetailsCtl.ascx" TagName="EmployeeDetailsCtl"
    TagPrefix="uc1" %>--%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">



        var CommandHandler1 = function (command, record) {

            window.location.href = 'EmployeeIntroduction.aspx?fid=' + record.data.AppraisalFormRef_ID + '&efid='
                + record.data.AppraisalEmployeeFormID;


        }



        var totalCost = function (records) {
            var i = 0,
                length = records.length,
                total = 0,
                record;

            for (; i < length; ++i) {
                record = records[i];
                total += record.get('Quantity') * record.get('Rate');
            }

            return total;
        };

        function searchList() {
            <%=PagingToolbar1.ClientID %>.doRefresh();
        }
        
    </script>
    <style>
        
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
    <%--<ext:ResourceManager ID="resourceManager1" runat="server" />--%>
    <div class="contentArea">
        <ext:Hidden runat="server" ID="hiddenValue" />
        <ext:Hidden runat="server" ID="HiddenStatus">
        </ext:Hidden>
        <ext:LinkButton runat="server" Hidden="true" ID="btnEditLevel">
            <DirectEvents>
                <Click OnEvent="btnEditLevel_Click">
                    <EventMask ShowMask="true" />
                </Click>
            </DirectEvents>
        </ext:LinkButton>
        <%--<uc1:EmployeeDetailsCtl Id="EmployeeDetailsCtl1" runat="server" />--%>
        <h4 class="heading">
            Your Review Forms</h4>
        <div>
            
            <table>
                <tr>
                    <td>
                        <ext:MultiCombo Width="400" LabelSeparator="" SelectionMode="All" LabelWidth="50"
                            ID="cmbPeriod" DisplayField="Start" ValueField="PeriodId" LabelAlign="Top"
                            runat="server" FieldLabel="Period">
                            <Store>
                                <ext:Store ID="Store1" runat="server">
                                    <Model>
                                        <ext:Model ID="Model1" IDProperty="PeriodId" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="PeriodId" />
                                                <ext:ModelField Name="Start" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                        </ext:MultiCombo>
                    </td>
                    <td style="padding-top: 20px">
                        <ext:Button runat="server" ID="btnLoad" Cls="btn btn-primary" Text="<i></i>Load" MarginSpec="0 0 0 20"
                            runat="server">
                            <Listeners>
                                <Click Handler="searchList();" />
                            </Listeners>
                        </ext:Button>
                    </td>
                </tr>
            </table>
            

            <ext:GridPanel StyleSpec="margin-top:15px;" ID="GridYourAppraisals" runat="server">
                <Store>
                    <ext:Store ID="Store3" runat="server" AutoLoad="true" PageSize="50">
                        <Proxy>
                            <ext:AjaxProxy Json="true" Url="../../Handler/YourAppraisalHandler.ashx">
                                <ActionMethods Read="GET" />
                                <Reader>
                                    <ext:JsonReader Root="data" TotalProperty="total" />
                                </Reader>
                            </ext:AjaxProxy>
                        </Proxy>
                        <AutoLoadParams>
                            <ext:Parameter Name="start" Value="0" Mode="Raw" />
                        </AutoLoadParams>
                        <Parameters>
                                <ext:StoreParameter Name="PeriodId" Value="#{cmbPeriod}.getValue()" Mode="Raw"
                                    ApplyMode="Always" />
                        </Parameters>
                        <Model>
                            <ext:Model ID="Model4" runat="server" IDProperty="AppraisalEmployeeFormID">
                                <Fields>
                                    <ext:ModelField Name="PeriodName" Type="String" />
                                    <ext:ModelField Name="AppraisalEmployeeFormID" Type="String" />
                                    <ext:ModelField Name="AppraisalFormRef_ID" Type="String" />
                                    <ext:ModelField Name="AppraisalFormName" Type="string" />
                                    <ext:ModelField Name="RolloutDate" Type="Date" />
                                    <ext:ModelField Name="StatusText" Type="string" />
                                    <ext:ModelField Name="DueDate" Type="Date" />
                                    <ext:ModelField Name="FromDate" Type="Date" />
                                    <ext:ModelField Name="ToDate" Type="Date" />
                                    <ext:ModelField Name="DaysRemaining" Type="string" />
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                <ColumnModel>
                    <Columns>
                       <ext:Column ID="Column2" Flex="1" Sortable="false" MenuDisabled="true" runat="server" Text="Period Name"
                            Width="200" Align="Left" DataIndex="PeriodName" />
                        <ext:Column ID="Column5" Flex="1" Sortable="false" MenuDisabled="true" runat="server" Text="Appraisal Name"
                            Width="250" Align="Left" DataIndex="AppraisalFormName" />
                        <ext:DateColumn ID="DateColumn2" runat="server" Text="Due Date" Sortable="true" DataIndex="DueDate"
                            Width="150" Align="Left" Format="MMMM dd, yyyy">
                        </ext:DateColumn>
                        <ext:Column ID="Column1" Sortable="false" MenuDisabled="true" runat="server" Text="Days Remaining"
                            Width="150" Align="Left" DataIndex="DaysRemaining" />
                        <ext:Column ID="Column6" Sortable="false" MenuDisabled="true" runat="server" Text="Status"
                            Width="150" Align="Left" DataIndex="StatusText" />
                        <ext:CommandColumn runat="server" Text="" Align="Center" Width="150">
                            <Commands>
                                <ext:CommandSeparator />
                                <ext:GridCommand Cls="editGridButton" ToolTip-Text="View" Text="View Appraisal"
                                    CommandName="Edit" />
                            </Commands>
                            <Listeners>
                                <Command Handler="CommandHandler1(command,record);" />
                            </Listeners>
                        </ext:CommandColumn>
                    </Columns>
                </ColumnModel>
                <SelectionModel>
                    <ext:RowSelectionModel ID="RowSelectionModel2" runat="server" Mode="Single" />
                </SelectionModel>
                <BottomBar>
                    <ext:PagingToolbar ID="PagingToolbar1" runat="server" PageSize="50" DisplayInfo="true"
                        DisplayMsg="Displaying Requests {0} - {1} of {2}" EmptyMsg="No Records to display">
                        <Items>
                            <ext:ComboBox Hidden="true" runat="server" SelectedIndex="0" Width="80" ID="cmbPageSize">
                                <Items>
                                    <ext:ListItem Text="20" Value="20" />
                                    <ext:ListItem Text="30" Value="30" />
                                    <ext:ListItem Text="50" Value="50" />
                                    <ext:ListItem Text="100" Value="100" />
                                </Items>
                                <Listeners>
                                    <Select Handler="#{PagingToolbar1}.pageSize = parseInt( '50'); searchList();" />
                                </Listeners>
                            </ext:ComboBox>
                        </Items>
                    </ext:PagingToolbar>
                </BottomBar>
            </ext:GridPanel>
            <%--<div class="buttonBlock">
                    <ext:LinkButton runat="server" StyleSpec="padding:0px;" ID="btnAddLevel" Cls="btnFlat"
                        BaseCls="btnFlat" Text="<i></i>Add New Allowance Settings">
                        <DirectEvents>
                            <Click OnEvent="btnAddLevel_Click">
                                <EventMask ShowMask="true" />
                            </Click>
                        </DirectEvents>
                    </ext:LinkButton>
                </div>--%>
        </div>
</asp:Content>
