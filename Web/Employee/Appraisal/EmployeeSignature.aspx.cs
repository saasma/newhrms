﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using System.Xml;
using Utils.Web;
using Utils.Helper;
using System.Web.UI.HtmlControls;

namespace Web.Appraisal
{
    public partial class EmployeeSignature : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest && !IsPostBack )
            {
                Initialise();
                InitialiseCommentBlock();
                ShowCommentHistory();
            }

            JavascriptHelper.AttachNonDialogPopUpCode(Page, "revsertPopup", "RevertImport.aspx", 450, 500);
            btnRevert.OnClientClick = string.Format("return ImportPopup12({0});", GetEmployeeFormID());
            
        }
        public int GetFormID()
        {
            if (string.IsNullOrEmpty(Request.QueryString["fid"]))
                return 0;
            return int.Parse(Request.QueryString["fid"]);
        }
        public int GetEmployeeFormID()
        {
            if (string.IsNullOrEmpty(Request.QueryString["efid"]))
                return 0;
            return int.Parse(Request.QueryString["efid"]);
        }

        public void ShowCommentHistory()
        {
            if (CommonManager.CompanySetting.WhichCompany != WhichCompany.DishHome)
                return;

            AppraisalEmployeeForm empForm = AppraisalManager.GetEmployeeAppraisaleForm(GetEmployeeFormID());
            if (empForm != null && empForm.EmployeeId != SessionManager.CurrentLoggedInEmployeeId)
                return;

            // list for other comment
            List<AppraisalEmployeeSummaryComment> list = AppraisalManager.GetAppraisalCommentList(GetEmployeeFormID());
            string commenter = "";
            foreach (AppraisalEmployeeSummaryComment item in list)
            {
                commenter = item.CommenterDisplayTitle;

                HtmlGenericControl signature = new HtmlGenericControl("span");
                signature.ID = "signature" + item.SummaryCommentId;
                signature.Attributes["class"] = "summaryCommmentSignature";
                signature.Attributes.CssStyle["margin-top"] = "20px";
                signature.InnerHtml = commenter + " Signed on ";
                if (item.CommentedOn != null)
                    signature.InnerHtml += item.CommentedOn.Value.ToString();

                HtmlGenericControl label = new HtmlGenericControl("span");
                label.ID = "label" + item.SummaryCommentId;
                label.Attributes["class"] = "summaryCommmentLabel";


                label.InnerHtml = commenter + "'s Comments";


                HtmlGenericControl div = new HtmlGenericControl("div");
                div.ID = "div" + item.SummaryCommentId;
                div.Attributes["class"] = "summaryCommmentDiv";
                div.InnerHtml = item.Comment;

                commentList.Controls.Add(signature);

                if (item.StatusStepID != (int)FlowStepEnum.Step1SaveAndSend)
                {
                    commentList.Controls.Add(label);
                   

                    commentList.Controls.Add(div);
                }
            }
        }

        public void Initialise()
        {
            ddlRecommendationList.DataSource = AppraisalManager.GetAllRecommendFor();
            ddlRecommendationList.DataBind();

            AppraisalForm form = AppraisalManager.getFormInstanceByID(GetFormID());
            if (form != null)
            {
                // Introudction

                spanIntroductionName.InnerHtml = form.SignationName;
                divIntroductionDescription.InnerHtml = (form.SignationDescription);


            }

            AppraisalEmployeeForm empForm = AppraisalManager.GetEmployeeAppraisaleForm(GetEmployeeFormID());

            GetDocumentNextStepUsingApprovalFlowResult nextStep = null;

            if (empForm != null)
            {
               
                // allow to revert for Department head
                 if (CommonManager.CompanySetting.WhichCompany == WhichCompany.DishHome && empForm.Status == (int)FlowStepEnum.Step3
                     && SessionManager.CurrentLoggedInEmployeeId != empForm.EmployeeId)
                     btnRevert.Visible = true;

                nextStep = CommonManager.GetDocumentNextStep(
                    (int)empForm.Status, FlowTypeEnum.Appraisal, empForm.EmployeeId, empForm.RolloutID.Value, false);

                if (nextStep.ApprovalEmployee1  != null)
                    lblForwardTo.Text = " Forward to " + EmployeeManager.GetEmployeeName(nextStep.ApprovalEmployee1.Value);
            }


            


            // signature
            if (empForm.Status == (int)AppraisalStatus.Saved)
            {
                spanSupervisorSignature.Visible = false;
                spanEmployeeSecondSignature.Visible = false;
                spanEmployeeSignature.InnerHtml = new EmployeeManager().GetById(empForm.EmployeeId).Name + " has not yet signed.";
            }
            else if (empForm.Status == (int)AppraisalStatus.SaveAndSend)
            {

                spanEmployeeSignature.InnerHtml = new EmployeeManager().GetById(empForm.EmployeeId).Name + " Signed on ";
                if (empForm.SelfSubmittedOn != null)
                    spanEmployeeSignature.InnerHtml += empForm.SelfSubmittedOn.Value.ToString();

                spanSupervisorSignature.Visible = true;
                spanSupervisorSignature.InnerHtml = "Supervisor has not yet signed on.";
            }
            else if (empForm.Status == (int)AppraisalStatus.SupervisorManagerCommented)
            {

                spanEmployeeSignature.InnerHtml = new EmployeeManager().GetById(empForm.EmployeeId).Name + " Signed on ";
                if (empForm.SelfSubmittedOn != null)
                    spanEmployeeSignature.InnerHtml += empForm.SelfSubmittedOn.Value.ToString();

                spanSupervisorSignature.Visible = true;
                if (empForm.SupervisorSubmittedOn != null)
                    spanSupervisorSignature.InnerHtml = "Supervisor Signed on " + empForm.SupervisorSubmittedOn.Value.ToString();

                spanEmployeeSecondSignature.Visible = true;
                spanEmployeeSecondSignature.InnerHtml = EmployeeManager.GetEmployeeName(empForm.EmployeeId) + " has not yet signed on.";
            }
            else
            {
                spanEmployeeSignature.InnerHtml = EmployeeManager.GetEmployeeName(empForm.EmployeeId) + " Signed on ";
                if (empForm.SelfSubmittedOn != null)
                    spanEmployeeSignature.InnerHtml += empForm.SelfSubmittedOn.Value.ToString();

                spanSupervisorSignature.Visible = true;
                if (empForm.SupervisorSubmittedOn != null)
                    spanSupervisorSignature.InnerHtml = "Supervisor Signed on " + empForm.SupervisorSubmittedOn.Value.ToString();


                AppraisalEmployeeSummaryComment comment = AppraisalManager.GetAppraisalSelfEmployeeSummaryComment(GetEmployeeFormID());
                spanEmployeeSecondSignature.Visible = true;
                if (empForm.SupervisorReviewAgreed != null)
                {

                    spanEmployeeSecondSignature.InnerHtml = EmployeeManager.GetEmployeeName(empForm.EmployeeId) + " commented ";

                }
                if (empForm.SelfCommentedOn != null)
                    spanEmployeeSecondSignature.InnerHtml += empForm.SelfCommentedOn.Value.ToString();
            }


            if (empForm.EmployeeId == SessionManager.CurrentLoggedInEmployeeId)
            {
                btnSave.Hide();;//.Visible = false;
                txtComments.Visible = false;

                // if not submitted then employee can save submit
                if (empForm.Status == (int)AppraisalStatus.Saved)
                {
                    btnSave.Show();//.Visible = true;
                    txtComments.Visible = true;
                }

                // sometime CurrentApprovalSelectedEmployeeId is null, need to check why and remove below code, currently show employee comment for this step
                if (empForm.CurrentApprovalSelectedEmployeeId == null && empForm.Status == (int)AppraisalStatus.SupervisorManagerCommented)
                {
                    btnSave.Show();//.Visible = true;
                    txtComments.Visible = true;
                    spanEmployeeSecondSignature.Visible = true;
                }

                // if current employee being seeing the form and processing emp is emp
                if (empForm.CurrentApprovalSelectedEmployeeId == SessionManager.CurrentLoggedInEmployeeId)
                {
                    btnSave.Show();//.Visible = true;
                    txtComments.Visible = true;
                    spanEmployeeSecondSignature.Visible = true;
                }

                
            }
            else
            {

                if (empForm.Status != (int)AppraisalStatus.SaveAndSend)
                {
                    btnSave.Hide();//.Visible = false;
                }
            }

        }


        public void InitialiseCommentBlock()
        {


            AppraisalForm form = AppraisalManager.getFormInstanceByID(GetFormID());
            if (form != null)
            {
                // Introudction

                spanIntroductionNameComment.InnerHtml = form.SummaryName;
                divIntroductionDescriptionComment.InnerHtml = (form.SummaryDescription);
                if (string.IsNullOrEmpty(form.SummaryDescription))
                    divIntroductionDescriptionComment.Visible = false;

            }

            AppraisalEmployeeForm empForm = AppraisalManager.GetEmployeeAppraisaleForm(GetEmployeeFormID());

            

            AppraisalEmployeeSummaryComment comment = AppraisalManager.GetAppraisalSelfEmployeeSummaryComment(GetEmployeeFormID());


            spanEmployeeComment.InnerHtml = new EmployeeManager().GetById(empForm.EmployeeId).Name + "'s Comment";
            if (comment != null)
                txtComments.Text = comment.Comment;

            //if (empForm.SupervisorReviewAgreed == null || empForm.SupervisorReviewAgreed == true)
            //    rdbIAgree.Checked = true;
            //else
            //{
            //    rdbIAgree.Checked = false;
            //    rdbIDisAgree.Checked = true;
            //}


            // agree disagree show case after manager submitted only
            if(empForm != null && empForm.Status >= (int)AppraisalStatus.SupervisorManagerCommented &&
                empForm.AppraisalForm != null && empForm.AppraisalForm.ShowAgreeDisAgreeButtons != null && empForm.AppraisalForm.ShowAgreeDisAgreeButtons.Value)
            {
                agreeDisagreeBlock.Visible = true;


                if(empForm.EmployeeId == SessionManager.CurrentLoggedInEmployeeId && empForm.Status == (int)AppraisalStatus.SupervisorManagerCommented)
                {
                    rdbIAgree.Enabled = true;
                    rdbIDisAgree.Enabled = true;
                }
                else
                {
                    rdbIAgree.Enabled = false;
                    rdbIAgree.Enabled = false;
                }

            }


            if (empForm.EmployeeId == SessionManager.CurrentLoggedInEmployeeId)
            {
                if (empForm.Status != (int)AppraisalStatus.SupervisorManagerCommented)
                {
                    //btnSave.Visible = false;
                    txtComments.Visible = false;
                    //rdbIAgree.Enabled = false;
                    //rdbIDisAgree.Enabled = false;

                    if (comment != null)
                    {
                        acitivtySummary.InnerHtml = comment.Comment; ;
                        acitivtySummary.Visible = true;
                    }
                }
                else
                {
                      
                }
            }
            else
            {

                if (empForm.Status == (int)AppraisalStatus.SaveAndSend)
                {
                   // agreeDisagreeBlock.Visible = false;
                    commentBlock.Visible = false;
                    spanEmployeeComment.Visible = false;
                }

                if (comment != null)
                {
                    acitivtySummary.InnerHtml = comment.Comment; ;
                    acitivtySummary.Visible = true;
                }
                txtComments.Visible = false;
               
                //rdbIAgree.Enabled = false;
                //rdbIDisAgree.Enabled = false;
            }

            // I agree/disagree block
            //if (form.HideIAgreeDisAgreeButtons != null && form.HideIAgreeDisAgreeButtons.Value)
            //{
            //    agreeDisagreeBlock.Visible = false;
            //}


            // supervisor comment block
            divSupervisorComments.Visible = false;
            txtSupervisorComments.Text = empForm.SupervisorStep2Comment;
            lblSupervisorComments.InnerHtml = empForm.SupervisorStep2Comment;

            if (empForm.EmployeeId == SessionManager.CurrentLoggedInEmployeeId)
            {
                if (empForm.Status >= (int)AppraisalStatus.SupervisorManagerCommented)
                {
                    divSupervisorComments.Visible = true;
                    txtSupervisorComments.Visible = false;
                    lblSupervisorComments.Visible = true;

                }
            }
            else
            {

                if (empForm.Status == (int)AppraisalStatus.SaveAndSend)
                {

                    //int? additionStep = AppraisalManager.GetAdditionalSteps(empForm.NextStepOrStatusId.Value, FlowTypeEnum.Appraisal, empForm.AppraisalEmployeeFormID);
                    //if (additionStep != null)
                    //{
                    //    AppraisalAdditionalStep stepEnum = (AppraisalAdditionalStep)additionStep.Value;

                    //    if (stepEnum == AppraisalAdditionalStep.RecommendationBlock)
                    //    {
                    //        recommendBlock.Visible = true;
                    //        if (empForm.RecommendForId != null)
                    //            UIHelper.SetSelectedInDropDown(ddlRecommendationList, empForm.RecommendForId.Value);
                    //    }
                    //}

                    
                    divSupervisorComments.Visible = true;
                    txtSupervisorComments.Enabled = true;
                }
                else
                {   
                    divSupervisorComments.Visible = true;
                    txtSupervisorComments.Visible = false;
                    lblSupervisorComments.Visible = true;
                }
            }

            if (empForm.Status == (int)AppraisalStatus.Saved)
            {
                mainCommentBlock.Visible = false;
                return;
            }
            else if (empForm.Status == (int)AppraisalStatus.SaveAndSend)
            {
                divSupervisorComments.Visible = true;
                spanEmployeeComment.Visible = false;
                commentBlock.Visible = false;
                acitivtySummary.Visible = false;
                
            }
        }


        public void btnCalculate_Click(object sender, EventArgs e)
        {

            try
            {

                //AppraisalManager.CalculateMarks(GetEmployeeFormID());

                RatingEmployeeBlock1.LoadScore();

                Initialise();
            }
            catch { }
        }

        public void btnSave_Click(object sender, DirectEventArgs e)
        {

            Status status = new Status();
            AppraisalEmployeeForm empForm = AppraisalManager.GetEmployeeAppraisaleForm(GetEmployeeFormID());

            int recommendForId = 0;
            string strValue = ddlRecommendationList.SelectedValue;
            if (recommendBlock.Visible)
            {
                int.TryParse(strValue, out recommendForId);
            }

            if (empForm.Status == (int)AppraisalStatus.Saved) // if current status is Saved then processs for SavedAndSend
                status = AppraisalManager.SaveEmployeeAppraisalForm(GetEmployeeFormID(), -1, "", false);

            else if (empForm.Status == (int)AppraisalStatus.SaveAndSend) // process for send for Review to self Employee
            {

                AppraisalManager.UpdateStep2SupervisorComment(GetEmployeeFormID(), txtSupervisorComments.Text.Trim(), recommendForId);
                status = AppraisalManager.SaveEmployeeAppraisalForm(GetEmployeeFormID(), recommendForId, txtSupervisorComments.Text.Trim(), false);
            }
            // process for self Employee comment and I agree/disagree saving
            else
            {
                bool? iAgree = null;

                if (empForm != null &&
                    empForm.AppraisalForm != null && empForm.AppraisalForm.ShowAgreeDisAgreeButtons != null && empForm.AppraisalForm.ShowAgreeDisAgreeButtons.Value)
                {
                    iAgree = false;
                    if (rdbIAgree.Checked)
                        iAgree = true;
                }

                AppraisalManager.UpdateEmployeeSelfSummaryComment(GetEmployeeFormID(),
                txtComments.Text.Trim(), iAgree);

                status = AppraisalManager.SaveEmployeeAppraisalForm(GetEmployeeFormID(), recommendForId, txtComments.Text.Trim(), true);
            }

            if (status.IsSuccess)
            {
                //divMsgCtl.InnerHtml = "Appraisal saved and forwarded";
                //divMsgCtl.Hide = false;
                NewMessage.ShowNormalMessage("Appraisal saved and forwarded");
                //RatingEmployeeBlock1.LoadScore();

                btnSave.Hide();
                //Initialise();
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
                //divWarningMsg.InnerHtml = status.ErrorMessage;
                //divWarningMsg.Hide = false;
            }
        }


    }
}