﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using System.Xml;
using BLL.BO;
using Utils.Web;
using System.Web.UI.WebControls;

namespace Web.Employee.Appraisal
{
    public partial class QualificationCtl : System.Web.UI.UserControl
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            //if (!IsPostBack && !X.IsAjaxRequest)
            {
                Initialise();

            }
        }

        public int GetFormID()
        {
            if (string.IsNullOrEmpty(Request.QueryString["fid"]))
                return 0;
            return int.Parse(Request.QueryString["fid"]);
        }
        public int GetEmployeeFormID()
        {
            if (string.IsNullOrEmpty(Request.QueryString["efid"]))
                return 0;
            return int.Parse(Request.QueryString["efid"]);
        }


       

        public void Initialise()
        {




            AppraisalForm form = AppraisalManager.getFormInstanceByID(GetFormID());
            AppraisalEmployeeForm empForm = AppraisalManager.GetEmployeeAppraisaleForm(GetEmployeeFormID());

            if(form.HideHistorySection==true)
            {
                this.Visible = false;
                return;
            }


            if (form != null)
            {

                if (empForm.EducationScore != null)
                {
                    scoreEducation.InnerHtml = "Score : " + empForm.EducationScore;
                }

                if (empForm.LocationScore != null)
                {
                    scoreLocation.InnerHtml = "Score : " + empForm.LocationScore;
                }

                if (empForm.SeniorityScore != null)
                {
                    scoreSeniority.InnerHtml = "Score : " + empForm.SeniorityScore;
                }


                spanIntroductionName.InnerHtml = form.HistoryName;
                divIntroductionDescription.InnerHtml = (form.HistoryDescription);
                int EmpID = empForm.EmployeeId;
                TransferHistory(EmpID);
                WorkHistory(EmpID);




                List<HEducation> _HEducation = NewHRManager.GetEducationByEmployeeID(EmpID).Where(x => x.Status == (int)HRStatusEnum.Approved).ToList();


                this.StoreEducation.DataSource = _HEducation;
                this.StoreEducation.DataBind();

                //btnNext.OnClientClick = string.Format("window.location='CoreValues.aspx?fid={0}&efid={1}';return false;",
                //    GetFormID(), GetEmployeeFormID());
            }







        

        }

      
        private void TransferHistory(int EmpID)
        {
            gridTransferList.Store[0].DataSource = BranchManager.GetTransferListForAppraisal(EmpID);
            gridTransferList.Store[0].DataBind();
        }
        private void WorkHistory(int EmpID)
        {
            List<GetEmployeeDesignationHistoryForAppraisalResult> dataTerminations = BranchManager.GetHistoryForAppraisal(EmpID);

            ServiceHistory.Store[0].DataSource = dataTerminations;
            ServiceHistory.Store[0].DataBind();

            gridLevelGradeHistory.Store[0].DataSource = NewHRManager.GetLevelGradeHistory(EmpID).ToList();
            gridLevelGradeHistory.Store[0].DataBind();
        }



    }
}