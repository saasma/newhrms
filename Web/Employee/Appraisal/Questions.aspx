﻿<%@ Page Title="Employee Appraisal" Language="C#" MasterPageFile="~/Master/EmployeeMS.Master"
    AutoEventWireup="true" CodeBehind="Questions.aspx.cs" Inherits="Web.Appraisal.Questions" %>

<%@ Register Src="~/Controls/PagingCtl.ascx" TagName="PagingCtl" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Appraisal/UserControls/CompetencyBlock.ascx" TagName="CompetencyBlock"
    TagPrefix="uc1" %>
<%@ Register Src="~/Appraisal/UserControls/RatingEmployeeBlock.ascx" TagName="RatingEmployeeBlock"
    TagPrefix="uc2" %>
<%@ Register Src="EmployeeMenu.ascx" TagName="EmployeeMenu" TagPrefix="uc3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../../css/ratingsrc/jquery.rateit.min.js" type="text/javascript"></script>
    <link href="../../css/ratingsrc/rateit.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">


       
        
    </script>
    <style type="text/css">
        .PaginationBar
        {
            margin-left: 0px;
        }
        .ratingTable td
        {
            padding: 2px !important;
        }
        .ratingTable
        {
            border: 1px solid lightgray;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
    <div class="separator bottom" style="padding-bottom: 1px">
    </div>
    <uc2:MsgCtl ID="divMsgCtl" EnableViewState="false" Hide="true" runat="server" />
    <uc2:WarningCtl ID="divWarningMsg" EnableViewState="false" Hide="true" runat="server" />
    <div class="innerLR" style="padding-left: 0px">
        <table>
            <tr>
                <td valign="top" style="padding-top: 16px;">
                    <uc3:EmployeeMenu Id="EmployeeMenu1" runat="server" />
                </td>
                <td style="width: 10px">
                </td>
                <td valign="top" style="width: 1000px">
                    <uc2:RatingEmployeeBlock Id="RatingEmployeeBlock1" runat="server" />
                    <div style="margin-left: 710px">
                        <span class="PaginationFadedText">Show</span>
                        <asp:DropDownList AutoPostBack="true" ID="ddlRecords" runat="server" OnSelectedIndexChanged="ddlRecords_SelectedIndexChanged">
                            <asp:ListItem Text="10">10</asp:ListItem>
                            <asp:ListItem Text="20">20</asp:ListItem>
                            <asp:ListItem Text="30">30</asp:ListItem>
                            <asp:ListItem Value="9999" Text="9999">All</asp:ListItem>
                        </asp:DropDownList>
                        <span class="PaginationFadedText" style="padding-right: 10px;">records per page</span>
                    </div>
                    <uc1:CompetencyBlock Id="CompetencyBlock1" runat="server" />
                    <div runat="server" id="pagingBar">
                        <div class="PaginationBar">
                            <span style='padding-left: 10px;'>Page &nbsp;</span>
                            <asp:DropDownList AutoPostBack="true" ID="ddlPageNumber" Style='display: none' runat="server"
                                OnSelectedIndexChanged="ddlPageNumber_SelectedIndexChanged">
                            </asp:DropDownList>
                            <asp:Label Text="1" ID="lblCurrentPage" Style='font-weight: bold' runat="server"></asp:Label>
                            of
                            <asp:Label ID="lblTotalPage" Text="10" Style='font-weight: bold' runat="server"></asp:Label>
                            <asp:Label ID="lblTotalRecords" Text="" Style='font-weight: bold' runat="server"></asp:Label>
                            &nbsp;&nbsp;&nbsp; <span class="PaginationFadedText">|</span>&nbsp;&nbsp;&nbsp;
                            <span style="padding-right: 20px; margin-left: 15px; font: bold 11px arial;">
                                <asp:LinkButton ID="btnPrevious" runat="server" Text="&laquo; Prev" OnClick="btnPrevious_Click"
                                    Style="padding-right: 10px;" />
                                &nbsp;&nbsp;
                                <%--don't do validation will be do vadalition in forward only--%>
                                <asp:LinkButton ID="btnNext" runat="server" Text="Next &raquo;" OnClientClick="valGroup='Appraisal';return CheckValidation()"
                                    OnClick="btnSave_Click" />
                            </span>
                        </div>
                    </div>
                    <div class="ratingButtons">
                        <asp:Button ID="btnSave" OnClick="btnSave_Click" CssClass="update" OnClientClick="valGroup='Appraisal';return CheckValidation()"
                            runat="server" Text="Save" ValidationGroup="Appraisal" />
                        <asp:Button Width="120" ID="btnNextOtherPage" OnClick="btnSave_Click" CssClass="save" OnClientClick="valGroup='Appraisal';return CheckValidation()"
                            runat="server" Text="Save and Next" />
                    </div>
                </td>
            </tr>
        </table>
    </div>
    <%--<div id="floatingDiv" runat="server" style="position: fixed; top: 390px;">
        <table style="width: auto" class="ratingTable table table-bordered table-condensed table-striped table-primary">
            <thead>
                <tr>
                    <th colspan="2" style="color: Black">
                        Supervisor Rating
                    </th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td style="font-size: 11px!important;">
                        Quantitive Attributes
                    </td>
                    <td>
                        <asp:Label ID="lblSupervisorQuestion" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong style="font-size: 11px!important;">Performance </br> Summary</strong>
                    </td>
                    <td>
                        <asp:Label ID="lblSupervisorGradeName" runat="server" />
                    </td>
                </tr>
            </tbody>
        </table>
    </div>--%>
    <script type="text/javascript">
        $('.rateit-reset').prop('title', 'Clear');
    </script>
</asp:Content>
