﻿<%@ Page Title="Employee Appraisal" Language="C#" MasterPageFile="~/Master/EmployeeMS.Master"
    AutoEventWireup="true" CodeBehind="EmployeeQualificationAndExpirence.aspx.cs"
    Inherits="Web.Appraisal.EmployeeQualificationAndExpirence" %>



<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Appraisal/UserControls/CompetencyBlock.ascx" TagName="CompetencyBlock"
    TagPrefix="uc1" %>
<%@ Register Src="~/NewHR/UserControls/EduCtrlTest.ascx" TagName="ECtrl" TagPrefix="ucE" %>
<%@ Register Src="~/Appraisal/UserControls/RatingEmployeeBlock.ascx" TagName="RatingEmployeeBlock"
    TagPrefix="uc2" %>
    <%@ Register Src="~/Employee/Appraisal/QualificationCtl.ascx" TagName="QualificationBlock"
    TagPrefix="uc3" %>

<%@ Register Src="EmployeeMenu.ascx" TagName="EmployeeMenu" TagPrefix="uc3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <ext:ResourceManager ID="ResourceManager1" runat="server" />
    <script src="../../css/ratingsrc/jquery.rateit.min.js" type="text/javascript"></script>
    <link href="../../css/ratingsrc/rateit.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">

        $(document).ready
    (
        function () {

        }
    )

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
    <div class="separator bottom" style="padding-bottom: 1px">
    </div>
    <uc2:MsgCtl ID="divMsgCtl" EnableViewState="false" Hide="true" runat="server" />
    <uc2:WarningCtl ID="divWarningMsg" EnableViewState="false" Hide="true" runat="server" />
    <div class="innerLR" style="padding-left: 0px">
        <table>
            <tr>
                <td valign="top" style="padding-top: 16px;">
                    <uc3:EmployeeMenu ID="EmployeeMenu1" runat="server" />
                </td>
                <td style="width: 10px"></td>
                <td valign="top" style="width: 1000px">
                    <uc2:RatingEmployeeBlock ID="RatingEmployeeBlock1" runat="server" />
                  
                    <uc3:QualificationBlock id="blockQualificationAndEducation" runat="server" />

                    <div class="ratingButtons" style="">
                      <%--  <asp:Button ID="btnSave" Visible="false" OnClick="btnSave_Click" CssClass="update" OnClientClick="valGroup='Appraisal';return CheckValidation()"
                            runat="server" Text="Next" ValidationGroup="Appraisal" />--%>
                        <asp:Button ID="btnNext" OnClick="btnSave_Click" CssClass="save" 
                            runat="server" Text="Next" />
                    </div>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
