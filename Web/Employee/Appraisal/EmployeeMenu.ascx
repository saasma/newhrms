﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EmployeeMenu.ascx.cs"
    Inherits="Web.Employee.Appraisal.EmployeeMenu" %>
<style type="text/css">
    .performanceUL
    {
        margin-left: 0px;
        padding: 0px;
    }
    .performanceUL img
    {
        margin-right: 10px;
    }
    .performanceUL li
    {
        list-style-type: none;
        font-weight: normal;
        font-size: 13px;
        background-color: white;
        padding: 3px;
        margin-bottom: 2px;
        padding-left: 5px;
    }
    .performanceUL a
    {
        color: black !important;
        text-decoration: none;
        cursor: pointer;
    }
</style>
<ul class="performanceUL" runat="server" id="menuList" style="">
    <li style="background-color: #47759E; color: White; font-size: 16px;">Performance Review</li>
    <li id="Li1" runat="server">
        <img src="../../images/appraisal_left_icon.png" /><a id="a1" runat="server" href="EmployeeHistory.aspx">History</a></li>
    <li runat="server">
        <img src="../../images/appraisal_left_icon.png" /><a id="anchorIntroduction" runat="server"
            href="EmployeeIntroduction.aspx">Introduction</a></li>
    <li runat="server" id="liObjective">
        <img src="../../images/appraisal_left_icon.png" /><a id="anchorObjective" runat="server"
            href="EmployeeObjective.aspx"> Objective</a></li>
    <li runat="server" id="liActivity">
        <img src="../../images/appraisal_left_icon.png" /><a id="anchorEmpActivity" runat="server"
            href="EmployeeActivity.aspx"> Activity</a></li>
    <li runat="server" id="liQualificationAndExpn">
        <img src="../../images/appraisal_left_icon.png" /><a id="anchorQualificationAndExp" runat="server" href="EmployeeQualificationAndExpirence.aspx">
            Qualification/Experience</a></li>
    <li runat="server" id="liCompetency">
        <img src="../../images/appraisal_left_icon.png" /><a id="anchorComptency" runat="server"
            href="CoreValues.aspx"> Competencies</a></li>
    <li id="liTarget" runat="server">
        <img src="../../images/appraisal_left_icon.png" /><a id="anchorTarget" runat="server"
            href="TargetList.aspx">Targets</a></li>
    <li runat="server">
        <img src="../../images/appraisal_left_icon.png" /><a id="anchorQuestion" runat="server"
            href="Questions.aspx"> Questionnaire</a></li>
    <li id="liReview" runat="server">
        <img src="../../images/appraisal_left_icon.png" /><a id="anchorReview" runat="server"
            href="ReviewList.aspx"> Supervisor Review</a></li>
    <%-- <li id="liSummaryComment" runat="server">
        <img src="../../images/appraisal_left_icon.png" /><a id="anchorSummary" runat="server"
            href="EmployeeSummaryComment.aspx"> Summary Comment</a></li>--%>
    <li runat="server">
        <img src="../../images/appraisal_left_icon.png" /><a id="anchorSignature" runat="server"
            href="EmployeeSignature.aspx"> Signature</a></li>
</ul>
