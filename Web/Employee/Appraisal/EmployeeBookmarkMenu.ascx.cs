﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using BLL.Base;
using DAL;
using BLL.Manager;

namespace Web.Employee.Appraisal
{
    public partial class EmployeeBookmarkMenu : BaseUserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public int GetFormID()
        {
            if (string.IsNullOrEmpty(Request.QueryString["fid"]))
                return 0;
            return int.Parse(Request.QueryString["fid"]);
        }

        public void LoadMenu(string activityBlock, string competencyBlock, string qBlock, string reviewBlock, string summaryBlock,
            string objectiveBlock,string targetBlock)
        {
            anchorIntroduction.HRef = Request.Url.ToString() + anchorIntroduction.HRef;
            anchorObjective.HRef = Request.Url.ToString() + "#" + objectiveBlock;
            anchorEmpActivity.HRef = Request.Url.ToString() + "#" + activityBlock;
            anchorComptency.HRef = Request.Url.ToString() + "#" + competencyBlock;
            anchorQuestion.HRef = Request.Url.ToString() + "#" + qBlock;
            anchorReview.HRef = Request.Url.ToString() + "#" + reviewBlock;
            anchorSummary.HRef = Request.Url.ToString() + "#" + summaryBlock;
            anchorTarget.HRef = Request.Url.ToString() + "#" + targetBlock;
            anchorEducationQualification.HRef = Request.Url.ToString() + "#blockEducationForBookmarking";

            AppraisalForm form = AppraisalManager.getFormInstanceByID(GetFormID());

            if (form.HideObjectiveBlock != null && form.HideObjectiveBlock.Value)
                liObjective.Visible = !form.HideObjectiveBlock.Value;

            if (form.HideTarget != null && form.HideTarget.Value)
                liTarget.Visible = false;

            if (form.HideSupervisorReview != null && form.HideSupervisorReview.Value)
                liSupervisorReview.Visible = false;

            if (form.HideActivitySummary != null && form.HideActivitySummary.Value)
                liActivity.Visible = false;

            if(form.HideHistorySection)
            {
                liEducationQualification.Visible = false;
            }

            if (form.HideCompetencyCoreValues != null && form.HideCompetencyCoreValues.Value)
                liCompetency.Visible = false;


            anchorIntroduction.InnerHtml = form.IntroductionName;
            anchorObjective.InnerHtml = form.ObjectiveName;
            anchorEmpActivity.InnerHtml = form.ActivityName;
            anchorComptency.InnerHtml = form.CompentencyName;
            anchorQuestion.InnerHtml = form.QuestionnareName;
            anchorReview.InnerHtml = form.ReviewName;
            //anchorSummary.InnerHtml = form.SummaryName;
            anchorTarget.InnerHtml = form.TargetName;
            anchorEducationQualification.InnerHtml = form.HistoryName;

            if (!string.IsNullOrEmpty(form.SignationName))
                anchorSummary.InnerHtml = form.SignationName;
        }
    }
}