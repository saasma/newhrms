﻿<%@ Page Title="Employee Appraisal" Language="C#" MasterPageFile="~/Master/EmployeeMS.Master"
    AutoEventWireup="true" CodeBehind="TargetList.aspx.cs" Inherits="Web.Appraisal.TargetList" %>

<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Appraisal/UserControls/CompetencyBlock.ascx" TagName="CompetencyBlock"
    TagPrefix="uc1" %>
<%@ Register Src="~/Appraisal/UserControls/RatingEmployeeBlock.ascx" TagName="RatingEmployeeBlock"
    TagPrefix="uc2" %>
<%@ Register Src="EmployeeMenu.ascx" TagName="EmployeeMenu" TagPrefix="uc3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../../css/ratingsrc/jquery.rateit.min.js" type="text/javascript"></script>
    <link href="../../css/ratingsrc/rateit.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">


       
        
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
<ext:ResourceManager runat="server" Visible="false" ID="resourceMgr" Namespace=""  DisableViewState="false" />
    <div class="separator bottom" style="padding-bottom: 1px">
    </div>
    <uc2:MsgCtl ID="divMsgCtl" EnableViewState="false" Hide="true" runat="server" />
    <uc2:WarningCtl ID="divWarningMsg" EnableViewState="false" Hide="true" runat="server" />
    <div class="innerLR" style="padding-left: 0px">
        <table>
            <tr>
                <td valign="top" style="padding-top: 16px;">
                    <uc3:EmployeeMenu Id="EmployeeMenu1" runat="server" />
                </td>
                <td style="width: 10px">
                </td>
                <td valign="top" style="width: 1000px">
                    <uc2:RatingEmployeeBlock Id="RatingEmployeeBlock1" runat="server" />
                    <uc1:CompetencyBlock Id="CompetencyBlock1" runat="server" />
                    <div class="ratingButtons">
                        <asp:Button ID="btnSave" OnClick="btnSave_Click" CssClass="update" OnClientClick="valGroup='Appraisal';return CheckValidation()"
                            runat="server" Text="Save" ValidationGroup="Appraisal" />
                        <asp:Button Width="120" ID="btnNext" OnClick="btnSave_Click" CssClass="save" OnClientClick="valGroup='Appraisal';return CheckValidation()"
                            runat="server" Text="Save and Next" />
                    </div>
                </td>
            </tr>
        </table>
    </div>
    <script type="text/javascript">
        $('.rateit-reset').prop('title', 'Clear');
    </script>
</asp:Content>
