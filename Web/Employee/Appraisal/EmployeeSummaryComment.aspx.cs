﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using System.Xml;
using BLL.BO;

namespace Web.Appraisal
{
    public partial class EmployeeSummaryComment : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest && !IsPostBack)
            {
                Initialise();

            }
        }
        public int GetFormID()
        {
            if (string.IsNullOrEmpty(Request.QueryString["fid"]))
                return 0;
            return int.Parse(Request.QueryString["fid"]);
        }
        public int GetEmployeeFormID()
        {
            if (string.IsNullOrEmpty(Request.QueryString["efid"]))
                return 0;
            return int.Parse(Request.QueryString["efid"]);
        }

        public void btnNextPage_Click(object sender, EventArgs e)
        {


            AppraisalEmployeeForm empForm = AppraisalManager.GetEmployeeAppraisaleForm(GetEmployeeFormID());


            Response.Redirect(string.Format("EmployeeSignature.aspx?fid={0}&efid={1}",
GetFormID(), GetEmployeeFormID()));

        }

        public void btnSave_Click(object sender, EventArgs e)
        {
            if (btnSave.Visible)
            {
                 AppraisalEmployeeForm empForm = AppraisalManager.GetEmployeeAppraisaleForm(GetEmployeeFormID());

                

                if (empForm.EmployeeId != SessionManager.CurrentLoggedInEmployeeId
                    && empForm.Status == (int)AppraisalStatus.SaveAndSend)
                {
                    AppraisalManager.UpdateStep2SupervisorComment(GetEmployeeFormID(), txtSupervisorComments.Text.Trim(),-1);
                }
                else
                {
                    AppraisalManager.UpdateEmployeeSelfSummaryComment(GetEmployeeFormID(),
                    txtComments.Text.Trim(), rdbIAgree.Checked);
                }


                divMsgCtl.InnerHtml = "Appraisal saved.";
                divMsgCtl.Hide = false;

            }

            if (btnNext == sender)
            {
                Response.Redirect(string.Format("EmployeeSignature.aspx?fid={0}&efid={1}",
                  GetFormID(), GetEmployeeFormID()), true);
            }
            Initialise();
        }

        public void Initialise()
        {

           
            AppraisalForm form = AppraisalManager.getFormInstanceByID(GetFormID());
            if (form != null)
            {
                // Introudction
                
                spanIntroductionName.InnerHtml = form.SummaryName;
                divIntroductionDescription.InnerHtml = (form.SummaryDescription);

            }

            AppraisalEmployeeForm empForm = AppraisalManager.GetEmployeeAppraisaleForm(GetEmployeeFormID());

            AppraisalEmployeeSummaryComment comment = AppraisalManager.GetAppraisalSelfEmployeeSummaryComment(GetEmployeeFormID());


            spanEmployeeComment.InnerHtml = new EmployeeManager().GetById(empForm.EmployeeId).Name + "'s Comment";
            if (comment != null)
                txtComments.Text = comment.Comment;

            if (empForm.SupervisorReviewAgreed == null || empForm.SupervisorReviewAgreed == true)
                rdbIAgree.Checked = true;
            else
            {
                rdbIAgree.Checked = false;
                rdbIDisAgree.Checked = true;
            }


            if (empForm.EmployeeId == SessionManager.CurrentLoggedInEmployeeId)
            {
                if (empForm.Status != (int)AppraisalStatus.SupervisorManagerCommented)
                {
                    btnSave.Visible = false;
                    txtComments.Visible = false;
                    rdbIAgree.Enabled = false;
                    rdbIDisAgree.Enabled = false;

                    if (comment != null)
                    {
                        acitivtySummary.InnerHtml = comment.Comment; ;
                        acitivtySummary.Visible = true;
                    }
                }
                else
                {

                }
            }
            else
            {

                if (empForm.Status == (int)AppraisalStatus.SaveAndSend)
                {
                    agreeDisagreeBlock.Visible = false;
                    commentBlock.Visible = false;
                    spanEmployeeComment.Visible = false;
                }

                if (comment != null)
                {
                    acitivtySummary.InnerHtml = comment.Comment; ;
                    acitivtySummary.Visible = true;
                }
                txtComments.Visible = false;
                btnSave.Visible = true;
                rdbIAgree.Enabled = false;
                rdbIDisAgree.Enabled = false;
            }

            // I agree/disagree block
            if (form.ShowAgreeDisAgreeButtons != null && form.ShowAgreeDisAgreeButtons.Value) { }
            else
            {
                agreeDisagreeBlock.Visible = false;
            }
            

            // supervisor comment block
            divSupervisorComments.Visible = false;
            txtSupervisorComments.Text = empForm.SupervisorStep2Comment;            
            if (empForm.EmployeeId == SessionManager.CurrentLoggedInEmployeeId)
            {
                if (empForm.Status >= (int)AppraisalStatus.SupervisorManagerCommented)
                {
                    divSupervisorComments.Visible = true;
                    txtSupervisorComments.Enabled = false;
                }
            }
            else
            {

                if (empForm.Status == (int)AppraisalStatus.SaveAndSend)
                {
                    divSupervisorComments.Visible = true;
                    txtSupervisorComments.Enabled = true;
                }
                else
                {
                    divSupervisorComments.Visible = true;
                    txtSupervisorComments.Enabled = false;
                }
            }


        }

        




    }
}