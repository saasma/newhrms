﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using System.Xml;
using Utils.Helper;
using System.IO;
namespace Web.Employee.Appraisal
{
    public partial class ReviewAllCtl : BaseUserControl
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            //if (!IsPostBack && !X.IsAjaxRequest)
            {
                Initialise();

            }
        }
        public int GetFormID()
        {
            if (string.IsNullOrEmpty(Request.QueryString["fid"]))
                return 0;
            return int.Parse(Request.QueryString["fid"]);
        }

        public int GetEmployeeFormID()
        {
            if (string.IsNullOrEmpty(Request.QueryString["efid"]))
                return 0;
            return int.Parse(Request.QueryString["efid"]);
        }

        public void Initialise()
        {

            AppraisalForm form = AppraisalManager.getFormInstanceByID(GetFormID());
            AppraisalEmployeeForm empForm = AppraisalManager.GetEmployeeAppraisaleForm(GetEmployeeFormID());

            if (empForm != null)
            {

                
                

                if (CommonManager.CompanySetting.WhichCompany == WhichCompany.DishHome)
                {
                    // only dep head can revert and dep head is after step3
                    if (empForm.Status == (int)FlowStepEnum.Step3 && empForm.CurrentApprovalSelectedEmployeeId==SessionManager.CurrentLoggedInEmployeeId)
                        btnRevert.Visible = true;
                }
                else
                {
                    btnRevert.Visible = SessionManager.CurrentLoggedInEmployeeId != 0
                        && empForm.CurrentApprovalSelectedEmployeeId == SessionManager.CurrentLoggedInEmployeeId;
                }

                // if approved or in last step then hide commenting box
                if (empForm.Status == (int)FlowStepEnum.Step15End)
                {
                    btnSave.Hide();//.Visible = false;
                    SummaryCommentBlock2.CommentBlock.Visible = false;
                    btnRevert.Visible = false;
                }

            // permission check if can view the document or not
                if (SessionManager.CurrentLoggedInEmployeeId != 0 && empForm.EmployeeId != SessionManager.CurrentLoggedInEmployeeId)
                {

                    // if appraisal and already submitted by employee then show the form always
                    bool isSupervisor = (empForm.Status > (int)AppraisalStatus.SaveAndSend && empForm.SupervisorEmployeeId == SessionManager.CurrentLoggedInEmployeeId);

                    if (isSupervisor == false)
                    {
                        if (!CommonManager.CanViewDocument(FlowTypeEnum.Appraisal, empForm.Status.Value,
                            empForm.EmployeeId, SessionManager.CurrentLoggedInEmployeeId, empForm.AppraisalEmployeeFormID))
                        {

                            // if has commented for this appraisal then allow to view but hide buttons
                            if (BLL.BaseBiz.PayrollDataContext.AppraisalEmployeeSummaryComments.Any(
                                x => x.AppraisalEmployeeFormRef_ID == empForm.AppraisalEmployeeFormID && x.CommentedEmployeeId == SessionManager.CurrentLoggedInEmployeeId))
                            {

                                btnSave.Visible = false;
                                btnRevert.Visible = false;
                            }
                            else
                            {
                                NotPermissionContent();
                                return;
                            }
                        }
                    }

                    if (!CommonManager.CanChangeDocuemnt(FlowTypeEnum.Appraisal, empForm.Status.Value,
                           empForm.EmployeeId, SessionManager.CurrentLoggedInEmployeeId,empForm.AppraisalEmployeeFormID))
                    {
                        btnSave.Hide();//.Visible = false;
                        SummaryCommentBlock2.CommentBlock.Visible = false;
                        btnRevert.Visible = false;
                    }
                }
            }


            if (form != null)
            {
                // Introudction               
                IntroductionName.InnerHtml = form.IntroductionName;
                IntroductionDescription.InnerHtml = (form.IntroductionDescription);

                if (form.HideObjectiveBlock != null && form.HideObjectiveBlock.Value)
                    blockObjective.Visible = false;
                spanObjectiveName.InnerHtml = form.ObjectiveName;
                divObjectiveDescription.InnerHtml = form.ObjectiveDescription;


                //activityName.InnerHtml = form.ActivityName;
                //activityDescription.InnerHtml = (form.ActivityDescription);
                //acitivtySummary.InnerHtml = empForm.ActivitySummary;


                CompetencyBlock1.AppraisalFormID = GetFormID();


                if (X.IsAjaxRequest == false)
                {
                    CompetencyBlock1.SetActivitySummary();
                    CompetencyBlock1.LoadActivityDetails(false);
                    CompetencyBlock1.LoadCompetencyDetails();
                    CompetencyBlock1.LoadTargetDetails();
                    CompetencyBlock1.LoadQuestionnaireDetails(0, int.MaxValue);
                    CompetencyBlock1.LoadReviewDetails();
                }
            }


            //bookmark
            EmployeeMenu1.LoadMenu(CompetencyBlock1.GetActivityBlockID, CompetencyBlock1.GetCompetencyBlockID,
                CompetencyBlock1.GetQuestionnareBlockID, CompetencyBlock1.GetReviewBlockID, SummaryCommentBlock2.GetSummaryBlockID, blockObjective.ClientID,
                CompetencyBlock1.GetTargetBlockID);


            JavascriptHelper.AttachNonDialogPopUpCode(Page, "revsertPopup", "RevertImport.aspx", 450, 500);
            btnRevert.OnClientClick = string.Format("return ImportPopup12({0});",empForm.AppraisalEmployeeFormID);
        }



        public void btnSave_Click(object sender, EventArgs e)
        {

            int recommendForId = 0;
            string strValue = SummaryCommentBlock2.RecommendDropDown.SelectedValue;
            int.TryParse(strValue, out recommendForId);

            double? additionalPoints = SummaryCommentBlock2.GetAdditionalQuestionnairePoints();

            // if admin user then it should be mapped
            if (SessionManager.CurrentLoggedInEmployeeId == 0)
            {
                int currentEmployeeId = 0; string currentEmployeeUserName = "";
                UserManager.SetCurrentEmployeeAndUser(ref currentEmployeeId, ref currentEmployeeUserName);

                if (currentEmployeeId == 0)
                {
                    NewMessage.ShowWarningMessage("Please assign the employee from Manage user before changing appraisal.");
                    return;
                }

            }

            Status status =
                AppraisalManager.UpdateAppraisalSummaryComment(GetEmployeeFormID(), SummaryCommentBlock2.CommentTextBox.Text.Trim(), recommendForId,additionalPoints);

            //SummaryCommentBlock2.Initialise();

            if (status.IsSuccess)
            {
                //divMsgCtl.InnerHtml = "Appraisal saved and forwarded";
                //divMsgCtl.Hide = false;

                if(additionalPoints != null)
                {
                    NewMessage.ShowNormalMessage("Appraisal saved and forwarded", "hideRevert();window.location.reload(false);");
                }
                else
                    NewMessage.ShowNormalMessage("Appraisal saved and forwarded","hideRevert();");

                //btnRevert.Visible = false;
                btnSave.Hide();//.Visible = false;

            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);

                //divWarningMsg.InnerHtml = status.ErrorMessage;
                ///divWarningMsg.Hide = false;
            }

            //Initialise();
        }

      

    }
}