﻿<%@ Page Title="Employee Appraisal" Language="C#" MasterPageFile="~/Master/EmployeeMS.Master"
    AutoEventWireup="true" CodeBehind="EmployeeActivity.aspx.cs" Inherits="Web.Appraisal.EmployeeActivity" %>

<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Appraisal/UserControls/CompetencyBlock.ascx" TagName="CompetencyBlock"
    TagPrefix="uc1" %>
<%@ Register Src="~/Appraisal/UserControls/RatingEmployeeBlock.ascx" TagName="RatingEmployeeBlock"
    TagPrefix="uc2" %>
<%@ Register Src="EmployeeMenu.ascx" TagName="EmployeeMenu" TagPrefix="uc3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../../css/ratingsrc/jquery.rateit.min.js" type="text/javascript"></script>
    <link href="../../css/ratingsrc/rateit.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">

        $(document).ready
    (
        function () {

            if (typeof (CKEDITOR) != 'undefined') {

                CKEDITOR.config.toolbar = [
                  ['Styles', 'Format', 'Font', 'FontSize'],

                  ['Bold', 'Italic', 'Underline', 'StrikeThrough', '-', 'Undo', 'Redo', '-', 'Cut', 'Copy', 'Paste', 'Find', 'Replace', '-', 'Outdent', 'Indent', '-'],

                  ['NumberedList', 'BulletedList', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'],
                  ['Image', 'Table', '-', 'Link', 'TextColor', 'BGColor'], ['Maximize']
                ];
            }
        }
    )
      
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
    <ext:ResourceManager ID="ResourceManager1" DisableViewState="false" runat="server" />
    <div class="separator bottom" style="padding-bottom: 1px">
    </div>
    <uc2:MsgCtl ID="divMsgCtl" EnableViewState="false" Hide="true" runat="server" />
    <uc2:WarningCtl ID="divWarningMsg" EnableViewState="false" Hide="true" runat="server" />
    <div class="innerLR" style="padding-left: 0px">
        <table>
            <tr>
                <td valign="top" style="padding-top: 16px;">
                    <uc3:EmployeeMenu ID="EmployeeMenu1" runat="server" />
                </td>
                <td style="width: 10px">
                </td>
                <td valign="top" style="width: 1000px">
                    <uc2:RatingEmployeeBlock ID="RatingEmployeeBlock1" runat="server" />
                    <div runat="server" id="blockIntroduction">
                        <span class="ratingBlockTitleCls" runat="server" id="spanIntroductionName"></span>
                        <div runat="server" class='ratingContainerBlockDescription' id="divIntroductionDescription">
                        </div>
                    </div>
                    <%-- <div class="" style="margin-left: 20px; margin-bottom: 13px;">
                        <div runat="server" class="ratingCommentBlock" id="commentBlock1">
                            <span runat="server" class="spanRatingLevelClass" id="spanEmployeeRatingLevel">Employee
                                's Rating </span>
                            <asp:TextBox runat="server" value="0" max="5" min="0" step="1" ID="ratingEmployeeValue" />
                            <span class="rateit" runat="server" id="ratingEmployee" data-rateit-backingfld="#ctl00_mainContent_ratingEmployeeValue">
                            </span>
                        </div>
                        <div runat="server" class="ratingCommentBlock" id="commentBlock2">
                            <span runat="server" class="spanRatingLevelClass" id="spanSupervisorRatingLevel">Supervisor
                                's Rating </span>
                            <asp:TextBox runat="server" value="0" max="5" min="0" step="1" ID="ratingSupervisorValue" />
                            <span class="rateit" runat="server" id="ratingSupervisor" data-rateit-backingfld="#ctl00_mainContent_ratingSupervisorValue">
                            </span>
                        </div>
                    </div>--%>
                    <div style="clear: both; margin-bottom: 20px;">
                    </div>
                    <span class="ratingBlockTitleCls" style="margin-left: 0px; padding-top: 10px;" runat="server"
                        id="span1">Activity Summary</span>
                    <div class="ratingButtons" style="margin-top: 10px">
                        <CKEditor:CKEditorControl ExtraPlugins="autogrow" ID="CKEditor1" Width="800px" Height="300px"
                            runat="server" BasePath="~/ckeditor/">
                        </CKEditor:CKEditorControl>
                        <asp:RequiredFieldValidator ID="valReqd" runat="server" ControlToValidate="CKEditor1"
                            Display="None" ErrorMessage="Activity Summary is required." ValidationGroup="Appraisal" />
                    </div>
                    <div runat="server" visible="false" style="margin-left: 0px" class='ratingContainerBlockDescription'
                        id="acitivtySummary">
                    </div>
                    <div style="margin-top: 10px" runat="server" id="blockRating">
                        <uc1:CompetencyBlock runat="server" Visible="false" ID="activityBlock" />
                        <div id="block" runat="server">
                            <div class="ratingContainerBlock" id="ctl00_mainContent_ctl02_containerBlockActivity1">
                                <div runat="server" class="ratingCommentBlock" id="appraiseeBlock">
                                    <span runat="server" class="spanRatingLevelClass" id="spanRatingLevel">Appraisee's Marks
                                    </span>
                                    <asp:TextBox runat="server" max="5" min="0" step="1" ID="appraiseeMarkingValue" />
                                </div>
                                <div runat="server" class="ratingCommentBlock" id="supervisorBlock">
                                    <span runat="server" class="spanRatingLevelClass" id="span2">Supervisor's Marks
                                    </span>
                                    <asp:TextBox runat="server" max="5" min="0" step="1" ID="supervisorMarkingValue" />
                                </div>
                                <div style="clear: both">
                                </div>
                            </div>
                            <div id="supvervisorComment" runat="server" style="padding-left: 0px">
                                Supervisor Comment
                                <br />
                                <asp:TextBox Width="400px" Height="50px" ID="txtSupervisorComment" runat="server"
                                    TextMode="MultiLine">
                            
                                </asp:TextBox>
                            </div>
                        </div>
                    </div>
                    <div style="margin-top: 20px">
                        <table>
                            <tr>
                                <td>
                                    <ext:FileUploadField ID="FileDocumentUpload" runat="server" Width="360" Icon="Attach"
                                        MarginSpec='0 0 0 0' FieldLabel="Job Description" LabelAlign="top" LabelSeparator=" :" />
                                    <asp:RequiredFieldValidator ValidationGroup="Appraisal" ControlToValidate="FileDocumentUpload"
                                        Display="None" Text="Please select a file." ID="uploadActv" runat="server" ErrorMessage="Please select a file."></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding-top:20px">
                                    <ext:LinkButton ID="btnDownload" runat="server" Icon="Attach">
                                        <DirectEvents>
                                            <Click OnEvent="btnDownload_Click">
                                            </Click>
                                        </DirectEvents>
                                    </ext:LinkButton>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="ratingButtons">
                        <asp:Button ID="btnSave" OnClick="btnSave_Click" CssClass="update" OnClientClick="valGroup='Appraisal';return CheckValidation()"
                            runat="server" Text="Save" ValidationGroup="Appraisal" />
                        <asp:Button ID="btnNext" OnClick="btnSave_Click" CssClass="save" OnClientClick="valGroup='Appraisal';return CheckValidation()"
                            runat="server" Text="Next" />
                    </div>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
