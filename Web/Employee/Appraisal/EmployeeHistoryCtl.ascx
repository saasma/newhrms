﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EmployeeHistoryCtl.ascx.cs"
    Inherits="Web.Employee.Appraisal.EmployeeHistoryCtl" %>
<%@ Register Src="~/NewHR/UserControls/EducationCtl.ascx" TagName="EducationCtl"
    TagPrefix="ucEducation" %>
<style type="text/css">
    .x-form-item-label
    {
        font-style: italic;
        color: #2F75B5;
        font-weight: bold;
    }
    
    
    
    .table thead th
    {
        vertical-align: bottom;
    }
    .table-bordered th, .table-bordered td
    {
        border-color: #d7d8da;
    }
    .table-condensed th
    {
        padding: 3px 10px;
    }
    .table th, .table td
    {
        border-top-color: #f8f8f8;
    }
    .table-bordered th, .table-bordered td
    {
        border-left: 1px solid #dddddd;
    }
    .table-condensed th, .table-condensed td
    {
        padding: 4px 5px;
    }
    
    .table-primary thead th
    {
        background: #47759e;
    }
</style>
<table class="fieldTable">
    <tr>
        <td colspan="4">
            <span class="x-form-item-label">Employee Status History</span>
            <ext:Label ID="lblStatus" runat="server">
            </ext:Label>
        </td>
    </tr>
    <tr>
        <td colspan="4">
            <span class="x-form-item-label">Education Details</span>
            <%-- <ext:TextArea Disabled="true" ID="txtComment" runat="server" Width="500px">
            </ext:TextArea>--%>
          <%--  <ext:Label ID="lblEduDetails" runat="server">
            </ext:Label>--%>
            <ucEducation:educationctl isdisplaymode="true" id="EducationCtl" runat="server" />
        </td>
    </tr>
</table>
<%--<div style="padding-left: 10px; padding-top: 10px;">
    <span class="x-form-item-label">Salary Details</span>
    <ext:Label ID="lblSalaryDetails" runat="server">
    </ext:Label>
</div>--%>
<table class="fieldTable" style="margin-top: 0px">
    <tr>
        <td>
            <span class="x-form-item-label">Transfer History</span>
            <ext:GridPanel ID="gridTransferList" runat="server" Width="700px">
                <Store>
                    <ext:Store ID="Store3" runat="server">
                        <Model>
                            <ext:Model ID="Model4" runat="server" IDProperty="BranchDepartmentId">
                                <Fields>
                                    <ext:ModelField Name="Type" Type="string" />
                                    <ext:ModelField Name="FromBranch" Type="string" />
                                    <ext:ModelField Name="ToBranch" Type="string" />
                                    <ext:ModelField Name="FromDate" Type="string" />
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                <ColumnModel>
                    <Columns>
                        <ext:Column ID="Column8" Sortable="false" MenuDisabled="true" runat="server" Text="Type"
                            Align="Left" Width="200" DataIndex="Type" />
                        <ext:Column ID="Column4" Flex="1"  Width="200" Sortable="false" MenuDisabled="true" runat="server"
                            Text="From" Align="Left" DataIndex="FromBranch">
                        </ext:Column>
                        <ext:Column ID="Column5" Flex="1"  Sortable="false" MenuDisabled="true" runat="server" Text="To"
                            Align="Left" Width="100" DataIndex="ToBranch" />
                        <ext:Column ID="Column1" Flex="1"  Sortable="false" MenuDisabled="true" runat="server" Text="Date"
                            Align="Left" Width="100" DataIndex="FromDate" />
                    </Columns>
                </ColumnModel>
                <SelectionModel>
                    <ext:RowSelectionModel ID="RowSelectionModel2" runat="server" Mode="Single" />
                </SelectionModel>
            </ext:GridPanel>
        </td>
    </tr>
    <tr>
        <td>
            <span class="x-form-item-label">Training History</span>
            <ext:GridPanel ID="gridTraining" runat="server" Width="700px">
                <Store>
                    <ext:Store ID="Store2" runat="server">
                        <Model>
                            <ext:Model ID="Model1" runat="server">
                                <Fields>
                                    <ext:ModelField Name="TrainingName" Type="string" />
                                    <ext:ModelField Name="InstitutionName" Type="string" />
                                    <ext:ModelField Name="DurationText" Type="string" />
                                    <ext:ModelField Name="TrainingFrom" Type="string" />
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                <ColumnModel>
                    <Columns>
                        <ext:Column ID="Column2"  Sortable="false" MenuDisabled="true" runat="server" Text="Training Name"
                            Align="Left" Width="200" DataIndex="TrainingName" />
                        <ext:Column ID="Column3" Flex="1"  Width="200" Sortable="false" MenuDisabled="true" runat="server"
                            Text="Institution" Align="Left" DataIndex="InstitutionName">
                        </ext:Column>
                        <ext:Column ID="Column6"  Flex="1" Sortable="false" MenuDisabled="true" runat="server" Text="Duration"
                            Align="Left" Width="100" DataIndex="DurationText" />
                        <ext:Column ID="Column7" Sortable="false" MenuDisabled="true" runat="server" Text="Date"
                            Align="Left" Width="100"  Flex="1"  DataIndex="TrainingFrom" />
                    </Columns>
                </ColumnModel>
                <SelectionModel>
                    <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" Mode="Single" />
                </SelectionModel>
            </ext:GridPanel>
        </td>
    </tr>
     <tr runat="server" id="rowCurrentPay">
        <td>
            <span class="x-form-item-label">Current Pay</span>
            <ext:GridPanel ID="gridCurrentPay" runat="server" Width="700px">
                <Store>
                    <ext:Store ID="Store1" runat="server">
                        <Model>
                            <ext:Model ID="Model2" runat="server">
                                <Fields>
                                    <ext:ModelField Name="Title" Type="string" />
                                    <ext:ModelField Name="Amount" Type="string" />

                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                <ColumnModel>
                    <Columns>
                        <ext:Column ID="Column9"  Sortable="false" MenuDisabled="true" runat="server" Text="Title"
                            Align="Left" Width="150" DataIndex="Title" />
                        <ext:Column ID="Column10" Flex="1"  Width="100" Sortable="false" MenuDisabled="true" runat="server"
                            Text="Amount" Align="Left" DataIndex="Amount">
                            <Renderer Fn="getFormattedAmount" />
                        </ext:Column>
                       

                    </Columns>
                </ColumnModel>
                <SelectionModel>
                    <ext:RowSelectionModel ID="RowSelectionModel3" runat="server" Mode="Single" />
                </SelectionModel>
            </ext:GridPanel>
        </td>
    </tr>
    <tr>
        <td colspan="4">
            <span class="x-form-item-label">Notes</span>
            <%-- <ext:TextArea ID="txtNotes" Disabled="true" runat="server" Width="500px">
            </ext:TextArea>--%>
            <ext:Label ID="lblNotes" runat="server">
            </ext:Label>
        </td>
    </tr>
</table>
