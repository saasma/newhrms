﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL.Entity;
using BLL;
using System.IO;
using BLL.BO;
using Utils.Helper;
using Bll;


namespace Web.Employee
{
    public partial class RevertImport : BasePage
    {
        public bool HasImport
        {
            get
            {
                if (ViewState["import"] == null)
                    return false;
                return Convert.ToBoolean(ViewState["import"]);
            }
            set
            {
                ViewState["import"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest && !IsPostBack)
            {
                Initialize();
            }

            
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            
        }

        public int GetID()
        {
            return int.Parse(Request.QueryString["efid"]);
        }

        protected void Initialize()
        {
            AppraisalEmployeeForm empForm = AppraisalManager.GetEmployeeAppraisaleForm(GetID());


            if (empForm.CurrentApprovalSelectedEmployeeId != null &&
                empForm.CurrentApprovalSelectedEmployeeId.Value == SessionManager.CurrentLoggedInEmployeeId)
            { }
            else
            {
                btnConfirmRevert.Visible = false;
                Response.Write("Not enough permission");
                Response.End();
                return;
            }

            header.InnerHtml = "Appraisal Revert for : " + EmployeeManager.GetEmployeeName(empForm.EmployeeId);
            disp.Text = empForm.StatusText == null ? "" : empForm.StatusText + " ( Step " + empForm.Status + ")";


            

            List<ApprovalFlow> list =
               TravelAllowanceManager.GetApprovalFlowListForEmployeeAppraisal(empForm.AppraisalEmployeeFormID);
            list.Insert(0, new ApprovalFlow { StepID = 0, StepName = "Employee Submit (Start)" });

            // for dish home department head can revert to Employee change or supervisor change only
            if (CommonManager.CompanySetting.WhichCompany == WhichCompany.DishHome)
            {
                list.Clear();
                list.Add(new ApprovalFlow { });
                list.Add(new ApprovalFlow { StepID = 0,StepName = "Employee" });
                list.Add(new ApprovalFlow { StepID = 1, StepName = "Supervisor" });
            }

           

            //foreach (var item in list)
            //{
            //    item.StepName += " ( Step " + item.StepID + ")";
            //}
            
            //list.Insert(0, new ApprovalFlow { StepID = -1, StepName = "All" });
            //cmbStatus.SelectedItems.Add(new Ext.Net.ListItem { Index = 0 });

            cmbRevertStatus.Store[0].DataSource = list;
            cmbRevertStatus.Store[0].DataBind();

            cmbRevertStatus.ClearValue();
        }


        protected void btnConfirmRevert_Click(object sender, DirectEventArgs e)
        {

            int empAppraisalForm = GetID();

            if (cmbRevertStatus.SelectedItem == null || cmbRevertStatus.SelectedItem.Value == null)
            {
                NewMessage.ShowWarningMessage("Status is required.");
                return;
            }

            if (string.IsNullOrEmpty(txtNotes.Text.Trim()))
            {
                NewMessage.ShowWarningMessage("Note is required.");
                return;
            }

            Status status = new Status();

            status = AppraisalManager.ReverseAppraisalStatus(empAppraisalForm, int.Parse(cmbRevertStatus.SelectedItem.Value)
                    , txtNotes.Text.Trim());
            


            if (status.IsSuccess)
            {

                NewMessage.ShowNormalMessage("Appraisal reverted.", "closePopup");

            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }

        }


    }

    


}