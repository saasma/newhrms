﻿<%@ Page Title="Employee Appraisal" Language="C#" MasterPageFile="~/Master/EmployeeMS.Master"
    AutoEventWireup="true" CodeBehind="EmployeeIntroduction.aspx.cs" Inherits="Web.Appraisal.EmployeeIntroduction" %>

<%@ Register Src="~/Appraisal/UserControls/CompetencyBlock.ascx" TagName="CompetencyBlock"
    TagPrefix="uc1" %>
<%@ Register Src="~/Appraisal/UserControls/RatingEmployeeBlock.ascx" TagName="RatingEmployeeBlock"
    TagPrefix="uc2" %>
<%@ Register Src="EmployeeMenu.ascx" TagName="EmployeeMenu" TagPrefix="uc3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../../css/ratingsrc/jquery.rateit.min.js" type="text/javascript"></script>
    <link href="../../css/ratingsrc/rateit.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
    <div class="separator bottom">
    </div>
    <div class="innerLR" style="padding-left: 0px">
        <table>
            <tr>
                <td valign="top" style="padding-top: 16px;">
                    <uc3:EmployeeMenu Id="EmployeeMenu1" runat="server" />
                </td>
                <td style="width: 10px">
                </td>
                <td valign="top" style="width: 1000px">
                    <uc2:RatingEmployeeBlock Id="RatingEmployeeBlock1" runat="server" />
                    <div runat="server" id="blockIntroduction">
                        
                        <span class="ratingBlockTitleCls" runat="server" id="spanIntroductionName"></span>
                        <div runat="server" class='ratingContainerBlockDescription' id="divIntroductionDescription">
                        </div>
                    </div>
                    <div class="ratingButtons">
                        <asp:Button ID="btnNext" CssClass="save" OnClientClick="" runat="server" Text="Next" />
                    </div>
                </td>
            </tr>
        </table>
    </div>
    <script type="text/javascript">
        $('.rateit-reset').prop('title', 'Clear');
    </script>
</asp:Content>
