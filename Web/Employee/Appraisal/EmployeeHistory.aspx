﻿<%@ Page Title="Employee Appraisal" Language="C#" MasterPageFile="~/Master/EmployeeMS.Master"
    AutoEventWireup="true" CodeBehind="EmployeeHistory.aspx.cs" Inherits="Web.Appraisal.EmployeeHistory" %>
    
<%@ Register Src="~/Employee/Appraisal/EmployeeDetailsScoreCtl.ascx" TagName="Score"
    TagPrefix="uc1" %>
<%@ Register Src="~/Employee/Appraisal/EmployeeHistoryCtl.ascx" TagName="history"
    TagPrefix="uc1" %>
<%@ Register Src="~/Appraisal/UserControls/RatingEmployeeBlock.ascx" TagName="RatingEmployeeBlock"
    TagPrefix="uc2" %>
<%@ Register Src="EmployeeMenu.ascx" TagName="EmployeeMenu" TagPrefix="uc3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
    <ext:ResourceManager runat="server" ScriptMode="Release" />
    <div class="separator bottom">
    </div>
    <div class="innerLR" style="padding-left: 0px">
        <table>
            <tr>
                <td valign="top" style="padding-top: 16px;">
                    <uc3:EmployeeMenu Id="EmployeeMenu1" runat="server" />
                </td>
                <td style="width: 10px">
                </td>
                <td valign="top" style="width: 1000px">
                    <uc2:RatingEmployeeBlock Id="RatingEmployeeBlock1" runat="server" />
                   <%-- <uc1:Score id="Score" runat="server" />--%>
                    <uc1:history id="history" runat="server" />
                   <%-- <div class="ratingButtons">
                        <asp:Button ID="btnNext" CssClass="save" OnClientClick="" runat="server" Text="Next" />
                    </div>--%>
                </td>
            </tr>
        </table>
    </div>
    <script type="text/javascript">
        $('.rateit-reset').prop('title', 'Clear');
    </script>
</asp:Content>
