﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using System.Xml;

namespace Web.Appraisal
{
    public partial class EmployeeIntroduction : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();

            }
        }
        public int GetFormID()
        {
            if (string.IsNullOrEmpty(Request.QueryString["fid"]))
                return 0;
            return int.Parse(Request.QueryString["fid"]);
        }
        public int GetEmployeeFormID()
        {
            if (string.IsNullOrEmpty(Request.QueryString["efid"]))
                return 0;
            return int.Parse(Request.QueryString["efid"]);
        }
        public void Initialise()
        {


            AppraisalForm form = AppraisalManager.getFormInstanceByID(GetFormID());
            if (form != null)
            {
                // Introudction

                spanIntroductionName.InnerHtml = form.IntroductionName;
                divIntroductionDescription.InnerHtml = (form.IntroductionDescription);

                string nextPage = "EmployeeObjective.aspx";


                if (form.HideObjectiveBlock != null && form.HideObjectiveBlock.Value)
                {
                    nextPage = "EmployeeActivity.aspx";

                    if (form.HideActivitySummary != null && form.HideActivitySummary.Value)
                    {
                        nextPage = "EmployeeQualificationAndExpirence.aspx";

                        if (form.HideHistorySection)
                        {
                            // Competency
                            nextPage = "CoreValues.aspx";

                            if (form.HideCompetencyCoreValues != null && form.HideCompetencyCoreValues.Value)
                            {
                                if (AppraisalManager.HasTargetForForm(GetFormID()))
                                    nextPage = "TargetList.aspx";
                                else
                                    nextPage = "Questions.aspx";
                            }
                        }
                    }
                }


                //if(form.HideObjectiveBlock != null && form.HideObjectiveBlock.Value)
                //    btnNext.OnClientClick = string.Format("window.location='EmployeeActivity.aspx?fid={0}&efid={1}';return false;",
                //    GetFormID(), GetEmployeeFormID());
                //else 
                btnNext.OnClientClick = string.Format("window.location='{2}?fid={0}&efid={1}';return false;",
                    GetFormID(), GetEmployeeFormID(),nextPage);
            }
        }

        




    }
}