﻿<%@ Page Title="Employee Appraisal" Language="C#" MasterPageFile="~/Master/EmployeeMS.Master"
    AutoEventWireup="true" CodeBehind="EmployeeSignature.aspx.cs" Inherits="Web.Appraisal.EmployeeSignature" %>

<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Appraisal/UserControls/CompetencyBlock.ascx" TagName="CompetencyBlock"
    TagPrefix="uc1" %>
<%@ Register Src="~/Appraisal/UserControls/RatingEmployeeBlock.ascx" TagName="RatingEmployeeBlock"
    TagPrefix="uc2" %>
<%@ Register Src="EmployeeMenu.ascx" TagName="EmployeeMenu" TagPrefix="uc3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../../css/ratingsrc/jquery.rateit.min.js" type="text/javascript"></script>
    <link href="../../css/ratingsrc/rateit.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">

    </script>
    <style type="text/css">
        .summaryCommmentSignatureNew
        {
            border-bottom: 1px solid #D3D3D3;
            color: #808080;
            display: block;
            float: left;
            margin-bottom: 20px;
            margin-left: 20px;
            padding-bottom: 3px;
            width: 400px;
            clear:both;
        }
       
    </style>
    <script type="text/javascript">
        function ImportPopup12(efid) {

            revsertPopup('efid=' + efid);
            return false;
        }
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
    <div class="separator bottom" style="padding-bottom: 1px">
    </div>
       <ext:ResourceManager ID="ResourceManager1" runat="server" DisableViewState="false" ScriptMode="Release" />
    <uc2:MsgCtl ID="divMsgCtl" EnableViewState="false" Hide="true" runat="server" />
    <uc2:WarningCtl ID="divWarningMsg" EnableViewState="false" Hide="true" runat="server" />
    <div class="innerLR" style="padding-left: 0px">
        <table>
            <tr>
                <td valign="top" style="padding-top: 16px;">
                    <uc3:EmployeeMenu Id="EmployeeMenu1" runat="server" />
                </td>
                <td style="width: 10px">
                </td>
                <td valign="top" style="width: 1000px">
                    <uc2:RatingEmployeeBlock Id="RatingEmployeeBlock1" runat="server" />
                    <%--Comment block--%>
                    <div runat="server" id="mainCommentBlock">
                        <div runat="server" id="blockIntroductionComment">
                            <span class="ratingBlockTitleCls" runat="server" id="spanIntroductionNameComment">
                            </span>
                            <div runat="server" class='ratingContainerBlockDescription' id="divIntroductionDescriptionComment">
                            </div>
                        </div>
                        <div runat="server" id="divSupervisorComments" style="margin-top: 10px;">
                            <div runat="server" id="recommendBlock" visible="false" style="margin-bottom: 10px;">
                                Recommendation<br />
                                <asp:DropDownList AppendDataBoundItems="true" DataValueField="RecommendForId" DataTextField="Name"
                                    ID="ddlRecommendationList" runat="server" Width="200px">
                                    <asp:ListItem Text="" Value="-1"></asp:ListItem>
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlRecommendationList"
                                    Display="None" ErrorMessage="Recomendation is required." InitialValue="-1" ValidationGroup="Appraisal" />
                            </div>
                            <span class="" style="margin-left: 0px" runat="server" id="span2">Supervisor's Comment</span>
                            <div class="ratingButtons" style="margin-top: 10px" runat="server" id="divSupervisorCommentsDetails">
                                <asp:TextBox TextMode="MultiLine" ID="txtSupervisorComments" runat="server" Width="700px"
                                    Height="65px">
                                </asp:TextBox>
                                <div runat="server" style="margin-left: 0px" visible="false" class='ratingContainerBlockDescription'
                                    id="lblSupervisorComments">
                                </div>
                            </div>
                        </div>
                        <div runat="server" id="agreeDisagreeBlock" class="ratingButtons" visible="false" style="margin-top: 20px;
                            margin-bottom: 0px">
                            <asp:RadioButton runat="server" Text="I agree" Checked="true" GroupName="agree" ID="rdbIAgree" />
                            &nbsp;&nbsp;
                            <asp:RadioButton runat="server" Text="I do not agree" GroupName="agree" ID="rdbIDisAgree" />
                            <br />
                            <br />
                            <br />
                        </div>
                        <span class="" style="margin-left: 0px" runat="server" id="spanEmployeeComment">Summary
                            Comment</span>
                        <div class="ratingButtons" style="margin-top: 10px" runat="server" id="commentBlock">
                            <asp:TextBox TextMode="MultiLine" ID="txtComments" runat="server" Width="700px" Height="65px">
                            </asp:TextBox>
                        </div>
                        <div runat="server" style="margin-left: 0px" visible="false" class='ratingContainerBlockDescription'
                            id="acitivtySummary">
                        </div>
                    </div>
                    <%--Signature block--%>
                    <div runat="server" id="blockIntroduction" style="margin-top: 10px;">
                        <span class="ratingBlockTitleCls" runat="server" id="spanIntroductionName"></span>
                        <div runat="server" class='ratingContainerBlockDescription' id="divIntroductionDescription">
                        </div>
                    </div>
                    <div style="margin-top: -10px;" class="ratingContainerBlockDescription">
                        <span class="summaryCommmentSignatureNew" style="margin-left: 0px" runat="server" id="spanEmployeeSignature"
                            visible="true"></span><span class="summaryCommmentSignatureNew" runat="server" id="spanSupervisorSignature"
                                visible="false"></span><span style="margin-left: 0px" class="summaryCommmentSignatureNew"
                                    runat="server" id="spanEmployeeSecondSignature" visible="false"></span>
                        <div style="clear: both">
                        </div>
                    </div>
                </td>
            </tr>
        </table>
       <%-- comment history for self employee if company has comment view setting enabled--%>
        <div id="commentList" runat="server" style="margin-top: 10px;padding-left: 165px;clear:both;">     

    </div>
        <table>
            <tr>
                <%--<td style="padding-top: 0px; padding-left: 194px;">
                    <div class="ratingButtons">
                        <asp:Button ID="btnCalculate" Width="150px" OnClick="btnCalculate_Click" CssClass="update"
                            runat="server" Text="Calculate" ValidationGroup="Appraisal" />
                    </div>
                </td>--%>
                <td style="padding-top: 0px; padding-left: 200px;">
                    <div class="ratingButtons">
                        <%--<asp:Button ID="btnSave" Width="150px" OnClick="btnSave_Click" CssClass="update"
                            OnClientClick="valGroup='Appraisal';if(confirm('Are you sure, you want to submit the appraisal, after this submit you will not be able to change anything?')) return CheckValidation(); else return false;"
                            runat="server" Text="Save and Forward" ValidationGroup="Appraisal" />--%>

                        <ext:Button ID="btnSave" runat="server" Width="150"  Height="30"
                            Text="Save and Forward">
                            <DirectEvents>
                                <Click OnEvent="btnSave_Click">
                                    <Confirmation ConfirmRequest="true" Message="Are you sure, you want to submit the appraisal, after this submit you will not be able to change anything?" />
                                    <EventMask ShowMask="true" />
                                </Click>
                            </DirectEvents>
                            <Listeners>
                                <Click Handler="valGroup='Appraisal';return CheckValidation();" />
                            </Listeners>
                        </ext:Button>
                    </div>
                </td>
                <td   style='padding-top: 20px;'>
                <asp:LinkButton runat="server" ID="btnRevert" Visible="false" Text="Revert" OnClientClick='return ImportPopup12();' style='margin-left:10px' />
                </td>
                <td style='padding-top: 16px; display: none;'>
                    (
                    <asp:Label runat="server" ID="lblForwardTo" />
                    )
                </td>
                <%--<td style='    padding-top: 16px;padding-left:20px;'>
                    <asp:LinkButton runat="server" Text="Revert" />
                </td>--%>
            </tr>
        </table>
    </div>
    <script type="text/javascript">
        $('.rateit-reset').prop('title', 'Clear');
    </script>
</asp:Content>
