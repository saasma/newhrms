﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="QualificationCtl.ascx.cs" Inherits="Web.Employee.Appraisal.QualificationCtl" %>
<div id="blockEducationForBookmarking">
    </div>
<div runat="server" id="blockIntroduction">
    <span class="ratingBlockTitleCls" runat="server" id="spanIntroductionName"></span>
    <div runat="server" class='ratingContainerBlockDescription' id="divIntroductionDescription">
    </div>
</div>
<div style="clear: both; margin-bottom: 10px;">
</div>
<div >
    <table>
        <tr>
            <td style="padding-top: 10px">
                <span class="x-form-item-label" >Education</span>
                <ext:GridPanel ID="GridEducation" Width="1000" runat="server" Cls="itemgrid" Scroll="None">
                    <Store>
                        <ext:Store ID="StoreEducation" runat="server">
                            <Model>
                                <ext:Model ID="Model2" runat="server" IDProperty="EductionId">
                                    <Fields>
                                        <ext:ModelField Name="Course" Type="String" />
                                        <ext:ModelField Name="LevelName" Type="string" />
                                        <ext:ModelField Name="Order" Type="string" />
                                        <ext:ModelField Name="College" Type="string" />
                                        <ext:ModelField Name="Institution" Type="string" />
                                        <ext:ModelField Name="Country" Type="string" />
                                        <ext:ModelField Name="Percentage" Type="string" />
                                        <ext:ModelField Name="PassedYear" Type="string" />
                                        <ext:ModelField Name="IsEditable" Type="Int" />
                                        <ext:ModelField Name="ContainsFile" Type="Int" />
                                        <ext:ModelField Name="DivisionName" Type="String" />
                                    </Fields>
                                </ext:Model>
                            </Model>
                        </ext:Store>
                    </Store>
                    <ColumnModel>
                        <Columns>

                            <ext:Column ID="Column6" Width="200" runat="server" Text="Course Name" DataIndex="Course" Flex="1" Wrap="true" />
                            <ext:Column ID="Column15" Width="200" runat="server" Text="Level" DataIndex="LevelName" Wrap="true">
                            </ext:Column>

                            <ext:Column ID="Column16" Width="200" runat="server" Text="Institution" DataIndex="College" Wrap="true">
                            </ext:Column>
                            <ext:Column ID="Column81" runat="server" Width="100" Text="Passed Year" DataIndex="PassedYear" Wrap="true">
                            </ext:Column>
                            <ext:Column ID="Column17" runat="server" Width="100" Text="% / Grade" DataIndex="Percentage" Wrap="true">
                            </ext:Column>
                            <ext:Column ID="Column18" runat="server" Width="110" Text="Division" DataIndex="DivisionName" Wrap="true">
                            </ext:Column>

                        </Columns>
                    </ColumnModel>
                    <View>
                        <ext:GridView ID="GridView1" runat="server">
                        </ext:GridView>
                    </View>
                </ext:GridPanel>

                <div id="divEducationRating"  runat="server" style="padding-top: 10px; padding-bottom: 5px;">
                    <span runat="server" id="scoreEducation">Score : 0
                    </span>
                   


                </div>

            </td>
        </tr>
        <tr>
            <td style="padding-top: 10px">
                <span class="x-form-item-label">Geographical Location</span>
                <ext:GridPanel ID="gridTransferList" runat="server" Cls="itemgrid" Width="700px">
                    <Store>
                        <ext:Store ID="Store3" runat="server">
                            <Model>
                                <ext:Model ID="Model4" runat="server" IDProperty="BranchDepartmentId">
                                    <Fields>
                                        <ext:ModelField Name="Type" Type="string" />
                                        <ext:ModelField Name="FromBranch" Type="string" />
                                        <ext:ModelField Name="ToBranch" Type="string" />
                                        <ext:ModelField Name="FromDate" Type="string" />
                                    </Fields>
                                </ext:Model>
                            </Model>
                        </ext:Store>
                    </Store>
                    <ColumnModel>
                        <Columns>
                            <ext:Column ID="Column8" Sortable="false" MenuDisabled="true" runat="server" Text="Type"
                                Align="Left" Width="200" DataIndex="Type" />
                            <ext:Column ID="Column4" Width="200" Sortable="false" MenuDisabled="true" runat="server"
                                Text="From" Align="Left" DataIndex="FromBranch">
                            </ext:Column>
                            <ext:Column ID="Column5" Sortable="false" MenuDisabled="true" runat="server" Text="To"
                                Align="Left" Width="200" DataIndex="ToBranch" />
                            <ext:Column ID="Column1" Sortable="false" MenuDisabled="true" runat="server" Text="Date"
                                Align="Left" Width="100" DataIndex="FromDate" />
                        </Columns>
                    </ColumnModel>
                    <SelectionModel>
                        <ext:RowSelectionModel ID="RowSelectionModel2" runat="server" Mode="Single" />
                    </SelectionModel>
                </ext:GridPanel>


                <div id="divTransferBlock"  runat="server" style="padding-top: 10px; padding-bottom: 5px;">
                    <span runat="server" id="scoreLocation">Score : 0
                    </span>

                </div>
            </td>
        </tr>
        <tr>
            <td style="padding-top: 10px">

                <div class="panel-body" style="padding-left: 0px">
                    <span class="x-form-item-label">Seniority</span>
                    <ext:GridPanel StyleSpec="" Width="400" ID="ServiceHistory" runat="server"
                        Cls="itemgrid">
                        <Store>
                            <ext:Store ID="Store1" runat="server" AutoLoad="true">
                                <Model>
                                    <ext:Model ID="Model1" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="EIN" Type="String" />
                                            <ext:ModelField Name="Level" Type="String" />
                                            <ext:ModelField Name="StatusName" Type="String" />
                                            <ext:ModelField Name="FromDate" Type="String" />
                                            <ext:ModelField Name="FromDateEng" Type="Date" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>

                                <ext:Column ID="Column2" Sortable="false" MenuDisabled="true" Hidden="true" runat="server" Text="Position"
                                    Width="180" Align="Left" DataIndex="Level" />
                                <ext:Column ID="Column9" Sortable="false" MenuDisabled="true" runat="server" Text="Status"
                                    Width="110" Align="Left" DataIndex="StatusName" />
                                <ext:Column ID="Column10" Sortable="false" MenuDisabled="true" Flex="1" runat="server" Text="Date"
                                    Width="110" Align="Left" DataIndex="FromDate" />
                                <%--   <ext:DateColumn ID="colRolloutDate" runat="server" Align="Left" Text="English Date"
                                                        MenuDisabled="true" Sortable="false" Format="yyyy/MM/dd" DataIndex="FromDateEng"
                                                        Resizable="false" Draggable="false" Width="103">
                                                    </ext:DateColumn>--%>
                            </Columns>
                        </ColumnModel>
                        <SelectionModel>
                            <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" Mode="Single" />
                        </SelectionModel>
                    </ext:GridPanel>


                    <ext:GridPanel ID="gridLevelGradeHistory" StyleSpec="margin-top:15px;" runat="server" Width="600" Cls="itemgrid">
                                                    <Store>
                                                        <ext:Store ID="StorePreviousEmployment" runat="server">
                                                            <Model>
                                                                <ext:Model ID="Model3" runat="server">
                                                                    <Fields>
                                                                        <ext:ModelField Name="FromDate" Type="String" />
                                                                        <ext:ModelField Name="OldLevel" Type="string" />
                                                                        <ext:ModelField Name="PrevStepGrade" Type="string" />
                                                                        <ext:ModelField Name="NewLevel" Type="string" />
                                                                        <ext:ModelField Name="StepGrade" Type="string" />
                                                                        <ext:ModelField Name="ChangeType" Type="string" />
                                                                    </Fields>
                                                                </ext:Model>
                                                            </Model>
                                                        </ext:Store>
                                                    </Store>
                                                    <ColumnModel>
                                                        <Columns>
                                                            <ext:Column ID="Column3" runat="server" Text="From Date" DataIndex="FromDate" Width="100" />
                                                            <ext:Column ID="Column7" runat="server" Text="Old Level" DataIndex="OldLevel" Width="150">
                                                            </ext:Column>
                                                               <ext:Column ID="Column11" runat="server" Text="Old Grade" DataIndex="PrevStepGrade"
                                                                Width="80">
                                                            </ext:Column>
                                                            <ext:Column ID="Column12" runat="server" Text="New Level" Flex="1" DataIndex="NewLevel" Width="150">
                                                            </ext:Column>
                                                           
                                                               <ext:Column ID="Column13" runat="server" Text="New Grade" DataIndex="StepGrade" Width="80">
                                                            </ext:Column>

                                                        </Columns>
                                                    </ColumnModel>
                                                    <View>
                                                        <ext:GridView EnableTextSelection="true" />
                                                    </View>
                                                    <SelectionModel>
                                                        <ext:RowSelectionModel ID="RowSelectionModel3" runat="server" Mode="Single" />
                                                    </SelectionModel>
                                                </ext:GridPanel>

                    <div id="divHistoryBlock" runat="server" style="padding-top: 10px; padding-bottom: 5px;">
                        <span runat="server" id="scoreSeniority">Score : 0
                    </span>


                    </div>

                </div>
            </td>
        </tr>
    </table>
</div>
