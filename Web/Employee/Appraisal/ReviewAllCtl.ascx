﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ReviewAllCtl.ascx.cs"
    Inherits="Web.Employee.Appraisal.ReviewAllCtl" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Appraisal/UserControls/SummaryCommentBlock.ascx" TagName="SummaryCommentBlock"
    TagPrefix="uc1" %>
<%@ Register Src="~/Appraisal/UserControls/CompetencyBlock.ascx" TagName="CompetencyBlock"
    TagPrefix="uc1" %>
<%@ Register Src="~/Appraisal/UserControls/RatingEmployeeBlock.ascx" TagName="RatingEmployeeBlock"
    TagPrefix="uc2" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="EmployeeBookmarkMenu.ascx" TagName="EmployeeMenu" TagPrefix="uc3" %>
<%@ Register Src="~/Employee/Appraisal/EmployeeHistoryCtl.ascx" TagName="history"
    TagPrefix="uc1" %>
<%@ Register Src="~/Employee/Appraisal/QualificationCtl.ascx" TagName="QualificationBlock"
    TagPrefix="uc3" %>
<script src="../../css/ratingsrc/jquery.rateit.min.js" type="text/javascript"></script>
<link href="../../css/ratingsrc/rateit.css" rel="stylesheet" type="text/css" />
<div class="separator bottom" style="padding-bottom: 1px">
</div>
<uc2:MsgCtl ID="divMsgCtl" EnableViewState="false" Hide="true" runat="server" />
<uc2:WarningCtl ID="divWarningMsg" EnableViewState="false" Hide="true" runat="server" />
<script type="text/javascript">
 function ImportPopup12(efid) {

     revsertPopup('efid=' + efid);
     return false;
 }

 function hideRevert() {
    <%=btnRevert.ClientID %>.hide();
 }
</script>
<div class="innerLR" style="padding-left: 0px">
    <ext:ResourceManager ID="ResourceManager1" runat="server" DisableViewState="false"
        ScriptMode="Release" />
    <table>
        <tr>
            <td valign="top" style="padding-top: 16px;" id="tdHideOnPrint">
                <uc3:EmployeeMenu ID="EmployeeMenu1" runat="server" />
            </td>
            <td style="width: 10px">
            </td>
            <td valign="top" style="width: 1000px">
                <uc2:RatingEmployeeBlock ID="RatingEmployeeBlock1" runat="server" />
                <div style='padding-left: 0px; margin-bottom: 25px'>
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion2" href="#collapseOne2">View History
                                </a>
                            </h4>
                        </div>
                        <div id="collapseOne2" class="panel-collapse collapse in">
                            <uc1:history ID="history" runat="server" />
                        </div>
                    </div>
                </div>
                <div id="blockIntroduction">
                    <span class="ratingBlockTitleCls" runat="server" id="IntroductionName"></span>
                    <div runat="server" class='ratingContainerBlockDescription' id="IntroductionDescription">
                    </div>
                </div>
                <div id="blockObjective" runat="server">
                    <span class="ratingBlockTitleCls" runat="server" id="spanObjectiveName"></span>
                    <div runat="server" class='ratingContainerBlockDescription' id="divObjectiveDescription">
                    </div>
                </div>
                <uc3:QualificationBlock ID="blockQualificationAndEducation" runat="server" />
                <%--Core values--%>
                <uc1:CompetencyBlock Id="CompetencyBlock1" runat="server" />
                <uc1:SummaryCommentBlock ID="SummaryCommentBlock2" runat="server" />
            </td>
        </tr>
    </table>
    <div class="ratingButtons" style="margin-left: 198px">
        <%-- <asp:Button ID="btnSave" Width="150px" OnClick="btnSave_Click" CssClass="update"
            OnClientClick="valGroup='Appraisal';if(confirm('Are you sure, you want to submit the appraisal, after this submit you will not be able to change anything?')) return CheckValidation(); else return false;"
            runat="server" Text="Save and Forward" ValidationGroup="Appraisal" />--%>
        <table>
            <tr>
                <td>
                    <ext:Button ID="btnSave" runat="server" Width="150" Height="30" Text="Save and Forward">
                        <DirectEvents>
                            <Click OnEvent="btnSave_Click">
                                <Confirmation ConfirmRequest="true" Message="Are you sure, you want to submit the appraisal, after this submit you will not be able to change anything?" />
                                <EventMask ShowMask="true" />
                            </Click>
                        </DirectEvents>
                        <Listeners>
                            <Click Handler="valGroup='Appraisal';return CheckValidation();" />
                        </Listeners>
                    </ext:Button>
                </td>
                <td>
                    <ext:LinkButton ID="btnRevert" Visible="false" runat="server" Text="Revert" StyleSpec='margin-left:20px' />
                </td>
            </tr>
        </table>
        <%--<asp:LinkButton runat="server" Visible="false" ID="btnRevert" Text="Revert" OnClientClick='return ImportPopup12();' style='margin-left:10px' />--%>
    </div>
</div>
<script type="text/javascript">
    $('.rateit-reset').prop('title', 'Clear');
</script>
<style type="text/css">
    @media print
    {
        header
        {
            display: none;
        }
        #tdHideOnPrint, .ratingButtons
        {
            display: none;
        }
        .mainwrapper:before
        {
            border: none;
        }
    }
</style>
