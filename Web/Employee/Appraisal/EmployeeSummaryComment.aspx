﻿<%@ Page Title="Employee Appraisal" Language="C#" MasterPageFile="~/Master/EmployeeMS.Master"
    AutoEventWireup="true" CodeBehind="EmployeeSummaryComment.aspx.cs" Inherits="Web.Appraisal.EmployeeSummaryComment" %>

<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Appraisal/UserControls/CompetencyBlock.ascx" TagName="CompetencyBlock"
    TagPrefix="uc1" %>
<%@ Register Src="~/Appraisal/UserControls/RatingEmployeeBlock.ascx" TagName="RatingEmployeeBlock"
    TagPrefix="uc2" %>
<%@ Register Src="EmployeeMenu.ascx" TagName="EmployeeMenu" TagPrefix="uc3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../../css/ratingsrc/jquery.rateit.min.js" type="text/javascript"></script>
    <link href="../../css/ratingsrc/rateit.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
       

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
    <div class="separator bottom" style="padding-bottom: 1px">
    </div>
    <uc2:MsgCtl ID="divMsgCtl" EnableViewState="false" Hide="true" runat="server" />
    <uc2:WarningCtl ID="divWarningMsg" EnableViewState="false" Hide="true" runat="server" />
    <div class="innerLR" style="padding-left: 0px">
        <table>
            <tr>
                <td valign="top" style="padding-top: 16px;">
                    <uc3:EmployeeMenu Id="EmployeeMenu1" runat="server" />
                </td>
                <td style="width: 10px">
                </td>
                <td valign="top" style="width: 1000px">
                    <uc2:RatingEmployeeBlock Id="RatingEmployeeBlock1" runat="server" />
                    <div runat="server" id="blockIntroduction">
                        <span class="ratingBlockTitleCls" runat="server" id="spanIntroductionName"></span>
                        <div runat="server" class='ratingContainerBlockDescription' id="divIntroductionDescription">
                        </div>
                    </div>
                    <div runat="server" id="divSupervisorComments">
                        <span class="" style="margin-left: 0px" runat="server" id="span1">Supervisor's Comment</span>
                        <div class="ratingButtons" style="margin-top: 10px" runat="server" id="divSupervisorCommentsDetails">
                            <asp:TextBox TextMode="MultiLine" ID="txtSupervisorComments" runat="server" Width="700px"
                                Height="65px">
                            </asp:TextBox>
                        </div>
                    </div>
                    <div runat="server" id="agreeDisagreeBlock" class="ratingButtons" style="margin-top: 20px;
                        margin-bottom: 0px">
                        <asp:RadioButton runat="server" Text="I agree" Checked="true" GroupName="agree" ID="rdbIAgree" />
                        &nbsp;&nbsp;
                        <asp:RadioButton runat="server" Text="I do not agree" GroupName="agree" ID="rdbIDisAgree" />
                        <br />
                        <br />
                        <br />
                    </div>
                    <span class="" style="margin-left: 0px" runat="server" id="spanEmployeeComment">Summary
                        Comment</span>
                    <div class="ratingButtons" style="margin-top: 10px" runat="server" id="commentBlock">
                        <asp:TextBox TextMode="MultiLine" ID="txtComments" runat="server" Width="700px" Height="65px">
                        </asp:TextBox>
                    </div>
                    <div runat="server" style="margin-left: 0px" visible="false" class='ratingContainerBlockDescription'
                        id="acitivtySummary">
                    </div>
                    <div class="ratingButtons">
                        <asp:Button ID="btnSave" Width="150px" OnClick="btnSave_Click" CssClass="update"
                            OnClientClick="valGroup='Appraisal';return CheckValidation()" runat="server"
                            Text="Save" ValidationGroup="Appraisal" />
                        <asp:Button ID="btnNext" OnClientClick="valGroup='Appraisal';return CheckValidation()"
                            CssClass="save" OnClick="btnSave_Click" runat="server" Text="Next" />
                    </div>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
