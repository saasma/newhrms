﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EmployeeBookmarkMenu.ascx.cs"
    Inherits="Web.Employee.Appraisal.EmployeeBookmarkMenu" %>
<style type="text/css">
    .performanceUL
    {
        margin-left: 0px;
        padding:0px;
    }
    .performanceUL img
    {
        margin-right: 10px;
    }
    .performanceUL li
    {
        list-style-type: none;
        font-weight: normal;
        font-size: 13px;
        background-color: white;
        padding: 3px;
        margin-bottom: 2px;
        padding-left: 5px;
    }
    .performanceUL a
    {
        color: black !important;
        text-decoration: none;
        cursor: pointer;
    }
</style>
<ul class="performanceUL" runat="server" id="menuList">
    <li style="background-color: #47759E; color: White; font-size: 16px;">Performance Review</li>
    <li runat="server">
        <asp:Image runat="server" ImageUrl="~/images/appraisal_left_icon.png" /><a id="anchorIntroduction"
            runat="server" href="#blockIntroduction">1. Introduction</a></li>
    <li runat="server" id="liObjective">
        <asp:Image ID="Image1" runat="server" ImageUrl="~/images/appraisal_left_icon.png" /><a
            id="anchorObjective" runat="server" href="#blockObjective">2. Objective</a></li>
    <li runat="server" id="liActivity">
        <asp:Image ID="Image2" runat="server" ImageUrl="~/images/appraisal_left_icon.png" /><a
            id="anchorEmpActivity" runat="server" href="#ctl00_mainContent_ctl02_blockActivity">3.
            Activity</a></li>
    <li runat="server" id="liEducationQualification">
        <asp:Image ID="Image8" runat="server" ImageUrl="~/images/appraisal_left_icon.png" /><a
            id="anchorEducationQualification" runat="server" href="#blockEducationForBookmarking"></a></li>
    <li runat="server" id="liCompetency">
        <asp:Image ID="Image3" runat="server" ImageUrl="~/images/appraisal_left_icon.png" /><a
            id="anchorComptency" runat="server" href="#ctl00_mainContent_ctl02_block">4. Competencies</a></li>
    <li id="liTarget" runat="server">
        <asp:Image ID="Image7" runat="server" ImageUrl="~/images/appraisal_left_icon.png" /><a
            id="anchorTarget" runat="server" href="#ctl00_mainContent_ctl02_block">5. Targets</a></li>
    <li runat="server">
        <asp:Image ID="Image4" runat="server" ImageUrl="~/images/appraisal_left_icon.png" /><a
            id="anchorQuestion" runat="server" href="#ctl00_mainContent_ctl02_blockQuestionnare">6.
            Questionnaire</a></li>
    <li id="liSupervisorReview" runat="server">
        <asp:Image ID="Image5" runat="server" ImageUrl="~/images/appraisal_left_icon.png" /><a
            id="anchorReview" runat="server" href="#ctl00_mainContent_ctl02_blockReview">7.
            Supervisor Review</a></li>
    <li id="liSignatureComment" runat="server">
        <asp:Image ID="Image6" runat="server" ImageUrl="~/images/appraisal_left_icon.png" /><a
            id="anchorSummary" runat="server" href="#ctl00_mainContent_ctl03_blockSummary">8.
            Summary Comment</a></li>
    <%--   <li runat="server"><a id="a7" runat="server" href="EmployeeSignature.aspx">8. Signature</a></li>--%>
</ul>
