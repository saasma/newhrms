﻿<%@ Page Title="Revert Step" Language="C#" MasterPageFile="~/Master/ForEmployeePopupPage.Master"
    AutoEventWireup="true" CodeBehind="RevertImport.aspx.cs" Inherits="Web.Employee.RevertImport" %>

<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" src="<%= ResolveUrl("~/Employee/override.js?v=") + Web.Helper.WebHelper.Version %>"></script>
    <script type="text/javascript">




        function closePopup() {
            window.opener.close();
            window.close();
         
    }

   

    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="mainContent" runat="server">
    <ext:ResourceManager ID="ResourceManager1" runat="server" />
   
    <div class="popupHeader" style="margin-top: -10px; height: 50px; padding-top: 10px;
        padding-left: 20px; color: White;">
        <h4 runat="server" id="header">
            Appraisal Revert for :
        </h4>
    </div>
    <div class=" marginal" style='margin-top: 0px'>
        <uc2:MsgCtl ID="divMsgCtl" EnableViewState="false" Hide="true" Width="390px" runat="server" />
        <uc2:WarningCtl ID="divWarningMsg" EnableViewState="false" Hide="true" runat="server" />
        <table class="fieldTable">
            <tr>
                <td>
                    <ext:DisplayField ID="disp" runat="server" FieldLabel="Current Status" />
                </td>
            </tr>
            <tr>
                <td runat="server" id="employeeTD">
                    <ext:ComboBox ID="cmbRevertStatus" Width="200" runat="server" ValueField="StepID"
                        DisplayField="StepName" FieldLabel="Revert to" LabelAlign="top" LabelSeparator=""
                        ForceSelection="true" QueryMode="Local">
                        <Store>
                            <ext:Store ID="Store2" runat="server">
                                <Model>
                                    <ext:Model ID="Model6" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="StepID" Type="String" />
                                            <ext:ModelField Name="StepName" Type="String" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                    </ext:ComboBox>
                    <asp:RequiredFieldValidator Display="None" ID="valcmbContition" runat="server" ValidationGroup="SaveUpdate"
                        ControlToValidate="cmbRevertStatus" ErrorMessage="Revert to is required." />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <ext:TextArea ID="txtNotes" runat="server" FieldLabel="Notes" LabelSeparator="" LabelAlign="Top"
                        Rows="3" Cols="55" />
                    <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator1" runat="server"
                        ValidationGroup="SaveUpdate" ControlToValidate="txtNotes" ErrorMessage="Notes is required." />
                </td>
            </tr>
            <tr>
                <td valign="bottom" colspan="2">
                    <div class="popupButtonDiv">
                        <ext:Button runat="server" Width="100" Height="25" ID="btnConfirmRevert" Text="<i></i>Revert">
                            <DirectEvents>
                                <Click OnEvent="btnConfirmRevert_Click">
                                    <Confirmation ConfirmRequest="true" Message="Are you sure, you want to revert the appraisal?" />
                                    <EventMask ShowMask="true" />
                                </Click>
                            </DirectEvents>
                            <Listeners>
                                <Click Handler="valGroup = 'SaveUpdate'; return CheckValidation();">
                                </Click>
                            </Listeners>
                        </ext:Button>
                        <ext:LinkButton runat="server" StyleSpec="padding:0px;" ID="LinkButton2" Cls="btnFlatLeftGap"
                            Text="<i></i>Cancel">
                            <Listeners>
                                <Click Handler="#{windowRevert}.hide();">
                                </Click>
                            </Listeners>
                        </ext:LinkButton>
                    </div>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
