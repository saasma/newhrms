﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using System.Xml;
using BLL.BO;
using System.IO;

namespace Web.Appraisal
{
    public partial class EmployeeActivity : BasePage
    {

        protected override void OnInit(EventArgs e)
        {

            Initialise();

        }

       


        public int GetFormID()
        {
            if (string.IsNullOrEmpty(Request.QueryString["fid"]))
                return 0;
            return int.Parse(Request.QueryString["fid"]);
        }
        public int GetEmployeeFormID()
        {
            if (string.IsNullOrEmpty(Request.QueryString["efid"]))
                return 0;
            return int.Parse(Request.QueryString["efid"]);
        }

        public void btnSave_Click(object sender, EventArgs e)
        {
            AppraisalEmployeeForm empForm = AppraisalManager.GetEmployeeAppraisaleForm(GetEmployeeFormID());
            if (this.FileDocumentUpload.HasFile)
            {
                int fileSize = FileDocumentUpload.PostedFile.ContentLength;
                string UserFileName = this.FileDocumentUpload.FileName;
                string ServerFileName = Guid.NewGuid().ToString() + Path.GetExtension(UserFileName);
                string relativePath = HttpContext.Current.Server.MapPath(@"~/Uploads/Document/") + ServerFileName;
                this.FileDocumentUpload.PostedFile.SaveAs(relativePath);
                empForm.ActivitySummaryFileName = UserFileName;
                empForm.ActivitySummaryFileUrl = "~/Uploads/Document/";
                empForm.FileFormat = Path.GetExtension(this.FileDocumentUpload.FileName).Replace(".", "").Trim();
                empForm.FileType = this.FileDocumentUpload.PostedFile.ContentType;
                empForm.ActivitySummaryServerFileName = ServerFileName;
            }

            AppraisalForm form = AppraisalManager.getFormInstanceByID(GetFormID());

            if (btnSave.Visible)
            {
                if (empForm.EmployeeId == SessionManager.CurrentLoggedInEmployeeId && CKEditor1.Visible)
                {
                    if (string.IsNullOrEmpty(CKEditor1.Text.Trim()))
                    {
                        divWarningMsg.InnerHtml = "Summary is required.";
                        divWarningMsg.Hide = false;
                        CKEditor1.Focus();
                        return;
                    }
                }



                activityBlock.AppraisalFormID = GetFormID();
                AppraisalValue value = new AppraisalValue();// activityBlock.GetEmployeeActivityList();

                double markingValue = 0;



                //if (int.TryParse(supervisorMarkingValue.Text.Trim(), out markingValue))
                //    value.SupervisorRating = markingValue;

                // marking type
                if (form.ActivityMarkingType == 2)
                {
                    if (appraiseeMarkingValue.Visible == true && empForm.Status == (int)AppraisalStatus.Saved)
                    {
                        if (appraiseeMarkingValue.Text.Trim() == "")
                        {
                            divWarningMsg.InnerHtml = "Marking is required.";
                            divWarningMsg.Hide = false;
                            return;
                        }

                        if (double.TryParse(appraiseeMarkingValue.Text.Trim(), out markingValue))
                            value.SelfActivityMarks = markingValue;
                        else
                        {
                            divWarningMsg.InnerHtml = "Invalid marks.";
                            divWarningMsg.Hide = false;
                            return;
                        }

                        if (value.SelfActivityMarks > form.ActivityMaximumMarks)
                        {
                            divWarningMsg.InnerHtml = "Marks can not be greater then " + form.ActivityMaximumMarks + ".";
                            divWarningMsg.Hide = false;
                            return;
                        }
                        if (value.SelfActivityMarks < 0)
                        {
                            divWarningMsg.InnerHtml = "Marks can not be negative.";
                            divWarningMsg.Hide = false;
                            return;
                        }
                    }
                    else if (supervisorMarkingValue.Visible = true && empForm.Status == (int)AppraisalStatus.SaveAndSend)
                    {
                        if (supervisorMarkingValue.Text.Trim() == "")
                        {
                            divWarningMsg.InnerHtml = "Marking is required.";
                            divWarningMsg.Hide = false;
                            return;
                        }

                        if (double.TryParse(supervisorMarkingValue.Text.Trim(), out markingValue))
                        {
                            value.SupervisorActivityMarks = markingValue;
                            
                        }
                        else
                        {
                            divWarningMsg.InnerHtml = "Invalid marks.";
                            divWarningMsg.Hide = false;
                            return;
                        }

                        if (value.SupervisorActivityMarks > form.ActivityMaximumMarks)
                        {
                            divWarningMsg.InnerHtml = "Marks can not be greater then " + form.ActivityMaximumMarks + ".";
                            divWarningMsg.Hide = false;
                            return;
                        }
                        if (value.SupervisorActivityMarks < 0)
                        {
                            divWarningMsg.InnerHtml = "Marks can not be negative.";
                            divWarningMsg.Hide = false;
                            return;
                        }
                    }
                    //if (value.SupervisorRating > form.ActivityMaximumMarks)
                    //{
                    //    divWarningMsg.InnerHtml = "Marks can not be greater then " + form.ActivityMaximumMarks + ".";
                    //    divWarningMsg.Hide = false;
                    //    return;
                    //}

                    //if (value.SupervisorRating < 0)
                    //{
                    //    divWarningMsg.InnerHtml = "Marks can not be negative.";
                    //    divWarningMsg.Hide = false;
                    //    return;
                    //}
                }
                value.SupervisorComment = txtSupervisorComment.Text.Trim();

                AppraisalManager.UpdateEmployeeActivitySummary(GetEmployeeFormID(),
                    CKEditor1.Text.Trim(), value.SelfActivityMarks, value.SupervisorActivityMarks, value.SupervisorComment, empForm);

                divMsgCtl.InnerHtml = "Activity saved.";
                divMsgCtl.Hide = false;
                btnDownload.Text = empForm.ActivitySummaryFileName;
            }

            if (sender == btnNext)
            {
                string nextPage = "EmployeeQualificationAndExpirence.aspx";

                if (form.HideHistorySection)
                {

                    nextPage = "CoreValues.aspx";

                    if (form.HideCompetencyCoreValues != null && form.HideCompetencyCoreValues.Value)
                    {
                        if (AppraisalManager.HasTargetForForm(GetFormID()))
                            nextPage = "TargetList.aspx";
                        else
                            nextPage = "Questions.aspx";
                    }
                }

                Response.Redirect(string.Format("{2}?fid={0}&efid={1}",
                    GetFormID(), GetEmployeeFormID(), nextPage), true);

            }
        }

        protected void btnDownload_Click(object sendeer, DirectEventArgs e)
        {
            //int activityId = int.Parse(hdnActivityId.Text);
            DownloadFile();
            
        }


        protected void DownloadFile()
        {
            AppraisalEmployeeForm empForm = AppraisalManager.GetEmployeeAppraisaleForm(GetEmployeeFormID());
            AppraisalEmployeeForm obj = AppraisalManager.GetEmployeeAppraisaleForm(empForm.EmployeeId, GetFormID());
            string path = Context.Server.MapPath(obj.ActivitySummaryFileUrl + obj.ActivitySummaryServerFileName);
            string contentType = obj.FileType;
            string name = obj.ActivitySummaryFileName;
            byte[] bytes = File.ReadAllBytes(path);
            Context.Response.Clear();
            Context.Response.ClearHeaders();
            Context.Response.AppendHeader("Content-Disposition", "attachment; filename=\"" + name + "\"");
            Context.Response.ContentType = contentType;
            Context.Response.BinaryWrite(bytes);
            Context.Response.End();
        }

        public void Initialise()
        {
            AppraisalForm form = AppraisalManager.getFormInstanceByID(GetFormID());
            if (form != null)
            {
               
                spanIntroductionName.InnerHtml = form.ActivityName;
                divIntroductionDescription.InnerHtml = (form.ActivityDescription);
                //btnNext.OnClientClick = string.Format("window.location='CoreValues.aspx?fid={0}&efid={1}';return false;",
                //    GetFormID(), GetEmployeeFormID());
            }

            AppraisalEmployeeForm empForm = AppraisalManager.GetEmployeeAppraisaleForm(GetEmployeeFormID());
            if (empForm.ActivitySummaryFileName != null)
                btnDownload.Text = empForm.ActivitySummaryFileName;
            else
                btnDownload.Hide();

            if (empForm.Status != (int)AppraisalStatus.Saved)
            {
                //acitivtySummary.Visible = true;
                //activityBlock.Visible = true;
            }

            activityBlock.AppraisalFormID = GetFormID();
            activityBlock.LoadActivityDetails(true);
            FileDocumentUpload.Hide();
            
            if (empForm.EmployeeId == SessionManager.CurrentLoggedInEmployeeId)
            {
                if(empForm.Status==(int)AppraisalStatus.Saved)
                FileDocumentUpload.Show();

                if (empForm.Status != (int)AppraisalStatus.Saved)
                {
                    btnSave.Visible = false;
                    acitivtySummary.InnerHtml = empForm.ActivitySummary;
                    acitivtySummary.Visible = true;
                    CKEditor1.Visible = false;
                }
                else
                {
                    CKEditor1.Text = empForm.ActivitySummary;
                }
            }
            else
            {
                acitivtySummary.InnerHtml = empForm.ActivitySummary;
                acitivtySummary.Visible = true;
                CKEditor1.Visible = false;

                if (empForm.Status != (int)AppraisalStatus.SaveAndSend)
                {
                    btnSave.Visible = false;
                }
            }



            if (form.AllowActivitySelfRating == false)
            {
                appraiseeBlock.Style["visibility"] = "hidden";

                if (empForm.Status == (int)AppraisalStatus.Saved)
                {
                    blockRating.Visible = false;
                }
            }

            // for no marking case
            if(form.ActivityMarkingType == 0)
            {
                appraiseeBlock.Visible = false;
                supervisorBlock.Visible = false;
                X.AddScript("document.getElementById('ctl00_mainContent_ctl02_containerBlockActivity1').style.display='none';");
            }

            spanRatingLevel.InnerHtml = new EmployeeManager().GetById(empForm.EmployeeId).Name + "'s Marks";

            if (empForm.ActivitySelfRating != null)
            {
                appraiseeMarkingValue.Text = empForm.ActivitySelfRating.ToString();
            }
            if (empForm.ActivitySupervisorRating != null)
            {
                supervisorMarkingValue.Text = empForm.ActivitySupervisorRating.ToString();
               
            }

            txtSupervisorComment.Text = empForm.ActivitySupervisorComment == null ? "" : empForm.ActivitySupervisorComment.ToString();

            // show hide marking block
            if (SessionManager.CurrentLoggedInEmployeeId == empForm.EmployeeId)
            {
                if (empForm.Status == (int)AppraisalStatus.Saved)
                {

                    supervisorBlock.Visible = false;
                    txtSupervisorComment.Visible = false;
                }
                else
                {
                    supervisorMarkingValue.Enabled = false;
                    appraiseeMarkingValue.Enabled = false;
                    txtSupervisorComment.Enabled = false;
                }

            }
            else
            {
                if (empForm.Status == (int)AppraisalStatus.SaveAndSend)
                {
                    supervisorMarkingValue.Enabled = true;
                }
                else
                {
                    supervisorMarkingValue.Enabled = false;
                    txtSupervisorComment.Enabled = false;
                }
                appraiseeMarkingValue.Enabled = false;
            }
           

        }

        




    }
}