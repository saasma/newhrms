﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using BLL.Base;
using DAL;
using BLL.Manager;

namespace Web.Employee.Appraisal
{
    public partial class EmployeeHistoryCtl : BaseUserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public int GetEmployeeFormID()
        {
            if (string.IsNullOrEmpty(Request.QueryString["efid"]))
                return 0;
            return int.Parse(Request.QueryString["efid"]);
        }

        public void Page_PreRender(object sender,EventArgs e)
        {

            AppraisalEmployeeForm empForm = AppraisalManager.GetEmployeeAppraisaleForm(GetEmployeeFormID());

            int empId = empForm.EmployeeId;


            EmployeeManager empMgr = new EmployeeManager();
            lblStatus.Html = empMgr.GetAllCurrentStatusForAppraisal(empId);
            EducationCtl.ShowEmpty = true;
            EducationCtl.HideButtonBlock();
            EducationCtl.Initialise(empId);



            // salary details

            //lblSalaryDetails.Html = new PayManager().GetHTMLSalaryDetailsForAppraisal(empId);

            // branch Transfer details
            gridTransferList.Store[0].DataSource = BranchManager.GetTransferListForAppraisal(empId);
            gridTransferList.Store[0].DataBind();

            List<HTraining> _HTraining = NewHRManager.GetTrainingByEmployeeID(empId);
            foreach (HTraining item in _HTraining)
            {
                if (item.TrainingFromEngDate != null)
                {
                    item.TrainingFrom = item.TrainingFromEngDate.Value.ToShortDateString();
                }
                else
                    item.TrainingFrom = "";
            }
            this.Store2.DataSource = _HTraining;
            this.Store2.DataBind();

            //AppraisalEmployeeForm empForm = AppraisalManager.GetEmployeeAppraisaleForm(GetEmployeeFormID());
            AppraisalRollout rollOut = AppraisalManager.GetRolloutByFormID(empForm.RolloutID.Value);

            //lblEduDetails.Text = rollOut.EducationDetails;
            lblNotes.Text = rollOut.Notes;


            List<GetEmployeeIncomeListForAppraisalResult> incomes = AppraisalManager.GetEmployeeIncomesForAppraisals(empId);
            if (incomes == null)
                rowCurrentPay.Visible = false;
            else
            {
                gridCurrentPay.Store[0].DataSource = incomes;
                gridCurrentPay.Store[0].DataBind();
            }



        }
    }
}