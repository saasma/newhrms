﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using System.Xml;

namespace Web.Appraisal
{
    public partial class EmployeeObjective : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();

            }
        }
        public int GetEmployeeFormID()
        {
            if (string.IsNullOrEmpty(Request.QueryString["efid"]))
                return 0;
            return int.Parse(Request.QueryString["efid"]);
        }
        public int GetFormID()
        {
            if (string.IsNullOrEmpty(Request.QueryString["fid"]))
                return 0;
            return int.Parse(Request.QueryString["fid"]);
        }
        public void Initialise()
        {

           
            AppraisalForm form = AppraisalManager.getFormInstanceByID(GetFormID());
            if (form != null)
            {
                // Introudction
               
                spanIntroductionName.InnerHtml = form.ObjectiveName;
                divIntroductionDescription.InnerHtml = (form.ObjectiveDescription);


                if (AppraisalManager.HasActivitySummaryForForm(GetFormID()))
                    btnNext.OnClientClick = string.Format("window.location='EmployeeActivity.aspx?fid={0}&efid={1}';return false;",
                        GetFormID(), GetEmployeeFormID());
                else
                    btnNext.OnClientClick = string.Format("window.location='CoreValues.aspx?fid={0}&efid={1}';return false;",
                        GetFormID(), GetEmployeeFormID());


                
            }
        }

        




    }
}