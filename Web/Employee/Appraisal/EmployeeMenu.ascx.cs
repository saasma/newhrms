﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using BLL.Base;
using DAL;
using BLL.Manager;
using BLL;

namespace Web.Employee.Appraisal
{
    public partial class EmployeeMenu : BaseUserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadMenu();
            }
        }
        public int GetEmployeeFormID()
        {
            if (string.IsNullOrEmpty(Request.QueryString["efid"]))
                return 0;
            return int.Parse(Request.QueryString["efid"]);
        }
        public void LoadMenu()
        {
            // hide Review,Performance & Summary Comment for first step
            AppraisalEmployeeForm empForm = AppraisalManager.GetEmployeeAppraisaleForm(GetEmployeeFormID());
            AppraisalForm form = empForm.AppraisalForm;

            anchorIntroduction.InnerHtml = form.IntroductionName;
            if (form.HideObjectiveBlock != null && form.HideObjectiveBlock.Value)
                liObjective.Visible = !form.HideObjectiveBlock.Value;
            anchorObjective.InnerHtml = form.ObjectiveName;
            anchorEmpActivity.InnerHtml = form.ActivityName;

            if (form.HideCompetencyCoreValues != null && form.HideCompetencyCoreValues.Value)
                liCompetency.Visible = false;

            anchorComptency.InnerHtml = form.CompentencyName;
            anchorQuestion.InnerHtml = form.QuestionnareName;
            anchorReview.InnerHtml = form.ReviewName;
            //anchorSummary.InnerHtml = form.SummaryName;
            if (!string.IsNullOrEmpty(form.SignationName))
                anchorSignature.InnerHtml = form.SignationName;
            anchorTarget.InnerHtml = form.TargetName;

            if (form.HideTarget != null && form.HideTarget.Value)
                liTarget.Visible = false;

            if (form.HideSupervisorReview != null && form.HideSupervisorReview.Value)
                liReview.Visible = false;

            if (form.HideActivitySummary != null && form.HideActivitySummary.Value)
                liActivity.Visible = false;

            if (form.HideHistorySection)
            {
                liQualificationAndExpn.Visible = false;
            }
            else
                anchorQualificationAndExp.InnerHtml = form.HistoryName;



            if (empForm != null)
            {
                if (empForm.Status == 0)
                {
                    liReview.Visible = false;
                   
                    //liSummaryComment.Visible = false;

                    //signature.InnerHtml = "&nbsp;Signature";
                }

                // permission check if can view the document or not
                if (empForm.EmployeeId != SessionManager.CurrentLoggedInEmployeeId)
                {

                    // allow admin use to view details
                    if (SessionManager.CurrentLoggedInEmployeeId == 0)
                    {
                    }
                    else
                    {
                        if (!CommonManager.CanViewDocument(FlowTypeEnum.Appraisal, empForm.Status.Value,
                            empForm.EmployeeId, SessionManager.CurrentLoggedInEmployeeId, empForm.AppraisalEmployeeFormID    ))
                        {
                            NotPermissionContent();
                            return;
                        }
                    }
                }
            }

            string pageUrl = CurrentPageUrl.ToLower();


            foreach (Control ctl in menuList.Controls)
            {
                foreach (Control item in ctl.Controls)
                {
                    HtmlAnchor anchor = item as HtmlAnchor;
                    if (anchor != null)
                    {

                        {
                            anchor.Attributes["href"] += "?fid=" + Request.QueryString["fid"] +
                                "&efid=" + Request.QueryString["efid"];
                            //anchor.Visible = true;

                            if (anchor.HRef.ToLower().Contains(pageUrl))
                            {
                                ((HtmlGenericControl)anchor.Parent)
                                    .Style["background-color"] = "#A3BDD5";
                                anchor
                                    .Style["color"] = "white!important";
                            }
                        }
                    }
                }
            }
        }
    }
}