﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using System.Xml;
using BLL.BO;
using Utils.Web;
namespace Web.Appraisal
{
    public partial class EmployeeQualificationAndExpirence : BasePage
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            //if (!IsPostBack && !X.IsAjaxRequest)
            {
                Initialise();

            }
        }

        public int GetFormID()
        {
            if (string.IsNullOrEmpty(Request.QueryString["fid"]))
                return 0;
            return int.Parse(Request.QueryString["fid"]);
        }
        public int GetEmployeeFormID()
        {
            if (string.IsNullOrEmpty(Request.QueryString["efid"]))
                return 0;
            return int.Parse(Request.QueryString["efid"]);
        }

        public void btnSave_Click(object sender, EventArgs e)
        {

            string nextPage = "CoreValues.aspx";

            AppraisalForm form = AppraisalManager.getFormInstanceByID(GetFormID());


            if (form.HideCompetencyCoreValues != null && form.HideCompetencyCoreValues.Value)
            {
                if (AppraisalManager.HasTargetForForm(GetFormID()))
                    nextPage = "TargetList.aspx";
                else
                    nextPage = "Questions.aspx";
            }


            Response.Redirect(string.Format("{2}?fid={0}&efid={1}",
                GetFormID(), GetEmployeeFormID(), nextPage), true);

        }

        public void Initialise()
        {


            

            AppraisalForm form = AppraisalManager.getFormInstanceByID(GetFormID());
            AppraisalEmployeeForm empForm = AppraisalManager.GetEmployeeAppraisaleForm(GetEmployeeFormID());


          

           

            // show hide marking block
            if (SessionManager.CurrentLoggedInEmployeeId == empForm.EmployeeId)
            {
                if (empForm.Status == (int)AppraisalStatus.Saved)
                {

                    
                }
                else if(empForm.Status >= (int)AppraisalStatus.SaveAndSend)
                {
                  
                }

            }
            else
            {
                if (empForm.Status == (int)AppraisalStatus.SaveAndSend)
                {
                   ;

                    //btnSave.Visible = true;
                }
                else
                {
                    
                }
            }
           
            
        }




    }
}