﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using BLL.Base;
using DAL;
using BLL.Manager;
using BLL;
using System.Drawing;

namespace Web.Employee.Appraisal
{
    public partial class EmployeeDetailsScoreCtl : BaseUserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Initialise();
            }
        }

        public void Initialise()
        {
            AppraisalEmployeeForm empForm = AppraisalManager.GetEmployeeAppraisaleForm(GetEmployeeFormID());
            if (empForm != null)
            {
                AppraisalForm form = empForm.AppraisalForm;
                if (form != null)
                {

           

                }
            }

            /// hide emp score for civil not needed as employee does not rate 
            if (CommonManager.CompanySetting.WhichCompany == WhichCompany.Civil)
                gridEmployee.Visible = false;
        }

        public int GetEmployeeFormID()
        {
            if (string.IsNullOrEmpty(Request.QueryString["efid"]))
                return 0;
            return int.Parse(Request.QueryString["efid"]);
        }

       
        public void Page_PreRender(object sender,EventArgs e)
        {

            //LoadScore();
        }

        public void LoadScore()
        {
            AppraisalEmployeeForm empForm = AppraisalManager.GetEmployeeAppraisaleForm(GetEmployeeFormID());
            AppraisalForm form = empForm.AppraisalForm;

            int empId = empForm.EmployeeId;

            bool hasEmployeeRatingEnabled = false;
            if (form.AllowActivitySelfRating.Value || form.AllowCompetencySelfRating.Value || form.AllowQuestionSelfRating.Value)
                hasEmployeeRatingEnabled = true;

            if (empForm.Status == (int)AppraisalStatus.Saved)
            {
                gridSupervisor.Visible = false;
                //return;
            }

            

            LoadNewScore(empForm);
        }

        public void LoadNewScore(AppraisalEmployeeForm empForm)
        {
            AppraisalForm form = empForm.AppraisalForm;

            //List<AppraisalFormPoint> points = AppraisalManager.GetPoints(form.AppraisalFormID);

            List<AppraisalFormPoint> empPoints = new List<AppraisalFormPoint>();
            List<AppraisalFormPoint> supPoints = new List<AppraisalFormPoint>();

            List<AppraisalFormPoint> emp = new List<AppraisalFormPoint>();
            List<AppraisalFormPoint> sup = new List<AppraisalFormPoint>();

            List<AppraisalFormGrading> gradingList = BLL.BaseBiz.PayrollDataContext.AppraisalFormGradings
               .Where(x => x.AppraisalFormRef_ID == form.AppraisalFormID).ToList();


            // Activity
            if (form.HideActivitySummary != null && form.HideActivitySummary.Value) { }
            else
            {
                if (form.ActivityMarkingType != 0)
                {
                    AppraisalFormPoint activityEmp = new AppraisalFormPoint { Name = "Activity", IsParent = true, IsEditable = true };
                    empPoints.Add(activityEmp);
                    AppraisalFormPoint activitySup = new AppraisalFormPoint { Name = "Activity", IsParent = true, IsEditable = true };
                    supPoints.Add(activitySup);
                }
            }

            int index = 1;
            double empTotal = 0, supTotal = 0;

            if (form.HideCompetencyCoreValues != null && form.HideCompetencyCoreValues.Value) { }
            else
            {

                #region Competency Score

                // add Competencies
                List<AppraisalCategory> comptencyCategoryList = NewHRManager.GetAppraisalFromCompetencyCategory(form.AppraisalFormID);
                List<AppraisalEmployeeFormCompetency> employeeComptencyList = AppraisalManager.GetEmployeeCompetenchList(empForm.AppraisalEmployeeFormID);

                AppraisalFormPoint comptencyCatEmp = new AppraisalFormPoint { Name = "Competencies", ShowEmptyInPointColumn = true, ShowEmptyInScoreColumn = true, IsParent = true };
                empPoints.Add(comptencyCatEmp);
                AppraisalFormPoint comptencyCatSup = new AppraisalFormPoint { Name = "Competencies", ShowEmptyInPointColumn = true, ShowEmptyInScoreColumn = true, IsParent = true };
                supPoints.Add(comptencyCatSup);

               


                // calculate Competency category total
                foreach (AppraisalEmployeeFormCompetency item in employeeComptencyList)
                {
                    AppraisalFormCompetency dbComp = BLL.BaseBiz.PayrollDataContext.AppraisalFormCompetencies
                        .FirstOrDefault(x => x.CompetencyRef_ID == item.CompetencyRef_ID);
                    AppraisalCompetency dbActualComp = BLL.BaseBiz.PayrollDataContext.AppraisalCompetencies
                        .FirstOrDefault(x => x.CompetencyID == item.CompetencyRef_ID);

                    AppraisalCategory category = comptencyCategoryList.FirstOrDefault(x => x.CategoryID == dbActualComp.CategoryID);
                    if (category != null)
                    {
                        category.EmployeePoints += (item.SelfRating == null ? 0 : item.SelfRating.Value);
                        category.SupervisorPoints += (item.ManagerRating == null ? 0 : item.ManagerRating.Value);
                    }
                }

                foreach (AppraisalCategory competencyCategory in comptencyCategoryList)
                {

                    AppraisalFormPoint compEmp = new AppraisalFormPoint
                    {
                        Name = index + ". " + competencyCategory.Name,
                        Type = (int)AppraisalBlockType.Competency,
                        SourceId = competencyCategory.CategoryID,
                        Points = competencyCategory.EmployeePoints,
                        IsEditable = true,
                        ShowEmptyInPointColumn = false,
                        ShowEmptyInScoreColumn = true
                    };
                    AppraisalFormPoint compSup = new AppraisalFormPoint
                    {
                        Name = index + ". " + competencyCategory.Name,
                        Type = (int)AppraisalBlockType.Competency,
                        SourceId = competencyCategory.CategoryID,
                        Points = competencyCategory.SupervisorPoints,
                        IsEditable = true,
                        ShowEmptyInPointColumn = false,
                        ShowEmptyInScoreColumn = true
                    };

                    empTotal += competencyCategory.EmployeePoints;
                    supTotal += competencyCategory.SupervisorPoints;

                    empPoints.Add(compEmp);
                    supPoints.Add(compSup);

                    index++;
                }

                AppraisalFormPoint comptencyTotalCatEmp = new AppraisalFormPoint { Name = "Total", Points = empTotal, Percentage = GetValue((empForm.EmployeeCompetencyTotalScore == null ? 0 : empForm.EmployeeCompetencyTotalScore.Value)), IsParent = false, IsCompetencyTotal = true };
                empPoints.Add(comptencyTotalCatEmp);
                AppraisalFormPoint comptencyTotalCatSup = new AppraisalFormPoint { Name = "Total", Points = supTotal, Percentage = GetValue((empForm.SupervisorCompetencyTotalScore == null ? 0 : empForm.SupervisorCompetencyTotalScore.Value)), IsParent = false, IsCompetencyTotal = true };
                supPoints.Add(comptencyTotalCatSup);

                #endregion
            }


            #region Question Score

            //if (form.PerformanceQuestionnaireValue != null && form.PerformanceQuestionnaireValue.Value != 0)
            {

                // add Quistionate
                List<AppraisalFormQuestionnaireCategory> categoryList = AppraisalManager.GetQuestionCategoryList(form.AppraisalFormID);
                List<AppraisalEmployeeFormQuestionnaire> empQuestionList = AppraisalManager.GetEmployeeQuestionList(empForm.AppraisalEmployeeFormID);

                index = 1;

                AppraisalFormPoint questionCatEmp = new AppraisalFormPoint { Name = "Questionnaire", ShowEmptyInPointColumn = true, ShowEmptyInScoreColumn = true, IsParent = true };
                empPoints.Add(questionCatEmp);
                AppraisalFormPoint questionCatSup = new AppraisalFormPoint { Name = "Questionnaire", ShowEmptyInPointColumn = true, ShowEmptyInScoreColumn = true, IsParent = true };
                supPoints.Add(questionCatSup);

                empTotal = 0; supTotal = 0;

                // calculate Category total
                foreach (AppraisalEmployeeFormQuestionnaire item in empQuestionList)
                {
                    AppraisalFormQuestionnaire dbComp = BLL.BaseBiz.PayrollDataContext.AppraisalFormQuestionnaires
                        .FirstOrDefault(x => x.QuestionnareID == item.QuestionnaireRef_ID);
                    AppraisalFormQuestionnaireCategory category = categoryList.FirstOrDefault(x => x.CategoryID == dbComp.CategoryID);

                    category.EmployeePoints += (item.SelfRating == null ? 0 : item.SelfRating.Value);
                    category.SupervisorPoints += (item.ManagerRating == null ? 0 : item.ManagerRating.Value);
                }

                foreach (AppraisalFormQuestionnaireCategory category in categoryList)
                {
                    AppraisalFormPoint compEmp = new AppraisalFormPoint
                    {
                        Name = index + ". " + category.Name,
                        Type = (int)AppraisalBlockType.Question,
                        SourceId = category.CategoryID,
                        Points = category.EmployeePoints,
                        IsEditable = true,
                        ShowEmptyInPointColumn = false,
                        ShowEmptyInScoreColumn = true
                    };
                    AppraisalFormPoint compSup = new AppraisalFormPoint
                    {
                        Name = index + ". " + category.Name,
                        Type = (int)AppraisalBlockType.Question,
                        SourceId = category.CategoryID,
                        Points = category.SupervisorPoints,
                        IsEditable = true,
                        ShowEmptyInPointColumn = false,
                        ShowEmptyInScoreColumn = true
                    };

                    empTotal += category.EmployeePoints;
                    supTotal += category.SupervisorPoints;

                    empPoints.Add(compEmp);
                    supPoints.Add(compSup);

                    index++;
                }

                AppraisalFormPoint TotalCatEmp = new AppraisalFormPoint { Name = "Total", Points = empTotal, Percentage = GetValue(empForm.FinalEmpQuestionScore), IsParent = false, IsQuestionaireTotal = true };
                empPoints.Add(TotalCatEmp);

                AppraisalFormPoint TotalCatSup = new AppraisalFormPoint { Name = "Total", Points = supTotal, Percentage = GetValue(empForm.FinalSupQuestionScore), IsParent = false, IsQuestionaireTotal = true };
                supPoints.Add(TotalCatSup);

                // in case of civil additional points can be added by ceo 
                if (empForm.AdditionalQuestionnaireScore != null && empForm.AdditionalQuestionnaireScore != 0)
                {
                    // show questionnnaire score
                    TotalCatSup.Percentage = double.Parse(GetValue(empForm.FinalSupQuestionScoreWithoutAddition).ToString("n2"));

                    // ceo score
                    supPoints.Add(new AppraisalFormPoint { IsParent = false, IsQuestionaireTotal = true , Name="Additional Points", Percentage = GetValue(empForm.AdditionalQuestionnaireScore) });

                    // question score
                    supPoints.Add(new AppraisalFormPoint { IsParent = false, IsQuestionaireTotal = true, Name = "Grand Total", Percentage = GetValue(empForm.FinalSupQuestionScore) });
                }

               

            }
            #endregion


            #region Target

            // Targets
            if (form.HideTarget != null && form.HideTarget.Value) { }
            else
            {
                AppraisalFormPoint targetEmp = new AppraisalFormPoint { Name = "Target", Points = GetValue(empForm.TargetScore), Percentage = GetValue(empForm.TargetScore), IsParent = true, IsEditable = true };
                empPoints.Add(targetEmp);
                AppraisalFormPoint targetSup = new AppraisalFormPoint { Name = "Target", Points = GetValue(empForm.SupervisorTargetScore), Percentage = GetValue(empForm.SupervisorTargetScore), IsParent = true, IsEditable = true };
                supPoints.Add(targetSup);
            }

            #endregion


            #region Education qualification like history

            // Targets
            if (form.HideHistorySection) { }
            else
            {
                // 1. Education qualification
                AppraisalFormPoint education = new AppraisalFormPoint { Name = "Education ", Points = GetValue(empForm.EducationScore), Percentage = GetValue(empForm.EducationPercent), IsParent = true, IsEditable = true };
                supPoints.Add(education);

                // 2. Transfer
                AppraisalFormPoint transfer = new AppraisalFormPoint { Name = "Geographical Location", Points = GetValue(empForm.LocationScore), Percentage = GetValue(empForm.LocationTransferPercent), IsParent = true, IsEditable = true };
                supPoints.Add(transfer);

                // 3. Work history
                AppraisalFormPoint workhistory = new AppraisalFormPoint { Name = "Seniority", Points = GetValue(empForm.SeniorityScore), Percentage = GetValue(empForm.WorkHistorySeniorityPercent), IsParent = true, IsEditable = true };
                supPoints.Add(workhistory);
            }

            #endregion


            // Total Score
            AppraisalFormPoint empTotalScore = new AppraisalFormPoint { Name = "Total Score", Percentage = GetValue(empForm.SelfEmployeeRatingScore), ShowEmptyInPointColumn = true, IsParent = true, IsTotalGroup = true };
            empPoints.Add(empTotalScore);
            AppraisalFormPoint supTotalScore = new AppraisalFormPoint { Name = "Total Score", Percentage = GetValue(empForm.SupervisorRatingScore), ShowEmptyInPointColumn = true, IsParent = true, IsTotalGroup = true };
            supPoints.Add(supTotalScore);

            // Grade, if there is value in grade column then only display
           
            if (gradingList.Any(x => !string.IsNullOrEmpty(x.Grade)))
            {
                AppraisalFormPoint empGrade = new AppraisalFormPoint { Name = "Grade", PercentageValue = empForm.EmployeeGrade, ShowEmptyInPointColumn = true, IsParent = true, IsTotalGroup = true, IsTextRow = true };
                empPoints.Add(empGrade);
                AppraisalFormPoint supGrade = new AppraisalFormPoint { Name = "Grade", PercentageValue = empForm.SupervisorGrade, ShowEmptyInPointColumn = true, IsParent = true, IsTotalGroup = true, IsTextRow = true };
                supPoints.Add(supGrade);
            }


            // Total Score
            AppraisalFormPoint empPerformanceSummary = new AppraisalFormPoint { Name = "Performance Summary", PercentageValue = empForm.EmployeeGradeSummary, ShowEmptyInPointColumn = true, IsParent = true, IsTotalGroup = true, IsTextRow = true };
            empPoints.Add(empPerformanceSummary);
            AppraisalFormPoint supPerformanceSummary = new AppraisalFormPoint { Name = "Performance Summary", PercentageValue = empForm.SupervisorGradeSummary, ShowEmptyInPointColumn = true, IsParent = true, IsTotalGroup = true, IsTextRow = true };
            supPoints.Add(supPerformanceSummary);

            gridEmployee.DataSource = empPoints;
            gridEmployee.DataBind();

            gridSupervisor.DataSource = supPoints;
            gridSupervisor.DataBind();

            //gridSupRating.Store[0].DataSource = supPoints;
            //gridSupRating.Store[0].DataBind();



        }

        public double GetValue(double? value)
        {
            if (value == null)
                return 0;
            return Convert.ToDouble(value.Value.ToString("n2"));
        }

        protected void gridEmployee_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                GridView HeaderGrid = (GridView)sender;
                GridViewRow HeaderGridRow = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert);
                TableCell HeaderCell = new TableCell();
                if (sender as GridView == gridEmployee)
                    HeaderCell.Text = "Employee Self Rating";
                else
                    HeaderCell.Text = "Supervisor Rating";
                HeaderCell.ColumnSpan = 3;
                HeaderCell.ForeColor = Color.White;
                HeaderCell.BackColor = Color.FromArgb(72, 117, 158);//#48759E
                HeaderGridRow.Cells.Add(HeaderCell);

              
                (sender as GridView).Controls[0].Controls.AddAt(0, HeaderGridRow);

            } 
        }
    }
}