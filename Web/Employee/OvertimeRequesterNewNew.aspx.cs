﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL.Entity;
using BLL;
using System.IO;
using BLL.BO;
using Utils.Helper;
using Bll;
namespace Web.Employee
{
    public partial class OvertimeRequesterNewNew : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest && !IsPostBack)
            {
                Initialize();
            }

            JavascriptHelper.AttachPopUpCode(Page, "overtimeAdd", "OvertimeRequestAddPopup.aspx", 700, 500);
            JavascriptHelper.AttachPopUpCode(Page, "overtimeEdit", "OvertimeRequestEditPopup.aspx", 500, 550);
        }

        private void Initialize()
        {
            cmbType.SetValue("1");

            LoadEmployees();
        }

        public void LoadEmployees()
        {
            int type = 1;


            if (!string.IsNullOrEmpty(cmbType.SelectedItem.Text))
                type = int.Parse(cmbType.SelectedItem.Value);

            int totalRecords = 0;

            List<GetOvertimeRequestByIDResult> list = OvertimeManager.GetLeaveRequestsByEmpID(SessionManager.CurrentLoggedInEmployeeId, type, 1, 99999,-1, -1, ref totalRecords);

            gridOverTimeReq.GetStore().DataSource = list;
            gridOverTimeReq.GetStore().DataBind();
        }

        protected void MyData_Refresh(object sender, StoreReadDataEventArgs e)
        {
            LoadEmployees();
        }

        protected void btnLoad_Click(object sender, DirectEventArgs e)
        {
            LoadEmployees();
        }

        protected void btnLoadRef_Click(object sender, DirectEventArgs e)
        {
            LoadEmployees();
        }
        
        public void btnExport_Click(object sender, EventArgs e)
        {
            int type = 1;

            if (!string.IsNullOrEmpty(cmbType.SelectedItem.Text))
                type = int.Parse(cmbType.SelectedItem.Value);

            int totalRecords = 0;

            List<GetOvertimeRequestByIDResult> list = OvertimeManager.GetLeaveRequestsByEmpID(SessionManager.CurrentLoggedInEmployeeId, type, 1, 99999,-1, -1, ref totalRecords);

            Dictionary<string, string> title = new Dictionary<string, string>();
            title["Overtime Request Report"] = "";

            ExcelHelper.ExportToExcel("Overtime Request", list,
                new List<String>() { "RequestID", "EmployeeID", "SupervisorName", "SupervisorID", "Status", "Duration", "TotalRows", "RowNumber" },
                new List<String>() { },
                new Dictionary<string, string>() { { "StartTime", "Start Time" }, { "EndTime", "End Time" }, { "CheckInTime", "In Time" }, { "CheckOutTime", "Out Time" }, { "DurationModified", "Duration" }, { "ApprovedTime", "Approved Time" }, { "StatusModified", "Status" } },
                new List<string>() { },
                new Dictionary<string, string>() { },
                new List<string> { "Date", "StartTime", "EndTime", "CheckInTime", "CheckOutTime", "DurationModified", "ApprovedTime", "Reason", "StatusModified" });
        }



    }
}