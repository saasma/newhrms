﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL;
using BLL.Manager;
using Web.ReportDataSetTableAdapters;
using Web.Master;
using BLL;
using Web.CP.Report.Templates.Pay.Detail;
using Utils;
using BLL.Base;
using System.IO;
using Utils.Calendar;
using BLL.BO;
using Ext.Net;

namespace Web.Employee
{
    public class HolidayClass //: BasePage
    {

        public List<GetHolidaysForAttendenceResult> GetHolidays(DateTime FromDate, DateTime ToDate)
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated == false)
            {
                return new List<GetHolidaysForAttendenceResult>();
            }


            DateTime startDateEng = FromDate;
            DateTime endDateEng = ToDate;
            List<GetHolidaysForAttendenceResult> list = null;

            HolidayManager holidayMgr = new HolidayManager();
            list = holidayMgr.GetMonthsHolidaysForAttendence(0, 0, SessionManager.CurrentCompanyId, 0, startDateEng, endDateEng,0);

            List<GetHolidaysForAttendenceResult> nonbranchholidaylist = new List<GetHolidaysForAttendenceResult>();
            // check & remove if branch holiday applies & not in that branch
            foreach (GetHolidaysForAttendenceResult branchHoliday in list.Where(x => x.BranchIDList != ""))
            {
                if (branchHoliday.DateEng != null && !string.IsNullOrEmpty(branchHoliday.BranchIDList))
                {
                    int branchId = BranchManager.GetEmployeeCurrentBranch(SessionManager.CurrentLoggedInEmployeeId, branchHoliday.DateEng.Value);
                    //if (branchId != branchHoliday.BranchId)
                    if (!branchHoliday.BranchList.Contains(branchId))
                        nonbranchholidaylist.Add(branchHoliday);
                }
            }

            // remove female holiday for male-
            if (SessionManager.CurrentLoggedInEmployeeId != 0)
            {
                int? gender = EmployeeManager.GetEmployeeGender(SessionManager.CurrentLoggedInEmployeeId);
                // if gender not femaly then remove femaly holidays from list
                if (gender != 0)
                {
                    list.RemoveAll(x => x.Type == 0);
                }
            }

            foreach (GetHolidaysForAttendenceResult holidayToDelete in nonbranchholidaylist)
            {
                list.Remove(holidayToDelete);
            }


            // If calendar is nepali then set nepali date for current month
            if (SessionManager.IsEnglish == false)
            {
                GetHolidaysForAttendenceResult dateRange = new GetHolidaysForAttendenceResult();

                dateRange.Id = 0;

                try
                {

                    CustomDate date1 = new CustomDate(startDateEng.Day, startDateEng.Month, startDateEng.Year, true);
                    date1 = CustomDate.ConvertEngToNep(date1);


                    CustomDate date2 = new CustomDate(endDateEng.Day, endDateEng.Month, endDateEng.Year, true);
                    date2 = CustomDate.ConvertEngToNep(date2);


                    string[] yearNos = new string[4];
                    yearNos[0] = date1.Year.ToString()[0].ToString();
                    yearNos[1] = date1.Year.ToString()[1].ToString();
                    yearNos[2] = date1.Year.ToString()[2].ToString();
                    yearNos[3] = date1.Year.ToString()[3].ToString();

                    dateRange.Abbreviation
                        = Resources.NepaliCalendar.ResourceManager.GetString(DateHelper.GetMonthName(date1.Month, false))
                        + ", " +
                        Resources.NepaliCalendar.ResourceManager.GetString("N" + yearNos[0]) +
                        Resources.NepaliCalendar.ResourceManager.GetString("N" + yearNos[1]) +
                        Resources.NepaliCalendar.ResourceManager.GetString("N" + yearNos[2]) +
                        Resources.NepaliCalendar.ResourceManager.GetString("N" + yearNos[3])
                        ;

                    yearNos[0] = date2.Year.ToString()[0].ToString();
                    yearNos[1] = date2.Year.ToString()[1].ToString();
                    yearNos[2] = date2.Year.ToString()[2].ToString();
                    yearNos[3] = date2.Year.ToString()[3].ToString();

                    dateRange.Abbreviation
                        += "  -  " + Resources.NepaliCalendar.ResourceManager.GetString(DateHelper.GetMonthName(date2.Month, false))
                        + ", " +
                        Resources.NepaliCalendar.ResourceManager.GetString("N" + yearNos[0]) +
                        Resources.NepaliCalendar.ResourceManager.GetString("N" + yearNos[1]) +
                        Resources.NepaliCalendar.ResourceManager.GetString("N" + yearNos[2]) +
                        Resources.NepaliCalendar.ResourceManager.GetString("N" + yearNos[3])
                        ;
                }
                catch { }
                list.Add(dateRange);
            }

            return list;
        }


    }
}