<%@ Page Title="Attendance Details" Language="C#" MaintainScrollPositionOnPostback="true"
    MasterPageFile="~/Master/EmployeeExt.Master" AutoEventWireup="true" CodeBehind="OtherEmployeeAttendance.aspx.cs"
    Inherits="Web.Employee.OtherEmployeeAttendance" %>

<%@ Register Src="~/Attendance/UserControl/EmployeeAtteCtl.ascx" TagName="ReportContainer"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
<asp:ScriptManager ID="ScriptManager1" runat="server" ScriptMode="Release">
        <Services>
            <asp:ServiceReference Path="~/PayrollService.asmx" />
        </Services>
    </asp:ScriptManager>
    <h4>
Employee Attendance Details</h4>
    <uc1:ReportContainer runat="server" PageType="Supervisor"   id="report" />
    
</asp:Content>
