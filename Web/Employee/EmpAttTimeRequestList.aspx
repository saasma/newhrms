﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EmpAttTimeRequestList.aspx.cs"
    Inherits="Web.Employee.EmpAttTimeRequestList" %>

<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/FooterCtl.ascx" TagName="FooterCtl" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/HeaderCtl.ascx" TagName="HeaderCtl" TagPrefix="uc3" %>
<%@ Register Src="~/UserControls/LogoutCtl.ascx" TagName="LogoutCtl" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/TopMenu.ascx" TagName="TopMenu" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head id="Head1" runat="server">
    <meta http-equiv="x-ua-compatible" content="IE=8">
    <title>My Attendance Request</title>
    <link id="Link5" runat="server" rel="stylesheet" href="~/payrollmenu_files/mbcsmbmcp.css"
        type="text/css" />
    <link id="Link1" rel="stylesheet" type="text/css" href="<%= ResolveUrl("~/css/core.css?v=") + Web.Helper.WebHelper.Version %>" />
    <link id="Link2" rel="Stylesheet" type="text/css" href="<%= ResolveUrl("~/Styles/calendar/calendar.css?v=") + Web.Helper.WebHelper.Version %>" />
    <link id="Link4" runat="server" rel="stylesheet" href="~/Styles/design.css" />
    <ext:ResourcePlaceHolder ID="ResourcePlaceHolder1" runat="server" Mode="Script" />
    <style type="text/css">
        
        .details
        {
            padding-top: 10px;
        }
        .details tr td
        {
            padding-top: 4px;
        }
        .leavetype .leaveTitle
        {
            width: 128px;
        }
        a.x-datepicker-eventdate
        {
            border: 1px solid #a3bad9;
            padding: 1px; /* background-color: Cyan;*/
        }
        
        
        .icon-delete
        {
            background-image: url("../images/delet.png") !important;
        }
        
        .x-form
        {
            padding: 10px 10px 2px !important;
        }
        .x-form-item
        {
            margin-bottom: 6px;
        }
        .x-form-item label.x-form-item-label
        {
            padding-top: 1px;
        }
        .topgap
        {
            padding-top: 5px;
        }
        thead td, th
        {
            border: 0px solid;
        }
        
        #CompanyX.startTime_Container .x-form-field-wrap
        {
            margin-top:-5px!important;
        }
        
        <%--half day type--%>
        #CompanyX.chkHalfDayType_Container .x-form-item-label
        {
            padding-top:3px!important;
        }
        
        .x-form-spinner-splitter{visibility:hidden!important;}
        #mbmcpebul_table{margin-top:0px!important;}
       .bodypart {
            margin: 0 auto!important;}
           /*hide calendar icon*/
.ext-cal-ic-rem {display:none;}

  
        .itemList {
  margin: 0px;padding:0px;
}
.x-window-body-default {
  background: #E8F1FF;
}

    </style>
    <script type="text/javascript" src="<%= ResolveUrl("~/Employee/common.js?v=") + Web.Helper.WebHelper.Version %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/Employee/override.js?v=") + Web.Helper.WebHelper.Version %>"></script>
    <script type="text/javascript">

     var currentMonth = null;
     var eventWindow = null;
     var txtStartDate = null;
     var txtEndDate = null;
     var btnShow = null;
     var holidayList = null; 
     var hdnRequestID = null;

     Ext.onReady(
        function () {

            eventWindow = <%=eventWindow.ClientID %>;
            txtStartDate = <%=txtStartDate.ClientID %>;
            txtEndDate = <%=txtEndDate.ClientID %>;
            btnShow = <%=btnShow.ClientID %>;
            hdnRequestID = <%=hdnRequestID.ClientID %>;

        });

        

        var loadHoliday = function (month, year) {
            Ext.net.Mask.show();
            Ext.net.DirectMethod.request({
                url: "../LeaveRequestService.asmx/GetHolidays",
                cleanRequest: true,
                json: true,
                params: {
                    month: month,
                    year: year
                },
                success: function (result) {
                    holidayList = result;
                    //Ext.Msg.alert("Json Message", result);
                    CompanyX.DatePicker1.highlightDates(result);
                    Ext.net.Mask.hide();

                    //find the range case
                    for (i = 0; i < result.length; i++) {
                        if (result[i].Id == 0) {
                            //CompanyX.lblCalendarTitle.setText(result[i].Abbreviation);
                            break;
                        }
                    }
                },
                failure: function (result) { Ext.net.Mask.hide(); }
            });
        };
 
        

        function eventSelected(cal, rec, el,el1,el2) {
        
            if(rec.data.Notes == 'P')
            {
                txtStartDate.setValue(rec.data.StartDate);
                txtEndDate.setValue(rec.data.EndDate);
                <%= hdnRequestID.ClientID %>.setValue('');
            }
            else
            {
         
                <%= hdnRequestID.ClientID %>.setValue(rec.data.EventId);
            }
            <%= btnShow.ClientID %>.fireEvent('click');
        }

    var CommandHandlerClear = function(command, record){
           var GridPanelStore = <%=gridAttRequest.ClientID %>.getStore();
                if(command=="Delete")
                {
                    record.data.InTimeString = "";
                    record.data.InNote = "";
                    record.data.OutTimeString = "";
                    record.data.OutNote = "";
                    record.data.InTimeDT = null;
                    record.data.OutTimeDT = null;
                }
                 GridPanelStore.commitChanges;
                  <%=gridAttRequest.ClientID %>.getView().refresh(false);
           };

               
        var CalculateWorkHours = function(e1,e2,record)
        {
            var overNightShift = false;            

            if(record.data.InTimeDT == null || record.data.OutTimeDT == null)
            {
                return;
            }

            if(record.data.OvernightShift != null && record.data.OvernightShift == true)
            {
                overNightShift = true;
            }

            if(record.data.InTimeDT != '' && record.data.OutTimeDT != '')
            {
                var strTime = Ext.util.Format.date(record.data.InTimeDT, 'g:i a')
                var eTime = Ext.util.Format.date(record.data.OutTimeDT, 'g:i a')


                var diffTime = calculateTotalMinutes(eTime,overNightShift,true) - calculateTotalMinutes(strTime, overNightShift, false)

                if(diffTime <= 0)
                {
                    alert('End time must be greater than Start time.');
                    return "";
                }

                return (Math.floor(diffTime/60) + ":" + (diffTime%60));
              
            }
        };


        function calculateTotalMinutes(time, overNightShift, endTime)
        {
            var parts = time.split(/ |:/);
            var totalHours = 0;

            if(parts[2] == 'pm')
            {
               if(parseInt(parts[0], 10) == 12)
               {
                    return (12 * 60 + parseInt(parts[1], 10));
               }
               else
               {
                    return (12 * 60 + parseInt(parts[0], 10) * 60 + parseInt(parts[1], 10));
               }
            }
            else
            {
                if(overNightShift == true && endTime == true)
                {
                    if(parseInt(parts[0], 10) == 12)
                    {
                        return (parseInt(parts[0], 10) * 60 + parseInt(parts[1], 10)) + 12*60;
                    }
                    else
                    {
                        return (parseInt(parts[0], 10) * 60 + parseInt(parts[1], 10)) + 24*60;
                    }
                }
                else
                {
                    return (parseInt(parts[0], 10) * 60 + parseInt(parts[1], 10));
                }
            }
    
            //return (parts[2] == 'pm' * 12 * 60) + parseInt(parts[0], 10) * 60 + parseInt(parts[1], 10);
        }


        var CommandHandler = function(value, command,record,p2) {
            <%= hdnRequestID.ClientID %>.setValue(record.data.RequestID);
            if(command == 'Edit')
            { 
                 <%= btnShow.ClientID %>.fireEvent('click');
            }
            else if(command == 'Delete')
            {
                <%= btnDelete.ClientID %>.fireEvent('click');
            }
       };

       var RemoveItemLine = function (column, command, record, recordIndex, cellIndex) {
            var store = this.grid.store;
            store.remove(record);
        };

    var onShow = function (toolTip, grid) {
            var view = grid.getView(),
                store = grid.getStore(),
                record = view.getRecord(view.findItemByChild(toolTip.triggerElement)),
                column = view.getHeaderByCell(toolTip.triggerElement),
                data = record.get(column.dataIndex);
            
            if(column.dataIndex == 'InNote' || column.dataIndex == 'OutNote')
            {
                toolTip.update(data);
            }
            else
            {
                toolTip.update('');
            }   
        };
      
    </script>
</head>
<body style="margin: 0px">
    <form id="Form1" runat="server">
    <ext:ResourceManager ID="ResourceManager1" runat="server" DisableViewState="true"
        ShowWarningOnAjaxFailure="false" ScriptMode="Release" Namespace="CompanyX" />
    <uc3:HeaderCtl ID="HeaderCtl1" runat="server" />
    <ext:Hidden ID="hdnRequestID" runat="server" Cls="hidden-inputs" />
    <ext:LinkButton ID="btnShow" runat="server" Hidden="true" Cls="hidden-inputs">
        <DirectEvents>
            <Click OnEvent="btnShow_Click">
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:LinkButton>
    <ext:LinkButton ID="btnDelete" runat="server" Hidden="true" Cls="hidden-inputs">
        <DirectEvents>
            <Click OnEvent="btnDelete_Click">
                <EventMask ShowMask="true" />
                <Confirmation ConfirmRequest="true" Message="Are you sure, you want to delete the time request?" />
            </Click>
        </DirectEvents>
    </ext:LinkButton>
    <ext:DateField LabelSeparator="" runat="server" Hidden="true" ID="txtStartDate" Width="180"
        Cls="hidden-inputs">
    </ext:DateField>
    <ext:DateField LabelSeparator="" runat="server" Hidden="true" ID="txtEndDate" Width="180"
        Cls="hidden-inputs">
    </ext:DateField>
    <div id="bodypart">
        <div class="bodypart">
            <%-- <ext:ToolTip ID="toolTipForCalendar" CloseAction="Hide" runat="server" Target="={#{CalendarPanel1}.body}"
                Delegate=".ext-cal-evt" Calendar="={#{CalendarPanel1}}">
                <Listeners>
                    <Show Fn="attachToolTip" />
                </Listeners>
            </ext:ToolTip>--%>
            <h3 style='margin-top: 20px; margin-bottom: 0px; font-size: 16px;'>
                My Attendance Requests</h3>
            <h4 style='margin-top: 5px; margin-bottom: 5px;' runat="server" id="notes">
                Note : In case of Missing Check In or/and Check Out, and Card Missed. In case of
                other cases refer <a href='EmpAttTimeCommentRequestList.aspx'>"Late Attendance Request"</a>
            </h4>
            <div style="clear: both">
            </div>
            <table>
                <tr>
                    <td colspan="3">
                        <ext:Label runat="server" Height="30" ID="lblCalendarTitle" Text="&nbsp;" />
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <ext:DatePicker ID="DatePicker1" runat="server" Width="219" StyleSpec="border:1px solid #A3BAD9;">
                            <Listeners>
                                <Select Fn="CompanyX.setStartDate" Scope="CompanyX" />
                            </Listeners>
                            <Plugins>
                                <ext:GenericPlugin ID="HighlightDates1" runat="server" InstanceName="Ext.ux.DatePicker.HighlightDates" />
                            </Plugins>
                        </ext:DatePicker>
                    </td>
                    <td valign="top" style="padding-left: 10px">
                        <ext:LinkButton ID="lnkAssign" runat="server" StyleSpec="  margin-bottom: 10px;"
                            Text="Request Attendance">
                            <%--<Listeners>
                                <Click Handler="#{eventWindow}.center();#{eventWindow}.show();" />
                            </Listeners>--%>
                            <DirectEvents>
                                <Click OnEvent="lnkAssign_Click">
                                    <EventMask ShowMask="true" />
                                </Click>
                            </DirectEvents>
                        </ext:LinkButton>
                        <ext:CalendarPanel ShowNavBar="false" ShowDayView="false" ShowWeekView="false" ID="CalendarPanel1"
                            HeaderAsText="true" Header="true" Height="450" Width="972" runat="server" ActiveIndex="2"
                            Border="true" Margins="0">
                            <CalendarStore ID="GroupStore1" runat="server">
                                <%-- <Calendars>
                                    <ext:CalendarModel CalendarId="1" Title="Present" />
                                    <ext:CalendarModel CalendarId="2" Title="Request" />
                                    <ext:CalendarModel CalendarId="3" Title="Approved" />
                                    <ext:CalendarModel CalendarId="4" Title="Rejected" />
                                </Calendars>--%>
                            </CalendarStore>
                            <EventStore ID="EventStore1" runat="server" NoMappings="true">
                                <Proxy>
                                    <ext:AjaxProxy Url="../LeaveRequestService.asmx/GetTimeRequests" Json="true">
                                        <ActionMethods Read="POST" />
                                        <Reader>
                                            <ext:JsonReader Root="d" />
                                        </Reader>
                                    </ext:AjaxProxy>
                                </Proxy>
                                <Mappings>
                                    <ext:ModelField Name="StartDate" Type="Date" DateFormat="M$" />
                                    <ext:ModelField Name="EndDate" Type="Date" DateFormat="M$" />
                                </Mappings>
                                <Parameters>
                                    <ext:StoreParameter Name="StartDate" Mode="Raw" Value="CompanyX.CalendarPanel1.layout.activeItem.lastRenderStart" />
                                    <ext:StoreParameter Name="EndDate" Mode="Raw" Value="CompanyX.CalendarPanel1.layout.activeItem.lastRenderEnd" />
                                </Parameters>
                                <Listeners>
                                    <BeforeLoad Handler="Ext.net.Mask.show();" />
                                    <Load Handler="Ext.net.Mask.hide();" />
                                </Listeners>
                            </EventStore>
                            <MonthView ID="MonthView1" runat="server" ShowHeader="true" ShowWeekLinks="false"
                                ShowWeekNumbers="true">
                            </MonthView>
                            <Listeners>
                                <ViewChange Fn="CompanyX.viewChange" Scope="CompanyX" />
                                <EventClick Fn="eventSelected" />
                                <DayClick Fn="CompanyX.dayClickTimeAtt" Scope="CompanyX" />
                                <RangeSelect Fn="CompanyX.rangeSelectTimeAtt" Scope="CompanyX" />
                                <%--<EventClick  Fn="CompanyX.record.show" Scope="CompanyX" />--%>
                                <%-- <EventAdd    Fn="CompanyX.record.addFromEventDetailsForm" Scope="CompanyX" />
                                <EventUpdate Fn="CompanyX.record.updateFromEventDetailsForm" Scope="CompanyX" />
                                <EventDelete Fn="CompanyX.record.removeFromEventDetailsForm" Scope="CompanyX" />--%>
                            </Listeners>
                        </ext:CalendarPanel>
                    </td>
                    <td valign="top">
                    </td>
                </tr>
            </table>
            <%--  </Items>
    </ext:Viewport>--%>
            <div style="margin-top: 15px; font-size: 15px;">
                My Attendance Requests</div>
            <ext:TabPanel ID="TabPanel1" runat="server" MinHeight="500" Width="1253">
                <Items>
                    <ext:Panel ID="Panel1" runat="server" Title="Pending">
                        <Content>
                            <ext:GridPanel StyleSpec="margin-top:15px;" ID="gridPending" runat="server" Cls="itemgrid"
                                Scroll="None" Width="840">
                                <Store>
                                    <ext:Store ID="Store2" runat="server">
                                        <Model>
                                            <ext:Model ID="Model1" runat="server" IDProperty="RequestID">
                                                <Fields>
                                                    <ext:ModelField Name="RequestID" Type="String" />
                                                    <ext:ModelField Name="StartDateEng" Type="Date" />
                                                    <ext:ModelField Name="EndDateEng" Type="Date" />
                                                    <ext:ModelField Name="WorkDays" Type="string" />
                                                    <ext:ModelField Name="SubmittedOnEng" Type="Date">
                                                    </ext:ModelField>
                                                    <ext:ModelField Name="StatusName" Type="string" />
                                                    <ext:ModelField Name="Status" Type="Int" />
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <ColumnModel>
                                    <Columns>
                                        <ext:DateColumn ID="colStartDate" runat="server" Align="Right" Text="From" Width="100"
                                            MenuDisabled="true" Sortable="false" Format="yyyy/MM/dd" DataIndex="StartDateEng">
                                        </ext:DateColumn>
                                        <ext:DateColumn ID="colEndDate" runat="server" Align="Right" Text="To" Width="100"
                                            MenuDisabled="true" Sortable="false" Format="yyyy/MM/dd" DataIndex="EndDateEng">
                                        </ext:DateColumn>
                                        <ext:Column ID="colWorkDays" Sortable="false" MenuDisabled="true" runat="server"
                                            Text="Work Days" Width="100" Align="Center" DataIndex="WorkDays">
                                        </ext:Column>
                                        <ext:DateColumn ID="colSubmittedOn" runat="server" Align="Right" Text="To" Width="100"
                                            MenuDisabled="true" Sortable="false" Format="yyyy/MM/dd" DataIndex="SubmittedOnEng">
                                        </ext:DateColumn>
                                        <ext:Column ID="colStatusName" Sortable="false" MenuDisabled="true" runat="server"
                                            Text="Status" Width="300" Align="Left" DataIndex="StatusName">
                                        </ext:Column>
                                        <ext:ImageCommandColumn ID="ImageCommandColumn2" runat="server" Width="70" Text="Action"
                                            MenuDisabled="true" Sortable="false" Align="Center">
                                            <Commands>
                                                <ext:ImageCommand CommandName="Edit" Text="Edit" Cls="GreenCls">
                                                    <ToolTip Text="Edit Attendance Request Lines" />
                                                </ext:ImageCommand>
                                            </Commands>
                                            <Listeners>
                                                <Command Fn="CommandHandler">
                                                </Command>
                                            </Listeners>
                                        </ext:ImageCommandColumn>
                                        <ext:ImageCommandColumn ID="ImageCommandColumn3" runat="server" Width="70" Text="Action"
                                            MenuDisabled="true" Sortable="false" Align="Center">
                                            <Commands>
                                                <ext:ImageCommand CommandName="Delete" Text="Delete" Cls="RedCls">
                                                    <ToolTip Text="Delete" />
                                                </ext:ImageCommand>
                                            </Commands>
                                            <Listeners>
                                                <Command Fn="CommandHandler">
                                                </Command>
                                            </Listeners>
                                        </ext:ImageCommandColumn>
                                    </Columns>
                                </ColumnModel>
                                <SelectionModel>
                                    <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" Mode="Single" />
                                </SelectionModel>
                            </ext:GridPanel>
                        </Content>
                    </ext:Panel>
                    <ext:Panel ID="panelRecommended" runat="server" Title="Recommended">
                        <Content>
                            <ext:GridPanel StyleSpec="margin-top:15px;" ID="gridRecommended" runat="server" Cls="itemgrid"
                                Scroll="None" Width="840">
                                <Store>
                                    <ext:Store ID="Store7" runat="server">
                                        <Model>
                                            <ext:Model ID="Model7" runat="server" IDProperty="RequestID">
                                                <Fields>
                                                    <ext:ModelField Name="RequestID" Type="String" />
                                                    <ext:ModelField Name="StartDateEng" Type="Date" />
                                                    <ext:ModelField Name="EndDateEng" Type="Date" />
                                                    <ext:ModelField Name="WorkDays" Type="string" />
                                                    <ext:ModelField Name="SubmittedOnEng" Type="Date" />
                                                    <ext:ModelField Name="StatusName" Type="string" />
                                                    <ext:ModelField Name="Status" Type="Int" />
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <ColumnModel>
                                    <Columns>
                                        <ext:DateColumn ID="DateColumn7" runat="server" Align="Right" Text="From" Width="100"
                                            MenuDisabled="true" Sortable="false" Format="yyyy/MM/dd" DataIndex="StartDateEng">
                                        </ext:DateColumn>
                                        <ext:DateColumn ID="DateColumn8" runat="server" Align="Right" Text="To" Width="100"
                                            MenuDisabled="true" Sortable="false" Format="yyyy/MM/dd" DataIndex="EndDateEng">
                                        </ext:DateColumn>
                                        <ext:Column ID="Column5" Sortable="false" MenuDisabled="true" runat="server" Text="Work Days"
                                            Width="100" Align="Center" DataIndex="WorkDays">
                                        </ext:Column>
                                        <ext:DateColumn ID="DateColumn9" runat="server" Align="Right" Text="Submitted On"
                                            Width="100" MenuDisabled="true" Sortable="false" Format="yyyy/MM/dd" DataIndex="SubmittedOnEng">
                                        </ext:DateColumn>
                                        <ext:Column ID="Column6" Sortable="false" MenuDisabled="true" runat="server" Text="Status"
                                            Width="300" Align="Left" DataIndex="StatusName">
                                        </ext:Column>
                                    </Columns>
                                </ColumnModel>
                                <SelectionModel>
                                    <ext:RowSelectionModel ID="RowSelectionModel5" runat="server" Mode="Single" />
                                </SelectionModel>
                            </ext:GridPanel>
                        </Content>
                    </ext:Panel>
                    <ext:Panel ID="Panel2" runat="server" Title="Approved">
                        <Content>
                            <ext:GridPanel StyleSpec="margin-top:15px;" ID="gridApproved" runat="server" Cls="itemgrid"
                                Scroll="None" Width="840">
                                <Store>
                                    <ext:Store ID="Store3" runat="server">
                                        <Model>
                                            <ext:Model ID="Model2" runat="server" IDProperty="RequestID">
                                                <Fields>
                                                    <ext:ModelField Name="RequestID" Type="String" />
                                                    <ext:ModelField Name="StartDateEng" Type="Date" />
                                                    <ext:ModelField Name="EndDateEng" Type="Date" />
                                                    <ext:ModelField Name="WorkDays" Type="string" />
                                                    <ext:ModelField Name="SubmittedOnEng" Type="Date" />
                                                    <ext:ModelField Name="StatusName" Type="string" />
                                                    <ext:ModelField Name="Status" Type="Int" />
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <ColumnModel>
                                    <Columns>
                                        <ext:DateColumn ID="DateColumn1" runat="server" Align="Right" Text="From" Width="100"
                                            MenuDisabled="true" Sortable="false" Format="yyyy/MM/dd" DataIndex="StartDateEng">
                                        </ext:DateColumn>
                                        <ext:DateColumn ID="DateColumn2" runat="server" Align="Right" Text="To" Width="100"
                                            MenuDisabled="true" Sortable="false" Format="yyyy/MM/dd" DataIndex="EndDateEng">
                                        </ext:DateColumn>
                                        <ext:Column ID="Column1" Sortable="false" MenuDisabled="true" runat="server" Text="Work Days"
                                            Width="100" Align="Center" DataIndex="WorkDays">
                                        </ext:Column>
                                        <ext:DateColumn ID="DateColumn3" runat="server" Align="Right" Text="Submitted On"
                                            Width="100" MenuDisabled="true" Sortable="false" Format="yyyy/MM/dd" DataIndex="SubmittedOnEng">
                                        </ext:DateColumn>
                                        <ext:Column ID="Column2" Sortable="false" MenuDisabled="true" runat="server" Text="Status"
                                            Width="300" Align="Left" DataIndex="StatusName">
                                        </ext:Column>
                                    </Columns>
                                </ColumnModel>
                                <SelectionModel>
                                    <ext:RowSelectionModel ID="RowSelectionModel3" runat="server" Mode="Single" />
                                </SelectionModel>
                            </ext:GridPanel>
                        </Content>
                    </ext:Panel>
                    <ext:Panel ID="Panel3" runat="server" Title="Rejected">
                        <Content>
                            <ext:GridPanel StyleSpec="margin-top:15px;" ID="gridRejected" runat="server" Cls="itemgrid"
                                Scroll="None" Width="840">
                                <Store>
                                    <ext:Store ID="Store4" runat="server">
                                        <Model>
                                            <ext:Model ID="Model3" runat="server" IDProperty="RequestID">
                                                <Fields>
                                                    <ext:ModelField Name="RequestID" Type="String" />
                                                    <ext:ModelField Name="StartDateEng" Type="Date" />
                                                    <ext:ModelField Name="EndDateEng" Type="Date" />
                                                    <ext:ModelField Name="WorkDays" Type="string" />
                                                    <ext:ModelField Name="SubmittedOnEng" Type="Date" />
                                                    <ext:ModelField Name="StatusName" Type="string" />
                                                    <ext:ModelField Name="Status" Type="Int" />
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <ColumnModel>
                                    <Columns>
                                        <ext:DateColumn ID="DateColumn4" runat="server" Align="Right" Text="From" Width="100"
                                            MenuDisabled="true" Sortable="false" Format="yyyy/MM/dd" DataIndex="StartDateEng">
                                        </ext:DateColumn>
                                        <ext:DateColumn ID="DateColumn5" runat="server" Align="Right" Text="To" Width="100"
                                            MenuDisabled="true" Sortable="false" Format="yyyy/MM/dd" DataIndex="EndDateEng">
                                        </ext:DateColumn>
                                        <ext:Column ID="Column3" Sortable="false" MenuDisabled="true" runat="server" Text="Work Days"
                                            Width="100" Align="Center" DataIndex="WorkDays">
                                        </ext:Column>
                                        <ext:DateColumn ID="DateColumn6" runat="server" Align="Right" Text="Submitted On"
                                            Width="100" MenuDisabled="true" Sortable="false" Format="yyyy/MM/dd" DataIndex="SubmittedOnEng">
                                        </ext:DateColumn>
                                        <ext:Column ID="Column4" Sortable="false" MenuDisabled="true" runat="server" Text="Status"
                                            Width="300" Align="Left" DataIndex="StatusName">
                                        </ext:Column>
                                    </Columns>
                                </ColumnModel>
                                <SelectionModel>
                                    <ext:RowSelectionModel ID="RowSelectionModel4" runat="server" Mode="Single" />
                                </SelectionModel>
                            </ext:GridPanel>
                        </Content>
                    </ext:Panel>
                </Items>
            </ext:TabPanel>
            <%--Grid--%>
        </div>
        <div style="margin-top: 15px; font-size: 15px; margin-left: 50px;">
        </div>
    </div>
    <ext:Window runat="server" Hidden="false" X="-1000" Y="-1000" Title="Attendance Request"
        ID="eventWindow" Modal="false" Width="1050" Height="600">
        <Content>
            <div style="padding: 15px; padding-top: 5px;">
                <table class="tblDetails">
                    <tr>
                        <td style="width: 170px;">
                            <ext:DateField LabelSeparator="" FieldLabel="Start Date" runat="server" LabelAlign="Top"
                                ID="txtStartDateAdd" Width="150">
                                <Plugins>
                                    <ext:GenericPlugin ID="GenericPlugin1" runat="server" InstanceName="Ext.ux.DatePicker.HighlightWithNepDates" />
                                </Plugins>
                            </ext:DateField>
                            <asp:RequiredFieldValidator runat="server" ID="rfvStartDateAdd" ValidationGroup="TimeAtt"
                                ControlToValidate="txtStartDateAdd" Display="None" ErrorMessage="Start date is required."></asp:RequiredFieldValidator>
                        </td>
                        <td style="width: 170px;">
                            <ext:DateField LabelSeparator="" FieldLabel="End Date" runat="server" LabelAlign="Top"
                                ID="txtEndDateAdd" Width="150">
                                <Plugins>
                                    <ext:GenericPlugin ID="GenericPlugin2" runat="server" InstanceName="Ext.ux.DatePicker.HighlightWithNepDates" />
                                </Plugins>
                            </ext:DateField>
                            <asp:RequiredFieldValidator runat="server" ID="rfvEndateDateAdd" ValidationGroup="TimeAtt"
                                ControlToValidate="txtEndDateAdd" Display="None" ErrorMessage="End date is required."></asp:RequiredFieldValidator>
                        </td>
                        <td style="width: 320px;">
                            <ext:TextField ID="txtInNoteAdd" FieldStyle="border:1px solid rgb(173, 216, 230);"
                                runat="server" FieldLabel="Pre-fill Note" Text="" LabelAlign="Top" LabelSeparator=""
                                Width="300" />
                        </td>
                        <td>
                            <ext:Button ID="btnLoad" runat="server" StyleSpec="margin-top:15px;" Cls="btn btn-save"
                                Text="Fill" Width="100">
                                <DirectEvents>
                                    <Click OnEvent="btnLoad_Click">
                                        <EventMask ShowMask="true" />
                                        <ExtraParams>
                                            <ext:Parameter Name="gridItems" Value="Ext.encode(#{gridAttRequest}.getRowsValues({ selectedOnly: false }))"
                                                Mode="Raw" />
                                        </ExtraParams>
                                    </Click>
                                </DirectEvents>
                                <Listeners>
                                    <Click Handler="valGroup = 'TimeAtt';">
                                    </Click>
                                </Listeners>
                            </ext:Button>
                        </td>
                    </tr>
                </table>
                <table class="tblDetails">
                    <tr>
                        <td>
                            <ext:GridPanel StyleSpec="margin-top:15px; border-color: transparent;" ID="gridAttRequest" runat="server" Cls="itemgrid"
                                Width="1000" Scroll="Vertical" Height="350">
                                <Store>
                                    <ext:Store ID="Store1" runat="server">
                                        <Model>
                                            <ext:Model ID="Model4" runat="server" IDProperty="SN">
                                                <Fields>
                                                    <ext:ModelField Name="SN" Type="Int" />
                                                    <ext:ModelField Name="RequestLineID" Type="String" />
                                                    <ext:ModelField Name="DateEng" Type="Date" />
                                                    <ext:ModelField Name="DayName" Type="string" />
                                                    <ext:ModelField Name="Description" Type="string" />
                                                    <ext:ModelField Name="InTimeDT" Type="Date" />
                                                    <ext:ModelField Name="OutTimeDT" Type="Date" />
                                                    <ext:ModelField Name="InNote" Type="string" />
                                                    <ext:ModelField Name="OutNote" Type="string" />
                                                    <ext:ModelField Name="WorkHours" Type="string" />
                                                    <ext:ModelField Name="OvernightShift" Type="Boolean" />
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <ColumnModel>
                                    <Columns>
                                        <ext:DateColumn ID="colDate" runat="server" Align="Right" Text="Date" Width="100"
                                            MenuDisabled="true" Sortable="false" Format="yyyy/MM/dd" DataIndex="DateEng">
                                        </ext:DateColumn>
                                        <ext:Column ID="colDayName" Sortable="false" MenuDisabled="true" runat="server" Text="Day"
                                            Width="100" Align="Left" DataIndex="DayName">
                                        </ext:Column>
                                        <ext:DateColumn ID="colInTime1" Sortable="false" MenuDisabled="true" runat="server"
                                            Text="Start Time" Align="Right" Width="100" DataIndex="InTimeDT" Format="hh:mm tt">
                                            <Editor>
                                                <ext:TimeField ID="TimeField1" runat="server" MinTime="00:00" MaxTime="23:59" Increment="1"
                                                    Format="hh:mm tt" SelectedTime="08:00">
                                                </ext:TimeField>
                                            </Editor>
                                        </ext:DateColumn>
                                        <ext:Column ID="colInNote" Sortable="false" MenuDisabled="true" runat="server" Text="In Note"
                                            Align="Left" Width="200" DataIndex="InNote">
                                            <Editor>
                                                <ext:TextField ID="txtInNote" runat="server">
                                                </ext:TextField>
                                            </Editor>
                                        </ext:Column>
                                        <%--<ext:CheckColumn ID="colOvernightShift" MenuDisabled="true" Sortable="false" Text="Overnight Shift" DataIndex="OvernightShift" runat="server" Width="120" Editable="true">                                                      
                                       </ext:CheckColumn>     --%>
                                        <ext:DateColumn ID="colOutTime1" Sortable="false" MenuDisabled="true" runat="server"
                                            Text="End Time" Align="Right" Width="100" DataIndex="OutTimeDT" Format="hh:mm tt">
                                            <Editor>
                                                <ext:TimeField ID="TimeField2" runat="server" MinTime="00:00" MaxTime="23:59" Increment="1"
                                                    Format="hh:mm tt" SelectedTime="08:00">
                                                </ext:TimeField>
                                            </Editor>
                                        </ext:DateColumn>
                                        <ext:Column ID="colOutNote" Sortable="false" MenuDisabled="true" runat="server" Text="Out Note"
                                            Align="Left" Width="200" DataIndex="OutNote">
                                            <Editor>
                                                <ext:TextField ID="txtOutNote" runat="server">
                                                </ext:TextField>
                                            </Editor>
                                        </ext:Column>
                                        <ext:Column ID="colWorkHours" Sortable="false" MenuDisabled="true" runat="server"
                                            Text="Work Hours" Align="Center" Width="80" DataIndex="WorkHours">
                                            <Renderer Fn="CalculateWorkHours" />
                                        </ext:Column>
                                        <ext:CommandColumn ID="CommandColumn1" runat="server" Width="75" Text="Actions" Align="Center">
                                            <Commands>
                                                <ext:CommandSeparator />
                                                <ext:GridCommand ToolTip-Text="Delete" Text="Clear" CommandName="Delete" />
                                            </Commands>
                                            <Listeners>
                                                <Command Handler="CommandHandlerClear(command,record);" />
                                            </Listeners>
                                        </ext:CommandColumn>
                                        <ext:CommandColumn ID="CommandColumn2" runat="server" Sortable="false" MenuDisabled="true"
                                            Width="50" Align="Center">
                                            <Commands>
                                                <ext:GridCommand Icon="Delete" CommandName="Delete" Cls="deleteicon" />
                                            </Commands>
                                            <Listeners>
                                                <Command Fn="RemoveItemLine">
                                                </Command>
                                            </Listeners>
                                        </ext:CommandColumn>
                                    </Columns>
                                </ColumnModel>
                                <SelectionModel>
                                    <ext:RowSelectionModel ID="RowSelectionModel2" runat="server" Mode="Single" />
                                </SelectionModel>
                                <Plugins>
                                    <ext:CellEditing ID="CellEditing1" runat="server" ClicksToEdit="1">
                                    </ext:CellEditing>
                                </Plugins>
                            </ext:GridPanel>
                            <ext:ToolTip ID="ToolTip1"
                                runat="server"
                                Target="={#{gridAttRequest}.getView().el}"
                                Delegate=".x-grid-cell"
                                TrackMouse="true">
                                <Listeners>
                                    <Show Handler="onShow(this, #{gridAttRequest});" />
                                </Listeners>
                            </ext:ToolTip>
                        </td>
                    </tr>
                </table>
                <br />
                <table class="tblDetails">
                    <tr runat="server" id="trApplyTo">
                        <td>
                            <ext:DisplayField ID="lblApplyTo" LabelSeparator="" LabelStyle="font-weight:bold"
                                LabelWidth="75" runat="server" FieldLabel="Approval">
                            </ext:DisplayField>
                        </td>
                    </tr>
                    <tr runat="server" id="trRecApp">
                        <td runat="server" id="tdRecommend">
                            <ext:ComboBox ID="cmbRecommender" runat="server" ValueField="Value" DisplayField="Text"
                                FieldLabel="Sent for Recommend to *" Width="200" LabelAlign="top" LabelSeparator=""
                                ForceSelection="true" QueryMode="Local">
                                <Store>
                                    <ext:Store ID="Store5" runat="server">
                                        <Model>
                                            <ext:Model ID="Model5" runat="server">
                                                <Fields>
                                                    <ext:ModelField Name="Value" Type="String" />
                                                    <ext:ModelField Name="Text" Type="String" />
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <Items>
                                    <ext:ListItem Text="--Select Recommender--" Value="-1" />
                                </Items>
                                <SelectedItems>
                                    <ext:ListItem Text="--Select Recommender--" Value="-1" />
                                </SelectedItems>
                            </ext:ComboBox>
                        </td>
                        <td>
                            <ext:ComboBox ID="cmbApproval" runat="server" ValueField="Value" DisplayField="Text"
                                FieldLabel="Sent for Approval to *" Width="200" LabelAlign="top" LabelSeparator=""
                                ForceSelection="true" QueryMode="Local">
                                <Store>
                                    <ext:Store ID="Store6" runat="server">
                                        <Model>
                                            <ext:Model ID="Model6" runat="server">
                                                <Fields>
                                                    <ext:ModelField Name="Value" Type="String" />
                                                    <ext:ModelField Name="Text" Type="String" />
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <Items>
                                    <ext:ListItem Text="--Select Approval--" Value="-1" />
                                </Items>
                                <SelectedItems>
                                    <ext:ListItem Text="--Select Approval--" Value="-1" />
                                </SelectedItems>
                            </ext:ComboBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <ext:Button runat="server" Cls="btn btn-primary btn-sect" Width="150" StyleSpec="margin-top:10px"
                                Height="30" ID="btnSend" Text="<i></i>Send For Approval">
                                <DirectEvents>
                                    <Click OnEvent="btnSend_Click">
                                        <Confirmation ConfirmRequest="true" Message="Confirm send the request?" />
                                        <EventMask ShowMask="true" />
                                        <ExtraParams>
                                            <ext:Parameter Name="gridItems" Value="Ext.encode(#{gridAttRequest}.getRowsValues({ selectedOnly: false }))"
                                                Mode="Raw" />
                                        </ExtraParams>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                        </td>
                    </tr>
                </table>
            </div>
        </Content>
    </ext:Window>
    <uc1:FooterCtl ID="FooterCtl1" runat="server" />
    </form>
</body>
</html>
