﻿<%@ Page Title="Add New Claim" Language="C#" MasterPageFile="~/Master/EmployeeMS.Master"
    AutoEventWireup="true" CodeBehind="NewInsuranceClaim.aspx.cs" Inherits="Web.Employee.NewInsuranceClaim" %>

<%@ Register Src="~/Controls/ContentHeader.ascx" TagName="ContentHeader" TagPrefix="C" %>
<%@ Register Src="~/Employee/UserControls/InsuranceClaim.ascx" TagName="InsuranceClaim" TagPrefix="ucF" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">

        var CommandHandler = function(command, record){
            <%= hiddenClaimId.ClientID %>.setValue(record.data.ClaimID);
            if(command=="Edit")
                <%=btnEdit.ClientID %>.fireEvent('click');
            if(command=="Delete")
                <%=btnDelete.ClientID %>.fireEvent('click');
        }
              

        function searchList() {
            <%=GridClaimList.ClientID %>.getStore().pageSize = <%=cmbPageSize.ClientID %>.getValue();
            <%=PagingToolbar1.ClientID%>.doRefresh();
        }

        //var prepareToolbar = function (grid, toolbar, rowIndex, record) {
        //    debugger;
        //    var editBtn = toolbar.items.get(0);
        //    var deleteBtn = toolbar.items.get(1);
        //    if(record.data.Status != 1) {
        //        editBtn.setVisible(false);
        //        deleteBtn.setVisible(false);              
        //    }
        //};  


        <%-- var ClaimOfChanged = function () { 
            var r = this.getStore().getById(this.getValue()).data.Relation;
            try
            {
                var indexByName= <%=cmbRelation.ClientID %>.getStore().find('Name',r);
                var id=<%=cmbRelation.ClientID %>.getStore().getAt(indexByName).get('ID');
            }
            catch(err){
           
            }
            if(id)
                <%=cmbRelation.ClientID %>.setValue(id);
        }

        function ClearFields()
        {
            <%=dfClaimDate.ClientID%>.setValue('');
            <%=txtDetailsOfIllness.ClientID %>.setValue('');
            <%=txtConsultantName.ClientID%>.setValue('');
            <%=txtHostpitalName.ClientID%>.setValue('');
            <%=txtDoctorAddress.ClientID%>.setValue('');
            <%=txtDoctorName.ClientID%>.setValue('');
            <%=dfDiagnosisDate.ClientID%>.setValue('');
            <%=txtDetailsOfTreatment.ClientID %>.setValue('');
            <%=txtDetailsOfAccident.ClientID%>.setValue('');
            <%=txtLocationOfAccidents.ClientID%>.setValue('');
            <%=dfDateOfAccident.ClientID%>.setValue('');
            <%=txtTotalClaim.ClientID %>.setValue('');
            
           
        }


        function ClaimTypeChanged(title) {
            if (title == "1")
            {   
                <%=txtDetailsOfIllness.ClientID %>.setVisible(true);
                <%=txtConsultantName.ClientID%>.setVisible(true);
                <%=txtHostpitalName.ClientID%>.setVisible(true);
                <%=txtDoctorAddress.ClientID%>.setVisible(true);
                <%=txtDoctorName.ClientID%>.setVisible(true);
                <%=dfDiagnosisDate.ClientID%>.setVisible(true);


                <%=txtDetailsOfTreatment.ClientID %>.setVisible(false);
                <%=txtDetailsOfAccident.ClientID%>.setVisible(false);
                <%=txtLocationOfAccidents.ClientID%>.setVisible(false);
                <%=tfttme.ClientID%>.setVisible(false);
                <%=dfDateOfAccident.ClientID%>.setVisible(false);
            }      
            else
            {
                <%=txtDetailsOfIllness.ClientID %>.setVisible(false);
                <%=txtConsultantName.ClientID%>.setVisible(false);
                <%=txtHostpitalName.ClientID%>.setVisible(false);
                <%=txtDoctorAddress.ClientID%>.setVisible(false);
                <%=txtDoctorName.ClientID%>.setVisible(false);
                <%=dfDiagnosisDate.ClientID%>.setVisible(false);

                <%=txtDetailsOfTreatment.ClientID %>.setVisible(true);
                <%=txtDetailsOfAccident.ClientID%>.setVisible(true);
                <%=txtLocationOfAccidents.ClientID%>.setVisible(true);
                <%=tfttme.ClientID%>.setVisible(true);
                <%=dfDateOfAccident.ClientID%>.setVisible(true);
            }
        }--%>
             
    </script>
    <script src="override.js" type="text/javascript"></script>
    <link href="../css/newDesignChange.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
    <C:ContentHeader runat="server" />
    <ext:Hidden runat="server" ID="hiddenClaimId" />
    <ext:LinkButton runat="server" Hidden="true" ID="btnEdit">
        <DirectEvents>
            <Click OnEvent="btnEdit_Click">
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:LinkButton>
    <ext:Button ID="btnDelete" runat="server" Hidden="true">
        <DirectEvents>
            <Click OnEvent="btnDelete_Click">
                <Confirmation ConfirmRequest="true" Message="Are you sure, you want to delete the request?" />
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:Button>

    <%--<ext:Store ID="storeRelation" runat="server">
        <Model>
            <ext:Model ID="modelRelation" runat="server" IDProperty="ID">
                <Fields>
                    <ext:ModelField Name="ID" Type="string" />
                    <ext:ModelField Name="Name" Type="string" />
                </Fields>
            </ext:Model>
        </Model>
    </ext:Store>--%>

    <ext:Store ID="storecmbStatus" runat="server">
        <Model>
            <ext:Model ID="modelcmbStatus" runat="server" IDProperty="Value">
                <Fields>
                    <ext:ModelField Name="Text" Type="string" />
                    <ext:ModelField Name="Value" Type="string" />
                </Fields>
            </ext:Model>
        </Model>
    </ext:Store>


    <ext:Store ID="storeCmbClaimType" runat="server">
        <Model>
            <ext:Model ID="modelCmbClaimType" runat="server" IDProperty="Value">
                <Fields>
                    <ext:ModelField Name="Text" Type="string" />
                    <ext:ModelField Name="Value" Type="string" />
                </Fields>
            </ext:Model>
        </Model>
    </ext:Store>




    <h4>My Claims</h4>
    <table border="0">
        <tr>
            <td>
                <ext:ComboBox ID="cmbSearchClaimType" runat="server" StoreID="storeCmbClaimType" FieldLabel="Claim Type" ForceSelection="true" DisplayField="Text" ValueField="Value" LabelAlign="Top" QueryMode="Local" LabelSeparator="">
                    <Triggers>
                        <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                    </Triggers>
                    <Listeners>
                        <Select Handler="this.getTrigger(0).show();searchList();" />
                        <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                        <TriggerClick Handler="if (index == 0) {   this.clearValue();   this.getTrigger(0).hide(); searchList();  }" />
                    </Listeners>
                </ext:ComboBox>
            </td>
            <td>
                <ext:ComboBox runat="server" MarginSpec="0 0 0 10" Width="150" ID="cmbStatus" StoreID="storecmbStatus" ForceSelection="true" FieldLabel="Status" LabelAlign="Top" DisplayField="Text" ValueField="Value" QueryMode="Local" LabelSeparator="">
                    <Triggers>
                        <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                    </Triggers>
                    <Listeners>
                        <Select Handler="this.getTrigger(0).show();searchList();" />
                        <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                        <TriggerClick Handler="if (index == 0) {   this.clearValue();   this.getTrigger(0).hide(); searchList();  }" />
                    </Listeners>
                </ext:ComboBox>
            </td>


            <td style=" padding-right:10px; padding-left: 10px;">
                <ext:DateField ID="dfSearchClaimDateFrom" runat="server" FieldLabel="From" LabelAlign="top" LabelSeparator="" Width="150">
                    <Plugins>
                        <ext:GenericPlugin ID="GenericPlugin1" runat="server" InstanceName="Ext.ux.DatePicker.HighlightWithNepDates" />
                    </Plugins>
                </ext:DateField>
            </td>
            <td style="padding-right: 10px;">
                <ext:DateField ID="dfSearchClaimDateTo" runat="server" FieldLabel="To" LabelAlign="top" LabelSeparator="" Width="150">
                    <Plugins>
                        <ext:GenericPlugin ID="GenericPlugin3" runat="server" InstanceName="Ext.ux.DatePicker.HighlightWithNepDates" />
                    </Plugins>
                </ext:DateField>
            </td>

            <td>
                <ext:Button runat="server" MarginSpec="15 0 0 10" ID="btnSearch" Cls="btn btn-default" Text="<i></i>Search">
                    <Listeners>
                        <Click Handler="searchList();" />
                    </Listeners>
                </ext:Button>
            </td>
            <td>
                <ucF:InsuranceClaim ID="InsuranceClaim" runat="server" />
                <ext:Button runat="server" MarginSpec="15 0 0 10" ID="btnAddClaim" Cls="btn btn-primary" Text="<i></i>Add New Claim">
                    <Listeners>
                        <Click Handler="#{hiddenClaimId}.setValue('');" />
                    </Listeners>
                    <DirectEvents>
                        <Click OnEvent="btnAddClaim_Click">
                            <EventMask ShowMask="true" />
                        </Click>
                    </DirectEvents>
                </ext:Button>
            </td>


        </tr>
    </table>
    <ext:GridPanel ID="GridClaimList" MarginSpec="10 0 0 0" runat="server" Header="true" AutoScroll="true" Scroll="Horizontal">
        <Store>
            <ext:Store ID="StoreGridClaimList" runat="server" OnReadData="Store_ReadData" RemoteSort="true" AutoLoad="true">
                <Proxy>
                    <ext:PageProxy />
                </Proxy>
                <Model>
                    <ext:Model ID="ModelGridClaimList" runat="server" IDProperty="ClaimID">
                        <Fields>
                            <ext:ModelField Name="ClaimID" />
                            <ext:ModelField Name="ClaimDate" Type="Date" />
                            <ext:ModelField Name="ClaimOfFamilyName" />
                            <ext:ModelField Name="RelationName" />
                            <ext:ModelField Name="DiagnosisDate" Type="Date" />
                            <ext:ModelField Name="DoctorName" />
                            <ext:ModelField Name="DoctorAddress" />
                            <ext:ModelField Name="HospitalNameAddress" />
                            <ext:ModelField Name="Details" />
                            <ext:ModelField Name="AccidentDateTime" Type="Date" />
                            <ext:ModelField Name="AccidentLocation" />
                            <ext:ModelField Name="DetailsOfTreatment" />
                            <ext:ModelField Name="TotalClaimAmount" />
                            <ext:ModelField Name="statusValue" />
                            <ext:ModelField Name="Status" />
                        </Fields>
                    </ext:Model>
                </Model>
            </ext:Store>
        </Store>
        <ColumnModel>
            <Columns>
                <ext:DateColumn ID="ColClaimDate" Sortable="false" MenuDisabled="true" runat="server" Text="Claim Date" Format="yyyy-MM-dd" Width="80" DataIndex="ClaimDate" />
                <ext:Column ID="ColAccidentDetails" Sortable="false" MenuDisabled="true" runat="server" Text="Details (illness/accident)" Width="200" DataIndex="Details" />
                <ext:Column ID="ColRelation" Sortable="false" MenuDisabled="true" runat="server" Text="Claim Of" Width="100" DataIndex="RelationName" />
                <ext:NumberColumn ID="colTotalClaim" Sortable="false" MenuDisabled="true" runat="server" Text="Claim Amount" Width="100" DataIndex="TotalClaimAmount" />
                <ext:DateColumn ID="ColDiagnosisDate" Sortable="false" MenuDisabled="true" runat="server" Text="Diagnosis Date" Format="yyyy-MMM-dd" Width="100" DataIndex="DiagnosisDate" />
                <ext:DateColumn ID="ColDateTimeOfAccident" Sortable="false" MenuDisabled="true" runat="server" Text="Accident Date" Width="160" Format="yyyy-MM-dd hh:mm:ss a" DataIndex="AccidentDateTime" />
                <ext:Column ID="ColDoctorName" Sortable="false" MenuDisabled="true" runat="server" Text="Doctor Name" Width="120" DataIndex="DoctorName" />
                <ext:Column ID="ColHospitalName" Sortable="false" MenuDisabled="true" runat="server" Text="Hospital's Name" Width="180" DataIndex="HospitalNameAddress" />
                <ext:Column ID="colStatus" Sortable="false" MenuDisabled="true" runat="server" Text="Status" Width="100" DataIndex="statusValue" />
                <ext:CommandColumn runat="server" Align="Right" Text="">
                    <Commands>
                        <ext:GridCommand Icon="pencil" ToolTip-Text="Edit" CommandName="Edit" />
                        <ext:GridCommand Icon="Cancel" ToolTip-Text="Delete" CommandName="Delete" />
                    </Commands>
                    <Listeners>
                        <Command Handler="CommandHandler(command,record);" />
                    </Listeners>
                    <%-- <PrepareToolbar Fn="prepareToolbar" />--%>
                </ext:CommandColumn>
            </Columns>

        </ColumnModel>
        <%-- <SelectionModel>
            <ext:CellSelectionModel runat="server">
                <DirectEvents>
                    <Select OnEvent="btnView_Click" />
                </DirectEvents>
            </ext:CellSelectionModel>
        </SelectionModel>--%>
        <SelectionModel>
            <ext:RowSelectionModel ID="RowSelectionModel2" runat="server" Mode="Single" />
        </SelectionModel>
        <BottomBar>
            <ext:PagingToolbar Height="40" ID="PagingToolbar1" runat="server" StoreID="StoreGridClaimList" DisplayInfo="true">
                <Items>
                    <ext:Label ID="lblPageSize" runat="server" Text="Page size:" />
                    <ext:ToolbarSpacer ID="ToolbarSpacer_listItems" runat="server" Width="10" />
                    <ext:ComboBox ID="cmbPageSize" runat="server" Width="80" SelectOnFocus="true" Selectable="true"
                        ValueField="Value" DisplayField="Text" ForceSelection="true" AllowBlank="false">
                        <Listeners>
                            <Select Handler="searchList()" />
                        </Listeners>
                        <Items>
                            <ext:ListItem Value="10" Text="10" />
                            <ext:ListItem Value="20" Text="20" />
                            <ext:ListItem Value="50" Text="50" />
                            <ext:ListItem Value="100000" Text="All" />
                        </Items>
                        <SelectedItems>
                            <ext:ListItem Index="0">
                            </ext:ListItem>
                        </SelectedItems>
                    </ext:ComboBox>
                </Items>
            </ext:PagingToolbar>
        </BottomBar>
    </ext:GridPanel>
    <%--<ext:Window ID="windowAddEditClaim" ButtonAlign="Left" AutoScroll="true" runat="server"
        Title="Add/Update Claim" Icon="ApplicationAdd" Width="620" Height="600" BodyPadding="5"
        Hidden="true" Modal="true">
        <Content>
            <table class="fieldTable">
                <tr>
                    <td>
                        <ext:ComboBox ID="cmbClaimType" runat="server" StoreID="storeCmbClaimType" ForceSelection="true" FieldLabel="Claim Type *" DisplayField="Text" ValueField="Value"
                            LabelAlign="Top" QueryMode="Local" LabelSeparator="">
                            <Listeners>
                                <Select Handler="ClaimTypeChanged(this.getValue());">
                                </Select>
                            </Listeners>
                        </ext:ComboBox>
                        <asp:RequiredFieldValidator Display="None" ID="rfvcmbClaimType" runat="server"
                            ValidationGroup="SaveUpdateClaimPopup" ControlToValidate="cmbClaimType" ErrorMessage="Claim Type is required." /></td>
                    <td>
                        <ext:DateField ID="dfClaimDate" runat="server" CausesValidation="true" FieldLabel="Claim Date *" LabelAlign="top" LabelSeparator="" Width="150" AllowBlank="false">
                            <Plugins>
                                <ext:GenericPlugin ID="GenericPlugin2" runat="server" InstanceName="Ext.ux.DatePicker.HighlightWithNepDates" />
                            </Plugins>
                        </ext:DateField>
                        <asp:RequiredFieldValidator Enabled="true" Display="None" ID="rfvdfClaimDate" runat="server" ValidationGroup="SaveUpdateClaimPopup" ControlToValidate="dfClaimDate" ErrorMessage="Claim Date is required." />
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td>
                        <ext:TextField ID="txtStaffId" runat="server" FieldLabel="Staff ID" Disabled="true" LabelAlign="Top" LabelSeparator=""></ext:TextField>
                    </td>
                    <td>
                        <ext:TextField ID="txtStaffName" runat="server" FieldLabel="Name" Disabled="true" LabelAlign="Top" LabelSeparator=""></ext:TextField>
                    </td>
                    <td>
                        <ext:TextField ID="txtInsurencePolicyNo" runat="server" Disabled="true" FieldLabel="Insurance Policy No." LabelAlign="Top" LabelSeparator=""></ext:TextField>
                    </td>
                </tr>
                <tr>
                    <td>
                        <ext:ComboBox ID="cmbClaimOf" runat="server" ForceSelection="true" FieldLabel="Claim Of *" Width="175" DisplayField="Name" ValueField="FamilyId" LabelAlign="Top" QueryMode="Local" LabelSeparator="">
                            <Store>
                                <ext:Store ID="storecmbClaimOf" runat="server">
                                    <Model>
                                        <ext:Model ID="modelcmbClaimOf" runat="server" IDProperty="FamilyId">
                                            <Fields>
                                                <ext:ModelField Name="Name" Type="string" />
                                                <ext:ModelField Name="FamilyId" Type="string" />
                                                <ext:ModelField Name="Relation" Type="String" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                            <SelectedItems>
                                <ext:ListItem Value="0" />
                            </SelectedItems>
                            <Listeners>
                                <Select Fn="ClaimOfChanged">
                                </Select>
                            </Listeners>
                        </ext:ComboBox>
                        <asp:RequiredFieldValidator Display="None" ID="rfvcmbClaimOf" runat="server" ValidationGroup="SaveUpdateClaimPopup" ControlToValidate="cmbClaimOf" ErrorMessage="Claim Of is required." />
                    </td>
                    <td>
                        <ext:ComboBox ID="cmbRelation" Disabled="true" runat="server" ForceSelection="true" StoreID="storeRelation"
                            FieldLabel="Relation" DisplayField="Name" ValueField="ID" LabelAlign="Top" QueryMode="Local" LabelSeparator="">
                            <SelectedItems>
                                <ext:ListItem Value="0" />
                            </SelectedItems>
                        </ext:ComboBox>
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td>
                        <ext:DateField ID="dfDiagnosisDate" EmptyDate="" PaddingSpec="15 0 0 0" runat="server" FieldLabel="Diagnosis Date" LabelAlign="top" LabelSeparator="" Width="175" AllowBlank="false">
                            <Plugins>
                                <ext:GenericPlugin ID="GenericPlugin3" runat="server" InstanceName="Ext.ux.DatePicker.HighlightWithNepDates" />
                            </Plugins>
                        </ext:DateField>
                    </td>
                    <td>
                        <ext:TextField ID="txtDoctorName" PaddingSpec="15 0 0 0" runat="server" FieldLabel="Doctor's Name" LabelAlign="Top" LabelSeparator=""></ext:TextField>
                    </td>
                    <td>
                        <ext:TextField ID="txtDoctorAddress" PaddingSpec="15 0 0 0" runat="server" FieldLabel="Doctor's Address" LabelAlign="Top" LabelSeparator=""></ext:TextField>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <ext:TextField ID="txtHostpitalName" runat="server" FieldLabel="Hospital's Name & Address (if relevant)" Width="340" LabelAlign="Top" LabelSeparator=""></ext:TextField>
                    </td>
                    <td>
                        <ext:TextField ID="txtConsultantName" runat="server" FieldLabel="Consultant's Name" LabelAlign="Top" LabelSeparator=""></ext:TextField>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <ext:TextArea ID="txtDetailsOfIllness" runat="server" FieldLabel="Details of illness" LabelAlign="Top" LabelSeparator="" Width="520"></ext:TextArea>
                    </td>
                </tr>
                <tr>
                    <td>
                        <ext:DateField ID="dfDateOfAccident" EmptyDate="" Hidden="true" runat="server" FieldLabel="Date of Accident" LabelAlign="top" LabelSeparator="" Width="175" AllowBlank="false">
                            <Plugins>
                                <ext:GenericPlugin ID="GenericPlugin4" runat="server" InstanceName="Ext.ux.DatePicker.HighlightWithNepDates" />
                            </Plugins>
                        </ext:DateField>
                    </td>
                    <td>
                        <ext:TimeField ID="tfttme" Hidden="true" FieldLabel="Time of Accident" LabelAlign="top" LabelSeparator="" runat="server" MinTime="00:00" MaxTime="12:00" Increment="10" SelectedTime="00:00" Format="hh:mm tt" />
                    </td>
                    <td>
                        <ext:TextField ID="txtLocationOfAccidents" Hidden="true" runat="server" FieldLabel="Location of Accident" LabelAlign="Top" LabelSeparator=""></ext:TextField>

                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <ext:TextArea ID="txtDetailsOfAccident" Hidden="true" runat="server" FieldLabel="Details of Accident" LabelAlign="Top" LabelSeparator="" Width="520"></ext:TextArea>
                    </td>

                </tr>
                <tr>
                    <td colspan="3">
                        <ext:TextArea ID="txtDetailsOfTreatment" Hidden="true" runat="server" FieldLabel="Details of Treatment Undertaken" LabelAlign="Top" LabelSeparator="" Width="520"></ext:TextArea>
                    </td>
                </tr>
                <tr>
                    <td>
                        <ext:NumberField ID="txtTotalClaim" AllowDecimals="true" MinValue="0" runat="server" FieldLabel="Total Claim (in Rs.) *" LabelAlign="Top" LabelSeparator=""></ext:NumberField>
                        <asp:RequiredFieldValidator Display="None" ID="rfvtxtTotalClaim" runat="server" ValidationGroup="SaveUpdateClaimPopup" ControlToValidate="txtTotalClaim" ErrorMessage="Claim amount is required." />
                    </td>
                </tr>
            </table>
        </Content>
        <Buttons>
            <ext:Button runat="server" Cls="btn btn-primary" StyleSpec="margin-left:10px" ValidationGroup="SaveUpdateClaimPopup"
                ID="btnSave" Text="<i></i>Save">
                <DirectEvents>
                    <Click OnEvent="btnSave_Click">
                        <EventMask ShowMask="true" />
                    </Click>
                </DirectEvents>
                <Listeners>
                    <Click Handler="valGroup = 'SaveUpdateClaimPopup'; return CheckValidation();">
                    </Click>
                </Listeners>
            </ext:Button>
            <ext:LinkButton runat="server" StyleSpec="margin-left:10px;padding:0px;" Cls="btnFlatLeftGap" ID="LinkButton1"
                Text="<i></i>Cancel">
                <Listeners>
                    <Click Handler="#{windowAddEditClaim}.hide();">
                    </Click>
                </Listeners>
            </ext:LinkButton>
        </Buttons>
    </ext:Window>--%>
    <br />
</asp:Content>
