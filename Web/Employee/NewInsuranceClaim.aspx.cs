﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using System.Xml;
using BLL.BO;
using Web.Helper;

namespace Web.Employee
{
    public partial class NewInsuranceClaim : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();

               // dfSearchClaimDateFrom.Text = DateTime.Now.Date.ToString();
                //dfSearchClaimDateTo.Text = DateTime.Now.Date.ToString();

            }
        }
        public void ClearFields()
        {
            hiddenClaimId.Text = "";
            //dfClaimDate.Text = "";
            //dfDiagnosisDate.Text = "";
            //txtDoctorName.Text = "";
            //txtDoctorAddress.Text = "";
            //txtHostpitalName.Text = "";
            //txtConsultantName.Text = "";
            //txtDetailsOfIllness.Text = "";
            //txtTotalClaim.Clear();
            //dfDateOfAccident.Text = "";
            //txtLocationOfAccidents.Text = "";
            //txtDetailsOfAccident.Text = "";
            //txtDetailsOfTreatment.Text = "";
            //cmbClaimType.Clear();
            //cmbClaimOf.Clear();
            //cmbRelation.Clear();
        }
        public void btnSave_Click(object sender, DirectEventArgs e)
        {
            //DAL.SCClaim obj = new DAL.SCClaim();
            //bool isInsert = string.IsNullOrEmpty(hiddenClaimId.Text.Trim());
            //if (!isInsert)
            //    obj.ClaimID = int.Parse(hiddenClaimId.Text.Trim());

            //if (cmbClaimType.SelectedItem != null && cmbClaimType.SelectedItem.Index != -1)
            //    obj.ClaimType = int.Parse(cmbClaimType.SelectedItem.Value);

            //if (!String.IsNullOrEmpty(dfClaimDate.Text))
            //    obj.ClaimDate = dfClaimDate.SelectedDate;
            //else
            //{
            //    NewMessage.ShowWarningMessage("Claim date is required !");
            //    return;
            //}

            //obj.EmployeeId = SessionManager.CurrentLoggedInEmployeeId;

            //if (cmbClaimOf.SelectedItem != null && cmbClaimOf.SelectedItem.Index != -1)
            //    obj.ClaimOfFamilyId = int.Parse(cmbClaimOf.SelectedItem.Value);

            //if (cmbRelation.SelectedItem != null && cmbRelation.SelectedItem.Index != -1)
            //    obj.RelationId = int.Parse(cmbRelation.SelectedItem.Value);

            //if (!string.IsNullOrEmpty(txtTotalClaim.Text))
            //    obj.TotalClaimAmount = Convert.ToDecimal(txtTotalClaim.Text);

            //if (obj.ClaimType != null && obj.ClaimType == (int)ClaimType.Medical)
            //{
            //    if (!String.IsNullOrEmpty(dfDiagnosisDate.Text) && !dfDiagnosisDate.IsEmpty)
            //        obj.DiagnosisDate = dfDiagnosisDate.SelectedDate;
            //    else
            //    {
            //        NewMessage.ShowWarningMessage("Diagnosis date is required !");
            //        return;
            //    }

            //    if (!String.IsNullOrEmpty(txtDoctorName.Text))
            //        obj.DoctorName = txtDoctorName.Text;
            //    else
            //    {
            //        NewMessage.ShowWarningMessage("Doctor name is required !");
            //        return;
            //    }



            //    if (!String.IsNullOrEmpty(txtDoctorAddress.Text))
            //        obj.DoctorAddress = txtDoctorAddress.Text;



            //    if (!String.IsNullOrEmpty(txtHostpitalName.Text))
            //        obj.HospitalNameAddress = txtHostpitalName.Text;
            //    else
            //    {
            //        NewMessage.ShowWarningMessage("Hospital name is required !");
            //        return;
            //    }


            //    if (!String.IsNullOrEmpty(txtConsultantName.Text))
            //        obj.ConsultantName = txtConsultantName.Text;
            //    else
            //    {
            //        NewMessage.ShowWarningMessage("Consultant name is required !");
            //        return;
            //    }


            //    if (!String.IsNullOrEmpty(txtDetailsOfIllness.Text))
            //        obj.Details = txtDetailsOfIllness.Text;
            //    else
            //    {
            //        NewMessage.ShowWarningMessage("Illness details is required !");
            //        return;
            //    }

            //}


            //if (obj.ClaimType != null && obj.ClaimType == (int)ClaimType.Accidental)
            //{
            //    DateTime? dt = null;
            //    if (!String.IsNullOrEmpty(dfDateOfAccident.Text) && !dfDateOfAccident.IsEmpty)
            //    {
            //        dt = dfDateOfAccident.SelectedDate;//CommonManager.GetEngDate(dfDateOfAccident.Text, SessionManager.IsEnglish);
            //        TimeSpan time = tfttme.SelectedTime;
            //        obj.AccidentDateTime = dt + time;
            //    }
            //    else
            //    {
            //        NewMessage.ShowWarningMessage("Date of Accident is required !");
            //        return;
            //    }

            //    if (!String.IsNullOrEmpty(txtLocationOfAccidents.Text))
            //        obj.AccidentLocation = txtLocationOfAccidents.Text;
            //    else
            //    {
            //        NewMessage.ShowWarningMessage("Location of accident is required !");
            //        return;
            //    }

            //    if (!String.IsNullOrEmpty(txtDetailsOfAccident.Text))
            //        obj.Details = txtDetailsOfAccident.Text;
            //    else
            //    {
            //        NewMessage.ShowWarningMessage("Accident details is required !");
            //        return;
            //    }

            //    if (!String.IsNullOrEmpty(txtDetailsOfTreatment.Text))
            //        obj.DetailsOfTreatment = txtDetailsOfTreatment.Text;
            //    else
            //    {
            //        NewMessage.ShowWarningMessage("Tratment details is required !");
            //        return;
            //    }

            //}
            //Status status = HRManager.InsertUpdateClaimRequest(obj, isInsert);
            //if (status.IsSuccess)
            //{
            //    hiddenClaimId.Text = "";
            //    //windowAddEditClaim.Hide();
            //    PagingToolbar1.DoRefresh();
            //    if (isInsert)
            //        NewMessage.ShowNormalMessage("Information saved successfully.");
            //    else
            //        NewMessage.ShowNormalMessage("Information updated successfully.");
            //}
            //else
            //{
            //    NewMessage.ShowWarningMessage(status.ErrorMessage);
            //}

        }
        public void Initialise()
        {
            storecmbStatus.DataSource = HRManager.GetTextValuesForInsuranceStatus(typeof(ClaimStatus)); //ExtControlHelper.GetTextValues(typeof(ClaimStatus));
            storecmbStatus.DataBind();

            storeCmbClaimType.DataSource = ExtControlHelper.GetTextValues(typeof(ClaimType));
            storeCmbClaimType.DataBind();
            //cmbClaimType.SetValue(ClaimType.Medical);


            //EEmployee emp = new EmployeeManager().GetById(SessionManager.CurrentLoggedInEmployeeId);

            //txtStaffId.Text = emp.EmployeeId.ToString();
            //txtStaffName.Text = emp.Name;
            //txtInsurencePolicyNo.Text = "";

            ////GetFamilyMemberNameByRelation
            //List<HFamily> lst = CommonManager.GetAllActiveFamilyListByEmpID(SessionManager.CurrentLoggedInEmployeeId);
            //HFamily obj = new HFamily();
            //obj.FamilyId = 0;
            //obj.Name = emp.Name;
            //obj.Relation = "Self";
            //lst.Add(obj);
            //storecmbClaimOf.DataSource = lst;
            //storecmbClaimOf.DataBind();

            //FixedValueFamilyRelation fr = new FixedValueFamilyRelation();
            //fr.ID = 0;
            //fr.Name = "Self";
            //List<FixedValueFamilyRelation> Lfr = CommonManager.GetRelationList();
            //Lfr.Add(fr);
            //storeRelation.DataSource = Lfr;
            //storeRelation.DataBind();

        }
        protected void Store_ReadData(object sender, StoreReadDataEventArgs e)
        {
            int type = -1; int status = -1; int ein = -1;
            if (cmbSearchClaimType.SelectedItem != null && cmbSearchClaimType.SelectedItem.Value != null)
            {
                type = int.Parse(cmbSearchClaimType.SelectedItem.Value);
            }

            if (cmbStatus.SelectedItem != null && cmbStatus.SelectedItem.Value != null)
            {
                status = int.Parse(cmbStatus.SelectedItem.Value);
            }
            int? totalRecords = 0;
            ein = SessionManager.CurrentLoggedInEmployeeId;

            DateTime? fromdate = null;
            DateTime? todate = null;
            if (!string.IsNullOrEmpty(dfSearchClaimDateFrom.RawText))
                fromdate =DateTime.Parse( dfSearchClaimDateFrom.Text);

            if (!string.IsNullOrEmpty(dfSearchClaimDateTo.RawText))
                todate = DateTime.Parse(dfSearchClaimDateTo.Text);



            StoreGridClaimList.DataSource = HRManager.GetEmployeeInsuranceClaimList(type, status, ein, fromdate, todate, e.Page - 1, e.Limit, ref totalRecords);
            StoreGridClaimList.DataBind();
            e.Total = totalRecords.Value;



        }

        public void btnAddClaim_Click(object sender, DirectEventArgs e)
        {
            InsuranceClaim.ShowPopUp(hiddenClaimId.Text.Trim());
        }


        public void btnDelete_Click(object sender, DirectEventArgs e)
        {
            if (!string.IsNullOrEmpty(hiddenClaimId.Text.Trim()))
            {
                Status status = HRManager.DeleteClaim(Convert.ToInt32(hiddenClaimId.Text.Trim()));
                if (status.IsSuccess)
                {
                    hiddenClaimId.Text = "";
                    PagingToolbar1.DoRefresh();
                    NewMessage.ShowNormalMessage("Record deleted successfully.");
                }
                else
                {
                    NewMessage.ShowWarningMessage(status.ErrorMessage);
                }
            }
        }
        public void btnEdit_Click(object sender, DirectEventArgs e)
        {
            if (!string.IsNullOrEmpty(hiddenClaimId.Text.Trim()))
            {
                LoadEditData();
            }
        }

        protected void LoadEditData()
        {
            DAL.SCClaim obj = HRManager.getClaim(Convert.ToInt32(hiddenClaimId.Text.Trim()));
            if (obj != null)
            {
                InsuranceClaim.ShowPopUp(hiddenClaimId.Text.Trim());
            }
        }

        //public void btnView_Click(object sender, DirectEventArgs e)
        //{
        //    CellSelectionModel sm = this.GridClaimList.GetSelectionModel() as CellSelectionModel;
        //    hiddenClaimId.Text = sm.SelectedCell.RecordID.ToString();
        //    if (!string.IsNullOrEmpty(hiddenClaimId.Text.Trim()))
        //    {
        //        LoadEditData();
        //    }

        //}
        protected void btnExport_Click(object sender, EventArgs e)
        {
            //List<GetEveningCounterRequestForEmpSPResult> resultSet = AllowanceManager.GetEveningCounterRequestForEmpReport(0, 0, SessionManager.CurrentLoggedInEmployeeId, int.Parse(cmbType.SelectedItem.Value),
            //    int.Parse(cmbStatus.SelectedItem.Value), int.Parse(cmbCounterTypeFilter.SelectedItem.Value));

            //foreach (var item in resultSet)
            //{
            //    item.FormattedStartDate = WebHelper.FormatDateForExcel(item.StartDate.Value);
            //    item.FormattedEndDate = WebHelper.FormatDateForExcel(item.EndDate.Value);
            //}

            //Dictionary<string, string> title = new Dictionary<string, string>();
            //title["Allowance Request Report"] = "";

            //Bll.ExcelHelper.ExportToExcel("Allowance Request Report", resultSet,
            //       new List<String>() { "CounterRequestID", "EmployeeID", "CreatedBy", "CreatedOn", "ApprovalID", "ApprovalName",
            //        "ForwardedOn", "ForwardedBy", "PayrollPeriodId", "ApprovedOn", "ApprovedBy", "RecommendedBy", "RecommendedOn", "RejectedBy", "RejectedOn",
            //        "EveningCounterTypeId", "MonthName", "MonthTotalDays", "Status", "StartDate", "EndDate", "TotalRows", "RowNumber" },
            //   new List<String>() { },
            //   new Dictionary<string, string>() { { "EveningCounterTypeStr", "Allowance Type" }, { "FormattedStartDate", "Start Date" }, { "FormattedEndDate", "End Date" }, { "StatusStr", "Status" } },
            //   new List<string>() { }
            //   , new List<string> { }
            //   , new List<string> { "FormattedStartDate", "FormattedEndDate" }
            //   , new Dictionary<string, string>() { }
            //   , new List<string> { "EveningCounterTypeStr", "FormattedStartDate", "FormattedEndDate", "Days", "Reason", "StatusStr" });

        }
    }
}

