﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL.Entity;
using BLL;
using System.IO;
using BLL.BO;
using Utils.Helper;
using Bll;


namespace Web.Employee
{
    public partial class OvertimeRequestEditPopup : BasePage
    {

        public bool HasImport
        {
            get
            {
                if (ViewState["import"] == null)
                    return false;
                return Convert.ToBoolean(ViewState["import"]);
            }
            set
            {
                ViewState["import"] = value;
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "sdf", "var hasImport = " + (this.HasImport ? "true;" : "false;"), true);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest && !IsPostBack)
            {
                Initialize();
            }
        }

        protected void Initialize()
        {
            cmbOvertimeType.GetStore().DataSource = OvertimeManager.GetOvertimeList();
            cmbOvertimeType.GetStore().DataBind();

            Setting setting = OvertimeManager.GetSetting();

            if (setting.MultipleOvertimeRequestApproval != null && setting.MultipleOvertimeRequestApproval.Value)
            {
                string review = "", approval = "";
                LeaveRequestManager.GetApplyToForEmployee(SessionManager.CurrentLoggedInEmployeeId, ref review, ref approval);
                lblApplyTo.Text = approval;
            }

            if(!string.IsNullOrEmpty(Request.QueryString["RId"]))
            {
                hdnRequestID.Text = Request.QueryString["RId"];

                LoadData();
            }

        }

        private void LoadData()
        {
            OvertimeRequest requestInstance = new OvertimeRequest();
            requestInstance = OvertimeManager.getRequestByID(hdnRequestID.Text.Trim());
            if (requestInstance.Status != 0)
            {
                tfExtraWorkStartAt.Disable();
                tfExtraWorkEndAt.Disable();

                txtReason.Enabled = false;
                btnSave.Enabled = false;
                txtDate.Disable();
                btnSave.Hide();
                txtReason.Disable();

                pnlBorder.Show();
                lblWarningMsg.Text = "Status has been changed. This request cannot be edited.";
                lblWarningMsg.Show();
            }

            txtDate.Disable();
            btnSave.Text = "Update";

            DateTime startDateTime = requestInstance.StartTime.Value;
            DateTime endDateTime = requestInstance.EndTime.Value;

            TimeSpan tsStart = new TimeSpan(startDateTime.Hour, startDateTime.Minute, startDateTime.Second);
            TimeSpan tsEnd = new TimeSpan(endDateTime.Hour, endDateTime.Minute, endDateTime.Second);

            tfExtraWorkStartAt.Value = tsStart;
            tfExtraWorkEndAt.Value = tsEnd;

            txtReason.Text = requestInstance.Reason;

            cmbOvertimeType.SelectedItem.Value = requestInstance.OvertimeTypeId.Value.ToString();

            txtDate.SelectedDate = requestInstance.Date.Value;
        }

        protected void btnSave_Click(object sender, DirectEventArgs e)
        {            
            Page.Validate("SaveOvertimeReq");
            if (Page.IsValid)
            {
                bool isEdit = false;
                OvertimeRequest requestInstance = new OvertimeRequest();
                if (!string.IsNullOrEmpty(hdnRequestID.Text.Trim()))
                {
                    requestInstance.OvertimeRequestID = int.Parse(hdnRequestID.Text.Trim());
                    isEdit = true;
                }

                requestInstance.OvertimeTypeId = int.Parse(cmbOvertimeType.SelectedItem.Value);

                DateTime dt = txtDate.SelectedDate;
                requestInstance.Date = dt;
                requestInstance.EmployeeID = SessionManager.CurrentLoggedInEmployeeId;

                requestInstance.StartTime = new DateTime(dt.Year, dt.Month, dt.Day, tfExtraWorkStartAt.SelectedTime.Hours, tfExtraWorkStartAt.SelectedTime.Minutes, tfExtraWorkStartAt.SelectedTime.Seconds);
                requestInstance.EndTime = new DateTime(dt.Year, dt.Month, dt.Day, tfExtraWorkEndAt.SelectedTime.Hours, tfExtraWorkEndAt.SelectedTime.Minutes, tfExtraWorkEndAt.SelectedTime.Seconds);
                requestInstance.Reason = txtReason.Text;
                requestInstance.Status = (int)OvertimeStatusEnum.Pending;

                int approvalID1 = 0;
                int approvalID2 = 0;
                string approvalName1 = "";
                string approvalName2 = "";

                Setting setting = OvertimeManager.GetSetting();

                if (setting.MultipleOvertimeRequestApproval != null && setting.MultipleOvertimeRequestApproval.Value)
                {
                    List<TextValue> listApproval = LeaveRequestManager.GetApplyToListForEmployee(SessionManager.CurrentLoggedInEmployeeId, true, PreDefindFlowType.Overtime);

                    if (listApproval.Count > 0)
                    {
                        if (listApproval.Count == 1)
                        {
                            approvalID1 = int.Parse(listApproval[0].Value);
                            approvalName1 = listApproval[0].Text;
                        }
                        else
                        {
                            approvalID1 = int.Parse(listApproval[0].Value);
                            approvalName1 = listApproval[0].Text;
                            approvalID2 = int.Parse(listApproval[1].Value);
                            approvalName2 = listApproval[1].Text;
                        }
                    }
                    else
                    {
                        NewMessage.ShowWarningMessage("Approval is required.");
                        return;
                    }
                }
               
                if (approvalID1 != 0)
                {
                    requestInstance.ApprovalID = approvalID1;
                    requestInstance.ApprovalName = approvalName1;
                }

                if (approvalID2 != 0)
                {
                    requestInstance.ApprovalID2 = approvalID2;
                    requestInstance.ApprovalName2 = approvalName2;
                }

                ResponseStatus isSuccess = new ResponseStatus();

                ResponseStatus isValidDate = OvertimeManager.isRequestedDateValid(requestInstance.EmployeeID, requestInstance.Date, requestInstance.StartTime, requestInstance.EndTime, isEdit);

                if (!OvertimeManager.CanEmployeeRequestForOverTime(requestInstance.EmployeeID.Value, requestInstance.OvertimeTypeId.Value, requestInstance.Date.Value))
                {
                    NewMessage.ShowWarningMessage("Employee is not eligible for " + cmbOvertimeType.SelectedItem.Text + ".");
                    return;
                }

                int minimumOvertimeBuffer = OvertimeManager.GetOvertimeMinimumBufferTime(requestInstance.EmployeeID.Value, requestInstance.Date.Value);

                if (minimumOvertimeBuffer != 0)
                {
                    TimeSpan EndTimeSpan = OvertimeManager.getOfficeEndTime(SessionManager.CurrentLoggedInEmployeeId);

                    TimeSpan ts = tfExtraWorkStartAt.SelectedTime - EndTimeSpan;

                    if (ts.TotalMinutes < minimumOvertimeBuffer)
                    {
                        NewMessage.ShowWarningMessage("You cannot apply overtime earlier than " + minimumOvertimeBuffer + " minutes from end of office time.");
                        tfExtraWorkStartAt.Focus();
                        return;
                    }
                }


                requestInstance.NepaliDate = txtDate.SelectedDate.ToShortDateString().Replace('-', '/');

                if (isValidDate.IsSuccessType)
                {
                    isSuccess = OvertimeManager.InsertUpdateRequest(requestInstance);
                    if (isSuccess.IsSuccessType)
                    {
                        NewMessage.ShowNormalMessage("Overtime request updated successfully.","closePopup()");
                    }
                    else
                    {
                        NewMessage.ShowWarningMessage(isSuccess.ErrorMessage);
                    }
                }
                else
                {
                    NewMessage.ShowWarningMessage(isValidDate.ErrorMessage);
                }
            }
            else
            {
                NewMessage.ShowWarningMessage("Validation fail.");
            }

        }


    }
}