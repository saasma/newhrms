﻿<%@ Page Language="C#" Title="Allowance Approval" AutoEventWireup="true" MasterPageFile="~/Master/ForEmployeePopupPage.Master"
    CodeBehind="AllowanceApprovePopup.aspx.cs" Inherits="Web.Employee.AllowanceApprovePopup" %>

<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<%@ Register Assembly="DevExpress.Web.v16.1, Version=16.1.8.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function ConfirmInstitutionDeletion() {
            if (ddllist.selectedIndex == -1) {
                alert('No Insurance company to delete.');
                clearUnload();
                return false;
            }
            if (confirm('Do you want to delete the insurance company?')) {
                clearUnload();
                return true;
            }
            else {
                return false;
            }
        }

        function DisplayInstitutionInTextBox(dropdown_id) {

        }



        function closePopup() {
            if (hasImport)
                window.opener.refreshEventList(window); ;
        }


        window.onunload = closePopup;  
    </script>
    <script src="override.js" type="text/javascript"></script>
    <style type="text/css">
        body
        {
            background-color: #E8F1FF !important;
        }
        strong
        {
            text-align: right;
            font-size: 12px !important;
            font-weight: bold !important;
        }
        .tbl > tbody > tr > td
        {
            padding-bottom: 10px;
        }
        div.notify
        {
            width: inherit !important;
        }
        .marginal
        {
            margin-top: 10px !important;
        }
        .tableLightColor th
        {
            font-weight: normal !important;
        }
        .marginal
        {
            background-color: #E8F1FF !important;
            width: 550px;
            height: 520px;
            margin-top: 0px !important;
            margin-left: 0px !important;
            padding-left: 20px !important;
            padding-top: 20px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
    <ext:ResourceManager ID="ResourceManager1" DisableViewState="false" ScriptMode="Release"
        Namespace="" runat="server" />
    <asp:HiddenField ID="Hidden_CounterTypePrev" runat="server" />
    <asp:HiddenField ID="Hidden_FromPrev" runat="server" />
    <asp:HiddenField ID="Hidden_ToPrev" runat="server" />
    <asp:HiddenField ID="Hidden_DaysPrev" runat="server" />
    <ext:Window runat="server" Shadow="false" Modal="true" Hidden="true" Width="300"
        Height="200" Title="Forward to next Reviewer" ID="windowReview">
        <Content>
            <div style="padding: 10px">
                <table>
                    <tr>
                        <td colspan="2">
                            <ext:ComboBox ID="cmbReviewerList" LabelAlign="Top" DisplayField="Text" ValueField="Value"
                                LabelSeparator="" ForceSelection="true" Width="200" runat="server" FieldLabel="Reviewer *">
                                <Store>
                                    <ext:Store ID="store1" runat="server">
                                        <Model>
                                            <ext:Model ID="Model7" runat="server" IDProperty="Value">
                                                <Fields>
                                                    <ext:ModelField Name="Value" Type="String" />
                                                    <ext:ModelField Name="Text" />
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <%--<SelectedItems>
                                    <ext:ListItem Index="0" />
                                </SelectedItems>--%>
                            </ext:ComboBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="Reviewer is required."
                                ControlToValidate="cmbReviewerList" ValidationGroup="ForwardForReview" Display="None" />
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-top: 10px">
                            <ext:Button ID="btnForward" runat="server" Width="90" StyleSpec="margin-right:5px;"
                                Text="Forward">
                                <DirectEvents>
                                    <Click OnEvent="btnForward_Click">
                                        <Confirmation ConfirmRequest="true" Message="Are you sure to forward the request for Review?" />
                                        <EventMask ShowMask="true" />
                                    </Click>
                                </DirectEvents>
                                <Listeners>
                                    <Click Handler="valGroup='ForwardForReview';return CheckValidation();" />
                                </Listeners>
                            </ext:Button>
                        </td>
                        <td style="padding-top: 10px">
                            <ext:Button ID="Button2" runat="server" Width="90" StyleSpec="margin-right:5px;"
                                Text="Close">
                                <Listeners>
                                    <Click Handler="#{windowReview}.hide();" />
                                </Listeners>
                            </ext:Button>
                        </td>
                    </tr>
                </table>
            </div>
        </Content>
    </ext:Window>
    <div class="popupHeader">
        <span style='margin-left: 20px; padding-top: 8px; display: block' runat="server"
            id="spanOvertime">Allowance approval</span>
    </div>
    <div class="marginal" style="width: auto; margin: 0px;">
        <uc2:MsgCtl ID="divMsgCtl" EnableViewState="false" Hide="true" Width="390px" runat="server" />
        <uc2:WarningCtl ID="divWarningMsg" EnableViewState="false" Hide="true" runat="server" />
        <asp:HiddenField ID="Hidden_RequestID" runat="server" />
        <table style="margin-bottom: 25px;" class="tbl">
            <tbody>
                <tr>
                    <td colspan="2">
                        <ext:DisplayField FieldLabel="Allowance Type" LabelAlign="Left" ID="lblCounterType"
                            runat="server" Width="350" LabelWidth="100">
                        </ext:DisplayField>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <ext:DisplayField FieldLabel="EIN" LabelAlign="Left" ID="lblDate" runat="server"
                            Width="250" LabelWidth="100">
                        </ext:DisplayField>
                    </td>
                </tr>
                <tr>
                    <td>
                        <ext:DisplayField FieldLabel="Employee" LabelAlign="Left" ID="lblName" runat="server"
                            Width="250" LabelWidth="100">
                        </ext:DisplayField>
                    </td>
                    <td style="padding-left: 30px">
                        <ext:LinkButton runat="server" ID="btnHistory" Text="Allowance History">
                            <DirectEvents>
                                <Click OnEvent="btnHistory_Click">
                                    <EventMask ShowMask="true" />
                                </Click>
                            </DirectEvents>
                        </ext:LinkButton>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <ext:DisplayField FieldLabel="Position" LabelAlign="Left" ID="lblPosition" runat="server"
                            Width="250" LabelWidth="100">
                        </ext:DisplayField>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <ext:DisplayField FieldLabel="Branch" LabelAlign="Left" ID="lblBranch" runat="server"
                            Width="250" LabelWidth="100">
                        </ext:DisplayField>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <ext:DisplayField FieldLabel="Reason" LabelSeparator="" LabelAlign="Left" ID="lblReason" runat="server"
                            Width="400" LabelWidth="100">
                        </ext:DisplayField>
                    </td>
                </tr>
                <tr>
                    <td>
                        <ext:DateField ID="txtFromDate" runat="server" FieldLabel="From Date *" LabelAlign="Left"
                            LabelSeparator="" Width="250" LabelWidth="100" AllowBlank="false">
                            <DirectEvents>
                                <Select OnEvent="Date_Change">
                                </Select>
                            </DirectEvents>
                            <Plugins>
                                <ext:GenericPlugin ID="HighlightDates1" runat="server" InstanceName="Ext.ux.DatePicker.HighlightWithNepDates" />
                            </Plugins>
                        </ext:DateField>
                        <asp:RequiredFieldValidator Enabled="true" Display="None" ID="valtxtPublicationName"
                            runat="server" ValidationGroup="SaveUpdate" ControlToValidate="txtFromDate" ErrorMessage="From date is required." />
                    </td>
                    <td style="padding-left: 30px">
                        <ext:DisplayField ID="txtFromDateNep" runat="server" FieldLabel="" LabelWidth="0"
                            LabelSeparator="" Width="175" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <ext:DateField ID="txtToDate" runat="server" LabelWidth="100" FieldLabel="To Date *"
                            LabelAlign="Left" LabelSeparator="" Width="250" Hidden="false" AllowBlank="false">
                            <DirectEvents>
                                <Select OnEvent="Date_Change">
                                </Select>
                            </DirectEvents>
                            <Plugins>
                                <ext:GenericPlugin ID="GenericPlugin1" runat="server" InstanceName="Ext.ux.DatePicker.HighlightWithNepDates" />
                            </Plugins>
                            
                        </ext:DateField>
                        <asp:RequiredFieldValidator Enabled="true" Display="None" ID="RequiredFieldValidator111"
                            runat="server" ValidationGroup="SaveUpdate" ControlToValidate="txtToDate" ErrorMessage="To Date is required." />
                    </td>
                    <td style="padding-left: 30px">
                        <ext:DisplayField ID="txtToDateNep" runat="server" FieldLabel="" LabelWidth="0" LabelSeparator=""
                            Width="175" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <ext:NumberField ID="txtDays" AllowDecimals="false" runat="server" LabelAlign="Left"
                            FieldLabel="Units *" LabelSeparator=" " Width="250" LabelWidth="100" Hidden="false"
                            AllowBlank="false" MinValue="0">
                        </ext:NumberField>
                        <asp:RequiredFieldValidator Enabled="true" Display="None" ID="RequiredFieldValidator11"
                            runat="server" ValidationGroup="SaveUpdate" ControlToValidate="txtDays" ErrorMessage="Day(s) is required." />
                    </td>
                    <td style="padding-left: 30px">
                        <ext:DisplayField Text="" LabelAlign="Top"
                            ID="lblRate" runat="server" Width="250" LabelWidth="100">
                        </ext:DisplayField>
                    </td>
                </tr>
                <tr>
                    <td>
                        <ext:DisplayField FieldLabel="Recommender" LabelAlign="Top" ID="lblRecommendBy" runat="server"
                            Width="250" LabelWidth="100">
                        </ext:DisplayField>
                    </td>
                    <td style="padding-left: 30px">
                        <ext:DisplayField FieldLabel="Approval" LabelAlign="Top" ID="lblApprovedBy" runat="server"
                            Width="250" LabelWidth="100">
                        </ext:DisplayField>
                    </td>
                </tr>
            </tbody>
        </table>
        <table class="tbl" style="width: 500px;">
            <tr>
                <td colspan="2">
                    <ext:Button runat="server" Width="120px" Cls="btn btn-primary" ID="btnApprove" Text="<i></i>Approve">
                        <DirectEvents>
                            <Click OnEvent="btnApprove_Click">
                                <EventMask ShowMask="true" />
                                <Confirmation ConfirmRequest="true" Message="Are you sure you want to approve this request?" />
                            </Click>
                        </DirectEvents>
                        <Listeners>
                            <Click Handler="valGroup = 'SaveUpdate'; return CheckValidation();">
                            </Click>
                        </Listeners>
                    </ext:Button>
                    <ext:Button runat="server" Width="120px" Visible="false" Cls="btn btn-primary" ID="btnRecommend"
                        Text="<i></i>Recommend">
                        <DirectEvents>
                            <Click OnEvent="btnRecommend_Click">
                                <EventMask ShowMask="true" />
                                <Confirmation ConfirmRequest="true" Message="Are you sure you want to recommend this request?" />
                            </Click>
                        </DirectEvents>
                        <Listeners>
                            <Click Handler="valGroup = 'SaveUpdate'; return CheckValidation();">
                            </Click>
                        </Listeners>
                    </ext:Button>
                </td>
                <td>
                    <ext:Button ID="btnRecommendForReview" Cls="btn btn-success" Hidden="true" ToolTip="Forward for Review"
                        runat="server" Width="120px" StyleSpec="margin-right:10px;" Text="Forward">
                        <Listeners>
                            <Click Handler="#{windowReview}.show();" />
                        </Listeners>
                    </ext:Button>
                </td>
                <td colspan="2">
                    <ext:Button runat="server" Cls="btn btn-info" Width="120px" ID="btnReject" Text="<i></i>Reject">
                        <DirectEvents>
                            <Click OnEvent="btnReject_Click">
                                <EventMask ShowMask="true" />
                                <Confirmation ConfirmRequest="true" Message="Are you sure you want to reject this request?" />
                            </Click>
                        </DirectEvents>
                        <Listeners>
                            <Click Handler="valGroup = 'SaveUpdate'; return CheckValidation();">
                            </Click>
                        </Listeners>
                    </ext:Button>
                </td>
            </tr>
        </table>
        <div>
            <cc2:EmptyDisplayGridView CssClass="tableLightColor" UseAccessibleHeader="true" ShowHeaderWhenEmpty="True"
                ID="gridHistory" runat="server" AutoGenerateColumns="False" CellPadding="4" GridLines="None"
                ShowFooterWhenEmpty="False" Width="480px">
                <RowStyle BackColor="#E3EAEB" />
                <Columns>
                    <asp:BoundField HeaderStyle-Width="160px" HeaderStyle-HorizontalAlign="Left" HeaderText="Modified by"
                        DataField="ModifiedByName"></asp:BoundField>
                    <asp:BoundField DataField="ModifiedOn" DataFormatString="{0:dd-M-yyyy}" HeaderStyle-Width="125px"
                        HeaderStyle-HorizontalAlign="Left" HeaderText="Modified on"></asp:BoundField>
                    <asp:BoundField DataField="Column1Before" HeaderStyle-Width="180px" HeaderStyle-HorizontalAlign="Left"
                        HeaderText="Before"></asp:BoundField>
                    <asp:BoundField DataField="Column1After" HeaderStyle-Width="180px" HeaderStyle-HorizontalAlign="Left"
                        HeaderText="After"></asp:BoundField>
                </Columns>
                <RowStyle CssClass="odd" />
                <AlternatingRowStyle CssClass="even" />
                <EmptyDataTemplate>
                    <b>No list. </b>
                </EmptyDataTemplate>
            </cc2:EmptyDisplayGridView>
        </div>
        <div>
            <cc2:EmptyDisplayGridView CssClass="tableLightColor" UseAccessibleHeader="true" ShowHeaderWhenEmpty="True"
                ID="EmptyDisplayGridView1" runat="server" AutoGenerateColumns="False" CellPadding="4"
                GridLines="None" ShowFooterWhenEmpty="False" Width="480px">
                <RowStyle BackColor="#E3EAEB" />
                <Columns>
                    <asp:BoundField HeaderStyle-Width="160px" HeaderStyle-HorizontalAlign="Left" HeaderText="Allowance"
                        DataField="AllowanceName"></asp:BoundField>
                    <asp:BoundField DataField="StartDate" HeaderStyle-Width="180px" HeaderStyle-HorizontalAlign="Left"
                        HeaderText="Start Date"></asp:BoundField>
                    <asp:BoundField DataField="EndDate" HeaderStyle-Width="180px" HeaderStyle-HorizontalAlign="Left"
                        HeaderText="End Date"></asp:BoundField>
                    <asp:BoundField DataField="Units" HeaderStyle-Width="180px" HeaderStyle-HorizontalAlign="Left"
                        HeaderText="Units"></asp:BoundField>
                    <asp:BoundField DataField="Status" HeaderStyle-Width="180px" HeaderStyle-HorizontalAlign="Left"
                        HeaderText="Status"></asp:BoundField>
                </Columns>
                <RowStyle CssClass="odd" />
                <AlternatingRowStyle CssClass="even" />
                <EmptyDataTemplate>
                    <b>No list. </b>
                </EmptyDataTemplate>
            </cc2:EmptyDisplayGridView>
        </div>
        <ext:Window ButtonAlign="Left" runat="server" Layout="BorderLayout" ID="windowNew"
            Title="History" Width="500" Height="300" Hidden="true">
            <Items>
                <ext:GridPanel Region="Center" ID="grid" runat="server" Cls="itemgrid" Scroll="Both">
                    <Store>
                        <ext:Store ID="Store2" runat="server" AutoLoad="true">
                            <Model>
                                <ext:Model ID="Model4" runat="server">
                                    <Fields>
                                        <ext:ModelField Name="AllowanceName" Type="String" />
                                        <ext:ModelField Name="StartDate" Type="String" />
                                        <ext:ModelField Name="EndDate" Type="String" />
                                        <ext:ModelField Name="Units" Type="String" />
                                        <ext:ModelField Name="Status" Type="String" />
                                    </Fields>
                                </ext:Model>
                            </Model>
                        </ext:Store>
                    </Store>
                    <ColumnModel>
                        <Columns>
                            <ext:Column ID="Column4" Sortable="true" MenuDisabled="true" runat="server" Text="Type"
                                Width="130" Align="Left" DataIndex="AllowanceName">
                            </ext:Column>
                            <ext:Column ID="colEmployeeName" Sortable="true" MenuDisabled="true" runat="server"
                                Text="Start Date" Width="150" Align="Left" DataIndex="StartDate">
                            </ext:Column>
                            <ext:Column ID="Column1" Sortable="true" MenuDisabled="true" runat="server" Text="End Date"
                                Width="150" Align="Left" DataIndex="EndDate">
                            </ext:Column>
                            <ext:Column ID="Column5" Sortable="true" MenuDisabled="true" runat="server" Text="Units"
                                Width="60" Align="Center" DataIndex="Units">
                            </ext:Column>
                            <ext:Column ID="Column6" Sortable="true" MenuDisabled="true" runat="server" Text="Status"
                                Width="100" Align="Left" DataIndex="Status">
                            </ext:Column>
                        </Columns>
                    </ColumnModel>
                    <View>
                        <ext:GridView ID="GridView1" runat="server" StripeRows="true" />
                    </View>
                </ext:GridPanel>
            </Items>
            <%--<Buttons>
                <ext:Button ID="btnClearSelection" StyleSpec="margin-left:10px" runat="server" Cls="btn btn-primary"
                    Text="Cancel" Width="120">
                    <Listeners>
                        <Click Handler="#{windowNew}.hide();" />
                    </Listeners>
                </ext:Button>
            </Buttons>--%>
        </ext:Window>
    </div>
</asp:Content>
