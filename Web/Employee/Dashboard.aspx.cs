﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL;
using BLL.Manager;
using Web.ReportDataSetTableAdapters;
using Web.Master;
using BLL;
using Web.CP.Report.Templates.Pay.Detail;
using Utils;
using BLL.Base;
using System.IO;
using Utils.Calendar;
using BLL.BO;
using Ext.Net;
using System.Text;

namespace Web.Employee
{
    public partial class Dashboard : BasePage
    {

        public string EmpSearchNavigation()
        {
            return ResolveUrl("~/Employee/HR/EmployeeSearchDetail.aspx");
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            //if (CommonManager.CompanySetting.WhichCompany == WhichCompany.AceTravels)
            //{
            //    Response.Redirect("~/Employee/EmployeeAAA/PersonalDetails.aspx");
            //}


            if (CommonManager.CompanySetting.WhichCompany == WhichCompany.NIBL)
            {
                Response.Redirect("DashboardNew.aspx");
            }

            if (!IsPostBack && ! X.IsAjaxRequest)
            {
                Initialise();
                RegisterLegendColors();

                ResourceManager1.RegisterIcon(Icon.Information);

                CheckPwdChangedBeforePwdChangingDays();
                ShowPasswordChangedMessage();

                BindActivity();
                CheckIn();
                CheckOutPopup();
                TimeAttRejectedComment();

                LoadHPLDutySchedule();

                if (CommonManager.CompanySetting.WhichCompany == WhichCompany.Civil)
                    LoadJoinAndRetiring();
            }            

        }


        public void LoadJoinAndRetiring()
        {
            divNewJoinesAndRetiring.Visible = true;

            CustomDate today = CustomDate.GetTodayDate(IsEnglish);
            int currentYear = today.Year;

            DateTime FromDate;// DateManager.GetStartDate(Utils.Helper.Util.GetCurrentDateAndTime().AddDays(-days));
            DateTime ToDate;// DateManager.GetEndDate(FromDate.AddDays(6));

            CustomDate firstDateOfThisMonth = new CustomDate(1, today.Month, currentYear, IsEnglish);
            CustomDate lastDateOfThisMonth = firstDateOfThisMonth.GetLastDateOfThisMonth();

            FromDate = DateManager.GetStartDate(firstDateOfThisMonth.EnglishDate);
            ToDate = DateManager.GetEndDate(lastDateOfThisMonth.EnglishDate);

            List<EmployeeServiceBo> CompeteThisMonth = DashboardManager.GetEmployeeJoiningListForEmpDashboard(FromDate, ToDate)
                .OrderBy(x => x.JoinDateEng).ToList();

            gridNewJoines.GetStore().DataSource = CompeteThisMonth;
            gridNewJoines.GetStore().DataBind();



            gridRetiring.GetStore().DataSource = DashboardManager.GetEmployeeRetiringListForDashboard(FromDate, ToDate).ToList();
            gridRetiring.GetStore().DataBind();
        }

        private void LoadHPLDutySchedule()
        {
            // show hpl duty list to only Dr Sandip (49)
            if (CommonManager.CompanySetting.WhichCompany == WhichCompany.HPL && SessionManager.CurrentLoggedInEmployeeId == 49)
            {
              
                int columnWidth = 70;

                Model SiteModel = (this.gridSchedule.Store[0].Model[0] as Model);
                string columnId = "Item_{0}";

                int weekNumber = AttendanceManager.GetWeekNumber(DateTime.Today);

                List<HPLDutyPeriod> listPeriods = listPeriods = NewHRManager.GetHPLDutyPeriodsForEmployeeDashboardOfSandip();

                int listcount = listPeriods.Count;

                int k = 0;

                foreach (var item in listPeriods)
                {
                    string gridIndexIdMonth = string.Format(columnId, ++k);

                    ModelField field = new ModelField(gridIndexIdMonth, ModelFieldType.String);
                    field.UseNull = true;
                    SiteModel.Fields.Add(field);

                    Column column = new Column();
                    column.Text = item.MonthName;
                    column.Sortable = false;
                    column.Draggable = false;
                    column.MenuDisabled = true;

                    Column column1 = new Column();
                    column1.Text = item.PeriodName;
                    column1.Sortable = false;
                    column1.Draggable = false;
                    column1.MenuDisabled = true;

                    column.Columns.Add(column1);

                    Column dataColumn = new Column { DataIndex = gridIndexIdMonth, Align = Alignment.Center, Text = "WK " + item.WeekNo.ToString().PadLeft(2, '0'), Width = columnWidth, MenuDisabled = true, Sortable = false, Draggable = false };
                    dataColumn.Renderer.Fn = "myRenderer1";
                    column1.Columns.Add(dataColumn);

                    gridSchedule.ColumnModel.Columns.Add(column);

                }

                List<int> employeeIds = NewHRManager.GetScheduleEmployeesByYearForDashboard(DateTime.Today.Year);

                object[] data = new object[employeeIds.Count];

                int sn = 0, l = 2;
                int row = 0;
                foreach (int employeeId in employeeIds)
                {
                    sn++;
                    object[] rowData = new object[listcount + 3];
                    rowData[0] = sn.ToString();
                    rowData[1] = employeeId.ToString();
                    rowData[2] = EmployeeManager.GetEmployeeById(employeeId).Name;
                    l = 2;
                    foreach (var item in listPeriods)
                    {
                        HPLDutySchedule obj = NewHRManager.GetEmployeeDutyScheduleForPeriod(employeeId, item.PeriodId);
                        if (obj != null)
                        {
                            if (obj.Value != null && obj.Value.Value)
                                rowData[++l] = "On";
                            else
                                rowData[++l] = "Off";
                        }
                        else
                            rowData[++l] = null;
                    }

                    //Add a Row
                    data[row++] = rowData;

                }

                gridSchedule.Store[0].DataSource = data;
                gridSchedule.Store[0].DataBind();
            }
            else
            {
                gridSchedule.Visible = false;
                divHPLDuty.Visible = false;
            }
        }
        

        void SetDate()
        {
            try
            {
                DateTime date = BLL.BaseBiz.GetCurrentDateAndTime();
                //set dates
                lblDay.Text = date.DayOfWeek.ToString();

                lblEngDate.Text = date.ToString("dd MMMM, yyyy");


                //CultureInfo culture = CultureInfo.GetCultureInfo("ne-NP");
                CustomDate nepalidate = DateManager.GetTodayNepaliDate();

                string[] yearNos = new string[4];
                yearNos[0] = nepalidate.Year.ToString()[0].ToString();
                yearNos[1] = nepalidate.Year.ToString()[1].ToString();
                yearNos[2] = nepalidate.Year.ToString()[2].ToString();
                yearNos[3] = nepalidate.Year.ToString()[3].ToString();

                string[] dayNos = new string[2];
                dayNos[0] = "";
                dayNos[1] = "";


                dayNos[0] = nepalidate.Day.ToString()[0].ToString();

                lblNepDate.Text = Resources.NepaliCalendar.ResourceManager.GetString("N" + dayNos[0].ToString());
                if (nepalidate.Day.ToString().Length >= 2)
                {
                    dayNos[1] = nepalidate.Day.ToString()[1].ToString();
                    lblNepDate.Text += Resources.NepaliCalendar.ResourceManager.GetString("N" + dayNos[1].ToString());
                }

                lblNepDate.Text += " " + Resources.NepaliCalendar.ResourceManager.GetString(DateHelper.GetMonthName(nepalidate.Month, false))
                    + ", " +

                    Resources.NepaliCalendar.ResourceManager.GetString("N" + yearNos[0]) +
                    Resources.NepaliCalendar.ResourceManager.GetString("N" + yearNos[1]) +
                    Resources.NepaliCalendar.ResourceManager.GetString("N" + yearNos[2]) +
                    Resources.NepaliCalendar.ResourceManager.GetString("N" + yearNos[3])
                    ;
            }
            catch (Exception exp1) { }
        }

        void BindLeaveList(int employeeId)
        {
            PayrollPeriod lastPayroll = CommonManager.GetLastPayrollPeriod();

            List<TextValue> leaveList = new List<TextValue>();

            string days = "days";
            if (CommonManager.CalculationConstant.CompanyHasHourlyLeave.Value)
                days = "hours";

            if (lastPayroll != null)
            {
                List<GetLeaveListAsPerEmployeeResult> list = LeaveAttendanceManager
                    .GetAllLeavesForLeaveRequest(employeeId, lastPayroll.PayrollPeriodId, true)
                    .Where(x => x.IsParentGroupLeave == false).ToList();

                //  List<GetEmployeeLeaveBalanceResult> balanceList = LeaveAttendanceManager.GetEmployeeLeaveBalance(employeeId);
                foreach (GetLeaveListAsPerEmployeeResult balance in list)
                {
                    //if (balance.FreqOfAccrual != LeaveAccrue.COMPENSATORY)
                    {
                        TextValue leave = new TextValue();
                        leave.Text
                            =string.Format("{0}<br/>{1} {2} left", balance.Title, string.Format("{0:0.##}", balance.NewBalance),days
                           );

                        leaveList.Add(leave);
                    }
                }

            }
            rptLeaveList.DataSource = leaveList;
            rptLeaveList.DataBind();
        }

        void RegisterLegendColors()
        {
            StringBuilder str = new StringBuilder("\nlegends = new Array();");

            //add holiday legend color also
            str.AppendFormat("legends['{0}'] = '{1}';", HolidaysConstant.National_Holiday, HolidaysConstant.GetColor(HolidaysConstant.National_Holiday));
            str.AppendFormat("legends['{0}'] = '{1}';", HolidaysConstant.Caste_Holiday, HolidaysConstant.GetColor(HolidaysConstant.Caste_Holiday));
            str.AppendFormat("legends['{0}'] = '{1}';", HolidaysConstant.Weekly_Holiday, HolidaysConstant.GetColor(HolidaysConstant.Weekly_Holiday));
            str.AppendFormat("legends['{0}'] = '{1}';", HolidaysConstant.Female_Holiday, HolidaysConstant.GetColor(HolidaysConstant.Female_Holiday));

            X.ResourceManager.RegisterBeforeClientInitScript(str.ToString());

            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "LegendColorReg123", str.ToString(), true);
        }

        void BindOtherRequestedLeave(int employeeId)
        {


            //DateTime date = BLL.BaseBiz.GetCurrentDateAndTime();
            //DateTime minDate = date.AddDays(CommonManager.CompanySetting.LeaveApprovalMinimumPastDays * -1);

            List<GetLeaveByDateApprovalResult> list = LeaveAttendanceManager
                .GetLeaveByDateApproval(((int)LeaveRequestStatusEnum.Request
                    + "-" + (int)LeaveRequestStatusEnum.Recommended).ToString(),
                SessionManager.CurrentCompanyId, -1, 0, 50, -1, -1, "", "0", SessionManager.CurrentLoggedInEmployeeId);


            rptRequestedLeaveList.DataSource = list;
            rptRequestedLeaveList.DataBind();
        }
        private void Initialise()
        {


            /*
            HolidayManager holidayMgr = new HolidayManager();
            storeMYTimesheet.DataSource = holidayMgr.GetHolidayList();
            storeMYTimesheet.DataBind();
            */

            try
            {

                DateTime todayDate = new DateTime();
                todayDate = CommonManager.GetCurrentDateAndTime();
                HolidayClass service = new HolidayClass();
                //LeaveRequestService service = new LeaveRequestService();
                List<GetHolidaysForAttendenceResult> bindList = new List<GetHolidaysForAttendenceResult>();

                //DateTime FromDate = new DateTime();
                DateTime ToDate = new DateTime();
                DateTime TodayDate = new DateTime();

                TodayDate = CommonManager.GetCurrentDateAndTime();

                //FromDate = new DateTime(todayDate.Year, 1, 1);
                ToDate = TodayDate.AddYears(10);

                bindList = service.GetHolidays(TodayDate, ToDate);
                bindList = bindList.Where(x => x.IsWeekly != true && x.Type != 0).ToList();


                List<NoticeBoard> noticeBoard = new List<NoticeBoard>();
                noticeBoard = NoticeManager.getAllNotice((int)NoticeBoardStatusEnum.Published);
                noticeBoard = noticeBoard
                                .OrderByDescending(x => x.PublishDate)
                                .Where(y => y.ExpiryDate == null || y.ExpiryDate >= CommonManager.GetCurrentDateAndTime())
                                .Take(7)
                                .ToList();
                //noticeBoard = noticeBoard.Take(7).ToList();
                storeNoticeBoard.DataSource = noticeBoard;
                storeNoticeBoard.DataBind();

                if (noticeBoard.Count <= 0)
                    gridNoticeBoard.Hide();


                List<GetHolidaysForAttendenceResult> list = new List<GetHolidaysForAttendenceResult>();

                foreach (var item in bindList)
                {
                    if (item.DateEng != null)
                    {
                        item.EngDateLongForDashboard = item.DateEng.Value.ToString("ddd dd MMM yyyy");
                        int days = (item.DateEng.Value.Date - CommonManager.GetCurrentDateAndTime().Date).Days;
                        if (days < 0)
                            continue;

                        list.Add(item);

                        item.DaysLeftForDashboard = "In " + days.ToString() + " Days";
                        item.NepDateLongForDashboard = item.DateEngText;

                        CustomDate nepalidate = new CustomDate(item.DateEng.Value.Day, item.DateEng.Value.Month, item.DateEng.Value.Year, true); //DateManager.GetTodayNepaliDate();
                        nepalidate = CustomDate.ConvertEngToNep(nepalidate);

                        string[] yearNos = new string[4];
                        yearNos[0] = nepalidate.Year.ToString()[0].ToString();
                        yearNos[1] = nepalidate.Year.ToString()[1].ToString();
                        yearNos[2] = nepalidate.Year.ToString()[2].ToString();
                        yearNos[3] = nepalidate.Year.ToString()[3].ToString();

                        string[] dayNos = new string[2];
                        dayNos[0] = "";
                        dayNos[1] = "";


                        dayNos[0] = nepalidate.Day.ToString()[0].ToString();

                        item.NepDateLongForDashboard = Resources.NepaliCalendar.ResourceManager.GetString("N" + dayNos[0].ToString());
                        if (nepalidate.Day.ToString().Length >= 2)
                        {
                            dayNos[1] = nepalidate.Day.ToString()[1].ToString();
                            item.NepDateLongForDashboard += Resources.NepaliCalendar.ResourceManager.GetString("N" + dayNos[1].ToString());
                        }

                        item.NepDateLongForDashboard += " " + Resources.NepaliCalendar.ResourceManager.GetString(DateHelper.GetMonthName(nepalidate.Month, false))
                            + ", " +

                            Resources.NepaliCalendar.ResourceManager.GetString("N" + yearNos[0]) +
                            Resources.NepaliCalendar.ResourceManager.GetString("N" + yearNos[1]) +
                            Resources.NepaliCalendar.ResourceManager.GetString("N" + yearNos[2]) +
                            Resources.NepaliCalendar.ResourceManager.GetString("N" + yearNos[3])
                            ;

                    }
                }


                storeMYTimesheet.DataSource = list;
                storeMYTimesheet.DataBind();

                //List<ResignationRequest> resignationReqList = CommonManager.GetAssignedResignationDetail();
                //if (resignationReqList.Count != 0)
                //{
                //    foreach (var resignationreq in resignationReqList)
                //    {
                //        resignationreq.DaysCount = CommonManager.DaysCount(resignationreq.ExpectedDate.ToString());
                //    }
                //    storeResignationReqList.DataSource = resignationReqList;
                //    storeResignationReqList.DataBind();
                //}
                //else
                //{
                //    regreq.Style["display"] = "none";
                //}

            }
            catch (Exception exp)
            {
                Log.log("Employee dashboard error", exp);
            }

            CommonManager mgr = new CommonManager();

            int empId = SessionManager.CurrentLoggedInEmployeeId;
            EEmployee emp = EmployeeManager.GetEmployeeById(empId);

            if (emp == null)
                return;

            bool hasPhoto = false;
            if (emp.HHumanResources.Count > 0)
            {
                if (!string.IsNullOrEmpty(emp.HHumanResources[0].UrlPhotoThumbnail))
                {
                    image.ImageUrl = Page.ResolveUrl(Config.UploadLocation + "/" + emp.HHumanResources[0].UrlPhotoThumbnail);
                    if (File.Exists(Server.MapPath(image.ImageUrl)))
                        hasPhoto = true;
                }
            }
            if (!hasPhoto)
            {
                image.ImageUrl = Page.ResolveUrl(Web.Helper.WebHelper.GetEmployeeNoImage(empId));

            }


            

            welcome.InnerHtml =
                string.Format(welcome.InnerHtml,emp.Title + " " + emp.FirstName);

            SetDate();

            BindLeaveList(empId);

            BindOtherRequestedLeave(empId);

            DateTime now = DateTime.Now;

            // message
            rptMessages.DataSource = PayrollMessageManager.GetPayrollMessageByUserName(
                SessionManager.User.UserName, 0, 50).Data
                .Where(x => (x.IsRead==false || x.DueDateEng>now) 
                    && x.Subject.Contains("Leave Request From") == false &&
                    x.Subject.Contains("Leave Approved By") == false)
                    .OrderByDescending(x => x.ReceivedDateEng).ToList();                
            rptMessages.DataBind();

            // This Week
            rptThisWeek.DataSource = DashboardManager.GetEmployeeThisWeekList();
            rptThisWeek.DataBind();

            // Next 30 days
            // This Week
            rpt30Days.DataSource = DashboardManager.Next30DaysAfterThisWeekList();
            rpt30Days.DataBind();

            LoadBlocks();

            // hide attendance
            if (CommonManager.CompanySetting.WhichCompany == WhichCompany.WDN)
                linkAttendance.Visible = false;
        }

        //private void BindMessages()
        //{
        //    storeMessages.DataSource = PayrollMessageManager.GetPayrollMessageByUserName(SessionManager.User.UserName, 0, 50).Data;
        //    storeMessages.DataBind();

        //    //X.Js.Call("filterGrid");
        //}

        //protected void ReloadMessageGrid(object s, StoreReadDataEventArgs e)
        //{
        //    BindMessages();
        //}


        public void LoadBlocks()
        {
            LoadOvertime();
            LoadTADA();
            LoadEveningCounter();
            LoadTravelRequests();
            LoadTimeRequest();

        }

        public void LoadOvertime()
        {
            int? pendingForRecommend = 0, recommendForApproval = 0, approvedForForward = 0;

            OvertimeManager.SetOvertimeCount(ref pendingForRecommend, ref recommendForApproval, ref approvedForForward);

            if (pendingForRecommend > 0 || recommendForApproval > 0)
            {
                blockOvertime.Visible = true;

                if (pendingForRecommend > 0)
                {
                    overtimeRecommend.InnerHtml = pendingForRecommend.ToString();
                }
                else
                    overtimeRecommendLi.Visible = false;

                if (recommendForApproval> 0)
                {
                    overtimeApprove.InnerHtml = recommendForApproval.ToString();
                }
                else
                    overtimeApproveLi.Visible = false;
            }

        }

        public void LoadTimeRequest()
        {
            int? pendingForRecommend = 0, recommendForApproval = 0;

            AttendanceManager.SetTimeRequestCount(ref pendingForRecommend, ref recommendForApproval);

            if (pendingForRecommend > 0 || recommendForApproval > 0)
            {
                blockTimeRequest.Visible = true;

                if (pendingForRecommend > 0)
                {
                    spanRequestTimeRequest.InnerHtml = pendingForRecommend.ToString();
                }
                else
                    liRequestTimeRequest.Visible = false;

                if (recommendForApproval > 0)
                {
                    spanRecommendedTimeRequest.InnerHtml = recommendForApproval.ToString();
                }
                else
                    liRecommendedTimeRequest.Visible = false;
            }

        }
        public void LoadTravelRequests()
        {
            int review = 0;

            OvertimeManager.SetTravelRequestsCountForSupervisor(ref review);

            if (review > 0)
            {
                blockTravelRequestReview.Visible = true;


                spanTravelRequest.InnerHtml = review.ToString();
               
            }

        }
        public void LoadEveningCounter()
        {
            int? pendingForRecommend = 0, recommendForApproval = 0, approvedForForward = 0;

            OvertimeManager.SetEveningCounterCount(ref pendingForRecommend, ref recommendForApproval, ref approvedForForward);

            if (pendingForRecommend > 0 || recommendForApproval > 0)
            {
                blockEveningCounter.Visible = true;

                if (pendingForRecommend > 0)
                {
                    eveningCounterRecommneded.InnerHtml = pendingForRecommend.ToString();
                }
                else
                    eveningCounterRecommnededLi.Visible = false;

                if (recommendForApproval > 0)
                {
                    eveningCounterApprove.InnerHtml = recommendForApproval.ToString();
                }
                else
                    eveningCounterApproveLi.Visible = false;
            }

        }
        public void LoadTADA()
        {
            int? pendingForRecommend = 0, recommendForApproval = 0, approvedForForward = 0;

            OvertimeManager.SetTADACount(ref pendingForRecommend, ref recommendForApproval, ref approvedForForward);

            if (pendingForRecommend > 0 || recommendForApproval > 0)
            {
                blockTADA.Visible = true;

                if (pendingForRecommend > 0)
                {
                    tadaRecommend.InnerHtml = pendingForRecommend.ToString();
                }
                else
                    tadaRecommendLi.Visible = false;

                if (recommendForApproval > 0)
                {
                    tadaApprove.InnerHtml = recommendForApproval.ToString();
                }
                else
                    tadaApproveLi.Visible = false;
            }

        }

        private void CheckPwdChangedBeforePwdChangingDays()
        {
            int days = 0;
            if (!IsPwdChangedBeforePwdChangingDays(SessionManager.UserName, ref days))
            {
                Setting objSetting = UserManager.GetSettingForPasswordChangeRule();
                if (objSetting.PasswordChangeType == (int)PasswordChangeTypeEnum.RecommendPwdChange)
                {
                    WPasswordNotification.Show();
                    return;
                }
            }
        }

        private void ShowPasswordChangedMessage()
        {
            if (Session["PwdChanged"] != null && Session["PwdChanged"].ToString() == "1")
            {
                Session["PwdChanged"] = "0";
                NewMessage.ShowNormalPopup("Your Password has been changed.");
            }
        }

        private void BindActivity()
        {
            if (CommonManager.CompanySetting.WhichCompany == WhichCompany.Rigo)
            {
                tblActivity.Visible = true;
                
                if (NewHRManager.IsActivitySaved(DateTime.Today))
                {
                    divTodayPresent.Visible = true;
                    divTodayAbsent.Visible = false;
                    trTodaySubmit.Visible = false;
                }
                else
                {
                    trTodaySubmit.Visible = true;
                    divTodayPresent.Visible = false;
                    divTodayAbsent.Visible = true;
                }

                ActivityComment obj = NewHRManager.GetLatestUnapprovedEmpActivityComment();
                if (obj != null)
                {
                    NewActivityDetail objNewActivityDetail = NewHRManager.GetNewActivityDetailByActivityId(obj.ActivityId.Value);
                    string day = "";
                    if (objNewActivityDetail != null)
                        day = objNewActivityDetail.DateEng.Value.ToString("MM/dd/yyyy");

                    hrComment.InnerHtml = "HR activity comment for date " + day + " : " + obj.Comment;
                    trHRComment.Visible = true;
                    hdnActivityId.Text = obj.ActivityId.Value.ToString();

                }
                else
                    trHRComment.Visible = false;

                List<DAL.AttendanceReportResult> atteList =
                BLL.BaseBiz.PayrollDataContext.AttendanceReport(SessionManager.CurrentLoggedInEmployeeId, 0, DateTime.Today.AddDays(-1), DateTime.Today.AddDays(-1), 0, 0, 99999).ToList();

                if (atteList.Count > 0)
                {
                    if (atteList[0].Type == (int)TimeAttendenceDayType.Holiday || atteList[0].Type == (int)TimeAttendenceDayType.WeeklyHoliday)
                    {
                        trYesterday.Visible = false;
                        trYesterdaySubmit.Visible = false;
                        return;
                    }
                }

                if (NewHRManager.IsActivitySaved(DateTime.Today.AddDays(-1)))
                {
                    divYesterdayPresent.Visible = true;
                    divYesterdayAbsent.Visible = false;
                    trYesterdaySubmit.Visible = false;
                }
                else
                {
                    divYesterdayAbsent.Visible = true;
                    divYesterdayPresent.Visible = false;
                    trYesterdaySubmit.Visible = true;
                }

                

            }
        }

        private void BindActivityComment()
        {
            if (CommonManager.CompanySetting.WhichCompany == WhichCompany.Rigo)
            {

            }
        }

        private void CheckIn()
        {
            Setting setting = OvertimeManager.GetSetting();
            if (setting != null && setting.ShowCheckInCheckOutPopup != null && setting.ShowCheckInCheckOutPopup.Value)
            {
                bool? isManualAtte = new AttendanceManager().IsManualAttendancePossible(SessionManager.CurrentLoggedInEmployeeId);
                
                if (isManualAtte != null && isManualAtte.Value)
                {
                    if (!AttendanceManager.IsCheckedInOrCheckedOut((int)ChkINOUTEnum.ChkIn))
                    {
                        int loggedInCount = AttendanceManager.GetLoggedInCountForUser();
                        ChangeUserActivity obj = AttendanceManager.GetLastChangeUserActivityForEmployee();
                        if (obj != null && loggedInCount == 1)
                        {
                            hdnLoggedInTime.Text = obj.DateEng.Value.ToString();
                            dfLoginDateTime.Text = string.Format("{0} | {1}", obj.DateEng.Value.ToString("h:mm tt"), obj.DateEng.Value.ToString("dddd, dd MMMM yyyy"));
                            dfMessage.Text = "This is your first login today. Do you want to Check In?";
                            btnCheckIn.Visible = true;
                            btnCheckOut.Visible = false;
                            btnCheckInCancel.Visible = true;
                            btnCheckOutCancel.Visible = false;
                            WCheckInOut.Visible = true;
                            WCheckInOut.Center();
                            WCheckInOut.Show();
                        }
                    }
                }
            }
            
        }

        protected void btnCheckIn_Click(object sender, DirectEventArgs e)
        {
            DAL.AttendanceMapping map = BLL.BaseBiz.PayrollDataContext.AttendanceMappings
                   .SingleOrDefault(x => x.EmployeeId == SessionManager.CurrentLoggedInEmployeeId);

            if (map == null)
            {
                Notification.Show(new NotificationConfig
                {
                    Title = "",
                    Icon = Icon.Information,
                    Html = "Mapping does not exists"
                });
                return;
            }

            AttendanceCheckInCheckOut chkInOut = new AttendanceCheckInCheckOut();
            chkInOut.ChkInOutID = Guid.NewGuid();
            chkInOut.DeviceID = map.DeviceId;
            chkInOut.AuthenticationType = (byte)TimeAttendanceAuthenticationType.SelfManualEntryFromSoftware; 

            if(sender == btnCheckIn)
                chkInOut.InOutMode = (int)ChkINOUTEnum.ChkIn;
            else
                chkInOut.InOutMode = (int)ChkINOUTEnum.ChkOut;

            chkInOut.IPAddress = HttpContext.Current.Request.UserHostAddress;
            chkInOut.ManualVisibleTime = DateTime.Parse(hdnLoggedInTime.Text).ToShortTimeString();

            DateTime chkinoutDate = DateTime.Parse(hdnLoggedInTime.Text);
            DateTime chkinoutTime = DateTime.Parse(hdnLoggedInTime.Text);

            chkInOut.DateTime = new DateTime(chkinoutDate.Year, chkinoutDate.Month, chkinoutDate.Day, chkinoutTime.Hour, chkinoutTime.Minute, chkinoutTime.Second);
            chkInOut.Date = chkInOut.DateTime.Date;

            chkInOut.ModificationRemarks = "Manual Attendance";

            AttendanceCheckInCheckOutComment comment = new AttendanceCheckInCheckOutComment();

            comment.AttendanceDate = chkinoutDate;

            if (sender == btnCheckIn)
                comment.InOutMode = (int)ChkINOUTEnum.ChkIn;
            else
                comment.InOutMode = (int)ChkINOUTEnum.ChkOut;

            if(!string.IsNullOrEmpty(txtNote.Text.Trim()))
                comment.Note = txtNote.Text.Trim();
            comment.EmpID = SessionManager.CurrentLoggedInEmployeeId;

            Status status = AttendanceManager.InsertCheckInAndComments(chkInOut, comment);
            if (status.IsSuccess)
            {
                if (sender == btnCheckOut)
                {
                    Session.Abandon();
                    System.Web.Security.FormsAuthentication.SignOut();
                    Response.Redirect("~/Default.aspx", true);
                }

                WCheckInOut.Close();
                Notification.Show(new NotificationConfig
                {
                    Title = "",
                    Icon = Icon.Information,
                    Html = "Your Time has been Recorded"
                });

                if (NewHRManager.RedirectToEmployeePlan())
                    Response.Redirect("~/Employee/EmpPlan.aspx");
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }
        }

        private void CheckOutPopup()
        {
            if (Session["logOut"] != null)
            {
                Session["logOut"] = null;

                DateTime now = DateTime.Now;

                hdnLoggedInTime.Text = now.ToString();
                dfLoginDateTime.Text = string.Format("{0} | {1}", now.ToString("h:mm tt"), now.ToString("dddd, dd MMMM yyyy"));
                dfMessage.Text = "All done. Do you want to Check Out?";
                btnCheckIn.Visible = false;
                btnCheckOut.Visible = true;
                btnCheckInCancel.Visible = false;
                btnCheckOutCancel.Visible = true;
                WCheckInOut.Title = "Check Out";
                WCheckInOut.Closable = false;
                WCheckInOut.Visible = true;
                WCheckInOut.Center();
                WCheckInOut.Show();                
            }
        }

        protected void btnCheckInCancel_Click(object sender, DirectEventArgs e)
        {
            if (sender == btnCheckInCancel)
            {
                WCheckInOut.Close();
                return;
            }

            Session.Abandon();
            System.Web.Security.FormsAuthentication.SignOut();
            Response.Redirect("~/Default.aspx", true);
        }

        private void TimeAttRejectedComment()
        {
            TimeRequest obj = AttendanceManager.GetRejectedTimeRequestForEmployee();
            if (obj != null && !string.IsNullOrEmpty(obj.RejectComment))
            {
                lblTimeAttRejectedComment.Text = string.Format("Your time attendance rejected : {0}.", obj.RejectComment);
                divTimeAttRejectComment.Visible = true;
            }
        }

        protected void btnShowActivityWindow_Click(object sender, DirectEventArgs e)
        {
            EmpActCtrl1.EditActivity(int.Parse(hdnActivityId.Text), true);

        }
    }
}
