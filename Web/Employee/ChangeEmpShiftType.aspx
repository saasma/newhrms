﻿<%@ Page Title="Change Shift Type" Language="C#" MasterPageFile="~/Master/EmployeeExt.Master" AutoEventWireup="true" CodeBehind="ChangeEmpShiftType.aspx.cs" Inherits="Web.Employee.ChangeEmpShiftType" %>

<%@ Register Src="~/NewHR/UserControls/ChangeEmpShiftCtrl.ascx" TagName="ChangeShiftTypeCtrl" TagPrefix="uc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">

  

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="mainContent" runat="server">

  <h3 style="margin-top:10px">
           Change shift schedule
        </h3>

    <uc1:ChangeShiftTypeCtrl Id="ChangeShiftCtrl" runat="server" />

</asp:Content>
