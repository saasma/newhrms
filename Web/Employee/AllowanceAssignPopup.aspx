﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/ForEmployeePopupPage.Master"
    AutoEventWireup="true" CodeBehind="AllowanceAssignPopup.aspx.cs" Inherits="Web.Employee.AllowanceAssignPopup" %>

<%@ Register Src="UserControls/AssignAllowanceCtrl.ascx" TagName="AllowanceAssCtrl"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" src="<%= ResolveUrl("~/Employee/override.js?v=") + Web.Helper.WebHelper.Version %>"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
    <uc1:AllowanceAssCtrl Id="AllowanceAssCtrl1" runat="server" />
</asp:Content>
