﻿<%@ Page Title="Beta Dashboard" Language="C#" MasterPageFile="~/Master/EmployeeMS.Master"
    AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Web.Employee.Beta.Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="mainContent" runat="server">
    <div class="contentpanel" style="padding-left: 10px">
        <div class="innerLR">
            <div class="col-sm-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <div class="panel-btns" style="display: none;">
                            <a href="#" class="panel-minimize tooltips" data-toggle="tooltip" title="Minimize Panel">
                                <i class="fa fa-minus"></i></a>
                        </div>
                        <h3 class="panel-title">
                            Training
                        </h3>
                    </div>
                    <div class="panel-body panel-body-small">
                        <ul role="menu" class="dropdown-menu dropdown-demo-only custommenulist">
                            <li><a id="A11" target="_blank" runat="server" href="~/Employee/TrainingRequests/EmpTrainingRequest.aspx">
                                <i class="fa fa-bars"></i>Training Request</a> </li>
                        </ul>
                        <div style="clear: both">
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <div class="panel-btns" style="display: none;">
                            <a href="#" class="panel-minimize tooltips" data-toggle="tooltip" title="Minimize Panel">
                                <i class="fa fa-minus"></i></a>
                        </div>
                        <h3 class="panel-title">
                            Employee Activity
                        </h3>
                    </div>
                    <div class="panel-body panel-body-small">
                        <ul role="menu" class="dropdown-menu dropdown-demo-only custommenulist">
                            <li><a id="A1" target="_blank" runat="server" href="~/Employee/EmployeeActivity.aspx"><i
                                class="fa fa-bars"></i>Employee Activity</a> </li>
                        </ul>
                        <div style="clear: both">
                        </div>
                    </div>
                </div>
            </div>


            <div class="col-sm-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <div class="panel-btns" style="display: none;">
                            <a href="#" class="panel-minimize tooltips" data-toggle="tooltip" title="Minimize Panel">
                                <i class="fa fa-minus"></i></a>
                        </div>
                        <h3 class="panel-title">
                           Employee  Attendance Request
                        </h3>
                    </div>
                    
                    <div class="panel-body panel-body-small">
                        <ul role="menu" class="dropdown-menu dropdown-demo-only custommenulist">
                            <li><a id="A3" target="_blank" runat="server" href="~/Employee/EmpAttTimeRequestList.aspx"><i
                                class="fa fa-bars"></i>My Attendance Requests</a> </li>
                        </ul>
                        <div style="clear: both">
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <div class="panel-btns" style="display: none;">
                            <a href="#" class="panel-minimize tooltips" data-toggle="tooltip" title="Minimize Panel">
                                <i class="fa fa-minus"></i></a>
                        </div>
                        <h3 class="panel-title">
                           Overtime
                        </h3>
                    </div>
                    
                    <div class="panel-body panel-body-small">
                        <ul role="menu" class="dropdown-menu dropdown-demo-only custommenulist">
                            <li><a id="A2" target="_blank" runat="server" href="~/Employee/OvertimeRequesterNewNew.aspx"><i
                                class="fa fa-bars"></i>Overtime Request</a> </li>
                        </ul>
                        <div style="clear: both">
                        </div>
                    </div>
               

                <div class="panel-body panel-body-small">
                        <ul role="menu" class="dropdown-menu dropdown-demo-only custommenulist">
                            <li><a id="A4" target="_blank" runat="server" href="~/Employee/AApproveOvertimeNewNew.aspx"><i
                                class="fa fa-bars"></i>Overtime Approval</a> </li>
                        </ul>
                        <div style="clear: both">
                        </div>
                    </div>
                </div>
            </div>


            <div class="col-sm-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <div class="panel-btns" style="display: none;">
                            <a href="#" class="panel-minimize tooltips" data-toggle="tooltip" title="Minimize Panel">
                                <i class="fa fa-minus"></i></a>
                        </div>
                        <h3 class="panel-title">
                           Employee Profile
                        </h3>
                    </div>
                    
                    <div class="panel-body panel-body-small">
                        <ul role="menu" class="dropdown-menu dropdown-demo-only custommenulist">
                            <li><a id="A5" target="_blank" runat="server" href="~/Employee/Hr/EmpDetailsNew.aspx"><i
                                class="fa fa-bars"></i>My Profile</a> </li>
                        </ul>
                        <div style="clear: both">
                        </div>
                    </div>
               

             
                </div>
            </div>

        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
