﻿<%@ Page Title="Holidays This Year" Language="C#" MasterPageFile="~/Master/EmployeeMS.Master"
    AutoEventWireup="true" CodeBehind="AllHolidayReport.aspx.cs" Inherits="Web.Employee.AllHolidayReport" %>

<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
      
      
        .details tr td
        {
            padding-top: 4px;
        }
        .leaveList
        {
            list-style-type: none;
            /*border: 2px solid #BFBFBF;
            background-color: #D0CECE;*/
            display: inline-block;
            padding: 10px;
        }
        .leaveList li
        {
            border: 1px solid #DEDEDE;
            background-color: #F2F2F2;
            float: left;
            padding: 5px;
            padding-left: 8px;
            padding-right: 8px;
            text-align: center;
        }
        
        .blockTitle
        {
            color: #0C6E92;
            text-transform: uppercase;
            font-size: 14px;
            font-weight: bold;
            padding-left: 2px;
        }
        .blockText
        {
            color: #305496;
        }
        .blockBottomBorder
        {
            border-bottom: 2px solid #D9D9D9;
            padding-bottom: 1px;
        }
        .weekBlock
        {
            background-color: #DEDEDE;
            display: inline-block;
            padding: 4px;
            margin-top: 10px;
            padding-left: 8px;

        
        .widget .widget-body.list ul li{line-height:inherit!important;}
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
    <ext:ResourceManager ID="ResourceManager1" runat="server" />
    <table>
        <tr>
            <td>
                <h2 style="margin-top: 25px;">
                    Company Holidays This Year
                </h2>
            </td>
        </tr>
        <tr>
            <td>
                <div style="margin-top: 15px;">
                    <div style="">
                        <ext:GridPanel ID="GridPanelMYTimesheet" runat="server" Width="680" Header="false"
                            HideHeaders="true">
                            <Store>
                                <ext:Store ID="storeMYTimesheet" runat="server">
                                    <Model>
                                        <ext:Model ID="model2" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="EngDateLongForDashboard" Type="String">
                                                </ext:ModelField>
                                                <ext:ModelField Name="Id" Type="String">
                                                </ext:ModelField>
                                                <ext:ModelField Name="Name" Type="String">
                                                </ext:ModelField>
                                                <ext:ModelField Name="NepDateLongForDashboard" Type="String">
                                                </ext:ModelField>
                                                <ext:ModelField Name="DaysLeftForDashboard" Type="String">
                                                </ext:ModelField>
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                            <ColumnModel>
                                <Columns>
                                    <ext:TemplateColumn ID="TemplateColumn1" runat="server" MenuDisabled="true" Header="Template"
                                        Width="500">
                                        <Template ID="Template1" runat="server" Width="480">
                                            <Html>
                                                <tpl for=".">
                                                               <%--<tpl if="amount &gt; 0">
                                                                      {numberCol}
                                                                </tpl>
                                                               <tpl if="amount &ls; 0 || amount  == 0">
                                                                      0
                                                                </tpl>--%>
                                                                <table style="width:480px; margin-top:10px; margin-top:10px;">
                                                                    <tr>
                                                                        <td>
                                                                            <div style="width:275px;">
                                                                                <strong>{EngDateLongForDashboard}</strong>
                                                                            </div>
                                                                        </td>
                                                                        <td>
                                                                            <div style="width:275px;">
                                                                                <strong>{Name}</strong>
                                                                            </div>
                                                                        </td>
                                                                    </tr>

                                                                    <tr>
                                                                        <td>
                                                                            <div style="width:250px;">
                                                                                {NepDateLongForDashboard}
                                                                            </div>
                                                                        </td>
                                                                        <td>
                                                                            <div style="width:250px;">
                                                                                {DaysLeftForDashboard}
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                </table>

                                                         </tpl>
                                            </Html>
                                        </Template>
                                    </ext:TemplateColumn>
                                    <%--<ext:Column ID="Column1" runat="server" DataIndex="EngDateLongForDashboard" Text="EngDateLongForDashboard" Width="200"
                                                MenuDisabled="true">
                                            </ext:Column>
                                            <ext:Column ID="Column2" runat="server" DataIndex="Name" Text="Name" Width="150"
                                                MenuDisabled="true">
                                            </ext:Column>
                                            <ext:Column ID="Column3" runat="server" DataIndex="NepDateLongForDashboard" Text="NepDateLongForDashboard"
                                                Width="200" MenuDisabled="true">
                                            </ext:Column>
                                            <ext:Column ID="Column4" runat="server" DataIndex="DaysLeftForDashboard" Text="DaysLeftForDashboard" Width="100"
                                                MenuDisabled="true">
                                            </ext:Column>--%>
                                </Columns>
                            </ColumnModel>
                        </ext:GridPanel>
                    </div>
                </div>
            </td>
        </tr>
    </table>
</asp:Content>
