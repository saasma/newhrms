﻿var CompanyX = {


    getCalendar: function () {
        return CompanyX.CalendarPanel1;
    },


    getStore: function () {
        return CompanyX.EventStore1;
    },

    getWindow: function () {
        return CompanyX.EventEditWindow1;
    },

    viewChange: function (p, vw, dateInfo) {

        var win = this.getWindow();
        //        if (win) {
        //            win.hide();
        //        }
        if (dateInfo !== null) {
            // will be null when switching to the event edit form, so ignore
            // if (datePickerChanged)
            this.DatePicker1.setValue(dateInfo.activeDate);
            //this.updateTitle(dateInfo.viewStart, dateInfo.viewEnd);
        }
        if (CompanyX.storeLeaveApproval != undefined) {
            //reloadGrids();
        }

    },

    updateTitle: function (startDt, endDt) {
        var msg = '';
    },

    setStartDate: function (picker, date) {

        this.getCalendar().setStartDate(date);
        //CompanyX.hdFromDateEng.value = date.toDateString();

        //reloadGrids();
    },

    rangeSelect: function (cal, dates, callback) {
        //        this.record.show(cal, dates);

        //        this.getWindow().on('hide', callback, cal, { single: true });
        chkHalfDay.setValue(false);
        cmbLeaveType2.clearValue();
        txtStartDate.setValue(dates.StartDate);
        txtEndDate.setValue(dates.EndDate);
        clearField();
        eventWindow.center();
        eventWindow.show();
        //this.record.show(cal, dates);
        //this.getWindow().on('hide', callback, cal, { single: true });
    },

    dayClick: function (cal, dt, allDay, el) {
        //        this.record.show.call(this, cal, {
        //            StartDate: dt,
        //            IsAllDay: allDay
        //        }, el);
        if (typeof (chkHalfDay) != 'undefined' && chkHalfDay != null)
            chkHalfDay.setValue(false);
        cmbLeaveType2.clearValue();
        txtStartDate.setValue(dt);
        txtEndDate.setValue(dt);
        if (typeof (clearField) != 'undefined')
            clearField();
        eventWindow.center()
        eventWindow.show();

    },


    dayClickLR: function (cal, dt, allDay, el) {

        if (typeof (chkHalfDay) != 'undefined' && chkHalfDay != null)
            chkHalfDay.setValue(false);

        if (typeof (clearField) != 'undefined')
            clearField();

        leaveReq('Mode=1&StartDate=' + dt + '&EndDate=' + dt);

    },

    rangeSelectLR: function (cal, dates, callback) {

        chkHalfDay.setValue(false);
        cmbLeaveType2.clearValue();
        txtStartDate.setValue(dates.StartDate);
        txtEndDate.setValue(dates.EndDate);
        leaveReq('Mode=2&StartDate=' + dates.StartDate + '&EndDate=' + dates.EndDate);
    },

    approvalDayClick: function (cal, dt, allDay, el) {

        var date = CompanyX.DatePicker1.value;

        var currentMonth = date.getMonth();
        var currentYear = date.getFullYear();


        if (dt.getMonth() != currentMonth || dt.getFullYear() != currentYear)
            return;
        CompanyX.DatePicker1.setValue(dt);

        //Set hdFromDate and hdStatus For passing values while calling  web service method. -> GetLeaveDetails
        hdFromDateEng.value = dt.toDateString();
        // Status Indicates Leave as Approved.
        hdStatus.value = 2;


    },

    RangeSelection: function (cal, dates, callback) {

        //        hdStartDate.setValue(dates.StartDate.toDateString());
        //        hdEndDate.setValue(dates.EndDate.toDateString());
    },

    record: {
        add: function (win, rec) {
            var chkIsHalfDay = CompanyX.chkIsHalfDay;
            var cboHourlyLeave = CompanyX.cboHourlyLeave;
            var chkIsHalfDay = CompanyX.chkIsHalfDay;
            var hdStartDateEng = CompanyX.hdStartDateEng;
            var hdEndDateEng = CompanyX.hdEndDateEng;
            var hdCompanyHasHourlyLeave = CompanyX.hdCompanyHasHourlyLeave;
            var chkIsHourlyLeave = getChkAllDay(); // CompanyX.chkIsHourlyLeave;


            var reason = EventEditWindow1.titleField.getValue();
            //Trim Left and right spaces.
            reason = reason.toString().trim(); //reason.replace(/^\s+|\s+$/g, "");
            if (reason == "") {
                showValidateMsg("Reason can not be empty.");
                return;
            }
            // If Leave is full day and not exceeds NewBAlance
            if (chkIsHalfDay != undefined) {
                if (!chkIsHalfDay.checked) {
                    if (rec.data.StartDate != null && rec.data.EndDate != null &&
                         !this.record.isDateInterceptValid(rec.data.StartDate.toDateString(), rec.data.EndDate.toDateString()))
                        return;
                    if (!this.record.CheckDate(rec, hdStartDateEng.value, true))
                        return;
                    if (!this.record.CheckDate(rec, hdEndDateEng.value, false))
                        return;
                    if (parseInt(rec.data.CalendarId) != -1) {
                        if (!this.record.CheckDays(rec)) {
                            rec.commit();
                            return;
                        }
                    }
                }
                else {
                    if (!this.record.CheckDate(rec, hdStartDateEng.value, true))
                        return;
                }
            }


            //set is half day checkbox
            if (chkIsHalfDay != undefined) {
                rec.data.Location = (chkIsHalfDay.checked) ? "True" : "False";
            }
            else {
                rec.data.Location = "False";
            }

            // rec.data.Reminder is currently used to check if LeaveType is Hourly Leave
            // rec.data.Reminder is set
            //      :true  -> if Leave Type is hourly Leave
            //      :false -> if leave type is not houly Leave
            if (chkIsHourlyLeave != undefined) {
                rec.data.Reminder = chkIsHourlyLeave.checked == true ? "false" : "true";
            }
            else
                rec.data.Reminder = "false";

            // rec.data.Url is currently used to hold the number of Leave Hours.
            // rec.data.Url is set to Leave HOurs iff rec.data.Reminder is true i.e If Leave Type can be used as Hourly Leave
            if (rec.data.Reminder.toString() == "true" && typeof (cboHourlyLeave) != 'undefined' && cboHourlyLeave != null)
                rec.data.Url = cboHourlyLeave.getValue();
            else
                rec.data.Url = "0";

            //Url {0} = hour for hourly leave, {1} = EditSequence, {2} = Apply to for supervisor selection
            //            rec.data.Url += ";0;" + CompanyX.cmbApplyTo.getValue();
            //rec.data.IsNew = false;

            Ext.net.Mask.show();
            Ext.net.DirectMethod.request({
                url: "../LeaveRequestService.asmx/SaveEvent",
                cleanRequest: true,
                json: true,
                params: {
                    entity: rec.data,
                    halfDayType: CompanyX.chkHalfDayType.getValue(),
                    selectedSupervisor: CompanyX.cmbApplyTo.getValue(),
                    startTime: EventEditWindow1.dateRangeField.items.items[1].value, //CompanyX.startTime.getValue(),
                    endTime: EventEditWindow1.dateRangeField.items.items[3].value//CompanyX.endTime.getValue()
                },
                success: function (result) {
                    CompanyX.record.addCallback(result, win, rec, "add");
                }
            });

        },

        // Callback for delete leave
        removeCallback: function (result, win, rec) {
            if (result.IsSuccess.toLowerCase() == 'true') {
                CompanyX.getStore().remove(rec);
                rec.commit();
                CompanyX.getStore().commitChanges();
                //CompanyX.getStore().reload(); //don't refresh, remove will remove from the calendar
                CompanyX.storePendingRequest.reload();
                Ext.net.Mask.hide();
                win.hide();
            }
            else if (result.IsSuccess.toLowerCase() == 'false') {
                Ext.net.Mask.hide();
                showValidateMsg(result.ErrorMessage);
            }
        },

        addCallback: function (result, win, rec, actionType) {

            if (result.IsSuccess.toLowerCase() == 'true') {

                var IsHour = rec.data.Reminder;
                var DaysOrHour = rec.data.Url.toString().split(';')[0] == "" ? "0" : rec.data.Url.toString().split(';')[0];
                var Reason = rec.data.Title;
                var fromDate = rec.data.StartDate.toDateString();
                var toDate = rec.data.EndDate.toDateString();
                var status = "request";

                //( rec.data.Reminder, DaysOrHour, Comment, fromDate, toDate, string status)
                // Only Notify to User if
                //      i.  New Leave is Requested
                //      ii. Leave is Updated.
                //                if (actionType == "add")// || actionType == "update") 
                //                {
                //                    Ext.net.DirectMethods.NotifyThroughMessageAndEmail(rec.data.EventId, IsHour, DaysOrHour, Reason, fromDate, toDate, status);
                //                }
                rec.commit();
                CompanyX.getStore().commitChanges();
                CompanyX.getStore().reload();
                CompanyX.storePendingRequest.reload();
                CompanyX.storeApprovedRequest.reload();
                Ext.net.Mask.hide();
                win.hide();

            }
            else if (result.IsSuccess.toLowerCase() == 'false') {
                Ext.net.Mask.hide();
                showValidateMsg(result.ErrorMessage);
                //Ext.Msg.alert("", result.ErrorMessage);
                //Ext.net.Mask.hide();
                //CompanyX.storePendingRequest.reload();
            }



        },

        CheckDate: function (rec, date1, isStartDate) {
            return true;
            var t2 = null;
            var t1 = null;
            if (isStartDate) {
                t2 = new Date(rec.data.StartDate).getTime();
                t1 = new Date(date1).getTime();
                if (parseInt(t2) >= parseInt(t1))
                    return true;
                else {
                    showValidateMsg("Invalid date selection", String.format("Leave starting date should be greater than current period start date ({0})."
                        , date1));
                    return false;
                }
            }
            else {
                t2 = new Date(rec.data.EndDate).getTime();
                t1 = new Date(date1).getTime();
                if (parseInt(t2) <= parseInt(t1))
                    return true;
                else {
                    showValidateMsg("Invalid date selection", String.format("Leave end date should not lie beyond the date ({0}).", date1));
                    return false;
                }
            }
        },

        isDateInterceptValid: function (startDate, endDate) {
            var t2 = new Date(endDate).getTime();
            var t1 = new Date(startDate).getTime();
            if (parseInt(t1) > parseInt(t2)) {
                Ext.Msg.alert("Invalid Date", "Invalid date selection." + '<br>' + "Leave starting date exceeds leave ending date.");
                return false;
            }
            else {
                return true;
            }
        },

        update: function (win, rec) {
            var chkIsHourlyLeave = getChkAllDay(); // CompanyX.chkIsHourlyLeave;
            var cboHourlyLeave = CompanyX.cboHourlyLeave;
            var chkIsHalfDay = CompanyX.chkIsHalfDay;
            var hdCompanyHasHourlyLeave = CompanyX.hdCompanyHasHourlyLeave;
            var hdStartDateEng = CompanyX.hdStartDateEng;
            var hdEndDateEng = CompanyX.hdEndDateEng;
            var hdEditSequence = CompanyX.hdEditSequence;


            //validate reason

            var reason = EventEditWindow1.titleField.getValue();
            //Trim Left and right spaces.
            reason = reason.toString().trim(); //.replace(/^\s+|\s+$/g, "");
            if (reason == "") {
                Ext.Msg.alert("Leave Request Failed", "Reason can not be empty.");
                return;
            }

            // If Leave is full day and not exceeds NewBAlance
            if (chkIsHalfDay != undefined) {
                if (!chkIsHalfDay.checked) {
                    if (!this.record.CheckDate(rec, hdStartDateEng.value, true))
                        return;
                    if (!this.record.CheckDate(rec, hdEndDateEng.value, false))
                        return;
                    if (parseInt(rec.data.CalendarId) != -1) {
                        if (!this.record.CheckDays(rec)) {
                            rec.commit();
                            return;
                        }
                    }
                }
                else {
                    if (!this.record.CheckDate(rec, hdStartDateEng.value, true))
                        return;
                }
            }


            // rec.data.Reminder is currently used to check if LeaveType is Hourly Leave
            // rec.data.Reminder is set
            //      :true  -> if Leave Type is hourly Leave
            //      :false -> if leave type is not houly Leave

            if (chkIsHourlyLeave != undefined) {
                rec.data.Reminder = chkIsHourlyLeave.checked == true ? "false" : "true";
            }
            else
                rec.data.Reminder = "false";

            //set is half day checkbox
            if (chkIsHalfDay != undefined) {
                rec.data.Location = (chkIsHalfDay.checked) ? "True" : "False";
            }
            else {
                rec.data.Location = "False";
            }

            // rec.data.Url is currently used to hold the number of Leave Hours.
            // rec.data.Url is set to Leave HOurs iff rec.data.Reminder is true i.e If Leave Type can be used as Hourly Leave
            if (rec.data.Reminder.toString() == "true" && typeof (cboHourlyLeave) != 'undefined' && cboHourlyLeave != null)
                rec.data.Url = cboHourlyLeave.getValue();
            else
                rec.data.Url = "0";

            //rec.commit();
            //ASCII CODE For ô is Alt+147
            rec.data.Url += (";" + (hdEditSequence.getValue() == "" ? "0" : hdEditSequence.getValue()));
            //Url {0} = hour for hourly leave, {1} = EditSequence, {2} = Apply to for supervisor selection
            //rec.data.Url += ";" + CompanyX.cmbApplyTo.getValue();


            Ext.net.Mask.show();
            Ext.net.DirectMethod.request({
                url: "../LeaveRequestService.asmx/UpdateEvent",
                cleanRequest: true,
                json: true,
                params: {
                    entity: rec.data,
                    halfDayType: CompanyX.chkHalfDayType.getValue(),
                    selectedSupervisor: CompanyX.cmbApplyTo.getValue(),
                    startTime: EventEditWindow1.dateRangeField.items.items[1].value, //CompanyX.startTime.getValue(),
                    endTime: EventEditWindow1.dateRangeField.items.items[3].value//CompanyX.endTime.getValue()
                },
                success: function (result) {

                    CompanyX.record.addCallback(result, win, rec, "update");
                }
            });
            //chkIsHourlyLeave.setValue(false);
            //rec.commit();

        },

        showMsg: function (msg) {
            Ext.net.Notification.show({ title: "Message", html: msg });
        },



        remove: function (win, rec) {

            if (confirm("Cancel the leave request?") == false)
                return;



            Ext.net.Mask.show();
            Ext.net.DirectMethod.request({
                url: "../LeaveRequestService.asmx/DeleteLeaveRequest",
                cleanRequest: true,
                json: true,
                params: {
                    leaveRequestId: rec.data.EventId
                },
                success: function (result) {
                    CompanyX.record.addCallback(result, win, rec, "remove");
                }


            });
            //            if (typeof (chkIsHourlyLeave) != 'undefined')
            //                chkIsHourlyLeave.setValue(false);

        },

        edit: function (win, rec) {
            win.hide();
            CompanyX.getCalendar().showEditForm(rec);
        },

        resize: function (cal, rec) {
            rec.commit();
            showMsg('Event ' + rec.data.Title + ' was updated');
        },



        show: function (cal, rec, el) {

            if (isRequestPage() == false) {
                if (rec.data != undefined) {

                    MoreHandler(rec.data.EventId);

                }
                return;
            }
            else {
                hdnLeaveRequestId.setValue(rec.data.EventId);
                btnShow.fireEvent('click');
                return;
            }


            var chkIsHourlyLeave = getChkAllDay(); // CompanyX.chkIsHourlyLeave;
            var cboHourlyLeave = CompanyX.cboHourlyLeave;
            var chkIsHalfDay = CompanyX.chkIsHalfDay;
            var hdStartDateEng = CompanyX.hdStartDateEng;
            var hdEndDateEng = CompanyX.hdEndDateEng;
            var hdEditSequence = CompanyX.hdEditSequence;
            var hdCompanyHasHourlyLeave = CompanyX.hdCompanyHasHourlyLeave;
            var ids;





            if (rec.data != undefined) {
                ids = rec.data.Url.toString().split(';');
                hdEditSequence.setValue(ids[1]);
                CompanyX.chkHalfDayType.setValue(ids[4]);
                CompanyX.cmbApplyTo.setValue(ids[5]);
            }

            CompanyX.getWindow().show(rec, el);
            if (isRequestPage()) {
                //showHideHalfDayCheckbox(CompanyX.getWindow().get(0).form.items.items[2].getSelectedIndex());
            }

            //            if (hdStartDateEng.getValue() != '' && hdEndDateEng.getValue() != '') {
            //                //finds a Start Date of event update window
            //                CompanyX.EventEditWindow1.items.itemAt(0).items.items[1].items.items[0].setMinValue(hdStartDateEng.getValue());
            //                CompanyX.EventEditWindow1.items.itemAt(0).items.items[1].items.items[0].setMaxValue(hdEndDateEng.getValue());
            //                //finds a End Date of event update window
            //                CompanyX.EventEditWindow1.items.itemAt(0).items.items[1].items.items[1].setMinValue(hdStartDateEng.getValue());
            //                CompanyX.EventEditWindow1.items.itemAt(0).items.items[1].items.items[1].setMaxValue(hdEndDateEng.getValue());
            //            }

            chkIsHourlyLeave = getChkAllDay();
            if (chkIsHourlyLeave != undefined) {
                if (hdCompanyHasHourlyLeave.value.toString() == "True") {

                    if (typeof (ids) != 'undefined') {
                        // start time
                        //                        CompanyX.EventEditWindow1.items.itemAt(0).items.items[1].items.items[3].setValue(ids[6]);
                        //                        //end tim7
                        //                        CompanyX.EventEditWindow1.items.itemAt(0).items.items[1].items.items[4].setValue(ids[7]);
                        //CompanyX.startTime.setValue(ids[6]);
                        //CompanyX.endTime.setValue(ids[7]);
                    }

                    // set Leave Type to company default;
                    //chkIsHourlyLeave.setValue(false);
                    if (hdCompanyHasHourlyLeave.value.toLowerCase() == "true") {
                        //document.getElementById('divHourly').style.display = "none";
                        cboHourlyLeave.show();
                        chkIsHourlyLeave.show();
                        chkIsHourlyLeave.setValue(false);
                        //CompanyX.EventEditWindow1.items.itemAt(0).items.items[1].items.items[1].show();
                        Ext.get('date-range-to-label').hide();

                    }
                    else {
                        cboHourlyLeave.hide();
                        //document.getElementById('divHourly').style.display = "none";
                        chkIsHourlyLeave.hide();
                        //CompanyX.EventEditWindow1.items.itemAt(0).items.items[1].items.items[1].show();
                        chkIsHourlyLeave.setValue(false);
                        // Ext.get('date-range-to-label').show();
                    }

                    // Set Leave Type either hourly or non hourly
                    if (rec.data != undefined) {
                        if (rec.data.Reminder == "True") {
                            getChkAllDay().setValue(false);
                            //chkIsHourlyLeave.setValue(true);
                            cboHourlyLeave.show();
                            //document.getElementById('divHourly').style.display = "";
                            cboHourlyLeave.setValue(rec.data.Url.toString().split(';')[0]);
                        }
                        else if (rec.data.Reminder == "False") {
                            //chkIsHourlyLeave.setValue(false);
                            getChkAllDay().setValue(true);
                            cboHourlyLeave.hide();
                            //document.getElementById('divHourly').style.display = "none";
                        }
                    }
                    else {
                        //cboHourlyLeave.setLoadedIndex(0);
                    }


                }
                else {
                    if (typeof (cboHourlyLeave) != 'undefined' && cboHourlyLeave != null)
                        cboHourlyLeave.hide();
                }
            }




            if (chkIsHalfDay != undefined) {
                chkIsHalfDay.setValue(CompanyX.hdIsHalfDayLeave.value);

                if (rec.data != undefined) {
                    chkIsHalfDay.setValue(rec.data.Location == "True" ? true : false);
                }
                else
                    chkIsHalfDay.setValue(false);
            }
            //CompanyX.EventEditWindow1.items.items[0].items.items[2].label.setValue("600px");
            rec.dirty = true; //set dirty for saving in all cases
            showGridPagingHiddenBar();
        },

        saveAll: function () {
            CompanyX.getStore().submitData();
        },

        CheckDays: function (rec) {

            //Check Balance for Leave or Not
            if (LeaveRequestNegativeLeaveReqBalanceNotAllowed == false)
                return true;

            //return true;
            if (CompanyX.storeLeaves.getById(rec.data.CalendarId) != undefined) {
                var NewBalance = CompanyX.storeLeaves.getById(rec.data.CalendarId).data.NewBalance;
                var t2 = rec.data.EndDate.getTime();
                var t1 = rec.data.StartDate.getTime();
                var totalDays = parseInt((t2 - t1) / (24 * 3600 * 1000));
                if (totalDays > parseInt(NewBalance)) {
                    Ext.Msg.alert("Leave Days Exceed", "Can not request leave for more than " + NewBalance);
                    return false;
                }
                else
                    return true;
            }
            else
                return false;
        }
    },

    dayClickTimeAtt: function (cal, dt, el) {

        
        txtStartDate.setValue(dt);
        txtEndDate.setValue(dt);
        hdnRequestID.setValue('');
        btnShow.fireEvent('click');
    },
    rangeSelectTimeAtt: function (cal, dates, callback) {

        txtStartDate.setValue(dates.StartDate);
        txtEndDate.setValue(dates.EndDate);
        hdnRequestID.setValue('');
        btnShow.fireEvent('click');
    }
};

// OVERRIDE MONTH VIEW EVENT
// REASON:
//          By default calendar month view displays Reason as title
//          After overriden month view displays the Leave Type Name as Title
//          note: Holds the Leave Type Name in this context.
// PARAMETER CHANGED:
//          Title:
//              ORIGINAL VALUE: (evt[M.IsAllDay.name] ? '': evt[M.StartDate.name].format('g:ia ')) + (!title || title.length == 0 ? '(No title)': title)
//                   NEW VALUE: note      


// Some how paging bar is being hidden so show it using class
function showGridPagingHiddenBar() {
    var classLinks = Ext.query('.x-toolbar-left-row');
    Ext.each(classLinks, function (item, index) {
        item.style.visibility = '';
    });


}
function loadRequest()
{
CompanyX.getStore().reload();
                CompanyX.storePendingRequest.reload();
                CompanyX.storeApprovedRequest.reload();
}


Ext.override
(
    Ext.calendar.view.Month, {
        getTemplateEventData: function(evt) {
            var M = Ext.calendar.data.EventMappings,
            selector = this.getEventSelectorCls(evt[M.EventId.name]),
            title = evt[M.Title.name];
            return Ext.applyIf({
                _selectorCls: selector,
                _colorCls: 'ext-color-' + (evt[M.CalendarId.name] ?
                evt[M.CalendarId.name] : 'default') + (evt._renderAsAllDay ? '-ad': ''),
                _elId: selector + '-' + evt._weekIndex,
                _isRecurring: evt.Recurrence && evt.Recurrence != '',
                _isReminder: evt[M.Reminder.name] && evt[M.Reminder.name] != '',
                Title: evt.Notes
            },
            evt);
        }
    }
);



var reloadGrids = function () {
    CompanyX.EventStore1.reload();
    //CompanyX.storeLeaveApproval.reload();
    //CompanyX.storePendingRequest.reload();
};

var reloadGridsAfterApproval = function () {
    CompanyX.EventStore1.reload();
    CompanyX.storeLeaveApproval.reload();
    CompanyX.storePendingRequest.reload();
};

var valGroup;
var valControl;
// validate all controls of the current validation group &
//show message
function CheckValidation() {

    if (typeof (Page_Validators) == 'undefined')
        return true;

    for (var ii = 0; ii < Page_Validators.length; ii++) {
        var val = Page_Validators[ii];
        valControl = null;
        if (val.validationGroup == valGroup) {

            ValidatorEnable(val);
            if (val.isvalid == false) {

                valControl = val.controltovalidate;
                var isExtMsg = false;

                // if ext enabled in the page then show Warning in Ext Control
                if (typeof (Ext) != 'undefined') {
                    isExtMsg = true;
                    Ext.Msg.show({
                        title: 'Validation failure!', //<- the dialog's title  
                        msg: val.errormessage, //<- the message  
                        buttons: Ext.Msg.OK, //<- YES and NO buttons  
                        icon: Ext.Msg.WARNING, // <- error icon  
                        minWidth: 300,
                        fn: callback //<- the function that is executed when the user clicks on any button  
                    });
                }
                else
                    alert(val.errormessage);

                if (isExtMsg == false) {
                    //for ext control
                    if (document.getElementById(val.controltovalidate).tagName == "TABLE") {
                        eval(valControl).focus();
                    }
                    else
                        document.getElementById(val.controltovalidate).focus();
                }

                return false;
            }
        }
    }

}


function callback(parameters) {
    if (typeof (valControl) == 'undefined')
        return;

    Ext.defer(function () {
        //Ext.get(valControl).focus();
       
            var element = eval(valControl);
            if (element != undefined)
                element.focus();
        
    }, 100);
}

var loadHoliday = function (month, year) {
    Ext.net.Mask.show();
    Ext.net.DirectMethod.request({
        url: "../LeaveRequestService.asmx/GetHolidays",
        cleanRequest: true,
        json: true,
        params: {
            month: month,
            year: year
        },
        success: function (result) {
            holidayList = result;
            //Ext.Msg.alert("Json Message", result);
            CompanyX.DatePicker1.highlightDates(result);
            Ext.net.Mask.hide();

            //find the range case
            for (i = 0; i < result.length; i++) {
                if (result[i].Id == 0) {
                    CompanyX.lblCalendarTitle.setText(result[i].Abbreviation);
                    break;  
                }
            }

        },
        failure: function (result) { Ext.net.Mask.hide(); }
    });
};






///// Override DatePicker & is triggered when month or date selection changes
Ext.DatePicker.prototype.update = function (date, forceRefresh) {
    
    var me = this,
            active = me.activeDate;
    if (me.rendered) {
        me.activeDate = date;
        if (!forceRefresh && active && me.el && active.getMonth() == date.getMonth() && active.getFullYear() == date.getFullYear()) {
            me.selectedUpdate(date, active);
        } else {
            me.fullUpdate(date, active);
        }
    }

   
    if (this.id == "DatePicker1") {

        if (currentMonth == null) {
            currentMonth = date.getMonth();
            currentYear = date.getFullYear();
            loadHoliday(date.getMonth(), date.getFullYear());

        }
         else if (currentMonth != date.getMonth() || currentYear != date.getFullYear()) 
        {
            currentMonth = date.getMonth();
            currentYear = date.getFullYear();
            loadHoliday(date.getMonth(), date.getFullYear());
            if (CompanyX.getCalendar() != undefined)
                CompanyX.getCalendar().setStartDate(date);
            reloadGrids();
        }
    }

    return me;

};

function isRequestPage() {
    if (typeof (isLeaveRequestPage) != 'undefined' && isLeaveRequestPage == true)
        return true;
    return false;
    //        var url = window.location.href;
    //        var page = url.substring(url.lastIndexOf('/') + 1);
    //    
    //        return page.toLowerCase() == "leaverequest.aspx";
}

function showValidateMsg(msg, callback) {
    Ext.Msg.show({
        title: 'Error', //<- the dialog's title  
        msg: msg, //<- the message  
        buttons: Ext.Msg.OK, //<- YES and NO buttons  
        icon: Ext.Msg.WARNING, // <- error icon  
        minWidth: 300,
        fn: callback //<- the function that is executed when the user clicks on any button  
    });
}

function showValidateMsg(msg, callback, title) {
    if (typeof (title) == 'undefined')
        title = "Validation Failure";
    Ext.Msg.show({
        title: title, //<- the dialog's title  
        msg: msg, //<- the message  
        buttons: Ext.Msg.OK, //<- YES and NO buttons  
        icon: Ext.Msg.WARNING, // <- error icon  
        minWidth: 300,
        fn: callback //<- the function that is executed when the user clicks on any button  
    });
}