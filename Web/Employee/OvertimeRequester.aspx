<%@ Page Title="Overtime Request" Language="C#" MasterPageFile="~/Master/EmployeeMS.Master"
    AutoEventWireup="true" CodeBehind="OvertimeRequester.aspx.cs" Inherits="Web.Employee.OvertimeRequester" %>

<%@ Import Namespace="BLL.Manager" %>
<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/PagingCtl.ascx" TagName="PagingCtl" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        var loadingButtonWhenSubmitting = false;
        var skipLoadingCheck = true;
        function addSkillSetToEmployeePopup() {
            positionHistoryPopup("isPopup=true");
            return false;
        }

        function addSkillSetToEmployeePopup1(requestid) {
            var theText = requestid.getAttribute("value");
            positionHistoryPopup("isPopup=true&reqid=" + theText);
        }


        function refreshEventList(popupWindow) {
            if (typeof (popupWindow) != 'undefined')
                popupWindow.close();

            __doPostBack('<%=btnLoad.UniqueID %>', '');

        }
        
       
    </script>
    <style type="text/css">
        .tableLightColor a:hover
        {
            color: #048FC2;
            text-decoration: none;
        }
        .tableLightColor th
        {
            font-weight:normal !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
    <div class="contentArea" style="margin-top:10px">
        <div class="attribute" style="padding:10px">
            <%-- <asp:LinkButton OnClientClick="addSkillSetToEmployeePopup()" id="btnOvertimRequest" Text="New Overtime Request"  runat="server"  />
            --%>
            <table>
                <tr>
                    <td></td>
                    <td style="padding-left:15px;">Period</td>
                    <td style="padding-left:15px;">Status</td>
                    <td style="padding-left:15px;">Overtime Type</td>
                    <td style="padding-left:15px;"></td>
                </tr>
                <tr>
                    <td>
                        <asp:LinkButton ID="btnOvertimRequest" OnClientClick="addSkillSetToEmployeePopup(); return false;"
                            CssClass="save" Style="margin-right: 10px;" Height="30px" Width="100px" runat="server"
                            Text="New Request" />
                    </td>
                    <td style="padding-left:15px;">
                        <asp:DropDownList ID="ddlType" runat="server">
                            <asp:ListItem Value="-1"  Selected="True" Text="All" />
                            <asp:ListItem Value="1">This Week</asp:ListItem>
                            <asp:ListItem Value="2">This Month</asp:ListItem>
                            <asp:ListItem Value="3">Last Month</asp:ListItem>
                            <asp:ListItem Value="4">This Year</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td style="padding-left:15px;">
                        <asp:DropDownList ID="ddlStatus" runat="server">
                            <asp:ListItem Value="-1">All</asp:ListItem>
                            <asp:ListItem Value="0" Selected="True">Pending</asp:ListItem>
                            <asp:ListItem Value="1">Recommended</asp:ListItem>
                            <asp:ListItem Value="2">Approved</asp:ListItem>
                            <asp:ListItem Value="3">Rejected</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td style="padding-left:15px;">
                        <asp:DropDownList DataTextField="Name" DataValueField="OvertimeTypeId" ID="ddlOvertimeType" AppendDataBoundItems="true"
                            runat="server" Style="width: 180px; margin-right: 25px;">
                            <asp:ListItem Text="All" Value="-1" Selected="True"/>
                        </asp:DropDownList>
                    </td>
                    <td valign="top" style="padding-bottom: 8px;padding-left:15px;">
                        <asp:Button ID="btnLoad" CssClass="load" OnClientClick="valGroup='AEEmployee';return CheckValidation();"
                            OnClick="btnLoad_Click" runat="server" Text="Load" />
                    </td>
                </tr>
            </table>
            <%--</ContentTemplate>
            </asp:UpdatePanel>--%>
        </div>
        <div class="clear">
            <cc2:EmptyDisplayGridView CssClass="tableLightColor" UseAccessibleHeader="true" ShowHeaderWhenEmpty="True"
                ID="gvw" runat="server" DataKeyNames="RequestID" AutoGenerateColumns="False" CellPadding="4"
                GridLines="None" ShowFooterWhenEmpty="False" OnRowCreated="gvwEmployees_RowCreated"  PagerStyle-CssClass="defaultPagingBar"
                PagerStyle-HorizontalAlign="Center" OnPageIndexChanged="gvwRoles_PageIndexChanged" OnPageIndexChanging="gvwRoles_PageIndexChanging" 
                OnRowDeleting="gvw_RowDeleting">
                <RowStyle BackColor="#E3EAEB" />
                <Columns>
                    <asp:BoundField HeaderStyle-Width="20px" DataField="RequestID" HeaderText="RequestID"
                        Visible="false"></asp:BoundField>
                    <asp:BoundField HeaderStyle-Width="20px" DataField="EmployeeID" HeaderText="EmployeeID"
                        Visible="false"></asp:BoundField>
                    <asp:BoundField DataField="OvertimeType" HeaderStyle-Width="180px" HeaderStyle-HorizontalAlign="Left"
                        HeaderText="Overtime"></asp:BoundField>
                    <asp:BoundField DataField="Date" DataFormatString="{0:yyyy-MMM-dd}" HeaderStyle-Width="110px" ItemStyle-HorizontalAlign="Left"
                        HeaderStyle-HorizontalAlign="Left" HeaderText="Date"></asp:BoundField>
                    
                    <asp:BoundField DataField="StartTime" HeaderStyle-Width="80px" HeaderStyle-HorizontalAlign="Left"
                        HeaderText="Start"></asp:BoundField>
                    <asp:BoundField DataField="EndTime" HeaderStyle-Width="80px" HeaderStyle-HorizontalAlign="Left"
                        HeaderText="End"></asp:BoundField>
                    <asp:BoundField DataField="DurationModified" HeaderStyle-Width="100px" HeaderStyle-HorizontalAlign="Center"
                        HeaderText="Requested Hour"></asp:BoundField>
                     <asp:BoundField DataField="ApprovedTime" HeaderStyle-Width="100px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                        HeaderText="Approved Hour"></asp:BoundField>      
                    <asp:BoundField DataField="CheckInTime" HeaderStyle-Width="100px" HeaderStyle-HorizontalAlign="Left"
                        HeaderText="Device In"></asp:BoundField>
                    <asp:BoundField DataField="CheckOutTime" HeaderStyle-Width="100px" HeaderStyle-HorizontalAlign="Left"
                        HeaderText="Device Out"></asp:BoundField>
                    <asp:BoundField DataField="Reason" HeaderStyle-Width="300px" HeaderStyle-HorizontalAlign="Left"
                        HeaderText="Reason"></asp:BoundField>
                                 
                    <%--  <asp:BoundField DataField="SupervisorName" HeaderStyle-Width="140px" HeaderStyle-HorizontalAlign="Left"
                        HeaderText="Approval"></asp:BoundField>--%>
                    <asp:BoundField DataField="StatusModified" HeaderStyle-Width="120px" HeaderStyle-HorizontalAlign="Left"
                        HeaderText="Status"></asp:BoundField>
                    <asp:TemplateField HeaderStyle-Width="70px" HeaderStyle-HorizontalAlign="Left" HeaderText="">
                        <ItemTemplate>
                            <asp:HyperLink NavigateUrl="javascript:void(0)" value='<%# Eval("RequestID") %>'
                                onclick='<%# "addSkillSetToEmployeePopup1(this);" %>' runat="server" Text="Edit"
                                ID="btnAction">
                            </asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-Width="70px" HeaderStyle-HorizontalAlign="Left" HeaderText="">
                        <ItemTemplate> 
                            <asp:LinkButton ID="lnkDelete" Visible='<%# IsInSaveStatus(int.Parse(Eval("Status").ToString())) %>' Text="Delete"
                             runat="server" CommandName="Delete" OnClientClick="return confirm('Are you sure, you want to delete the record?');" />                            
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <PagerSettings Mode="NumericFirstLast" NextPageText="Next �" PreviousPageText="� Prev"
                    FirstPageText="First" LastPageText="Last" />
                <PagerStyle CssClass="defaultPagingBar" />

                <RowStyle CssClass="odd" />
                <AlternatingRowStyle CssClass="even" />
                <EmptyDataTemplate>
                    <b>No list. </b>
                </EmptyDataTemplate>
            </cc2:EmptyDisplayGridView>
            
            <uc1:PagingCtl ID="pagintCtl" OnNextRecord="btnNext_Click" OnPrevRecord="btnPrevious_Click"
                OnDropDownShowRecordsChanged="ddlRecords_SelectedIndexChanged" runat="server" />

        </div>
        <div class="buttonsDiv">
            <asp:LinkButton ID="LinkButton1" runat="server" Text="Export to Excel" OnClick="btnExport_Click"
                CssClass=" excel marginRight" Style="float: right;" />
        </div>
    </div>
</asp:Content>
