﻿<%@ Page Title="Branch Employees" Language="C#" MasterPageFile="~/Master/EmployeeExt.Master"
    AutoEventWireup="true" CodeBehind="BranchEmployees.aspx.cs" Inherits="Web.Employee.BranchEmployees" %>

<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="mainContent" runat="server">
    <div class="innerLR1">
        <h3 style="margin-top: 10px">
            Branch Employees
        </h3>
        <div class="alert alert-info" style="margin-top: 0px; padding-top: 0px!important;">
            <table>
                <tr>
                    <td>
                        <ext:ComboBox ForceSelection="true" Width="200" MatchFieldWidth="false" MarginSpec="0 5 0 5"
                            LabelSeparator="" LabelAlign="Top" QueryMode="Local" ID="cmbBranch" DisplayField="Name"
                            ValueField="BranchId" runat="server" FieldLabel="Branch">
                            <Items>
                                <ext:ListItem Index="0" Value="-1" Text="All">
                                </ext:ListItem>
                            </Items>
                            <Store>
                                <ext:Store ID="Store5" runat="server">
                                    <Model>
                                        <ext:Model ID="Model7" IDProperty="BranchId" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="BranchId" Type="String" />
                                                <ext:ModelField Name="Name" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                            <Triggers>
                                <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                            </Triggers>
                            <Listeners>
                                <Select Handler="this.getTrigger(0).show(); SelEmpSearch();" />
                                <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                <TriggerClick Handler="if (index == 0) { 
                                                       this.clearValue(); 
                                                       this.getTrigger(0).hide();
                                                   }" />
                            </Listeners>
                        </ext:ComboBox>
                    </td>
                    <td>
                        <ext:Store runat="server" ID="storeSearch" AutoLoad="false">
                            <Proxy>
                                <ext:AjaxProxy Url="../Handler/EmpSearch.ashx">
                                    <ActionMethods Read="GET" />
                                    <Reader>
                                        <ext:JsonReader Root="plants" TotalProperty="total" />
                                    </Reader>
                                </ext:AjaxProxy>
                            </Proxy>
                            <Model>
                                <ext:Model ID="Model5" IDProperty="EmployeeId" Name="ItemLineModel" runat="server">
                                    <Fields>
                                        <ext:ModelField Name="Name" Type="String" />
                                        <ext:ModelField Name="EmployeeId" Type="String" />
                                    </Fields>
                                </ext:Model>
                            </Model>
                        </ext:Store>
                        <ext:ComboBox LabelSeparator="" ID="cmbEmpSearch" FieldLabel="Search Employee" EmptyText="Employee Name"
                            LabelWidth="80" LabelAlign="Top" runat="server" DisplayField="Name" ValueField="EmployeeId"
                            StoreID="storeSearch" TypeAhead="false" Width="200" PageSize="9999" HideBaseTrigger="true"
                            MinChars="1" TriggerAction="All" ForceSelection="false">
                            <ListConfig LoadingText="Searching..." MinWidth="200" StyleSpec="border-top:1px solid #98c0f4;">
                                <ItemTpl ID="ItemTpl2" runat="server">
                                    <Html>
                                        <tpl>
                                                        <div class="search-item">
                                                                        <span>{Name}</span>  
                                                         </div>
					                            </tpl>
                                    </Html>
                                </ItemTpl>
                            </ListConfig>
                            <Triggers>
                                <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                            </Triggers>
                            <Listeners>
                                <Select Handler="this.getTrigger(0).show(); SelEmpSearch();" />
                                <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                <TriggerClick Handler="if (index == 0) { 
                                                       this.clearValue(); 
                                                       this.getTrigger(0).hide();
                                                   }" />
                            </Listeners>
                        </ext:ComboBox>
                    </td>
                    <td style="padding-left: 10px">
                        <ext:Button ID="btnLoad" runat="server" Text="Load" Width="100" Cls="btn btn-default"
                            MarginSpec="25 10 10 10">
                            <DirectEvents>
                                <Click OnEvent="btnLoad_Click">
                                    <EventMask ShowMask="true" />
                                </Click>
                            </DirectEvents>
                        </ext:Button>
                    </td>
                </tr>
            </table>
        </div>
        <ext:GridPanel StyleSpec="margin-top:15px;" ID="gridBranchEmployees" runat="server"
            Cls="itemgrid" Scroll="None">
            <Store>
                <ext:Store ID="storeBranchEmployees" runat="server" AutoLoad="true">
                    <Model>
                        <ext:Model ID="Model6" runat="server">
                            <Fields>
                                <ext:ModelField Name="Name" Type="String" />
                                <ext:ModelField Name="OfficeEmail" Type="String" />
                                <ext:ModelField Name="OfficeMobile" Type="String" />
                                <ext:ModelField Name="OfficePhone" Type="String" />
                                <ext:ModelField Name="Extension" Type="String" />
                                <ext:ModelField Name="Branch" Type="String" />
                                <ext:ModelField Name="Department" Type="String" />
                            </Fields>
                        </ext:Model>
                    </Model>
                </ext:Store>
            </Store>
            <ColumnModel>
                <Columns>
                    <ext:Column ID="colName" Sortable="false" MenuDisabled="true" runat="server" Text="Name"
                        Width="280" Align="Left" DataIndex="Name">
                    </ext:Column>
                    <ext:Column ID="Column1" Sortable="false" MenuDisabled="true" runat="server" Text="Branch"
                        Width="200" Align="Left" DataIndex="Branch">
                    </ext:Column>
                    <ext:Column ID="Column2" Sortable="false" MenuDisabled="true" runat="server" Text="Department"
                        Width="200" Align="Left" DataIndex="Department">
                    </ext:Column>
                    <ext:Column Selectable="true" ID="colOfficeEmail" Sortable="false" MenuDisabled="true"
                        runat="server" Text="Office Email" Width="250" Align="Left" DataIndex="OfficeEmail">
                        <Editor>
                            <ext:TextField runat="server" />
                        </Editor>
                    </ext:Column>
                    <ext:Column Selectable="true" ID="colOfficeMobile" Sortable="false" MenuDisabled="true"
                        runat="server" Text="Office Mobile" Width="150" Align="Left" DataIndex="OfficeMobile">
                        <Editor>
                            <ext:TextField ID="TextField1" runat="server" />
                        </Editor>
                    </ext:Column>
                    <ext:Column Selectable="true" ID="colOfficePhone" Sortable="false" MenuDisabled="true"
                        runat="server" Text="Office Phone" Width="150" Align="Left" DataIndex="OfficePhone">
                        <Editor>
                            <ext:TextField ID="TextField2" runat="server" />
                        </Editor>
                    </ext:Column>
                    <ext:Column Flex="1" Selectable="true" ID="colExtension" Sortable="false" MenuDisabled="true"
                        runat="server" Text="Extension" Width="150" Align="Left" DataIndex="Extension">
                        <Editor>
                            <ext:TextField ID="TextField3" runat="server" />
                        </Editor>
                    </ext:Column>
                </Columns>
            </ColumnModel>
            <SelectionModel>
                <ext:CellSelectionModel ID="RowSelectionModel5" runat="server" Mode="Single" />
            </SelectionModel>
            <View>
                <ext:GridView runat="server" SingleSelect="true" Selectable="true">
                </ext:GridView>
            </View>
            <Plugins>
                <ext:CellEditing ID="CellEditing1" ClicksToEdit="1" runat="server">
                </ext:CellEditing>
            </Plugins>
        </ext:GridPanel>
    </div>
    <br />
</asp:Content>
