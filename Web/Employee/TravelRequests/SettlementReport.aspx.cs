﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using System.Xml;

namespace Web.Appraisal
{
    public partial class SettlementReport : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();
            }
        }

        public void Initialise()
        {
            cmbLocations.Store[0].DataSource = TravelAllowanceManager.GetLocationList().OrderBy(x => x.LocationName).ToList();
            cmbLocations.Store[0].DataBind();

            storeBranch.DataSource = ActivityManager.GetAllBranches();
            storeBranch.DataBind();

            txtFromDateFilter.SelectedDate = DateTime.Now;
            txtEndDateFilter.SelectedDate = DateTime.Now;

            List<GenericComboBoxList> comboList = new List<GenericComboBoxList>();
            GenericComboBoxList item = new GenericComboBoxList();
            item.Name = "All";
            item.ID = -1;
            comboList.Add(item);
            item = new GenericComboBoxList();
            item.Name = "Settled";
            item.ID = (int)FlowStepEnum.Settled;
            comboList.Add(item);
            storeFilterAll.DataSource = comboList;
            storeFilterAll.DataBind();
            ComboFilterAll.ClearValue();


            CommonManager comManager = new CommonManager();
            cmbCountry.Store[0].DataSource = comManager.GetAllCountries();
            cmbCountry.Store[0].DataBind();

            cmbTravelBy.Store[0].DataSource = TravelAllowanceManager.getTravelByList();
            cmbTravelBy.Store[0].DataBind();

            cmbExpensePaidBy.Store[0].DataSource = TravelAllowanceManager.getExpensePaidByList();
            cmbExpensePaidBy.Store[0].DataBind();

            LoadLevels(-1, DateTime.Now.AddDays(-1000), DateTime.Now.AddDays(1000), -1, "");
        }
        
        private void LoadLevels(int branchID, DateTime fromDate, DateTime toDate, int show, string NameFilt)
        {
            //HardCoded Date Just incase we need date in future. Just pass the desired date to the function and Date filter works fine.

            GridLevels.GetStore().DataSource = TravelAllowanceManager.GetAllTravelRequestSettlement(branchID,fromDate, toDate,show, NameFilt);
            GridLevels.GetStore().DataBind();

        }


        
        public void ClearLevelFields()
        {
            hiddenValue.Text = "";
            //txtName.Text = "";
            //txtDescription.Text = "";
        }


        protected void btnSearch_Click(object sender, DirectEventArgs e)
        {
            int BranchID, Show;
            DateTime fromDate = new DateTime();
            DateTime toDate = new DateTime();
            string nameText;

            if (cmbBranch.SelectedItem != null)
            {
                BranchID = int.Parse(cmbBranch.SelectedItem.Value);
            }
            else
                BranchID = -1;

            if (txtFromDateFilter.SelectedDate != null)
            {
                fromDate = txtFromDateFilter.SelectedDate;
            }
            else
                fromDate = DateTime.Now.AddDays(-30);

            if (txtEndDateFilter.SelectedDate != null)
            {
                toDate = txtEndDateFilter.SelectedDate;
            }
            else
                toDate = DateTime.Now.AddDays(30);

            if (ComboFilterAll.SelectedItem != null)
            {
                Show = int.Parse(ComboFilterAll.SelectedItem.Value);
            }
            else
                Show = -1;

            nameText = txtNameFilter.Text;

            LoadLevels(BranchID, fromDate, toDate, Show, nameText);


        }

        
        protected void btnAddLevel_Click(object sender, DirectEventArgs e)
        {
            
            ClearLevelFields();
            //WindowLevel.Show();
        }


        protected void btnDeleteLevel_Click(object sender, DirectEventArgs e)
        {
            int TAllowance = int.Parse(hiddenValue.Text.Trim());
            Status status = TravelAllowanceManager.DeleteTravelAllowance(TAllowance);
            if (status.IsSuccess)
            {
                btnSearch_Click(null, null);
                NewMessage.ShowNormalMessage("Travel Allowance deleted.");
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }
        }

        

        protected void btnEditLevel_Click(object sender, DirectEventArgs e)
        {

            TARequest request = new TARequest();
            request = TravelAllowanceManager.getRequestByID(int.Parse(hiddenValue.Text));
            LoadEmployeeInfo(request.EmployeeId.Value);
            LoadAllowanceGrid(request.LocationId.Value, request.RequestID);
            LoadEditData(request);
            btnPrint.Show();
            Window1.Show();

        }

        protected void LoadAllowanceGrid(int locationId, int? RequestID)
        {
            //List<TARequestLine> lines = new List<TARequestLine>();
            //lines = TravelAllowanceManager.getAllowanceByEmployeeID(SessionManager.CurrentLoggedInEmployeeId, RequestID, countryID).Where(x => x.UnitType == null).ToList();
            //storeAllowances.DataSource = lines;
            //storeAllowances.DataBind();

            List<TARequestLine> lines = new List<TARequestLine>();
            
            
            lines = TravelAllowanceManager.getAllowanceByEmployeeID(SessionManager.CurrentLoggedInEmployeeId, RequestID, locationId).Where(x => x.UnitType == null).ToList();
            storeAllowances.DataSource = lines;
            storeAllowances.DataBind();

            
        }


        private void LoadEditData(TARequest loadRequest)
        {
            

            txtPlaceToTravel.Text = loadRequest.PlaceOfTravel;
            //cmbCountry.SelectedItem.Value = loadRequest.CountryId.Value.ToString();
            //ExtControlHelper.ComboBoxSetSelected(loadRequest.CountryId.Value.ToString(), cmbCountry);
            cmbCountry.SetValue(loadRequest.CountryId.ToString());

            txtPurposeOfTravel.Text = loadRequest.PurposeOfTravel;
            txtFromDate.Text = loadRequest.TravellingFromEng.Value.ToShortDateString();
            txtToDate.Text = loadRequest.TravellingToEng.Value.ToShortDateString();

            //cmbTravelBy.SelectedItem.Value = loadRequest.TravelBy.Value.ToString();
            //ExtControlHelper.ComboBoxSetSelected(loadRequest.TravelBy.Value.ToString(), cmbTravelBy);
            cmbTravelBy.SetValue(loadRequest.TravelBy.Value.ToString());
            //cmbExpensePaidBy.SelectedItem.Value = loadRequest.ExpensePaidBy.Value.ToString();
            //ExtControlHelper.ComboBoxSetSelected(loadRequest.ExpensePaidBy.Value.ToString(), cmbExpensePaidBy);
            cmbExpensePaidBy.SetValue(loadRequest.ExpensePaidBy.Value.ToString());
            //ExtControlHelper.ComboBoxSetSelected(loadRequest.LocationId.Value, cmbLocations);
            cmbLocations.SetValue(loadRequest.LocationId.Value.ToString());
            LoadAllowanceGrid(loadRequest.LocationId.Value);
            
            //X.Msg.Alert(loadRequest.LocationId.Value.ToString(), loadRequest.LocationId.Value).Show();

            txtDayCount.Text = (Math.Abs((loadRequest.TravellingFromEng.Value - loadRequest.TravellingToEng.Value).TotalDays) + 1).ToString();
            txtNightCount.Text = (Math.Abs((loadRequest.TravellingFromEng.Value - loadRequest.TravellingToEng.Value).TotalDays)).ToString();



        }

        protected void LoadEmployeeInfo(int EmployeeID)
        {
            EEmployee eemployee = EmployeeManager.GetEmployeeById(EmployeeID);
            lblName.Text = eemployee.Name;
            lblDepartment.Text = eemployee.Department.Name;
            lblDesignation.Text = eemployee.EDesignation.Name;
            lblBranch.Text = eemployee.Branch.Name;
            List<TARequestStatusHistory> statusHistory = TravelAllowanceManager.getTravelRequestForwardHistory(hiddenValue.Text);
            
            string body = "";
            foreach (var status in statusHistory)
            {
                body = body + "<tr><td  colspan='2' style='font-weight:bold'>" + status.whoseComment + "</td></tr><tr><td colspan='2' style='color:blue'>" + status.ApprovalRemarks + "</td></tr><tr><td>" + status.ApprovedByName + "</td><td>" + status.ApprovedOn.Value.ToShortDateString() + "</td></tr>";
            }
            body = "<table>" + body + "</table>";
            txtCommentSection.Html = body;

        }


        protected void Country_Change(object sender, DirectEventArgs e)
        {
            //LoadAllowanceGrid(int.Parse(cmbCountry.SelectedItem.Value));
        }


        protected void CheckInOutSelectChange(object sender, DirectEventArgs e)
        {
            string actualInDate = "";
            string actualOutDate = "";
            if (txtFromDate.Text != "0001-01-01 12:00:00 AM")
                actualInDate = txtFromDate.Text;
            if (txtToDate.Text != "0001-01-01 12:00:00 AM")
                actualOutDate = txtToDate.Text;
            DateTime englishInDate;
            DateTime englishOutDate;
            if (!string.IsNullOrEmpty(actualInDate) && !string.IsNullOrEmpty(actualOutDate))
            {
                englishInDate = DateTime.Parse(actualInDate);
                englishOutDate = DateTime.Parse(actualOutDate);

                txtDayCount.Text = (Math.Abs((englishInDate - englishOutDate).TotalDays) + 1).ToString();
                txtNightCount.Text = (Math.Abs((englishInDate - englishOutDate).TotalDays)).ToString();

                if ((englishInDate - englishOutDate).TotalDays == 0)
                {
                    txtDayCount.Text = "1";
                    txtNightCount.Text = "1";
                }

            }
            else
            {
                txtDayCount.Text = "0";
                txtNightCount.Text = "0";
            }
        }


        protected void LoadAllowanceGrid(int locationId)
        {
            //List<TARequestLine> lines = new List<TARequestLine>();
            //int? RequestID = null;
            //if (!string.IsNullOrEmpty(hiddenValue.Text))
            //{
            //    RequestID = int.Parse(hiddenValue.Text);
            //}
            //lines = TravelAllowanceManager.getAllowanceByEmployeeID(SessionManager.CurrentLoggedInEmployeeId, RequestID, countryID);
            //storeAllowances.DataSource = lines;
            //storeAllowances.DataBind();

            List<TARequestLine> lines = new List<TARequestLine>();
            int? RequestID = null;
            if (!string.IsNullOrEmpty(Hidden_QuerryStringID.Text))
            {
                RequestID = int.Parse(Hidden_QuerryStringID.Text);
            }
            lines = TravelAllowanceManager.getAllowanceByEmployeeID(SessionManager.CurrentLoggedInEmployeeId, RequestID, locationId).Where(x => x.UnitType == null).ToList();
            storeAllowances.DataSource = lines;
            storeAllowances.DataBind();


        }

        protected void btnDetailLevel_Click(object sender, DirectEventArgs e)
        {
            
        }


        protected void btnDetailSaveUpdate_Click(object sender, DirectEventArgs e)
        {
            List<TAAllowanceRate> lines = new List<TAAllowanceRate>();
            string json = e.ExtraParams["Values"];

            if (string.IsNullOrEmpty(json))
            {
                return;
            }


            lines = JSON.Deserialize<List<TAAllowanceRate>>(json);
            int TAllowance = int.Parse(hiddenValue.Text.Trim());

            Status status = TravelAllowanceManager.InsertUpdateAllowanceDetail(lines,TAllowance);
            if (status.IsSuccess)
            {
                Window1.Hide();
                btnSearch_Click(null, null);
                NewMessage.ShowNormalMessage("Rating Scale saved.");
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }


        }


        protected void ButtonNext_Click(object sender, DirectEventArgs e)
        {

            TARequest request = new TARequest();
            TARequestStatusHistory hist = new TARequestStatusHistory();
            bool isEdit;
            bool isStatusIncrease=true;

            isEdit = string.IsNullOrEmpty(hiddenValue.Text);
            if (!isEdit)
            {
                request.RequestID = int.Parse(hiddenValue.Text);
                request.Status = TravelAllowanceManager.getRequestByID(int.Parse(hiddenValue.Text)).Status;
            }

            if (!string.IsNullOrEmpty(txtDayCount.Text))
            {
                request.Days = int.Parse(txtDayCount.Text);
            }
            if (!string.IsNullOrEmpty(txtNightCount.Text))
            {
                request.Night = int.Parse(txtNightCount.Text);
            }

            hist.ApprovalRemarks = txtHRComment.Text;
            request.PlaceOfTravel = txtPlaceToTravel.Text;
            request.CountryId = int.Parse(cmbCountry.SelectedItem.Value);
            request.CountryName = cmbCountry.SelectedItem.Text;
            request.LocationId = int.Parse(cmbLocations.SelectedItem.Value);
            request.PurposeOfTravel = txtPurposeOfTravel.Text;
            request.TravellingFromEng = DateTime.Parse(txtFromDate.Text);
            request.TravellingToEng = DateTime.Parse(txtToDate.Text);
            request.TravelBy = int.Parse(cmbTravelBy.SelectedItem.Value);
            request.TravelByText = cmbTravelBy.SelectedItem.Text;
            request.ExpensePaidBy = int.Parse(cmbExpensePaidBy.SelectedItem.Value);
            request.ExpensePaidByText = cmbExpensePaidBy.SelectedItem.Value;
            request.EmployeeId = SessionManager.CurrentLoggedInEmployeeId;

            List<TARequestLine> lines = new List<TARequestLine>();
            string json = e.ExtraParams["AllowanceValues"];
            if (string.IsNullOrEmpty(json))
            {
                return;
            }


            lines = JSON.Deserialize<List<TARequestLine>>(json);

            Status status = TravelAllowanceManager.InsertUpdateTravelRequests(request, lines, hist, isStatusIncrease);
            if (status.IsSuccess)
            {

                NewMessage.ShowNormalMessage("Allowance Forwarded");
                Window1.Close();
                btnSearch_Click(null, null);
                //Response.Redirect("TravelRequestHRView.aspx");
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }


        }


        protected void cmbLocations_Change(object sender, DirectEventArgs e)
        {

            List<TARequestLine> lines = new List<TARequestLine>();
            string json = e.ExtraParams["Values"];
            if (!string.IsNullOrEmpty(json))
            {

                lines = JSON.Deserialize<List<TARequestLine>>(json);

                foreach (TARequestLine line in lines)
                {

                    TAAllowanceRate rate = TravelAllowanceManager.GetRate(SessionManager.CurrentLoggedInEmployeeId,
                        int.Parse(cmbLocations.SelectedItem.Value),
                         line.AllowanceId.Value);

                    if (rate != null && rate.Rate != null)
                        line.Rate = rate.Rate;
                    else
                        line.Rate = 0;

                    //line.Total = Convert.ToDecimal(line.Quantity) * Convert.ToDecimal(line.Rate);
                }

            }


            storeAllowances.DataSource = lines;
            storeAllowances.DataBind();
            //LoadAllowanceGrid(int.Parse(cmbLocations.SelectedItem.Value));
        }

        protected void btnPrint_Click(object sender, EventArgs e)
        {
            CP.Report.ReportHelper.TravelRequestPrint(int.Parse(hiddenValue.Text), txtFromDate.SelectedDate.ToShortDateString(), txtToDate.SelectedDate.ToShortDateString());
        } 
 
    }
}
