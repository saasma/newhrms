﻿<%@ Page Title="Travel Request Settlement List" Language="C#" MasterPageFile="~/Master/EmployeeExt.Master"
    AutoEventWireup="true" CodeBehind="TravelAdvanceSettlement.aspx.cs" Inherits="Web.Appraisal.TravelAdvanceSettlement" %>

<%--<%@ Register Src="~/NewHR/UserControls/EmployeeDetailsCtl.ascx" TagName="EmployeeDetailsCtl"
    TagPrefix="uc1" %>--%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">


        
        var CommandHandler1 = function(command, record){
            <%= hiddenValue.ClientID %>.setValue(record.data.RequestID);
                if(command=="Edit")
                {
                    <%= btnEditLevel.ClientID %>.fireEvent('click');
                }
             }
             


             var totalCost = function (records) {
            var i = 0,
                length = records.length,
                total = 0,
                record;

            for (; i < length; ++i) {
                record = records[i];
                total += record.get('Quantity') * record.get('Rate');
            }

            return total;
        };
        



        var afterEdit = function()
        {
            var records = <%=GridPanel1.ClientID %>.getStore();
             var i = 0,
                length = records.data.length,
                total = 0,
                record;

            for (; i < length; ++i) {
                record = records.data.items[i];
                total += record.data.Quantity * record.data.Rate;
            }

            total = getFormattedAmount(total);
            <%=dispTotal.ClientID %>.setValue(total);
        }

        function DateTypeSelect()
        {
            var type = <%=cmbType.ClientID %>.getValue();

            if(type == -1)
            {
                <%=txtFromDateFilter.ClientID %>.show();
                <%=txtToDateFilter.ClientID %>.show();
            }
            else
            {
                <%=txtFromDateFilter.ClientID %>.hide();
                <%=txtToDateFilter.ClientID %>.hide();
            }
        }
        
        function searchList() {
             <%=GridLevels.ClientID %>.getStore().pageSize = <%=cmbPageSize.ClientID %>.getValue();
            <%=PagingToolbar1.ClientID %>.doRefresh();
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
    <div class="contentArea">
        <h4 class="heading">
            Travel Request Settlement Report</h4>
        <ext:Hidden runat="server" ID="hiddenValue" />
        <ext:Hidden runat="server" ID="Hidden_QuerryStringID">
        </ext:Hidden>
        <ext:LinkButton runat="server" Hidden="true" ID="btnEditLevel">
            <DirectEvents>
                <Click OnEvent="btnEditLevel_Click">
                    <EventMask ShowMask="true" />
                </Click>
            </DirectEvents>
        </ext:LinkButton>
        <ext:LinkButton runat="server" Hidden="true" ID="btnDetailLevel">
            <DirectEvents>
                <Click OnEvent="btnDetailLevel_Click">
                    <EventMask ShowMask="true" />
                </Click>
            </DirectEvents>
        </ext:LinkButton>
        <ext:Button ID="btnDeleteLevel" runat="server" Hidden="true">
            <DirectEvents>
                <Click OnEvent="btnDeleteLevel_Click">
                    <Confirmation ConfirmRequest="true" Message="Are you sure, you want to delete the Allowance settings?" />
                    <EventMask ShowMask="true" />
                </Click>
            </DirectEvents>
        </ext:Button>
        <%--<uc1:EmployeeDetailsCtl Id="EmployeeDetailsCtl1" runat="server" />--%>
        <table class="fieldTable firsttdskip">
            <tr>
                <td>
                    <ext:ComboBox ID="cmbType" runat="server" Width="150" LabelWidth="40" FieldLabel="Show"
                        LabelAlign="Top" LabelSeparator="" ForceSelection="true" QueryMode="Local">
                        <Items>
                            <ext:ListItem Text="This Month" Value="1" />
                            <ext:ListItem Text="Last Month" Value="2" />
                            <ext:ListItem Text="This Year" Value="3" />
                            <ext:ListItem Text="Date Range" Value="-1" />
                        </Items>
                        <SelectedItems>
                            <ext:ListItem Text="This Month" Value="1" />
                        </SelectedItems>
                        <Listeners>
                            <Select Fn="DateTypeSelect" />
                        </Listeners>
                    </ext:ComboBox>
                </td>
                <td>
                    <ext:DateField ID="txtFromDateFilter" runat="server" FieldLabel="From Date" LabelSeparator=""
                        LabelAlign="Top" Width="150" Hidden="true" />
                </td>
                <td>
                    <ext:DateField ID="txtToDateFilter" runat="server" FieldLabel="To Date" LabelSeparator=""
                        LabelAlign="Top" Width="150" Hidden="true" />
                </td>
                <td style="padding-top: 20px">
                    <ext:Button runat="server" ID="btnLoad" Cls="btn btn-default" StyleSpec="margin-top:10px"
                        Height="30" Text="<i></i>Load">
                        <Listeners>
                            <Click Fn="searchList">
                            </Click>
                        </Listeners>
                    </ext:Button>
                </td>
            </tr>
        </table>
        <ext:GridPanel StyleSpec="margin-top:15px;" ID="GridLevels" runat="server" OnReadData="Store_ReadData">
            <Store>
                <ext:Store ID="storeTR" runat="server" AutoLoad="true" OnReadData="Store_ReadData"
                    RemotePaging="true" RemoteSort="true">
                    <Model>
                        <ext:Model ID="Model4" runat="server" IDProperty="RequestID">
                            <Fields>
                                <ext:ModelField Name="RequestID" Type="String" />
                                <ext:ModelField Name="EmployeeId" Type="string" />
                                <ext:ModelField Name="FromDate" Type="Date" />
                                <ext:ModelField Name="ToDate" Type="Date" />
                                <ext:ModelField Name="EmpName" Type="string" />
                                <ext:ModelField Name="Position" Type="string" />
                                <ext:ModelField Name="Department" Type="string" />
                                <ext:ModelField Name="Destination" Type="string" />
                                <ext:ModelField Name="Reason" Type="string" />
                                <ext:ModelField Name="Duration" Type="string" />
                                <ext:ModelField Name="Status" Type="string" />
                                <ext:ModelField Name="LocationName" Type="string" />
                                <ext:ModelField Name="StatusName" Type="string" />
                                <ext:ModelField Name="ApprovedAmount" Type="string" />
                                <ext:ModelField Name="AdvanceAmount" Type="string" />
                                <ext:ModelField Name="ClaimAmount" Type="string" />
                                <ext:ModelField Name="SettledAmount" Type="string" />
                            </Fields>
                        </ext:Model>
                    </Model>
                </ext:Store>
            </Store>
            <ColumnModel>
                <Columns>
                    <ext:DateColumn ID="DateColumn1" runat="server" Text="From Date" Sortable="true"
                        DataIndex="FromDate" Width="85" Align="Left" Format="MM/dd/yyyy">
                    </ext:DateColumn>
                    <ext:DateColumn ID="DateColumn2" runat="server" Text="To Date" Sortable="true" DataIndex="ToDate"
                        Width="86" Align="Left" Format="MM/dd/yyyy">
                    </ext:DateColumn>
                    <ext:Column ID="Column5" Sortable="false" MenuDisabled="true" runat="server" Text="Destination"
                        Width="120" Align="Left" DataIndex="Destination" />
                    <ext:Column ID="Column10" Sortable="false" MenuDisabled="true" runat="server" Text="Location"
                        Width="130" Align="Left" DataIndex="LocationName" />
                    <ext:Column ID="Column7" Sortable="false" MenuDisabled="true" runat="server" Text="Duration"
                        Width="120" Align="Left" DataIndex="Duration" />
                    <ext:Column ID="Column8" Sortable="false" MenuDisabled="true" runat="server" Text="Status"
                        Width="110" Align="Left" DataIndex="StatusName">
                    </ext:Column>
                    <ext:Column ID="Column3" Sortable="false" MenuDisabled="true" runat="server" Text="Approved"
                        Width="90" Align="Right" DataIndex="ApprovedAmount">
                        <Renderer Handler="getFormattedAmount" />
                    </ext:Column>
                    <ext:Column ID="Column4" Sortable="false" MenuDisabled="true" runat="server" Text="Advance"
                        Width="90" Align="Right" DataIndex="AdvanceAmount">
                        <Renderer Handler="getFormattedAmount" />
                    </ext:Column>
                    <ext:Column ID="Column18" Sortable="false" MenuDisabled="true" runat="server" Text="Claimed"
                        Width="90" Align="Right" DataIndex="ClaimAmount">
                        <Renderer Handler="getFormattedAmount" />
                    </ext:Column>
                    <ext:Column ID="Column19" Sortable="false" MenuDisabled="true" runat="server" Text="Settled"
                        Width="90" Align="Right" DataIndex="SettledAmount">
                        <Renderer Handler="getFormattedAmount" />
                    </ext:Column>
                    <ext:CommandColumn runat="server" Align="Center" Width="50">
                        <Commands>
                            <ext:CommandSeparator />
                            <ext:GridCommand Cls="editGridButton" ToolTip-Text="Edit" Text="<i class='fa fa-pencil'></i>"
                                CommandName="Edit" />
                        </Commands>
                        <Listeners>
                            <Command Handler="CommandHandler1(command,record);" />
                        </Listeners>
                    </ext:CommandColumn>
                </Columns>
            </ColumnModel>
            <SelectionModel>
                <ext:RowSelectionModel ID="RowSelectionModel2" runat="server" Mode="Single" />
            </SelectionModel>
            <BottomBar>
                <ext:PagingToolbar ID="PagingToolbar1" runat="server" StoreID="storeTR" DisplayInfo="true">
                    <Items>
                        <ext:Label ID="lblPageSize" runat="server" Text="Page size:" />
                        <ext:ToolbarSpacer ID="ToolbarSpacer_listItems" runat="server" Width="10" />
                        <ext:ComboBox ID="cmbPageSize" runat="server" Width="80" SelectOnFocus="true" Selectable="true"
                            ValueField="Value" DisplayField="Text" ForceSelection="true" AllowBlank="false">
                            <Listeners>
                                <Select Handler="searchList()" />
                                <%--#{cmbPageSize}.pageSize = this.getValue();#{PagingToolbar1}.moveFirst();--%>
                            </Listeners>
                            <Items>
                                <ext:ListItem Value="20" Text="20" />
                                <ext:ListItem Value="50" Text="50" />
                                <ext:ListItem Value="100" Text="100" />
                            </Items>
                            <SelectedItems>
                                <ext:ListItem Index="0">
                                </ext:ListItem>
                            </SelectedItems>
                        </ext:ComboBox>
                    </Items>
                </ext:PagingToolbar>
            </BottomBar>
        </ext:GridPanel>
        <div style="float: right; padding-right: 30px; display: none">
            <ext:Button runat="server" AutoPostBack="true" OnClick="btnExportTR_Click" ID="btnExportTR"
                Text="<i></i>Export To Excel">
            </ext:Button>
        </div>
        <ext:Window ID="Window1" runat="server" Title="Travel Allowance Settlement" AutoScroll="true"
            Icon="Application" Width="1000" Height="600" BodyPadding="5" Hidden="true" Modal="true">
            <Content>
                <div style="width: 400px; float: left">
                    <table id="albums1" width="400px;">
                        <tr>
                            <td>
                                <table>
                                    <tr>
                                        <td valign="top" style="display: none">
                                            <ext:Image ID="image" ImageUrl="~/images/sample.jpg" runat="server" Width="120px"
                                                Height="120px" />
                                        </td>
                                        <td valign="top" style="padding-left: 10px">
                                            <table>
                                                <tr>
                                                    <td>
                                                        <ext:Label ID="lblName" StyleSpec="font-weight:bold;color:#0099FF;font-size:16px;"
                                                            runat="server">
                                                        </ext:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <ext:Label ID="lblBranch" runat="server">
                                                        </ext:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <ext:Label ID="lblDepartment" runat="server">
                                                        </ext:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <ext:Label ID="lblDesignation" runat="server">
                                                        </ext:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
                <div style="width: 500px; float: left; padding-left: 25px;">
                    <div style="float: right; width: 90px; margin-top:10px;">
                        <ext:LinkButton ID="btnPrint" runat="server" Text="Print" OnClick="btnPrint_Click" Hidden="true"
                            Icon="Printer" AutoPostBack="true">
                        </ext:LinkButton>
                    </div>
                    <table id="albums" cellspacing="0px">
                        <tr style="padding-bottom: 1em;">
                            <td colspan="1" style="margin-right: 25px; margin-left: 25px;">
                                <ext:TextField ID="txtPlaceToTravel" LabelSeparator="" Width="200" runat="server"
                                    FieldLabel="Place to Travel" Disabled="true" LabelAlign="Top">
                                </ext:TextField>
                            </td>
                            <td colspan="1" style="padding-left: 25px; padding-top: 0px;">
                                <ext:ComboBox ID="cmbCountry" LabelSeparator="" runat="server" Width="200" FieldLabel="Country"
                                    LabelAlign="Top" Disabled="true" QueryMode="Local" DisplayField="CountryName"
                                    ValueField="CountryId">
                                    <Store>
                                        <ext:Store ID="store2" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="CountryId" Type="String" />
                                                <ext:ModelField Name="CountryName" />
                                            </Fields>
                                        </ext:Store>
                                    </Store>
                                    <DirectEvents>
                                        <Change OnEvent="Country_Change">
                                        </Change>
                                    </DirectEvents>
                                </ext:ComboBox>
                            </td>
                        </tr>
                        <tr style="padding-bottom: 1em;">
                            <td colspan="5">
                                <ext:TextArea ID="txtPurposeOfTravel" Height="70" LabelSeparator="" Width="520" runat="server"
                                    FieldLabel="Purpose of Travel" Disabled="true" LabelAlign="Top">
                                </ext:TextArea>
                            </td>
                        </tr>
                        <tr style="padding-bottom: 1em;">
                            <td colspan="1">
                                <ext:DateField FieldLabel="Travelling from" Width="150" ID="txtFromDate" runat="server"
                                    EmptyDate="" LabelAlign="Top" LabelSeparator="">
                                    <DirectEvents>
                                        <Change OnEvent="CheckInOutSelectChange">
                                        </Change>
                                    </DirectEvents>
                                </ext:DateField>
                            </td>
                            <td colspan="1">
                                <ext:DateField FieldLabel="Travelling to" Width="150" ID="txtToDate" runat="server"
                                    LabelAlign="Top" EmptyDate="" LabelSeparator="">
                                    <DirectEvents>
                                        <Change OnEvent="CheckInOutSelectChange">
                                        </Change>
                                    </DirectEvents>
                                </ext:DateField>
                            </td>
                            <td colspan="1" style="padding-right: 0px;">
                                <ext:NumberField ID="txtDayCount" LabelSeparator="" runat="server" Width="60" FieldLabel="Days"
                                    LabelAlign="Top">
                                </ext:NumberField>
                            </td>
                            <td colspan="1">
                                <ext:NumberField ID="txtNightCount" LabelSeparator="" runat="server" Width="60" FieldLabel="Night"
                                    LabelAlign="Top">
                                </ext:NumberField>
                            </td>
                        </tr>
                        <tr style="padding-bottom: 1em;">
                            <td colspan="1">
                                <ext:ComboBox ForceSelection="true" LabelSeparator="" ID="cmbTravelBy" runat="server"
                                    FieldLabel="Travel by" LabelAlign="Top" QueryMode="Local" DisplayField="Name"
                                    ValueField="Value">
                                    <Store>
                                        <ext:Store ID="storeTravelBy" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="Name" />
                                                <ext:ModelField Name="Value" Type="String" />
                                            </Fields>
                                        </ext:Store>
                                    </Store>
                                </ext:ComboBox>
                            </td>
                            <td colspan="1">
                                <ext:ComboBox ForceSelection="true" LabelSeparator="" ID="cmbExpensePaidBy" runat="server"
                                    FieldLabel="Expense Paid by" LabelAlign="Top" QueryMode="Local" DisplayField="Name"
                                    ValueField="Value">
                                    <Store>
                                        <ext:Store ID="storeExpensePaidBy" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="Name" />
                                                <ext:ModelField Name="Value" Type="String" />
                                            </Fields>
                                        </ext:Store>
                                    </Store>
                                </ext:ComboBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <ext:ComboBox ID="cmbLocations" ForceSelection="true" runat="server" Width="150"
                                    Disabled="true" FieldLabel="Location for Allowance *" LabelAlign="Top" LabelSeparator=" "
                                    QueryMode="Local" DisplayField="LocationName" ValueField="LocationId">
                                    <Store>
                                        <ext:Store ID="store1" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="LocationId" Type="String" />
                                                <ext:ModelField Name="LocationName" />
                                            </Fields>
                                        </ext:Store>
                                    </Store>
                                    <DirectEvents>
                                        <Select OnEvent="cmbLocations_Change">
                                            <EventMask ShowMask="true" />
                                            <ExtraParams>
                                                <ext:Parameter Name="Values" Value="Ext.encode(#{gridAllowances}.getRowsValues({selectedOnly : false}))"
                                                    Mode="Raw" />
                                            </ExtraParams>
                                        </Select>
                                    </DirectEvents>
                                </ext:ComboBox>
                            </td>
                        </tr>
                    </table>
                </div>
                <table style="clear: both">
                    <tr>
                        <td>
                            <div>
                                <span style="color: Blue; padding-top: 5px">Approved</span>
                                <ext:GridPanel StyleSpec="margin-top:15px;" ID="gridAllowances" runat="server" Width="400"
                                    Height="170" Style="color: #f7f7f7" Cls="itemgrid">
                                    <Store>
                                        <ext:Store ID="storeAllowances" runat="server">
                                            <Model>
                                                <ext:Model ID="Model1" runat="server" Name="Model1">
                                                    <Fields>
                                                        <ext:ModelField Name="LineId" Type="String" />
                                                        <ext:ModelField Name="RequestId" Type="string" />
                                                        <ext:ModelField Name="AllowanceId" Type="string" />
                                                        <ext:ModelField Name="AllowanceName" Type="string" />
                                                        <ext:ModelField Name="Quantity" Type="Float" />
                                                        <ext:ModelField Name="Rate" Type="Float" />
                                                        <ext:ModelField Name="Total" Type="Float" />
                                                        <ext:ModelField Name="IsEditable" Type="String" />
                                                    </Fields>
                                                </ext:Model>
                                            </Model>
                                        </ext:Store>
                                    </Store>
                                    <Plugins>
                                        <ext:CellEditing ID="CellEditing1" ClicksToEdit="1" runat="server">
                                            <Listeners>
                                            </Listeners>
                                        </ext:CellEditing>
                                    </Plugins>
                                    <ColumnModel>
                                        <Columns>
                                            <ext:Column ID="Column1" Sortable="false" MenuDisabled="true" runat="server" Text="Allowances"
                                                Align="Left" DataIndex="AllowanceName" Width="150" />
                                            <ext:Column ID="Column2" Sortable="false" MenuDisabled="true" runat="server" Text="Qty"
                                                Align="Left" DataIndex="Quantity" Width="80">
                                                <%--<Editor>
                                                                    <ext:NumberField ID="txtQTY" SelectOnFocus="true" runat="server" AllowBlank="false" />
                                                                </Editor>--%>
                                            </ext:Column>
                                            <ext:Column ID="Column9" Sortable="false" MenuDisabled="true" runat="server" Text="Rate"
                                                Align="Left" DataIndex="Rate" Width="80">
                                            </ext:Column>
                                            <ext:Column runat="server" Width="90" ID="Total" Text="Total" Sortable="false" Groupable="false"
                                                DataIndex="Total" CustomSummaryType="totalCost">
                                                <Renderer Handler="return Ext.util.Format.number((record.data.Quantity * record.data.Rate),'0,000.00');" />
                                            </ext:Column>
                                        </Columns>
                                    </ColumnModel>
                                    <SelectionModel>
                                        <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" Mode="Single" />
                                    </SelectionModel>
                                </ext:GridPanel>
                            </div>
                        </td>
                        <td>
                            <div style="margin-left: 25px;">
                                <span style="color: Blue; padding-top: 5px">Actual Amount</span>
                                <ext:GridPanel ID="GridPanel1" runat="server" Width="525" Height="170" Cls="itemgrid">
                                    <Store>
                                        <ext:Store ID="storeSettlement" runat="server">
                                            <Model>
                                                <ext:Model ID="Model2" runat="server" Name="Model2">
                                                    <Fields>
                                                        <ext:ModelField Name="LineId" Type="String" />
                                                        <ext:ModelField Name="RequestId" Type="string" />
                                                        <ext:ModelField Name="AllowanceId" Type="string" />
                                                        <ext:ModelField Name="AllowanceName" Type="string" />
                                                        <ext:ModelField Name="FromDate" Type="string" />
                                                        <ext:ModelField Name="ToDate" Type="string" />
                                                        <ext:ModelField Name="Quantity" Type="Float" />
                                                        <ext:ModelField Name="Rate" Type="Float" />
                                                        <ext:ModelField Name="Total" Type="Float" />
                                                        <ext:ModelField Name="HRTotal" Type="Float" />
                                                    </Fields>
                                                </ext:Model>
                                            </Model>
                                        </ext:Store>
                                    </Store>
                                    <Plugins>
                                        <ext:CellEditing ID="CellEditing2" ClicksToEdit="1" runat="server">
                                            <Listeners>
                                                <Edit Fn="afterEdit" />
                                            </Listeners>
                                        </ext:CellEditing>
                                    </Plugins>
                                    <ColumnModel>
                                        <Columns>
                                            <ext:Column ID="Column11" Sortable="false" MenuDisabled="true" runat="server" Text="Allowances"
                                                Align="Left" DataIndex="AllowanceName" Visible="false" Width="175" />
                                            <ext:Column ID="Column15" Sortable="false" MenuDisabled="true" runat="server" Text="From Date"
                                                Align="Left" DataIndex="FromDate" Width="115" />
                                            <ext:Column ID="Column16" Sortable="false" MenuDisabled="true" runat="server" Text="To Date"
                                                Align="Left" DataIndex="ToDate" Width="115" />
                                            <ext:Column ID="Column12" Sortable="false" MenuDisabled="true" runat="server" Text="Qty"
                                                Align="Left" DataIndex="Quantity" Width="60">
                                                <Editor>
                                                    <ext:NumberField ID="txtQTY" SelectOnFocus="true" runat="server" AllowBlank="false" />
                                                </Editor>
                                            </ext:Column>
                                            <ext:Column ID="Column13" Sortable="false" Visible="false" MenuDisabled="true" runat="server"
                                                Text="Rate" Align="Left" DataIndex="Rate" Width="125">
                                            </ext:Column>
                                            <ext:Column runat="server" Align="Right" Width="100" ID="Column14" Text="Total" Sortable="false"
                                                SummaryType="Sum" Groupable="false" DataIndex="Total" CustomSummaryType="totalCost">
                                                <Renderer Handler="return Ext.util.Format.number((record.data.Quantity * record.data.Rate),'0,000.00');" />
                                            </ext:Column>
                                            <ext:Column ID="Column17" Align="Right" Sortable="false" MenuDisabled="true" runat="server"
                                                Text="HR Total" DataIndex="HRTotal" Width="100">
                                                <Renderer Handler="getFormattedAmount" />
                                            </ext:Column>
                                        </Columns>
                                    </ColumnModel>
                                    <Features>
                                        <ext:Summary ID="Summary1" runat="server" />
                                    </Features>
                                </ext:GridPanel>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <ext:DisplayField ID="dispTotal1" Text="0" runat="server" FieldLabel="Total " LabelAlign="Left">
                            </ext:DisplayField>
                        </td>
                        <td>
                            <table style="width: 100%">
                                <tr>
                                    <td>
                                        <ext:DisplayField Width="120" ID="txtAdvanceAmount" LabelWidth="70" Text="0" runat="server"
                                            FieldLabel="Advance " LabelAlign="Left" PaddingSpec="0 0 0 25">
                                        </ext:DisplayField>
                                    </td>
                                    <td>
                                        <ext:DisplayField Width="100" LabelWidth="40" ID="dispTotal" Text="0" runat="server"
                                            FieldLabel="Total " LabelAlign="Left" PaddingSpec="0 0 0 130">
                                        </ext:DisplayField>
                                    </td>
                                    <td>
                                        <ext:DisplayField Width="120" LabelWidth="70" LabelSeparator="" ID="dispFinalHRTotal"
                                            Text="0" runat="server" FieldLabel="HR Settled " LabelAlign="Left" PaddingSpec="0 0 0 0">
                                        </ext:DisplayField>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <ext:Button ID="btnSaveAndLater" Cls="btn btn-primary" Width="150" runat="server"
                    StyleSpec=" float:right; margin-right:25px;margin-top:10px;" Text="&nbsp;&nbsp;Save and Send&nbsp;&nbsp;"
                    ValidationGroup="InsertUpdate">
                    <DirectEvents>
                        <Click OnEvent="ButtonNext_Click">
                            <Confirmation ConfirmRequest="true" Message="Are you sure, you want to forward for settlement, after forwarding you can not change anything?" />
                            <EventMask ShowMask="true" />
                            <ExtraParams>
                                <ext:Parameter Name="AllowanceValues" Value="Ext.encode(#{GridPanel1}.getRowsValues({selectedOnly : false}))"
                                    Mode="Raw" />
                                <ext:Parameter Name="AllowanceValuesOrig" Value="Ext.encode(#{gridAllowances}.getRowsValues({selectedOnly : false}))"
                                    Mode="Raw" />
                            </ExtraParams>
                        </Click>
                    </DirectEvents>
                    <Listeners>
                        <Click Handler="valGroup = 'InsertUpdate'; return CheckValidation();">
                        </Click>
                    </Listeners>
                </ext:Button>
            </Content>
        </ext:Window>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtPlaceToTravel"
            ErrorMessage="Place to travel is required." Display="None" ValidationGroup="InsertUpdate">
        </asp:RequiredFieldValidator>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="cmbCountry"
            ErrorMessage="Country is required" Display="None" ValidationGroup="InsertUpdate">
        </asp:RequiredFieldValidator>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtPurposeOfTravel"
            ErrorMessage="Purpose of Travel is required" E Display="None" ValidationGroup="InsertUpdate">
        </asp:RequiredFieldValidator>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtFromDate"
            ErrorMessage="From Date is required" Display="None" ValidationGroup="InsertUpdate">
        </asp:RequiredFieldValidator>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="txtToDate"
            ErrorMessage="To Date is required" Display="None" ValidationGroup="InsertUpdate">
        </asp:RequiredFieldValidator>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="cmbTravelBy"
            ErrorMessage="Travel is required" Display="None" ValidationGroup="InsertUpdate">
        </asp:RequiredFieldValidator>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ControlToValidate="cmbExpensePaidBy"
            ErrorMessage="Expense Paid by is required" Display="None" ValidationGroup="InsertUpdate">
        </asp:RequiredFieldValidator>
    </div>
</asp:Content>
