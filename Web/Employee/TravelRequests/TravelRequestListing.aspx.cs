﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using System.Xml;

namespace Web.Appraisal
{
    public partial class TravelRequestListing : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();
            }
        }

        public void Initialise()
        {

            if (CommonManager.SettingTA.IsLocationEditable != null && CommonManager.SettingTA.IsLocationEditable.Value)
            {
                ColumnDestination.Hide();
            }

            LoadLevels();
        }
        
        private void LoadLevels()
        {
            GridLevels.GetStore().DataSource = TravelAllowanceManager.GetAllTravelRequestListingByUser(SessionManager.CurrentLoggedInEmployeeId,null,null, -1);
            GridLevels.GetStore().DataBind();
        }


        
        public void ClearLevelFields()
        {
            hiddenValue.Text = "";
            //txtName.Text = "";
            //txtDescription.Text = "";
        }

      
        protected void btnEditLevel_Click(object sender, DirectEventArgs e)
        {

            TARequest request = new TARequest();
            request = TravelAllowanceManager.getRequestByID(int.Parse(hiddenValue.Text));
            //if (request.Status == (int)FlowStepEnum.Step2 || request.Status == null)
            {
                Response.Redirect("NewtravelRequest.aspx?id="+request.RequestID.ToString());
            }
            //else
            //{
            //    X.Msg.Alert("Warning", "This Request Cant be modifed, its already been forwarded.").Show();
            //}

        }



        protected void btnDelete_Click(object sender, DirectEventArgs e)
        {

            int taid = int.Parse(hiddenValue.Text);

            Status status = TravelAllowanceManager.DeleteTABySelfEmployee(taid);
            if (status.IsSuccess)
            {
                LoadLevels();
                NewMessage.ShowNormalMessage("Travel Request has been deleted.");
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }

        }

    }
}

