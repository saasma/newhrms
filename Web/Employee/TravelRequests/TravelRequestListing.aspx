﻿<%@ Page Title="Travel Request Listing" Language="C#" MasterPageFile="~/Master/EmployeeExt.Master"
    AutoEventWireup="true" CodeBehind="TravelRequestListing.aspx.cs" Inherits="Web.Appraisal.TravelRequestListing" %>

<%--<%@ Register Src="~/NewHR/UserControls/EmployeeDetailsCtl.ascx" TagName="EmployeeDetailsCtl"
    TagPrefix="uc1" %>--%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">


        
        var CommandHandler1 = function(command, record){
            <%= hiddenValue.ClientID %>.setValue(record.data.RequestID);
                if(command=="Edit")
                {
                    <%= btnEditLevel.ClientID %>.fireEvent('click');
                }
                if(command=="Delete")
                {
                    <%= btnDelete.ClientID %>.fireEvent('click');
                }
             }
             


             var totalCost = function (records) {
            var i = 0,
                length = records.length,
                total = 0,
                record;

            for (; i < length; ++i) {
                record = records[i];
                total += record.get('Quantity') * record.get('Rate');
            }

            return total;
        };
        
    </script>
    <style>
        .bodyContainer
        {
            background-color: #E8F1FF;
            padding: 20px 0 20px 30px;
            margin: 20px;
            border: 1px solid #BFD9FB;
            border-radius: 3px;
            width: 975px;
            display: block;
            float: left;
        }
        table#albums
        {
            border-collapse: separate;
            border-spacing: 0 10px;
        }
        
        table#albums1
        {
            border-collapse: separate;
            border-spacing: 0 40px;
        }
        
        
        .grids
        {
            padding: 0 0 0 0;
            border: 1px solid silver;
            border-radius: 2px;
            width: 1156px;
            margin-bottom: 5px;
            height: auto;
        }
        #lblAttachment
        {
            border: 2px solid #ccc;
            padding: 5px 10px;
            background: #eee;
            margin: 5px;
            float: none;
        }
        .bodypart
        {
            margin: 0 auto !important;
        }
        p
        {
            margin: 10px 0;
        }
        .x-btn-default-small
        {
            background-color: #f5f5f5;
            border-color: #e1e1e1;
            background-image: -webkit-linear-gradient(top,#f6f6f6,#f5f5f5 50%,#e8e8e8 51%,#f5f5f5);
        }
        .x-btn-default-small .x-btn-inner
        {
            color: #666;
        }
        .windowContentWrapper td
        {
            vertical-align: middle;
            margin-right: 0;
            padding: 10px 10px 10px 0;
        }
        .windowContentWrapper table
        {
        }
        .x-window-body-default
        {
            background: #E8F1FF;
        }
        .windowContentWrapper
        {
            margin: 0;
            padding: 5px;
        }
        .bolds
        {
            padding: 0 100px 0 0;
        }
        .bolds .x-label-value
        {
            color: Gray;
            font-size: large;
            font-weight: bold;
        }
        .boldLabels
        {
            font-weight: bold;
            font-size: 15px;
        }
        .darkOrange
        {
            margin-left: 5px;
            color: #ff8c00;
        }
    </style>
    <style type="text/css">
        .innerLR
        {
            padding: 0px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
    <%--<ext:ResourceManager ID="resourceManager1" runat="server" />--%>
    <div class="contentArea">
        <ext:Hidden runat="server" ID="hiddenValue" />
        <ext:Hidden runat="server" ID="HiddenStatus">
        </ext:Hidden>
        <ext:LinkButton runat="server" Hidden="true" ID="btnEditLevel">
            <DirectEvents>
                <Click OnEvent="btnEditLevel_Click">
                    <EventMask ShowMask="true" />
                </Click>
            </DirectEvents>
        </ext:LinkButton>
        <ext:LinkButton runat="server" Hidden="true" ID="btnDelete">
            <DirectEvents>
                <Click OnEvent="btnDelete_Click">
                    <Confirmation Message="Are you sure, you want to delete the Travel Request?" ConfirmRequest="true" />
                    <EventMask ShowMask="true" />
                </Click>
            </DirectEvents>
        </ext:LinkButton>
        <h4 class="heading">
            Your Travel Requests</h4>
        <ext:GridPanel StyleSpec="margin-top:15px;" ID="GridLevels" runat="server">
            <Store>
                <ext:Store ID="Store3" runat="server">
                    <Model>
                        <ext:Model ID="Model4" runat="server" IDProperty="RequestID">
                            <Fields>
                                <ext:ModelField Name="RequestID" Type="String" />
                                <ext:ModelField Name="EmployeeId" Type="string" />
                                <ext:ModelField Name="FromDate" Type="Date" />
                                <ext:ModelField Name="EmpName" Type="string" />
                                <ext:ModelField Name="Position" Type="string" />
                                <ext:ModelField Name="Department" Type="string" />
                                <ext:ModelField Name="Destination" Type="string" />
                                <ext:ModelField Name="Reason" Type="string" />
                                <ext:ModelField Name="Duration" Type="string" />
                                <ext:ModelField Name="Status" Type="string" />
                                <ext:ModelField Name="StatusName" Type="string" />
                                <ext:ModelField Name="StatusModified" Type="string" />
                                <ext:ModelField Name="ExpensePaidByText" Type="string" />
                                <ext:ModelField Name="TravelByText" Type="string" />
                                <ext:ModelField Name="ToDate" Type="Date" />
                                <ext:ModelField Name="Total" Type="string" />
                                <ext:ModelField Name="LocationName" Type="string" />
                                <ext:ModelField Name="NextApprovalSupervisor" Type="string" />
                            </Fields>
                        </ext:Model>
                    </Model>
                </ext:Store>
            </Store>
            <ColumnModel>
                <Columns>
                    <ext:DateColumn ID="DateColumn1" runat="server" Text="From Date" Sortable="true"
                        DataIndex="FromDate" Width="86" Align="Left" Format="MM/dd/yyyy">
                    </ext:DateColumn>
                    <ext:DateColumn ID="DateColumn2" runat="server" Text="To Date" Sortable="true" DataIndex="ToDate"
                        Width="86" Align="Left" Format="MM/dd/yyyy">
                    </ext:DateColumn>
                    <ext:Column ID="ColumnDestination" Sortable="false" MenuDisabled="true" runat="server" Text="Destination"
                        Width="120" Align="Left" DataIndex="Destination" />
                    <ext:Column ID="Column11" Flex="1" Sortable="false" MenuDisabled="true" runat="server" Text="Location"
                        Width="110" Align="Left" DataIndex="LocationName" />
                    <ext:Column ID="Column6" Sortable="false" MenuDisabled="true" runat="server" Text="Reason"
                        Width="200" Align="Left" DataIndex="Reason" />
                    <ext:Column ID="Column7" Sortable="false" MenuDisabled="true" runat="server" Text="Duration"
                        Width="115" Align="Left" DataIndex="Duration" />
                    <ext:Column ID="Column1" Sortable="false" MenuDisabled="true" runat="server" Text="Paid by"
                        Width="100" Align="Left" DataIndex="ExpensePaidByText" />
                    <%--    <ext:Column ID="Column2" Sortable="false" MenuDisabled="true" runat="server" Text="Travel By"
                            Width="90" Align="Left" DataIndex="TravelByText" />--%>
                    <ext:Column ID="ColumnTotalAmount" Sortable="false" MenuDisabled="true" runat="server" Text="Allowance"
                        Width="80" Align="Right" DataIndex="Total">
                        <Renderer Fn="getFormattedAmount" />
                    </ext:Column>
                    <ext:Column ID="Column8" Sortable="false" MenuDisabled="true" runat="server" Text="Status"
                        Width="90" Align="Left" DataIndex="StatusName">
                    </ext:Column>
                    <ext:Column ID="Column4" Sortable="false" MenuDisabled="true" runat="server" Text="Awaiting Approval"
                        Width="120" Align="Left" DataIndex="NextApprovalSupervisor" />
                    <ext:CommandColumn runat="server" Text="" Align="Center" Width="80">
                        <Commands>
                            <ext:CommandSeparator />
                            <ext:GridCommand Cls="editGridButton" ToolTip-Text="Edit" Text="<i class='fa fa-pencil'></i>"
                                CommandName="Edit" />
                            <ext:GridCommand Cls="editGridButton" ToolTip-Text="Edit" Text="<i class='fa fa-trash-o'></i>"
                                CommandName="Delete" />
                        </Commands>
                        <Listeners>
                            <Command Handler="CommandHandler1(command,record);" />
                        </Listeners>
                    </ext:CommandColumn>
                </Columns>
            </ColumnModel>
            <SelectionModel>
                <ext:RowSelectionModel ID="RowSelectionModel2" runat="server" Mode="Single" />
            </SelectionModel>
        </ext:GridPanel>
        <%--<div class="buttonBlock">
                    <ext:LinkButton runat="server" StyleSpec="padding:0px;" ID="btnAddLevel" Cls="btnFlat"
                        BaseCls="btnFlat" Text="<i></i>Add New Allowance Settings">
                        <DirectEvents>
                            <Click OnEvent="btnAddLevel_Click">
                                <EventMask ShowMask="true" />
                            </Click>
                        </DirectEvents>
                    </ext:LinkButton>
                </div>--%>
    </div>
</asp:Content>
