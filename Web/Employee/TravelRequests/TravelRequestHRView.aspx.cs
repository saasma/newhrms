﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using System.Xml;
using Utils;
using System.IO;

namespace Web.Appraisal
{
    public partial class TravelRequestHRView : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();
            }
        }

        public void Initialise()
        {

            if (CommonManager.SettingTA.IsLocationEditable != null && CommonManager.SettingTA.IsLocationEditable.Value)
            {
                ColumnDestination.Hide();
            }

            LoadLevels();
        }

        public void btnReload_Click(object sender, DirectEventArgs e)
        {
            LoadLevels();
        }

        private void LoadLevels()
        {
            //HardCoded Date Just incase we need date in future. Just pass the desired date to the function and Date filter works fine.
            GridLevels.GetStore().DataSource = TravelAllowanceManager.GetAllTravelRequestForHR(null,null);
            GridLevels.GetStore().DataBind();

        }
        

        
        public void ClearLevelFields()
        {
            hiddenValue.Text = "";
            //txtName.Text = "";
            //txtDescription.Text = "";
        }

      
        
        protected void btnAddLevel_Click(object sender, DirectEventArgs e)
        {
            
            ClearLevelFields();
            //WindowLevel.Show();
        }


        protected void btnDeleteLevel_Click(object sender, DirectEventArgs e)
        {
            int TAllowance = int.Parse(hiddenValue.Text.Trim());
            Status status = TravelAllowanceManager.DeleteTravelAllowance(TAllowance);
            if (status.IsSuccess)
            {
                LoadLevels();
                NewMessage.ShowNormalMessage("Travel Allowance deleted.");
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }
        }

        

        protected void btnEditLevel_Click(object sender, DirectEventArgs e)
        {

            TARequest request = new TARequest();
            request = TravelAllowanceManager.getRequestByID(int.Parse(hiddenValue.Text));

            TravelRequestCtl1.LoadTravelRequest(request);

        }

       
        protected void btnDetailLevel_Click(object sender, DirectEventArgs e)
        {
            
        }


        //protected void btnDetailSaveUpdate_Click(object sender, DirectEventArgs e)
        //{
        //    List<TAAllowanceRate> lines = new List<TAAllowanceRate>();
        //    string json = e.ExtraParams["Values"];

        //    if (string.IsNullOrEmpty(json))
        //    {
        //        return;
        //    }


        //    lines = JSON.Deserialize<List<TAAllowanceRate>>(json);
        
        //    Status status = TravelAllowanceManager.InsertUpdateAllowanceDetail(lines);
        //    if (status.IsSuccess)
        //    {
        //        Window1.Hide();
        //        LoadLevels();
        //        NewMessage.ShowNormalMessage("Rating Scale saved.");
        //    }
        //    else
        //    {
        //        NewMessage.ShowWarningMessage(status.ErrorMessage);
        //    }


        //}


       

 
    }
}