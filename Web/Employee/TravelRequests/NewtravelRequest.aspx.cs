﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using System.Text;
using BLL.Manager;
using Utils.Helper;
using DAL;
using System.Drawing;
using BLL.Base;
using Ext.Net;
using Utils;
using System.IO;
using BLL.BO;
using Web.CP.Report.Templates.HR;


namespace Web.CP
{
   
    public partial class NewtravelRequest : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack && !X.IsAjaxRequest)
            {
                Initialise();
            }
        }


        void Initialise()
        {

            // show/hide for location editable ccase
            if (CommonManager.SettingTA.IsLocationEditable != null && CommonManager.SettingTA.IsLocationEditable.Value)
            {
                cmbLocations.Hide();
                rowDomestic.Style["display"] = "";
               
                rowEditableLocation.Style["display"] = "";
                rowSingleLocationCountry.Style["display"] = "none";
                txtPlaceToTravel.Hide();
                cmbCountry.Hide();

                txtRequestedAdvanceAmount.Hide();

                LoadAllowanceGrid(0);
                ColumnQuantity.Hide();
                ColumnTotal.Hide();
                ColumnRate.Text = "Amount";

                storeCountry.DataSource = CommonManager.GetCountryNames();
                storeCountry.DataBind();

                reqdCountry.Enabled = false;
                reqdLocations.Enabled = false;
                reqdPlaceToTravel.Enabled = false;

            }

            CommonManager comManager = new CommonManager();
            cmbCountry.Store[0].DataSource = comManager.GetAllCountries();
            cmbCountry.Store[0].DataBind();

            cmbLocations.Store[0].DataSource = TravelAllowanceManager.GetLocationList().OrderBy(x => x.LocationName).ToList();
            cmbLocations.Store[0].DataBind();

            cmbTravelBy.Store[0].DataSource = TravelAllowanceManager.getTravelByList();
            cmbTravelBy.Store[0].DataBind();

            cmbExpensePaidBy.Store[0].DataSource = TravelAllowanceManager.getExpensePaidByList();
            cmbExpensePaidBy.Store[0].DataBind();

            cmbCountry.SelectedItem.Value = "524";
            //cmbLocations.SelectedItem.Index = 0;
            if (TravelAllowanceManager.GetLocationList().Count > 0)
                LoadAllowanceGrid(TravelAllowanceManager.GetLocationList().OrderBy(x => x.LocationName).FirstOrDefault().LocationId);
            LoadEmployeeInfo();

            string msgType = Request.QueryString["type"];
            if (!string.IsNullOrEmpty(msgType))
            {
                if (msgType == "1")
                    SetMessage(lblMsg, "Travel request has been saved.");
                else if (msgType == "2")
                    SetMessage(lblMsg, "Travel request has been saved & forwarded.");
            }


            // Load Forward to list
            List<TextValue> approvalList = TravelAllowanceManager.GetForwardToListForTravelOrder((int)FlowStepEnum.Saved,
                SessionManager.CurrentLoggedInEmployeeId);
            cmbForwardList.Store[0].DataSource = approvalList;
            cmbForwardList.Store[0].DataBind();
            if (approvalList.Count == 1)
                ExtControlHelper.ComboBoxSetSelected(approvalList[0].ID.ToString(), cmbForwardList);
            
            if (!string.IsNullOrEmpty(Request.QueryString["ID"]))
            {
                Hidden_QuerryStringID.Text = Request.QueryString["ID"];
                LoadEditData(approvalList);

                btnPrint.Show();
            }
        }

        private void LoadEditData(List<TextValue> approvalList)
        {


            TARequest loadRequest = new TARequest();
            loadRequest = TravelAllowanceManager.getTARequestByID(int.Parse(Hidden_QuerryStringID.Text));
            txtPlaceToTravel.Text = loadRequest.PlaceOfTravel;
            //cmbCountry.SelectedItem.Value = loadRequest.CountryId.Value.ToString();
            if (loadRequest.CountryId != null)
                ExtControlHelper.ComboBoxSetSelected(loadRequest.CountryId.Value, cmbCountry);
            //cmbCountry.SetValue(loadRequest.CountryId.ToString());


            cmbCountry.Disable(true);
            cmbLocations.Disable(true);

            if (loadRequest.Status == (int)FlowStepEnum.Saved)// || loadRequest.Status == (int)FlowStepEnum.Step1SaveAndSend)
            { }
            else
            {
                SetWarning(lblMsg, "Forwarded travel request can not be changed.");
                btnSaveAndLater.Hide();
                btnSaveAndSend.Hide();
            }
            if (loadRequest.Status == (int)FlowStepEnum.Step1SaveAndSend)
                btnSaveAndLater.Hide();


            if (loadRequest.ExtendArrivalDate != null)
            {
                extensionDateValue.SelectedDate = loadRequest.ExtendArrivalDate.Value;
                extensionReasonValue.Text = loadRequest.ExtendReason;

                extensionDateValue.Show();
                extensionReasonValue.Show();
            }

            txtPurposeOfTravel.Text = loadRequest.PurposeOfTravel;
            txtFromDate.Text = loadRequest.TravellingFromEng.Value.ToShortDateString();
            txtToDate.Text = loadRequest.TravellingToEng.Value.ToShortDateString();

            //cmbTravelBy.SelectedItem.Value = loadRequest.TravelBy.Value.ToString();
            ExtControlHelper.ComboBoxSetSelected(loadRequest.TravelBy.Value.ToString(), cmbTravelBy);

            if (loadRequest.TravelByText.Equals("Other"))
            {
                txtOtherName.Text = loadRequest.OtherTravelByName == null ? "" : loadRequest.OtherTravelByName;
                txtOtherName.Show();
            }
            //cmbTravelBy.SetValue(loadRequest.TravelBy.Value.ToString());
            //cmbExpensePaidBy.SelectedItem.Value = loadRequest.ExpensePaidBy.Value.ToString();
            ExtControlHelper.ComboBoxSetSelected(loadRequest.ExpensePaidBy.Value.ToString(), cmbExpensePaidBy);
            if (loadRequest.LocationId != null)
                ExtControlHelper.ComboBoxSetSelected(loadRequest.LocationId.Value, cmbLocations);
            //cmbExpensePaidBy.SetValue(loadRequest.ExpensePaidBy.Value.ToString());

            if (loadRequest.IsDomestic != null)
            {
                if (loadRequest.IsDomestic == false)
                {
                    radioDomestic.Checked = false;
                    radioInternaltional.Checked = true;
                }
            }

            gridLocations.Store[0].DataSource = loadRequest.TARequestLocations.OrderBy(x => x.LineID).ToList();
            gridLocations.Store[0].DataBind();

            if (!string.IsNullOrEmpty(loadRequest.Number))
            {
                txtRefNumber.Text = loadRequest.Number;
                rowRefNumber.Style["display"] = "";

            }


            if (loadRequest.RequestedAdvance != null)
                txtRequestedAdvanceAmount.Text = GetCurrency(loadRequest.RequestedAdvance);

            dispTotal.Text = GetCurrency(loadRequest.Total);

            txtDayCount.Text = (Math.Abs((loadRequest.TravellingFromEng.Value - loadRequest.TravellingToEng.Value).TotalDays) + 1).ToString();
            txtNightCount.Text = (Math.Abs((loadRequest.TravellingFromEng.Value - loadRequest.TravellingToEng.Value).TotalDays)).ToString();

            if (loadRequest.LocationId != null)
            {
                LoadAllowanceGrid(loadRequest.LocationId.Value);
            }
            else
                LoadAllowanceGrid(0);

            // Load Forward To
            TARequestStatusHistory firstSelection =
                loadRequest.TARequestStatusHistories.OrderBy(x => x.ApprovedOn).FirstOrDefault();
            if (firstSelection != null && firstSelection.ApprovalEmployeeId != null)
            {
                if (approvalList.Any(x => x.ID == firstSelection.ApprovalEmployeeId.Value) == false)
                {
                    approvalList.Add(new TextValue
                    {
                        ID = firstSelection.ApprovalEmployeeId.Value,
                        Text
                            = EmployeeManager.GetEmployeeName(firstSelection.ApprovalEmployeeId.Value)
                    });
                    cmbForwardList.Store[0].DataSource = approvalList;
                    cmbForwardList.Store[0].DataBind();


                }

                ExtControlHelper.ComboBoxSetSelected(firstSelection.ApprovalEmployeeId.ToString(), cmbForwardList);
            }
            else if (loadRequest.CurrentApprovalSelectedEmployeeId != null)
                ExtControlHelper.ComboBoxSetSelected(loadRequest.CurrentApprovalSelectedEmployeeId.ToString(), cmbForwardList);
        }

        protected void cmbLocations_Change(object sender, DirectEventArgs e)
        {

            string isRateEditable = "false";
            string LocationID = cmbLocations.SelectedItem.Value;
            //isRateEditable = AllowanceManager.GetLocationByID(LocationID);
            

            List<TARequestLine> lines = new List<TARequestLine>();
            string json = e.ExtraParams["Values"];
            if (!string.IsNullOrEmpty(json))
            {

                lines = JSON.Deserialize<List<TARequestLine>>(json);

                foreach (TARequestLine line in lines)
                {

                    TAAllowanceRate rate = TravelAllowanceManager.GetRate(SessionManager.CurrentLoggedInEmployeeId,
                        int.Parse(cmbLocations.SelectedItem.Value),
                         line.AllowanceId.Value);

                    if (rate != null && rate.Rate != null)
                        line.Rate = rate.Rate;
                    else
                        line.Rate = 0;

                    //line.Total = Convert.ToDecimal(line.Quantity) * Convert.ToDecimal(line.Rate);
                }

            }


            storeAllowances.DataSource = lines;
            storeAllowances.DataBind();
            //LoadAllowanceGrid(int.Parse(cmbLocations.SelectedItem.Value));
        }

        protected void CheckInOutSelectChange(object sender, DirectEventArgs e)
        {
            string actualInDate = "";
            string actualOutDate = "";
            if (txtFromDate.SelectedDate.Year != 0001)
                actualInDate = txtFromDate.Text;
            if (txtToDate.SelectedDate.Year != 0001)
                actualOutDate = txtToDate.Text;
            DateTime englishInDate;
            DateTime englishOutDate;
            if (!string.IsNullOrEmpty(actualInDate) && !string.IsNullOrEmpty(actualOutDate))
            {
                englishInDate = DateTime.Parse(actualInDate);
                englishOutDate = DateTime.Parse(actualOutDate);

                txtDayCount.Text = (Math.Abs((englishInDate - englishOutDate).TotalDays) + 1).ToString();
                txtNightCount.Text = (Math.Abs((englishInDate - englishOutDate).TotalDays)).ToString();

                if ((englishInDate - englishOutDate).TotalDays == 0)
                {
                    txtDayCount.Text = "1";
                    txtNightCount.Text = "1";
                }

            }
            else
            {
                txtDayCount.Text = "0";
                txtNightCount.Text = "0";
            }
        }

        protected void LoadAllowanceGrid(int locationId)
        {
            List<TARequestLine> lines = new List<TARequestLine>();
            int? RequestID = null;
            if (!string.IsNullOrEmpty(Hidden_QuerryStringID.Text))
            {
                RequestID = int.Parse(Hidden_QuerryStringID.Text);
            }
            lines = TravelAllowanceManager.getAllowanceByEmployeeID(SessionManager.CurrentLoggedInEmployeeId, RequestID, locationId).Where(x => x.UnitType == null).ToList();
         
            storeAllowances.DataSource = lines;
            storeAllowances.DataBind();

        }


        protected void LoadEmployeeInfo()
        {
            int EmployeeID = SessionManager.CurrentLoggedInEmployeeId;

            EEmployee emp = EmployeeManager.GetEmployeeById(EmployeeID);

            bool hasPhoto = false;
            if (emp.HHumanResources.Count > 0)
            {
                if (!string.IsNullOrEmpty(emp.HHumanResources[0].UrlPhotoThumbnail))
                {
                    image.ImageUrl = Page.ResolveUrl(Config.UploadLocation + "/" + emp.HHumanResources[0].UrlPhotoThumbnail);
                    if (File.Exists(Server.MapPath(image.ImageUrl)))
                        hasPhoto = true;
                }
            }
            if (!hasPhoto)
            {
                image.ImageUrl = Page.ResolveUrl(Web.Helper.WebHelper.GetEmployeeNoImage(emp.EmployeeId));

            }

            lblName.Text = emp.Title + " " + emp.Name;
            lblBranch.Text = emp.Branch.Name;
            lblDepartment.Text = emp.Department.Name;
        }


        protected void ButtonNext_Click(object sender, DirectEventArgs e)
        {

            Page.Validate("InsertUpdate");
            if (Page.IsValid)
            {
                Ext.Net.Button btn = (Ext.Net.Button)sender;
                TARequest request = new TARequest();
                TARequestStatusHistory hist = new TARequestStatusHistory();
                List<TARequestLine> lines = new List<TARequestLine>();

                bool isEdit;
                isEdit = !string.IsNullOrEmpty(Hidden_QuerryStringID.Text);
                if (isEdit)
                {
                    request.RequestID = int.Parse(Hidden_QuerryStringID.Text);
                }
                request.Status = 0;
                hist.Status = 0;
                if (sender == btnSaveAndLater)
                {
                    request.StatusName = "Saved";
                }
                else if (sender == btnSaveAndSend)
                {
                    request.Status = (int)FlowStepEnum.Step1SaveAndSend;
                    hist.Status = (int)FlowStepEnum.Step1SaveAndSend;


                }

                if (!string.IsNullOrEmpty(txtDayCount.Text))
                {
                    request.Days = int.Parse(txtDayCount.Text);
                }
                if (!string.IsNullOrEmpty(txtNightCount.Text))
                {
                    request.Night = int.Parse(txtNightCount.Text);
                }

                request.PlaceOfTravel = txtPlaceToTravel.Text;

                if (CommonManager.SettingTA.IsLocationEditable != null && CommonManager.SettingTA.IsLocationEditable.Value)
                {
                    request.IsDomestic = (radioDomestic.Checked ? true : false);
                    string json1 = e.ExtraParams["Locations"];
                    if (string.IsNullOrEmpty(json1))
                    {
                        NewMessage.ShowWarningMessage("Place need to be added.");
                        return;
                    }

                    List<TARequestLocation> list = JSON.Deserialize<List<TARequestLocation>>(json1);
                    List<TARequestLocation> newList = new List<TARequestLocation>();

                    foreach (var item in list)
                    {
                        if (string.IsNullOrEmpty(item.LocationPlace) && string.IsNullOrEmpty(item.Country))
                            continue;
                        if (!string.IsNullOrEmpty(item.LocationPlace) && string.IsNullOrEmpty(item.Country))
                        {
                            NewMessage.ShowWarningMessage("Country is required.");
                            return;
                        }
                        if (string.IsNullOrEmpty(item.LocationPlace) && !string.IsNullOrEmpty(item.Country))
                        {
                            NewMessage.ShowWarningMessage("Location is required.");
                            return;
                        }
                        if(radioDomestic.Checked && !item.Country.ToLower().Equals("Nepal".ToLower()))
                        {
                            NewMessage.ShowWarningMessage("For domestic, all country must be Nepal.");
                            return;
                        }
                        
                        if (string.IsNullOrEmpty(request.LocationName))
                            request.LocationName = item.LocationPlace;
                        else
                            request.LocationName += ", " + item.LocationPlace;

                        newList.Add(item);
                    }

                    request.TARequestLocations.AddRange(newList);
                    if (request.TARequestLocations.Count == 0)
                    {
                        NewMessage.ShowWarningMessage("Location is empty.");
                        return;
                    }
                }
                else
                {
                    request.CountryId = int.Parse(cmbCountry.SelectedItem.Value);
                    request.CountryName = cmbCountry.SelectedItem.Text;
                    request.LocationId = int.Parse(cmbLocations.SelectedItem.Value);
                }

                string json = e.ExtraParams["Values"];
                if (string.IsNullOrEmpty(json))
                {
                    return;
                }

                lines = JSON.Deserialize<List<TARequestLine>>(json);
                foreach (TARequestLine line in lines)
                {
                    line.Total = Convert.ToDecimal(line.Quantity) * Convert.ToDecimal(line.Rate);
                }
                request.Total = lines.Sum(x => x.Total).Value;

                //if (request.Total == 0)
                //{
                //    NewMessage.ShowWarningMessage("Total amount can not be zero.");
                //    return;
                //}

                request.PurposeOfTravel = txtPurposeOfTravel.Text;
                request.TravellingFromEng = DateTime.Parse(txtFromDate.Text);
                request.TravellingToEng = DateTime.Parse(txtToDate.Text);
                request.TravelBy = int.Parse(cmbTravelBy.SelectedItem.Value);
                request.TravelByText = cmbTravelBy.SelectedItem.Text;

                if (request.TravelByText.Equals("Other") && string.IsNullOrEmpty(txtOtherName.Text.Trim()))
                {
                    NewMessage.ShowWarningMessage("Other travel by name is required.");
                    return;
                }
                request.OtherTravelByName = txtOtherName.Text.Trim();

                request.ExpensePaidBy = int.Parse(cmbExpensePaidBy.SelectedItem.Value);
                request.ExpensePaidByText = cmbExpensePaidBy.SelectedItem.Text;
                request.EmployeeId = SessionManager.CurrentLoggedInEmployeeId;
                request.CurrentApprovalSelectedEmployeeId = int.Parse(cmbForwardList.SelectedItem.Value);

                if (!string.IsNullOrEmpty(txtRequestedAdvanceAmount.Text.Trim()))
                    request.RequestedAdvance = decimal.Parse(txtRequestedAdvanceAmount.Text.Trim());

                
                

                Status status = TravelAllowanceManager.InsertUpdateTravelRequests(request, lines, hist, false);
                if (status.IsSuccess)
                {


                    if (request.Status != (int)FlowStepEnum.Saved)
                    {
                        btnSaveAndLater.Hide();
                        btnSaveAndSend.Hide();
                    }

                    if (!isEdit)
                    {
                        if (request.Status == (int)FlowStepEnum.Saved)
                        {
                            Response.Redirect("NewtravelRequest.aspx?type=1&id=" + request.RequestID);
                        }
                        else
                        {
                            Response.Redirect("NewtravelRequest.aspx?type=2&id=" + request.RequestID);
                        }
                    }
                    else
                    {
                        if (request.Status == (int)FlowStepEnum.Saved)
                            SetMessage(lblMsg, "Travel request has been updated.");
                        else
                            SetMessage(lblMsg, "Travel request has been updated & forwarded.");
                    }
                }
                else
                {
                    //SetWarning(lblMsg, status.ErrorMessage);
                    NewMessage.ShowWarningMessage(status.ErrorMessage);
                }

            }



        }

        protected void btnPrint_Click(object sender, EventArgs e)
        {
            CP.Report.ReportHelper.TravelRequestPrint(int.Parse(Hidden_QuerryStringID.Text), txtFromDate.SelectedDate.ToShortDateString(), txtToDate.SelectedDate.ToShortDateString());
        }      
       


    }
}




