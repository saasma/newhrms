﻿<%@ Page Title="Travel Request Approval" Language="C#" MasterPageFile="~/Master/EmployeeExt.Master"
    AutoEventWireup="true" CodeBehind="TravelRequestHRView.aspx.cs" Inherits="Web.Appraisal.TravelRequestHRView" %>

<%@ Register Src="~/Employee/UserControls/TravelRequestCtl.ascx" TagName="TravelRequestCtl"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">


        
        var CommandHandler1 = function(command, record){
            <%= hiddenValue.ClientID %>.setValue(record.data.RequestID);
                if(command=="Edit")
                {
                    <%= btnEditLevel.ClientID %>.fireEvent('click');
                }
             }
             

        function searchList()
        {
            <%=btnReload.ClientID %>.fireEvent('click');
        }

        var totalCost = function (records) {
            var i = 0,
                length = records.length,
                total = 0,
                record;

            for (; i < length; ++i) {
                record = records[i];
                total += record.get('Quantity') * record.get('Rate');
            }

            return total;
        };
        
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
    <div class="contentArea">
        <ext:Hidden runat="server" ID="hiddenValue" />
        <ext:Hidden runat="server" ID="Hidden_QuerryStringID">
        </ext:Hidden>
        <ext:LinkButton runat="server" Hidden="true" ID="btnReload">
            <DirectEvents>
                <Click OnEvent="btnReload_Click">
                    <EventMask ShowMask="true" />
                </Click>
            </DirectEvents>
        </ext:LinkButton>
        <ext:LinkButton runat="server" Hidden="true" ID="btnEditLevel">
            <DirectEvents>
                <Click OnEvent="btnEditLevel_Click">
                    <EventMask ShowMask="true" />
                </Click>
            </DirectEvents>
        </ext:LinkButton>
        <ext:LinkButton runat="server" Hidden="true" ID="btnDetailLevel">
            <DirectEvents>
                <Click OnEvent="btnDetailLevel_Click">
                    <EventMask ShowMask="true" />
                </Click>
            </DirectEvents>
        </ext:LinkButton>
        <ext:Button ID="btnDeleteLevel" runat="server" Hidden="true">
            <DirectEvents>
                <Click OnEvent="btnDeleteLevel_Click">
                    <Confirmation ConfirmRequest="true" Message="Are you sure, you want to delete the Allowance settings?" />
                    <EventMask ShowMask="true" />
                </Click>
            </DirectEvents>
        </ext:Button>
        <h4 class="heading">
            Travel Request Approvals</h4>
        <ext:GridPanel StyleSpec="margin-top:15px;" ID="GridLevels" runat="server" Scroll="Both"
            OverflowX="auto" >
            <Store>
                <ext:Store ID="Store3" runat="server">
                    <Model>
                        <ext:Model ID="Model4" runat="server" IDProperty="RequestID">
                            <Fields>
                                <ext:ModelField Name="RequestID" Type="String" />
                                <ext:ModelField Name="EmployeeId" Type="string" />
                                <ext:ModelField Name="FromDate" Type="Date" />
                                <ext:ModelField Name="EmpName" Type="string" />
                                <ext:ModelField Name="Position" Type="string" />
                                <ext:ModelField Name="Department" Type="string" />
                                <ext:ModelField Name="Destination" Type="string" />
                                <ext:ModelField Name="Reason" Type="string" />
                                <ext:ModelField Name="Duration" Type="string" />
                                <ext:ModelField Name="Status" Type="string" />
                                <ext:ModelField Name="StatusName" Type="string" />
                                <ext:ModelField Name="LocationName" Type="string" />
                                <ext:ModelField Name="Total" Type="string" />
                            </Fields>
                        </ext:Model>
                    </Model>
                </ext:Store>
            </Store>
            <ColumnModel>
                <Columns>
                    <ext:DateColumn ID="DateColumn1" runat="server" Text="Date" Sortable="true" DataIndex="FromDate"
                        Width="85" Align="Left" Format="MM/dd/yyyy">
                    </ext:DateColumn>
                    <ext:Column ID="Column10" Sortable="false" MenuDisabled="true" runat="server" Text="Employee Name"
                        Width="150" Align="Left" DataIndex="EmpName" />
                    <ext:Column ID="Column3" Sortable="false" MenuDisabled="true" runat="server" Text="Position"
                        Width="100" Align="Left" DataIndex="Position" />
                    <%--<ext:Column ID="Column4" Sortable="false" MenuDisabled="true" runat="server" Text="Department"
                        Width="100" Align="Left" DataIndex="Department" />--%>
                    <ext:Column ID="ColumnDestination" Sortable="false" MenuDisabled="true" runat="server" Text="Destination"
                        Width="120" Align="Left" DataIndex="Destination" />
                    <ext:Column ID="Column11" Flex="1" Sortable="false" MenuDisabled="true" runat="server" Text="Location"
                        Width="150" Align="Left" DataIndex="LocationName" />
                    <ext:Column ID="Column6" Sortable="false" MenuDisabled="true" runat="server" Text="Reason for Travel"
                        Width="180" Align="Left" DataIndex="Reason" />
                    <ext:Column ID="Column7" Sortable="false" MenuDisabled="true" runat="server" Text="Duration"
                        Width="125" Align="Left" DataIndex="Duration" />
                    <ext:Column ID="ColumnTotalAmount" Sortable="false" MenuDisabled="true" runat="server" Text="Total Allowance"
                        Width="100" Align="Right" DataIndex="Total">
                        <Renderer Fn="getFormattedAmount" />
                    </ext:Column>
                    <ext:Column ID="Column8" Sortable="false" MenuDisabled="true" runat="server" Text="Status"
                        Width="100" Align="Left" DataIndex="StatusName">
                    </ext:Column>
                    <ext:CommandColumn runat="server" Align="Center" Width="80">
                        <Commands>
                            <ext:CommandSeparator />
                            <ext:GridCommand Cls="editGridButton" ToolTip-Text="Review" Text="Review"
                                CommandName="Edit" />
                        </Commands>
                        <Listeners>
                            <Command Handler="CommandHandler1(command,record);" />
                        </Listeners>
                    </ext:CommandColumn>
                </Columns>
            </ColumnModel>
            <SelectionModel>
                <ext:RowSelectionModel ID="RowSelectionModel2" runat="server" Mode="Single" />
            </SelectionModel>
        </ext:GridPanel>
    </div>
    <uc1:TravelRequestCtl Id="TravelRequestCtl1" runat="server" />
</asp:Content>
