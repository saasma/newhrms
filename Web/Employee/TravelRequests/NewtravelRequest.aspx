﻿<%@ Page Title="Travel Request" Language="C#" MasterPageFile="~/Master/EmployeeExt.Master"
    AutoEventWireup="true" CodeBehind="NewtravelRequest.aspx.cs" Inherits="Web.CP.NewtravelRequest" %>

<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/PagingCtl.ascx" TagName="PagingCtl" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        var totalCost = function (records) {
            var i = 0,
                length = records.length,
                total = 0,
                record;

            for (; i < length; ++i) {
                record = records[i];
                total += record.get('Quantity') * record.get('Rate');
            }

            <%=dispTotal.ClientID %>.setValue(total);

            return total;
        };
     
        var afterEdit = function()
        {
            var records = <%=gridAllowances.ClientID %>.getStore();
             var i = 0,
                length = records.data.length,
                total = 0,
                record;

            for (; i < length; ++i) {
                record = records.data.items[i];
                total += record.data.Quantity * record.data.Rate;
            }

            total = getFormattedAmount(total);
            <%=dispTotal.ClientID %>.setValue(total);
        }
        var qtyChange = function(e1,value,e3,e4)
        {
              var records = <%=gridAllowances.ClientID %>.getStore();
             var i = 0,
                length = records.data.length,
                total = 0,
                record;

            for (; i < length; ++i) {
                record = records.data.items[i];
                //if(record.data.Quantity == 0)
                    record.data.Quantity = value;
                record.commit();
            }

            afterEdit();
        }


        var beforeEdit = function(e1, e) {
            
            if(e.field=="Rate" && e.record.data.IsEditable!="true")
            {
                e.cancel = true;
                return;
            }

            // allow to edit last row allowance name only
            if(e.field=="AllowanceName" )
            {
                if(e.record.data.IsEditable=='false')
                {
                    e.cancel = true;
                    return;
                }
                
                if(e.record.store.data.items[e.record.store.data.items.length-1].data.AllowanceId   
                    == e.record.data.AllowanceId){}
                else
                {                
                    e.cancel = true;
                    return;
                }
            }
            
        
    };

     var addNewRow = function (grid) {
            var newRow = new Object();

            if(<%=radioDomestic.ClientID %>.getValue() == true)
                newRow.Country="Nepal";

            var rowIndex = grid.getStore().data.items.length;

            //gridProjectSplitter.stopEditing();
            grid.getStore().insert(rowIndex, newRow);
        }

        var RemoveItemLine = function (column, command, record, recordIndex, cellIndex) {
            var store = this.grid.store;
            store.remove(record);
        };


    </script>
    <style type="text/css">
        .bodypart
        {
            margin: 0 auto !important;
        }
        .tableTD > tbody > tr > td
        {
            padding-top: 15px;
        }
    </style>
      <script type="text/javascript" src="<%= ResolveUrl("~/Employee/override.js?v=") + Web.Helper.WebHelper.Version %>"></script>
  
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
    <ext:Hidden runat="server" ID="Hidden_QuerryStringID">
    </ext:Hidden>
    <ext:Hidden runat="server" ID="Hidden_IsRateEditable" Text="false">
    </ext:Hidden>
    <div class="contentArea">
        <h3 style="margin-top: 0px">
            New Travel Request</h3>
        <ext:Label ID="lblMsg" runat="server" />
        <div style="float: left; width: 200px;">
            <table>
                <tr>
                    <td>
                        <asp:Image ID="image" ImageUrl="~/images/sample.jpg" runat="server" Width="150px"
                            Height="150px" />
                    </td>
                </tr>
                <tr>
                    <td style="padding-top: 10px;">
                        <ext:Label ID="lblName" StyleSpec="font-weight:bold;color:#47759E;font-size:16px;"
                            runat="server">
                        </ext:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <ext:Label ID="lblBranch" runat="server">
                        </ext:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <ext:Label ID="lblDepartment" runat="server">
                        </ext:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <ext:Label ID="lblDesignation" runat="server">
                        </ext:Label>
                    </td>
                </tr>
            </table>
        </div>
        <ext:Store runat="server" ID="storeCountry" >
            <Model>
                <ext:Model ID="Model2"  runat="server">
                    <Fields>
                        <ext:ModelField Name="Country" />
                    </Fields>
                </ext:Model>
            </Model>
        </ext:Store>
        <div style="float: left; background-color: #D2E0FD; padding: 20px; padding-top: 5px;">
            <div style="float: right; width: 90px; margin-top: 10px;">
                <ext:LinkButton ID="btnPrint" runat="server" Text="Print" OnClick="btnPrint_Click"
                    Hidden="true" Icon="Printer" AutoPostBack="true">
                </ext:LinkButton>
            </div>
            <table class="tableTD">
                <tr style='display:none' id="rowRefNumber" runat="server">
                    <td colspan="1" style="margin-right: 25px; margin-left: 25px;">
                         <ext:TextField ID="txtRefNumber" Disabled="true" Width="200" runat="server" FieldLabel="Ref #"
                            LabelSeparator=" " LabelAlign="Top">
                        </ext:TextField>
                    </td>
                   
                </tr>
                <tr style='display:none' id="rowDomestic" runat="server">
                    <td colspan="1" style="margin-right: 25px; margin-left: 25px;">
                        <h4>Places to Travel</h4>
                        <ext:Radio Name="domestic" runat="server" Checked="true" ID="radioDomestic" BoxLabel="Domestic" />
                    </td>
                    <td colspan="1" style="padding-left: 25px; padding-top: 55px;">
                        <ext:Radio Name="domestic" runat="server" ID="radioInternaltional" BoxLabel="International" />
                    </td>
                </tr>
                 <tr style='display:none' id="rowEditableLocation" runat="server">
                    <td colspan="5">
                        <ext:GridPanel StyleSpec="margin-bottom:10px" ID="gridLocations" runat="server"
                        Cls="itemgrid">
                        <Store>
                            <ext:Store ID="store4" runat="server">
                                <Model>
                                    <ext:Model ID="Model1" Name="SettingModel" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="LocationPlace" Type="String" />
                                            <ext:ModelField Name="Country" Type="String" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <Plugins>
                            <ext:CellEditing ID="CellEditing2" ClicksToEdit="1" runat="server">
                            </ext:CellEditing>
                        </Plugins>                       
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn1" DataIndex="SequenceNo" runat="server"
                                    Width="35" />
                                <ext:Column ID="Column3" Flex="1" runat="server" Text="Place" Sortable="false" MenuDisabled="true"
                                    Width="200" DataIndex="LocationPlace" Border="false">
                                    <Editor>
                                        <ext:TextField ID="TextField1" runat="server" />
                                    </Editor>
                                </ext:Column>
                                <ext:Column ID="colTravelOrder" runat="server" Text="Country" Sortable="false" MenuDisabled="true"
                                    Width="200" DataIndex="Country" Border="false">
                                   
                                    <Editor>
                                        <ext:ComboBox DisplayField="Country" QueryMode="Local" ForceSelection="true" ValueField="Country"
                                            StoreID="storeCountry" ID="cmbCountryLocations" runat="server">
                                        </ext:ComboBox>
                                    </Editor>
                                </ext:Column>
                                
                                <ext:CommandColumn ID="CommandColumn1" runat="server" Sortable="false" MenuDisabled="true"
                                    Width="50" Align="Center">
                                    <Commands>
                                        <ext:GridCommand Icon="Delete" CommandName="Delete" Cls="deleteicon" />
                                    </Commands>
                                    <Listeners>
                                        <Command Fn="RemoveItemLine">
                                        </Command>
                                    </Listeners>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <SelectionModel>
                            <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" Mode="Single" />
                        </SelectionModel>
                    </ext:GridPanel>
                    <ext:Button Cls="btn btn-primary" ID="btnAddNewLineToMain" runat="server" Icon="Add"
                        Height="26" Text="Add New Line">
                        <Listeners>
                            <Click Handler="addNewRow(#{gridLocations});" />
                        </Listeners>
                    </ext:Button>
                    </td>
                </tr>
                <tr style='' id="rowSingleLocationCountry" runat="server">
                    <td colspan="1" style="margin-right: 25px; margin-left: 25px;">
                        <ext:TextField ID="txtPlaceToTravel" Width="200" runat="server" FieldLabel="Place to Travel *"
                            LabelSeparator=" " LabelAlign="Top">
                        </ext:TextField>
                    </td>
                    <td colspan="1" style="padding-left: 25px; padding-top: 15px;">
                        <ext:ComboBox ID="cmbCountry" ForceSelection="true" runat="server" Width="200" FieldLabel="Country *"
                            LabelAlign="Top" LabelSeparator=" " QueryMode="Local" DisplayField="CountryName"
                            ValueField="CountryId">
                            <Store>
                                <ext:Store ID="store2"  runat="server">
                                    <Fields>
                                        <ext:ModelField Name="CountryId" Type="String" />
                                        <ext:ModelField Name="CountryName" />
                                    </Fields>
                                </ext:Store>
                            </Store>
                           
                        </ext:ComboBox>
                    </td>
                </tr>
                <tr>
                    <td colspan="5">
                        <ext:TextArea ID="txtPurposeOfTravel" Width="540" runat="server" FieldLabel="Purpose of Travel *"
                            LabelSeparator=" " LabelAlign="Top">
                        </ext:TextArea>
                    </td>
                </tr>
                <tr>
                    <td colspan="1">
                        <ext:DateField FieldLabel="Travelling from *" Width="150" ID="txtFromDate" runat="server"
                            EmptyDate="" LabelAlign="Top" LabelSeparator=" ">
                            <DirectEvents>
                                <Change OnEvent="CheckInOutSelectChange">
                                </Change>
                            </DirectEvents>
                            <Plugins>
                                        <ext:GenericPlugin ID="HighlightDates1" runat="server" InstanceName="Ext.ux.DatePicker.HighlightWithNepDates" />
                                    </Plugins>
                        </ext:DateField>
                    </td>
                    <td colspan="1">
                        <ext:DateField FieldLabel="Travelling to *" Width="150" ID="txtToDate" runat="server"
                            LabelAlign="Top" EmptyDate="" LabelSeparator=" ">
                            <DirectEvents>
                                <Change OnEvent="CheckInOutSelectChange">
                                </Change>
                            </DirectEvents>
                            <Plugins>
                                        <ext:GenericPlugin ID="GenericPlugin1" runat="server" InstanceName="Ext.ux.DatePicker.HighlightWithNepDates" />
                                    </Plugins>
                        </ext:DateField>
                    </td>
                   
                    <td colspan="1" style="padding-right: 7px;">
                        <ext:NumberField ID="txtDayCount" runat="server" Width="60" FieldLabel="Days" LabelAlign="Top"
                            LabelSeparator=" ">
                            <%--<Listeners>
                                <Change Fn="qtyChange">
                                </Change>
                            </Listeners>--%>
                        </ext:NumberField>
                    </td>
                    <td colspan="1">
                        <ext:NumberField ID="txtNightCount" runat="server" Width="60" FieldLabel="Night"
                            LabelSeparator=" " LabelAlign="Top">
                        </ext:NumberField>
                    </td>
                </tr>
                <tr>
                    <td colspan="1">
                         <ext:ComboBox ForceSelection="true"  ID="cmbExpensePaidBy" runat="server" FieldLabel="Expense Paid by *"
                            LabelAlign="Top" LabelSeparator=" " QueryMode="Local" DisplayField="Name" ValueField="Value">
                            <Store>
                                <ext:Store ID="storeExpensePaidBy" runat="server">
                                    <Fields>
                                        <ext:ModelField Name="Name" Type="String" />
                                        <ext:ModelField Name="Value" />
                                    </Fields>
                                </ext:Store>
                            </Store>
                        </ext:ComboBox>
                    </td>
                     <td colspan="1">
                       <ext:ComboBox ForceSelection="true" ID="cmbTravelBy" runat="server" FieldLabel="Travel by *"
                            LabelAlign="Top" LabelSeparator=" " QueryMode="Local" DisplayField="Name" ValueField="Value">
                            <Store>
                                <ext:Store ID="storeTravelBy" runat="server">
                                    <Fields>
                                        <ext:ModelField Name="Name" />
                                        <ext:ModelField Name="Value" Type="String" />
                                    </Fields>
                                </ext:Store>
                            </Store>
                            <Listeners>
                                <Select Handler="if(this.getValue()==='5') {#{txtOtherName}.show();} else {#{txtOtherName}.hide();}" />
                            </Listeners>
                        </ext:ComboBox>
                    </td>
                    <td colspan="2">
                        <ext:TextField ID="txtOtherName" Width="120"  Hidden="true" runat="server" FieldLabel="Other Travel by"
                            LabelAlign="Top" LabelSeparator="">
                        </ext:TextField>
                    </td>
                </tr>
                <tr>
                     <td>
                        <ext:TextField ID="txtRequestedAdvanceAmount" runat="server" Width="150" FieldLabel="Advance Request"
                            LabelAlign="Top" LabelSeparator="">
                        </ext:TextField>
                    </td>
                    <td>
                        <ext:ComboBox ID="cmbLocations" ForceSelection="true" runat="server" Width="150"
                            FieldLabel="Location for Allowance *" LabelAlign="Top" LabelSeparator=" " QueryMode="Local"
                            DisplayField="LocationName" ValueField="LocationId">
                            <Store>
                                <ext:Store ID="store1" runat="server">
                                    <Fields>
                                        <ext:ModelField Name="LocationId" Type="String" />
                                        <ext:ModelField Name="LocationName" />
                                    </Fields>
                                </ext:Store>
                            </Store>
                            <DirectEvents>
                                <Select OnEvent="cmbLocations_Change">
                                    <EventMask ShowMask="true" />
                                    <ExtraParams>
                                        <ext:Parameter Name="Values" Value="Ext.encode(#{gridAllowances}.getRowsValues({selectedOnly : false}))"
                                            Mode="Raw" />
                                    </ExtraParams>
                                </Select>
                            </DirectEvents>
                        </ext:ComboBox>
                    </td>
                   
                </tr>
                <tr runat="server" id="rowGridAllowances">
                    <td colspan="5">
                        <ext:GridPanel StyleSpec="margin-top:15px;" ID="gridAllowances" runat="server" Width="540"
                            Cls="itemgrid">
                            <Store>
                                <ext:Store ID="storeAllowances" runat="server">
                                    <Model>
                                        <ext:Model ID="Model4" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="LineId" Type="String" />
                                                <ext:ModelField Name="RequestId" Type="string" />
                                                <ext:ModelField Name="AllowanceId" Type="string" />
                                                <ext:ModelField Name="AllowanceName" Type="string" />
                                                <ext:ModelField Name="Quantity" Type="Float" />
                                                <ext:ModelField Name="Rate" Type="Float" />
                                                <ext:ModelField Name="Total" Type="Float" />
                                                <ext:ModelField Name="IsEditable" Type="String" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                            <Plugins>
                                <ext:CellEditing ID="CellEditing1" ClicksToEdit="1" runat="server">
                                    <Listeners>
                                        <Edit Fn="afterEdit" />
                                    </Listeners>
                                </ext:CellEditing>
                            </Plugins>
                            <ColumnModel>
                                <Columns>
                                    <ext:Column ID="Column5" Flex="1" Sortable="false" MenuDisabled="true" runat="server" Text="Allowances"
                                        Align="Left" DataIndex="AllowanceName" Width="200">
                                          <Editor>
                                            <ext:TextField ID="NumberField1" SelectOnFocus="true" runat="server" AllowBlank="false">
                                            </ext:TextField>
                                        </Editor>
                                        </ext:Column>
                                    <ext:Column ID="ColumnQuantity" Sortable="false" MenuDisabled="true" runat="server" Text="Qty"
                                        Align="Center" DataIndex="Quantity" Width="100">
                                        <Editor>
                                            <ext:NumberField ID="txtQTY" SelectOnFocus="true" runat="server" AllowBlank="false">
                                            </ext:NumberField>
                                        </Editor>
                                    </ext:Column>
                                    <ext:Column ID="ColumnRate" Sortable="false" MenuDisabled="true" runat="server" Text="Rate"
                                        Align="Right" DataIndex="Rate" Width="125">
                                        <Editor>
                                            <ext:NumberField ID="txtRate" SelectOnFocus="true" runat="server" AllowBlank="false">
                                            </ext:NumberField>
                                        </Editor>
                                    </ext:Column>
                                    <ext:Column runat="server" Align="Right" Width="125" ID="ColumnTotal" Text="Total" Sortable="false"
                                        Groupable="false" DataIndex="Total" CustomSummaryType="totalCost">
                                        <Renderer Handler="return Ext.util.Format.number((record.data.Quantity * record.data.Rate),'0,000.00');" />
                                    </ext:Column>
                                </Columns>
                            </ColumnModel>
                            <SelectionModel>
                                <ext:RowSelectionModel ID="RowSelectionModel2" runat="server" Mode="Single" />
                            </SelectionModel>
                            <Listeners>
                                <BeforeEdit Fn="beforeEdit" />
                            </Listeners>
                        </ext:GridPanel>
                    </td>
                </tr>
                <tr>
                    <td colspan="5" style='padding-left: 335px'>
                        <ext:DisplayField FieldStyle="text-align:right" Width="200px" Text="0" LabelSeparator=""
                            FieldLabel="Total" ID="dispTotal" runat="server" />
                    </td>
                </tr>
              
                <tr>
                    <td colspan="5">
                        <table>
                            <tr>
                                <td>
                                    <ext:ComboBox ForceSelection="true" ID="cmbForwardList" runat="server" FieldLabel="Forward To *"
                                        LabelAlign="Top" Width="200" LabelSeparator=" " QueryMode="Local" DisplayField="Text"
                                        ValueField="ID">
                                        <Store>
                                            <ext:Store ID="store3" runat="server">
                                                <Fields>
                                                    <ext:ModelField Name="Text" />
                                                    <ext:ModelField Name="ID" Type="String" />
                                                </Fields>
                                            </ext:Store>
                                        </Store>
                                    </ext:ComboBox>
                                </td>
                                <td style="vertical-align: bottom; padding-left: 20px">
                                    <ext:Button ID="btnSaveAndLater" Width="150" Height="30"  runat="server" Text="&nbsp;&nbsp;Save and Finish Later&nbsp;&nbsp;"
                                        ValidationGroup="InsertUpdate">
                                        <DirectEvents>
                                            <Click OnEvent="ButtonNext_Click">
                                                <EventMask ShowMask="true" />
                                                <ExtraParams>
                                                    <ext:Parameter Name="Values" Value="Ext.encode(#{gridAllowances}.getRowsValues({selectedOnly : false}))"
                                                        Mode="Raw" />
                                                    <ext:Parameter Name="Locations" Value="Ext.encode(#{gridLocations}.getRowsValues({selectedOnly : false}))"
                                                        Mode="Raw" />
                                                </ExtraParams>
                                            </Click>
                                        </DirectEvents>
                                        <Listeners>
                                            <Click Handler="valGroup = 'InsertUpdate'; return CheckValidation();">
                                            </Click>
                                        </Listeners>
                                    </ext:Button>
                                </td>
                                <td style="vertical-align: bottom; padding-left: 20px">
                                    <ext:Button ID="btnSaveAndSend" Width="150" Height="30" runat="server" Text="&nbsp;&nbsp;Save and Forward &nbsp;&nbsp;"
                                        ValidationGroup="InsertUpdate">
                                        <DirectEvents>
                                            <Click OnEvent="ButtonNext_Click">
                                                <Confirmation ConfirmRequest="true" Message="Are you sure, you want to save and forward the Travel Request, after forward you can not make any changes?" />
                                                <EventMask ShowMask="true" />
                                                <ExtraParams>
                                                    <ext:Parameter Name="Values" Value="Ext.encode(#{gridAllowances}.getRowsValues({selectedOnly : false}))"
                                                        Mode="Raw" />
                                                      <ext:Parameter Name="Locations" Value="Ext.encode(#{gridLocations}.getRowsValues({selectedOnly : false}))"
                                                        Mode="Raw" />
                                                        
                                                </ExtraParams>
                                            </Click>
                                        </DirectEvents>
                                        <Listeners>
                                            <Click Handler="valGroup = 'InsertUpdate'; return CheckValidation();">
                                            </Click>
                                        </Listeners>
                                    </ext:Button>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <table style='    margin-top: 15px;'>
                <tr>
                    <td>
                        <ext:DateField Disabled="true" Hidden="true" FieldLabel="Extended date" Width="150" ID="extensionDateValue"
                            runat="server" LabelAlign="Top" EmptyDate="" LabelSeparator="">
                        </ext:DateField>
                    </td>
                    <td style='padding-left: 10px;'>
                        <ext:TextField Disabled="true" Hidden="true" ID="extensionReasonValue" LabelSeparator="" FieldLabel="Reason for extension"
                            LabelAlign="Top" runat="server" Width="400px">
                        </ext:TextField>
                    </td>
                </tr>
            </table>
        </div>
        <asp:RequiredFieldValidator ID="reqdPlaceToTravel" runat="server" ControlToValidate="txtPlaceToTravel"
            ErrorMessage="Place to travel is required." Display="None" ValidationGroup="InsertUpdate">
        </asp:RequiredFieldValidator>
         <asp:CompareValidator ID="RequiredFieldValidator101" runat="server" ControlToValidate="txtRequestedAdvanceAmount"
            ErrorMessage="Invalid advance amount." Type="Currency" Operator="DataTypeCheck"  Display="None" ValidationGroup="InsertUpdate">
        </asp:CompareValidator>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="cmbForwardList"
            ErrorMessage="Forward to is required." Display="None" ValidationGroup="InsertUpdate">
        </asp:RequiredFieldValidator>
        <asp:RequiredFieldValidator ID="reqdCountry" runat="server" ControlToValidate="cmbCountry"
            ErrorMessage="Country is required." Display="None" ValidationGroup="InsertUpdate">
        </asp:RequiredFieldValidator>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtPurposeOfTravel"
            ErrorMessage="Purpose of Travel is required." Display="None" ValidationGroup="InsertUpdate">
        </asp:RequiredFieldValidator>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtFromDate"
            ErrorMessage="From Date is required." Display="None" ValidationGroup="InsertUpdate">
        </asp:RequiredFieldValidator>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="txtToDate"
            ErrorMessage="To Date is required." Display="None" ValidationGroup="InsertUpdate">
        </asp:RequiredFieldValidator>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="cmbTravelBy"
            ErrorMessage="Travel by is required." Display="None" ValidationGroup="InsertUpdate">
        </asp:RequiredFieldValidator>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ControlToValidate="cmbExpensePaidBy"
            ErrorMessage="Expense Paid by is required." Display="None" ValidationGroup="InsertUpdate">
        </asp:RequiredFieldValidator>
        <asp:RequiredFieldValidator ID="reqdLocations" runat="server" ControlToValidate="cmbLocations"
            ErrorMessage="Location is required." Display="None" ValidationGroup="InsertUpdate">
        </asp:RequiredFieldValidator>
    </div>
</asp:Content>
