﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using System.Xml;

namespace Web.Appraisal
{
    public partial class TravelAdvanceSettlement : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();
            }
        }

        public void Initialise()
        {

            cmbLocations.Store[0].DataSource = TravelAllowanceManager.GetLocationList().OrderBy(x => x.LocationName).ToList();
            cmbLocations.Store[0].DataBind();

            CommonManager comManager = new CommonManager();
            cmbCountry.Store[0].DataSource = comManager.GetAllCountries();
            cmbCountry.Store[0].DataBind();

            cmbTravelBy.Store[0].DataSource = TravelAllowanceManager.getTravelByList();
            cmbTravelBy.Store[0].DataBind();

            cmbExpensePaidBy.Store[0].DataSource = TravelAllowanceManager.getExpensePaidByList();
            cmbExpensePaidBy.Store[0].DataBind();

            //LoadLevels();
            X.Js.Call("searchList");
        }
        
        private void LoadLevels()
        {
            List<GetTravelRequestsByUserResult> list = TravelAllowanceManager.GetAllTravelRequestListingByUser
                (SessionManager.CurrentLoggedInEmployeeId, null, null, -1)
                .Where(x => x.AdvanceStatus != null).ToList();

            foreach (GetTravelRequestsByUserResult item in list)
            {
                item.StatusName = ((AdvanceStatusEnum)item.AdvanceStatus).ToString();
            }

            GridLevels.GetStore().DataSource = list;

            GridLevels.GetStore().DataBind();

        }


        
        public void ClearLevelFields()
        {
            hiddenValue.Text = "";
            //txtName.Text = "";
            //txtDescription.Text = "";
        }

      
        
        protected void btnAddLevel_Click(object sender, DirectEventArgs e)
        {
            
            ClearLevelFields();
            //WindowLevel.Show();
        }


        protected void btnDeleteLevel_Click(object sender, DirectEventArgs e)
        {
            int TAllowance = int.Parse(hiddenValue.Text.Trim());
            Status status = TravelAllowanceManager.DeleteTravelAllowance(TAllowance);
            if (status.IsSuccess)
            {
                //LoadLevels();
                X.Js.Call("searchList");
                NewMessage.ShowNormalMessage("Travel Allowance deleted.");
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }
        }

        

        protected void btnEditLevel_Click(object sender, DirectEventArgs e)
        {

            TARequest request = new TARequest();
            request = TravelAllowanceManager.getRequestByID(int.Parse(hiddenValue.Text));
            LoadEmployeeInfo(request.EmployeeId.Value);
            LoadEditData(request);
            LoadAllowanceGrid(request.LocationId.Value, request.RequestID, request);
            btnPrint.Show();
            Window1.Center();
            Window1.Show();

        }

        protected void LoadAllowanceGrid(int LocationID, int? RequestID, TARequest request)
        {
            if (request.AdvanceStatus == (int)AdvanceStatusEnum.HRAdvance)
            {
                List<TARequestLine> lines = new List<TARequestLine>();
                lines = TravelAllowanceManager.getAllowanceByEmployeeID(SessionManager.CurrentLoggedInEmployeeId, RequestID, LocationID);
                storeAllowances.DataSource = lines;
                storeAllowances.DataBind();


                foreach (var line in lines)
                {
                    line.FromDate = txtFromDate.SelectedDate.ToShortDateString();
                    line.ToDate = txtToDate.SelectedDate.ToShortDateString();

                    line.HRTotal = 0;
                }
                decimal total = lines.Sum(x => x.Total).Value;
                dispTotal1.Text = GetCurrency(total.ToString());
                dispTotal.Text = GetCurrency(total.ToString());
                dispFinalHRTotal.Text = GetCurrency(request.SettledTotal == null ? 0 : request.SettledTotal.Value);
                storeSettlement.DataSource = lines;
                storeSettlement.DataBind();
            }
            else
            {
                List<TARequestLine> lines = new List<TARequestLine>();
                lines = TravelAllowanceManager.getAllowanceByEmployeeID(SessionManager.CurrentLoggedInEmployeeId, RequestID, LocationID);
                storeAllowances.DataSource = lines;
                storeAllowances.DataBind();

                decimal toatal1 = lines.Sum(x => x.Total).Value;
                dispTotal1.Text = GetCurrency(lines.Sum(x=>(decimal)x.Quantity.Value * x.Rate.Value));

                foreach (var line in lines)
                {
                    line.FromDate = txtFromDate.SelectedDate.ToShortDateString();
                    line.ToDate = txtToDate.SelectedDate.ToShortDateString();
                    line.Quantity = line.SettQuantity;
                    line.Total = line.SettTotal;

                    line.HRTotal = line.FinalTotal == null ? 0 : line.FinalTotal.Value;

                }
                decimal total = lines.Sum(x => x.Total).Value;
                dispFinalHRTotal.Text = GetCurrency(request.SettledTotal == null ? 0 : request.SettledTotal.Value);
                dispTotal.Text = GetCurrency(total);

                storeSettlement.DataSource = lines;
                storeSettlement.DataBind();
            }

        }





        private void LoadEditData(TARequest loadRequest)
        {
            txtAdvanceAmount.Text = GetCurrency(loadRequest.Advance);

            if (loadRequest.AdvanceStatus == (int)AdvanceStatusEnum.HRAdvance)
            {
                dispFinalHRTotal.Hide();
                btnSaveAndLater.Show();
                Column17.Hide();
            }
            else
            {
                dispFinalHRTotal.Show();
                btnSaveAndLater.Hide();
                Column17.Show();
            }

            if (loadRequest.AdvanceStatus == (int)AdvanceStatusEnum.HRAdvance)
            {

                txtPlaceToTravel.Text = loadRequest.PlaceOfTravel;

                cmbCountry.SetValue(loadRequest.CountryId.ToString());
                cmbLocations.SetValue(loadRequest.LocationId.Value.ToString());
                LoadAllowanceGrid(loadRequest.LocationId.Value);

                txtPurposeOfTravel.Text = loadRequest.PurposeOfTravel;
                txtFromDate.Text = loadRequest.TravellingFromEng.Value.ToShortDateString();
                txtToDate.Text = loadRequest.TravellingToEng.Value.ToShortDateString();

                cmbTravelBy.SetValue(loadRequest.TravelBy.Value.ToString());


                cmbExpensePaidBy.SetValue(loadRequest.ExpensePaidBy.Value.ToString());

                txtDayCount.Text = loadRequest.Days.ToString();
                txtNightCount.Text = loadRequest.Night.ToString();
                
            }
            else
            {


                txtPlaceToTravel.Text = loadRequest.PlaceOfTravel;

                cmbCountry.SetValue(loadRequest.CountryId.ToString());
                cmbLocations.SetValue(loadRequest.LocationId.Value.ToString());
                LoadAllowanceGrid(loadRequest.LocationId.Value);

                txtPurposeOfTravel.Text = loadRequest.PurposeOfTravel;
                txtFromDate.Text = loadRequest.SettTravellingFromEng.Value.ToShortDateString();
                txtToDate.Text = loadRequest.SettTravellingToEng.Value.ToShortDateString();

                cmbTravelBy.SetValue(loadRequest.SettTravelBy.Value.ToString());


                cmbExpensePaidBy.SetValue(loadRequest.SettExpensePaidBy.Value.ToString());

                txtDayCount.Text = loadRequest.SettDays.ToString();
                txtNightCount.Text = loadRequest.SettNight.ToString();

            }


        }

        protected void LoadEmployeeInfo(int EmployeeID)
        {
            EEmployee eemployee = EmployeeManager.GetEmployeeById(EmployeeID);
            lblName.Text = eemployee.Name;
            lblDepartment.Text = eemployee.Department.Name;
            lblDesignation.Text = eemployee.EDesignation.Name;
            lblBranch.Text = eemployee.Branch.Name;
            //List<TARequestStatusHistory> statusHistory = TravelAllowanceManager.getTravelRequestForwardHistory(hiddenValue.Text);
            
            //string body = "";
            //foreach (var status in statusHistory)
            //{
            //    body = body + "<tr><td  colspan='2' style='font-weight:bold'>" + status.whoseComment + "</td></tr><tr><td colspan='2' style='color:blue'>" + status.ApprovalRemarks + "</td></tr><tr><td>" + status.ApprovedByName + "</td><td>" + status.ApprovedOn.Value.ToShortDateString() + "</td></tr>";
            //}
            //body = "<table>" + body + "</table>";
            //txtCommentSection.Html = body;

        }


        protected void Country_Change(object sender, DirectEventArgs e)
        {
            //LoadAllowanceGrid(int.Parse(cmbCountry.SelectedItem.Value));
        }


        protected void CheckInOutSelectChange(object sender, DirectEventArgs e)
        {
            string actualInDate = "";
            string actualOutDate = "";
            if (txtFromDate.Text != "0001-01-01 12:00:00 AM")
                actualInDate = txtFromDate.Text;
            if (txtToDate.Text != "0001-01-01 12:00:00 AM")
                actualOutDate = txtToDate.Text;
            DateTime englishInDate;
            DateTime englishOutDate;
            if (!string.IsNullOrEmpty(actualInDate) && !string.IsNullOrEmpty(actualOutDate))
            {
                englishInDate = DateTime.Parse(actualInDate);
                englishOutDate = DateTime.Parse(actualOutDate);

                txtDayCount.Text = (Math.Abs((englishInDate - englishOutDate).TotalDays) + 1).ToString();
                txtNightCount.Text = (Math.Abs((englishInDate - englishOutDate).TotalDays)).ToString();

                if ((englishInDate - englishOutDate).TotalDays == 0)
                {
                    txtDayCount.Text = "1";
                    txtNightCount.Text = "1";
                }

            }
            else
            {
                txtDayCount.Text = "0";
                txtNightCount.Text = "0";
            }
        }


        protected void LoadAllowanceGrid(int LocationId)
        {
            List<TARequestLine> lines = new List<TARequestLine>();
            int? RequestID = null;
            if (!string.IsNullOrEmpty(hiddenValue.Text))
            {
                RequestID = int.Parse(hiddenValue.Text);
            }
            lines = TravelAllowanceManager.getAllowanceByEmployeeID(SessionManager.CurrentLoggedInEmployeeId, RequestID, LocationId);
            storeAllowances.DataSource = lines;
            storeAllowances.DataBind();

        }

        protected void btnDetailLevel_Click(object sender, DirectEventArgs e)
        {
            
        }


        protected void btnDetailSaveUpdate_Click(object sender, DirectEventArgs e)
        {
            List<TAAllowanceRate> lines = new List<TAAllowanceRate>();
            string json = e.ExtraParams["Values"];

            if (string.IsNullOrEmpty(json))
            {
                return;
            }


            lines = JSON.Deserialize<List<TAAllowanceRate>>(json);
            int TAllowance = int.Parse(hiddenValue.Text.Trim());

            Status status = TravelAllowanceManager.InsertUpdateAllowanceDetail(lines, TAllowance);
            if (status.IsSuccess)
            {
                Window1.Hide();
                //LoadLevels();
                X.Js.Call("searchList");
                NewMessage.ShowNormalMessage("Rating Scale saved.");
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }


        }


        protected void ButtonNext_Click(object sender, DirectEventArgs e)
        {

            TARequest request = new TARequest();
            TARequestStatusHistory hist = new TARequestStatusHistory();
            bool isEdit;
            bool isStatusIncrease=true;

            isEdit = string.IsNullOrEmpty(hiddenValue.Text);
            if (!isEdit)
            {
                request.RequestID = int.Parse(hiddenValue.Text);
            }

            if (!string.IsNullOrEmpty(txtDayCount.Text))
            {
                request.SettDays = int.Parse(txtDayCount.Text);
            }
            if (!string.IsNullOrEmpty(txtNightCount.Text))
            {
                request.SettNight = int.Parse(txtNightCount.Text);
            }

            //if(!string.IsNullOrEmpty(dispTotal.Text))
            //    request.ClaimedTotal = decimal.Parse(dispTotal.Text);

            //hist.ApprovalRemarks = txtHRComment.Text;
            request.PlaceOfTravel = txtPlaceToTravel.Text;
            request.CountryId = int.Parse(cmbCountry.SelectedItem.Value);
            request.LocationId = int.Parse(cmbLocations.SelectedItem.Value);

            request.CountryName = cmbCountry.SelectedItem.Text;
            request.PurposeOfTravel = txtPurposeOfTravel.Text;
            request.SettTravellingFromEng = DateTime.Parse(txtFromDate.Text);
            request.SettTravellingToEng = DateTime.Parse(txtToDate.Text);
            request.SettTravelBy = int.Parse(cmbTravelBy.SelectedItem.Value);
            request.SettTravelByText = cmbTravelBy.SelectedItem.Text;
            request.SettExpensePaidBy = int.Parse(cmbExpensePaidBy.SelectedItem.Value);
            request.SettExpensePaidByText = cmbExpensePaidBy.SelectedItem.Text;
            request.EmployeeId = SessionManager.CurrentLoggedInEmployeeId;
            request.AdvanceStatus =  (int)AdvanceStatusEnum.EmployeeSettle;

            List<TARequestLine> lines = new List<TARequestLine>();
            string json = e.ExtraParams["AllowanceValues"];
            if (string.IsNullOrEmpty(json))
            {
                return;
            }


            List<TARequestLine> linesOrig = new List<TARequestLine>();
            string jsonOrig = e.ExtraParams["AllowanceValuesOrig"];
            if (string.IsNullOrEmpty(jsonOrig))
            {
                return;
            }

            linesOrig = JSON.Deserialize<List<TARequestLine>>(jsonOrig);
            lines = JSON.Deserialize<List<TARequestLine>>(json);


            foreach (var values1 in lines)
            {
                values1.SettQuantity = values1.Quantity;
                values1.SettTotal = (decimal)values1.SettQuantity.Value * values1.Rate.Value;

                int index = 0;
                index = lines.IndexOf(values1);

                lines[index].Quantity = linesOrig[index].Quantity;
                lines[index].Total = linesOrig[index].Total;
            }


            request.ClaimedTotal = lines.Sum(x => x.SettTotal);

            Status status = TravelAllowanceManager.SettleTravelRequests(request, lines, hist, isStatusIncrease);
            if (status.IsSuccess)
            {

                NewMessage.ShowNormalMessage("Final Allowance forwarded for settlement.");
                Window1.Close();
                //LoadLevels();
                X.Js.Call("searchList");
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }

            
        }

        protected void cmbLocations_Change(object sender, DirectEventArgs e)
        {

            List<TARequestLine> lines = new List<TARequestLine>();
            string json = e.ExtraParams["Values"];
            if (!string.IsNullOrEmpty(json))
            {

                lines = JSON.Deserialize<List<TARequestLine>>(json);

                foreach (TARequestLine line in lines)
                {

                    TAAllowanceRate rate = TravelAllowanceManager.GetRate(SessionManager.CurrentLoggedInEmployeeId,
                        int.Parse(cmbLocations.SelectedItem.Value),
                         line.AllowanceId.Value);

                    if (rate != null && rate.Rate != null)
                        line.Rate = rate.Rate;
                    else
                        line.Rate = 0;

                    //line.Total = Convert.ToDecimal(line.Quantity) * Convert.ToDecimal(line.Rate);
                }

            }


            storeAllowances.DataSource = lines;
            storeAllowances.DataBind();
            //LoadAllowanceGrid(int.Parse(cmbLocations.SelectedItem.Value));
        }


        protected void Store_ReadData(object sender, StoreReadDataEventArgs e)
        {
            int totalRecords = 0;
            int type = 1;

            if (!string.IsNullOrEmpty(cmbType.SelectedItem.Value))
                type = int.Parse(cmbType.SelectedItem.Value);

            DateTime? startDate = null, endDate = null;
            if (type == -1)
            {
                if (txtFromDateFilter.SelectedDate == new DateTime())
                {
                    NewMessage.ShowWarningMessage("Please select from date.");
                    txtFromDateFilter.Focus();
                    return;
                }

                if (txtToDateFilter.SelectedDate == new DateTime())
                {
                    NewMessage.ShowWarningMessage("Please select to date.");
                    txtToDateFilter.Focus();
                    return;
                }

                startDate = txtFromDateFilter.SelectedDate;
                endDate = txtToDateFilter.SelectedDate;
            }

            List<GetTravelRequestsByUserResult> list = TravelAllowanceManager.GetAllTravelRequestListingByUserForAS(e.Start, int.Parse(cmbPageSize.SelectedItem.Value), SessionManager.CurrentLoggedInEmployeeId, startDate, endDate, type, out totalRecords).ToList();

            foreach (GetTravelRequestsByUserResult item in list)
            {
                item.StatusName = ((AdvanceStatusEnum)item.AdvanceStatus).ToString();
            }

            e.Total = totalRecords;
            storeTR.DataSource = list;
            storeTR.DataBind();

        }

        public void btnExportTR_Click(object sender, EventArgs e)
        {
            int totalRecords = 0;
            int type = 1;

            if (!string.IsNullOrEmpty(cmbType.SelectedItem.Value))
                type = int.Parse(cmbType.SelectedItem.Value);

            DateTime? startDate = null, endDate = null;
            if (type == -1)
            {
                if (txtFromDateFilter.SelectedDate == new DateTime())
                {
                    NewMessage.ShowWarningMessage("Please select from date.");
                    txtFromDateFilter.Focus();
                    return;
                }

                if (txtToDateFilter.SelectedDate == new DateTime())
                {
                    NewMessage.ShowWarningMessage("Please select to date.");
                    txtToDateFilter.Focus();
                    return;
                }

                startDate = txtFromDateFilter.SelectedDate;
                endDate = txtToDateFilter.SelectedDate;
            }

            List<GetTravelRequestsByUserResult> list = TravelAllowanceManager.GetAllTravelRequestListingByUserForAS(0, 999999, SessionManager.CurrentLoggedInEmployeeId, startDate, endDate, type, out totalRecords).ToList();
            foreach (var item in list)
            {
                item.FromDateEngFormatted = item.FromDate.Value.ToShortDateString();
                item.ToDateEngFormatted = item.ToDate.Value.ToShortDateString();
            }

            Bll.ExcelHelper.ExportToExcel("Travel Request Settlement Report", list,
                new List<String>() { "RequestID", "EmployeeId", "FromDate", "ToDate", "EmpName", "ExpensePaidByText", "TravelByText", "Status", "StatusModified", "AdvanceAmount", "AdvDate", "AdvanceStatus", "Total", "LocationId" },
            new List<String>() { },
            new Dictionary<string, string>() { { "FromDateEngFormatted", "From Date" }, { "ToDateEngFormatted", "To Date" }, { "LocationName", "Location" }, { "StatusName", "Status" } },
            new List<string>() { }
            , new Dictionary<string, string>() { }
            , new List<string> { "FromDateEngFormatted", "ToDateEngFormatted", "Position", "Department", "Destination", "LocationName", "Reason", "Duration", "StatusName" }); 



        }

        protected void btnPrint_Click(object sender, EventArgs e)
        {
            CP.Report.ReportHelper.TravelRequestPrint(int.Parse(hiddenValue.Text), txtFromDate.SelectedDate.ToShortDateString(), txtToDate.SelectedDate.ToShortDateString());
        } 

 
    }
}
