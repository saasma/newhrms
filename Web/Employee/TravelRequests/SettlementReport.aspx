﻿<%@ Page Title="Travel Request Approval" Language="C#" MasterPageFile="~/Master/EmployeeExt.Master"
    AutoEventWireup="true" CodeBehind="SettlementReport.aspx.cs" Inherits="Web.Appraisal.SettlementReport" %>

<%--<%@ Register Src="~/NewHR/UserControls/EmployeeDetailsCtl.ascx" TagName="EmployeeDetailsCtl"
    TagPrefix="uc1" %>--%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">


        
        var CommandHandler1 = function(command, record){
            <%= hiddenValue.ClientID %>.setValue(record.data.RequestID);
                if(command=="Edit")
                {
                    <%= btnEditLevel.ClientID %>.fireEvent('click');
                }
             }
             


        var totalCost = function (records) {
            var i = 0,
                length = records.length,
                total = 0,
                record;

            for (; i < length; ++i) {
                record = records[i];
                total += record.get('Quantity') * record.get('Rate');
            }

            return total;
        };

        var beforeEdit = function(e1, e) {
            
            if(e.field=="Rate" && e.record.data.IsEditable!="true")
            {
                e.cancel = true;
                return;
            }
        
    };
        
    </script>
    <style>
        .bodyContainer
        {
            background-color: #E8F1FF;
            padding: 20px 0 20px 30px;
            margin: 20px;
            border: 1px solid #BFD9FB;
            border-radius: 3px;
            width: 975px;
            display: block;
            float: left;
        }
        
        
        table#albums
        {
            border-collapse: separate;
            border-spacing: 0 10px;
        }
        
        table#albums1
        {
            border-collapse: separate;
            border-spacing: 0 40px;
        }
        
        
        
        .grids
        {
            padding: 0 0 0 0;
            border: 1px solid silver;
            border-radius: 2px;
            width: 1156px;
            margin-bottom: 5px;
            height: auto;
        }
        #lblAttachment
        {
            border: 2px solid #ccc;
            padding: 5px 10px;
            background: #eee;
            margin: 5px;
            float: none;
        }
        .bodypart
        {
            margin: 0 auto !important;
        }
        p
        {
            margin: 10px 0;
        }
        .x-btn-default-small
        {
            background-color: #f5f5f5;
            border-color: #e1e1e1;
            background-image: -webkit-linear-gradient(top,#f6f6f6,#f5f5f5 50%,#e8e8e8 51%,#f5f5f5);
        }
        .x-btn-default-small .x-btn-inner
        {
            color: #666;
        }
        .windowContentWrapper td
        {
            vertical-align: middle;
            margin-right: 0;
            padding: 10px 10px 10px 0;
        }
        .windowContentWrapper table
        {
        }
        .x-window-body-default
        {
            background: #E8F1FF;
        }
        .windowContentWrapper
        {
            margin: 0;
            padding: 5px;
        }
        .bolds
        {
            padding: 0 100px 0 0;
        }
        .bolds .x-label-value
        {
            color: Gray;
            font-size: large;
            font-weight: bold;
        }
        .boldLabels
        {
            font-weight: bold;
            font-size: 15px;
        }
        .darkOrange
        {
            margin-left: 5px;
            color: #ff8c00;
        }
        
        
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
    <div class="contentArea">
        <ext:Hidden runat="server" ID="hiddenValue" />
        <ext:Hidden runat="server" ID="Hidden_QuerryStringID">
        </ext:Hidden>
        <ext:LinkButton runat="server" Hidden="true" ID="btnEditLevel">
            <DirectEvents>
                <Click OnEvent="btnEditLevel_Click">
                    <EventMask ShowMask="true" />
                </Click>
            </DirectEvents>
        </ext:LinkButton>
        <ext:LinkButton runat="server" Hidden="true" ID="btnDetailLevel">
            <DirectEvents>
                <Click OnEvent="btnDetailLevel_Click">
                    <EventMask ShowMask="true" />
                </Click>
            </DirectEvents>
        </ext:LinkButton>
        <ext:Button ID="btnDeleteLevel" runat="server" Hidden="true">
            <DirectEvents>
                <Click OnEvent="btnDeleteLevel_Click">
                    <Confirmation ConfirmRequest="true" Message="Are you sure, you want to delete the Allowance settings?" />
                    <EventMask ShowMask="true" />
                </Click>
            </DirectEvents>
        </ext:Button>
        <div class="separator bottom">
        </div>
        <div class="innerLR">
            <%--<uc1:EmployeeDetailsCtl Id="EmployeeDetailsCtl1" runat="server" />--%>
           <div class="widget-body" style="width: auto;">
           <table>
            <tr>
                <td>
                    <ext:ComboBox ID="cmbBranch" runat="server" FieldLabel="Branch" Width="150px" DisplayField="Name" LabelAlign="Top" LabelSeparator=""
                                ValueField="BranchId" Editable="false" EmptyText="" QueryMode="Local">
                                <Store>
                                    <ext:Store ID="storeBranch" runat="server">
                                        <Model>
                                            <ext:Model ID="modelBranch" runat="server">
                                                <Fields>
                                                    <ext:ModelField Name="BranchId" Type="Int" />
                                                    <ext:ModelField Name="Name" Type="String" />
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                    </ext:ComboBox>
                </td>
                
                <td>
                    <ext:DateField FieldLabel="Start from" Width="150" ID="txtFromDateFilter" runat="server"
                                        EmptyDate="" LabelAlign="Top" LabelSeparator="">
                     </ext:DateField>
                </td>
                
                <td>
                      <ext:DateField FieldLabel="End from" Width="150" ID="txtEndDateFilter" runat="server"
                                        EmptyDate="" LabelAlign="Top" LabelSeparator="">
                     </ext:DateField>
                </td>
                
                <td>
                        <ext:ComboBox runat="server" ID="ComboFilterAll" LabelAlign="Top" FieldLabel="All"
                            LabelWidth="120" Width="120" DisplayField="Name" ValueField="ID" ForceSelection="true">
                            <Store>
                                <ext:Store ID="storeFilterAll" runat="server">
                                    <Model>
                                        <ext:Model IDProperty="AllFilterID">
                                            <Fields>
                                                <ext:ModelField Name="ID" Type="Int" />
                                                <ext:ModelField Name="Name" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                        </ext:ComboBox>
                </td>

                <td>
                     <ext:TextField FieldLabel="Name" Width="150" ID="txtNameFilter" runat="server" LabelAlign="Top" LabelSeparator="">
                     </ext:TextField>
                </td>
                
                <td>
                        <ext:Button ID="btnSearch" Cls="btnFlat" Width="150" BaseCls="btnFlat" runat="server" Height="42"
                        StyleSpec=" float:right;" Text="&nbsp;&nbsp;Search&nbsp;&nbsp;" >
                        <DirectEvents>
                            <Click OnEvent="btnSearch_Click">
                                <EventMask ShowMask="true" />
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                </td>
            </tr>
           </table>
           </div>
                
                <div class="widget-body" style="width: auto;">
                    <ext:GridPanel StyleSpec="margin-top:15px;" ID="GridLevels" runat="server" Scroll="Both"
                        OverflowX="auto" Height="450">
                        <Store>
                            <ext:Store ID="Store3" runat="server">
                                <Model>
                                    <ext:Model ID="Model4" runat="server" IDProperty="RequestID">
                                        <Fields>
                                            <ext:ModelField Name="RequestID" Type="String" />
                                            <ext:ModelField Name="EmployeeId" Type="string" />
                                            <ext:ModelField Name="FromDate" Type="Date" />
                                            <ext:ModelField Name="EmpName" Type="string" />
                                            <ext:ModelField Name="Position" Type="string" />
                                            <ext:ModelField Name="Department" Type="string" />
                                            <ext:ModelField Name="Destination" Type="string" />
                                            <ext:ModelField Name="Reason" Type="string" />
                                            <ext:ModelField Name="Duration" Type="string" />
                                            <ext:ModelField Name="Status" Type="string" />
                                            <ext:ModelField Name="StatusModified" Type="string" />
                                            <ext:ModelField Name="LocationName" Type="string" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:DateColumn ID="DateColumn1" runat="server" Text="Date" Sortable="true" DataIndex="FromDate"
                                    Width="85" Align="Left" Format="MM/dd/yyyy">
                                </ext:DateColumn>
                                <ext:Column ID="Column10" Sortable="false" MenuDisabled="true" runat="server" Text="Employee Name"
                                    Width="150" Align="Left" DataIndex="EmpName" />
                                <ext:Column ID="Column3" Sortable="false" MenuDisabled="true" runat="server" Text="Position"
                                    Width="150" Align="Left" DataIndex="Position" />
                                <ext:Column ID="Column4" Sortable="false" MenuDisabled="true" runat="server" Text="Department"
                                    Width="150" Align="Left" DataIndex="Department" />
                                <ext:Column ID="Column5" Sortable="false" MenuDisabled="true" runat="server" Text="Destination"
                                    Width="150" Align="Left" DataIndex="Destination" />
                                <ext:Column ID="Column11" Sortable="false" MenuDisabled="true" runat="server" Text="Location"
                                    Width="150" Align="Left" DataIndex="LocationName" />
                                <ext:Column ID="Column6" Sortable="false" MenuDisabled="true" runat="server" Text="Reason"
                                    Width="250" Align="Left" DataIndex="Reason" />
                                <ext:Column ID="Column7" Sortable="false" MenuDisabled="true" runat="server" Text="Duration"
                                    Width="125" Align="Left" DataIndex="Duration" />
                                <ext:Column ID="Column8" Sortable="false" MenuDisabled="true" runat="server" Text="Status"
                                    Width="100" Align="Left" DataIndex="StatusModified">
                                </ext:Column>
                                <ext:CommandColumn runat="server" Align="Center" Width="125">
                                    <Commands>
                                        <ext:GridCommand Text="<i></i>" Icon="pencil" CommandName="Edit" />
                                    </Commands>
                                    <Listeners>
                                        <Command Handler="CommandHandler1(command,record);" />
                                    </Listeners>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <SelectionModel>
                            <ext:RowSelectionModel ID="RowSelectionModel2" runat="server" Mode="Single" />
                        </SelectionModel>
                    </ext:GridPanel>
                </div>
            
            <ext:Window ID="Window1" runat="server" Title="Forward Travel Allowance." Icon="Application"
                Width="1020" Height="625" BodyPadding="5" Hidden="true" Modal="true">
                <Content>
                    <div style="width: 400px; float: left">
                        <table id="albums1" width="400px;">
                            <tr>
                                <td>
                                    <table>
                                        <tr>
                                            <td>
                                                <ext:Label ID="lblName" runat="server">
                                                </ext:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <ext:Label ID="lblBranch" runat="server">
                                                </ext:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <ext:Label ID="lblDepartment" runat="server">
                                                </ext:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <ext:Label ID="lblDesignation" runat="server">
                                                </ext:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <ext:TextArea ID="txtCommentSection" Height="25" runat="server">
                                    </ext:TextArea>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <ext:TextArea ID="txtHRComment" FieldLabel="Your Comment" LabelAlign="Top" runat="server"
                                        Width="400" Height="75">
                                    </ext:TextArea>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div style="width: 500px; float: left; padding-left: 25px;">
                        <div style="float: right; width: 90px; margin-top:10px;">
                            <ext:LinkButton ID="btnPrint" runat="server" Text="Print" OnClick="btnPrint_Click" Hidden="true"
                                Icon="Printer" AutoPostBack="true">
                            </ext:LinkButton>
                        </div>
                        <table id="albums" cellspacing="0px">
                            <tr style="padding-bottom: 1em;">
                                <td colspan="1" style="margin-right: 25px; margin-left: 25px;">
                                    <ext:TextField ID="txtPlaceToTravel" Width="200" runat="server" FieldLabel="Place to Travel"
                                        LabelAlign="Top">
                                    </ext:TextField>
                                </td>
                                <td colspan="1" style="padding-left: 25px; padding-top: 15px;">
                                    <ext:ComboBox ID="cmbCountry" runat="server" Width="200" FieldLabel="Country" LabelAlign="Top"
                                        QueryMode="Local" DisplayField="CountryName" ValueField="CountryId">
                                        <Store>
                                            <ext:Store ID="storeCountryAssign" runat="server">
                                                <Fields>
                                                    <ext:ModelField Name="CountryId" Type="String" />
                                                    <ext:ModelField Name="CountryName" />
                                                </Fields>
                                            </ext:Store>
                                        </Store>
                                        <DirectEvents>
                                            <Change OnEvent="Country_Change">
                                            </Change>
                                        </DirectEvents>
                                    </ext:ComboBox>
                                </td>
                            </tr>
                            <tr style="padding-bottom: 1em;">
                                <td colspan="5">
                                    <ext:TextArea ID="txtPurposeOfTravel" Width="550" runat="server" FieldLabel="Purpose of Travel"
                                        LabelAlign="Top">
                                    </ext:TextArea>
                                </td>
                            </tr>
                            <tr style="padding-bottom: 1em;">
                                <td colspan="1">
                                    <ext:DateField FieldLabel="Travelling from" Width="150" ID="txtFromDate" runat="server"
                                        EmptyDate="" LabelAlign="Top" LabelSeparator="">
                                        <DirectEvents>
                                            <Change OnEvent="CheckInOutSelectChange">
                                            </Change>
                                        </DirectEvents>
                                    </ext:DateField>
                                </td>
                                <td colspan="1">
                                    <ext:DateField FieldLabel="Travelling to" Width="150" ID="txtToDate" runat="server"
                                        LabelAlign="Top" EmptyDate="" LabelSeparator="">
                                        <DirectEvents>
                                            <Change OnEvent="CheckInOutSelectChange">
                                            </Change>
                                        </DirectEvents>
                                    </ext:DateField>
                                </td>
                                <td colspan="1" style="padding-top: 5px;">
                                    <ext:Label ID="TextArea4" Width="30" runat="server" Style="margin-right: 25px; margin-left: -35px;"
                                        Text="=">
                                    </ext:Label>
                                </td>
                                <td colspan="1" style="padding-right: 7px;">
                                    <ext:NumberField ID="txtDayCount" runat="server" Width="60" FieldLabel="Days" LabelAlign="Top">
                                    </ext:NumberField>
                                </td>
                                <td colspan="1">
                                    <ext:NumberField ID="txtNightCount" runat="server" Width="60" FieldLabel="Night"
                                        LabelAlign="Top">
                                    </ext:NumberField>
                                </td>
                            </tr>
                            <tr style="padding-bottom: 1em;">
                                <td colspan="1">
                                    <ext:ComboBox ID="cmbTravelBy" runat="server" FieldLabel="Travel by" LabelAlign="Top"
                                        QueryMode="Local" DisplayField="Name" ValueField="Value">
                                        <Store>
                                            <ext:Store ID="storeTravelBy" runat="server">
                                                <Fields>
                                                    <ext:ModelField Name="Name" />
                                                    <ext:ModelField Name="Value" Type="String" />
                                                </Fields>
                                            </ext:Store>
                                        </Store>
                                    </ext:ComboBox>
                                </td>
                                <td colspan="1">
                                    <ext:ComboBox ID="cmbExpensePaidBy" runat="server" FieldLabel="Expense Paid by" LabelAlign="Top"
                                        QueryMode="Local" DisplayField="Name" ValueField="Value">
                                        <Store>
                                            <ext:Store ID="storeExpensePaidBy" runat="server">
                                                <Fields>
                                                    <ext:ModelField Name="Name" />
                                                    <ext:ModelField Name="Value" Type="String" />
                                                </Fields>
                                            </ext:Store>
                                        </Store>
                                    </ext:ComboBox>
                                </td>
                            </tr>
                            <tr>
                    <td>
                        <ext:ComboBox ID="cmbLocations" ForceSelection="true" runat="server" Width="150"
                            FieldLabel="Location for Allowance *" LabelAlign="Top" LabelSeparator=" " QueryMode="Local"
                            DisplayField="LocationName" ValueField="LocationId">
                            <Store>
                                <ext:Store ID="store1" runat="server">
                                    <Fields>
                                        <ext:ModelField Name="LocationId" Type="String" />
                                        <ext:ModelField Name="LocationName" />
                                    </Fields>
                                </ext:Store>
                            </Store>
                            <DirectEvents>
                                <Select OnEvent="cmbLocations_Change">
                                    <EventMask ShowMask="true" />
                                    <ExtraParams>
                                        <ext:Parameter Name="Values" Value="Ext.encode(#{gridAllowances}.getRowsValues({selectedOnly : false}))"
                                            Mode="Raw" />
                                    </ExtraParams>
                                </Select>
                            </DirectEvents>
                        </ext:ComboBox>
                    </td>
                </tr>
                            <tr style="padding-bottom: 1em;">
                                <td colspan="5">
                                    <ext:GridPanel StyleSpec="margin-top:15px;" ID="gridAllowances" runat="server" Width="550"
                                        Height="125" Cls="itemgrid">
                                        <Store>
                                            <ext:Store ID="storeAllowances" runat="server">
                                                <Model>
                                                    <ext:Model ID="Model1" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="LineId" Type="String" />
                                                            <ext:ModelField Name="RequestId" Type="string" />
                                                            <ext:ModelField Name="AllowanceId" Type="string" />
                                                            <ext:ModelField Name="AllowanceName" Type="string" />
                                                            <ext:ModelField Name="Quantity" Type="Float" />
                                                            <ext:ModelField Name="Rate" Type="Float" />
                                                            <ext:ModelField Name="Total" Type="Float" />
                                                            <ext:ModelField Name="IsEditable" Type="String" />
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <Plugins>
                                            <ext:CellEditing ID="CellEditing1" ClicksToEdit="1" runat="server">
                                                <Listeners>
                                                </Listeners>
                                            </ext:CellEditing>
                                        </Plugins>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:Column ID="Column1" Sortable="false" MenuDisabled="true" runat="server" Text="Allowances"
                                                    Align="Left" DataIndex="AllowanceName" Width="200" />
                                                    
                                                <ext:Column ID="Column2" Sortable="false" MenuDisabled="true" runat="server" Text="qty"
                                                    Align="Left" DataIndex="Quantity" Width="100">
                                                    <Editor>
                                                        <ext:NumberField ID="txtQTY" SelectOnFocus="true" runat="server" AllowBlank="false" />
                                                    </Editor>
                                                </ext:Column>
                                                <ext:Column ID="Column9" Sortable="false" MenuDisabled="true" runat="server" Text="Rate"
                                                    Align="Left" DataIndex="Rate" Width="125">
                                                    <Editor>
                                                        <ext:NumberField ID="txtRate" SelectOnFocus="true" runat="server" AllowBlank="false">
                                                        </ext:NumberField>
                                                    </Editor>
                                                </ext:Column>
                                                <ext:Column runat="server" Width="125" ID="Total" Text="Total" Sortable="false" Groupable="false"
                                                    DataIndex="Total" CustomSummaryType="totalCost">
                                                    <Renderer Handler="return Ext.util.Format.number((record.data.Quantity * record.data.Rate),'0,000.00');" />
                                                </ext:Column>
                                            </Columns>
                                        </ColumnModel>
                                        <SelectionModel>
                                            <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" Mode="Single" />
                                        </SelectionModel>
                                        <Listeners>
                                            <BeforeEdit Fn="beforeEdit" />
                                        </Listeners>
                                    </ext:GridPanel>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div style="height: 5px;">
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <ext:Button ID="btnSaveAndLater" Cls="btnFlat" Width="150" BaseCls="btnFlat" runat="server"
                        StyleSpec=" float:right;" Text="&nbsp;&nbsp;Forward&nbsp;&nbsp;" ValidationGroup="InsertUpdate">
                        <DirectEvents>
                            <Click OnEvent="ButtonNext_Click">
                                <EventMask ShowMask="true" />
                                <ExtraParams>
                                    <ext:Parameter Name="AllowanceValues" Value="Ext.encode(#{gridAllowances}.getRowsValues({selectedOnly : false}))"
                                        Mode="Raw" />
                                </ExtraParams>
                            </Click>
                        </DirectEvents>
                        <Listeners>
                            <Click Handler="valGroup = 'InsertUpdate'; return CheckValidation();">
                            </Click>
                        </Listeners>
                    </ext:Button>
                </Content>
            </ext:Window>
        </div>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtPlaceToTravel"
            ErrorMessage="Place to travel is required." Display="None" ValidationGroup="InsertUpdate">
        </asp:RequiredFieldValidator>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="cmbCountry"
            ErrorMessage="Country is required" Display="None" ValidationGroup="InsertUpdate">
        </asp:RequiredFieldValidator>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtPurposeOfTravel"
            ErrorMessage="Purpose of Travel is required" E Display="None" ValidationGroup="InsertUpdate">
        </asp:RequiredFieldValidator>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtFromDate"
            ErrorMessage="From Date is required" Display="None" ValidationGroup="InsertUpdate">
        </asp:RequiredFieldValidator>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="txtToDate"
            ErrorMessage="To Date is required" Display="None" ValidationGroup="InsertUpdate">
        </asp:RequiredFieldValidator>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="cmbTravelBy"
            ErrorMessage="Travel is required" Display="None" ValidationGroup="InsertUpdate">
        </asp:RequiredFieldValidator>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ControlToValidate="cmbExpensePaidBy"
            ErrorMessage="Expense Paid by is required" Display="None" ValidationGroup="InsertUpdate">
        </asp:RequiredFieldValidator>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="cmbLocations"
            ErrorMessage="Location is required" Display="None" ValidationGroup="InsertUpdateSettle">
        </asp:RequiredFieldValidator>
    </div>
</asp:Content>
