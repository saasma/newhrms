﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL.Entity;
using BLL;
using System.IO;
using Utils;

namespace Web.Employee
{
    public partial class BranchEmployees : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                List<Branch> branchList = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId);
                cmbBranch.Store[0].DataSource = branchList;
                cmbBranch.Store[0].DataBind();

                int branhId = BLL.BaseBiz.PayrollDataContext.GetCurrentBranch(
                    SessionManager.CurrentLoggedInEmployeeId, DateTime.Now).Value;

                ExtControlHelper.ComboBoxSetSelected(branhId.ToString(), cmbBranch);

            }
        }

        public void btnLoad_Click(object sender, EventArgs e)
        {

            int branchId = -1;
            int empId = -1;

            if(cmbBranch.SelectedItem != null && cmbBranch.SelectedItem.Value != null)
                branchId = int.Parse(cmbBranch.SelectedItem.Value); 

            if (cmbEmpSearch.SelectedItem != null && cmbEmpSearch.SelectedItem.Value != null)
                empId = int.Parse(cmbEmpSearch.SelectedItem.Value);

            if (branchId == -1 && empId == -1)
            {
                NewMessage.ShowWarningMessage("Branch or Employee selection is required.");
                return;
            }

            gridBranchEmployees.Store[0].DataSource = NewHRManager.GetBranchEmployees(branchId, empId);
            gridBranchEmployees.Store[0].DataBind();
        }
    }
}