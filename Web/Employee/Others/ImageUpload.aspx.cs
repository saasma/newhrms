﻿using System;
using System.Collections.Generic;
using System.Linq;
using BLL;
using BLL.Manager;
using Utils.Helper;
using System.IO;
using Utils;
using DAL;
using Utils.Security;

namespace Web.Employee
{
    public partial class ImageUpload : System.Web.UI.Page
    {
        /// <summary>
        /// Property for holding the Id of the editing object/entity
        /// </summary>
        public int CustomId
        {
            get
            {
                if (ViewState["Id"] != null)
                {
                    return int.Parse(ViewState["Id"].ToString());
                }
                return 0;
            }
            set
            {
                ViewState["Id"] = value;
            }
        }
        private int employeeId = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Initialise();
            }
            if (SessionManager.CurrentLoggedInEmployeeId != 0)
            {
                employeeId = SessionManager.CurrentLoggedInEmployeeId;
            }
            else
            {
                employeeId = UrlHelper.GetIdFromQueryString("EmpId");
            }
        }

        private void Initialise()
        {
            var type = UrlHelper.GetIdFromQueryString("type");
            if (type != 0)
            {
                CustomId = type;
                if (CustomId == 4)
                {
                    Page.Title = "Upload document";
                }
                else
                {
                    Page.Title = "Upload image";
                    rowDesc.Visible = false;
                }
            }
        }

        protected void btnUpload_Click(object sender, EventArgs e)
        {
            if (fup.HasFile)
            {
                var ext = Path.GetExtension(fup.FileName);
                var guidFileName = Guid.NewGuid().ToString() + ext;
                var saveLocation = Path.Combine(Server.MapPath(Config.UploadLocation), guidFileName);


                var type = (UploadType)CustomId;
                var code = "closeWindow(\"{0}\");";
                var mgr = new HRManager();
                if (type != UploadType.Document)
                {
                    if (IsImage(ext) == false)
                    {
                        JavascriptHelper.DisplayClientMsg("Invalid image type.", Page);
                        return;
                    }
                    fup.SaveAs(saveLocation);
                    var filename = string.Empty;
                    try
                    {
                        mgr.SaveOrUpdateImage(type, guidFileName, employeeId);
                        filename = "../Uploads/" + guidFileName;
                        code = string.Format(code, filename);
                        JavascriptHelper.DisplayClientMsg("Image added.", Page, code);
                    }
                    catch (Exception exp1)
                    {
                        JavascriptHelper.DisplayClientMsg(exp1.Message, Page);
                    }

                    Page.ClientScript.RegisterClientScriptBlock(GetType(), "sdfdsf2333",
                        string.Format("opener.process('{0}');", filename), true);
                }
                else
                {
                    try
                    {
                        guidFileName =  guidFileName.Replace(Path.GetExtension(guidFileName), ".ashx");
                        saveLocation = saveLocation.Replace(Path.GetExtension(saveLocation), ".ashx");

                        var enc = new EncryptorDecryptor();
                        enc.Encrypt(fup.FileContent, saveLocation);

                        var doc = new HDocument();
                        doc.EmployeeId = employeeId;
                        doc.Name = Path.GetFileName(fup.FileName);

                        if (!string.IsNullOrEmpty(txtDesc.Text.Trim()))
                        {
                            doc.Description = txtDesc.Text.Trim();
                        }
                        else
                        {
                            doc.Description = Path.GetFileNameWithoutExtension(fup.FileName);
                        }
                        doc.ContentType = fup.PostedFile.ContentType;
                        doc.Url = guidFileName;
                        doc.Size = GetSize(fup.PostedFile.ContentLength);

                        mgr.SaveDocument(doc);


                        code = string.Format(code, mgr.GetHTMLDocuments(doc.EmployeeId.Value));
                        JavascriptHelper.DisplayClientMsg("Document added.", Page, code);
                    }
                    catch (Exception exp1)
                    {
                        JavascriptHelper.DisplayClientMsg(exp1.Message, Page);
                    }
                }
            }
        }

        /// <summary>
        /// Checks if the file has the valid image extension or not
        /// </summary>
        /// <param name="pExtension">Extension type</param>
        /// <returns>Is valid image or not</returns>
        public static bool IsImage(string pExtension)
        {
            if ((pExtension.ToLower() == ".jpg" | pExtension.ToLower() == ".gif" | pExtension.ToLower() == ".bmp" | pExtension.ToLower() == ".jpeg" | pExtension.ToLower() == ".png"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// Helper method to convert Bytes in KiloBytes
        /// </summary>
        /// <param name="byteValue"></param>
        /// <returns></returns>
        public static int ConvertBytesIntoKB(int byteValue)
        {
            var mb = (byteValue / 1024);
            if (mb.ToString("#") == string.Empty)
            {
                return 0;
            }
            return Convert.ToInt32(mb.ToString("#"));
        }
        public static string ConvertKBIntoMB(object value)
        {
            var kb = Convert.ToInt64(value);
            var mb = (kb / 1024.0);
            return mb.ToString("#.#");
        }

        public string GetSize(int byteValue)
        {
            if (byteValue < 1024)
            {
                return "1 kb";
            }
            var kb = ConvertBytesIntoKB(byteValue);
            if (kb > 1024)
            {
                return ConvertKBIntoMB(kb) + " mb";
            }
            else
            {
                return kb + " kb";
            }
        }
    }
}
