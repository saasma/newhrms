﻿<%@ Page Title="Notice Detail" ValidateRequest="false" EnableEventValidation="false" Language="C#" MasterPageFile="~/Master/EmployeeMS.Master"
    AutoEventWireup="true" CodeBehind="NoticeDetail.aspx.cs" Inherits="Web.Employee.NoticeDetail" %>

<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
      
      
        .details tr td
        {
            padding-top: 4px;
        }
        .leaveList
        {
            list-style-type: none;
            /*border: 2px solid #BFBFBF;
            background-color: #D0CECE;*/
            display: inline-block;
            padding: 10px;
        }
        .leaveList li
        {
            border: 1px solid #DEDEDE;
            background-color: #F2F2F2;
            float: left;
            padding: 5px;
            padding-left: 8px;
            padding-right: 8px;
            text-align: center;
        }
        
        .blockTitle
        {
            color: #0C6E92;
            text-transform: uppercase;
            font-size: 14px;
            font-weight: bold;
            padding-left: 2px;
        }
        .blockText
        {
            color: #305496;
        }
        .blockBottomBorder
        {
            border-bottom: 2px solid #D9D9D9;
            padding-bottom: 1px;
        }
        .weekBlock
        {
            background-color: #DEDEDE;
            display: inline-block;
            padding: 4px;
            margin-top: 10px;
            padding-left: 8px;
            padding-right: 50px;
            color: #333333;
            margin-left: 90px;
        }


        .submitAttendance{
            color: #ffffff;
            font-size: 14px;
            background: url('images/clock.png');
            background-position:left;
            background-repeat:no-repeat;
            background-color: #5CB85C;
            padding: 10px 5px 10px 20px;
            text-decoration: none;
            width: 100px;
            margin-right: 5px;
            margin-left: 5px;
            padding-left: 32px;

}

        .submitAttendance:hover {
            color: #ffffff;
            font-size: 14px;
            background: url('images/clock.png');
            background-position:left;
            background-repeat:no-repeat;
            background-color: #71DB8E;
            padding: 10px 5px 10px 20px;
            text-decoration: none;
            width: 100px;
            margin-right: 5px;
            margin-left: 5px;
            padding-left: 32px;

        }

        
        .scheduleLeave{
            color: #ffffff;
            font-size: 14px;
            background: url('images/plane.png');
            background-position:left;
            background-repeat:no-repeat;
            background-color: #3498db;
            padding: 10px 5px 10px 20px;
            text-decoration: none;
            width: 100px;
            margin-right: 5px;
            margin-left: 5px;
            padding-left: 30px;
}

        .scheduleLeave:hover {
            color: #ffffff;
            font-size: 14px;
            background: url('images/plane.png');
            background-position:left;
            background-repeat:no-repeat;
            background-color: #3cb0fd;
            padding: 10px 5px 10px 20px;
            text-decoration: none;
            width: 100px;
            margin-right: 5px;
            margin-left: 5px;
            padding-left: 30px;

        }
        
        .widget .widget-body.list ul li{line-height:inherit!important;}
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
    <ext:ResourceManager ID="ResourceManager1" runat="server" />
    
    <ext:Hidden ID="HiddenFileName" runat="server"></ext:Hidden>
    <ext:Hidden ID="HiddenFileURL" runat="server"></ext:Hidden>

    <table>
        <tr>
            <td>
                <%--<h2 style="margin-top:25px;">
                    Notice Detail
                </h2>--%>
            </td>
        </tr>
        <tr>
            <td>
                
                    <div style="margin-top: 15px;" >
                        <div style="">
                            <table>
                                <tr>
                                    <td>
                                        <ext:DisplayField runat="server" Text="" ID="txtCategory" ReadOnly="true"  LabelSeparator=""
                                        Width="800" FieldLabel=" "  LabelAlign="Top" MarginSpec="10 0 20 0"
                                         LabelStyle="color:#7A7A7A; font-size:20px; " FieldStyle="color:#7A7A7A; font-size:20px;"></ext:DisplayField>
                                    </td>
                                   
                                </tr>

                                <tr>
                                    <td colspan="2">
                                        <ext:DisplayField runat="server" Text="" ID="txtTitle" ReadOnly="true" LabelSeparator=""
                                        Width="800" FieldLabel="" LabelAlign="Top" 
                                        LabelStyle="color:#F54000; font-size:20px;" FieldStyle="color:#F54000; font-size:20px;line-height:22px;"></ext:DisplayField>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <ext:DisplayField runat="server" Text="" ID="txtPublishedDate" ReadOnly="true" 
                                        Width="800" FieldLabel="Published Date" LabelAlign="Left" LabelWidth="150" MarginSpec="10 0 20 0"
                                         LabelStyle="color:#7A7A7A; font-size:14px; font-style: italic;" FieldStyle="font-style: italic; color:#7A7A7A; font-size:14px;"></ext:DisplayField>
                                    </td>
                                   
                                </tr>
                                 <tr>
                                    <td colspan="2">
                                        <div style="width:800; border-top: 2px solid; border-top-color: #999999; padding-top:15px; border-bottom-color:#999999 !important;  border-bottom: 2px solid;">
                                            <ext:DisplayField runat="server" Text="" ID="txtBody" ReadOnly="true" FieldLabel="" LabelSeparator=""
                                            Width="800" LabelAlign="Top"  MinHeight="75"  ></ext:DisplayField>
                                        </div>
                                    </td>
                                </tr>


                                 <tr>
                                    
                                    <td>
                                        
                                        <table>
                                                <tr>
                                                    <td>
                                                        <ext:Image ID="imageAttach" Width="50" Height="50" ImageUrl="images/attachment.png" runat="server" ></ext:Image>
                                                        
                                                    </td>
                                                    <td>
                                                        <ext:LinkButton runat="server" Text="" ID="btnFile" StyleSpec="color:Black;" MarginSpec="12 0 0 10">
                                                            <DirectEvents>
                                                                <Click OnEvent="DownloadFile_Click">
                                                                </Click>
                                                            </DirectEvents>
                                                        </ext:LinkButton>
                                                    </td>
                                                </tr>
                                        </table>
                                       
                                    </td>
                                </tr>
                            </table>

                        </div>
                    </div>
            </td>
        </tr>
    </table>
    
    
    

</asp:Content>
