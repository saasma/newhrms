﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Manager;
using Utils.Helper;
using DAL;
using Utils.Security;

namespace Web.Employee
{
    public partial class ResetPassword : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Initialize();
            }
        }

        private void Initialize()
        {
            if (!string.IsNullOrEmpty(Request.QueryString["Token"]))
            {
                hdnToken.Value = Request.QueryString["Token"].ToString();
            }
        }

        protected void btnChange_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(hdnToken.Value))
                return;

            string msg = "";
            if (!CheckPasswordStrength(txtNewPwd.Text.Trim(), ref msg))
            {
                  spanMessage.Style.Add("display", "block");
                spanMessage.Text = msg;
                //divOuter.Style["display"] = "block";
                return;
            }

            if (Page.IsValid)
            {
                spanMessage.Style.Add("display", "block");

                UserManager mgr = new UserManager();

                string userName = string.Empty;

                string pwdToken = string.Empty;               

                pwdToken = new EncryptorDecryptor().Decrypt(hdnToken.Value);

                PwdRecovery objPR = UserManager.GetPasswordRecoveryById(pwdToken);
                if (objPR == null)
                {
                    spanMessage.Text = "Please request new password reset token.";
                    //divOuter.Style["display"] = "block";
                    return;
                }

                userName = objPR.UserName;
                DateTime dt = objPR.RequestedDate.Value;

                TimeSpan ts = DateTime.Now - dt;
                if (ts.TotalHours >= 5)
                {
                    //spanMessage.InnerText = "Password reset token has been expired, please request new one.";
                    //divOuter.Style["display"] = "block";
                    //return;

                    ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect",
                    "alert('Password reset token has been expired, please request new one.'); window.location='" +
                    Request.ApplicationPath + "/Employee/ForgotPassword.aspx';", true);

                    return;
                }

                if (UserManager.IsMatchedToLastTwoPasswords(userName, txtNewPwd.Text.Trim()))
                {
                    spanMessage.Text = "Password matches with the last two passwords set before, please try new password.";
                    //divOuter.Style["display"] = "block";
                    return;
                }


                mgr.ChangePassword(userName, txtNewPwd.Text.Trim());
                txtNewPwd.Text = string.Empty;
                txtConfirmPwd.Text = string.Empty;

                spanMessage.Text = "Password is reset successfully.";
                //divOuter.Style["display"] = "none";

                //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "redirect",
                //"alert('Password is reset successfully.'); window.location='" +
                //Request.ApplicationPath + "/Default.aspx';", true);

                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "Message",
                    "window.onload = function(){alert('Password is reset successfully.'); window.location='"
                    + Request.ApplicationPath + "/Default.aspx';" + "}", true);

            }
        }

        private bool CheckPasswordStrength(string pwd, ref string msg)
        {
            msg = "";
            if ((pwd.Length < 6) || (!pwd.Where(x => Char.IsDigit(x)).Any()) || (!pwd.Where(x => Char.IsUpper(x)).Any()))
            {
                msg = "Password must contain at least 6 characters containing at least one uppercase letter (A-Z) and at least one number (0-9).";
                return false;
            }

            return true;

        }

    }
}