﻿<%@ Page Title="Dashboard" Language="C#" MasterPageFile="~/Master/EmployeeMS.Master"
    AutoEventWireup="true" CodeBehind="Dashboard.aspx.cs" Inherits="Web.Employee.Dashboard" %>

<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>

<%@ Register Src="~/Employee/UserControls/EmpActivityCtrl.ascx" TagName="ea" TagPrefix="uc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .details tr td {
            padding-top: 4px;
        }

        .leaveList {
            list-style-type: none; /*border: 2px solid #BFBFBF;
            background-color: #D0CECE;*/
            display: inline-block;
            padding: 10px;
        }

            .leaveList li {
                border: 1px solid #DEDEDE;
                background-color: #F2F2F2;
                float: left;
                padding: 5px;
                padding-left: 8px;
                padding-right: 8px;
                text-align: center;
                margin-bottom: 10px;
                margin-right: 10px;
            }

        .noticeContainer td {
            padding: 0px;
        }

        .blockTitle {
            color: #428BCA; /*text-transform: uppercase;*/
            font-size: 14px;
            font-weight: bold;
            padding-left: 2px;
        }

        .x-grid-body {
            border-top-color: white;
        }

        .blockText {
            color: #305496;
        }

        .blockBottomBorder {
            border-bottom: 1px solid #D9E7F3;
            padding-bottom: 1px;
        }

        .weekBlock {
            background-color: #DEDEDE;
            display: inline-block;
            padding: 4px;
            margin-top: 10px;
            padding-left: 8px;
            padding-right: 50px;
            color: #333333;
            margin-left: 90px;
        }


        .submitAttendance {
            color: #ffffff;
            font-size: 14px;
            background: url('images/clock.png');
            background-position: left;
            background-repeat: no-repeat;
            background-color: #5CB85C;
            padding: 10px 5px 10px 20px;
            text-decoration: none;
            width: 100px;
            margin-right: 5px;
            margin-left: 5px;
            padding-left: 32px;
        }

            .submitAttendance:hover {
                color: #ffffff;
                font-size: 14px;
                background: url('images/clock.png');
                background-position: left;
                background-repeat: no-repeat;
                background-color: #71DB8E;
                padding: 10px 5px 10px 20px;
                text-decoration: none;
                width: 100px;
                margin-right: 5px;
                margin-left: 5px;
                padding-left: 32px;
            }

        .searchEmployee {
            color: #ffffff;
            font-size: 14px;
            background: url('images/searchEmployee.png');
            background-position: left;
            background-repeat: no-repeat;
            background-color: #5CB85C;
            padding: 10px 5px 10px 20px;
            text-decoration: none;
            width: 100px;
            margin-right: 5px;
            margin-left: 5px;
            padding-left: 32px;
            background-size: 27px;
        }

        .leaveList td {
            padding-bottom: 8px;
        }

        .searchEmployee:hover {
            color: #ffffff;
            font-size: 14px;
            background: url('images/searchEmployee.png');
            background-position: left;
            background-repeat: no-repeat;
            background-color: #71DB8E;
            padding: 10px 5px 10px 20px;
            text-decoration: none;
            width: 100px;
            margin-right: 5px;
            margin-left: 5px;
            padding-left: 32px;
            background-size: 27px;
        }

        .scheduleLeave {
            color: #ffffff;
            font-size: 14px;
            background-position: left;
            background-repeat: no-repeat;
            background-color: #3498db;
            padding: 10px 5px 10px 20px;
            text-decoration: none;
            width: 100px;
            margin-right: 5px;
            margin-left: 5px;
            padding-left: 30px;
        }

            .scheduleLeave:hover {
                color: #ffffff;
                font-size: 14px;
                background-position: left;
                background-repeat: no-repeat;
                background-color: #3cb0fd;
                padding: 10px 5px 10px 20px;
                text-decoration: none;
                width: 100px;
                margin-right: 5px;
                margin-left: 5px;
                padding-left: 30px;
            }

        .widget .widget-body.list ul li {
            line-height: inherit !important;
        }

        td {
            padding: 2px;
        }

        .activityClass {
            width: 300px;
            height: 25px;
            float: left;
            padding-top: 5px;
            font-weight: bold;
            padding-left: 5px;
            background-color: #5395CE;
            color: White;
            margin-top: 10px;
        }

        .bottomBlock {
            clear: both;
            padding: 0 0 10px 10px;
            background: #D7E7FF;
            float: left;
            width: 400px;
            margin-left: -5px;
            margin-top: 20px;
        }

        .lblClass {
            font-weight: bold;
            color: Red;
            margin-top: 10px;
        }

        img{margin-bottom: 4px!important;}
        
        #ctl00_mainContent_hrComment
        {
            cursor:pointer;   
            cursor: hand;
        }
        .blueCls
        {
            color:Blue !important;
        }
    </style>
    <script type="text/javascript" src="<%= ResolveUrl("~/Employee/override.js?v=") + Web.Helper.WebHelper.Version %>"></script>
    <link href="<%=ResolveUrl("~/bootstrap/css/bootstrap.css?v=") + Web.Helper.WebHelper.Version %>"
        rel="stylesheet" />
    <!-- Glyphicons -->
    <link rel="stylesheet" href="<%=ResolveUrl("~/theme/css/glyphicons.css?v=")  + Web.Helper.WebHelper.Version %>" />
    <script src="<%=ResolveUrl("~/theme/scripts/jquery-1.8.2.min.js?v=")+ Web.Helper.WebHelper.Version %>"
        type="text/javascript"></script>
    <!-- Theme -->
    <link rel="stylesheet" href="<%=ResolveUrl("~/theme/css/style.min.css?v=") + Web.Helper.WebHelper.Version %>" />
    <!-- LESS 2 CSS -->
    <script src="<%=ResolveUrl("~/theme/scripts/less-1.3.3.min.js?v=")+ Web.Helper.WebHelper.Version %>"
        type="text/javascript"></script>
    <script type="text/javascript">

        var currentMonth = null;
        ///// Override DatePicker & is triggered when month or date selection changes
        Ext.DatePicker.prototype.update = function (date, forceRefresh) {

            var me = this,
            active = me.activeDate;
            if (me.rendered) {
                me.activeDate = date;
                if (!forceRefresh && active && me.el && active.getMonth() == date.getMonth() && active.getFullYear() == date.getFullYear()) {
                    me.selectedUpdate(date, active);
                } else {
                    me.fullUpdate(date, active);
                }
            }


            if (this.id == 'ctl00_mainContent_DatePickerWithHolidayDisplay') {

                if (currentMonth == null) {
                    currentMonth = date.getMonth();
                    currentYear = date.getFullYear();
                    loadHoliday(date.getMonth(), date.getFullYear());

                }
                else if (currentMonth != date.getMonth() || currentYear != date.getFullYear()) {
                    currentMonth = date.getMonth();
                    currentYear = date.getFullYear();
                    loadHoliday(date.getMonth(), date.getFullYear());

                }
            }

            return me;

        };


        var myRenderer1 = function (value, metadata) {

            if (value != null && value != '') {
                if (value == 'On')
                    metadata.style = "background-color: #92d050; color:#006100;";
                else
                    metadata.style = "background-color: lightsalmon; color:#9c0006;";
            }

            return value;
        };

        var loadHoliday = function (month, year) {
            Ext.net.Mask.show();
            Ext.net.DirectMethod.request({
                url: "../LeaveRequestService.asmx/GetHolidays",
                cleanRequest: true,
                json: true,
                params: {
                    month: month,
                    year: year
                },
                success: function (result) {
                    holidayList = result;
                    //Ext.Msg.alert("Json Message", result);
                    CompanyX.ctl00_mainContent_DatePickerWithHolidayDisplay.highlightDates(result);
                    Ext.net.Mask.hide();

                    //find the range case
                    for (i = 0; i < result.length; i++) {
                        if (result[i].Id == 0) {
                            CompanyX.ctl00_mainContent_lblCalendarTitle.setText(result[i].Abbreviation);
                            break;
                        }
                    }

                },
                failure: function (result) { Ext.net.Mask.hide(); }
            });
        };


    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
    <ext:ResourceManager ID="ResourceManager1" runat="server" DisableViewState="false"
        ShowWarningOnAjaxFailure="false" ScriptMode="Release" Namespace="CompanyX" />
    <ext:Hidden ID="hdnLoggedInTime" runat="server" />
    <ext:Hidden ID="hdnActivityId" runat="server" />

    <ext:Button ID="btnShowActivityWindow" runat="server" Hidden="true">
        <DirectEvents>
            <Click OnEvent="btnShowActivityWindow_Click">
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:Button>

    <uc1:ea ID="EmpActCtrl1" runat="server" />

    <div class="contentArea">
        <uc2:WarningCtl ID="divWarningMsg" EnableViewState="false" Hide="true" runat="server" />
        <table>
            <tr>
                <td valign="top">
                    <div style="margin-top: 15px">
                        <table>
                            <tr>
                                <td>
                                    <asp:Image ID="image" runat="server" Width="120px" Height="120px" />
                                </td>
                                <td style="padding-left: 30px;">
                                    <div>
                                        <span id="welcome" style="color: #44546A; font-size: 22px; font-weight: bold" runat="server">Welcome {0},</span> <span style="color: #6772C4;">what would you like to do?</span>
                                        <asp:Label Style="text-align: center; font-size: 16px; color: #339900; line-height: 20px; padding-left: 30px"
                                            ID="lblDay" runat="server" Text=""></asp:Label>
                                        <asp:Label Style="text-align: center; font-size: 16px; color: #FF3300; line-height: 20px; padding-left: 15px"
                                            ID="lblNepDate" runat="server" Text=""></asp:Label>
                                        <asp:Label Style="text-align: center; font-size: 16px; color: #336699; line-height: 20px; padding-left: 15px"
                                            ID="lblEngDate" runat="server" Text=""></asp:Label>
                                        <div style="padding-top: 5px;">
                                            <ul class="leaveList">
                                                <asp:Repeater runat="server" ID="rptLeaveList">
                                                    <ItemTemplate>
                                                        <li>
                                                            <%#Eval("Text") %></li>
                                                    </ItemTemplate>
                                                    <%--<SeparatorTemplate>
                                                        <li style="background-color: transparent; border: 0px"></li>
                                                    </SeparatorTemplate>--%>
                                                </asp:Repeater>
                                            </ul>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div style="padding-top: 20px">
                    </div>
                    <div style="padding-bottom: 15px; padding-left: 173px">
                        <a href="SubmitAttendance.aspx" class="submitAttendance" runat="server" id="linkAttendance">Attendance </a>
                        <a href="LeaveRequest.aspx"
                            class="scheduleLeave" title="Leave Request">Schedule a Leave </a><a href='<%=EmpSearchNavigation()%>'
                                class="searchEmployee">Search Employee </a>
                    </div>
                    <div style="margin-top: 10px">
                        <div class="blockBottomBorder">
                            <img src="images/notice-small.png" width="20" height="20" />
                            <span class="blockTitle">Notices</span> <span class="blockText"><a href="NoticeList.aspx">View All</a></span>
                        </div>
                        <div>
                            <div style="padding-left: 87px; font-size: 13px;" class='noticeContainer'>
                                <ext:GridPanel ID="gridNoticeBoard" StyleSpec="border-top:0px;" runat="server" Width="700"
                                    Header="false" HideHeaders="true">
                                    <Store>
                                        <ext:Store ID="storeNoticeBoard" runat="server">
                                            <Model>
                                                <ext:Model ID="model1" runat="server">
                                                    <Fields>
                                                        <ext:ModelField Name="NoticeID" />
                                                        <ext:ModelField Name="Title" />
                                                        <ext:ModelField Name="PublishedDateStr" />
                                                        <ext:ModelField Name="FileName" />
                                                        <ext:ModelField Name="URL" />
                                                        <ext:ModelField Name="StatusStr" />
                                                    </Fields>
                                                </ext:Model>
                                            </Model>
                                        </ext:Store>
                                    </Store>
                                    <ColumnModel>
                                        <Columns>
                                            <ext:TemplateColumn ID="TemplateColumn2" runat="server" MenuDisabled="true" Header="Template"
                                                Width="500">
                                                <Template ID="Template2" runat="server" Width="600">
                                                    <Html>
                                                        <tpl for=".">
                                                               
                                                                <table style="width:600px;">
                                                                    <tr>
                                                                        <td>
                                                                            <div style="width:80px;">
                                                                                <%--{PublishedDateStr:date("d-m-Y")}--%>
                                                                                {PublishedDateStr}
                                                                            </div>
                                                                        </td>
                                                                        <td>
                                                                            <div style="width:350px;">
                                                                                {Title}
                                                                            </div>
                                                                        </td>
                                                                        <%--<td>
                                                                            <div style="width:100px;">
                                                                                <a href="">View Detail</a> 
                                                                            </div>
                                                                        </td>--%>
                                                                    </tr>

                                                                </table>

                                                         </tpl>
                                                    </Html>
                                                </Template>
                                            </ext:TemplateColumn>
                                            <ext:TemplateColumn ID="TemplateColumn4" runat="server" Text="Attachment" Width="50"
                                                DataIndex="NoticeID" TemplateString='<tpl for="."><tpl if="FileName.trim().length!=0"><a href="../NoticeFileDownloader.ashx?id={URL}"> <img src="../images/paperclip.png" width="15" height="15" title="{FileName}"/></a></tpl></tpl>' />
                                            <ext:TemplateColumn ID="TemplateColumn3" runat="server" Text="Action" Width="100"
                                                DataIndex="NoticeID" TemplateString='<a href="NoticeDetail.aspx?id={NoticeID}"> View Detail</a>' />
                                        </Columns>
                                    </ColumnModel>
                                </ext:GridPanel>
                            </div>
                        </div>
                    </div>



                    <%--<div style="margin-top: 10px" id="regreq" runat="server">
                        <div class="blockBottomBorder">
                            <img src="images/notice-small.png" width="20" height="20" />
                            <span class="blockTitle">Resignation requests</span> <span class="blockText"><a href="NoticeList.aspx">View All</a></span>
                        </div>
                        <div>
                            <div style="padding-left: 87px; font-size: 13px;" class='noticeContainer'>
                                <ext:GridPanel ID="GridPanel1" StyleSpec="border-top:0px;" runat="server" Width="700"
                                    Header="false" HideHeaders="true">
                                    <Store>
                                        <ext:Store ID="storeResignationReqList" runat="server">
                                            <Model>
                                                <ext:Model ID="model5" runat="server">
                                                    <Fields>
                                                        <ext:ModelField Name="ID" />
                                                        <ext:ModelField Name="EmployeeName" />
                                                        <ext:ModelField Name="ExpectedDate" />
                                                        <ext:ModelField Name="DaysCount" />
                                                    </Fields>
                                                </ext:Model>
                                            </Model>
                                        </ext:Store>
                                    </Store>
                                    <ColumnModel>
                                        <Columns>
                                            <ext:TemplateColumn ID="TemplateColumn5" runat="server" MenuDisabled="true" Header="Template"
                                                Width="500">
                                                <Template ID="Template3" runat="server" Width="600">
                                                    <Html>
                                                        <tpl for=".">                                                               
                                                                <table style="width:600px;">
                                                                    <tr>
                                                                        <td>
                                                                            <div style="width:80px;">
                                                                                {EmployeeName} has resigned,
                                                                            </div>
                                                                        </td>
                                                                        <td>
                                                                            <div style="width:350px;">
                                                                                {DaysCount} days until effective. Review Finance clearance.
                                                                            </div>
                                                                        </td>                                                                                                                                 
                                                                    </tr>
                                                                </table>
                                                         </tpl>
                                                    </Html>
                                                </Template>
                                            </ext:TemplateColumn>
                                        </Columns>
                                    </ColumnModel>
                                </ext:GridPanel>
                            </div>
                        </div>
                    </div>--%>

                    <div style="margin-top: 10px">
                        <div class="blockBottomBorder">
                            <img src="images/mail.png" />
                            <span class="blockTitle">Messages</span> <span class="blockText">for you</span>
                        </div>
                        <div>
                            <div style="padding-left: 87px; font-size: 13px;">
                                <asp:Repeater runat="server" ID="rptMessages">
                                    <HeaderTemplate>
                                        <table class='leaveList'>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td>
                                                <span style="font-weight: bold">
                                                    <%# Eval("SendByName")%></span> Says
                                            </td>
                                            <td>&#9658;
                                                <%# Eval("Subject")%>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </div>
                        </div>
                    </div>
                    <div style="margin-top: 10px">
                        <div class="blockBottomBorder">
                            <img src="images/leave.png" />
                            <span class="blockTitle">Leave</span> <span class="blockText">request</span>
                        </div>
                        <div>
                            <div style="padding-left: 87px; font-size: 13px;">
                                <asp:Repeater runat="server" ID="rptRequestedLeaveList">
                                    <HeaderTemplate>
                                        <table class='leaveList'>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td>
                                                <span style="font-weight: bold">
                                                    <%# Eval("EmployeeName") %></span>
                                            </td>
                                            <td>&#9658; Has requested <span style="font-weight: bold">
                                                <%# string.Format("{0:0.##}", Eval("DaysOrHours").ToString()) %></span>
                                                <%# BLL.Manager.LeaveAttendanceManager.DaysOrHourTitle.ToLower()%>
                                                <span style="color: #C65911; font-weight: bold">
                                                    <%#Eval("Title") %></span> starting <span style="font-weight: bold">
                                                        <%# Eval("FromDate", "{0:dddd, MMMM d, yyyy}").ToString().Replace("12:00AM", "")%></span>
                                            </td>
                                            <td>
                                                <asp:HyperLink runat="server" NavigateUrl='<%# "~/Employee/LeaveApproval.aspx#" +  Eval("LeaveRequestId") %>'
                                                    Text="Review Request" />
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </div>
                        </div>
                    </div>
                    <div style="margin-top: 10px; margin-left: -7px;" id="divTimeAttRejectComment" runat="server"
                        visible="false">
                        <div class="blockBottomBorder">
                            <img src="images/clock.png" />
                            <asp:Label ID="lblTimeAttRejectedComment" runat="server" CssClass="lblClass" />
                        </div>
                    </div>
                    <div style="margin-top: 15px" class="blockBottomBorder">
                        <img src="images/happening.png" />
                        <span class="blockTitle">Happening</span> <span class="blockText">in the company</span>
                    </div>
                    <div class="weekBlock">
                        THIS WEEK
                    </div>
                    <div style="padding-left: 87px; font-size: 13px;">
                        <asp:Repeater runat="server" ID="rptThisWeek">
                            <HeaderTemplate>
                                <table>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td>
                                        <span style="font-weight: bold">
                                            <%# Eval("Text") %></span>
                                    </td>
                                    <td>&#9658;
                                        <%# Eval("Value") %>
                                        <%# Eval("Other") %>
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </table>
                            </FooterTemplate>
                        </asp:Repeater>
                    </div>
                    <div class="weekBlock">
                        NEXT 30 DAYS
                    </div>
                    <div style="padding-left: 87px; font-size: 13px;">
                        <asp:Repeater runat="server" ID="rpt30Days">
                            <HeaderTemplate>
                                <table>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td>
                                        <span style="font-weight: bold">
                                            <%# Eval("Text") %></span>
                                    </td>
                                    <td>&#9658;
                                        <%# Eval("Value") %>
                                        <%# Eval("Other") %>
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </table>
                            </FooterTemplate>
                        </asp:Repeater>
                    </div>
                    <div runat="server" id="divNewJoinesAndRetiring" visible="false">
                        <div class="blockBottomBorder" style="padding-top: 15px;">
                            <img src="images/leave.png" />
                            <span class="blockText">New Joines</span>
                        </div>
                        <div>
                            <ext:GridPanel StyleSpec="margin-top:15px;" ID="gridNewJoines" runat="server" Cls="itemgrid">
                                <Store>
                                    <ext:Store ID="Store3" runat="server">
                                        <Model>
                                            <ext:Model ID="Model4" runat="server" IDProperty="EmployeeId">
                                                <Fields>
                                                    <ext:ModelField Name="EmployeeId" Type="string" />

                                                    <ext:ModelField Name="Name" Type="string" />
                                                    <ext:ModelField Name="Branch" Type="string" />
                                                    <ext:ModelField Name="Department" Type="string" />


                                                    <ext:ModelField Name="JoinDateEng" Type="string" />

                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <ColumnModel>
                                    <Columns>


                                          <ext:RowNumbererColumn runat="server" Text="SN" Width="30" Align="Center"    />
                                        <ext:Column ID="Column8" Sortable="false" MenuDisabled="true" runat="server" Text="Employee"
                                            Align="Left" Width="180" DataIndex="Name" />

                                        <ext:Column ID="ColumnBranch" Width="150" Sortable="false" MenuDisabled="true" runat="server"
                                            Text="Branch" Align="Left" DataIndex="Branch">
                                        </ext:Column>
                                        <ext:Column ID="ColumnDepartment" Width="150" Sortable="false" MenuDisabled="true"
                                            runat="server" Text="Depratment" Align="Left" DataIndex="Department">
                                        </ext:Column>
                                        <ext:DateColumn ID="columnJoinDate1" Width="120" Flex="1" Format="yyyy-MMM-d" Sortable="false" MenuDisabled="true"
                                            runat="server" Text="Join Date" Align="Left" DataIndex="JoinDateEng">
                                        </ext:DateColumn>

                                    </Columns>
                                </ColumnModel>
                                <SelectionModel>
                                    <ext:RowSelectionModel ID="RowSelectionModel2111" runat="server" Mode="Single" />
                                </SelectionModel>
                            </ext:GridPanel>
                        </div>
                         <div class="blockBottomBorder" style="padding-top: 15px;">
                            <img src="images/leave.png" />
                            <span class="blockText">Resigned List</span>
                        </div>
                        <div>
                            <ext:GridPanel StyleSpec="margin-top:15px;" ID="gridRetiring" runat="server" Cls="itemgrid">
                                <Store>
                                    <ext:Store ID="Store1" runat="server">
                                        <Model>
                                            <ext:Model ID="Model3" runat="server" IDProperty="EmployeeId">
                                                <Fields>
                                                    <ext:ModelField Name="EmployeeId" Type="string" />

                                                    <ext:ModelField Name="Name" Type="string" />
                                                    <ext:ModelField Name="Branch" Type="string" />
                                                    <ext:ModelField Name="Department" Type="string" />


                                                    <ext:ModelField Name="RetiringDateEng" Type="string" />

                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <ColumnModel>
                                    <Columns>
                                        

                                        <ext:RowNumbererColumn runat="server" Text="SN" Width="30" Align="Center"    />
                                        <ext:Column ID="Column1" Sortable="false" MenuDisabled="true" runat="server" Text="Employee"
                                            Align="Left" Width="180" DataIndex="Name" />

                                        <ext:Column ID="Column3" Width="150" Sortable="false" MenuDisabled="true" runat="server"
                                            Text="Branch" Align="Left" DataIndex="Branch">
                                        </ext:Column>
                                        <ext:Column ID="Column4" Width="150" Sortable="false" MenuDisabled="true"
                                            runat="server" Text="Depratment" Align="Left" DataIndex="Department">
                                        </ext:Column>
                                        <ext:DateColumn ID="DateColumn1" Width="120" Flex="1" Format="yyyy-MMM-d" Sortable="false" MenuDisabled="true"
                                            runat="server" Text="Date" Align="Left" DataIndex="RetiringDateEng">
                                        </ext:DateColumn>

                                    </Columns>
                                </ColumnModel>
                                <SelectionModel>
                                    <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" Mode="Single" />
                                </SelectionModel>
                            </ext:GridPanel>
                        </div>
                    </div>
                </td>
                <td style="width: 20px">&nbsp;
                </td>
                <td valign="top" style="width: 250px; padding-top: 20px;">
                    <div>
                        <div>
                            <div style="margin-left: 10px;">
                                <div style="margin-top: 5px; margin-bottom: 5px">
                                    <ext:Label runat="server" Width="200" Height="30" ID="lblCalendarTitle" Text="&nbsp;" />
                                </div>
                                <ext:DatePicker ID="DatePickerWithHolidayDisplay" runat="server" Width="215" StyleSpec="border:1px solid #A3BAD9;">
                                    <Listeners>
                                        <Select Fn="CompanyX.setStartDate" Scope="CompanyX" />
                                    </Listeners>
                                    <Plugins>
                                        <ext:GenericPlugin ID="HighlightDates1" runat="server" InstanceName="Ext.ux.DatePicker.HighlightDates" />
                                    </Plugins>
                                </ext:DatePicker>
                            </div>
                            <div class="blockBottomBorder" style="padding-top: 5px">
                                <span class="blockTitle">Upcoming Holidays</span> <span style="padding-left: 20px"
                                    class="blockText"><a href="AllHolidayReport.aspx">View All</a></span>
                            </div>
                            <ext:GridPanel EmptyText="there are no immediate holidays." ID="GridPanelMYTimesheet"
                                runat="server" Width="245" Header="false" HideHeaders="true">
                                <Store>
                                    <ext:Store ID="storeMYTimesheet" runat="server">
                                        <Model>
                                            <ext:Model ID="model2" runat="server">
                                                <Fields>
                                                    <ext:ModelField Name="EngDateLongForDashboard" Type="String">
                                                    </ext:ModelField>
                                                    <ext:ModelField Name="Id" Type="String">
                                                    </ext:ModelField>
                                                    <ext:ModelField Name="Name" Type="String">
                                                    </ext:ModelField>
                                                    <ext:ModelField Name="NepDateLongForDashboard" Type="String">
                                                    </ext:ModelField>
                                                    <ext:ModelField Name="DaysLeftForDashboard" Type="String">
                                                    </ext:ModelField>
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <ColumnModel>
                                    <Columns>
                                        <ext:TemplateColumn ID="TemplateColumn1" runat="server" MenuDisabled="true" Header="Template"
                                            Width="245">
                                            <Template ID="Template1" runat="server" Width="245">
                                                <Html>
                                                    <tpl for=".">
                                                               
                                                                <table style="width:245px;">
                                                                    <tr>
                                                                        <td>
                                                                            <div style="width:120px;">
                                                                                {EngDateLongForDashboard}
                                                                            </div>
                                                                        </td>
                                                                        <td>
                                                                            <div style="width:175px;">
                                                                                {Name}
                                                                            </div>
                                                                        </td>
                                                                    </tr>

                                                                    <tr>
                                                                        <td>
                                                                            <div style="width:120px;">
                                                                                {NepDateLongForDashboard}
                                                                            </div>
                                                                        </td>
                                                                        <td>
                                                                            <div style="width:120px;">
                                                                                {DaysLeftForDashboard}
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                </table>

                                                         </tpl>
                                                </Html>
                                            </Template>
                                        </ext:TemplateColumn>
                                    </Columns>
                                </ColumnModel>
                            </ext:GridPanel>
                        </div>
                    </div>
                    <div class="widget" runat="server" id="blockOvertime" visible="false">
                        <div class="widget-head">
                            <h4 class="heading glyphicons history">
                                <i></i>Overtime</h4>
                            <a href="AApproveOvertime.aspx" class="details pull-right">view all</a>
                        </div>
                        <div class="widget-body list">
                            <ul>
                                <li runat="server" id="overtimeRecommendLi"><span>Pending</span> <span runat="server"
                                    id="overtimeRecommend" class="count">0</span> </li>
                                <li runat="server" id="overtimeApproveLi"><span>Recommended</span> <span class="count"
                                    runat="server" id="overtimeApprove">0</span> </li>
                            </ul>
                        </div>
                    </div>
                    <div class="widget" runat="server" id="blockEveningCounter" visible="false">
                        <div class="widget-head">
                            <h4 class="heading glyphicons history">
                                <i></i>Allowance</h4>
                            <a href="AllowanceApprove.aspx" class="details pull-right">view all</a>
                        </div>
                        <div class="widget-body list">
                            <ul>
                                <li runat="server" id="eveningCounterRecommnededLi"><span>Pending</span> <span runat="server"
                                    id="eveningCounterRecommneded" class="count">0</span> </li>
                                <li runat="server" id="eveningCounterApproveLi"><span>Recommended</span> <span class="count"
                                    runat="server" id="eveningCounterApprove">0</span> </li>
                            </ul>
                        </div>
                    </div>
                    <div class="widget" runat="server" id="blockTravelRequestReview" visible="false">
                        <div class="widget-head">
                            <h4 class="heading glyphicons history">
                                <i></i>Travel Requests</h4>
                            <a href="TravelRequests/TravelRequestHRView.aspx" class="details pull-right">view all</a>
                        </div>
                        <div class="widget-body list">
                            <ul>
                                <li runat="server" id="liTravelRequests"><span>Review</span> <span runat="server"
                                    id="spanTravelRequest" class="count">0</span> </li>
                            </ul>
                        </div>
                    </div>
                    <div class="widget" runat="server" id="blockTADA" visible="false">
                        <div class="widget-head">
                            <h4 class="heading glyphicons history">
                                <i></i>TADA</h4>
                            <a href="TADA/TADAReviewList.aspx" class="details pull-right">view all</a>
                        </div>
                        <div class="widget-body list">
                            <ul>
                                <li runat="server" id="tadaRecommendLi"><span>Pending</span> <span runat="server"
                                    id="tadaRecommend" class="count">0</span> </li>
                                <li runat="server" id="tadaApproveLi"><span>Recommended</span> <span class="count"
                                    runat="server" id="tadaApprove">0</span> </li>
                            </ul>
                        </div>
                    </div>
                    <div class="widget" runat="server" id="blockTimeRequest" visible="false">
                        <div class="widget-head">
                            <h4 class="heading glyphicons history">
                                <i></i>Time Request</h4>
                            <a href="ApproveTimeAttendance.aspx" class="details pull-right">view all</a>
                        </div>
                        <div class="widget-body list">
                            <ul>
                                <li runat="server" id="liRequestTimeRequest"><span>Pending</span> <span runat="server"
                                    id="spanRequestTimeRequest" class="count">0</span> </li>
                                <li runat="server" id="liRecommendedTimeRequest"><span>Recommended</span> <span class="count"
                                    runat="server" id="spanRecommendedTimeRequest">0</span> </li>
                            </ul>
                        </div>
                    </div>
                    <table runat="server" id="tblActivity" visible="false">
                        <tr>
                            <td>
                                <div class="widget" runat="server" id="Div2" visible="true" style="width: 300px; margin-top: 10px; border-color: transparent; float: left;">
                                    <div class="widget-head" style="background-color: #5B9BD5">
                                        <h4 class="heading">
                                            <i></i>Your Activity Report</h4>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="widget" runat="server" id="Div1" visible="true" style="width: 300px; margin-top: -20px; border-color: transparent; float: left;">
                                    <div class="widget-head" style="background-color: #DCEAF6; border-color: transparent;">
                                        <h4 class="heading" style="color: Black;">
                                            <i></i>Today</h4>
                                    </div>
                                    <div runat="server" id="divTodayPresent" class="widget-body list" style="background-color: #E2EFDA; border-color: transparent; color: #6E9454;">
                                        <img src="../images/likeNew.png" style="margin-left: -40px; width: 20px; height: 20px;" />
                                        Nice, All Done
                                    </div>
                                    <div runat="server" id="divTodayAbsent" visible="false" class="widget-body list"
                                        style="background-color: #F4B084; border-color: transparent; color: brown;">
                                        <img src="../images/infoNew.png" style="margin-left: -40px; width: 20px; height: 20px;" />
                                        Not Yet Submitted
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr runat="server" id="trTodaySubmit">
                            <td>
                                <a id="A1" href="~/Employee/EmployeeActivity.aspx" runat="server">Submit now</a>
                            </td>
                        </tr>
                        <tr id="trYesterday" runat="server">
                            <td>
                                <div class="widget" runat="server" id="Div3" visible="true" style="width: 300px; margin-top: 2px; border-color: transparent; float: left;">
                                    <div class="widget-head" style="background-color: #DCEAF6; border-color: transparent;">
                                        <h4 class="heading" style="color: Black;">
                                            <i></i>Yesterday</h4>
                                    </div>
                                    <div runat="server" id="divYesterdayPresent" class="widget-body list" style="background-color: #E2EFDA; border-color: transparent; color: #6E9454;">
                                        <img src="../images/likeNew.png" style="margin-left: -60px; width: 20px; height: 20px;" />
                                        Nice, All Done
                                    </div>
                                    <div runat="server" id="divYesterdayAbsent" visible="false" class="widget-body list"
                                        style="background-color: #F4B084; border-color: transparent; color: brown;">
                                        <img src="../images/infoNew.png" style="margin-left: -60px; width: 20px; height: 20px;" />
                                        Not Yet Submitted
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr runat="server" id="trYesterdaySubmit">
                            <td>
                                <a id="A2" href="~/Employee/EmployeeActivity.aspx" runat="server">Submit now</a>
                            </td>
                        </tr>
                        <tr runat="server" id="trHRComment">
                            <td>
                                <a id="hrComment" runat="server" title="Click to save your comment." onclick="activityComment();"></a>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <%--HPL duty list--%>
        <div runat="server" id="divHPLDuty">
            <span style='margin-top: 20px; display: block; margin-bottom: 15px; font-weight: bold;'>Employee Duty Schedule</span>
            <ext:GridPanel StyleSpec="margin-top:5px;" ID="gridSchedule" runat="server" Cls="itemgrid"
                Width="870">
                <Store>
                    <ext:Store ID="storeSchedule" runat="server">
                        <Model>
                            <ext:Model ID="Model6" runat="server" IDProperty="ScheduleId">
                                <Fields>
                                    <ext:ModelField Name="ScheduleId" Type="String" />
                                    <ext:ModelField Name="EmployeeId" Type="String" />
                                    <ext:ModelField Name="Name" Type="String" />
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                <ColumnModel>
                    <Columns>
                        <ext:Column ID="Column2" Sortable="false" MenuDisabled="true" runat="server" Text="EIN"
                            Width="40" Align="Center" DataIndex="EmployeeId">
                        </ext:Column>
                        <ext:Column ID="colEmployeeName" Sortable="false" MenuDisabled="true" runat="server"
                            Text="Name" Width="139" Align="Left" DataIndex="Name">
                        </ext:Column>
                    </Columns>
                </ColumnModel>
                <SelectionModel>
                    <ext:RowSelectionModel ID="RowSelectionModel2" runat="server" Mode="Single" />
                </SelectionModel>
                <Plugins>
                    <ext:CellEditing ID="CellEditing1" ClicksToEdit="1" runat="server">
                        <%--<Listeners>                              
                                                                    <Edit Fn="afterEdit" />
                                                                </Listeners>--%>
                    </ext:CellEditing>
                </Plugins>
            </ext:GridPanel>
        </div>
    </div>
    <ext:Window ID="WPasswordNotification" runat="server" Title="Password Change Notification"
        Icon="None" Height="150" Width="300" BodyPadding="5" Hidden="true" Modal="true">
        <Content>
            <div style="background-color: White; height: 150px; width: 300px;">
                <br />
                <p style="margin-left: 20px; padding-bottom: 5px;">
                    <b>Your password has expired.</b>
                </p>
                <b><a style="margin-left: 20px; text-decoration: underline;" href="ChangePassword.aspx">Click here</a> to change your password.</b>
            </div>
        </Content>
    </ext:Window>
    <ext:Window ID="WCheckInOut" runat="server" Title="Check In" Icon="Application" Frame="true"
        Resizable="false" Visible="false" Width="400" Height="260" BodyPadding="5" Hidden="true"
        Modal="true">
        <Content>
            <table class="fieldTable" style="margin-left: 15px;">
                <tr>
                    <td style="color: Green;">
                        <ext:DisplayField ID="dfMessage" runat="server" FieldStyle="color:Green;" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <ext:DisplayField ID="dfLoginDateTime" runat="server" FieldStyle="color:#6AAFE0; background-color:#DBE9F5; padding-left:5px; height:25px; padding-top:3px; padding-right:30px" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <ext:TextField ID="txtNote" runat="server" FieldLabel="Note" Text="" LabelAlign="Top"
                            LabelSeparator="" Width="300px" />
                    </td>
                </tr>
            </table>
            <div class="popupButtonDiv bottomBlock" style="padding-left: 35px;">
                <ext:Button runat="server" ID="btnCheckIn" Cls="btn btn-primary" Text="<i></i>Check In">
                    <DirectEvents>
                        <Click OnEvent="btnCheckIn_Click">
                            <EventMask ShowMask="true" />
                        </Click>
                    </DirectEvents>
                </ext:Button>
                <ext:Button runat="server" ID="btnCheckOut" Cls="btn btn-primary" Text="<i></i>Check Out">
                    <DirectEvents>
                        <Click OnEvent="btnCheckIn_Click">
                            <EventMask ShowMask="true" />
                        </Click>
                    </DirectEvents>
                </ext:Button>
                <div class="btnFlatOr" style="width: 10px;">
                </div>
                <ext:Button runat="server" ID="btnCheckInCancel" Cls="btn btn-primary" Text="<i></i>Cancel">
                    <DirectEvents>
                        <Click OnEvent="btnCheckInCancel_Click">
                            <EventMask ShowMask="true" />
                        </Click>
                    </DirectEvents>
                </ext:Button>
                <ext:Button runat="server" ID="btnCheckOutCancel" Cls="btn btn-primary" Text="<i></i>Cancel">
                    <DirectEvents>
                        <Click OnEvent="btnCheckInCancel_Click">
                            <EventMask ShowMask="true" />
                        </Click>
                    </DirectEvents>
                </ext:Button>
            </div>
        </Content>
    </ext:Window>

    <script type="text/javascript">
        var stub = "Your Password has been changed.";
        function ShowMsg() {
            Ext.net.Notification.show({
                iconCls: 'icon-information',
                pinEvent: 'click',
                html: 'Your Password has been changed.',
                title: 'Title'
            });
        }


        function activityComment() {

            <%= btnShowActivityWindow.ClientID %>.fireEvent('click');
            return false;
    
        }

       var rendererClass = function(value, meta, record) {
            meta.tdCls = 'blueCls';
            return value;
        }; 
        function searchList()
        {
        }
    </script>


</asp:Content>
