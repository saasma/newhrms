<%@ Page Title="Date Converter" Language="C#" MasterPageFile="~/Master/ForEmployeePopupPage.Master"
    AutoEventWireup="true" CodeBehind="DateConverter.aspx.cs" Inherits="Web.Employee.DateConverter" %>

<%@ Register Src="~/Controls/CalendarCtl.ascx" TagPrefix="uc1" TagName="Calendar" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        var skipLoadingCheck = true;
        function closePopup() {

            if (hasImport)
                window.opener.refreshWindow(); ;
        }

        window.onunload = closePopup; 
    </script>
    <style type="text/css">
        html
        {
            margin-top: -20px !important;
        }
        
        input[type=text], textarea, select
        {
            border: 1px solid #A2B4C6;
        }
        
        .popupHeader
        {
            height:auto!important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
    <ext:ResourceManager runat="server" DisableViewState="false" />
    <div class="popupHeader">
        <h3>
            Date Converter</h3>
    </div>
    <div class=" marginal" style='margin-top: 10px'>
        <ext:ComboBox ID="cmb" runat="server" StyleSpec="margin-bottom:10px" FieldLabel="Type"
            ForceSelection="true" LabelAlign="Top" Width="165">
            <Items>
                <ext:ListItem Text="English to Nepali" Value="1" />
                <ext:ListItem Text="Nepali to English" Value="2" />
            </Items>
            <DirectEvents>
                <Select OnEvent="cmb_Select">
                    <EventMask ShowMask="true">
                    </EventMask>
                </Select>
            </DirectEvents>
        </ext:ComboBox>
        <ext:Container runat="server" ID="divEng">
            <Content>
                English:<div style="margin-top: 6px">
                </div>
                <uc1:Calendar Id="calEng" runat="server" IsEnglishCalendar="true" />
            </Content>
        </ext:Container>
        <ext:Container runat="server" ID="divNep">
            <Content>
                Nepali:<div style="margin-top: 6px">
                </div>
                <uc1:Calendar Id="calNep" runat="server" IsEnglishCalendar="false" />
            </Content>
        </ext:Container>
        <%--  <ext:TextField ID="txtEngCalender1" LabelSeparator="" runat="server" FieldLabel="English (dd/mm/yyy)"
            LabelAlign="Top" Width="165">
            <CustomConfig>
                <ext:ConfigItem Name="ShowWhenEmpty" Value="true" Mode="Value" />
            </CustomConfig>
        </ext:TextField>--%>
        <ext:Button runat="server" ID="btnConvert" Text="Convert" StyleSpec="margin-top:10px;margin-bottom:10px;">
            <DirectEvents>
                <Click OnEvent="btnConvert_Click">
                    <EventMask ShowMask="true" />
                </Click>
            </DirectEvents>
        </ext:Button>
        <table>
            <tr>
                <td>
                    <ext:TextField LabelAlign="Top" Width="165" ID="txtNep" runat="server" FieldLabel="Nepali">
                    </ext:TextField>
                    <ext:TextField LabelAlign="Top" Width="165" ID="txtEng" runat="server" FieldLabel="English">
                    </ext:TextField>
                </td>
                <td style="padding-left: 15px; padding-top: 15px">
                    <ext:Label ID="lblConertedDate" StyleSpec="font-size:14px" runat="server" />
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
