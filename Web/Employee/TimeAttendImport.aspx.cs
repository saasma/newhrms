﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using BLL;
using System.Data;
using System.IO;
using BLL.Manager;
using DAL;
using BLL.Entity;
using System.Data.OleDb;
using BLL.BO;

namespace Web.Employee
{
    public partial class TimeAttendImport : BasePage
    {
        public bool HasImport
        {
            get
            {
                if (ViewState["import"] == null)
                    return false;
                return Convert.ToBoolean(ViewState["import"]);
            }
            set
            {
                ViewState["import"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "sdf", "var hasImport = " + (this.HasImport ? "true;" : "false;"), true);
        }




        protected void btnExport_Click(object sender, EventArgs e)
        {
            string template = ("~/App_Data/ExcelTemplate/AssignTimeAttend.xls");

            ExcelGenerator.ExportTimeAttendance(template, "TimeAttendance" + ".xls");

        }


        protected void btnImport_Click(object sender, EventArgs e)
        {
            string template = Server.MapPath("~/App_Data/ExcelTemplate/" + Guid.NewGuid() + ".xls");


            List<TextValue> listTextValue = LeaveAttendanceManager.GetEmployeeListForLeaveAssignWithID();

            List<TimeRequestLine> list = new List<TimeRequestLine>();

            if (fupUpload.HasFile && fupUpload.FileName.ToLower().Contains(".xls"))
            {
                fupUpload.SaveAs(template);

                DataTable datatable = ExcelGenerator.ImportFromExcel(template, "select * from [Sheet1$]");

                if (datatable == null)
                {
                    divWarningMsg.InnerHtml = Resources.Messages.HPL_Allowance_ImportFail;
                    divWarningMsg.Hide = false;
                    File.Delete(template);
                    return;
                }

                int importedCount = 0;

                try
                {
                    int rowNumber = 0;

                    EEmployee objEmp = new EEmployee();

                    foreach (DataRow row in datatable.Rows)
                    {
                        TimeRequestLine objTimeRequestLine = new TimeRequestLine(); 

                        rowNumber += 1;


                        if (row["Employee"] != DBNull.Value && row["Employee"].ToString() != "")
                        {
                            try
                            {
                                objTimeRequestLine.Value = listTextValue.SingleOrDefault(x => x.Text.Trim().ToLower() == row["Employee"].ToString().Trim().ToLower()).Value;
                                objTimeRequestLine.Text = row["Employee"].ToString().Trim().Split('-')[0].Trim();
                            }
                            catch (Exception ex)
                            {
                                divWarningMsg.InnerHtml = "Please select proper employee for the row : " + rowNumber;
                                divWarningMsg.Hide = false;
                                return;
                            }

                        }
                        else
                        {
                            divWarningMsg.InnerHtml = "Employee is required for the row : " + rowNumber;
                            divWarningMsg.Hide = false;
                            return;
                        }

                        importedCount += 1;

                        DateTime date = new DateTime();

                        if (row["Date"] != DBNull.Value && row["Date"].ToString() != "")
                        {
                            try
                            {
                                date = Convert.ToDateTime(row["Date"].ToString());
                                objTimeRequestLine.DateEng = date;
                                objTimeRequestLine.DateName = BLL.BaseBiz.GetAppropriateDate(objTimeRequestLine.DateEng.Value);
                            }
                            catch
                            {

                                divWarningMsg.InnerHtml = "Date is invalid for the row : " + rowNumber;
                                divWarningMsg.Hide = false;
                                return;
                            }
                        }
                        else
                        {
                            divWarningMsg.InnerHtml = "Date is required for the row : " + rowNumber;
                            divWarningMsg.Hide = false;
                            return;
                        }

                        if (row["Start Time"] != DBNull.Value && row["Start Time"].ToString() != "")
                        {
                            try
                            {
                                DateTime dtStart = DateTime.Parse(row["Start Time"].ToString());
                                int hoursStart = dtStart.Hour;
                                int minutesStart = dtStart.Minute;
                                objTimeRequestLine.InTime = new TimeSpan(hoursStart, minutesStart, 0);
                                objTimeRequestLine.InTimeDT = new DateTime(date.Year, date.Month, date.Day, hoursStart, minutesStart, 0);
                            }
                            catch
                            {

                                divWarningMsg.InnerHtml = "Start Time is invalid for the row : " + rowNumber;
                                divWarningMsg.Hide = false;
                                return;
                            }
                        }
                        
                        if (row["End Time"] != DBNull.Value && row["End Time"].ToString() != "")
                        {
                            try
                            {
                                DateTime dtEnd = DateTime.Parse(row["End Time"].ToString());
                                int hoursStart = dtEnd.Hour;
                                int minutesStart = dtEnd.Minute;
                                objTimeRequestLine.OutTime = new TimeSpan(hoursStart, minutesStart, 0);
                                objTimeRequestLine.OutTimeDT = new DateTime(date.Year, date.Month, date.Day, hoursStart, minutesStart, 0);
                            }
                            catch
                            {

                                divWarningMsg.InnerHtml = "End Time is invalid for the row : " + rowNumber;
                                divWarningMsg.Hide = false;
                                return;
                            }
                        }                       

                        if (objTimeRequestLine.InTime == null && objTimeRequestLine.OutTime == null)
                        {
                            divWarningMsg.InnerHtml = "Start Time or End Time is required for the row : " + rowNumber;
                            divWarningMsg.Hide = false;
                            return;

                        }

                        if (row["In Note"] != DBNull.Value && row["In Note"].ToString() != "")
                        {
                            objTimeRequestLine.InNote = row["In Note"].ToString();
                        }

                        if (row["Out Note"] != DBNull.Value && row["Out Note"].ToString() != "")
                        {
                            objTimeRequestLine.InNote = row["Out Note"].ToString();
                        }

                        objTimeRequestLine.SN = importedCount;
                        list.Add(objTimeRequestLine);
                    }

                    if (list.Count > 0)
                    {
                        Session["AssignTAImp"] = list;
                        divMsgCtl.InnerHtml = "Time attendance imported successfully.";
                        divMsgCtl.Hide = false;

                        this.HasImport = true;
                    }
                    else
                    {
                        divWarningMsg.InnerHtml = "Error while importing time attendance.";
                        divWarningMsg.Hide = false;
                    }


                }

                    //sheet name changed error
                catch (OleDbException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_SheetNameError);
                    divWarningMsg.Hide = false;
                }
                catch (ArgumentException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_ColumnNameChangedError);
                    divWarningMsg.Hide = false;
                }
            }
        }
    }
}