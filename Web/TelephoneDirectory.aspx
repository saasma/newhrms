<%@ Page Title="Telephone Directory" Language="C#" 
    AutoEventWireup="true" MaintainScrollPositionOnPostback="true" CodeBehind="TelephoneDirectory.aspx.cs"
    Inherits="Web.TelephoneDirectory" %>

<%@ Register Src="~/Controls/PagingCtl.ascx" TagName="Paging" TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Telephone Directory</title>
    <link href="css/style.default.css" rel="stylesheet" type="text/css" />
    <link href="css/core.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        function ACE_item_selected(source, eventArgs) {
            var value = eventArgs.get_value();
        }
    </script>

    <style type="text/css">
        .txtClass
        {
            color: White;
        }
        .attribute
        {
            float: none !important;
        }
        input[type=text]
        {
            height: 24px;
        }
        input, button, select, textarea
        {
            font-family: inherit;
            font-size: inherit;
            line-height: inherit;
        }
        select
        {
            height: 24px;
        }
        .pageheader h4
        {
            color: #1C69B5 !important;
        }
        .pageheader
        {
            height: auto;
        }
        
        .sortLink a
        {
            color: #333333;
        }
        .searchBox{
		color:#999999;
        }
    </style>
</head>
<body>
    <form id="asd" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" ScriptMode="Release">
    </asp:ScriptManager>
    <asp:HiddenField ID="hdnSortField" runat="server" />
    <asp:HiddenField ID="hdnSortDir" runat="server" />
    <asp:HiddenField ID="hdnSorby" runat="server" />
    <div class="pageheader" style='padding-top: 5px; padding-bottom: 5px;'>
        <div class="media">
            <div class="media-body">
                <h4>
                    Telephone Directory
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <div>
            <div class="attribute" style="padding: 10px">
                <table>
                    <tr>
                        <td>
                            <asp:TextBox ID="txtUserNameSearch" Width="180px" runat="server" OnTextChanged="txtSearch_Click"
                                AutoPostBack="true" />
                            <cc1:TextBoxWatermarkExtender ID="TBWE2" runat="server" TargetControlID="txtUserNameSearch"
                                WatermarkText="Employee Name" WatermarkCssClass="searchBoxText" />
                            <cc1:AutoCompleteExtender EnableCaching="true" ID="AutoCompleteExtenderOrganization"
                                runat="server" MinimumPrefixLength="2" ServiceMethod="GetEmployeeNames" ServicePath="~/PayrollService.asmx"
                                TargetControlID="txtUserNameSearch" OnClientItemSelected="ACE_item_selected"
                                CompletionSetCount="10" CompletionInterval="250" CompletionListCssClass="AutoExtender"
                                CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight">
                            </cc1:AutoCompleteExtender>
                            <asp:DropDownList DataValueField="BranchId" Width="150px" DataTextField="Name" ID="ddlBranch"
                                runat="server" AppendDataBoundItems="true" AutoPostBack="True" OnSelectedIndexChanged="ddlBranch_SelectedIndex">
                                <asp:ListItem Text="Everywhere" Value="-1" />
                            </asp:DropDownList>

                              <asp:DropDownList ID="ddlDepartment" Width="130px"  AutoPostBack="True" DataTextField="Name" DataValueField="DepartmentId" OnSelectedIndexChanged="ddlBranch_SelectedIndex"
                                runat="server" AppendDataBoundItems="true">
                                <asp:ListItem Text="All Department" Value="-1" />
                            </asp:DropDownList>
                        </td>
                        <td style="padding-left: 20px">
                            <asp:TextBox ID="txtSearch" runat="server" AutoPostBack="true"   OnTextChanged="txtSearch_Click" />
                            <cc1:TextBoxWatermarkExtender ID="asdfasdf" runat="server" TargetControlID="txtSearch"
                                WatermarkText="Search" WatermarkCssClass="searchBox"  />
                        </td>
                        <td style="padding-left: 20px">
                            <asp:Button ID="btnLoad" runat="server" Text="Load" OnClick="btnLoad_Click" CssClass="btn btn-primary btn-sect btn-sm"
                                Width="80px" />
                        </td>
                        <td style="padding-left: 20px">
                            <asp:Button runat="server" AutoPostBack="true" OnClick="btnExport_Click" ID="btnExport"
                                CssClass="excel marginRight" Text="Export To Excel" />
                        </td>
                    </tr>
                </table>
            </div>
            <div id="divGrid">
                <asp:GridView Style='margin-bottom: 0px' Width="100%" ShowHeaderWhenEmpty="True"
                    CssClass="table table-primary mb30 table-bordered table-hover" PagerStyle-CssClass="defaultPagingBar"
                    ID="gridDirectory" runat="server" DataKeyNames="EmployeeId" AllowSorting="True"
                    OnSorting="gvwEmployees_Sorting" AutoGenerateColumns="False" PagerStyle-HorizontalAlign="Center"
                    OnRowCreated="gvwEmployees_RowCreated" CellPadding="2" GridLines="None" ShowFooterWhenEmpty="False"
                    OnPageIndexChanged="gvwRoles_PageIndexChanged" OnPageIndexChanging="gvwRoles_PageIndexChanging">
                    <Columns>
                        <asp:BoundField HeaderText="Name" HeaderStyle-HorizontalAlign="Left" DataField="Name"
                            HeaderStyle-CssClass="sortLink" SortExpression="Name" />
                        <asp:BoundField HeaderText="Branch" HeaderStyle-HorizontalAlign="Left" DataField="Branch"
                            HeaderStyle-CssClass="sortLink" SortExpression="Branch" />
                        <asp:BoundField HeaderText="Department" HeaderStyle-HorizontalAlign="Left" DataField="Department"
                            HeaderStyle-CssClass="sortLink" SortExpression="Department" />
                        <asp:BoundField HeaderText="Location" HeaderStyle-HorizontalAlign="Left" DataField="Location"
                            HeaderStyle-CssClass="sortLink" SortExpression="Location" />
                       
                        <asp:BoundField HeaderText="Email" HeaderStyle-HorizontalAlign="Left" DataField="Email"
                            HeaderStyle-CssClass="sortLink" SortExpression="Email" />
                        <asp:BoundField HeaderText="Mobile" HeaderStyle-HorizontalAlign="Left" DataField="Mobile"
                            HeaderStyle-CssClass="sortLink" SortExpression="Mobile" />
                        <asp:BoundField HeaderText="Code" HeaderStyle-HorizontalAlign="Left" DataField="AreaCode"
                            HeaderStyle-CssClass="sortLink" SortExpression="AreaCode" />
                             <asp:BoundField HeaderText="Ext" HeaderStyle-HorizontalAlign="Left" DataField="Extension"
                            HeaderStyle-CssClass="sortLink" SortExpression="Extension" />

                        <asp:BoundField HeaderText="Phone" HeaderStyle-HorizontalAlign="Left" DataField="BranchPhone"
                            HeaderStyle-CssClass="sortLink" SortExpression="BranchPhone" />
                        <asp:BoundField HeaderText="Fax" HeaderStyle-HorizontalAlign="Left" DataField="Fax"
                            HeaderStyle-CssClass="sortLink" SortExpression="Fax" />
                        <asp:BoundField HeaderText="Direct Line" HeaderStyle-HorizontalAlign="Left" DataField="DirectLine"
                            HeaderStyle-CssClass="sortLink" SortExpression="DirectLine" />
                    </Columns>
                    <PagerSettings Mode="NumericFirstLast" NextPageText="Next �" PreviousPageText="� Prev"
                        FirstPageText="First" LastPageText="Last" />
                    <PagerStyle CssClass="defaultPagingBar" />
                    <EmptyDataTemplate>
                        <b>No Employee. </b>
                    </EmptyDataTemplate>
                </asp:GridView>
                <uc1:Paging ID="pagingCtl" OnNextRecord="btnNext_Click" OnPrevRecord="btnPrevious_Click"
                    OnDropDownShowRecordsChanged="ddlRecords_SelectedIndexChanged" runat="server" />
            </div>
        </div>
    </div>
    </form>
</body>
</html>
