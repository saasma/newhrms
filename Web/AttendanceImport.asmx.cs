﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

using DAL;
using System.Data.Linq;
using Library;
using BLL.Manager;
using Utils;
using BLL;
using Utils.Helper;
using System.Net.Mail;
using BLL.BO;


namespace Web
{
    /// <summary>
    /// Summary description for AttendanceImport
    /// </summary>
    [WebService(Namespace = "http://rigonepal.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class AttendanceImport : System.Web.Services.WebService
    {
        //public PayrollDataContext payrollDataContext;

       


        [WebMethod]
        public DateTime? GetLastDate( string ipAddress)
        {

            if (CommonManager.IsOldAPIWithoutKeyAccessible() == false)
                throw new Exception("No Permission");

            //if (CommonManager.IsAPIKeyValid(apiKey) == false)
            //    return null;

            AttendanceLastDate last = BLL.BaseBiz.PayrollDataContext.
                AttendanceLastDates.SingleOrDefault(x => x.IPAddress == ipAddress.ToLower());

            if (last != null)
                return last.LastDate;

            return null;
        }

        [WebMethod()]
        public string ImportAtte( List<CheckInOut> checkInOutList)
        {

            if (CommonManager.IsOldAPIWithoutKeyAccessible() == false)
                throw new Exception("No Permission");

            //if (CommonManager.IsAPIKeyValid(apiKey) == false)
            //    return null;

            try
            {

                if (checkInOutList.Count == 0)
                    return "";


                //using (PayrollDataContext payrollDataContext = new PayrollDataContext(Config.ConnectionString))
                {



                    // First remove duplication for same DeviceID,InOutMode and DateTime as sometime repeated data may come
                    List<CheckInOut> duplicatedRemovedList = new List<CheckInOut>();
                    for (int i = 0; i < checkInOutList.Count; i++)
                    {
                        CheckInOut item = checkInOutList[i];
                        if (duplicatedRemovedList.Any(x => x.DeviceID == item.DeviceID && x.DateTime == item.DateTime && x.InOutMode == item.InOutMode))
                        {
                        }
                        else
                            duplicatedRemovedList.Add(item);
                    }

                    checkInOutList = duplicatedRemovedList;


                    foreach (CheckInOut cIO in checkInOutList)
                    {

                        if (string.IsNullOrEmpty(cIO.DeviceID))
                            continue;

                        AttendanceCheckInCheckOut chkInOut = new AttendanceCheckInCheckOut();
                        chkInOut.ChkInOutID = Guid.NewGuid();
                        chkInOut.DeviceID = cIO.DeviceID;
                        chkInOut.AuthenticationType = byte.Parse(cIO.AuthenticationType.ToString());
                        chkInOut.InOutMode = byte.Parse(cIO.InOutMode.ToString());
                        chkInOut.DateTime = cIO.DateTime;
                        chkInOut.Date = chkInOut.DateTime.Date;
                        chkInOut.IPAddress = cIO.IPAddress;
                        chkInOut.ModifiedOn = DateTime.Now;


                        BLL.BaseBiz.PayrollDataContext.AttendanceCheckInCheckOuts.InsertOnSubmit(chkInOut);


                    }

                    foreach (string ipAddress in checkInOutList.Select(o => o.IPAddress).Distinct())
                    {
                        // Update last read date
                        AttendanceLastDate dbLastDate = BLL.BaseBiz.PayrollDataContext
                            .AttendanceLastDates.SingleOrDefault(x => x.IPAddress == ipAddress.ToLower());

                        DateTime AttLastDate = checkInOutList.OrderByDescending(x => x.DateTime)
                                   .Where(x => x.IPAddress == ipAddress.ToLower()).Take(1)
                                   .SingleOrDefault().DateTime;

                        DateTime TomorrrowDateTime = GetCurrentDateAndTime().AddHours(1);
                        if (AttLastDate < TomorrrowDateTime)
                        {
                            if (dbLastDate != null)
                            {
                                dbLastDate.LastDate = AttLastDate;

                            }
                            else
                            {
                                dbLastDate = new AttendanceLastDate();
                                dbLastDate.IPAddress = ipAddress.ToLower();
                                dbLastDate.LastDate = AttLastDate;
                                //dbLastDate.LastDate = checkInOutList.OrderByDescending(x => x.DateTime)
                                //    .Where(x => x.IPAddress == ipAddress.ToLower()).Take(1)
                                //    .SingleOrDefault().DateTime;

                                BLL.BaseBiz.PayrollDataContext.AttendanceLastDates.InsertOnSubmit(dbLastDate);
                            }
                        }
                    }




                    BLL.BaseBiz.PayrollDataContext.SubmitChanges();

                    Log.log("Attendance log uploaded : " + checkInOutList.Count, "Attendance Message");

                    return "";
                }
            }
            catch (Exception exp)
            {
                Log.log("Attendance import error", exp);
                return exp.ToString();
            }
            //return "";
        }


        [WebMethod]
        public DateTime? GetLastDateNew(string apiKey,string ipAddress)
        {

            if (CommonManager.IsAPIKeyValid(apiKey) == false)
                return null;

            AttendanceLastDate last = BLL.BaseBiz.PayrollDataContext.
                AttendanceLastDates.SingleOrDefault(x => x.IPAddress == ipAddress.ToLower());

            if (last != null)
                return last.LastDate;

            return null;
        }

        [WebMethod()]
        public string ImportAtteNew(string apiKey,List<CheckInOut> checkInOutList)
        {

            if (CommonManager.IsAPIKeyValid(apiKey) == false)
                return null;

            try
            {

                if (checkInOutList.Count == 0)
                    return "";


                //using (PayrollDataContext payrollDataContext = new PayrollDataContext(Config.ConnectionString))
                {



                    // First remove duplication for same DeviceID,InOutMode and DateTime as sometime repeated data may come
                    List<CheckInOut> duplicatedRemovedList = new List<CheckInOut>();
                    for (int i = 0; i < checkInOutList.Count; i++)
                    {
                        CheckInOut item = checkInOutList[i];
                        if (duplicatedRemovedList.Any(x => x.DeviceID == item.DeviceID && x.DateTime == item.DateTime && x.InOutMode == item.InOutMode))
                        {
                        }
                        else
                            duplicatedRemovedList.Add(item);
                    }

                    checkInOutList = duplicatedRemovedList;


                    foreach (CheckInOut cIO in checkInOutList)
                    {

                        if (string.IsNullOrEmpty(cIO.DeviceID))
                            continue;

                        AttendanceCheckInCheckOut chkInOut = new AttendanceCheckInCheckOut();
                        chkInOut.ChkInOutID = Guid.NewGuid();
                        chkInOut.DeviceID = cIO.DeviceID;
                        chkInOut.AuthenticationType = byte.Parse(cIO.AuthenticationType.ToString());
                        chkInOut.InOutMode = byte.Parse(cIO.InOutMode.ToString());
                        chkInOut.DateTime = cIO.DateTime;
                        chkInOut.Date = chkInOut.DateTime.Date;
                        chkInOut.IPAddress = cIO.IPAddress;
                        chkInOut.ModifiedOn = DateTime.Now;


                        BLL.BaseBiz.PayrollDataContext.AttendanceCheckInCheckOuts.InsertOnSubmit(chkInOut);


                    }

                    foreach (string ipAddress in checkInOutList.Select(o => o.IPAddress).Distinct())
                    {
                        // Update last read date
                        AttendanceLastDate dbLastDate = BLL.BaseBiz.PayrollDataContext
                            .AttendanceLastDates.SingleOrDefault(x => x.IPAddress == ipAddress.ToLower());

                        if (dbLastDate != null)
                        {
                            dbLastDate.LastDate = checkInOutList.OrderByDescending(x => x.DateTime)
                                .Where(x => x.IPAddress == ipAddress.ToLower()).Take(1)
                                .SingleOrDefault().DateTime;

                        }
                        else
                        {
                            dbLastDate = new AttendanceLastDate();
                            dbLastDate.IPAddress = ipAddress.ToLower();
                            dbLastDate.LastDate = checkInOutList.OrderByDescending(x => x.DateTime)
                                .Where(x => x.IPAddress == ipAddress.ToLower()).Take(1)
                                .SingleOrDefault().DateTime;

                            BLL.BaseBiz.PayrollDataContext.AttendanceLastDates.InsertOnSubmit(dbLastDate);
                        }
                    }




                    BLL.BaseBiz.PayrollDataContext.SubmitChanges();

                    Log.log("Attendance log uploaded : " + checkInOutList.Count, "Attendance Message");

                    return "";
                }
            }
            catch (Exception exp)
            {
                Log.log("Attendance import error", exp);
                return exp.ToString();
            }
            //return "";
        }

        [WebMethod()]
        public EmailContent GetMailContent(int type, string key)
        {
            if (key.Equals("SendMailToLateEntryAttendanceKey") == false)
                return null;
            return
             BLL.Manager.CommonManager.GetMailContent(type);
        }

        [WebMethod()]
        public string GetEmployeeCIEmail(int employeeId)
        {
            if (CommonManager.IsOldAPIWithoutKeyAccessible() == false)
                throw new Exception("No Permission");

            return BLL.Manager.EmployeeManager.GetEmployeeEmailForLateEntry(employeeId);
        }

        [WebMethod()]
        public List<AttendanceAllEmployeeReportResult> GetAttendanceDailyReport(DateTime filterDate, int Status, string BranchID, string key)
        {
            if (CommonManager.IsOldAPIWithoutKeyAccessible() == false)
                throw new Exception("No Permission");

            if (key.Equals("SendMailToLateEntryAttendanceKey") == false)
                return null;
            List<AttendanceAllEmployeeReportResult> attendanceList = new List<AttendanceAllEmployeeReportResult>();
            return attendanceList = BLL.Manager.AttendanceManager.GetAttendanceDailyReportWebServiceCalling(filterDate, Status, BranchID);
        }
        [WebMethod()]
        public List<GetAbsentListForEmailResult> GetAbsentListForEmail(DateTime StartDate, DateTime EndDate, string key)
        {
            if (CommonManager.IsOldAPIWithoutKeyAccessible() == false)
                throw new Exception("No Permission");

            if (key.Equals("SendMailToLateEntryAttendanceKey") == false)
                return null;
            return BLL.Manager.AttendanceManager.GetAbsentListForEmail(StartDate, EndDate);
        }

        [WebMethod()]
        public List<AbsentEmpIDBO> GetAbsentEmpIDList(DateTime StartDate, DateTime EndDate, string key)
        {
            if (CommonManager.IsOldAPIWithoutKeyAccessible() == false)
                throw new Exception("No Permission");

            if (key.Equals("SendMailToLateEntryAttendanceKey") == false)
                return null;
            return BLL.Manager.AttendanceManager.GetAbsentEmpIDList(StartDate, EndDate);
        }

        [WebMethod()]
        public List<SupervisorEmails> GetSupervisorEmailListForMail(DateTime StartDate, DateTime EndDate, string key)
        {
            if (CommonManager.IsOldAPIWithoutKeyAccessible() == false)
                throw new Exception("No Permission");

            if (key.Equals("SendMailToLateEntryAttendanceKey") == false)
                return null;
            return BLL.Manager.AttendanceManager.GetSupervisorEmailListForMail(StartDate, EndDate);
        }

        [WebMethod()]
        public List<GetForDeviceMappingResult> GetDeviceMapping(string key)
        {
            if (CommonManager.IsOldAPIWithoutKeyAccessible() == false)
                throw new Exception("No Permission");

            if (key.Equals("SendMailToLateEntryAttendanceKey") == false)
                return null;

            List<GetForDeviceMappingResult> _list = BLL.Manager.TaxManager.GetForDeviceMappingWebServiceCalling();
            return _list;
        }

        /// <summary>
        /// Reurn the list of employees late for the period, used for prabhu to send late mail to employee
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        [WebMethod()]
        public List<GetAutoWeeklyLateMailListToEmployeeResult> GetAutoWeeklyMailListToEmployee(string key)
        {
            if (CommonManager.IsOldAPIWithoutKeyAccessible() == false)
                throw new Exception("No Permission");

            if (key.Equals("SendMailToLateEntryAttendanceKey") == false)
                return null;

            DateTime endDate = DateTime.Now.Date; //friday
            DateTime startDate = DateTime.Now.Date.AddDays(-5); // sunday

            List<GetAutoWeeklyLateMailListToEmployeeResult> _list =
                BLL.BaseBiz.PayrollDataContext.GetAutoWeeklyLateMailListToEmployee(startDate, endDate).ToList();

            return _list;
        }

        [WebMethod()]
        public List<int> GetAllEmployeeIDs(string key)
        {
            if (CommonManager.IsOldAPIWithoutKeyAccessible() == false)
                throw new Exception("No Permission");

            if (key.Equals("SendMailToLateEntryAttendanceKey") == false)
                return null;

            List<int> _list = EmployeeManager.GetAllEmployeeID();
            return _list;
        }

        [WebMethod()]
        public DateTime GetCurrentDateAndTime()
        {
            return CommonManager.GetCurrentDateAndTime();
        }

        [WebMethod()]
        public bool SendMailAsync(string to, string body, string subject, string bcc, string senderEmail)
        {
            if (CommonManager.IsOldAPIWithoutKeyAccessible() == false)
                throw new Exception("No Permission");

            return SMTPHelper.SendMailAsyncWebServiceOnly(to, body, subject, "", senderEmail);
        }

        [WebMethod()]
        public bool EnableWeeklyAbsentScheduler()
        {
            return Config.EnableWeeklyAbsentScheduler;
        }
        [WebMethod()]
        public bool EnableDailyLateEntryWithAbsent()
        {
            return Config.EnableDailyLateEntryWithAbsent;
        }

        [WebMethod()]
        public int GetLateEntryEmailContentID()
        {
            return (int)EmailContentType.LateIn;
        }

        [WebMethod()]
        public int GetBirthDayEmailContentID()
        {
            return (int)EmailContentType.BirthdayNotification;
        }

        [WebMethod()]
        public int GetBirthDayWishEmailContentID()
        {
            return (int)EmailContentType.BirthdayWishNotification;
        }

        [WebMethod()]
        public int GetAbsentEmailContentID()
        {
            return (int)EmailContentType.Absent;
        }

        [WebMethod()]
        public int GetWeeklyAbsentEmailContentIDForEmployee()
        {
            return (int)EmailContentType.AbsentWeeklyForEmployee;
        }

        [WebMethod()]
        public int GetWeeklyAbsentEmailContentIDForSupervisor()
        {
            return (int)EmailContentType.AbsentWeeklyForSuperviser;
        }


        [WebMethod()]
        public List<BirthDayBo> GetBirthDayEmploye(string key)
        {
            if (CommonManager.IsOldAPIWithoutKeyAccessible() == false)
                throw new Exception("No Permission");

            if (key.Equals("SendMailToLateEntryAttendanceKey") == false)
                return null;
            DateTime Today = BLL.BaseBiz.GetCurrentDateAndTime();
            int ThisMonth = Today.Month;
            List<BirthDayBo> _listBrithdayEmps = DashboardManager.GetUpcommingBirthDay(ThisMonth, true);
            return _listBrithdayEmps = _listBrithdayEmps.Where(x => x.Day == Today.Day).ToList();
        }


      
    }
}

