﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using BLL.Manager;
using DAL;
using BLL;
using Utils.Helper;
using Utils;
using Utils.Security;
using System.Web.Security;

namespace Web
{
    public partial class TelephoneDirectory : Page
    {
        #region "Control state related"
        private string _sortBy = "";
        private SortDirection _sortDirection = SortDirection.Ascending;

        protected override void OnInit(EventArgs e)
        {
            Page.RegisterRequiresControlState(this);
            base.OnInit(e);
        }
        protected override void LoadControlState(object savedState)
        {
            object[] rgState = (object[])savedState;
            base.LoadControlState(rgState[0]);
            _sortBy = rgState[1].ToString();
            _sortDirection = (SortDirection)rgState[2];
            //_tempCurrentPage = (int)rgState[2];

        }
        protected override object SaveControlState()
        {
            object[] rgState = new object[3];
            rgState[0] = base.SaveControlState();
            rgState[1] = _sortBy;
            rgState[2] = _sortDirection;
            // rgState[2] = _tempCurrentPage;
            return rgState;
        }
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
				
				
                Initialise();
                //sdfsd fsddlBranch_SelectedIndex(null, null);
            }

            //if (Request.Form["__EVENTTARGET"] !=  j jkjk null && Request.Form["__EVENTTARGET"].Equals("Reload"))
            //{
            //    gvwRoles.SelectedIndex = -1;
            //    BindUsers();
            //}
        }

        private void Initialise()
        {
            List<Department> list = DepartmentManager.GetAllDepartments();

            if (CommonManager.CompanySetting.WhichCompany == WhichCompany.NIBL)
            {
                list.Add(new Department { DepartmentId = -2, Name = "NIBL Capital" });

                // skip cde office dep for NIBL
                list = list.Where(x => x.DepartmentId != 1).ToList();
            }
            ddlDepartment.DataSource = list.OrderBy(x => x.Name).ToList();
            ddlDepartment.DataBind();

            // default selection to IT department for NIBl
            //if (CommonManager.CompanySetting.WhichCompany == WhichCompany.NIBL)
            //    ddlDepartment.SelectedValue = "4";
            

            ddlBranch.DataSource = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId);
            ddlBranch.DataBind();
            BindData();
        }

        public void txtSearch_Click(object sender, EventArgs e)
        {
            pagingCtl.CurrentPage = 1;
            BindData();
        }


        protected void gvwEmployees_RowCreated(object sender, GridViewRowEventArgs e)
        {

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover",
                "this.originalClass=this.className;this.className='selected'");

                e.Row.Attributes.Add("onmouseout",
                "this.className=this.originalClass;");


            }

            int sortColumnIndex = 0;

            if (e.Row.RowType == DataControlRowType.Header)
            {
                sortColumnIndex = GetSortColumnIndex();
            }

            //if (sortColumnIndex != 0)
            //{
                AddSortImage(sortColumnIndex, e.Row);
           // }



        }

        protected void AddSortImage(int columnIndex, GridViewRow HeaderRow)
        {
            System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();

            //if (showImage) // this is a boolean variable which should be false 
            {            //  on page load so that image wont show up initially.

                if (_sortDirection.ToString() == "Ascending")
                {
                    sortImage.ImageUrl = "~/Images/up.png";

                }

                else
                {
                    sortImage.ImageUrl = "~/Images/down.png";
                }

                HeaderRow.Cells[columnIndex].Controls.Add(sortImage);
            }

        }
        protected int GetSortColumnIndex()
        {


            foreach (DataControlField field in gridDirectory.Columns)
            {
                if (field.SortExpression != "" && field.SortExpression == _sortBy)
                    return gridDirectory.Columns.IndexOf(field);
                //else
                //    return -1;
            }
            return 0;
        }
        protected void gvwEmployees_Sorting(object sender, GridViewSortEventArgs e)
        {

            if (e.SortExpression != _sortBy)
            {
                e.SortDirection = SortDirection.Ascending;
                _sortDirection = e.SortDirection;
            }
            else if (_sortDirection == SortDirection.Ascending)
            {
                e.SortDirection = SortDirection.Descending;
            }
            else if (_sortDirection == SortDirection.Descending)
            {
                e.SortDirection = SortDirection.Ascending;
            }

            _sortDirection = e.SortDirection;

            _sortBy = e.SortExpression;


            e.Cancel = true;


            pagingCtl.CurrentPage = 1;
            //_tempCurrentPage = 1;
            BindData();
        }
        void BindData()
        {
            int totalRecords = 0;
            string Employee = "-1";
            string SearchText = "-1";
            int DepartmentId = -1;
            if (!string.IsNullOrEmpty(txtUserNameSearch.Text))
                Employee = txtUserNameSearch.Text.Trim();

            if (!string.IsNullOrEmpty(txtSearch.Text))
                SearchText = txtSearch.Text.Trim();


            if (Employee == "Employee Name")//Empty Text
                Employee = "-1";


            string sort = (_sortBy + " " + _sortDirection).ToLower();

            if (string.IsNullOrEmpty(_sortBy))
                sort = "";

            if (!string.IsNullOrEmpty(ddlDepartment.SelectedItem.Value))
                DepartmentId = int.Parse(ddlDepartment.SelectedItem.Value);
                
            gridDirectory.SelectedIndex = -1;

            // in first load show it emp only
            if (!IsPostBack && CommonManager.CompanySetting.WhichCompany==WhichCompany.NIBL)
            {
                DepartmentId = 4;
            }

            gridDirectory.DataSource = EmployeeManager.GetEmployeeAddressReport(int.Parse(ddlBranch.SelectedItem.Value),DepartmentId, Employee, pagingCtl.CurrentPage - 1, int.Parse(pagingCtl.DDLRecords.SelectedValue), ref totalRecords, sort, SearchText);
            pagingCtl.UpdatePagingBar(totalRecords);
            gridDirectory.DataBind();
        }

        public void btnExport_Click(object sender, EventArgs e)
        {
           // string sortBy = hdnSorby.Value;
            int DepartmentId = -1;
            string sortBy = (_sortBy + " " + _sortDirection).ToLower();

            if (string.IsNullOrEmpty(_sortBy))
                sortBy = "";
            int totalRecords = 0;
            string Employee = "-1";
            if (!string.IsNullOrEmpty(txtUserNameSearch.Text))
                Employee = txtUserNameSearch.Text.Trim();

            if (Employee == "Employee Name")//Empty Text
                Employee = "-1";

            string SearchText = "-1";

            if (!string.IsNullOrEmpty(txtSearch.Text))
                SearchText = txtSearch.Text.Trim();

            if (!string.IsNullOrEmpty(ddlDepartment.SelectedItem.Value))
                DepartmentId = int.Parse(ddlDepartment.SelectedItem.Value);

            // in first load show it emp only
            //if (!IsPostBack && CommonManager.CompanySetting.WhichCompany == WhichCompany.NIBL)
            //{
            //    DepartmentId = 4;
            //}

            List<GetEmployeeAddressReportResult> list = EmployeeManager.GetEmployeeAddressReport(int.Parse(ddlBranch.SelectedItem.Value),DepartmentId, Employee, 0, 99999, ref totalRecords, sortBy, SearchText);
            List<string> hiddenList = new List<string>();
                hiddenList = new List<string> { "TotalRows","EmployeeId","Title","BranchID","DepartmentCode","RowNumber","TotalRows","Location" };

            string title1 = "Telephone Directory";
            string title2 = "";

            Bll.ExcelHelper.ExportToExcel("Telephone Directory", list, hiddenList,
            new List<String>() { },
            new Dictionary<string, string>(){{"DirectLine","Direct Line"},{"AreaCode","Code"},{"BranchPhone","Phone"}},
            new List<string>() { },
            new List<string>() { },
            new List<string>() {}
            , new Dictionary<string, string>() {
              {title1,title2}
            }
            , new List<string> { "Name", "Branch", "Department", "Location", "Email", "Mobile", "AreaCode","Extension", "BranchPhone", "Fax", "DirectLine" });
        }
        public void btnLoad_Click(object sender, EventArgs e)
        {
            
            BindData();
        }
        public string GetEmployeeName(object value)
        {
            if (value == null)
                return "";
            return (value as EEmployee).Name;

        }
        public string GetEmployeeId(object value)
        {
            if (value == null)
                return "";
            return (value as EEmployee).EmployeeId.ToString();
        }

        protected void ddlBranch_SelectedIndex(object sender, EventArgs e)
        {
            pagingCtl.CurrentPage = 1;
            string sortBy = hdnSorby.Value;
            BindData();
        }

        protected void ddlDepartment_SelectedIndex(object sender, EventArgs e)
        {
            pagingCtl.CurrentPage = 1;
            string sortBy = hdnSorby.Value;
            BindData();
        }
       
        protected void btnPrevious_Click()
        {
            pagingCtl.CurrentPage -= 1;
            //_tempCurrentPage = int.Parse(pagintCtl.LabelCurrentPage.Text);
            //_tempCurrentPage -= 1;
            string sortBy = hdnSorby.Value;
            BindData();
        }

        protected void btnNext_Click()
        {
            pagingCtl.CurrentPage += 1;
            //_tempCurrentPage = int.Parse(pagintCtl.LabelCurrentPage.Text);
            //_tempCurrentPage += 1;

            string sortBy = hdnSorby.Value;
            BindData();
        }

        protected void ddlRecords_SelectedIndexChanged()
        {
            //SessionManager.EmployeeDisplayRecodsPerPage = int.Parse(pagintCtl.DDLRecords.SelectedValue);
            //_tempCurrentPage = 1;
            pagingCtl.CurrentPage = 1;
            string sortBy = hdnSorby.Value;
            BindData();
        }

        protected void gvwRoles_PageIndexChanged(object sender, EventArgs e)
        {

        }

        public bool CanChange(object value)
        {
            return Convert.ToBoolean(value);
        }


        protected void gvwRoles_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gridDirectory.PageIndex = e.NewPageIndex;
            gridDirectory.SelectedIndex = -1;
            string sortBy = hdnSorby.Value;
            BindData();
        }
    }
}