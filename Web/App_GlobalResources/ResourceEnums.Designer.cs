//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option or rebuild the Visual Studio project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Web.Application.StronglyTypedResourceProxyBuilder", "14.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class ResourceEnums {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal ResourceEnums() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Resources.ResourceEnums", global::System.Reflection.Assembly.Load("App_GlobalResources"));
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Annual Grade Fitting.
        /// </summary>
        internal static string AnnualGradeFitting {
            get {
                return ResourceManager.GetString("AnnualGradeFitting", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Dalit Kota.
        /// </summary>
        internal static string DalitKota {
            get {
                return ResourceManager.GetString("DalitKota", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Destination Branch.
        /// </summary>
        internal static string DestinationBranch {
            get {
                return ResourceManager.GetString("DestinationBranch", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Direct Hire.
        /// </summary>
        internal static string DirectHire {
            get {
                return ResourceManager.GetString("DirectHire", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Free Competition.
        /// </summary>
        internal static string FreeCompetition {
            get {
                return ResourceManager.GetString("FreeCompetition", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Going And Returing.
        /// </summary>
        internal static string GoingAndReturing {
            get {
                return ResourceManager.GetString("GoingAndReturing", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Going Only.
        /// </summary>
        internal static string GoingOnly {
            get {
                return ResourceManager.GetString("GoingOnly", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Higher of the 2 branches.
        /// </summary>
        internal static string HigherOf2Branches {
            get {
                return ResourceManager.GetString("HigherOf2Branches", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Lower of the 2 branches.
        /// </summary>
        internal static string LowerOf2Branches {
            get {
                return ResourceManager.GetString("LowerOf2Branches", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Marital Status.
        /// </summary>
        internal static string MaritalStatus {
            get {
                return ResourceManager.GetString("MaritalStatus", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to No Attendance.
        /// </summary>
        internal static string NoAttendance {
            get {
                return ResourceManager.GetString("NoAttendance", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to No TA.
        /// </summary>
        internal static string NoTA {
            get {
                return ResourceManager.GetString("NoTA", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Returing Only.
        /// </summary>
        internal static string ReturingOnly {
            get {
                return ResourceManager.GetString("ReturingOnly", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Salary Structure Grade Fitting.
        /// </summary>
        internal static string SalaryStructureChange {
            get {
                return ResourceManager.GetString("SalaryStructureChange", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Source Branch.
        /// </summary>
        internal static string SourceBranch {
            get {
                return ResourceManager.GetString("SourceBranch", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Status Date.
        /// </summary>
        internal static string StatusDate {
            get {
                return ResourceManager.GetString("StatusDate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Tax Status.
        /// </summary>
        internal static string TaxStatus {
            get {
                return ResourceManager.GetString("TaxStatus", resourceCulture);
            }
        }
    }
}
