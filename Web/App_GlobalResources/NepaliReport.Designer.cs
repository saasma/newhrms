//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option or rebuild the Visual Studio project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Web.Application.StronglyTypedResourceProxyBuilder", "10.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class NepaliReport {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal NepaliReport() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Resources.NepaliReport", global::System.Reflection.Assembly.Load("App_GlobalResources"));
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to आवास सुबिधाको.
        /// </summary>
        internal static string Abash {
            get {
                return ResourceManager.GetString("Abash", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to औषधि उपचार.
        /// </summary>
        internal static string Ausadhi {
            get {
                return ResourceManager.GetString("Ausadhi", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to भत्ता (सबै किसिमको ).
        /// </summary>
        internal static string Bhatta {
            get {
                return ResourceManager.GetString("Bhatta", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to बोनस.
        /// </summary>
        internal static string Bonus {
            get {
                return ResourceManager.GetString("Bonus", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to दशै.
        /// </summary>
        internal static string Dashain {
            get {
                return ResourceManager.GetString("Dashain", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to घरभाडा.
        /// </summary>
        internal static string GarBhada {
            get {
                return ResourceManager.GetString("GarBhada", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to जीवन बीमा.
        /// </summary>
        internal static string InsuranceIncome {
            get {
                return ResourceManager.GetString("InsuranceIncome", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to मोटर सुविधा.
        /// </summary>
        internal static string Motor {
            get {
                return ResourceManager.GetString("Motor", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to रीवार्ड.
        /// </summary>
        internal static string Reward {
            get {
                return ResourceManager.GetString("Reward", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to तलब.
        /// </summary>
        internal static string Talab {
            get {
                return ResourceManager.GetString("Talab", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to टेलीफोन सुबिधा.
        /// </summary>
        internal static string Telephone {
            get {
                return ResourceManager.GetString("Telephone", resourceCulture);
            }
        }
    }
}
