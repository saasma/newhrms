﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;

namespace Web
{
    public partial class Error : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
          
            //if (Session["Message"] != null)
            {
                string msg = "";
                try
                {
                    msg = File.ReadAllText(Server.MapPath("~/App_Data/logs/message.log"));
                    msg = msg.Replace("---", "---\n");
                }
                catch { }

                //lblMessage.Text = "<h2>" + Session["Message"].ToString() + "</h2>";
                lblDetails.Text = "<pre>" + msg + "</pre>";
            }
        }

    }
}