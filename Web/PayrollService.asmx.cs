using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using BLL;
using BLL.Manager;
using DAL;
using System.Text;
using Utils.Helper;
using Utils.Calendar;
using Ext.Net;
using Utils;
using BLL.BO;
using System.Xml;

namespace Web
{

    

    /// <summary>
    /// Summary description for PayrollService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    //[System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class PayrollService : System.Web.Services.WebService
    {


        [WebMethod(true)]
        public bool SendSignOffMail()
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated == false || SessionManager.CurrentLoggedInEmployeeId != 0)
                return false;

            return CommonManager.SendSignOffApprovalRequest();           
        }

        //[WebMethod(true)]
        //public bool SetSelCompany(int companyId)
        //{
        //    SessionManager.CurrentCompanyId = companyId;
        //    return true;
        //}


        [WebMethod(true)]
        public string GetCalculationHistory(string periodId)
        {

            if (HttpContext.Current.User.Identity.IsAuthenticated == false)
                return "";
            int value = int.Parse(periodId.Split(new char[] { ':' })[0]);
            return PayManager.GetCalculationHistory(value);
            //return "";
        }

        [WebMethod(true)]
        public string GetSalarySaveRemainingList(string periodId)
        {

            if (HttpContext.Current.User.Identity.IsAuthenticated == false || SessionManager.CurrentLoggedInEmployeeId != 0)
                return "";
            int value = int.Parse(periodId.Split(new char[] { ':' })[0]);
            return PayManager.GetSalarySaveRemainingList(value);
            //return "";
        }
        [WebMethod(true)]
        public string GetLoanAdjustmentHistory(int empId,int deductionId)
        {

            if (HttpContext.Current.User.Identity.IsAuthenticated == false)
                return "";

            return PayManager.GetLoanAdjustmentHistory(empId, deductionId);
            //return "";
        }

        //[WebMethod(true)]
        //public bool SetSelEmployee(int employeeId)
        //{
        //    SessionManager.CurrentEmployeeId = employeeId;
        //    return true;
        //}

        [WebMethod(true)]
        public string AddIncomeToEmployee(int incomeId,int employeeId)
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated == false || SessionManager.CurrentLoggedInEmployeeId != 0)
                return "";

            PayManager payMgr = new PayManager();

            PayManager.EnableIncomeIFAlreadyAssociated(employeeId, incomeId);

            payMgr.AddIncomeToEmployee(incomeId,
                    employeeId, false, SessionManager.CurrentCompanyId,-1,-1,-1);

            PEmployeeIncome empIncome = payMgr.GetEmployeeIncome(employeeId, incomeId);


            return empIncome.EmployeeIncomeId.ToString();
        }

        [WebMethod(true)]
        public List<KeyValue> GetTSOByIRO(int IROId)
        {

            if (HttpContext.Current.User.Identity.IsAuthenticated == false)
                return null;
            List<IRO_TSO> list = CommonManager.GetTSOByIROId(IROId);
            List<KeyValue> values = new List<KeyValue>();
            foreach (IRO_TSO item in list)
            {
                KeyValue value = new KeyValue(item.TSOId.ToString(), item.Name);
                values.Add(value);
            }
            return values;
        }

        [WebMethod(true)]
        public List<KeyValue> GetDepartmentsByBranch(int branchId)
        {

            if (HttpContext.Current.User.Identity.IsAuthenticated == false)
                return null;
            DepartmentManager depMgr = new DepartmentManager();
            List<GetDepartmentListResult> deps = DepartmentManager.GetAllDepartmentsByBranch(branchId);
            List<KeyValue> values = new List<KeyValue>();
            foreach (GetDepartmentListResult dep in deps)
            {
                KeyValue value = new KeyValue(dep.DepartmentId.ToString(), dep.Name);
                values.Add(value);
            }
            return values;
        }

        [WebMethod(true)]
        public List<KeyValue> GetSubDepartmentsByDepartment(int departmentId)
        {

            if (HttpContext.Current.User.Identity.IsAuthenticated == false)
                return null;
            DepartmentManager depMgr = new DepartmentManager();
            List<SubDepartment> deps = DepartmentManager.GetAllSubDepartmentsByDepartment(departmentId);
            List<KeyValue> values = new List<KeyValue>();
            foreach (SubDepartment dep in deps)
            {
                KeyValue value = new KeyValue(dep.SubDepartmentId.ToString(), dep.Name);
                values.Add(value);
            }
            return values;
        }

        [WebMethod(true)]
        public List<KeyValue> GetVDCByDistrict(int districtID)
        {

            if (HttpContext.Current.User.Identity.IsAuthenticated == false)
                return null;
            CommonManager mgr = new CommonManager();
            List<VDCList> vdcs = mgr.GetAllVDCs(districtID);
            List<KeyValue> values = new List<KeyValue>();
            foreach (VDCList vdc in vdcs)
            {
                KeyValue value = new KeyValue(vdc.VDCId.ToString(), vdc.Name);
                values.Add(value);
            }
            return values;
        }

        [WebMethod(true)]
        public List<KeyValue> GetDistrictByZone(int zoneId)
        {

            if (HttpContext.Current.User.Identity.IsAuthenticated == false)
                return null;
            CommonManager mgr = new CommonManager();
            List<DistrictList> vdcs = mgr.GetAllDistricts(zoneId);
            List<KeyValue> values = new List<KeyValue>();
            foreach (DistrictList vdc in vdcs)
            {
                KeyValue value = new KeyValue(vdc.DistrictId.ToString(), vdc.District);
                values.Add(value);
            }
            return values;
        }

        [WebMethod(true)]
        public string DeleteEmployeeStatus(int statusId)
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated == false || SessionManager.CurrentLoggedInEmployeeId != 0)
                return "";

            int empId  = EmployeeManager.DeleteStatus(statusId);

            return new EmployeeManager().GetCurrentStatus(empId);
        }
        [WebMethod(true)]
        public string GetStatusList(int empId)
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated == false || SessionManager.CurrentLoggedInEmployeeId != 0)
                return "";

            

            return new EmployeeManager().GetCurrentStatus(empId);
        }
        [WebMethod(true)]
        public string SaveEmployeeStatus(int statusId,string status, string fromDate,int employeeId)
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated == false || SessionManager.CurrentLoggedInEmployeeId != 0)
                return "";

            ECurrentStatus empStatus = new ECurrentStatus();
            empStatus.ECurrentStatusId = statusId;
            empStatus.EmployeeId = employeeId;
            empStatus.CurrentStatus = int.Parse(status);
            empStatus.FromDate = fromDate;
            //empStatus.ToDate = toDate;

            empStatus.FromDateEng = BLL.BaseBiz.GetEngDate(empStatus.FromDate, null);
            //empStatus.ToDateEng = BLL.BaseBiz.GetEngDate(empStatus.ToDate, null);

            EmployeeManager empMgr = new EmployeeManager();

            
            if (empStatus.ECurrentStatusId == 0)
            {
                if (empMgr.Save(empStatus).IsSuccessType)
                    return empMgr.GetCurrentStatus(empStatus.EmployeeId);
            }
            else
            {
                //if (empMgr.Update(empStatus))
                empMgr.Update(empStatus);
                return empMgr.GetCurrentStatus(empStatus.EmployeeId);
            }


            return null;
        }

        [WebMethod]
        public  string GetIncrementedDate(string date)
        {

            if (HttpContext.Current.User.Identity.IsAuthenticated == false)
                return "";
            CustomDate cd = CustomDate.GetCustomDateFromString(date, SessionManager.CurrentCompany.IsEnglishDate);
            return cd.IncrementByOneDay().ToString();
               
        }



        /// <summary>
        /// If incomeId is not zero deletes for the employee & return the update list
        /// </summary>
        /// <param name="incomeId"></param>
        /// <returns></returns>
        [WebMethod(true)]
        public string DeleteIncome(int empIncomeId, int employeeId,string sourceType)
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated == false || SessionManager.CurrentLoggedInEmployeeId != 0)
                return "";

            PayManager pay = new PayManager();

            if (sourceType.ToLower() == "restore matrix")
            {
                PayManager.ReverseSourceMatrixOrAmount(empIncomeId);
            }
            else
            {
                if (empIncomeId != 0)
                {
                    pay.Delete(empIncomeId);
                }
            }

            if (employeeId == 0)
            {
                PEmployeeIncome inc = pay.GetEmployeeIncome(empIncomeId);
                if (inc != null && employeeId == 0)
                    employeeId = inc.EmployeeId;
            }

            return pay.GetHTMLEmployeeIncomeList(employeeId);
        }

        /// <summary>
        /// If incomeId is not zero deletes for the employee & return the update list
        /// </summary>
        /// <param name="incomeId"></param>
        /// <returns></returns>
        [WebMethod(true)]
        public string GetIncomeList(int empIncomeId, int employeeId)
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated == false || SessionManager.CurrentLoggedInEmployeeId != 0)
                return "";

            PayManager pay = new PayManager();
            if (empIncomeId != 0)
            {
                pay.Delete(empIncomeId);
            }

            if (employeeId == 0)
            {
                PEmployeeIncome inc = pay.GetEmployeeIncome(empIncomeId);
                if (inc != null && employeeId == 0)
                    employeeId = inc.EmployeeId;
            }

            return pay.GetHTMLEmployeeIncomeList(employeeId);
        }


        [WebMethod(true)]
        public string GetDeductionList(int empDeductionId, int employeeId)
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated == false || SessionManager.CurrentLoggedInEmployeeId != 0)
                return "";

            PayManager pay = new PayManager();
            if (empDeductionId != 0)
            {

                PEmployeeDeduction entity = new PEmployeeDeduction();
                entity.EmployeeDeductionId = empDeductionId;

                pay.RemoveDeductionFromEmployee(entity);
            }

            if (employeeId == 0)
            {
                PEmployeeDeduction inc = pay.GetEmployeeDeduction(empDeductionId);
                if (inc != null && employeeId == 0)
                    employeeId = inc.EmployeeId;
            }

            return PayManager.GetHTMLEmployeeDeductionList(employeeId);
        }



        //GetHTMLEmployeeLeaveList
        [WebMethod(true)]
        public string GetEducationList(int educationId, int employeeId)
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated == false)
                return "";
            HRManager mgr = new HRManager();
            if (educationId != 0)
            {
                HEducation entity = new HEducation();
                entity.EductionId = educationId;


                mgr.DeleteEducation(entity);
            }
            return mgr.GetHTMLEmployeeEduction(employeeId);
        }
        [WebMethod(true)]
        public string GetTrainingList(int trainingId, int employeeId)
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated == false)
                return "";
            HRManager mgr = new HRManager();
            if (trainingId != 0)
            {
                HTraining entity = new HTraining();
                entity.TrainingId = trainingId;
                mgr.DeleteTraining(entity);
            }
            return mgr.GetHTMLEmployeeTraining(employeeId);
        }
        [WebMethod(true)]
        public string GetActivityList(int activityId, int employeeId)
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated == false)
                return "";
            HRManager mgr = new HRManager();
            if (activityId != 0)
            {
                HExtraActivity entity = new HExtraActivity();
                entity.CurricularId = activityId;
                mgr.DeleteCurriculum(entity);
            }
            return mgr.GetHTMLEmployeeCurriculum(employeeId);
        }

        [WebMethod(true)]
        public string GetFamilyList(int familyId, int employeeId)
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated == false)
                return "";
            HRManager mgr = new HRManager();
            if (familyId != 0)
            {
                HFamily entity = new HFamily();
                entity.FamilyId = familyId;
                mgr.DeleteFamily(entity);
            }
            return mgr.GetHTMLEmployeeFamily(employeeId);
        }
        [WebMethod(true)]
        public string GetDisciplinaryActionList(int disciplinaryId, int employeeId)
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated == false)
                return "";
            HRManager mgr = new HRManager();
            if (disciplinaryId != 0)
            {
                HDisciplinaryAction entity = new HDisciplinaryAction();
                entity.DisciplinaryId = disciplinaryId;
                mgr.DeleteDiscipline(entity);
            }
            return mgr.GetHTMLEmployeeDiscipline(employeeId);
        }

        [WebMethod(true)]
        public string GetEvaluationList(int evaluationId, int employeeId)
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated == false)
                return "";
            HRManager mgr = new HRManager();
            if (evaluationId != 0)
            {
                HEvaluation entity = new HEvaluation();
                entity.EvaluationId = evaluationId;
                mgr.DeleteEvaluation(entity);
            }
            return mgr.GetHTMLEmployeeEvaluation(employeeId);
        }

        [WebMethod(true)]
        public string GetAccidentList(int accidentId, int employeeId)
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated == false)
                return "";
            HRManager mgr = new HRManager();
            if (accidentId != 0)
            {
                HAccident entity = new HAccident();
                entity.AccidentId = accidentId;
                mgr.DeleteAccident(entity);
            }
            return mgr.GetHTMLEmployeeAccident(employeeId);
        }

        [WebMethod(true)]
        public string GetSpecialEventList(int eventId, int employeeId)
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated == false)
                return "";
            HRManager mgr = new HRManager();
            if (eventId != 0)
            {
                HSpecialEvent entity = new HSpecialEvent();
                entity.EventId = eventId;
                mgr.DeleteSpecialEvent(entity);
            }
            return mgr.GetHTMLSpecialEvent(employeeId);
        }
        [WebMethod(true)]
        public string GetPreviousEmploymentList(int prevEmploymentId, int employeeId)
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated == false)
                return "";
            HRManager mgr = new HRManager();
            if (prevEmploymentId != 0)
            {
                HPreviousEmployment entity = new HPreviousEmployment();
                entity.PrevEmploymentId = prevEmploymentId;
                mgr.DeletePreviousEmployment(entity);
            }
            return mgr.GetHTMLPreviousEmployment(employeeId);
        }

        //GetHTMLEmployeeLeaveList
        [WebMethod(true)]
        public string GetLeaveList(int leaveId, int employeeId)
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated == false || SessionManager.CurrentLoggedInEmployeeId != 0)
                return "";

            LeaveAttendanceManager mgr = new LeaveAttendanceManager();
            if (leaveId != 0)
            {
                LEmployeeLeave entity = new LEmployeeLeave();
                entity.EmployeeId = employeeId;
                entity.LeaveTypeId = leaveId;

                mgr.RemoveLeaveFromEmployee(entity);
            }
            return mgr.GetHTMLEmployeeLeaveList(employeeId);
        }

        [WebMethod(true)]
        public string ChangeIsCurrentStatus(int companyId)
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated == false || SessionManager.CurrentLoggedInEmployeeId != 0)
                return "";

            MyCache.Reset();
            CompanyManager mgr = new CompanyManager();
            return mgr.ChangeIsDefault(companyId);
        }

        [WebMethod(true)]
        public int GetDefaultCompany()
        {
            
            CompanyManager mgr = new CompanyManager();
            return mgr.GetDefault();
        }

        [WebMethod(true)]
        public string GetTaxList(int taxId, int employeeId)
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated == false)
                return "";
            TaxManager tax = new TaxManager();
            if (taxId != 0)
            {
                PEmployeeTax emptax = new PEmployeeTax();
                emptax.EmployeeId = employeeId;
                emptax.TaxId = taxId;
                tax.Delete(emptax);
            }
            return tax.GetHTMLEmployeeTaxList(employeeId);
        }

        [WebMethod(true)]
        public string GetRemoveAreaValue(int vdcId)
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated == false)
                return "";
            CommonManager mgr = new CommonManager();
            return mgr.GetRemoteAreaValue(vdcId);
        }


        [WebMethod(true)]
        public string DeleteDocument(int documentId, int employeeId)
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated == false || SessionManager.CurrentLoggedInEmployeeId != 0)
                return "";

            HRManager mgr = new HRManager();
            HDocument doc = new HDocument();
            doc.DocumentId = documentId;
            mgr.DeleteDocument(doc);
            return mgr.GetHTMLDocuments(employeeId);
        }

        [WebMethod(true)]
        public string GetEmpContribution(int employeeId)
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated == false)
                return "";
            EmployeeManager mgr = new EmployeeManager();
            return mgr.GetContribution(employeeId);
        }

        [WebMethod(true)]
        public string DeleteEmpContribution(int contributionId, int employeeId)
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated == false)
                return "";
            EmployeeManager mgr = new EmployeeManager();
            EContribution entity = new EContribution();
            entity.ContributionId = contributionId;
            if (mgr.Delete(entity))
                return GetEmpContribution(employeeId);
            return "";
        }

        [WebMethod(true)]
        public string SaveContribution(int employeeId, string desc, double percent, string startDate, string endDate)
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated == false || SessionManager.CurrentLoggedInEmployeeId != 0)
                return "";

            EContribution entity = new EContribution();
            entity.EmployeeId = employeeId;
            entity.Description = desc;
            entity.Percentage = double.Parse(Util.FormatPercentForInput(percent));
            entity.StartDate = startDate;
            entity.EndDate = endDate;

            EmployeeManager mgr = new EmployeeManager();
            try
            {
                mgr.Save(entity);
                return GetEmpContribution(employeeId);
            }
            catch (ArgumentException)
            {
                return "PercentError";
            }
        }


        //[WebMethod(true)]
        //public void SaveDeleteGratuityRule(int incomeId, bool saveAll, bool removeAll)
        //{
        //    CommonManager mgr = new CommonManager();
        //    mgr.SaveDeleteGratuityRule(incomeId, saveAll, removeAll);
        //}


        //[WebMethod(true)]
        //public string[] GetActiveDirectoryUser(string prefixText)
        //{
        //    if (HttpContext.Current.User.Identity.IsAuthenticated == false || SessionManager.CurrentLoggedInEmployeeId != 0)
        //        return new string[] { };

        //    prefixText = prefixText.ToLower();

        //    List<string> suggestions = new List<string>();

        //    string[] values = LdapHelper.GetAllAdDomainUserList().ToArray();

        //    foreach (string dr in values)
        //    {
        //        if (dr.ToLower().Contains(prefixText))
        //        {
        //            suggestions.Add(AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem(dr, dr));
        //        }
        //    }
        //    return suggestions.ToArray();

        //}

        [WebMethod(true)]
        public string[] GetEmployeeNames(string prefixText)
        {
            prefixText = prefixText.Trim();

            //if (HttpContext.Current.User.Identity.IsAuthenticated == false)
            //    return new string[] { };

            EmployeeManager mgr = new EmployeeManager();

            List<string> suggestions = new List<string>();

            // for Supervisor or Manager
            if (SessionManager.CurrentLoggedInEmployeeId != 0)
            {
                List<TextValue> list = LeaveAttendanceManager.GetEmployeeListForLeaveAssign()
                        .Where(x => x.Text.ToLower().Contains(prefixText.ToLower())).ToList();
                foreach (TextValue dr in list)
                {
                    suggestions.Add(AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem(dr.Text, dr.Value.ToString()));

                }
            }
            else
            {

               

                //string customRoleDepartmentList = null;
                //if (SessionManager.IsCustomRole)
                //    customRoleDepartmentList = SessionManager.CustomRoleDeparmentIDList;

                List<GetPrefixedEmpNamesResult> list = mgr.GetPrefixedEmplyeeNames(prefixText.ToLower(), null, false);

                foreach (GetPrefixedEmpNamesResult dr in list)
                {
                    suggestions.Add(AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem(dr.Name, dr.EmployeeId.ToString()));

                }
            }
            return suggestions.ToArray();

        }

        [WebMethod(true)]
        public string[] GetEmployeeNamesWithID(string prefixText)
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated == false)
                return new string[] { };
            prefixText = prefixText.Trim();

            bool isEIN = false;
            int eid = 0;
            if (int.TryParse(prefixText, out eid))
            {
                isEIN = true;
            }

            if (HttpContext.Current.User.Identity.IsAuthenticated == false || SessionManager.CurrentLoggedInEmployeeId != 0)
                return new string[] { };

            EmployeeManager mgr = new EmployeeManager();

            List<string> suggestions = new List<string>();

            string customRoleDepartmentList = null;
            if (SessionManager.IsCustomRole)
                customRoleDepartmentList = SessionManager.CustomRoleDeparmentIDList;

            List<GetPrefixedEmpNamesWithIDResult> list = mgr.GetPrefixedEmplyeeNamesWithID(isEIN, eid, prefixText.ToLower(), customRoleDepartmentList, false);

            foreach (GetPrefixedEmpNamesWithIDResult dr in list)
            {
                suggestions.Add(AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem(dr.Name, dr.EmployeeId.ToString()));

            }
            return suggestions.ToArray();

        }

        [WebMethod(true)]
        public string[] GetEmployeeNamesWithRetiredAndID(string prefixText)
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated == false)
                return new string[] { };
            prefixText = prefixText.Trim();

            bool isEIN = false;
            int eid = 0;
            if (int.TryParse(prefixText, out eid))
            {
                isEIN = true;
            }

            if (HttpContext.Current.User.Identity.IsAuthenticated == false || SessionManager.CurrentLoggedInEmployeeId != 0)
                return new string[] { };

            EmployeeManager mgr = new EmployeeManager();

            List<string> suggestions = new List<string>();

            string customRoleDepartmentList = null;
            if (SessionManager.IsCustomRole)
                customRoleDepartmentList = SessionManager.CustomRoleDeparmentIDList;

            List<GetPrefixedEmpNamesWithIDResult> list = mgr.GetPrefixedEmplyeeNamesWithID(isEIN, eid, prefixText.ToLower(), customRoleDepartmentList, true);

            foreach (GetPrefixedEmpNamesWithIDResult dr in list)
            {
                suggestions.Add(AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem(dr.Name, dr.EmployeeId.ToString()));

            }
            return suggestions.ToArray();

        }

        [WebMethod(true)]
        public string[] GetEmployeeNamesWithIDAndINo(string prefixText)
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated == false)
                return new string[] { };
            prefixText = prefixText.Trim();

            bool isEIN = false;
            int eid = 0;
            if (int.TryParse(prefixText, out eid))
            {
                isEIN = true;
            }

            if (HttpContext.Current.User.Identity.IsAuthenticated == false || SessionManager.CurrentLoggedInEmployeeId != 0)
                return new string[] { };

            EmployeeManager mgr = new EmployeeManager();

            List<string> suggestions = new List<string>();

            string customRoleDepartmentList = null;
            if (SessionManager.IsCustomRole)
                customRoleDepartmentList = SessionManager.CustomRoleDeparmentIDList;

            List<GetPrefixedEmpNamesWithIDAndINoResult> list = mgr.GetPrefixedEmplyeeNamesWithIDAndINo(isEIN, eid, prefixText.ToLower(), customRoleDepartmentList, false);

            foreach (GetPrefixedEmpNamesWithIDAndINoResult dr in list)
            {
                suggestions.Add(AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem(dr.Name, dr.EmployeeId.ToString()));

            }

            return suggestions.ToArray();

        }
        [WebMethod(true)]
        public string[] GetRetiredEmployeeNamesWithID(string prefixText)
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated == false)
                return new string[] { };
            prefixText = prefixText.Trim();

            bool isEIN = false;
            int eid = 0;
            if (int.TryParse(prefixText, out eid))
            {
                isEIN = true;
            }

            if (HttpContext.Current.User.Identity.IsAuthenticated == false || SessionManager.CurrentLoggedInEmployeeId != 0)
                return new string[] { };

            EmployeeManager mgr = new EmployeeManager();

            List<string> suggestions = new List<string>();

            string customRoleDepartmentList = null;
            if (SessionManager.IsCustomRole)
                customRoleDepartmentList = SessionManager.CustomRoleDeparmentIDList;

            List<GetRetiredPrefixedEmpNamesWithIDResult> list = mgr.GetRetiredPrefixedEmpNamesWithID(isEIN, eid, prefixText.ToLower(), customRoleDepartmentList, false);

            foreach (GetRetiredPrefixedEmpNamesWithIDResult dr in list)
            {
                suggestions.Add(AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem(dr.Name, dr.EmployeeId.ToString()));

            }
            return suggestions.ToArray();

        }

        [WebMethod(true)]
        public string[] GetEmployeeNamesWithRetiredAlso(string prefixText)
        {
            prefixText = prefixText.Trim();

            if (HttpContext.Current.User.Identity.IsAuthenticated == false)
                return new string[] { };

            EmployeeManager mgr = new EmployeeManager();

            List<string> suggestions = new List<string>();

            string customRoleDepartmentList = null;
            if (SessionManager.IsCustomRole)
                customRoleDepartmentList = SessionManager.CustomRoleDeparmentIDList;

            if (SessionManager.CurrentLoggedInEmployeeId == 0)
            {
                List<GetPrefixedEmpNamesResult> list = mgr.GetPrefixedEmplyeeNames(prefixText.ToLower(), customRoleDepartmentList, true);

                foreach (GetPrefixedEmpNamesResult dr in list)
                {
                    suggestions.Add(AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem(dr.Name, dr.EmployeeId.ToString()));

                }
            }
            else
            {
                List<TextValue> list = LeaveAttendanceManager.GetEmployeeListForLeaveAssign().Where(x => x.Text.ToLower().Contains(prefixText.ToLower())).ToList();

                foreach (TextValue dr in list)
                {
                    suggestions.Add(AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem(dr.Text, dr.Value.ToString()));

                }
            }

            return suggestions.ToArray();

        }

        [WebMethod(true)]
        public string[] GetEmployeeNamesAndEINWithRetiredAlso(string prefixText)
        {
            prefixText = prefixText.Trim();

            if (HttpContext.Current.User.Identity.IsAuthenticated == false)
                return new string[] { };

            EmployeeManager mgr = new EmployeeManager();

            List<string> suggestions = new List<string>();

            string customRoleDepartmentList = null;
            if (SessionManager.IsCustomRole)
                customRoleDepartmentList = SessionManager.CustomRoleDeparmentIDList;

            if (SessionManager.CurrentLoggedInEmployeeId == 0)
            {
                List<GetPrefixedEmpNamesResult> list = mgr.GetPrefixedEmplyeeNames(prefixText.ToLower(), customRoleDepartmentList, true);

                foreach (GetPrefixedEmpNamesResult dr in list)
                {
                    suggestions.Add(AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem(dr.Name + " - " + dr.EmployeeId.ToString(), dr.EmployeeId.ToString()));

                }
            }
            else
            {
                List<TextValue> list = LeaveAttendanceManager.GetEmployeeListForLeaveAssign().Where(x => x.Text.ToLower().Contains(prefixText.ToLower())).ToList();

                foreach (TextValue dr in list)
                {
                    suggestions.Add(AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem(dr.Text, dr.Value.ToString()));

                }
            }

            return suggestions.ToArray();

        }

        [WebMethod(true)]
        public string[] GetEmployeeByIdNo(string prefixText)
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated == false || SessionManager.CurrentLoggedInEmployeeId != 0)
                return new string[] { };

            EmployeeManager mgr = new EmployeeManager();

            List<string> suggestions = new List<string>();

            string customRoleDepartmentList = null;
            if (SessionManager.IsCustomRole)
                customRoleDepartmentList = SessionManager.CustomRoleDeparmentIDList;

            List<GetPrefixedEmpIdCardNoResult> list = mgr.GetPrefixedEmplyeeIDCardNo(prefixText, customRoleDepartmentList);

            foreach (GetPrefixedEmpIdCardNoResult dr in list)
            {
                suggestions.Add(AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem(dr.Name, dr.EmployeeId.ToString()));

            }
            return suggestions.ToArray();

        }
        #region Calculation

        /// <summary>
        /// Ajax method when tax dependent variable or adjustment income is changed
        /// </summary>
        //[WebMethod(true)]
        //public  string GetUpdateAmountForTax(string content, int payrollPeriodId,
        //    bool isResignedOrRetiredOnly)
        //{
         

        //    if (HttpContext.Current.User.Identity.IsAuthenticated == false || SessionManager.CurrentLoggedInEmployeeId != 0)
        //        return "";

        //    string[] str = content.Split(new char[] { ':' }, StringSplitOptions.RemoveEmptyEntries);

        //    int empId = int.Parse(str[0]);
        //    CalculationColumnType columnType = (CalculationColumnType)int.Parse(str[1]);
        //    int sourceId = int.Parse(str[2]);
        //    decimal amount = decimal.Parse(str[3]);

        //    return CalculationManager.HandleToReCalcTaxAfterAmountChangedInGrid(empId, columnType, sourceId, amount,
        //                                                                        payrollPeriodId, isResignedOrRetiredOnly);

        //}
        #endregion

        [WebMethod]
        public void ToggleCheckList(int checkListId)
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated == false)
                return;
            CommonManager.ToggleCheckListState(checkListId);
        }

        #region "Overtime"

        [WebMethod(true)]
        public string GetOvertimeInfo(int employeeId, string timeSlot)
        {

            if (HttpContext.Current.User.Identity.IsAuthenticated == false)
                return "";
            EmployeeManager empMgr = new EmployeeManager();
            EEmployee emp = EmployeeManager.GetEmployeeById(employeeId);

            OOvertimeSetting setting = OvertimeManager.GetOvertimeSettingForEmployee(emp.GradeId.Value, timeSlot);

            //no setting then return as empty as not overtime defined
            if (setting == null)
            {
                return string.Empty;
            }

            OTTreatmentTypeEnum treatment = (OTTreatmentTypeEnum) setting.TreatmentType.Value;

            if( treatment==OTTreatmentTypeEnum.Overtime)
                return string.Format( Resources.Messages.OvertimeOTInfo,setting.RatePercent);
            else
                return string.Format(Resources.Messages.OvertimeLeaveInfo,setting.LeaveHours);                    

        }

        #endregion 

        [WebMethod]
        public string[] GetEmpNamesForEmployee(string prefixText)
        {
            prefixText = prefixText.Trim();

            if (HttpContext.Current.User.Identity.IsAuthenticated == false)
                return new string[] { };

            EmployeeManager mgr = new EmployeeManager();

            List<string> suggestions = new List<string>();

            string customRoleDepartmentList = null;
            if (SessionManager.IsCustomRole)
                customRoleDepartmentList = SessionManager.CustomRoleDeparmentIDList;

            List<GetPrefixedEmpNamesResult> list = mgr.GetPrefixedEmplyeeNames(prefixText.ToLower(), customRoleDepartmentList, false);

            foreach (GetPrefixedEmpNamesResult dr in list)
            {
                suggestions.Add(AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem(dr.Name, dr.EmployeeId.ToString()));

            }
            return suggestions.ToArray();

        }

        [WebMethod(true)]
        public string DeleteEmployeeProbation(int statusId)
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated == false || SessionManager.CurrentLoggedInEmployeeId != 0)
                return "";

            int empId = EmployeeManager.DeleteProbation(statusId);

            return new EmployeeManager().GetProbationList(empId);
        }

        [WebMethod(true)]
        public string GetProbationList(int empId)
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated == false || SessionManager.CurrentLoggedInEmployeeId != 0)
                return "";



            return new EmployeeManager().GetProbationList(empId);
        }

        [WebMethod(true)]
        public string[] GetEmployeeNamesWithIDAndINoWithEmpPhoto(string prefixText)
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated == false)
                return new string[] { };
            prefixText = prefixText.Trim();

            bool isEIN = false;
            int eid = 0;
            if (int.TryParse(prefixText, out eid))
            {
                isEIN = true;
            }

            if (HttpContext.Current.User.Identity.IsAuthenticated == false || SessionManager.CurrentLoggedInEmployeeId != 0)
                return new string[] { };

            EmployeeManager mgr = new EmployeeManager();

            List<string> suggestions = new List<string>();

            string customRoleDepartmentList = null;
            if (SessionManager.IsCustomRole)
                customRoleDepartmentList = SessionManager.CustomRoleDeparmentIDList;

            List<GetPrefixedEmpNamesWithIDAndINoAnPhotoUrlResult> list = mgr.GetPrefixedEmplyeeNamesWithIDAndINoWithEmpPhoto(isEIN, eid, prefixText.ToLower(), customRoleDepartmentList, false);

            foreach (GetPrefixedEmpNamesWithIDAndINoAnPhotoUrlResult dr in list)
            {
                string photoUrl = "";

                if (!string.IsNullOrEmpty(dr.PhotoUrl))
                    photoUrl = Config.UploadLocation + "/" + dr.PhotoUrl;
                else
                {
                    if (dr.Gender == 1)
                        photoUrl = "malepic";
                    else
                        photoUrl = "femalepic";
                }

                string name = photoUrl + ":" + dr.Name;
                //System.Net.WebUtility.HtmlEncode()
                suggestions.Add(AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem(name, dr.EmployeeId.ToString()));

            }

            return suggestions.ToArray();

        }


        [WebMethod(true)]
        public string[] GetMenuSearchList(string prefixText)
        {
            
            if (HttpContext.Current.User.Identity.IsAuthenticated == false)
                return new string[] { };
            prefixText = prefixText.Trim().ToLower();

            XmlDocument doc = MyCache.GetMenuXml(SessionManager.User.UserID.ToString(),CommonManager.CompanySetting.HasLevelGradeSalary);

            List<MenuSearchClass> listResult = new List<MenuSearchClass>();

            XmlNodeList moduleList = doc.ChildNodes[1].ChildNodes;


            foreach (XmlNode module in moduleList)
            {
                MenuSearchClass objModule = new MenuSearchClass();
                objModule.moduleId = int.Parse(module.Attributes["moduleId"].Value);
                objModule.title = module.Attributes["title"].Value;
                objModule.url = module.Attributes["url"].Value;
                objModule.cssClass = "moduleCssClass";
                objModule.IsModule = true;
                listResult.Add(objModule);

                int childPageCount = 0;

                XmlNodeList groupList = module.ChildNodes;

                foreach (XmlNode group in groupList)
                {
                    XmlNodeList pageList = group.ChildNodes;

                    foreach (XmlNode page in pageList)
                    {
                        // hide page for Non Level Grade
                        if(page != null && page.Attributes != null && CommonManager.CompanySetting.HasLevelGradeSalary == false 
                            && page.Attributes["hideForNonLevelCompany"] != null && page.Attributes["hideForNonLevelCompany"].Value.ToLower().Trim() == "true")
                        {
                        }

                        else if (page.Attributes["title"].Value.Trim().ToLower().Contains(prefixText))
                        {
                            if (UserManager.IsPageAccessible(page.Attributes["url"].Value.ToLower()))
                            {
                                MenuSearchClass objChildPage = new MenuSearchClass();
                                objChildPage.moduleId = int.Parse(page.Attributes["moduleId"].Value);
                                objChildPage.title = page.Attributes["title"].Value;
                                objChildPage.url = page.Attributes["url"].Value;
                                objChildPage.IsModule = false;
                                objChildPage.cssClass = "childCssClass";
                                listResult.Add(objChildPage);
                                childPageCount++;
                            }
                        }
                    }
                }

                if (childPageCount == 0)
                    listResult.Remove(objModule);
            }

            List<string> suggestions = new List<string>();

            foreach (var item in listResult)
            {
                string name = "";
                if (item.IsModule)
                    name = item.moduleId.ToString() + ":" + item.title;
                else
                    name = "0" + ":" + item.title;
                    

                //System.Net.WebUtility.HtmlEncode()
                suggestions.Add(AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem(name, item.url.ToString()));
            }
            

            return suggestions.ToArray();

        }


   

    }
}
