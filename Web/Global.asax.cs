﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using Utils;
using System.Threading;
using System.Text;
using System.IO;
using BLL;
using System.Security.Principal;
using Utils.Security;
using DAL;
using BLL.Manager;
using Utils.Helper;
using DevExpress.XtraReports.Security;
using DevExpress.DataAccess.Web;
using DevExpress.DataAccess.Sql;
using DevExpress.Xpo.DB;

namespace Web
{
    public class Global : System.Web.HttpApplication
    {

        public class LoggedInUser
        {
            public bool IsEmployee { get; set; }
            public string IPAddress { get; set; }
            public string UserName { get; set; }
        }

        protected void Application_Start(object sender, EventArgs e)
        {
            // from Devexpress 16 permission required to execute script so this code needed
            ScriptPermissionManager.GlobalInstance = new ScriptPermissionManager(ExecutionMode.Unrestricted);
            //List<LoggedInUser> list = new List<LoggedInUser>();
            //Application["UsersOnline"] = list;

            DevExpress.XtraReports.Web.QueryBuilder.DefaultQueryBuilderContainer.Register<IDataSourceWizardDBSchemaProviderFactory, MyDataSourceWizardDBSchemaProviderFactory>();
            //string[] values = Config.Values;
            //if (values != null)            
            //{
            //    SecurityHelper.isValidToRun = true;
            //    SecurityHelper.hasPwd = Convert.ToBoolean(values[0]);
            //    SecurityHelper.pwd = values[1];
            //}

        }




        protected void Session_Start(object sender, EventArgs e)
        {
            //CommonManager.SaveNeededTableIfNotExists();
            //SessionStartLoggedIn();

            // can not place this like method, makes application slow
           // AttendanceManager.GenerateAsyncAttendanceEmployeeTable();
        }

        public void SessionStartLoggedIn()
        {
            //Application.Lock();
            //List<LoggedInUser> list = Application["UsersOnline"] as List<LoggedInUser>;
            //if (list != null && !string.IsNullOrEmpty(User.Identity.Name))
            //{
            //    string userName = User.Identity.Name;
            //    if (list.Any(x => x.UserName == userName) == false)
            //    {
            //        UUser user = UserManager.GetUserByUserName(userName);
            //        LoggedInUser log = new LoggedInUser();
            //        log.IsEmployee = user.IsEmployeeType == null ? false : user.IsEmployeeType.Value;
            //        log.UserName = userName;
            //        log.IPAddress = GetClientIP();
            //        list.Add(log);
            //    }


            //    Session["UserName"] = User.Identity.Name;
            //}
            ////Application["UsersOnline"] = (int)Application["UsersOnline"] + 1;
            //Application.UnLock();
        }

        public string GetClientIP()
        {
            HttpRequest currentRequest = HttpContext.Current.Request;
            string ipAddress = currentRequest.ServerVariables["HTTP_X_FORWARDED_FOR"];

            if (ipAddress == null || ipAddress.ToLower() == "unknown")
                ipAddress = currentRequest.ServerVariables["REMOTE_ADDR"];

            return ipAddress;
        }

		
		
		
		
		
		
		
		
		
        protected void Session_End(object sender, EventArgs e)
        {
            //Application.Lock();
            //List<LoggedInUser> list = Application["UsersOnline"] as List<LoggedInUser>;
            //if (Session["UserName"] != null)
            //{
            //    string userName = Session["UserName"].ToString();
            //    if (!string.IsNullOrEmpty(userName))
            //    {
            //        LoggedInUser log = list.FirstOrDefault(x => x.UserName == userName);
            //        if (log != null)
            //        {
            //            list.Remove(log);
            //        }
            //    }
            //}
          
            //Application.UnLock();
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            // Intialization codes
            //string test = Config.GetEmailSenderName;

            //if (LicenseValidator.isLicenseEnabled)
            //{
            //    if (LicenseValidator.IsAllowedToRun == false)
            //    {
            //        HttpContext.Current.Response.Write("<h3 style='color:red'>" + LicenseValidator.LicenseErrorMessage + "</h3>");
            //        HttpContext.Current.Response.End();
            //    }
            //}


            //if (SecurityHelper.isValidToRun == null)
            //{
            //    Response.Write("<h3 style='color:red'>" + Resources.Messages.InvalidInstallationMsg + "</h3>");
            //    Response.End();
            //    Response.Close();
            //}

            
        }

        protected void Application_EndRequest(object sender, EventArgs e)
        {

            BLL.BaseBiz.Dispose();

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {
            // Extract the forms authentication cookie
            string cookieName = FormsAuthentication.FormsCookieName;
            HttpCookie authCookie = Context.Request.Cookies[cookieName];
            if (null == authCookie)
            {
                
                // There is no authentication cookie, then redirect if not login page
                if ( (Request.Url.AbsoluteUri.ToLower().Contains("/default.aspx") == false &&
                    Request.Url.AbsoluteUri.ToLower().Contains("/forgotpassword.aspx") == false &&
                    Request.Url.AbsoluteUri.ToLower().Contains("/telephonedirectory.aspx") == false
                    )
                    && Request.Url.AbsoluteUri.ToLower().EndsWith("aspx"))
                {
                    if (Request.Url.AbsoluteUri.ToString().ToLower().Contains("/employee/"))
                        Response.Redirect("~/Employee/Default.aspx", true);
                    else
                        Response.Redirect("~/Default.aspx", true);
                }
                return;

            }
            FormsAuthenticationTicket authTicket = null;
            try
            {
                authTicket = FormsAuthentication.Decrypt(authCookie.Value);
            }
            catch (Exception ex)
            {
                // Log exception details (omitted for simplicity)
                return;
            }
            if (null == authTicket)
            {
                // Cookie failed to decrypt.
                return;
            }
            // When the ticket was created, the UserData property wasassigned
            // a pipe delimited string of role names.
            string[] roles = new string[1];
            roles[0] = authTicket.UserData.ToString().Split(new char[] { ':' })[0];

     

            // Create an Identity object
            FormsIdentity id = new FormsIdentity(authTicket);
            // This principal will flow throughout the request.
            GenericPrincipal principal = new GenericPrincipal(id, roles);
            // Attach the new principal object to the current HttpContextobject
            Context.User = principal;


           
        }

        protected void Application_Error(object sender, EventArgs e)
        {

            BLL.BaseBiz.Dispose();


            string body = "";
            HttpContext context = HttpContext.Current;

            bool error = false;
            if (context.AllErrors != null && context.AllErrors.Length > 0)
            {
                if (context.Session != null)
                {
                    try
                    {
                        error = true;
                        File.WriteAllText(Server.MapPath("~/App_Data/logs/message.log"), "");
                    }
                    catch { }
                }

                foreach (Exception ex in context.AllErrors)
                {
                    if (ex != null && !(ex is ThreadAbortException) && !(ex.Message.Equals("File does not exist.")))
                    {
                        //body += BuildExceptionDescription(ex);

                        //MaybeRestartWorkApplication(ex, utility);
                        Log.log("Error", ex);

                        if (error == true)
                        {
                            File.WriteAllText(Server.MapPath("~/App_Data/logs/message.log"), ex.ToString());                          
                            
                        }
                    }
                }
                //LogException(body);
            }

        }


        //private static void LogException(string body)
        //{
            
        //    string file = HttpContext.Current.Server.MapPath(Config.LogFileLoc);
        //    if (System.IO.File.Exists(file))
        //    {
        //        System.IO.FileInfo fi = new System.IO.FileInfo(file);
        //        // Limiting errorlog file max size to 10MB
        //        // It prevents errorlog from growing to whole hard-drive
        //        // Once it becomes greater than 10 MB, it deletes the file and write just arrived error.
        //        if (fi.Length > (long)10485760)
        //        {
        //            GC.Collect();
        //            GC.WaitForPendingFinalizers();
        //            System.IO.File.Delete(file);
        //        }
        //    }

        //    //using (FileStream stream = new FileStream(file, System.IO.FileMode.Append, System.IO.FileAccess.Write))
        //    //{
        //    //    using (StreamWriter writer = new StreamWriter(stream))
        //    //    {
        //    //        writer.WriteLine("\n" + body);
        //    //    }
        //    //}
            
        //}

        /// <summary>
        /// Gets a stacktrace string, including inner exceptions.
        /// </summary>
        /// <param name="ex"></param>
        /// <returns></returns>
        private static string GetStackTrace(Exception ex)
        {
            StringBuilder b = new StringBuilder();
            Exception current = ex;
            int depth = 0;
            while (current != null && depth < 20)
            {
                b.AppendLine(string.Format("{0}: {1}", current.GetType().FullName, current.Message));
                b.AppendLine(current.StackTrace);
                b.AppendLine("");
                current = current.InnerException;
                depth++;
            }
            return b.ToString();
        }

        /// <summary>
        /// Present a lot of information about an exception
        /// </summary>
        /// <param name="ex"></param>
        /// <returns></returns>
        private static string BuildExceptionDescription(Exception ex)
        {
            return string.Format(
                "Unhandled {2}: {0}<br /><br />Source: {3}<br /><br />{1}",
                ex.Message,
                GetStackTrace(ex).Replace("\n", "<br />"),
                ex.GetType().FullName,
                ex.Source);
        }

      

        protected void Application_End(object sender, EventArgs e)
        {

        }
    }


    public class MyDataSourceWizardDBSchemaProviderFactory : IDataSourceWizardDBSchemaProviderFactory
    {
        public IDBSchemaProvider Create()
        {
            return new MyDBSchemaProvider();
        }
    }

    public class MyDBSchemaProvider : DevExpress.DataAccess.Sql.IDBSchemaProvider
    {
        public DBSchema GetSchema(DevExpress.DataAccess.Sql.SqlDataConnection connection, SchemaLoadingMode schemaLoadingMode)
        {
            //Load DB Schema without loading columns 
            DevExpress.DataAccess.Sql.DBSchema defaultSchema = connection.GetDBSchema(false);
            //Select only required tables/views/procedures 
            // skip all tables
            DBTable[] tables = defaultSchema.Tables.Where((table) => { return table.Name.StartsWith("Csdfdafadf"); }).ToArray();
            DBTable[] views = defaultSchema.Views.Where((view) => { return view.Name.Contains("AttendanceCheckInCheckOutView") == false; }).ToArray();
            DBStoredProcedure[] storedProcedures = defaultSchema.StoredProcedures;
            //Create a new schema 
            return new DBSchema(tables, views, storedProcedures);
        }
        public void LoadColumns(DevExpress.DataAccess.Sql.SqlDataConnection connection, params DevExpress.Xpo.DB.DBTable[] tables)
        {
            //Load columns for current tables 
            connection.LoadDBColumns(tables);
        }
    }

}