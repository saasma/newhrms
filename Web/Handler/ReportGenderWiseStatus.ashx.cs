﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
namespace Web.Handler
{
    /// <summary>
    /// Summary description for ReportGenderWiseStatus
    /// </summary>
    public class ReportGenderWiseStatus : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "application/json";
            int start = 0;
            int pagesize = 50;
            string StatusName = "-1";

            if (HttpContext.Current.Request.IsAuthenticated == false ||
                SessionManager.CurrentLoggedInEmployeeId != 0)
            {
                return;
            }

            if (!string.IsNullOrEmpty(context.Request["StatusName"]))
            {
               int Status =  int.Parse(context.Request["StatusName"].ToString());
              StatusName =  CommonManager.GetStatusName().SingleOrDefault(x=>x.StatusId==Status).Name;
            }

            //PageSize
            if (!string.IsNullOrEmpty(context.Request["PageSize"]))
                pagesize = Convert.ToInt32(context.Request["PageSize"]);


            Paging<ReportNewHR_GenderWiseStatusResult> list = null;
            List<ReportNewHR_GenderWiseStatusResult> resultSet = NewHRManager.GetGenderWiseStatusReport(start, pagesize, StatusName);

            if (resultSet.Count > 0)
                list = new Paging<ReportNewHR_GenderWiseStatusResult>(resultSet, Convert.ToInt32(resultSet[0].TotalRows));
            else
                list = new Paging<ReportNewHR_GenderWiseStatusResult>(resultSet, 0);


            context.Response.Write(JSON.Serialize(list));
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}