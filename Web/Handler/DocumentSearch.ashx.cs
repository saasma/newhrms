﻿using System;
using System.Web;
using System.Web.Services;
using BLL;
using BLL.Manager;
using System.Collections.Generic;
using DAL;
using Ext.Net;
using BLL.BO;
namespace Web.Handler
{
    /// <summary>
    /// Summary description for DocumentSearch
    /// </summary>
    public class DocumentSearch : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            string query = context.Request["query"];

            if (query == null)
                return;

            query = query.Trim();

            List<TextValue> list = new List<TextValue>();

            string pageName = HttpContext.Current.Request.UrlReferrer.AbsoluteUri;
            if (pageName.ToLower().Contains("dashboarddocument.aspx"))
                list = DocumentManager.GetDocumentSearchByTitlePartyNameID(query);
            else
                list = DocumentManager.GetDocumentNameSearch(query);

            context.Response.Write(string.Format("{{total:{1},'plants':{0}}}", JSON.Serialize(list), list.Count));
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}