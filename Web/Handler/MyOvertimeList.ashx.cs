﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;

namespace Web.Handler
{
    /// <summary>
    /// Summary description for OvertimeListForApproval
    /// </summary>
    /// 
    public class MyOvertimeList : IHttpHandler
    {
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "application/json";
            int start = 0;
            int pagesize = 50;
            int EmployeeId = -1;
            int OverTimePeriodID = -1;
            int status = -1;
            DateTime? StartDate = null;
            DateTime? EndDate = null;

            if (!string.IsNullOrEmpty(context.Request["status"]))
                status = Convert.ToInt32(context.Request["status"].ToString());

            

            //PageSize
            if (!string.IsNullOrEmpty(context.Request["PageSize"]))
                pagesize = Convert.ToInt32(context.Request["PageSize"]);

           
            if (!string.IsNullOrEmpty(context.Request["OverTimePeriodID"]))
                OverTimePeriodID = Convert.ToInt32(context.Request["OverTimePeriodID"]);


            OvertimePeriod _OverTimPeriod= OvertimeManager.GetOverTimPeriodById(OverTimePeriodID);
            if (_OverTimPeriod != null)
            {
                StartDate = _OverTimPeriod.StartDate;
                EndDate = _OverTimPeriod.EndDate;
            }


            Paging<GetMyOvertimeListResult> list = null;
            List<GetMyOvertimeListResult> resultSet = OvertimeManager.GetMyOvertimelist(start, pagesize, status, 
                StartDate, EndDate);

            if (resultSet.Count > 0)
                list = new Paging<GetMyOvertimeListResult>(resultSet, Convert.ToInt32(resultSet[0].TotalRows));
            else
                list = new Paging<GetMyOvertimeListResult>(resultSet, 0);
            context.Response.Write(JSON.Serialize(list));
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}