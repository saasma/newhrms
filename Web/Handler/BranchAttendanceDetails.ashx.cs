﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using Utils.Calendar;

namespace Web.Handler
{
    /// <summary>
    /// Summary description for BranchAttendanceDetails
    /// </summary>
    public class BranchAttendanceDetails : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated == false || SessionManager.CurrentLoggedInEmployeeId != 0)
            {
                context.Response.Write("");
                context.Response.End();
                return;
            }

            context.Response.ContentType = "application/json";
            int start = 0, departmentId = -1;
            int pagesize = 100;
            string sort = "";
            bool GridOrThumbnail = true;
            string branchId = "";
            DateTime selectedDate = DateTime.Today;


            if (!string.IsNullOrEmpty(context.Request["start"]))
                start = Convert.ToInt32(context.Request["start"].ToString());
            
            if (!string.IsNullOrEmpty(context.Request["PageSize"]))
                pagesize = Convert.ToInt32(context.Request["PageSize"]);

            if (!string.IsNullOrEmpty(context.Request["Branch"]))
                branchId = context.Request["Branch"];

            if (!string.IsNullOrEmpty(context.Request["Department"]))
                departmentId = int.Parse(context.Request["Department"].ToString());

            if (!string.IsNullOrEmpty(context.Request["SelectedDate"]))
            {
                if (BLL.BaseBiz.IsEnglish)
                    selectedDate = Convert.ToDateTime(context.Request["SelectedDate"]);
                else
                {
                    string date = context.Request["SelectedDate"];
                    CustomDate cd = CustomDate.GetCustomDateFromString(date, false);
                    selectedDate = cd.EnglishDate;
                }
            }

            int counter = 0;

            List<EmpBranchAttendanceDetails> resultSet = new List<EmpBranchAttendanceDetails>();

            Paging<EmpBranchAttendanceDetails> list = new Paging<EmpBranchAttendanceDetails>();

            List<AttendanceAllEmployeeReportNewResult> listRecords = AttendanceManager.GetAttendanceDailyReportNew(selectedDate, 0, branchId, departmentId);

            List<AttendanceAllEmployeeReportNewResult> listTimely = listRecords.Where(x => (x.AtteState != 4 && x.AtteState != 9 && (x.InState == 1 || x.InState == 2) && (x.OutState == 1 || x.OutState == 2))).OrderBy(x => x.EmplooyeeName).ToList();

            foreach (var item in listTimely)
            {
                EmpBranchAttendanceDetails obj = new EmpBranchAttendanceDetails();
                obj.EmployeeId = item.EmployeeId.Value;
                obj.Name = item.EmplooyeeName;
                obj.UrlPhoto = GetEmployeePhotoUrl(item.EmployeeId.Value);
                obj.InTimeValue = (item.InTime == null ? "" : DateTime.Parse(item.InTime.Value.ToString()).ToString("hh:mm tt"));
                obj.OutTimeValue = (item.OutTime == null ? "" : DateTime.Parse(item.OutTime.Value.ToString()).ToString("hh:mm tt"));
                obj.cssCls = "clsTimely";
                obj.cssCls2 = "clsTimely2";
                obj.cssCls3 = "clsTimely2";
                obj.SN = ++counter;
                resultSet.Add(obj);
            }

            

            List<AttendanceAllEmployeeReportNewResult> listUntimely = listRecords.Where(x => (x.AtteState != 4 && x.AtteState != 9) && (x.InState == 3 || x.OutState == 3)).OrderBy(x => x.EmplooyeeName).ToList();
            //foreach (var item in listUntimely)
            //{
            //    EmpBranchAttendanceDetails obj = new EmpBranchAttendanceDetails();
            //    obj.EmployeeId = item.EmployeeId.Value;
            //    obj.Name = item.EmplooyeeName;
            //    obj.UrlPhoto = GetEmployeePhotoUrl(item.EmployeeId.Value);
            //    obj.InTimeValue = (item.InTime == null ? "" : DateTime.Parse(item.InTime.Value.ToString()).ToString("hh:mm tt"));
            //    obj.OutTimeValue = (item.OutTime == null ? "" : DateTime.Parse(item.OutTime.Value.ToString()).ToString("hh:mm tt"));
            //    obj.cssCls = "clsUntimely";
            //    obj.cssCls2 = "clsUntimely2";
            //    obj.SN = ++counter;
            //    resultSet.Add(obj);
            //}

            List<AttendanceAllEmployeeReportNewResult> listTimelyInUntimelyOut = listRecords.Where(x => (x.AtteState != 4 && x.AtteState != 9) && (x.InState != 3) && (x.OutState == 3)).OrderBy(x => x.EmplooyeeName).ToList();
            foreach (var item in listTimelyInUntimelyOut)
            {
                EmpBranchAttendanceDetails obj = new EmpBranchAttendanceDetails();
                obj.EmployeeId = item.EmployeeId.Value;
                obj.Name = item.EmplooyeeName;
                obj.UrlPhoto = GetEmployeePhotoUrl(item.EmployeeId.Value);
                obj.InTimeValue = (item.InTime == null ? "" : DateTime.Parse(item.InTime.Value.ToString()).ToString("hh:mm tt"));
                obj.OutTimeValue = (item.OutTime == null ? "" : DateTime.Parse(item.OutTime.Value.ToString()).ToString("hh:mm tt"));
                obj.cssCls = "clsUntimely";
                obj.cssCls2 = "clsTimely2";
                obj.cssCls3 = "clsUntimely2";
                obj.SN = ++counter;
                resultSet.Add(obj);
            }

            List<AttendanceAllEmployeeReportNewResult> listUntimelyInTimelyOut = listRecords.Where(x => (x.AtteState != 4 && x.AtteState != 9) && (x.InState == 3) && (x.OutState != 3)).OrderBy(x => x.EmplooyeeName).ToList();
            foreach (var item in listUntimelyInTimelyOut)
            {
                EmpBranchAttendanceDetails obj = new EmpBranchAttendanceDetails();
                obj.EmployeeId = item.EmployeeId.Value;
                obj.Name = item.EmplooyeeName;
                obj.UrlPhoto = GetEmployeePhotoUrl(item.EmployeeId.Value);
                obj.InTimeValue = (item.InTime == null ? "" : DateTime.Parse(item.InTime.Value.ToString()).ToString("hh:mm tt"));
                obj.OutTimeValue = (item.OutTime == null ? "" : DateTime.Parse(item.OutTime.Value.ToString()).ToString("hh:mm tt"));
                obj.cssCls = "clsUntimely";
                obj.cssCls2 = "clsUntimely2";
                obj.cssCls3 = "clsTimely2";
                obj.SN = ++counter;
                resultSet.Add(obj);
            }

            List<AttendanceAllEmployeeReportNewResult> listUntimelyInUntimelyOut = listRecords.Where(x => (x.AtteState != 4 && x.AtteState != 9) && (x.InState == 3) && (x.OutState == 3)).OrderBy(x => x.EmplooyeeName).ToList();
            foreach (var item in listUntimelyInUntimelyOut)
            {
                EmpBranchAttendanceDetails obj = new EmpBranchAttendanceDetails();
                obj.EmployeeId = item.EmployeeId.Value;
                obj.Name = item.EmplooyeeName;
                obj.UrlPhoto = GetEmployeePhotoUrl(item.EmployeeId.Value);
                obj.InTimeValue = (item.InTime == null ? "" : DateTime.Parse(item.InTime.Value.ToString()).ToString("hh:mm tt"));
                obj.OutTimeValue = (item.OutTime == null ? "" : DateTime.Parse(item.OutTime.Value.ToString()).ToString("hh:mm tt"));
                obj.cssCls = "clsUntimely";
                obj.cssCls2 = "clsUntimely2";
                obj.cssCls3 = "clsUntimely2";
                obj.SN = ++counter;
                resultSet.Add(obj);
            }

            List<AttendanceAllEmployeeReportNewResult> listLeave = listRecords.Where(x => x.AtteState == 4).OrderBy(x => x.EmplooyeeName).ToList();
            foreach (var item in listLeave)
            {
                EmpBranchAttendanceDetails obj = new EmpBranchAttendanceDetails();
                obj.EmployeeId = item.EmployeeId.Value;
                obj.Name = item.EmplooyeeName;
                obj.UrlPhoto = GetEmployeePhotoUrl(item.EmployeeId.Value);
                obj.InTimeValue = (item.InTime == null ? "" : DateTime.Parse(item.InTime.Value.ToString()).ToString("hh:mm tt"));
                obj.OutTimeValue = (item.OutTime == null ? "" : DateTime.Parse(item.OutTime.Value.ToString()).ToString("hh:mm tt"));
                obj.cssCls = "clsLeave";
                obj.cssCls2 = "clsLeave2";
                obj.cssCls3 = "clsLeave2";
                obj.SN = ++counter;
                resultSet.Add(obj);
            }

            List<AttendanceAllEmployeeReportNewResult> listAbsent = listRecords.Where(x => x.AtteState == 9).OrderBy(x => x.EmplooyeeName).ToList();
            foreach (var item in listAbsent)
            {
                EmpBranchAttendanceDetails obj = new EmpBranchAttendanceDetails();
                obj.EmployeeId = item.EmployeeId.Value;
                obj.Name = item.EmplooyeeName;
                obj.UrlPhoto = GetEmployeePhotoUrl(item.EmployeeId.Value);
                obj.InTimeValue = (item.InTime == null ? "" : DateTime.Parse(item.InTime.Value.ToString()).ToString("hh:mm tt"));
                obj.OutTimeValue = (item.OutTime == null ? "" : DateTime.Parse(item.OutTime.Value.ToString()).ToString("hh:mm tt"));
                obj.cssCls = "clsAbsent";
                obj.cssCls2 = "clsAbsent2";
                obj.cssCls3 = "clsAbsent2";
                obj.SN = ++counter;
                resultSet.Add(obj);
            }

            if (resultSet.Count > 0)
                list = new Paging<EmpBranchAttendanceDetails>(resultSet, Convert.ToInt32(resultSet.Count()));
            else
                list = new Paging<EmpBranchAttendanceDetails>(resultSet, 0);

            context.Response.Write(JSON.Serialize(list));

        }

        public string GetEmployeePhotoUrl(int employeeId)
        {
            string photoUrl = "";

            HHumanResource hr = BLL.BaseBiz.PayrollDataContext.HHumanResources.SingleOrDefault(x => x.EmployeeId == employeeId);

            if (hr != null)
                photoUrl = hr.UrlPhotoThumbnail;

            if (string.IsNullOrEmpty(photoUrl))
            {
                string title = BLL.BaseBiz.PayrollDataContext.EEmployees.SingleOrDefault(x => x.EmployeeId == employeeId).Title;

                if (!string.IsNullOrEmpty(title))
                {
                    if (title.ToLower() == "mr")
                        photoUrl = "../images/male.png";
                    else
                        photoUrl = "../images/female.png";
                }
                else
                    photoUrl = "../images/female.png";
            }

            return photoUrl;
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}