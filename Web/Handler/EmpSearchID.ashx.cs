﻿using System;
using System.Web;
using System.Web.Services;
using BLL;
using BLL.Manager;
using System.Collections.Generic;
using DAL;
using Ext.Net;
namespace Web.Handler
{
    /// <summary>
    /// Summary description for EmpSearch
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    public class EmpSearchID : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {

            if (HttpContext.Current.User.Identity.IsAuthenticated == false)
            {
                context.Response.Write("");
                context.Response.End();
                return;
            }

            context.Response.ContentType = "text/plain";
            string query = context.Request["query"];

            string retAlso = context.Request["RetiredAlso"];
            bool includeRetiredAlso = (retAlso != null && retAlso.ToLower().Equals("true")) ? true : false;

            if (query == null)
                return;

            query = query.Trim();

            bool isEIN = false;
            int eid = 0;
            if (int.TryParse(query, out eid))
            {
                isEIN = true;
            }

            EmployeeManager mgr = new EmployeeManager();

            List<string> suggestions = new List<string>();

            string customRoleDepartmentList = null;
            if (SessionManager.IsCustomRole)
                customRoleDepartmentList = SessionManager.CustomRoleDeparmentIDList;

            List<GetPrefixedEmpNamesWithIDResult> list = mgr.GetPrefixedEmplyeeNamesWithID(isEIN, eid, query.ToLower(), customRoleDepartmentList, includeRetiredAlso);

            //foreach (GetPrefixedEmpNamesResult dr in list)
            //{
            //    suggestions.Add(AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem(dr.Name, dr.EmployeeId.ToString()));

            //}
            //return suggestions.ToArray();

            context.Response.Write(string.Format("{{total:{1},'plants':{0}}}", JSON.Serialize(list), list.Count));

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}