﻿using System;
using System.Web;
using System.Web.Services;
using BLL;
using BLL.Manager;
using System.Collections.Generic;
using DAL;
using Ext.Net;

namespace Web.Handler
{
    /// <summary>
    /// Summary description for PartySearch
    /// </summary>
    public class PartySearch : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            string query = context.Request["query"];

            if (query == null)
                return;

            query = query.Trim();

            List<string> suggestions = new List<string>();

            List<DocParty> list = DocumentManager.GetDocPartySearch(query);

            context.Response.Write(string.Format("{{total:{1},'plants':{0}}}", JSON.Serialize(list), list.Count));

            
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}