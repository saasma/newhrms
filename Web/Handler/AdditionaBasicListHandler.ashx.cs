﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;

namespace Web.Handler
{
    /// <summary>
    /// Summary description for PayrollMessageHandler
    /// </summary>
    public class AdditionaBasicListHandler : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {

            if (HttpContext.Current.User.Identity.IsAuthenticated == false)
            {
                context.Response.Write("");
                context.Response.End();
                return;
            }

            context.Response.ContentType = "application/json";

            var start = 0;
            var limit = 50;
            var UserName = string.Empty;
            if (!string.IsNullOrEmpty(context.Request["start"]))
            {
                start = int.Parse(context.Request["start"]);
            }

            //if (!string.IsNullOrEmpty(context.Request["limit"]))
            //{
            //    limit = int.Parse(context.Request["limit"]);
            //}

            int employeeId = -1;
            int levelId = -1;
            int branchId = -1;
            int incomeId = -1;
            bool showHistory = false;


            if (!string.IsNullOrEmpty(context.Request["ShowHistory"]))
            {
                showHistory = bool.Parse(context.Request["ShowHistory"]);
            }
            if (!string.IsNullOrEmpty(context.Request["EmployeeId"]))
            {
                employeeId = int.Parse(context.Request["EmployeeId"]);
            }
            if (!string.IsNullOrEmpty(context.Request["BranchId"]))
            {
                branchId = int.Parse(context.Request["BranchId"]);
            }

            if (!string.IsNullOrEmpty(context.Request["LevelId"]))
            {
                levelId = int.Parse(context.Request["LevelId"]);
            }
            if (!string.IsNullOrEmpty(context.Request["incomeId"]))
            {
                incomeId = int.Parse(context.Request["incomeId"]);
            }


            Paging<GetAdditionaBasicListResult> payrollMessages = NewPayrollManager
                .GetAdditionBasicList(start, limit, employeeId, levelId, branchId,incomeId, showHistory);

            context.Response.Write(JSON.Serialize(payrollMessages));

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}