﻿using System;
using System.Web;
using System.Web.Services;
using BLL;
using BLL.Manager;
using System.Collections.Generic;
using DAL;
using Ext.Net;
namespace Web.Handler
{
    /// <summary>
    /// Summary description for EmpSearch
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    public class EmpSearch : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {


            if (HttpContext.Current.User.Identity.IsAuthenticated == false)
            {
                context.Response.Write("");
                context.Response.End();
                return;
            }

            context.Response.ContentType = "text/plain";
            string query = context.Request["query"];

            if (query == null)
                return;

            query = query.Trim();

            //if (HttpContext.Current.User.Identity.IsAuthenticated == false || SessionManager.CurrentLoggedInEmployeeId != 0)
            //    return new string[] { };

            EmployeeManager mgr = new EmployeeManager();

            List<string> suggestions = new List<string>();

            string customRoleDepartmentList = null;
            if (SessionManager.IsCustomRole)
                customRoleDepartmentList = SessionManager.CustomRoleDeparmentIDList;

            List<GetPrefixedEmpNamesResult> list = mgr.GetPrefixedEmplyeeNames(query.ToLower(), customRoleDepartmentList,false);

            //foreach (GetPrefixedEmpNamesResult dr in list)
            //{
            //    suggestions.Add(AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem(dr.Name, dr.EmployeeId.ToString()));

            //}
            //return suggestions.ToArray();

            context.Response.Write(string.Format("{{total:{1},'plants':{0}}}", JSON.Serialize(list), list.Count));

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}