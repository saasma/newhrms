﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;

namespace Web.Handler
{
    /// <summary>
    /// Summary description for PayrollMessageHandler
    /// </summary>
    public class PromotionListReportHandler : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {

            if (HttpContext.Current.Request.IsAuthenticated == false ||
                 SessionManager.CurrentLoggedInEmployeeId != 0)
            {
                return;
            }

            context.Response.ContentType = "application/json";
            int start = 0;
            int pagesize = 50;
            int EmoloyeeID = -1;
            bool? type = null;

            int? empId = -1;
            int? levelGradeId = -1;

            DateTime? fromDate = null;
            DateTime? toDate = null;

            if (!string.IsNullOrEmpty(context.Request["LevelGradeChangeId"]))
                levelGradeId = Convert.ToInt32(context.Request["LevelGradeChangeId"].ToString());

            if (!string.IsNullOrEmpty(context.Request["start"]))
                start = Convert.ToInt32(context.Request["start"].ToString());

            if (!string.IsNullOrEmpty(context.Request["type"]) && context.Request["type"]!= "All")
                type = Convert.ToBoolean(context.Request["type"].ToString());

            //PageSize
            if (!string.IsNullOrEmpty(context.Request["PageSize"]))
                pagesize = Convert.ToInt32(context.Request["PageSize"]);

            try
            {
                if (!string.IsNullOrEmpty(context.Request["empId"]))
                    empId = Convert.ToInt32(context.Request["empId"].ToString());
            }
            catch (Exception e)
            {
                empId = null;
            }

            try
            {
                if (!string.IsNullOrEmpty(context.Request["fromDate"]))
                    fromDate = Convert.ToDateTime( BLL.BaseBiz.GetEngDate( context.Request["fromDate"]
                        ,BLL.BaseBiz.IsEnglish));
            }
            catch (Exception e)
            {
                fromDate = null;
                
            }

            try
            {
                if (!string.IsNullOrEmpty(context.Request["toDate"]))
                    toDate = Convert.ToDateTime(BLL.BaseBiz.GetEngDate( context.Request["toDate"].ToString()
                        , BLL.BaseBiz.IsEnglish));
            }
            catch (Exception e)
            {
                toDate = null;
                
            }





            Paging<GetLevelGradeChangeDetailsSPResult> list = null;
            List<GetLevelGradeChangeDetailsSPResult> resultSet = NewHRManager.GetLevelGradeReport
                (start, pagesize, fromDate, toDate, empId,type,levelGradeId);


            if (resultSet.Count > 0)
                list = new Paging<GetLevelGradeChangeDetailsSPResult>(resultSet, Convert.ToInt32(resultSet[0].TotalRows));
            else
                list = new Paging<GetLevelGradeChangeDetailsSPResult>(resultSet, 0);

            context.Response.Write(JSON.Serialize(list));

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}