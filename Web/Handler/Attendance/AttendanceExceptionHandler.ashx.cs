﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ext.Net;
using DAL;
using BLL.Manager;

namespace Web.Handler.Attendance
{
    /// <summary>
    /// Summary description for AttendanceExceptionHandler
    /// </summary>
    public class AttendanceExceptionHandler : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {

            context.Response.ContentType = "application/json";
            
            var start = 0;
            var limit = 10;

            int EmployeeId = 0;
            int AllEmployee = 0;
            int BranchID = 0;
            //bool skipHoliday = true;

            //includeHoliday = bool.Parse(context.Request["IncludeHoliday"]);

            AllEmployee = int.Parse(context.Request["AllFilter"]);
            BranchID = int.Parse(context.Request["BranchFilter"]);
            DateTime startDate = new DateTime();
            DateTime endDate = new DateTime();

            if (!string.IsNullOrEmpty(context.Request["EmployeeId"]))
                EmployeeId = int.Parse(context.Request["EmployeeId"]);

            bool excudeHoliday = true;
            if (!string.IsNullOrEmpty(context.Request["HolidayFilter"]))
                excudeHoliday = bool.Parse(context.Request["HolidayFilter"]);
            

            if (!string.IsNullOrEmpty(context.Request["StartDate"]))
            {
                startDate = BLL.BaseBiz.GetEngDate(context.Request["StartDate"],BLL.BaseBiz.IsEnglish);
            }

            if (!string.IsNullOrEmpty(context.Request["EndDate"]))
            {
                endDate =  BLL.BaseBiz.GetEngDate(context.Request["EndDate"],BLL.BaseBiz.IsEnglish);
            }
            
 
            if (!string.IsNullOrEmpty(context.Request["start"]))
            {
                start = int.Parse(context.Request["start"]);
            }
            int page = 0;
            if (!string.IsNullOrEmpty(context.Request["page"]))
            {
                page = int.Parse(context.Request["page"]);
            }

            //page = page * start;

            Paging<AttendanceExceptionResult> list = null;
            List<AttendanceExceptionResult> resultset = AttendanceManager.GetAttendanceExceptionList(EmployeeId, AllEmployee, startDate, endDate, BranchID, page, 100, excudeHoliday);
            if (resultset.Count > 0)
            {
                list = new Paging<AttendanceExceptionResult>(resultset, resultset[0].TotalRows.Value);
            }
            else
                list = new Paging<AttendanceExceptionResult>(new List<AttendanceExceptionResult>(), 0);

            context.Response.Write(JSON.Serialize(list));
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}