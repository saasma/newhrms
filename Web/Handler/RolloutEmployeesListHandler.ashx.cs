﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;

namespace Web.Handler
{
    /// <summary>
    /// Summary description for PayrollMessageHandler
    /// </summary>
    public class RolloutEmployeesListHandler : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {

            if (HttpContext.Current.User.Identity.IsAuthenticated == false)
            {
                context.Response.Write("");
                context.Response.End();
                return;
            }

            context.Response.ContentType = "application/json";
            int start = 0;
            int pagesize = 50;
            int EmoloyeeID = -1;
            int statusId = -1;
            int levelId = -1;
            int branchId = -1;
            int formId = -1;
            int appraisalGroup = -1;
            string periodIds = "";

            if (!string.IsNullOrEmpty(context.Request["start"]))
                start = Convert.ToInt32(context.Request["start"].ToString());

            //PageSize
            if (!string.IsNullOrEmpty(context.Request["PageSize"]))
                pagesize = Convert.ToInt32(context.Request["PageSize"]);

            string statusFilter = "";
            if (!string.IsNullOrEmpty(context.Request["statusFilter"]))
                statusFilter = (context.Request["statusFilter"]);

            if (!string.IsNullOrEmpty(context.Request["EmployeeId"]))
                EmoloyeeID = Convert.ToInt32(context.Request["EmployeeId"]);
            if (!string.IsNullOrEmpty(context.Request["LevelId"]))
                levelId = Convert.ToInt32(context.Request["LevelId"]);
            if (!string.IsNullOrEmpty(context.Request["StatusId"]))
                statusId = Convert.ToInt32(context.Request["StatusId"]);
            if (!string.IsNullOrEmpty(context.Request["BranchId"]))
                branchId = Convert.ToInt32(context.Request["BranchId"]);
            if (!string.IsNullOrEmpty(context.Request["FormId"]))
                formId = Convert.ToInt32(context.Request["FormId"]);

            if (!string.IsNullOrEmpty(context.Request["AppraisalGroupID"]))
                appraisalGroup = Convert.ToInt32(context.Request["AppraisalGroupID"]);

            if (!string.IsNullOrEmpty(context.Request["PeriodId"]))
                periodIds = context.Request["PeriodId"];

            if (periodIds.Contains("-1"))
                periodIds = "";
            

            Paging<GetRolloutEmployeesListResult> list = null;
            List<GetRolloutEmployeesListResult> resultSet = AppraisalManager.GetEmployeeRolloutList(start, pagesize, EmoloyeeID
                , levelId, statusId, branchId, formId, statusFilter, periodIds, appraisalGroup);


            if (resultSet.Count > 0)
                list = new Paging<GetRolloutEmployeesListResult>(resultSet, Convert.ToInt32(resultSet[0].TotalRows));
            else
                list = new Paging<GetRolloutEmployeesListResult>(resultSet, 0);

            context.Response.Write(JSON.Serialize(list));

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}