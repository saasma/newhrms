﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;

namespace Web.Handler
{
    /// <summary>
    /// Summary description for PayrollMessageHandler
    /// </summary>
    public class AAEmployeeCatalogueHandler : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {

            
            //if (HttpContext.Current.User.Identity.IsAuthenticated == false || SessionManager.CurrentLoggedInEmployeeId != 0)

            if (HttpContext.Current.User.Identity.IsAuthenticated == false || SessionManager.CurrentLoggedInEmployeeId != 0)
            {
                context.Response.Write("");
                context.Response.End();
                return;
            }

            context.Response.ContentType = "application/json";
            int start = 0;
            int pagesize = 100;
            string sort = "";
            bool GridOrThumbnail = true;
            string Search = "";
            int branchId = -1;
            int departmentId = -1;
            int designationId = -1;
            int levelId = -1;
            int StatusID = -1;
            string INO = "";
            bool statusOnly = false;


            if (!string.IsNullOrEmpty(context.Request["start"]))
                start = Convert.ToInt32(context.Request["start"].ToString());
            //PageSize
            if (!string.IsNullOrEmpty(context.Request["PageSize"]))
                pagesize = Convert.ToInt32(context.Request["PageSize"]);

            if (!string.IsNullOrEmpty(context.Request["Branch"]))
                branchId = Convert.ToInt32(context.Request["Branch"]);

            if (!string.IsNullOrEmpty(context.Request["Designation"]))
                designationId = Convert.ToInt32(context.Request["Designation"]);

            if (!string.IsNullOrEmpty(context.Request["Department"]))
                departmentId = Convert.ToInt32(context.Request["Department"]);

            if (!string.IsNullOrEmpty(context.Request["GridOrThumbnail"]))
                GridOrThumbnail = bool.Parse(context.Request["GridOrThumbnail"]);

            if (!string.IsNullOrEmpty(context.Request["Search"]))
            {
                Search = context.Request["Search"];
            }

            if (!string.IsNullOrEmpty(context.Request["INo"]))
                INO = context.Request["INo"];

            if (!string.IsNullOrEmpty(context.Request["Level"]))
                levelId = Convert.ToInt32(context.Request["Level"]);

            if (!string.IsNullOrEmpty(context.Request["Status"]))
                StatusID = Convert.ToInt32(context.Request["Status"]);

            if (!string.IsNullOrEmpty(context.Request["MinStatusOnly"]))
                statusOnly = Convert.ToBoolean(context.Request["MinStatusOnly"]);

            if (!string.IsNullOrEmpty(context.Request["sort"]))
            {
                sort = context.Request["sort"];

                if (sort == "1")
                {
                    sort = "Name ASC";
                }
                else if (sort == "2")
                {
                    sort = "Name DESC";
                }
                else if (sort == "3")
                {
                    sort = "JoinDate ASC";
                }
                else if (sort == "4")
                {
                    sort = "JoinDate DESC";
                }
            }

            Paging<GetEmployeesCatelogueResult> list = new Paging<GetEmployeesCatelogueResult>();


            if (UserManager.IsPageAccessible("cp/AAEmployeeCatalogue.aspx") == true)
            {
                List<GetEmployeesCatelogueResult> resultSet = EmployeeManager.GetEmployeeCatalogue(start, pagesize, sort, GridOrThumbnail, Search,
                    branchId, departmentId, designationId, INO, levelId, StatusID, statusOnly);

                if (resultSet.Count > 0)
                    list = new Paging<GetEmployeesCatelogueResult>(resultSet, Convert.ToInt32(resultSet[0].TotalRows));
                else
                    list = new Paging<GetEmployeesCatelogueResult>(resultSet, 0);
            }

            context.Response.Write(JSON.Serialize(list));

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}