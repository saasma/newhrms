﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;

namespace Web.Handler
{
    /// <summary>
    /// Summary description for PayrollMessageHandler
    /// </summary>
    public class AllowanceRequestForEmpHandler : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {

            if (HttpContext.Current.User.Identity.IsAuthenticated == false)
            {
                context.Response.Write("");
                context.Response.End();
                return;
            }

            context.Response.ContentType = "application/json";
            int start = 0;
            int pagesize = 50;
            int EmoloyeeID = -1, period = -1, status = -1, eveninigCounterTypeId = -1;
            

            if (!string.IsNullOrEmpty(context.Request["start"]))
                start = Convert.ToInt32(context.Request["start"].ToString());

            //PageSize
            if (!string.IsNullOrEmpty(context.Request["PageSize"]))
                pagesize = Convert.ToInt32(context.Request["PageSize"]);

            if (!string.IsNullOrEmpty(context.Request["Period"]))
                period = Convert.ToInt32(context.Request["Period"]);

            if (!string.IsNullOrEmpty(context.Request["Status"]))
                status = Convert.ToInt32(context.Request["Status"]);

            if (!string.IsNullOrEmpty(context.Request["CounterTypeId"]))
                eveninigCounterTypeId = Convert.ToInt32(context.Request["CounterTypeId"]);

            EmoloyeeID = SessionManager.CurrentLoggedInEmployeeId;


            Paging<GetEveningCounterRequestForEmpSPResult> list = null;
            List<GetEveningCounterRequestForEmpSPResult> resultSet = AllowanceManager.GetEveningCounterRequestForEmpReport(start, pagesize, EmoloyeeID, period, status, eveninigCounterTypeId);


            if (resultSet.Count > 0)
                list = new Paging<GetEveningCounterRequestForEmpSPResult>(resultSet, Convert.ToInt32(resultSet[0].TotalRows));
            else
                list = new Paging<GetEveningCounterRequestForEmpSPResult>(resultSet, 0);

            context.Response.Write(JSON.Serialize(list));

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}