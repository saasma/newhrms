﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ext.Net;
using BLL.Manager;
using DAL;
using System.Web.SessionState;

namespace Web.Handler
{
    /// <summary>
    /// Summary description for HREvaluationInfoHandler
    /// </summary>
    /// 

    public class HREvaluationInfoHandler : IHttpHandler 
        //IRequiresSessionState
    {

        public void ProcessRequest(HttpContext context)
        {

            if (HttpContext.Current.User.Identity.IsAuthenticated == false)
            {
                context.Response.Write("");
                context.Response.End();
                return;
            }

            context.Response.ContentType = "application/json";
            //string companyid = 
            var start = 0;
            var limit = 10;
            int companyId = 0;
            int evaluationId = 0;
            int pageSize = 10; int branchId = -1; int departmentId = -1;
            int subDepartmentId = -1; int designationId = -1; string empSearch = "";
            //
            if (!string.IsNullOrEmpty(context.Request["evaluationId"]))
            {
                evaluationId = int.Parse(context.Request["evaluationId"]);
            }

          if (!string.IsNullOrEmpty(context.Request["branchId"]))
            {
                branchId = int.Parse(context.Request["branchId"]);
            }
             if (!string.IsNullOrEmpty(context.Request["departmentId"]))
            {
                departmentId = int.Parse(context.Request["departmentId"]);
            }
             if (!string.IsNullOrEmpty(context.Request["subDepartmentId"]))
             {
                 subDepartmentId = int.Parse(context.Request["subDepartmentId"]);
             }
             if (!string.IsNullOrEmpty(context.Request["designationId"]))
            {
                designationId = int.Parse(context.Request["designationId"]);
            }
              if (!string.IsNullOrEmpty(context.Request["empSearch"]))
            {
                empSearch = context.Request["empSearch"].ToString();
            }

            if (!string.IsNullOrEmpty(context.Request["companyId"]))
            {
                companyId = int.Parse(context.Request["companyId"]);
            }
            if (!string.IsNullOrEmpty(context.Request["start"]))
            {
                start = int.Parse(context.Request["start"]);
            }

            if (!string.IsNullOrEmpty(context.Request["limit"]))
            {
                limit = int.Parse(context.Request["limit"]);
            }
            //Paging<HREvaluationInfoHandler> resultinfo = PositionGradeStepAmountManager.GetEvaluationPeriod(start, limit);
            Paging<GetEvaluationInfoResult> list = null;
            List<GetEvaluationInfoResult> resultset = PositionGradeStepAmountManager.GetEvaluationPeriod(start, limit,
                 companyId, evaluationId,branchId,departmentId,subDepartmentId, designationId,empSearch,-1);
            if (resultset.Count > 0)
            {
                list = new Paging<GetEvaluationInfoResult>(resultset, resultset[0].TotalRows.Value);
            }
            else
                list = new Paging<GetEvaluationInfoResult>(new List<GetEvaluationInfoResult>(), 0);

            context.Response.Write(JSON.Serialize(list));
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}