﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using Utils.Calendar;

namespace Web.Handler
{
    /// <summary>
    /// Summary description for BranchAttendanceHandler
    /// </summary>
    public class BranchAttendanceHandler : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated == false || SessionManager.CurrentLoggedInEmployeeId != 0)
            {
                context.Response.Write("");
                context.Response.End();
                return;
            }

            context.Response.ContentType = "application/json";
            int start = 0;
            int pagesize = 100;
            string sort = "";
            bool GridOrThumbnail = true;
            string branchId = "";
            DateTime selectedDate = DateTime.Today;


            if (!string.IsNullOrEmpty(context.Request["start"]))
                start = Convert.ToInt32(context.Request["start"].ToString());
            
            if (!string.IsNullOrEmpty(context.Request["PageSize"]))
                pagesize = Convert.ToInt32(context.Request["PageSize"]);

            if (!string.IsNullOrEmpty(context.Request["Branch"]))
                branchId = context.Request["Branch"];


            string date = "";

            if (!string.IsNullOrEmpty(context.Request["SelectedDate"]))
            {
                date = context.Request["SelectedDate"];

                if (BLL.BaseBiz.IsEnglish)
                    selectedDate = Convert.ToDateTime(date);
                else
                {
                    CustomDate cd = CustomDate.GetCustomDateFromString(date, false);
                    selectedDate = cd.EnglishDate;
                }
            }

            List<EmpBranchAttendance> resultSet = new List<EmpBranchAttendance>();

            Paging<EmpBranchAttendance> list = new Paging<EmpBranchAttendance>();

            List<AttendanceAllEmployeeReportNewResult> listRecords = AttendanceManager.GetAttendanceDailyReportNew(selectedDate, 0, branchId);

            int[] branchIds = listRecords.Select(x => x.BranchId.Value).Distinct().ToArray();

            int absentCount = 0;
            foreach (int brId in branchIds)
            {
                List<AttendanceAllEmployeeReportNewResult> listBr = listRecords.Where(x => x.BranchId == brId).ToList();

                EmpBranchAttendance objEmpBranchAttendance = new EmpBranchAttendance();
                objEmpBranchAttendance.TotalEmployeesCount = listBr.Count();

                absentCount = 0;

                if (listBr.Count > 0)
                {
                    objEmpBranchAttendance.BranchName = listBr[0].Branch;
                    objEmpBranchAttendance.BranchId = listBr[0].BranchId.Value;

                    absentCount = listBr.Where(x => x.AtteState == 4 || x.AtteState == 9).Count();
                }

                objEmpBranchAttendance.PresentEmployeesCount = objEmpBranchAttendance.TotalEmployeesCount - absentCount;
                objEmpBranchAttendance.Date = date;
                resultSet.Add(objEmpBranchAttendance);

            }

            if (resultSet.Count > 0)
                list = new Paging<EmpBranchAttendance>(resultSet, Convert.ToInt32(resultSet.Count()));
            else
                list = new Paging<EmpBranchAttendance>(resultSet, 0);

            context.Response.Write(JSON.Serialize(list));

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}