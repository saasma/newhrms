﻿using System;
using System.Web;
using System.Web.Services;
using BLL;
using BLL.Manager;
using System.Collections.Generic;
using DAL;
using Ext.Net;
using BLL.BO;

namespace Web.Handler
{
    /// <summary>
    /// Summary description for EmployeeSearch
    /// </summary>
    public class EmployeeSearch : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {

            if (HttpContext.Current.User.Identity.IsAuthenticated == false)
            {
                context.Response.Write("");
                context.Response.End();
                return;
            }

            context.Response.ContentType = "text/plain";
            string query = context.Request["query"];

            if (query == null)
                return;

            query = query.Trim();

            bool isEIN = false;
            int eid = 0;
            if (int.TryParse(query, out eid))
            {
                isEIN = true;
            }

            EmployeeManager mgr = new EmployeeManager();

            List<string> suggestions = new List<string>();

            string customRoleDepartmentList = null;
            if (SessionManager.IsCustomRole)
                customRoleDepartmentList = SessionManager.CustomRoleDeparmentIDList;

            List<GetPrefixedEmpNamesWithIDResult> list = mgr.GetPrefixedEmplyeeNamesWithID(isEIN, eid, query.ToLower(), customRoleDepartmentList, false);
            List<GetPrefixedEmpNamesWithIDResult> newList = new List<GetPrefixedEmpNamesWithIDResult>();

            if (SessionManager.CurrentLoggedInEmployeeId != 0)
            {
                foreach (var item in list)
                {
                    List<TextValue> listTV = LeaveAttendanceManager.GetEmployeeListForLeaveAssign();
                    foreach (var itemTV in listTV)
                    {
                        if (itemTV.ID == item.EmployeeId)
                            newList.Add(item);
                    }
                }
                context.Response.Write(string.Format("{{total:{1},'plants':{0}}}", JSON.Serialize(newList), newList.Count));
            }
            else
                context.Response.Write(string.Format("{{total:{1},'plants':{0}}}", JSON.Serialize(list), list.Count));


        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}