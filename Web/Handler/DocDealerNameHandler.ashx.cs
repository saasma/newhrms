﻿using System;
using System.Web;
using System.Web.Services;
using BLL;
using BLL.Manager;
using System.Collections.Generic;
using DAL;
using Ext.Net;
using BLL.BO;

namespace Web.Handler
{
    /// <summary>
    /// Summary description for DocDealerNameHandler
    /// </summary>
    public class DocDealerNameHandler : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            string query = context.Request["query"];

            int? Type = null;
            if (!string.IsNullOrEmpty(context.Request["Type"]))
                Type = Convert.ToInt32(context.Request["Type"]);


            if (query == null)
                return;

            query = query.Trim();

            List<TextValue> list = new List<TextValue>();
            list = DocumentManager.GetDealerNameSearch(query, Type.Value);
            context.Response.Write(string.Format("{{total:{1},'plants':{0}}}", JSON.Serialize(list), list.Count));
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}