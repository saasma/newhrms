﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;

namespace Web.Handler
{
    /// <summary>
    /// Summary description for PayrollMessageHandler
    /// </summary>
    public class EmpAppraisalReviewHandler : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {

            if (HttpContext.Current.User.Identity.IsAuthenticated == false)
            {
                context.Response.Write("");
                context.Response.End();
                return;
            }

            context.Response.ContentType = "application/json";
            int start = 0;
            int pagesize = 50;
            int EmoloyeeID = -1;
            string periodIds = "";
            

            if (!string.IsNullOrEmpty(context.Request["start"]))
                start = Convert.ToInt32(context.Request["start"].ToString());

            //PageSize
            if (!string.IsNullOrEmpty(context.Request["PageSize"]))
                pagesize = Convert.ToInt32(context.Request["PageSize"]);

            EmoloyeeID = SessionManager.CurrentLoggedInEmployeeId;

            if (!string.IsNullOrEmpty(context.Request["PeriodId"]))
                periodIds = context.Request["PeriodId"];

            if (periodIds.Contains("-1"))
                periodIds = "";

            Paging<GetAppraisalListForReviewResult> list = null;
            List<GetAppraisalListForReviewResult> resultSet = AppraisalManager.GetAppraisalReviewList(start, pagesize, EmoloyeeID, periodIds);


            if (resultSet.Count > 0)
                list = new Paging<GetAppraisalListForReviewResult>(resultSet, Convert.ToInt32(resultSet[0].TotalRows));
            else
                list = new Paging<GetAppraisalListForReviewResult>(resultSet, 0);

            context.Response.Write(JSON.Serialize(list));

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}