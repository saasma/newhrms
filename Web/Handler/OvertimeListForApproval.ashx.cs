﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;

namespace Web.Handler
{
    /// <summary>
    /// Summary description for OvertimeListForApproval
    /// </summary>
    /// 
    public class OvertimeListForApproval : IHttpHandler
    {
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "application/json";
            int start = 0;
            int pagesize = 50;
            int EmployeeId = -1;
            int OverTimePeriodID = -1;
            int status = -1;
            DateTime? StartDate = null;
            DateTime? EndDate = null;

            if (!string.IsNullOrEmpty(context.Request["status"]))
                status = Convert.ToInt32(context.Request["status"].ToString());

            if (!string.IsNullOrEmpty(context.Request["start"]))
                start = Convert.ToInt32(context.Request["start"].ToString());

            DateTime? date = null;

            if (!string.IsNullOrEmpty(context.Request["Date"]))
                date = Convert.ToDateTime(context.Request["Date"].ToString());

            //PageSize
            if (!string.IsNullOrEmpty(context.Request["PageSize"]))
                pagesize = Convert.ToInt32(context.Request["PageSize"]);

            if (!string.IsNullOrEmpty(context.Request["EmployeeId"]))
                EmployeeId = Convert.ToInt32(context.Request["EmployeeId"]);

            if (!string.IsNullOrEmpty(context.Request["OverTimePeriodID"]))
                OverTimePeriodID = Convert.ToInt32(context.Request["OverTimePeriodID"]);


            OvertimePeriod _OverTimPeriod= OvertimeManager.GetOverTimPeriodById(OverTimePeriodID);
            if (_OverTimPeriod != null)
            {
                StartDate = _OverTimPeriod.StartDate;
                EndDate = _OverTimPeriod.EndDate;
            }

          
            Paging<GetOvertimeForApprovalResult> list = null;
            List<GetOvertimeForApprovalResult> resultSet = OvertimeManager.GetOvertimelistForApproval(start, pagesize, EmployeeId, status, 
                StartDate, EndDate,date);

            if (resultSet.Count > 0)
                list = new Paging<GetOvertimeForApprovalResult>(resultSet, Convert.ToInt32(resultSet[0].TotalRows));
            else
                list = new Paging<GetOvertimeForApprovalResult>(resultSet, 0);
            context.Response.Write(JSON.Serialize(list));
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}