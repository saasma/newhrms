﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;


namespace Web.Handler
{
    /// <summary>
    /// Summary description for EmployeeProjectContributionHandler
    /// </summary>
    public class EmployeeProjectContributionHandler : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "application/json";
            int start = 0;
            int pagesize = 50;
            int EmployeeId = -1;
            string startDate = null, endDate = null;
            int IsEmpView = -1;
            int status = -2;

            if (HttpContext.Current.Request.IsAuthenticated == false ||
                SessionManager.CurrentLoggedInEmployeeId != 0)
            {
                return;
            }

            if (!string.IsNullOrEmpty(context.Request["start"]))
                start = Convert.ToInt32(context.Request["start"].ToString());

            if (!string.IsNullOrEmpty(context.Request["status"]))
                status = Convert.ToInt32(context.Request["status"].ToString());

            //PageSize
            if (!string.IsNullOrEmpty(context.Request["PageSize"]))
                pagesize = Convert.ToInt32(context.Request["PageSize"]);

            if (!string.IsNullOrEmpty(context.Request["EmployeeId"]))
                EmployeeId = Convert.ToInt32(context.Request["EmployeeId"]);
            if (!string.IsNullOrEmpty(context.Request["StartDate"]))
                startDate = context.Request["StartDate"].ToString();
            //if (!string.IsNullOrEmpty(context.Request["EndDate"]))
            //    endDate = context.Request["EndDate"].ToString();

            Paging<GetEmployeProjectContributionListResult> list = null;
            
            List<GetEmployeProjectContributionListResult> resultSet = NewTimeSheetManager.GetEmployeProjectContributionList(start, pagesize, EmployeeId, startDate);

            if (resultSet.Count > 0)
                list = new Paging<GetEmployeProjectContributionListResult>(resultSet, Convert.ToInt32(resultSet[0].TotalRows));
            //list = new Paging<GetTimeSheetListResult>(resultSet, rowsCount);

            else
                list = new Paging<GetEmployeProjectContributionListResult>(resultSet, 0);


            context.Response.Write(JSON.Serialize(list));
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}