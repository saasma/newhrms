﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;

namespace Web.Handler
{
    /// <summary>
    /// Summary description for TimeSheetListForApproval
    /// </summary>
    public class MyTimeSheetList : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "application/json";
            int start = 0;
            int pagesize = 50;
            int EmployeeId = -1;
            string startDate=null, endDate=null;
            int IsEmpView = -1;
            int status = -2;

            if (!string.IsNullOrEmpty(context.Request["start"]))
                start = Convert.ToInt32(context.Request["start"].ToString());

            if (!string.IsNullOrEmpty(context.Request["status"]))
                status = Convert.ToInt32(context.Request["status"].ToString());

            //PageSize
            if (!string.IsNullOrEmpty(context.Request["PageSize"]))
                pagesize = Convert.ToInt32(context.Request["PageSize"]);

            if (!string.IsNullOrEmpty(context.Request["EmployeeId"]))
                EmployeeId = Convert.ToInt32(context.Request["EmployeeId"]);
            if (!string.IsNullOrEmpty(context.Request["StartDate"]))
                startDate = context.Request["StartDate"].ToString();
            if (!string.IsNullOrEmpty(context.Request["EndDate"]))
                endDate = context.Request["EndDate"].ToString();
            bool isMonthWise = false;
            if (!string.IsNullOrEmpty(context.Request["IsMonthWise"]))
                isMonthWise = bool.Parse(context.Request["IsMonthWise"]);

            Paging<GetTimeSheetListForEmployeeResult> list = null;
            List<GetTimeSheetListForEmployeeResult> resultSet = NewTimeSheetManager.GetTimeSheetListForEmployee
                (start, pagesize, EmployeeId, startDate, endDate, status, isMonthWise);

            if (resultSet.Count > 0)
                list = new Paging<GetTimeSheetListForEmployeeResult>(resultSet, Convert.ToInt32(resultSet[0].TotalRows));
            else
                list = new Paging<GetTimeSheetListForEmployeeResult>(resultSet, 0);


            context.Response.Write(JSON.Serialize(list));
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}