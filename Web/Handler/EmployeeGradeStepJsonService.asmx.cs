﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using Ext.Net;
using DAL;
using BLL.Manager;

namespace Web.Handler
{
    /// <summary>
    /// Summary description for EmployeeGradeStepJsonService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
     //To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
     [System.Web.Script.Services.ScriptService]
    public class EmployeeGradeStepJsonService : System.Web.Services.WebService
    {

        [WebMethod]
        public Paging<GetLeaveApprovalSettingListResult> GetApprovalList(int start, int limit, string departmentId, string branchId,string teamIdStr)
        {

            if (HttpContext.Current.User.Identity.IsAuthenticated == false)
            {
                return null;
            }


            int depId = -1;
            int bId = -1;
            int teamId = -1;

            if (departmentId != "" && departmentId != null)
                int.TryParse(departmentId, out depId);

            if (branchId != "" && branchId != null)
                int.TryParse(branchId, out bId);

            if (teamIdStr != "" && teamIdStr != null)
                int.TryParse(teamIdStr, out teamId);

            List<GetLeaveApprovalSettingListResult> list = LeaveRequestManager.GetLeaveApprovalList
              (depId, start, limit, bId, teamId);

            if (list.Count > 0)
                return new Paging<GetLeaveApprovalSettingListResult>(list, list[0].TotalRows.Value);
            else
                return new Paging<GetLeaveApprovalSettingListResult>(list, 0);
        }

        [WebMethod] //(int start, int limit, string sort, string dir, string filter)
        public Paging<GetEmployeeListGradeStepResult> EmployeeListGradeStepPaging(int start, int limit, string branchId, string departmentId, string name, string positionId, string gradeId, string stepId)
        {

            if (HttpContext.Current.User.Identity.IsAuthenticated == false)
            {
                return null;
            }

            int? _branchId, _departmentId, _positionId, _gradeId, _stepId;
            
            if (string.IsNullOrEmpty(branchId))
                _branchId = null;
            else
                _branchId = int.Parse(branchId);
            if (string.IsNullOrEmpty(departmentId))
                _departmentId = null;
            else
                _departmentId = int.Parse(departmentId);
            if (string.IsNullOrEmpty(positionId))
                _positionId = null;
            else
                _positionId = int.Parse(positionId);
            if (string.IsNullOrEmpty(gradeId))
                _gradeId = null;
            else
                _gradeId = int.Parse(gradeId);
            if (string.IsNullOrEmpty(stepId))
                _stepId = null;
            else
                _stepId = int.Parse(stepId);

            List<GetEmployeeListGradeStepResult> list = PositionGradeStepAmountManager.GetEmployeeListGradeStep
                (_branchId, _departmentId, name, _positionId, _gradeId, _stepId,start,limit);
           
            if(list.Count>0)
                return new Paging<GetEmployeeListGradeStepResult>(list, list[0].TotalRows.Value);
            else
                return new Paging<GetEmployeeListGradeStepResult>(list, 0);
        }
    }
}
