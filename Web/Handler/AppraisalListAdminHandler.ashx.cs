﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;

namespace Web.Handler
{
    /// <summary>
    /// Summary description for PayrollMessageHandler
    /// </summary>
    public class AppraisalListAdminHandler : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {

            if (HttpContext.Current.User.Identity.IsAuthenticated == false)
            {
                context.Response.Write("");
                context.Response.End();
                return;
            }

            context.Response.ContentType = "application/json";
            int start = 0;
            int pagesize = 50;
            int EmoloyeeID = -1;
            

            if (!string.IsNullOrEmpty(context.Request["start"]))
                start = Convert.ToInt32(context.Request["start"].ToString());

            //PageSize
            if (!string.IsNullOrEmpty(context.Request["PageSize"]))
                pagesize = Convert.ToInt32(context.Request["PageSize"]);







            Paging<GetAppraisalListAdminSPResult> list = null;
            List<GetAppraisalListAdminSPResult> resultSet = NewHRManager.GetALLAppraisalList(start, pagesize);


            if (resultSet.Count > 0)
                list = new Paging<GetAppraisalListAdminSPResult>(resultSet, Convert.ToInt32(resultSet[0].TotalRows));
            else
                list = new Paging<GetAppraisalListAdminSPResult>(resultSet, 0);

            context.Response.Write(JSON.Serialize(list));

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}