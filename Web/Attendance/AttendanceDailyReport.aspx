﻿<%@ Page Title="Attendance Daily Report" Language="C#" MasterPageFile="~/Master/NewDetails.Master" AutoEventWireup="true" CodeBehind="AttendanceDailyReport.aspx.cs" Inherits="Web.Attendance.AttendanceDailyReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">

    <style type="text/css">
        .hideLeftBlockCssInPage {
            margin: 0px !important;
            padding-left: 10px !important;
        }

        #menu {
            display: none;
        }

        .tdClass1 {
            width: 100px !important;
            background-color: #9BC2E6;
            height: 28px;
            padding-left: 5px !important;
            color: black;
            font-weight: bold;
            text-align: center;
        }

        .tdClass2 {
            width: 100px !important;
            background-color: #DDEBF6;
            height: 28px;
            padding-left: 5px !important;
            text-align: center;
            color: #2F75B5;
            font-weight: bold;
        }
    </style>
    <style type="text/css">
        .innerLR {
            padding: 0px !important;
        }
    </style>
    <script type="text/javascript" src="<%= ResolveUrl("~/Employee/override.js?v=") + Web.Helper.WebHelper.Version %>"></script>
    <script type="text/javascript">
    
        function searchList() {
            <%=gridAttReport.ClientID %>.getStore().pageSize = <%=cmbPageSize.ClientID %>.getValue();
              <%=PagingToolbar1.ClientID %>.doRefresh();
          }
    
          function chkEnglish_Change()
          {
              var value = <%= chkEnglish.ClientID %>.getValue();
            if(value)
            {
                <%= calNepDate.ClientID %>.disable();
                <%= calEngDate.ClientID %>.enable();
            }
            else
            {
                <%= calNepDate.ClientID %>.enable();
                <%= calEngDate.ClientID %>.disable();
            }
        }

    </script>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">

    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>Daily Attendance Report

                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel" style="margin-top: 0px !important; padding-top: 0px !important;">
        <div class="innerLR">

            <ext:Hidden ID="hdnColumns" runat="server" />
            <ext:Hidden ID="Hidden_AtteStatus" runat="server" />

            <div class="attribute" style="padding: 0px">
                <ext:Container ID="listingContainer" runat="server">
                    <Content>
                        <table class="tblexp">
                            <tr>
                                <td>
                                    <pr:CalendarExtControl Width="110px" FieldLabel="&nbsp;" StyleSpec="margin-left:0px;"
                                        ID="calNepDate" runat="server" LabelAlign="Top" LabelSeparator="" />
                                </td>
                                <td style='vertical-align: bottom; padding-bottom: 0px;'>
                                    <ext:Checkbox ID="chkEnglish" runat="server" BoxLabel="English Date" MarginSpec="0 0 0 15">
                                        <Listeners>
                                            <Change Fn="chkEnglish_Change" />
                                        </Listeners>
                                    </ext:Checkbox>
                                </td>
                                <td>
                                    <ext:DateField ID="calEngDate" runat="server" FieldLabel="&nbsp;" LabelSeparator="" MarginSpec="0 0 0 5"
                                        LabelAlign="Top" Width="110">
                                        <Plugins>
                                            <ext:GenericPlugin ID="HighlightDates1" runat="server" InstanceName="Ext.ux.DatePicker.HighlightWithNepDates" />
                                        </Plugins>
                                    </ext:DateField>
                                </td>
                                <td style='vertical-align: bottom; padding-bottom: 0px;'>
                                    <ext:ComboBox ID="cmbBranch" Width="150" runat="server" ValueField="BranchId" DisplayField="BranchName" EmptyText="Branch" MarginSpec="0 0 0 55"
                                        FieldLabel="" LabelSeparator="" ForceSelection="true"
                                        QueryMode="Local">
                                        <Store>
                                            <ext:Store ID="Store5" runat="server">
                                                <Model>
                                                    <ext:Model ID="Model5" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="BranchId" />
                                                            <ext:ModelField Name="BranchName" />
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <Triggers>
                                            <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                                        </Triggers>
                                        <Listeners>
                                            <Select Handler="this.getTrigger(0).show();" />
                                            <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                            <TriggerClick Handler="if (index == 0) {
                                                               this.clearValue();
                                                               this.getTrigger(0).hide();
                                                           }" />
                                        </Listeners>
                                    </ext:ComboBox>
                                </td>
                                <td style='vertical-align: bottom; padding-bottom: 0px;'>
                                    <ext:Button ID="btnLoad" runat="server" Text="Show" Width="100"
                                        MarginSpec="10 0 0 15">
                                        <Listeners>
                                            <Click Fn="searchList">
                                            </Click>
                                        </Listeners>
                                    </ext:Button>
                                </td>
                                <td style='vertical-align: bottom; padding-bottom: 0px;'>
                                    <ext:Button ID="btnExport" runat="server" Text="Export" Width="100" AutoPostBack="true" OnClick="btnExport_Click"
                                        MarginSpec="10 0 0 385">
                                    </ext:Button>

                                </td>
                            </tr>
                        </table>
                        <br />
                        <table>
                            <tr>
                                <td class="tdClass1">Total</td>
                                <td class="tdClass1">At Work</td>
                                <td style="width: 50px;"></td>
                                <td class="tdClass1">On Leave</td>
                                <td class="tdClass1">Absent</td>
                                <td style="width: 50px;"></td>
                                <td class="tdClass1">Timely In</td>
                                <td class="tdClass1">Early In</td>
                                <td class="tdClass1">Late In</td>
                                <td class="tdClass1">Timely Out</td>
                                <td class="tdClass1">Early Out</td>
                                <td class="tdClass1">Late Out</td>
                            </tr>
                            <tr>
                                <td class="tdClass2" style="background-color: #BDD7EE;">
                                    <ext:LinkButton ID="btnTotal" runat="server" Text="0" Width="100">
                                        <Listeners>
                                            <Click Handler="#{Hidden_AtteStatus}.setValue('0'); searchList();" />
                                        </Listeners>
                                    </ext:LinkButton>
                                </td>

                                <td class="tdClass2" style="background-color: #C6E0B4;">
                                    <ext:LinkButton ID="btnAtWork" runat="server" Text="0" Width="100">
                                        <Listeners>
                                            <Click Handler="#{Hidden_AtteStatus}.setValue('11'); searchList();" />
                                        </Listeners>
                                    </ext:LinkButton>
                                </td>

                                <td style="width: 50px;"></td>

                                <td class="tdClass2" style="background-color: #FFD966;">
                                    <ext:LinkButton ID="btnOnLeave" runat="server" Text="0" Width="100">
                                        <Listeners>
                                            <Click Handler="#{Hidden_AtteStatus}.setValue('12'); searchList();" />
                                        </Listeners>
                                    </ext:LinkButton>
                                </td>

                                <td class="tdClass2" style="background-color: #ED7D31;">
                                    <ext:LinkButton ID="btnAbsent" runat="server" Text="0" Width="100">
                                        <Listeners>
                                            <Click Handler="#{Hidden_AtteStatus}.setValue('13'); searchList();" />
                                        </Listeners>
                                    </ext:LinkButton>
                                </td>

                                <td style="width: 50px;"></td>

                                <td class="tdClass2" style="background-color: #A9D08E;">
                                    <ext:LinkButton ID="btnTimelyIn" runat="server" Text="0" Width="100">
                                        <Listeners>
                                            <Click Handler="#{Hidden_AtteStatus}.setValue('14'); searchList();" />
                                        </Listeners>
                                    </ext:LinkButton>
                                </td>

                                <td class="tdClass2" style="background-color: #70AD47;">
                                    <ext:LinkButton ID="btnEarlyIn" runat="server" Text="0" Width="100">
                                        <Listeners>
                                            <Click Handler="#{Hidden_AtteStatus}.setValue('15'); searchList();" />
                                        </Listeners>
                                    </ext:LinkButton>
                                </td>

                                <td class="tdClass2" style="background-color: #F4B084;">
                                    <ext:LinkButton ID="btnLateIn" runat="server" Text="0" Width="100">
                                        <Listeners>
                                            <Click Handler="#{Hidden_AtteStatus}.setValue('16'); searchList();" />
                                        </Listeners>
                                    </ext:LinkButton>
                                </td>

                                <td class="tdClass2" style="background-color: #A9D08E;">
                                    <ext:LinkButton ID="btnTimelyOut" runat="server" Text="0" Width="100">
                                        <Listeners>
                                            <Click Handler="#{Hidden_AtteStatus}.setValue('17'); searchList();" />
                                        </Listeners>
                                    </ext:LinkButton>
                                </td>

                                <td class="tdClass2" style="background-color: #F4B084;">
                                    <ext:LinkButton ID="btnEarlyOut" runat="server" Text="0" Width="100">
                                        <Listeners>
                                            <Click Handler="#{Hidden_AtteStatus}.setValue('18'); searchList();" />
                                        </Listeners>
                                    </ext:LinkButton>
                                </td>

                                <td class="tdClass2" style="background-color: #70AD47;">
                                    <ext:LinkButton ID="btnLateOut" runat="server" Text="0" Width="100">
                                        <Listeners>
                                            <Click Handler="#{Hidden_AtteStatus}.setValue('19'); searchList();" />
                                        </Listeners>
                                    </ext:LinkButton>
                                </td>
                            </tr>
                        </table>
                    </Content>
                </ext:Container>
            </div>

            <div style="float: right; margin-right: 20px;">
                <ext:ImageButton ID="btnColWindow" runat="server" ImageUrl="../Styles/images/side_edit.png" ToolTip="Show/hide columns">
                    <DirectEvents>
                        <Click OnEvent="btnColWindow_Click">
                            <EventMask ShowMask="true" />
                        </Click>
                    </DirectEvents>
                </ext:ImageButton>
            </div>

            <ext:KeyMap ID="KeyMap1" runat="server" Target="={#{listingContainer}.getBody()}">
                <Binding>
                    <ext:KeyBinding Handler="#{btnLoad}.fireEvent('click');">
                        <Keys>
                            <ext:Key Code="ENTER" />
                        </Keys>
                    </ext:KeyBinding>
                </Binding>
            </ext:KeyMap>

            <ext:GridPanel StyleSpec="margin-top:20px;" ID="gridAttReport" runat="server"
                Cls="itemgrid" OnReadData="Store_ReadData" Scroll="Horizontal">
                <Store>
                    <ext:Store ID="storeAttReport" runat="server" AutoLoad="true" OnReadData="Store_ReadData"
                        RemotePaging="true" RemoteSort="true">
                        <Proxy>
                            <ext:PageProxy Timeout="9999999" />
                        </Proxy>
                        <Model>
                            <ext:Model ID="Model1" runat="server" IDProperty="EmployeeId">
                                <Fields>
                                    <ext:ModelField Name="EmployeeId" Type="String" />
                                    <ext:ModelField Name="EmplooyeeName" Type="String" />
                                    <ext:ModelField Name="Branch" Type="String" />
                                    <ext:ModelField Name="Department" Type="String" />
                                    <ext:ModelField Name="InTime" Type="String" />
                                    <ext:ModelField Name="OutTime" Type="String" />
                                    <ext:ModelField Name="LunchIn" Type="String" />
                                    <ext:ModelField Name="LunchOut" Type="String" />
                                    <ext:ModelField Name="InTimeString" Type="String" />
                                    <ext:ModelField Name="OutTimeString" Type="String" />
                                    <ext:ModelField Name="LunchInString" Type="String" />
                                    <ext:ModelField Name="LunchOutString" Type="String" />
                                    <ext:ModelField Name="InRemarksText" Type="String" />
                                    <ext:ModelField Name="OutRemarksText" Type="String" />
                                    <ext:ModelField Name="RefinedWorkHour" Type="String" />
                                    <ext:ModelField Name="InNote" Type="String" />
                                    <ext:ModelField Name="OutNote" Type="String" />
                                    <ext:ModelField Name="LeaveName" Type="String" />
                                    <ext:ModelField Name="InIPAddress" Type="String" />
                                    <ext:ModelField Name="OutIPAddress" Type="String" />
                                </Fields>
                            </ext:Model>
                        </Model>
                        <Sorters>
                            <ext:DataSorter Property="EmployeeId" Direction="ASC" />
                        </Sorters>
                    </ext:Store>
                </Store>
                <ColumnModel ID="ColumnModel1" runat="server">
                    <Columns>
                        <ext:Column ID="colEmployeeId" runat="server" Text="EIN" DataIndex="EmployeeId" MenuDisabled="true"
                            Sortable="true" Align="Center" Width="70" Locked="true" />
                        <ext:Column ID="colEmplooyeeName" runat="server" Text="Name" DataIndex="EmplooyeeName"
                            MenuDisabled="true" Sortable="true" Align="Left" Width="160" Locked="true" />
                        <ext:Column ID="colBranch" runat="server" Text="Branch" DataIndex="Branch" MenuDisabled="true"
                            Sortable="true" Align="Left" Width="145" />
                        <ext:Column ID="colDepartment" runat="server" Text="Department" DataIndex="Department" MenuDisabled="true"
                            Sortable="true" Align="Left" Width="145" />
                        <ext:Column ID="colInTime" runat="server" Text="Office In" DataIndex="InTimeString" MenuDisabled="true"
                            Sortable="true" Align="Left" Width="70" />
                        <ext:Column ID="colInNote" runat="server" Text="In Note" DataIndex="InNote"
                            MenuDisabled="true" Sortable="true" Align="Left" Width="150" />
                        <ext:Column ID="colOutTime" runat="server" Text="Office Out" DataIndex="OutTimeString"
                            MenuDisabled="true" Sortable="true" Align="Left" Width="70" />
                        <ext:Column ID="colOutNote" runat="server" Text="Out Note" DataIndex="OutNote"
                            MenuDisabled="true" Sortable="true" Align="Left" Width="150" />
                        <ext:Column ID="colLunchIn" runat="server" Text="Lunch In" DataIndex="LunchInString"
                            MenuDisabled="true" Sortable="true" Align="Left" Width="80" />
                        <ext:Column ID="colLunchOut" runat="server" Text="Lunch Out" DataIndex="LunchOutString"
                            MenuDisabled="true" Sortable="true" Align="Left" Width="80" />



                        <ext:Column ID="colRefinedWorkHour" runat="server" Text="Work Hours" DataIndex="RefinedWorkHour"
                            MenuDisabled="true" Sortable="true" Align="Left" Width="90" />
                        <ext:Column ID="colLeaveName" runat="server" Text="Leave" DataIndex="LeaveName"
                            MenuDisabled="true" Sortable="true" Align="Left" Width="120" />

                        <ext:Column ID="Column1" runat="server" Text="In IP" DataIndex="InIPAddress"
                            MenuDisabled="true" Sortable="true" Align="Left" Width="100" />
                        <ext:Column ID="Column2" runat="server" Text="Out IP" DataIndex="OutIPAddress"
                            MenuDisabled="true" Sortable="true" Align="Left" Width="100" />
                    </Columns>
                </ColumnModel>
                <Plugins>
                    <ext:FilterHeader ID="FilterHeader2" runat="server" Remote="true" OnCreateFilterableField="OnCreateFilterableField" />
                </Plugins>
                <BottomBar>
                    <ext:PagingToolbar Height="40" ID="PagingToolbar1" runat="server" StoreID="storeAttReport"
                        DisplayInfo="true">
                        <Items>
                            <ext:Label ID="lblPageSize" runat="server" Text="Page size:" />
                            <ext:ToolbarSpacer ID="ToolbarSpacer_listItems" runat="server" Width="10" />
                            <ext:ComboBox ID="cmbPageSize" runat="server" Width="80" SelectOnFocus="true" Selectable="true"
                                ValueField="Value" DisplayField="Text" ForceSelection="true" AllowBlank="false">
                                <Listeners>
                                    <Select Handler="searchList()" />
                                    <%--#{cmbPageSize}.pageSize = this.getValue();#{PagingToolbar1}.moveFirst();--%>
                                </Listeners>
                                <Items>
                                    <ext:ListItem Value="100" Text="100" />
                                    <ext:ListItem Value="500" Text="500" />
                                    <ext:ListItem Value="100000" Text="All" />
                                </Items>
                                <SelectedItems>
                                    <ext:ListItem Index="0">
                                    </ext:ListItem>
                                </SelectedItems>
                            </ext:ComboBox>
                        </Items>
                    </ext:PagingToolbar>
                </BottomBar>
            </ext:GridPanel>


        </div>

        <ext:Window ID="wColumns" Width="250" Height="430" Title="Only Show Columns"
            Closable="true" Resizable="false" Hidden="true" runat="server" AutoScroll="true">
            <Content>
                <div class="windowContentWrapper">
                    <table style="margin-left: 30px; margin-top: 20px;">
                        <tr>
                            <td>
                                <ext:Checkbox ID="chkEIN" runat="server" BoxLabel="EIN">
                                </ext:Checkbox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <ext:Checkbox ID="chkEmplooyeeName" runat="server" BoxLabel="Name">
                                </ext:Checkbox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <ext:Checkbox ID="chkBranch" runat="server" BoxLabel="Branch">
                                </ext:Checkbox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <ext:Checkbox ID="chkDepartment" runat="server" BoxLabel="Department">
                                </ext:Checkbox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <ext:Checkbox ID="chkInTime" runat="server" BoxLabel="Office In">
                                </ext:Checkbox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <ext:Checkbox ID="chkInNote" runat="server" BoxLabel="In Note">
                                </ext:Checkbox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <ext:Checkbox ID="chkOutTime" runat="server" BoxLabel="Office Out">
                                </ext:Checkbox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <ext:Checkbox ID="chkOutNote" runat="server" BoxLabel="Out Note">
                                </ext:Checkbox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <ext:Checkbox ID="chkLunchOut" runat="server" BoxLabel="Lunch Out">
                                </ext:Checkbox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <ext:Checkbox ID="chkLunchIn" runat="server" BoxLabel="Lunch In">
                                </ext:Checkbox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <ext:Checkbox ID="chkRefinedWorkHour" runat="server" BoxLabel="Work Hours">
                                </ext:Checkbox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <ext:Checkbox ID="chkLeaveName" runat="server" BoxLabel="Leave Name">
                                </ext:Checkbox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <ext:Button ID="btnSave" runat="server" Text="Show" Width="100"
                                    MarginSpec="10 0 0 15">
                                    <DirectEvents>
                                        <Click OnEvent="btnSave_Click">
                                            <EventMask ShowMask="true" />
                                        </Click>
                                    </DirectEvents>
                                </ext:Button>
                            </td>
                        </tr>

                    </table>
                </div>
            </Content>
        </ext:Window>
    </div>


</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
