﻿<%@ Page Title="Attendance Summary" Language="C#" MasterPageFile="~/Master/NewDetails.Master"
    AutoEventWireup="true" CodeBehind="AttendanceSummary.aspx.cs" Inherits="Web.CP.AttendanceSummary" %>

<%@ Register Src="~/newhr/UserControls/GenerateAttendanceRptCtl.ascx" TagName="GenerateAttendanceRptCtl"
    TagPrefix="uc1" %>
<%@ MasterType VirtualPath="~/Master/NewDetails.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .x-grid-cell-inner
        {
            padding: 5px 5px 5px 5px;
        }
        .x-column-header-inner
        {
            padding: 7px 5px 7px 5px;
        }
    </style>
    <script type="text/javascript">

    function searchList() {
              <%=PagingToolbar1.ClientID %>.doRefresh();
        }

  var refreshparentWindow = function()
  {
  
    <%=lnkRebindDate.ClientID %>.fireEvent('Click'); 
    <%=AEWindow.ClientID %>.hide();
  }
   
    function SelEmpSearch()
    {
        <%=hdnEmpSearch.ClientID %>.setValue(<%=cmbEmpSearch.ClientID %>.getValue());
    }


    var renderUrl = function(value)
    {
        return "<a href='AttendanceDetails.aspx?" + value + "'>" + "Details</a>";
    }

     Ext.define('MyOverrides', {
            override: "Ext.ux.grid.column.ProgressBar",

            renderer: function (value, meta) {
                var me = this,
            text,
            cls = me.progressCls,
            pCls,
            cWidth = me.getWidth(true) - 2;

                if (me.hideIfEmpty && (!value && value !== 0 || value < 0)) {
                    return "";
                }

                value = value || 0;

                text = Ext.String.format(me.progressText, Math.round(value)); //you should change this line

                pCls = cls + ' ' + cls + '-' + me.ui;
                meta.tdCls = "x-progress-column-cell";
                meta.style = "padding:0px;margin:0px;";
                v = '<div class="' + pCls + '" style="margin:1px 1px 1px 1px;width:' + cWidth + 'px;"><div class="' + cls + '-text ' + cls + '-text-back" style="width:' + (cWidth - 2) + 'px;">' +
                text +
            '</div>' +
            '<div class="' + cls + '-bar ' + cls + '-bar-' + me.ui + '" style="width: ' + value + '%;">' +
                '<div class="' + cls + '-text" style="width:' + (cWidth - 2) + 'px;">' +
                    '<div>' + text + '</div>' +
                '</div>' +
            '</div></div>'
                return v;
            }
        });


    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <script type="text/javascript" src="<%= ResolveUrl("~/Employee/override.js?v=") + Web.Helper.WebHelper.Version %>"></script>
    <ext:LinkButton ID="lnkRebindDate" Hidden="true" Text="" runat="server">
        <DirectEvents>
            <Click OnEvent="RebindDate">
            </Click>
        </DirectEvents>
    </ext:LinkButton>
    <ext:Hidden ID="hdnEmployeeId" runat="server" />
    <ext:Hidden ID="hdnBranchDepartmentId" runat="server" />
    <ext:Hidden ID="hdnMsg" runat="server" />
    <ext:Hidden ID="hdnEmpSearch" runat="server" />
    <ext:Hidden ID="hdnSortBy" runat="server" />
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Time Attendance Summary
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel" style="margin-top: 0px !important; padding-top: 0px !important;">
        <div class="innerLR" style='padding-top:5px;'>
            <table>
                <tr>
                    <td>
                        Last Proceed for &nbsp;<span id="dispLastDate" runat="server" style=""></span>
                           
                    </td>
                    <td style="padding-left: 50px;">
                        <ext:LinkButton ID="btnGenerateAttendance" Text="Generate Attendance Report" runat="server">
                            <DirectEvents>
                                <Click OnEvent="btnGenerateAttendance_Click">
                                    <EventMask ShowMask="true" />
                                </Click>
                            </DirectEvents>
                        </ext:LinkButton>
                    </td>
                </tr>
            </table>
            <ext:Store runat="server" ID="storeSearch" AutoLoad="false">
                <Proxy>
                    <ext:AjaxProxy Url="../Handler/EmpSearchID.ashx">
                        <ActionMethods Read="GET" />
                        <Reader>
                            <ext:JsonReader Root="plants" TotalProperty="total" />
                        </Reader>
                    </ext:AjaxProxy>
                </Proxy>
                <Model>
                    <ext:Model ID="Model5" IDProperty="EmployeeId" Name="ItemLineModel" runat="server">
                        <Fields>
                            <ext:ModelField Name="Name" Type="String" />
                            <ext:ModelField Name="EmployeeId" Type="String" />
                        </Fields>
                    </ext:Model>
                </Model>
            </ext:Store>
            <div class="alert alert-info" style="margin-top: 10px; padding-top: 0px!important;">
                <ext:Container ID="listingContainer" runat="server">
                    <Content>
                        <table>
                            <tr>
                                <td>
                                    <ext:DateField LabelWidth="120" MarginSpec="0 5 0 5" Width="100" FieldLabel="From"
                                        ID="calFromDate" runat="server" LabelAlign="Top" LabelSeparator="">
                                        <Plugins>
                                            <ext:GenericPlugin ID="HighlightDates1" runat="server" InstanceName="Ext.ux.DatePicker.HighlightWithNepDates" />
                                        </Plugins>
                                    </ext:DateField>
                                </td>
                                <td>
                                    <ext:DateField LabelWidth="120" MarginSpec="0 5 0 5" Width="100" FieldLabel="To"
                                        ID="calToDate" runat="server" LabelAlign="Top" LabelSeparator="">
                                        <Plugins>
                                            <ext:GenericPlugin ID="GenericPlugin1" runat="server" InstanceName="Ext.ux.DatePicker.HighlightWithNepDates" />
                                        </Plugins>
                                    </ext:DateField>
                                </td>
                                <td>
                                    <ext:ComboBox Width="140" MatchFieldWidth="false" MarginSpec="0 5 0 5" LabelSeparator=""
                                        LabelAlign="Top" QueryMode="Local" ID="cmbBranch" DisplayField="Name" ValueField="BranchId"
                                        runat="server" FieldLabel="Branch">
                                        <Items>
                                            <ext:ListItem Index="0" Value="-1" Text="All">
                                            </ext:ListItem>
                                        </Items>
                                        <Store>
                                            <ext:Store ID="Store5" runat="server">
                                                <Model>
                                                    <ext:Model ID="Model7" IDProperty="BranchId" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="BranchId" />
                                                            <ext:ModelField Name="Name" />
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                    </ext:ComboBox>
                                </td>
                                <td>
                                    <ext:ComboBox FieldLabel="Department" MatchFieldWidth="false" ID="cmbDepartment"
                                        Width="140" MarginSpec="0 5 0 5" runat="server" ValueField="DepartmentId" DisplayField="Name"
                                        LabelAlign="Top" LabelSeparator="" QueryMode="Local">
                                        <Items>
                                            <ext:ListItem Index="0" Value="-1" Text="All">
                                            </ext:ListItem>
                                        </Items>
                                        <Store>
                                            <ext:Store ID="Store6" runat="server">
                                                <Model>
                                                    <ext:Model ID="Model8" IDProperty="DepartmentId" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="DepartmentId" />
                                                            <ext:ModelField Name="Name" />
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                    </ext:ComboBox>
                                </td>
                                <td>
                                    <ext:ComboBox ID="cmbLevel" MatchFieldWidth="false" runat="server" FieldLabel="Level/Position"
                                        Width="140" MarginSpec="0 5 0 5" DisplayField="Name" ValueField="LevelId" QueryMode="Local"
                                        LabelSeparator="" LabelAlign="Top">
                                        <Items>
                                            <ext:ListItem Index="0" Value="-1" Text="All">
                                            </ext:ListItem>
                                        </Items>
                                        <Store>
                                            <ext:Store ID="storeLevel" runat="server">
                                                <Model>
                                                    <ext:Model ID="modelLevel" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="LevelId" Type="Int" />
                                                            <ext:ModelField Name="Name" Type="String" />
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                    </ext:ComboBox>
                                </td>
                                <td>
                                    <ext:ComboBox MatchFieldWidth="false" FieldLabel="Designation" ID="cmbDesignation"
                                        Width="140" MarginSpec="0 5 0 5" runat="server" ValueField="Text" DisplayField="Text"
                                        LabelAlign="Top" LabelSeparator="" QueryMode="Local">
                                        <Items>
                                            <ext:ListItem Index="0" Value="All" Text="All">
                                            </ext:ListItem>
                                        </Items>
                                        <Store>
                                            <ext:Store ID="Store7" runat="server">
                                                <Model>
                                                    <ext:Model ID="Model9" IDProperty="Text" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="Text" />
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                    </ext:ComboBox>
                                </td>
                                <td>
                                    <ext:ComboBox LabelSeparator="" ID="cmbEmpSearch" FieldLabel="Search Employee" EmptyText="Employee Name"
                                        LabelWidth="80" LabelAlign="Top" runat="server" DisplayField="Name" ValueField="EmployeeId"
                                        StoreID="storeSearch" TypeAhead="false" Width="200" PageSize="9999" HideBaseTrigger="true"
                                        MinChars="1" TriggerAction="All" ForceSelection="false">
                                        <ListConfig LoadingText="Searching..." MinWidth="200" StyleSpec="border-top:1px solid #98c0f4;">
                                            <ItemTpl ID="ItemTpl2" runat="server">
                                                <Html>
                                                    <tpl>
                                                        <div class="search-item">
                                                                        <span>{Name}</span>  
                                                         </div>
					                            </tpl>
                                                </Html>
                                            </ItemTpl>
                                        </ListConfig>
                                        <Triggers>
                                            <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                                        </Triggers>
                                        <Listeners>
                                            <Select Handler="this.getTrigger(0).show(); SelEmpSearch();" />
                                            <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                            <TriggerClick Handler="if (index == 0) { 
                                                       this.clearValue(); 
                                                       this.getTrigger(0).hide();
                                                   }" />
                                        </Listeners>
                                    </ext:ComboBox>
                                </td>
                                <td>
                                    <ext:Button ID="btnLoad" runat="server" Text="Load" Width="100" Cls="btn btn-default"
                                        MarginSpec="25 10 10 10">
                                        <Listeners>
                                            <Click Fn="searchList">
                                            </Click>
                                        </Listeners>
                                    </ext:Button>
                                </td>
                                <td style='vertical-align: bottom; padding-bottom: 10px;'>
                                    <i class="fa fa-caret-down" onclick='document.getElementById("<%= rowFilter.ClientID %>").style.display="";' style='font-size: 18px; padding: 2px;
                                        cursor: pointer; width: 24px; padding-left: 7px;'></i>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <ext:Checkbox runat="server" ID="chkIsGroup" BoxLabel="Use Grouping">
                                        <DirectEvents>
                                            <Change OnEvent="chkIsGroup_Change">
                                                <EventMask ShowMask="true" />
                                            </Change>
                                        </DirectEvents>
                                    </ext:Checkbox>
                                </td>
                                <td>
                                    <ext:Checkbox runat="server" Hidden="true" ID="chkGroupBranchWise" BoxLabel="Branchwise" />
                                </td>
                                <td>
                                    <ext:Checkbox runat="server" Hidden="true" ID="chkGroupDepWise" BoxLabel="Departmentwise" />
                                </td>
                                <td>
                                    <ext:Checkbox runat="server" Hidden="true" ID="chkPositionWise" BoxLabel="Position/Levelwise" />
                                </td>
                                <td>
                                    <ext:Checkbox runat="server" Hidden="true" ID="chkDesignation" BoxLabel="Designationwise" />
                                </td>
                            </tr>
                            <tr id="rowFilter" style='display:none' runat="server">
                                <td colspan="5">
                                    <ext:ComboBox ID="cmbPayGroup" Hidden="true" Width="140" runat="server" ValueField="PayGroupID"
                                        DisplayField="Name" FieldLabel="Pay Group" LabelAlign="top" LabelSeparator=""
                                        ForceSelection="true" QueryMode="Local">
                                        <Store>
                                            <ext:Store ID="Store13" runat="server">
                                                <Model>
                                                    <ext:Model ID="Model14" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="PayGroupID" Type="String" />
                                                            <ext:ModelField Name="Name" />
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <SelectedItems>
                                            <ext:ListItem Index="0" />
                                        </SelectedItems>
                                    </ext:ComboBox>
                                </td>
                            </tr>
                        </table>
                    </Content>
                </ext:Container>
            </div>
            <ext:KeyMap ID="KeyMap1" runat="server" Target="={#{listingContainer}.getBody()}">
                <Binding>
                    <ext:KeyBinding Handler="#{btnLoad}.fireEvent('click');">
                        <Keys>
                            <ext:Key Code="ENTER" />
                        </Keys>
                    </ext:KeyBinding>
                </Binding>
            </ext:KeyMap>
            <ext:GridPanel StyleSpec="margin-top:15px;" ID="gridBranchTransfer" runat="server"
                Cls="itemgrid" OnReadData="Store_ReadData">
                <Store>
                    <ext:Store PageSize="50" ID="storeEmpList" runat="server" AutoLoad="true" OnReadData="Store_ReadData"
                        RemotePaging="true" RemoteSort="true">
                        <Proxy>
                            <ext:PageProxy />
                        </Proxy>
                        <Model>
                            <ext:Model ID="Model1" runat="server">
                                <Fields>
                                    <ext:ModelField Name="RowNumber" Type="String" />
                                    <ext:ModelField Name="EmployeeId" Type="String" />
                                    <ext:ModelField Name="IDCardNo" Type="String" />
                                    <ext:ModelField Name="Name" Type="String" />
                                    <ext:ModelField Name="Branch" Type="String" />
                                    <ext:ModelField Name="Department" Type="String" />
                                    <ext:ModelField Name="LevelPosition" Type="String" />
                                    <ext:ModelField Name="Designation" Type="String" />
                                    <ext:ModelField Name="MissingOrExtraText" Type="String" />
                                    <ext:ModelField Name="WorkPercent" Type="String" />
                                    <ext:ModelField Name="AvgMinText" Type="String" />
                                    <ext:ModelField Name="StandardMinText" Type="String" />
                                    <ext:ModelField Name="WorkedMinText" Type="String" />
                                    <ext:ModelField Name="LateInCount" Type="String" />
                                    <ext:ModelField Name="WorkDays" Type="String" />
                                    <ext:ModelField Name="AbsentDays" Type="String" />
                                    <ext:ModelField Name="LeaveDays" Type="String" />
                                    <ext:ModelField Name="QueryString" Type="String" />
                                    <ext:ModelField Name="OTMinuteText" Type="String" />
                                    <ext:ModelField Name="LateMinText" Type="String" />
                                    <ext:ModelField Name="LateMin" Type="String" />
                                </Fields>
                            </ext:Model>
                        </Model>
                        <Sorters>
                            <ext:DataSorter Property="Name" Direction="ASC" />
                        </Sorters>
                    </ext:Store>
                </Store>
                <ColumnModel ID="ColumnModel1" runat="server">
                    <Columns>
                        <ext:Column ID="colFromDate" runat="server" Text="SN" DataIndex="RowNumber" MenuDisabled="false"
                            Sortable="false" Align="Center" Width="50" />
                        <ext:Column ID="Column_EIN" runat="server" Text="EIN" DataIndex="EmployeeId" MenuDisabled="false"
                            Sortable="true" Align="Center" Width="50" />
                        <ext:Column ID="Column_ID" runat="server" Text="I No" DataIndex="IDCardNo" MenuDisabled="false"
                            Sortable="true" Align="Center" Width="50" />
                        <ext:Column ID="Column_Name" runat="server" Text="Name" DataIndex="Name" MenuDisabled="false"
                            Sortable="true" Align="Left" Width="150" />
                        <ext:Column ID="Column_Branch" runat="server" Hidden="true" Text="Branch" DataIndex="Branch"
                            MenuDisabled="false" Sortable="true" Align="Left" Width="120" />
                        <ext:Column ID="Column_Dep" runat="server" Hidden="true" Text="Department" DataIndex="Department"
                            MenuDisabled="false" Sortable="true" Align="Left" Width="120" />
                        <ext:Column ID="Column_LevelPos" runat="server" Hidden="true" Text="Position" DataIndex="LevelPosition"
                            MenuDisabled="false" Sortable="true" Align="Left" Width="120" />
                        <ext:Column ID="Column_Designation" runat="server" Hidden="true" Text="Designation"
                            DataIndex="Designation" MenuDisabled="false" Sortable="true" Align="Left" Width="120" />
                        <ext:Column ID="colFromDateEng" runat="server" Text="Standard hrs" DataIndex="StandardMinText"
                            MenuDisabled="false" Sortable="true" Align="Right" Width="90" />
                        <ext:Column ID="Column8" runat="server" Text="Worked hrs" DataIndex="WorkedMinText"
                            MenuDisabled="false" Sortable="true" Align="Right" Width="90" />
                        <ext:Column ID="income1" runat="server" Text="Avg hrs" DataIndex="AvgMinText" MenuDisabled="false"
                            Sortable="true" Align="Right" Width="80">
                        </ext:Column>
                        <ext:Column ID="income2" runat="server" Text="Missing/Extra" DataIndex="MissingOrExtraText"
                            MenuDisabled="false" Sortable="true" Align="Right" Width="100">
                        </ext:Column>
                        <ext:Column ID="income3" runat="server" Text="Late" DataIndex="LateInCount" MenuDisabled="false"
                            Sortable="true" Align="Right" Width="70">
                        </ext:Column>
                         <ext:Column ID="Column3" runat="server" Text="Late Min" DataIndex="LateMinText" MenuDisabled="false"
                            Sortable="true" Align="Right" Width="90"> </ext:Column>
                             <ext:Column ID="Column4" runat="server" Text="Late Min Only" DataIndex="LateMin" MenuDisabled="false"
                            Sortable="true" Align="Right" Width="90"> </ext:Column>
                       
                        <ext:Column ID="Column9" runat="server" Text="Workdays" DataIndex="WorkDays" MenuDisabled="false"
                            Sortable="true" Align="Right" Width="80">
                        </ext:Column>
                        <ext:Column ID="Column2" runat="server" Text="OT hr" Hidden="true" DataIndex="OTMinuteText" MenuDisabled="false"
                            Sortable="true" Align="Right" Width="80">
                        </ext:Column>
                        <ext:Column ID="Column10" runat="server" Text="Absent" DataIndex="AbsentDays" MenuDisabled="false"
                            Sortable="true" Align="Right" Width="80">
                        </ext:Column>
                        <ext:Column ID="Column11" runat="server" Text="Leaves" DataIndex="LeaveDays" MenuDisabled="false"
                            Sortable="true" Align="Right" Width="80">
                        </ext:Column>
                        <ext:ProgressBarColumn ID="colTestProgress" runat="server" Width="100" Align="Left"
                            DataIndex="WorkPercent" Text="Work %" Resizable="false" Draggable="false" Sortable="true">
                        </ext:ProgressBarColumn>
                        <ext:Column ID="Column1" runat="server" Text="Details" DataIndex="QueryString" MenuDisabled="false"
                            Sortable="true" Align="Right" Width="80">
                            <Renderer Fn="renderUrl" />
                        </ext:Column>
                    </Columns>
                </ColumnModel>
                <BottomBar>
                    <ext:PagingToolbar ID="PagingToolbar1" runat="server" StoreID="storeEmpList" DisplayInfo="true">
                    </ext:PagingToolbar>
                </BottomBar>
            </ext:GridPanel>
        </div>
        <div class="alert alert-info" style="margin-top: 15px; height: 50px;">
            <div style="float: right;">
                <ext:Button runat="server" AutoPostBack="true" OnClick="btnExport_Click" ID="btnExport"
                    Text="<i></i>Export To Excel">
                </ext:Button>
            </div>
        </div>
    </div>
    <ext:Window ID="AEWindow" runat="server" Title="Generate Attendance" Icon="Application"
        Height="350" Width="400" BodyPadding="5" Hidden="true" Modal="true">
        <Content>
            <uc1:GenerateAttendanceRptCtl runat="server" Id="GenerateAttendanceRptCtl1" />
        </Content>
    </ext:Window>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
