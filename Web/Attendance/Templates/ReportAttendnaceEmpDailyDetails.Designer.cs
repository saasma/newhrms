namespace Web.CP.Report.Templates.HR
{
    partial class ReportAttendnaceEmpDailyDetails
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used. 
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraReports.UI.XRSummary xrSummary1 = new DevExpress.XtraReports.UI.XRSummary();
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell16 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DayTypeLeave = new DevExpress.XtraReports.UI.FormattingRule();
            this.DayTypeWeeklyHoliday = new DevExpress.XtraReports.UI.FormattingRule();
            this.DayTypeHoliday = new DevExpress.XtraReports.UI.FormattingRule();
            this.DayTypeOther = new DevExpress.XtraReports.UI.FormattingRule();
            this.ApprovedLeave = new DevExpress.XtraReports.UI.FormattingRule();
            this.LeaveWithOutApprovalOrNoInOutTime = new DevExpress.XtraReports.UI.FormattingRule();
            this.OfficeInOutInTime = new DevExpress.XtraReports.UI.FormattingRule();
            this.LateIn = new DevExpress.XtraReports.UI.FormattingRule();
            this.columnEmployeeName = new DevExpress.XtraReports.UI.XRTableCell();
            this.columnCheckInTime = new DevExpress.XtraReports.UI.XRTableCell();
            this.columnCheckInRemarks = new DevExpress.XtraReports.UI.XRTableCell();
            this.columnCheckOutTime = new DevExpress.XtraReports.UI.XRTableCell();
            this.columnCheckOutRemarks = new DevExpress.XtraReports.UI.XRTableCell();
            this.columnWorkHour = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.Title = new DevExpress.XtraReports.UI.XRControlStyle();
            this.pageHeaderBand1 = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.tableHeaderEmployeeName = new DevExpress.XtraReports.UI.XRTableCell();
            this.tableHeaderCheckInTime = new DevExpress.XtraReports.UI.XRTableCell();
            this.tableHeaderCheckInRemarks = new DevExpress.XtraReports.UI.XRTableCell();
            this.tableHeaderCheckOutTime = new DevExpress.XtraReports.UI.XRTableCell();
            this.tableHeaderCheckOutRemarks = new DevExpress.XtraReports.UI.XRTableCell();
            this.tableHeaderWorkingHour = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.reportHeaderBand1 = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.labelTitle = new DevExpress.XtraReports.UI.XRLabel();
            this.topMarginBand1 = new DevExpress.XtraReports.UI.TopMarginBand();
            this.bottomMarginBand1 = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.HesderStyle = new DevExpress.XtraReports.UI.XRControlStyle();
            this.AttendanceStyle = new DevExpress.XtraReports.UI.XRControlStyle();
            this.HeaderStyle = new DevExpress.XtraReports.UI.XRControlStyle();
            this.PayBankEven = new DevExpress.XtraReports.UI.XRControlStyle();
            this.PayBankOdd = new DevExpress.XtraReports.UI.XRControlStyle();
            this.PayCashEven = new DevExpress.XtraReports.UI.XRControlStyle();
            this.PayCashOdd = new DevExpress.XtraReports.UI.XRControlStyle();
            this.ReportHeaderStyle = new DevExpress.XtraReports.UI.XRControlStyle();
            this.GrossTotalStyle = new DevExpress.XtraReports.UI.XRControlStyle();
            this.advanceEven = new DevExpress.XtraReports.UI.XRControlStyle();
            this.advanceOdd = new DevExpress.XtraReports.UI.XRControlStyle();
            this.xrControlStyle1 = new DevExpress.XtraReports.UI.XRControlStyle();
            this.TestStyle = new DevExpress.XtraReports.UI.XRControlStyle();
            this.DataStyle = new DevExpress.XtraReports.UI.XRControlStyle();
            this.LineStyle = new DevExpress.XtraReports.UI.XRControlStyle();
            this.ReportStyle = new DevExpress.XtraReports.UI.XRControlStyle();
            this.StyleDate = new DevExpress.XtraReports.UI.XRControlStyle();
            this.AttendanceHeaderStyle = new DevExpress.XtraReports.UI.XRControlStyle();
            this.xrControlStyle2 = new DevExpress.XtraReports.UI.XRControlStyle();
            this.EvenStyle = new DevExpress.XtraReports.UI.XRControlStyle();
            this.OddStyle = new DevExpress.XtraReports.UI.XRControlStyle();
            this.AttributeHeaderStyle = new DevExpress.XtraReports.UI.XRControlStyle();
            this.xrControlStyle3 = new DevExpress.XtraReports.UI.XRControlStyle();
            this.reportDataSet1 = new Web.ReportDataSet();
            this.attendanceAllEmployeeReportTableAdapter1 = new Web.ReportDataSetTableAdapters.AttendanceAllEmployeeReportTableAdapter();
            this.report_HR_GetAttendanceDaysTableAdapter = new Web.ReportDataSetTableAdapters.Report_HR_GetAttendanceDaysTableAdapter();
            this.ReportFooter = new DevExpress.XtraReports.UI.ReportFooterBand();
            this.xrTable3 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell17 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTotal = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow6 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell18 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTimely = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell19 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblLate = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow8 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell20 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblApproved = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow10 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell21 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblAbsentee = new DevExpress.XtraReports.UI.XRTableCell();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportDataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable1});
            this.Detail.HeightF = 25F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable1
            // 
            this.xrTable1.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(201)))), ((int)(((byte)(214)))), ((int)(((byte)(237)))));
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(1.041667F, 0F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow3});
            this.xrTable1.Scripts.OnBeforePrint = "xrTable1_BeforePrint";
            this.xrTable1.Scripts.OnPrintOnPage = "xrTable1_PrintOnPage";
            this.xrTable1.SizeF = new System.Drawing.SizeF(945.9583F, 25F);
            this.xrTable1.StylePriority.UseBorderColor = false;
            this.xrTable1.StylePriority.UseBorders = false;
            this.xrTable1.StylePriority.UseFont = false;
            this.xrTable1.StylePriority.UseTextAlignment = false;
            this.xrTable1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell16,
            this.columnEmployeeName,
            this.columnCheckInTime,
            this.columnCheckInRemarks,
            this.columnCheckOutTime,
            this.columnCheckOutRemarks,
            this.columnWorkHour,
            this.xrTableCell7});
            this.xrTableRow3.Font = new System.Drawing.Font("Tahoma", 10.75F);
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.StylePriority.UseFont = false;
            this.xrTableRow3.StylePriority.UseTextAlignment = false;
            this.xrTableRow3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableRow3.Weight = 1D;
            // 
            // xrTableCell16
            // 
            this.xrTableCell16.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell16.Font = new System.Drawing.Font("Tahoma", 9F);
            this.xrTableCell16.FormattingRules.Add(this.DayTypeLeave);
            this.xrTableCell16.FormattingRules.Add(this.DayTypeWeeklyHoliday);
            this.xrTableCell16.FormattingRules.Add(this.DayTypeHoliday);
            this.xrTableCell16.FormattingRules.Add(this.DayTypeOther);
            this.xrTableCell16.FormattingRules.Add(this.ApprovedLeave);
            this.xrTableCell16.FormattingRules.Add(this.LeaveWithOutApprovalOrNoInOutTime);
            this.xrTableCell16.FormattingRules.Add(this.OfficeInOutInTime);
            this.xrTableCell16.FormattingRules.Add(this.LateIn);
            this.xrTableCell16.Name = "xrTableCell16";
            this.xrTableCell16.StylePriority.UseBorders = false;
            this.xrTableCell16.StylePriority.UseFont = false;
            xrSummary1.Func = DevExpress.XtraReports.UI.SummaryFunc.RecordNumber;
            xrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
            this.xrTableCell16.Summary = xrSummary1;
            this.xrTableCell16.Weight = 0.13483090395985314D;
            // 
            // DayTypeLeave
            // 
            this.DayTypeLeave.Condition = "[Type]==1";
            this.DayTypeLeave.DataMember = "AttendanceReport";
            // 
            // 
            // 
            this.DayTypeLeave.Formatting.BackColor = System.Drawing.Color.LavenderBlush;
            this.DayTypeLeave.Formatting.ForeColor = System.Drawing.Color.DarkRed;
            this.DayTypeLeave.Name = "DayTypeLeave";
            // 
            // DayTypeWeeklyHoliday
            // 
            this.DayTypeWeeklyHoliday.Condition = "[Type]==2";
            // 
            // 
            // 
            this.DayTypeWeeklyHoliday.Formatting.BackColor = System.Drawing.Color.Ivory;
            this.DayTypeWeeklyHoliday.Formatting.ForeColor = System.Drawing.Color.Olive;
            this.DayTypeWeeklyHoliday.Name = "DayTypeWeeklyHoliday";
            // 
            // DayTypeHoliday
            // 
            this.DayTypeHoliday.Condition = "[Type]==3";
            this.DayTypeHoliday.DataMember = "AttendanceReport";
            // 
            // 
            // 
            this.DayTypeHoliday.Formatting.BackColor = System.Drawing.Color.Honeydew;
            this.DayTypeHoliday.Formatting.BorderColor = System.Drawing.Color.Blue;
            this.DayTypeHoliday.Formatting.ForeColor = System.Drawing.Color.DarkGreen;
            this.DayTypeHoliday.Name = "DayTypeHoliday";
            // 
            // DayTypeOther
            // 
            this.DayTypeOther.Condition = "[Type]==0";
            this.DayTypeOther.DataMember = "AttendanceReport";
            // 
            // 
            // 
            this.DayTypeOther.Formatting.BackColor = System.Drawing.Color.AliceBlue;
            this.DayTypeOther.Formatting.ForeColor = System.Drawing.Color.MidnightBlue;
            this.DayTypeOther.Name = "DayTypeOther";
            // 
            // ApprovedLeave
            // 
            this.ApprovedLeave.Condition = "[AtteState] = 1 or [AtteState] = 4";
            this.ApprovedLeave.DataMember = "AttendanceAllEmployeeReport";
            // 
            // 
            // 
            this.ApprovedLeave.Formatting.BackColor = System.Drawing.Color.LavenderBlush;
            this.ApprovedLeave.Formatting.Font = new System.Drawing.Font("Times New Roman", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ApprovedLeave.Formatting.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.ApprovedLeave.Name = "ApprovedLeave";
            // 
            // LeaveWithOutApprovalOrNoInOutTime
            // 
            this.LeaveWithOutApprovalOrNoInOutTime.Condition = "[AtteState]=9";
            this.LeaveWithOutApprovalOrNoInOutTime.DataMember = "AttendanceAllEmployeeReport";
            // 
            // 
            // 
            this.LeaveWithOutApprovalOrNoInOutTime.Formatting.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.LeaveWithOutApprovalOrNoInOutTime.Formatting.Font = new System.Drawing.Font("Times New Roman", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LeaveWithOutApprovalOrNoInOutTime.Formatting.ForeColor = System.Drawing.Color.White;
            this.LeaveWithOutApprovalOrNoInOutTime.Name = "LeaveWithOutApprovalOrNoInOutTime";
            // 
            // OfficeInOutInTime
            // 
            this.OfficeInOutInTime.Condition = "[AtteState]=10";
            this.OfficeInOutInTime.DataMember = "AttendanceAllEmployeeReport";
            // 
            // 
            // 
            this.OfficeInOutInTime.Formatting.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.OfficeInOutInTime.Formatting.Font = new System.Drawing.Font("Times New Roman", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OfficeInOutInTime.Formatting.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(125)))), ((int)(((byte)(149)))));
            this.OfficeInOutInTime.Name = "OfficeInOutInTime";
            // 
            // LateIn
            // 
            this.LateIn.Condition = "[AtteState]=11 or [AtteState] =12";
            this.LateIn.DataMember = "AttendanceAllEmployeeReport";
            // 
            // 
            // 
            this.LateIn.Formatting.BackColor = System.Drawing.Color.LightGoldenrodYellow;
            this.LateIn.Formatting.Font = new System.Drawing.Font("Times New Roman", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LateIn.Formatting.ForeColor = System.Drawing.Color.Olive;
            this.LateIn.Name = "LateIn";
            // 
            // columnEmployeeName
            // 
            this.columnEmployeeName.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "AttendanceAllEmployeeReport.EmplooyeeName")});
            this.columnEmployeeName.EvenStyleName = "EvenStyle";
            this.columnEmployeeName.Font = new System.Drawing.Font("Tahoma", 9F);
            this.columnEmployeeName.FormattingRules.Add(this.ApprovedLeave);
            this.columnEmployeeName.FormattingRules.Add(this.DayTypeLeave);
            this.columnEmployeeName.FormattingRules.Add(this.DayTypeWeeklyHoliday);
            this.columnEmployeeName.FormattingRules.Add(this.DayTypeHoliday);
            this.columnEmployeeName.FormattingRules.Add(this.DayTypeOther);
            this.columnEmployeeName.FormattingRules.Add(this.LeaveWithOutApprovalOrNoInOutTime);
            this.columnEmployeeName.FormattingRules.Add(this.OfficeInOutInTime);
            this.columnEmployeeName.FormattingRules.Add(this.LateIn);
            this.columnEmployeeName.Name = "columnEmployeeName";
            this.columnEmployeeName.OddStyleName = "OddStyle";
            this.columnEmployeeName.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 0, 0, 0, 100F);
            this.columnEmployeeName.StylePriority.UseFont = false;
            this.columnEmployeeName.StylePriority.UsePadding = false;
            this.columnEmployeeName.Text = "columnEmployeeName";
            this.columnEmployeeName.Weight = 0.65104173711585078D;
            // 
            // columnCheckInTime
            // 
            this.columnCheckInTime.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.columnCheckInTime.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "AttendanceAllEmployeeReport.InTime", "{0:H:mm:ss}")});
            this.columnCheckInTime.EvenStyleName = "EvenStyle";
            this.columnCheckInTime.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.columnCheckInTime.FormattingRules.Add(this.ApprovedLeave);
            this.columnCheckInTime.FormattingRules.Add(this.DayTypeLeave);
            this.columnCheckInTime.FormattingRules.Add(this.DayTypeWeeklyHoliday);
            this.columnCheckInTime.FormattingRules.Add(this.DayTypeHoliday);
            this.columnCheckInTime.FormattingRules.Add(this.DayTypeOther);
            this.columnCheckInTime.FormattingRules.Add(this.LeaveWithOutApprovalOrNoInOutTime);
            this.columnCheckInTime.FormattingRules.Add(this.OfficeInOutInTime);
            this.columnCheckInTime.FormattingRules.Add(this.LateIn);
            this.columnCheckInTime.Name = "columnCheckInTime";
            this.columnCheckInTime.OddStyleName = "OddStyle";
            this.columnCheckInTime.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 2, 0, 0, 100F);
            this.columnCheckInTime.StylePriority.UseBorders = false;
            this.columnCheckInTime.StylePriority.UsePadding = false;
            this.columnCheckInTime.StylePriority.UseTextAlignment = false;
            this.columnCheckInTime.Text = "columnCheckInTime";
            this.columnCheckInTime.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.columnCheckInTime.Weight = 0.40449356338124492D;
            this.columnCheckInTime.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.ClearBorderControlEvent);
            // 
            // columnCheckInRemarks
            // 
            this.columnCheckInRemarks.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.columnCheckInRemarks.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "AttendanceAllEmployeeReport.RefinedInRemarks", "{0}")});
            this.columnCheckInRemarks.EvenStyleName = "EvenStyle";
            this.columnCheckInRemarks.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.columnCheckInRemarks.FormattingRules.Add(this.ApprovedLeave);
            this.columnCheckInRemarks.FormattingRules.Add(this.DayTypeLeave);
            this.columnCheckInRemarks.FormattingRules.Add(this.DayTypeWeeklyHoliday);
            this.columnCheckInRemarks.FormattingRules.Add(this.DayTypeHoliday);
            this.columnCheckInRemarks.FormattingRules.Add(this.DayTypeOther);
            this.columnCheckInRemarks.FormattingRules.Add(this.LeaveWithOutApprovalOrNoInOutTime);
            this.columnCheckInRemarks.FormattingRules.Add(this.OfficeInOutInTime);
            this.columnCheckInRemarks.FormattingRules.Add(this.LateIn);
            this.columnCheckInRemarks.Multiline = true;
            this.columnCheckInRemarks.Name = "columnCheckInRemarks";
            this.columnCheckInRemarks.OddStyleName = "OddStyle";
            this.columnCheckInRemarks.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.columnCheckInRemarks.StylePriority.UseBorders = false;
            this.columnCheckInRemarks.StylePriority.UsePadding = false;
            this.columnCheckInRemarks.Text = "[AttendanceAllEmployeeReport.RefinedInRemarks]";
            this.columnCheckInRemarks.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.columnCheckInRemarks.Weight = 0.84365750330820455D;
            this.columnCheckInRemarks.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.ClearBorderControlEvent);
            // 
            // columnCheckOutTime
            // 
            this.columnCheckOutTime.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "AttendanceAllEmployeeReport.OutTime", "{0:H:mm:ss}")});
            this.columnCheckOutTime.EvenStyleName = "EvenStyle";
            this.columnCheckOutTime.Font = new System.Drawing.Font("Tahoma", 9F);
            this.columnCheckOutTime.FormattingRules.Add(this.ApprovedLeave);
            this.columnCheckOutTime.FormattingRules.Add(this.DayTypeLeave);
            this.columnCheckOutTime.FormattingRules.Add(this.DayTypeWeeklyHoliday);
            this.columnCheckOutTime.FormattingRules.Add(this.DayTypeHoliday);
            this.columnCheckOutTime.FormattingRules.Add(this.DayTypeOther);
            this.columnCheckOutTime.FormattingRules.Add(this.LeaveWithOutApprovalOrNoInOutTime);
            this.columnCheckOutTime.FormattingRules.Add(this.OfficeInOutInTime);
            this.columnCheckOutTime.FormattingRules.Add(this.LateIn);
            this.columnCheckOutTime.Name = "columnCheckOutTime";
            this.columnCheckOutTime.OddStyleName = "OddStyle";
            this.columnCheckOutTime.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 0, 0, 0, 100F);
            this.columnCheckOutTime.StylePriority.UseFont = false;
            this.columnCheckOutTime.StylePriority.UsePadding = false;
            this.columnCheckOutTime.Text = "columnCheckOutTime";
            this.columnCheckOutTime.Weight = 0.41605029997490073D;
            // 
            // columnCheckOutRemarks
            // 
            this.columnCheckOutRemarks.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "AttendanceAllEmployeeReport.RefinedOutRemarks")});
            this.columnCheckOutRemarks.EvenStyleName = "EvenStyle";
            this.columnCheckOutRemarks.FormattingRules.Add(this.ApprovedLeave);
            this.columnCheckOutRemarks.FormattingRules.Add(this.DayTypeLeave);
            this.columnCheckOutRemarks.FormattingRules.Add(this.DayTypeWeeklyHoliday);
            this.columnCheckOutRemarks.FormattingRules.Add(this.DayTypeHoliday);
            this.columnCheckOutRemarks.FormattingRules.Add(this.DayTypeOther);
            this.columnCheckOutRemarks.FormattingRules.Add(this.LeaveWithOutApprovalOrNoInOutTime);
            this.columnCheckOutRemarks.FormattingRules.Add(this.OfficeInOutInTime);
            this.columnCheckOutRemarks.FormattingRules.Add(this.LateIn);
            this.columnCheckOutRemarks.Multiline = true;
            this.columnCheckOutRemarks.Name = "columnCheckOutRemarks";
            this.columnCheckOutRemarks.OddStyleName = "OddStyle";
            this.columnCheckOutRemarks.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 0, 0, 0, 100F);
            this.columnCheckOutRemarks.StylePriority.UsePadding = false;
            this.columnCheckOutRemarks.Text = "columnCheckOutRemarks";
            this.columnCheckOutRemarks.Weight = 0.92455623573344448D;
            // 
            // columnWorkHour
            // 
            this.columnWorkHour.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "AttendanceAllEmployeeReport.RefinedWorkHour", "{0}")});
            this.columnWorkHour.EvenStyleName = "EvenStyle";
            this.columnWorkHour.Font = new System.Drawing.Font("Tahoma", 9F);
            this.columnWorkHour.FormattingRules.Add(this.ApprovedLeave);
            this.columnWorkHour.FormattingRules.Add(this.DayTypeLeave);
            this.columnWorkHour.FormattingRules.Add(this.DayTypeWeeklyHoliday);
            this.columnWorkHour.FormattingRules.Add(this.DayTypeHoliday);
            this.columnWorkHour.FormattingRules.Add(this.DayTypeOther);
            this.columnWorkHour.FormattingRules.Add(this.LeaveWithOutApprovalOrNoInOutTime);
            this.columnWorkHour.FormattingRules.Add(this.OfficeInOutInTime);
            this.columnWorkHour.FormattingRules.Add(this.LateIn);
            this.columnWorkHour.Name = "columnWorkHour";
            this.columnWorkHour.OddStyleName = "OddStyle";
            this.columnWorkHour.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 0, 0, 0, 100F);
            this.columnWorkHour.StylePriority.UseFont = false;
            this.columnWorkHour.StylePriority.UsePadding = false;
            this.columnWorkHour.Text = "columnWorkHour";
            this.columnWorkHour.Weight = 0.462278109049467D;
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "AttendanceAllEmployeeReport.LeaveName")});
            this.xrTableCell7.EvenStyleName = "EvenStyle";
            this.xrTableCell7.Font = new System.Drawing.Font("Tahoma", 9F);
            this.xrTableCell7.FormattingRules.Add(this.ApprovedLeave);
            this.xrTableCell7.FormattingRules.Add(this.DayTypeLeave);
            this.xrTableCell7.FormattingRules.Add(this.DayTypeWeeklyHoliday);
            this.xrTableCell7.FormattingRules.Add(this.DayTypeHoliday);
            this.xrTableCell7.FormattingRules.Add(this.DayTypeOther);
            this.xrTableCell7.FormattingRules.Add(this.LeaveWithOutApprovalOrNoInOutTime);
            this.xrTableCell7.FormattingRules.Add(this.OfficeInOutInTime);
            this.xrTableCell7.FormattingRules.Add(this.LateIn);
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.OddStyleName = "OddStyle";
            this.xrTableCell7.StylePriority.UseFont = false;
            this.xrTableCell7.Text = "xrTableCell7";
            this.xrTableCell7.Weight = 0.53604961522929162D;
            // 
            // Title
            // 
            this.Title.BackColor = System.Drawing.Color.White;
            this.Title.BorderColor = System.Drawing.SystemColors.ControlText;
            this.Title.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.Title.BorderWidth = 1;
            this.Title.Font = new System.Drawing.Font("Times New Roman", 21F);
            this.Title.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(84)))), ((int)(((byte)(132)))), ((int)(((byte)(213)))));
            this.Title.Name = "Title";
            // 
            // pageHeaderBand1
            // 
            this.pageHeaderBand1.BackColor = System.Drawing.Color.White;
            this.pageHeaderBand1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable2});
            this.pageHeaderBand1.HeightF = 25F;
            this.pageHeaderBand1.Name = "pageHeaderBand1";
            this.pageHeaderBand1.StylePriority.UseBackColor = false;
            // 
            // xrTable2
            // 
            this.xrTable2.BackColor = System.Drawing.Color.Transparent;
            this.xrTable2.BorderColor = System.Drawing.Color.Transparent;
            this.xrTable2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTable2.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold);
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(1.041667F, 0F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow4});
            this.xrTable2.SizeF = new System.Drawing.SizeF(945.9583F, 25F);
            this.xrTable2.StylePriority.UseBackColor = false;
            this.xrTable2.StylePriority.UseBorderColor = false;
            this.xrTable2.StylePriority.UseBorders = false;
            this.xrTable2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.BackColor = System.Drawing.Color.Transparent;
            this.xrTableRow4.BorderColor = System.Drawing.Color.Transparent;
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell8,
            this.tableHeaderEmployeeName,
            this.tableHeaderCheckInTime,
            this.tableHeaderCheckInRemarks,
            this.tableHeaderCheckOutTime,
            this.tableHeaderCheckOutRemarks,
            this.tableHeaderWorkingHour,
            this.xrTableCell3});
            this.xrTableRow4.ForeColor = System.Drawing.SystemColors.ControlText;
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.StylePriority.UseBackColor = false;
            this.xrTableRow4.StylePriority.UseBorderColor = false;
            this.xrTableRow4.StylePriority.UseForeColor = false;
            this.xrTableRow4.Weight = 1D;
            // 
            // xrTableCell8
            // 
            this.xrTableCell8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(201)))), ((int)(((byte)(214)))), ((int)(((byte)(237)))));
            this.xrTableCell8.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(190)))), ((int)(((byte)(216)))));
            this.xrTableCell8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.xrTableCell8.Name = "xrTableCell8";
            this.xrTableCell8.StylePriority.UseBackColor = false;
            this.xrTableCell8.StylePriority.UseBorderColor = false;
            this.xrTableCell8.StylePriority.UseFont = false;
            this.xrTableCell8.StylePriority.UseTextAlignment = false;
            this.xrTableCell8.Text = "SN";
            this.xrTableCell8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell8.Weight = 0.12342039780435093D;
            // 
            // tableHeaderEmployeeName
            // 
            this.tableHeaderEmployeeName.Name = "tableHeaderEmployeeName";
            this.tableHeaderEmployeeName.StyleName = "AttributeHeaderStyle";
            this.tableHeaderEmployeeName.Text = "Name";
            this.tableHeaderEmployeeName.Weight = 0.59594476451476952D;
            // 
            // tableHeaderCheckInTime
            // 
            this.tableHeaderCheckInTime.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(199)))), ((int)(((byte)(209)))), ((int)(((byte)(228)))));
            this.tableHeaderCheckInTime.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(190)))), ((int)(((byte)(216)))));
            this.tableHeaderCheckInTime.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.tableHeaderCheckInTime.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.tableHeaderCheckInTime.Name = "tableHeaderCheckInTime";
            this.tableHeaderCheckInTime.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.tableHeaderCheckInTime.Scripts.OnBeforePrint = "xrTableCell5_BeforePrint";
            this.tableHeaderCheckInTime.StyleName = "AttributeHeaderStyle";
            this.tableHeaderCheckInTime.StylePriority.UseBorders = false;
            this.tableHeaderCheckInTime.StylePriority.UsePadding = false;
            this.tableHeaderCheckInTime.StylePriority.UseTextAlignment = false;
            this.tableHeaderCheckInTime.Text = "In Time";
            this.tableHeaderCheckInTime.Weight = 0.37026157959779138D;
            this.tableHeaderCheckInTime.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.ClearBorderControlEvent);
            // 
            // tableHeaderCheckInRemarks
            // 
            this.tableHeaderCheckInRemarks.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(199)))), ((int)(((byte)(209)))), ((int)(((byte)(228)))));
            this.tableHeaderCheckInRemarks.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(190)))), ((int)(((byte)(216)))));
            this.tableHeaderCheckInRemarks.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.tableHeaderCheckInRemarks.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.tableHeaderCheckInRemarks.Name = "tableHeaderCheckInRemarks";
            this.tableHeaderCheckInRemarks.Scripts.OnBeforePrint = "xrTableCell10_BeforePrint";
            this.tableHeaderCheckInRemarks.StyleName = "AttributeHeaderStyle";
            this.tableHeaderCheckInRemarks.StylePriority.UseBorders = false;
            this.tableHeaderCheckInRemarks.Text = "In Remarks";
            this.tableHeaderCheckInRemarks.Weight = 0.77225975180899875D;
            this.tableHeaderCheckInRemarks.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.ClearBorderControlEvent);
            // 
            // tableHeaderCheckOutTime
            // 
            this.tableHeaderCheckOutTime.Name = "tableHeaderCheckOutTime";
            this.tableHeaderCheckOutTime.StyleName = "AttributeHeaderStyle";
            this.tableHeaderCheckOutTime.Text = "Out Time";
            this.tableHeaderCheckOutTime.Weight = 0.38084041383761424D;
            // 
            // tableHeaderCheckOutRemarks
            // 
            this.tableHeaderCheckOutRemarks.Name = "tableHeaderCheckOutRemarks";
            this.tableHeaderCheckOutRemarks.StyleName = "AttributeHeaderStyle";
            this.tableHeaderCheckOutRemarks.Text = "Out Remarks";
            this.tableHeaderCheckOutRemarks.Weight = 0.846312021126969D;
            // 
            // tableHeaderWorkingHour
            // 
            this.tableHeaderWorkingHour.Name = "tableHeaderWorkingHour";
            this.tableHeaderWorkingHour.StyleName = "AttributeHeaderStyle";
            this.tableHeaderWorkingHour.Text = "Work Hour";
            this.tableHeaderWorkingHour.Weight = 0.42315601863454494D;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.StyleName = "AttributeHeaderStyle";
            this.xrTableCell3.Text = "Leave";
            this.xrTableCell3.Weight = 0.49068454755279345D;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1});
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Weight = 1D;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.Text = "xrTableCell1";
            this.xrTableCell1.Weight = 3D;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell6});
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Weight = 1D;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.Text = "xrTableCell6";
            this.xrTableCell6.Weight = 3D;
            // 
            // reportHeaderBand1
            // 
            this.reportHeaderBand1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.labelTitle,
            this.xrTable3});
            this.reportHeaderBand1.HeightF = 182.0434F;
            this.reportHeaderBand1.Name = "reportHeaderBand1";
            // 
            // labelTitle
            // 
            this.labelTitle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(227)))), ((int)(((byte)(214)))));
            this.labelTitle.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTitle.ForeColor = System.Drawing.Color.Black;
            this.labelTitle.LocationFloat = new DevExpress.Utils.PointFloat(0F, 152.46F);
            this.labelTitle.Name = "labelTitle";
            this.labelTitle.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.labelTitle.Scripts.OnBeforePrint = "xrLabel1_BeforePrint";
            this.labelTitle.SizeF = new System.Drawing.SizeF(947F, 29.58333F);
            this.labelTitle.StyleName = "ReportHeaderStyle";
            this.labelTitle.StylePriority.UseBackColor = false;
            this.labelTitle.StylePriority.UseForeColor = false;
            this.labelTitle.StylePriority.UseTextAlignment = false;
            this.labelTitle.Text = "Daily Attendance Report of ";
            this.labelTitle.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.labelTitle.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.ClearBorderControlEvent);
            // 
            // topMarginBand1
            // 
            this.topMarginBand1.HeightF = 50F;
            this.topMarginBand1.Name = "topMarginBand1";
            // 
            // bottomMarginBand1
            // 
            this.bottomMarginBand1.HeightF = 50F;
            this.bottomMarginBand1.Name = "bottomMarginBand1";
            // 
            // HesderStyle
            // 
            this.HesderStyle.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(32)))), ((int)(((byte)(31)))));
            this.HesderStyle.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HesderStyle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(84)))), ((int)(((byte)(132)))), ((int)(((byte)(213)))));
            this.HesderStyle.Name = "HesderStyle";
            // 
            // AttendanceStyle
            // 
            this.AttendanceStyle.Name = "AttendanceStyle";
            // 
            // HeaderStyle
            // 
            this.HeaderStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(84)))), ((int)(((byte)(132)))), ((int)(((byte)(213)))));
            this.HeaderStyle.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.HeaderStyle.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.HeaderStyle.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HeaderStyle.ForeColor = System.Drawing.Color.White;
            this.HeaderStyle.Name = "HeaderStyle";
            this.HeaderStyle.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.HeaderStyle.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // PayBankEven
            // 
            this.PayBankEven.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(243)))), ((int)(((byte)(250)))));
            this.PayBankEven.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(199)))), ((int)(((byte)(209)))), ((int)(((byte)(228)))));
            this.PayBankEven.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.PayBankEven.Font = new System.Drawing.Font("Tahoma", 9F);
            this.PayBankEven.Name = "PayBankEven";
            this.PayBankEven.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            // 
            // PayBankOdd
            // 
            this.PayBankOdd.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(199)))), ((int)(((byte)(209)))), ((int)(((byte)(228)))));
            this.PayBankOdd.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.PayBankOdd.Font = new System.Drawing.Font("Tahoma", 9F);
            this.PayBankOdd.Name = "PayBankOdd";
            this.PayBankOdd.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            // 
            // PayCashEven
            // 
            this.PayCashEven.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(243)))), ((int)(((byte)(250)))));
            this.PayCashEven.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(199)))), ((int)(((byte)(209)))), ((int)(((byte)(228)))));
            this.PayCashEven.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.PayCashEven.Font = new System.Drawing.Font("Tahoma", 9F);
            this.PayCashEven.Name = "PayCashEven";
            this.PayCashEven.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            // 
            // PayCashOdd
            // 
            this.PayCashOdd.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(199)))), ((int)(((byte)(209)))), ((int)(((byte)(228)))));
            this.PayCashOdd.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.PayCashOdd.Font = new System.Drawing.Font("Tahoma", 9F);
            this.PayCashOdd.Name = "PayCashOdd";
            this.PayCashOdd.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            // 
            // ReportHeaderStyle
            // 
            this.ReportHeaderStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(227)))), ((int)(((byte)(214)))));
            this.ReportHeaderStyle.BorderColor = System.Drawing.Color.Transparent;
            this.ReportHeaderStyle.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ReportHeaderStyle.ForeColor = System.Drawing.Color.Black;
            this.ReportHeaderStyle.Name = "ReportHeaderStyle";
            // 
            // GrossTotalStyle
            // 
            this.GrossTotalStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.GrossTotalStyle.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(199)))), ((int)(((byte)(209)))), ((int)(((byte)(228)))));
            this.GrossTotalStyle.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.GrossTotalStyle.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GrossTotalStyle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(32)))), ((int)(((byte)(32)))));
            this.GrossTotalStyle.Name = "GrossTotalStyle";
            this.GrossTotalStyle.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.GrossTotalStyle.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // advanceEven
            // 
            this.advanceEven.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.advanceEven.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(199)))), ((int)(((byte)(209)))), ((int)(((byte)(228)))));
            this.advanceEven.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.advanceEven.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.advanceEven.ForeColor = System.Drawing.SystemColors.ControlText;
            this.advanceEven.Name = "advanceEven";
            this.advanceEven.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.advanceEven.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // advanceOdd
            // 
            this.advanceOdd.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(237)))), ((int)(((byte)(196)))));
            this.advanceOdd.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(199)))), ((int)(((byte)(209)))), ((int)(((byte)(228)))));
            this.advanceOdd.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.advanceOdd.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.advanceOdd.ForeColor = System.Drawing.SystemColors.ControlText;
            this.advanceOdd.Name = "advanceOdd";
            this.advanceOdd.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.advanceOdd.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrControlStyle1
            // 
            this.xrControlStyle1.Name = "xrControlStyle1";
            this.xrControlStyle1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            // 
            // TestStyle
            // 
            this.TestStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(84)))), ((int)(((byte)(132)))), ((int)(((byte)(213)))));
            this.TestStyle.BorderColor = System.Drawing.Color.White;
            this.TestStyle.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.TestStyle.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TestStyle.ForeColor = System.Drawing.Color.White;
            this.TestStyle.Name = "TestStyle";
            this.TestStyle.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TestStyle.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // DataStyle
            // 
            this.DataStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(220)))), ((int)(((byte)(225)))));
            this.DataStyle.BorderColor = System.Drawing.Color.White;
            this.DataStyle.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.DataStyle.ForeColor = System.Drawing.Color.White;
            this.DataStyle.Name = "DataStyle";
            this.DataStyle.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // LineStyle
            // 
            this.LineStyle.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(84)))), ((int)(((byte)(132)))), ((int)(((byte)(213)))));
            this.LineStyle.Name = "LineStyle";
            // 
            // ReportStyle
            // 
            this.ReportStyle.Font = new System.Drawing.Font("Times New Roman", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ReportStyle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(155)))), ((int)(((byte)(24)))));
            this.ReportStyle.Name = "ReportStyle";
            this.ReportStyle.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // StyleDate
            // 
            this.StyleDate.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(84)))), ((int)(((byte)(132)))), ((int)(((byte)(213)))));
            this.StyleDate.Name = "StyleDate";
            this.StyleDate.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.StyleDate.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // AttendanceHeaderStyle
            // 
            this.AttendanceHeaderStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(84)))), ((int)(((byte)(132)))), ((int)(((byte)(213)))));
            this.AttendanceHeaderStyle.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.AttendanceHeaderStyle.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.AttendanceHeaderStyle.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AttendanceHeaderStyle.ForeColor = System.Drawing.Color.White;
            this.AttendanceHeaderStyle.Name = "AttendanceHeaderStyle";
            this.AttendanceHeaderStyle.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.AttendanceHeaderStyle.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrControlStyle2
            // 
            this.xrControlStyle2.Name = "xrControlStyle2";
            this.xrControlStyle2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            // 
            // EvenStyle
            // 
            this.EvenStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(243)))), ((int)(((byte)(250)))));
            this.EvenStyle.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(190)))), ((int)(((byte)(216)))));
            this.EvenStyle.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.EvenStyle.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.EvenStyle.Name = "EvenStyle";
            this.EvenStyle.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // OddStyle
            // 
            this.OddStyle.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(190)))), ((int)(((byte)(216)))));
            this.OddStyle.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.OddStyle.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OddStyle.Name = "OddStyle";
            this.OddStyle.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.OddStyle.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // AttributeHeaderStyle
            // 
            this.AttributeHeaderStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(201)))), ((int)(((byte)(214)))), ((int)(((byte)(237)))));
            this.AttributeHeaderStyle.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(190)))), ((int)(((byte)(216)))));
            this.AttributeHeaderStyle.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.AttributeHeaderStyle.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AttributeHeaderStyle.Name = "AttributeHeaderStyle";
            this.AttributeHeaderStyle.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.AttributeHeaderStyle.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrControlStyle3
            // 
            this.xrControlStyle3.Name = "xrControlStyle3";
            // 
            // reportDataSet1
            // 
            this.reportDataSet1.DataSetName = "ReportDataSet";
            this.reportDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // attendanceAllEmployeeReportTableAdapter1
            // 
            this.attendanceAllEmployeeReportTableAdapter1.ClearBeforeFill = true;
            // 
            // report_HR_GetAttendanceDaysTableAdapter
            // 
            this.report_HR_GetAttendanceDaysTableAdapter.ClearBeforeFill = true;
            // 
            // ReportFooter
            // 
            this.ReportFooter.HeightF = 229.667F;
            this.ReportFooter.Name = "ReportFooter";
            // 
            // xrTable3
            // 
            this.xrTable3.LocationFloat = new DevExpress.Utils.PointFloat(689.5F, 0F);
            this.xrTable3.Name = "xrTable3";
            this.xrTable3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow5,
            this.xrTableRow6,
            this.xrTableRow7,
            this.xrTableRow8,
            this.xrTableRow10});
            this.xrTable3.SizeF = new System.Drawing.SizeF(257.5F, 143.6458F);
            // 
            // xrTableRow5
            // 
            this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell17,
            this.lblTotal});
            this.xrTableRow5.Name = "xrTableRow5";
            this.xrTableRow5.Weight = 1D;
            // 
            // xrTableCell17
            // 
            this.xrTableCell17.Name = "xrTableCell17";
            this.xrTableCell17.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.xrTableCell17.StylePriority.UseBorderColor = false;
            this.xrTableCell17.StylePriority.UsePadding = false;
            this.xrTableCell17.StylePriority.UseTextAlignment = false;
            this.xrTableCell17.Text = "Total Attendees";
            this.xrTableCell17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell17.Weight = 0.55920084102228451D;
            // 
            // lblTotal
            // 
            this.lblTotal.Font = new System.Drawing.Font("Times New Roman", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 100F);
            this.lblTotal.StylePriority.UseFont = false;
            this.lblTotal.StylePriority.UsePadding = false;
            this.lblTotal.StylePriority.UseTextAlignment = false;
            this.lblTotal.Text = "lblTotal";
            this.lblTotal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblTotal.Weight = 0.28782393645650095D;
            // 
            // xrTableRow6
            // 
            this.xrTableRow6.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell18,
            this.lblTimely});
            this.xrTableRow6.Name = "xrTableRow6";
            this.xrTableRow6.Weight = 1D;
            // 
            // xrTableCell18
            // 
            this.xrTableCell18.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.xrTableCell18.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(125)))), ((int)(((byte)(149)))));
            this.xrTableCell18.Name = "xrTableCell18";
            this.xrTableCell18.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.xrTableCell18.StylePriority.UseBackColor = false;
            this.xrTableCell18.StylePriority.UseForeColor = false;
            this.xrTableCell18.StylePriority.UsePadding = false;
            this.xrTableCell18.StylePriority.UseTextAlignment = false;
            this.xrTableCell18.Text = "Timely Attendees";
            this.xrTableCell18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell18.Weight = 0.55920100538022255D;
            // 
            // lblTimely
            // 
            this.lblTimely.Font = new System.Drawing.Font("Times New Roman", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTimely.Name = "lblTimely";
            this.lblTimely.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 100F);
            this.lblTimely.StylePriority.UseFont = false;
            this.lblTimely.StylePriority.UsePadding = false;
            this.lblTimely.StylePriority.UseTextAlignment = false;
            this.lblTimely.Text = "lblTimely";
            this.lblTimely.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblTimely.Weight = 0.28782402105249089D;
            // 
            // xrTableRow7
            // 
            this.xrTableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell19,
            this.lblLate});
            this.xrTableRow7.Name = "xrTableRow7";
            this.xrTableRow7.Weight = 1D;
            // 
            // xrTableCell19
            // 
            this.xrTableCell19.BackColor = System.Drawing.Color.LightGoldenrodYellow;
            this.xrTableCell19.ForeColor = System.Drawing.Color.Olive;
            this.xrTableCell19.Name = "xrTableCell19";
            this.xrTableCell19.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.xrTableCell19.StylePriority.UseBackColor = false;
            this.xrTableCell19.StylePriority.UseForeColor = false;
            this.xrTableCell19.StylePriority.UsePadding = false;
            this.xrTableCell19.StylePriority.UseTextAlignment = false;
            this.xrTableCell19.Text = "Late Attendees";
            this.xrTableCell19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell19.Weight = 1.213427353966914D;
            // 
            // lblLate
            // 
            this.lblLate.Font = new System.Drawing.Font("Times New Roman", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLate.Name = "lblLate";
            this.lblLate.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 100F);
            this.lblLate.StylePriority.UseFont = false;
            this.lblLate.StylePriority.UsePadding = false;
            this.lblLate.StylePriority.UseTextAlignment = false;
            this.lblLate.Text = "lblLate";
            this.lblLate.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblLate.Weight = 0.62455797586280082D;
            // 
            // xrTableRow8
            // 
            this.xrTableRow8.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell20,
            this.lblApproved});
            this.xrTableRow8.Name = "xrTableRow8";
            this.xrTableRow8.Weight = 1D;
            // 
            // xrTableCell20
            // 
            this.xrTableCell20.BackColor = System.Drawing.Color.LavenderBlush;
            this.xrTableCell20.Name = "xrTableCell20";
            this.xrTableCell20.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.xrTableCell20.StylePriority.UseBackColor = false;
            this.xrTableCell20.StylePriority.UsePadding = false;
            this.xrTableCell20.StylePriority.UseTextAlignment = false;
            this.xrTableCell20.Text = "Approved Leaves";
            this.xrTableCell20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell20.Weight = 1.7726281814049956D;
            // 
            // lblApproved
            // 
            this.lblApproved.Font = new System.Drawing.Font("Times New Roman", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblApproved.Name = "lblApproved";
            this.lblApproved.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 100F);
            this.lblApproved.StylePriority.UseFont = false;
            this.lblApproved.StylePriority.UsePadding = false;
            this.lblApproved.StylePriority.UseTextAlignment = false;
            this.lblApproved.Text = "lblApproved";
            this.lblApproved.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblApproved.Weight = 0.91238217485743267D;
            // 
            // xrTableRow10
            // 
            this.xrTableRow10.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell21,
            this.lblAbsentee});
            this.xrTableRow10.Name = "xrTableRow10";
            this.xrTableRow10.Weight = 1D;
            // 
            // xrTableCell21
            // 
            this.xrTableCell21.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.xrTableCell21.ForeColor = System.Drawing.Color.White;
            this.xrTableCell21.Name = "xrTableCell21";
            this.xrTableCell21.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.xrTableCell21.StylePriority.UseBackColor = false;
            this.xrTableCell21.StylePriority.UseForeColor = false;
            this.xrTableCell21.StylePriority.UsePadding = false;
            this.xrTableCell21.StylePriority.UseTextAlignment = false;
            this.xrTableCell21.Text = "Absentees";
            this.xrTableCell21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell21.Weight = 1.7726281814049956D;
            // 
            // lblAbsentee
            // 
            this.lblAbsentee.Font = new System.Drawing.Font("Times New Roman", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAbsentee.Name = "lblAbsentee";
            this.lblAbsentee.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 100F);
            this.lblAbsentee.StylePriority.UseFont = false;
            this.lblAbsentee.StylePriority.UsePadding = false;
            this.lblAbsentee.StylePriority.UseTextAlignment = false;
            this.lblAbsentee.Text = "lblAbsentee";
            this.lblAbsentee.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblAbsentee.Weight = 0.91238217485743267D;
            // 
            // ReportAttendnaceEmpDailyDetails
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.pageHeaderBand1,
            this.reportHeaderBand1,
            this.topMarginBand1,
            this.bottomMarginBand1,
            this.ReportFooter});
            this.DataAdapter = this.attendanceAllEmployeeReportTableAdapter1;
            this.DataMember = "Report_HR_GetAttendanceDays";
            this.DataSource = this.reportDataSet1;
            this.FormattingRuleSheet.AddRange(new DevExpress.XtraReports.UI.FormattingRule[] {
            this.DayTypeLeave,
            this.DayTypeWeeklyHoliday,
            this.DayTypeHoliday,
            this.DayTypeOther,
            this.ApprovedLeave,
            this.LeaveWithOutApprovalOrNoInOutTime,
            this.OfficeInOutInTime,
            this.LateIn});
            this.Margins = new System.Drawing.Printing.Margins(26, 27, 50, 50);
            this.PageHeight = 1169;
            this.PageWidth = 1000;
            this.PaperKind = System.Drawing.Printing.PaperKind.Custom;
            this.ReportPrintOptions.DetailCountOnEmptyDataSource = 0;
            this.Scripts.OnBeforePrint = "ReportAttendnaceDays_BeforePrint";
            this.StyleSheet.AddRange(new DevExpress.XtraReports.UI.XRControlStyle[] {
            this.Title,
            this.HesderStyle,
            this.AttendanceStyle,
            this.HeaderStyle,
            this.PayBankEven,
            this.PayBankOdd,
            this.PayCashEven,
            this.PayCashOdd,
            this.ReportHeaderStyle,
            this.GrossTotalStyle,
            this.advanceEven,
            this.advanceOdd,
            this.xrControlStyle1,
            this.TestStyle,
            this.DataStyle,
            this.LineStyle,
            this.ReportStyle,
            this.StyleDate,
            this.AttendanceHeaderStyle,
            this.xrControlStyle2,
            this.EvenStyle,
            this.OddStyle,
            this.AttributeHeaderStyle,
            this.xrControlStyle3});
            this.Version = "12.1";
            this.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.ClearBorderControlEvent);
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportDataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.XRControlStyle Title;
        private DevExpress.XtraReports.UI.PageHeaderBand pageHeaderBand1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell6;
        private DevExpress.XtraReports.UI.ReportHeaderBand reportHeaderBand1;
        private DevExpress.XtraReports.UI.TopMarginBand topMarginBand1;
        private DevExpress.XtraReports.UI.BottomMarginBand bottomMarginBand1;
        private DevExpress.XtraReports.UI.XRTable xrTable1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell9;
        private DevExpress.XtraReports.UI.XRTable xrTable2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell10;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell2;
        public DevExpress.XtraReports.UI.XRTableCell tableHeaderPresentDays;
        private DevExpress.XtraReports.UI.XRControlStyle HesderStyle;
        private DevExpress.XtraReports.UI.XRControlStyle AttendanceStyle;
        private DevExpress.XtraReports.UI.XRControlStyle HeaderStyle;
        private ReportCompanyInfo reportCompanyInfo1;
        private DevExpress.XtraReports.UI.XRControlStyle PayBankEven;
        private DevExpress.XtraReports.UI.XRControlStyle PayBankOdd;
        private DevExpress.XtraReports.UI.XRControlStyle PayCashEven;
        private DevExpress.XtraReports.UI.XRControlStyle PayCashOdd;
        private DevExpress.XtraReports.UI.XRControlStyle ReportHeaderStyle;
        private DevExpress.XtraReports.UI.XRControlStyle GrossTotalStyle;
        private DevExpress.XtraReports.UI.XRControlStyle advanceEven;
        private DevExpress.XtraReports.UI.XRControlStyle advanceOdd;
        private DevExpress.XtraReports.UI.XRControlStyle xrControlStyle1;
        private DevExpress.XtraReports.UI.XRControlStyle TestStyle;
        private DevExpress.XtraReports.UI.XRControlStyle DataStyle;
        private DevExpress.XtraReports.UI.XRControlStyle LineStyle;
        private DevExpress.XtraReports.UI.XRControlStyle ReportStyle;
        private DevExpress.XtraReports.UI.XRControlStyle StyleDate;
        private DevExpress.XtraReports.UI.XRControlStyle AttendanceHeaderStyle;
        private DevExpress.XtraReports.UI.XRControlStyle xrControlStyle2;
        private DevExpress.XtraReports.UI.XRControlStyle EvenStyle;
        private DevExpress.XtraReports.UI.XRControlStyle OddStyle;
        private DevExpress.XtraReports.UI.XRControlStyle AttributeHeaderStyle;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell11;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell13;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell15;
        public DevExpress.XtraReports.UI.FormattingRule DayTypeLeave;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell12;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell14;
        private DevExpress.XtraReports.UI.XRControlStyle xrControlStyle3;
        private DevExpress.XtraReports.UI.FormattingRule DayTypeWeeklyHoliday;
        private DevExpress.XtraReports.UI.FormattingRule DayTypeHoliday;
        private DevExpress.XtraReports.UI.FormattingRule DayTypeOther;

        private DevExpress.XtraReports.UI.XRTableCell columnCheckInTime;
        private DevExpress.XtraReports.UI.XRTableCell columnCheckInRemarks;
        private DevExpress.XtraReports.UI.XRTableCell columnCheckOutTime;
        private DevExpress.XtraReports.UI.XRTableCell columnCheckOutRemarks;
        private DevExpress.XtraReports.UI.XRTableCell columnWorkHour;
        private DevExpress.XtraReports.UI.XRTableCell tableHeaderCheckInTime;
        private DevExpress.XtraReports.UI.XRTableCell tableHeaderCheckInRemarks;
        private DevExpress.XtraReports.UI.XRTableCell tableHeaderCheckOutTime;
        private DevExpress.XtraReports.UI.XRTableCell tableHeaderCheckOutRemarks;
        private DevExpress.XtraReports.UI.XRTableCell tableHeaderWorkingHour;
        private DevExpress.XtraReports.UI.XRTableCell columnEmployeeName;
        private DevExpress.XtraReports.UI.XRTableCell tableHeaderEmployeeName;
        private ReportDataSet reportDataSet1;
        private ReportDataSetTableAdapters.AttendanceAllEmployeeReportTableAdapter attendanceAllEmployeeReportTableAdapter1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell7;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell3;
        private ReportDataSetTableAdapters.Report_HR_GetAttendanceDaysTableAdapter report_HR_GetAttendanceDaysTableAdapter;
        private DevExpress.XtraReports.UI.FormattingRule ApprovedLeave;
        public DevExpress.XtraReports.UI.XRLabel labelTitle;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell16;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell8;
        private DevExpress.XtraReports.UI.FormattingRule LeaveWithOutApprovalOrNoInOutTime;
        private DevExpress.XtraReports.UI.FormattingRule OfficeInOutInTime;
        private DevExpress.XtraReports.UI.FormattingRule LateIn;
        private DevExpress.XtraReports.UI.ReportFooterBand ReportFooter;
        private DevExpress.XtraReports.UI.XRTable xrTable3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell17;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow6;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell18;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow7;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell19;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow8;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell20;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow10;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell21;
        public DevExpress.XtraReports.UI.XRTableCell lblTotal;
        public DevExpress.XtraReports.UI.XRTableCell lblTimely;
        public DevExpress.XtraReports.UI.XRTableCell lblLate;
        public DevExpress.XtraReports.UI.XRTableCell lblApproved;
        public DevExpress.XtraReports.UI.XRTableCell lblAbsentee;
    }
}
