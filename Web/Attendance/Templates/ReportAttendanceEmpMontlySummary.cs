using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Web.ReportDataSetTableAdapters;
using BLL;
using DevExpress.XtraTreeList.StyleFormatConditions;
using DevExpress.XtraGrid;

namespace Web.CP.Report.Templates.HR
{
    public partial class ReportAttendanceEmpMontlySummary : DevExpress.XtraReports.UI.XtraReport
    {

        public ReportAttendanceEmpMontlySummary()
        {
            InitializeComponent();


            //FormattingRule formatingRule = new FormattingRule();
            //formatingRule.Condition = "[Value] == 'SL'";
            //formatingRule.Formatting.BackColor = Color.WhiteSmoke;
            //formatingRule.Formatting.ForeColor = Color.IndianRed;

        }


        public DevExpress.XtraReports.UI.XRLabel XRTitle
        {
            get
            {
                return xrLabel1;
            }
        }      

        public void SetSubReport(object sender)
        {
            XRSubreport subReport = (XRSubreport)sender;

            if (subReport.Name.Equals("subReportCompanyInfo"))
            {
                Web.CP.Report.Templates.HR.ReportCompanyInfo companyInfoReport =
                    (Web.CP.Report.Templates.HR.ReportCompanyInfo)subReport.ReportSource;

                ReportDataSet.Report_Company_InfoDataTable compayInfoTable = ReportHelper.GetCompanyDataTable();

                companyInfoReport.DataSource = compayInfoTable;
                companyInfoReport.DataMember = "Company Information";
            }
           ReportHelper.BindCompanyInfo(sender);
        
        }


        public void HideColumns()
        {
            xrTableCell44.WidthF = 0;
            xrTableCell69.WidthF = 0;
            xrTableCell44.Visible = false;
            xrTableCell69.Visible = false;
            
        }

        public void HideColumn32()
        {
            xrTableCell42.WidthF = 0;
            xrTableCell67.WidthF = 0;
            xrTableCell42.Visible = false;
            xrTableCell67.Visible = false;
        }
        public void HideColumn31()
        {
            xrTableCell41.WidthF = 0;
            xrTableCell70.WidthF = 0;
            xrTableCell41.Visible = false;
            xrTableCell70.Visible = false;
        }
        public void HideColumn30()
        {
            xrTableCell39.WidthF = 0;
            xrTableCell68.WidthF = 0;
            xrTableCell39.Visible = false;
            xrTableCell68.Visible = false;
        }
        public void HideColumn29()
        {
            xrTableCell40.WidthF = 0;
            xrTableCell61.WidthF = 0;
            xrTableCell40.Visible = false;
            xrTableCell61.Visible = false;
        }


        protected void ClearBorderControlEvent(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            //xrLabel2.Text = SessionManager.CurrentCompany.Name;
            ReportHelper.TableCell_BeforePrintEvent(sender, e);

            XRTableCell valueCell = sender as XRTableCell;

            if (valueCell != null)
            {
                valueCell.BackColor = ReportHelper.GetAttendanceCellColor(valueCell.Text.Trim());
            }

        }


        protected void ClearBorderLessControlEvent(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            ReportHelper.TableCell_BeforePrintEvent(sender, e);
        }
       
    }
}

