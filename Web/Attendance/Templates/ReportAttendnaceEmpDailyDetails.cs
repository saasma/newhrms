using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Web.ReportDataSetTableAdapters;
using BLL;

namespace Web.CP.Report.Templates.HR
{
    public partial class ReportAttendnaceEmpDailyDetails : DevExpress.XtraReports.UI.XtraReport
    {

        public ReportAttendnaceEmpDailyDetails()
        {
            InitializeComponent();
        }

        public DevExpress.XtraReports.UI.XRLabel XRTitle
        {
            get
            {
                return labelTitle;
            }
        }      


        public void SetSubReport(object sender)
        {
            XRSubreport subReport = (XRSubreport)sender;

            if (subReport.Name.Equals("subReportCompanyInfo"))
            {
                Web.CP.Report.Templates.HR.ReportCompanyInfo companyInfoReport =  (Web.CP.Report.Templates.HR.ReportCompanyInfo)subReport.ReportSource;

                ReportDataSet.Report_Company_InfoDataTable compayInfoTable = ReportHelper.GetCompanyDataTable();

                companyInfoReport.DataSource = compayInfoTable;
                companyInfoReport.DataMember = "Company Information";
            }
           ReportHelper.BindCompanyInfo(sender);
        }

        
        protected void ClearBorderControlEvent(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            ReportHelper.TableCell_BeforePrintEvent(sender, e);
        }
        protected void ClearBorderLessControlEvent(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            ReportHelper.TableCell_BeforePrintEvent(sender, e);
        }
       

    }
}