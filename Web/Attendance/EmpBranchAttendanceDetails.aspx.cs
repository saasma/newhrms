﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using System.Text;
using BLL.Manager;
using Utils.Helper;
using DAL;
using System.Drawing;
using BLL.Base;
using Ext.Net;

namespace Web.Attendance
{
    public partial class EmpBranchAttendanceDetails : BasePage
    {
        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.LeaveAndAttendance;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Initialise();
            }
        }

        void Initialise()
        {
            cmbBranch.Store[0].DataSource = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId);
            cmbBranch.Store[0].DataBind();

            if (!string.IsNullOrEmpty(Request.QueryString["BranchId"]))
            {
                ExtControlHelper.ComboBoxSetSelected(Request.QueryString["BranchId"], cmbBranch);

                cmbDepartment.Store[0].DataSource = DepartmentManager.GetAllDepartmentsByBranch(int.Parse(Request.QueryString["BranchId"]));
                cmbDepartment.Store[0].DataBind();

                txtDate.Text = Request.QueryString["Date"];
            }
        }

        protected void cmbBranch_Select(object sender, DirectEventArgs e)
        {
            cmbDepartment.Clear();

            if (cmbBranch.SelectedItem == null || cmbBranch.SelectedItem.Value == null)
                return;


            cmbDepartment.Store[0].DataSource = DepartmentManager.GetAllDepartmentsByBranch(int.Parse(cmbBranch.SelectedItem.Value));
            cmbDepartment.Store[0].DataBind();
        }

    }
}