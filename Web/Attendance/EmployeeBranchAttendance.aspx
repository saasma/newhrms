﻿<%@ Page Title="Branch Attendance Card" Language="C#" MasterPageFile="~/Master/NewDetails.Master" AutoEventWireup="true" CodeBehind="EmployeeBranchAttendance.aspx.cs" Inherits="Web.Attendance.EmployeeBranchAttendance" %>


<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .button
        {
            clear: both;
        }
        
        
        .phones-view
        {
            background-color: #fff;
            /* text-shadow: #fff 0 1px 0; */
            position: relative;
            display: block;
            height: auto;
        }
        
        .phones-view div.phone img
        {
            margin-bottom: 1px;
        }
        
        .phones-view div.phone
        {
            float: left;
            padding: 3px;
            margin: 3px; /*    margin: 10px 0 0 25px;*/
            text-align: left;
            line-height: 11px;
            color: #333;
            font-size: 11px;
            font-family : Arial;
            height: 135px;
            width: 285px;
            overflow: hidden;
            border-top: 1px solid transparent;
            cursor: pointer;
            background-color: #dfe8f6;
            display: inline-block;
        }
        
        .x-ie6 .phones-view div.phone, .x-ie7 .phones-view div.phone, .x-ie8 .phones-view div.phone
        {
            border-top: none;
            padding: 3px 2px;
            margin: 2px;
        }
        
        .phones-view div.phone-hover
        {
            background-color: #eee;
        }
        
        .phones-view .x-item-selected
        {
            border: 1px solid blue !important;
        }
        
        .phones-view div.phone strong
        {
            color: #000;
            display: block;
        }
        
        .phones-view div.phone span
        {
            color: #000;
        }
        
        .phones-view div.phone:after
        {
            content: 'Tenant : null';
            visibility: hidden;
            background-color: White;
            color: White;
        }
        .phone tr td
        {
            margin-bottom: 5px !important;
            width: 282px;
            text-align:center;
            background-color:#3C8DBC;
            height:35px;
            color:White;
            font-weight:normal !important;
        }
        #content
        {
            padding-left: 5px !important;
        }
        .contentLeftBlockRemoval
        {
            padding-left: 5px !important;
        }
        
        .txtCenterCls
        {
            text-align:center !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <ext:Hidden ID="Hidden_BranchId" runat="server">
    </ext:Hidden>
    <ext:Hidden ID="Hidden_Date" runat="server" />

    <ext:Hidden ID="Hidden_Sort" Text="1" runat="server">
    </ext:Hidden>
    <ext:Hidden ID="Hidden_GridOrThumbnail" Text="true" runat="server">
    </ext:Hidden>
    
    <ext:Menu ID="Menu1" runat="server" Width="150">
        <Items>
            <ext:MenuItem ID="menuButton1" Width="190" Icon="Pencil" runat="server" Text="View Details"
                OnClientClick="var a = #{Hidden_BranchId}.getValue(); b = #{Hidden_Date}.getValue(); window.open('../Attendance/EmpBranchAttendanceDetails.aspx?BranchId='+a + '&Date='+b)">
            </ext:MenuItem>
        </Items>
        <Listeners>
        </Listeners>
    </ext:Menu>
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Branch Attendance Card
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <div class="alert alert-info">
            <div>
                <table>
                    <tr>
                        <td>
                            <pr:CalendarExtControl Width="120" FieldLabel="" ID="txtDate"
                                runat="server" LabelAlign="Top" LabelSeparator="" EmptyText="Date"> 
                                <DirectEvents>
                                    <Change OnEvent="txtDate_Change" />
                                </DirectEvents>
                            </pr:CalendarExtControl>
                        </td>
                        <td style="padding-left: 10px">
                            <ext:ComboBox FieldLabel="" SelectionMode="All" ID="cmbBranch" Width="180px" EmptyText="Branch"
                                runat="server" ValueField="BranchId" DisplayField="Name" LabelAlign="Left" LabelSeparator=""
                                ForceSelection="true" QueryMode="Local">
                                <Store>
                                    <ext:Store ID="Store1" runat="server">
                                        <Model>
                                            <ext:Model ID="Model3" IDProperty="BranchId" runat="server">
                                                <Fields>
                                                    <ext:ModelField Name="BranchId" />
                                                    <ext:ModelField Name="Name" />
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <Triggers>
                                    <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                                </Triggers>
                                <Listeners>
                                    <Select Handler="this.getTrigger(0).show();" />
                                    <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                    <TriggerClick Handler="if (index == 0) { 
                                           this.clearValue(); 
                                           this.getTrigger(0).hide();
                                       }" />
                                </Listeners>
                            </ext:ComboBox>
                        </td>
                        <td style="vertical-align: bottom; padding-left: 10px;">
                            <ext:Button ID="btnSearch" runat="server" Cls="btn btn-default btn-metro" Width="100" Height="30" Text="Search">
                                <Listeners>
                                    <Click Handler="searchList();" />
                                </Listeners>
                            </ext:Button>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
       
        <div style="clear: both">
        </div>
        <ext:Panel ID="RightView" runat="server" MarginSpec="1 1 1 1" BodyCls="dbPanelBodyStyle"
            FrameHeader="false" Layout="BorderLayout" Border="true" Height="600" Style="width: 1200px;"
            Region="Center">
            <Items>
                <ext:Panel ID="Panel_VendorTransactionList" Hidden="false" runat="server" FrameHeader="false"
                    Border="false" Region="Center" Header="false" Title="Employee Catalogue" Layout="BorderLayout">
                    <TopBar>
                        <ext:Toolbar ID="Toolbar1" runat="server" Height="34" Hidden="true"> 
                            <Items>
                              
                                <ext:ComboBox ID="cmbSortBy" Height="28" LabelSeparator="" runat="server" FieldLabel="Sort By"
                                    LabelAlign="Right" LabelWidth="50" Width="200" Editable="false">
                                    <Items>
                                        <ext:ListItem Text="EIN" Value="employeeId" />
                                        <ext:ListItem Text="Name" Value="name" />
                                    </Items>
                                    <SelectedItems>
                                        <ext:ListItem Index="0" />
                                    </SelectedItems>
                                    <Listeners>
                                        <Select Handler="#{Hidden_Sort}.setValue(#{cmbSortBy}.getValue() + #{cmbSortByOrder}.getValue());searchList();" />
                                    </Listeners>
                                </ext:ComboBox>
                                <ext:ComboBox ID="cmbSortByOrder" Height="28" LabelSeparator="" runat="server" FieldLabel="Order"
                                    LabelAlign="Right" LabelWidth="50" Width="150" Editable="false">
                                    <Items>
                                        <ext:ListItem Text="ASC" Value=" ascending" />
                                        <ext:ListItem Text="DESC" Value=" descending" />
                                    </Items>
                                    <SelectedItems>
                                        <ext:ListItem Index="0" />
                                    </SelectedItems>
                                    <Listeners>
                                        <Select Handler="#{Hidden_Sort}.setValue(#{cmbSortBy}.getValue() + #{cmbSortByOrder}.getValue());searchList();" />
                                    </Listeners>
                                </ext:ComboBox>
                            </Items>
                        </ext:Toolbar>
                    </TopBar>
                    <Items>
                         <ext:DataView ContextMenuID="Menu1" ID="phones" runat="server" DeferInitialRefresh="false"
                            ItemSelector="div.phone" OverItemCls="phone-hover" MultiSelect="true" AutoScroll="true"
                            Cls="phones-view" TrackOver="true">
                            <Store>
                                <ext:Store AutoLoad="false" ID="storeBranchAtt" runat="server" PageSize="500">
                                    <Proxy>
                                        <ext:AjaxProxy Json="true" Url="../Handler/BranchAttendanceHandler.ashx">
                                            <ActionMethods Read="GET" />
                                            <Reader>
                                                <ext:JsonReader Root="data" TotalProperty="total" />
                                            </Reader>
                                        </ext:AjaxProxy>
                                    </Proxy>
                                    <AutoLoadParams>
                                        <ext:Parameter Name="start" Value="0" Mode="Raw" />
                                    </AutoLoadParams>
                                    <Parameters>
                                        <ext:StoreParameter Name="SelectedDate" Value="#{txtDate}.getRawValue()" Mode="Raw"
                                            ApplyMode="Always" />
                                        <ext:StoreParameter Name="Branch" Value="#{cmbBranch}.getValue()" Mode="Raw" ApplyMode="Always" />
                                        <ext:StoreParameter Name="sort" Value="#{Hidden_Sort}.getValue()" Mode="Raw" ApplyMode="Always" />
                                    </Parameters>
                                    <Model>
                                        <ext:Model ID="Model1" runat="server" IDProperty="BranchId">
                                            <Fields>
                                                <ext:ModelField Name="BranchId" Type="Int" />
                                                <ext:ModelField Name="BranchName" Type="String" />
                                                <ext:ModelField Name="TotalEmployeesCount" Type="Int" />
                                                <ext:ModelField Name="PresentEmployeesCount" Type="Int" />
                                                <ext:ModelField Name="Date" Type="String" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                     <Sorters>
                                        <ext:DataSorter Property="BranchName" Direction="ASC" />
                                    </Sorters>
                                </ext:Store>
                            </Store>
                             <Tpl ID="Tpl1" runat="server">
                                <Html>
                                    <tpl for=".">
                                               
                                                <tpl>
                                                     
                                                  <div class="phone" style="background-color:#F2F2F2">
                                                               <table>
                                                                   <tr>
                                                                    <td colspan="2" style="height:50px; padding-top:9px;">
                                                                        <span style='font-size:20px; color:White;'>{BranchName}</span>
                                                                    </td>
                                                                   </tr>
                                                                   <tr>
                                                                    <td style="height:27px !important; font-size:13px;">
                                                                        {TotalEmployeesCount}
                                                                    </td>
                                                                    <td style="height:27px !important; font-size:13px;">
                                                                        {PresentEmployeesCount}
                                                                    </td>
                                                                   </tr>
                                                                   <tr>
                                                                    <td style="height:27px !important; font-size:13px;">Total Employee</td>
                                                                    <td style="height:27px !important; font-size:13px;">Present Employee</td>
                                                                   </tr>
                                                                   <tr>
                                                                    <td colspan="2" style="background-color:#367FA8; padding-bottom:7px;">
                                                                        <a href="../Attendance/EmpBranchAttendanceDetails.aspx?BranchId={BranchId}&Date={Date}" runat="server" ><span style="color:White;">More Info</span></a>                                                                    
                                                                    </td>
                                                                   </tr>
                                                               </table>

                                                                
                                                                
                                                        </div>
                                                        </tpl>

                                                               
                                                    
                                              </tpl>
                                </Html>
                            </Tpl>
                            <Plugins>
                                <ext:DataViewAnimated ID="DataViewAnimated1" runat="server" Duration="550" IDProperty="id" />
                            </Plugins>
                            <Listeners>
                                <ItemMouseEnter Fn="itemClick" />
                            </Listeners>
                        </ext:DataView>
                    </Items>
                    <BottomBar>
                        <ext:PagingToolbar ID="PagingToolbar_AdjustmentList" runat="server" StoreID="storeBranchAtt">
                        </ext:PagingToolbar>
                    </BottomBar>
                </ext:Panel>
            </Items>
        </ext:Panel>
    </div>
    <br />
    <script type="text/javascript">
        function medicalTaxCreditPopupCall(medicalTaxId) {
            var ret = medicalTaxCreditPopup("Id=" + medicalTaxId);
            if (ret == "Yes")
                __doPostBack('Reload', '');
        }

        function parentReloadCallbackFunction(childWindow, ret) {
            childWindow.close();
            if (ret == "Yes")
                __doPostBack('Reload', '');

        }

        function refreshWindow() {
            __doPostBack('Reload', '');
        }
    </script>
    <script type="text/javascript">

        var Hidden_BranchId = null;
        var PagingToolbar_AdjustmentList = null;       



        //to track if dash board mode or customer details listing
        var isDashboardVisible = true;

        Ext.onReady(function () {
            
            Hidden_BranchId = <%=Hidden_BranchId.ClientID %>;
            PagingToolbar_AdjustmentList = <%= PagingToolbar_AdjustmentList.ClientID %>;

        });

      
        function searchList() 
        {
            var dateValue = <%= txtDate.ClientID %>.getValue();

            if(dateValue == null || dateValue == '')
            {
                alert('Date is required.');
                return false;
            } 
            <%=storeBranchAtt.ClientID %>.reload();
        }

        function setValueInControl() 
        {
        }
      
        function RenderForZeroAmount(value)
        {
            if(value.toString() == "0")
                return "-" ;
            else
                return value;
        }

        

        var getRenewedImage = function(value)
        {
            if( value == true)
                return "<img src='../images/ok.gif' style='width:15px;height:15px' />";
            else
                return "<img src='../images/exclaim.gif'  style='width:15px;height:15px' />";
        }

           function searchByText()
        {
            setValueInControl();
            searchList();
        }


        var GetGridRow = function (value, meta, record) 
        {
            if(record == null || typeof(record) == 'undefined')
                return "";

                 var link = "";
            if(record.data.DocumentType == "")
                link = DocumentTab.MemberPayment;
                
            else if(record.data.DocumentType == 'Journal')
               link = DocumentTab.AdjustmentEntries;
            
            else if(record.data.DocumentType == 'GeneralPayment')
                link = DocumentTab.GeneralPayment;

            var absUrl = getAbsoluteUrl(link , record.data.DocumentID);
            return "<a href='" + absUrl +"'>" + value + "<a/>";
        };         


         var memID = null;
       
        

       

        var itemClick = function (view, record, item, index, e) 
        {

            var item = e.getTarget(".phone");

            var row = e.getTarget("tr.phonesRow"),
            item2 = row && Ext.get(row).child("td.phonesCell");

            if (item) {
                memID = record.data.BranchId;
                <%= Hidden_BranchId.ClientID %>.setValue(memID);
             
            }
            

        };
       
    </script>
</asp:Content>
