<%@ Page Title="Time Attendance Summary" Language="C#" MaintainScrollPositionOnPostback="true" MasterPageFile="~/Master/HROld.Master" AutoEventWireup="true" CodeBehind="HRAttendanceMontlySummaryDevice.aspx.cs" Inherits="Web.CP.Report.HRAttendanceMontlySummaryDevice" %>
<%@ Register assembly="DevExpress.XtraReports.v16.1.Web, Version=16.1.8.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.XtraReports.Web" tagprefix="dxxr" %>
<%@ Register src="~/Controls/Report/ReportContainer.ascx" tagname="ReportContainer" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
     <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Time Attendance Summary
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <uc1:ReportContainer runat="server"   OnReloadReport="LoadReport" id="report" />
    </div>

</asp:Content>
