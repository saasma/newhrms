﻿<%@ Page Title="Late Entry Skip Dates" Language="C#" MasterPageFile="~/Master/NewDetails.Master"
    AutoEventWireup="true" CodeBehind="LateEntrySkipDate.aspx.cs" Inherits="Web.CP.LateEntrySkipDate" %>

<%@ MasterType VirtualPath="~/Master/NewDetails.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">

    function searchList() {
              <%=PagingToolbar1.ClientID %>.doRefresh();
        }

  
    var CommandHandler = function(command, record){
            <%= hiddenValue.ClientID %>.setValue(record.data.ID);
            
                if(command=="Edit")
                {
                    <%= btnEdit.ClientID %>.fireEvent('click');
                }
                else
                {
                    <%= btnDelete.ClientID %>.fireEvent('click');
                }

             }

    </script>
    <style type="text/css">
        .holiday, .holiday a, .holiday td
        {
            color: #469146;
            background-color: #F0FFF0 !important;
        }
        .leave, .leave a, .leave td
        {
            color: #D95CA9;
            background-color: #FFF0F5 !important;
        }
        
        .weeklyholiday, .weeklyholiday a, .weeklyholiday td
        {
            color: #8F8F1A;
            background-color: #FAFAD2 !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <script type="text/javascript" src="<%= ResolveUrl("~/Employee/override.js?v=") + Web.Helper.WebHelper.Version %>"></script>
    <ext:Hidden ID="hdnEmployeeId" runat="server" />
    <ext:Hidden ID="hdnBranchDepartmentId" runat="server" />
    <ext:Hidden ID="hdnMsg" runat="server" />
    <ext:Hidden ID="hdnEmpSearch" runat="server" />
    <ext:Hidden ID="hdnSortBy" runat="server" />
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Late Entry Skip Dates
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel" style="margin-top: 0px !important; padding-top: 0px !important;">
        <div class="innerLR">
            <div style="clear: both">
            </div>
            <span runat='server' style="font-size: 14px; padding-top: 15px; margin-bottom: 10px"
                id="details"></span>
            <div class="alert alert-info" style="margin-top: 10px; padding-top: 0px!important;">
                <ext:Container ID="listingContainer" runat="server">
                    <Content>
                        <table>
                            <tr>
                                <td>
                                    <ext:DateField LabelWidth="120" MarginSpec="0 5 0 5" Width="100" FieldLabel="From"
                                        ID="calFromDate" runat="server" LabelAlign="Top" LabelSeparator="">
                                        <Plugins>
                                            <ext:GenericPlugin ID="HighlightDates1" runat="server" InstanceName="Ext.ux.DatePicker.HighlightWithNepDates" />
                                        </Plugins>
                                    </ext:DateField>
                                </td>
                                <td>
                                    <ext:DateField LabelWidth="120" MarginSpec="0 5 0 5" Width="100" FieldLabel="To"
                                        ID="calToDate" runat="server" LabelAlign="Top" LabelSeparator="">
                                        <Plugins>
                                            <ext:GenericPlugin ID="GenericPlugin1" runat="server" InstanceName="Ext.ux.DatePicker.HighlightWithNepDates" />
                                        </Plugins>
                                    </ext:DateField>
                                </td>
                                <td>
                                    <ext:Button ID="btnLoad" runat="server" Text="Load" Width="100" Cls="btn btn-default"
                                        MarginSpec="25 10 10 10">
                                        <Listeners>
                                            <Click Fn="searchList">
                                            </Click>
                                        </Listeners>
                                    </ext:Button>
                                </td>
                            </tr>
                        </table>
                    </Content>
                </ext:Container>
            </div>
            <ext:KeyMap ID="KeyMap1" runat="server" Target="={#{listingContainer}.getBody()}">
                <Binding>
                    <ext:KeyBinding Handler="#{btnLoad}.fireEvent('click');">
                        <Keys>
                            <ext:Key Code="ENTER" />
                        </Keys>
                    </ext:KeyBinding>
                </Binding>
            </ext:KeyMap>
            <ext:GridPanel StyleSpec="margin-top:15px;" ID="gridBranchTransfer" runat="server"
                Cls="itemgrid" OnReadData="Store_ReadData">
               
                <Store>
                    <ext:Store PageSize="1000" ID="storeGrid" runat="server" AutoLoad="true" OnReadData="Store_ReadData"
                        RemotePaging="true" RemoteSort="true">
                        <Proxy>
                            <ext:PageProxy />
                        </Proxy>
                        <Model>
                            <ext:Model ID="Model1" runat="server">
                                <Fields>
                                    <ext:ModelField Name="ID" Type="String" />
                                    <ext:ModelField Name="DateEng" Type="Date" />
                                    <ext:ModelField Name="Date" Type="String" />
                                    <ext:ModelField Name="Comment" Type="String" />
                                </Fields>
                            </ext:Model>
                        </Model>
                        <Sorters>
                            <ext:DataSorter Property="date" Direction="ASC" />
                        </Sorters>
                    </ext:Store>
                </Store>
                <ColumnModel ID="ColumnModel1" runat="server">
                    <Columns>
                        <ext:DateColumn ID="DateColumn1" runat="server" Text="Date" DataIndex="DateEng" Format="dd-MMM-yyyy"
                            MenuDisabled="false" Sortable="true" Align="Left" Width="120" />
                        <ext:Column ID="Column4" runat="server" Text="Nep Date" DataIndex="Date" MenuDisabled="false"
                            Sortable="true" Align="Left" Width="120" />
                        <ext:Column ID="Column2" runat="server" Text="Comment" DataIndex="Comment" MenuDisabled="false"
                            Sortable="true" Align="Left" Width="300" />
                        <ext:CommandColumn ID="CommandColumn1" runat="server" Width="140" Text="Actions"
                            Align="Center">
                            <Commands>
                                <ext:CommandSeparator />
                                <ext:GridCommand Cls="editGridButton" ToolTip-Text="Edit" Text="Edit" Icon="ApplicationEdit"
                                    CommandName="Edit" />
                                <ext:GridCommand Cls="editGridButton" ToolTip-Text="Delete" Text="Delete" Icon="ApplicationDelete"
                                    CommandName="Delete" />
                            </Commands>
                            <Listeners>
                                <Command Handler="CommandHandler(command,record);" />
                            </Listeners>
                        </ext:CommandColumn>
                    </Columns>
                </ColumnModel>
                <BottomBar>
                    <ext:PagingToolbar ID="PagingToolbar1" runat="server" DisplayInfo="true">
                    </ext:PagingToolbar>
                </BottomBar>
            </ext:GridPanel>
        </div>
        <div class="buttonBlockSection" style="margin-top: 10px" runat="server" id="buttonBlock">
            <ext:Button runat="server" Cls="btn btn-primary btn-sect" Height="30px" ID="btnAddNew"
                Text="<i></i>Add New">
                <DirectEvents>
                    <Click OnEvent="btnAddNew_Click">
                        <EventMask ShowMask="true" />
                    </Click>
                </DirectEvents>
            </ext:Button>
        </div>
        <div class="alert alert-info" style="margin-top: 15px; height: 50px;">
            <div style="float: right;">
                <ext:Button runat="server" AutoPostBack="true" OnClick="btnExport_Click" ID="btnExport"
                    Text="<i></i>Export To Excel">
                </ext:Button>
            </div>
        </div>
        <ext:Hidden ID="hiddenValue" runat="server" />
        <ext:LinkButton ID="btnEdit" runat="server" Hidden="true">
            <DirectEvents>
                <Click OnEvent="btnEdit_Click">
                    <EventMask ShowMask="true" />
                </Click>
            </DirectEvents>
        </ext:LinkButton>
        <ext:LinkButton ID="btnDelete" runat="server" Hidden="true">
            <DirectEvents>
                <Click OnEvent="btnDelete_Click">
                    <EventMask ShowMask="true" />
                    <Confirmation ConfirmRequest="true" Message="Are you sure, you want to delete ?" />
                </Click>
            </DirectEvents>
        </ext:LinkButton>
        <ext:Window ID="window" runat="server" Title="Late Skip Date" Icon="Application"
            Width="700" Height="620" BodyPadding="5" Hidden="true" Modal="true">
            <Content>
                <table class="fieldTable">
                    <tr>
                        <td>
                            <ext:DateField ID="txtDate" runat="server" FieldLabel="Date *" EmptyText="" LabelAlign="Left"
                                LabelSeparator="" Width="300" StyleSpec="margin-top:7px;">
                                <Plugins>
                                    <ext:GenericPlugin ID="GenericPlugin2" runat="server" InstanceName="Ext.ux.DatePicker.HighlightWithNepDates" />
                                </Plugins>
                            </ext:DateField>
                            <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator2" runat="server"
                                ValidationGroup="SaveUpdProjectAssociation" ControlToValidate="txtDate" ErrorMessage="Date is required." />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <ext:TextField ID="txtComment" runat="server" FieldLabel="Comment" EmptyText="" Width="300"
                                LabelAlign="Left" LabelSeparator="" StyleSpec="margin-top:7px;">
                            </ext:TextField>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <ext:ItemSelector ID="selectorBranch" ValueField="BranchId" DisplayField="Name" LabelSeparator=""
                                runat="server" FieldLabel="Branch"  AllowBlank="false" Note="If no selection then applies to all Branch"
                                FromTitle="Available Branch" Width="600" Height="400" ToTitle="Selected Branch">
                                <Store>
                                    <ext:Store ID="store1" runat="server" AutoLoad="true">
                                        <Model>
                                            <ext:Model ID="Model2" runat="server">
                                                <Fields>
                                                    <ext:ModelField Name="BranchId" Type="String" />
                                                    <ext:ModelField Name="Name" Type="String" />
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <Items>
                                    <%--<ext:ListItem Text="One Hundred Twenty Three" Value="123" />
                                    <ext:ListItem Text="One" Value="1" />
                                    <ext:ListItem Text="Two" Value="2" />
                                    <ext:ListItem Text="Three" Value="3" />
                                    <ext:ListItem Text="Four" Value="4" />
                                    <ext:ListItem Text="Five" Value="5" />
                                    <ext:ListItem Text="Six" Value="6" />
                                    <ext:ListItem Text="Seven" Value="7" />
                                    <ext:ListItem Text="Eight" Value="8" />
                                    <ext:ListItem Text="Nine" Value="9" />--%>
                                </Items>
                                <SelectedItems>
                                  <%--   <ext:ListItem Value="5" />
                                    <ext:ListItem Value="4" />
                                    <ext:ListItem Value="6" />--%>
                                </SelectedItems>
                            </ext:ItemSelector>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td valign="bottom">
                                        <div class="popupButtonDiv">
                                            <ext:Button runat="server" ID="btnSave" Height="30px" Width="80px" Cls="btn btn-primary"
                                                Text="<i></i>Save">
                                                <DirectEvents>
                                                    <Click OnEvent="btnSave_Click">
                                                        <EventMask ShowMask="true" />
                                                    </Click>
                                                </DirectEvents>
                                                <Listeners>
                                                    <Click Handler="valGroup = 'SaveUpdProjectAssociation'; if(CheckValidation()) return true;">
                                                    </Click>
                                                </Listeners>
                                            </ext:Button>
                                    </td>
                                    <td>
                                        <div class="btnFlatOr">
                                            or</div>
                                    </td>
                                    <td>
                                        <ext:LinkButton runat="server" Cls="btnFlatLeftGap" ID="LinkButton1" Text="<i></i>Cancel">
                                            <Listeners>
                                                <Click Handler="#{window}.hide();">
                                                </Click>
                                            </Listeners>
                                        </ext:LinkButton>
                                    </td>
                                    <td style="padding-left: 100px">
                                        <ext:Button ID="Button1" runat="server" Text="Reset" Handler="#{selectorBranch}.reset();" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </Content>
        </ext:Window>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
