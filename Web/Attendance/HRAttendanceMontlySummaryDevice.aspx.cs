﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//using Web.CP.Report.Templates;

using BLL;
using DevExpress.XtraReports.UI;
using Web.ReportDataSetTableAdapters;
using Utils.Calendar;
using BLL.Base;
using BLL.Manager;
using DevExpress.XtraReports.Web;
using System.IO;
using DAL;

//pass the datetime from selected payroll period instead of payrollperiod id itself

namespace Web.CP.Report
{
    public partial class HRAttendanceMontlySummaryDevice : BasePage
    {

        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.LeaveAndAttendance;
            }
        }

        AttendanceManager attendanceManager = new AttendanceManager();

        protected void Page_Load(object sender, EventArgs e)
        {
            //if (!IsPostBack || CommonManager.CompanySetting.RemoveStyleWhileExportingReport)
                LoadReport();

                //report.Filter.Employee = true;
                report.Filter.PayrollFrom = true;
                report.Filter.PayrollTo = false;
                report.Filter.Department = false;
        }

        protected void LoadReport()
        {
            ReportDataSetTableAdapters.AttendanceGetMainReportNewTableAdapter adap = new ReportDataSetTableAdapters.AttendanceGetMainReportNewTableAdapter();

            Report.Templates.HR.ReportAttendanceEmpMontlySummary report1 = new Report.Templates.HR.ReportAttendanceEmpMontlySummary();

            report1.HideColumns();

            PayrollPeriod payrollPeriod = CommonManager.GetPayrollPeriod(report.Filter.StartDate.Month,
                                                                     report.Filter.StartDate.Year);

            if (payrollPeriod == null)
                return;
            
            // Set for DBDefence
            BLL.BaseBiz.SetConnectionPwd(adap.Connection);

            ReportDataSet.AttendanceGetMainReportNewDataTable refinedDataTable = new ReportDataSet.AttendanceGetMainReportNewDataTable();
            refinedDataTable = adap.GetData(report.Filter.EmployeeId, payrollPeriod.PayrollPeriodId,report.Filter.BranchId);

            int AbsCount=0;
            int day = 28;
            int totalDaysInMonth =31;
            totalDaysInMonth = attendanceManager.GetTotalDaysInMonth(payrollPeriod.PayrollPeriodId);
            if (totalDaysInMonth < 29)
            {
                report1.HideColumn29();
                report1.HideColumn30();
                report1.HideColumn31();
                report1.HideColumn32();
            }
            else if (totalDaysInMonth < 30)
            {
                report1.HideColumn30();
                report1.HideColumn31();
                report1.HideColumn32();
            }
            else if (totalDaysInMonth < 31)
            {
                report1.HideColumn31();
                report1.HideColumn32();
            }
            else if (totalDaysInMonth < 32)
            {
                report1.HideColumn32();
            }



            foreach (var val1 in refinedDataTable)
            {
                AbsCount = 0;
                if (val1.D1 == null || string.IsNullOrEmpty(val1.D1.Trim()) || val1.D1.Trim().ToLower().Contains("-a"))       
                    AbsCount++;
                if (val1.D3 == null || string.IsNullOrEmpty(val1.D2.Trim()) || val1.D2.Trim().ToLower().Contains("-a"))
                    AbsCount++;
                if (val1.D3 == null || string.IsNullOrEmpty(val1.D3.Trim()) || val1.D3.Trim().ToLower().Contains("-a"))
                    AbsCount++;
                if (val1.D4 == null || string.IsNullOrEmpty(val1.D4.Trim()) || val1.D4.Trim().ToLower().Contains("-a"))
                    AbsCount++;
                if (val1.D5 == null || string.IsNullOrEmpty(val1.D5.Trim()) || val1.D5.Trim().ToLower().Contains("-a"))
                    AbsCount++;

                if (val1.D6 == null || string.IsNullOrEmpty(val1.D6.Trim()) || val1.D6.Trim().ToLower().Contains("-a"))
                    AbsCount++;
                if (val1.D7 == null || string.IsNullOrEmpty(val1.D7.Trim()) || val1.D7.Trim().ToLower().Contains("-a"))
                    AbsCount++;
                if (val1.D8 == null || string.IsNullOrEmpty(val1.D8.Trim()) || val1.D8.Trim().ToLower().Contains("-a"))
                    AbsCount++;
                if (val1.D9 == null || string.IsNullOrEmpty(val1.D9.Trim()) || val1.D9.Trim().ToLower().Contains("-a"))
                    AbsCount++;
                if (val1.D10 == null || string.IsNullOrEmpty(val1.D10.Trim()) || val1.D10.Trim().ToLower().Contains("-a"))
                    AbsCount++;

                if (val1.D11 == null || string.IsNullOrEmpty(val1.D11.Trim()) || val1.D11.Trim().ToLower().Contains("-a"))
                    AbsCount++;
                if (val1.D12 == null || string.IsNullOrEmpty(val1.D12.Trim()) || val1.D12.Trim().ToLower().Contains("-a"))
                    AbsCount++;
                if (val1.D13 == null || string.IsNullOrEmpty(val1.D13.Trim()) || val1.D13.Trim().ToLower().Contains("-a"))
                    AbsCount++;
                if (val1.D14 == null || string.IsNullOrEmpty(val1.D14.Trim()) || val1.D14.Trim().ToLower().Contains("-a"))
                    AbsCount++;
                if (val1.D15 == null || string.IsNullOrEmpty(val1.D15.Trim()) || val1.D15.Trim().ToLower().Contains("-a"))
                    AbsCount++;


                if (val1.D16 == null || string.IsNullOrEmpty(val1.D16.Trim()) || val1.D16.Trim().ToLower().Contains("-a"))
                    AbsCount++;
                if (val1.D17 == null || string.IsNullOrEmpty(val1.D17.Trim()) || val1.D17.Trim().ToLower().Contains("-a"))
                    AbsCount++;
                if (val1.D18 == null || string.IsNullOrEmpty(val1.D18.Trim()) || val1.D18.Trim().ToLower().Contains("-a"))
                    AbsCount++;
                if (val1.D19 == null || string.IsNullOrEmpty(val1.D19.Trim()) || val1.D19.Trim().ToLower().Contains("-a"))
                    AbsCount++;
                if (val1.D20 == null || string.IsNullOrEmpty(val1.D20.Trim()) || val1.D20.Trim().ToLower().Contains("-a"))
                    AbsCount++;

                if (val1.D21 == null || string.IsNullOrEmpty(val1.D21.Trim()) || val1.D21.Trim().ToLower().Contains("-a"))
                    AbsCount++;
                if (val1.D22 == null || string.IsNullOrEmpty(val1.D22.Trim()) || val1.D22.Trim().ToLower().Contains("-a"))
                    AbsCount++;
                if (val1.D23 == null || string.IsNullOrEmpty(val1.D23.Trim()) || val1.D23.Trim().ToLower().Contains("-a"))
                    AbsCount++;
                if (val1.D24 == null || string.IsNullOrEmpty(val1.D24.Trim()) || val1.D24.Trim().ToLower().Contains("-a"))
                    AbsCount++;
                if (val1.D25 == null || string.IsNullOrEmpty(val1.D25.Trim()) || val1.D25.Trim().ToLower().Contains("-a"))
                    AbsCount++;

                if (val1.D26 == null || string.IsNullOrEmpty(val1.D26.Trim()) || val1.D26.Trim().ToLower().Contains("-a"))
                    AbsCount++;
                if (val1.D27 == null || string.IsNullOrEmpty(val1.D27.Trim()) || val1.D27.Trim().ToLower().Contains("-a"))
                    AbsCount++;

                if (totalDaysInMonth >= 28)
                {
                    if (val1.D28 == null || string.IsNullOrEmpty(val1.D28.Trim()) || val1.D28.Trim().ToLower().Contains("-a"))
                        AbsCount++;
                }

                if (totalDaysInMonth >= 29)
                {
                    if (val1.D29 == null || string.IsNullOrEmpty(val1.D29.Trim()) || val1.D29.Trim().ToLower().Contains("-a"))
                        AbsCount++;
                }

                if (totalDaysInMonth >= 30)
                {
                    if (val1.D30 == null || string.IsNullOrEmpty(val1.D30.Trim()) || val1.D30.Trim().ToLower().Contains("-a"))
                        AbsCount++;
                }

                if (totalDaysInMonth >= 31)
                {
                    if (val1.D31 == null || string.IsNullOrEmpty(val1.D31.Trim()) || val1.D31.Trim().ToLower().Contains("-a"))
                        AbsCount++;
                }

                if (totalDaysInMonth >= 32)
                {
                    if (val1.D32 == null || string.IsNullOrEmpty(val1.D32.Trim()) || val1.D32.Trim().ToLower().Contains("-a"))
                        AbsCount++;
                }

                val1.absc = AbsCount;
               
            }

            report1.DataSource = refinedDataTable;
            report1.DataMember = "AttendanceReport";

            report.DisplayReport(report1);

            if (ReportHelper.IsReportExportState())
            {
                this.report.clearCache = true;
            }

            report1.XRTitle.Text = "Attendance Summary of " + payrollPeriod.Name;
            
        }

      
    }
}

