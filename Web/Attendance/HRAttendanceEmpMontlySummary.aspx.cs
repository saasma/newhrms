﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//using Web.CP.Report.Templates;

using BLL;
using DevExpress.XtraReports.UI;
using Web.ReportDataSetTableAdapters;
using Utils.Calendar;
using BLL.Base;
using BLL.Manager;
using DevExpress.XtraReports.Web;
using System.IO;
using DAL;

namespace Web.CP.Report
{
    public partial class HRAttendanceEmpMontlySummary : BasePage
    {

        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.LeaveAndAttendance;
            }
        }

        AttendanceManager attendanceManager = new AttendanceManager();

        protected void Page_Load(object sender, EventArgs e)
        {
            report.Filter.ShowDynamic = true;
            report.Filter.DynamicLabel = "Report Options";
            if (!IsPostBack)
            {
                report.Filter.DDLDynamic.Items.Add(new ListItem { Text = "Data from Attendance Register", Value = "0" });
                report.Filter.DDLDynamic.Items.Add(new ListItem { Text = "Data from Device", Value = "1" });
                report.Filter.DDLDynamic.Items.Add(new ListItem { Text = "Data from Device with Time", Value = "2" });
                report.Filter.DDLDynamic.SelectedIndex = 0;
            }


            //if (!IsPostBack || CommonManager.CompanySetting.RemoveStyleWhileExportingReport)
            if (report.Filter.DDLDynamic.SelectedIndex == 0)
                LoadReportFromAttendanceRegister();
             if (report.Filter.DDLDynamic.SelectedIndex == 1)
                LoadReportFromDeviceAttendanceEmployeeTable();
             if (report.Filter.DDLDynamic.SelectedIndex == 2)
                LoadReportFromDeviceAttendanceEmployeeWithTimeTable();


                //report.Filter.Employee = true;
                report.Filter.PayrollFrom = true;
                report.Filter.PayrollTo = false;
                report.Filter.Department = false;
        }

        protected void LoadReport()
        {
            if (report.Filter.DDLDynamic.SelectedIndex == 0)
                LoadReportFromAttendanceRegister();
            if (report.Filter.DDLDynamic.SelectedIndex == 1)
                LoadReportFromDeviceAttendanceEmployeeTable();
            if (report.Filter.DDLDynamic.SelectedIndex == 2)
                LoadReportFromDeviceAttendanceEmployeeWithTimeTable();
        }

        protected void LoadReportFromDeviceAttendanceEmployeeTable()
        {
            ReportDataSetTableAdapters.AttendanceGetMainReportNewTableAdapter adap = new ReportDataSetTableAdapters.AttendanceGetMainReportNewTableAdapter();

            Report.Templates.HR.ReportAttendanceEmpMontlySummary report1 = new Report.Templates.HR.ReportAttendanceEmpMontlySummary();

            report1.HideColumns();

            PayrollPeriod payrollPeriod = CommonManager.GetPayrollPeriod(report.Filter.StartDate.Month,
                                                                     report.Filter.StartDate.Year);

            if (payrollPeriod == null)
            {
                divWarningMsg.InnerHtml = "Payroll period of " + DateHelper.GetMonthName(report.Filter.StartDate.Month, IsEnglish) + " does not exists.";
                divWarningMsg.Hide = false;
                return;
            }

            if (BLL.BaseBiz.PayrollDataContext.AttendanceEmployeeDates.Any(x => payrollPeriod.StartDateEng >= x.LastUptoDate.Value.Date && payrollPeriod.StartDateEng <= x.LastDate) == false
                &&
                BLL.BaseBiz.PayrollDataContext.AttendanceEmployeeDates.Any(x => payrollPeriod.EndDateEng >= x.LastUptoDate.Value.Date && payrollPeriod.EndDateEng <= x.LastDate) == false)
            {
                divWarningMsg.InnerHtml = "Attendance data does not exists, please generate the device attendance data of " + payrollPeriod.Name + " from <a href='../newhr/GenerateAttendanceReportData.aspx'>Generate Attendance</a> for the result.";
                divWarningMsg.Hide = false;
                //return;
            }

            // Set for DBDefence
            BLL.BaseBiz.SetConnectionPwd(adap.Connection);

            ReportDataSet.AttendanceGetMainReportNewDataTable refinedDataTable = new ReportDataSet.AttendanceGetMainReportNewDataTable();
            refinedDataTable = adap.GetData(report.Filter.EmployeeId, payrollPeriod.PayrollPeriodId,report.Filter.BranchId);

            int AbsCount = 0;
            int day = 28;
            int totalDaysInMonth = 31;
            totalDaysInMonth = attendanceManager.GetTotalDaysInMonth(payrollPeriod.PayrollPeriodId);
            if (totalDaysInMonth < 29)
            {
                report1.HideColumn29();
                report1.HideColumn30();
                report1.HideColumn31();
                report1.HideColumn32();
            }
            else if (totalDaysInMonth < 30)
            {
                report1.HideColumn30();
                report1.HideColumn31();
                report1.HideColumn32();
            }
            else if (totalDaysInMonth < 31)
            {
                report1.HideColumn31();
                report1.HideColumn32();
            }
            else if (totalDaysInMonth < 32)
            {
                report1.HideColumn32();
            }



            foreach (var val1 in refinedDataTable)
            {
                AbsCount = 0;
                if (val1.D1 == null || string.IsNullOrEmpty(val1.D1.Trim()) || val1.D1.Trim().ToLower().Contains("-a"))
                    AbsCount++;
                if (val1.D3 == null || string.IsNullOrEmpty(val1.D2.Trim()) || val1.D2.Trim().ToLower().Contains("-a"))
                    AbsCount++;
                if (val1.D3 == null || string.IsNullOrEmpty(val1.D3.Trim()) || val1.D3.Trim().ToLower().Contains("-a"))
                    AbsCount++;
                if (val1.D4 == null || string.IsNullOrEmpty(val1.D4.Trim()) || val1.D4.Trim().ToLower().Contains("-a"))
                    AbsCount++;
                if (val1.D5 == null || string.IsNullOrEmpty(val1.D5.Trim()) || val1.D5.Trim().ToLower().Contains("-a"))
                    AbsCount++;

                if (val1.D6 == null || string.IsNullOrEmpty(val1.D6.Trim()) || val1.D6.Trim().ToLower().Contains("-a"))
                    AbsCount++;
                if (val1.D7 == null || string.IsNullOrEmpty(val1.D7.Trim()) || val1.D7.Trim().ToLower().Contains("-a"))
                    AbsCount++;
                if (val1.D8 == null || string.IsNullOrEmpty(val1.D8.Trim()) || val1.D8.Trim().ToLower().Contains("-a"))
                    AbsCount++;
                if (val1.D9 == null || string.IsNullOrEmpty(val1.D9.Trim()) || val1.D9.Trim().ToLower().Contains("-a"))
                    AbsCount++;
                if (val1.D10 == null || string.IsNullOrEmpty(val1.D10.Trim()) || val1.D10.Trim().ToLower().Contains("-a"))
                    AbsCount++;

                if (val1.D11 == null || string.IsNullOrEmpty(val1.D11.Trim()) || val1.D11.Trim().ToLower().Contains("-a"))
                    AbsCount++;
                if (val1.D12 == null || string.IsNullOrEmpty(val1.D12.Trim()) || val1.D12.Trim().ToLower().Contains("-a"))
                    AbsCount++;
                if (val1.D13 == null || string.IsNullOrEmpty(val1.D13.Trim()) || val1.D13.Trim().ToLower().Contains("-a"))
                    AbsCount++;
                if (val1.D14 == null || string.IsNullOrEmpty(val1.D14.Trim()) || val1.D14.Trim().ToLower().Contains("-a"))
                    AbsCount++;
                if (val1.D15 == null || string.IsNullOrEmpty(val1.D15.Trim()) || val1.D15.Trim().ToLower().Contains("-a"))
                    AbsCount++;


                if (val1.D16 == null || string.IsNullOrEmpty(val1.D16.Trim()) || val1.D16.Trim().ToLower().Contains("-a"))
                    AbsCount++;
                if (val1.D17 == null || string.IsNullOrEmpty(val1.D17.Trim()) || val1.D17.Trim().ToLower().Contains("-a"))
                    AbsCount++;
                if (val1.D18 == null || string.IsNullOrEmpty(val1.D18.Trim()) || val1.D18.Trim().ToLower().Contains("-a"))
                    AbsCount++;
                if (val1.D19 == null || string.IsNullOrEmpty(val1.D19.Trim()) || val1.D19.Trim().ToLower().Contains("-a"))
                    AbsCount++;
                if (val1.D20 == null || string.IsNullOrEmpty(val1.D20.Trim()) || val1.D20.Trim().ToLower().Contains("-a"))
                    AbsCount++;

                if (val1.D21 == null || string.IsNullOrEmpty(val1.D21.Trim()) || val1.D21.Trim().ToLower().Contains("-a"))
                    AbsCount++;
                if (val1.D22 == null || string.IsNullOrEmpty(val1.D22.Trim()) || val1.D22.Trim().ToLower().Contains("-a"))
                    AbsCount++;
                if (val1.D23 == null || string.IsNullOrEmpty(val1.D23.Trim()) || val1.D23.Trim().ToLower().Contains("-a"))
                    AbsCount++;
                if (val1.D24 == null || string.IsNullOrEmpty(val1.D24.Trim()) || val1.D24.Trim().ToLower().Contains("-a"))
                    AbsCount++;
                if (val1.D25 == null || string.IsNullOrEmpty(val1.D25.Trim()) || val1.D25.Trim().ToLower().Contains("-a"))
                    AbsCount++;

                if (val1.D26 == null || string.IsNullOrEmpty(val1.D26.Trim()) || val1.D26.Trim().ToLower().Contains("-a"))
                    AbsCount++;
                if (val1.D27 == null || string.IsNullOrEmpty(val1.D27.Trim()) || val1.D27.Trim().ToLower().Contains("-a"))
                    AbsCount++;

                if (totalDaysInMonth >= 28)
                {
                    if (val1.D28 == null || string.IsNullOrEmpty(val1.D28.Trim()) || val1.D28.Trim().ToLower().Contains("-a"))
                        AbsCount++;
                }

                if (totalDaysInMonth >= 29)
                {
                    if (val1.D29 == null || string.IsNullOrEmpty(val1.D29.Trim()) || val1.D29.Trim().ToLower().Contains("-a"))
                        AbsCount++;
                }

                if (totalDaysInMonth >= 30)
                {
                    if (val1.D30 == null || string.IsNullOrEmpty(val1.D30.Trim()) || val1.D30.Trim().ToLower().Contains("-a"))
                        AbsCount++;
                }

                if (totalDaysInMonth >= 31)
                {
                    if (val1.D31 == null || string.IsNullOrEmpty(val1.D31.Trim()) || val1.D31.Trim().ToLower().Contains("-a"))
                        AbsCount++;
                }

                if (totalDaysInMonth >= 32)
                {
                    if (val1.D32 == null || string.IsNullOrEmpty(val1.D32.Trim()) || val1.D32.Trim().ToLower().Contains("-a"))
                        AbsCount++;
                }

                val1.absc = AbsCount;

            }

            report1.DataSource = refinedDataTable;
            report1.DataMember = "AttendanceReport";

            report.DisplayReport(report1);

            if (ReportHelper.IsReportExportState())
            {
                this.report.clearCache = true;
            }

            report1.XRTitle.Text = "Attendance Device Summary of " + payrollPeriod.Name;

        }
        protected void LoadReportFromAttendanceRegister()
        {
            ReportDataSetTableAdapters.AttendanceGetMainReportTableAdapter adap = new ReportDataSetTableAdapters.AttendanceGetMainReportTableAdapter();



            Report.Templates.HR.ReportAttendanceEmpMontlySummary report1 = new Report.Templates.HR.ReportAttendanceEmpMontlySummary();

            

            PayrollPeriod payrollPeriod = CommonManager.GetPayrollPeriod(report.Filter.StartDate.Month,
                                                                     report.Filter.StartDate.Year);

            if (IsPostBack==false)
                return;

            if (payrollPeriod == null)
            {
                divWarningMsg.InnerHtml = "Payroll period of " + DateHelper.GetMonthName(report.Filter.StartDate.Month,IsEnglish) + " does not exists.";
                divWarningMsg.Hide = false;
                return;
            }

            if (BLL.BaseBiz.PayrollDataContext.Attendences.Any(x => x.PayrollPeriodId == payrollPeriod.PayrollPeriodId) == false)
            {
                divWarningMsg.InnerHtml = "Attendance not saved, please save the attendance of " + payrollPeriod.Name + " from <a href='../CP/AttendanceTracking.aspx'>Attendance Register</a> for the result.";
                divWarningMsg.Hide = false;
                return;
            }

            // Set for DBDefence
            BLL.BaseBiz.SetConnectionPwd(adap.Connection);

            ReportDataSet.AttendanceGetMainReportDataTable refinedDataTable = new ReportDataSet.AttendanceGetMainReportDataTable();
            refinedDataTable = adap.GetData(report.Filter.EmployeeId, payrollPeriod.PayrollPeriodId,report.Filter.BranchId);




            int AbsCount = 0;
            int day = 28;
            int totalDaysInMonth = 31;
            totalDaysInMonth = attendanceManager.GetTotalDaysInMonth(payrollPeriod.PayrollPeriodId);
            if (totalDaysInMonth < 29)
            {
                report1.HideColumn29();
                report1.HideColumn30();
                report1.HideColumn31();
                report1.HideColumn32();
            }
            else if (totalDaysInMonth < 30)
            {
                report1.HideColumn30();
                report1.HideColumn31();
                report1.HideColumn32();
            }
            else if (totalDaysInMonth < 31)
            {
                report1.HideColumn31();
                report1.HideColumn32();
            }
            else if (totalDaysInMonth < 32)
            {
                report1.HideColumn32();
            }


            report1.DataSource = refinedDataTable;
            report1.DataMember = "AttendanceReport";

            // append Weekday in header
            //int index=3;
            //DateTime startDate = payrollPeriod.StartDateEng.Value;
            //for (int i = 1; i <= payrollPeriod.TotalDays; i++)
            //{
            //    report1.rowHeader.Cells[index].Text += "/" + startDate.DayOfWeek.ToString().Remove(1);
            //    report1.rowHeader.Cells[index].Visible = true;

            //    startDate = startDate.AddDays(1);
            //    index += 1;

               
            //}
            //for (int i = payrollPeriod.TotalDays.Value; i <= 32; i++)
            //    report1.rowHeader.Cells[i].Visible = false;
            
           

            report.DisplayReport(report1);

            if (ReportHelper.IsReportExportState())
            {
                this.report.clearCache = true;

            }

            report1.XRTitle.Text = "Attendance Register Summary of " + payrollPeriod.Name;
            
            

        }
        protected void LoadReportFromDeviceAttendanceEmployeeWithTimeTable()
        {
            ReportDataSetTableAdapters.AttendanceGetMainReportNewWithTimeTableAdapter adap = new ReportDataSetTableAdapters.AttendanceGetMainReportNewWithTimeTableAdapter();

            Report.Templates.HR.ReportAttendanceEmpMontlySummaryWithTime report1 = new Report.Templates.HR.ReportAttendanceEmpMontlySummaryWithTime();

            report1.HideColumns();

            PayrollPeriod payrollPeriod = CommonManager.GetPayrollPeriod(report.Filter.StartDate.Month,
                                                                     report.Filter.StartDate.Year);

            if (payrollPeriod == null)
            {
                divWarningMsg.InnerHtml = "Payroll period of " + DateHelper.GetMonthName(report.Filter.StartDate.Month, IsEnglish) + " does not exists.";
                divWarningMsg.Hide = false;
                return;
            }

            if (BLL.BaseBiz.PayrollDataContext.AttendanceEmployeeDates.Any(x => payrollPeriod.StartDateEng >= x.LastUptoDate.Value.Date && payrollPeriod.StartDateEng <= x.LastDate) == false
                &&
                BLL.BaseBiz.PayrollDataContext.AttendanceEmployeeDates.Any(x => payrollPeriod.EndDateEng >= x.LastUptoDate.Value.Date && payrollPeriod.EndDateEng <= x.LastDate) == false)
            {
                divWarningMsg.InnerHtml = "Attendance data does not exists, please generate the device attendance data of " + payrollPeriod.Name + " from <a href='../newhr/GenerateAttendanceReportData.aspx'>Generate Attendance</a> for the result.";
                divWarningMsg.Hide = false;
                //return;
            }

            // Set for DBDefence
            BLL.BaseBiz.SetConnectionPwd(adap.Connection);

            ReportDataSet.AttendanceGetMainReportNewWithTimeDataTable refinedDataTable = new ReportDataSet.AttendanceGetMainReportNewWithTimeDataTable();
            refinedDataTable = adap.GetData(report.Filter.EmployeeId, payrollPeriod.PayrollPeriodId, report.Filter.BranchId);

            int AbsCount = 0;
            int day = 28;
            int totalDaysInMonth = 31;
            totalDaysInMonth = attendanceManager.GetTotalDaysInMonth(payrollPeriod.PayrollPeriodId);
            if (totalDaysInMonth < 29)
            {
                report1.HideColumn29();
                report1.HideColumn30();
                report1.HideColumn31();
                report1.HideColumn32();
            }
            else if (totalDaysInMonth < 30)
            {
                report1.HideColumn30();
                report1.HideColumn31();
                report1.HideColumn32();
            }
            else if (totalDaysInMonth < 31)
            {
                report1.HideColumn31();
                report1.HideColumn32();
            }
            else if (totalDaysInMonth < 32)
            {
                report1.HideColumn32();
            }



            foreach (var val1 in refinedDataTable)
            {
                AbsCount = 0;
                if (val1.D1 == null || string.IsNullOrEmpty(val1.D1.Trim()) || val1.D1.Trim().ToLower().Contains("-a"))
                    AbsCount++;
                if (val1.D3 == null || string.IsNullOrEmpty(val1.D2.Trim()) || val1.D2.Trim().ToLower().Contains("-a"))
                    AbsCount++;
                if (val1.D3 == null || string.IsNullOrEmpty(val1.D3.Trim()) || val1.D3.Trim().ToLower().Contains("-a"))
                    AbsCount++;
                if (val1.D4 == null || string.IsNullOrEmpty(val1.D4.Trim()) || val1.D4.Trim().ToLower().Contains("-a"))
                    AbsCount++;
                if (val1.D5 == null || string.IsNullOrEmpty(val1.D5.Trim()) || val1.D5.Trim().ToLower().Contains("-a"))
                    AbsCount++;

                if (val1.D6 == null || string.IsNullOrEmpty(val1.D6.Trim()) || val1.D6.Trim().ToLower().Contains("-a"))
                    AbsCount++;
                if (val1.D7 == null || string.IsNullOrEmpty(val1.D7.Trim()) || val1.D7.Trim().ToLower().Contains("-a"))
                    AbsCount++;
                if (val1.D8 == null || string.IsNullOrEmpty(val1.D8.Trim()) || val1.D8.Trim().ToLower().Contains("-a"))
                    AbsCount++;
                if (val1.D9 == null || string.IsNullOrEmpty(val1.D9.Trim()) || val1.D9.Trim().ToLower().Contains("-a"))
                    AbsCount++;
                if (val1.D10 == null || string.IsNullOrEmpty(val1.D10.Trim()) || val1.D10.Trim().ToLower().Contains("-a"))
                    AbsCount++;

                if (val1.D11 == null || string.IsNullOrEmpty(val1.D11.Trim()) || val1.D11.Trim().ToLower().Contains("-a"))
                    AbsCount++;
                if (val1.D12 == null || string.IsNullOrEmpty(val1.D12.Trim()) || val1.D12.Trim().ToLower().Contains("-a"))
                    AbsCount++;
                if (val1.D13 == null || string.IsNullOrEmpty(val1.D13.Trim()) || val1.D13.Trim().ToLower().Contains("-a"))
                    AbsCount++;
                if (val1.D14 == null || string.IsNullOrEmpty(val1.D14.Trim()) || val1.D14.Trim().ToLower().Contains("-a"))
                    AbsCount++;
                if (val1.D15 == null || string.IsNullOrEmpty(val1.D15.Trim()) || val1.D15.Trim().ToLower().Contains("-a"))
                    AbsCount++;


                if (val1.D16 == null || string.IsNullOrEmpty(val1.D16.Trim()) || val1.D16.Trim().ToLower().Contains("-a"))
                    AbsCount++;
                if (val1.D17 == null || string.IsNullOrEmpty(val1.D17.Trim()) || val1.D17.Trim().ToLower().Contains("-a"))
                    AbsCount++;
                if (val1.D18 == null || string.IsNullOrEmpty(val1.D18.Trim()) || val1.D18.Trim().ToLower().Contains("-a"))
                    AbsCount++;
                if (val1.D19 == null || string.IsNullOrEmpty(val1.D19.Trim()) || val1.D19.Trim().ToLower().Contains("-a"))
                    AbsCount++;
                if (val1.D20 == null || string.IsNullOrEmpty(val1.D20.Trim()) || val1.D20.Trim().ToLower().Contains("-a"))
                    AbsCount++;

                if (val1.D21 == null || string.IsNullOrEmpty(val1.D21.Trim()) || val1.D21.Trim().ToLower().Contains("-a"))
                    AbsCount++;
                if (val1.D22 == null || string.IsNullOrEmpty(val1.D22.Trim()) || val1.D22.Trim().ToLower().Contains("-a"))
                    AbsCount++;
                if (val1.D23 == null || string.IsNullOrEmpty(val1.D23.Trim()) || val1.D23.Trim().ToLower().Contains("-a"))
                    AbsCount++;
                if (val1.D24 == null || string.IsNullOrEmpty(val1.D24.Trim()) || val1.D24.Trim().ToLower().Contains("-a"))
                    AbsCount++;
                if (val1.D25 == null || string.IsNullOrEmpty(val1.D25.Trim()) || val1.D25.Trim().ToLower().Contains("-a"))
                    AbsCount++;

                if (val1.D26 == null || string.IsNullOrEmpty(val1.D26.Trim()) || val1.D26.Trim().ToLower().Contains("-a"))
                    AbsCount++;
                if (val1.D27 == null || string.IsNullOrEmpty(val1.D27.Trim()) || val1.D27.Trim().ToLower().Contains("-a"))
                    AbsCount++;

                if (totalDaysInMonth >= 28)
                {
                    if (val1.D28 == null || string.IsNullOrEmpty(val1.D28.Trim()) || val1.D28.Trim().ToLower().Contains("-a"))
                        AbsCount++;
                }

                if (totalDaysInMonth >= 29)
                {
                    if (val1.D29 == null || string.IsNullOrEmpty(val1.D29.Trim()) || val1.D29.Trim().ToLower().Contains("-a"))
                        AbsCount++;
                }

                if (totalDaysInMonth >= 30)
                {
                    if (val1.D30 == null || string.IsNullOrEmpty(val1.D30.Trim()) || val1.D30.Trim().ToLower().Contains("-a"))
                        AbsCount++;
                }

                if (totalDaysInMonth >= 31)
                {
                    if (val1.D31 == null || string.IsNullOrEmpty(val1.D31.Trim()) || val1.D31.Trim().ToLower().Contains("-a"))
                        AbsCount++;
                }

                if (totalDaysInMonth >= 32)
                {
                    if (val1.D32 == null || string.IsNullOrEmpty(val1.D32.Trim()) || val1.D32.Trim().ToLower().Contains("-a"))
                        AbsCount++;
                }

                val1.absc = AbsCount;

            }

            report1.DataSource = refinedDataTable;
            report1.DataMember = "AttendanceReport";

            report.DisplayReport(report1);

            if (ReportHelper.IsReportExportState())
            {
                this.report.clearCache = true;
            }

            report1.XRTitle.Text = "Attendance Device Summary of " + payrollPeriod.Name;

        }

      
    }
}
