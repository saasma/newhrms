﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using BLL.Manager;
using Utils.Base;
using BLL;
using DAL;
using Utils.Web;
using Utils.Helper;
using System.Text.RegularExpressions;
using Ext.Net;
using Utils.Calendar;
using DevExpress.XtraTreeList.StyleFormatConditions;
using DevExpress.XtraGrid;
//using DevExpress.Web.ASPxGridView;
//using DevExpress.Web.ASPxEditors;
using DevExpress.Web;


namespace Web.CP
{
    public partial class AttendanceDailyReportOld : BasePage
    {
        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.LeaveAndAttendance;
            }
        }

        AttendanceManager attendanceManager = new AttendanceManager();


        public List<AttendanceAllEmployeeReportResult> DataList(int Status)
        {
            List<AttendanceAllEmployeeReportResult> list = null;
            if (Cache["AtteList"] != null)
            {
                list = Cache["AtteList"] as List<AttendanceAllEmployeeReportResult>;
            }
            else
            {
                DateTime selectedDate = date.SelectedDate.EnglishDate;

                if (chkIsEnglish.Checked)
                    selectedDate = engDate.SelectedDate.EnglishDate;

                list =
                AttendanceManager.GetAttendanceDailyReport(selectedDate, 0, "");
                Cache["AtteList"] = list;
            }


            if (Status != 0 && Status != 4)
            {

                return list.Where(x => x.AtteState == Status).ToList();

            }
            else if (Status == 4)
            {
                return list.Where(x => x.AtteState == Status || x.AtteState == 1).ToList();
            }
            else// if (Status == 0)
                return list;

            //return list;
        }


        protected void Page_Load(object sender, EventArgs e)
        {

            //!X.IsAjaxRequest||
            if (!IsPostBack )
            {
                if (IsEnglish)
                {
                    chkIsEnglish.Visible = false;
                    engDate.Visible = false;
                }

                Hidden_AtteStatus.Value = "0";
                date.IsEnglishCalendar = this.IsEnglish;

                date.SelectTodayDate();
                engDate.IsEnglishCalendar = true;
                engDate.SelectTodayDate();

                

                if (SessionManager.User.IsEnglishPreferred != null && SessionManager.User.IsEnglishPreferred)
                {
                    chkIsEnglish.Checked = true;
                    engDate.Enabled = true;
                    date.Enabled = false;
                }

              

                Cache.Remove("AtteList");
            }

            btnTotalEmp.ControlStyle.BackColor = System.Drawing.Color.White;
            btnTotalEmp.ControlStyle.ForeColor = System.Drawing.Color.Black;

            btnTImelyAttendees.ControlStyle.BackColor = System.Drawing.Color.LightGreen;
            btnTImelyAttendees.ControlStyle.ForeColor = System.Drawing.Color.Black;

            btnLateAttendees.ControlStyle.BackColor = System.Drawing.Color.Khaki;
            btnLateAttendees.ControlStyle.ForeColor = System.Drawing.Color.Black;

            btnOnLeave.ControlStyle.BackColor = System.Drawing.Color.Pink;
            btnOnLeave.ControlStyle.ForeColor = System.Drawing.Color.Black;

            btnAbsentees.ControlStyle.BackColor = System.Drawing.ColorTranslator.FromHtml("#FA9393");
            btnAbsentees.ControlStyle.ForeColor = System.Drawing.Color.Black;

            // JavascriptHelper.AttachEnableDisablingJSCode(chkApplyOnceFor, tblStatuesContainer.ClientID, false,"applyOnceStateChanged");
            JavascriptHelper.AttachEnableDisablingJSCode(chkIsEnglish, engDate.ClientID + "_date", false, "StateChanged");


            LoadGrid();
            ASPxGridView1.SettingsPager.AlwaysShowPager = true;

            if (!IsPostBack)
            {
                ASPxGridView1.SettingsPager.PageSize = 100;
            }

            if (chkIsEnglish.Checked)
            {
                engDate.Enabled = true;
                date.Enabled = false;
            }
            else
            {
                engDate.Enabled = false;
                date.Enabled = true;
            }
        }

        protected void btnXlsExport_Click(object sender, EventArgs e)
        {

            //ASPxGridView1.Columns[5].Visible = false;
            //ASPxGridView1.Columns[6].Visible = true;
            //ASPxGridView1.Columns[7].Visible = false;
            //ASPxGridView1.Columns[8].Visible = true;



            ASPxGridViewExporter1.WriteXlsxToResponse("Attendance details " + date.SelectedDate.ToString());

            //ASPxGridView1.Columns[5].Visible = true;
            //ASPxGridView1.Columns[6].Visible = false;
            //ASPxGridView1.Columns[7].Visible = true;
            //ASPxGridView1.Columns[8].Visible = false;
        }

        public void LoadGrid()
        {


            List<AttendanceAllEmployeeReportResult> bindList = new List<AttendanceAllEmployeeReportResult>();
            int totalEmployee = 0;
            int timelyAttendees = 0;
            int lateAttendees = 0;
            int leaveEmployee = 0;
            int absentEmployee = 0;

            int atteStatus = int.Parse(Hidden_AtteStatus.Value);

            bindList = DataList(0);
            totalEmployee = bindList.Count();
            foreach (var val1 in bindList)
            {
                if (val1.AtteState == 10)
                    timelyAttendees++;
                else if (val1.AtteState == 11)
                    lateAttendees++;
                else if (val1.AtteState == 4 || val1.AtteState == 1)
                    leaveEmployee++;
                else if (val1.AtteState == 9)
                    absentEmployee++;
            }


            lblToatalEmp.Text = totalEmployee.ToString();
            lblTimelyCheckIn.Text = timelyAttendees.ToString();
            lblLateCheckIn.Text = lateAttendees.ToString();
            lblOnLeave.Text = leaveEmployee.ToString();
            lblAbsents.Text = absentEmployee.ToString();



            bindList = DataList(atteStatus);
            DrawBorder(atteStatus);

            //DrawBorder(10);

            ASPxGridView1.DataSource = bindList;
            ASPxGridView1.DataBind();

           
           
        }


      

        protected void ASPxGridView1_HtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e)
        {
            e.Row.ForeColor = System.Drawing.Color.Black;
            if (e.RowType != GridViewRowType.Data) return;
            int AtteState = Convert.ToInt32(e.GetValue("AtteState"));
            if (AtteState == 10)
            {
                e.Row.BackColor = System.Drawing.Color.LightGreen;
            }

            if (AtteState == 11)
            {
                e.Row.BackColor = System.Drawing.Color.Khaki;
            }

            if (AtteState == 9)
            {

                e.Row.BackColor = System.Drawing.ColorTranslator.FromHtml("#FA9393");
            }

            if (AtteState == 4 || AtteState == 1)
            {
                e.Row.BackColor = System.Drawing.Color.Pink;
            }

           

        }

        protected void ASPxGridView1_CustomColumnDisplayText(object sender, ASPxGridViewColumnDisplayTextEventArgs e)
        {
            
            int cellvalue=0;
            string Time;
            if (e.Column.FieldName == "InRemarks")
            {
                if (e.Value != null)
                {
                    cellvalue = int.Parse((e.Value).ToString());
                    e.DisplayText = attendanceManager.CheckInOutTimeCheck(1, cellvalue);
                }
            }

            if (e.Column.FieldName == "OutRemarks")
            {
                if (e.Value != null)
                {
                    cellvalue = int.Parse((e.Value).ToString());
                    e.DisplayText = attendanceManager.CheckInOutTimeCheck(2, cellvalue);
                }
            }

            if (e.Column.FieldName == "InTime" || e.Column.FieldName == "OutTime" || e.Column.FieldName == "LunchIn" || e.Column.FieldName == "LunchOut")
            {
                if (e.Value != null)
                {
                    Time = Convert.ToDateTime(((e.Value).ToString())).ToString("hh:mm tt");
                    e.DisplayText = Time;
                }
            }

            
        }

        

        protected void ASPxGridView1_HeaderFilterFillItems(object sender, ASPxGridViewHeaderFilterEventArgs e)
        {
            if (e.Column.FieldName == "InRemarks")
            {

                e.Values.RemoveAll(filterValue=> true);

                e.AddShowAll();

                e.AddValue("Timely In", string.Empty, "[InRemarks] == 0");

                e.AddValue("In Late", string.Empty, "[InRemarks] > 0");

                e.AddValue("In Early", string.Empty, "[InRemarks] < 0 and [InRemarks] != null");

                
            }

            if (e.Column.FieldName == "OutRemarks")
            {
                e.Values.RemoveAll(filterValue => true);

                e.AddShowAll();

                e.AddValue("Timely Out", string.Empty, "[OutRemarks] == 0");

                e.AddValue("Late Out", string.Empty, "[OutRemarks] > 0");

                e.AddValue("Early Out", string.Empty, "[OutRemarks] < 0 and [OutRemarks] != null ");
            }
        }

        protected void ASPxGridView1_CustomUnboundColumnData(object sender, ASPxGridViewColumnDataEventArgs e)
        {
            if (e.Column.FieldName == "SN")
            { 
                e.Value = string.Format("{0} .", (e.ListSourceRowIndex)+1);
            }
        }

        protected void ASPxButton1_Click(object sender, EventArgs e)
        {
            List<AttendanceAllEmployeeReportResult> bindList = new List<AttendanceAllEmployeeReportResult>();
            DateTime date = this.date.SelectedDate.EnglishDate;
            //X.Msg.Alert("", date.ToString()).Show();
            Hidden_AtteStatus.Value = "0";
            Cache.Remove("AtteList");
           

            LoadGrid();
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            btnTotalEmp.CssClass = "";
            btnTImelyAttendees.CssClass = "";
            btnLateAttendees.CssClass = "";
            btnOnLeave.CssClass = "";
            btnAbsentees.CssClass = "";

            if (Hidden_AtteStatus.Value == "0")
            {
                btnTotalEmp.CssClass = "selectedButton";
            }

            if (Hidden_AtteStatus.Value == "10")
            {
                btnTImelyAttendees.CssClass = "selectedButton";
            }

            if (Hidden_AtteStatus.Value == "11")
            {
                btnLateAttendees.CssClass = "selectedButton";

            }

            if (Hidden_AtteStatus.Value == "4")
            {
                btnOnLeave.CssClass = "selectedButton";

            }

            if (Hidden_AtteStatus.Value == "9")
            {
                btnAbsentees.CssClass = "selectedButton";

            }
        }

        protected void btnFilter_Click(object sender, EventArgs e)
        {
          
              
            List<AttendanceAllEmployeeReportResult> bindList = new List<AttendanceAllEmployeeReportResult>();
            string filterButtonID = ((System.Web.UI.WebControls.LinkButton)sender).ID;
            
            DateTime date = this.date.SelectedDate.EnglishDate;

            if (filterButtonID == "btnTotalEmp")
            {
                bindList = DataList(0);
                Hidden_AtteStatus.Value = "0";

            }

            if (filterButtonID == "btnTImelyAttendees")
            {
                bindList = DataList(10);
                Hidden_AtteStatus.Value = "10";
                
            }

            if (filterButtonID == "btnLateAttendees")
            {
                bindList = DataList(11);
                Hidden_AtteStatus.Value = "11";
                
            }

            if (filterButtonID == "btnOnLeave")
            {
                bindList = DataList(4);
                Hidden_AtteStatus.Value = "4";
                
            }

            if (filterButtonID == "btnAbsentees")
            {
                bindList = DataList(9);
                Hidden_AtteStatus.Value = "9";
                
            }
            

            ASPxGridView1.DataSource = bindList;
            ASPxGridView1.DataBind();// = "AttendanceAllEmployeeReport";

        }



        public void DrawBorder(int nextcontrol)
        {
            int currentControl = int.Parse(Hidden_AtteStatus.Value);
            int nextControl = nextcontrol;

            

            if (nextControl == 0)
            {
                btnTotalEmp.Style.Add(HtmlTextWriterStyle.BorderStyle, "1px solid #0000FF");
                btnTImelyAttendees.CssClass = btnTImelyAttendees.CssClass.Replace("SelectedBtn", "");
                btnLateAttendees.CssClass = btnLateAttendees.CssClass.Replace("SelectedBtn", "");
                btnOnLeave.CssClass = btnOnLeave.CssClass.Replace("SelectedBtn", "");                
                btnAbsentees.CssClass = btnOnLeave.CssClass.Replace("SelectedBtn", "");

            }
            else if (nextControl == 10)
            {
                btnTotalEmp.Style.Add(HtmlTextWriterStyle.BorderStyle, "1px solid #0000FF");
                btnTotalEmp.CssClass = btnTotalEmp.CssClass.Replace("SelectedBtn", "");
                btnLateAttendees.CssClass = btnLateAttendees.CssClass.Replace("SelectedBtn", "");
                btnOnLeave.CssClass = btnOnLeave.CssClass.Replace("SelectedBtn", "");                
                btnAbsentees.CssClass = btnOnLeave.CssClass.Replace("SelectedBtn", "");
            }
            else if (nextControl == 11)
            {
                btnTotalEmp.Style.Add(HtmlTextWriterStyle.BorderStyle, "1px solid #0000FF");
                btnTImelyAttendees.CssClass = btnTImelyAttendees.CssClass.Replace("SelectedBtn", "");
                btnTotalEmp.CssClass = btnTotalEmp.CssClass.Replace("SelectedBtn", "");
                btnOnLeave.CssClass = btnOnLeave.CssClass.Replace("SelectedBtn", "");                
                btnAbsentees.CssClass = btnOnLeave.CssClass.Replace("SelectedBtn", "");
            }
            else if (nextControl == 4)
            {
                btnTotalEmp.Style.Add(HtmlTextWriterStyle.BorderStyle, "1px solid #0000FF");
                btnTImelyAttendees.CssClass = btnTImelyAttendees.CssClass.Replace("SelectedBtn", "");
                btnLateAttendees.CssClass = btnLateAttendees.CssClass.Replace("SelectedBtn", "");
                btnTotalEmp.CssClass = btnTotalEmp.CssClass.Replace("SelectedBtn", "");                
                btnAbsentees.CssClass = btnOnLeave.CssClass.Replace("SelectedBtn", "");
            }
            else if (nextControl == 9)
            {
                btnTImelyAttendees.CssClass = btnTImelyAttendees.CssClass.Replace("SelectedBtn", "");
                btnLateAttendees.CssClass = btnLateAttendees.CssClass.Replace("SelectedBtn", "");
                btnOnLeave.CssClass = btnOnLeave.CssClass.Replace("SelectedBtn", "");
                btnTotalEmp.CssClass = btnTotalEmp.CssClass.Replace("SelectedBtn", "");
            }



        }

        const string PageSizeSessionKey = "ed5e843d-cff7-47a7-815e-832923f7fb09";

        protected int GridPageSize
        {
            get
            {
                if (Session[PageSizeSessionKey] == null)
                    return ASPxGridView1.SettingsPager.PageSize;
                return (int)Session[PageSizeSessionKey];
            }
            set { Session[PageSizeSessionKey] = value; }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            ASPxGridView1.SettingsPager.PageSize = GridPageSize;
        }

        protected void Grid_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
        {
            GridPageSize = int.Parse(e.Parameters);
            ASPxGridView1.SettingsPager.PageSize = GridPageSize;
            ASPxGridView1.DataBind();

            
        }

        protected void PagerCombo_Load(object sender, EventArgs e)
        {
            (sender as ASPxComboBox).Value = ASPxGridView1.SettingsPager.PageSize;
        }





    }
}
