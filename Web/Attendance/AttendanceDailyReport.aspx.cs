﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using Utils.Helper;
using System.IO;
using Utils;
using Utils.Security;
using Utils.Calendar;
using Web.Helper;
using System.Collections;

namespace Web.Attendance
{
    public partial class AttendanceDailyReport : BasePage
    {
        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.LeaveAndAttendance;
            }
        }

        AttendanceManager attendanceManager = new AttendanceManager();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Initialise();
            }


        }

        private void Initialise()
        {
            if (IsEnglish)
            {
                calNepDate.Hide();
            }

            Hidden_AtteStatus.Value = "0";

            CustomDate cdToday = CustomDate.GetTodayDate(false);
            calNepDate.Text = cdToday.ToString();
            CustomDate cdTodayEng = CustomDate.GetTodayDate(true);
            calEngDate.Text = cdTodayEng.ToString();

            calNepDate.Enable();
            calEngDate.Disable();
            chkEnglish.Checked = false;

            if (SessionManager.User.IsEnglishPreferred != null && SessionManager.User.IsEnglishPreferred)
            {
                calNepDate.Disable();
                calEngDate.Enable();
                chkEnglish.Checked = true;
            }

            BindBranchComboBox();

            Cache.Remove("AtteList");
        }

        private void BindBranchComboBox()
        {
            cmbBranch.Store[0].DataSource = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId);
            cmbBranch.Store[0].DataBind();
        }

        public List<AttendanceAllEmployeeReportNewResult> DataList(int Status)
        {
            string branchID = "";

            if (cmbBranch.SelectedItem != null && cmbBranch.SelectedItem.Value != null)
                branchID = cmbBranch.SelectedItem.Value;

            List<AttendanceAllEmployeeReportNewResult> list = null;
            DateTime selectedDate = DateTime.Today;
                
            if(chkEnglish.Checked)
                selectedDate = calEngDate.SelectedDate;
            else
                selectedDate = GetEngDate(calNepDate.Text);

            list = AttendanceManager.GetAttendanceDailyReportNew(selectedDate.Date, 0, branchID);

            //if (Status != 0 && Status != 4)
            //{

            //    return list.Where(x => x.AtteState == Status).ToList();

            //}
            //else if (Status == 4)
            //{
            //    return list.Where(x => x.AtteState == Status || x.AtteState == 1).ToList();
            //}
            //else// if (Status == 0)
            //    return list;


            //return list;

            //status = 0 Total
            if (Status != 0)
            {
                if (Status == 11) //At Work
                    return list.Where(x => (x.AtteState != 4 && x.AtteState != 9)).ToList();
                else if (Status == 12) //On Leave
                    return list.Where(x => x.AtteState == 4).ToList();
                else if (Status == 13) //Absent
                    return list.Where(x => x.AtteState == 9).ToList();
                else if (Status == 14) //Timely In
                    return list.Where(x => ((x.AtteState == 10 || x.AtteState == 11) && x.InState == 1)).ToList();
                else if (Status == 15) //Early In
                    return list.Where(x => ((x.AtteState == 10 || x.AtteState == 11) && x.InState == 2)).ToList();
                else if (Status == 16) //Late In
                    return list.Where(x => ((x.AtteState == 10 || x.AtteState == 11) && x.InState == 3)).ToList();
                else if (Status == 17) //Timely Out    
                    return list.Where(x => ((x.AtteState == 10 || x.AtteState == 11) && x.OutState == 1)).ToList();
                else if (Status == 18) //Early Out
                    return list.Where(x => ((x.AtteState == 10 || x.AtteState == 11) && x.OutState == 3)).ToList();
                else if (Status == 19) //Late Out
                    return list.Where(x => ((x.AtteState == 10 || x.AtteState == 11) && x.OutState == 2)).ToList();
            }

            return list;
        }

        protected void Store_ReadData(object sender, StoreReadDataEventArgs e)
        {
            int totalRecords = 0;

            List<AttendanceAllEmployeeReportNewResult> bindList = new List<AttendanceAllEmployeeReportNewResult>();
            int totalEmployee = 0;
            int timelyAttendees = 0;
            int lateAttendees = 0;
            int leaveEmployee = 0;
            int absentEmployee = 0;
            int earlyIn = 0, timelyIn = 0, lateIn = 0, ealryOut = 0, timelyOut = 0, lateOut = 0;

            int atteStatus = int.Parse(Hidden_AtteStatus.Text);

            bindList = DataList(0);
            totalEmployee = bindList.Count();
            foreach (var val1 in bindList)
            {
                if (val1.AtteState == 4 || val1.AtteState == 1)
                    leaveEmployee++;
                else if (val1.AtteState == 9)
                    absentEmployee++;
                else
                {
                    int inState = (val1.InState == null ? 0 : val1.InState.Value);
                    int outState = (val1.OutState == null ? 0 : val1.OutState.Value);
                    
                    if (inState == 1)
                        timelyIn++;
                    else if (inState == 2)
                        earlyIn++;
                    else if (inState == 3)
                        lateIn++;

                    if (outState == 1)
                        timelyOut++;
                    else if (outState == 2)
                        lateOut++;
                    else if (outState == 3)
                        ealryOut++;
                }
            }

            btnTotal.Text = totalEmployee.ToString();
            btnAbsent.Text = absentEmployee.ToString();
            btnOnLeave.Text = leaveEmployee.ToString();
            btnAtWork.Text = (totalEmployee - absentEmployee - leaveEmployee).ToString();

            btnTimelyIn.Text = timelyIn.ToString();
            btnEarlyIn.Text = earlyIn.ToString();
            btnLateIn.Text = lateIn.ToString();

            btnTimelyOut.Text = timelyOut.ToString();
            btnEarlyOut.Text = ealryOut.ToString();
            btnLateOut.Text = lateOut.ToString();

            bindList = DataList(atteStatus);

            //-- start filtering -----------------------------------------------------------
            FilterHeaderConditions fhc = new FilterHeaderConditions(e.Parameters["filterheader"]);

            foreach (FilterHeaderCondition condition in fhc.Conditions)
            {
                string dataIndex = condition.DataIndex;
                FilterType type = condition.Type;
                string op = condition.Operator;
                object value = null;

                switch (condition.Type)
                {
                    case FilterType.Boolean:
                        value = condition.Value<bool>();
                        break;

                    case FilterType.Date:
                        switch (condition.Operator)
                        {
                            case "=":
                                value = condition.Value<DateTime>();
                                break;

                            case "compare":
                                value = FilterHeaderComparator<DateTime>.Parse(condition.JsonValue);
                                break;
                        }
                        break;
                    case FilterType.String:
                        value = condition.Value<string>();
                        break;
                    case FilterType.Numeric:
                        value = condition.Value<int>();
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }

                bindList.RemoveAll(item =>
                {
                    object oValue = item.GetType().GetProperty(dataIndex).GetValue(item, null);
                    string matchValue = null;
                    string itemValue = null;

                    if (type == FilterType.String)
                    {
                        matchValue = (string)value;

                        if (dataIndex == "EmployeeId")
                            itemValue = oValue.ToString();
                        else
                            itemValue = oValue as string;

                        matchValue = matchValue.ToLower();
                        itemValue = itemValue.ToLower();
                    }

                    switch (op)
                    {
                        case "=":
                            return oValue == null || !oValue.Equals(value);
                        case "compare":
                            return !((IEquatable<IComparable>)value).Equals((IComparable)oValue);
                        case "+":
                            return itemValue == null || !itemValue.StartsWith(matchValue);
                        case "-":
                            return itemValue == null || !itemValue.EndsWith(matchValue);
                        case "!":
                            return itemValue == null || itemValue.IndexOf(matchValue) >= 0;
                        case "*":
                            return itemValue == null || itemValue.IndexOf(matchValue) < 0;
                        default:
                            throw new Exception("Not supported operator");
                    }
                });
            }
            //-- end filtering ------------------------------------------------------------

            //-- start sorting ------------------------------------------------------------

            if (e.Sort.Length > 0)
            {
                bindList.Sort(delegate(AttendanceAllEmployeeReportNewResult obj1, AttendanceAllEmployeeReportNewResult obj2)
                {
                    object a;
                    object b;

                    int direction = e.Sort[0].Direction == Ext.Net.SortDirection.DESC ? -1 : 1;

                    a = obj1.GetType().GetProperty(e.Sort[0].Property).GetValue(obj1, null);
                    b = obj2.GetType().GetProperty(e.Sort[0].Property).GetValue(obj2, null);
                    return CaseInsensitiveComparer.Default.Compare(a, b) * direction;
                });
            }


            int pageSize = int.Parse(cmbPageSize.SelectedItem.Value);

            Session["ExportData"] = bindList;

            gridAttReport.Store[0].DataSource = bindList.Skip(e.Start - 1).Take(pageSize);
            gridAttReport.Store[0].DataBind();

            List<EmpAttendanceReportColumn> listEmpAttendanceReportColumn = AttendanceManager.GetEmpAttendanceReportColumn();

            if (listEmpAttendanceReportColumn.Count > 0)
            {
                if(listEmpAttendanceReportColumn.Any(x => x.ColumnName == "colEmployeeId"))
                    colEmployeeId.Show();
                else
                    colEmployeeId.Hide();

                if (listEmpAttendanceReportColumn.Any(x => x.ColumnName == "colEmplooyeeName"))
                    colEmplooyeeName.Show();
                else
                    colEmplooyeeName.Hide();

                if (listEmpAttendanceReportColumn.Any(x => x.ColumnName == "colBranch"))
                    colBranch.Show();
                else
                    colBranch.Hide();

                if (listEmpAttendanceReportColumn.Any(x => x.ColumnName == "colDepartment"))
                    colDepartment.Show();
                else
                    colDepartment.Hide();

                if (listEmpAttendanceReportColumn.Any(x => x.ColumnName == "colInTime"))
                    colInTime.Show();
                else
                    colInTime.Hide();

                if (listEmpAttendanceReportColumn.Any(x => x.ColumnName == "colInNote"))
                    colInNote.Show();
                else
                    colInNote.Hide();

                if (listEmpAttendanceReportColumn.Any(x => x.ColumnName == "colOutTime"))
                    colOutTime.Show();
                else
                    colOutTime.Hide();

                if (listEmpAttendanceReportColumn.Any(x => x.ColumnName == "colOutNote"))
                    colOutNote.Show();
                else
                    colOutNote.Hide();

                if (listEmpAttendanceReportColumn.Any(x => x.ColumnName == "colLunchOut"))
                    colLunchOut.Show();
                else
                    colLunchOut.Hide();

                if (listEmpAttendanceReportColumn.Any(x => x.ColumnName == "colLunchIn"))
                    colLunchIn.Show();
                else
                    colLunchIn.Hide();

                if (listEmpAttendanceReportColumn.Any(x => x.ColumnName == "colRefinedWorkHour"))
                    colRefinedWorkHour.Show();
                else
                    colRefinedWorkHour.Hide();

                if (listEmpAttendanceReportColumn.Any(x => x.ColumnName == "colLeaveName"))
                    colLeaveName.Show();
                else
                    colLeaveName.Hide();
            }

            if (bindList.Count > 0)
                e.Total = bindList.Count();
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            if (Session["ExportData"] == null)
                return;

            List<AttendanceAllEmployeeReportNewResult> list = (List<AttendanceAllEmployeeReportNewResult>)Session["ExportData"];

            List<string> hiddenColumnList = new List<string>();
            hiddenColumnList = new List<string> { "WorkHour", "RefinedInRemarks", "RefinedOutRemarks", "RefinedLeave", "AtteState", "InState", "OutState", "InRemarksText",
            "OutRemarksText", "Type", "DayValue", "InRemarks", "OutRemarks", "EmployeeId", "InTime", "OutTime", "LunchIn", "LunchOut", "BranchId"};

            List<EmpAttendanceReportColumn> listEmpAttendanceReportColumn = AttendanceManager.GetEmpAttendanceReportColumn();

            if (listEmpAttendanceReportColumn.Count > 0)
            {
                if (!listEmpAttendanceReportColumn.Any(x => x.ColumnName == "colEmployeeId"))
                    hiddenColumnList.Add("EmployeeId");

                if (!listEmpAttendanceReportColumn.Any(x => x.ColumnName == "colEmplooyeeName"))
                    hiddenColumnList.Add("EmplooyeeName");

                if (!listEmpAttendanceReportColumn.Any(x => x.ColumnName == "colBranch"))
                    hiddenColumnList.Add("Branch");

                if (!listEmpAttendanceReportColumn.Any(x => x.ColumnName == "colDepartment"))
                    hiddenColumnList.Add("Department");

                if (!listEmpAttendanceReportColumn.Any(x => x.ColumnName == "colInTime"))
                    hiddenColumnList.Add("InTimeString");

                if (!listEmpAttendanceReportColumn.Any(x => x.ColumnName == "colInNote"))
                    hiddenColumnList.Add("InNote");

                if (!listEmpAttendanceReportColumn.Any(x => x.ColumnName == "colOutTime"))
                    hiddenColumnList.Add("OutTimeString");

                if (!listEmpAttendanceReportColumn.Any(x => x.ColumnName == "colOutNote"))
                    hiddenColumnList.Add("OutNote");

                if (!listEmpAttendanceReportColumn.Any(x => x.ColumnName == "colLunchOut"))
                    hiddenColumnList.Add("LunchOutString");

                if (!listEmpAttendanceReportColumn.Any(x => x.ColumnName == "colLunchIn"))
                    hiddenColumnList.Add("LunchInString");

                if (!listEmpAttendanceReportColumn.Any(x => x.ColumnName == "colRefinedWorkHour"))
                    hiddenColumnList.Add("RefinedWorkHour");

                if (!listEmpAttendanceReportColumn.Any(x => x.ColumnName == "colLeaveName"))
                    hiddenColumnList.Add("LeaveName");
            }

            Bll.ExcelHelper.ExportToExcel("Daily Attendance Report", list,
                   hiddenColumnList,
               new List<String>() { },
               new Dictionary<string, string>() { {"EmployeeId","EIN"}, {"EmplooyeeName","Name"}, 
                    {"InTimeString","Office In"}, {"InNote","In Note"}, { "OutTimeString", "Office Out" },
                    { "OutNote", "Out Note" }, { "LunchOutString", "Lunch Out" }, { "LunchInString", "Lunch In" }, 
                    { "RefinedWorkHour", "Work Hours" }, { "LeaveName", "Leave Name" } },
               new List<string>() { }
               , new List<string> { }
               , new List<string> { }
               , new Dictionary<string, string>() { }
               , new List<string> { "EmployeeId", "EmplooyeeName", "Branch", "Department", "InTimeString", "InNote", "OutTimeString", "OutNote", "LunchOutString", "LunchInString", "RefinedWorkHour", "LeaveName" });

            

        }

        protected Field OnCreateFilterableField(object sender, ColumnBase column, Field defaultField)
        {
            ((TextField)defaultField).Icon = Icon.Magnifier;

            return defaultField;
        }

        protected void btnExportHelper_Click(object sender, EventArgs e)
        {
            if (Session["expData"] == null)
                return;

            List<AttendanceAllEmployeeReportNewResult> list = (List<AttendanceAllEmployeeReportNewResult>)Session["expData"];

            Session["expData"] = null;

            List<string> hiddenColumnList = new List<string>();
            hiddenColumnList = new List<string> { "WorkHour", "RefinedInRemaks", "RefinedOutRemaks", "RefiredLeave", "AtteState", "InState", "OutState", "InRemarksText",
            "OutRemarksText", "Type", "DayValue"};



            Bll.ExcelHelper.ExportToExcel("Daily Attendance Report", list,
                   hiddenColumnList,
               new List<String>() { },
               new Dictionary<string, string>() { {"EmployeeId","EIN"}, {"EmplooyeeName","Name"}, 
                    {"InTime","Office In"}, {"InNote","In Note"}, { "OutTime", "Office Out" },
                    { "OutNote", "Out Note" }, { "LunchOut", "Lunch Out" }, { "LunchIn", "Lunch In" }, 
                    { "RefinedWorkHour", "Work Hours" }, { "LeaveName", "Leave Name" } },
               new List<string>() { }
               , new List<string> { }
               , new List<string> {}
               , new Dictionary<string, string>() { }
               , new List<string> { "EmployeeId", "EmplooyeeName", "Branch", "Department", "InTime", "InNote", "OutTime", "OutNote", "LunchOut", "LunchIn", "RefinedWorkHour", "LeaveName" });

            

        }

        protected void btnColWindow_Click(object sender, DirectEventArgs e)
        {

            chkEIN.Checked = true;
            chkEmplooyeeName.Checked = true;
            chkBranch.Checked = true;
            chkDepartment.Checked = true;
            chkInTime.Checked = true;
            chkInNote.Checked = true;
            chkOutTime.Checked = true;
            chkOutNote.Checked = true;
            chkLunchOut.Checked = true;
            chkLunchIn.Checked = true;
            chkRefinedWorkHour.Checked = true;
            chkLeaveName.Checked = true;

            List<EmpAttendanceReportColumn> listEmpAttendanceReportColumn = AttendanceManager.GetEmpAttendanceReportColumn();

            if (listEmpAttendanceReportColumn.Count > 0)
            {
                if (!listEmpAttendanceReportColumn.Any(x => x.ColumnName == "colEmployeeId"))
                    chkEIN.Checked = false;

                if (!listEmpAttendanceReportColumn.Any(x => x.ColumnName == "colEmplooyeeName"))
                    chkEmplooyeeName.Checked = false;

                if (!listEmpAttendanceReportColumn.Any(x => x.ColumnName == "colBranch"))
                    chkBranch.Checked = false;

                if (!listEmpAttendanceReportColumn.Any(x => x.ColumnName == "colDepartment"))
                    chkDepartment.Checked = false;

                if (!listEmpAttendanceReportColumn.Any(x => x.ColumnName == "colInTime"))
                    chkInTime.Checked = false;

                if (!listEmpAttendanceReportColumn.Any(x => x.ColumnName == "colInNote"))
                    chkInNote.Checked = false;

                if (!listEmpAttendanceReportColumn.Any(x => x.ColumnName == "colOutTime"))
                    chkOutTime.Checked = false;

                if (!listEmpAttendanceReportColumn.Any(x => x.ColumnName == "colOutNote"))
                    chkOutNote.Checked = false;

                if (!listEmpAttendanceReportColumn.Any(x => x.ColumnName == "colLunchOut"))
                    chkLunchOut.Checked = false;

                if (!listEmpAttendanceReportColumn.Any(x => x.ColumnName == "colLunchIn"))
                    chkLunchIn.Checked = false;

                if (!listEmpAttendanceReportColumn.Any(x => x.ColumnName == "colRefinedWorkHour"))
                    chkRefinedWorkHour.Checked = false;

                if (!listEmpAttendanceReportColumn.Any(x => x.ColumnName == "colLeaveName"))
                    chkLeaveName.Checked = false;
            }

            wColumns.Center();
            wColumns.Show();
        }


        protected void btnSave_Click(object sender, DirectEventArgs e)
        {
            hdnColumns.Text = "";

            List<EmpAttendanceReportColumn> list = new List<EmpAttendanceReportColumn>();


            if (chkEIN.Checked)
                list.Add(new EmpAttendanceReportColumn() { ColumnName = "colEmployeeId", ReportType = (int)ReportTypeEnum.EmpDailyAttendendance });

            if(chkEmplooyeeName.Checked)
                list.Add(new EmpAttendanceReportColumn() { ColumnName = "colEmplooyeeName", ReportType = (int)ReportTypeEnum.EmpDailyAttendendance });

            if (chkBranch.Checked)
                list.Add(new EmpAttendanceReportColumn() { ColumnName = "colBranch", ReportType = (int)ReportTypeEnum.EmpDailyAttendendance });
                
            if (chkDepartment.Checked)
                list.Add(new EmpAttendanceReportColumn() { ColumnName = "colDepartment", ReportType = (int)ReportTypeEnum.EmpDailyAttendendance });
                
            if (chkInTime.Checked)
                list.Add(new EmpAttendanceReportColumn() { ColumnName = "colInTime", ReportType = (int)ReportTypeEnum.EmpDailyAttendendance });
                
            if (chkInNote.Checked)
                list.Add(new EmpAttendanceReportColumn() { ColumnName = "colInNote", ReportType = (int)ReportTypeEnum.EmpDailyAttendendance });
                
            if (chkOutTime.Checked)
                list.Add(new EmpAttendanceReportColumn() { ColumnName = "colOutTime", ReportType = (int)ReportTypeEnum.EmpDailyAttendendance });
                
            if (chkOutNote.Checked)
                list.Add(new EmpAttendanceReportColumn() { ColumnName = "colOutNote", ReportType = (int)ReportTypeEnum.EmpDailyAttendendance });
                
            if (chkLunchOut.Checked)
                list.Add(new EmpAttendanceReportColumn() { ColumnName = "colLunchOut", ReportType = (int)ReportTypeEnum.EmpDailyAttendendance });
                
            if (chkLunchIn.Checked)
                list.Add(new EmpAttendanceReportColumn() { ColumnName = "colLunchIn", ReportType = (int)ReportTypeEnum.EmpDailyAttendendance });
                
            if (chkRefinedWorkHour.Checked)
                list.Add(new EmpAttendanceReportColumn() { ColumnName = "colRefinedWorkHour", ReportType = (int)ReportTypeEnum.EmpDailyAttendendance });
                
            if (chkLeaveName.Checked)
                list.Add(new EmpAttendanceReportColumn() { ColumnName = "colLeaveName", ReportType = (int)ReportTypeEnum.EmpDailyAttendendance });

            AttendanceManager.SaveUpdateEmpAttendanceReportColumns(list);

            wColumns.Close();
            X.Js.Call("searchList");

        }
    }
}