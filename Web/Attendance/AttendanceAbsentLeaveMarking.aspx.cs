﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using Utils.Helper;
using System.IO;
using Utils;
using Utils.Security;
using Utils.Calendar;
using Web.Attendance;
using BLL.Entity;

namespace Web.CP
{
    public partial class AttendanceAbsentLeaveMarking : BasePage
    {


        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.HumanResource;
            }
        }


        new void Load()
        {
            List<Branch> branchList = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId);
            cmbBranch.Store[0].DataSource = branchList;
            cmbBranch.Store[0].DataBind();

            cmbDepartment.Store[0].DataSource = DepartmentManager.GetAllDepartments().OrderBy(x => x.Name).ToList();
            cmbDepartment.Store[0].DataBind();

            cmbDesignation.Store[0].DataSource = CommonManager.GetUniqueDesignationList();
            cmbDesignation.Store[0].DataBind();

            this.storeLevel.DataSource = ActivityManager.GetLevels().OrderBy(x => x.Name);
            this.storeLevel.DataBind();

            List<LLeaveType> leaveList = LeaveAttendanceManager.GetAllLeaves(SessionManager.CurrentCompanyId);
            

            StoreAutoApplyLeaveList.DataSource = leaveList.Where(x => x.AbsentMarkingOrder != null && x.AbsentMarkingOrder != 0).ToList();
            StoreAutoApplyLeaveList.DataBind();

            // set half day is allowed
            for (int i = leaveList.Count - 1; i >= 0; i--)
            {
                if (leaveList[i].IsHalfDayAllowed != null && leaveList[i].IsHalfDayAllowed.Value)
                {
                    LLeaveType halfday = new LLeaveType();
                    halfday.LeaveTypeId = leaveList[i].LeaveTypeId;
                    halfday.Title = leaveList[i].Title + " Half day";
                    halfday.AbsentMarkingOrder = leaveList[i].AbsentMarkingOrder;
                    leaveList.Insert(i+1, halfday);
                }
            }

            StoreLeaveList.DataSource = leaveList.Where(x => x.AbsentMarkingOrder != null && x.AbsentMarkingOrder != 0).ToList();
            StoreLeaveList.DataBind();

            int branchId = -1, depId = -1, levelId = -1, designationId = -1, empid = -1;
            DateTime startdate, todate;
            bool isGroup = false;

            startdate = Convert.ToDateTime(Request.QueryString["from"]);
            todate = Convert.ToDateTime(Request.QueryString["to"]);

            empid = Convert.ToInt32(Request.QueryString["ein"]);
            branchId = Convert.ToInt32(Request.QueryString["gb"]);
            depId = Convert.ToInt32(Request.QueryString["gd"]);
            levelId = Convert.ToInt32(Request.QueryString["gp"]);
            designationId = Convert.ToInt32(Request.QueryString["gdd"]);

           

            calFromDate.SelectedDate = startdate;
            calToDate.SelectedDate = todate;

            ExtControlHelper.ComboBoxSetSelected(branchId, cmbBranch);
            ExtControlHelper.ComboBoxSetSelected(depId, cmbDepartment);
            ExtControlHelper.ComboBoxSetSelected(levelId, cmbLevel);
            ExtControlHelper.ComboBoxSetSelected(designationId, cmbDesignation);

            if (empid != -1)
            {

                cmbEmpSearch.InsertItem(0, EmployeeManager.GetEmployeeName(empid), empid);
                cmbEmpSearch.SelectedItem.Value = empid.ToString();
                cmbEmpSearch.SelectedItem.Text = EmployeeManager.GetEmployeeName(empid);
                cmbEmpSearch.UpdateSelectedItems();
            }

        }

        protected void btnAssign_Click(object sender, DirectEventArgs e)
        {

            

        }

        public LeaveRequest ProcessUpdate(int status, ref string message,int ein,DateTime date,int leaveTypeId,bool isFullDay)
        {
            message = "";

            LeaveRequest lq = new LeaveRequest();
            lq.EmployeeId = ein;
            lq.LeaveTypeId = leaveTypeId;

          

            LeaveRequest request = LeaveAttendanceManager.GetLeaveRequestById(lq.LeaveRequestId);


            

            lq.IsSpecialCase = false;
            lq.SpecialComment = "";

            if (isFullDay)
                lq.IsHalfDayLeave = false;
            else
                lq.IsHalfDayLeave = true;

            lq.FromDateEng = date;
            lq.ToDateEng = date;
                



            lq.Reason ="Leave assigned by Admin";


            if (request == null || (request != null && (request.IsCancelRequest == null || request.IsCancelRequest == false)))
            {
                if (LeaveAttendanceManager.IsLeaveRequestMadeOnDay(lq))
                {
                    message = "Leave request already exists on this date(s).";
                    return null;
                }
            }

     


            
                if (lq.IsHalfDayLeave.Value)
                    lq.DaysOrHours = 0.5;
                else
                {

                    lq.DaysOrHours = (lq.ToDateEng.Value - lq.FromDateEng.Value).Days + 1;
                }


                lq.IsHour = false;

            

            int currentEmployeeId = 0; string currentEmployeeUserName = "";
            UserManager.SetCurrentEmployeeAndUser(ref currentEmployeeId, ref currentEmployeeUserName);

            lq.FromDate = LeaveAttendanceManager.GetAppropriateDate(lq.FromDateEng.Value);
            lq.ToDate = LeaveAttendanceManager.GetAppropriateDate(lq.ToDateEng.Value);
            lq.CreatedOn = BLL.BaseBiz.GetCurrentDateAndTime();
            lq.CreatedBy = currentEmployeeId;
            lq.ModifiedOn = BLL.BaseBiz.GetCurrentDateAndTime();

            lq.EditSequence = 1;

            lq.Status = status;

            


            {
                lq.Comment = "";
                lq.ApprovedOn = BLL.BaseBiz.GetCurrentDateAndTime();
                lq.ApprovedBy = currentEmployeeId;
            }

            if (lq.LeaveTypeId != 0)
            {
                LLeaveType leaveType = LeaveAttendanceManager.GetLeaveTypeById(lq.LeaveTypeId);

                if (leaveType.FreqOfAccrual == LeaveAccrue.COMPENSATORY)
                {
                    lq.CompensatorIsAddType = false;
                }

                if (leaveType.IsParentGroupLeave != null && leaveType.IsParentGroupLeave.Value)
                {


                    //if (cmbChildLeaves.SelectedItem != null && cmbChildLeaves.SelectedItem.Value != null)
                    //{
                    //    lq.ChildLeaveTypeId = int.Parse(cmbChildLeaves.SelectedItem.Value);
                    //}
                    //else
                    //{
                    //    message = "Child leave must be selected for Group type.";
                    //    return lq;
                    //}
                }
            }





            return lq;
        }


        [DirectMethod]
        public object AssignLeave(string ein,string date,string leaveTypeId,bool isFullDay,bool isPhantom)
        {
            int empId = int.Parse(ein);
            DateTime leaveDate = Convert.ToDateTime(date);
            int leaveId = int.Parse(leaveTypeId);


            string message = "";
            LeaveRequestStatusEnum statusAssign = LeaveRequestStatusEnum.Approved;

            LeaveRequest leaveRequest = ProcessUpdate((int)statusAssign, ref message,empId,leaveDate,leaveId,isFullDay);
            if (!string.IsNullOrEmpty(message))
            {
                return new { valid = false, Message = message };
            }

            //Call to validate for leave
            leaveRequest.FromDate = leaveRequest.FromDateEng.Value.ToShortDateString();
            leaveRequest.ToDate = leaveRequest.ToDateEng.Value.ToShortDateString();
            DAL.LeaveRequest testLeave = new LeaveRequest();
            testLeave.CompensatorIsAddType = leaveRequest.CompensatorIsAddType;
            ResponseStatus statusValidation = LeaveRequestManager.GetLeaveRequestFromCalendarEntity(leaveRequest, testLeave, true, leaveRequest.EmployeeId);
            leaveRequest.ChildLeaveTypeId = testLeave.ChildLeaveTypeId;
            leaveRequest.ChildLeaveTypeId2 = testLeave.ChildLeaveTypeId2;

            if (leaveRequest.ChildLeaveTypeId != null)
            {
                leaveRequest.LeaveDetails.AddRange(testLeave.LeaveDetails);
            }

            if (statusValidation.IsSuccess == "false")
            {
                //NewMessage.ShowWarningMessage(statusValidation.ErrorMessage);
                //return;
                return new { valid = false, Message = statusValidation.ErrorMessage };
            }

            AttendanceAbsentLeaveMark mark = new AttendanceAbsentLeaveMark();
            mark.EmployeeId = empId;
            mark.DateEng = leaveDate;
            mark.LeaveTypeId = leaveId;
            mark.CreatedBy = SessionManager.CurrentLoggedInUserID;
            mark.CreatedOn = DateTime.Now;
            mark.IsHalfDay = isFullDay ? false : true;
            if (BLL.BaseBiz.PayrollDataContext.AttendanceAbsentLeaveMarks.Any(x =>
                x.EmployeeId == empId && x.DateEng == leaveDate) == false)
            {
                BLL.BaseBiz.PayrollDataContext.AttendanceAbsentLeaveMarks.InsertOnSubmit(mark);
            }

            ResponseStatus status = LeaveAttendanceManager.HandleLeaveRequest(leaveRequest, true);
            
        



            if (status.IsSuccessType == true)
            {
                // NewMessage.ShowNormalMessage("The Information has been updated");
                Notification.Show(new NotificationConfig
                {
                    Title = "Notification",
                    Icon = Icon.Information,
                    AutoHide = true,
                    Html = "Leave has been assigned.",
                });
                return new { valid = true };
            }
            else
                return new { valid = false, Message = status.ErrorMessage };
        }

        protected void Page_Load(object sender, EventArgs e)
        {
           


            if (!X.IsAjaxRequest && !IsPostBack)
            {

                Load();

                //details.InnerHtml = "";

                //if (empid != -1 && empid != 0)
                //    details.InnerHtml = EmployeeManager.GetEmployeeById(empid).Name;

                //if (branchId != -1 && branchId != 0)
                //{
                //    details.InnerHtml += BranchManager.GetBranchById(branchId).Name;
                //}

                //if (depId != -1 && depId != 0)
                //{
                //    details.InnerHtml += "  ," + DepartmentManager.GetDepartmentById(depId).Name;
                //}

                //if (levelId != -1 && levelId != 0)
                //{
                //    details.InnerHtml += "  ," + NewPayrollManager.GetLevelById(levelId).Name;
                //}

                //if (designationId != -1 && designationId != 0)
                //{
                //    details.InnerHtml += "  ," + new CommonManager().GetDesignationById(designationId).Name;
                //}

                //details.InnerHtml += "  from  " + startdate.ToShortDateString() + "  to  " + todate.ToShortDateString();

               Initialize();

                              

            }
        }

    
        private void Initialize()
        {

            DispDate();
        }

        private void Clear()
        {
            
        }
        public void DispDate()
        {
            AttendanceEmployeeDate _AttendanceEmployeeDate = AttendanceManager.GetAttendanceGeneratedLastDate();
            if(_AttendanceEmployeeDate!= null)
            dispLastDate.InnerHtml = String.Format("{0: MMMM d, yyyy}", _AttendanceEmployeeDate.Date.Value) + " - " + String.Format("{0:MMMM d, yyyy}", _AttendanceEmployeeDate.LastDate);
        }

        protected void RebindDate(object sender, DirectEventArgs e)
        {
            string strlastDateCtl = dispLastDate.ClientID;
            AttendanceEmployeeDate _AttendanceEmployeeDate = AttendanceManager.GetAttendanceGeneratedLastDate();
            if (_AttendanceEmployeeDate != null)
            {
                string dateRange = String.Format("{0: MMMM d, yyyy}", _AttendanceEmployeeDate.Date.Value) + "-" + String.Format("{0:MMMM d, yyyy}", _AttendanceEmployeeDate.LastDate);
                X.AddScript(strlastDateCtl + ".innerHTML ='" + dateRange + "'");
            }

        }
        protected void btnGenerateAttendance_Click(object sender, DirectEventArgs e)
        {
            //GenerateAttendanceRptCtl1.Clear();
            AEWindow.Show();
        }

        class Data
        {
            public int EmployeeId { get; set; }
            public int? LeaveTypeId { get; set; }
            public DateTime Date { get; set; }
        }

        protected void btnSaveLeave_DirectClick(object sender, DirectEventArgs e)
        {
            //JSON representation
            string gridJSON = e.ExtraParams["gridItems"];

            List<Data> entryLineObj = JSON.Deserialize<List<Data>>(gridJSON)
                .Where(x => x.LeaveTypeId != null && x.LeaveTypeId != 0).ToList();



            List<LeaveRequest> leaveList = new List<LeaveRequest>();

            foreach (var item in entryLineObj)
            {

                LeaveRequest partial = new LeaveRequest();

                partial.EmployeeId = item.EmployeeId;
                partial.LeaveTypeId = item.LeaveTypeId.Value;
                partial.FromDateEng = item.Date;
                partial.ToDateEng = item.Date;
                partial.FromDate = BLL.BaseBiz.GetAppropriateDate(partial.FromDateEng.Value);
                partial.ToDate = BLL.BaseBiz.GetAppropriateDate(partial.ToDateEng.Value);
                partial.DaysOrHours = 1;
                partial.IsHour = false;
                partial.Reason = "Leave Assigned for Absent days";
                partial.Reason = HttpUtility.HtmlEncode(partial.Reason);
                partial.Comment = "";
                partial.IsHalfDayLeave = false;
                partial.CreatedOn = DateTime.Now;
                partial.ModifiedOn = partial.CreatedOn;
                partial.ApprovedOn = partial.CreatedOn;
                partial.Status = (int)LeaveRequestStatusEnum.Approved;
                partial.ApprovedBy = SessionManager.User.UserMappedEmployeeId;


                partial.EditSequence = 1;

                LLeaveType leaveType = LeaveAttendanceManager.GetLeaveTypeById(partial.LeaveTypeId);
                if (leaveType.FreqOfAccrual == LeaveAccrue.COMPENSATORY)
                {
                    partial.CompensatorIsAddType = false;
                }

                //partial.LeaveDetails.Add(new LeaveDetail
                //{
                //    DateOn = partial.FromDate,
                //    DateOnEng = partial.FromDateEng.Value,
                //    LeaveTypeId = partial.LeaveTypeId
                //});

                leaveList.Add(partial);


                ResponseStatus  status = LeaveAttendanceManager.HandleLeaveRequest(partial, true);
                if (status.IsSuccessType == false)
                {
                    NewMessage.ShowWarningMessage(status.ErrorMessage);
                }


            }


            DateTime from = calFromDate.SelectedDate;
            DateTime to = calToDate.SelectedDate;

            List<int> einList = leaveList.Select(x => x.EmployeeId).ToList();

            foreach (var ein in einList)
            {
                AttendanceManager.GenerateAttendanceEmployeeTable(ein, from, to, false);
            }

            PagingToolbar1.DoRefresh();
        }

        protected void Store_ReadData(object sender, StoreReadDataEventArgs e)
        {
            int totalRecords = 0;

            int branchId = -1, depId = -1, levelId = -1;
            string designationName = "";

            if (cmbBranch.SelectedItem != null && cmbBranch.SelectedItem.Value != null)
                branchId = int.Parse(cmbBranch.SelectedItem.Value);
            if (cmbDepartment.SelectedItem != null && cmbDepartment.SelectedItem.Value != null)
                depId = int.Parse(cmbDepartment.SelectedItem.Value);
            if (cmbLevel.SelectedItem != null && cmbLevel.SelectedItem.Value != null)
                levelId = int.Parse(cmbLevel.SelectedItem.Value);
            if (cmbDesignation.SelectedItem != null && cmbDesignation.SelectedItem.Value != null)
                designationName = cmbDesignation.SelectedItem.Value;

            if (designationName.ToLower() == "all")
                designationName = "";

            int employeeId = -1;

            int.TryParse(cmbEmpSearch.SelectedItem.Value, out employeeId);

            if (employeeId == 0)
                employeeId = -1;

            hdnSortBy.Text = (e.Sort[0].Property + " " + e.Sort[0].Direction).ToLower();

            DateTime startdate = calFromDate.SelectedDate.Date;
            DateTime todate = calToDate.SelectedDate.Date;

            List<GetAttendanceEmployeeReportDetailsResult> list = AttendanceManager.GetAttendanceDetails
                (branchId, depId, levelId, designationName, employeeId, e.Page - 1, startdate, todate, e.Limit, (e.Sort[0].Property + " " + e.Sort[0].Direction).ToLower(), ref totalRecords,
               false, false,true,false,true,true);

            foreach (var item in list)
            {
                if (item.MarkedLeaveTypeId != null)
                {
                    item.LeaveTypeId = item.MarkedLeaveTypeId;
                    item.LeaveName = item.MarkedLeave;
                }
            }

            e.Total = totalRecords;

            //if (list.Any())
            storeEmpList.DataSource = list;
            storeEmpList.DataBind();



            List<LLeaveType> leaveList = LeaveAttendanceManager.GetAllLeaves(SessionManager.CurrentCompanyId)
                .Where(x => x.AbsentMarkingOrder != null && x.AbsentMarkingOrder != 0)
                .OrderBy(x=>x.AbsentMarkingOrder).ToList();
            
            List<int> empIDList = list.Select(x => x.EmployeeId).Distinct().ToList();
            List<GetEmployeeLeaveBalanceResult> balList = new List<GetEmployeeLeaveBalanceResult>();

            foreach (var ein in empIDList)
            {
                List<GetEmployeeLeaveBalanceResult> tempList =
                    LeaveRequestManager.GetEmployeeCurrentLeaveBalance(ein);

                foreach (LLeaveType leave in leaveList)
                {
                    GetEmployeeLeaveBalanceResult result = tempList.FirstOrDefault(x => x.LeaveTypeId == leave.LeaveTypeId);
                    if (result != null)
                    {
                        balList.Add(result);
                    }
                    else
                        balList.Add(new GetEmployeeLeaveBalanceResult { LeaveTypeId = leave.LeaveTypeId,Title = leave.Title,NewBalance = 0,EmployeeId = ein });
                }

            }


        //        public static List<GetEmployeeLeaveBalanceResult> GetEmployeeCurrentLeaveBalance(int employeeId)
        //{

            storeLeaveBal.ClearFilter(true);
            storeLeaveBal.DataSource = balList;
            storeLeaveBal.DataBind();
        }

        public void btnExport_Click(object sender, EventArgs e)
        {

            int totalRecords = 0;

            int branchId = -1, depId = -1, levelId = -1;
            string designationName = "";

            if (cmbBranch.SelectedItem != null && cmbBranch.SelectedItem.Value != null)
                branchId = int.Parse(cmbBranch.SelectedItem.Value);
            if (cmbDepartment.SelectedItem != null && cmbDepartment.SelectedItem.Value != null)
                depId = int.Parse(cmbDepartment.SelectedItem.Value);
            if (cmbLevel.SelectedItem != null && cmbLevel.SelectedItem.Value != null)
                levelId = int.Parse(cmbLevel.SelectedItem.Value);
            if (cmbDesignation.SelectedItem != null && cmbDesignation.SelectedItem.Value != null)
                designationName = cmbDesignation.SelectedItem.Value;

            if (designationName.ToLower() == "all")
                designationName = "";

            int employeeId = -1;

            int.TryParse(cmbEmpSearch.SelectedItem.Value, out employeeId);

            if (employeeId == 0)
                employeeId = -1;

            DateTime startdate = calFromDate.SelectedDate.Date;
            DateTime todate = calToDate.SelectedDate.Date;

            List<GetAttendanceEmployeeReportDetailsResult> list = AttendanceManager.GetAttendanceDetails
                (branchId, depId, levelId, designationName, employeeId, 0, startdate, todate, 999999, hdnSortBy.Text.ToLower(), ref totalRecords,
               false, false,true,false,true,true);


            List<string> hiddenList = new List<string>();

            
            {
                hiddenList = new List<string>{"OTMinuteText","OTMinute","TotalOTMinute","IsLate","IslateIn","TotalActualLate","TotalAbsent","Date","TotalStandardMin","IslateIn","TotalWorkedMin","TotalMissingOrExtra","TotalLate","ClockIn","ClockOut","DayType", "Total","TotalDays","QueryString","BranchID","DepartmentID","LevelPositionID","DesignationID",
                    "StandardMin","WorkedMin","TotalRows","MissingOrExtra","AvgMin","LevelPosition","Designation"};
            }

            string title1 = "Attendance Details";
            string title2 = "";

            if (employeeId != -1 && employeeId != 0)
                title2 += " for " + EmployeeManager.GetEmployeeById(employeeId).Name;

            if (branchId != -1 && branchId != 0)
                title2 += " Branch " + BranchManager.GetBranchById(branchId).Name;

            title2 += " from " + startdate.ToShortDateString() + " to " + todate.ToShortDateString();
           
            Bll.ExcelHelper.ExportToExcel("Attendance Details", list,
                hiddenList,
            new List<String>() { },
            new Dictionary<string, string>() { {"OTMinuteText","OT hr"},{"IsAbsent","Absent"},{"IsActualLate","Late"}, {"EngDate","Date"}, {"RowNumber","SN"},{"EmployeeId","EIN"},
                {"IdCardNo","I No"},{"StandardMinText","Standard hrs"},{"WorkedMinText","Worked hrs"},{"AvgMinText","Avg hrs"}
                ,{"MissingOrExtraText","Missing/Extras"},{"LateInCount","Late"}
                ,{"NepDate","Nep Date"},{"DayValueText","Day Type"},{"WorkPercent","Work %"} ,{"ClockInText","In"},{"ClockOutText","Out"}},
            new List<string>() { "RowNumber", "LateInCount", "Workdays", "AbsentDays", "LeaveDays", "WorkPercent" }
            , new Dictionary<string, string>() {
              {title1,title2}
            }
            , new List<string> { "RowNumber","EmployeeId","IdCardNo","Name", "Branch","Department", "EngDate","NepDate","DayValueText","LeaveName","LevelPosition","Designation"
                ,"StandardMinText","WorkedMinText","AvgMinText","ClockInText","ClockOutText","OTMinuteText","LateInCount",
                "WorkDays","AbsentDays","LeaveDays","MissingOrExtraText","IsActualLate","IsAbsent"});


        }


    }
}