﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using System.Text;
using BLL.Manager;
using Utils.Helper;
using DAL;
using System.Drawing;
using BLL.Base;
using Ext.Net;
using Utils.Calendar;

namespace Web.Attendance
{
    public partial class EmployeeBranchAttendance : BasePage
    {
        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.LeaveAndAttendance;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Initialise();
            }
        }

        void Initialise()
        {
            cmbBranch.Store[0].DataSource = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId);
            cmbBranch.Store[0].DataBind();

            txtDate.Text = CustomDate.GetTodayDate(IsEnglish).ToString();
            Hidden_Date.Text = txtDate.Text.Trim();
        }

        protected void txtDate_Change(object sender, DirectEventArgs e)
        {
            Hidden_Date.Text = txtDate.Text.Trim();
        }
    }
}