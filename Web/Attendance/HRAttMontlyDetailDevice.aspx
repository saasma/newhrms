﻿<%@ Page Title="Time Attendance Detail" Language="C#" MasterPageFile="~/Master/HROld.Master" AutoEventWireup="true" CodeBehind="HRAttMontlyDetailDevice.aspx.cs" Inherits="Web.Attendance.HRAttMontlyDetailDevice" %>

<%@ Register assembly="DevExpress.XtraReports.v16.1.Web, Version=16.1.8.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.XtraReports.Web" tagprefix="dxxr" %>
<%@ Register src="~/Controls/Report/ReportFilterBranchDep.ascx" tagname="ReportFilterBranchDep" tagprefix="uc1" %>
<%@ Register src="~/Controls/Report/ReportContainer.ascx" tagname="ReportContainer" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

<style>



.DXRResetStyle td
{
	/*width:120px!important;*/
}

</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server"><h4 style="padding-left:10px">
    
     <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Time Attendance Details
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <uc1:ReportContainer runat="server"  OnReloadReport="LoadReport"  id="report" />
    </div>
</asp:Content>
