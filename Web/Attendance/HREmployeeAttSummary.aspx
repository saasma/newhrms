﻿<%@ Page Title="Employee Attendance Summary" Language="C#" MasterPageFile="~/Master/HROld.Master"
    AutoEventWireup="true" CodeBehind="HREmployeeAttSummary.aspx.cs" Inherits="Web.Attendance.HREmployeeAttSummary" %>

<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .grayBackground .x-grid-cell
        {
            background-color: lightgray;
        }
        
        
        .x-column-header-inner
        {
            background-color: #428BCA;
            color: White;
        }
        
        .cellBorderCls .x-grid-cell
        {
            border: solid 1px #D9EDF7;
        }
    </style>
    <script type="text/javascript">




        var myRenderer1 = function (value, metadata, record) {
            if (record.data.EmployeeId == 0) {
                metadata.style = "background-color: lightgray;";
            }
            else {
                metadata.style = "background-color: #EEF6FB;";
            }

            return value;
        };

        var myRenderer2 = function (value, metadata) {

            metadata.style = "background-color: lavenderblush;";

            return parseFloat(value).toFixed(2);
        };

        var myRenderer3 = function (value, metadata, record) {
            metadata.style = "background-color: blanchedalmond;";

            return parseFloat(value).toFixed(2);
        };

        var myRenderer4 = function (value, metadata, record) {
            metadata.style = "background-color: blanchedalmond;";

            return parseFloat(value).toFixed(2) + "%";
        };

        var myRenderer5 = function (value, metadata, record) {
            return parseFloat(value).toFixed(2) + "%";
        };

        var applyRowBackground = function (record, rowIndex, rowParams, store) {
            if (record.data.EmployeeId == 0) {
                return 'grayBackground';
            }
        };

  
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <ext:ResourceManager ID="ResourceManager1" DisableViewState="false"  runat="server" />
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Employee Work Days Report
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <table>
            <tr>
                <td style="width: 200px; height: 30px;">
                    Start Date
                </td>
                <td style="width: 200px; height: 30px;">
                    End Date
                </td>
            </tr>
            <tr>
                <td style="width: 200px;">
                    <pr:CalendarExtControl Width="150px" ID="txtStartDate" runat="server" LabelSeparator="" />
                    <asp:RequiredFieldValidator Display="None" ID="rfvStartDate" runat="server" ValidationGroup="LoadGrid"
                        ControlToValidate="txtStartDate" ErrorMessage="Start date is required." />
                </td>
                <td style="width: 200px;">
                    <pr:CalendarExtControl Width="150px" ID="txtEndDate" runat="server" LabelSeparator="" />
                    <asp:RequiredFieldValidator Display="None" ID="rfvEndDate" runat="server" ValidationGroup="LoadGrid"
                        ControlToValidate="txtEndDate" ErrorMessage="End date is required." />
                    <asp:CompareValidator ID="compval" Display="None" ControlToValidate="txtEndDate"
                        ControlToCompare="txtStartDate" ValidationGroup="LoadGrid" Type="Date" ErrorMessage="Start date cannot be greater than or equal to end date."
                        runat="server" />
                </td>
                <td style="padding-bottom: 5px; width: 100px;">
                    <ext:Button ID="btnLoad" runat="server" Text="Show" Cls="btn btn-primary">
                        <DirectEvents>
                            <Click OnEvent="btnLoad_Click" Timeout="999999">
                                <EventMask ShowMask="true" />
                            </Click>
                        </DirectEvents>
                        <Listeners>
                            <Click Handler="valGroup = 'LoadGrid'; if(CheckValidation()) return this.disable(); else return false;">
                            </Click>
                        </Listeners>
                    </ext:Button>
                </td>
                <td>
                    <ext:LinkButton ID="lnkExcelPrint" Icon="PageExcel" runat="server" Text="Export"
                        OnClick="btnExcelPrint_Click" Hidden="false" AutoPostBack="true">
                    </ext:LinkButton>
                </td>
            </tr>
        </table>
        <br />
        <ext:Label runat="server" ID="lblMsg" />
        <ext:GridPanel StyleSpec="margin-top:15px;" ID="gridEmployeeLeaveSummary" runat="server"
            Cls="itemgrid" Scroll="Both" Width="1700" OverflowX="Auto"   OverflowY="Auto" Hidden="true">
            <Store>
                <ext:Store ID="Store1" runat="server">
                    <Model>
                        <ext:Model ID="Model4" runat="server" IDProperty="EmployeeId">
                            <Fields>
                                <ext:ModelField Name="EmployeeId" Type="Int" />
                                <ext:ModelField Name="Name" Type="String" />
                                <ext:ModelField Name="TotalDays" Type="Float" />
                                <ext:ModelField Name="WH" Type="Float" />
                                <ext:ModelField Name="PH" Type="Float" />
                                <ext:ModelField Name="StdDays" Type="Float" />
                                <ext:ModelField Name="LT1" Type="Float" />
                                <ext:ModelField Name="LT2" Type="Float" />
                                <ext:ModelField Name="LT3" Type="Float" />
                                <ext:ModelField Name="LT4" Type="Float" />
                                <ext:ModelField Name="LT5" Type="Float" />
                                <ext:ModelField Name="LT6" Type="Float" />
                                <ext:ModelField Name="LT7" Type="Float" />
                                <ext:ModelField Name="LT8" Type="Float" />
                                <ext:ModelField Name="LT9" Type="Float" />
                                <ext:ModelField Name="LT10" Type="Float" />
                                <ext:ModelField Name="LT11" Type="Float" />
                                <ext:ModelField Name="LT12" Type="Float" />
                                <ext:ModelField Name="UPL" Type="Float" />
                                <ext:ModelField Name="AbsDays" Type="Float" />
                                <ext:ModelField Name="LeaveDays" Type="Float" />
                                <ext:ModelField Name="WorkDays" Type="Float" />
                                <ext:ModelField Name="Percentage" Type="Float" />
                            </Fields>
                        </ext:Model>
                    </Model>
                </ext:Store>
            </Store>
            <ColumnModel>
                <Columns>
                    <ext:RowNumbererColumn Text="SN" Width="40" />
                     <ext:Column ID="Column2" Sortable="true" MenuDisabled="true" runat="server" Header="EIN"
                        Align="Left" Width="50" DataIndex="EmployeeId">
                    </ext:Column>
                    <ext:Column ID="Column3" Sortable="true" MenuDisabled="true" runat="server" Text="Name"
                        Align="Left" Width="180" DataIndex="Name">
                        <Renderer Fn="myRenderer1" />
                    </ext:Column>
                    <ext:NumberColumn ID="colTotalDays" runat="server" Sortable="true" Align="Center"
                        MenuDisabled="true" Header="Total Days" Width="90" DataIndex="TotalDays">
                        <Renderer Fn="myRenderer2" />
                    </ext:NumberColumn>
                    <ext:NumberColumn ID="colWH" runat="server" Sortable="true" Align="Center" MenuDisabled="true"
                        Header="WH" Width="70" DataIndex="WH">
                        <Renderer Fn="myRenderer2" />
                    </ext:NumberColumn>
                    <ext:NumberColumn ID="colPH" runat="server" Sortable="true" Align="Center" MenuDisabled="true"
                        Header="PH" Width="70" DataIndex="PH">
                        <Renderer Fn="myRenderer2" />
                    </ext:NumberColumn>
                    <ext:NumberColumn ID="colStdDays" runat="server" Sortable="true" Align="Center" MenuDisabled="true"
                        Header="Std Days" Width="90" DataIndex="StdDays">
                        <Renderer Fn="myRenderer2" />
                    </ext:NumberColumn>
                    <ext:NumberColumn ID="colLT1" runat="server" Sortable="true" Align="Center" MenuDisabled="true"
                        Header="LT1" Width="70" DataIndex="LT1">
                        <Renderer Fn="myRenderer2" />
                    </ext:NumberColumn>
                    <ext:NumberColumn ID="colLT2" runat="server" Sortable="true" Align="Center" MenuDisabled="true"
                        Header="LT2" Width="70" DataIndex="LT2">
                        <Renderer Fn="myRenderer2" />
                    </ext:NumberColumn>
                    <ext:NumberColumn ID="colLT3" runat="server" Sortable="true" Align="Center" MenuDisabled="true"
                        Header="LT3" Width="70" DataIndex="LT3">
                        <Renderer Fn="myRenderer2" />
                    </ext:NumberColumn>
                    <ext:NumberColumn ID="colLT4" runat="server" Sortable="true" Align="Center" MenuDisabled="true"
                        Header="LT4" Width="70" DataIndex="LT4">
                        <Renderer Fn="myRenderer2" />
                    </ext:NumberColumn>
                    <ext:NumberColumn ID="colLT5" runat="server" Sortable="true" Align="Center" MenuDisabled="true"
                        Header="LT5" Width="70" DataIndex="LT5">
                        <Renderer Fn="myRenderer2" />
                    </ext:NumberColumn>
                    <ext:NumberColumn ID="colLT6" runat="server" Sortable="true" Align="Center" MenuDisabled="true"
                        Header="LT6" Width="70" DataIndex="LT6">
                        <Renderer Fn="myRenderer2" />
                    </ext:NumberColumn>
                    <ext:NumberColumn ID="colLT7" runat="server" Sortable="true" Align="Center" MenuDisabled="true"
                        Header="LT7" Width="70" DataIndex="LT7">
                        <Renderer Fn="myRenderer2" />
                    </ext:NumberColumn>
                    <ext:NumberColumn ID="colLT8" runat="server" Sortable="true" Align="Center" MenuDisabled="true"
                        Header="LT8" Width="70" DataIndex="LT8">
                        <Renderer Fn="myRenderer2" />
                    </ext:NumberColumn>
                    <ext:NumberColumn ID="colLT9" runat="server" Sortable="true" Align="Center" MenuDisabled="true"
                        Header="LT9" Width="70" DataIndex="LT9">
                        <Renderer Fn="myRenderer2" />
                    </ext:NumberColumn>
                    <ext:NumberColumn ID="colLT10" runat="server" Sortable="true" Align="Center" MenuDisabled="true"
                        Header="LT10" Width="70" DataIndex="LT10">
                        <Renderer Fn="myRenderer2" />
                    </ext:NumberColumn>
                    <ext:NumberColumn ID="colLT11" runat="server" Sortable="true" Align="Center" MenuDisabled="true"
                        Header="LT11" Width="70" DataIndex="LT11">
                        <Renderer Fn="myRenderer2" />
                    </ext:NumberColumn>
                    <ext:NumberColumn ID="colLT12" runat="server" Sortable="true" Align="Center" MenuDisabled="true"
                        Header="LT12" Width="70" DataIndex="LT12">
                        <Renderer Fn="myRenderer2" />
                    </ext:NumberColumn>
                    <ext:NumberColumn ID="colUPL" runat="server" Sortable="true" Align="Center" MenuDisabled="true"
                        Header="UPL" Width="70" DataIndex="UPL">
                        <Renderer Fn="myRenderer2" />
                    </ext:NumberColumn>
                    <ext:NumberColumn ID="colAbsDays" runat="server" Sortable="true" Align="Center" MenuDisabled="true"
                        Header="ABS" Width="70" DataIndex="AbsDays">
                        <Renderer Fn="myRenderer2" />
                    </ext:NumberColumn>
                    <ext:NumberColumn ID="colLeaveDays" runat="server" Sortable="true" Align="Center"
                        MenuDisabled="true" Header="Leave Days" Width="90" DataIndex="LeaveDays">
                        <Renderer Fn="myRenderer2" />
                    </ext:NumberColumn>
                    <ext:NumberColumn ID="colWorkDays" runat="server" Sortable="true" Align="Center"
                        MenuDisabled="true" Header="Work Days" Width="90" DataIndex="WorkDays">
                        <Renderer Fn="myRenderer3" />
                    </ext:NumberColumn>
                    <ext:NumberColumn ID="colPercentage" runat="server" Sortable="true" Align="Center"
                        MenuDisabled="true" Header="Percentage" Width="90" DataIndex="Percentage">
                        <Renderer Fn="myRenderer4" />
                    </ext:NumberColumn>
                </Columns>
            </ColumnModel>
            <%--<View>
                <ext:GridView ID="GridPanel1" runat="server">
                    <GetRowClass Fn="applyRowBackground" />
                </ext:GridView>
            </View>
            --%>
        </ext:GridPanel>
        <ext:GridPanel ID="gridTotal" runat="server" Cls="itemgrid" Scroll="Both" Width="1700"
            OverflowX="Auto" Hidden="true" HideHeaders="true">
            <Store>
                <ext:Store ID="Store2" runat="server">
                    <Model>
                        <ext:Model ID="Model1" runat="server" IDProperty="EmployeeId">
                            <Fields>
                                <ext:ModelField Name="EmployeeId" Type="Int" />
                                <ext:ModelField Name="Name" Type="String" />
                                <ext:ModelField Name="TotalDays" Type="Float" />
                                <ext:ModelField Name="WH" Type="Float" />
                                <ext:ModelField Name="PH" Type="Float" />
                                <ext:ModelField Name="StdDays" Type="Float" />
                                <ext:ModelField Name="LT1" Type="Float" />
                                <ext:ModelField Name="LT2" Type="Float" />
                                <ext:ModelField Name="LT3" Type="Float" />
                                <ext:ModelField Name="LT4" Type="Float" />
                                <ext:ModelField Name="LT5" Type="Float" />
                                <ext:ModelField Name="LT6" Type="Float" />
                                <ext:ModelField Name="LT7" Type="Float" />
                                <ext:ModelField Name="LT8" Type="Float" />
                                <ext:ModelField Name="LT9" Type="Float" />
                                <ext:ModelField Name="LT10" Type="Float" />
                                <ext:ModelField Name="LT11" Type="Float" />
                                <ext:ModelField Name="LT12" Type="Float" />
                                <ext:ModelField Name="UPL" Type="Float" />
                                <ext:ModelField Name="AbsDays" Type="Float" />
                                <ext:ModelField Name="LeaveDays" Type="Float" />
                                <ext:ModelField Name="WorkDays" Type="Float" />
                                <ext:ModelField Name="Percentage" Type="Float" />
                            </Fields>
                        </ext:Model>
                    </Model>
                </ext:Store>
            </Store>
            <ColumnModel>
                <Columns>
                    <ext:Column ID="Column1" Sortable="true" MenuDisabled="true" runat="server" Header="EIN"
                        Align="Left" Width="90" DataIndex="EmployeeId">
                    </ext:Column>
                    <ext:Column ID="colTName" Sortable="true" MenuDisabled="true" runat="server" Text="Name"
                        Align="Left" Width="180" DataIndex="Name">
                    </ext:Column>
                    <ext:NumberColumn ID="colTTotalDays" runat="server" Sortable="false" Align="Center"
                        MenuDisabled="true" Header="Total Days" Width="90" DataIndex="TotalDays">
                    </ext:NumberColumn>
                    <ext:NumberColumn ID="colTWH" runat="server" Sortable="true" Align="Center" MenuDisabled="true"
                        Header="WH" Width="70" DataIndex="WH">
                    </ext:NumberColumn>
                    <ext:NumberColumn ID="colTPH" runat="server" Sortable="true" Align="Center" MenuDisabled="true"
                        Header="PH" Width="70" DataIndex="PH">
                    </ext:NumberColumn>
                    <ext:NumberColumn ID="colTStdDays" runat="server" Sortable="false" Align="Center"
                        MenuDisabled="true" Header="Std Days" Width="90" DataIndex="StdDays">
                    </ext:NumberColumn>
                    <ext:NumberColumn ID="colTLT1" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                        Header="LT1" Width="70" DataIndex="LT1">
                    </ext:NumberColumn>
                    <ext:NumberColumn ID="colTLT2" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                        Header="LT2" Width="70" DataIndex="LT2">
                    </ext:NumberColumn>
                    <ext:NumberColumn ID="colTLT3" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                        Header="LT3" Width="70" DataIndex="LT3">
                    </ext:NumberColumn>
                    <ext:NumberColumn ID="colTLT4" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                        Header="LT4" Width="70" DataIndex="LT4">
                    </ext:NumberColumn>
                    <ext:NumberColumn ID="colTLT5" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                        Header="LT5" Width="70" DataIndex="LT5">
                    </ext:NumberColumn>
                    <ext:NumberColumn ID="colTLT6" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                        Header="LT6" Width="70" DataIndex="LT6">
                    </ext:NumberColumn>
                    <ext:NumberColumn ID="colTLT7" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                        Header="LT7" Width="70" DataIndex="LT7">
                    </ext:NumberColumn>
                    <ext:NumberColumn ID="colTLT8" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                        Header="LT8" Width="70" DataIndex="LT8">
                    </ext:NumberColumn>
                    <ext:NumberColumn ID="colTLT9" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                        Header="LT9" Width="70" DataIndex="LT9">
                    </ext:NumberColumn>
                    <ext:NumberColumn ID="colTLT10" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                        Header="LT10" Width="70" DataIndex="LT10">
                    </ext:NumberColumn>
                    <ext:NumberColumn ID="colTLT11" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                        Header="LT11" Width="70" DataIndex="LT11">
                    </ext:NumberColumn>
                    <ext:NumberColumn ID="colTLT12" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                        Header="LT12" Width="70" DataIndex="LT12">
                    </ext:NumberColumn>
                    <ext:NumberColumn ID="colTUPL" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                        Header="UPL" Width="70" DataIndex="UPL">
                    </ext:NumberColumn>
                    <ext:NumberColumn ID="colTAbsDays" runat="server" Sortable="false" Align="Center"
                        MenuDisabled="true" Header="ABS" Width="70" DataIndex="AbsDays">
                    </ext:NumberColumn>
                    <ext:NumberColumn ID="colTLeaveDays" runat="server" Sortable="true" Align="Center"
                        MenuDisabled="true" Header="Leave Days" Width="90" DataIndex="LeaveDays">
                    </ext:NumberColumn>
                    <ext:NumberColumn ID="colTWorkDays" runat="server" Sortable="true" Align="Center"
                        MenuDisabled="true" Header="Work Days" Width="90" DataIndex="WorkDays">
                    </ext:NumberColumn>
                    <ext:NumberColumn ID="colTPercentage" runat="server" Sortable="false" Align="Center"
                        MenuDisabled="true" Header="Percentage" Width="90" DataIndex="Percentage">
                        <Renderer Fn="myRenderer5" />
                    </ext:NumberColumn>
                </Columns>
            </ColumnModel>
            <View>
                <ext:GridView ID="GridPanel1" runat="server">
                    <GetRowClass Fn="applyRowBackground" />
                </ext:GridView>
            </View>
        </ext:GridPanel>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
