﻿<%@ Page Title="Late Processing Details" Language="C#" MasterPageFile="~/Master/NewDetails.Master"
    AutoEventWireup="true" CodeBehind="LateProcessingDetails.aspx.cs" Inherits="Web.CP.LateProcessingDetails" %>

<%@ MasterType VirtualPath="~/Master/NewDetails.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">

    function searchList() {
              <%=PagingToolbar1.ClientID %>.doRefresh();
        }

 
   
  Ext.onReady
  (
    function()
    {
        
    }
  );

  var lateRender = function(value)
  {
    if(value == 'true')
        return 'Yes';

        return '';
  }



   var getRowClass = function (record) {
    
        var dayValue = record.data.DayValueText;
        

         if(dayValue=="Working Day")
         {
            return "";
         }
         else if(dayValue=="Holiday")
         {
            return "holiday";
         }
         else if(dayValue=="Leave" || dayValue=="Half Leave")
         {
            return "leave";
         }
        
         else //if(dayValue=="Weekly Holiday")
         {
            return "weeklyholiday";
         }

      };

    </script>
    <style type="text/css">
        .holiday, .holiday a, .holiday td
        {
            color: #469146;
            background-color: #F0FFF0 !important;
        }
        .leave, .leave a, .leave td
        {
            color: #D95CA9;
            background-color: #FFF0F5 !important;
        }
        
        .weeklyholiday, .weeklyholiday a, .weeklyholiday td
        {
            color: #8F8F1A;
            background-color: #FAFAD2 !important;
        }
        
        .x-grid-cell-inner
        {
            padding: 5px 5px 5px 5px;
        }
        .x-column-header-inner
        {
            padding: 7px 5px 7px 5px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <script type="text/javascript" src="<%= ResolveUrl("~/Employee/override.js?v=") + Web.Helper.WebHelper.Version %>"></script>
    <ext:Hidden ID="hdnEmployeeId" runat="server" />
    <ext:Hidden ID="hdnBranchDepartmentId" runat="server" />
    <ext:Hidden ID="hdnMsg" runat="server" />
    <ext:Hidden ID="hdnEmpSearch" runat="server" />
    <ext:Hidden ID="hdnSortBy" runat="server" />
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Late Entry Deduction
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel" style="margin-top: 0px !important; padding-top: 0px !important;">
        <div class="innerLR">
            For this report, data should be generated from "Generate Attendance Report" for
            the specified date
            <div style="clear: both">
            </div>
            <span runat='server' style="font-size: 14px; padding-top: 15px; margin-bottom: 10px"
                id="details"></span>
            <div class="alert alert-info" style="margin-top: 10px; padding-top: 0px!important;">
                <ext:Container ID="listingContainer" runat="server">
                    <Content>
                        <table>
                            <tr>
                                <td>
                                    <ext:DateField LabelWidth="120" MarginSpec="0 5 0 5" Width="100" FieldLabel="From"
                                        ID="calFromDate" runat="server" LabelAlign="Top" LabelSeparator="">
                                        <Plugins>
                                            <ext:GenericPlugin ID="HighlightDates1" runat="server" InstanceName="Ext.ux.DatePicker.HighlightWithNepDates" />
                                        </Plugins>
                                    </ext:DateField>
                                </td>
                                <td>
                                    <ext:DateField LabelWidth="120" MarginSpec="0 5 0 5" Width="100" FieldLabel="To"
                                        ID="calToDate" runat="server" LabelAlign="Top" LabelSeparator="">
                                        <Plugins>
                                            <ext:GenericPlugin ID="GenericPlugin1" runat="server" InstanceName="Ext.ux.DatePicker.HighlightWithNepDates" />
                                        </Plugins>
                                    </ext:DateField>
                                </td>
                                <td>
                                    <ext:ComboBox Width="140" MatchFieldWidth="false" MarginSpec="0 5 0 5" LabelSeparator=""
                                        LabelAlign="Top" QueryMode="Local" ID="cmbBranch" DisplayField="Name" ValueField="BranchId"
                                        runat="server" FieldLabel="Branch">
                                        <Items>
                                            <ext:ListItem Index="0" Value="-1" Text="All">
                                            </ext:ListItem>
                                        </Items>
                                        <Store>
                                            <ext:Store ID="Store5" runat="server">
                                                <Model>
                                                    <ext:Model ID="Model7" IDProperty="BranchId" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="BranchId" />
                                                            <ext:ModelField Name="Name" />
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                    </ext:ComboBox>
                                </td>
                                <td>
                                    <ext:ComboBox FieldLabel="Department" MatchFieldWidth="false" ID="cmbDepartment"
                                        Width="140" MarginSpec="0 5 0 5" runat="server" ValueField="DepartmentId" DisplayField="Name"
                                        LabelAlign="Top" LabelSeparator="" QueryMode="Local">
                                        <Items>
                                            <ext:ListItem Index="0" Value="-1" Text="All">
                                            </ext:ListItem>
                                        </Items>
                                        <Store>
                                            <ext:Store ID="Store6" runat="server">
                                                <Model>
                                                    <ext:Model ID="Model8" IDProperty="DepartmentId" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="DepartmentId" />
                                                            <ext:ModelField Name="Name" />
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                    </ext:ComboBox>
                                </td>
                                <td>
                                    <ext:ComboBox ID="cmbLevel" MatchFieldWidth="false" runat="server" FieldLabel="Level/Position"
                                        Width="140" MarginSpec="0 5 0 5" DisplayField="Name" ValueField="LevelId" QueryMode="Local"
                                        LabelSeparator="" LabelAlign="Top">
                                        <Items>
                                            <ext:ListItem Index="0" Value="-1" Text="All">
                                            </ext:ListItem>
                                        </Items>
                                        <Store>
                                            <ext:Store ID="storeLevel" runat="server">
                                                <Model>
                                                    <ext:Model ID="modelLevel" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="LevelId" Type="Int" />
                                                            <ext:ModelField Name="Name" Type="String" />
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                    </ext:ComboBox>
                                </td>
                                <td>
                                    <ext:ComboBox MatchFieldWidth="false" FieldLabel="Designation" ID="cmbDesignation"
                                        Width="140" MarginSpec="0 5 0 5" runat="server" ValueField="DesignationId" DisplayField="LevelAndDesignation"
                                        LabelAlign="Top" LabelSeparator="" QueryMode="Local">
                                        <Items>
                                            <ext:ListItem Index="0" Value="-1" Text="All">
                                            </ext:ListItem>
                                        </Items>
                                        <Store>
                                            <ext:Store ID="Store7" runat="server">
                                                <Model>
                                                    <ext:Model ID="Model9" IDProperty="DesignationId" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="DesignationId" />
                                                            <ext:ModelField Name="LevelAndDesignation" />
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                    </ext:ComboBox>
                                </td>
                                <td>
                                    <ext:Store runat="server" ID="storeSearch" AutoLoad="false">
                                        <Proxy>
                                            <ext:AjaxProxy Url="../Handler/EmpSearchID.ashx">
                                                <ActionMethods Read="GET" />
                                                <Reader>
                                                    <ext:JsonReader Root="plants" TotalProperty="total" />
                                                </Reader>
                                            </ext:AjaxProxy>
                                        </Proxy>
                                        <Model>
                                            <ext:Model ID="Model5" IDProperty="EmployeeId" Name="ItemLineModel" runat="server">
                                                <Fields>
                                                    <ext:ModelField Name="Name" Type="String" />
                                                    <ext:ModelField Name="EmployeeId" Type="String" />
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                    <ext:ComboBox LabelSeparator="" ID="cmbEmpSearch" FieldLabel="Search Employee" EmptyText="Employee Name"
                                        LabelAlign="Top" runat="server" DisplayField="Name" ValueField="EmployeeId" StoreID="storeSearch"
                                        TypeAhead="false" Width="200" PageSize="9999" HideBaseTrigger="true" MinChars="1"
                                        TriggerAction="All" ForceSelection="false">
                                        <ListConfig LoadingText="Searching..." MinWidth="200" StyleSpec="border-top:1px solid #98c0f4;">
                                            <ItemTpl ID="ItemTpl2" runat="server">
                                                <Html>
                                                    <tpl>
                                                        <div class="search-item">
                                                                        <span>{Name}</span>  
                                                         </div>
					                            </tpl>
                                                </Html>
                                            </ItemTpl>
                                        </ListConfig>
                                        <Triggers>
                                            <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                                        </Triggers>
                                        <Listeners>
                                            <Select Handler="this.getTrigger(0).show();" />
                                            <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                            <TriggerClick Handler="if (index == 0) { 
                                                       this.clearValue(); 
                                                       this.getTrigger(0).hide();
                                                   }" />
                                        </Listeners>
                                    </ext:ComboBox>
                                </td>
                                <td>
                                    <ext:Button ID="btnLoad" runat="server" Text="Load" Width="100" Cls="btn btn-default"
                                        MarginSpec="25 10 10 10">
                                        <Listeners>
                                            <Click Fn="searchList">
                                            </Click>
                                        </Listeners>
                                    </ext:Button>
                                </td>
                                <td style="padding-top: 20px; padding-left: 10px;">
                                    <ext:Button StyleSpec="" ID="btnSaveLateMark" runat="server" Cls="btn btn-primary"
                                        Text="Set Late Deduction">
                                        <DirectEvents>
                                            <Click OnEvent="btnSaveLateMark_Save">
                                                <EventMask ShowMask="true" />
                                                <Confirmation ConfirmRequest="true" Message="Confirm make for late for deduction?" />
                                                <ExtraParams>
                                                    <ext:Parameter Name="gridItems" Value="Ext.encode(#{gridBranchTransfer}.getRowsValues({ selectedOnly: false }))"
                                                        Mode="Raw" />
                                                </ExtraParams>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <ext:Checkbox runat="server" ID="chkIsLateOnly" BoxLabel="Late Marked Only" />
                                </td>
                            </tr>
                        </table>
                    </Content>
                </ext:Container>
            </div>
            <ext:KeyMap ID="KeyMap1" runat="server" Target="={#{listingContainer}.getBody()}">
                <Binding>
                    <ext:KeyBinding Handler="#{btnLoad}.fireEvent('click');">
                        <Keys>
                            <ext:Key Code="ENTER" />
                        </Keys>
                    </ext:KeyBinding>
                </Binding>
            </ext:KeyMap>
            <ext:GridPanel StyleSpec="margin-top:15px;" ID="gridBranchTransfer" runat="server"
                Cls="itemgrid" OnReadData="Store_ReadData">
                <Store>
                    <ext:Store PageSize="50" ID="storeEmpList" runat="server" AutoLoad="true" OnReadData="Store_ReadData"
                        RemotePaging="true" RemoteSort="true">
                        <Proxy>
                            <ext:PageProxy />
                        </Proxy>
                        <Model>
                            <ext:Model ID="Model1" IDProperty="RecordID" runat="server">
                                <Fields>
                                    <ext:ModelField Name="RowNumber" Type="String" />
                                    <ext:ModelField Name="EmployeeId" Type="String" />
                                    <ext:ModelField Name="IdCardNo" Type="String" />
                                    <ext:ModelField Name="Name" Type="String" />
                                    <ext:ModelField Name="Date" Type="Date" />
                                    <ext:ModelField Name="NepDate" Type="String" />
                                    <ext:ModelField Name="DayValueText" Type="String" />
                                    <ext:ModelField Name="OfficeInText" Type="String" />
                                    <ext:ModelField Name="OfficeOutText" Type="String" />
                                    <ext:ModelField Name="ClockInText" Type="String" />
                                    <ext:ModelField Name="ClockOutText" Type="String" />
                                    <ext:ModelField Name="WorkedMinText" Type="String" />
                                    <ext:ModelField Name="LateMinText" Type="String" />
                                    <ext:ModelField Name="InComments" Type="String" />
                                    <ext:ModelField Name="Shift" Type="String" />
                                    <ext:ModelField Name="IsLateMarked" Type="Boolean" />
                                </Fields>
                            </ext:Model>
                        </Model>
                        <Sorters>
                            <ext:DataSorter Property="date" Direction="ASC" />
                        </Sorters>
                    </ext:Store>
                </Store>
                <ColumnModel ID="ColumnModel1" runat="server">
                    <Columns>
                        <ext:Column ID="colFromDate" runat="server" Text="SN" DataIndex="RowNumber" MenuDisabled="false"
                            Sortable="false" Align="Center" Width="40" />
                        <ext:Column ID="Column_EIN" runat="server" Text="EIN" DataIndex="EmployeeId" MenuDisabled="false"
                            Sortable="true" Align="Center" Width="40" />
                        <ext:Column ID="Column_ID" runat="server" Text="I No" DataIndex="IdCardNo" MenuDisabled="false"
                            Sortable="true" Align="Center" Width="40" />
                        <ext:Column ID="Column_Name" runat="server" Text="Name" DataIndex="Name" MenuDisabled="false"
                            Sortable="true" Align="Left" Width="150" />
                        <ext:DateColumn ID="DateColumn1" runat="server" Text="Date" DataIndex="Date" Format="dd-MMM-yyyy"
                            MenuDisabled="false" Sortable="true" Align="Left" Width="90" />
                        <ext:Column ID="Column4" runat="server" Text="Nep Date" DataIndex="NepDate" MenuDisabled="false"
                            Sortable="true" Align="Left" Width="90" />
                        <ext:Column ID="Column2" runat="server" Text="Day Type" DataIndex="DayValueText"
                            MenuDisabled="false" Sortable="true" Align="Left" Width="100" />
                        <ext:Column ID="Column8" runat="server" Text="Work hr" DataIndex="WorkedMinText"
                            MenuDisabled="false" Sortable="true" Align="Right" Width="50" />
                        <ext:Column ID="Column11" runat="server" Text="Shift" DataIndex="Shift" MenuDisabled="false"
                            Sortable="true" Align="Left" Width="80" />
                        <ext:Column ID="Column5" runat="server" Text="Office Time" DataIndex="OfficeInText"
                            MenuDisabled="false" Sortable="true" Align="Right" Width="60" />
                        <ext:Column ID="Column10" runat="server" Text="Office Time" DataIndex="OfficeOutText"
                            MenuDisabled="false" Sortable="true" Align="Right" Width="60" />
                        <ext:Column ID="Column1" runat="server" Text="In" DataIndex="ClockInText" MenuDisabled="false"
                            Sortable="true" Align="Right" Width="60" />
                        <ext:Column ID="Column3" runat="server" Text="Out" DataIndex="ClockOutText" MenuDisabled="false"
                            Sortable="true" Align="Right" Width="60" />
                        <ext:Column ID="Column6" runat="server" Text="Late hr" DataIndex="LateMinText" MenuDisabled="false"
                            Sortable="true" Align="Right" Width="60">
                        </ext:Column>
                        <ext:Column ID="Column7" runat="server" Text="In Comments" DataIndex="InComments"
                            MenuDisabled="false" Sortable="true" Align="Left" Width="120">
                        </ext:Column>
                       
                        <ext:CheckColumn ID="Column12" runat="server" Text="Late Deduction" DataIndex="IsLateMarked"
                            MenuDisabled="false" Editable="true" Sortable="true" Align="Left" Width="120">
                        </ext:CheckColumn>
                    </Columns>
                </ColumnModel>
                <SelectionModel>
                    <ext:CellSelectionModel ID="RowSelectionModel1" runat="server" Mode="Single" />
                </SelectionModel>
                <View>
                    <ext:GridView ID="GridView1" runat="server">
                        <GetRowClass Fn="getRowClass" />
                    </ext:GridView>
                </View>
                <BottomBar>
                    <ext:PagingToolbar ID="PagingToolbar1" runat="server" DisplayInfo="true">
                    </ext:PagingToolbar>
                </BottomBar>
            </ext:GridPanel>
        </div>
        <div class="alert alert-info" style="margin-top: 15px; height: 50px;">
            <div style="float: right;">
                <ext:Button runat="server" AutoPostBack="true" OnClick="btnExport_Click" ID="btnExport"
                    Text="<i></i>Export To Excel">
                </ext:Button>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
