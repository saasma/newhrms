﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using Utils.Helper;
using System.IO;
using Utils;
using Utils.Security;
using Utils.Calendar;

namespace Web.CP
{
    public partial class LateEntrySkipDate : BasePage
    {


        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.HumanResource;
            }
        }
     
        protected void Page_Load(object sender, EventArgs e)
        {
           


            if (!X.IsAjaxRequest && !IsPostBack)
            {

               
               Initialize();

                              

            }
        }

    
        private void Initialize()
        {

            selectorBranch.Store[0].DataSource = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId);
            selectorBranch.Store[0].DataBind();

        }

        protected void btnAddNew_Click(object sender, DirectEventArgs e)
        {
            ClearControls();
            window.Show();
        }

        protected void ClearControls()
        {
            txtDate.Text = "";
            hiddenValue.Text = "";
            txtComment.Text = "";
            selectorBranch.SelectedItems.Clear();

        }

        protected void btnSave_Click(object sender, DirectEventArgs e)
        {
            Page.Validate("SaveUpdProjectAssociation");
            if (Page.IsValid)
            {
                AttendanceSkipLateDeductDate obj = new AttendanceSkipLateDeductDate();


                obj.DateEng = DateTime.Parse(txtDate.Text.Trim());
                obj.Comment = txtComment.Text.Trim();

                if (!string.IsNullOrEmpty(hiddenValue.Text))
                    obj.ID = int.Parse(hiddenValue.Text);

               
                foreach (Ext.Net.ListItem item in selectorBranch.SelectedItems)
                {
                    obj.AttendanceSkipBranches.Add(new AttendanceSkipBranch {BranchID=int.Parse(item.Value) });
                }

            

                Status status = LeaveAttendanceManager.SaveUpdateSkipDate(obj);
                if (status.IsSuccess)
                {
                    if (!string.IsNullOrEmpty(hiddenValue.Text))
                        NewMessage.ShowNormalMessage("Record updated successfully.");
                    else
                        NewMessage.ShowNormalMessage("Record saved successfully.");
                    window.Close();
                    storeGrid.Reload();

                }
                else
                {
                    NewMessage.ShowWarningMessage(status.ErrorMessage);
                }

            }
        }

        protected void btnEdit_Click(object sender, DirectEventArgs e)
        {

            int id = int.Parse(hiddenValue.Text);


            AttendanceSkipLateDeductDate obj = LeaveAttendanceManager.GetDate(id);
            if (obj != null)
            {
                
                txtDate.Text = obj.DateEng.ToString();
                txtComment.Text = obj.Comment;

                selectorBranch.SelectedItems.Clear();
                foreach (AttendanceSkipBranch item in obj.AttendanceSkipBranches)
                {
                    selectorBranch.SelectedItems.Add(new Ext.Net.ListItem { Value=item.BranchID.ToString() });
                }

                selectorBranch.UpdateSelectedItems();

                window.Show();
            }
        }

        protected void btnDelete_Click(object sender, DirectEventArgs e)
        {
            int id = int.Parse(hiddenValue.Text);

            LeaveAttendanceManager.DeleteSkipDate(new AttendanceSkipLateDeductDate { ID = id });

            NewMessage.ShowNormalMessage("Record deleted successfully.");
            storeGrid.Reload();

        }



        protected void Store_ReadData(object sender, StoreReadDataEventArgs e)
        {
            int totalRecords = 0;


            DateTime? startdate = null;
            DateTime? todate = null;

            if (!string.IsNullOrEmpty(calFromDate.RawText))
                startdate = calFromDate.SelectedDate.Date;

            if (!string.IsNullOrEmpty(calToDate.RawText))
                todate = calToDate.SelectedDate.Date;


            List<AttendanceSkipLateDeductDate> list = LeaveAttendanceManager.GetSkipLateDates(startdate, todate);

            e.Total = totalRecords;

            //if (list.Any())
            storeGrid.DataSource = list;
            storeGrid.DataBind();

        }

        public void btnExport_Click(object sender, EventArgs e)
        {

            DateTime? startdate = null;
            DateTime? todate = null;

            if (!string.IsNullOrEmpty(calFromDate.RawText))
                startdate = calFromDate.SelectedDate.Date;

            if (!string.IsNullOrEmpty(calToDate.RawText))
                todate = calToDate.SelectedDate.Date;


            List<AttendanceSkipLateDeductDate> list = LeaveAttendanceManager.GetSkipLateDates(startdate, todate);



            Bll.ExcelHelper.ExportToExcel("Late Skip Dates", list,
                new List<String> { },
            new List<String>() { },
            new Dictionary<string, string>() {},
            new List<string>() { }
            , new Dictionary<string, string>() {
              
            }
            , new List<string> { });


        }


    }
}