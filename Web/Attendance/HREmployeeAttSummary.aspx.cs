﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using BLL.BO;
using System.IO;
using System.Text;
using Utils.Calendar;
using Bll;

namespace Web.Attendance
{
    public partial class HREmployeeAttSummary : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

            }
            lblMsg.Html = "";
        }

        protected void btnLoad_Click(object sender, DirectEventArgs e)
        {
            if (string.IsNullOrEmpty(txtStartDate.Text.Trim()))
            {
                NewMessage.ShowWarningMessage("Start date is required.");
                return;
            }

            if (string.IsNullOrEmpty(txtEndDate.Text.Trim()))
            {
                NewMessage.ShowWarningMessage("End date is required.");
                return;
            }

            //this.gridEmployeeLeaveSummary.ColumnModel.Set("Hidden", Column10);


            DateTime startDateEng = BLL.BaseBiz.GetEngDate(txtStartDate.Text.Trim(), IsEnglish);
            DateTime endDateEng = BLL.BaseBiz.GetEngDate(txtEndDate.Text.Trim(), IsEnglish);



            if (startDateEng >= endDateEng)
            {
                NewMessage.ShowWarningMessage("End date should be greater than start date.");
                return;
            }

            CustomDate starDateCD = CustomDate.GetCustomDateFromString(txtStartDate.Text.Trim(), IsEnglish);
            PayrollPeriod payrollPeriod = CommonManager.GetPayrollPeriod(starDateCD.Month, starDateCD.Year);

            if (payrollPeriod == null)
            {
                lblMsg.Html = "Start date payroll period does not exists.";
                return;
            }

            CustomDate endDateCD = CustomDate.GetCustomDateFromString(txtEndDate.Text.Trim(), IsEnglish);
            PayrollPeriod endPayrollPeriod = CommonManager.GetPayrollPeriod(endDateCD.Month, endDateCD.Year);
            if (endPayrollPeriod == null)
            {
                lblMsg.Html = "End date payroll period does not exists.";
                return;
            }

            if(BLL.BaseBiz.PayrollDataContext.Attendences.Any(x=>x.PayrollPeriodId == endPayrollPeriod.PayrollPeriodId)==false)
            {
                lblMsg.Html = "End date payroll period attendance not saved, please save the attendance of " + DateHelper.GetMonthName(endPayrollPeriod.Month,IsEnglish) + " from <a href='../CP/AttendanceTracking.aspx'>Attendance Register</a> for accurate data.";
                return;
            }


            List<LLeaveType> listLLeaveType = NewHRManager.GetLeaveType();

            List<GetEmployeeLeaveDaysForPeriodResult> list = NewHRManager.GetEmployeeLeaveList(startDateEng, endDateEng, payrollPeriod.PayrollPeriodId);
            if (list.Count > 0)
            {

                gridEmployeeLeaveSummary.GetStore().DataSource = list.Where(x => x.EmployeeId != 0).ToList();
                gridEmployeeLeaveSummary.GetStore().DataBind();



                if (listLLeaveType.Count < 12)
                {
                    colLT12.Hide();
                    colTLT12.Hide();
                }

                if (listLLeaveType.Count < 11)
                {
                    colLT11.Hide();
                    colTLT11.Hide();
                }

                if (listLLeaveType.Count < 10)
                {
                    colLT10.Hide();
                    colTLT10.Hide();
                }

                if (listLLeaveType.Count < 9)
                {
                    colLT9.Hide();
                    colTLT9.Hide();
                }

                if (listLLeaveType.Count < 8)
                {
                    colLT8.Hide();
                    colTLT8.Hide();
                }

                if (listLLeaveType.Count < 7)
                {
                    colLT7.Hide();
                    colTLT7.Hide();
                }

                if (listLLeaveType.Count < 6)
                {
                    colLT6.Hide();
                    colTLT6.Hide();
                }

                if (listLLeaveType.Count < 5)
                {
                    colLT5.Hide();
                    colTLT5.Hide();
                }

                if (listLLeaveType.Count < 4)
                {
                    colLT4.Hide();
                    colTLT4.Hide();
                }

                if (listLLeaveType.Count < 3)
                {
                    colLT3.Hide();
                    colTLT3.Hide();
                }

                if (listLLeaveType.Count < 2)
                {
                    colLT2.Hide();
                    colTLT2.Hide();
                }

                if (listLLeaveType.Count < 1)
                {
                    colLT1.Hide();
                    colTLT1.Hide();
                }

                if (listLLeaveType.Count >= 1)
                    colLT1.Text = listLLeaveType[0].Abbreviation;

                if (listLLeaveType.Count >= 2)
                    colLT2.Text = listLLeaveType[1].Abbreviation;

                if (listLLeaveType.Count >= 3)
                    colLT3.Text = listLLeaveType[2].Abbreviation;

                if (listLLeaveType.Count >= 4)
                    colLT4.Text = listLLeaveType[3].Abbreviation;

                if (listLLeaveType.Count >= 5)
                    colLT5.Text = listLLeaveType[4].Abbreviation;

                if (listLLeaveType.Count >= 6)
                    colLT6.Text = listLLeaveType[5].Abbreviation;

                if (listLLeaveType.Count >= 7)
                    colLT7.Text = listLLeaveType[6].Abbreviation;

                if (listLLeaveType.Count >= 8)
                    colLT8.Text = listLLeaveType[7].Abbreviation;

                if (listLLeaveType.Count >= 9)
                    colLT9.Text = listLLeaveType[8].Abbreviation;

                if (listLLeaveType.Count >= 10)
                    colLT10.Text = listLLeaveType[9].Abbreviation;

                if (listLLeaveType.Count >= 11)
                    colLT11.Text = listLLeaveType[10].Abbreviation;

                if (listLLeaveType.Count >= 12)
                    colLT12.Text = listLLeaveType[11].Abbreviation;

                gridEmployeeLeaveSummary.Show();


                gridEmployeeLeaveSummary.AddCls("cellBorderCls");

                gridTotal.GetStore().DataSource = list.Where(x => x.EmployeeId == 0);
                gridTotal.GetStore().DataBind();
                gridTotal.Show();
                gridTotal.AddCls("cellBorderCls");
            }

        }


        protected void btnExcelPrint_Click(object sender, EventArgs e)
        {


            DateTime startDateEng = BLL.BaseBiz.GetEngDate(txtStartDate.Text.Trim(), IsEnglish);
            DateTime endDateEng = BLL.BaseBiz.GetEngDate(txtEndDate.Text.Trim(), IsEnglish);



            if (startDateEng >= endDateEng)
            {
                NewMessage.ShowWarningMessage("End date should be greater than start date.");
                return;
            }

            CustomDate starDateCD = CustomDate.GetCustomDateFromString(txtStartDate.Text.Trim(), IsEnglish);
            PayrollPeriod payrollPeriod = CommonManager.GetPayrollPeriod(starDateCD.Month, starDateCD.Year);

            if (payrollPeriod == null)
                return;

            List<LLeaveType> listLLeaveType = NewHRManager.GetLeaveType();

            List<GetEmployeeLeaveDaysForPeriodResult> list = NewHRManager.GetEmployeeLeaveList(startDateEng, endDateEng, payrollPeriod.PayrollPeriodId);
            if (list.Count > 0)
            {

                //list = list.OrderByDescending(x => x.EmployeeId).ToList();


                List<String> _ColumnToHide = new List<string>();

              

                if (listLLeaveType.Count < 12)
                {
                    //colLT12.Hide();
                    //colTLT12.Hide();
                    _ColumnToHide.Add("LT12");

                }

                if (listLLeaveType.Count < 11)
                {
                    //colLT11.Hide();
                    //colTLT11.Hide();
                    _ColumnToHide.Add("LT11");
                }

                if (listLLeaveType.Count < 10)
                {
                    //colLT10.Hide();
                    //colTLT10.Hide();
                    _ColumnToHide.Add("LT10");
                }

                if (listLLeaveType.Count < 9)
                {
                    //colLT9.Hide();
                    //colTLT9.Hide();
                    _ColumnToHide.Add("LT9");
                }

                if (listLLeaveType.Count < 8)
                {
                    //colLT8.Hide();
                    //colTLT8.Hide();
                    _ColumnToHide.Add("LT8");
                }

                if (listLLeaveType.Count < 7)
                {
                    //colLT7.Hide();
                    //colTLT7.Hide();
                    _ColumnToHide.Add("LT7");
                }

                if (listLLeaveType.Count < 6)
                {
                    //colLT6.Hide();
                    //colTLT6.Hide();
                    _ColumnToHide.Add("LT6");
                }

                if (listLLeaveType.Count < 5)
                {
                    //colLT5.Hide();
                    //colTLT5.Hide();
                    _ColumnToHide.Add("LT5");
                }

                if (listLLeaveType.Count < 4)
                {
                    //colLT4.Hide();
                    //colTLT4.Hide();
                    _ColumnToHide.Add("LT4");
                }

                if (listLLeaveType.Count < 3)
                {
                    //colLT3.Hide();
                    //colTLT3.Hide();
                    _ColumnToHide.Add("LT3");
                }

                if (listLLeaveType.Count < 2)
                {
                    //colLT2.Hide();
                    //colTLT2.Hide();
                    _ColumnToHide.Add("LT2");
                }

                if (listLLeaveType.Count < 1)
                {
                    //colLT1.Hide();
                    //colTLT1.Hide();
                    _ColumnToHide.Add("LT1");
                }

                Dictionary<string, string> _listRenameColumn = new Dictionary<string, string>();



                if (listLLeaveType.Count >= 1)
                    // colLT1.Text = listLLeaveType[0].Abbreviation;
                    _listRenameColumn.Add("LT1", listLLeaveType[0].Abbreviation);


                if (listLLeaveType.Count >= 2)
                    //colLT2.Text = listLLeaveType[1].Abbreviation;
                    _listRenameColumn.Add("LT2", listLLeaveType[1].Abbreviation);

                if (listLLeaveType.Count >= 3)
                    //colLT3.Text = listLLeaveType[2].Abbreviation;
                    _listRenameColumn.Add("LT3", listLLeaveType[2].Abbreviation);

                if (listLLeaveType.Count >= 4)
                    //colLT4.Text = listLLeaveType[3].Abbreviation;
                    _listRenameColumn.Add("LT4", listLLeaveType[3].Abbreviation);

                if (listLLeaveType.Count >= 5)
                    //colLT5.Text = listLLeaveType[4].Abbreviation;
                    _listRenameColumn.Add("LT5", listLLeaveType[4].Abbreviation);

                if (listLLeaveType.Count >= 6)
                    //colLT6.Text = listLLeaveType[5].Abbreviation;
                    _listRenameColumn.Add("LT6", listLLeaveType[5].Abbreviation);

                if (listLLeaveType.Count >= 7)
                    //colLT7.Text = listLLeaveType[6].Abbreviation;
                    _listRenameColumn.Add("LT7", listLLeaveType[6].Abbreviation);

                if (listLLeaveType.Count >= 8)
                    //colLT8.Text = listLLeaveType[7].Abbreviation;
                    _listRenameColumn.Add("LT8", listLLeaveType[7].Abbreviation);

                if (listLLeaveType.Count >= 9)
                    //colLT9.Text = listLLeaveType[8].Abbreviation;
                    _listRenameColumn.Add("LT9", listLLeaveType[8].Abbreviation);

                if (listLLeaveType.Count >= 10)
                    //colLT10.Text = listLLeaveType[9].Abbreviation;
                    _listRenameColumn.Add("LT10", listLLeaveType[9].Abbreviation);

                if (listLLeaveType.Count >= 11)
                    //colLT11.Text = listLLeaveType[10].Abbreviation;
                    _listRenameColumn.Add("LT11", listLLeaveType[10].Abbreviation);

                if (listLLeaveType.Count >= 12)
                    //colLT12.Text = listLLeaveType[11].Abbreviation;
                    _listRenameColumn.Add("LT12", listLLeaveType[11].Abbreviation);


                _listRenameColumn.Add("TotalDays", "Total Days");
                _listRenameColumn.Add("LeaveDays", "Leave Days");
                _listRenameColumn.Add("StdDays", "Std Days");
                _listRenameColumn.Add("AbsDays", "ABS");
                _listRenameColumn.Add("WorkDays", "Work Days");
                
                //gridTotal.GetStore().DataSource = list.Where(x => x.EmployeeId == 0);

                ExcelHelper.ExportToExcel("EmployeeAttSummary", list,
                    _ColumnToHide,
                    //new List<String>() { "EmployeeId", "SN", "Status" },
              new List<String>() { },
              _listRenameColumn,
                    // new Dictionary<string, string>() { { "Position", "Job Title" }, { "HireDate", "Hire Date" } },
              new List<string>() { }
              , new Dictionary<string, string>() { },
              new List<string> { "Name", "TotalDays", "WH", "PH", "StdDays", "LT1", "LT2", "LT3", "LT4", "LT5", "LT6", "LT7", "LT8", "LT9", "LT10", "LT11", "LT12", "UPL", "AbsDays", "LeaveDays", "WorkDays","Percentage" }
             );
            }

        }
    }
}