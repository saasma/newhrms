<%@ Page Title="Employee Attendance Detail" Language="C#" MaintainScrollPositionOnPostback="true"
    MasterPageFile="~/Master/NewDetails.Master" AutoEventWireup="true" CodeBehind="HRAttendanceEmpMonthDetails.aspx.cs"
    Inherits="Web.CP.Report.HRAttendanceEmpMonthDetails" %>

<%@ Register Src="UserControl/EmployeeAtteCtl.ascx" TagName="ExceptionCtl" TagPrefix="uc3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Employee Attendance Details
                </h4>
            </div>
        </div>
    </div>
      <div class="contentpanel" style="margin-top: 0px !important; padding-top: 0px !important;">
        <div class="innerLR">
    (Employee should be selected for the result)
    <uc3:ExceptionCtl ID="ExceptionCtl1" PageType="Admin" runat="server" />

            </div>
          </div>
</asp:Content>
