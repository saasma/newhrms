﻿<%@ Page Title="Time Request Attendance Report" Language="C#" MasterPageFile="~/Master/NewDetails.Master" AutoEventWireup="true" CodeBehind="TimeReqAttendanceReport.aspx.cs" Inherits="Web.Attendance.TimeReqAttendanceReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">

<script type="text/javascript">

    function searchList() {
         
              <%=PagingToolbar1.ClientID %>.doRefresh();
        }
        
    var selEmpId = null;
    function ACE_item_selected(source, eventArgs) {
        var value = eventArgs.get_value();
        selEmpId = value;
        document.getElementById('<%= hiddenEmployeeID.ClientID %>').value = selEmpId;
    }

</script>
<style type="text/css">
    .holiday, .holiday a, .holiday td
    {
        color: #469146;
        background-color: #F0FFF0 !important;
    }
    .leave, .leave a, .leave td
    {
        color: #D95CA9;
        background-color: #FFF0F5 !important;
    }
    
    .weeklyholiday, .weeklyholiday a, .weeklyholiday td
    {
        color: #8F8F1A;
        background-color: #FAFAD2 !important;
    }
</style>
<script type="text/javascript" src="<%= ResolveUrl("~/Employee/override.js?v=") + Web.Helper.WebHelper.Version %>"></script>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">


    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Time Request Attendance Report
                </h4>
            </div>
        </div>
    </div>

    <div class="contentpanel" style="margin-top: 0px !important; padding: 0px !important;">
    <div class="innerLR">
        <div class="alert alert-info" style="margin-top: 10px; padding-top: 0px!important;">
            <table style="position:relative">
                <tr>
                    <td style="height:60px">
                        <ext:DateField LabelWidth="180" MarginSpec="0 5 0 5" Width="100" FieldLabel="From"
                            ID="calFromDate" runat="server" LabelAlign="Top" LabelSeparator="">
                            <Plugins>
                                <ext:GenericPlugin ID="HighlightDates1" runat="server" InstanceName="Ext.ux.DatePicker.HighlightWithNepDates" />
                            </Plugins>
                        </ext:DateField>
                    </td>
                    <td style="padding-left:10px;"> 
                        <ext:DateField LabelWidth="180" MarginSpec="0 5 0 5" Width="100" FieldLabel="To"
                            ID="calToDate" runat="server" LabelAlign="Top" LabelSeparator="">
                            <Plugins>
                                <ext:GenericPlugin ID="GenericPlugin1" runat="server" InstanceName="Ext.ux.DatePicker.HighlightWithNepDates" />
                            </Plugins>
                        </ext:DateField>
                    </td>
                    <td style="padding-top: 3px;padding-left:10px;" runat="server" id="tdEmpSearch">
                        <span style="color: #000000">Employee </span>
                        <ext:Hidden runat="server" ID="hiddenEmployeeID" />
                        <asp:TextBox Style='margin-top: 5px;' Width="180px" ID="txtEmpSearch" runat="server" />
                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtEmpSearch" 
                            WatermarkText="Search Text" WatermarkCssClass="searchBoxText" />
                        <cc1:AutoCompleteExtender EnableCaching="true" ID="AutoCompleteExtenderOrganization"
                            runat="server" MinimumPrefixLength="2" ServiceMethod="GetEmployeeNamesAndEINWithRetiredAlso"
                            ServicePath="~/PayrollService.asmx" TargetControlID="txtEmpSearch" OnClientItemSelected="ACE_item_selected"
                            CompletionSetCount="10" CompletionInterval="250" CompletionListCssClass="AutoExtender"
                            CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight">
                        </cc1:AutoCompleteExtender>
                    </td>
                    <td style="padding-left:10px;"> 
                        <ext:Button ID="btnLoad" runat="server" Text="Load" Width="100" 
                            MarginSpec="25 10 10 10">
                            <Listeners>
                                <Click Fn="searchList">
                                </Click>
                            </Listeners>
                        </ext:Button>
                    </td>
                </tr>
                <%--  <tr>
                    <td>
                        <ext:Checkbox runat="server" ID="chkIsLateOnly" BoxLabel="Late Only" />
                    </td>
                    <td>
                        <ext:Checkbox runat="server" ID="chkIsAbsentOnly" BoxLabel="Absent Only" />
                    </td>
                </tr>--%>
            </table>
        </div>
        <ext:GridPanel StyleSpec="margin-top:15px;" EnableViewState="false" ID="gridBranchTransfer"
            runat="server" OnReadData="Store_ReadData">
            <Store>
                <ext:Store PageSize="50" ID="storeEmpList" RemoteSort="false" RemotePaging="true"
                    runat="server" AutoLoad="false" OnReadData="Store_ReadData">
                    <Proxy>
                        <ext:PageProxy />
                    </Proxy>
                    <Model>
                        <ext:Model ID="Model1" runat="server">
                            <Fields>
                                <ext:ModelField Name="RowNumber" Type="String" />
                                <ext:ModelField Name="EmployeeId" Type="String" />
                                <ext:ModelField Name="Name" Type="String" />
                                <ext:ModelField Name="EngDateFormatted" Type="String" />
                                <ext:ModelField Name="DeviceInString" Type="String" />
                                <ext:ModelField Name="DeviceOutString" Type="String" />
                                <ext:ModelField Name="RequestInString" Type="String" />
                                <ext:ModelField Name="RequestOutString" Type="String" />
                                <ext:ModelField Name="InComment" Type="String" />
                                <ext:ModelField Name="OutComment" Type="String" />
                                <ext:ModelField Name="ActualDate" Type="String" />
                                
                            </Fields>
                        </ext:Model>
                    </Model>
                </ext:Store>
            </Store>
            <ColumnModel ID="ColumnModel1" runat="server">
                <Columns>
                    <ext:Column ID="colFromDate" runat="server" Text="SN" DataIndex="RowNumber" MenuDisabled="false"
                        Locked="true" Sortable="false" Align="Center" Width="40" />
                    <ext:Column ID="Column10" runat="server" Text="EIN" DataIndex="EmployeeId" MenuDisabled="false"
                        Locked="true" Sortable="false" Align="Center" Width="50" />
                    <ext:Column ID="Column_Name" runat="server" Text="Name" DataIndex="Name" MenuDisabled="false"
                        Locked="true" Sortable="false" Align="Left" Width="150" />                  
                    <ext:Column ID="Column11" runat="server" Text="Date" DataIndex="EngDateFormatted" MenuDisabled="false"
                        Locked="true" Sortable="false" Align="Left" Width="100" />
                    <ext:Column ID="Column4" runat="server" Text="Nep Date" DataIndex="ActualDate" MenuDisabled="false"
                        Sortable="false" Align="Left" Width="100" />                   
                    <ext:Column ID="Column1" runat="server" Text="Device In" DataIndex="DeviceInString"
                        MenuDisabled="false" Sortable="false" Align="Right" Width="80" />
                    <ext:Column ID="Column3" runat="server" Text="Device Out" DataIndex="DeviceOutString"
                        MenuDisabled="false" Sortable="false" Align="Right" Width="80" />
                    <ext:Column ID="Column8" runat="server" Text="Request In" DataIndex="RequestInString"
                        MenuDisabled="false" Sortable="false" Align="Right" Width="80" />
                    <ext:Column ID="income3" runat="server" Text="Request Out" DataIndex="RequestOutString" MenuDisabled="false"
                        Sortable="false" Align="Right" Width="80">
                    </ext:Column>
                     <ext:Column ID="Column5" runat="server" Text="In Comment" DataIndex="InComment" MenuDisabled="false"
                        Sortable="false" Align="Left" Width="300">
                    </ext:Column>
                    <ext:Column ID="Column7" runat="server" Text="Out Comment" DataIndex="OutComment"
                        MenuDisabled="false" Sortable="false" Align="Left" Width="300">
                    </ext:Column>
                </Columns>
            </ColumnModel>
            <View>
                <ext:GridView ID="GridView1" runat="server">
                    <Listeners>
                    </Listeners>
                </ext:GridView>
            </View>
            <BottomBar>
                <ext:PagingToolbar ID="PagingToolbar1" runat="server" DisplayInfo="true">
                </ext:PagingToolbar>
            </BottomBar>
        </ext:GridPanel>
        <div class="alert alert-info" style="margin-top: 15px; height: 50px;">
            <div style="float: right;">
                <ext:Button runat="server" AutoPostBack="true" OnClick="btnExport_Click" ID="btnExport"
                    Text="<i></i>Export To Excel">
                </ext:Button>
            </div>
        </div>
    </div>
</div>


</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
