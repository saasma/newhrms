﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using Utils.Helper;
using System.IO;
using Utils;
using Utils.Security;
using Utils.Calendar;

namespace Web.CP
{
    public partial class CumulativeLateProcessingDetails : BasePage
    {


        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.HumanResource;
            }
        }
        new void Load()
        {
            List<Branch> branchList = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId);
            cmbBranch.Store[0].DataSource = branchList;
            cmbBranch.Store[0].DataBind();

            cmbDepartment.Store[0].DataSource = DepartmentManager.GetAllDepartments().OrderBy(x => x.Name).ToList();
            cmbDepartment.Store[0].DataBind();

            cmbDesignation.Store[0].DataSource = new CommonManager().GetAllDesignations().OrderBy(x => x.LevelAndDesignation).ToList();
            cmbDesignation.Store[0].DataBind();

            this.storeLevel.DataSource = ActivityManager.GetLevels().OrderBy(x => x.Name);
            this.storeLevel.DataBind();


            calFromDate.SelectedDate = DateTime.Now;
            calToDate.SelectedDate = DateTime.Now;

            //int branchId = -1, depId = -1, levelId = -1, designationId = -1, empid = -1;
            //DateTime startdate, todate;
            //bool isGroup = false;

            //startdate = Convert.ToDateTime(Request.QueryString["from"]);
            //todate = Convert.ToDateTime(Request.QueryString["to"]);

            //empid = Convert.ToInt32(Request.QueryString["ein"]);
            //branchId = Convert.ToInt32(Request.QueryString["gb"]);
            //depId = Convert.ToInt32(Request.QueryString["gd"]);
            //levelId = Convert.ToInt32(Request.QueryString["gp"]);
            //designationId = Convert.ToInt32(Request.QueryString["gdd"]);

           

            //calFromDate.SelectedDate = startdate;
            //calToDate.SelectedDate = todate;

            //ExtControlHelper.ComboBoxSetSelected(branchId, cmbBranch);
            //ExtControlHelper.ComboBoxSetSelected(depId, cmbDepartment);
            //ExtControlHelper.ComboBoxSetSelected(levelId, cmbLevel);
            //ExtControlHelper.ComboBoxSetSelected(designationId, cmbDesignation);

            //if (empid != -1)
            //{

            //    cmbEmpSearch.InsertItem(0, EmployeeManager.GetEmployeeName(empid), empid);
            //    cmbEmpSearch.SelectedItem.Value = empid.ToString();
            //    cmbEmpSearch.SelectedItem.Text = EmployeeManager.GetEmployeeName(empid);
            //    cmbEmpSearch.UpdateSelectedItems();
            //}

        }
        protected void Page_Load(object sender, EventArgs e)
        {
           


            if (!X.IsAjaxRequest && !IsPostBack)
            {

                Load();

                //details.InnerHtml = "";

                //if (empid != -1 && empid != 0)
                //    details.InnerHtml = EmployeeManager.GetEmployeeById(empid).Name;

                //if (branchId != -1 && branchId != 0)
                //{
                //    details.InnerHtml += BranchManager.GetBranchById(branchId).Name;
                //}

                //if (depId != -1 && depId != 0)
                //{
                //    details.InnerHtml += "  ," + DepartmentManager.GetDepartmentById(depId).Name;
                //}

                //if (levelId != -1 && levelId != 0)
                //{
                //    details.InnerHtml += "  ," + NewPayrollManager.GetLevelById(levelId).Name;
                //}

                //if (designationId != -1 && designationId != 0)
                //{
                //    details.InnerHtml += "  ," + new CommonManager().GetDesignationById(designationId).Name;
                //}

                //details.InnerHtml += "  from  " + startdate.ToShortDateString() + "  to  " + todate.ToShortDateString();

               Initialize();

                              

            }
        }

    
        private void Initialize()
        {
            

        }

        private void Clear()
        {
            
        }


        protected void Store_ReadData(object sender, StoreReadDataEventArgs e)
        {
            int totalRecords = 0;

            int branchId = -1, depId = -1, levelId = -1, designationId = -1;

            if (cmbBranch.SelectedItem != null && cmbBranch.SelectedItem.Value != null)
                branchId = int.Parse(cmbBranch.SelectedItem.Value);
            if (cmbDepartment.SelectedItem != null && cmbDepartment.SelectedItem.Value != null)
                depId = int.Parse(cmbDepartment.SelectedItem.Value);
            if (cmbLevel.SelectedItem != null && cmbLevel.SelectedItem.Value != null)
                levelId = int.Parse(cmbLevel.SelectedItem.Value);
            if (cmbDesignation.SelectedItem != null && cmbDesignation.SelectedItem.Value != null)
                designationId = int.Parse(cmbDesignation.SelectedItem.Value);

            int employeeId = -1;

            int.TryParse(cmbEmpSearch.SelectedItem.Value, out employeeId);

            if (employeeId == 0)
                employeeId = -1;

            hdnSortBy.Text = (e.Sort[0].Property + " " + e.Sort[0].Direction).ToLower();

            DateTime startdate = calFromDate.SelectedDate.Date;
            DateTime todate = calToDate.SelectedDate.Date;

            List<GetListForCumulativeLateProcessingResult> list = AttendanceManager.GetCumulativeLateProcessing
                (branchId, depId, levelId, designationId, employeeId, e.Page - 1, startdate, todate, e.Limit, (e.Sort[0].Property + " " + e.Sort[0].Direction).ToLower(), ref totalRecords,
                chkIsLateOnly.Checked);

            e.Total = totalRecords;

            //if (list.Any())
            storeEmpList.DataSource = list;
            storeEmpList.DataBind();

        }



        public void btnSaveMarkLateDeduction_Save(object sender, DirectEventArgs e)
        {

            string entryline = e.ExtraParams["gridItems"];
            List<GetListForCumulativeLateProcessingResult> entryLineObj = JSON.Deserialize<List<GetListForCumulativeLateProcessingResult>>(entryline);

            foreach (GetListForCumulativeLateProcessingResult item in entryLineObj)
            {
                string[] values = item.RecordID.Split(new char[] { ','});

                item.EmployeeId = int.Parse(values[0]);
                item.Date = DateTime.Parse(values[1]);
            }


            AttendanceManager.SetSaveMarkLateDeduction(entryLineObj);

            NewMessage.ShowNormalMessage("Late marking deduction has been updated for " + entryLineObj.Count + " records.","searchList");

        }

        public void btnSaveLateMark_Save(object sender, DirectEventArgs e)
        {

            string entryline = e.ExtraParams["gridItems"];
            List<GetListForCumulativeLateProcessingResult> entryLineObj = JSON.Deserialize<List<GetListForCumulativeLateProcessingResult>>(entryline);

            foreach (GetListForCumulativeLateProcessingResult item in entryLineObj)
            {
                string[] values = item.RecordID.Split(new char[] { ','});

                item.EmployeeId = int.Parse(values[0]);
                item.Date = DateTime.Parse(values[1]);
            }


            AttendanceManager.SetUpdateSkipCumLateDeduction(entryLineObj);

            NewMessage.ShowNormalMessage("Skip Late deduction has been updated for " + entryLineObj.Count + " records.","searchList");

        }

        public void btnExport_Click(object sender, EventArgs e)
        {

            int totalRecords = 0;

            int branchId = -1, depId = -1, levelId = -1, designationId = -1;

            if (cmbBranch.SelectedItem != null && cmbBranch.SelectedItem.Value != null)
                branchId = int.Parse(cmbBranch.SelectedItem.Value);
            if (cmbDepartment.SelectedItem != null && cmbDepartment.SelectedItem.Value != null)
                depId = int.Parse(cmbDepartment.SelectedItem.Value);
            if (cmbLevel.SelectedItem != null && cmbLevel.SelectedItem.Value != null)
                levelId = int.Parse(cmbLevel.SelectedItem.Value);
            if (cmbDesignation.SelectedItem != null && cmbDesignation.SelectedItem.Value != null)
                designationId = int.Parse(cmbDesignation.SelectedItem.Value);

            int employeeId = -1;

            int.TryParse(cmbEmpSearch.SelectedItem.Value, out employeeId);

            if (employeeId == 0)
                employeeId = -1;

            DateTime startdate = calFromDate.SelectedDate.Date;
            DateTime todate = calToDate.SelectedDate.Date;

            List<GetListForLateProcessingResult> list = AttendanceManager.GetLateProcessing
                (branchId, depId, levelId, designationId, employeeId, 0, startdate, todate, 999999,hdnSortBy.Text.ToLower(), ref totalRecords,
                chkIsLateOnly.Checked);


            List<string> hiddenList = new List<string>();

            
            {
                hiddenList = new List<string>{"RecordID","TotalOTMinute","IsLate","IslateIn","TotalActualLate","TotalAbsent","Date","TotalStandardMin","IslateIn","TotalWorkedMin","TotalMissingOrExtra","TotalLate","ClockIn","ClockOut","DayType", "Total","TotalDays","QueryString","BranchID","DepartmentID","LevelPositionID","DesignationID",
                    "LateMin","WorkedMin","TotalRows","OfficeIn","OfficeOut","Branch","Department","LevelPosition","Designation","OutComments"};
            }

            string title1 = "Late Processing Details";
            string title2 = "";

            if (employeeId != -1 && employeeId != 0)
                title2 += " for " + EmployeeManager.GetEmployeeById(employeeId).Name;

            if (branchId != -1 && branchId != 0)
                title2 += " Branch " + BranchManager.GetBranchById(branchId).Name;

            title2 += " from " + startdate.ToShortDateString() + " to " + todate.ToShortDateString();

            Bll.ExcelHelper.ExportToExcel("Late Processing Details", list,
                hiddenList,
            new List<String>() { },
            new Dictionary<string, string>() { {"EngDate","Date"}, {"RowNumber","SN"},{"EmployeeId","EIN"},
                {"IdCardNo","I No"},{"WorkedMinText","Worked hrs"},
                {"LateMinText","Late hr"}
                ,{"NepDate","Nep Date"},{"DayValueText","Day Type"},{"WorkPercent","Work %"} ,{"ClockInText","In"},{"ClockOutText","Out"}
                ,{"OfficeInText","Office In"},{"OfficeOutText","Office Out"}},
            new List<string>() { "RowNumber", "LateInCount", "Workdays", "AbsentDays", "LeaveDays", "WorkPercent" }
            , new Dictionary<string, string>() {
              {title1,title2}
            }
            , new List<string> { "RowNumber","EmployeeId","IdCardNo","Name","EngDate","NepDate","DayValueText","Branch","Department","LevelPosition","Designation"
                ,"StandardMinText","WorkedMinText","Shift","OfficeInText","OfficeOutText","ClockInText","ClockOutText","LateMinText","LateInCount",
                "WorkDays","AbsentDays","LeaveDays","MissingOrExtraText","IsLateMarked"});


        }


    }
}