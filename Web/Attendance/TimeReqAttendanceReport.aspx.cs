﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using BLL.Manager;
using Utils.Base;
using BLL;
using DAL;
using Utils.Web;
using Utils.Helper;
using System.Text.RegularExpressions;
using Ext.Net;
using Utils.Calendar;
using System.Text;

namespace Web.Attendance
{
    public partial class TimeReqAttendanceReport : BasePage
    {
        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.LeaveAndAttendance;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest && !IsPostBack)
            {
                CustomDate today = CustomDate.GetTodayDate(IsEnglish);
                calFromDate.SelectedDate = today.EnglishDate;
                calToDate.SelectedDate = calFromDate.SelectedDate;
            }
        }

        protected void Store_ReadData(object sender, StoreReadDataEventArgs e)
        {
            int employeeId = 0;

            if (!string.IsNullOrEmpty(txtEmpSearch.Text) && !txtEmpSearch.Text.ToLower().Equals("search text"))
                int.TryParse(hiddenEmployeeID.Text, out employeeId);

            if (string.IsNullOrEmpty(calFromDate.Text))
            {
                NewMessage.ShowWarningMessage("From date is required.");
                calFromDate.Focus();
                return;
            }

            if (string.IsNullOrEmpty(calToDate.Text))
            {
                NewMessage.ShowWarningMessage("To date is required.");
                calToDate.Focus();
                return;
            }

            DateTime startdate = calFromDate.SelectedDate.Date;
            DateTime todate = calToDate.SelectedDate.Date;

            List<AttendanceReportForTimeRequestResult> list = AttendanceManager.GetAttendanceReportForTimeRequest(employeeId, startdate, todate, e.Page - 1, e.Limit);

            int total = 0;
            if (list.Count > 0 && list[0].TotalRows != null)
                total = list[0].TotalRows.Value;

            e.Total = total;

            storeEmpList.DataSource = list;
            storeEmpList.DataBind();

        }

        public void btnExport_Click(object sender, EventArgs e)
        {
            int employeeId = 0;
            if (!string.IsNullOrEmpty(txtEmpSearch.Text) && !txtEmpSearch.Text.ToLower().Equals("search text"))
                int.TryParse(hiddenEmployeeID.Text, out employeeId);

            if (string.IsNullOrEmpty(calFromDate.Text))
            {
                NewMessage.ShowWarningMessage("From date is required.");
                calFromDate.Focus();
                return;
            }

            if (string.IsNullOrEmpty(calToDate.Text))
            {
                NewMessage.ShowWarningMessage("To date is required.");
                calToDate.Focus();
                return;
            }

            DateTime startdate = calFromDate.SelectedDate.Date;
            DateTime todate = calToDate.SelectedDate.Date;

            List<AttendanceReportForTimeRequestResult> list = AttendanceManager.GetAttendanceReportForTimeRequest(employeeId, startdate, todate, 0, int.MaxValue - 1000);

            List<string> hiddenList = new List<string>();

            {
                hiddenList = new List<string> { "TotalRows", "DeviceIn", "DeviceOut", "RequestIn", "RequestOut", "DateEng" };
            }

            string title1 = "Time Request Attendance Report";
            string title2 = "";

            if (employeeId != -1 && employeeId != 0)
                title2 += " for " + EmployeeManager.GetEmployeeById(employeeId).Name;



            title2 += " from " + startdate.ToShortDateString() + " to " + todate.ToShortDateString();

            Bll.ExcelHelper.ExportToExcel("Time Request Attendance Report", list, hiddenList,
            new List<String>() { },
            new Dictionary<string, string>() {  {"EmployeeId","EIN"}, {"RowNumber","SN"},{"DeviceInString","Device In"}
                ,{"DeviceOutString","Device Out"}, {"RequestInString","Time Request In"},{"RequestOutString","Time Request Out"} ,{"InComment","In Comment"},
                {"OutComment","Out Comment"}, {"ActualDate", "Nep Date"}, {"EngDateFormatted","Date"}
            },
            new List<string>() { }
            ,
            new List<string>() { "RowNumber", "EmployeeId" },
            new List<string>() {  }
            , new Dictionary<string, string>() {
              {title1,title2}
            }
            , new List<string> { "RowNumber","EmployeeId","Name", "EngDateFormatted","ActualDate", "DeviceInString","DeviceOutString",
                "RequestInString","RequestOutString","InComment"
                ,"OutComment"});


        }
    }
}