﻿<%@ Page Title="Attendance Details" Language="C#" MasterPageFile="~/Master/NewDetails.Master"
    AutoEventWireup="true" CodeBehind="AttendanceDetails.aspx.cs" Inherits="Web.CP.AttendanceDetails" %>

<%@ Register Src="~/newhr/UserControls/GenerateAttendanceRptCtl.ascx" TagName="GenerateAttendanceRptCtl"
    TagPrefix="uc1" %>
<%@ MasterType VirtualPath="~/Master/NewDetails.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">

    function searchList() {
              <%=PagingToolbar1.ClientID %>.doRefresh();
        }

 
   
  Ext.onReady
  (
    function()
    {
        
    }
  );

  var lateRender = function(value)
  {
    if(value == 'true')
        return 'Yes';

        return '';
  }

   var refreshparentWindow = function()
  {
  
    <%=lnkRebindDate.ClientID %>.fireEvent('Click'); 
    <%=AEWindow.ClientID %>.hide();
  }


   var getRowClass = function (record) {
    
        var dayValue = record.data.DayValueText;
        

         if(dayValue=="Working Day")
         {
            return "";
         }
         else if(dayValue=="Holiday")
         {
            return "holiday";
         }
         else if(dayValue=="Leave" || dayValue=="Half Leave")
         {
            return "leave";
         }
        
         else //if(dayValue=="Weekly Holiday")
         {
            return "weeklyholiday";
         }

      };

    </script>
    <style type="text/css">
        .holiday, .holiday a, .holiday td
        {
            color: #469146;
            background-color: #F0FFF0 !important;
        }
        .leave, .leave a, .leave td
        {
            color: #D95CA9;
            background-color: #FFF0F5 !important;
        }
        
        .weeklyholiday, .weeklyholiday a, .weeklyholiday td
        {
            color: #8F8F1A;
            background-color: #FAFAD2 !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <script type="text/javascript" src="<%= ResolveUrl("~/Employee/override.js?v=") + Web.Helper.WebHelper.Version %>"></script>
    <ext:Hidden ID="hdnEmployeeId" runat="server" />
    <ext:LinkButton ID="lnkRebindDate" Hidden="true" Text="" runat="server">
        <DirectEvents>
            <Click OnEvent="RebindDate">
            </Click>
        </DirectEvents>
    </ext:LinkButton>
    <ext:Hidden ID="hdnBranchDepartmentId" runat="server" />
    <ext:Hidden ID="hdnMsg" runat="server" />
    <ext:Hidden ID="hdnEmpSearch" runat="server" />
    <ext:Hidden ID="hdnSortBy" runat="server" />
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Attendance Details
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel" style="margin-top: 0px !important; padding-top: 0px !important;">
        <div class="innerLR">
            <table>
                <tr>
                    <td>
                        Last Proceed for &nbsp;<span id="dispLastDate" runat="server" style=""></span>
                           
                    </td>
                    <td style="padding-left: 50px;">
                        <ext:LinkButton ID="btnGenerateAttendance" Text="Generate Attendance Report" runat="server">
                            <DirectEvents>
                                <Click OnEvent="btnGenerateAttendance_Click">
                                    <EventMask ShowMask="true" />
                                </Click>
                            </DirectEvents>
                        </ext:LinkButton>
                    </td>
                </tr>
            </table>
           
            <div style="clear: both">
            </div>
            <span runat='server' style="font-size: 14px; padding-top: 15px; margin-bottom: 10px"
                id="details"></span>
            <div class="alert alert-info" style="margin-top: 10px; padding-top: 0px!important;">
                <ext:Container ID="listingContainer" runat="server">
                    <Content>
                        <table>
                            <tr>
                                <td>
                                    <ext:DateField LabelWidth="120" MarginSpec="0 5 0 5" Width="100" FieldLabel="From"
                                        ID="calFromDate" runat="server" LabelAlign="Top" LabelSeparator="">
                                        <Plugins>
                                            <ext:GenericPlugin ID="HighlightDates1" runat="server" InstanceName="Ext.ux.DatePicker.HighlightWithNepDates" />
                                        </Plugins>
                                    </ext:DateField>
                                </td>
                                <td>
                                    <ext:DateField LabelWidth="120" MarginSpec="0 5 0 5" Width="100" FieldLabel="To"
                                        ID="calToDate" runat="server" LabelAlign="Top" LabelSeparator="">
                                        <Plugins>
                                            <ext:GenericPlugin ID="GenericPlugin1" runat="server" InstanceName="Ext.ux.DatePicker.HighlightWithNepDates" />
                                        </Plugins>
                                    </ext:DateField>
                                </td>
                                <td>
                                    <ext:ComboBox Width="140" MatchFieldWidth="false" MarginSpec="0 5 0 5" LabelSeparator=""
                                        LabelAlign="Top" QueryMode="Local" ID="cmbBranch" DisplayField="Name" ValueField="BranchId"
                                        runat="server" FieldLabel="Branch">
                                        <Items>
                                            <ext:ListItem Index="0" Value="-1" Text="All">
                                            </ext:ListItem>
                                        </Items>
                                        <Store>
                                            <ext:Store ID="Store5" runat="server">
                                                <Model>
                                                    <ext:Model ID="Model7" IDProperty="BranchId" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="BranchId" />
                                                            <ext:ModelField Name="Name" />
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                    </ext:ComboBox>
                                </td>
                                <td>
                                    <ext:ComboBox FieldLabel="Department" MatchFieldWidth="false" ID="cmbDepartment"
                                        Width="140" MarginSpec="0 5 0 5" runat="server" ValueField="DepartmentId" DisplayField="Name"
                                        LabelAlign="Top" LabelSeparator="" QueryMode="Local">
                                        <Items>
                                            <ext:ListItem Index="0" Value="-1" Text="All">
                                            </ext:ListItem>
                                        </Items>
                                        <Store>
                                            <ext:Store ID="Store6" runat="server">
                                                <Model>
                                                    <ext:Model ID="Model8" IDProperty="DepartmentId" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="DepartmentId" />
                                                            <ext:ModelField Name="Name" />
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                    </ext:ComboBox>
                                </td>
                                <td>
                                    <ext:ComboBox ID="cmbLevel" MatchFieldWidth="false" runat="server" FieldLabel="Level/Position"
                                        Width="140" MarginSpec="0 5 0 5" DisplayField="Name" ValueField="LevelId" QueryMode="Local"
                                        LabelSeparator="" LabelAlign="Top">
                                        <Items>
                                            <ext:ListItem Index="0" Value="-1" Text="All">
                                            </ext:ListItem>
                                        </Items>
                                        <Store>
                                            <ext:Store ID="storeLevel" runat="server">
                                                <Model>
                                                    <ext:Model ID="modelLevel" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="LevelId" Type="Int" />
                                                            <ext:ModelField Name="Name" Type="String" />
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                    </ext:ComboBox>
                                </td>
                                <td>
                                    <ext:ComboBox MatchFieldWidth="false" FieldLabel="Designation" ID="cmbDesignation"
                                        Width="140" MarginSpec="0 5 0 5" runat="server" ValueField="Text" DisplayField="Text"
                                        LabelAlign="Top" LabelSeparator="" QueryMode="Local">
                                        <Items>
                                            <ext:ListItem Index="0" Value="All" Text="All">
                                            </ext:ListItem>
                                        </Items>
                                        <Store>
                                            <ext:Store ID="Store7" runat="server">
                                                <Model>
                                                    <ext:Model ID="Model9" IDProperty="Text" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="Text" />
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                    </ext:ComboBox>
                                </td>
                                <td>
                                    <ext:Store runat="server" ID="storeSearch" AutoLoad="false">
                                        <Proxy>
                                            <ext:AjaxProxy Url="../Handler/EmpSearchID.ashx">
                                                <ActionMethods Read="GET" />
                                                <Reader>
                                                    <ext:JsonReader Root="plants" TotalProperty="total" />
                                                </Reader>
                                            </ext:AjaxProxy>
                                        </Proxy>
                                        <Model>
                                            <ext:Model ID="Model5" IDProperty="EmployeeId" Name="ItemLineModel" runat="server">
                                                <Fields>
                                                    <ext:ModelField Name="Name" Type="String" />
                                                    <ext:ModelField Name="EmployeeId" Type="String" />
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                    <ext:ComboBox LabelSeparator="" ID="cmbEmpSearch" FieldLabel="Search Employee" EmptyText="Employee Name"
                                        LabelAlign="Top" runat="server" DisplayField="Name" ValueField="EmployeeId" StoreID="storeSearch"
                                        TypeAhead="false" Width="200" PageSize="9999" HideBaseTrigger="true" MinChars="1"
                                        TriggerAction="All" ForceSelection="false">
                                        <ListConfig LoadingText="Searching..." MinWidth="200" StyleSpec="border-top:1px solid #98c0f4;">
                                            <ItemTpl ID="ItemTpl2" runat="server">
                                                <Html>
                                                    <tpl>
                                                        <div class="search-item">
                                                                        <span>{Name}</span>  
                                                         </div>
					                            </tpl>
                                                </Html>
                                            </ItemTpl>
                                        </ListConfig>
                                        <Triggers>
                                            <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                                        </Triggers>
                                        <Listeners>
                                            <Select Handler="this.getTrigger(0).show();" />
                                            <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                            <TriggerClick Handler="if (index == 0) { 
                                                       this.clearValue(); 
                                                       this.getTrigger(0).hide();
                                                   }" />
                                        </Listeners>
                                    </ext:ComboBox>
                                </td>
                                <td>
                                    <ext:Button ID="btnLoad" runat="server" Text="Load" Width="100" Cls="btn btn-default"
                                        MarginSpec="25 10 10 10">
                                        <Listeners>
                                            <Click Fn="searchList">
                                            </Click>
                                        </Listeners>
                                    </ext:Button>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <ext:Checkbox runat="server" ID="chkIsLateOnly" BoxLabel="Late Only" />
                                </td>
                                <td>
                                    <ext:Checkbox runat="server" ID="chkIsAbsentOnly" BoxLabel="Absent Only" />
                                </td>
                                <td>
                                    <ext:Checkbox runat="server" ID="chkIsLeaveOnly" BoxLabel="Leave Only" />
                                </td>
                            </tr>
                        </table>
                    </Content>
                </ext:Container>
            </div>
            <ext:KeyMap ID="KeyMap1" runat="server" Target="={#{listingContainer}.getBody()}">
                <Binding>
                    <ext:KeyBinding Handler="#{btnLoad}.fireEvent('click');">
                        <Keys>
                            <ext:Key Code="ENTER" />
                        </Keys>
                    </ext:KeyBinding>
                </Binding>
            </ext:KeyMap>
            <ext:GridPanel StyleSpec="margin-top:15px;" ID="gridBranchTransfer" runat="server"
                Cls="itemgrid" OnReadData="Store_ReadData">
                <Store>
                    <ext:Store PageSize="50" ID="storeEmpList" runat="server" AutoLoad="true" OnReadData="Store_ReadData"
                        RemotePaging="true" RemoteSort="true">
                        <Proxy>
                            <ext:PageProxy />
                        </Proxy>
                        <Model>
                            <ext:Model ID="Model1" runat="server">
                                <Fields>
                                    <ext:ModelField Name="RowNumber" Type="String" />
                                    <ext:ModelField Name="EmployeeId" Type="String" />
                                    <ext:ModelField Name="IdCardNo" Type="String" />
                                    <ext:ModelField Name="Name" Type="String" />
                                    <ext:ModelField Name="Date" Type="Date" />
                                    <ext:ModelField Name="NepDate" Type="String" />
                                    <ext:ModelField Name="DayValueText" Type="String" />
                                      <ext:ModelField Name="LevelPosition" Type="String" />
                                    <ext:ModelField Name="Designation" Type="String" />
                                    <ext:ModelField Name="MissingOrExtraText" Type="String" />
                                    <ext:ModelField Name="ClockInText" Type="String" />
                                    <ext:ModelField Name="ClockOutText" Type="String" />
                                    <ext:ModelField Name="LateMinText" Type="String" />
                                     <ext:ModelField Name="LateMin" Type="String" />
                                    <ext:ModelField Name="StandardMinText" Type="String" />
                                    <ext:ModelField Name="WorkedMinText" Type="String" />
                                    <ext:ModelField Name="IsActualLate" Type="String" />
                                    <ext:ModelField Name="IsAbsent" Type="String" />
                                    <ext:ModelField Name="OTMinuteText" Type="String" />
                                    <ext:ModelField Name="InComments" Type="String" />
                                    <ext:ModelField Name="OutComments" Type="String" />
                                    <ext:ModelField Name="Branch" Type="String" />
                                    <ext:ModelField Name="Department" Type="String" />
                                    <ext:ModelField Name="LeaveName" Type="String" />
                                </Fields>
                            </ext:Model>
                        </Model>
                        <Sorters>
                            <ext:DataSorter Property="date" Direction="ASC" />
                        </Sorters>
                    </ext:Store>
                </Store>
                <ColumnModel ID="ColumnModel1" runat="server">
                    <Columns>
                        <ext:Column ID="colFromDate" runat="server" Text="SN" DataIndex="RowNumber" MenuDisabled="false"
                            Locked="true" Sortable="false" Align="Center" Width="50" />
                        <ext:Column ID="Column_EIN" runat="server" Text="EIN" DataIndex="EmployeeId" MenuDisabled="false"
                            Locked="true" Sortable="true" Align="Center" Width="50" />
                        <ext:Column ID="Column_ID" runat="server" Text="I No" DataIndex="IdCardNo" MenuDisabled="false"
                            Locked="true" Sortable="true" Align="Center" Width="50" />
                        <ext:Column ID="Column_Name" runat="server" Text="Name" DataIndex="Name" MenuDisabled="false"
                            Locked="true" Sortable="true" Align="Left" Width="150" />
                        <ext:Column ID="Column10" runat="server" Text="Branch" DataIndex="Branch" MenuDisabled="false"
                            Sortable="true" Align="Left" Width="100" />
                        <ext:Column ID="Column11" runat="server" Text="Department" DataIndex="Department"
                            MenuDisabled="false" Sortable="true" Align="Left" Width="100" />
                         <ext:Column ID="Column15" runat="server" Text="Designation" DataIndex="Designation"
                            MenuDisabled="false" Sortable="true" Align="Left" Width="100" />
                        <ext:DateColumn ID="DateColumn1" runat="server" Text="Date" DataIndex="Date" Format="dd-MMM-yyyy"
                            MenuDisabled="false" Sortable="true" Align="Left" Width="100" />
                        <ext:Column ID="Column4" runat="server" Text="Nep Date" DataIndex="NepDate" MenuDisabled="false"
                            Sortable="true" Align="Left" Width="100" />
                        <ext:Column ID="Column2" runat="server" Text="Day Type" DataIndex="DayValueText"
                            MenuDisabled="false" Sortable="true" Align="Left" Width="120" />
                        <ext:Column ID="Column6" runat="server" Text="Leave Name" DataIndex="LeaveName" MenuDisabled="false"
                            Sortable="true" Align="Left" Width="120" />
                        <ext:Column ID="colFromDateEng" runat="server" Text="Standard hr" DataIndex="StandardMinText"
                            MenuDisabled="false" Sortable="true" Align="Right" Width="80" />
                        <ext:Column ID="Column8" runat="server" Text="Worked hr" DataIndex="WorkedMinText"
                            MenuDisabled="false" Sortable="true" Align="Right" Width="80" />
                        <ext:Column ID="Column1" runat="server" Text="In" DataIndex="ClockInText" MenuDisabled="false"
                            Sortable="true" Align="Right" Width="60" />
                        <ext:Column ID="Column3" runat="server" Text="Out" DataIndex="ClockOutText" MenuDisabled="false"
                            Sortable="true" Align="Right" Width="60" />
                        <%-- <ext:Column ID="Column6" runat="server" Text="OT hr" DataIndex="OTMinuteText" MenuDisabled="false"
                            Sortable="true" Align="Right" Width="60">
                        </ext:Column>--%>
                        <ext:Column ID="income2" runat="server" Text="Missing/Extra" DataIndex="MissingOrExtraText"
                            MenuDisabled="false" Sortable="true" Align="Right" Width="100">
                        </ext:Column>
                        <ext:Column ID="Column12" runat="server" Hidden="true" Text="OT" DataIndex="OTMinuteText"
                            MenuDisabled="false" Sortable="true" Align="Right" Width="100">
                        </ext:Column>
                        <ext:Column ID="income3" runat="server" Text="Late" DataIndex="IsActualLate" MenuDisabled="false"
                            Sortable="true" Align="Right" Width="50">
                        </ext:Column>
                         <ext:Column ID="Column13" runat="server" Text="Late Min" DataIndex="LateMinText"
                            MenuDisabled="false" Sortable="true" Align="Right" Width="100">
                        </ext:Column>
                          <ext:Column ID="Column14" runat="server" Text="Late Min Only" DataIndex="LateMin"
                            MenuDisabled="false" Sortable="true" Align="Right" Width="100">
                        </ext:Column>
                        <ext:Column ID="Column5" runat="server" Text="Absent" DataIndex="IsAbsent" MenuDisabled="false"
                            Sortable="true" Align="Right" Width="60">
                        </ext:Column>
                        <ext:Column ID="Column7" runat="server" Text="In Comments" DataIndex="InComments"
                            MenuDisabled="false" Sortable="true" Align="Left" Width="120">
                        </ext:Column>
                        <ext:Column ID="Column9" runat="server" Text="Out Comments" DataIndex="OutComments"
                            MenuDisabled="false" Sortable="true" Align="Left" Width="120">
                        </ext:Column>
                    </Columns>
                </ColumnModel>
                <View>
                    <ext:GridView ID="GridView1" runat="server">
                        <GetRowClass Fn="getRowClass" />
                    </ext:GridView>
                </View>
                <BottomBar>
                    <ext:PagingToolbar ID="PagingToolbar1" runat="server" DisplayInfo="true">
                    </ext:PagingToolbar>
                </BottomBar>
            </ext:GridPanel>
        </div>
        <div class="alert alert-info" style="margin-top: 15px; height: 50px;">
            <div style="float: right;">
                <ext:Button runat="server" AutoPostBack="true" OnClick="btnExport_Click" ID="btnExport"
                    Text="<i></i>Export To Excel">
                </ext:Button>
            </div>
        </div>
    </div>
    <ext:Window ID="AEWindow" runat="server" Title="Generate Attendance" Icon="Application"
        Height="350" Width="400" BodyPadding="5" Hidden="true" Modal="false">
        <Content>
            <uc1:GenerateAttendanceRptCtl runat="server" Id="GenerateAttendanceRptCtl1" />
        </Content>
    </ext:Window>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
