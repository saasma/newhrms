﻿<%@ Page Language="C#" EnableViewState="false" EnableEventValidation="false"  MaintainScrollPositionOnPostback="true" MasterPageFile="~/Master/HROld.Master"
    AutoEventWireup="true" CodeBehind="AttendanceDailyReportOld.aspx.cs" Title="Attendance Daily Report"
    Inherits="Web.CP.AttendanceDailyReportOld" %>

<%@ Register Assembly="DevExpress.Web.v16.1, Version=16.1.8.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Assembly="Karpach.WebControls" Namespace="Karpach.WebControls" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/UpDownCtl.ascx" TagName="UpDownCtl" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="InfoMsgCtl" TagPrefix="uc2" %>
<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>
<%@ Register Assembly="DevExpress.Web.v16.1, Version=16.1.8.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v16.1, Version=16.1.8.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.Export" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .SelectedBtn
        {
            border: solid 3px #0000FF;
        }
        
        .selectedButton
        {
            border: 2px solid blue !important;
        }
    </style>
    <script type="text/javascript">

        isEnglish = false;
        function updateGrid(filtervariable) {
            var filterCondition = "[AtteState] = " + filtervariable;
            grid1.ApplyFilter(filterCondition);
        };

        
    </script>
    <script type="text/javascript">
        var CustomPager = {
            gotoBox_Init: function (s, e) {
                s.SetText(1 + grid1.GetPageIndex());
            },
            gotoBox_KeyPress: function (s, e) {
                if (e.htmlEvent.keyCode != 13)
                    return;
                ASPxClientUtils.PreventEventAndBubble(e.htmlEvent);
                CustomPager.applyGotoBoxValue(s);
            },
            gotoBox_ValueChanged: function (s, e) {
                CustomPager.applyGotoBoxValue(s);
            },
            applyGotoBoxValue: function (textBox) {
                if (grid1.InCallback())
                    return;
                var pageIndex = parseInt(textBox.GetText()) - 1;
                if (pageIndex < 0)
                    pageIndex = 0;
                grid1.GotoPage(pageIndex);
            },
            combo_SelectedIndexChanged: function (s, e) {
                grid1.PerformCallback(s.GetSelectedItem().value);
            }
        };

        function StateChanged(chk) {
            var engDate = '<%= engDate.ClientID %>';
            var date = '<%= date.ClientID %>';

            if (chk.checked) {
                enableElement(engDate + '_date');
                disableElement(date + '_date');
            }
            else {
                disableElement(engDate + '_date');
                enableElement(date + '_date');
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Daily Attendance Reports
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <asp:HiddenField runat="server" ID="Hidden_AtteStatus" />
        <div class="attribute" style="padding: 10px">
            <table class="tblexp">
                <tr>
                    <td>
                        <My:Calendar Id="date" runat="server" />
                    </td>
                    <td>
                        <asp:CheckBox ID="chkIsEnglish" runat="server" Text="English Date" />
                    </td>
                    <td>
                        <My:Calendar  Id="engDate" Enabled="false" runat="server" />
                    </td>
                    <td style='vertical-align: bottom; padding-bottom: 14px;'>
                        <dx:ASPxButton ID="ASPxButton1" runat="server" Text="Load" Style="margin-left: 7px;
                            width: 100px; margin-top: 12px;" OnClick="ASPxButton1_Click">
                        </dx:ASPxButton>
                    </td>
                    <td style='vertical-align: bottom; padding-left: 5px; padding-bottom: 14px; width: 200px'>
                        <%--  <pr:CalendarExtControl ID="cal" runat="server" />--%>
                        <dx:ASPxButton ID="btnXlsExport" runat="server" Text="Export to XLS" UseSubmitBehavior="False"
                            OnClick="btnXlsExport_Click" />
                    </td>
                </tr>
            </table>
        </div>
        <div class="attribute" style="padding: 10px">
            <table class="tblfilters">
                <tr>
                    <td>
                        <%--<ext:LinkButton ID="btnAllEmp"  runat="server"  Style="margin-left:125px; " OnClientClick="grid1.ClearFilter(); " Text="Total Employee">
                            </ext:LinkButton> --%>
                        <asp:LinkButton CssClass="SelectedBtn" runat="server" ID="btnTotalEmp" Style="margin-left: 50px;
                            padding: 7px;" Text="Total Employee" OnClick="btnFilter_Click"></asp:LinkButton>
                    </td>
                    <td>
                        <asp:Label ID="lblToatalEmp" Text="0" Style="margin-left: 10px;" runat="server">
                        </asp:Label>
                    </td>
                    <td>
                        <%-- updateGrid  onclick="btnFilter_Click" <ext:LinkButton ID="btnTimely"   Style="margin-left:125px;" OnClientClick="updateGrid('10');" runat="server" Text="Timely Check In"/>--%>
                        <asp:LinkButton ID="btnTImelyAttendees" CssClass="SelectedBtn" OnClick="btnFilter_Click"
                            Style="margin-left: 125px; padding: 7px;" runat="server" Text="Timely In/Out" />
                    </td>
                    <td>
                        <asp:Label ID="lblTimelyCheckIn" Text="0" Style="margin-left: 10px;" runat="server">
                        </asp:Label>
                    </td>
                    <td>
                        <%--<ext:LinkButton ID="btnLate"  Style="margin-left:125px;" runat="server" OnClientClick="updateGrid('11');" Text="Late Check In">
                            </ext:LinkButton> --%>
                        <asp:LinkButton ID="btnLateAttendees" CssClass="SelectedBtn" Style="margin-left: 125px;
                            padding: 7px;" runat="server" OnClick="btnFilter_Click" Text="Untimely In/Out" />
                    </td>
                    <td>
                        <asp:Label ID="lblLateCheckIn" Text="0" Style="margin-left: 10px;" runat="server">
                        </asp:Label>
                    </td>
                    <td>
                        <%-- <ext:LinkButton ID="btnLeave"    Style="margin-left:125px;" runat="server" OnClientClick="updateGrid('4');" Text="On Leave">
                            </ext:LinkButton> --%>
                        <asp:LinkButton ID="btnOnLeave" CssClass="SelectedBtn" Style="margin-left: 125px;
                            padding: 7px;" runat="server" OnClick="btnFilter_Click" Text="On Leave" />
                    </td>
                    <td>
                        <asp:Label ID="lblOnLeave" Text="0" Style="margin-left: 10px;" runat="server">
                        </asp:Label>
                    </td>
                    <td>
                        <%--<ext:LinkButton ID="btnAbsent"   Style="margin-left:125px;" runat="server" OnClientClick="updateGrid('9');" Text="Absents">
                            </ext:LinkButton> --%>
                        <asp:LinkButton ID="btnAbsentees" CssClass="SelectedBtn" Style="margin-left: 125px;
                            padding: 7px;" runat="server" OnClick="btnFilter_Click" Text="Absent" />
                    </td>
                    <td>
                        <asp:Label ID="lblAbsents" Text="0" Style="margin-left: 10px;" runat="server">
                        </asp:Label>
                    </td>
                </tr>
            </table>
        </div>
        <div class="contentArea">
            <div style="clear: both; margin-left: -10px" id="divGrid" runat="server">
                <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="false" Theme="Aqua"
                    OnHtmlRowPrepared="ASPxGridView1_HtmlRowPrepared" ClientInstanceName="grid1"
                    OnCustomColumnDisplayText="ASPxGridView1_CustomColumnDisplayText" OnHeaderFilterFillItems="ASPxGridView1_HeaderFilterFillItems"
                    OnCustomCallback="Grid_CustomCallback" OnCustomUnboundColumnData="ASPxGridView1_CustomUnboundColumnData">
                    <Columns>
                        <dx:GridViewDataTextColumn FieldName="SN" VisibleIndex="0" Width="40" UnboundType="String">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="EmplooyeeName" Width="150" VisibleIndex="0" />
                        <dx:GridViewDataTextColumn FieldName="Branch" Width="125" VisibleIndex="1">
                            <Settings HeaderFilterMode="CheckedList" />
                        </dx:GridViewDataTextColumn>
                         <dx:GridViewDataTextColumn FieldName="Department" Width="125" VisibleIndex="2">
                            <Settings HeaderFilterMode="CheckedList" />
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="InTime" Width="75" VisibleIndex="3">
                            <PropertiesTextEdit DisplayFormatString="hh : mm tt" />
                            <CellStyle HorizontalAlign="Center">
                            </CellStyle>
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="OutTime" Width="75" VisibleIndex="6">
                            <PropertiesTextEdit DisplayFormatString="hh : mm tt" />
                            <CellStyle HorizontalAlign="Center">
                            </CellStyle>
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="LunchIn" Width="75" VisibleIndex="4">
                            <PropertiesTextEdit DisplayFormatString="hh:mm tt" />
                            <CellStyle HorizontalAlign="Center">
                            </CellStyle>
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="LunchOut" Width="75" VisibleIndex="5">
                            <PropertiesTextEdit DisplayFormatString="hh : mm tt" />
                            <CellStyle HorizontalAlign="Center">
                            </CellStyle>
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="InRemarks" Width="150" VisibleIndex="6" Visible="false">
                            <CellStyle HorizontalAlign="Center">
                            </CellStyle>
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="InRemarksText" Width="150" 
                            VisibleIndex="7">
                            <CellStyle HorizontalAlign="Center">
                            </CellStyle>
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="OutRemarks" Width="150" VisibleIndex="8" Visible="false">
                            <CellStyle HorizontalAlign="Center">
                            </CellStyle>
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="OutRemarksText" Width="150" 
                            VisibleIndex="9">
                            <CellStyle HorizontalAlign="Center">
                            </CellStyle>
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="RefinedWorkHour" Width="75" VisibleIndex="10"
                            Caption="Work Hour">
                            <CellStyle HorizontalAlign="Center">
                            </CellStyle>
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="InNote" Width="200" VisibleIndex="11">
                            <CellStyle HorizontalAlign="Left">
                            </CellStyle>
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="OutNote" Width="200" VisibleIndex="12">
                            <CellStyle HorizontalAlign="Left">
                            </CellStyle>
                        </dx:GridViewDataTextColumn>

                        <dx:GridViewDataTextColumn FieldName="LeaveName" Width="100">
                            <CellStyle HorizontalAlign="Left">
                            </CellStyle>
                        </dx:GridViewDataTextColumn>

                         <dx:GridViewDataTextColumn FieldName="InIPAddress" Width="100" >
                            <CellStyle HorizontalAlign="Left">
                            </CellStyle>
                        </dx:GridViewDataTextColumn>

                         <dx:GridViewDataTextColumn FieldName="OutIPAddress" Width="100">
                            <CellStyle HorizontalAlign="Left">
                            </CellStyle>
                        </dx:GridViewDataTextColumn>

                        <dx:GridViewDataTextColumn FieldName="AtteState" Visible="false" />
                    </Columns>
                    <SettingsPopup>
                        <HeaderFilter Height="250px" />
                    </SettingsPopup>
                    <Templates>
                        <PagerBar>
                            <table class="OptionsTable" style="width: 100%">
                                <tr>
                                    <td style="padding-left: 8px;">
                                        <dx:ASPxButton runat="server" ID="FirstButton" Text="First" Enabled="<%# Container.Grid.PageIndex > 0 %>"
                                            AutoPostBack="false" UseSubmitBehavior="false">
                                            <ClientSideEvents Click="function() { grid1.GotoPage(0) }" />
                                        </dx:ASPxButton>
                                    </td>
                                    <td>
                                        <dx:ASPxButton runat="server" ID="PrevButton" Text="Prev" Enabled="<%# Container.Grid.PageIndex > 0 %>"
                                            AutoPostBack="false" UseSubmitBehavior="false">
                                            <ClientSideEvents Click="function() { grid1.PrevPage() }" />
                                        </dx:ASPxButton>
                                    </td>
                                    <td>
                                        <dx:ASPxTextBox runat="server" ID="GotoBox" Width="30">
                                            <ClientSideEvents Init="CustomPager.gotoBox_Init" ValueChanged="CustomPager.gotoBox_ValueChanged"
                                                KeyPress="CustomPager.gotoBox_KeyPress" />
                                        </dx:ASPxTextBox>
                                    </td>
                                    <td>
                                        <span style="white-space: nowrap">of
                                            <%# Container.Grid.PageCount %>
                                        </span>
                                    </td>
                                    <td>
                                        <dx:ASPxButton runat="server" ID="NextButton" Text="Next" Enabled="<%# Container.Grid.PageIndex < Container.Grid.PageCount - 1 %>"
                                            AutoPostBack="false" UseSubmitBehavior="false">
                                            <ClientSideEvents Click="function() { grid1.NextPage() }" />
                                        </dx:ASPxButton>
                                    </td>
                                    <td>
                                        <dx:ASPxButton runat="server" ID="LastButton" Text="Last" Enabled="<%# Container.Grid.PageIndex < Container.Grid.PageCount - 1 %>"
                                            AutoPostBack="false" UseSubmitBehavior="false">
                                            <ClientSideEvents Click="function() { grid1.GotoPage(grid1.GetPageCount() - 1); }" />
                                        </dx:ASPxButton>
                                    </td>
                                    <td style="width: 100%">
                                    </td>
                                    <td style="white-space: nowrap">
                                        <span style="white-space: nowrap">Records per page: </span>
                                    </td>
                                    <td>
                                        <dx:ASPxComboBox runat="server" ID="Combo" Width="70px" DropDownWidth="70px" ValueType="System.Int32"
                                            SelectedIndex="0" OnLoad="PagerCombo_Load">
                                            <Items>
                                                <dx:ListEditItem Value="100" Text="100" />
                                                <dx:ListEditItem Value="500" Text="500" />
                                                <dx:ListEditItem Value="99999" Text="All" />
                                            </Items>
                                            <ClientSideEvents SelectedIndexChanged="CustomPager.combo_SelectedIndexChanged" />
                                        </dx:ASPxComboBox>
                                    </td>
                                </tr>
                            </table>
                        </PagerBar>
                    </Templates>
                    <Settings EnableFilterControlPopupMenuScrolling="True" ShowFilterBar="Visible" ShowHeaderFilterButton="true"
                        ShowFilterRow="True" ShowFilterRowMenu="True" />
                </dx:ASPxGridView>
                <dx:ASPxGridViewExporter ID="ASPxGridViewExporter1" GridViewID="ASPxGridView1" runat="server">
                </dx:ASPxGridViewExporter>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="pageScriptsContent" runat="server">
    <style>
        .tableField
        {
            padding: 20px;
        }
        .tableField td
        {
            padding-bottom: 5px;
        }
    </style>
</asp:Content>
