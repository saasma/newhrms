﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


using BLL;
using DevExpress.XtraReports.UI;
using Web.CP.Report.Templates.HR;
using Web.ReportDataSetTableAdapters;
using Utils.Calendar;
using BLL.Base;
using DAL;
using BLL.Manager;
using System.Data;
using DevExpress.XtraPrinting;
using BLL.BO;
using Web.CP.Report;


namespace Web.Attendance
{
    public partial class HRAttMontlyDetailDevice : BasePage
    {
        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.LeaveAndAttendance;
            }
        }

        private int Name_Column_Index = 1;

        protected void LoadReportHandler()
        { }

        AttendanceManager attendanceManager = new AttendanceManager();

        protected void Page_Load(object sender, EventArgs e)
        {
            LoadReport();

            report.Filter.PayrollFrom = true;
            report.Filter.PayrollTo = false;
            report.Filter.Department = false;
        }

        private void Event_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            

            if (Web.CP.Report.ReportHelper.IsReportExportState())
            {
                ((XRLabel)sender).BackColor = System.Drawing.Color.Transparent;
                ((XRLabel)sender).BorderColor = System.Drawing.Color.Black;

               
            }

        }

        protected void ClearBorderControlEvent(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if ((XRLabel)sender != null)
            {
                XRLabel lbl = (XRLabel)sender;
                lbl.BackColor = System.Drawing.Color.Transparent;
                if (lbl != null && !string.IsNullOrEmpty(lbl.Text.Trim()))
                {
                    lbl.BackColor = ReportHelper.GetAttendanceCellColor(lbl.Text.Trim());
                }
            }
            
        }
        protected void ClearBorderLessControlEvent(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            ReportHelper.TableCell_BeforePrintEvent(sender, e);
        }

        private void CreateUsingLabels(XtraReport report, List<TextValue> headerList, int totalDays)
        {
            DataTable dataTable = (report.DataSource as DataSet).Tables[0];
            int firstSecondColumnExtraWidth = 120;

            Color colorHeader = Color.FromArgb(201, 214, 237);
            Color colorEven = Color.FromArgb(239, 243, 250);

            int colCount = dataTable.Columns.Count; ;
            int colWidth = 60;

            report.PageWidth = (colWidth * colCount) + 200 - 20 * totalDays;

            int currentXLocation = 0;
            int prevWidth = 0;
            int currentWidth = 0;           

            currentXLocation = 0;
            prevWidth = 0;
            currentWidth = 0;
            // Create header captions
            for (int i = 0; i < colCount; i++)
            {
                XRLabel label = new XRLabel();
                label.BeforePrint += new System.Drawing.Printing.PrintEventHandler(Event_BeforePrint);
                label.TextAlignment = TextAlignment.MiddleLeft;

                if (i == 0)
                {
                    currentWidth = firstSecondColumnExtraWidth;
                }
                else if (i == 1 || i == 4 || i == (6 + headerList.Count))
                {
                    currentWidth = colWidth;//for other amount columns
                    label.TextAlignment = TextAlignment.MiddleCenter;
                }
                else
                {
                    currentWidth = colWidth - 20;//for other amount columns
                    label.TextAlignment = TextAlignment.MiddleCenter;
                }
                
                currentXLocation += (prevWidth);
                prevWidth = currentWidth;

                label.Location = new Point(currentXLocation, 50);
                label.Text = dataTable.Columns[i].Caption;
                label.Padding = new PaddingInfo(2, 2, 2, 2);

                label.Font = new Font(label.Font.FontFamily, label.Font.Size, FontStyle.Bold);
                label.CanGrow = false;//dont allow to grow as design will be meshed up

                if (i == 0)
                {
                    label.Borders = DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom;
                    label.Size = new Size(currentWidth, 50);
                }
                else
                    label.Borders = DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom;

                label.Size = new Size(currentWidth, 50);

                ReportHelper.HeaderLabelStyle(label);
                // Place the headers onto a PageHeader band
                report.Bands[BandKind.PageHeader].Controls.Add(label);
            }


            currentXLocation = 0;
            prevWidth = 0;
            currentWidth = 0;
           
            for (int i = 0; i < colCount; i++)
            {
                XRLabel label = new XRLabel();
                label.BeforePrint += new System.Drawing.Printing.PrintEventHandler(Event_BeforePrint);

                if (i == 0)
                {
                    label.BackColor = Color.FromArgb(239, 243, 250);
                }

                else if (i > 0 && i <= headerList.Count + 7)
                {
                    label.BackColor = Color.FromName("blanchedalmond");
                }
                else
                {

                    label.BeforePrint += new System.Drawing.Printing.PrintEventHandler(ClearBorderControlEvent);
                }

               
                if (i == 0)
                {
                    currentWidth = firstSecondColumnExtraWidth;
                    label.TextAlignment = TextAlignment.MiddleLeft;
                }
                else if (i == 1 || i == 4 || i == (6 + headerList.Count))
                {
                    currentWidth = colWidth;
                    label.TextAlignment = TextAlignment.MiddleCenter;
                }
                else
                {
                    currentWidth = colWidth - 20;
                    label.TextAlignment = TextAlignment.MiddleCenter;
                }
                currentXLocation += (prevWidth);
                prevWidth = currentWidth;

                label.Location = new Point(currentXLocation, 0);
               
                label.Padding = new PaddingInfo(2, 2, 2, 0);
                if (i == 0)
                    label.DataBindings.Add("Text", null, dataTable.Columns[i].ColumnName);
                else
                    label.DataBindings.Add("Text", null, dataTable.Columns[i].ColumnName, "{0:n2}");




                label.CanGrow = false;

                if (i == 0)

                    label.Borders = DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom;

                else
                    label.Borders = DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom;

                label.Size = new Size(currentWidth, 20);
               
                label.Font = new Font(label.Font.FontFamily, label.Font.Size, FontStyle.Regular);

                report.Bands[BandKind.Detail].Controls.Add(label);
            }


        }



        protected void LoadReport()
        {
            ReportDataSetTableAdapters.AttendanceGetMainReportNewTableAdapter adap = new ReportDataSetTableAdapters.AttendanceGetMainReportNewTableAdapter();

            CP.Report.Templates.Pay.ReportAttEmpMonthlyDetails mainReport = new CP.Report.Templates.Pay.ReportAttEmpMonthlyDetails();

            PayrollPeriod payrollPeriod = CommonManager.GetPayrollPeriod(report.Filter.StartDate.Month,
                                                                     report.Filter.StartDate.Year);

            if (payrollPeriod == null)
                return;

            List<LLeaveType> listLLeaveType = NewHRManager.GetLeaveType();

            List<TextValue> headerList = new List<TextValue>();
            foreach (var item in listLLeaveType)
            {
                TextValue obj = new TextValue() { Text = item.Abbreviation, Value = item.Abbreviation };
                headerList.Add(obj);
            }

            int branchId = report.Filter.BranchId;

            List<AttendanceGetMainReportNewWithBranchFilterResult> listRes = NewHRManager.GetAttendanceMainReportList(report.Filter.EmployeeId, payrollPeriod.PayrollPeriodId, report.Filter.BranchId);
                                     

            DataTable dataTable = CreateDataTable(listRes, headerList, payrollPeriod.TotalDays.Value);
            DataSet dset = new DataSet();
            dset.Tables.Add(dataTable);
            mainReport.DataSource = dset;
            mainReport.DataMember = "Report";

            mainReport.labelTitle.Text = "Attendance Summary Report" + " of " + payrollPeriod.Name;           

            CreateUsingLabels(mainReport, headerList, payrollPeriod.TotalDays.Value);

            report.DisplayReport(mainReport);

            if (ReportHelper.IsReportExportState())
            {
                this.report.clearCache = true;
            }

        }

        private void GetLeaveType(string leaveTypeValue,ref int WH,ref int PH,ref int UPL,ref int ABS, ref int LT1, ref int LT2, ref int LT3, ref int LT4, ref int LT5, ref int LT6, ref int LT7, ref int LT8, ref int LT9, ref int LT10, ref int LT11, ref int LT12, List<TextValue> headerList)
        {
            if (leaveTypeValue == null || string.IsNullOrEmpty(leaveTypeValue.Trim()) || leaveTypeValue.Trim().ToLower().Contains("-a"))
                ABS++;
            else if (leaveTypeValue.Trim().ToLower() == "wh")
                WH++;
            else if (leaveTypeValue.Trim().ToLower() == "ph")
                PH++;
            else if (leaveTypeValue.Trim().ToLower() == "upl")
                UPL++;
            else
            {

                for (int i = 0; i < headerList.Count; i++)
                {
                    if (leaveTypeValue.Trim().ToLower() == headerList[i].Value.Trim().ToLower())
                    {
                        if (i == 0)
                            LT1++;
                        else if (i == 1)
                            LT2++;
                        else if (i == 2)
                            LT3++;
                        else if (i == 3)
                            LT4++;
                        else if (i == 4)
                            LT5++;
                        else if (i == 5)
                            LT6++;
                        else if (i == 6)
                            LT7++;
                        else if (i == 7)
                            LT8++;
                        else if (i == 8)
                            LT9++;
                        else if (i == 9)
                            LT10++;
                        else if (i == 10)
                            LT11++;
                        else if (i == 11)
                            LT12++;

                        break;
                    }
                }

            }
        }

        private DataTable CreateDataTable(List<AttendanceGetMainReportNewWithBranchFilterResult> data, List<TextValue> headerList, int totalDays)
        {
            DataTable dataTable = new DataTable();
            dataTable.TableName = "Report";
            
            dataTable.Columns.Add("Name", typeof(string));
            dataTable.Columns.Add("Total Days", typeof(float));
            dataTable.Columns.Add("WH", typeof(float));
            dataTable.Columns.Add("PH", typeof(float));
            dataTable.Columns.Add("Std Days", typeof(float));

            // Create table cells, fill the header cells with text, bind the cells to data
            for (int i = 0; i < headerList.Count; i++)
            {
                DataColumn column = new DataColumn(headerList[i].Value, typeof(float));
                column.Caption = headerList[i].Text;
                dataTable.Columns.Add(column);
            }

            dataTable.Columns.Add("UPL", typeof(float));
            dataTable.Columns.Add("ABS", typeof(float));
            dataTable.Columns.Add("Leave Days", typeof(float));

            for (int i = 1; i <= totalDays; i++)
            {
                DataColumn column = new DataColumn(i.ToString(), typeof(string));
                column.Caption = i.ToString();
                dataTable.Columns.Add(column);
            }

            PayrollPeriod payrollPeriod = CommonManager.GetPayrollPeriod(report.Filter.StartDate.Month,
                                                                     report.Filter.StartDate.Year);

           
            int day = 28;
            int totalDaysInMonth = 31;
            totalDaysInMonth = attendanceManager.GetTotalDaysInMonth(payrollPeriod.PayrollPeriodId);

            int headerCount = headerList.Count;

            foreach (var val1 in data)
            {
                List<object> list = new List<object>();
                list.Add(val1.Name);
                list.Add(double.Parse(totalDaysInMonth.ToString()));
                int AbsCount = 0, WH = 0, PH = 0, StdDays = 0, UPL = 0, LT1 = 0, LT2 = 0, LT3 = 0, LT4 = 0, LT5 = 0, LT6 = 0, LT7 = 0, LT8 = 0, LT9 = 0, LT10 = 0, LT11 = 0, LT12 = 0;

                GetLeaveType(val1.D1, ref WH, ref PH, ref UPL, ref AbsCount, ref LT1, ref LT2, ref LT3, ref LT4, ref LT5, ref LT6, ref LT7, ref LT8, ref LT9, ref LT10, ref LT11, ref LT12, headerList);
                GetLeaveType(val1.D2, ref WH, ref PH, ref UPL, ref AbsCount, ref LT1, ref LT2, ref LT3, ref LT4, ref LT5, ref LT6, ref LT7, ref LT8, ref LT9, ref LT10, ref LT11, ref LT12, headerList);
                GetLeaveType(val1.D3, ref WH, ref PH, ref UPL, ref AbsCount, ref LT1, ref LT2, ref LT3, ref LT4, ref LT5, ref LT6, ref LT7, ref LT8, ref LT9, ref LT10, ref LT11, ref LT12, headerList);
                GetLeaveType(val1.D4, ref WH, ref PH, ref UPL, ref AbsCount, ref LT1, ref LT2, ref LT3, ref LT4, ref LT5, ref LT6, ref LT7, ref LT8, ref LT9, ref LT10, ref LT11, ref LT12, headerList);
                GetLeaveType(val1.D5, ref WH, ref PH, ref UPL, ref AbsCount, ref LT1, ref LT2, ref LT3, ref LT4, ref LT5, ref LT6, ref LT7, ref LT8, ref LT9, ref LT10, ref LT11, ref LT12, headerList);
                GetLeaveType(val1.D6, ref WH, ref PH, ref UPL, ref AbsCount, ref LT1, ref LT2, ref LT3, ref LT4, ref LT5, ref LT6, ref LT7, ref LT8, ref LT9, ref LT10, ref LT11, ref LT12, headerList);
                GetLeaveType(val1.D7, ref WH, ref PH, ref UPL, ref AbsCount, ref LT1, ref LT2, ref LT3, ref LT4, ref LT5, ref LT6, ref LT7, ref LT8, ref LT9, ref LT10, ref LT11, ref LT12, headerList);
                GetLeaveType(val1.D8, ref WH, ref PH, ref UPL, ref AbsCount, ref LT1, ref LT2, ref LT3, ref LT4, ref LT5, ref LT6, ref LT7, ref LT8, ref LT9, ref LT10, ref LT11, ref LT12, headerList);
                GetLeaveType(val1.D9, ref WH, ref PH, ref UPL, ref AbsCount, ref LT1, ref LT2, ref LT3, ref LT4, ref LT5, ref LT6, ref LT7, ref LT8, ref LT9, ref LT10, ref LT11, ref LT12, headerList);
                GetLeaveType(val1.D10, ref WH, ref PH, ref UPL, ref AbsCount, ref LT1, ref LT2, ref LT3, ref LT4, ref LT5, ref LT6, ref LT7, ref LT8, ref LT9, ref LT10, ref LT11, ref LT12, headerList);
                GetLeaveType(val1.D11, ref WH, ref PH, ref UPL, ref AbsCount, ref LT1, ref LT2, ref LT3, ref LT4, ref LT5, ref LT6, ref LT7, ref LT8, ref LT9, ref LT10, ref LT11, ref LT12, headerList);
                GetLeaveType(val1.D12, ref WH, ref PH, ref UPL, ref AbsCount, ref LT1, ref LT2, ref LT3, ref LT4, ref LT5, ref LT6, ref LT7, ref LT8, ref LT9, ref LT10, ref LT11, ref LT12, headerList);
                GetLeaveType(val1.D13, ref WH, ref PH, ref UPL, ref AbsCount, ref LT1, ref LT2, ref LT3, ref LT4, ref LT5, ref LT6, ref LT7, ref LT8, ref LT9, ref LT10, ref LT11, ref LT12, headerList);
                GetLeaveType(val1.D14, ref WH, ref PH, ref UPL, ref AbsCount, ref LT1, ref LT2, ref LT3, ref LT4, ref LT5, ref LT6, ref LT7, ref LT8, ref LT9, ref LT10, ref LT11, ref LT12, headerList);
                GetLeaveType(val1.D15, ref WH, ref PH, ref UPL, ref AbsCount, ref LT1, ref LT2, ref LT3, ref LT4, ref LT5, ref LT6, ref LT7, ref LT8, ref LT9, ref LT10, ref LT11, ref LT12, headerList);
                GetLeaveType(val1.D16, ref WH, ref PH, ref UPL, ref AbsCount, ref LT1, ref LT2, ref LT3, ref LT4, ref LT5, ref LT6, ref LT7, ref LT8, ref LT9, ref LT10, ref LT11, ref LT12, headerList);
                GetLeaveType(val1.D17, ref WH, ref PH, ref UPL, ref AbsCount, ref LT1, ref LT2, ref LT3, ref LT4, ref LT5, ref LT6, ref LT7, ref LT8, ref LT9, ref LT10, ref LT11, ref LT12, headerList);
                GetLeaveType(val1.D18, ref WH, ref PH, ref UPL, ref AbsCount, ref LT1, ref LT2, ref LT3, ref LT4, ref LT5, ref LT6, ref LT7, ref LT8, ref LT9, ref LT10, ref LT11, ref LT12, headerList);
                GetLeaveType(val1.D19, ref WH, ref PH, ref UPL, ref AbsCount, ref LT1, ref LT2, ref LT3, ref LT4, ref LT5, ref LT6, ref LT7, ref LT8, ref LT9, ref LT10, ref LT11, ref LT12, headerList);
                GetLeaveType(val1.D20, ref WH, ref PH, ref UPL, ref AbsCount, ref LT1, ref LT2, ref LT3, ref LT4, ref LT5, ref LT6, ref LT7, ref LT8, ref LT9, ref LT10, ref LT11, ref LT12, headerList);
                GetLeaveType(val1.D21, ref WH, ref PH, ref UPL, ref AbsCount, ref LT1, ref LT2, ref LT3, ref LT4, ref LT5, ref LT6, ref LT7, ref LT8, ref LT9, ref LT10, ref LT11, ref LT12, headerList);
                GetLeaveType(val1.D22, ref WH, ref PH, ref UPL, ref AbsCount, ref LT1, ref LT2, ref LT3, ref LT4, ref LT5, ref LT6, ref LT7, ref LT8, ref LT9, ref LT10, ref LT11, ref LT12, headerList);
                GetLeaveType(val1.D23, ref WH, ref PH, ref UPL, ref AbsCount, ref LT1, ref LT2, ref LT3, ref LT4, ref LT5, ref LT6, ref LT7, ref LT8, ref LT9, ref LT10, ref LT11, ref LT12, headerList);
                GetLeaveType(val1.D24, ref WH, ref PH, ref UPL, ref AbsCount, ref LT1, ref LT2, ref LT3, ref LT4, ref LT5, ref LT6, ref LT7, ref LT8, ref LT9, ref LT10, ref LT11, ref LT12, headerList);
                GetLeaveType(val1.D25, ref WH, ref PH, ref UPL, ref AbsCount, ref LT1, ref LT2, ref LT3, ref LT4, ref LT5, ref LT6, ref LT7, ref LT8, ref LT9, ref LT10, ref LT11, ref LT12, headerList);
                GetLeaveType(val1.D26, ref WH, ref PH, ref UPL, ref AbsCount, ref LT1, ref LT2, ref LT3, ref LT4, ref LT5, ref LT6, ref LT7, ref LT8, ref LT9, ref LT10, ref LT11, ref LT12, headerList);
                GetLeaveType(val1.D27, ref WH, ref PH, ref UPL, ref AbsCount, ref LT1, ref LT2, ref LT3, ref LT4, ref LT5, ref LT6, ref LT7, ref LT8, ref LT9, ref LT10, ref LT11, ref LT12, headerList);


                if (totalDaysInMonth >= 28)
                    GetLeaveType(val1.D28, ref WH, ref PH, ref UPL, ref AbsCount, ref LT1, ref LT2, ref LT3, ref LT4, ref LT5, ref LT6, ref LT7, ref LT8, ref LT9, ref LT10, ref LT11, ref LT12, headerList);

                if (totalDaysInMonth >= 29)
                    GetLeaveType(val1.D29, ref WH, ref PH, ref UPL, ref AbsCount, ref LT1, ref LT2, ref LT3, ref LT4, ref LT5, ref LT6, ref LT7, ref LT8, ref LT9, ref LT10, ref LT11, ref LT12, headerList);

                if (totalDaysInMonth >= 30)
                    GetLeaveType(val1.D30, ref WH, ref PH, ref UPL, ref AbsCount, ref LT1, ref LT2, ref LT3, ref LT4, ref LT5, ref LT6, ref LT7, ref LT8, ref LT9, ref LT10, ref LT11, ref LT12, headerList);

                if (totalDaysInMonth >= 31)
                    GetLeaveType(val1.D31, ref WH, ref PH, ref UPL, ref AbsCount, ref LT1, ref LT2, ref LT3, ref LT4, ref LT5, ref LT6, ref LT7, ref LT8, ref LT9, ref LT10, ref LT11, ref LT12, headerList);

                if (totalDaysInMonth >= 32)
                    GetLeaveType(val1.D32, ref WH, ref PH, ref UPL, ref AbsCount, ref LT1, ref LT2, ref LT3, ref LT4, ref LT5, ref LT6, ref LT7, ref LT8, ref LT9, ref LT10, ref LT11, ref LT12, headerList);

                list.Add(double.Parse(WH.ToString()));
                list.Add(double.Parse(PH.ToString()));
                StdDays = totalDaysInMonth - (WH + PH);
                list.Add(double.Parse(StdDays.ToString()));

                int totalLeaveDays = 0;
                totalLeaveDays = UPL + AbsCount + LT1 + LT2 + LT3 + LT4 + LT5 + LT6 + LT7 + LT8 + LT9 + LT10 + LT11 + LT12;


                if (headerCount > 0)
                {                  
                    if (headerCount >= 1)
                        list.Add(double.Parse(LT1.ToString()));

                    if (headerCount >= 2)
                        list.Add(double.Parse(LT2.ToString()));

                    if (headerCount >= 3)
                        list.Add(double.Parse(LT2.ToString()));

                    if (headerCount >= 4)
                        list.Add(double.Parse(LT2.ToString()));

                    if (headerCount >= 5)
                        list.Add(double.Parse(LT2.ToString()));

                    if (headerCount >= 6)
                        list.Add(double.Parse(LT2.ToString()));

                    if (headerCount >= 7)
                        list.Add(double.Parse(LT2.ToString()));

                    if (headerCount >= 8)
                        list.Add(double.Parse(LT2.ToString()));

                    if (headerCount >= 9)
                        list.Add(double.Parse(LT2.ToString()));

                    if (headerCount >= 10)
                        list.Add(double.Parse(LT2.ToString()));
                }

                list.Add(double.Parse(UPL.ToString()));
                list.Add(double.Parse(AbsCount.ToString()));
                list.Add(double.Parse(totalLeaveDays.ToString()));
                
                list.Add(val1.D1 == null ? "" : val1.D1);
                list.Add(val1.D2 == null ? "" : val1.D2);
                list.Add(val1.D3 == null ? "" : val1.D3);
                list.Add(val1.D4 == null ? "" : val1.D4);
                list.Add(val1.D5 == null ? "" : val1.D5);
                list.Add(val1.D6 == null ? "" : val1.D6);
                list.Add(val1.D7 == null ? "" : val1.D7);
                list.Add(val1.D8 == null ? "" : val1.D8);
                list.Add(val1.D9 == null ? "" : val1.D9);
                list.Add(val1.D10 == null ? "" : val1.D10);
                list.Add(val1.D11 == null ? "" : val1.D11);
                list.Add(val1.D12 == null ? "" : val1.D12);
                list.Add(val1.D13 == null ? "" : val1.D13);
                list.Add(val1.D14 == null ? "" : val1.D14);
                list.Add(val1.D15 == null ? "" : val1.D15);
                list.Add(val1.D16 == null ? "" : val1.D16);
                list.Add(val1.D17 == null ? "" : val1.D17);
                list.Add(val1.D18 == null ? "" : val1.D18);
                list.Add(val1.D19 == null ? "" : val1.D19);
                list.Add(val1.D20 == null ? "" : val1.D20);
                list.Add(val1.D21 == null ? "" : val1.D21);
                list.Add(val1.D22 == null ? "" : val1.D22);
                list.Add(val1.D23 == null ? "" : val1.D23);
                list.Add(val1.D24 == null ? "" : val1.D24);
                list.Add(val1.D25 == null ? "" : val1.D25);
                list.Add(val1.D26 == null ? "" : val1.D26);
                list.Add(val1.D27 == null ? "" : val1.D27);

                if(totalDaysInMonth >= 28)
                    list.Add(val1.D28 == null ? "" : val1.D28);

                if (totalDaysInMonth >= 29)
                    list.Add(val1.D29 == null ? "" : val1.D29);

                if (totalDaysInMonth >= 30)
                    list.Add(val1.D30 == null ? "" : val1.D30);

                if (totalDaysInMonth >= 31)
                    list.Add(val1.D31 == null ? "" : val1.D31);

                if (totalDaysInMonth >= 32)
                    list.Add(val1.D32 == null ? "" : val1.D32);
           
                dataTable.Rows.Add(list.ToArray());
            }



            return dataTable;


        }
    }
}