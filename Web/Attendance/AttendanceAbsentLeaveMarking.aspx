﻿<%@ Page Title="Attendance Details" Language="C#" MasterPageFile="~/Master/NewDetails.Master"
    AutoEventWireup="true" CodeBehind="AttendanceAbsentLeaveMarking.aspx.cs" Inherits="Web.CP.AttendanceAbsentLeaveMarking" %>

<%@ Register Src="~/newhr/UserControls/GenerateAttendanceRptCtl.ascx" TagName="GenerateAttendanceRptCtl"
    TagPrefix="uc1" %>
<%@ MasterType VirtualPath="~/Master/NewDetails.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">

    function searchList() {
              <%=PagingToolbar1.ClientID %>.doRefresh();
        }

 

   
  Ext.onReady
  (
    function()
    {
        
    }
  );

  var lateRender = function(value)
  {
    if(value == 'true')
        return 'Yes';

        return '';
  }

   var refreshparentWindow = function()
  {
  
    <%=lnkRebindDate.ClientID %>.fireEvent('Click'); 
    <%=AEWindow.ClientID %>.hide();
  }


   var getRowClass = function (record) {
    
        var dayValue = record.data.DayValueText;
        

         if(dayValue=="Working Day")
         {
            return "";
         }
         else if(dayValue=="Holiday")
         {
            return "holiday";
         }
         else if(dayValue=="Leave" || dayValue=="Half Leave")
         {
            return "leave";
         }
        
         else //if(dayValue=="Weekly Holiday")
         {
            return "weeklyholiday";
         }

      };

        var leaveRenderer = function (value, metaData, record, rowIndex, colIndex, store) {
                    return record.data.LeaveName;

//                    var r = <%= StoreLeaveList.ClientID %>.getById(value.toString().toLowerCase());
//                    if (Ext.isEmpty(r)) {
//                        return "";
//                    }
//                    return r.data.Title;
                };

     // Fix for Group panel Expand/Collapse or row adding case : Uncaught TypeError: Cannot read property 'isCollapsedPlaceholder' of undefined

     Ext.view.Table.override({
        indexInStore: function(node) {
            node = (node && node.isCollapsedPlaceholder) ? this.getNode(node) : this.getNode(node, false);

            if (!node && node !== 0) {
                return -1;
            }

            var recordIndex = node.getAttribute('data-recordIndex');

            if (recordIndex) {
                return parseInt(recordIndex, 10);
            }

            return this.dataSource.indexOf(this.getRecord(node));
        }
    });
	
    var empSelection = function(p1,p2,p3,p4)
    {
        var ein = p2.data.EmployeeId;
        var storeLeaveBal = <%=storeLeaveBal.ClientID %>;

        storeLeaveBal.clearFilter(true);
        //storeLeaveBal.filter('EmployeeId', parseInt(ein));
        storeLeaveBal.filter({ property: 'EmployeeId', value: parseInt(ein), exactMatch: true });
    }

    var afterEdit = function (obj,e,e1,e2,e3) {
      
            /*
            Properties of 'e' include:
            e.grid - This grid
            e.record - The record being edited
            e.field - The field name being edited
            e.value - The value being set
            e.originalValue - The original value for the field, before the edit.
            e.row - The grid row index
            e.column - The grid column index
            */
//            e.record.get('Name')
                        
            var storeLeaves = e.grid.getStore();
            var storeLeaveBal = <%=storeLeaveBal.ClientID %>;

            var record;
            var i=0;
            // reset bal
            for(;i<storeLeaveBal.data.length;i++)
            {
                record = storeLeaveBal.data.items[i];
                record.data.CurrentBalance = record.data.NewBalance;
            }

            // set new balance
            i=0;
            j=0;
            for(;i<storeLeaves.data.length;i++)
            {
                record = storeLeaves.data.items[i];
                
                var leaveTypeId = record.data.LeaveTypeId;
                var ein = record.data.EmployeeId;

                j=0;
                for(;j<storeLeaveBal.data.length;j++)
                {
                    var leaverecord = storeLeaveBal.data.items[j];
                    if(leaverecord.data.LeaveTypeId == leaveTypeId && leaverecord.data.EmployeeId == ein)
                    {
                        leaverecord.data.CurrentBalance -= 1;
                         leaverecord.commit();
                    }
                }
            }
        };

        var UpdateRow = function () {                
                var plugin = this.editingPlugin;
                var gridProjects = <%=gridBranchTransfer.ClientID %>;
                var ein=gridProjects.getSelectionModel().getSelection()[0].data.EmployeeId;
                var date = gridProjects.getSelectionModel().getSelection()[0].data.Date;
                var leaveTypeId = this.getValues(false, false, false, true).LeaveTypeId;

                var strDate = date.getFullYear() + "/" + (date.getMonth()+1) + "/" + (date.getDate());
                var leaveName = <%= cmbLeaves.ClientID %>.getRawValue();

                var isFullDayLeave = true;

                if(leaveName.toString().indexOf("Half day") >= 1)
                    isFullDayLeave=false;


                if (this.getForm().isValid()) { // local validation      
                     Ext.getBody().mask("Loading...");           
                   Ext.net.DirectMethods.AssignLeave
                   (
                       ein,strDate,leaveTypeId,isFullDayLeave,plugin.context.record.phantom, 
                       {
                            success : function (result) 
                            {
                                
                                gridProjects.getSelectionModel().getSelection()[0].data.LeaveName = leaveName;

                                Ext.getBody().unmask();
                                if (!result.valid) {
                               
                                     Ext.Msg.show({
                                        title: 'Update failure!', //<- the dialog's title  
                                        msg: result.Message, //<- the message  
                                        buttons: Ext.Msg.OK, //<- YES and NO buttons  
                                        icon: Ext.Msg.WARNING, // <- error icon  
                                        minWidth: 300
                                    });
                                    return;
                                }
                                else{
                                    // reset balance
                                    var deduct = 1;
                                    if(isFullDayLeave==false)
                                        deduct = 0.5;

                                   
                                    var storeLeaveBal = <%= storeLeaveBal.ClientID %>;
                                    var j=0;                                    
                                    for(;j<storeLeaveBal.data.length;j++)
                                    {
                                        var leaverecord = storeLeaveBal.data.items[j];
                                        if(leaverecord.data.LeaveTypeId.toString() == leaveTypeId.toString() && leaverecord.data.EmployeeId.toString() == ein.toString())
                                        {
                                            leaverecord.data.NewBalance -= deduct;
                                            leaverecord.commit();
                                            break;
                                        }
                                    }
                                    
                                }


                                plugin.completeEdit();
                            },
                            failure: function (result) {   Ext.getBody().unmask();}
                        }
                    );
                }
            };

            
    </script>
    <style type="text/css">
        .holiday, .holiday a, .holiday td
        {
            color: #469146;
        }
        .leave, .leave a, .leave td
        {
            color: #D95CA9;
        }
        
        .weeklyholiday, .weeklyholiday a, .weeklyholiday td
        {
            color: #8F8F1A;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <script type="text/javascript" src="<%= ResolveUrl("~/Employee/override.js?v=") + Web.Helper.WebHelper.Version %>"></script>
    <ext:Hidden ID="hdnEmployeeId" runat="server" />
    <ext:LinkButton ID="lnkRebindDate" Hidden="true" Text="" runat="server">
        <DirectEvents>
            <Click OnEvent="RebindDate">
            </Click>
        </DirectEvents>
    </ext:LinkButton>
    <ext:Hidden ID="hdnBranchDepartmentId" runat="server" />
    <ext:Hidden ID="hdnMsg" runat="server" />
    <ext:Hidden ID="hdnEmpSearch" runat="server" />
    <ext:Hidden ID="hdnSortBy" runat="server" />
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Attendance Absent Leave Marking
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel" style="margin-top: 0px !important; padding-top: 0px !important;">
        <ext:Store ID="StoreLeaveList" runat="server">
            <Model>
                <ext:Model ID="Model2" runat="server">
                    <Fields>
                        <ext:ModelField Name="LeaveTypeId" Type="String" />
                        <ext:ModelField Name="Title" />
                    </Fields>
                </ext:Model>
            </Model>
        </ext:Store>
        <ext:Store ID="StoreAutoApplyLeaveList" runat="server">
            <Model>
                <ext:Model ID="Model3" runat="server">
                    <Fields>
                        <ext:ModelField Name="LeaveTypeId" Type="String" />
                        <ext:ModelField Name="Title" />
                    </Fields>
                </ext:Model>
            </Model>
        </ext:Store>
        <div class="innerLR">
            <table style='margin-top: 10px;'>
                <tr>
                    <td>
                        Last Proceed for &nbsp;<span id="dispLastDate" runat="server" style=""></span>
                    </td>
                    <td style="padding-left: 50px;">
                        <ext:LinkButton ID="btnGenerateAttendance" Text="Generate Attendance" runat="server">
                            <DirectEvents>
                                <Click OnEvent="btnGenerateAttendance_Click">
                                    <EventMask ShowMask="true" />
                                </Click>
                            </DirectEvents>
                        </ext:LinkButton>
                    </td>
                </tr>
            </table>
            <div style="clear: both">
            </div>
            <span runat='server' style="font-size: 14px; padding-top: 15px; margin-bottom: 10px"
                id="details"></span>
            <div class="alert alert-info" style="margin-top: 10px; padding-top: 0px!important;">
                <ext:Container ID="listingContainer" runat="server">
                    <Content>
                        <table>
                            <tr>
                                <td>
                                    <ext:DateField LabelWidth="120" MarginSpec="0 5 0 5" Width="100" FieldLabel="From"
                                        ID="calFromDate" runat="server" LabelAlign="Top" LabelSeparator="">
                                        <Plugins>
                                            <ext:GenericPlugin ID="HighlightDates1" runat="server" InstanceName="Ext.ux.DatePicker.HighlightWithNepDates" />
                                        </Plugins>
                                    </ext:DateField>
                                </td>
                                <td>
                                    <ext:DateField LabelWidth="120" MarginSpec="0 5 0 5" Width="100" FieldLabel="To"
                                        ID="calToDate" runat="server" LabelAlign="Top" LabelSeparator="">
                                        <Plugins>
                                            <ext:GenericPlugin ID="GenericPlugin1" runat="server" InstanceName="Ext.ux.DatePicker.HighlightWithNepDates" />
                                        </Plugins>
                                    </ext:DateField>
                                </td>
                                <td>
                                    <ext:ComboBox Width="140" MatchFieldWidth="false" MarginSpec="0 5 0 5" LabelSeparator=""
                                        LabelAlign="Top" QueryMode="Local" ID="cmbBranch" DisplayField="Name" ValueField="BranchId"
                                        runat="server" FieldLabel="Branch">
                                        <Items>
                                            <ext:ListItem Index="0" Value="-1" Text="All">
                                            </ext:ListItem>
                                        </Items>
                                        <Store>
                                            <ext:Store ID="Store5" runat="server">
                                                <Model>
                                                    <ext:Model ID="Model7" IDProperty="BranchId" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="BranchId" />
                                                            <ext:ModelField Name="Name" />
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                    </ext:ComboBox>
                                </td>
                                <td>
                                    <ext:ComboBox FieldLabel="Department" MatchFieldWidth="false" ID="cmbDepartment"
                                        Width="140" MarginSpec="0 5 0 5" runat="server" ValueField="DepartmentId" DisplayField="Name"
                                        LabelAlign="Top" LabelSeparator="" QueryMode="Local">
                                        <Items>
                                            <ext:ListItem Index="0" Value="-1" Text="All">
                                            </ext:ListItem>
                                        </Items>
                                        <Store>
                                            <ext:Store ID="Store6" runat="server">
                                                <Model>
                                                    <ext:Model ID="Model8" IDProperty="DepartmentId" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="DepartmentId" />
                                                            <ext:ModelField Name="Name" />
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                    </ext:ComboBox>
                                </td>
                                <td>
                                    <ext:ComboBox ID="cmbLevel" MatchFieldWidth="false" runat="server" FieldLabel="Level/Position"
                                        Width="140" MarginSpec="0 5 0 5" DisplayField="Name" ValueField="LevelId" QueryMode="Local"
                                        LabelSeparator="" LabelAlign="Top">
                                        <Items>
                                            <ext:ListItem Index="0" Value="-1" Text="All">
                                            </ext:ListItem>
                                        </Items>
                                        <Store>
                                            <ext:Store ID="storeLevel" runat="server">
                                                <Model>
                                                    <ext:Model ID="modelLevel" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="LevelId" Type="Int" />
                                                            <ext:ModelField Name="Name" Type="String" />
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                    </ext:ComboBox>
                                </td>
                                <td>
                                    <ext:ComboBox MatchFieldWidth="false" FieldLabel="Designation" ID="cmbDesignation"
                                        Width="140" MarginSpec="0 5 0 5" runat="server" ValueField="Text" DisplayField="Text"
                                        LabelAlign="Top" LabelSeparator="" QueryMode="Local">
                                        <Items>
                                            <ext:ListItem Index="0" Value="All" Text="All">
                                            </ext:ListItem>
                                        </Items>
                                        <Store>
                                            <ext:Store ID="Store7" runat="server">
                                                <Model>
                                                    <ext:Model ID="Model9" IDProperty="Text" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="Text" />
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                    </ext:ComboBox>
                                </td>
                                <td>
                                    <ext:Store runat="server" ID="storeSearch" AutoLoad="false">
                                        <Proxy>
                                            <ext:AjaxProxy Url="../Handler/EmpSearchID.ashx">
                                                <ActionMethods Read="GET" />
                                                <Reader>
                                                    <ext:JsonReader Root="plants" TotalProperty="total" />
                                                </Reader>
                                            </ext:AjaxProxy>
                                        </Proxy>
                                        <Model>
                                            <ext:Model ID="Model5" IDProperty="EmployeeId" Name="ItemLineModel" runat="server">
                                                <Fields>
                                                    <ext:ModelField Name="Name" Type="String" />
                                                    <ext:ModelField Name="EmployeeId" Type="String" />
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                    <ext:ComboBox LabelSeparator="" ID="cmbEmpSearch" FieldLabel="Search Employee" EmptyText="Employee Name"
                                        LabelAlign="Top" runat="server" DisplayField="Name" ValueField="EmployeeId" StoreID="storeSearch"
                                        TypeAhead="false" Width="200" PageSize="9999" HideBaseTrigger="true" MinChars="1"
                                        TriggerAction="All" ForceSelection="false">
                                        <ListConfig LoadingText="Searching..." MinWidth="200" StyleSpec="border-top:1px solid #98c0f4;">
                                            <ItemTpl ID="ItemTpl2" runat="server">
                                                <Html>
                                                    <tpl>
                                                        <div class="search-item">
                                                                        <span>{Name}</span>  
                                                         </div>
					                            </tpl>
                                                </Html>
                                            </ItemTpl>
                                        </ListConfig>
                                        <Triggers>
                                            <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                                        </Triggers>
                                        <Listeners>
                                            <Select Handler="this.getTrigger(0).show();" />
                                            <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                            <TriggerClick Handler="if (index == 0) { 
                                                       this.clearValue(); 
                                                       this.getTrigger(0).hide();
                                                   }" />
                                        </Listeners>
                                    </ext:ComboBox>
                                </td>
                                <td>
                                    <ext:Button ID="btnLoad" runat="server" Text="Load" Width="100" Cls="btn btn-default"
                                        MarginSpec="25 10 10 10">
                                        <Listeners>
                                            <Click Fn="searchList">
                                            </Click>
                                        </Listeners>
                                    </ext:Button>
                                </td>
                            </tr>
                            <%--    <tr>
                                <td colspan="8">
                                    <table>
                                        <tr>
                                            <td style="padding-top: 10px; padding-left: 5px;">
                                                <ext:Button ID="btnSaveLeave" runat="server" Width="120" Height="30" MarginSpec="0 0 0 0"
                                                    Text="Assign Leaves">
                                                    <DirectEvents>
                                                        <Click OnEvent="btnSaveLeave_DirectClick" Timeout="9999999">
                                                            <Confirmation ConfirmRequest="true" Message="Are you sure, you want to save the changes?" />
                                                            <ExtraParams>
                                                                <ext:Parameter Name="gridItems" Value="Ext.encode(#{gridBranchTransfer}.getRowsValues({ selectedOnly: false }))"
                                                                    Mode="Raw" />
                                                            </ExtraParams>

                                                            <EventMask ShowMask="true" />
                                                        </Click>
                                                    </DirectEvents>
                                                    <Listeners>
                                                        <Click Handler="">
                                                        </Click>
                                                    </Listeners>
                                                </ext:Button>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>--%>
                        </table>
                    </Content>
                </ext:Container>
            </div>
            <ext:KeyMap ID="KeyMap1" runat="server" Target="={#{listingContainer}.getBody()}">
                <Binding>
                    <ext:KeyBinding Handler="#{btnLoad}.fireEvent('click');">
                        <Keys>
                            <ext:Key Code="ENTER" />
                        </Keys>
                    </ext:KeyBinding>
                </Binding>
            </ext:KeyMap>
            <table style='float: left'>
                <tr>
                    <td valign="top">
                        <ext:GridPanel StyleSpec="margin-top:15px;" ID="gridBranchTransfer" runat="server"
                            Cls="itemgrid" OnReadData="Store_ReadData" Width='850px'>
                            <Store>
                                <ext:Store PageSize="50" ID="storeEmpList" runat="server" AutoLoad="false" OnReadData="Store_ReadData"
                                    RemotePaging="true" RemoteSort="true" GroupField="NameAndEIN">
                                    <Proxy>
                                        <ext:PageProxy />
                                    </Proxy>
                                    <Model>
                                        <ext:Model ID="Model1" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="RowNumber" Type="String" />
                                                <ext:ModelField Name="EmployeeId" Type="String" />
                                                <ext:ModelField Name="IdCardNo" Type="String" />
                                                <ext:ModelField Name="Name" Type="String" />
                                                <ext:ModelField Name="Date" Type="Date" />
                                                <ext:ModelField Name="NepDate" Type="String" />
                                                <ext:ModelField Name="DayValueText" Type="String" />
                                                <ext:ModelField Name="InComments" Type="String" />
                                                <ext:ModelField Name="OutComments" Type="String" />
                                                <ext:ModelField Name="Branch" Type="String" />
                                                <ext:ModelField Name="Department" Type="String" />
                                                <ext:ModelField Name="LeaveName" Type="String" />
                                                <ext:ModelField Name="LeaveTypeId" Type="String" />
                                                <ext:ModelField Name="NameAndEIN" Type="String" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                    <Sorters>
                                        <ext:DataSorter Property="date" Direction="ASC" />
                                    </Sorters>
                                </ext:Store>
                            </Store>
                            <Features>
                                <ext:Grouping StartCollapsed="false" IsDynamic="true" ID="Group1" runat="server"
                                    HideGroupedHeader="false" EnableGroupingMenu="true" />
                            </Features>
                            <ColumnModel ID="ColumnModel1" runat="server">
                                <Columns>
                                    <ext:Column ID="colFromDate" runat="server" Text="SN" DataIndex="RowNumber" MenuDisabled="false"
                                        Locked="true" Sortable="false" Align="Center" Width="50" />
                                    <ext:Column ID="Column_EIN" runat="server" Text="EIN" DataIndex="EmployeeId" MenuDisabled="false"
                                        Locked="true" Sortable="true" Align="Center" Width="50" />
                                    <ext:Column ID="Column_ID" runat="server" Text="I No" DataIndex="IdCardNo" MenuDisabled="false"
                                        Locked="true" Sortable="true" Align="Center" Width="50" />
                                    <ext:Column ID="Column_Name" runat="server" Text="Name" DataIndex="NameAndEIN" MenuDisabled="false"
                                        Locked="true" Sortable="true" Align="Left" Width="150" />
                                    <ext:Column ID="Column10" runat="server" Text="Branch" DataIndex="Branch" MenuDisabled="false"
                                        Sortable="true" Align="Left" Width="100" />
                                    <ext:DateColumn ID="DateColumn1" runat="server" Text="Date" DataIndex="Date" Format="dd-MMM-yyyy"
                                        MenuDisabled="false" Sortable="true" Align="Left" Width="100" />
                                    <ext:Column ID="Column4" runat="server" Text="Nep Date" DataIndex="NepDate" MenuDisabled="false"
                                        Sortable="true" Align="Left" Width="100" />
                                    <ext:Column ID="colTravelOrder" runat="server" Text="Assign Leave" Sortable="false"
                                        MenuDisabled="true" Width="190" DataIndex="LeaveTypeId" Border="false">
                                        <Renderer Fn="leaveRenderer" />
                                        <Editor>
                                            <ext:ComboBox DisplayField="Title" QueryMode="Local" ForceSelection="true" ValueField="LeaveTypeId"
                                                StoreID="StoreLeaveList" ID="cmbLeaves" runat="server">
                                                <Triggers>
                                                    <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                                                </Triggers>
                                                <Listeners>
                                                    <Select Handler="this.getTrigger(0).show();" />
                                                    <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                                    <TriggerClick Handler="if (index == 0) { 
                                           this.clearValue(); 
                                           this.getTrigger(0).hide();
                                       }" />
                                                </Listeners>
                                            </ext:ComboBox>
                                        </Editor>
                                    </ext:Column>
                                </Columns>
                            </ColumnModel>
                            <Listeners>
                                <Select Fn="empSelection" />
                            </Listeners>
                            <Plugins>
                                <ext:RowEditing ID="RowEditing1dd" runat="server" ClicksToEdit="1" ClicksToMoveEditor="1"
                                    AutoCancel="false" SaveHandler="UpdateRow">
                                    <%--  <Listeners>
                            <BeforeEdit Fn="beforeEdit" />
                        </Listeners>--%>
                                    <Listeners>
                                        <Edit Fn="afterEdit" />
                                    </Listeners>
                                </ext:RowEditing>
                            </Plugins>
                            <View>
                                <ext:GridView ID="GridView1" runat="server">
                                    <GetRowClass Fn="getRowClass" />
                                </ext:GridView>
                            </View>
                            <BottomBar>
                                <ext:PagingToolbar ID="PagingToolbar1" runat="server" DisplayInfo="true">
                                </ext:PagingToolbar>
                            </BottomBar>
                        </ext:GridPanel>
                    </td>
                    <td valign="top">
                    </td>
                </tr>
            </table>
            <div id='div' class='fixme' style='float:left;'>
                <ext:GridPanel StyleSpec="margin-top:15px;" ID="gridLeaveBalance" runat="server"
                    Cls="itemgrid" Width="250" OnReadData="Store_ReadData">
                    <Store>
                        <ext:Store ID="storeLeaveBal" runat="server" AutoLoad="true">
                            <Model>
                                <ext:Model ID="Model4" runat="server">
                                    <Fields>
                                        <ext:ModelField Name="EmployeeId" Type="int" />
                                        <ext:ModelField Name="LeaveTypeId" Type="int" />
                                        <ext:ModelField Name="Name" Type="String" />
                                        <ext:ModelField Name="Title" Type="String" />
                                        <ext:ModelField Name="NewBalance" Type="Float" />
                                        <ext:ModelField Name="CurrentBalance" Type="Float" />
                                    </Fields>
                                </ext:Model>
                            </Model>
                        </ext:Store>
                    </Store>
                    <ColumnModel ID="ColumnModel2" runat="server">
                        <Columns>
                            <ext:Column ID="Column1" Flex="1" runat="server" Text="Leave" DataIndex="Title" MenuDisabled="false"
                                Sortable="false" Align="Left" Width="120" />
                            <ext:Column ID="Column2" runat="server" Text="Balance" DataIndex="NewBalance" MenuDisabled="false"
                                Sortable="false" Align="Right" Width="100" />
                            <%-- <ext:Column ID="Column3" runat="server" Text="Rem Balance" DataIndex="CurrentBalance"
                                        MenuDisabled="false" Locked="true" Sortable="false" Align="Right" Width="100" />--%>
                        </Columns>
                    </ColumnModel>
                </ext:GridPanel>
            </div>
        </div>
        <div class="alert alert-info" style="margin-top: 15px; height: 50px;">
            <div style="float: right;">
                <ext:Button runat="server" AutoPostBack="true" OnClick="btnExport_Click" ID="btnExport"
                    Text="<i></i>Export To Excel">
                </ext:Button>
            </div>
        </div>
    </div>
    <ext:Window ID="AEWindow" runat="server" Title="Generate Attendance" Icon="Application"
        Height="350" Width="400" BodyPadding="5" Hidden="true" Modal="true">
        <Content>
            <uc1:GenerateAttendanceRptCtl runat="server" Id="GenerateAttendanceRptCtl1" />
        </Content>
    </ext:Window>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
    <script type="text/javascript">
        var fixmeTop = $('.fixme').offset().top;       // get initial position of the element

        $(window).scroll(function () {                  // assign scroll event listener

            var currentScroll = $(window).scrollTop(); // get current position

            if (currentScroll >= fixmeTop) {           // apply position: fixed if you
                $('.fixme').css({                      // scroll to that element or below it
                    position: 'fixed',
                    top: '0',
                    left: '930px'
                });
            } else {                                   // apply position: static
                $('.fixme').css({                      // if you scroll above it
                    position: 'static'
                });
            }

        });
    </script>
</asp:Content>
