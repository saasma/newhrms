<%@ Page Title="HR Attendance Report" Language="C#" MaintainScrollPositionOnPostback="true" MasterPageFile="~/Master/HROld.Master" AutoEventWireup="true" CodeBehind="HRAttendanceEmpMontlySummary.aspx.cs" Inherits="Web.CP.Report.HRAttendanceEmpMontlySummary" %>

<%@ Register Assembly="DevExpress.XtraReports.v16.1.Web, Version=16.1.8.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraReports.Web" TagPrefix="dxxr" %>
<%@ Register Src="~/Controls/Report/ReportContainer.ascx" TagName="ReportContainer" TagPrefix="uc1" %>

<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="InfoMsgCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/ErrorMsgCtl.ascx" TagName="ErrorMsgCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">

    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>Monthly Attendance Summary
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">

        <uc2:InfoMsgCtl runat="server" Hide="true" EnableViewState="false" ID="msgInfo" />
            <uc2:WarningCtl ID="divWarningMsg" EnableViewState="false" Hide="true" runat="server" />
            <uc2:ErrorMsgCtl ID="divErrorMsg" Hide="true" EnableViewState="false" runat="server" />


        <uc1:ReportContainer runat="server" OnReloadReport="LoadReport" ID="report" />
    </div>

</asp:Content>
