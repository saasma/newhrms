﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using Utils.Helper;
using System.IO;
using Utils;
using Utils.Security;
using Utils.Calendar;

namespace Web.CP
{
    public partial class AttendanceSummary : BasePage
    {
        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.HumanResource;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
               Initialize();

                              

            }
        }
        public void DispDate()
        {
            AttendanceEmployeeDate _AttendanceEmployeeDate = AttendanceManager.GetAttendanceGeneratedLastDate();
            if (_AttendanceEmployeeDate != null)
            dispLastDate.InnerHtml = String.Format("{0: MMMM d, yyyy}", _AttendanceEmployeeDate.Date.Value) + " - " + String.Format("{0:MMMM d, yyyy}", _AttendanceEmployeeDate.LastDate);
        }

        protected void RebindDate(object sender, DirectEventArgs e)
        {
            string strlastDateCtl = dispLastDate.ClientID;
            AttendanceEmployeeDate _AttendanceEmployeeDate = AttendanceManager.GetAttendanceGeneratedLastDate();
            if (_AttendanceEmployeeDate != null)
            {
                string dateRange = String.Format("{0: MMMM d, yyyy}", _AttendanceEmployeeDate.Date.Value) + "-" + String.Format("{0:MMMM d, yyyy}", _AttendanceEmployeeDate.LastDate);
                X.AddScript(strlastDateCtl + ".innerHTML ='" + dateRange + "'");
            }

        }
        protected void btnGenerateAttendance_Click(object sender, DirectEventArgs e)
        {
            GenerateAttendanceRptCtl1.Clear();
            AEWindow.Show();
        }

        
        protected void chkIsGroup_Change(object sender, DirectEventArgs e)
        {
            if(chkIsGroup.Checked)
            {
                chkGroupBranchWise.Show();
                chkGroupDepWise.Show();
                chkPositionWise.Show();
                chkDesignation.Show();

                Column_EIN.Hide();
                Column_ID.Hide();
                Column_Name.Hide();

                Column_Branch.Show();
                Column_Dep.Show();
                Column_LevelPos.Show();
                Column_Designation.Show();
            }
            else
            {
                chkGroupBranchWise.Hide();
                chkGroupDepWise.Hide();
                chkPositionWise.Hide();
                chkDesignation.Hide();

                Column_EIN.Show();
                Column_ID.Show();
                Column_Name.Show();

                Column_Branch.Hide();
                Column_Dep.Hide();
                Column_LevelPos.Hide();
                Column_Designation.Hide();
            }
        }

        private void Initialize()
        {
            List<Branch> branchList = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId);
            cmbBranch.Store[0].DataSource = branchList;
            cmbBranch.Store[0].DataBind();

            cmbDepartment.Store[0].DataSource = DepartmentManager.GetAllDepartments().OrderBy(x => x.Name).ToList();
            cmbDepartment.Store[0].DataBind();

            cmbDesignation.Store[0].DataSource = CommonManager.GetUniqueDesignationList();
            cmbDesignation.Store[0].DataBind();

            this.storeLevel.DataSource = ActivityManager.GetLevels().OrderBy(x => x.Name);
            this.storeLevel.DataBind();
            DispDate();

            if (CommonManager.Setting.EnableWeeklyLikePay != null && CommonManager.Setting.EnableWeeklyLikePay.Value)
            {
               

                List<PayGroup> payList = PeriodicPayManager.GetPayGroup(); ;
                payList.Insert(0, new PayGroup { PayGroupID = 0, Name = "Regular Pay" });

                payList.Insert(0, new PayGroup { PayGroupID = -1, Name = "All" });
                cmbPayGroup.Store[0].DataSource = payList;
                cmbPayGroup.Store[0].DataBind();
                cmbPayGroup.Show();

                if (!string.IsNullOrEmpty(Request.QueryString["ShowWeeklyPay"]))
                {
                    ExtControlHelper.ComboBoxSetSelected(Request.QueryString["ShowWeeklyPay"], cmbPayGroup);
                    rowFilter.Style["display"] = "";

                    calFromDate.Text = Request.QueryString["start"];
                    calToDate.Text = Request.QueryString["end"];

                    PagingToolbar1.DoRefresh();
                }
            }
        }

        private void Clear()
        {
            
        }


        protected void Store_ReadData(object sender, StoreReadDataEventArgs e)
        {
            int totalRecords = 0;
            int branchId = -1, depId = -1, levelId = -1;
            string designationName = "";

            if (cmbBranch.SelectedItem != null && cmbBranch.SelectedItem.Value != null)
                branchId = int.Parse(cmbBranch.SelectedItem.Value);
            if (cmbDepartment.SelectedItem != null && cmbDepartment.SelectedItem.Value != null)
                depId = int.Parse(cmbDepartment.SelectedItem.Value);
            if (cmbLevel.SelectedItem != null && cmbLevel.SelectedItem.Value != null)
                levelId = int.Parse(cmbLevel.SelectedItem.Value);
            if (cmbDesignation.SelectedItem != null && cmbDesignation.SelectedItem.Value != null)
                designationName = cmbDesignation.SelectedItem.Value;

            if (designationName.ToLower() == "all")
                designationName = "";

            int employeeId = -1;

            int.TryParse(cmbEmpSearch.SelectedItem.Value, out employeeId);

            if (employeeId == 0)
                employeeId = -1;

            hdnSortBy.Text = (e.Sort[0].Property + " " + e.Sort[0].Direction).ToLower();

            int payGroupID = -1;
            if (cmbPayGroup.SelectedItem != null && cmbPayGroup.SelectedItem.Value != null)
                payGroupID = int.Parse(cmbPayGroup.SelectedItem.Value);
            

            DateTime startdate = calFromDate.SelectedDate.Date;
            DateTime todate = calToDate.SelectedDate.Date;

            List<GetAttendanceEmployeeReportSummaryResult> list = AttendanceManager.GetAttendanceSummary
                (branchId, depId, levelId, designationName, employeeId, e.Page - 1, startdate, todate, e.Limit, (e.Sort[0].Property + " " + e.Sort[0].Direction).ToLower(), ref totalRecords,
                chkIsGroup.Checked, chkGroupBranchWise.Checked, chkGroupDepWise.Checked, chkPositionWise.Checked, 
                chkDesignation.Checked, false,payGroupID);

            e.Total = totalRecords;

            //if (list.Any())
            storeEmpList.DataSource = list;
            storeEmpList.DataBind();

        }


        public void btnExport_Click(object sender, EventArgs e)
        {
            int totalRecords = 0;
            int branchId = -1, depId = -1, levelId = -1;
            string designationName="";

            if (cmbBranch.SelectedItem != null && cmbBranch.SelectedItem.Value != null)
                branchId = int.Parse(cmbBranch.SelectedItem.Value);
            if (cmbDepartment.SelectedItem != null && cmbDepartment.SelectedItem.Value != null)
                depId = int.Parse(cmbDepartment.SelectedItem.Value);
            if (cmbLevel.SelectedItem != null && cmbLevel.SelectedItem.Value != null)
                levelId = int.Parse(cmbLevel.SelectedItem.Value);
            if (cmbDesignation.SelectedItem != null && cmbDesignation.SelectedItem.Value != null)
                designationName = cmbDesignation.SelectedItem.Value;

            if (designationName.ToLower() == "all")
                designationName = "";

            int employeeId = -1;

            int.TryParse(cmbEmpSearch.SelectedItem.Value, out employeeId);

            if (employeeId == 0)
                employeeId = -1;

            int payGroupID = -1;
            if (cmbPayGroup.SelectedItem != null && cmbPayGroup.SelectedItem.Value != null)
                payGroupID = int.Parse(cmbPayGroup.SelectedItem.Value);
            

            DateTime startdate = calFromDate.SelectedDate.Date;
            DateTime todate = calToDate.SelectedDate.Date;

            List<GetAttendanceEmployeeReportSummaryResult> list = AttendanceManager.GetAttendanceSummary
                (branchId, depId, levelId, designationName, employeeId, 0, startdate, todate, 9999999, (hdnSortBy.Text).ToLower(), ref totalRecords,
                chkIsGroup.Checked, chkGroupBranchWise.Checked, chkGroupDepWise.Checked, chkPositionWise.Checked, 
                chkDesignation.Checked, false,payGroupID);
            List<string> hiddenList = new List<string>();

            if (!chkIsGroup.Checked)
            {
                hiddenList = new List<string>{"OTMinute", "Total","TotalDays","QueryString","BranchID","DepartmentID","LevelPositionID","DesignationID",
                    "StandardMin","WorkedMin","TotalRows","MissingOrExtra","AvgMin","Branch","Department","LevelPosition","Designation"};
            }
            else
            {
                hiddenList = new List<string>{ "OTMinute","Total","TotalDays","QueryString","BranchID","DepartmentID","LevelPositionID","DesignationID",
                    "StandardMin","WorkedMin","TotalRows","MissingOrExtra","AvgMin","EmployeeId","Name","IdCardNo"};
            }

            Bll.ExcelHelper.ExportToExcel("Attendance Summary", list,
                hiddenList,
            new List<String>() { },
            new Dictionary<string, string>() {  {"RowNumber","SN"},{"EmployeeId","EIN"},{"OTMinuteText","OT hrs"},
                {"IdCardNo","I No"},{"StandardMinText","Standard hrs"},{"WorkedMinText","Worked hrs"},{"AvgMinText","Avg hrs"}
                ,{"MissingOrExtraText","Missing/Extras"},{"LateInCount","Late"}, {"LateMinText","Late Min" }, { "LateMin","LateMinOnly"}
                ,{"AbsentDays","Absent"},{"LeaveDays","Leaves"},{"WorkPercent","Work %"} },
            new List<string>() { "RowNumber","LateInCount","Workdays", "AbsentDays", "LeaveDays","WorkPercent","LateMin" }
            , new Dictionary<string, string>() {
              {"Attendance summary report form ",calFromDate.SelectedDate.ToShortDateString() + " to " + calToDate.SelectedDate.ToShortDateString()}
            }
            , new List<string> { "RowNumber","EmployeeId","IdCardNo","Name","Branch","Department","LevelPosition","Designation"
                ,"StandardMinText","WorkedMinText","AvgMinText","MissingOrExtraText","LateInCount","LateMinText","LateMin",
                "WorkDays","OTMinuteText","AbsentDays","LeaveDays","WorkPercent"});


        }


    }
}