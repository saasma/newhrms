﻿<%@ Control Language="C#" AutoEventWireup="true" EnableViewState="false" CodeBehind="EmployeeAtteCtl.ascx.cs"
    Inherits="Web.Attendance.UserControl.EmployeeAtteCtl" %>
<script type="text/javascript">

    function searchList() {
         
              <%=PagingToolbar1.ClientID %>.doRefresh();
        }


  var lateRender = function(value)
  {
    if(value == 'true')
        return 'Yes';
        return '';
  }



   var getRowClass = function (record) {
    
        var dayValue = record.data.DayType;
        

         if(dayValue=="Working Day")
         {
            return "";
         }
         else if(dayValue=="Holiday")
         {
            return "holiday";
         }
         else if(dayValue=="Leave" || dayValue=="Half Leave")
         {
            return "leave";
         }
        
         else //if(dayValue=="Weekly Holiday")
         {
            return "weeklyholiday";
         }

      };

       var selEmpId = null;
    function ACE_item_selected(source, eventArgs) {
        var value = eventArgs.get_value();
        selEmpId = value;
        document.getElementById('<%= hiddenEmployeeID.ClientID %>').value = selEmpId;
    }

</script>
<style type="text/css">
    .holiday, .holiday a, .holiday td
    {
        color: #469146;
        background-color: #F0FFF0 !important;
    }
    .leave, .leave a, .leave td
    {
        color: #D95CA9;
        background-color: #FFF0F5 !important;
    }
    
    .weeklyholiday, .weeklyholiday a, .weeklyholiday td
    {
        color: #8F8F1A;
        background-color: #FAFAD2 !important;
    }
</style>
<script type="text/javascript" src="<%= ResolveUrl("~/Employee/override.js?v=") + Web.Helper.WebHelper.Version %>"></script>

    
<div class="contentpanel" style="margin-top: 0px !important; padding: 0px !important;">
    <div class="innerLR">
        <div class="alert alert-info" style="margin-top: 10px; padding-top: 0px!important;">
            <table style="position:relative">
                <tr>
                    <td style="height:60px">
                        <ext:DateField LabelWidth="180" MarginSpec="0 5 0 5" Width="100" FieldLabel="From"
                            ID="calFromDate" runat="server" LabelAlign="Top" LabelSeparator="">
                            <Plugins>
                                <ext:GenericPlugin ID="HighlightDates1" runat="server" InstanceName="Ext.ux.DatePicker.HighlightWithNepDates" />
                            </Plugins>
                        </ext:DateField>
                    </td>
                    <td style="padding-left:10px;"> 
                        <ext:DateField LabelWidth="180" MarginSpec="0 5 0 5" Width="100" FieldLabel="To"
                            ID="calToDate" runat="server" LabelAlign="Top" LabelSeparator="">
                            <Plugins>
                                <ext:GenericPlugin ID="GenericPlugin1" runat="server" InstanceName="Ext.ux.DatePicker.HighlightWithNepDates" />
                            </Plugins>
                        </ext:DateField>
                    </td>
                    <td style="padding-top: 3px;padding-left:10px;" runat="server" id="tdEmpSearch">
                        <span style="color: #000000">Employee </span>
                        <ext:Hidden runat="server" ID="hiddenEmployeeID" />
                        <asp:TextBox Style='margin-top: 5px;' Width="180px" ID="txtEmpSearch" runat="server" />
                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtEmpSearch" 
                            WatermarkText="Search Text" WatermarkCssClass="searchBoxText" />
                        <cc1:AutoCompleteExtender EnableCaching="true" ID="AutoCompleteExtenderOrganization"
                            runat="server" MinimumPrefixLength="2" ServiceMethod="GetEmployeeNamesAndEINWithRetiredAlso"
                            ServicePath="~/PayrollService.asmx" TargetControlID="txtEmpSearch" OnClientItemSelected="ACE_item_selected"
                            CompletionSetCount="10" CompletionInterval="250" CompletionListCssClass="AutoExtender"
                            CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight">
                        </cc1:AutoCompleteExtender>
                    </td>
                    <td style="padding-left:10px;"> 
                        <ext:Button ID="btnLoad" runat="server" Text="Load" Width="100" 
                            MarginSpec="25 10 10 10">
                            <Listeners>
                                <Click Fn="searchList">
                                </Click>
                            </Listeners>
                        </ext:Button>
                    </td>
                </tr>
                <%--  <tr>
                    <td>
                        <ext:Checkbox runat="server" ID="chkIsLateOnly" BoxLabel="Late Only" />
                    </td>
                    <td>
                        <ext:Checkbox runat="server" ID="chkIsAbsentOnly" BoxLabel="Absent Only" />
                    </td>
                </tr>--%>
            </table>
        </div>
        <ext:GridPanel StyleSpec="margin-top:15px;" EnableViewState="false" ID="gridBranchTransfer"
            runat="server" OnReadData="Store_ReadData">
            <Store>
                <ext:Store PageSize="50" ID="storeEmpList" RemoteSort="false" RemotePaging="true"
                    runat="server" AutoLoad="false" OnReadData="Store_ReadData">
                    <Proxy>
                        <ext:PageProxy />
                    </Proxy>
                    <Model>
                        <ext:Model ID="Model1" runat="server">
                            <Fields>
                                <ext:ModelField Name="RowNumber" Type="String" />
                                <%--<ext:ModelField Name="EmployeeId" Type="String" />
                                <ext:ModelField Name="IdCardNo" Type="String" />--%>
                                <ext:ModelField Name="Name" Type="String" />
                                <ext:ModelField Name="DateEng" Type="Date" />
                                <ext:ModelField Name="ActualDate" Type="String" />
                                <ext:ModelField Name="DayType" Type="String" />
                                <ext:ModelField Name="RefinedInRemarks" Type="String" />
                                <ext:ModelField Name="RefinedOutRemarks" Type="String" />
                                <ext:ModelField Name="RefinedWorkHour" Type="String" />
                                <%--<ext:ModelField Name="IsActualLate" Type="String" />
                                <ext:ModelField Name="IsAbsent" Type="String" />--%>
                                <ext:ModelField Name="LeaveName" Type="String" />
                                <ext:ModelField Name="ShiftName" Type="String" />
                                <ext:ModelField Name="EmployeeComment" Type="String" />
                                <ext:ModelField Name="Branch" Type="String" />
                                <ext:ModelField Name="Department" Type="String" />
                            </Fields>
                        </ext:Model>
                    </Model>
                </ext:Store>
            </Store>
            <ColumnModel ID="ColumnModel1" runat="server">
                <Columns>
                    <ext:Column ID="colFromDate" runat="server" Text="SN" DataIndex="RowNumber" MenuDisabled="false"
                        Locked="true" Sortable="false" Align="Center" Width="40" />
                    <ext:Column ID="Column_Name" runat="server" Text="Name" DataIndex="Name" MenuDisabled="false"
                        Locked="true" Sortable="false" Align="Left" Width="150" />
                <%--<ext:Column ID="Column6" runat="server" Text="Branch" DataIndex="Branch" MenuDisabled="false"
                    Locked="true" Sortable="false" Align="Left" Width="150" />
                <ext:Column ID="Column9" runat="server" Text="Department" DataIndex="Department" MenuDisabled="false"
                    Locked="true" Sortable="false" Align="Left" Width="150" />--%>
                    <ext:DateColumn ID="DateColumn1" runat="server" Text="Date" DataIndex="DateEng" Format="dd-MMM-yyyy"
                        MenuDisabled="false" Sortable="false" Align="Left" Width="100" />
                    <ext:Column ID="Column4" runat="server" Text="Nep Date" DataIndex="ActualDate" MenuDisabled="false"
                        Sortable="false" Align="Left" Width="100" />
                    <ext:Column ID="Column2" runat="server" Text="Day Type" DataIndex="DayType" MenuDisabled="false"
                        Sortable="false" Align="Left" Width="120" />
                    <ext:Column ID="Column1" runat="server" Text="In Time" DataIndex="RefinedInRemarks"
                        MenuDisabled="false" Sortable="false" Align="Right" Width="70" />
                    <ext:Column ID="Column3" runat="server" Text="Out Time" DataIndex="RefinedOutRemarks"
                        MenuDisabled="false" Sortable="false" Align="Right" Width="70" />
                    <ext:Column ID="Column8" runat="server" Text="Worked hr" DataIndex="RefinedWorkHour"
                        MenuDisabled="false" Sortable="false" Align="Right" Width="70" />
                    <ext:Column ID="income3" runat="server" Text="Leave" DataIndex="LeaveName" MenuDisabled="false"
                        Sortable="false" Align="Left" Width="120">
                    </ext:Column>
                     <ext:Column ID="Column5" runat="server" Text="Shift" DataIndex="ShiftName" MenuDisabled="false"
                        Sortable="false" Align="Left" Width="120">
                    </ext:Column>
                    <ext:Column ID="Column7" runat="server" Text="Comments" DataIndex="EmployeeComment"
                        MenuDisabled="false" Sortable="false" Align="Left" Width="300">
                    </ext:Column>
                </Columns>
            </ColumnModel>
            <View>
                <ext:GridView ID="GridView1" runat="server">
                    <Listeners>
                    </Listeners>
                    <GetRowClass Fn="getRowClass" />
                </ext:GridView>
            </View>
            <BottomBar>
                <ext:PagingToolbar ID="PagingToolbar1" runat="server" DisplayInfo="true">
                </ext:PagingToolbar>
            </BottomBar>
        </ext:GridPanel>
        <div class="alert alert-info" style="margin-top: 15px; height: 50px;">
            <div style="float: right;">
                <ext:Button runat="server" AutoPostBack="true" OnClick="btnExport_Click" ID="btnExport"
                    Text="<i></i>Export To Excel">
                </ext:Button>
            </div>
        </div>
    </div>
</div>
