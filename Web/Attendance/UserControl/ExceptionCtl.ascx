﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ExceptionCtl.ascx.cs"
    Inherits="Web.Attendance.UserControl.ExceptionCtl" %>
<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Assembly="Karpach.WebControls" Namespace="Karpach.WebControls" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/UpDownCtl.ascx" TagName="UpDownCtl" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="InfoMsgCtl" TagPrefix="uc2" %>
<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>
<style type="text/css">
    
</style>
<script type="text/javascript">
        
        var prepareCommand = function (grid, command, record, row) {
            // you can prepare group command
            if (command.command == 'Delete' && ( record.data.InTime == null && record.data.OutTime == null ))
            {
                command.hidden = true;
                command.hideMode = 'visibility';    //you can try 'display' also
            }


           
        };


       

        function ACE_item_selected(source, eventArgs) {
            var value = eventArgs.get_value();
            selEmpId = value;
            <%= Hidden_FilterEmployeeID.ClientID %>.setValue(selEmpId);
        }
        function clearEmp()
        {
            <%= Hidden_FilterEmployeeID.ClientID %>.setValue('');
        }
        function SelectAllChange()
        {

          var value;
          value = <%= ComboFilterAll.ClientID %>.getValue();
          <%= Hidden_AllFilter.ClientID %>.setValue(value);
         

        }

        function TypeChange()
        {
             var value;
            value = <%= ComboFilterAll.ClientID %>.getValue();
            if(value.toString()!="2")
                <%=btnLeave.ClientID %>.hide();
            else
                <%=btnLeave.ClientID %>.show();
        }

        function SelectBranchChange()
        {
          var value;
          value = <%= comboBranchFilter.ClientID %>.getValue();
          <%= Hidden_BranchFilter.ClientID %>.setValue(value);

        }

        var CommandDeleteHandler = function(command, record) {

            <%= Hidden_DeleteEIN.ClientID %>.setValue(record.data.EmployeeId);
                

                 var dateStr = 
                    record.data.DateEng.getFullYear() + '/' + (record.data.DateEng.getMonth() + 1) + '/' + record.data.DateEng.getDate();

                <%= Hidden_DeleteDate.ClientID %>.setValue(dateStr); 

                



            if(command == 'DeleteInTime')
            {
                <%= Hidden_CheckInOrOut.ClientID %>.setValue('true');
            }
            else
                <%= Hidden_CheckInOrOut.ClientID %>.setValue('false');

            <%= btnDeleteTime.ClientID %>.fireEvent('click');
       };

</script>
<style type="text/css">
    .tableDesign > tbody > tr > td
    {
        padding-left: 5px;
        padding-bottom: 6px;
    }
</style>
<div class="pageheader" style="border-bottom: 0px solid red">
    <div class="media">
        <div class="media-body">
            <h4 runat="server" id="title">
                Attendance Exception Entry
            </h4>
        </div>
    </div>
</div>
<div class="contentpanel" style="margin-top: 0px !important; padding-top: 0px !important;">
    <%--<ext:ResourceManager ID="ResourceManager1" runat="server" GZip="true" DisableViewState="false"
        ShowWarningOnAjaxFailure="false" Namespace="CompanyX" ScriptMode="Release">
    </ext:ResourceManager>--%>
    <ext:Hidden runat="server" ID="Hidden_Selection">
    </ext:Hidden>
    <ext:Hidden runat="server" ID="Hidden_EvaluationId">
    </ext:Hidden>
    <ext:Hidden runat="server" ID="TempEngDate">
    </ext:Hidden>
    <ext:Hidden runat="server" ID="Hidden_InTimeID">
    </ext:Hidden>
    <ext:Hidden runat="server" ID="Hidden_OutTimeID">
    </ext:Hidden>
    <ext:Hidden runat="server" ID="Hidden_CIODate">
    </ext:Hidden>
    <ext:Hidden runat="server" ID="Hidden_EmployeeID">
    </ext:Hidden>
    <ext:Hidden runat="server" ID="Hidden_DeleteEIN">
    </ext:Hidden>
    <ext:Hidden runat="server" ID="Hidden_DeleteDate">
    </ext:Hidden>
    <ext:Hidden runat="server" ID="Hidden_CheckInOrOut">
    </ext:Hidden>
    <ext:Hidden runat="server" ID="Hidden_FilterEmployeeID" Text="0">
    </ext:Hidden>
    <ext:Hidden runat="server" ID="Hidden_AllFilter" Text="0">
    </ext:Hidden>
    <ext:Hidden runat="server" ID="Hidden_AllFilter2" Text="0">
    </ext:Hidden>
    <ext:Hidden runat="server" ID="Hidden_BranchFilter" Text="0">
    </ext:Hidden>
    <ext:Button ID="btnEdit" runat="server" StyleSpec="display:none">
        <DirectEvents>
            <Click OnEvent="btnEdit_Click">
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:Button>
    <ext:Button ID="btnDeleteTime" runat="server" Hidden="true">
        <DirectEvents>
            <Click OnEvent="btnDeleteTime_Click">
                <Confirmation ConfirmRequest="true" Message="Confirm delete the time?" />
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:Button>
    <ext:Button ID="btnClear" runat="server" StyleSpec="display:none">
        <DirectEvents>
            <Click OnEvent="btnClear_Click">
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:Button>
    <div class="contentArea">
        <div class="attribute" style="padding-bottom: 0px">
            <table class="tableDesign" cellpadding="0" cellspacing="0">
                <tbody>
                    <tr>
                        <td>
                            <pr:CalendarExtControl LabelWidth="120" Width="150" FieldLabel="Start Date *" ID="CalendarExtControl1"
                                runat="server" LabelAlign="Top" LabelSeparator="" />
                        </td>
                        <td>
                            <pr:CalendarExtControl LabelWidth="120" Width="150" FieldLabel="End Date *" ID="CalendarExtControl2"
                                runat="server" LabelAlign="Top" LabelSeparator="" />
                        </td>
                        <td style="padding-top: 5px;" runat="server" id="tdSearch">
                            <div>
                                Emp Search
                            </div>
                            <asp:TextBox ID="txtEmpSearch" onchange="if(this.value=='') clearEmp();" runat="server"
                                Style="margin-top: 2px;" />
                            <cc1:AutoCompleteExtender EnableCaching="true" ID="AutoCompleteExtenderOrganization"
                                runat="server" MinimumPrefixLength="2" ServiceMethod="GetEmployeeNames" ServicePath="~/PayrollService.asmx"
                                TargetControlID="txtEmpSearch" OnClientItemSelected="ACE_item_selected" CompletionSetCount="10"
                                CompletionInterval="250" CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList"
                                CompletionListHighlightedItemCssClass="AutoExtenderHighlight">
                            </cc1:AutoCompleteExtender>
                        </td>
                        <td>
                            <ext:ComboBox runat="server" ID="ComboFilterAll" LabelAlign="Top" LabelSeparator=""
                                FieldLabel="Exception Type" LabelWidth="120" Width="150" DisplayField="Name"
                                ValueField="ID" ForceSelection="true">
                                <Store>
                                    <ext:Store ID="storeFilterAll" runat="server">
                                        <Model>
                                            <ext:Model IDProperty="AllFilterID">
                                                <Fields>
                                                    <ext:ModelField Name="ID" Type="Int" />
                                                    <ext:ModelField Name="Name" />
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <SelectedItems>
                                    <ext:ListItem Index="0" />
                                </SelectedItems>
                                <Listeners>
                                    <Change Fn="SelectAllChange" />
                                    <Select Fn="TypeChange" />
                                </Listeners>
                            </ext:ComboBox>
                        </td>
                        <td>
                            <ext:ComboBox runat="server" ID="comboBranchFilter" LabelSeparator="" LabelAlign="Top"
                                FieldLabel="Branch" LabelWidth="120" Width="150" DisplayField="Name" ValueField="BranchId"
                                ForceSelection="true">
                                <Store>
                                    <ext:Store ID="StoreBranchFilter" runat="server">
                                        <Model>
                                            <ext:Model IDProperty="BranchId">
                                                <Fields>
                                                    <ext:ModelField Name="BranchId" Type="Int" />
                                                    <ext:ModelField Name="Name" />
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <Listeners>
                                    <Change Fn="SelectBranchChange" />
                                </Listeners>
                            </ext:ComboBox>
                        </td>
                        <td>
                            <ext:ComboBox runat="server" ID="cmbHolidayFilter" LabelSeparator="" SelectedIndex="0"
                                LabelAlign="Top" FieldLabel="Holiday Filter" LabelWidth="120" Width="150" ForceSelection="true">
                                <Items>
                                    <ext:ListItem Value="true" Text="Exlcude Holiday" />
                                    <ext:ListItem Value="false" Text="Include Holiday" />
                                </Items>
                                <SelectedItems>
                                    <ext:ListItem Index="1" />
                                </SelectedItems>
                            </ext:ComboBox>
                        </td>
                        <td style='vertical-align: bottom; padding-bottom: 14px;'>
                            <ext:Button ID="btnLoad" runat="server" Height="30" Text="Load" Icon="ArrowRefresh"
                                Style="margin-top: 25px;" Width="80">
                                <Listeners>
                                    <Click Handler="return searchList();" />
                                </Listeners>
                            </ext:Button>
                        </td>
                        <td style='vertical-align: bottom; padding-bottom: 14px;'>
                            <ext:Button ID="btnLeave" Hidden="true" Height="30" runat="server" Text="Assign Absent"
                                Style="margin-top: 25px; margin-left: 30px;">
                                <DirectEvents>
                                    <Click OnEvent="btnLeave_Click">
                                        <ExtraParams>
                                            <ext:Parameter Name="GridAttendanceExp" Value="Ext.encode(#{gvwEvalulations}.getRowsValues({selectedOnly : true}))"
                                                Mode="Raw" />
                                        </ExtraParams>
                                        <EventMask ShowMask="true">
                                        </EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                        </td>
                        <td style='vertical-align: bottom; padding-bottom: 14px;' id="tdAssignTime" runat="server">
                            <ext:Button ID="btnAssignTime" Height="30" runat="server" Text="Assign Time" Style="margin-top: 25px;
                                margin-left: 30px;">
                                <DirectEvents>
                                    <Click OnEvent="btnAssignTime_Click">
                                        <EventMask ShowMask="true">
                                        </EventMask>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                        </td>
                        <td runat="server" id="tdOTImport">
                            <asp:LinkButton ID="btnExport" runat="server" Text="Import Time for OT" OnClientClick="otImport();return false;"
                                CssClass=" excel marginRight tiptip" Style="float: left;" />
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <ext:Store ID="storeRatings" runat="server">
            <Model>
                <ext:Model IDProperty="RatingId" runat="server">
                    <Fields>
                        <ext:ModelField Name="RatingId" Type="Int" />
                        <ext:ModelField Name="Name" />
                    </Fields>
                </ext:Model>
            </Model>
        </ext:Store>
        <br />
        <div style="clear: both; width: auto;" id="divGrid" runat="server">
            <ext:GridPanel ID="gvwEvalulations" runat="server" StripeRows="true" ClicksToEdit="1"
                Title="Employee List" TrackMouseOver="true" Cls="gridtbl" AutoHeight="true">
                <%--<Listeners>
                    <RowDblClick Fn="rowDoubleClick" />
                    <Command Fn="Clear_Click"></Command>
                </Listeners>--%>
                <SelectionModel>
                    <ext:CheckboxSelectionModel ID="RowSelectionModel" Mode="Simple" runat="server">
                    </ext:CheckboxSelectionModel>
                </SelectionModel>
                <Plugins>
                    <ext:CellEditing ID="CellEditing1" ClicksToEdit="1" runat="server">
                        <Listeners>
                            <BeforeEdit Fn="BeforeEditHandler" />
                            <Edit Fn="AfterEditHandler" />
                        </Listeners>
                    </ext:CellEditing>
                </Plugins>
                <Store>
                    <ext:Store ID="Store2" runat="server" PageSize="100" AutoLoad="false">
                        <Proxy>
                            <ext:AjaxProxy Url="~/Handler/Attendance/AttendanceExceptionHandler.ashx">
                                <ActionMethods Read="GET" />
                                <Reader>
                                    <ext:JsonReader Root="data" TotalProperty="total" />
                                </Reader>
                            </ext:AjaxProxy>
                        </Proxy>
                        <AutoLoadParams>
                            <ext:Parameter Name="start" Value="={0}" />
                            <ext:Parameter Name="limit" Value="100" />
                        </AutoLoadParams>
                        <Parameters>
                            <ext:StoreParameter Name="start" Value="={0}" />
                            <ext:StoreParameter Name="limit" Value="100" Mode="Raw" />
                            <ext:StoreParameter Name="StartDate" Value="#{CalendarExtControl1}.getValue()" Mode="Raw" />
                            <ext:StoreParameter Name="EndDate" Value="#{CalendarExtControl2}.getValue()" Mode="Raw" />
                            <ext:StoreParameter Name="EmployeeId" Value="#{Hidden_FilterEmployeeID}.getValue()"
                                Mode="Raw" />
                            <ext:StoreParameter Name="AllFilter" Value="#{Hidden_AllFilter}.getValue()" Mode="Raw" />
                            <ext:StoreParameter Name="BranchFilter" Value="#{Hidden_BranchFilter}.getValue()"
                                Mode="Raw" />
                            <ext:StoreParameter Name="HolidayFilter" Value="#{cmbHolidayFilter}.getValue()" Mode="Raw" />
                        </Parameters>
                        <Model>
                            <ext:Model Root="Data" TotalProperty="TotalRecords">
                                <Fields>
                                    <ext:ModelField Name="EmployeeId" />
                                    <ext:ModelField Name="Name" />
                                    <ext:ModelField Name="DateEng" Type="Date" />
                                    <ext:ModelField Name="ActualDate" />
                                    <ext:ModelField Name="InTime" Type="Date" />
                                    <ext:ModelField Name="OutTime" Type="Date" />
                                    <ext:ModelField Name="RefinedInRemarks" Type="String" />
                                    <ext:ModelField Name="RefinedOutRemarks" />
                                    <ext:ModelField Name="ForColoring" />
                                    <ext:ModelField Name="AllRemarks" />
                                    <ext:ModelField Name="InOutModified" />
                                    <ext:ModelField Name="ModifiedRemarks" />
                                    <ext:ModelField Name="MofificationReason" />
                                    <ext:ModelField Name="EmpNote" />
                                    <ext:ModelField Name="DayValue" />
                                    <ext:ModelField Name="EmployeeComment" />
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                <ColumnModel ID="ColumnModel2" runat="server">
                    <Columns>
                        <ext:Column ID="Column11" Width="50" runat="server" Locked="true" ColumnID="EmpName1"
                            Text="EIN" DataIndex="EmployeeId" />
                        <ext:Column ID="Column1" Width="120" runat="server" Locked="true" ColumnID="EmpName"
                            Text="Employee Name" DataIndex="Name" />
                        <ext:Column ID="Column2" Width="90" runat="server" Align="Center" Text="Date" DataIndex="ActualDate">
                        </ext:Column>
                        <ext:Column ID="Column3" Width="90" runat="server" Align="Center" ColumnID="In Time"
                            Text="In Time" DataIndex="InTime">
                            <Renderer Fn="Ext.util.Format.dateRenderer('h : i a')" />
                        </ext:Column>
                        <ext:DateColumn ID="DateColumn1" runat="server" Width="90" Align="Center" Text="Out Time"
                            DataIndex="OutTime" Format="hh : mm tt">
                        </ext:DateColumn>
                        <ext:Column ID="Column4" runat="server" Text="In Out Remarks" Width="175" ColumnID="ChkInOutRemarks"
                            DataIndex="AllRemarks">
                        </ext:Column>
                        <ext:Column ID="Column5" runat="server" Text="Modification Remarks" Width="175" ColumnID="ModifiedRemarks"
                            DataIndex="ModifiedRemarks">
                        </ext:Column>
                        <ext:Column ID="Column6" runat="server" Text="Modification Reason" Width="175" ColumnID="MofificationReason"
                            DataIndex="MofificationReason">
                        </ext:Column>
                        <ext:Column ID="colEmpNote" runat="server" Text="Employee Note" Width="175" ColumnID="colEmpNote"
                            DataIndex="EmployeeComment">
                        </ext:Column>
                        <ext:Column ID="Column7" runat="server" Text="Absent" Width="90" ColumnID="DayValue"
                            MenuDisabled="true" DataIndex="DayValue">
                        </ext:Column>
                        <ext:ImageCommandColumn ID="ImageCommandColumn1" runat="server" Width="152">
                            <Commands>
                                <ext:ImageCommand CommandName="Delete" Text="Edit">
                                    <ToolTip Text="Edit the In/Out Time." />
                                </ext:ImageCommand>
                                <ext:ImageCommand CommandName="Add" Text="Add">
                                    <ToolTip Text="Add In/Out Time." />
                                </ext:ImageCommand>
                            </Commands>
                            <PrepareCommand Fn="prepareCommand" />
                            <Listeners>
                                <Command Fn="Clear_Click">
                                </Command>
                            </Listeners>
                        </ext:ImageCommandColumn>
                        <ext:CommandColumn ID="ContextCommand" Text='Delete' runat="server" Width="50">
                            <%-- <PrepareToolbar Fn="fnPrepareToolbarShowHideCommand">
                            </PrepareToolbar>--%>
                            <Commands>
                                <ext:GridCommand>
                                    <Menu EnableScrolling="false">
                                        <Items>
                                            <ext:MenuCommand Text="Delete In Time" Icon="Accept" CommandName="DeleteInTime" />
                                            <ext:MenuCommand Text="Delete Out Time" Icon="Accept" CommandName="DeleteOutTime" />
                                        </Items>
                                    </Menu>
                                    <ToolTip Text="Delete Action" />
                                </ext:GridCommand>
                            </Commands>
                            <Listeners>
                                <Command Handler="CommandDeleteHandler(command,record);" />
                            </Listeners>
                        </ext:CommandColumn>
                    </Columns>
                </ColumnModel>
                <BottomBar>
                    <ext:PagingToolbar ID="PagingToolbar1" runat="server" PageSize="100" DisplayInfo="true"
                        DisplayMsg="(Double click to edit) Displaying employees {0} - {1} of {2}" EmptyMsg="No Evaluation to display">
                        <%-- <Items>
                            <ext:ComboBox Hidden="true" runat="server" SelectedIndex="0" Width="80" ID="cmbPageSize">
                                <Items>
                                    <ext:ListItem Text="20" Value="20" />
                                    <ext:ListItem Text="30" Value="30" />
                                    <ext:ListItem Text="50" Value="50" />
                                </Items>
                                <Listeners>
                                    <Select Handler="#{PagingToolbar1}.pageSize = parseInt( '20'); searchList();" />
                                </Listeners>
                            </ext:ComboBox>
                        </Items>--%>
                    </ext:PagingToolbar>
                </BottomBar>
            </ext:GridPanel>
        </div>
    </div>
    <ext:Window runat="server" ID="window" Icon="Application" Hidden="true" Width="400"
        Resizable="true" Height="300" Title="Exception Time Entry">
        <Content>
            <table class="tableDesign" style="margin-top: 10px">
                <tr>
                    <td>
                        <ext:DisplayField ID="lblEmployee" Width="300" LabelWidth="120" runat="server" FieldLabel="Employee" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <ext:DisplayField ID="lblActualDate" Width="300" LabelWidth="120" runat="server"
                            FieldLabel="Date " />
                    </td>
                </tr>
                <tr>
                    <td>
                        <ext:ComboBox ID="comboInOutMode" Width="300" LabelWidth="120" runat="server" FieldLabel="In-Out Mode *"
                            DisplayField="Name" ValueField="ID" ValidationGroup="AddTimeVal">
                            <Store>
                                <ext:Store ID="storeInOutMode" runat="server">
                                    <Model>
                                        <ext:Model ID="Model1" runat="server" IDProperty="ID">
                                            <Fields>
                                                <ext:ModelField Name="ID" Type="Int" />
                                                <ext:ModelField Name="Name" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                        </ext:ComboBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <ext:TimeField ID="txtTime" QueryMode="Local" ForceSelection="true" Width="300" runat="server"
                            LabelWidth="120" Increment="1" FieldLabel="Check In Time *" ValidationGroup="AddTimeVal" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <ext:TextArea ID="txtComment" Width="300" LabelWidth="120" runat="server" FieldLabel="Note *"
                            ValidationGroup="AddTimeVal" />
                    </td>
                </tr>
            </table>
            <%--<asp:RequiredFieldValidator Enabled="true" runat="server" Display="None" ID="RequiredFieldValidator1"
                ErrorMessage="In Time is required." ValidationGroup="AddTimeVal" ControlToValidate="txtTime" />--%>
            <asp:RequiredFieldValidator Enabled="true" runat="server" Display="None" ID="RequiredFieldValidator3"
                ErrorMessage="Note is required." ValidationGroup="AddTimeVal" ControlToValidate="txtComment" />
            <asp:RequiredFieldValidator runat="server" Enabled="true" Display="None" ID="RequiredFieldValidator2"
                ErrorMessage="In-Out Mode is required" ValidationGroup="AddTimeVal" ControlToValidate="comboInOutMode" />
        </Content>
        <Buttons>
            <ext:Button ID="btnSave" runat="server" Text="Save">
                <DirectEvents>
                    <Click OnEvent="btnSave_Click">
                        <EventMask ShowMask="true" />
                    </Click>
                </DirectEvents>
                <Listeners>
                    <Click Handler="valGroup = 'AddTimeVal'; return CheckValidation();" />
                </Listeners>
            </ext:Button>
            <ext:Button ID="btnCancel" runat="server" Text="Cancel">
                <Listeners>
                    <Click Handler=" #{window}.hide();">
                    </Click>
                </Listeners>
            </ext:Button>
        </Buttons>
    </ext:Window>
    <ext:Window runat="server" ID="WindowEditTime" Icon="Application" Hidden="true" Width="400"
        Resizable="true" Height="300" Title="Exception Time Entry">
        <Content>
            <table class="tableDesign" style="margin-top: 10px">
                <tr>
                    <td>
                        <ext:TimeField ID="TimeFieldIn" Width="300" runat="server" LabelWidth="120" Increment="1"
                            FieldLabel="In Time *" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <ext:TextField ID="txtEditNote1" Width="300" runat="server" LabelWidth="120" Increment="1"
                            FieldLabel="In Edit Note *" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <ext:TimeField ID="TimeFieldOut" Width="300" runat="server" LabelWidth="120" Increment="1"
                            FieldLabel="Out Time *" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <ext:TextField ID="txtEditNote2" Width="300" runat="server" LabelWidth="120" Increment="1"
                            FieldLabel="Out Edit Note *" />
                    </td>
                </tr>
            </table>
        </Content>
        <Buttons>
            <ext:Button ID="Button1" runat="server" Text="Save">
                <DirectEvents>
                    <Click OnEvent="btnSaveTime_Click">
                        <EventMask ShowMask="true" />
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="Button2" runat="server" Text="Cancel">
                <Listeners>
                    <Click Handler=" #{WindowEditTime}.hide();">
                    </Click>
                </Listeners>
            </ext:Button>
        </Buttons>
    </ext:Window>
    <ext:Window runat="server" ID="WindowLeave" Icon="Application" Hidden="true" Width="500"
        Resizable="true" Height="200" Title="Exception Time Entry" Modal="true">
        <Content>
            <table class="tableDesign" style="margin-top: 10px">
                <tr>
                    <td>
                        <ext:DisplayField ID="DisplayFieldMessage" Width="450" runat="server" LabelWidth="120"
                            Increment="1" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <ext:DisplayField ID="DisplayFieldAbsentees" Width="450" runat="server" LabelWidth="120"
                            Increment="1" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <ext:DisplayField ID="DisplayFieldLeaves" Width="450" runat="server" LabelWidth="120"
                            Increment="1" />
                    </td>
                </tr>
            </table>
        </Content>
        <Buttons>
            <ext:Button ID="Button3" runat="server" Text="Save">
                <DirectEvents>
                    <Click OnEvent="btnAbsent_Click">
                        <ExtraParams>
                            <ext:Parameter Name="GridAttendanceExp" Value="Ext.encode(#{gvwEvalulations}.getRowsValues({selectedOnly : true}))"
                                Mode="Raw" />
                        </ExtraParams>
                        <EventMask ShowMask="true" />
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="Button5" runat="server" Text="Delete">
                <DirectEvents>
                    <Click OnEvent="btnDeleteAbs_Click">
                        <ExtraParams>
                            <ext:Parameter Name="GridAttendanceExp" Value="Ext.encode(#{gvwEvalulations}.getRowsValues({selectedOnly : true}))"
                                Mode="Raw" />
                        </ExtraParams>
                        <EventMask ShowMask="true" />
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button ID="Button4" runat="server" Text="Cancel">
                <Listeners>
                    <Click Handler=" #{WindowLeave}.hide();">
                    </Click>
                </Listeners>
            </ext:Button>
        </Buttons>
    </ext:Window>
    <ext:Hidden ID="hdnHistory" runat="server" />
    <ext:Hidden ID="hdnEmployeeId" runat="server" />
    <ext:Store runat="server" ID="storeSearch" AutoLoad="false">
        <Proxy>
            <ext:AjaxProxy Url="../../Handler/EmployeeSearch.ashx">
                <ActionMethods Read="GET" />
                <Reader>
                    <ext:JsonReader Root="plants" TotalProperty="total" />
                </Reader>
            </ext:AjaxProxy>
        </Proxy>
        <Model>
            <ext:Model ID="Model2" IDProperty="Name" Name="ItemLineModel" runat="server">
                <Fields>
                    <ext:ModelField Name="Name" Type="String" />
                    <ext:ModelField Name="EmployeeId" Type="String" />
                </Fields>
            </ext:Model>
        </Model>
    </ext:Store>
    <ext:Window runat="server" Hidden="true" Title="Assign Time Attendance" ID="winAssignTimeAttend"
        Modal="true" Width="1100" Height="600">
        <Content>
            <div style="padding: 15px; padding-top: 5px;">
                <table class="fieldTable" style="margin-left: 20px;">
                    <tr>
                        <td style="width: 170px;">
                            <ext:DateField LabelSeparator="" FieldLabel="Start Date" LabelAlign="Top" runat="server"
                                ID="txtStartDateAdd" Width="150">
                            </ext:DateField>
                            <asp:RequiredFieldValidator runat="server" ID="rfvStartDateAdd" ValidationGroup="TimeAtt"
                                ControlToValidate="txtStartDateAdd" Display="None" ErrorMessage="Start date is required."></asp:RequiredFieldValidator>
                        </td>
                        <td style="width: 170px;">
                            <ext:DateField LabelSeparator="" FieldLabel="End Date" LabelAlign="Top" runat="server"
                                ID="txtEndDateAdd" Width="150">
                            </ext:DateField>
                            <asp:RequiredFieldValidator runat="server" ID="rfvEndateDateAdd" ValidationGroup="TimeAtt"
                                ControlToValidate="txtEndDateAdd" Display="None" ErrorMessage="End date is required."></asp:RequiredFieldValidator>
                        </td>
                        <td>
                            <ext:ComboBox LabelSeparator="" ID="cmbSearch" FieldLabel="Search Employee" LabelWidth="70"
                                LabelAlign="Top" runat="server" DisplayField="Name" ValueField="EmployeeId" StoreID="storeSearch"
                                TypeAhead="false" Width="180" PageSize="9999" HideBaseTrigger="true" MinChars="1"
                                TriggerAction="All" ForceSelection="true">
                                <ListConfig LoadingText="Searching..." MinWidth="200" StyleSpec="border-top:1px solid #98c0f4;">
                                    <ItemTpl ID="ItemTpl1" runat="server">
                                        <Html>
                                            <tpl>
                                            <div class="search-item">
                                                            <span>{Name}</span>  
                                             </div>
					                </tpl>
                                        </Html>
                                    </ItemTpl>
                                </ListConfig>
                                <Triggers>
                                    <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                                </Triggers>
                                <Listeners>
                                    <Select Handler="this.getTrigger(0).show(); SelectEmp()" />
                                    <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                    <TriggerClick Handler="if (index == 0) { 
                                           this.clearValue(); 
                                           this.getTrigger(0).hide();
                                       }" />
                                </Listeners>
                            </ext:ComboBox>
                        </td>
                        <td style="width: 320px;">
                            <ext:TextField ID="txtInNoteAdd" runat="server" FieldLabel="Note" Text="" LabelAlign="Top"
                                LabelSeparator="" Width="300" />
                        </td>
                        <td>
                            <ext:Button ID="Button6" runat="server" StyleSpec="margin-top:17px;" Cls="btn btn-save"
                                Text="Load" Width="100">
                                <DirectEvents>
                                    <Click OnEvent="btnLoad_Click">
                                        <EventMask ShowMask="true" />
                                    </Click>
                                </DirectEvents>
                                <Listeners>
                                    <Click Handler="valGroup = 'TimeAtt'; if(CheckValidation()) return ''; else return false;">
                                    </Click>
                                </Listeners>
                            </ext:Button>
                        </td>
                    </tr>
                </table>
                <table class="fieldTable" style="margin-left: 20px;">
                    <tr>
                        <td>
                            <ext:GridPanel StyleSpec="margin-top:15px;" ID="gridOTList" runat="server" Cls="itemgrid"
                                Scroll="Vertical" Height="380" Width="990">
                                <Store>
                                    <ext:Store ID="Store6" runat="server">
                                        <Model>
                                            <ext:Model ID="OTModel" Name="SettingModel" runat="server" IDProperty="SN">
                                                <Fields>
                                                    <ext:ModelField Name="SN" Type="Int" />
                                                    <ext:ModelField Name="DateEng" Type="Date" />
                                                    <ext:ModelField Name="InTimeDT" Type="Date" />
                                                    <ext:ModelField Name="OutTimeDT" Type="Date" />
                                                    <ext:ModelField Name="InNote" Type="string" />
                                                    <ext:ModelField Name="OutNote" Type="string" />
                                                    <ext:ModelField Name="WorkHours" Type="string" />
                                                    <ext:ModelField Name="OvernightShift" Type="Boolean" />
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <ColumnModel>
                                    <Columns>
                                        <ext:DateColumn ID="DateColumn2" runat="server" Align="Right" Text="Date" Width="100"
                                            MenuDisabled="true" Sortable="false" Format="yyyy-MM-dd" DataIndex="DateEng">
                                            <Editor>
                                                <ext:DateField ID="dfDateAdd" runat="server" />
                                            </Editor>
                                        </ext:DateColumn>
                                        <ext:DateColumn ID="colInTime1" Sortable="false" MenuDisabled="true" runat="server"
                                            Text="Start Time" Align="Right" Width="100" DataIndex="InTimeDT" Format="hh:mm tt">
                                            <Editor>
                                                <ext:TimeField ID="TimeField1" runat="server" MinTime="00:00" MaxTime="23:59" Increment="1"
                                                    Format="hh:mm tt" SelectedTime="08:00">
                                                </ext:TimeField>
                                            </Editor>
                                        </ext:DateColumn>
                                        <ext:Column ID="Column8" Sortable="false" MenuDisabled="true" runat="server" Text="In Note"
                                            Align="Left" Width="200" DataIndex="InNote">
                                            <Editor>
                                                <ext:TextField ID="txtInNote" runat="server">
                                                </ext:TextField>
                                            </Editor>
                                        </ext:Column>
                                        <ext:CheckColumn ID="colOvernightShift" MenuDisabled="true" Sortable="false" Text="Overnight Shift"
                                            DataIndex="OvernightShift" runat="server" Width="120" Editable="true">
                                        </ext:CheckColumn>
                                        <ext:DateColumn ID="colOutTime1" Sortable="false" MenuDisabled="true" runat="server"
                                            Text="End Time" Align="Right" Width="100" DataIndex="OutTimeDT" Format="hh:mm tt">
                                            <Editor>
                                                <ext:TimeField ID="TimeField2" runat="server" MinTime="00:00" MaxTime="23:59" Increment="1"
                                                    Format="hh:mm tt" SelectedTime="08:00">
                                                </ext:TimeField>
                                            </Editor>
                                        </ext:DateColumn>
                                        <ext:Column ID="Column9" Sortable="false" MenuDisabled="true" runat="server" Text="Out Note"
                                            Align="Left" Width="200" DataIndex="OutNote">
                                            <Editor>
                                                <ext:TextField ID="TextField1" runat="server">
                                                </ext:TextField>
                                            </Editor>
                                        </ext:Column>
                                        <ext:Column ID="Column10" Sortable="false" MenuDisabled="true" runat="server" Text="Hours:Minutes"
                                            Align="Center" Width="120" DataIndex="WorkHours">
                                            <Renderer Fn="CalculateWorkHours" />
                                        </ext:Column>
                                        <ext:CommandColumn ID="CommandColumn1" runat="server" Sortable="false" MenuDisabled="true"
                                            Width="50" Align="Center">
                                            <Commands>
                                                <ext:GridCommand Icon="Delete" CommandName="Delete" Cls="deleteicon" />
                                            </Commands>
                                            <Listeners>
                                                <Command Fn="RemoveItemLine">
                                                </Command>
                                            </Listeners>
                                        </ext:CommandColumn>
                                    </Columns>
                                </ColumnModel>
                                <SelectionModel>
                                    <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" Mode="Single" />
                                </SelectionModel>
                                <Plugins>
                                    <ext:CellEditing ID="CellEditing2" runat="server" ClicksToEdit="1">
                                    </ext:CellEditing>
                                </Plugins>
                                <View>
                                    <ext:GridView ID="GridView2" runat="server">
                                        <Plugins>
                                            <ext:GridDragDrop ID="GridDragDrop1" runat="server" DragGroup="ddGroup" DropGroup="ddGroup" />
                                        </Plugins>
                                    </ext:GridView>
                                </View>
                            </ext:GridPanel>
                        </td>
                    </tr>
                </table>
                <br />
                <div style="margin-left: 30px;">
                    <ext:Button runat="server" ID="btnAssignTimeAtt" Cls="btn btn-primary" Text="<i></i>Assign">
                        <DirectEvents>
                            <Click OnEvent="btnAssignTimeAtt_Click">
                                <Confirmation ConfirmRequest="true" Message="Are you sure, you want to assign time attendance?">
                                </Confirmation>
                                <EventMask ShowMask="true" />
                                <ExtraParams>
                                    <ext:Parameter Name="gridItems" Value="Ext.encode(#{gridOTList}.getRowsValues({ selectedOnly: false }))"
                                        Mode="Raw" />
                                </ExtraParams>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                </div>
            </div>
        </Content>
    </ext:Window>
</div>
<ext:ResourcePlaceHolder ID="ResourcePlaceHolder1" Mode="Script" runat="server">
</ext:ResourcePlaceHolder>
<script type="text/javascript">

    var EvaluationCombo = null;
    var hdnEvaluationId = null;
    function PopUpPositionCall() {
        var ret = PopUpPosition("isPopup=true");
        if (typeof (ret) != 'undefined') {
            //ChangeDropDownList('ddlDepartments.ClientID ', ret);
        }
    }
    function PopUpPositionCall1() {
        var ret = PopUpPosition1("isPopup=true");
        if (typeof (ret) != 'undefined') {
            //ChangeDropDownList('ddlDepartments.ClientID ', ret);
        }
    }


    var cmb = function (e1, e2, e3) {
        
    }

    Ext.onReady(function () { 

    
     hdnEvaluationId =<%=Hidden_EvaluationId.ClientID %>

    });

    function searchList()
    {
        valGroup='Evaluation';

        if( CheckValidation()==true)
       

        <%= PagingToolbar1.ClientID %>.moveFirst();
        <%= Hidden_EmployeeID.ClientID %>.setValue('0');
        
        var valueSel =   <%= Hidden_AllFilter.ClientID %>.getValue();
        
        <%= Hidden_AllFilter2.ClientID %>.setValue(valueSel);


    }

    var dateRenderer = function(value,e1,e2,e3)
    {
        return value;
    }
    
        var record;
        var BeforeEditHandler = function(e) {
            record = null;
            isDateColumnChanged = false;
            switch(e.field.toLowerCase())
            {
                case "evalulationdate":
                    isDateColumnChanged = true;
                    record = e.record;
                break;
            }       
        }
        var AfterEditHandler = function(e)
        {
           e.record.commit();
        }
        function refreshGridViewForDateInGrid(date)
        {
            if( date == "" )
                return;
          
            record.set('EvalulationDate',date);
            record.commit();
            //record.store.commit();
        }

        var ratingRenderer = function(value)
        {
            var r = <%= storeRatings.ClientID %>.getById(value);

            if( Ext.isEmpty(r))
                return "";

            return r.data.Name;
        }

        var rowDoubleClick = function(e,e1,e2,e3)
        {
            var employeeId = e.selModel.selections.items[0].data.EmployeeId;
            var ForColoring =  e.selModel.selections.items[0].data.ForColoring;
            var DateEng = e.selModel.selections.items[0].data.DateEng;
            var ActualDate = e.selModel.selections.items[0].data.ActualDate;
            var InTime =  e.selModel.selections.items[0].data.InTime;
            var OutTime = e.selModel.selections.items[0].data.OutTime;

            <%= Hidden_Selection.ClientID %>.setValue( employeeId +"#"+ ForColoring +"#"+ DateEng+"#"+ActualDate+"#"+ InTime + "#" + OutTime);

            <%= btnEdit.ClientID %>.fireEvent('click');
        }
        

        

        var Clear_Click = function(value, command,record,p2)
        {

            if (command == 'Delete')
            {
                var employeeId = record.data.EmployeeId;
                var ForColoring =  record.data.ForColoring;
                var DateEng = record.data.DateEng;
                var ActualDate = record.data.ActualDate;
                var InTime =  record.data.InTime;
                var OutTime = record.data.OutTime;

            <%= Hidden_Selection.ClientID %>.setValue( employeeId +"#"+ ForColoring +"#"+ DateEng+"#"+ActualDate+"#"+ InTime + "#" + OutTime);
                           

                <%= btnClear.ClientID %>.fireEvent('click');
            }
            else if(command == 'Add')
            {
                var employeeId = record.data.EmployeeId;
                var ForColoring =  record.data.ForColoring;
                var DateEng = record.data.DateEng;
                var ActualDate = record.data.ActualDate;
                var InTime =  record.data.InTime;
                var OutTime = record.data.OutTime;

                <%= Hidden_Selection.ClientID %>.setValue( employeeId +"#"+ ForColoring +"#"+ DateEng+"#"+ActualDate+"#"+ InTime + "#" + OutTime);

                <%= btnEdit.ClientID %>.fireEvent('click');

            }
            
        }


        var historyRenderer = function(value, meta, record)
        {
       
            return "<a href='#'  onclick=\"DisplayHistory("+ record.data.EvaluationId + "," + record.data.EmployeeId +");\">History</a>";
        }

        
        var SelectEmp = function()
        {
            var val = <%= cmbSearch.ClientID %>.getValue();
            <%= hdnEmployeeId.ClientID %>.setValue(val);
        };

        var RemoveItemLine = function (column, command, record, recordIndex, cellIndex) {
            var store = this.grid.store;
            store.remove(record);
        };

        var CalculateWorkHours = function(e1,e2,record)
        {
            var overNightShift = false;
            if(record.data.OvernightShift != null && record.data.OvernightShift == true)
            {
                overNightShift = true;
            }

            if(record.data.InTimeDT == null || record.data.OutTimeDT == null)
            {
                return;
            }

            if(record.data.InTimeDT != '' && record.data.OutTimeDT != '')
            {
                var strTime = Ext.util.Format.date(record.data.InTimeDT, 'g:i a')
                var eTime = Ext.util.Format.date(record.data.OutTimeDT, 'g:i a')
                
                var diffTime = calculateTotalMinutes(eTime,overNightShift,true) - calculateTotalMinutes(strTime, overNightShift, false)

                if(diffTime <= 0)
                {
                    alert('End time must be greater than Start time.');
                    return "";
                }


                return (Math.floor(diffTime/60) + ":" + (diffTime%60));
              
            }
        };

       

        function calculateTotalMinutes(time, overNightShift, endTime)
        {
            var parts = time.split(/ |:/);
            var totalHours = 0;

            if(parts[2] == 'pm')
            {
               if(parseInt(parts[0], 10) == 12)
               {
                    return (12 * 60 + parseInt(parts[1], 10));
               }
               else
               {
                    return (12 * 60 + parseInt(parts[0], 10) * 60 + parseInt(parts[1], 10));
               }
            }
            else
            {
                if(overNightShift == true && endTime == true)
                {
                    return (parseInt(parts[0], 10) * 60 + parseInt(parts[1], 10)) + 24*60;
                }
                else
                {
                    return (parseInt(parts[0], 10) * 60 + parseInt(parts[1], 10));
                }
            }
    
            //return (parts[2] == 'pm' * 12 * 60) + parseInt(parts[0], 10) * 60 + parseInt(parts[1], 10);
        }
       
       
</script>
<style type="text/css">
    .tableField
    {
        padding: 20px;
    }
</style>
