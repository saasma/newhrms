﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using BLL.Manager;
using Utils.Base;
using BLL;
using DAL;
using Utils.Web;
using Utils.Helper;
using System.Text.RegularExpressions;
using Ext.Net;
using Utils.Calendar;
using System.Text;

namespace Web.Attendance.UserControl
{
    public enum PageType
    {
        Self = 1,
        Supervisor = 2,
        Admin = 3
    }

    public partial class EmployeeAtteCtl : BaseUserControl
    {
        private PageType _pageType = PageType.Self;
        public PageType PageType
        {
            get { return _pageType; }
            set { _pageType = value; }
        }

      
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!X.IsAjaxRequest && !IsPostBack)
            {
                CustomDate today = CustomDate.GetTodayDate(IsEnglish);
                calFromDate.SelectedDate = today.EnglishDate;
                calToDate.SelectedDate = calFromDate.SelectedDate;


                if (this.PageType == UserControl.PageType.Self)
                {
                    tdEmpSearch.Style["display"] = "none";
                    Column_Name.Hide();
                }
            }
        }




        protected void Store_ReadData(object sender, StoreReadDataEventArgs e)
        {



            int employeeId = 0;
            int supervisorid = 0;

            if (!string.IsNullOrEmpty(txtEmpSearch.Text) && !txtEmpSearch.Text.ToLower().Equals("search text"))
                int.TryParse(hiddenEmployeeID.Text, out employeeId);

            if (PageType == UserControl.PageType.Admin && employeeId == 0)
            {
                X.Js.AddScript("Ext.net.Mask.hide();");
                NewMessage.ShowWarningMessage("Employee should be selected.");
                return;

            }

            if (this.PageType == UserControl.PageType.Self)
                employeeId = SessionManager.CurrentLoggedInEmployeeId;
            else if (this.PageType == UserControl.PageType.Supervisor)
            {
                supervisorid = SessionManager.CurrentLoggedInEmployeeId;
            }

            DateTime startdate = calFromDate.SelectedDate.Date;
            DateTime todate = calToDate.SelectedDate.Date;

            List<AttendanceReportResult> list = AttendanceManager.GetAttendanceOfDateRange(employeeId, 0, startdate, todate,
                supervisorid, e.Page - 1, e.Limit);

            int total = 0;
            if (list.Count > 0 && list[0].TotalRows != null)
                total = list[0].TotalRows.Value;

            e.Total = total;

            //if (list.Any())
            storeEmpList.DataSource = list;
            storeEmpList.DataBind();

            X.Js.AddScript("Ext.net.Mask.hide();");
        }

        public void btnExport_Click(object sender, EventArgs e)
        {

            int employeeId = 0;
            int supervisorid = 0;
            if (!string.IsNullOrEmpty(txtEmpSearch.Text) && !txtEmpSearch.Text.ToLower().Equals("search text"))
            int.TryParse(hiddenEmployeeID.Text, out employeeId);

            if (this.PageType == UserControl.PageType.Self)
                employeeId = SessionManager.CurrentLoggedInEmployeeId;
            else if (this.PageType == UserControl.PageType.Supervisor)
            {
                supervisorid = SessionManager.CurrentLoggedInEmployeeId;
            }

            DateTime startdate = calFromDate.SelectedDate.Date;
            DateTime todate = calToDate.SelectedDate.Date;

            List<AttendanceReportResult> list = AttendanceManager.GetAttendanceOfDateRange(employeeId, 0, startdate, todate,
                supervisorid, 0, int.MaxValue - 1000);


            List<string> hiddenList = new List<string>();


            {
                hiddenList = new List<string> { "TotalRows", "WorkHour","StandardMin", "DateEng", "Type", "InTime", "OutTime", "InRemarks", "OutRemarks", "ExtraHour" };
            }


            if (this.PageType == UserControl.PageType.Self)
            {
                hiddenList.Add("Name");
            }

            string title1 = "Attendance Details";
            string title2 = "";

            if (employeeId != -1 && employeeId != 0)
                title2 += " for " + EmployeeManager.GetEmployeeById(employeeId).Name;

           

            title2 += " from " + startdate.ToShortDateString() + " to " + todate.ToShortDateString();

            Bll.ExcelHelper.ExportToExcel("Attendance Details", list, hiddenList,
            new List<String>() { },
            new Dictionary<string, string>() {  {"DateEngText","Date"}, {"RowNumber","SN"},{"EmployeeId","EIN"}
                
                ,{"ActualDate","Nep Date"},{"DayType","Day Type"},{"RefinedInRemarks","In Time"} ,{"RefinedOutRemarks","Out Time"},{"RefinedWorkHour","Worked hr"}
                ,{"LeaveName","Leave"},{"EmployeeComment","Comment"}
            },
            new List<string>() { }
            ,
            new List<string>() { "RowNumber", "EmployeeId" },
            new List<string>() { "DateEngText" }
            , new Dictionary<string, string>() {
              {title1,title2}
            }
            , new List<string> { "RowNumber","EmployeeId","Name", "Branch", "Department", "DateEngText","ActualDate", "DayType","RefinedInRemarks",
                "RefinedOutRemarks","RefinedWorkHour","LeaveName"
                ,"EmployeeComment"});


        }

        
    }
}