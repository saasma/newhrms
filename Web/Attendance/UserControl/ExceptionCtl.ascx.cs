﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using BLL.Manager;
using Utils.Base;
using BLL;
using DAL;
using Utils.Web;
using Utils.Helper;
using System.Text.RegularExpressions;
using Ext.Net;
using Utils.Calendar;
using System.Text;

namespace Web.Attendance.UserControl
{
    public partial class ExceptionCtl : BaseUserControl
    {

        private bool isBranchMode = false;
        public bool IsBranchMode
        {
            get { return isBranchMode; }
            set
            {
                isBranchMode = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

            if (this.IsBranchMode)
            {
                title.InnerHtml = "Branch Attendance";
                this.Page.Title = title.InnerHtml;
                tdSearch.Style["display"] = "none";
                tdAssignTime.Style["display"] = "none";

                tdOTImport.Visible = false;
            }
            
            if (!X.IsAjaxRequest)
            {
                if (
                    (CommonManager.Setting.DisableAttendanceTimeAddEdit != null && CommonManager.Setting.DisableAttendanceTimeAddEdit.Value)
                    )
                    ImageCommandColumn1.Hide();
            }

            // ReportDataSetTableAdapters.AttendanceExceptionTableAdapter adap = new ReportDataSetTableAdapters.AttendanceExceptionTableAdapter();

            JavascriptHelper.AttachNonDialogPopUpCode(Page, "PopUpPosition", "../HRRating.aspx", 625, 600);
            JavascriptHelper.AttachNonDialogPopUpCode(Page, "PopUpPosition1", "../HREvaluationPeriod.aspx", 625, 600);


            JavascriptHelper.AttachNonDialogPopUpCode(Page, "otImport", "../ExcelWindow/OTTimeImport.aspx", 450, 500);
          

            if (!X.IsAjaxRequest)
            {
                CalendarExtControl1.Text = CustomDate.GetTodayDate(IsEnglish).ToString();
                CalendarExtControl2.Text = CustomDate.GetTodayDate(IsEnglish).ToString();
                Initialize();
                X.Js.AddScript("isEnglish={0};", IsEnglish.ToString().ToLower());

                List<GenericComboBoxList> comboList = new List<GenericComboBoxList>();
                GenericComboBoxList item = new GenericComboBoxList();
                item.Name = "All";
                item.ID = 1;
                comboList.Add(item);
                item = new GenericComboBoxList();
                item.Name = "Exception Only";
                item.ID = 0;
                comboList.Add(item);
                item = new GenericComboBoxList();
                item.Name = "Both Time Missing";
                item.ID = 2;
                comboList.Add(item);
                storeFilterAll.DataSource = comboList;
                storeFilterAll.DataBind();
                ComboFilterAll.ClearValue();

                List<Branch> branchList = new List<Branch>();
                branchList = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId);
                List<Branch> copyBranchList = new List<Branch>();

                if (this.IsBranchMode == false)
                {
                    copyBranchList = branchList.Select(x => new Branch { BranchId = x.BranchId, Name = x.Name }).ToList();

                    Branch branch = new Branch();
                    branch.Name = "All";
                    branch.BranchId = 0;
                    copyBranchList.Insert(0, branch);
                }
                else
                {
                    copyBranchList = branchList
                        .Where(x => x.BranchHeadId == SessionManager.CurrentLoggedInEmployeeId)
                        .Select(x => new Branch { BranchId = x.BranchId, Name = x.Name }).ToList();

                    Branch branch = new Branch();
                    branch.Name = "All";
                    branch.BranchId = 0;
                    copyBranchList.Insert(0, branch);
                }


                StoreBranchFilter.DataSource = copyBranchList;
                StoreBranchFilter.DataBind();
            }
            Register();

            if (SessionManager.IsReadOnlyUser)
                gvwEvalulations.Listeners.ClearListeners();
        }

        private void Initialize()
        {

            storeRatings.DataSource = PositionGradeStepAmountManager.GetAllRatings(SessionManager.CurrentCompanyId);
            storeRatings.DataBind();

            //gvwEvalulations.Store[0].BaseParams["companyId"] = SessionManager.CurrentCompanyId.ToString();

        }





        public GetEvaluationInfoResult GetEvalulation(int evalulationPeriodId, int employeeId)
        {
            List<GetEvaluationInfoResult> list = PositionGradeStepAmountManager.GetEvaluationPeriod(0, 10,
               SessionManager.CurrentCompanyId, evalulationPeriodId, -1, -1, -1, -1, "", employeeId);

            if (list.Count > 0)
            {
                return list[0];
            }

            return null;
        }



        [DirectMethod]
        public void btnClear_Click(object sender, DirectEventArgs e)
        {

            TimeFieldOut.Hidden = false;
            string[] values = Hidden_Selection.Text.Split(new char[] { '#' });

            int employeeId = int.Parse(values[0]);
            string ActualDate = values[3];
            DateTime chkinoutTime = GetEngDate(ActualDate);
            Hidden_CIODate.Text = chkinoutTime.ToShortDateString();

            Hidden_EmployeeID.Text = employeeId.ToString();

            string Time1 = AttendanceManager.GetCheckInOutTimeToModify(employeeId, chkinoutTime, 1);
            string InTime = "";
            string InTimeID = "";

            if (Time1 != "")
            {
                string[] timeValues = Time1.Split(new char[] { '#' });

                InTime = timeValues[0];
                InTimeID = timeValues[1];
                txtEditNote1.Text = timeValues[2];
                Hidden_InTimeID.Text = InTimeID;
                TimeFieldIn.Text = InTime;
            }
            else
            {
                txtEditNote1.Text = "";
                Hidden_InTimeID.Text = "";
                TimeFieldIn.Clear();
            }


            string Time2 = AttendanceManager.GetCheckInOutTimeToModify(employeeId, chkinoutTime, 2);

            if (Time2 != "")
            {
                string[] timeValues = Time2.Split(new char[] { '#' });
                string OutTime = timeValues[0];
                string OutTimeID = timeValues[1];
                txtEditNote2.Text = timeValues[2];
                Hidden_OutTimeID.Text = OutTimeID;

                if (InTimeID == OutTimeID)
                {
                    TimeFieldOut.Hidden = true;
                }
                TimeFieldOut.Text = OutTime;

            }
            else
            {
                txtEditNote2.Text = "";
                Hidden_OutTimeID.Text = "";
                TimeFieldOut.Clear();
            }
            //txtEditNote2.Text = Time2.Split(new char[] { '#' })[2];


            WindowEditTime.Show();

            /*
            string msg = AttendanceManager.DeleteModifiedTime(employeeId, chkinoutTime);
            if (msg == "true")
            {
                X.Js.AddScript("searchList();");
            }
            */



            // MessageBoxConfig msgboxconfig = new MessageBoxConfig();
            // msgboxconfig.Message = employeeId+ "<br>" +chkinoutTime.ToString();
            //X.Msg.Show(msgboxconfig);

        }


        public void btnEdit_Click(object sender, DirectEventArgs e)
        {
            string[] values = Hidden_Selection.Text.Split(new char[] { '#' });

            int employeeId = int.Parse(values[0]);


            string DateEng = (values[2]);
            string ActualDate = values[3];
            //BufferString = Hidden_Selection.Text;
            lblEmployee.Text = AttendanceManager.GetEmployeeByID(employeeId).Name;
            lblActualDate.Text = ActualDate;

            TempEngDate.Text = ActualDate; //Convert.ToDateTime(DateEng).ToShortDateString();
            GenericComboBoxList item = new GenericComboBoxList();
            List<GenericComboBoxList> comboList = new List<GenericComboBoxList>();
            comboList = new List<GenericComboBoxList>();
            item = new GenericComboBoxList();
            item.Name = "Check In";
            item.ID = 0;
            comboList.Add(item);
            item = new GenericComboBoxList();
            item.Name = "CheckOut";
            item.ID = 1;
            comboList.Add(item);
            storeInOutMode.DataSource = comboList;
            storeInOutMode.DataBind();

            //clear field
            comboInOutMode.ClearValue();
            txtTime.Clear();
            txtComment.Text = "";

            window.Show();


        }

        public void btnAbsent_Click(object sender, DirectEventArgs e)
        {
            string entryline = e.ExtraParams["GridAttendanceExp"];
            List<AttendanceExceptionResult> entryLineObj = JSON.Deserialize<List<AttendanceExceptionResult>>(entryline);

            //foreach (var value1 in entryLineObj)
            for (int i = entryLineObj.Count - 1; i >= 0; i--)
            {
                var value1 = entryLineObj[i];
                if (
                    (!string.IsNullOrEmpty(value1.InTime.ToString()) || !string.IsNullOrEmpty(value1.OutTime.ToString()))
                    //if not in out time & is not saved as absent then only remove
                    && (value1.DayValue == null || value1.DayValue != "ABS")

                    )
                {
                    entryLineObj.Remove(value1);
                }
            }

            List<AttendanceAbsent> ABSLIST = AttendanceManager.GetAbsenteesAddList(entryLineObj);
            //<AttendanceAbsent> DELETELIST = AttendanceManager.AbsenteesDeleteList(entryLineObj);

            AttendanceManager.AddAbsentToDB(ABSLIST);
            X.Js.AddScript("searchList();");
            WindowLeave.Hide();
        }

        public void btnDeleteTime_Click(object sender, DirectEventArgs e)
        {
            int ein = int.Parse(Hidden_DeleteEIN.Text);
            DateTime date = Convert.ToDateTime(Hidden_DeleteDate.Text);
            bool isInTime = Convert.ToBoolean(Hidden_CheckInOrOut.Text);

            Status status = AttendanceManager.DeleteTime(ein, date, isInTime);
            if (status.IsSuccess == false)
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);

            }
            else
            {
                NewMessage.ShowNormalMessage("Time has been deleted.", "searchList");
            }

        }
        public void btnDeleteAbs_Click(object sender, DirectEventArgs e)
        {
            string entryline = e.ExtraParams["GridAttendanceExp"];
            List<AttendanceExceptionResult> entryLineObj = JSON.Deserialize<List<AttendanceExceptionResult>>(entryline);

            //foreach (var value1 in entryLineObj)
            for (int i = entryLineObj.Count - 1; i >= 0; i--)
            {
                var value1 = entryLineObj[i];
                if (
                    (!string.IsNullOrEmpty(value1.InTime.ToString()) || !string.IsNullOrEmpty(value1.OutTime.ToString()))
                    //if not in out time & is not saved as absent then only remove
                    && (value1.DayValue == null || value1.DayValue != "ABS")

                    )
                {
                    entryLineObj.Remove(value1);
                }
            }

            //List<AttendanceAbsent> ABSLIST = AttendanceManager.GetAbsenteesAddList(entryLineObj);
            List<AttendanceAbsent> DELETELIST = AttendanceManager.AbsenteesDeleteList(entryLineObj);

            AttendanceManager.DeleteAbsentFromDB(DELETELIST);
            X.Js.AddScript("searchList();");
            WindowLeave.Hide();
        }

        public void btnLeave_Click(object sender, DirectEventArgs e)
        {
            if (Hidden_AllFilter2.Text == "2")
            {
                string entryline = e.ExtraParams["GridAttendanceExp"];
                List<AttendanceExceptionResult> entryLineObj = JSON.Deserialize<List<AttendanceExceptionResult>>(entryline);

                //foreach (var value1 in entryLineObj)
                for (int i = entryLineObj.Count - 1; i >= 0; i--)
                {
                    var value1 = entryLineObj[i];
                    if (
                        (!string.IsNullOrEmpty(value1.InTime.ToString()) || !string.IsNullOrEmpty(value1.OutTime.ToString()))
                        //if not in out time & is not saved as absent then only remove
                        && (value1.DayValue == null || value1.DayValue != "ABS")

                        )
                    {
                        entryLineObj.Remove(value1);
                    }
                }

                List<AttendanceAbsent> ABSLIST = AttendanceManager.GetAbsenteesAddList(entryLineObj);
                List<AttendanceAbsent> DELETELIST = AttendanceManager.AbsenteesDeleteList(entryLineObj);

                DisplayFieldMessage.Text = entryLineObj.Count() + " Employee Selected with no In-Out Time.";

                DisplayFieldAbsentees.Text = ABSLIST.Count() + " Employee Selected would be saved on ABS List";

                DisplayFieldLeaves.Text = DELETELIST.Count() + " Employee Selected would be deleted from ABS List";

                WindowLeave.Show();
            }
            else
            {
                X.Msg.Alert("", "This works only if Grid is loaded with Absentees Filter.").Show();
            }

        }



        public void btnSaveTime_Click(object sender, DirectEventArgs e)
        {

            if (TimeFieldIn.Hidden == false && (TimeFieldIn.SelectedValue == null || string.IsNullOrEmpty(TimeFieldIn.SelectedValue.ToString())))
            {
                X.MessageBox.Show(
            new MessageBoxConfig
            {
                Message = "In time is required.",
                Buttons = MessageBox.Button.OK,
                Title = "Warning",
                Icon = MessageBox.Icon.WARNING,
                MinWidth = 300
            });
                return;
            }

            if (TimeFieldOut.Hidden == false && (TimeFieldOut.SelectedValue == null || string.IsNullOrEmpty(TimeFieldOut.SelectedValue.ToString())))
            {
                X.MessageBox.Show(
            new MessageBoxConfig
            {
                Message = "Out time is required.",
                Buttons = MessageBox.Button.OK,
                Title = "Warning",
                Icon = MessageBox.Icon.WARNING,
                MinWidth = 300
            });
                return;
            }

            string checkInTime = TimeFieldIn.Text;
            string checkOutTime = TimeFieldOut.Text;
            string CIODate = Hidden_CIODate.Text;

            string EditNote1 = txtEditNote1.Text;
            string EditNote2 = txtEditNote2.Text;

            string checkInID = Hidden_InTimeID.Text;
            string checkOutID = Hidden_OutTimeID.Text;

            string employeeID = Hidden_EmployeeID.Text;

            DateTime CheckInDateTime = new DateTime();
            DateTime CheckOutDateTime = new DateTime();


            CheckInDateTime = DateTime.Parse(CIODate + " " + checkInTime);
            CheckOutDateTime = DateTime.Parse(CIODate + " " + checkOutTime);

            bool isSuccess = true;
            try
            {
                isSuccess = AttendanceManager.UpdateCheckInCheckOutTime(checkInID, checkOutID, employeeID, CheckInDateTime, CheckOutDateTime, EditNote1, EditNote2);

            }
            catch (Exception exp)
            {
                if (exp.Message.ToLower().Contains("cannot insert duplicate key in object"))
                {
                    X.MessageBox.Show(
                    new MessageBoxConfig
                    {
                        Message = "Attendance already exists for this time & mode.",
                        Buttons = MessageBox.Button.OK,
                        Title = "Warning",
                        Icon = MessageBox.Icon.WARNING,
                        MinWidth = 300
                    });
                    return;
                }
            }
            if (isSuccess == true)
            {
                WindowEditTime.Hide();
                X.Js.AddScript("searchList();");
            }
        }




        public void btnSave_Click(object sender, DirectEventArgs e)
        {
            //Page.Validate("AEEvaluation");

            if (string.IsNullOrEmpty(txtTime.Text) || string.IsNullOrEmpty(txtComment.Text) || string.IsNullOrEmpty(comboInOutMode.SelectedItem.Value))
            {

                X.MessageBox.Show(
              new MessageBoxConfig
              {
                  Message = "Please fill the form correctly.",
                  Buttons = MessageBox.Button.OK,
                  Title = "Warning",
                  Icon = MessageBox.Icon.WARNING,
                  MinWidth = 300
              });
                return;
            }


            //if (Page.IsValid)
            {
                string[] values = Hidden_Selection.Text.Split(new char[] { '#' });

                int employeeId = int.Parse(values[0]);

                DAL.AttendanceMapping map = BLL.BaseBiz.PayrollDataContext.AttendanceMappings
                    .SingleOrDefault(x => x.EmployeeId == employeeId);


                if (map == null)
                {

                    X.MessageBox.Show(
                  new MessageBoxConfig
                  {
                      Message = "Employee mapping with the device does not exists.",
                      Buttons = MessageBox.Button.OK,
                      Title = "Warning",
                      Icon = MessageBox.Icon.WARNING,
                      MinWidth = 300
                  });
                    return;
                }




                AttendanceCheckInCheckOut chkInOut = new AttendanceCheckInCheckOut();
                chkInOut.ChkInOutID = Guid.NewGuid();
                chkInOut.DeviceID = map.DeviceId;
                chkInOut.AuthenticationType = (byte)TimeAttendanceAuthenticationType.SupervisorAdminModified;
                chkInOut.InOutMode = byte.Parse(comboInOutMode.SelectedItem.Value);

                string actualDate = TempEngDate.Text;
                string actualTime = txtTime.Text;

                DateTime chkinoutDate = GetEngDate(actualDate);
                DateTime chkinoutTime = Convert.ToDateTime(actualTime);

                chkInOut.DateTime = new DateTime(chkinoutDate.Year, chkinoutDate.Month, chkinoutDate.Day, chkinoutTime.Hour, chkinoutTime.Minute, 0);
                chkInOut.Date = chkInOut.DateTime.Date;
                chkInOut.ModifiedBy = SessionManager.User.UserID;
                chkInOut.ModifiedOn = BLL.BaseBiz.GetCurrentDateAndTime();
                chkInOut.ModificationRemarks = txtComment.Text;



                try
                {
                    AttendanceManager.SaveOrUpdateAttendanceException(chkInOut);
                }
                catch (Exception exp1)
                {
                    if (exp1.Message.ToLower().Contains("cannot insert duplicate key in object"))
                    {
                        X.MessageBox.Show(
                        new MessageBoxConfig
                        {
                            Message = "Attendance already exists for this time & mode.",
                            Buttons = MessageBox.Button.OK,
                            Title = "Warning",
                            Icon = MessageBox.Icon.WARNING,
                            MinWidth = 300
                        });
                        return;
                    }
                }

                window.Hide();
                X.Js.AddScript("searchList();");
            }
        }

        public void Register()
        {

            //ResourceManager1.RegisterClientInitScript("Date1", string.Format("isEnglish = {0};", IsEnglish ? "true" : "false"));



            //ResourceManager1.RegisterClientInitScript("Date2", string.Format("todayDate = '{0}';function calendarOnClick(event, sourceElement, e3){{}};",
            //                                                          CustomDate.GetTodayDate(IsEnglish).ToString()));
        }

        private void Clear()
        {
            txtStartDateAdd.Text = "";
            txtEndDateAdd.Text = "";
            hdnEmployeeId.Text = "";
            txtInNoteAdd.Text = "";
            cmbSearch.Text = "";
        }

        protected void btnAssignTime_Click(object sender, DirectEventArgs e)
        {
            Clear();
            gridOTList.GetStore().DataSource = new List<TimeRequestLine>();
            gridOTList.GetStore().DataBind();

            winAssignTimeAttend.Center();
            winAssignTimeAttend.Show();
        }

        protected void btnLoad_Click(object sender, DirectEventArgs e)
        {
            DateTime startDate = new DateTime();
            DateTime endDate = new DateTime();

            if (txtStartDateAdd.SelectedDate != new DateTime())
                startDate = txtStartDateAdd.SelectedDate;
            else
            {
                NewMessage.ShowWarningMessage("Start date is required.");
                return;
            }

            if (txtEndDateAdd.SelectedDate != new DateTime())
                endDate = txtEndDateAdd.SelectedDate;
            else
            {
                NewMessage.ShowWarningMessage("End date is required.");
                return;
            }

            if (startDate > DateTime.Now.Date || endDate.Date > DateTime.Now.Date)
            {
                NewMessage.ShowWarningMessage("Date cannot be greater than today date.");
                return;
            }

            int employeeId = 0;

            if (!string.IsNullOrEmpty(hdnEmployeeId.Text))
                employeeId = int.Parse(hdnEmployeeId.Text);
            else
            {
                NewMessage.ShowWarningMessage("Please select employee.");
                cmbSearch.Focus();
                return;
            }

            if (AttendanceManager.CheckTimeRequestIsSaved(startDate, endDate, employeeId))
            {
                NewMessage.ShowWarningMessage("Time request is already saved for the period.");
                return;
            }

            string message = AttendanceManager.CheckEmpTimeAttRequestThreshold(startDate, endDate);
            if (message != "")
            {
                NewMessage.ShowWarningMessage(message);
                return;
            }

            List<TimeRequestLine> list = AttendanceManager.GetTimeRequestLineForDateRange(startDate, endDate, txtInNoteAdd.Text.Trim(), employeeId);


            gridOTList.GetStore().DataSource = list;
            gridOTList.GetStore().DataBind();

            //winAssignTimeAttend.Height = (list.Count * 25) + 300;
            winAssignTimeAttend.Show();
            winAssignTimeAttend.Center();

        }


        protected void btnAssignTimeAtt_Click(object sender, DirectEventArgs e)
        {
            string entryLineJson = e.ExtraParams["gridItems"];

            List<TimeRequestLine> timeRequestList = JSON.Deserialize<List<TimeRequestLine>>(entryLineJson);
            List<TimeRequestLine> newTimeRequestList = new List<TimeRequestLine>();
            TimeRequest objTimeRequest = new TimeRequest();

            if (timeRequestList.Count == 0)
            {
                btnAssignTimeAtt.Enable();
                NewMessage.ShowWarningMessage("No row added to assign time.");
                return;
            }

            int workDaysCount = 0;
            int row = 0;

            int employeeId = 0;
            if (!string.IsNullOrEmpty(hdnEmployeeId.Text))
                employeeId = int.Parse(hdnEmployeeId.Text);
            else
            {
                NewMessage.ShowWarningMessage("Please select employee.");
                cmbSearch.Focus();
                return;
            }

            foreach (var item in timeRequestList)
            {
                TimeRequestLine obj = new TimeRequestLine();
                row++;
                if (item.DateEng != null)
                {
                    obj.DateEng = item.DateEng;
                    obj.DateName = BLL.BaseBiz.GetAppropriateDate(obj.DateEng.Value);
                }
                else
                {
                    NewMessage.ShowWarningMessage("Date is required for the row " + row.ToString());
                    btnAssignTimeAtt.Enable();
                    return;
                }

                objTimeRequest.EmployeeId = employeeId;
                obj.OvernightShift = item.OvernightShift;

                TimeSpan tsStartTime, tsEndTime;

                obj.Description = item.Description;

                if (string.IsNullOrEmpty(obj.Description))
                    workDaysCount++;

                if (item.InTimeDT != null && item.InTimeDT != new DateTime())
                {

                    tsStartTime = new TimeSpan(item.InTimeDT.Value.Hour, item.InTimeDT.Value.Minute, 0);
                    obj.InTime = tsStartTime;
                }

                obj.InNote = item.InNote;

                if (item.OutTimeDT != null && item.OutTimeDT != new DateTime())
                {
                    tsEndTime = new TimeSpan(item.OutTimeDT.Value.Hour, item.OutTimeDT.Value.Minute, 0);
                    obj.OutTime = tsEndTime;
                }

                if (obj.InTime == null && obj.OutTime == null)
                {
                    NewMessage.ShowWarningMessage("Start time or End time is required for the row " + row.ToString());
                    btnAssignTimeAtt.Enable();
                    return;
                }

                obj.OutNote = item.OutNote;

                if ((item.InTimeDT != null && item.InTimeDT != new DateTime()) && (item.OutTimeDT != null && item.OutTimeDT != new DateTime()))
                {
                    if (obj.OutTime.Value <= obj.InTime.Value && obj.OvernightShift == false)
                    {
                        NewMessage.ShowWarningMessage("Out time must be greater than In time for row " + row.ToString());
                        return;
                    }

                    TimeSpan tsOutTime = new TimeSpan();
                    if (item.OvernightShift.Value)
                    {
                        tsOutTime = obj.OutTime.Value.Add(new TimeSpan(24, 0, 0));
                    }
                    else
                        tsOutTime = obj.OutTime.Value;

                    TimeSpan totalWorkHourTS = tsOutTime - obj.InTime.Value;
                    double time = 0;

                    if (item.OvernightShift.Value)
                    {
                        time = double.Parse((totalWorkHourTS.Hours).ToString() + "." + totalWorkHourTS.Minutes.ToString());
                        obj.WorkHours = time;
                    }
                    else
                    {
                        time = double.Parse(totalWorkHourTS.Hours.ToString() + "." + totalWorkHourTS.Minutes.ToString());
                        obj.WorkHours = time;
                    }

                }

                newTimeRequestList.Add(obj);

            }

            objTimeRequest.StartDateEng = newTimeRequestList[0].DateEng;
            objTimeRequest.StartDate = newTimeRequestList[0].DateName;
            objTimeRequest.EndDateEng = newTimeRequestList[newTimeRequestList.Count - 1].DateEng;
            objTimeRequest.EndDate = newTimeRequestList[newTimeRequestList.Count - 1].DateName;
            objTimeRequest.WorkDays = workDaysCount;
            objTimeRequest.SubmittedOnEng = System.DateTime.Now;
            objTimeRequest.SubmittedOn = BLL.BaseBiz.GetAppropriateDate(objTimeRequest.SubmittedOnEng.Value);
            objTimeRequest.Status = (int)AttendanceRequestStatusEnum.Approved;

            //string IPAddress = AttendanceManager.GetIPAddressByEmployeeId(objTimeRequest.EmployeeId.Value);
            //if (IPAddress != "")
                objTimeRequest.IPAddress = "";

            if (SessionManager.CurrentLoggedInEmployeeId != 0)
                objTimeRequest.ApprovalId1 = SessionManager.CurrentLoggedInEmployeeId;

            objTimeRequest.TimeRequestLines.AddRange(newTimeRequestList);

            Status status = AttendanceManager.ApproveAttRequestTime(new List<TimeRequest>{objTimeRequest}, true,true);

            if (status.IsSuccess)
            {
                NewMessage.ShowNormalMessage("Time request saved successfully.");
                winAssignTimeAttend.Close();
                X.Js.AddScript("searchList();");
            }
            else
                NewMessage.ShowWarningMessage(status.ErrorMessage);
        }

        
    }
}