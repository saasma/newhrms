﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Base;
using BLL.Manager;
using Utils.Helper;
using DAL;
using Utils.Web;
using BLL.Entity;

namespace Web.CP
{
    public partial class ChangeEmployeeStatus : BasePage
    {
        PayManager payMgr = new PayManager();
        int employeeId = 0;
        int statusId = 0;
        EmployeeManager empMgr = new EmployeeManager();

        protected void Page_Load(object sender, EventArgs e)
        {
            JavascriptHelper.AttachEnableDisablingJSCode(chkDOJToNotDefined, calTo.ClientID + "_date", false);


          
            employeeId = UrlHelper.GetIdFromQueryString("EId");
            if (!string.IsNullOrEmpty(Request.QueryString["statusId"]))
                statusId = UrlHelper.GetIdFromQueryString("statusId");
            if (!IsPostBack)
            {
                Initialise();                

            }
            //else if (eventTarget != null && eventTarget.Equals("Reload"))
            //{
            //    Initialise();
            //}

            
        }

        void Initialise()
        {

            ddlEventType.DataSource = CommonManager.GetServieEventTypes();
            ddlEventType.DataBind();

            if (IsEnglish == false && SessionManager.User.IsEnglishPreferred != null && SessionManager.User.IsEnglishPreferred)
            {
                calFrom.IsEnglishCalendar = true;
                calTo.IsEnglishCalendar = true;
            }
            else
            {
                calFrom.IsEnglishCalendar = IsEnglish;
                calTo.IsEnglishCalendar = IsEnglish;
            }

            //calFrom.IsEnglishCalendar = SessionManager.CurrentCompany.IsEnglishDate;
            calFrom.SelectTodayDate();
            //calTo.IsEnglishCalendar = SessionManager.CurrentCompany.IsEnglishDate;
            calTo.SelectTodayDate();

            int empId = employeeId;

            if (empId != 0)
            {
                BizHelper.Load(new JobStatus(), ddlStatus);

                if (statusId == 0)
                {
                    ECurrentStatus lastStatus = empMgr.GetCurrentLastStatus(employeeId);
                    HidePreviousStatus(lastStatus.CurrentStatus.ToString());
                }
            }

            if (statusId != 0)
            {
                ECurrentStatus status = empMgr.GetStatus(statusId);

                bool isFirstEmployeeCurrentStatus = NewHRManager.IsFirstEmployeeCurrentStatus(empId, statusId);
                if (isFirstEmployeeCurrentStatus)
                {
                    lblStatusName.Text = "Appointment/Hire Status";
                    lblFromDate.Text = "Appointment/Hire Date";
                }

                if (IsEnglish == false && SessionManager.User.IsEnglishPreferred != null && SessionManager.User.IsEnglishPreferred)
                    calFrom.SetSelectedDate(GetEngCustomDate(status.FromDate,false).ToString(), true);
                else
                    calFrom.SetSelectedDate(status.FromDate, IsEnglish);


                if (status.DefineToDate != null && status.DefineToDate.Value)
                    chkDOJToNotDefined.Checked = status.DefineToDate.Value;

                if (chkDOJToNotDefined.Checked && !string.IsNullOrEmpty(status.ToDate))
                {
                    calTo.Enabled = true;
                    if (IsEnglish == false && SessionManager.User.IsEnglishPreferred != null && SessionManager.User.IsEnglishPreferred)
                        calTo.SetSelectedDate(GetEngCustomDate(status.ToDate, false).ToString(), true);
                    else
                        calTo.SetSelectedDate(status.ToDate, IsEnglish);
                }
                HidePreviousStatus(status.CurrentStatus.ToString());
                txtNote.Text = status.Note;
                UIHelper.SetSelectedInDropDown(ddlStatus, status.CurrentStatus.ToString());

                if (status.EventID != null && status.EventID != -1)
                    UIHelper.SetSelectedInDropDown(ddlEventType, status.EventID.ToString());
                else if (isFirstEmployeeCurrentStatus)
                    UIHelper.SetSelectedInDropDown(ddlEventType, ((int)ServiceEventType.Appointment).ToString());
                //ddlStatus.SelectedValue = status.CurrentStatus.ToString();
            }
            
        }

        public void btnSaveStatus_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {

                if (HttpContext.Current.User.Identity.IsAuthenticated == false || SessionManager.CurrentLoggedInEmployeeId != 0)
                    return;

                string statusHTML = "";

                ECurrentStatus empStatus = new ECurrentStatus();
                empStatus.ECurrentStatusId = statusId;
                empStatus.EmployeeId = employeeId;
                empStatus.CurrentStatus = int.Parse(ddlStatus.SelectedValue);

                                 
                
                empStatus.Note = txtNote.Text.Trim();

                if (chkDOJToNotDefined.Checked)
                {
                    empStatus.DefineToDate = true;

                    if (IsEnglish == false && SessionManager.User.IsEnglishPreferred != null && SessionManager.User.IsEnglishPreferred)
                    {
                        empStatus.ToDateEng = BLL.BaseBiz.GetEngDate(calTo.SelectedDate.ToString(), true);
                        empStatus.ToDate = BLL.BaseBiz.GetAppropriateDate(empStatus.ToDateEng.Value);
                    }
                    else
                    {
                        empStatus.ToDateEng = BLL.BaseBiz.GetEngDate(calTo.SelectedDate.ToString(), null);
                        empStatus.ToDate = BLL.BaseBiz.GetAppropriateDate(empStatus.ToDateEng.Value); 
                    }
                }
                else
                {
                    empStatus.DefineToDate = false;
                }

				 if (empStatus.CurrentStatus == 0)
                {
                    JavascriptHelper.DisplayClientMsg("Status selection is required.", this);
                    return;
                }

                 if (IsEnglish == false && SessionManager.User.IsEnglishPreferred != null && SessionManager.User.IsEnglishPreferred)
                 {
                     empStatus.FromDateEng = BLL.BaseBiz.GetEngDate(calFrom.SelectedDate.ToString(), true);
                     empStatus.FromDate = BLL.BaseBiz.GetAppropriateDate(empStatus.FromDateEng);                     
                 }
                 else
                 {
                     empStatus.FromDateEng = BLL.BaseBiz.GetEngDate(calFrom.SelectedDate.ToString(), null);
                     empStatus.FromDate = BLL.BaseBiz.GetAppropriateDate(empStatus.FromDateEng); 
                 }

                 empStatus.EventID = int.Parse(ddlEventType.SelectedValue);

                EmployeeManager empMgr = new EmployeeManager();

                string msg = "";
                ResponseStatus status = null;

                if (empStatus.ECurrentStatusId == 0)
                {
                    msg = "Status saved.";
                    status = empMgr.Save(empStatus);
                        //statusHTML = empMgr.GetCurrentStatus(empStatus.EmployeeId);
                }
                else
                {
                    msg = "Status updated.";
                    //if (empMgr.Update(empStatus))
                    status = empMgr.Update(empStatus);
                    //statusHTML = empMgr.GetCurrentStatus(empStatus.EmployeeId);
                }

                if (status.IsSuccessType == false)
                {
                    divWarningMsg.InnerHtml = status.ErrorMessage;
                    divWarningMsg.Hide = false;
                    //JavascriptHelper.DisplayClientMsg(status.ErrorMessage,this);
                    return;
                }

                JavascriptHelper.DisplayClientMsg(msg, this, "closePopup();");
            }
        }

        public void HidePreviousStatus(string currentStatus)
        {

            if (!string.IsNullOrEmpty(currentStatus))
            {
                //now no status exists so disable change
                //if (currentStatus == JobStatus.PERMANENT)
                //{
                //    btnSaveStatus.Visible = false;
                //    ddlStatus.Visible = false;
                //    valReqdName2.Visible = false;
                //    calStatusDate.Visible = false;
                //    return;
                //}

                //if (CommonManager.CompanySetting.IsStatusDownGradeEnable == false)
                //{

                //    for (int i = ddlStatus.Items.Count - 1; i >= 0; i--)
                //    {
                //        ListItem item = ddlStatus.Items[i];
                //        if (item.Value.Equals(currentStatus))
                //        {
                //            //remove all previous status;
                //            for (int j = i - 1; j > 0; j--)
                //            {
                //                ddlStatus.Items.RemoveAt(j);
                //            }
                //            break;
                //        }
                //    }
                //}
                //else
                {
                    ddlStatus.Items.RemoveAt(1);// remove single All Employee status only
                }
            }
        }

        protected void btnOk_Click(object sender, EventArgs e)
        {

            if (Page.IsValid)
            {

               
            }
        }
    }
}
