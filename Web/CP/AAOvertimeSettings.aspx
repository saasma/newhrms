﻿<%@ Page Title="Pay Overtime Settings" Language="C#" MasterPageFile="~/Master/HROld.Master"
    AutoEventWireup="true" CodeBehind="AAOvertimeSettings.aspx.cs" Inherits="Web.CP.AAOvertimeSettings" %>

<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningMsgCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="InfoMsgCtl" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">


        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                alert("Please Enter Only Numeric Value:");
                return false;
            }

            return true;
        }

 
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <%--  <h2>Employee List</h2>
    --%>
    <ext:ResourceManager ID="ResourceManager1" DisableViewState="false" runat="server" />
    <div class="contentpanel">
        <div class="contentArea1">
            <div>
                <uc2:InfoMsgCtl ID="msgInfo" EnableViewState="false" Hide="true" runat="server" />
                <uc2:WarningMsgCtl ID="msgWarning" EnableViewState="false" Hide="true" runat="server" />
                <table>
                    <tr>
                        <td valign="top">
                            <h3 style="margin-bottom: 7px;">
                                Overtime Rules</h3>
                            <table style="clear: both">
                                <tr>
                                    <td colspan="4" style="border-top: solid 1px #7f7f7f; padding-top: 5px;">
                                        <p>
                                            Minimum eligible minute for Morning/Evening OT
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:TextBox ID="txtMinTime" onkeypress="return isNumberKey(event)" runat="server"></asp:TextBox>
                                    </td>
                                    <td>
                                        <p>
                                            minutes</p>
                                    </td>
                                    <td>
                                        <p style="color: #7f7f7f; margin-left: 25px;">
                                            put 0 if you want to allow any time</p>
                                    </td>
                                </tr>
                                 <tr>
                                    <td colspan="4" style="padding-top: 5px;">
                                        <p>
                                            Maximum Minute in a Day
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:TextBox ID="txtMaxMinInADay" onkeypress="return isNumberKey(event)" runat="server"></asp:TextBox>
                                    </td>
                                    <td>
                                        
                                    </td>
                                    <td>
                                        <p style="color: #7f7f7f; margin-left: 25px;">
                                            put 0 for no restriction</p>
                                    </td>
                                </tr>
                                 <tr>
                                    <td colspan="4" style="padding-top: 5px;">
                                        <p>
                                            Maximum Minute in a Week
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:TextBox ID="txtMaxMinInAWeek" onkeypress="return isNumberKey(event)" runat="server"></asp:TextBox>
                                    </td>
                                    <td>
                                       
                                    </td>
                                    <td>
                                        <p style="color: #7f7f7f; margin-left: 25px;">
                                             put 0 for no restriction</p>
                                    </td>
                                </tr>
                                <tr style="display: none">
                                    <td colspan="4">
                                        <p>
                                            Maximum claim minutes
                                        </p>
                                    </td>
                                </tr>
                                <tr style="display: none">
                                    <td>
                                        <asp:TextBox ID="txtMaxTime" onkeypress="return isNumberKey(event)" runat="server"></asp:TextBox>
                                    </td>
                                    <td>
                                        <p>
                                            minutes</p>
                                    </td>
                                    <td>
                                        <p style="color: #7f7f7f; margin-left: 25px;">
                                            put 0 if you want to allow any time</p>
                                    </td>
                                </tr>
                                <tr style="display: none">
                                    <td colspan="4">
                                        <p>
                                            Round to the multiple of
                                        </p>
                                    </td>
                                </tr>
                                <tr style="display: none">
                                    <td>
                                        <asp:TextBox ID="txtRounding" onkeypress="return isNumberKey(event)" runat="server"></asp:TextBox>
                                    </td>
                                    <td>
                                        <p>
                                            minutes</p>
                                    </td>
                                    <td>
                                        <p style="color: #7f7f7f; margin-left: 25px;">
                                            put 0 if you want to allow any time</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <p>
                                            Hours in Month for Calculation
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:TextBox ID="txtMonthHours" onkeypress="return isNumberKey(event)" runat="server"></asp:TextBox>
                                    </td>
                                    <td>
                                        <p>
                                            Hours</p>
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <p>
                                            Past Permitted Days
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:TextBox ID="txtPastPerimittedDays" onkeypress="return isNumberKey(event)" runat="server"></asp:TextBox>
                                    </td>
                                    <td>
                                        <p>
                                            day(s)</p>
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <p>
                                            Overtime Income
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:DropDownList AppendDataBoundItems="true" ID="ddlIncomes" runat="server" Width="180px"
                                            DataTextField="Title" DataValueField="IncomeId">
                                            <asp:ListItem Text="--Select Income--" Value="-1" />
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4" style='padding-top:20px;'>
                                        <p>
                                            Allow future days request for OT
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                       <asp:CheckBox runat="server" ID="chkAllowFutureDays" />
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                                <%--  <tr>
                                    <td colspan="4">
                                        <p>
                                            Minimum Buffer Time After Regular Shift
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top">
                                        <asp:TextBox ID="txtMinBufferTimeAfterRegShift" onkeypress="return isNumberKey(event)" runat="server"></asp:TextBox>
                                    </td>
                                    <td valign="top">
                                        <p>
                                            Minute(s)</p>
                                    </td>
                                    <td valign="top" style="padding-left:20">
                                        Exclude Buffer Time for these Levels
                                        <asp:TreeView ID="tvException" Visible="false"  runat="server" ShowCheckBoxes="None" ShowLines="false">                                          
                                        </asp:TreeView>

                                        <asp:CheckBoxList ID="cbLevelException" Width="200px" runat="server" DataValueField="Value" DataTextField="Key">
                                          
                                        </asp:CheckBoxList>
                                        
                                    </td>
                                </tr>
                                --%>
                                <tr>
                                    <td>
                                        <asp:Button ID="btnSave" cls="btn btn-primary btn-sect btn-sm" ValidationGroup="SaveUpdate"
                                            runat="server" Text="Save Changes" Width="100" Height="30" Style="margin-top: 25px;"
                                            OnClick="btnSave_Click" />
                                    </td>
                                </tr>
                            </table>
                            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator2" ValidationGroup="SaveUpdate"
                                ControlToValidate="txtRounding"></asp:RequiredFieldValidator>
                            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ValidationGroup="SaveUpdate"
                                ControlToValidate="txtMaxTime"></asp:RequiredFieldValidator>
                            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator3" ValidationGroup="SaveUpdate"
                                ControlToValidate="txtMinTime"></asp:RequiredFieldValidator>
                            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator6" ValidationGroup="SaveUpdate"
                                ControlToValidate="txtPastPerimittedDays" ErrorMessage="Past permitted days is required."></asp:RequiredFieldValidator>
                            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator4" ValidationGroup="SaveUpdate"
                                ControlToValidate="txtMonthHours" ErrorMessage="Hours in Month is required."></asp:RequiredFieldValidator>
                            <%--<asp:RequiredFieldValidator InitialValue="-1" runat="server" ID="RequiredFieldValidator5"
                                ValidationGroup="SaveUpdate" ControlToValidate="ddlIncomes" ErrorMessage="Overtime income is required."></asp:RequiredFieldValidator>--%>
                        </td>
                        <td valign="top" style="padding-right: 20px; padding-left: 20px;">
                            <h3 style="margin-bottom: 7px;">
                                HR Setting</h3>
                            <table style="clear: both">
                                <tr>
                                    <td colspan="2" style="border-top: solid 1px #7f7f7f;">
                                        <p>
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:CheckBox Text="Enable Joining Date" ID="chkEnableJoiningDate" runat="server" />
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="border-top: solid 1px #7f7f7f; padding-top: 10px;">
                                        <asp:CheckBox Text="Hide Team HR Details" ID="chkHideTeamHRDetails" runat="server" />
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                                 <tr>
                                    <td colspan="2" style="border-top: solid 1px #7f7f7f; padding-top: 10px;">
                                        <asp:CheckBox Text="Make EIN Editable" ID="chkEINEditable" runat="server" />
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                                 <tr>
                                    <td colspan="2" style="border-top: solid 1px #7f7f7f; padding-top: 10px;">
                                        <asp:CheckBox Text="Show Review Dashboard" ID="chkShowReviewDashboard" runat="server" />
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Button ID="btnHideTeamHRDetails" cls="btn btn-primary btn-sm btn-sect" runat="server"
                                            Text="Save" Width="100" Height="30" Style="margin-top: 25px;" OnClick="btnHideTeamHRDetails_Click" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td valign="top">
                            <h3 style="margin-bottom: 7px;">
                                TADA Setting</h3>
                            <table style="clear: both">
                                <tr>
                                    <td colspan="2" style="border-top: solid 1px #7f7f7f;">
                                        <p>
                                            TADA Income
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:DropDownList AppendDataBoundItems="true" ID="ddlTADAIncomeList" runat="server"
                                            Width="180px" DataTextField="Title" DataValueField="IncomeId">
                                            <asp:ListItem Text="--Select Income--" Value="-1" />
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Button ID="btnSaveTADA" ValidationGroup="SaveUpdateTADA" runat="server" Text="Save"
                                            Width="100" Height="30" Style="margin-top: 25px;" OnClick="btnSaveTADA_Click" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td valign="top" style="padding-left: 20px;">
                            <h3 style="margin-bottom: 7px;">
                                Pay Setting</h3>
                            <table style="clear: both">
                                <tr>
                                    <td style="border-top: solid 1px #7f7f7f; padding-top: 15px; padding-bottom: 15px;">
                                        <p>
                                            Allow multi Add-On
                                        </p>
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="chkMultiAddOn" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="border-top: solid 1px #7f7f7f; padding-top: 15px; padding-bottom: 15px;">
                                        <p>
                                            Hide Current month Payslip / Tax Details
                                        </p>
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="chkHidePaySlip" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="border-top: solid 1px #7f7f7f; padding-top: 15px; padding-bottom: 15px;">
                                        <p>
                                            Hide All month Payslip / Tax Details
                                        </p>
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="chkHideAllMonthPaySlip" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="border-top: solid 1px #7f7f7f; padding-top: 15px; padding-bottom: 15px;">
                                        <p>
                                            Exclude Retirement day in Calculation
                                        </p>
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="chkExcludeRetDayInCalculation" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="border-top: solid 1px #7f7f7f; padding-top: 15px; padding-bottom: 15px;">
                                        <p>
                                            Process Approved OT/Allowance in Add-On only
                                        </p>
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="chkProcessOTAllowanceInAddOn" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="border-top: solid 1px #7f7f7f; padding-top: 15px; padding-bottom: 15px;">
                                        <p>
                                            Allow Negative Net Salary Saving
                                        </p>
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="chkAllowNegativeNetSalarySaving" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="border-top: solid 1px #7f7f7f; padding-top: 15px; padding-bottom: 15px;">
                                        <p>
                                            Enable Health Insurance in tax deduction
                                        </p>
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="chkEnableHealthInsurance" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="border-top: solid 1px #7f7f7f; padding-top: 15px; padding-bottom: 15px;">
                                        <p>
                                            Bring Gratuity in Retirement
                                        </p>
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="chkBringGratuityInRetirement" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:Button ID="btnSavePaySetting" runat="server" Text="Save" Width="100" Height="30"
                                            Style="margin-top: 25px;" OnClick="btnSavePaySetting_Click" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>
