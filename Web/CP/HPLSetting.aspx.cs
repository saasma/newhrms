﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using BLL.Manager;
using BLL;
using DAL;
using Utils.Web;
using Utils.Helper;
using System.Text;

namespace Web.CP
{

    public class AllowanceItem
    {
        public string Key { get; set; }
        public string Description { get; set; }
        public string Calculation { get; set; }
        public string HeaderName { get; set; }
        public double? Rate { get; set; }
    }

    public partial class HPLSetting : BasePage
    {
        CommonManager commonMgr = new CommonManager();



        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                Initialise();


          

        }






        public void Initialise()
        {

            DAL.HPLAllowanceRate rate = HPLAllowanceManager.GetAllowanceRate();

            if (rate.HideOtherAllowance != null)
                chkHideOtherAllowance.Checked = rate.HideOtherAllowance.Value;
            else
                chkHideOtherAllowance.Checked = false;

            List<AllowanceItem> items = new List<AllowanceItem>();

            items.Add(new AllowanceItem { Key = "Days_At_Palati", Description = "Palati Allowance",
                                          HeaderName = (string.IsNullOrEmpty(rate.PalatiHeader) ? "Head work allowance" : rate.PalatiHeader),
                                          Calculation = "Rate Per Day",
                                          Rate = rate.Days_At_Palati_Rate
            });

            items.Add(new AllowanceItem { Key = "On_Call_Rate", Description = "On Call Allowance",
                                          HeaderName = (string.IsNullOrEmpty(rate.OnCallHeader) ? "On call allowance" : rate.OnCallHeader),
                                          Calculation = "Rate Per Day",
                                          Rate = rate.On_Call_Rate
            });

            items.Add(new AllowanceItem { Key = "Shift_Afternoon_Rate", Description = "Shift Afternoon Rate",
                                          HeaderName = (string.IsNullOrEmpty(rate.ShiftAfternoonHeader) ? "Shift afternoon" : rate.ShiftAfternoonHeader),
                                          Calculation = "Percentage",
                                          Rate = rate.Shift_Afternoon_Rate
            });
            items.Add(new AllowanceItem { Key = "Shift_Night_Rate", Description = "Shift Night Rate",
                                          HeaderName = (string.IsNullOrEmpty(rate.ShiftNightHeader) ? "Shift night" : rate.ShiftNightHeader),
                                          Calculation = "Percentage",
                                          Rate = rate.Shift_Night_Rate
            });
            items.Add(new AllowanceItem { Key = "Split_Shift_Morning_Rate", Description = "Split Shift Morning Rate",
                                          HeaderName = (string.IsNullOrEmpty(rate.SplitShiftMorningHeader) ? "Shift morning" : rate.SplitShiftMorningHeader),
                                          Calculation = "Percentage",
                                          Rate = rate.Split_Shift_Morning_Rate
            });
            items.Add(new AllowanceItem { Key = "Split_Shift_Evening_Rate", Description = "Split Shift Evening Rate",
                                          HeaderName = (string.IsNullOrEmpty(rate.SplitShiftEveningHeader) ? "Shift evening" : rate.SplitShiftEveningHeader),
                                          Calculation = "Percentage",
                                          Rate = rate.Split_Shift_Evening_Rate
            });

            items.Add(new AllowanceItem { Key = "OT_150_Rate", Description = "Overtime: Normal",
                                          HeaderName = (string.IsNullOrEmpty(rate.OT150Header) ? "Overtime 150%" : rate.OT150Header),
                                          Calculation = "Percentage",
                                          Rate = rate.OT_150_Rate
            });
            items.Add(new AllowanceItem { Key = "OT_200_Rate", Description = "Overtime : Night",
                                          HeaderName = (string.IsNullOrEmpty(rate.OT200Header) ? "Overtime 200%" : rate.OT200Header),
                                          Calculation = "Percentage",
                                          Rate = rate.OT_200_Rate
            });
            items.Add(new AllowanceItem { Key = "OT_300_Rate", Description = "Overtime : Festival",
                                          HeaderName = (string.IsNullOrEmpty(rate.OT300Header) ? "Overtime 300%" : rate.OT300Header),
                                          Calculation = "Percentage",
                                          Rate = rate.OT_300_Rate
            });

            items.Add(new AllowanceItem { Key = "Public_Holiday_Rate", Description = "Overtime : Public Holiday",
                                          HeaderName = (string.IsNullOrEmpty(rate.PublicHolidayHeader) ? "Public holiday" : rate.PublicHolidayHeader),
                                          Calculation = "Percentage",
                                          Rate = rate.Public_Holiday_Rate
            });

            gvw.DataSource = items;
            gvw.DataBind();

            //HPLAllowanceRate rate = HPLAllowanceManager.GetAllowanceRate();

            //if (rate != null)
            //{


            //}


           

        }


       
        protected void btnSave_Click1(object sender, EventArgs e)
        {
          
        }

        protected void gvw_RowEditing(object sender, GridViewEditEventArgs e)
        {
            gvw.EditIndex = e.NewEditIndex;
            Initialise();
        }

        protected void gvw_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {

            string key = gvw.DataKeys[e.RowIndex]["Key"].ToString();
            string description = gvw.DataKeys[e.RowIndex]["Description"].ToString();
            string rateValue = (gvw.Rows[e.RowIndex].FindControl("txt") as TextBox).Text;
            string headerName = (gvw.Rows[e.RowIndex].FindControl("txtHeaderName") as TextBox).Text;
            double value = 0;

            if (double.TryParse(rateValue, out value)==false)
            {

                divWarningMsg.InnerHtml = "Rate is not valid for " + description;
                divWarningMsg.Hide = false;
                return;
            }

            value = double.Parse(rateValue);
           

            if (string.IsNullOrEmpty(headerName))
            {

                divWarningMsg.InnerHtml = "Header name required for " + description;
                divWarningMsg.Hide = false;
                return;
            }

            HPLAllowanceManager.UpdateAllowanceRate(key, value,headerName);

            gvw.EditIndex = -1;
            Initialise();


            divMsgCtl.InnerHtml = "Saved";
            divMsgCtl.Hide = false;

        }

        public void chkHideOtherAllowance_Change(object sender, EventArgs e)
        {
            HPLAllowanceManager.HideOtherAllowance(chkHideOtherAllowance.Checked);
        }
        protected void gvw_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gvw.EditIndex = -1;
            Initialise();
        }

      
    }

}

