<%@ Page Title="Branch List" Language="C#" MasterPageFile="~/Master/HROld.Master"
    AutoEventWireup="true" CodeBehind="ManageBranch.aspx.cs" Inherits="Web.CP.ManageBranch" %>

<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function popupAddIncomeCall() {

            var ret = popupIncome();
            if (typeof (ret) != 'undefined') {
                if (ret == 'ReloadIncome') {
                    //refreshIncomeList(0);
                    __doPostBack('Reload', '');
                }
            }

            return false;
        }

        function popupUpdateIncomeCall(incomeId) {
            var ret = popupIncome('IId=' + incomeId);
            if (typeof (ret) != 'undefined') {
                if (ret == 'ReloadIncome') {
                    //refreshIncomeList(0);
                    __doPostBack('Reload', '');
                }
            }
            return false;
        }

        //callback function in case of chrome to be called from modal popup as it doesn't supports modal window
        function parentIncomeCallbackFunction(state, popupWindow) {
            if (state == 'ReloadIncome') {
                //refreshIncomeList(0);
                popupWindow.close();
                __doPostBack('Reload', '');
                // popupWindow.close();
            }
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <ext:ResourceManager runat="server" ScriptMode="Release" DisableViewState="false" />
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Branches in your organization
                </h4>
            </div>
        </div>
    </div>
    <ext:TransformGrid ID="TransformGrid1" runat="server" Table="ctl00_ContentPlaceHolderMain_gvwBranches">
        <ColumnModel>
            <Columns>
                <ext:Column ID="Column1" runat="server" Text="Branch Name" DataIndex="Name" />
            </Columns>
        </ColumnModel>
        <Plugins>
            <ext:FilterHeader ID="FilterHeader1" runat="server" />
        </Plugins>
    </ext:TransformGrid>
   <%-- <ext:GridPanel StyleSpec="margin-top:15px;" ID="gridBranchTransfer" runat="server"
        Cls="itemgrid">
        <Plugins>
            <ext:FilterHeader ID="FilterHeader2" runat="server" />
        </Plugins>
        <ColumnModel ID="ColumnModel1" runat="server">
            <Columns>
                <ext:Column ID="Column4" runat="server" Text="Nep Date" DataIndex="Date" MenuDisabled="false"
                    Sortable="true" Align="Left" Width="120" Filterable="false" />
                <ext:Column ID="Column2" runat="server" Text="Comment" DataIndex="Comment" MenuDisabled="false"
                    Sortable="true" Align="Left" Width="300" />
            </Columns>
        </ColumnModel>
    </ext:GridPanel>--%>
    <div class="contentpanel">
        <div class="contentArea">
            <%-- <asp:UpdatePanel ID="updPanel" runat="server">
                <Triggers>
                    <asp:PostBackTrigger ControlID="btnExport" />
                </Triggers>
                <ContentTemplate>--%>
            <uc2:MsgCtl ID="divMsgCtl" EnableViewState="false" Hide="true" runat="server" />
            <uc2:WarningCtl ID="divWarningMsg" EnableViewState="false" Hide="true" runat="server" />

            <div class="attribute" runat="server" id="divSearch" style="padding: 10px">
                <table>
                    <tr>
                        <td style="width:90px;">
                            Search Branch
                        </td>
                        <td style="width:200px;">
                            <asp:TextBox ID="txtBranchName" runat="server" Width="180px" />
                        </td>
                        <td style="width:50px;">
                            Status
                        </td>
                        <td style="width:90px; padding-top:5px;">
                            <asp:DropDownList ID="ddlStatus" DataTextField="Text"
                                Width="70px" CssClass="marginbtns" DataValueField="Value" runat="server"
                                AppendDataBoundItems="true">
                                <asp:ListItem Value="-1" Selected="True">All</asp:ListItem>
                                <asp:ListItem Value="0" >Active</asp:ListItem>
                                <asp:ListItem Value="1">Inactive</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td>
                            <asp:Button ID="btnLoad" Text="Load" OnClick="btnLoad_Click" runat="server" CssClass="btn btn-default btn-sect btn-sm" Style="width: 80px" />
                        </td>
                    </tr>
                </table>
            </div>

            <div style='clear:both;'>
            
            </div>

            <asp:GridView Style="visibility: hidden;" CssClass="table table-primary mb30 table-bordered table-hover"
                PagerStyle-HorizontalAlign="Center" PagerStyle-CssClass="defaultPagingBar" UseAccessibleHeader="true"
                ID="gvwBranches" CellPadding="3" Width="100%" runat="server" DataKeyNames="BranchId"
                AutoGenerateColumns="False" GridLines="None" AllowPaging="True" PageSize="500"
                OnRowCreated="gvwEmployees_RowCreated" OnRowDeleting="gvwIncomes_RowDeleting">
                <Columns>
                    <asp:TemplateField HeaderStyle-Width="50" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                        HeaderText="SN">
                        <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                        <ItemTemplate>
                            <%# Container.DisplayIndex + 1 %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                        DataField="Name" HeaderText="Branch Name">
                        <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                        DataField="Code" HeaderText="Code">
                        <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                    </asp:BoundField>
                  
                    <asp:BoundField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                        DataField="Phone" HeaderText="Phone">
                        <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                    </asp:BoundField>
                   
                    <%--<asp:BoundField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                DataField="Head" HeaderText="Branch Head">
                                <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                            </asp:BoundField>--%>
                    <asp:TemplateField HeaderText="Branch Head">
                        <ItemTemplate>
                            <%# Eval("Head")%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Region Head">
                        <ItemTemplate>
                            <%# Eval("RegionHead")%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-HorizontalAlign="Center" HeaderText="Head Office"
                        ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <%# Utils.Web.UIHelper.GetYesImage( Eval("IsHeadOffice"),Page) %>
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-HorizontalAlign="Center" HeaderText="Is Regional Office"
                        ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <%# Eval("IsRegionalOffice") == null ? "" : (Convert.ToBoolean(Eval("IsRegionalOffice")) ? "Yes":"" )%>
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-HorizontalAlign="Center" HeaderText="Remote Branch"
                        ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <%# Eval("IsInRemoteArea") == null ? "" : (Convert.ToBoolean(Eval("IsInRemoteArea")) ? "Yes" : "")%>
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-HorizontalAlign="Center" HeaderText="Active"
                        ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <%# Utils.Web.UIHelper.GetYesNoImageTag(Eval("IsActive") == null ? "true" : Eval("IsActive"), Page)%>
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-Width="30px" HeaderText="Edit" HeaderStyle-HorizontalAlign="Center"
                        ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <asp:ImageButton ID="btnEdit" Visible='<%# Eval("BranchId").ToString()=="0" ? false : true %>'
                                CommandName="Delete" ImageUrl="~/images/edit.gif" runat="server" OnClientClick='<%# "return popupUpdateIncomeCall(" +  Eval("BranchId") + ");" %>'
                                AlternateText="Edit" />
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Delete" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="30px">
                        <ItemTemplate>
                            <asp:ImageButton ID="ImageButton2" OnClientClick="return confirm('Are you sure, you want to delete the Branch?');"
                                runat="server" CommandName="Delete" ImageUrl="~/images/delet.png" />
                        </ItemTemplate>
                        <HeaderStyle Width="30px"></HeaderStyle>
                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <RowStyle CssClass="odd" />
                <AlternatingRowStyle CssClass="even" />
                <SelectedRowStyle CssClass="selected" />
                <PagerStyle HorizontalAlign="Center" CssClass="defaultPagingBar"></PagerStyle>
                <EmptyDataTemplate>
                    No Branch has been created.
                </EmptyDataTemplate>
            </asp:GridView>
            <div class="buttonsDivSection" style="margin-top: 10px">
                <asp:LinkButton ID="btnAddNew" Height="30px" OnClientClick="return popupAddIncomeCall();"
                    runat="server" CssClass="btn btn-primary btn-sm btn-sect floatleft" Text="Add New Branch" />
            </div>
            <div class="buttonsDivSection" style="margin-top: 10px;">
                <asp:LinkButton Style='margin-left: 20px;' ID="btnExport" OnClick="btnExport_Click"
                    Height="30px" runat="server" CssClass="btn btn-primary btn-sm btn-sect floatleft"
                    Text="Export" />
            </div>
            <br />
            <br />
            <br />
        </div>
    </div>
</asp:Content>
