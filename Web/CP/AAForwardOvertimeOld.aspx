<%@ Page Title="Forward Overtime" Language="C#" MasterPageFile="~/Master/HROld.Master"
    AutoEventWireup="true" EnableViewState="true" CodeBehind="AAForwardOvertimeOld.aspx.cs"
    Inherits="Web.CP.AAForwardOvertimeOld" %>

<%@ Import Namespace="BLL.Manager" %>
<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/PagingCtl.ascx" TagName="PagingCtl" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

<link href="../_assets/themes/yui/tabs.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript">
        var loadingButtonWhenSubmitting = false;
        var skipLoadingCheck = true;
        function addSkillSetToEmployeePopup(requestid) {
            var theText = requestid.getAttribute("value");
            positionHistoryPopup("isPopup=true&reqid=" + theText);
        }
        //ctl00_mainContent_gvw_ctl02_btnAction var theText = $(requestid).attributes.value.nodeValue();

        function refreshEventList(popupWindow) {
            if (typeof (popupWindow) != 'undefined')
                popupWindow.close();

            __doPostBack('<%=btnLoad.UniqueID %>', '');

        }

        function selectDeselectAll(chk) {
            $('input[type=checkbox]').each(
                    function (index) {
                        if (this.id.indexOf('chkDelete') > 0)
                            this.checked = chk.checked;
                    }
                );
        }


        function assignOvertime() {

            assignovertimePopup("isPopup=true&assign=true");
        }

       
    </script>
    <style type="text/css">
        .tableLightColor a:hover
        {
            color: #048FC2;
            text-decoration: none;
        }
        #content
        {
            padding: 0px !important;
        }
        .contentLeftBlockRemoval
        {
            padding-left: 0px;
        }
        .mb30 th
        {
             font-weight:normal !important;
        }
    </style>
    <script type="text/javascript" src="<%= ResolveUrl("~/Employee/override.js?v=") + Web.Helper.WebHelper.Version %>"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <ext:ResourceManager ID="ResourceManager1" DisableViewState="false" runat="server"
        ScriptMode="Release" />
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Overtime Requests
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <div class="attribute">
            <table class="fieldTable">
                <tr>
                    <td>
                        Overtime Type
                    </td>
                    <td>
                        Period
                    </td>
                    <td>
                        From
                    </td>
                    <td>
                        To
                    </td>
                    <td>
                        Branch
                    </td>
                    <td style="padding-left: 5px">
                        Search Employee
                    </td>
                    <td style="padding-left: 5px; display:none;">
                        Status
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    
                </tr>
                <tr>
                    <td>
                        <asp:DropDownList ID="ddlOvertimeType" AutoPostBack="false" AppendDataBoundItems="true"
                            DataTextField="Name" DataValueField="OvertimeTypeId" runat="server" Width="150px">
                            <asp:ListItem Text="--Select Type--" Value="-1" Selected="True" />
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlType" runat="server">
                            <asp:ListItem Value="-1" Text="All" Selected="True" />
                            <asp:ListItem Value="1">This Week</asp:ListItem>
                            <asp:ListItem Value="2">This Month</asp:ListItem>
                            <asp:ListItem Value="3">Last Month</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td runat="server" id="t2">
                        <ext:DateField ID="dateFrom" runat="server" Width="100px">
                            <Plugins>
                                <ext:GenericPlugin ID="HighlightDates1" runat="server" InstanceName="Ext.ux.DatePicker.HighlightWithNepDates" />
                            </Plugins>
                        </ext:DateField>
                    </td>
                    <td runat="server" id="t3">
                        <ext:DateField ID="dateTo" runat="server" Width="100px">
                            <Plugins>
                                <ext:GenericPlugin ID="GenericPlugin1" runat="server" InstanceName="Ext.ux.DatePicker.HighlightWithNepDates" />
                            </Plugins>
                        </ext:DateField>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlBranch" AutoPostBack="false" AppendDataBoundItems="true"
                            DataTextField="Name" DataValueField="BranchId" runat="server" Width="150px">
                            <asp:ListItem Text="--Select Branch--" Value="-1" Selected="True" />
                        </asp:DropDownList>
                    </td>
                    <td style="padding-left: 5px">
                        <asp:TextBox Width="160px" ID="txtEmpSearchText" runat="server"></asp:TextBox>
                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtEmpSearchText"
                            WatermarkText="Employee Name" WatermarkCssClass="searchBoxText" />
                        <cc1:AutoCompleteExtender EnableCaching="true" ID="AutoCompleteExtenderOrganization"
                            runat="server" MinimumPrefixLength="2" ServiceMethod="GetEmployeeNames" ServicePath="~/PayrollService.asmx"
                            TargetControlID="txtEmpSearchText" CompletionSetCount="10" CompletionInterval="250"
                            CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList"
                            CompletionListHighlightedItemCssClass="AutoExtenderHighlight">
                        </cc1:AutoCompleteExtender>
                    </td>
                    <td style="padding-left: 5px; display:none;">
                        <asp:DropDownList ID="ddlStatus" runat="server">
                            <asp:ListItem Text="All" Value="-1" />
                            <asp:ListItem Value="0">Pending</asp:ListItem>
                            <asp:ListItem Value="1">Recommended</asp:ListItem>
                            <asp:ListItem Value="2" Selected="True">Approved</asp:ListItem>
                            <asp:ListItem Value="3">Rejected</asp:ListItem>
                            <asp:ListItem Value="4">Forwarded/Posted</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td>
                    </td>
                    
                    <td valign="top">
                        <asp:Button ID="btnLoad" CssClass="btn btn-default btn-sm" OnClientClick="valGroup='AEEmployee';return CheckValidation();"
                            OnClick="btnLoad_Click" runat="server" Width="80px" Text="Load" />
                    </td>
                    <td>
                        <asp:LinkButton ID="btnOvertimRequest" OnClientClick="assignOvertime(); return false;"
                            CssClass="save" Style="height: 16px; margin-right: 10px; width: 120px" runat="server"
                            Text="Assign Overtime" />
                    </td>
                </tr>
            </table>
            <%--</ContentTemplate>
            </asp:UpdatePanel>--%>
        </div>
        <uc2:MsgCtl ID="divMsgCtl" EnableViewState="false" Hide="true" Width="390px" runat="server" />
        <uc2:WarningCtl ID="divWarningMsg" EnableViewState="false" Hide="true" runat="server" />
        <div class="clear">

            <cc1:TabContainer ID="tab" runat="server" AutoPostBack="true" OnActiveTabChanged="btnTab_click"
                    CssClass="yui" ActiveTabIndex="0">
                    <cc1:TabPanel runat="server" ID="tabAll">
                        <HeaderTemplate>
                            <asp:Label runat="server" ID="lblAll" Text="All" />
                        </HeaderTemplate>
                    </cc1:TabPanel>
                    <cc1:TabPanel runat="server" ID="tabPending">
                        <HeaderTemplate>
                            <asp:Label runat="server" ID="lblPending" Text="Pending" />
                        </HeaderTemplate>
                    </cc1:TabPanel>
                    <cc1:TabPanel runat="server" ID="tabRecommended">
                        <HeaderTemplate>
                            <asp:Label runat="server" ID="lblRecommended" Text="Recommended" />
                        </HeaderTemplate>
                    </cc1:TabPanel>
                    <cc1:TabPanel runat="server" ID="tabApproved">
                        <HeaderTemplate>
                            <asp:Label runat="server" ID="Approved" Text="Approved" />
                        </HeaderTemplate>
                    </cc1:TabPanel>
                    <cc1:TabPanel runat="server" ID="tabRejected">
                        <HeaderTemplate>
                            <asp:Label runat="server" ID="Rejected" Text="Rejected" />
                        </HeaderTemplate>
                    </cc1:TabPanel>
                    <cc1:TabPanel runat="server" ID="tabForwardedPosted">
                        <HeaderTemplate>
                            <asp:Label runat="server" ID="lblForwardedPosted" Text="Forwarded/Posted" />
                        </HeaderTemplate>
                    </cc1:TabPanel>
                </cc1:TabContainer>

            <cc2:EmptyDisplayGridView CssClass="table table-primary mb30 table-bordered table-hover"
                UseAccessibleHeader="true" ShowHeaderWhenEmpty="True" ID="gvw" runat="server"
                DataKeyNames="RequestID" AutoGenerateColumns="False" CellPadding="4" GridLines="None"
                ShowFooterWhenEmpty="False" OnRowCreated="gvwEmployees_RowCreated">
                <Columns>
                    <asp:TemplateField HeaderStyle-Width="40px" HeaderText="Select" ItemStyle-HorizontalAlign="Center"
                        HeaderStyle-HorizontalAlign="Center">
                        <HeaderTemplate>
                            <asp:CheckBox ID="chk12Delete1" onclick="selectDeselectAll(this)" runat="server" />
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:CheckBox Visible='<%# Eval("Status").ToString() == "2" ? true : false %>' ID="chkDelete"
                                runat="server" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="OvertimeType" HeaderStyle-Width="130px" HeaderStyle-HorizontalAlign="Left"
                        HeaderText="Type"></asp:BoundField>
                    <asp:BoundField HeaderStyle-Width="30px" HeaderStyle-HorizontalAlign="Left" HeaderText="EIN"
                        DataField="EmployeeID"></asp:BoundField>
                    <asp:BoundField DataField="EmployeeName" HeaderStyle-Width="160px" HeaderStyle-HorizontalAlign="Left"
                        HeaderText="Employee"></asp:BoundField>
                    <asp:BoundField DataField="Date" DataFormatString="{0:yyyy/MM/dd}" HeaderStyle-Width="90px"
                        HeaderStyle-HorizontalAlign="Left" HeaderText="Date"></asp:BoundField>
                    <asp:BoundField DataField="NepDate" HeaderStyle-Width="90px" HeaderStyle-HorizontalAlign="Left"
                        HeaderText="Nep Date"></asp:BoundField>
                    <asp:BoundField DataField="StartTime" HeaderStyle-Width="60px" HeaderStyle-HorizontalAlign="Left"
                        HeaderText="Start"></asp:BoundField>
                    <asp:BoundField DataField="EndTime" HeaderStyle-Width="60px" HeaderStyle-HorizontalAlign="Left"
                        HeaderText="End"></asp:BoundField>
                    <asp:BoundField DataField="CheckInTime" HeaderStyle-Width="90px" HeaderStyle-HorizontalAlign="Left"
                        HeaderText="In Time"></asp:BoundField>
                    <asp:BoundField DataField="CheckOutTime" HeaderStyle-Width="90px" HeaderStyle-HorizontalAlign="Left"
                        HeaderText="Out Time"></asp:BoundField>
                    <%-- <asp:BoundField DataField="Position" HeaderText="Position"></asp:BoundField>--%>
                    <asp:BoundField DataField="DurationModified" HeaderStyle-Width="100px" HeaderStyle-HorizontalAlign="Left"
                        HeaderText="Requested Hour"></asp:BoundField>
                    <asp:BoundField DataField="ApprovedTime" HeaderStyle-Width="100px" HeaderStyle-HorizontalAlign="Left"
                        HeaderText="Approved Hour"></asp:BoundField>
                    <asp:BoundField DataField="Reason" HeaderStyle-Width="250px" HeaderStyle-HorizontalAlign="Left"
                        HeaderText="Reason"></asp:BoundField>
                    <asp:BoundField DataField="SupervisorName" HeaderStyle-Width="150px" HeaderStyle-HorizontalAlign="Left"
                        HeaderText="Proccessed By"></asp:BoundField>
                    <asp:BoundField DataField="StatusModified" HeaderStyle-Width="100px" HeaderStyle-HorizontalAlign="Left"
                        HeaderText="Status"></asp:BoundField>
                    <asp:TemplateField HeaderStyle-Width="20px" HeaderStyle-HorizontalAlign="Left" HeaderText="">
                        <ItemTemplate>
                            <asp:HyperLink NavigateUrl="javascript:void(0)" value='<%# Eval("RequestID") %>'
                                onclick='<%# "addSkillSetToEmployeePopup(this);" %>' runat="server" Text="Edit"
                                ID="btnAction">
                            </asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <%-- <%# "<a  href='#'  onclick='addSkillSetToEmployeePopup(" + Eval("RequestID") + ")'>Action</a>"%>--%>
                <RowStyle CssClass="odd" />
                <AlternatingRowStyle CssClass="even" />
                <EmptyDataTemplate>
                    <b>No list. </b>
                </EmptyDataTemplate>
            </cc2:EmptyDisplayGridView>
            <uc1:PagingCtl ID="pagingCtl" ShowDropDown="true" OnNextRecord="btnNext_Click" OnChangePage="ChangePageNumber"
                OnPrevRecord="btnPrevious_Click" OnDropDownShowRecordsChanged="ddlRecords_SelectedIndexChanged"
                runat="server" />
        </div>
        <div class="buttonsDiv">
            <asp:Button ID="btnDelete" CssClass="btn btn-success btn-sm" OnClick="btnDelete_Click"
                Visible="true" Width="100" CausesValidation="false" runat="server" OnClientClick="return confirm('Do you want to forward the selected requests?');"
                Text="Forward" />
            <asp:Button ID="btnReject" ToolTip="Reject Approved List" CssClass="btn btn-warning btn-sm"
                OnClick="btnRejected_Click" Visible="true" Width="100" CausesValidation="false"
                runat="server" OnClientClick="return confirm('Do you want to reject the selected requests?');"
                Text="Reject" />
            <asp:Button ID="btnExport" CssClass="btn btn-info btn-sm" OnClick="btnExport_Click"
                Visible="true" Width="100px" CausesValidation="false" runat="server" Text="Export" />
        </div>
    </div>
</asp:Content>
