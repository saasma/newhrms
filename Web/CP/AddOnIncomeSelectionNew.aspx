﻿<%@ Page Title="Add-On Income Selection" Language="C#" MasterPageFile="~/Master/ForPopupPage.Master"
    AutoEventWireup="true" CodeBehind="AddOnIncomeSelectionNew.aspx.cs" Inherits="Web.CP.AddOnIncomeSelectionNew" %>

<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="InfoMsgCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarMsgCtl" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .subheader
        {
            font-size: 14px;
            margin-bottom: 5px;
            color: Gray;
        }
        .selected
        {
        }
    </style>
    <script type="text/javascript">




        function popupCreateNewCall() {
            var ret = popupCreateNew();
            if (typeof (ret) != 'undefined') {
                if (ret == "ReloadIncome") {
                    __doPostBack('Reload', '');
                }
            }
        }

        function popupUpdateIncomeCall(incomeId) {
            var ret = popupIncome('IId=' + incomeId);


            return false;
        }

        function parentIncomeCallbackFunction(text, closingWindow) {
            closingWindow.close();

        }
        function doubleClickHander(lst) {
            $('#<%= btnOk.ClientID %>').click();
        }

      
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
    <div class="popupHeader">
        <h2 class="headlinespop" runat="server" id="header">
            Select Add-On Income for the period : {0}
        </h2>
    </div>
    <div style="padding-left: 20px">
        <uc2:InfoMsgCtl runat="server" Hide="true" Width="600px" EnableViewState="false"
            ID="msgInfo" />
        <uc2:WarMsgCtl runat="server" Hide="true" Width="600px" EnableViewState="false" ID="warMsgCtl" />
    </div>
    <div style="padding-left:20px;padding-top:5px;color:#F49D25;">
        Add-on Type
        <br />
        1. <b>Add to Regular Salary </b> : Income amount can be added in both Add On and Salary
        sheet
        <br />
        2. <b>Zeroize Selected Employee only </b>: Salary amount or Add from beginning tax amount
        will be 0 for those employee added in add-on
        <br />
        3. <b>Zeroize All Employee Salary </b>: Salary amount or Add from beginning tax amount
        will be 0 for all employees, for this option any employee should have non zero value</div>
    <div class="bevel marginal" style="margin-top: 10px">
        <div class="fields paddpop" style="width: 950px">
            <table>
                <tr>
                    <td>
                        <strong>Add-On</strong>
                        <asp:DropDownList ID="ddlAddOnName" AutoPostBack="true" runat="server" Width="200px"
                            DataTextField="Name" OnSelectedIndexChanged="ddlAddOnName_Changed" DataValueField="AddOnId" />
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <div class="subheader">
                            Incomes</div>
                        <asp:ListBox ondblclick="doubleClickHander(this)" ID="lstIncomeList" SelectionMode="Multiple"
                            DataTextField="Title" DataValueField="TypeSourceId" runat="server" Height="250px"
                            Width="250px"></asp:ListBox>
                        <div class="subheader">
                            Fixed Incomes</div>
                        <asp:ListBox ondblclick="doubleClickHander(this)" ID="lstFixedIncomeList" SelectionMode="Multiple"
                            DataTextField="Title" DataValueField="TypeSourceId" runat="server" Height="100px"
                            Width="250px"></asp:ListBox>
                        <div class="subheader">
                            Deductions</div>
                        <asp:ListBox ondblclick="doubleClickHander(this)" ID="lstDeductionList" SelectionMode="Multiple"
                            DataTextField="Title" DataValueField="DeductionId" runat="server" Height="100px"
                            Width="250px"></asp:ListBox>
                        <div>
                            <asp:Button ID="btnOk" runat="server" Text="Add" CssClass="update" ValidationGroup="AddIncome"
                                OnClientClick="valGroup='AddIncome';return CheckValidation()" OnClick="btnOk_Click" /></div>
                    </td>
                    <td style="width: 20px">
                    </td>
                    <td valign="top">
                        <div class="subheader">
                            Selected Incomes</div>
                        <asp:GridView Style="clear: both" PagerStyle-HorizontalAlign="Center" PagerStyle-CssClass="defaultPagingBar"
                            CssClass="tableLightColor" AutoGenerateColumns="false" UseAccessibleHeader="true"
                            OnRowCommand="gvwEmployees_RowCommand" ID="gvwSelectedIncomesIncomes" OnRowCreated="gvwEmployees_RowCreated"
                            runat="server" CellPadding="3" DataKeyNames="AddOnHeaderId,TypeSourceId,Type,SourceId,HeaderName"
                            rateColumns="False" OnRowDataBound="gvwSelectedIncomesIncomes_RowDataBound">
                            <Columns>
                                <asp:TemplateField HeaderText="Income" HeaderStyle-Width="200px" HeaderStyle-HorizontalAlign="Left"
                                    ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <%# Eval("HeaderName")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Income Type" HeaderStyle-Width="150px" HeaderStyle-HorizontalAlign="Left"
                                    ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <%#  new DAL.IncomeCalculation().Get(Eval("Calculation").ToString()) %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Income Setting" HeaderStyle-Width="170px" HeaderStyle-HorizontalAlign="Left"
                                    ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:DropDownList ID="ddlSetting" runat="server">
                                            <asp:ListItem Text="Your Own Amount" Value="false" />
                                            <asp:ListItem Text="Use Income Setting" Value="true" />
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Add-on Type" HeaderStyle-Width="100px" HeaderStyle-HorizontalAlign="Left"
                                    ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:DropDownList ID="ddlAddOnType" runat="server">
                                            <asp:ListItem Text="Add To Regular Salary" Value="true" />
                                            <asp:ListItem Text="Zeroize Selected Employee only" Value="false" Selected="True" />
                                            <asp:ListItem Text="Zeroize All Employee Salary" Value="" />
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Clear" HeaderStyle-Width="200px" HeaderStyle-HorizontalAlign="Left"
                                    ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:LinkButton CommandArgument='<%# Eval("TypeSourceId") %>' runat="server" OnClientClick='return confirm("Confirm clear values?");'
                                            Width="120px" Text="Clear Values" CommandName="Clear" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderStyle-Width="30px" HeaderText="Edit" HeaderStyle-HorizontalAlign="Center"
                                    ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="btnEdit" Visible='<%# Convert.ToInt32( Eval("Type")) == 1 %>'
                                            ImageUrl="~/images/edit.gif" runat="server" OnClientClick='<%# "return popupUpdateIncomeCall(" +  Eval("SourceId") + ");" %>'
                                            AlternateText="Edit" />
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderStyle-Width="30px" HeaderText="Delete" HeaderStyle-HorizontalAlign="Center"
                                    ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="btnDelete" CommandName="Remove" CommandArgument='<%# Eval("TypeSourceId") %>'
                                            ImageUrl="~/images/delet.png" runat="server" OnClientClick='return confirm("Are you sure, you want to remove the income, if income has amounts then it will also be removed?")'
                                            AlternateText="Delete" />
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                        <div class="subheader" style="margin-top: 15px;">
                            Selected Deductions</div>
                        <asp:GridView Style="clear: both" PagerStyle-HorizontalAlign="Center" PagerStyle-CssClass="defaultPagingBar"
                            CssClass="tableLightColor" AutoGenerateColumns="false" UseAccessibleHeader="true"
                            OnRowCommand="gvwDeductions_RowCommand" ID="gvwDeductions" runat="server" CellPadding="3"
                            DataKeyNames="AddOnHeaderId,TypeSourceId,Type,SourceId,HeaderName" rateColumns="False">
                            <Columns>
                                <asp:TemplateField HeaderText="Deduction" HeaderStyle-Width="200px" HeaderStyle-HorizontalAlign="Left"
                                    ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <%# Eval("HeaderName")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Deduction Type" HeaderStyle-Width="150px" HeaderStyle-HorizontalAlign="Left"
                                    ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <%#  new DAL.DeductionCalculation().Get(Eval("Calculation").ToString()) %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderStyle-Width="30px" HeaderText="Delete" HeaderStyle-HorizontalAlign="Center"
                                    ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="btnDelete" CommandName="Remove" CommandArgument='<%# Eval("TypeSourceId") %>'
                                            ImageUrl="~/images/delet.png" runat="server" OnClientClick='return confirm("Are you sure, you want to remove the deduction, if deduction has amounts then it will also be removed?")'
                                            AlternateText="Delete" />
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                        <asp:Button ID="btnSaveIncomes" Width="120px" Style="padding-top: 0px;" runat="server"
                            Text="Save Incomes" CssClass="save" OnClientClick="return confirm('Confirm save the settings?');" OnClick="btnSaveIncomes_Click" />
                        <asp:Button ID="btnGenerateAmount" Width="120px" Style="padding-top: 0px;" runat="server"
                            Text="Generate Amount" CssClass="save" OnClick="btnGenerateAmount_Click" />
                    </td>
                    <td valign="top" style='padding-left: 10px'>
                        <div class="subheader">
                            &nbsp;</div>
                        <br />
                        <asp:Button Style="display: none" ID="btnCancel" runat="server" CssClass="cancel"
                            Text="Cancel" OnClientClick="closePopup();return false;" />
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
