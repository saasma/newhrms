﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/ForPopupPage.Master" AutoEventWireup="true"
    CodeBehind="AEIncomeOnly.aspx.cs" Inherits="Web.CP.AEIncomeOnly" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript">
        function closePopup() {
            window.returnValue = "Reload";
            window.close();
        }

        function closePopupFromPay() {
            window.returnValue = "ReloadIncome";
            window.close();
        }         
        
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
<div class="popupHeader">
</div>   <table>
    <tr>
    <td class="lf">Title</td>
    <td><asp:TextBox ID="txtTitle" runat="server"></asp:TextBox>
    <asp:RequiredFieldValidator ID="valReqdTitle" runat="server" ControlToValidate="txtTitle"
        Display="None" ErrorMessage="Income title is required." ValidationGroup="AEIncome"></asp:RequiredFieldValidator></td>
    </tr>    
    <tr>
    <td class="lf">Abbreviation</td>   
    <td> <asp:TextBox ID="txtAbbreviation" runat="server"></asp:TextBox>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtAbbreviation"
        Display="None" ErrorMessage="Income abbreviation is required." ValidationGroup="AEIncome"></asp:RequiredFieldValidator>
    </td>
    </tr>
    </table>
    <asp:Button ID="btnOk" runat="server" OnClientClick="valGroup='AEIncome';return CheckValidation()"
        OnClick="btnOk_Click" Text="Ok" ValidationGroup="AEIncome" />
    <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClientClick="window.close()" />
</asp:Content>
