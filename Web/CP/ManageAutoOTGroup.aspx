<%@ Page Title="OT Groups" Language="C#" MasterPageFile="~/Master/HROld.Master" AutoEventWireup="true"
    CodeBehind="ManageAutoOTGroup.aspx.cs" Inherits="Web.CP.ManageAutoOTGroup" %>

<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .table1 td, .table1 th
        {
            line-height: 0 !important;
        }
    </style>
    <script type="text/javascript">

        function insertUpdateGratuityClassCall(classId) {
            var ret = insertUpdateGratuityClass('Id=' + classId);

            if (typeof (ret) != 'undefined') {
                if (ret == 'Reload') {
                    __doPostBack('Reload', '');
                }
            }
        }
        function insertUpdateGratuityRuleCall(gratuityRuleId) {
            var ret = insertUpdateGratuityRule('Id=' + gratuityRuleId);

            if (typeof (ret) != 'undefined') {
                if (ret == 'Reload') {
                    __doPostBack('Reload', '');
                }
            }
        }


        function refresh(popupWindow) {
            popupWindow.close();
            __doPostBack('Reload', '');
        }

        function changeStates(chk) {
            var isChecked = $(chkAll).is(':checked');

            $("INPUT[type='checkbox']").attr('checked', isChecked);

        }


    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Auto OT Groups
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <div class="contentArea">
            <asp:LinkButton ID="Button1" class="btn btn-success btn-sm btn-sect" runat="server"
                Text="Add OT Group" Style="height: 30px; width: 100px; margin-bottom: 5px; margin-left: 20px;"
                OnClientClick="insertUpdateGratuityRuleCall();return false;" />
            <uc2:MsgCtl ID="divMsgCtl" EnableViewState="false" Hide="true" runat="server" />
            <uc2:WarningCtl ID="divWarningMsg" EnableViewState="false" Hide="true" runat="server" />
            <br />
            <div>
                <!-- panel-heading -->
                <div class="panel-body">
                    <cc2:EmptyDisplayGridView CssClass="table table-primary mb30 table-bordered table-hover table1"
                        UseAccessibleHeader="true" ID="gvwGratuityRules" ShowHeaderWhenEmpty="True" runat="server"
                        AutoGenerateColumns="False" GridLines="None" DataKeyNames="OTGroupId" OnRowDeleting="gvwClass_RowDeleting"
                        ShowFooterWhenEmpty="True">
                        <Columns>
                            <asp:TemplateField HeaderText="SN" HeaderStyle-Width="50px" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <%# Container.DataItemIndex + 1 %>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Name" HeaderStyle-Width="120px" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <%# Eval("Name")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="OT Allowed" HeaderStyle-Width="100px" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <%# Convert.ToBoolean( Eval("IsOTAllowed")) ? "Yes" : "No"%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Grace Minute" HeaderStyle-Width="100px" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <%# Eval("GraceMinuteToBeAddedInStandardHour") + " ( " + TimeSpan.FromMinutes(Convert.ToDouble(Eval("GraceMinuteToBeAddedInStandardHour"))).TotalHours + " hrs)"%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Max OT Minute In a Day" HeaderStyle-Width="200px"
                                ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <%# Eval("MaxOTMinuteInADay") + " ( " + TimeSpan.FromMinutes(Convert.ToDouble(Eval("MaxOTMinuteInADay"))).TotalHours + " hrs)"%>
                                </ItemTemplate>
                            </asp:TemplateField>
                             <asp:TemplateField HeaderText="Max OT Minute In Holiday" HeaderStyle-Width="200px"
                                ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <%# Eval("MaxOTMinuteInHoliday") + " ( " + TimeSpan.FromMinutes(Convert.ToDouble(Eval("MaxOTMinuteInHoliday"))).TotalHours + " hrs)"%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Standard Work Min for OT" HeaderStyle-Width="200px"
                                ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <%# Eval("StandardWorkMinOverridingFromShiftForOT") + " ( " + TimeSpan.FromMinutes( Convert.ToDouble( Eval("StandardWorkMinOverridingFromShiftForOT"))).TotalHours + " hrs)" %>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Level/Position List" HeaderStyle-Width="300px" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:Label Width="400px" runat="server" Text='<%# Eval("LevelList")%>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="50px">
                                <ItemTemplate>
                                    <input type='image' style='border-width: 0px;' onclick='return insertUpdateGratuityRuleCall(<%# Eval("OTGroupId") %>);'
                                        src='../images/edit.gif' />
                                    &nbsp;
                                    <asp:ImageButton ID="ImageButton2" OnClientClick="return confirm('Confirm delete the group?')"
                                        runat="server" CommandName="Delete" ImageUrl="~/images/delet.png" />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            </asp:TemplateField>
                        </Columns>
                        <RowStyle CssClass="odd" />
                        <AlternatingRowStyle CssClass="even" />
                        <EmptyDataTemplate>
                            <b>No group defined.</b>
                        </EmptyDataTemplate>
                    </cc2:EmptyDisplayGridView>
                    <br />
                </div>
            </div>
        </div>
    </div>
</asp:Content>
