﻿<%@ Page Title="Overtime Type" Language="C#" MasterPageFile="~/Master/NewDetails.Master"
    AutoEventWireup="true" CodeBehind="OvertimeType.aspx.cs" Inherits="Web.CP.OvertimeType" %>

<%--<%@ Register Src="~/NewHR/UserControls/EmployeeDetailsCtl.ascx" TagName="EmployeeDetailsCtl"
    TagPrefix="uc1" %>--%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">

        var CommandHandler1 = function(command, record){
            <%= hiddenValue.ClientID %>.setValue(record.data.OvertimeTypeId);
                if(command=="Edit")
                {
                    <%= btnEditLevel.ClientID %>.fireEvent('click');
                }
                else if(command=="Detail")
                {
                     <%= btnDetailLevel.ClientID %>.fireEvent('click');
                }
                else
                {
                
                    <%= btnDeleteLevel.ClientID %>.fireEvent('click');
                }

             }
             
            function typeChange(value)
            {
                //based on
                if(value == "1")
                {
                    document.getElementById("ctl00_ContentPlaceHolder_Main_rowPercent1").style.display = "block";
                    document.getElementById("ctl00_ContentPlaceHolder_Main_rowPercent2").style.visibility = "visible";

                    document.getElementById("ctl00_ContentPlaceHolder_Main_rowFixed").style.display = "none";
                }
                else
                {
                    document.getElementById("ctl00_ContentPlaceHolder_Main_rowPercent1").style.display = "none";
                    document.getElementById("ctl00_ContentPlaceHolder_Main_rowPercent2").style.visibility = "hidden";

                    document.getElementById("ctl00_ContentPlaceHolder_Main_rowFixed").style.display = "block";
                }
            }
             
    </script>
    <style type="text/css">
        .x-tree-icon
        {
            display: none !important;
        }
        
        .x-tree-arrows .x-grid-tree-node-expanded .x-tree-expander
        {
            display: none !important;
        }
        
        .hideLeftBlockCssInPage
        {
            margin: 0px !important;
            padding-left: 20px !important;
        }
        #menu
        {
            display: none;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Overtime Types
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <ext:Hidden runat="server" ID="Hidden_CopyValue" />
        <ext:Hidden runat="server" ID="hiddenValue" />
        <ext:Button Cls="btn btn-primary" runat="server" Hidden="true" ID="btnEditLevel"
            runat="server">
            <DirectEvents>
                <Click OnEvent="btnEditLevel_Click">
                    <EventMask ShowMask="true" />
                </Click>
            </DirectEvents>
        </ext:Button>
        <ext:Button Cls="btn btn-primary" runat="server" Hidden="true" ID="btnDetailLevel"
            runat="server">
            <DirectEvents>
                <Click OnEvent="btnDetailLevel_Click">
                    <EventMask ShowMask="true" />
                </Click>
            </DirectEvents>
        </ext:Button>
        <ext:Button Cls="btn btn-primary" ID="btnDeleteLevel" runat="server" Hidden="true">
            <DirectEvents>
                <Click OnEvent="btnDeleteLevel_Click">
                    <Confirmation ConfirmRequest="true" Message="Are you sure, you want to delete the Overtime type?" />
                    <EventMask ShowMask="true" />
                </Click>
            </DirectEvents>
        </ext:Button>
        <div class="separator bottom">
        </div>
        <div class="innerLR">
            <%--<uc1:EmployeeDetailsCtl Id="EmployeeDetailsCtl1" runat="server" />--%>
            <div class="widget">
                <div class="widget-body">
                    <ext:GridPanel StyleSpec="margin-top:15px;" ID="GridLevels" runat="server" Cls="itemgrid">
                        <Store>
                            <ext:Store ID="Store3" runat="server">
                                <Model>
                                    <ext:Model ID="Model4" runat="server" IDProperty="OvertimeTypeId">
                                        <Fields>
                                            <ext:ModelField Name="OvertimeTypeId" Type="string" />
                                            <ext:ModelField Name="Name" Type="string" />
                                            <ext:ModelField Name="Income" Type="String" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:Column ID="Column5" Sortable="false" MenuDisabled="true" runat="server" Text="Overtime Type"
                                    Width="300" Align="Left" DataIndex="Name" />
                                <ext:Column ID="Column2" Sortable="false" MenuDisabled="true" runat="server" Text="Income"
                                    Width="120" Align="Left" DataIndex="Income" />
                                <ext:CommandColumn runat="server" Text="Actions" Align="Center" Width="125">
                                    <Commands>
                                        <ext:GridCommand Text="<i></i>" Icon="pencil" CommandName="Edit" />
                                        <ext:GridCommand Text="<i></i>" Icon="Cancel" CommandName="Delete" />
                                        <ext:GridCommand ToolTip-Text="Level wise rate" Text="<i></i>" Icon="MoneyAdd" CommandName="Detail" />
                                    </Commands>
                                    <Listeners>
                                        <Command Handler="CommandHandler1(command,record);" />
                                    </Listeners>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <SelectionModel>
                            <ext:RowSelectionModel ID="RowSelectionModel2" runat="server" Mode="Simple" />
                        </SelectionModel>
                    </ext:GridPanel>
                    <div class="buttonBlock">
                        <%-- <ext:LinkButton runat="server" StyleSpec="padding:0px;" ID="btnAddLevel" Cls="btnFlat"
                        BaseCls="btnFlat" Text="<i></i>Add New Allowance" runat="server">
                        <DirectEvents>
                            <Click OnEvent="btnAddLevel_Click">
                                <EventMask ShowMask="true" />
                            </Click>
                        </DirectEvents>
                    </ext:LinkButton>--%>
                    </div>
                </div>
            </div>
            <div class="buttonBlock">
                <ext:Button Cls="btn btn-primary" runat="server" ID="btnAddNewLine" Text="<i></i>Add Overtime Type" OnDirectClick="btnAddNewLine_click">
                    <Listeners>
                        <Click Handler="#{hiddenValue}.setValue('');#{windowAddEditOvertime}.show();" />
                    </Listeners>
                </ext:Button>
            </div>
            <ext:Window ID="Window1" AutoScroll="true" runat="server" Title="Overtime Rate" Icon="Application"
                Width="600" Height="575" BodyPadding="5" Hidden="true" Modal="true">
                <Content>
                    <table class="fieldTable">
                        <tr>
                            <td align="right" colspan="5">
                                <div>
                                    <ext:Button Cls="btn btn-primary" ID="btnCopy" runat="server" Text="Copy" Icon="ArrowIn"
                                        IconAlign="Left">
                                        <DirectEvents>
                                            <Click OnEvent="btnCopy_Click">
                                                <EventMask ShowMask="true" />
                                                <ExtraParams>
                                                    <ext:Parameter Name="CopyValue" Value="Ext.encode(#{GridPanel1}.getRowsValues({selectedOnly : true}))"
                                                        Mode="Raw" />
                                                </ExtraParams>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <ext:GridPanel StyleSpec="margin-top:15px;" ID="GridPanel1" runat="server" Cls="itemgrid"
                                    Width="525" Height="400" ClicksToEdit="1">
                                    <Store>
                                        <ext:Store ID="storeAllowanceDetail" runat="server" AutoLoad="true">
                                            <Model>
                                                <ext:Model ID="Model1" runat="server" IDProperty="LevelId">
                                                    <Fields>
                                                        <ext:ModelField Name="OvertimeTypeId" Type="string" />
                                                        <ext:ModelField Name="LevelId" Type="string" />
                                                        <ext:ModelField Name="LevelName" Type="string" />
                                                        <ext:ModelField Name="CalculationType" Type="string" />
                                                        <ext:ModelField Name="Rate" Type="Float" />
                                                    </Fields>
                                                </ext:Model>
                                            </Model>
                                        </ext:Store>
                                    </Store>
                                    <Plugins>
                                        <ext:CellEditing ID="CellEditing1" ClicksToEdit="1" runat="server">
                                            <Listeners>
                                            </Listeners>
                                        </ext:CellEditing>
                                    </Plugins>
                                    <ColumnModel>
                                        <Columns>
                                            <ext:Column ID="Column1" TdCls="wrap" runat="server" Text="Level" Sortable="false"
                                                MenuDisabled="true" Width="175" DataIndex="LevelName" Border="false">
                                            </ext:Column>
                                            <ext:Column ID="Column_Score" TdCls="wrap" runat="server" Text="Calculation Method"
                                                Sortable="false" MenuDisabled="true" Width="200" DataIndex="CalculationType"
                                                Border="false">
                                                <Renderer Handler="if(value=='1') return 'Based on Salary'; else if(value=='2') return 'Fixed Rate';  else if(value=='3') return 'Based on Monthly Hour'; else if(value=='4') return 'Fixed Amount'; else '';" />
                                                <Editor>
                                                    <ext:ComboBox ID="cmb" runat="server">
                                                        <Items>
                                                            <ext:ListItem Text="Based on Salary" Value="1" />
                                                            <ext:ListItem Text="Fixed Rate" Value="2" />
                                                            <ext:ListItem Text="Based on Monthly Hour" Value="3" />
                                                            <ext:ListItem Text="Fixed Amount" Value="4" />
                                                        </Items>
                                                    </ext:ComboBox>
                                                </Editor>
                                            </ext:Column>
                                            <ext:Column ID="Column_Label" TdCls="wrap" runat="server" Text="Rate" Sortable="false"
                                                MenuDisabled="true" Width="120" DataIndex="Rate" Border="false">
                                                <Editor>
                                                    <ext:TextField ID="txtRate" SelectOnFocus="true" runat="server" AllowBlank="false" />
                                                </Editor>
                                            </ext:Column>
                                        </Columns>
                                    </ColumnModel>
                                    <SelectionModel>
                                        <ext:CellSelectionModel ID="CellSelectionModel1" runat="server" Mode="Single" />
                                    </SelectionModel>
                                </ext:GridPanel>
                            </td>
                        </tr>
                        <tr>
                            <td valign="bottom">
                                <div class="popupButtonDiv">
                                    <ext:Button Cls="btn btn-primary" runat="server" ID="LinkButton2" Text="<i></i>Save">
                                        <DirectEvents>
                                            <Click OnEvent="btnDetailSaveUpdate_Click">
                                                <EventMask ShowMask="true" />
                                                <ExtraParams>
                                                    <ext:Parameter Name="Values" Value="Ext.encode(#{GridPanel1}.getRowsValues({selectedOnly : false}))"
                                                        Mode="Raw" />
                                                </ExtraParams>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                    <div class="btnFlatOr">
                                        or</div>
                                    <ext:LinkButton runat="server" StyleSpec="padding:0px;" Cls="btnFlatLeftGap" ID="LinkButton3"
                                        Text="<i></i>Cancel" runat="server">
                                        <Listeners>
                                            <Click Handler="#{Window1}.hide();">
                                            </Click>
                                        </Listeners>
                                    </ext:LinkButton>
                                </div>
                            </td>
                        </tr>
                    </table>
                </Content>
            </ext:Window>
            <ext:Window ID="WindowCopy" AutoScroll="true" runat="server" Title="Overtime Rate"
                Icon="Application" Width="400" Height="575" BodyPadding="5" Hidden="true" Modal="true">
                <Content>
                    <table class="fieldTable">
                        <tr>
                            <td>
                                <ext:DisplayField ID="txtValueToBeCopied" runat="server" LabelAlign="Left" FieldLabel="Value to be copied"
                                    LabelSeparator=" " LabelWidth="125" Width="150">
                                </ext:DisplayField>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <ext:GridPanel StyleSpec="margin-top:15px;" ID="gridPanelCopyList" runat="server"
                                    Cls="itemgrid" Width="350" Height="400" ClicksToEdit="1">
                                    <Store>
                                        <ext:Store ID="storeCopyList" runat="server" AutoLoad="true">
                                            <Model>
                                                <ext:Model ID="Model2" runat="server">
                                                    <Fields>
                                                        <ext:ModelField Name="LevelId" Type="string" />
                                                        <ext:ModelField Name="LevelName" Type="string" />
                                                    </Fields>
                                                </ext:Model>
                                            </Model>
                                        </ext:Store>
                                    </Store>
                                    <ColumnModel>
                                        <Columns>
                                            <ext:Column ID="Column3" TdCls="wrap" runat="server" Text="Level" Sortable="false"
                                                MenuDisabled="true" Width="320" DataIndex="LevelName" Border="false">
                                            </ext:Column>
                                        </Columns>
                                    </ColumnModel>
                                    <SelectionModel>
                                        <ext:CheckboxSelectionModel runat="server" Mode="Single">
                                        </ext:CheckboxSelectionModel>
                                    </SelectionModel>
                                </ext:GridPanel>
                            </td>
                        </tr>
                        <tr>
                            <td valign="bottom">
                                <div class="popupButtonDiv">
                                    <ext:Button Cls="btn btn-primary" runat="server" ID="LinkButton1" Text="<i></i>Save">
                                        <DirectEvents>
                                            <Click OnEvent="btnCopyTo_Click">
                                                <EventMask ShowMask="true" />
                                                <ExtraParams>
                                                    <ext:Parameter Name="SelectedLevels" Value="Ext.encode(#{gridPanelCopyList}.getRowsValues({selectedOnly : true}))"
                                                        Mode="Raw" />
                                                </ExtraParams>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                    <div class="btnFlatOr">
                                        or</div>
                                    <ext:LinkButton runat="server" StyleSpec="padding:0px;" Cls="btnFlatLeftGap" ID="LinkButton4"
                                        Text="<i></i>Cancel" runat="server">
                                        <Listeners>
                                            <Click Handler="#{WindowCopy}.hide();">
                                            </Click>
                                        </Listeners>
                                    </ext:LinkButton>
                                </div>
                            </td>
                        </tr>
                    </table>
                </Content>
            </ext:Window>
            <ext:Window ID="windowAddEditOvertime" AutoScroll="true" runat="server" Title="Overtime Details"
                Icon="Application" Width="700" Height="600" BodyPadding="5" Hidden="true" Modal="true">
                <Content>
                    <table class="fieldTable">
                        <tr>
                            <td colspan="2">
                                <ext:TextField ID="txtName" runat="server" FieldLabel="Name *" LabelAlign="top" LabelSeparator=""
                                    Width="200" />
                                <asp:RequiredFieldValidator Display="None" ID="valtxtPublicationName" runat="server"
                                    ValidationGroup="SaveUpdate" ControlToValidate="txtName" ErrorMessage="Overtime Name is required." />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" valign="top">
                                <ext:Store ID="storeIncomes" runat="server">
                                    <Model>
                                        <ext:Model ID="modelLevel" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="IncomeId" Type="String" />
                                                <ext:ModelField Name="Title" Type="String" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                                <ext:ComboBox Note="Select the income head where the Overtime will be shown" ID="cmbIncomes"
                                    LabelSeparator="" ForceSelection="true" LabelAlign="Top" runat="server" FieldLabel="Income *"
                                    Width="200" DisplayField="Title" ValueField="IncomeId" StoreID="storeIncomes"
                                    EmptyText="" QueryMode="Local">
                                    <Triggers>
                                        <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                                    </Triggers>
                                    <Listeners>
                                        <Select Handler="this.getTrigger(0).show();" />
                                        <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                        <TriggerClick Handler="if (index == 0) { 
                                           this.clearValue(); 
                                           this.getTrigger(0).hide();
                                       }" />
                                    </Listeners>
                                </ext:ComboBox>
                                <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator1" runat="server"
                                    ValidationGroup="SaveUpdate" ControlToValidate="cmbIncomes" ErrorMessage="Overtime income is required." />
                            </td>
                        </tr>
                        <%--<tr>
                        <td colspan="2">
                            <ext:ComboBox ID="cmbCalculationType" runat="server" ValueField="Value" DisplayField="Text"
                                Width="200" FieldLabel="Calculation" LabelAlign="top" LabelSeparator="" ForceSelection="true"
                                QueryMode="Local">
                                <Items>
                                    <ext:ListItem Text="Based on Salary" Value="1" />
                                    <ext:ListItem Text="Fixed Rate" Value="2" />
                                </Items>
                                <Listeners>
                                    <Select Handler="typeChange(this.getValue())" />
                                </Listeners>
                            </ext:ComboBox>
                            <asp:RequiredFieldValidator Display="None" ID="valcmbPublicationType" runat="server"
                                ValidationGroup="SaveUpdate" ControlToValidate="cmbCalculationType" ErrorMessage="Calculation is required." />
                        </td>
                    </tr>
                    <tr runat="server" id="rowFixed" style="display: none">
                        <td>
                            <ext:NumberField ID="txtFixedRate" runat="server" LabelSeparator="" Width="100" />
                        </td>
                        <td>
                            per hour
                        </td>
                    </tr>
                    <tr runat="server" id="rowPercent1" style="display: none">
                        <td colspan="1">
                            <ext:NumberField ID="txtPercent" runat="server" LabelSeparator="" Width="100" />
                        </td>
                        <td>
                            % of
                        </td>
                    </tr>--%>
                        <tr runat="server" id="rowPercent2" style="">
                            <td valign="top">
                                <strong>Select Incomes</strong>
                                <ext:GridPanel StyleSpec="margin-top:15px;" ID="gridIncome" runat="server" Cls="itemgrid"
                                    Width="350" Height="400" ClicksToEdit="1">
                                    <Store>
                                        <ext:Store ID="store1" runat="server" AutoLoad="true">
                                            <Model>
                                                <ext:Model ID="Model3" runat="server" IDProperty="IncomeId">
                                                    <Fields>
                                                        <ext:ModelField Name="Title" />
                                                        <ext:ModelField Name="IncomeId" />
                                                    </Fields>
                                                </ext:Model>
                                            </Model>
                                        </ext:Store>
                                    </Store>
                                    <ColumnModel>
                                        <Columns>
                                            <ext:Column ID="Column4" TdCls="wrap" runat="server" Text="Incomes" Sortable="false"
                                                MenuDisabled="true" Width="320" DataIndex="Title" Border="false">
                                            </ext:Column>
                                        </Columns>
                                    </ColumnModel>
                                    <SelectionModel>
                                        <ext:CheckboxSelectionModel ID="CheckboxSelectionModel1" runat="server" Mode="Simple" >
                                        </ext:CheckboxSelectionModel>
                                    </SelectionModel>
                                </ext:GridPanel>
                                <span style="color: gray">The salary selection is required only if the overtime is calculated
                                    as certain % of the Incomes</span>
                            </td>
                            <td valign="top">
                                <strong>Not Applicable To</strong>
                                <ext:GridPanel StyleSpec="margin-top:15px;" ID="gridNotApplicableTo" runat="server"
                                    Cls="itemgrid" Width="290" Height="400" ClicksToEdit="1">
                                    <Store>
                                        <ext:Store ID="store2" runat="server" AutoLoad="true">
                                            <Model>
                                                <ext:Model ID="Model5" runat="server" IDProperty="LevelId">
                                                    <Fields>
                                                        <ext:ModelField Name="Name" />
                                                        <ext:ModelField Name="LevelId" Type="String" />
                                                    </Fields>
                                                </ext:Model>
                                            </Model>
                                        </ext:Store>
                                    </Store>
                                    <ColumnModel>
                                        <Columns>
                                            <ext:Column ID="Column6" TdCls="wrap" runat="server" Text="Levels" Sortable="false" Flex="1"
                                                MenuDisabled="true" Width="250" DataIndex="Name" Border="false">
                                            </ext:Column>
                                        </Columns>
                                    </ColumnModel>
                                    <SelectionModel>
                                        <ext:CheckboxSelectionModel ID="CheckboxSelectionModel2" runat="server" Mode="Simple">
                                        </ext:CheckboxSelectionModel>
                                    </SelectionModel>
                                </ext:GridPanel>
                                <span style="color: gray"></span>
                            </td>
                        </tr>
                        <tr>
                            <td valign="bottom">
                                <div class="popupButtonDiv">
                                    <ext:Button Cls="btn btn-primary" runat="server" ID="btnOvertimeType" Text="<i></i>Save">
                                        <DirectEvents>
                                            <Click OnEvent="btnOvertimeType_Click">
                                                <EventMask ShowMask="true" />
                                                <ExtraParams>
                                                    <ext:Parameter Name="GridIncomeValues" Value="Ext.encode(#{gridIncome}.getRowsValues({selectedOnly:true}))"
                                                        Mode="Raw" />
                                                         <ext:Parameter Name="GridLevelsValues" Value="Ext.encode(#{gridNotApplicableTo}.getRowsValues({selectedOnly:true}))"
                                                        Mode="Raw" />
                                                </ExtraParams>
                                            </Click>
                                        </DirectEvents>
                                        <Listeners>
                                            <Click Handler="valGroup = 'SaveUpdate'; if(CheckValidation()) return this.disable(); else return false;">
                                            </Click>
                                        </Listeners>
                                    </ext:Button>
                                    <div class="btnFlatOr">
                                        or</div>
                                    <ext:LinkButton runat="server" StyleSpec="padding:0px;" Cls="btnFlatLeftGap" ID="LinkButton5"
                                        Text="<i></i>Cancel" runat="server">
                                        <Listeners>
                                            <Click Handler="#{windowAddEditOvertime}.hide();">
                                            </Click>
                                        </Listeners>
                                    </ext:LinkButton>
                                </div>
                            </td>
                        </tr>
                    </table>
                </Content>
            </ext:Window>
        </div>
    </div>
    <br />
</asp:Content>
