﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using Utils.Helper;
using Utils.Web;
using BLL.Base;
using DAL;
using Utils.Calendar;
using BLL.BO;
using Web.Helper;

namespace Web.CP
{
    public partial class AllowanceForwardOld : BasePage
    {

        private PayManager payMgr = new PayManager();

        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (!IsPostBack)
            {
                List<Branch> branches = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId); ;
                //if (branches.Count == 1)
                //{
                //    brTd.Visible = false;
                //    brTd2.Visible = false;
                //    ddlBranch.SelectedIndex = 1;

                //}
                //ddlBranch.SelectedIndex = 1;
                ddlBranch.DataSource = branches;
                ddlBranch.DataBind();
                
                Initialise();
            }
            JavascriptHelper.AttachNonDialogPopUpCode(Page, "assignovertimePopup", "AllowanceForwardAssignPopup.aspx", 475, 550);
            JavascriptHelper.AttachNonDialogPopUpCode(Page, "positionHistoryPopup", "AllowanceForwardPopup.aspx", 625, 600);
        
        }

        protected void btnPrevious_Click()
        {
            pagingCtl.CurrentPage -= 1;
            //_tempCurrentPage = int.Parse(pagintCtl.LabelCurrentPage.Text);
            //_tempCurrentPage -= 1;
            LoadEmployees();
        }

        protected void ChangePageNumber()
        {
            LoadEmployees();
        }

        protected void btnNext_Click()
        {
            pagingCtl.CurrentPage += 1;
            //_tempCurrentPage = int.Parse(pagintCtl.LabelCurrentPage.Text);
            //_tempCurrentPage += 1;
            LoadEmployees();
        }

        protected void ddlRecords_SelectedIndexChanged()
        {
            //_tempCurrentPage = 1;
            pagingCtl.CurrentPage = 1;
            LoadEmployees();
        }

        public void ddlType_Change(object sender, EventArgs e)
        {
            if (ddlType.SelectedValue == "-1")
            {
                t2.Visible = true;
                t3.Visible = true;
            }
            else
            {
                t2.Visible = false;
                t3.Visible = false;
            }

            LoadEmployees();
        }

        protected void ddlStatus_Change(object sender, EventArgs e)
        {
            btnDelete.Visible = true;
            if (ddlStatus.SelectedItem.Value == "3")
            {
                btnDelete.Visible = false; 
            }
        }


        protected void btnForward_Click(object sender, EventArgs e)
        {

            bool employeeAdded = false;
            List<EveningCounterRequest> requestList = new List<EveningCounterRequest>();
            foreach (GridViewRow row in gvw.Rows)
            {

                EveningCounterRequest request = new EveningCounterRequest();
                int RequestID = int.Parse(gvw.DataKeys[row.RowIndex]["RequestID"].ToString());


                if (row.FindControl("chkDelete") == null)
                    continue;

                CheckBox chkDelete = (CheckBox)row.FindControl("chkDelete");

                if (chkDelete.Checked)
                {
                    employeeAdded = true;

                    request.CounterRequestID = RequestID;
                    requestList.Add(request);
                }
            }

            int count = 0;
            if (AllowanceManager.ForwardRequestInBulk(requestList, out count))
            {
                LoadEmployees();

                divMsgCtl.InnerHtml = count + " Allowance Request has been forwarded.";
                divMsgCtl.Hide = false;
            }

        }

        protected void btnReject_Click(object sender, EventArgs e)
        {

            bool employeeAdded = false;
            List<EveningCounterRequest> requestList = new List<EveningCounterRequest>();
            foreach (GridViewRow row in gvw.Rows)
            {

                EveningCounterRequest request = new EveningCounterRequest();
                int RequestID = int.Parse(gvw.DataKeys[row.RowIndex]["RequestID"].ToString());


                if (row.FindControl("chkDelete") == null)
                    continue;

                CheckBox chkDelete = (CheckBox)row.FindControl("chkDelete");

                if (chkDelete.Checked)
                {
                    employeeAdded = true;

                    request.CounterRequestID = RequestID;
                    requestList.Add(request);
                }
            }

            int count = 0;
            if (AllowanceManager.RejectRequestInBulk(requestList, out count))
            {
                LoadEmployees();

                divMsgCtl.InnerHtml = count + " Allowance Request has been rejected.";
                divMsgCtl.Hide = false;
            }

        }

        void Initialise()
        {
            gvw.Columns[7].HeaderText = AllowanceManager.AllowanceDaysOrHourLabel;


            if (!string.IsNullOrEmpty(Request.QueryString["status"]))
            {
                //ddlStatus.ClearSelection();
                //ddlStatus.SelectedValue = Request.QueryString["status"];

                int status = int.Parse(Request.QueryString["status"]);

                if (status == 0)
                    tab.ActiveTabIndex = 1;
                else if (status == 1)
                    tab.ActiveTabIndex = 2;
                else if (status == 2)
                    tab.ActiveTabIndex = 3;
            }

            LoadEmployees();

        }
        protected void btnLoad_Click(object sender, EventArgs e)
        {
        
            LoadEmployees();
        }
        protected void gvwEmployees_RowCreated(object sender, GridViewRowEventArgs e)
        {

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover",
                "this.originalClass=this.className;this.className='selected'");

                e.Row.Attributes.Add("onmouseout",
                "this.className=this.originalClass;");


            }

        }



        protected void LoadEmployees()
        {
            int type = 1;
            int status = -1;

            status = tab.ActiveTabIndex - 1;

            if (ddlType.SelectedItem != null)
                type = int.Parse(ddlType.SelectedValue);

            DateTime? start = null;
            DateTime? end = null;

            DateTime date;
            if (ddlType.SelectedValue == "-1")
            {
                if (DateTime.TryParse(dateFrom.Text, out date))
                    start = dateFrom.SelectedDate;
                if (DateTime.TryParse(dateTo.Text, out date))
                    end = dateTo.SelectedDate;
            }

            if (start != null && start == DateTime.MinValue)
                start = null;

            if (end != null && end == DateTime.MinValue)
                end = null;

            int? totalRecords = 0;

            int BranchID = -1;

            if (ddlBranch.SelectedIndex != null)
            {
                BranchID = int.Parse(ddlBranch.SelectedValue);
            }

            gvw.DataSource = OvertimeManager.GetEveningCounterListForAdmin(type, status, start, end, txtEmpSearchText.Text.Trim()
                , pagingCtl.CurrentPage, int.Parse(pagingCtl.DDLRecords.SelectedValue), ref totalRecords, BranchID
                ,-1,"",null);
            gvw.DataBind();

            pagingCtl.UpdatePagingBar(totalRecords.Value);

        }

        public string GetEngOrNepDate(object value)
        {
            if (value == null)
                return "";

            DateTime date = Convert.ToDateTime(value);

            CustomDate c = new CustomDate(date.Day, date.Month, date.Year, true);

            if (IsEnglish)
                return c.ToString();

            else
                return CustomDate.ConvertEngToNep(c).ToString();
        }

       
        protected void btnExport_Click(object sender, EventArgs e)
        {
            //GridViewExportUtil.Export("Overtime List.xls", gvw);

            int type = 1;
            int status = -1;

            status = tab.ActiveTabIndex - 1;

            if (ddlType.SelectedItem != null)
                type = int.Parse(ddlType.SelectedValue);

            DateTime? start = null;
            DateTime? end = null;

            DateTime date;
            if (ddlType.SelectedValue == "-1")
            {
                if (DateTime.TryParse(dateFrom.Text, out date))
                    start = dateFrom.SelectedDate;
                if (DateTime.TryParse(dateTo.Text, out date))
                    end = dateTo.SelectedDate;
            }

            if (start != null && start == DateTime.MinValue)
                start = null;

            if (end != null && end == DateTime.MinValue)
                end = null;

            int? totalRecords = 0;

            int BranchID = -1;

            if (ddlBranch.SelectedIndex != null)
            {
                BranchID = int.Parse(ddlBranch.SelectedValue);
            }

            List<GetEveningCounterRequestForAdminResult> list = OvertimeManager.GetEveningCounterListForAdmin(type, status, start, end, txtEmpSearchText.Text.Trim()
                , 1, 9999999, ref totalRecords, BranchID, -1, "", null);

            foreach (var item in list)
            {
                item.StartDateEngFormatted = WebHelper.FormatDateForExcel(item.StartTime);
                item.EndDateEngFormatted = WebHelper.FormatDateForExcel(item.EndTime);
            }

            Bll.ExcelHelper.ExportToExcel("Allowance List", list,
                new List<String>() {"TotalRows","RowNumber","RequestID", "Date","StartTime","EndTime","SupervisorID","Status", "DurationModified", "ApprovedOn"},
            new List<String>() { },
            new Dictionary<string, string>() { {"EmployeeID","EIN"}, {"EmployeeName","Employee"}, 
                    {"StartDateEngFormatted","Start Date"}, {"EndDateEngFormatted","End Date"}, { "StartDateNep", "Start Date(BS)" },
                    { "EndDateNep", "End Date(BS)" }, { "Duration", "Units" }, { "OvertimeType", "Allowance" }, 
                    { "SupervisorName", "Proccessed By" }, { "StatusModified", "Status" } },
            new List<string>() { }
            , new List<string> { }
            , new List<string> { "StartDateEngFormatted", "EndDateEngFormatted" }
            , new Dictionary<string, string>() { }
            , new List<string> { "EmployeeID", "EmployeeName", "StartDateEngFormatted", "EndDateEngFormatted", "StartDateNep", "EndDateNep", "Duration", "Reason", "OvertimeType", "SupervisorName", "StatusModified" });

        }

        //protected void btnPostToSalary_Click(object sender, EventArgs e)
        //{

        //    foreach (GridViewRow row in gvw.Rows)
        //    {


        //        int payrollPeriodId = (int)gvw.DataKeys[row.RowIndex]["PayrollPeriodId"];

        //        PayManager.PostOvertimePay(payrollPeriodId);
        //        msgCtl.InnerHtml = Resources.Messages.OvertimePostedMessage;
        //        msgCtl.Hide = false;
        //        break;
        //    }


        //}
        protected void txtEmployeeName_TextChanged(object sender, EventArgs e)
        {
            

        }

        public void btnTab_click(object sender, EventArgs e)
        {
            LoadEmployees();
        }
       

    }
}
