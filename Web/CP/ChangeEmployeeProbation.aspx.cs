﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Base;
using BLL.Manager;
using Utils.Helper;
using DAL;
using Utils.Web;
using BLL.Entity;

namespace Web.CP
{
    public partial class ChangeEmployeeProbation : BasePage
    {
        PayManager payMgr = new PayManager();
        int employeeId = 0;
        int statusId = 0;
        EmployeeManager empMgr = new EmployeeManager();

        protected void Page_Load(object sender, EventArgs e)
        {


            employeeId = UrlHelper.GetIdFromQueryString("EId");
            if (!string.IsNullOrEmpty(Request.QueryString["statusId"]))
                statusId = UrlHelper.GetIdFromQueryString("statusId");
            if (!IsPostBack)
            {
                Initialise();
            }
            //else if (eventTarget != null && eventTarget.Equals("Reload"))
            //{
            //    Initialise();
            //}


        }

        void Initialise()
        {


            calTo.IsEnglishCalendar = SessionManager.CurrentCompany.IsEnglishDate;
            calTo.SelectTodayDate();

            int empId = employeeId;


            if (statusId != 0)
            {
                ProbationHistory status = empMgr.GetProbation(statusId);


                {
                    calTo.Enabled = true;
                    calTo.SetSelectedDate(status.ToDate, IsEnglish);
                }
                txtNote.Text = status.Note;
                txtLetterNo.Text = status.LetterNo;

            }

        }

        public void btnSaveStatus_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {

                if (HttpContext.Current.User.Identity.IsAuthenticated == false || SessionManager.CurrentLoggedInEmployeeId != 0)
                    return;

                string statusHTML = "";

                ProbationHistory empStatus = new ProbationHistory();
                empStatus.ProbationId = statusId;
                empStatus.EmployeeId = employeeId;

                empStatus.Note = txtNote.Text.Trim();


                {
                    empStatus.ToDate = calTo.SelectedDate.ToString();
                    empStatus.ToDateEng = GetEngDate(empStatus.ToDate);
                }

                empStatus.FromDate = empStatus.ToDate;
                empStatus.FromDateEng = empStatus.ToDateEng.Value;

                empStatus.LetterNo = txtLetterNo.Text.Trim();

                //empStatus.ToDate = toDate;         

                EmployeeManager empMgr = new EmployeeManager();

                string msg = "";
                ResponseStatus status = null;

                if (empStatus.ProbationId == 0)
                {
                    msg = "Status saved.";
                    status = empMgr.Save(empStatus);
                    //statusHTML = empMgr.GetCurrentStatus(empStatus.EmployeeId);
                }
                else
                {
                    msg = "Status updated.";
                    //if (empMgr.Update(empStatus))
                    status = empMgr.Update(empStatus);
                    //statusHTML = empMgr.GetCurrentStatus(empStatus.EmployeeId);
                }

                if (status.IsSuccessType == false)
                {
                    JavascriptHelper.DisplayClientMsg(status.ErrorMessage, this);
                    return;
                }

                JavascriptHelper.DisplayClientMsg(msg, this, "closePopup();");
            }
        }

        public void HidePreviousStatus(string currentStatus)
        {



        }

        protected void btnOk_Click(object sender, EventArgs e)
        {

            if (Page.IsValid)
            {


            }
        }
    }
}