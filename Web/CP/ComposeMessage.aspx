<%@ Page MaintainScrollPositionOnPostback="true" Title="Payroll Message" Language="C#"
    MasterPageFile="~/Master/NewDetails.Master" AutoEventWireup="true" CodeBehind="ComposeMessage.aspx.cs"
    Inherits="Web.Employee.ComposeMessage" %>

<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/ExtMsgCtl.ascx" TagName="ExtMsgCtl" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../Scripts/jquery-1.3.2.min.js" type="text/javascript"></script>
    <%--  <script src="../Scripts/jquery.sexy-combo.pack.js" type="text/javascript"></script>

   <script src="../Scripts/jquery.sexy-combo.min.js" type="tet/javascript"></script>

    <script src="../Scripts/jquery.sexy-combo.js" type="text/javascript"></script>--%>
    <script type="text/javascript">

        var cboEmployee = null;
        var dfEmployees = null;
        var cboBranch = null;
        var cboDepartment = null;
        var cboBranch1 = null;
        var cboEmployee = null;
        var hdEmployees = null;
        var store;

        Ext.onReady(
            function () {
                
            }
        );
        function loadList()
        {
            var hdn  = <%=hdEmployees.ClientID %>;
            var store = <%=storeSelectedEmployeeList.ClientID %>;
            var list = "";
             for(i=0;i<store.data.items.length;i++)
                {
                    var item = store.data.items[i];
                    if(list == "")
                        list = item.data.UserName
                    else
                        list += ";" + item.data.UserName; 
                }
            hdn.setValue(list);

        }
        function RenderSelectedEmployee() {
            var id = cboEmployee.getValue();
            var value = cboEmployee.getRawValue();
            var store = <%=storeSelectedEmployeeList.ClientID %>;
            if( Ext.isEmpty( store.getById(id)))
            {
                var myArray = new EmployeeModel();

                myArray.data.UserName = id;
                myArray.data.Name = value;

                store.add(myArray);
                var html = "";
                for(i=0;i<store.data.items.length;i++)
                {
                    var item = store.data.items[i];
                    html += 
                    
                     "<span  id='" + item.data.UserName + "'>" + item.data.Name + "<img style='cursor:pointer;' onclick='deleteEmployee(\"" + item.data.UserName + "\")' src='../images/deletered.png' height='12px'/></span>";
                }
                dfEmployees.setValue(html);
            }
            loadList();
        }

        function deleteEmployee(empId) {
            var spans = ctl00_ContentPlaceHolder_Main_dfEmployees.container.dom.getElementsByTagName('span');
            for (var i = 0; i < spans.length; i++) {
                if (spans[i].id.toLowerCase() == empId.toLowerCase()) {
                    //AddRecord(spans[i].id, spans[i].innerText);
                    spans[i].outerHTML = '';
                }

            }
            if( Ext.isEmpty( <%=storeSelectedEmployeeList.ClientID %>.getById(empId)) == false)
            {
                <%=storeSelectedEmployeeList.ClientID %>.remove(<%=storeSelectedEmployeeList.ClientID %>.getById(empId));
            }
            loadList();
        }


     
        function EnableDisableControls(send) {
            switch (send.id.toLowerCase()) {
                //case "ctl00_maincontent_rdoall":
                case "ctl00_contentplaceholder_main_rdoall":
                    if (send.checked) {
                        DisableControl();
                    }
                    break;
                case "ctl00_maincontent_rdobranch":
                    if (send.checked) {
                        DisableControl();
                        //cboBranch.enable();
                    }
                    break;
                //case "ctl00_maincontent_rdodepartment":
                case "ctl00_contentplaceholder_main_rdodepartment":
                    if (send.checked) {
                        DisableControl();
                        cboDepartment.enable();
                        cboBranch1.enable();
                    }
                    break;
                //case "ctl00_maincontent_rdospecificemployee":
                case "ctl00_contentplaceholder_main_rdospecificemployee":
                    if (send.checked) {
                        DisableControl();
                        cboEmployee.enable();
                        ReloadSpecificEmployee();
                    }
                    break;
                default: break;
            }
        }

        function DisableControl() {
            dfEmployees.setValue('');
            //cboBranch.disable();
            cboDepartment.disable();
            cboBranch1.disable();
            cboEmployee.disable();
        }

        function AddEmployeeToList(empId) {
            hdEmployees.setValue(hdEmployees.getValue() + ";" + empId);
        }

        function ReloadSpecificEmployee() {
            //Rigo.direct.reloadSpecificEmployee();
            <%=bntLoadEmp.ClientID %>.fireEvent('click');
        }
    </script>
    <style type="text/css">
        .dirty-row
        {
            background: #FFFDD8;
        }
        
        .new-row
        {
            background: #c8ffc8;
        }
        .divDueDate
        {
            background: silver;
        }
        .x-panel-header
        {
            background-image: none;
        }
        .x-panel-header-text-container-default
        {
            color: White !important;
        }
        .fieldTable td, table tr.up td
        {
            padding-top: 0px;
        }
        .x-toolbar
        {
            padding: inherit;
        }
        #ctl00_mainContent_txtBody-inputCmp-iframeEl
        {
            border:1px solid lightgray;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <%--<ext:ResourceManager runat="server" ScriptMode="Debug" DisableViewState="false" ShowWarningOnAjaxFailure="false"
        Namespace="Rigo">
    </ext:ResourceManager>--%>
    <script type="text/javascript">
        Ext.onReady(
                function () {
                    cboEmployee =  <%=cboEmployee.ClientID%>;
                    //cboBranch.ClientID
                    cboDepartment =  <%=cboDepartment.ClientID%>;
                    cboBranch1 =  <%=cboBranch1.ClientID%>;
                    cboEmployee =  <%=cboEmployee.ClientID%>;
                    dfEmployees =  <%=dfEmployees.ClientID%>;
                    hdEmployees = <%=hdEmployees.ClientID%>;
                }
        );
    </script>


    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Send Message
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
   
        <ext:Hidden ID="hdEmployees" runat="server" />
        <ext:LinkButton ID="bntLoadEmp" runat="server" Hidden="true">
            <DirectEvents>
                <Click OnEvent="bntLoadEmp_Click">
                    <EventMask ShowMask="true" />
                </Click>
            </DirectEvents>
        </ext:LinkButton>

       
        <%-- <uc2:ExtMsgCtl ID="divMsgCtl" EnableViewState="false" Hide="true" runat="server" />
        <uc2:WarningCtl ID="divWarningMsg" EnableViewState="false" Hide="true" runat="server" />--%>
        <table width="100%" style="clear: both">
            <tr>
                <td>
                    <ext:Panel ID="Panel1" runat="server" Title="Send Message To" Height="200" Collapsible="true"
                        Collapsed="false">
                        <Content>
                            <div style="padding: 10px;">
                                <table class="fieldTable firsttdskip">
                                    <tbody>
                                        <tr>
                                            <td colspan="4">
                                                <ext:Radio ID="rdoAll" runat="server" BoxLabel="All Employees" Name="Message" Checked="true">
                                                    <Listeners>
                                                        <Change Handler="EnableDisableControls(this);" />
                                                    </Listeners>
                                                </ext:Radio>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <ext:Radio ID="rdoDepartment" runat="server" BoxLabel="Employees in a Branch" Name="Message">
                                                    <Listeners>
                                                        <Change Handler="EnableDisableControls(this);" />
                                                    </Listeners>
                                                </ext:Radio>
                                            </td>
                                            <td>
                                                <ext:ComboBox ID="cboBranch1" Width="250" runat="server" QueryMode="Local" ForceSelection="true"
                                                    DisplayField="Name" ValueField="BranchId" FieldLabel="Branch" Disabled="true">
                                                    <Store>
                                                        <ext:Store runat="server" ID="storeBranch1">
                                                            <Model>
                                                                <ext:Model>
                                                                    <Fields>
                                                                        <ext:ModelField Name="Name" />
                                                                        <ext:ModelField Name="BranchId" />
                                                                    </Fields>
                                                                </ext:Model>
                                                            </Model>
                                                        </ext:Store>
                                                    </Store>
                                                    <DirectEvents>
                                                        <Select OnEvent="cboBranch1_Select">
                                                            <EventMask ShowMask="true" />
                                                        </Select>
                                                    </DirectEvents>
                                                    <Listeners>
                                                        <Select Handler="#{dfEmployees}.setValue('All employees selected within ' + #{cboBranch1}.getRawValue() + '.');" />
                                                    </Listeners>
                                                </ext:ComboBox>
                                            </td>
                                            <td>
                                                <ext:ComboBox ID="cboDepartment" runat="server" QueryMode="Local" ForceSelection="true"
                                                    DisplayField="Name" ValueField="DepartmentId" Disabled="true" FieldLabel="Employees in a Department">
                                                    <Store>
                                                        <ext:Store runat="server" ID="storeDepartment">
                                                            <Model>
                                                                <ext:Model>
                                                                    <Fields>
                                                                        <ext:ModelField Name="Name" />
                                                                        <ext:ModelField Name="DepartmentId" />
                                                                    </Fields>
                                                                </ext:Model>
                                                            </Model>
                                                        </ext:Store>
                                                    </Store>
                                                    <Listeners>
                                                        <Select Handler="#{dfEmployees}.setValue('All employees selected within ' + #{cboDepartment}.getRawValue() + '.');" />
                                                    </Listeners>
                                                </ext:ComboBox>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <ext:Radio ID="rdoSpecificEmployee" runat="server" BoxLabel="Selected Employees" Name="Message">
                                                    <Listeners>
                                                        <Change Handler="EnableDisableControls(this);" />
                                                    </Listeners>
                                                </ext:Radio>
                                            </td>
                                            <td>
                                                <ext:ComboBox Width="250" Disabled="true" Mode="Local" SelectOnFocus="true" TriggerAction="All"
                                                    ID="cboEmployee" FieldLabel="Employee" QueryMode="Local" TypeAhead="true" Editable="true"
                                                    runat="server" DisplayField="Text" ValueField="Value">
                                                    <Store>
                                                        <ext:Store runat="server" ID="storeEmployee">
                                                            <Model>
                                                                <ext:Model runat="server" Name="EmpModel">
                                                                    <Fields>
                                                                        <ext:ModelField Name="Text" />
                                                                        <ext:ModelField Name="Value" />
                                                                    </Fields>
                                                                </ext:Model>
                                                            </Model>
                                                        </ext:Store>
                                                    </Store>
                                                    <Listeners>
                                                        <Select Handler="RenderSelectedEmployee();" />
                                                    </Listeners>
                                                </ext:ComboBox>
                                                <ext:Store runat="server" ID="storeSelectedEmployeeList">
                                                    <Model>
                                                        <ext:Model ID="Model1" runat="server" Name="EmployeeModel" IDProperty="UserName">
                                                            <Fields>
                                                                <ext:ModelField Name="UserName" />
                                                                <ext:ModelField Name="Name" />
                                                            </Fields>
                                                        </ext:Model>
                                                    </Model>
                                                </ext:Store>
                                            </td>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4">
                                                <ext:Panel StyleSpec="padding-top:5px;padding-bottom:5px;" ID="pnlEmloyeeHolder"
                                                    runat="server" Header="false" Height="80px">
                                                    <Content>
                                                        <ext:DisplayField ID="dfEmployees" runat="server">
                                                        </ext:DisplayField>
                                                    </Content>
                                                </ext:Panel>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </Content>
                    </ext:Panel>
                </td>
            </tr>
        </table>
        <table width="600px" style="padding-top: 10px; padding-left: 10px;">
            <tr>
                <td colspan="2">
                    <ext:TextField ID="txtSubject" runat="server" LabelAlign="Top" Width="600px" FieldLabel="Write a subject line" />
                    <asp:RequiredFieldValidator ID="valSub1" Enabled="true" ControlToValidate="txtSubject" runat="server" Display="None" ErrorMessage="Subject is required."
                        ValidationGroup="message" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <ext:HtmlEditor ID="txtBody" Border="true" runat="server" LabelAlign="Top" Width="600"
                        FieldLabel="Message" Height="200">
                    </ext:HtmlEditor>
                </td>
            </tr>
            <tr>
                <td>
                    <ext:Checkbox ID="chkSendMeAlso" BoxLabel="Send a copy to me" runat="server" />
                </td>
                <td align="left">
                    <p style="margin: 3px;">
                        Due Date</p>
                    <ext:DateField ID="dateDueDate" runat="server">
                    </ext:DateField>
                </td>
            </tr>
            <tr>
                <td>
                    <ext:Button ID="btnSend" runat="server" Width="100" Height="30" Text="Send">
                        <Listeners>
                            <Click Handler="valGroup='message';return CheckValidation();" />
                        </Listeners>
                        <DirectEvents>
                            <Click OnEvent="btnSend_Click">
                                <EventMask ShowMask="true" Msg="sending message..." />
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                </td>
            </tr>
        </table>
    
</div>
</asp:Content>
