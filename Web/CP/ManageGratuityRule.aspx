<%@ Page Title="Define Gratuity rules" Language="C#" MasterPageFile="~/Master/HROld.Master"
    AutoEventWireup="true" CodeBehind="ManageGratuityRule.aspx.cs" Inherits="Web.CP.ManageGratuityRule" %>

<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">

        function insertUpdateGratuityClassCall(classId) {
            var ret = insertUpdateGratuityClass('Id=' + classId);

            if (typeof (ret) != 'undefined') {
                if (ret == 'Reload') {
                    __doPostBack('Reload', '');
                }
            }
        }
        function insertUpdateGratuityRuleCall(gratuityRuleId) {
            var ret = insertUpdateGratuityRule('Id=' + gratuityRuleId);

            if (typeof (ret) != 'undefined') {
                if (ret == 'Reload') {
                    __doPostBack('Reload', '');
                }
            }
        }

        var chkAll = '#<%= chkAll.ClientID %>';

        function refresh(popupWindow) {
            popupWindow.close();
            __doPostBack('Reload', '');
        }

        function changeStates(chk) {
            var isChecked = $(chkAll).is(':checked');

            $("INPUT[type='checkbox']").attr('checked', isChecked);

        }


    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Gratuity/Severance Rules
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <div class="contentArea">
            <asp:LinkButton ID="Button1" class="btn btn-success btn-sm btn-sect" runat="server"
                Text="Add Rule" Style="height: 30px; width: 100px; margin-bottom: 5px; margin-left: 20px;"
                OnClientClick="insertUpdateGratuityRuleCall();return false;" />
            <uc2:MsgCtl ID="divMsgCtl" EnableViewState="false" Hide="true" runat="server" />
            <uc2:WarningCtl ID="divWarningMsg" EnableViewState="false" Hide="true" runat="server" />
            <br />
            <div>
                <!-- panel-heading -->
                <div class="panel-body">
                    <cc2:EmptyDisplayGridView CssClass="table table-primary mb30 table-bordered table-hover"
                        UseAccessibleHeader="true" ID="gvwGratuityRules" ShowHeaderWhenEmpty="True" runat="server"
                        AutoGenerateColumns="False" GridLines="None" DataKeyNames="GratuityRuleId" OnRowDeleting="gvwDepartments_RowDeleting"
                        ShowFooterWhenEmpty="True" Width="600px">
                        <Columns>
                            <asp:TemplateField HeaderText="Rule" HeaderStyle-Width="50px" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <%# Container.DataItemIndex + 1 %>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Service Year" HeaderStyle-Width="80px" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <%# Eval("FromServiceYear") + " - " + Eval("ToServiceYear")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="MonthsSalary" HeaderStyle-Width="80px" ItemStyle-HorizontalAlign="Center"
                                HeaderText="Month's Salary">
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            </asp:BoundField>
                            <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"
                                HeaderStyle-Width="150px" HeaderText="Class">
                                <ItemTemplate>
                                    <%#     GetApplicableTo(Eval("GratuityClassRef_ID"))%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="50px">
                                <ItemTemplate>
                                    <input type='image' style='border-width: 0px;' onclick='return insertUpdateGratuityRuleCall(<%# Eval("GratuityRuleId") %>);'
                                        src='../images/edit.gif' />
                                    &nbsp;
                                    <asp:ImageButton ID="ImageButton2" OnClientClick="return confirm('Confirm delete the gratuity rule?')"
                                        runat="server" CommandName="Delete" ImageUrl="~/images/delet.png" />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            </asp:TemplateField>
                        </Columns>
                        <RowStyle CssClass="odd" />
                        <AlternatingRowStyle CssClass="even" />
                        <EmptyDataTemplate>
                            <b>No Gratuity/Severance rule defined.</b>
                        </EmptyDataTemplate>
                    </cc2:EmptyDisplayGridView>
                    <br />
                    <asp:LinkButton ID="LinkButton1" class="btn btn-success btn-sm btn-sect" runat="server"
                        Text="Add Class/Group" Style="height: 30px; width: 150px; margin-bottom: 5px;
                        margin-left: 0px;" OnClientClick="insertUpdateGratuityClassCall();return false;" />
                    <br />
                    <cc2:EmptyDisplayGridView CssClass="table table-primary mb30 table-bordered table-hover"
                        UseAccessibleHeader="true" ID="gridClassList" ShowHeaderWhenEmpty="True" runat="server"
                        AutoGenerateColumns="False" GridLines="None" DataKeyNames="GratuityClassID" OnRowDeleting="gvwClass_RowDeleting"
                        ShowFooterWhenEmpty="True" Width="600px">
                        <Columns>
                            <asp:TemplateField HeaderText="Name" HeaderStyle-Width="80px" ItemStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <%# Eval("Name")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"
                                HeaderStyle-Width="150px" HeaderText="Default">
                                <ItemTemplate>
                                    <%#     Convert.ToBoolean(Eval("IsDefault")) == true  ? "Yes" : ""%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="50px">
                                <ItemTemplate>
                                    <input type='image' style='border-width: 0px;' onclick='return insertUpdateGratuityClassCall(<%# Eval("GratuityClassID") %>);'
                                        src='../images/edit.gif' />
                                    &nbsp;
                                    <asp:ImageButton ID="ImageButton2" OnClientClick="return confirm('Confirm delete the gratuity class?')"
                                        runat="server" CommandName="Delete" ImageUrl="~/images/delet.png" />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            </asp:TemplateField>
                        </Columns>
                        <RowStyle CssClass="odd" />
                        <AlternatingRowStyle CssClass="even" />
                        <EmptyDataTemplate>
                            <b>No Gratuity/Severance class defined.</b>
                        </EmptyDataTemplate>
                    </cc2:EmptyDisplayGridView>
                    <br />
                    <asp:Button ID="btnAddNew" runat="server" Style='margin-bottom: 10px; height: 30px'
                        CssClass="btn btn-warning btn-sm btn-sect" Text="Edit Settings" OnClick="btnAddNew_Click" />
                    <div class="attribute" style='background: url()!important' visible="false" runat="server"
                        id="section">
                        <span style="padding: 5pt 0pt 8px 10px; display: block;">
                            <asp:CheckBox ID="chkAll" onclick="changeStates(this)" runat="server" />
                            <b style="font-size: 16px;"><b>Salary includes:</b></span>
                        <%--<div style="overflow-y: scroll; height: 110px; width: 95%; border: 1px solid #333">--%>
                        <div style="padding-left: 10px">
                            <asp:CheckBoxList ID="chkListSalaries" RepeatColumns="3" RepeatDirection="Horizontal"
                                runat="server" DataTextField="Title" DataValueField="IncomeId">
                            </asp:CheckBoxList>
                        </div>
                        <div style="padding-left: 10px; margin-top: 30px;">
                            Applicable To (If applicable to all status then no selection required)
                            <asp:CheckBoxList RepeatColumns="4" RepeatDirection="Horizontal" ID="chkStatusList"
                                runat="server" DataTextField="Value" DataValueField="Key">
                            </asp:CheckBoxList>
                            <br />
                            <br />
                            <asp:RadioButton ID="rdbGratutiyFromJoining" runat="server" Checked="true" Text="From Joining Date"
                                GroupName="gratuity" />
                            <br />
                            <asp:RadioButton ID="rdbGratutiyFromStatusChange" runat="server" Text="From Above selected status change"
                                GroupName="gratuity" />
                        </div>
                        <div style="padding-left: 10px; margin-top: 30px;">
                            Deduction
                            <br />
                           <%-- <br />
                            <asp:CheckBox ID="chkDeductStopPayment" runat="server" Text="Deduct Stop Payment" />--%>
                            <br />
                            <asp:CheckBox ID="chkDeductUPLLWP" runat="server" Text="Deduct UPL/LWP" />
                            <br />
                            <asp:CheckBox ID="chkDeductAbsent" runat="server" Text="Deduct Absent" />
                        </div>
                        <br />
                        <asp:Button ID="btnSave" runat="server" Text="Save Changes" Style="height: 30px;
                            margin-left: 10px;" CssClass="btn btn-primary btn-sm btn-sect" OnClick="btnSave_Click" />
                        <%--</div>--%>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
