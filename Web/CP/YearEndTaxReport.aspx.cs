﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using Utils.Helper;
using Utils.Web;
using BLL.Base;
using DAL;
using Utils.Base;

namespace Web
{


    public partial class YearEndTaxReport : BasePage
    {


        protected void Page_Load(object sender, EventArgs e)
        {

          
           

            if (!IsPostBack)
            {
                List<FinancialDate> list = new CommonManager().GetAllFinancialDates(); ;

                foreach (FinancialDate item in list)
                    item.SetName(IsEnglish);

                ddlYear.DataSource = list; 
                ddlYear.DataBind();



            }

            {

                BindEmployees();

            }
        }



        void BindEmployees()
        {


            List<Report_CompleteYearTaxReportResult> list =
                EmployeeManager.GetYearEndTaxReport(int.Parse(ddlYear.SelectedValue));


            List<PayrollPeriod> list1 =
               BLL.BaseBiz.PayrollDataContext.PayrollPeriods.Where(x => x.FinancialDateId == int.Parse(ddlYear.SelectedValue)).
               OrderBy(x => x.PayrollPeriodId).ToList();

            int i = 2;
            foreach (PayrollPeriod period in list1)
            {


                CCalculation calculation =
                    BLL.BaseBiz.PayrollDataContext.CCalculations.FirstOrDefault(x => x.PayrollPeriodId == period.PayrollPeriodId);

                if (calculation != null)
                {
                    gvEmployeeIncome.Columns[i].HeaderText = period.Name.Substring(0, period.Name.IndexOf("/")) + "-SST";
                    gvEmployeeIncome.Columns[i+1].HeaderText = period.Name.Substring(0, period.Name.IndexOf("/")) + "-TDS";

                    PartialTax addon = BLL.BaseBiz.PayrollDataContext.PartialTaxes.FirstOrDefault(x => x.PayrollPeriodId == period.PayrollPeriodId);
                    if (addon != null)
                    {
                        gvEmployeeIncome.Columns[i + 2].HeaderText = addon.Name + "-TDS";
                    }
                    else
                        gvEmployeeIncome.Columns[i + 2].HeaderText = "Add-On TDS";

                    gvEmployeeIncome.Columns[i+3].HeaderText = period.Name.Substring(0, period.Name.IndexOf("/")) + " - SST Tax Amount";
                    gvEmployeeIncome.Columns[i + 4].HeaderText = period.Name.Substring(0, period.Name.IndexOf("/")) + " - TDS Tax Amount";
                }


                i += 5;
            }




            gvEmployeeIncome.DataSource = list;
            gvEmployeeIncome.DataBind();




        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
           
                GridViewExportUtil.Export("Month Wise Yearly Tax Details.xls", gvEmployeeIncome);
          
        }
     
    }

  

}
