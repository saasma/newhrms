﻿<%@ Page Language="C#" Title="Associate Salary to Account Groups" AutoEventWireup="true" MasterPageFile="~/Master/ForPopupPage.Master"
    CodeBehind="AESalaryHeadAssociation.aspx.cs" Inherits="Web.CP.PP.AESalaryHeadAssociation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function closePopup() {
            // alert(window.opener.parentReloadCallbackFunction)
            if (isNonModalBrowser() && typeof (window.opener.refreshWindow)) {
                window.opener.refreshWindow();
                window.close();
            } else {
                window.returnValue = "Reload";
                window.close();
            }
        }
    </script>
    <style type="text/css">
        label.lblingText
        {
            width: 110px;
            padding-right: 10px;
            text-align: left;
            float: left;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
    <div class="popupHeader">
        <h2 class="headlinespop">
            Associate Salary to Account Groups
        </h2>
    </div>
    <div class="bevel marginal">
        <div class="fields paddpop">
           
            <p>
                <asp:Label ID="lbl3" runat="server" CssClass="lblingText" Text="Group Name"></asp:Label>
                <asp:DropDownList Enabled="false" ID="ddlSalaryHead" AppendDataBoundItems="true" DataValueField="TypeSourceId" DataTextField="HeaderName" runat="server" Width="200px">
                    <asp:ListItem Text="--Select salary head--" Value="-1"></asp:ListItem>                
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" Display="None" ValidationGroup="HFamily"
                    runat="server" ControlToValidate="ddlSalaryHead" InitialValue="-1" ErrorMessage="Salary head is required."></asp:RequiredFieldValidator>
            </p>
            <asp:Label ID="Label1" runat="server" CssClass="lblingText" Text="Salary Head"></asp:Label>
                <asp:DropDownList ID="ddlGroup" AppendDataBoundItems="true" DataValueField="VoucherGroupID" DataTextField="AccountGroupName" runat="server" Width="200px">
                    <asp:ListItem Text="--Select Group--" Value="-1"></asp:ListItem>                
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="None" ValidationGroup="HFamily"
                    runat="server" ControlToValidate="ddlGroup" InitialValue="-1" ErrorMessage="Group is required."></asp:RequiredFieldValidator>
            </p>
            <p style="margin-top:20px!important;">
                <label class="lblingText">
                    &nbsp;</label>
                <asp:Button ID="btnSave" CssClass="save" OnClientClick="valGroup='HFamily';return CheckValidation()"
                    runat="server" OnClick="btnSave_Click" Text="Save" ValidationGroup="HFamily" />
                <asp:Button ID="btnCancel" CssClass="cancel" runat="server" Text="Cancel" OnClientClick="window.close()" />
            </p>
        </div>
    </div>
</asp:Content>
