﻿<%@ Page Language="C#" Title="Account group details" AutoEventWireup="true" MasterPageFile="~/Master/ForPopupPage.Master"
    CodeBehind="AEVoucherGroupNew.aspx.cs" Inherits="Web.CP.PP.AEVoucherGroupNew" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function closePopup() {
            // alert(window.opener.parentReloadCallbackFunction)
            if (isNonModalBrowser() && typeof (window.opener.refreshWindow)) {
                window.opener.refreshWindow();
                window.close();
            } else {
                window.returnValue = "Reload";
                window.close();
            }
        }
    </script>
    <style type="text/css">
        label.lblingText
        {
            width: 110px;
            padding-right: 10px;
            text-align: left;
            float: left;
        }
        div.fields
        {
            width: inherit !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
    <div class="popupHeader">
        <h2 class="headlinespop">
            Account group details
        </h2>
    </div>
    <div class="bevel marginal" style="">
        <div class="fields paddpop">
            <table>
                <tr>
                    <td valign="top">
                        <div>
                            <p>
                                <asp:Label ID="lbl1" runat="server" CssClass="lblingText" Text="Group Code"></asp:Label>
                                <asp:TextBox CssClass="hrinput" ID="txtCode" runat="server" Width="152px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="None" ValidationGroup="HFamily"
                                    runat="server" ControlToValidate="txtCode" ErrorMessage="Code is required."></asp:RequiredFieldValidator></p>
                            <p style="display:none">
                                <asp:Label ID="Label6" runat="server" CssClass="lblingText" Text="Card Department Code"></asp:Label>
                                <asp:TextBox CssClass="hrinput" ID="txtCardDepartmentCode" runat="server" Width="152px"></asp:TextBox>
                               <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator3" Display="None" ValidationGroup="HFamily"
                                    runat="server" ControlToValidate="txtCardDepartmentCode" ErrorMessage="Card department code is required."></asp:RequiredFieldValidator>--%>
                                    </p>
                            <p>
                                <asp:Label ID="lbl2" runat="server" CssClass="lblingText" Text="Group Name"></asp:Label>
                                <asp:TextBox CssClass="hrinput" ID="txtName" runat="server" Width="152px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" Display="None" ValidationGroup="HFamily"
                                    runat="server" ControlToValidate="txtName" ErrorMessage="Name is required."></asp:RequiredFieldValidator>
                            </p>
                            <p>
                                <asp:Label ID="Label2" runat="server" CssClass="lblingText" Text="Transaction Code"></asp:Label>
                                <asp:TextBox CssClass="hrinput" ID="txtTransactionCode" runat="server" Width="152px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" Display="None" ValidationGroup="HFamily"
                                    runat="server" ControlToValidate="txtTransactionCode" ErrorMessage="Transaction code is required."></asp:RequiredFieldValidator></p>
                            <p>
                                <asp:Label ID="Label1" runat="server" CssClass="lblingText" Text="Group"></asp:Label>
                                <asp:DropDownList ID="ddlGroup" runat="server" Width="152px">
                                    <asp:ListItem Text="--Select Group--" Value="-1"></asp:ListItem>
                                    <%-- <asp:ListItem Text="Grouped" Value="1"></asp:ListItem>--%>
                                    <%-- <asp:ListItem Text="Single" Value="2"></asp:ListItem>
                    <asp:ListItem Text="Inidividual" Value="3"></asp:ListItem>--%>
                                    <asp:ListItem Text="SUM Company" Value="2"></asp:ListItem>
                                    <asp:ListItem Text="SUM Branch" Value="1"></asp:ListItem>
                                    <%-- <asp:ListItem Text="Employee wise" Value="3"></asp:ListItem>
                                    <asp:ListItem Text="Branch Transaction" Value="4"></asp:ListItem>
                                    <asp:ListItem Text="Voucher Balance" Value="5"></asp:ListItem>--%>
                                    <asp:ListItem Text="Loan Employee Wise" Value="6"></asp:ListItem>
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" Display="None" ValidationGroup="HFamily"
                                    runat="server" ControlToValidate="ddlGroup" InitialValue="-1" ErrorMessage="Group is required."></asp:RequiredFieldValidator>
                            </p>
                            <p>
                                <asp:Label ID="Label3" runat="server" CssClass="lblingText" Text="Show in Voucher Type"></asp:Label>
                                <asp:DropDownList ID="ddlVoucherType" AppendDataBoundItems="true" DataValueField="VoucherTypeID"
                                    DataTextField="Name" runat="server" Width="152px">
                                    <asp:ListItem Text="--Select Voucher Type--" Value="-1"></asp:ListItem>
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" Display="None" ValidationGroup="HFamily"
                                    runat="server" ControlToValidate="ddlVoucherType" InitialValue="-1" ErrorMessage="Voucher type is required."></asp:RequiredFieldValidator>
                            </p>
                            
                        </div>
                    </td>
                    <td valign="top" style="padding-left: 20px" >
                        <asp:Label ID="Label4" runat="server" CssClass="lblingText" Text="Applicable For"></asp:Label>
                        <br />
                        <asp:CheckBoxList ID="ddlApplicableList" AppendDataBoundItems="true" DataValueField="Key"
                            DataTextField="Value" runat="server" Width="152px">
                        </asp:CheckBoxList>
                    </td>
                    <td valign="top" style="padding-left: 20px" runat="server" id="tdLevels">
                        <asp:CheckBox runat="server" ID="chkLevelFilter" AutoPostBack="true" OnCheckedChanged="chkLevelFilter_Selection" Text="Apply Level Filter" />
                      <%--  <asp:Label ID="Label7" runat="server" CssClass="lblingText" Text="Applicable Labels"></asp:Label>--%>
                        <br />
                        <asp:CheckBoxList ID="chkLevels" AppendDataBoundItems="true"  Enabled="false" DataValueField="LevelId"
                            DataTextField="Name" runat="server" Width="152px">
                        </asp:CheckBoxList>
                    </td>
                    <td valign="top" style="padding-left: 20px">
                        <asp:Label ID="Label5" runat="server" CssClass="lblingText" Text="Incomes/Deductions"></asp:Label>
                        <br />
                        <asp:CheckBoxList ID="chkInocmeDeductionList" AppendDataBoundItems="true" DataValueField="TypeSourceId"
                            DataTextField="HeaderName" runat="server" Width="202px">
                        </asp:CheckBoxList>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <label class="lblingText" style="clear: both">
                            &nbsp;</label>
                        <br />
                        <asp:Button ID="btnSave" Style="" CssClass="save" OnClientClick="valGroup='HFamily';return CheckValidation()"
                            runat="server" OnClick="btnSave_Click" Text="Save" ValidationGroup="HFamily" />
                        <asp:Button ID="btnCancel" CssClass="cancel" runat="server" Text="Cancel" OnClientClick="window.close()" />
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
