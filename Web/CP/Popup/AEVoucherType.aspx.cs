﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using BLL.Manager;
using DAL;
using BLL;
using Utils.Helper;
using Utils.Web;

namespace Web.CP.PP
{
    public partial class AEVoucherType : System.Web.UI.Page
    {
        HRManager hrMgr = new HRManager();
        int empId = 0;

        public int DocumentID
        {
            get
            {
                if (ViewState["ID"] != null)
                    return int.Parse(ViewState["ID"].ToString());
                return 0;
            }
            set
            {
                ViewState["ID"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                initialise();
            }
            
        }
        void initialise()
        {
          
            loadFamilyDetails();
        }
        public void loadFamilyDetails()
        {
            int familyId = UrlHelper.GetIdFromQueryString("ID");
            if (familyId != 0)
            {
                DAL.NIBLVoucherType family = null;

                family = VouccherManagerNew.GetVoucherType(familyId);
                

                this.DocumentID = familyId;
                if (family != null)
                {
                    ProcessFamily(family);
                    btnSave.Text = Resources.Messages.Update;
                }


            }
        }
        NIBLVoucherType ProcessFamily(NIBLVoucherType family)
        {
            if (family != null)
            {
                txtName.Text = family.Name;
                txtDescription.Text = family.Description;
            }
            else
            {
                family = new DAL.NIBLVoucherType();
                family.Name = txtName.Text.Trim();
                family.Description = txtDescription.Text.Trim();
                return family;
            }
            return null;
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                DAL.NIBLVoucherType Family = ProcessFamily(null);
                if (this.DocumentID != 0)
                {
                    Family.VoucherTypeID = this.DocumentID;
                    bool status = VouccherManagerNew.UpdateVoucherGroup(Family);

                    if (status == false)
                    {
                        JavascriptHelper.DisplayClientMsg("Group name already exists.", this);
                        return;
                    }

                    JavascriptHelper.DisplayClientMsg("Group is updated.", this, "closePopup();\n");

                }
                else
                {


                    bool status = VouccherManagerNew.SaveVoucherGorup(Family);

                    if (status == false)
                    {
                        JavascriptHelper.DisplayClientMsg("Group name already exists.", this);
                        return;
                    }

                    JavascriptHelper.DisplayClientMsg("Group is saved.", this, "closePopup();\n");
                    btnSave.Text = Resources.Messages.Update;
                }
            }
            else
            {
                JavascriptHelper.DisplayClientMsg("Validation fail.", this);
            }
        }
    }
}
