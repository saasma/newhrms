﻿<%@ Page Language="C#" Title="Report header/footer" ValidateRequest="false" EnableEventValidation="false"
    AutoEventWireup="true" MasterPageFile="~/Master/ForPopupPage.Master" CodeBehind="ReportHeaderFooterEdit.aspx.cs"
    Inherits="Web.CP.PP.ReportHeaderFooterEdit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function closePopup() {
            // alert(window.opener.parentReloadCallbackFunction)

            window.opener.refreshWindow(window);
            window.close();
        }
    </script>
    <style type="text/css">
        label.lblingText
        {
            width: 110px;
            padding-right: 10px;
            text-align: left;
            float: left;
        }
        .x-html-editor-input
        {
            border-top: 1px;
            border-top-color: lightgray;
            border-top-style: solid;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
    <ext:ResourceManager ID="ResourceManager1" DisableViewState="false" runat="server"
        ShowWarningOnAjaxFailure="false" ScriptMode="Release">
    </ext:ResourceManager>
    <div class="popupHeader">
        <h2 class="headlinespop" runat="server" id="header">
            Report Header/Footer Content :
        </h2>
    </div>
    <div style='display: none'>
        <%--CIT report--%>
        <div runat="server" id="reportCITHeader">
            <div>
                <font face="tahoma" size="2">Citizen Investment Trust<span style="white-space: pre;">
                </span></font>
            </div>
            <div>
                <font face="tahoma" size="2">Putalisadak, Kathmandu</font></div>
            <div>
                <font face="tahoma" size="2">
                    <br>
                </font>
            </div>
            <div>
                <font face="tahoma" size="2">Please find details of the following employees for their
                    contribution into Retirement Fund and enclosed bank</font></div>
            <div>
                <font face="tahoma" size="2">voucher for your necessary action.</font></div>
        </div>
        <div runat="server" id="reportCITFooter">
            <div style="font-size: 13px; font-family: helvetica, arial, verdana, sans-serif;">
                <br>
            </div>
            <div style="font-size: 13px; font-family: helvetica, arial, verdana, sans-serif;">
                <br>
            </div>
            <div style="text-align: center;">
                <b><font face="tahoma" size="2">Prepared by &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                    &nbsp; &nbsp; &nbsp;Accountant &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                    &nbsp; &nbsp; Approved by</font></b></div>
        </div>
    </div>
    <div class="bevel marginal">
        <div class="fields paddpop" style='width: inherit'>
            <table cellspacing="10" class="fieldTable">
                <tbody>
                    <tr>
                        <td valign="top">
                            Your Header</strong>
                        </td>
                        <td>
                            <ext:HtmlEditor FieldBodyCls="htmlBody" Height="200" Width="750" ID="htmlHeader"
                                runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            <strong>Your Footer</strong>
                        </td>
                        <td>
                            <ext:HtmlEditor FieldBodyCls="htmlBody" Height="200" Width="750" ID="htmlFooter"
                                runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style='text-align: right'>
                            <table style='float: right;'>
                                <tr>
                                    <td style='padding-right: 488px;'>
                                        <ext:Button Cls="btn btn-primary" Width="100" ID="btnDelete" runat="server" Text="Delete">
                                            <DirectEvents>
                                                <Click OnEvent="btnDelete_Click">
                                                    <Confirmation Message="Are you sure to delete the content, after deletion default will be loaded?"
                                                        ConfirmRequest="true" />
                                                    <EventMask ShowMask="true" />
                                                </Click>
                                            </DirectEvents>
                                        </ext:Button>
                                    </td>
                                    <td style='padding-right: 14px;'>
                                        <ext:Button Cls="btn btn-primary" Width="100" ID="btnSave" runat="server" Text="Save">
                                            <DirectEvents>
                                                <Click OnEvent="btnSave_Click">
                                                    <EventMask ShowMask="true" />
                                                </Click>
                                            </DirectEvents>
                                        </ext:Button>
                                    </td>
                                    <td>
                                        <ext:LinkButton Cls="btnFlatLeftGap" ID="btnCancel" runat="server" Text="Cancel">
                                            <Listeners>
                                                <Click Handler="window.close();" />
                                            </Listeners>
                                        </ext:LinkButton>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </tbody>
            </table>
            <div>
                Note : For spacing between text, user spacebar instead of tab.
            </div>
        </div>
    </div>
</asp:Content>
