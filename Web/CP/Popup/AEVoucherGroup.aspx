﻿<%@ Page Language="C#" Title="Account group details" AutoEventWireup="true" MasterPageFile="~/Master/ForPopupPage.Master"
    CodeBehind="AEVoucherGroup.aspx.cs" Inherits="Web.CP.PP.AEVoucherGroup" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function closePopup() {
            // alert(window.opener.parentReloadCallbackFunction)
            if (isNonModalBrowser() && typeof (window.opener.refreshWindow)) {
                window.opener.refreshWindow();
                window.close();
            } else {
                window.returnValue = "Reload";
                window.close();
            }
        }
    </script>
    <style type="text/css">
        label.lblingText
        {
            width: 110px;
            padding-right: 10px;
            text-align: left;
            float: left;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
    <div class="popupHeader">
        <h2 class="headlinespop">
            Account group details
        </h2>
    </div>
    <div class="bevel marginal">
        <div class="fields paddpop">
            <p>
                <asp:Label ID="lblGroupCode" runat="server" CssClass="lblingText" Text="Group Code"></asp:Label>
                <asp:TextBox CssClass="hrinput" ID="txtCode" runat="server" Width="152px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="None" ValidationGroup="HFamily"
                    runat="server" ControlToValidate="txtCode" ErrorMessage="Code is required."></asp:RequiredFieldValidator></p>
             <p>
                <asp:Label ID="lblTransCode" runat="server" CssClass="lblingText" Text="Transaction Code"></asp:Label>
                <asp:TextBox CssClass="hrinput" ID="txtTransactionCode" runat="server" Width="152px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" Display="None" ValidationGroup="HFamily"
                    runat="server" ControlToValidate="txtTransactionCode" ErrorMessage="Transaction code is required."></asp:RequiredFieldValidator></p>
            <p>
                <asp:Label ID="lbl2" runat="server" CssClass="lblingText" Text="Group Name"></asp:Label>
                <asp:TextBox CssClass="hrinput" ID="txtName" runat="server" Width="152px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" Display="None" ValidationGroup="HFamily"
                    runat="server" ControlToValidate="txtName" ErrorMessage="Name is required."></asp:RequiredFieldValidator>
            </p>
           
            <p runat="server" id="globusDesc" visible="false">
                <asp:Label ID="Label3" runat="server" CssClass="lblingText" Text="Short Name / Desc"></asp:Label>
                <asp:TextBox CssClass="hrinput" ID="txtShortNameDesc" runat="server" Width="152px"></asp:TextBox>
                <asp:RequiredFieldValidator Enabled="false" ID="reqdShortName" Display="None" ValidationGroup="HFamily"
                    runat="server" ControlToValidate="txtShortNameDesc" ErrorMessage="Short Name / Desc is required."></asp:RequiredFieldValidator></p>
            <p>
                <asp:Label ID="lbl3" runat="server" CssClass="lblingText" Text="Type"></asp:Label>
                <asp:DropDownList ID="ddlType" runat="server" Width="152px">
                    <asp:ListItem Text="--Select Type--" Value="-1"></asp:ListItem>
                    <asp:ListItem Text="Positive(Dr)" Value="1"></asp:ListItem>
                    <asp:ListItem Text="Negative(Cr)" Value="2"></asp:ListItem>
                    <%--  <asp:ListItem Text="Expense" Value="1"></asp:ListItem>
                    <asp:ListItem Text="Liabilities" Value="2"></asp:ListItem>
                    <asp:ListItem Text="Assets" Value="3"></asp:ListItem>
                    <asp:ListItem Text="Income" Value="4"></asp:ListItem>--%>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" Display="None" ValidationGroup="HFamily"
                    runat="server" ControlToValidate="ddlType" InitialValue="-1" ErrorMessage="Type is required."></asp:RequiredFieldValidator>
            </p>
            <p>
                <asp:Label ID="Label1" runat="server" CssClass="lblingText" Text="Group"></asp:Label>
                <asp:DropDownList ID="ddlGroup" runat="server" Width="152px">
                    <asp:ListItem Text="--Select Group--" Value="-1"></asp:ListItem>
                    <%-- <asp:ListItem Text="Grouped" Value="1"></asp:ListItem>--%>
                    <%-- <asp:ListItem Text="Single" Value="2"></asp:ListItem>
                    <asp:ListItem Text="Inidividual" Value="3"></asp:ListItem>--%>
                    <asp:ListItem Text="SUM Company" Value="2"></asp:ListItem>
                    <asp:ListItem Text="SUM Branch" Value="1"></asp:ListItem>
                    <asp:ListItem Text="Employee wise" Value="3"></asp:ListItem>
                    <asp:ListItem Text="Branch Transaction" Value="4"></asp:ListItem>
                    <asp:ListItem Text="Voucher Balance" Value="5"></asp:ListItem>
                    <asp:ListItem Text="Loan Employee Wise" Value="6"></asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" Display="None" ValidationGroup="HFamily"
                    runat="server" ControlToValidate="ddlGroup" InitialValue="-1" ErrorMessage="Group is required."></asp:RequiredFieldValidator>
            </p>
             <p runat="server" id="divCategory" visible="false">
                <asp:Label ID="Label2" runat="server" CssClass="lblingText" Text="Category *"></asp:Label>
                <asp:DropDownList ID="ddlCategory" AppendDataBoundItems="true" DataTextField="Name" DataValueField="CategoryID" runat="server" Width="152px">
                    <asp:ListItem Text="--Select Category--" Value="-1"></asp:ListItem>
                    
                </asp:DropDownList>
               <%-- <asp:RequiredFieldValidator ID="reqdCategory" Visible="false" Display="None" ValidationGroup="HFamily"
                    runat="server" ControlToValidate="ddlCategory" InitialValue="-1" ErrorMessage="Category is required."></asp:RequiredFieldValidator>--%>
            </p>
            <p>
                <asp:Label ID="lblDeduct" runat="server" CssClass="lblingText" Text="Deduct from employee account"></asp:Label>
                <asp:CheckBox ID="chkDeductFromEmpAccount" runat="server" />
                <p>
                    <label class="lblingText">
                        &nbsp;</label>
                    <asp:Button ID="btnSave" CssClass="save" OnClientClick="valGroup='HFamily';return CheckValidation()"
                        runat="server" OnClick="btnSave_Click" Text="Save" ValidationGroup="HFamily" />
                    <asp:Button ID="btnCancel" CssClass="cancel" runat="server" Text="Cancel" OnClientClick="window.close()" />
                </p>
        </div>
    </div>
</asp:Content>
