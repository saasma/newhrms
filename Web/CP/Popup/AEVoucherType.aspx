﻿<%@ Page Language="C#" Title="Voucher Type" AutoEventWireup="true" MasterPageFile="~/Master/ForPopupPage.Master"
    CodeBehind="AEVoucherType.aspx.cs" Inherits="Web.CP.PP.AEVoucherType" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function closePopup() {
            // alert(window.opener.parentReloadCallbackFunction)
            if (isNonModalBrowser() && typeof (window.opener.refreshWindow)) {
                window.opener.refreshWindow();
                window.close();
            } else {
                window.returnValue = "Reload";
                window.close();
            }
        }
    </script>
    <style type="text/css">
        label.lblingText
        {
            width: 110px;
            padding-right: 10px;
            text-align: left;
            float: left;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
    <div class="popupHeader">
        <h2 class="headlinespop">
            Voucher Type
        </h2>
    </div>
    <div class="bevel marginal">
        <div class="fields paddpop">
            <p>
                <asp:Label ID="lbl2" runat="server" CssClass="lblingText" Text="Name"></asp:Label>
                <asp:TextBox CssClass="hrinput" ID="txtName" runat="server" Width="152px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" Display="None" ValidationGroup="HFamily"
                    runat="server" ControlToValidate="txtName" ErrorMessage="Name is required."></asp:RequiredFieldValidator>
            </p>
            <p>
                <asp:Label ID="Label7" runat="server" CssClass="lblingText" Text="Description"></asp:Label>
                <asp:TextBox CssClass="hrinput" ID="txtDescription" runat="server" Width="152px"></asp:TextBox>
                <br />
                eg:[Salary for #Month#]
            </p>
            <p>
                <label class="lblingText">
                    &nbsp;</label>
                <asp:Button ID="btnSave" CssClass="save" OnClientClick="valGroup='HFamily';return CheckValidation()"
                    runat="server" OnClick="btnSave_Click" Text="Save" ValidationGroup="HFamily" />
                <asp:Button ID="btnCancel" CssClass="cancel" runat="server" Text="Cancel" OnClientClick="window.close()" />
            </p>
        </div>
    </div>
</asp:Content>
