﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using BLL.Manager;
using DAL;
using BLL;
using Utils.Helper;
using Utils.Web;

namespace Web.CP.PP
{
    public partial class AEVoucherGroup : System.Web.UI.Page
    {
        HRManager hrMgr = new HRManager();
        int empId = 0;

        public int DocumentID
        {
            get
            {
                if (ViewState["ID"] != null)
                    return int.Parse(ViewState["ID"].ToString());
                return 0;
            }
            set
            {
                ViewState["ID"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                initialise();
            }
            
        }
        void initialise()
        {

            if (CommonManager.Setting.AccountingVoucherEnum != null && CommonManager.Setting.AccountingVoucherEnum == (int)AccountingVoucherTypeEnum.GLOBUS)
            {
                lbl2.Text += " / MainCode";
                lblTransCode.Text = "Dr";

                // group name
                lblGroupCode.Text += " / Cr";


                lblDeduct.Visible = false;
                chkDeductFromEmpAccount.Visible = false;

                ddlCategory.DataSource = VouccherManager.GetVoucherGroupCategoryList().Where(x => x.CategoryID != (int)GlobusVoucherCategoryIDEnum.MainVoucher).ToList();
                ddlCategory.DataBind();

                //reqdCategory.Visible = true;
                divCategory.Visible = true;

                reqdShortName.Enabled = true;
                globusDesc.Visible = true;

                ddlGroup.Items.Add(new ListItem { Value="7", Text="Net Salary" });
            }

            loadFamilyDetails();
        }
        public void loadFamilyDetails()
        {
            int familyId = UrlHelper.GetIdFromQueryString("ID");
            if (familyId != 0)
            {
                DAL.VoucherGroup family = null;

                family = VouccherManager.GetVoucher(familyId);
                

                this.DocumentID = familyId;
                if (family != null)
                {
                    ProcessFamily(family);
                    btnSave.Text = Resources.Messages.Update;
                }


            }
        }
        VoucherGroup ProcessFamily(VoucherGroup family)
        {
            if (family != null)
            {
                txtCode.Text = family.Code;
                txtName.Text = family.AccountGroupName;
                UIHelper.SetSelectedInDropDown(ddlType, family.Type);
                UIHelper.SetSelectedInDropDown(ddlGroup, family.Group);
                txtTransactionCode.Text = family.TransactionCode == null ? "" : family.TransactionCode;
                if (family.DeductFromEmpAccount != null)
                    chkDeductFromEmpAccount.Checked = family.DeductFromEmpAccount.Value;
                if (family.CategoryRef_ID != null)
                    UIHelper.SetSelectedInDropDown(ddlCategory, family.CategoryRef_ID.Value);

                txtShortNameDesc.Text = family.Desc == null ? "" : family.Desc;

            }
            else
            {
                family = new DAL.VoucherGroup();
                family.Code = txtCode.Text.Trim();
                family.AccountGroupName = txtName.Text.Trim();
                family.Type = int.Parse(ddlType.SelectedValue);
                family.Group = int.Parse(ddlGroup.SelectedValue);
                family.TransactionCode = txtTransactionCode.Text.Trim();
                family.DeductFromEmpAccount = chkDeductFromEmpAccount.Checked;
                family.CategoryRef_ID = int.Parse(ddlCategory.SelectedValue);
                family.Desc = txtShortNameDesc.Text;
                if (family.CategoryRef_ID == -1)
                    family.CategoryRef_ID = null;
                return family;
            }
            return null;
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                DAL.VoucherGroup Family = ProcessFamily(null);
                if (this.DocumentID != 0)
                {
                    Family.VoucherGroupID = this.DocumentID;
                    bool status = VouccherManager.UpdateVoucherGroup(Family);

                    if (status == false)
                    {
                        JavascriptHelper.DisplayClientMsg("Group name already exists.", this);
                        return;
                    }

                    JavascriptHelper.DisplayClientMsg("Group is updated.", this, "closePopup();\n");

                }
                else
                {
                    

                    bool status = VouccherManager.SaveVoucherGorup(Family);

                    if (status == false)
                    {
                        JavascriptHelper.DisplayClientMsg("Group name already exists.", this);
                        return;
                    }

                    JavascriptHelper.DisplayClientMsg("Group is saved.", this, "closePopup();\n");
                    btnSave.Text = Resources.Messages.Update;
                }
            }
            else
            {
                JavascriptHelper.DisplayClientMsg("Validation fail.", this);
            }
        }
    }
}
