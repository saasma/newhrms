﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using BLL.Manager;
using DAL;
using BLL;
using Utils.Helper;
using Utils.Web;
using Ext.Net;

namespace Web.CP.PP
{
    public partial class ReportHeaderFooterEdit : System.Web.UI.Page
    {
        HRManager hrMgr = new HRManager();
        int empId = 0;

        public int DocumentID
        {
            get
            {
                if (ViewState["ID"] != null)
                    return int.Parse(ViewState["ID"].ToString());
                return 0;
            }
            set
            {
                ViewState["ID"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                initialise();
            }

        }
        void initialise()
        {

            loadFamilyDetails();
        }



        public void loadFamilyDetails()
        {
            int familyId = UrlHelper.GetIdFromQueryString("ID");
            if (familyId != 0)
            {
                header.InnerHtml += ((ReportHeaderFooterEnum)familyId).ToString();

                this.DocumentID = familyId;
                ReportHeaderFooter entity = VouccherManagerNew.GetReportHeaderFooter(familyId);
                if (entity != null)
                {
                    htmlHeader.Text = entity.Header;
                    htmlFooter.Text = entity.Footer;
                    btnSave.Text = Resources.Messages.Update;
                }
                else
                {
                    ReportHeaderFooterEnum type = (ReportHeaderFooterEnum)this.DocumentID;
                    switch (type)
                    {
                        case ReportHeaderFooterEnum.CITReport:
                            htmlHeader.Text = reportCITHeader.InnerHtml;
                            htmlFooter.Text = reportCITFooter.InnerHtml;
                            break;
                    }
                }

            }
        }

        protected void btnDelete_Click(object sender, DirectEventArgs e)
        {
            ReportHeaderFooter entity = new ReportHeaderFooter();
            entity.ReportId = this.DocumentID;
            entity.Header = htmlHeader.Text;
            entity.Footer = htmlFooter.Text;



            VouccherManagerNew.DeleteReportHeaderFooter(entity);


            NewMessage.ShowNormalMessage("Report content deleted.", "closePopup();");
            //JavascriptHelper.DisplayClientMsg("Report content is saved.", this, "closePopup();\n");
            btnSave.Text = Resources.Messages.Update;
        }
        protected void btnSave_Click(object sender, DirectEventArgs e)
        {
            //if (Page.IsValid)
            {
                ReportHeaderFooter entity = new ReportHeaderFooter();
                entity.ReportId = this.DocumentID;
                entity.Header = htmlHeader.Text;
                entity.Footer = htmlFooter.Text;



                VouccherManagerNew.SaveReportHeaderFooter(entity);


                NewMessage.ShowNormalMessage("Report content saved.", "closePopup();");
                //JavascriptHelper.DisplayClientMsg("Report content is saved.", this, "closePopup();\n");
                btnSave.Text = Resources.Messages.Update;

            }
            //else
            //{
            //    JavascriptHelper.DisplayClientMsg("Validation fail.", this);
            //}
        }
    }
}
