﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using BLL.Manager;
using DAL;
using BLL;
using Utils.Helper;
using Utils.Web;
using System.Collections.Generic;

namespace Web.CP.PP
{
    public partial class AESalaryHeadAssociation : System.Web.UI.Page
    {
        HRManager hrMgr = new HRManager();
        int empId = 0;



        public int Type
        {
            get
            {
                return int.Parse(Request.QueryString["type"]);
            }
        }
        public int SourceId
        {
            get
            {
                return int.Parse(Request.QueryString["sourceId"]);
            }
            
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                initialise();
            }
            
        }
        void initialise()
        {

            ddlGroup.DataSource = VouccherManager.GetVoucherGroupList();
            ddlGroup.DataBind();

            ddlSalaryHead.DataSource =
                CalculationManager.GetHeaderListForVoucher();
            ddlSalaryHead.DataBind();
          
            loadFamilyDetails();
        }
        public void loadFamilyDetails()
        {

            DAL.VoucherHeadGroup family = null;

            family = VouccherManager.GetVoucherHead(this.Type,this.SourceId);



            if (family != null)
            {
                ProcessFamily(family);
                btnSave.Text = Resources.Messages.Update;
            }
            else
            {
                UIHelper.SetSelectedInDropDown(ddlSalaryHead, this.Type + ":" + this.SourceId);
            }


        }
        VoucherHeadGroup ProcessFamily(VoucherHeadGroup family)
        {
            if (family != null)
            {

                UIHelper.SetSelectedInDropDown(ddlSalaryHead, family.TypeSourceId);
                UIHelper.SetSelectedInDropDown(ddlGroup, family.VoucherGroupID);


            }
            else
            {
                family = new DAL.VoucherHeadGroup();

                string[] values = ddlSalaryHead.SelectedValue.Split(new char[] { ':'});

                family.Type = int.Parse(values[0]);
                family.SourceId = int.Parse(values[1]);
                family.VoucherGroupID = int.Parse(ddlGroup.SelectedValue);
                family.Name = ddlSalaryHead.SelectedItem.Text;
                return family;
            }
            return null;
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                DAL.VoucherHeadGroup Family = ProcessFamily(null);

                //if (this.DocumentID != 0)
                {
                    //Family.ID = this.DocumentID;
                    Family.Type = this.Type;
                    Family.SourceId = this.SourceId;
                    VouccherManager.UpdateHead(Family);
                    JavascriptHelper.DisplayClientMsg("Head is updated.", this, "closePopup();\n");

                }
                //else
                //{
                    

                //    VoucherManager.SaveHead(Family);
                //    JavascriptHelper.DisplayClientMsg("Head is saved.", this, "closePopup();\n");
                //    btnSave.Text = Resources.Messages.Update;
                //}
            }
            else
            {
                JavascriptHelper.DisplayClientMsg("Validation fail.", this);
            }
        }
    }
}
