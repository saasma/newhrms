﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using BLL.Manager;
using DAL;
using BLL;
using Utils.Helper;
using Utils.Web;
using System.Collections.Generic;

namespace Web.CP.PP
{
    public partial class AEVoucherGroupNew : System.Web.UI.Page
    {
        HRManager hrMgr = new HRManager();
        int empId = 0;

        public int DocumentID
        {
            get
            {
                if (ViewState["ID"] != null)
                    return int.Parse(ViewState["ID"].ToString());
                return 0;
            }
            set
            {
                ViewState["ID"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                initialise();
            }
            
        }
        void initialise()
        {
          
           

            ddlVoucherType.DataSource = VouccherManagerNew.GetVoucherTypeList();
            ddlVoucherType.DataBind();

            List<KeyValue> list = (new JobStatus()).GetMembers();
            list.RemoveAt(0);
            ddlApplicableList.DataSource = list;
            ddlApplicableList.DataBind();


            List<BLevel> allLevels = NewPayrollManager.GetAllParentLevelList();
            chkLevels.DataSource = allLevels;
            chkLevels.DataBind();

            List<CalcGetHeaderListResult> incomeDeductionList = CalculationManager.GetHeaderListForVoucher();
            chkInocmeDeductionList.DataSource = incomeDeductionList.ToList();
            chkInocmeDeductionList.DataBind();

            loadFamilyDetails();

        }

        public void chkLevelFilter_Selection(object sender,EventArgs e)
        {
            chkLevels.Enabled = chkLevelFilter.Checked;
        }

        public void loadFamilyDetails()
        {
            int familyId = UrlHelper.GetIdFromQueryString("ID");
            if (familyId != 0)
            {
                DAL.NIBLVoucherGroup family = null;

                family = VouccherManagerNew.GetVoucher(familyId);
                

                this.DocumentID = familyId;
                if (family != null)
                {
                    ProcessFamily(family);
                    btnSave.Text = Resources.Messages.Update;
                }


            }
        }
        NIBLVoucherGroup ProcessFamily(NIBLVoucherGroup family)
        {
            if (family != null)
            {
                txtCode.Text = family.GroupCode;
                txtName.Text = family.GroupName;
                //UIHelper.SetSelectedInDropDown(ddlType, family.TransactionType);
                UIHelper.SetSelectedInDropDown(ddlGroup, family.Group);
                txtTransactionCode.Text = family.TransactionCode == null ? "" : family.TransactionCode;
                UIHelper.SetSelectedInDropDown(ddlVoucherType, family.VoucherTypeRef_ID);

                txtCardDepartmentCode.Text = family.NIBLCardDepartmentGroupCode;
                //txtDescription.Text = family.Description;
                chkLevelFilter.Checked = (family.EnableLevelFilter == null || family.EnableLevelFilter == false ? false : true);

                if (chkLevelFilter.Checked)
                    chkLevels.Enabled = true;

                foreach (ListItem item in chkInocmeDeductionList.Items)
                {

                    if (family.NIBLVoucherHeads.Any(x => x.TypeSourceID.Equals(item.Value)))
                    {
                        item.Selected = true;
                    }
                }

                foreach (ListItem item in ddlApplicableList.Items)
                {

                    if (family.NIBLVoucherStatus.Any(x => x.Status.ToString().Equals(item.Value)))
                    {
                        item.Selected = true;
                    }
                }

                foreach (ListItem item in chkLevels.Items)
                {

                    if (family.NIBLVoucherLevels.Any(x => x.LevelID.ToString().Equals(item.Value)))
                    {
                        item.Selected = true;
                    }
                }
            }
            else
            {
                family = new DAL.NIBLVoucherGroup();
                family.GroupCode = txtCode.Text.Trim();
                family.GroupName = txtName.Text.Trim();
                //family.TransactionType = int.Parse(ddlType.SelectedValue);
                family.Group = int.Parse(ddlGroup.SelectedValue);
                family.TransactionCode = txtTransactionCode.Text.Trim();
                family.VoucherTypeRef_ID = int.Parse(ddlVoucherType.SelectedValue);
                family.NIBLCardDepartmentGroupCode = txtCardDepartmentCode.Text.Trim();
                //family.Description = txtDescription.Text.Trim();
                family.EnableLevelFilter = chkLevelFilter.Checked;

                foreach (ListItem item in chkInocmeDeductionList.Items)
                {

                    if (item.Selected)
                    {
                        string[] values = item.Value.Split(new char[] { ':' });
                        family.NIBLVoucherHeads.Add(new NIBLVoucherHead
                        {
                            Name = item.Text,
                            Type = int.Parse(values[0]),
                            SourceId = int.Parse(values[1])
                        });
                    }
                }

                foreach (ListItem item in ddlApplicableList.Items)
                {

                    if (item.Selected)
                    {
                        family.NIBLVoucherStatus.Add(new NIBLVoucherStatus
                         {
                             Status = int.Parse(item.Value)
                         });
                    }
                }

                if (family.EnableLevelFilter.Value)
                {
                    foreach (ListItem item in chkLevels.Items)
                    {

                        if (item.Selected)
                        {
                            family.NIBLVoucherLevels.Add(new NIBLVoucherLevel
                            {
                                LevelID = int.Parse(item.Value)
                            });
                        }
                    }

                }
                return family;
            }
            return null;
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                DAL.NIBLVoucherGroup Family = ProcessFamily(null);
                if (this.DocumentID != 0)
                {
                    Family.VoucherGroupID = this.DocumentID;
                    bool status = VouccherManagerNew.UpdateVoucherGroup(Family);

                    if (status == false)
                    {
                        JavascriptHelper.DisplayClientMsg("Group name already exists.", this);
                        return;
                    }

                    JavascriptHelper.DisplayClientMsg("Group is updated.", this, "closePopup();\n");

                }
                else
                {


                    bool status = VouccherManagerNew.SaveVoucherGorup(Family);

                    if (status == false)
                    {
                        JavascriptHelper.DisplayClientMsg("Group name already exists.", this);
                        return;
                    }

                    JavascriptHelper.DisplayClientMsg("Group is saved.", this, "closePopup();\n");
                    btnSave.Text = Resources.Messages.Update;
                }
            }
            else
            {
                JavascriptHelper.DisplayClientMsg("Validation fail.", this);
            }
        }
    }
}
