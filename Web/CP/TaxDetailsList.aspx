<%@ Page Title="Tax Details List" Language="C#" EnableViewState="true" MasterPageFile="~/Master/Details.Master"
    AutoEventWireup="true" CodeBehind="TaxDetailsList.aspx.cs" Inherits="Web.TaxDetailsList" %>
    <%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<%@ Import Namespace="BLL.Manager" %>
<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/PagingCtl.ascx" TagName="PagingCtl" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">

        var skipLoadingCheck = true;
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
    <h3 style="margin-top: 10px" runat="server" id="title">
        Tax Calculation List
    </h3>
    <div class="attribute">
        <table cellpadding="3" cellspacing="0">
            <tr>
                <td class="filterHeader" runat="server" id="rowPayrollFrom1">
                    <strong>Payroll</strong>
                </td>
                <td rowspan="2" valign="bottom">
                    <asp:Button ID="btnLoad" Style='float: left' CssClass="load" runat="server" Text="Load" />
                </td>
            </tr>
            <tr>
                <td runat="server" id="rowPayrollFrom2">
                    <asp:DropDownList ID="ddlPayrollFromMonth" runat="server">
                    </asp:DropDownList>
                    <asp:DropDownList ID="ddlPayrollFromYear" runat="server">
                    </asp:DropDownList>
                </td>
            </tr>
        </table>
    </div>
    <div class="clear gridBlock">
         <uc2:MsgCtl ID="divMsgCtl" EnableViewState="false" Hide="true" runat="server" />
        <uc2:WarningCtl ID="divWarningMsg" EnableViewState="false" Hide="true" runat="server" />
        <cc2:EmptyDisplayGridView EnableViewState="false" CssClass="tableLightColor" UseAccessibleHeader="true" ShowHeaderWhenEmpty="True"
            ID="gvEmployeeIncome" runat="server" DataKeyNames="EIN" AutoGenerateColumns="False"
            CellPadding="4" GridLines="None" ShowFooterWhenEmpty="False" OnDataBound="gvw_DataBound">
            <RowStyle BackColor="#E3EAEB" />
            <Columns>
                <asp:BoundField DataField="EIN" HeaderText="EIN"></asp:BoundField>
                <asp:TemplateField HeaderText="Name" HeaderStyle-Wrap="false" ItemStyle-Wrap="false"
                    HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <asp:Label runat="server" Width="140" Text='<%# Eval("Name") %>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="Branch" HeaderStyle-Wrap="false" ItemStyle-Wrap="false"
                    HeaderStyle-HorizontalAlign="Left" HeaderText="Branch"></asp:BoundField>
                <asp:BoundField DataField="Department" HeaderStyle-Wrap="false" ItemStyle-Wrap="false"
                    HeaderStyle-HorizontalAlign="Left" HeaderText="Department"></asp:BoundField>
                <asp:BoundField DataField="Designation" HeaderStyle-Wrap="false" ItemStyle-Wrap="false"
                    HeaderStyle-HorizontalAlign="Left" HeaderText="Designation"></asp:BoundField>
                <asp:BoundField DataField="Sex" HeaderStyle-Wrap="false" ItemStyle-Wrap="false" HeaderStyle-HorizontalAlign="Left"
                    HeaderText="Sex"></asp:BoundField>
                <asp:BoundField DataField="TaxStatus" HeaderStyle-Wrap="false" ItemStyle-Wrap="false"
                    HeaderStyle-HorizontalAlign="Left" HeaderText="Tax Status"></asp:BoundField>
                <asp:BoundField DataField="YearlyGrossIncome" HeaderStyle-Wrap="false" ItemStyle-Wrap="false"
                    HeaderStyle-HorizontalAlign="Left" HeaderText="Yearly Gross Income" DataFormatString="{0:N2}" />
                <asp:BoundField HeaderStyle-Wrap="false" ItemStyle-Wrap="false" DataField="PastPF"
                    DataFormatString="{0:N2}" HeaderStyle-HorizontalAlign="Left" HeaderText="Past PF" />
                <asp:BoundField HeaderStyle-Wrap="false" ItemStyle-Wrap="false" DataField="PastCIT"
                    HeaderStyle-HorizontalAlign="Left" HeaderText="Past CIT" DataFormatString="{0:N2}" />
                <asp:BoundField HeaderStyle-Wrap="false" ItemStyle-Wrap="false" DataField="CurrentPF"
                    HeaderStyle-HorizontalAlign="Left" HeaderText="Current PF" DataFormatString="{0:N2}" />
                <asp:BoundField HeaderStyle-Wrap="false" ItemStyle-Wrap="false" DataField="CurrentCIT"
                    HeaderStyle-HorizontalAlign="Left" HeaderText="Current CIT" DataFormatString="{0:N2}" />
                <asp:BoundField HeaderStyle-Wrap="false" ItemStyle-Wrap="false" DataField="ForecastPF"
                    HeaderStyle-HorizontalAlign="Left" HeaderText="Forecast PF" DataFormatString="{0:N2}" />
                <asp:BoundField HeaderStyle-Wrap="false" ItemStyle-Wrap="false" DataField="ForecastCIT"
                    HeaderStyle-HorizontalAlign="Left" HeaderText="Forecast CIT" DataFormatString="{0:N2}" />
                <asp:BoundField HeaderStyle-Wrap="false" ItemStyle-Wrap="false" DataField="OneThird"
                    HeaderStyle-HorizontalAlign="Left" HeaderText="1/3 Calculation" DataFormatString="{0:N2}" />
                <asp:BoundField HeaderStyle-Wrap="false" ItemStyle-Wrap="false" DataField="Actual"
                    HeaderStyle-HorizontalAlign="Left" HeaderText="Actual" DataFormatString="{0:N2}" />
                <asp:BoundField HeaderStyle-Wrap="false" ItemStyle-Wrap="false" DataField="Allowable"
                    HeaderStyle-HorizontalAlign="Left" HeaderText="Maximum Limit" DataFormatString="{0:N2}" />
                <asp:BoundField HeaderStyle-Wrap="false" ItemStyle-Wrap="false" DataField="DeductForTax"
                    HeaderStyle-HorizontalAlign="Left" HeaderText="Deducted For Tax" DataFormatString="{0:N2}" />
                <asp:BoundField HeaderStyle-Wrap="false" ItemStyle-Wrap="false" DataField="RemoteArea"
                    HeaderStyle-HorizontalAlign="Left" HeaderText="Remote Area" DataFormatString="{0:N2}" />
                <asp:BoundField HeaderStyle-Wrap="false" ItemStyle-Wrap="false" DataField="InsuranceDeduction"
                    HeaderStyle-HorizontalAlign="Left" HeaderText="Insurance Deduction" DataFormatString="{0:N2}" />
                 <asp:BoundField HeaderStyle-Wrap="false" ItemStyle-Wrap="false" DataField="Handicapped"
                    HeaderStyle-HorizontalAlign="Left" HeaderText="Handicapped Deduction" DataFormatString="{0:N2}" />
                <asp:BoundField HeaderStyle-Wrap="false" ItemStyle-Wrap="false" DataField="TaxableAmount"
                    HeaderStyle-HorizontalAlign="Left" HeaderText="Taxable Income" DataFormatString="{0:N2}" />
                <asp:BoundField HeaderStyle-Wrap="false" ItemStyle-Wrap="false" DataField="OnePercentAmount"
                    HeaderStyle-HorizontalAlign="Left" HeaderText="1% Amount" DataFormatString="{0:N2}" />
                <asp:BoundField HeaderStyle-Wrap="false" ItemStyle-Wrap="false" DataField="FifteenPercentAmount"
                    HeaderStyle-HorizontalAlign="Left" HeaderText="15% Amount" DataFormatString="{0:N2}" />
                <asp:BoundField HeaderStyle-Wrap="false" ItemStyle-Wrap="false" DataField="TwentyFivePercentAmount"
                    HeaderStyle-HorizontalAlign="Left" HeaderText="25% Amount" DataFormatString="{0:N2}" />
                <asp:BoundField HeaderStyle-Wrap="false" ItemStyle-Wrap="false" DataField="FourtyPercentAmount"
                    HeaderStyle-HorizontalAlign="Left" HeaderText="40% Amount" DataFormatString="{0:N2}" />
                <asp:BoundField HeaderStyle-Wrap="false" ItemStyle-Wrap="false" DataField="OnePercentTax"
                    HeaderStyle-HorizontalAlign="Left" HeaderText="1% Tax" DataFormatString="{0:N2}" />
                <asp:BoundField HeaderStyle-Wrap="false" ItemStyle-Wrap="false" DataField="FifteenPercentTax"
                    HeaderStyle-HorizontalAlign="Left" HeaderText="15% Tax" DataFormatString="{0:N2}" />
                <asp:BoundField HeaderStyle-Wrap="false" ItemStyle-Wrap="false" DataField="TwentyFivePercentTax"
                    HeaderStyle-HorizontalAlign="Left" HeaderText="25% Tax" DataFormatString="{0:N2}" />
                <asp:BoundField HeaderStyle-Wrap="false" ItemStyle-Wrap="false" DataField="FourtyPercentTax"
                    HeaderStyle-HorizontalAlign="Left" HeaderText="40% Tax" DataFormatString="{0:N2}" />
                <asp:BoundField HeaderStyle-Wrap="false" ItemStyle-Wrap="false" DataField="TotalTaxInTheYear"
                    HeaderStyle-HorizontalAlign="Left" HeaderText="Total Tax" DataFormatString="{0:N2}" />
                <asp:BoundField HeaderStyle-Wrap="false" ItemStyle-Wrap="false" DataField="FemaleRebate"
                    HeaderStyle-HorizontalAlign="Left" HeaderText="Female Rebate" DataFormatString="{0:N2}" />
                <asp:BoundField HeaderStyle-Wrap="false" ItemStyle-Wrap="false" DataField="TaxPayable"
                    HeaderStyle-HorizontalAlign="Left" HeaderText="Tax Payable" DataFormatString="{0:N2}" />
                <asp:BoundField HeaderStyle-Wrap="false" ItemStyle-Wrap="false" DataField="TotalTaxPaidInPastMonthAndAddOnTax"
                    HeaderStyle-HorizontalAlign="Left" HeaderText="Total Tax Paid in the Past Months"
                    DataFormatString="{0:N2}" />
                <asp:BoundField HeaderStyle-Wrap="false" ItemStyle-Wrap="false" DataField="RemainingTax"
                    HeaderStyle-HorizontalAlign="Left" HeaderText="Remaining Tax" DataFormatString="{0:N2}" />
                <asp:BoundField HeaderStyle-Wrap="false" ItemStyle-Wrap="false" DataField="MonthlyTax"
                    HeaderStyle-HorizontalAlign="Left" HeaderText="Monthly Tax" DataFormatString="{0:N2}" />
                <asp:BoundField HeaderStyle-Wrap="false" ItemStyle-Wrap="false" DataField="Adjustment"
                    HeaderStyle-HorizontalAlign="Left" HeaderText="Adjustment" DataFormatString="{0:N2}" />
                <asp:BoundField HeaderStyle-Wrap="false" ItemStyle-Wrap="false" DataField="Adjusted"
                    HeaderStyle-HorizontalAlign="Left" HeaderText="Adjusted" DataFormatString="{0:N2}" />
            </Columns>
            <RowStyle CssClass="odd" />
            <AlternatingRowStyle CssClass="even" />
            <EmptyDataTemplate>
                <b>No employee list. </b>
            </EmptyDataTemplate>
        </cc2:EmptyDisplayGridView>
    </div>
    <div class="buttonsDiv">
        <asp:Button ID="btnUpdate" CssClass="excel" runat="server" Text="Export" OnClick="btnUpdate_Click" />
    </div>
</asp:Content>
