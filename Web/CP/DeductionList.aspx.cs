﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using Utils.Helper;

namespace Web.CP
{
    public partial class DeductionList : System.Web.UI.Page
    {
        PayManager payMgr = new PayManager();
        int employeeId = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            string eventTarget = Request.Form["__EVENTTARGET"];
            employeeId = UrlHelper.GetIdFromQueryString("Eid");
            if (!IsPostBack)
            {
                Initialise();
            }
            else if (eventTarget != null && eventTarget.Equals("Reload"))
            {
                Initialise();
            }

            JavascriptHelper.AttachPopUpCode(Page, "popupCreateNew", "AEDeduction.aspx", 600, 600);
        }

        void Initialise()
        {

            int empId = employeeId;
            lstDeductionList.DataSource = payMgr.GetDeductionListForEmp(empId).OrderBy(x => x.Title).ToList();
            lstDeductionList.DataBind();


        }

        protected void btnOk_Click(object sender, EventArgs e)
        {

            if (Page.IsValid)
            {

                bool result = false;

                foreach (ListItem item in lstDeductionList.Items)
                {
                    if (item.Selected)
                    {
                        result = payMgr.AddDeductionToEmployee(int.Parse(item.Value),
                    employeeId, rdbListAddTo.Items[1].Selected, SessionManager.CurrentCompanyId);
                    }
                }
                
                

                if (result)
                {
                    string msg;
                    if (rdbListAddTo.Items[1].Selected)
                        msg = "Deduction added to all employees.";
                    else
                        msg = "Deduction added to the employee.";

                    JavascriptHelper.DisplayClientMsg(msg, this, "closePopup();");
                }
            }
        }
    }
}
