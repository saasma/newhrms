﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using Utils.Helper;
using System.IO;
using Utils;
using Utils.Security;
using Utils.Calendar;

namespace Web.CP
{
    public partial class ServiceHistoryList : BasePage
    {
        bool HistoryServicesOpening = false;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                ColumnTeam.Visible = IsTeamColumnVisible();
                Initialize();

                hdnIsEng.Text = UserManager.IsEnglishDatePreferredInNepaliDateSys().ToString().ToLower();

            }
            JavascriptHelper.AttachNonDialogPopUpCode(Page, "shiftPopup", "../ExcelWindow/ServiceEventExcel.aspx", 550, 500);
            JavascriptHelper.AttachPopUpCode(Page, "popup", "BranchTransferLeaveSetting.aspx", 1200, 700);
            JavascriptHelper.AttachNonDialogPopUpCode(Page, "GetPopUp", "../ExcelWindow/OpeningServicesImport.aspx", 550, 550);

            HistoryServicesOpening = string.IsNullOrEmpty(Request.QueryString["opening"]) ? false : bool.Parse(Request.QueryString["opening"]);
            if (HistoryServicesOpening)
                lnkOpeningServicesExcelImport.Visible = true;
        }

        private void Initialize()
        {
            X.Js.Call("searchList");

            List<HR_Service_Event> eventList = CommonManager.GetServieEventTypesExcludingRetirementType();
            if (eventList.Count > 0)
            {
                cmbEventType.Store[0].DataSource = eventList;
                cmbEventType.Store[0].DataBind();
            }
            else
                reqdEventType.Enabled = false;

            cmbEventFilter.Store[0].DataSource = eventList;
            cmbEventFilter.Store[0].DataBind();

            cmbBranch.GetStore().DataSource = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId);
            cmbBranch.GetStore().DataBind();

            cmbSubDepartment.GetStore().DataSource = ListManager.GetSubDepartments();
            cmbSubDepartment.GetStore().DataBind();

            cmbLevel.Store[0].DataSource = NewPayrollManager.GetAllParentLevelList();
            cmbLevel.Store[0].DataBind();

            JobStatus status = new JobStatus();
            List<KeyValue> list = status.GetMembers();
            list.RemoveAt(0);
            cmbStatus.Store[0].DataSource = list;
            cmbStatus.Store[0].DataBind();

            WBranchTransfer.Hide();
            if (SessionManager.CurrentCompany.IsEnglishDate)
                colFromDate.Hide();

           
        }

        private void Clear()
        {
            txtLetterNumber.Text = "";
            calLetterDate.Text = "";
            cmbBranch.Clear();
            cmbDepartment.Clear();
            cmbSubDepartment.Clear();
            //txtSpecialResponsibility.Text = "";
            //calDepartureDate.Text = "";
            calFromDate.Text = "";
            txtNote.Text = "";

            cmbEmployee.Clear();
            cmbEmployee.Enable();
            lblEName.Text = "";
            lblEBranch.Text = "";
            //lblEDepartment.Text = "";
            lblESince.Text = "";
            lblInTime.Text = "";

            image1.ImageUrl = Page.ResolveUrl(Web.Helper.WebHelper.GetEmployeeNoImage(0));
        }

        private void LoadDepartment(int branchId)
        {
            cmbDepartment.GetStore().DataSource = DepartmentManager.GetAllDepartmentsByBranch(branchId);
            cmbDepartment.GetStore().DataBind();
        }

        protected void Branch_Select(object sender, DirectEventArgs e)
        {
            cmbDepartment.ClearValue();
            LoadDepartment(int.Parse(cmbBranch.SelectedItem.Value));
        }

        protected void btnNewTransfer_Click(object sender, DirectEventArgs e)
        {
            Clear();
            hdnBranchDepartmentId.Text = "";
            hdnEmployeeId.Text = "";
            WBranchTransfer.Show();

            cmbEmployee.ClearValue();
            cmbEventType.ClearValue();
            calFromDate.Text = "";
            txtLetterNumber.Text = "";
            calLetterDate.Text = "";
            cmbBranch.ClearValue();
            cmbDepartment.ClearValue();
            cmbLevel.ClearValue();
            cmbDesignation.ClearValue();
            cmbStatus.ClearValue();
            txtNote.Text = "";

        }

        protected void Store_ReadData(object sender, StoreReadDataEventArgs e)
        {
            int totalRecords = 0;
            int? total = 0;
            DateTime? fromDate = null;
            DateTime? toDate = null;

            int? eventType = null;

            if (txtFromDate.SelectedDate != DateTime.MinValue)
                fromDate = txtFromDate.SelectedDate;

            if (txtToDate.SelectedDate != DateTime.MinValue)
                toDate = txtToDate.SelectedDate;

            int employeeId = -1;
            string employeeSearch = "";

            int.TryParse(cmbEmpSearch.SelectedItem.Value, out employeeId);

            if (employeeId == 0)
                employeeId = -1;

            if (employeeId == -1)
            {
                if (!string.IsNullOrEmpty(cmbEmpSearch.Text))
                    employeeSearch = cmbEmpSearch.Text.Trim();
            }

            if (cmbEventFilter.SelectedItem != null && cmbEventFilter.SelectedItem.Value != null)
                eventType = int.Parse(cmbEventFilter.SelectedItem.Value);

            List<GetServiceHistoryNewResult> list = BranchManager.GetNewServiceHistory
                (e.Start, int.Parse(cmbPageSize.SelectedItem.Value), ref totalRecords, employeeSearch, employeeId, fromDate, toDate, eventType);

            e.Total = totalRecords;

            if (list.Any())
                storeEmpList.DataSource = list;
            storeEmpList.DataBind();

        }

        protected Field OnCreateFilterableField(object sender, ColumnBase column, Field defaultField)
        {
            if (column.DataIndex == "BranchDepartmentId")
            {
                ((TextField)defaultField).Icon = Icon.Magnifier;
            }

            return defaultField;
        }


        // Return the number of years, months, days, hours, minutes, seconds,
        // and milliseconds you need to add to from_date to get to_date.
        private void GetElapsedTime(DateTime from_date, DateTime to_date,
            out int years, out int months, out int days, out int hours,
            out int minutes, out int seconds, out int milliseconds)
        {
            // If from_date > to_date, switch them around.
            if (from_date > to_date)
            {
                GetElapsedTime(to_date, from_date,
                    out years, out months, out days, out hours,
                    out minutes, out seconds, out milliseconds);
                years = -years;
                months = -months;
                days = -days;
                hours = -hours;
                minutes = -minutes;
                seconds = -seconds;
                milliseconds = -milliseconds;
            }
            else
            {
                // Handle the years.
                years = to_date.Year - from_date.Year;

                // See if we went too far.
                DateTime test_date = from_date.AddMonths(12 * years);
                if (test_date > to_date)
                {
                    years--;
                    test_date = from_date.AddMonths(12 * years);
                }

                // Add months until we go too far.
                months = 0;
                while (test_date <= to_date)
                {
                    months++;
                    test_date = from_date.AddMonths(12 * years + months);
                }
                months--;

                // Subtract to see how many more days,
                // hours, minutes, etc. we need.
                from_date = from_date.AddMonths(12 * years + months);
                TimeSpan remainder = to_date - from_date;
                days = remainder.Days;
                hours = remainder.Hours;
                minutes = remainder.Minutes;
                seconds = remainder.Seconds;
                milliseconds = remainder.Milliseconds;
            }
        }

        private void EmployeeDetails(int employeeId)
        {
            EEmployee emp = EmployeeManager.GetEmployeeById(employeeId);

            bool hasPhoto = false;
            if (emp.HHumanResources.Count > 0)
            {
                if (!string.IsNullOrEmpty(emp.HHumanResources[0].UrlPhotoThumbnail))
                {
                    image1.ImageUrl = Page.ResolveUrl(Config.UploadLocation + "/" + emp.HHumanResources[0].UrlPhotoThumbnail);
                    if (File.Exists(Server.MapPath(image1.ImageUrl)))
                        hasPhoto = true;
                }
            }
            if (!hasPhoto)
            {
                image1.ImageUrl = Page.ResolveUrl(Web.Helper.WebHelper.GetEmployeeNoImage(emp.EmployeeId));
            }

            //lblEININo.Text = "EIN : " + emp.EmployeeId + ", I No : " + emp.EHumanResources[0].IdCardNo;

            lblEName.Text = emp.Title + " " + emp.Name + ", " + "EIN : " + emp.EmployeeId + ", I No : " + emp.EHumanResources[0].IdCardNo;
            lblEBranch.Text = emp.Branch.Name + ", " + emp.Department.Name;

            int years, months, days, hours;
            int minutes, seconds, milliseconds;
            ServicePeroid firstStatus = EmployeeManager.GetServicePeroidDate(emp.EmployeeId);
            GetElapsedTime(firstStatus.FromDateEng, BLL.BaseBiz.GetCurrentDateAndTime(), out years, out months, out days, out hours, out minutes, out seconds, out milliseconds);



            lblESince.Text = "Since " +
                (IsEnglish ? firstStatus.FromDateEng.ToString("yyyy-MMM-dd") : CustomDate.GetCustomDateFromString(firstStatus.FromDate, IsEnglish).ToStringMonthEng() + " (" + firstStatus.FromDateEng.ToString("yyyy-MMM-dd") + ")");
            lblInTime.Text = string.Format("For {0} years {1} months {2} days",
                years, months, days);
        }

        protected void btnSelectEmp_Click(object sender, DirectEventArgs e)
        {
            int employeeId = int.Parse(cmbEmployee.SelectedItem.Value);
            // hdnEmployeeId.Text = employeeId.ToString();

            EEmployee emp = EmployeeManager.GetEmployeeById(employeeId);

            if (emp == null)
            {
                buttonsBar.Visible = false;
                NewMessage.ShowWarningMessage("Please select an employee.");
                return;
            }
            else
                buttonsBar.Visible = true;



            block.Visible = true;

            EmployeeDetails(employeeId);

            LoadLastServiceHistory(employeeId);
        }

        public void LoadLastServiceHistory(int empId)
        {

            GetEmployeeCurrentBranchDepartmentResult branchDep =
               BLL.BaseBiz.PayrollDataContext.GetEmployeeCurrentBranchDepartment(DateTime.Now.Date, empId)
               .FirstOrDefault();

            int branchId = branchDep.BranchId.Value;
            int departmentId = branchDep.DepartmentId.Value;
            int statusId = BLL.BaseBiz.PayrollDataContext.GetEmployeeStatusForDate(DateTime.Now.Date, empId).Value;
            int designationId = BLL.BaseBiz.PayrollDataContext.GetEmployeeCurrentPositionForDate(DateTime.Now.Date, empId).Value;
            int levelId = CommonManager.GetLevelIDForDesignation(designationId);


            {
                cmbBranch.SetValue(branchId.ToString());
                LoadDepartment(branchId);
                cmbDepartment.SetValue(departmentId.ToString());

                cmbStatus.SetValue(statusId.ToString());

                cmbLevel.SetValue(levelId.ToString());

                cmbDesignation.GetStore().DataSource = NewHRManager.GetDesignationsByLevel(levelId).ToList();
                cmbDesignation.GetStore().DataBind();

                cmbDesignation.SetValue(designationId.ToString());
            }
        }

        protected void btnEdit_Click(object sender, DirectEventArgs e)
        {
            int eventSourceID = int.Parse(hdnBranchDepartmentId.Text);
            EmployeeServiceHistory history = CommonManager.GetServiceHistory(eventSourceID);
            int employeeId = history.EmployeeId;
            EEmployee emp = EmployeeManager.GetEmployeeById(employeeId);

            EmployeeDetails(employeeId);

            //BranchDepartmentHistory history = CommonManager.GetBranchDepartmentHistoryById(eventSourceID);

            cmbEmployee.Text = emp.Name;
            cmbEmployee.Disable();

            if (history.EventID != null)
                cmbEventType.SetValue(history.EventID.ToString());
            else
                cmbEventType.ClearValue();

            cmbBranch.SetValue(history.BranchId.Value.ToString());
            LoadDepartment(history.BranchId.Value);
            cmbDepartment.SetValue(history.DepartmentId.Value.ToString());
            txtLetterNumber.Text = history.LetterNo;
            //txtSpecialResponsibility.Text = history.SpecialResponsibility;                   

            cmbStatus.SetValue(history.StatusId.ToString());

            int levelId = CommonManager.GetLevelIDForDesignation(history.DesignationId.Value);
            cmbLevel.SetValue(levelId.ToString());

            cmbDesignation.GetStore().DataSource = NewHRManager.GetDesignationsByLevel(levelId).ToList();
            cmbDesignation.GetStore().DataBind();

            cmbDesignation.SetValue(history.DesignationId.ToString());


            if (IsEnglish == false && SessionManager.User.IsEnglishPreferred != null && SessionManager.User.IsEnglishPreferred)
            {

                if (history.LetterDateEng != null)
                    calLetterDate.Text = history.LetterDateEng.Value.ToString("yyyy/MM/dd");

                if (history.DateEng != null)
                    calFromDate.Text = history.DateEng.ToString("yyyy/MM/dd");

                //if(history.DepartureDateEng != null)
                //    calDepartureDate.Text = history.DepartureDateEng.Value.ToString("yyyy/MM/dd");

            }
            else
            {
                calLetterDate.Text = history.LetterDate;
                calFromDate.Text = history.Date;
                //calDepartureDate.Text = history.DepartureDate;
            }

            txtNote.Text = history.Notes;

            if (history.SubDepartmentId != null)
                cmbSubDepartment.SetValue(history.SubDepartmentId.Value.ToString());

            btnSave.Text = Resources.Messages.Update;
            WBranchTransfer.Show();
        }

        protected void btnDelete_Click(object sender, DirectEventArgs e)
        {
            int branchDepartmentId = int.Parse(hdnBranchDepartmentId.Text);

            Status status = CommonManager.DeleteServiceHistory(branchDepartmentId);

            if (status.IsSuccess)
            {
                NewMessage.ShowNormalMessage("Event deleted.");
                X.Js.Call("searchList");
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }
        }


        protected void btnSave_Click(object sender, DirectEventArgs e)
        {
            Page.Validate("SaveBrTran");
            if (Page.IsValid)
            {
                EmployeeServiceHistory obj = new EmployeeServiceHistory();

                if (!string.IsNullOrEmpty(txtLetterNumber.Text.Trim()))
                    obj.LetterNo = txtLetterNumber.Text.Trim();


                if (IsEnglish == false && SessionManager.User.IsEnglishPreferred != null && SessionManager.User.IsEnglishPreferred)
                {
                    obj.LetterDateEng = BLL.BaseBiz.GetEngDate(calLetterDate.Text.Trim(), true);
                    obj.LetterDate = BLL.BaseBiz.GetAppropriateDate(obj.LetterDateEng.Value);


                    obj.DateEng = BLL.BaseBiz.GetEngDate(calFromDate.Text.Trim(), true);
                    obj.Date = BLL.BaseBiz.GetAppropriateDate(obj.DateEng);

                    if (!string.IsNullOrEmpty(calToDate.Text.Trim()))
                    {
                        obj.ToDateEng = BLL.BaseBiz.GetEngDate(calToDate.Text.Trim(), true);
                        obj.ToDate = BLL.BaseBiz.GetAppropriateDate(obj.ToDateEng.Value);
                    }
                }
                else
                {
                    obj.Date = calFromDate.Text.Trim();
                    obj.DateEng = GetEngDate(obj.Date);

                    obj.ToDate = calToDate.Text.Trim();
                    if (!string.IsNullOrEmpty(obj.ToDate))
                        obj.ToDateEng = GetEngDate(obj.ToDate);

                    obj.LetterDate = calLetterDate.Text.Trim();
                    obj.LetterDateEng = GetEngDate(obj.LetterDate);


                }


                obj.BranchId = int.Parse(cmbBranch.SelectedItem.Value);
                obj.DepartmentId = int.Parse(cmbDepartment.SelectedItem.Value);
                obj.LevelId = int.Parse(cmbLevel.SelectedItem.Value);
                obj.DesignationId = int.Parse(cmbDesignation.SelectedItem.Value);
                obj.StatusId = int.Parse(cmbStatus.SelectedItem.Value);

                //if(!string.IsNullOrEmpty(txtSpecialResponsibility.Text.Trim()))
                //    obj.SpecialResponsibility = txtSpecialResponsibility.Text.Trim();



                obj.EventID = short.Parse(cmbEventType.SelectedItem.Value);


                if (!string.IsNullOrEmpty(txtNote.Text.Trim()))
                    obj.Notes = txtNote.Text.Trim();

                if (!string.IsNullOrEmpty(cmbSubDepartment.Text))
                    obj.SubDepartmentId = int.Parse(cmbSubDepartment.SelectedItem.Value);

                obj.EmployeeId = int.Parse(hdnEmployeeId.Text);

                if (!string.IsNullOrEmpty(hdnBranchDepartmentId.Text))
                {
                    obj.EventSourceID = int.Parse(hdnBranchDepartmentId.Text);
                }


                // If the it is last branch transfer then only allow to update
                //if (CommonManager.IsLastBranchTransfer(obj.BranchDepartmentId, obj.EmployeeId.Value) == false)
                //{
                //    NewMessage.ShowWarningMessage("Only latest branch transfer can be edited.");
                //    return;
                //}

                Status status = CommonManager.SaveUpdateServiceHistory(obj);

                if (status.IsSuccess)
                {

                    hdnMsg.Text = "Event updated.";


                    string updateJS = string.Format("opener.updateBranchDepartmentFromChangeHistory({0},{1},'{2}','{3}');",
                        obj.BranchId, obj.DepartmentId, new DepartmentManager().GetById(obj.DepartmentId.Value).Name, obj.SubDepartmentId);
                    Page.ClientScript.RegisterClientScriptBlock(GetType(), "sdfdsdfdsfsd", updateJS, true);


                    btnCancel_Click(null, null);
                }
                else
                    NewMessage.ShowWarningMessage(status.ErrorMessage);
            }
        }

        protected void cmbLevel_Select(object sender, DirectEventArgs e)
        {
            cmbDesignation.GetStore().DataSource = NewHRManager.GetDesignationsByLevel(
                int.Parse(cmbLevel.SelectedItem.Value)).ToList();
            cmbDesignation.GetStore().DataBind();
        }

        public bool IsTeamColumnVisible()
        {
            if (
                (CommonManager.Setting.LeaveRequestSettingUsingPreDefinedList != null &&
                CommonManager.Setting.LeaveRequestSettingUsingPreDefinedList.Value)

                 ||
                (CommonManager.Setting.LeaveRequestSettngUsingOneToOne != null &&
                CommonManager.Setting.LeaveRequestSettngUsingOneToOne.Value)
                )
                return false;

            return true;
        }

        protected void btnCancel_Click(object sender, DirectEventArgs e)
        {

            btnSave.Text = "Save";

            if (string.IsNullOrEmpty(hdnEmployeeId.Text))
            {
                WBranchTransfer.Close();
                return;
            }

            int employeeId = int.Parse(hdnEmployeeId.Text);

            if (employeeId != -1)
            {
                CommonManager.SaveFirstIBranchfNotExists(employeeId);
                Clear();
            }

            if (e == null)
                NewMessage.ShowNormalMessage(hdnMsg.Text);

            WBranchTransfer.Close();

            X.Js.Call("searchList");

        }
        public void btnExport_Click(object sender, EventArgs e)
        {
            int totalRecords = 0;
            DateTime? fromDate = null;
            DateTime? toDate = null;


            if (txtFromDate.SelectedDate != DateTime.MinValue)
                fromDate = txtFromDate.SelectedDate;

            if (txtToDate.SelectedDate != DateTime.MinValue)
                toDate = txtToDate.SelectedDate;

            int employeeId = -1;
            string employeeSearch = "";

            int.TryParse(cmbEmpSearch.SelectedItem.Value, out employeeId);

            if (employeeId == 0)
                employeeId = -1;

            if (employeeId == -1)
            {
                if (!string.IsNullOrEmpty(cmbEmpSearch.Text))
                    employeeSearch = cmbEmpSearch.Text.Trim();
            }

            int? eventType = null;

            if (cmbEventFilter.SelectedItem != null && cmbEventFilter.SelectedItem.Value != null)
                eventType = int.Parse(cmbEventFilter.SelectedItem.Value);

            List<GetServiceHistoryNewResult> list = BranchManager.GetNewServiceHistory
              (0, 99999, ref totalRecords, employeeSearch, employeeId, fromDate, toDate, eventType);



            List<string> hiddenList = new List<string>();
            hiddenList.Add("SN");
            hiddenList.Add("TotalRows");
            hiddenList.Add("EventID");
            hiddenList.Add("SourceID");
            hiddenList.Add("Type");
            hiddenList.Add("LetterDateEng");
            hiddenList.Add("LetterDate");
            hiddenList.Add("LetterNo");
            hiddenList.Add("CreatedOn");
            hiddenList.Add("EventSourceID");
            hiddenList.Add("BranchId");
            hiddenList.Add("DepartmentId");
            hiddenList.Add("LevelId");
            hiddenList.Add("DesignationId");
            hiddenList.Add("StatusId");
            hiddenList.Add("RowNumber");
            hiddenList.Add("Note");

            Dictionary<string, string> renameList = new Dictionary<string, string>();
            renameList.Add("FromDate", "Nepali Date");
            renameList.Add("FromDateEng", "English Date");
            renameList.Add("Level", "Position");

            Bll.ExcelHelper.ExportToExcel("Serice History List", list,
                hiddenList,
            new List<String>() { },
            renameList,
            new List<string>() { }
            , new Dictionary<string, string>() { }
            , new List<string> { "EventName", "Date", "DateEng", "EmployeeId", "IdCardNo", "Name", "Branch", "Department", "ServiceStatus", "Designation" });


        }


        //public void btnExport_Click(object sender, EventArgs e)
        //{
        //    int totalRecords = 0;
        //    DateTime? fromDate = null;
        //    DateTime? toDate = null;


        //    if (txtFromDate.SelectedDate != DateTime.MinValue)
        //        fromDate = txtFromDate.SelectedDate;

        //    if (txtToDate.SelectedDate != DateTime.MinValue)
        //        toDate = txtToDate.SelectedDate;

        //    int employeeId = -1;
        //    string employeeSearch = "";

        //    int.TryParse(cmbEmpSearch.SelectedItem.Value, out employeeId);

        //    if (employeeId == 0)
        //        employeeId = -1;

        //    if (employeeId == -1)
        //    {
        //        if (!string.IsNullOrEmpty(cmbEmpSearch.Text))
        //            employeeSearch = cmbEmpSearch.Text.Trim();
        //    }

        //    int? eventType = null;

        //       if (cmbEventFilter.SelectedItem != null && cmbEventFilter.SelectedItem.Value != null)
        //        eventType = int.Parse(cmbEventFilter.SelectedItem.Value);

        //       List<GetTransferListResult> list = BranchManager.GetTranserList
        //           (0, 999999, ref totalRecords, employeeSearch, employeeId, fromDate, toDate, eventType);

        //       foreach (var item in list)
        //       {
        //           item.FromDateEngFormatted = item.FromDateEng.Value.ToShortDateString();
        //       }

        //    Bll.ExcelHelper.ExportToExcel("Branch Transfer Report", list,
        //        new List<String>() {"BranchDepartmentId", "FromDateEng","Branch","Department","Type","EventID", "TimeElapsed", "FromBranchId", "BranchId", "DepeartmentId", "Note", "IsFirst", "CreatedBy", "CreatedOn", 
        //        "ModifiedBy", "ModifiedOn", "FromDepartmentId", "LetterNumber", "LetterDate", "LetterDateEng", "SpecialResponsibility", "DepartureDate", "DepartureDateEng", "Status", "AttendanceDate", "AttendanceDateEng",
        //        "IsDepartmentTransfer", "SubDepartmentId", "FromSubDepartmentId", "ActionType"},
        //    new List<String>() { },
        //    new Dictionary<string, string>() { { "FromDate", "Transfer Date" }, { "FromDateEngFormatted", "Transfer Date(AD)" }, { "EmployeeId", "EIN" }, { "EmployeeName", "Employee Name" }, { "FromBranch", "From Branch" }, { "ToBranch", "To Branch" } },
        //    new List<string>() { }
        //    , new Dictionary<string, string>() { } 
        //    , new List<string> { "FromDate", "FromDateEngFormatted", "EmployeeId", "EmployeeName", "FromBranch", "ToBranch" });


        //}

        protected void Page_PreRender(object sender, EventArgs e)
        {
            bool isEng = false;
            if (UserManager.IsEnglishDatePreferredInNepaliDateSys())
                isEng = true;

            UserControl ctrl = Master.ContentHeaderCtrl();
            ResourceManager rm = ctrl.FindControl("ResourceManager1") as ResourceManager;

            rm.RegisterClientInitScript("Date11111", string.Format("isEnglish = {0};", isEng.ToString().ToLower()));

            rm.RegisterClientInitScript("Date22222", string.Format("todayDate = '{0}';function calendarOnClick(event, sourceElement, e3){{}};",
                                                                      CustomDate.GetTodayDate(isEng).ToString()));
        }


    }
}