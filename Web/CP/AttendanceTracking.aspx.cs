﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using DAL;
using System.Text;
using Utils.Helper;

using BLL.Base;
using Utils.Calendar;

namespace Web.CP
{
    public partial class AttendanceTracking : BasePage
    {
        CommonManager commonMgr = new CommonManager();
        LeaveAttendanceManager leaveMgr = new LeaveAttendanceManager();
        
        List<LLeaveType> leaves = null;
        HolidayManager holidayMgr = new HolidayManager();
        System.Web.Script.Serialization.JavaScriptSerializer jsSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
        public bool isPayrollEditable = true;
            //(CommonManager.CompanySetting.WhichCompany == WhichCompany.PSI); //as we only display current year so for now make all atte editable, currently we are following Financial year, in future we may need to follow Leave year
       

        #region "Paging"

        private int _tempCurrentPage;
        private int? _tempCount = 0;


        protected override void OnInit(EventArgs e)
        {
            Page.RegisterRequiresControlState(this);
            base.OnInit(e);
        }
        protected override void LoadControlState(object savedState)
        {
            object[] rgState = (object[])savedState;
            base.LoadControlState(rgState[0]);
            
            _tempCurrentPage = (int)rgState[1];

        }
        protected override object SaveControlState()
        {
            object[] rgState = new object[3];
            rgState[0] = base.SaveControlState();
           
            rgState[1] = _tempCurrentPage;
            return rgState;
        }
        #endregion

        int totalDays = 0;

        List<string> filterEINList = new List<string>();

        public bool IsAtteReadonlyForAutoGeneration
        {
            set { }
            get
            {
                if (CommonManager.CompanySetting.WhichCompany == WhichCompany.NIBL)
                    return true;

                if (CommonManager.CompanySetting.AttendanceAutoAbsentMarkingForSalary)
                    return true;

                if (!string.IsNullOrEmpty(Request.QueryString["GenerateAtte"]))
                {
                    return true;
                }
                return false;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

          
            RegisterLegendColors();
            //load payroll periods first & then set total days
            if (!IsPostBack)
            {
                if( !LoadPayrollPeriods() )
                    return;
                
                //hide bulk leave assignment for Hourly leave
                if (CommonManager.CalculationConstant.CompanyHasHourlyLeave.Value)
                {
                    gvw.Columns[0].Visible = false;
                }
            }
            SetTotalDays();
            AddColumns();
            if (!IsPostBack)
            {
                _tempCurrentPage = 1;
                Initialise();
            }
            else
            {
                LoadAttedance();
                SavePayPresentDaysForSavingLater();
            }
           
            RegisterHolidaysLeaves();
            RegisterForCalculation();
            RegisterSettings();


            tdImport.Visible = CommonManager.CompanySetting.AttendanceShowLeaveImportInRegistry;

            absentToDate.InnerHtml = "Absent processing upto date : <br>" + CommonManager.Setting.AttendanceUpto +
               " (" + (CommonManager.Setting.AttendanceUptoDate == null ? "" :
                CommonManager.Setting.AttendanceUptoDate.Value.ToShortDateString()) + ")";

            JavascriptHelper.AttachNonDialogPopUpCode(Page, "importPopup", "../ExcelWindow/AttendanceExcel.aspx", 450, 500);
            JavascriptHelper.AttachNonDialogPopUpCode(Page, "currencyHistoryPopup", "AbsentToDate.aspx", 500, 400);
        }

        private int GetAttendanceId()
        {
            if (ddlPayrollPeriods.SelectedValue.Equals("-1")|| ddlPayrollPeriods.SelectedValue =="" )
                return 0;
            return int.Parse(ddlPayrollPeriods.SelectedValue);
        }


        /// <summary>
        /// Register setting like hourly leave allowed, hours per day
        /// </summary>
        private void RegisterSettings()
        {


            string code =
                string.Format("var hasHourlyLeave = {0};", CommonManager.CalculationConstant.CompanyHasHourlyLeave.Value ? "true" : "false");
            code +=
                string.Format("var hasHalfHourlyLeave = {0};", CommonManager.CompanySetting.HasHalfHourlyLeave ? "true" : "false");
            code +=
                string.Format("var hoursInADay = {0};", CommonManager.CalculationConstant.CompanyLeaveHoursInADay);
            code +=
                string.Format(" var oneHourLeave = parseFloat(1.0 / {0});", CommonManager.CalculationConstant.CompanyLeaveHoursInADay);

            ///if encash leave type can have negative balance or not
            code += string.Format("var isEncashLeaveTypeBeNegativeBalance = {0};", CommonManager.CompanySetting.IsEncashLeaveTypeBeNegativeBalance ? "true" : "false");
            code += string.Format("var isEncashLapseLeaveTypeBeNegativeBalance = {0};", CommonManager.CompanySetting.IsEncashLapseLeaveTypeBeNegativeBalance ? "true" : "false");
            code += string.Format("var isLapseLeaveBeNegativeBalance = {0};", CommonManager.CompanySetting.IsLapseLeaveBeNegativeBalance ? "true" : "false");

            Page.ClientScript.RegisterStartupScript(this.GetType(), "regihours", code, true);
        }

        /// <summary>
        /// Register Balance+Accured for all Leaves of all Employees
        /// </summary>
        void RegisterBalanceAccured()
        {
            if (GetAttendanceId() == 0)
                return;

            if (this.IsAtteReadonlyForAutoGeneration)
                return;

            //register using the kaye "EmployeeId:LeaveId"
            //StringBuilder jsCodes =new StringBuilder();

            //dictionaries types for holiding emp leave details
            Dictionary<string, string> list = new Dictionary<string, string>();
            Dictionary<string, string> listWorkDayDaysCount = new Dictionary<string, string>();
            Dictionary<string, string> listIsActive = new Dictionary<string, string>();
            StringBuilder employeeIDList = new StringBuilder("");
            foreach (GridViewRow row in this.gvw.Rows)
            {
                int employeeId = (int)gvw.DataKeys[row.DataItemIndex]["EmployeeId"];
                if (employeeIDList.Length == 0)
                    employeeIDList.Append(employeeId);
                else
                    employeeIDList.Append("," + employeeId);
            }


            //get all leaves for the employee
            List<LeaveGenerateEmployeeAccuredResult> leaveList
                   = LeaveAttendanceManager.GetBeginningBalaceAccuredForEmployee(0,
                   GetAttendanceId(), SessionManager.CurrentCompanyId, employeeIDList.ToString());

            foreach (var leave in leaveList)
            {
                //if Accured is null then not valid for the employee may be due to Status not applicatoin
                if (leave.Accured == null)
                    continue;

                string key = leave.EmployeeId.ToString() + ":" + leave.LeaveTypeId.ToString();
                //add balance+accured : WorkDayDaysCount as value for the key employeeid : leavetypeid
                list.Add(key, (leave.BeginningBalance + leave.Accured).ToString());
                if (leave.WorkDayDaysCount != null && leave.WorkDayDaysCount != 0)
                    listWorkDayDaysCount.Add(key, leave.WorkDayDaysCount.ToString());
                listIsActive.Add(key, (leave.IsActive.Value ? "true" : "false"));
            }

            string jsCodes = "var leaveBalance = "
                             +
                             jsSerializer.Serialize(
                                 list) +
                             ";";
            jsCodes += "var leaveWorkDayDaysCount = "
                             +
                             jsSerializer.Serialize(
                                 listWorkDayDaysCount) +
                             ";";
            jsCodes += "var leaveEmployeeIsActive = "
                            +
                            jsSerializer.Serialize(
                                listIsActive) +
                            ";";
            Page.ClientScript.RegisterStartupScript(this.GetType(), "LeaveAccuredBalance", jsCodes, true);

        }

        void RegisterHolidaysLeaves()
        {
            if(string.IsNullOrEmpty( ddlPayrollPeriods.SelectedValue))
                return;

            //if (this.IsAtteReadonlyForAutoGeneration)
            //    return;

            List<GetHolidaysForAttendenceResult> holidays = GetHolidayList();


            List<GetEmployeeListForBranchHolidayResult> branchEmployeeList = new List<GetEmployeeListForBranchHolidayResult>();
            //Register for Branch specific holiday if has, check for Branch Transfer also
            foreach (GetHolidaysForAttendenceResult branchHoliday in holidays.Where(x => x.BranchIDList != ""))
            {
                if (!string.IsNullOrEmpty(branchHoliday.BranchIDList))
                {
                    branchEmployeeList.AddRange(
                        BLL.BaseBiz.PayrollDataContext.GetEmployeeListForBranchHoliday(branchHoliday.DateEng, branchHoliday.Day.ToString(),
                        branchHoliday.BranchIDList, int.Parse(ddlPayrollPeriods.SelectedValue),
                        String.Join(",", filterEINList.ToArray()))
                        );
                }
            }
            

            string holidaysJSON = "\n var holidays = " + jsSerializer.Serialize(holidays) + ";\n";

            //register Default Leaves
            holidaysJSON = holidaysJSON+ "\n var defaultLeaves = " + jsSerializer.Serialize(LeaveAttendanceManager.DefaultLeaves) +
                           ";\n";


            Page.ClientScript.RegisterStartupScript(this.GetType(), "Holiday123", holidaysJSON, true);            


            // branch emp holiday
            StringBuilder str = new StringBuilder("\nvar employeeBranchHoliday = new Array();");
            //leaves = LeaveAttendanceManager.GetAllLeaves(SessionManager.CurrentCompanyId);
            foreach (GetEmployeeListForBranchHolidayResult emp in branchEmployeeList)
            {
                str.AppendFormat("employeeBranchHoliday['{0}'] = '';", emp.Value);
            }
           
            Page.ClientScript.RegisterStartupScript(this.GetType(), "EmpbranchHolidayJS", str.ToString(), true);


        }

        private List<GetHolidaysForAttendenceResult> GetHolidayList()
        {
            if (ddlPayrollPeriods.SelectedValue == "")
                return null;
            int payrollPeriodId = int.Parse( ddlPayrollPeriods.SelectedValue);
            PayrollPeriod period = CommonManager.GetPayrollPeriod(payrollPeriodId);
            return holidayMgr.GetMonthsHolidaysForAttendence
                (period.Month, period.Year.Value, SessionManager.CurrentCompanyId,payrollPeriodId,null,null,0);
        }

        public bool IsHoliday(int day, List<GetHolidaysForAttendenceResult> holidays)
        {
          
            foreach (GetHolidaysForAttendenceResult holiday in holidays)
            {
                if (holiday.Day.Value == day)
                    return true;
            }
            return false;
        }

        void SavePayPresentDaysForSavingLater()
        {
            //int employeeId, payrollPeriodId;
            //foreach (GridViewRow row in gvw.Rows)
            //{
            //    employeeId = (int)gvw.DataKeys[row.RowIndex]["EmployeeId"];
            //    payrollPeriodId = (int)gvw.DataKeys[row.RowIndex]["PayrollPeriodId"];

            //    HiddenField hfPayDays = row.FindControl("hfPayDays") as HiddenField;
            //    HiddenField hfPresentDays = row.FindControl("hfPresentDays") as HiddenField;

            //    //if( hfPayDays ==null)
            //    //    continue;
                

            //    KeyValue kv = new KeyValue(hfPayDays.Value, hfPresentDays.Value);


            //    payPresentDays.Add(employeeId + "-" + payrollPeriodId, kv);
            //}
        }

        void RegisterLegendColors()
        {
            StringBuilder str = new StringBuilder("\nlegends = new Array();");
            leaves = LeaveAttendanceManager.GetAllLeaves(SessionManager.CurrentCompanyId);
            foreach (LLeaveType leave in leaves)
            {
                str.AppendFormat("legends['{0}'] = '{1}';", leave.Abbreviation, leave.LegendColor);
            }
            //add holiday legend color also
            str.AppendFormat("legends['{0}'] = '{1}';", HolidaysConstant.National_Holiday, HolidaysConstant.GetColor(HolidaysConstant.National_Holiday));
            str.AppendFormat("legends['{0}'] = '{1}';", HolidaysConstant.Caste_Holiday, HolidaysConstant.GetColor(HolidaysConstant.Caste_Holiday));
            str.AppendFormat("legends['{0}'] = '{1}';", HolidaysConstant.Weekly_Holiday, HolidaysConstant.GetColor(HolidaysConstant.Weekly_Holiday));
            str.AppendFormat("legends['{0}'] = '{1}';", HolidaysConstant.Female_Holiday, HolidaysConstant.GetColor(HolidaysConstant.Female_Holiday));

            Page.ClientScript.RegisterStartupScript(this.GetType(), "LegendColorReg123", str.ToString(), true);
        }

        void SetTotalDays()
        {
            if (string.IsNullOrEmpty(ddlPayrollPeriods.SelectedValue))
                totalDays = 30;//default value to be shown if no payroll period in the list
            else
            {
                totalDays = commonMgr.GetTotalDays(int.Parse(ddlPayrollPeriods.SelectedValue));
            }

            
        }


        void AppendDefaultLeaves(List<LLeaveType> leaveTypes)
        {
            LLeaveType leaveAbs = new LLeaveType();
            leaveAbs.Abbreviation = LeaveAttendanceManager.DefaultLeaves[1][0];
            leaveAbs.Type = LeaveType.UNPAID;
            leaveAbs.IsHalfDayAllowed = true;
            leaveAbs.Title = LeaveAttendanceManager.DefaultLeaves[1][1];

            //LLeaveType leaveUnpaid = new LLeaveType();
            //leaveUnpaid.Abbreviation = LeaveAttendanceManager.DefaultLeaves[2][0];
            //leaveUnpaid.Type = LeaveType.HALFPAID;
            //leaveUnpaid.IsHalfDayAllowed = true;

            LLeaveType leaveHalfUnpaid = new LLeaveType();
            leaveHalfUnpaid.Abbreviation = LeaveAttendanceManager.DefaultLeaves[3][0];
            leaveHalfUnpaid.Type = LeaveType.UNPAID;
            leaveHalfUnpaid.IsHalfDayAllowed = false;
            leaveHalfUnpaid.Title = LeaveAttendanceManager.DefaultLeaves[3][1];

            leaveTypes.Add(leaveAbs);
            //leaveTypes.Add(leaveUnpaid);
            leaveTypes.Add(leaveHalfUnpaid);
        }

       

        /// <summary>
        /// Register each leave & holiday for pay/present/deductable days calculation
        /// </summary>
        void RegisterForCalculation()
        {
           

            LeaveAttendanceManager mgr = new LeaveAttendanceManager();

            StringBuilder str = new StringBuilder("\ntotalDays=" + totalDays + ";");

            //if (this.IsAtteReadonlyForAutoGeneration)
            //{
            //    Page.ClientScript.RegisterStartupScript(this.GetType(), "Msdfdsf234", str.ToString(), true);
            //    return;

            //}

            string payDeduct = string.Empty;
            string presentDeduct = string.Empty;
            List<LLeaveType> leaveTypes = new List<LLeaveType>();

            // do not register leave for this case as we will not allow to change leaves from attendance register
            if (this.IsAtteReadonlyForAutoGeneration == false)
            {
                leaveTypes = LeaveAttendanceManager.GetAllLeaves(SessionManager.CurrentCompanyId);
            }


            //add two default leaves
            AppendDefaultLeaves(leaveTypes);

            foreach (LLeaveType leave in leaveTypes)
            {
                str.AppendFormat("\nleaves['{0}'] = new Array();", leave.Abbreviation);
            
                //these are decrementing value so if no decrement set 0 or else value
                if( leave.Type == LeaveType.FULLYPAID)
                {
                    payDeduct = "0";
                    presentDeduct = "1";
                }
                else if( leave.Type == LeaveType.HALFPAID)
                {
                    payDeduct = "0.5";
                    presentDeduct = "1";
                }
                else if (leave.Type==LeaveType.NON_LEAVE_ABSENCE)
                {
                    payDeduct = "0";
                    presentDeduct = "1";
                }
                else if( leave.Type==LeaveType.UNPAID)
                {
                    payDeduct = "1";
                    presentDeduct = "1";
                }
                str.AppendFormat("leaves['{0}']['FullName'] = '{1}';", leave.Abbreviation, leave.Title);
                str.AppendFormat("leaves['{0}']['Pay'] = {1};",leave.Abbreviation, payDeduct);
                str.AppendFormat("leaves['{0}']['IsHalfDayAllowed'] = {1};", leave.Abbreviation, leave.IsHalfDayAllowed.Value ? "true" : "false");
                str.AppendFormat("leaves['{0}']['Present'] = {1};\n", leave.Abbreviation, presentDeduct);
                str.AppendFormat("leaves['{0}']['UnusedLeave'] = '{1}';\n", leave.Abbreviation, string.IsNullOrEmpty(leave.UnusedLeave) ? "" : leave.UnusedLeave);
                str.AppendFormat("leaves['{0}']['Type'] = '{1}';\n", leave.Abbreviation, leave.Type);
                str.AppendFormat("leaves['{0}']['IsWorkDaysType'] = {1};", leave.Abbreviation,
                                 (leave.FreqOfAccrual == LeaveAccrue.Based_On_WorkDays) ? "true" : "false");
                str.AppendFormat("leaves['{0}']['IsCompensatory'] = {1};", leave.Abbreviation,
                                    (leave.FreqOfAccrual == LeaveAccrue.COMPENSATORY) ? "true" : "false");
                str.AppendFormat("leaves['{0}']['AllowNegativeBalance'] = '{1}';", leave.Abbreviation, leave.AllowNegativeLeaveRequest != null && leave.AllowNegativeLeaveRequest.Value ? "true" : "false");

                if (leave.FreqOfAccrual == LeaveAccrue.Based_On_WorkDays)
                {
                    str.AppendFormat("leaves['{0}']['WorkDaysPeriod'] = {1};", leave.Abbreviation,
                                 leave.WorkDaysPerPeriod);
                    str.AppendFormat("leaves['{0}']['LeavePeriod'] = {1};", leave.Abbreviation,
                                 leave.LeavePerPeriod);
                    List<string> workDaysIncludeLeaves = new List<string>();
                    foreach (LLeaveWorkDay otherLeave in leave.LLeaveWorkDays)
                    {
                        LLeaveType otherLeaveType = mgr.GetLeaveType(otherLeave.OtherLeaveId);
                        workDaysIncludeLeaves.Add(otherLeaveType.Abbreviation);
                    }
                    str.AppendFormat("leaves['{0}']['WorkDaysIncludeLeaves'] = '{1}';", leave.Abbreviation,
                                     string.Join(",", workDaysIncludeLeaves.ToArray()));


                }


                if( leave.IsHalfDayAllowed.Value)
                {
                    str.AppendFormat("\nleaves['{0}'] = new Array();", leave.Abbreviation + LeaveAttendanceManager.HalfDayPostFix);

                    //these are decrementing value so if no decrement set 0 or else value
                    if (leave.Type == LeaveType.FULLYPAID)
                    {
                        payDeduct = "0";
                        presentDeduct = "0.5";
                    }
                    else if (leave.Type == LeaveType.HALFPAID)
                    {
                        payDeduct = "0.25";
                        presentDeduct = "0.5";
                    }
                    else if (leave.Type == LeaveType.NON_LEAVE_ABSENCE)
                    {
                        payDeduct = "0";
                        presentDeduct = "0.5";
                    }
                    else if (leave.Type == LeaveType.UNPAID)
                    {
                        payDeduct = "0.5";
                        presentDeduct = "0.5";
                    }

                    str.AppendFormat("leaves['{0}']['Pay'] = {1};", leave.Abbreviation + LeaveAttendanceManager.HalfDayPostFix, payDeduct);
                    str.AppendFormat("leaves['{0}']['Present'] = {1};\n", leave.Abbreviation + LeaveAttendanceManager.HalfDayPostFix, presentDeduct);
                }
            }
            //register for holidays also
            str.AppendFormat("\nleaves['{0}'] = new Array();", HolidaysConstant.National_Holiday);
            str.AppendFormat("leaves['{0}']['Pay'] = {1};", HolidaysConstant.National_Holiday, 0);
            str.AppendFormat("leaves['{0}']['Present'] = {1};\n", HolidaysConstant.National_Holiday, 0);

            str.AppendFormat("\nleaves['{0}'] = new Array();", HolidaysConstant.Female_Holiday);
            str.AppendFormat("leaves['{0}']['Pay'] = {1};", HolidaysConstant.Female_Holiday, 0);
            str.AppendFormat("leaves['{0}']['Present'] = {1};\n", HolidaysConstant.Female_Holiday, 0);

            str.AppendFormat("\nleaves['{0}'] = new Array();", HolidaysConstant.Caste_Holiday);
            str.AppendFormat("leaves['{0}']['Pay'] = {1};", HolidaysConstant.Caste_Holiday, 0);
            str.AppendFormat("leaves['{0}']['Present'] = {1};\n", HolidaysConstant.Caste_Holiday, 0);

            str.AppendFormat("\nleaves['{0}'] = new Array();", HolidaysConstant.Weekly_Holiday);
            str.AppendFormat("leaves['{0}']['Pay'] = {1};", HolidaysConstant.Weekly_Holiday, 0);            
            
            //weekly half day
            str.AppendFormat("\nleaves['{0}'] = new Array();", HolidaysConstant.Weekly_Holiday + LeaveAttendanceManager.HalfDayPostFix);

            str.AppendFormat("leaves['{0}']['Pay'] = {1};", HolidaysConstant.Weekly_Holiday + LeaveAttendanceManager.HalfDayPostFix, 0);
            str.AppendFormat("leaves['{0}']['Present'] = {1};", HolidaysConstant.Weekly_Holiday + LeaveAttendanceManager.HalfDayPostFix, 0);

            //calculated in client side as it depends on the day if full or half/second day
            //str.AppendFormat("leaves['{0}']['Present'] = {1};\n", HolidaysConstant.Weekly_Holiday, 0);


     
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Msdfdsf234", str.ToString(), true);
        }


        //retrive work days leave for the emp
        public Dictionary<string, string[]> GetWorkDayLeaveFromClient(string leaveCollection)
        {
            if (leaveCollection == null)
                leaveCollection = "";

            Dictionary<string, string[]> leaves = new Dictionary<string, string[]>();

            string[] workdayLeaves = leaveCollection.Split(new char[] {';'},StringSplitOptions.RemoveEmptyEntries);
            foreach (var leave in workdayLeaves)
            {
                if (leave == null)
                    continue;

                string[] leaveItems = leave.Split(new char[] { ':' }, StringSplitOptions.RemoveEmptyEntries);
                if (leaveItems.Length >= 3)
                {
                    leaves.Add(leaveItems[0], new string[] {leaveItems[1], leaveItems[2], leaveItems[3]});
                }
            }
            return leaves;
        }


        /// <summary>
        /// Save Attendance
        /// </summary>
        void ProcessToApply()
        {
            StringBuilder empAtteCellDetails = new StringBuilder("");
            StringBuilder empList = new StringBuilder("");
            StringBuilder xmlLeaveAdjustment = new StringBuilder();

            int employeeId, payrollPeriodId = 0;
            int? attendenceId;
            double payDays, presentDays;
            int type, typeValue;
            double deductDays;
            string value1, value2, value3;
            bool hasEmployeeExists = false;
            int employeeTotalDaysInAttedance;
            HiddenField hfPayDays, hfPresentDays, hfLeaveRequest;
            string leaveRequestDetails = "";
            LeaveAttendanceManager mgr = new LeaveAttendanceManager();

            // Retrieve holidays on this payrollperiod
            List<GetHolidaysForAttendenceResult> holidays = GetHolidayList();
            #region "Retrieve Leave Accural Details of all displayed Employees"

            StringBuilder employeeIDList = new StringBuilder("");
            foreach (GridViewRow row in gvw.Rows)
            {
                if (employeeIDList.Length == 0)
                    employeeIDList.Append(gvw.DataKeys[row.RowIndex]["EmployeeId"].ToString());
                else
                    employeeIDList.Append("," + gvw.DataKeys[row.RowIndex]["EmployeeId"].ToString());
            }
            //get all leaves for the all saving employee
            List<LeaveGenerateEmployeeAccuredResult> allLeaveList
                = LeaveAttendanceManager.GetBeginningBalaceAccuredForEmployee(0,
                GetAttendanceId(), SessionManager.CurrentCompanyId, employeeIDList.ToString());

            bool isEditable = true;
            if (ddlPayrollPeriods.SelectedItem != null)
                isEditable = !IsPayrollFinalSavedButNextPayrollAttendanceAlsoSaved(int.Parse(ddlPayrollPeriods.SelectedValue));

            #endregion

            empAtteCellDetails.Length = 0;
            empAtteCellDetails.Append("<root>");
            empList.Append("<root>");
            xmlLeaveAdjustment.Append("<root>");

            //Iterate Each Employee Start
            foreach (GridViewRow row in gvw.Rows)
            {
                attendenceId = (int)gvw.DataKeys[row.RowIndex]["AttendenceId"];
                employeeId = (int)gvw.DataKeys[row.RowIndex]["EmployeeId"];
                payrollPeriodId = (int)gvw.DataKeys[row.RowIndex]["PayrollPeriodId"];
                hfPayDays = row.FindControl("hfPayDays") as HiddenField;
                hfPresentDays = row.FindControl("hfPresentDays") as HiddenField;
                hfLeaveRequest = row.FindControl("hfLeaveRequest") as HiddenField;
                leaveRequestDetails = hfLeaveRequest.Value.Trim();

                //string hfWorkDayLeaves =
                //    Request.Form[(row.FindControl("hfWorkDayLeaves") as HiddenField).ClientID.Replace("_", "$")];
                //Dictionary<string, string[]> workdayLeaves = GetWorkDayLeaveFromClient(hfWorkDayLeaves);

                //if used in Calculation then iterate for other Employee
                if (isEditable == false && isPayrollEditable == false)
                {
                    continue;
                }

                payDays = double.Parse(string.IsNullOrEmpty(hfPayDays.Value) ? "0" : hfPayDays.Value);
                presentDays = double.Parse(string.IsNullOrEmpty(hfPresentDays.Value) ? "0" : hfPresentDays.Value);

                for (int i = 1; i <= totalDays; i++)
                {
                    TextBox txt = row.FindControl("txt" + i) as TextBox;
                    HiddenField hfType = row.FindControl("hfType" + i) as HiddenField;
                    string[] typeValues = hfType.Value.Trim().Split(new char[] { ':' });
                   



                    // don't access using property as it will be overiten
                    string value = txt.Text;
                    value1 = typeValues.Length >= 1 ? typeValues[0] : "";
                    value2 = typeValues.Length >= 2 ? typeValues[1] : "";
                    value3 = typeValues.Length >= 3 ? typeValues[2] : "";

                    type = 0;
                    typeValue = 0;
                    int.TryParse(value1, out type);
                    int.TryParse(value2, out typeValue);
                    double.TryParse(value3, out deductDays);
                    empAtteCellDetails.AppendFormat(@"<Atte N=""{0}"" V=""{1}"" T=""{2}"" TV=""{3}"" DD=""{4}"" EIN=""{5}"" />", i,
                                     value, type, typeValue, deductDays, employeeId);
                }



                string[] values = gvw.DataKeys[row.RowIndex]["DisableCellRange"].ToString().Split(new char[] { ':' });
                employeeTotalDaysInAttedance = int.Parse(values[1]) - int.Parse(values[0]) - 1;

                if (employeeTotalDaysInAttedance < 0)
                    employeeTotalDaysInAttedance = 0;


                // "<E EIN='' AttendanceId='' TotalDays='' PayDays='' PresentDays='"
                empList.AppendFormat(@"<E EIN=""{0}"" A=""{1}"" T=""{2}"" PayD=""{3}"" PreD=""{4}"" LR=""{5}"" />", employeeId, attendenceId, employeeTotalDaysInAttedance,
                     payDays, presentDays, leaveRequestDetails);

                hasEmployeeExists = true;




                //Leave adjustment Start
                //get all leaves for the employee
                List<LeaveGenerateEmployeeAccuredResult> leaveList
                    = allLeaveList.Where(l => l.EmployeeId == employeeId).ToList();

                foreach (var leave in leaveList)
                {
                    string key = employeeId.ToString() + ":" + leave.LeaveTypeId.ToString();
                    DAL.LLeaveType dbLeaveType = mgr.GetLeaveType(leave.LeaveTypeId);
                    bool isCompensatoryLeave = dbLeaveType.FreqOfAccrual == LeaveAccrue.COMPENSATORY;
                    //bool isHolidayOnThisDay;
                    float dayValue = 0;

                    for (int i = 1; i <= totalDays; i++)
                    {
                        TextBox txt = row.FindControl("txt" + i) as TextBox;
                        HiddenField hfType = row.FindControl("hfType" + i) as HiddenField;

                        string[] multiValues = new string[1];

                        // multi leave request in single day
                        if (hfType.Value.Contains(";"))
                        {
                            multiValues = hfType.Value.Trim().Split(new char[] { ';' });
                        }
                        else
                            multiValues[0] = hfType.Value.Trim();//.Split(new char[] { ':' });

                        //string[] typeValues = hfType.Value.Trim().Split(new char[] { ':' });
                        foreach (string multiValue in multiValues)
                        {
                            string[] typeValues = multiValue.Trim().Split(new char[] { ':' });

                            //if cell has leave value
                            if (typeValues.Length >= 1 && typeValues[0] == ((int)AttendaceColumnType.Leave).ToString())
                            {
                                int cellLeaveTypeId = typeValues.Length >= 2 ? int.Parse(typeValues[1]) : 0;

                                if (cellLeaveTypeId == leave.LeaveTypeId)
                                {
                                    /// Logic to calculate leave taken from the attendace cell, if value contains 
                                    /// case: /2 then half day leave so 0.5 is added in leave taken
                                    /// case: -2 then hourly leave so hourly leave value like 2 is added
                                    /// case: SL then 1 day leave is added
                                    bool isHalfDayLeaveSelected = false;
                                    bool isHourlyLeave = false;

                                    // If half day or hourly leave then mark it
                                    if (txt.Text.Contains(LeaveAttendanceManager.HalfDayPostFix))
                                    {
                                        isHalfDayLeaveSelected = true;
                                    }
                                    //else if (!txt.Text.Contains("--") && txt.Text.Contains(LeaveAttendanceManager.HourlyLeaveSeparator))
                                    else if (CommonManager.CalculationConstant.CompanyHasHourlyLeave != null
                                        && CommonManager.CalculationConstant.CompanyHasHourlyLeave.Value
                                        && txt.Text.Contains(LeaveAttendanceManager.HourlyLeaveSeparator))
                                        isHourlyLeave = true;

                                    if (CommonManager.CompanySetting.WhichCompany == WhichCompany.DishHome &&
                                        LeaveAttendanceManager.GetDishHomeSundayHalfDayAbbr() == txt.Text.Trim())
                                    {
                                        dayValue = 0;
                                    }
                                    // for half day it will be .5
                                    else if (isHalfDayLeaveSelected)
                                        dayValue = (float).5;
                                    else if (isHourlyLeave) //for hourly leave it will be the hours selected
                                    {
                                        string hourValue = txt.Text.Split(new char[] { '-' })[1].Replace("++", "").Replace("--", "");
                                        float value = float.Parse(hourValue); ;// / CommonManager.CalculationConstant.CompanyLeaveHoursInADay.Value;
                                        dayValue = value;
                                    }
                                    else // for full day, value is 1
                                    {
                                        dayValue = 1;

                                    }

                                    if (isCompensatoryLeave && txt.Text.Contains("++"))// && isHolidayOnThisDay)
                                    {
                                        // Process if has Holiday value set
                                        if (dbLeaveType.CompensatoryOnHolidayFulldayValue != null)
                                        {
                                            if (isHourlyLeave)
                                                leave.Accured += dayValue;
                                            else if (txt.Text.Contains("/2"))
                                            {
                                                leave.Accured += (float)dbLeaveType.CompensatoryOnHolidayHalfdayValue.Value;
                                            }
                                            else
                                            {
                                                leave.Accured += (float)dbLeaveType.CompensatoryOnHolidayFulldayValue.Value;
                                            }
                                        }
                                        else
                                            leave.Accured += dayValue;
                                    }
                                    else
                                        //sum the balance
                                        leave.Count += dayValue;
                                }
                            }
                        }
                    }
                }

                foreach (var leave in leaveList)
                {
                    //// don't check if multi selection in leave setting
                    //if (CommonManager.Setting.ExcludeStatusHierarchyInLeave != null
                    //    && CommonManager.Setting.ExcludeStatusHierarchyInLeave.Value)
                    //{ }
                    //else
                    //{
                    //    if (leave.Accured == null)
                    //        continue;
                    //}

                    //workDayDaysCount : (balance-taken) : accured // for work day leave
                    //string[] workDayValues = null;
                    //if (workdayLeaves.ContainsKey(leave.Abbreviation.Trim()))
                    //    workDayValues = workdayLeaves[leave.Abbreviation.Trim()] as string[];
                    //else
                    //  continue;
                    double accured = leave.Accured == null ? 0 : leave.Accured.Value;
                    //double workdayDaysCount = workDayValues == null ? 0 : double.Parse(workDayValues[0]);

                    xmlLeaveAdjustment.AppendFormat(
                        "<row empId='{0}' leaveId='{1}' begBal='{2}' accured='{3}' taken='{4}' workDayDaysCount='{5}' />",
                        employeeId, leave.LeaveTypeId, leave.BeginningBalance, accured, leave.Count,
                        0);
                }

                //Leave adjustment End



            }
            //Iterate Each Employee End

            xmlLeaveAdjustment.Append("</root>");
            empList.Append("</root>");
            empAtteCellDetails.Append("</root>");

            if (payrollPeriodId != 0 && hasEmployeeExists)
            {

                int? errorStatusCode = 0;

                string[] empNames = LeaveAttendanceManager.SaveAttendence(payrollPeriodId, empList.ToString(),
                                 empAtteCellDetails.ToString(), xmlLeaveAdjustment.ToString(), ref errorStatusCode);


                if (errorStatusCode == null)
                    errorStatusCode = 0;

                AttendanceSaveErrorStatus errorStatus = (AttendanceSaveErrorStatus)errorStatusCode;

                //if has emp names then those UPL/Abs like have been changed while in the salary saved payrollperiod
                if (errorStatus == AttendanceSaveErrorStatus.UPLChangedForSalaryAlreadySaved)
                {
                   
                        divErrorMsg.InnerHtml = "For \"" + string.Join(",", empNames) + "\" unpaid leaves has been changed even if the salary is already saved.";
                        divErrorMsg.Hide = false;
                    
                }
                else if (errorStatus == AttendanceSaveErrorStatus.LeaveRequestConflict)
                {

                    divErrorMsg.InnerHtml = "For \"" + string.Join(",", empNames) + "\" reqested leaves have been change so please try to resave the attendance.";
                    divErrorMsg.Hide = false;



                    LoadAttedance();
                }
                else
                {
                    BLL.BaseBiz.PayrollDataContext.ChangeUserActivities.InsertOnSubmit(
                      BLL.BaseBiz.GetChangeUserActivityLog("Attendance saved for " + ddlPayrollPeriods.SelectedItem.Text + "."));
                    BLL.BaseBiz.PayrollDataContext.SubmitChanges();

                    msgInfo.InnerHtml = Resources.Messages.AttendanceSavedMsg;
                    msgInfo.Hide = false;

                    LoadAttedance();
                }
            }


        }


        void AddColumns()
        {
            //List<GetHolidaysForAttendenceResult> holidays = GetHolidayList();

            //if( holidays == null)
            //    return;
            //;

              int payrollPeriodId = int.Parse( ddlPayrollPeriods.SelectedValue);
            PayrollPeriod period = CommonManager.GetPayrollPeriod(payrollPeriodId);
            DateTime start = period.StartDateEng.Value;

            bool isEditable = !IsPayrollFinalSavedButNextPayrollAttendanceAlsoSaved(payrollPeriodId);

            for (int i = 1; i <= totalDays; i++)
            {
                TemplateField field = new TemplateField();

                string weekDay = GetDayOfWeek(start.DayOfWeek);

                field.ItemTemplate = new AttendanceGridViewTemplate(DataControlRowType.DataRow, i.ToString(), i.ToString(), leaves, (isPayrollEditable || isEditable),this.IsAtteReadonlyForAutoGeneration);

                field.HeaderTemplate = new AttendanceGridViewTemplate(DataControlRowType.Header, i.ToString(), weekDay + "&nbsp;/&nbsp;" + i.ToString(), leaves, (isPayrollEditable || isEditable), this.IsAtteReadonlyForAutoGeneration);                

                
                gvw.Columns.Add(field);
                //gvw.Columns.Insert(gvw.Columns.Count - 1, field);
                start = start.AddDays(1);

            }           
        }

        private  string  GetDayOfWeek(DayOfWeek week)
        {
            return week.ToString().Substring(0, 3);
            //switch (week)
            //{
            //    case DayOfWeek.Friday:
            //        return "F";
                    
            //    case DayOfWeek.Monday:
            //        return "M";

            //    case DayOfWeek.Saturday:
            //        return "Sa";
            //    case DayOfWeek.Sunday:
            //        return "S";

            //    case DayOfWeek.Thursday:
            //        return "T";
            //    case DayOfWeek.Tuesday:
            //        return "T";
            //    case DayOfWeek.Wednesday:
            //        return "W";

            //}
            //return "";
        }


        ////To track if final calculation has been done or not
        //bool IsCalculationInitiaSaved()
        //{
        //    if(!string.IsNullOrEmpty(ddlPayrollPeriods.SelectedValue) )
        //    {
        //        return 
        //            (CalculationManager.IsCalculationSaved(int.Parse(ddlPayrollPeriods.SelectedValue))
        //             != null);

        //    }
        //    return false;   
        //}

        bool LoadPayrollPeriods()
        {
            ddlPayrollPeriods.DataSource = CommonManager.GetAllYear(SessionManager.CurrentCompanyId);
            ddlPayrollPeriods.DataBind();
            //Default select last payroll period
            if (ddlPayrollPeriods.Items.Count > 0)
            {
                ListItem item = ddlPayrollPeriods.Items[ddlPayrollPeriods.Items.Count - 1];
                item.Selected = true;
                return true;
            }
            else
            {
                return false;
                
            }

        }


        void Initialise()
        {

            List<UnitList> units = CommonManager.GetAllUnitList();
            if (units.Count >= 1)
            {

                ddlUnit.DataSource = units;
                ddlUnit.DataBind();
                tdUnit1.Visible = true;
                tdUnit2.Visible = true;
            }
            else
            {
                tdUnit1.Visible = false;
                tdUnit2.Visible = false;
            }
           

            List<LLeaveType> leaves = LeaveAttendanceManager.GetAllLeaves(SessionManager.CurrentCompanyId);
            AppendDefaultLeaves(leaves);

            //dlstLegends.DataSource = leaves;
            //dlstLegends.DataBind();

            ////create legend for hlidays
            //dlstLegendsHolidays.DataSource = LeaveAttendanceManager.GetLegendsForHolidays();
            //dlstLegendsHolidays.DataBind();


            if (SessionManager.IsCustomRole && SessionManager.CustomRoleDeparmentList.Count > 0)
            {
                //if (SessionManager.IsReadOnlyUser)
                //{
                //    btnApply.Visible = false;
                //    btnDelete.Visible = false;
                //}
                ddlFilterBy.SelectedIndex = 2;
                ddlFilterBy.Enabled = false;
                ddlFilterBy_SelectedIndexChanged(null, null);
                //btnImport.Visible = false;
            }

            if (IsPostBack == false)
            {
                JobStatus statues = new JobStatus();
                List<KeyValue> statusList = statues.GetMembersForHirarchyViewWithOnlyText() ;


              


                ddlStatus.DataSource = statusList;
                ddlStatus.DataValueField = "KeyValueCombined";
                ddlStatus.DataTextField = "Value";

                ddlStatus.DataBind();
            }

            LoadAttedance();
        }

        private int CalculateTotalPages(double totalRows)
        {
            int totalPages = (int)Math.Ceiling(totalRows / int.Parse(pagingCtl.DDLRecords.SelectedValue));

            return totalPages;
        }

        void GetTypeAndId(ref string type, ref int id)
        {
            if (ddlFilterByValues.SelectedValue == "-1")
            {
                type = "";
                id = 0;
            }
            else
            {
                type = ddlFilterBy.SelectedItem.Text;
                id = int.Parse(ddlFilterByValues.SelectedValue);
            }
        }

        /// <summary>
        /// Display appropriate message like Payroll already saved so not editable or some employee salary already saved so not editable.
        /// </summary>
        /// <param name="payrollPeriodId"></param>
        /// <param name="list"></param>
        public void ShowAppropriateMessage(int payrollPeriodId,List<GetAttandenceListResult> list)
        {

            //if (IsPayrollFinalSavedButNextPayrollAttendanceAlsoSaved(payrollPeriodId))
            //{
            //    divWarningMsg.InnerHtml = Resources.Messages.CalcAllEmployeeFinalSavedMsg;
            //    divWarningMsg.Hide = false;
            //}
            //else
            //{
            //    foreach (GetAttandenceListResult item in list)
            //    {
            //        if (item.IsUsedInCalculation != null && item.IsUsedInCalculation == 1)
            //        {
            //            divWarningMsg.InnerHtml = Resources.Messages.SomeOfTheEmployeeSalaryAlreadySavedMsg;
            //            divWarningMsg.Hide = false;
            //            break;
            //        }
            //    }
            //}
        }

        void LoadAttedance()
        {
            string type = string.Empty; int id = 0;
            GetTypeAndId(ref type, ref id);

            if (!string.IsNullOrEmpty(ddlPayrollPeriods.SelectedValue))
            {

                string[] value = ddlStatus.SelectedValue.Split(new char[] { ':' });
                int minStatus = int.Parse(value[0]);
                bool statusOnly = ddlStatus.SelectedItem.Text.ToString().ToLower().Contains("only");
                //int unitId = -1;

                List<GetAttandenceListResult> list = LeaveAttendanceManager.GetAttedenceList(SessionManager.CurrentCompanyId,
                       int.Parse(ddlPayrollPeriods.SelectedValue), txtEmpSearchText.Text.Trim(), type, id, _tempCurrentPage,
                       int.Parse(pagingCtl.DDLRecords.SelectedValue), ref _tempCount,minStatus,statusOnly,this.IsAtteReadonlyForAutoGeneration
                       ,int.Parse(ddlUnit.SelectedValue));

                filterEINList = list.Select(x => x.EmployeeId.ToString()).ToList();

                gvw.DataSource = list;
                gvw.DataBind();

                //ShowAppropriateMessage(int.Parse(ddlPayrollPeriods.SelectedValue), list);
            }
            else
            {
                gvw.DataSource = null;
                gvw.DataBind();
            }

            if (_tempCount <= 0)
            {
                pnl.Visible = false;
            }
            else
            {
                pnl.Visible = true;
            }

            int totalPages = (int)CalculateTotalPages(_tempCount.Value);
            pagingCtl.LabelTotalPage.Text = (totalPages).ToString();
            pagingCtl.LabelCurrentPage.Text = _tempCurrentPage.ToString();
            pagingCtl.LabelTotalRecords.Text = "(" + _tempCount + ")";
            // Update Pge numbers
            if (pagingCtl.DDLPageNumber.Items.Count != totalPages)
            {
                pagingCtl.DDLPageNumber.Items.Clear();
                for (int i = 1; i <= totalPages; i++)
                {
                    pagingCtl.DDLPageNumber.Items.Add(i.ToString());
                }
            }
            if (Request.Form["__EVENTTARGET"] == null ||
                  !Request.Form["__EVENTTARGET"].Equals(this.pagingCtl.DDLPageNumber.ClientID.Replace("_", "$")))
            {
                if (this.pagingCtl.DDLPageNumber.Items.FindByValue(_tempCurrentPage.ToString()) != null)
                    this.pagingCtl.DDLPageNumber.SelectedValue = _tempCurrentPage.ToString();
            }

            if (_tempCurrentPage <= 1)
            {
                pagingCtl.ButtonPrev.Enabled = false;
            }
            else
            {
                pagingCtl.ButtonPrev.Enabled = true;
            }

            if (_tempCurrentPage < totalPages)
                pagingCtl.ButtonNext.Enabled = true;
            else
                pagingCtl.ButtonNext.Enabled = false;
        }

        protected void btnLoad_Click(object sender, EventArgs e)
        {
            LoadAttedance();
        }

        protected void btnGenerateAtte_Click(object sender, EventArgs e)
        {
            string type = string.Empty; int id = 0;
            GetTypeAndId(ref type, ref id);

            if (!string.IsNullOrEmpty(ddlPayrollPeriods.SelectedValue))
            {
                string[] value = ddlStatus.SelectedValue.Split(new char[] { ':' });
                int minStatus = int.Parse(value[0]);
                bool statusOnly = ddlStatus.SelectedItem.Text.ToString().ToLower().Contains("only");

                int payrollPeriodId = int.Parse(ddlPayrollPeriods.SelectedValue);
                int empId = -1;

                if (!string.IsNullOrEmpty(txtEmpSearchText.Text.Trim())
                    && !string.IsNullOrEmpty(hiddenEmpId.Value))
                    empId = int.Parse(hiddenEmpId.Value);


                LeaveAttendanceManager.GenerateMonthlyAttendance(payrollPeriodId, empId);

                LoadAttedance();

                msgInfo.InnerHtml = "Attendance has been generated.";
                msgInfo.Hide = false;

            }
            
        }
        protected void gvw_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            LoadAttedance();
        }

        protected void btnApply_Click(object sender, EventArgs e)
        {

            ProcessToApply();

        }

        public void ChangePageNumber()
        {
            _tempCurrentPage = this.pagingCtl.CurrentPage;
            LoadAttedance();
        }

        protected void btnPrevious_Click()
        {
            _tempCurrentPage = int.Parse(pagingCtl.LabelCurrentPage.Text);
            _tempCurrentPage -= 1;
            LoadAttedance();
        }

        protected void btnNext_Click()
        {
            _tempCurrentPage = int.Parse(pagingCtl.LabelCurrentPage .Text);
            _tempCurrentPage += 1;
            LoadAttedance();
        }

        protected void ddlRecords_SelectedIndexChanged()
        {
            _tempCurrentPage = 1;
                LoadAttedance();
        }

        protected void ddlPayrollPeriods_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadAttedance();
        }

        protected void ddlFilterBy_SelectedIndexChanged(object sender, EventArgs e)
        {
            ListItem item = ddlFilterByValues.Items[0];
            item.Selected = false;
            ddlFilterByValues.Items.Clear();
            switch (int.Parse(ddlFilterBy.SelectedValue))
            {
                case 1:
                    BranchManager bMgr = new BranchManager();
                    
                    ddlFilterByValues.DataSource = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId);
                    ddlFilterByValues.DataTextField = "Name";
                    ddlFilterByValues.DataValueField = "BranchId";
                    
                    break;
                case 2:
                    DepartmentManager dMgr = new DepartmentManager();
                    ddlFilterByValues.DataSource = dMgr.GetDepartmentsByCompany(SessionManager.CurrentCompanyId);
                    ddlFilterByValues.DataValueField = "DepartmentId";
                    ddlFilterByValues.DataTextField = "Name";                   
                    break;
                case 3:
                    ddlFilterByValues.DataSource = CommonManager.GetAllCostCodes();
                    ddlFilterByValues.DataValueField = "CostCodeId";
                    ddlFilterByValues.DataTextField = "Name";
                    break;
            }
            ddlFilterByValues.DataBind();
            ddlFilterByValues.Items.Insert(0, item);

            LoadAttedance();
        }

        protected void ddlFilterByValues_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadAttedance();
        }

        protected void txtEmpSearchText_TextChanged(object sender, EventArgs e)
        {
            _tempCurrentPage = 1;
            LoadAttedance();
        }


        protected void Page_PreRender(object sender, EventArgs e)
        {
            //if( this.IsCalculationInitiaSaved())
            //{
            //    btnApply.Visible = false;
            //    btnCancel.Visible = false;
            //}
            //else
            //{
                btnApply.Visible = true;
                btnCancel.Visible = true;
            btnDelete.Visible = true;
            btnDeleteAll.Visible = true;
            //}

            int hasNotUsedEmployeeAttInCalc = 1;
            foreach (GridViewRow row in gvw.Rows)
            {
                hasNotUsedEmployeeAttInCalc = (int)gvw.DataKeys[row.RowIndex]["IsUsedInCalculation"];
                if( hasNotUsedEmployeeAttInCalc ==0)
                    break;
            }

            if (ddlPayrollPeriods.SelectedItem == null)
                return;

            hasNotUsedEmployeeAttInCalc =
                CalculationManager.IsNextPayrollAtteSaved(int.Parse(ddlPayrollPeriods.SelectedValue)) ? 1 : 0;

            if( hasNotUsedEmployeeAttInCalc==0)
            {
                btnApply.Visible = true;
                btnCancel.Visible = true;
                gvw.Columns[1].Visible = true;
            }
            else
            {
                if (isPayrollEditable)
                {
                    //btnImport.OnClientClick=
                    //     ("if(confirm('Are you really want to import the past Attendance?')) {importPopupProcess();return false;} else {return false;}");
                    btnApply.OnClientClick =
                        ("if(confirm('Are you really want to re-save the past Attendance?')) {return checkIfSavingValid();} else {return false;}");
                    btnApply.Visible = true;
                }
                else
                    btnApply.Visible = false;

                btnCancel.Visible = false;
                btnDelete.Visible = false;
                btnDeleteAll.Visible = false;
            //    pnl.Visible = false;
                if (gvw.Columns[1].HeaderText == "Delete" && gvw.Columns[0].Visible)
                    gvw.Columns[1].Visible = false;
            }
            RegisterBalanceAccured();

            btnFillAtte.Visible = btnApply.Visible;

            gvw.UseAccessibleHeader = true;
            if (SessionManager.IsCustomRole && SessionManager.CustomRoleDeparmentList.Count > 0)
            {
                if (SessionManager.IsReadOnlyUser)
                {
                    btnApply.Visible = false;
                    btnDelete.Visible = false;
                    btnDeleteAll.Visible = false;
                }
                
            }

            if (IsAtteReadonlyForAutoGeneration)
            {
                btnApply.Visible = false; // hide this features as if atte changed and saved then leave and upl re removed
                absent1.Visible = true;
                absent2.Visible = true;
                btnGenerateAtte.Visible = true;
                btnFillAtte.Visible = false;


                if (!IsPostBack)
                {
                     divAutoAbsentMarkingDateMsg.Visible = true;
                     List<AttendanceEmployeeDate> dates = AttendanceManager.GetAttendanceProcessedLastDate();
                     if (dates.Count > 0)
                     {
                         try
                         {
                             CustomDate date1 = new CustomDate(dates[0].Date.Value.Day, dates[0].Date.Value.Month, dates[0].Date.Value.Year, true);
                             date1 = CustomDate.ConvertEngToNep(date1);
                             CustomDate date2 = new CustomDate(dates[0].LastDate.Value.Day, dates[0].LastDate.Value.Month, dates[0].LastDate.Value.Year, true);
                             date2 = CustomDate.ConvertEngToNep(date2);

                             divAutoAbsentMarkingDateMsg.InnerHtml =
                                 string.Format("Last date of attendance generation is for <b>{0}({1})</b> to <b>{2}({3})</b> on {4}.",
                                 dates[0].Date.Value.ToString("yyyy-MMM-dd"), date1.ToStringShortMonthName(),
                                  dates[0].LastDate.Value.ToString("yyyy-MMM-dd"), date2.ToStringShortMonthName(),
                                  dates[0].LastUptoDate.Value.ToString("yyyy-MMM-dd"));
                         }
                         catch { }
                     }
                }
            }
             
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            StringBuilder xmlEmployees = new StringBuilder();
            xmlEmployees.Append("<root>");
            bool employeeAdded = false;
            for (int i = 0; i < this.gvw.Rows.Count; i++)
            {
                GridViewRow row = gvw.Rows[i];
                int  employeeId = (int)gvw.DataKeys[row.RowIndex]["EmployeeId"];         

                if (row.FindControl("chkDelete") == null)
                    continue;

                CheckBox chkDelete = (CheckBox)row.FindControl("chkDelete");
                string state = Request.Form[chkDelete.UniqueID];
                if (state == "on")
                {
                    xmlEmployees.AppendFormat("<row EmployeeId='{0}' />", employeeId);
                    employeeAdded = true;
                }
            }
            xmlEmployees.Append("</root>");



           
            if( employeeAdded)
            {
                bool result = LeaveAttendanceManager.DeleteAttendance(int.Parse(ddlPayrollPeriods.SelectedValue), xmlEmployees.ToString());

                if (result)
                {
                    BLL.BaseBiz.PayrollDataContext.ChangeUserActivities.InsertOnSubmit(
                 BLL.BaseBiz.GetChangeUserActivityLog("Attendance deleted for " + ddlPayrollPeriods.SelectedItem.Text + "."));
                    BLL.BaseBiz.PayrollDataContext.SubmitChanges();

                    msgInfo.InnerHtml = Resources.Messages.AttendanceDeleteMsg;
                    msgInfo.Hide = false;
                    //JavascriptHelper.DisplayClientMsg(Resources.Messages.AttendanceDeleteMsg, Page);
                    LoadAttedance();
                }
               
            }

            //if (calculation != null && employeeAdded)
            //{
            //    bool result = calcMgr.DeleteCalculation(calculation.CalculationId, xmlEmployees.ToString());

            //    if (result)
            //    {
            //        JavascriptHelper.DisplayClientMsg("Calculation deleted completely.", Page);
            //        calculation = null;
            //    }
            //    else
            //    {
            //        JavascriptHelper.DisplayClientMsg("Calculation deleted.", Page);
            //    }



            //    LoadCalculation(false, true);
            //    //ChangeDisplayState();

            //}
        }
        protected void btnDeleteAll_Click(object sender, EventArgs e)
        {
           

            {
                bool result = LeaveAttendanceManager.DeleteAllAttendance(int.Parse(ddlPayrollPeriods.SelectedValue));

                if (result)
                {
                    BLL.BaseBiz.PayrollDataContext.ChangeUserActivities.InsertOnSubmit(
                 BLL.BaseBiz.GetChangeUserActivityLog("Attendance of all employees deleted for " + ddlPayrollPeriods.SelectedItem.Text + "."));
                    BLL.BaseBiz.PayrollDataContext.SubmitChanges();

                    msgInfo.InnerHtml = Resources.Messages.AttendanceDeleteMsg;
                    msgInfo.Hide = false;
                    //JavascriptHelper.DisplayClientMsg(Resources.Messages.AttendanceDeleteMsg, Page);
                    LoadAttedance();
                }

            }

            //if (calculation != null && employeeAdded)
            //{
            //    bool result = calcMgr.DeleteCalculation(calculation.CalculationId, xmlEmployees.ToString());

            //    if (result)
            //    {
            //        JavascriptHelper.DisplayClientMsg("Calculation deleted completely.", Page);
            //        calculation = null;
            //    }
            //    else
            //    {
            //        JavascriptHelper.DisplayClientMsg("Calculation deleted.", Page);
            //    }



            //    LoadCalculation(false, true);
            //    //ChangeDisplayState();

            //}
        }

    }



    // Here is the code for building the template field
}
