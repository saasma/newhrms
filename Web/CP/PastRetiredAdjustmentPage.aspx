﻿<%@ Page Title="Past Retired Adjustment" Language="C#" MasterPageFile="~/Master/HROld.Master"
    AutoEventWireup="true" CodeBehind="PastRetiredAdjustmentPage.aspx.cs" MaintainScrollPositionOnPostback="true"
    Inherits="Web.CP.PastRetiredAdjustmentPage" %>

<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .mainFieldset
        {
            -moz-box-shadow: 1px 1px 3px #DDDDDD;
            background: none repeat scroll 0 0 #E9F0FB;
            border-color: #10B0EA;
            border-style: solid;
            border-width: 3px 1px 1px;
            float: left;
            font: bold 18px Arial,Helvetica,sans-serif;
            height: auto !important;
            margin-bottom: 20px;
            margin-top: 0;
            min-height: 80px;
            padding: 8px;
            position: relative;
            width: 800px !important;
        }
        .mainFieldset table
        {
            padding: 10px 20px 0;
        }
        .mainFieldset table tr td
        {
            padding: 0 10px 10px 0;
            text-align: left;
        }
        strong
        {
            display: block;
            padding-bottom: 5px;
        }
    </style>
    <script type="text/javascript">
        function openCalculationPopup(payrollPeriodId) {
            if (typeof (ein) == 'undefined')
                ein = 0;
            window.open('../CP/holdpaymentcalculation.aspx?HoldPayment=true&ID=' + payrollPeriodId, 'Hold Payment List', 'height=600,width=1000');
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Bring Past retired in current payroll</h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <uc2:MsgCtl ID="divMsgCtl" EnableViewState="false" Hide="true" runat="server" />
        <uc2:WarningCtl ID="divWarningMsg" EnableViewState="false" Hide="true" runat="server" />
        <div class="mainFieldset">
            <table cellpadding="0" cellspacing="0" width="700px">
                <tr>
                    <td width="200px" valign="top">
                        <strong>Employee Name</strong>
                        <asp:DropDownList AppendDataBoundItems="true" ID="ddlEmployees" DataTextField="Name"
                            DataValueField="EmployeeId" onchange="setEIN(this);" runat="server" OnSelectedIndexChanged="ddlEmployees_SelectedIndexChanged">
                            <asp:ListItem Value="-1">--Select Employee--</asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator InitialValue="-1" ID="valReqdResPerson" runat="server"
                            ControlToValidate="ddlEmployees" Display="None" ErrorMessage="Please select an employee."
                            ValidationGroup="StopPayment"></asp:RequiredFieldValidator>
                        <br />
                        <br />
                        <strong>Payroll Period</strong>
                        <asp:Label ID="lblPayrollPeriod" runat="server" />
                    </td>
                    <td valign="top" style="width: 60px">
                        <strong>EIN</strong>
                        <asp:Label ID="lblEmpId" Font-Bold="true" runat="server"></asp:Label>
                    </td>
                    <td>
                        <strong>Notes</strong>
                        <asp:TextBox MaxLength="200" ID="txtNotes" TextMode="MultiLine" Width="300px" Rows="4"
                            runat="server"></asp:TextBox>
                    </td>
                </tr>
                <%--   <tr>
                    <td width="360px" valign="top">
                    </td>
                </tr>--%>
                <tr>
                    <td>
                    </td>
                </tr>
            </table>
        </div>
        <div class="clear">
            <asp:Button ID="btnSave" OnClientClick="valGroup='StopPayment';return CheckValidation()"
                runat="server" Text="Save" CssClass="save" OnClick="btnSave_Click" />
            <asp:Button ID="btnCancel" runat="server" CssClass="cancel" Text="Cancel" OnClick="btnCancel_Click" />
            <br />
            <br />
            <div style='clear: both'>
                <strong>Released List</strong>
                <cc2:EmptyDisplayGridView ID="gvw" ShowHeaderWhenEmpty="true" runat="server" CellPadding="4"
                    CssClass="tableLightColor" GridLines="None" DataKeyNames="EmployeeId,PayrollPeriodId"
                    Width="900px" AutoGenerateColumns="False" OnPageIndexChanging="gvw_PageIndexChanging"
                    OnSelectedIndexChanged="gvw_SelectedIndexChanged1" OnRowDeleting="gvw_RowDeleting">
                    <RowStyle BackColor="#E3EAEB" />
                    <Columns>
                        <asp:BoundField DataField="EmployeeId" ItemStyle-Width="50" DataFormatString="{0:000}"
                            HeaderText="EIN" HeaderStyle-HorizontalAlign="Left" />
                        <asp:BoundField DataField="Name" HeaderText="Name" ItemStyle-Width="150px" HeaderStyle-HorizontalAlign="Left" />
                        <asp:BoundField DataField="PeriodName" HeaderText="Period" ItemStyle-Width="100"
                            HeaderStyle-HorizontalAlign="Left" />
                        <asp:TemplateField HeaderText="Notes" HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <%# Eval("Notes") %>
                            </ItemTemplate>
                        </asp:TemplateField>
                      
                        <%--<asp:TemplateField ItemStyle-Width="30px">
                            <ItemTemplate>
                                <asp:ImageButton ID="ImageButton1" ToolTip="Edit" runat="server" CommandName="Select"
                                    ImageUrl="~/images/edit.gif" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-Width="30px">
                            <ItemTemplate>
                                <asp:ImageButton ID="ImageButton2" ToolTip="Delete" OnClientClick="return confirm('Confirm delete the hold payment?');"
                                    runat="server" CommandName="Delete" ImageUrl="~/images/delete.gif" />
                            </ItemTemplate>
                        </asp:TemplateField>--%>
                    </Columns>
                    <RowStyle CssClass="odd" />
                    <AlternatingRowStyle CssClass="even" />
                    <EmptyDataTemplate>
                        No List
                    </EmptyDataTemplate>
                </cc2:EmptyDisplayGridView>
            </div>
           
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="pageScriptsContent" runat="server">
    <script type="text/javascript">

        function setEIN(ddl) {
            document.getElementById('<%= lblEmpId.ClientID %>').innerHTML = ddl.value;
        }

        function resumePaymentPopupCall(stopPaymentId) {
            var ret = resumePaymentPopup("Id=" + stopPaymentId);
            if (typeof (ret) != 'undefined') {
                if (ret == "Reload") {
                    __doPostBack("Reload", "");
                }
            }
        }


        function validateDOJToDate(source, args) {
            //debugger;



        }

    </script>
</asp:Content>
