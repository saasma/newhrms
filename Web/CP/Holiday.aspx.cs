﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using BLL.BO;
using System.IO;
using System.Text;
using Utils.Helper;
using Utils.Calendar;

namespace Web.CP
{
    public partial class Holiday : BasePage
    {

        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.LeaveAndAttendance;
            }
        }
        CommonManager cmnMgr = new CommonManager();
        HolidayManager holidayMgr = new HolidayManager();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();
            }
            JavascriptHelper.AttachNonDialogPopUpCode(Page, "HolidayImport", "../ExcelWindow/HolidayImport.aspx", 450, 500);
        }

        private void Initialise()
        {
            cmbDayOfWeek.Store[0].DataSource = BizHelper.GetMembersAsListItem(new DAL.DaysOfWeek());
            cmbDayOfWeek.Store[0].DataBind();

            cmbFullOrHalf.Store[0].DataSource = BizHelper.GetMembersAsListItem(new DAL.HolidayType());
            cmbFullOrHalf.Store[0].DataBind();

            cmbWHEffectiveFrom.Store[0].DataSource = CommonManager.GetCurrentYear(SessionManager.CurrentCompanyId);
            cmbWHEffectiveFrom.Store[0].DataBind();

            BindWeeklyHolidays();

            selectorBranch.Store[0].DataSource = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId);
            selectorBranch.Store[0].DataBind();

            List<FinancialDate> financialYearList = new CommonManager().GetAllFinancialDates();
            foreach (FinancialDate date in financialYearList)
            {
                date.SetName(IsEnglish);
            }
            
            cmbYearFilter.Store[0].DataSource = financialYearList;
            cmbYearFilter.Store[0].DataBind();


            // add holiday groups
            List<HolidayGroup> list = CommonManager.GetAllHolidayGroups();
            foreach (var item in list)
            {
                cmbGender.Items.Add(new Ext.Net.ListItem { Text = item.Name, Value = item.GroupId.ToString() });
            }
        }

        private void BindWeeklyHolidays()
        {
            List<GetWeeklyHolidayResult> lstWeeklyHolidays = holidayMgr.GetWeeklyHoliday();

            foreach (var item in lstWeeklyHolidays)
            {
                item.DayofWeekName = DAL.DaysOfWeek.Get(item.DayofWeek);
                item.IsFullDayValue = (item.IsFullDay == true ? "Full day" : "Second half");
            }

            gridWeeklyHolidays.Store[0].DataSource = lstWeeklyHolidays;
            gridWeeklyHolidays.Store[0].DataBind();

        }

        protected void Store_ReadData(object sender, StoreReadDataEventArgs e)
        {
            DateTime? startDate = null, endDate = null;
            int pageSize = int.Parse(cmbPageSize.SelectedItem.Value);
            int totalRecord = 0;

            if (cmbYearFilter.SelectedItem != null && cmbYearFilter.SelectedItem.Value != null && cmbYearFilter.SelectedItem.Value != "-1")
            {
                FinancialDate objFD = HolidayManager.GetFinancialDateById(int.Parse(cmbYearFilter.SelectedItem.Value));
                startDate = objFD.StartingDateEng.Value;
                endDate = objFD.EndingDateEng.Value;
            }

            List<GetAnnualHolidayListResult> list = HolidayManager.GetAnnualHolidayList(e.Start / pageSize, pageSize, startDate, endDate);

            foreach (var item in list)
            {
                CustomDate cd = CustomDate.GetCustomDateFromString(item.Date, false);
                item.NepDate = string.Format("{0} {1}, {2}", DateHelper.NepMonths[cd.Month].ToString(), cd.Day, cd.Year);
            }

            storeAnnualHoliday.DataSource = list;
            storeAnnualHoliday.DataBind();

            if (list.Count > 0)
                totalRecord = list[0].TotalRows.Value;

            e.Total = totalRecord;             
        }

        private void ClearWeeklyHoliday()
        {
            cmbDayOfWeek.ClearValue();
            cmbFullOrHalf.ClearValue();
            cmbWHEffectiveFrom.ClearValue();
            btnSaveWH.Text = "Add";
        }

        protected void btnAddWeeklyHoliday_Click(object sender, DirectEventArgs e)
        {
            ClearWeeklyHoliday();
            hdnWeeklyHolidayId.Text = "";
            wWH.Center();
            wWH.Show();
        }

        protected void btnSaveWH_Click(object sender, DirectEventArgs e)
        {
            Page.Validate("WeeklyHoliday");
            if (Page.IsValid)
            {
                if (cmbDayOfWeek.SelectedItem == null || cmbDayOfWeek.SelectedItem.Value == null)
                {
                    NewMessage.ShowWarningMessage("Days of Week is required");
                    cmbDayOfWeek.Focus();
                    return;
                }
                
                if (cmbFullOrHalf.SelectedItem == null || cmbFullOrHalf.SelectedItem.Value == null)
                {
                    NewMessage.ShowWarningMessage("Holiday Type is required");
                    cmbFullOrHalf.Focus();
                    return;
                }

                if (cmbWHEffectiveFrom.SelectedItem == null || cmbWHEffectiveFrom.SelectedItem.Value == null)
                {
                    NewMessage.ShowWarningMessage("Starting from is required");
                    cmbWHEffectiveFrom.Focus();
                    return;
                }

                WeeklyHoliday weekly = new WeeklyHoliday();

                if (!string.IsNullOrEmpty(hdnWeeklyHolidayId.Text))
                    weekly.ID = int.Parse(hdnWeeklyHolidayId.Text);

                weekly.EffectiveFrom = int.Parse(cmbWHEffectiveFrom.SelectedItem.Value);
                weekly.DayOfWeek = int.Parse(cmbDayOfWeek.SelectedItem.Value);
                weekly.IsFullDay = (cmbFullOrHalf.SelectedItem.Value == "FULL_DAY" ? true : false);
                weekly.CompanyId = SessionManager.CurrentCompanyId;
                Status status = HolidayManager.SaveUpdateWeeklyHoliday(weekly);
                if (status.IsSuccess)
                {
                    NewMessage.ShowNormalMessage("Weekly holiday saved successfully.");
                    wWH.Close();
                    BindWeeklyHolidays();
                }
                else
                    NewMessage.ShowWarningMessage(status.ErrorMessage);
            }
        }

        protected void btnEditWH_Click(object sender, DirectEventArgs e)
        {
            ClearWeeklyHoliday();
            int id = int.Parse(hdnWeeklyHolidayId.Text);

            WeeklyHoliday obj = HolidayManager.GetWeeklyHolidayById(id);
            if (obj != null)
            {
                cmbDayOfWeek.SetValue(obj.DayOfWeek.ToString());
                if (obj.IsFullDay)
                    cmbFullOrHalf.SetValue("FULL_DAY");
                else
                    cmbFullOrHalf.SetValue("SECOND_HALF");

                cmbWHEffectiveFrom.SetValue(obj.EffectiveFrom.ToString());

                btnSaveWH.Text = "Update";

                wWH.Center();
                wWH.Show();
            }
        }

        protected void btnDeleteWH_Click(object sender, DirectEventArgs e)
        {
            int id = int.Parse(hdnWeeklyHolidayId.Text);

            if (holidayMgr.DeleteWeeklyHoliday(id))
            {
                NewMessage.ShowNormalMessage("Weekly holiday deleted successfully.");
                BindWeeklyHolidays();
            }
            else
                NewMessage.ShowWarningMessage("Weekly holiday is associated with the attendance, so can not be deleted.");
        }

        private void ClearAnnualHolidays()
        {
            txtHolidayName.Clear();
            txtStartDate.Clear();
            txtEndDate.Clear();
            txtEndDate.Disable();
            chkUpto.Checked = false;
            chkUpto.Enable();
            chkIsNationalHoldiay.Clear();
            selectorBranch.SelectedItems.Clear();
            selectorBranch.UpdateSelectedItems();
            cmbGender.SetValue("-2");
            cmbGender.Enable();
            btnSaveAnnualHoliday.Text = "Add";
        }

        protected void btnAddAnnualHoliday_Click(object sender, DirectEventArgs e)
        {
            ClearAnnualHolidays();
            hdnAnnualHolidayId.Text = "";
            wAnnualHoliday.Center();
            wAnnualHoliday.Show();
        }

        protected void btnSaveAnnualHoliday_Click(object sender, DirectEventArgs e)
        {
            Page.Validate("AnnualHoliday");
            if (Page.IsValid)
            {
                List<HolidayList> list = new List<HolidayList>();

                int totalDays = 1;
                DateTime startDateEng = txtStartDate.SelectedDate;

                if (chkUpto.Checked)
                {
                    if (txtStartDate.SelectedDate != txtEndDate.SelectedDate)
                    {
                        if (txtStartDate.SelectedDate > txtEndDate.SelectedDate)
                        {
                            NewMessage.ShowWarningMessage("Starting date cannot be greater than upto date.");
                            txtStartDate.Focus();
                            return;
                        }

                        TimeSpan ts = txtEndDate.SelectedDate - txtStartDate.SelectedDate;

                        totalDays += ts.Days;                        
                    }
                }

                for (int i = 0; i < totalDays; i++)
                {
                    HolidayList entity = new HolidayList();
                    entity.CompanyId = SessionManager.CurrentCompanyId;
                    entity.EngDate = startDateEng.AddDays(i);
                    entity.Date = BLL.BaseBiz.GetAppropriateDate(entity.EngDate.Value);
                    entity.Description = txtHolidayName.Text.Trim();

                    if (holidayMgr.IsHolidayExists(entity.EngDate.ToString(), entity.Description) && string.IsNullOrEmpty(hdnAnnualHolidayId.Text))
                    {
                        NewMessage.ShowWarningMessage(string.Format("Holiday already exists for the date {0}", entity.Date));
                        return;
                    }

                    entity.IsNationalHoliday = chkIsNationalHoldiay.Checked;
                    entity.AppliesTo = -1;

                    string branchIds = "";

                    foreach (Ext.Net.ListItem item in selectorBranch.SelectedItems)
                    {
                        branchIds += item.Value + ",";
                    }

                    if(branchIds.Length > 0)
                        entity.BranchIDList = branchIds.Substring(0, branchIds.Length - 1);


                    if (entity.IsNationalHoliday.Value)
                        entity.AppliesTo = -1;
                    else
                    {
                        if (cmbGender.SelectedItem == null || cmbGender.SelectedItem.Value == null || cmbGender.SelectedItem.Value == "-2")
                        {
                            NewMessage.ShowWarningMessage("Applies to is required");
                            cmbGender.Focus();
                            return;
                        }

                        entity.AppliesTo = int.Parse(cmbGender.SelectedItem.Value);
                    }

                    // validate for hoiday group selection branch can not be selected
                    if (entity.AppliesTo >= 1 && branchIds.Length >= 1)
                    {
                        NewMessage.ShowWarningMessage("Both branch filter and applies to can not be selected.");
                        return;
                    }

                    entity.Created = DateTime.Now;
                    list.Add(entity);

                }
                
                Status status = new Status();

                if (string.IsNullOrEmpty(hdnAnnualHolidayId.Text))
                {
                    status = HolidayManager.SaveAnnualHolidays(list);
                    if (status.IsSuccess)
                        NewMessage.ShowNormalMessage("Holidays saved successfully.");
                }
                else
                {
                    list[0].ID = int.Parse(hdnAnnualHolidayId.Text);
                    status = HolidayManager.UpdateAnnualHoliday(list[0]);
                    if (status.IsSuccess)
                        NewMessage.ShowNormalMessage("Holiday saved successfully.");
                }

                if (status.IsSuccess)
                {
                    wAnnualHoliday.Close();
                    X.Js.Call("searchList");
                }
                else
                    NewMessage.ShowWarningMessage(status.ErrorMessage);
            }
        }

        protected void btnEditAH_Click(object sender, DirectEventArgs e)
        {
            int id = int.Parse(hdnAnnualHolidayId.Text);

            ClearAnnualHolidays();

            HolidayList obj = HolidayManager.GetAnnualHolidayById(id);
            if (obj != null)
            {
                txtHolidayName.Text = obj.Description;
                txtStartDate.SetValue(obj.EngDate.Value);
                chkUpto.Disable();

                chkIsNationalHoldiay.Checked = obj.IsNationalHoliday.Value;

                if (obj.BranchIDList != null)
                {
                    selectorBranch.SelectedItems.Clear();

                    string[] spl = obj.BranchIDList.Split(',');
                    foreach (var item in spl)
                    {
                        selectorBranch.SelectedItems.Add(new Ext.Net.ListItem() { Value = item });
                    }
                    selectorBranch.UpdateSelectedItems();
                }

                cmbGender.SetValue(obj.AppliesTo.ToString());

                if (obj.IsNationalHoliday.Value)
                    cmbGender.Disable();

                btnSaveAnnualHoliday.Text = "Update";

                wAnnualHoliday.Center();
                wAnnualHoliday.Show();
            }
        }

        protected void btnDeleteAH_Click(object sender, DirectEventArgs e)
        {
            int id = int.Parse(hdnAnnualHolidayId.Text);

            if (holidayMgr.DeleteHoliday(id))
            {
                NewMessage.ShowNormalMessage("Annual holiday deleted successfully.");
                X.Js.Call("searchList");
            }
            else
                NewMessage.ShowWarningMessage("Holiday is associated with the attendance, so can not be deleted.");
        }



    }
}