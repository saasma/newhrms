using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Base;
using BLL.Manager;
using Utils.Helper;
using Utils.Web;
using DAL;
using Utils.Calendar;

namespace Web.CP
{
    public partial class AEDeduction : BasePage, IDisableAfterCalculation
    {
        PayManager payMgr = new PayManager();
        int employeeId = 0;
        private PEmployeeDeduction empDeduction;
      
        public int DeductionId
        {
            get
            {
                if (ViewState["DeductionId"] == null)
                    return 0;
                return int.Parse(ViewState["DeductionId"].ToString());
            }
            set
            {
                ViewState["DeductionId"] = value;
            }
        }

        public PageModeList PageModeList
        {
            get
            {
                if (ViewState["PageModeList"] == null)
                    return PageModeList.NotSet;
                return (PageModeList)ViewState["PageModeList"];
            }
            set
            {
                ViewState["PageModeList"] = value;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            RestoreValues();
            if (!IsPostBack)
            {
                Initialise();
            }
            //else if (this.Request.Form["__EVENTTARGET"].Equals(upDeposit.ClientID))
            //{
            //    LoadDeposits();
            //}
            JavascriptHelper.AttachPopUpCode(Page, "popDepositList", "MGDeductionDepositList.aspx", 480, 260);
            JavascriptHelper.AttachEnableDisablingJSCode(chkHasVariableAmount, txtAmount2.ClientID,false);
            JavascriptHelper.AttachShowHideJSCode(chkApplyStatusFilter, divStatus.ClientID);
            JavascriptHelper.AttachPopUpCode(Page, "displayInstallment", "EMICalculation.aspx", 620, 560);
            JavascriptHelper.AttachPopUpCode(Page, "deductionHistory", "DeductionLoanAdvanceHistory.aspx", 720, 500);
            JavascriptHelper.CheckboxUncheckedConfirm(chkIsDeductionEnabled,
                                                      "Do you really want to disable the deduction for all employees?");

            JavascriptHelper.CheckboxCheckedConfirm(chkIsExemptFromIncomeTax,
                                                      "Do you really want to change the setting for tax?");
        }

        void RestoreValues()
        {
            txtAdvance.Text = Request.Form[txtAdvance.UniqueID];
            txtTakenOn.Text = Request.Form[txtTakenOn.UniqueID];
            txtInstallmentAmount.Text = Request.Form[txtInstallmentAmount.UniqueID];
            txtPaid.Text = Request.Form[txtPaid.UniqueID];
            txtBalance.Text = Request.Form[txtBalance.UniqueID];
            txtNoOfInstallments.Text = Request.Form[txtNoOfInstallments.UniqueID];
            txtRemainingInstallments.Text = Request.Form[txtRemainingInstallments.UniqueID];
            txtIsFixedInstallment.Value = Request.Form[txtIsFixedInstallment.UniqueID];
        }

        void EnableDisableAllValidators(bool enable)
        {
            //first disable all validation control
            foreach (BaseValidator ctl in Page.GetValidators("AEDeduction"))
            {
                ctl.Enabled = enable;
            }
        }

        void DisableValidationForHiddenPnlControls()
        {
            EnableDisableAllValidators(false);
                  
            
            //make apprpriate 
            int selIndex = ddlCalculation.SelectedIndex;
            if (selIndex == 1 || selIndex == 2)
            {
                valReqdAmt1.Enabled = true;
                valCompAmt1.Enabled = true;
                //valReqdCutOff1.Enabled = true;
                //valCompCutOff1.Enabled = true;
            }
            else if (selIndex == 3)
            {
                valReqdRate.Enabled = true;
                valCompRate.Enabled = true;
                valReqdCutOff2.Enabled = true;
                valCompCutOff2.Enabled = true;
            }
            else if (selIndex == 4 || selIndex == 5)
            {
                if (chkHasVariableAmount.Checked)
                {
                    valReqdAmt2.Enabled = true;
                    valCompAmt2.Enabled = true;
                }
                else
                {
                    valLAAdvance.Enabled = true;
                    valLATakenOn.Enabled = true;
                    //valLAInstallmentAmount.Enabled = true;
                }
            }
        }

        void Initialise()
        {
            calFrom.IsEnglishCalendar = this.IsEnglish;
            calTo.IsEnglishCalendar = this.IsEnglish;
            calFrom.SelectTodayDate();
            calTo.SelectTodayDate();
            calRepaymentTakenOn.SelectTodayDate();

            SessionManager.EmployeeDeduction = null;

            LoadDeposits();
            IncomeList();

            BizHelper.Load(new DeductionCalculation(), ddlCalculation);

            pnl2.Style.Add("display", "none");
            pnl3.Style.Add("display", "none");

            //for update
            this.CustomId = UrlHelper.GetIdFromQueryString("Id");
            this.DeductionId = UrlHelper.GetIdFromQueryString("DId");
            if (this.CustomId != 0)
            {
                this.PageModeList = PageModeList.UpdateEmployee;
                this.tdNotes.Visible=false;
                this.txtTitle.ReadOnly = true;
                this.rowCalculation.Style["display"] = "none";

               

            }
            else if (this.DeductionId != 0)
            {
                this.PageModeList = PageModeList.Update;
            }
            else
            {
                this.PageModeList = PageModeList.Insert;
            }

            JobStatus obJs = new JobStatus();
            List<KeyValue> list = obJs.GetMembers();
            list.RemoveAt(0);
            chkStatusList.DataSource = list;
            chkStatusList.DataBind();

           

            switch (this.PageModeList)
            {
                case PageModeList.UpdateEmployee:
                    //PDeduction deduction = new PDeduction();
                    //deduction.DeductionId = id;
                    //PDeduction entity = payMgr.GetById(deduction);

                    empDeduction = payMgr.GetEmployeeDeduction(this.CustomId);
                    if (empDeduction != null)
                    {
                        this.employeeId = empDeduction.EmployeeId;

                        btnSave.Text = Resources.Messages.Update;
                        PDeduction deduction = empDeduction.PDeduction;
                        Process(ref deduction, PageMode.Display);
                        Process(ref empDeduction);
                        SessionManager.EmployeeDeduction = empDeduction;
                        btnInstallment.OnClientClick =
                            string.Format(btnInstallment.OnClientClick, this.CustomId);
                        //EnableDisableControl(empDeduction.EmployeeId);

                        hiddenEmployeeId.Value = empDeduction.EmployeeId.ToString();
                        return;

                    }
                    break;
                case PageModeList.Update:

                    PDeduction ded = payMgr.GetDeductionById(this.DeductionId);
                    Process(ref ded, PageMode.Display);
                    btnSave.Text = Resources.Messages.Update;
                    pnlLoanRepayment.Style["display"] = "none";
                    break;
            }

            // prepare for adding new Deduction
            btnInstallment.OnClientClick =
                       string.Format(btnInstallment.OnClientClick, "0");

            PrepareForAdd();
        }

        /// <summary>
        /// Prepare for adding by hiding value fields
        /// </summary>
        void PrepareForAdd()
        {
            //for variable & fixed amount
            pnl1.Visible = false;

            pnlLoanRepayment.Style["display"] = "none";

            //percent of income
            rowRate.Visible = false;
            rowCutOff.Visible = false;


            //for loan/advance, hide all except first row
            colAmount.Visible = false;

            for (int i = 1; i < tblLoanAdvance.Rows.Count-1; i++)
            {
                tblLoanAdvance.Rows[i].Visible = false;
            }
        }

        void IncomeList()
        {
            //chkListIncomes.DataSource = payMgr.GetEmployeeIncomeList(employeeId);
            chkListIncomes.DataSource = PayManager.GetIncomeListByCompany(SessionManager.CurrentCompanyId);
            chkListIncomes.DataBind();            
        }
        void LoadDeposits()
        {
            //ListItem item = ddlDeposits.Items[0];
            //ListItem selItem = ddlDeposits.SelectedItem;
            //ddlDeposits.DataSource = payMgr.GetDepositList(SessionManager.CurrentCompanyId);
            //ddlDeposits.DataBind();
            //ddlDeposits.Items.Insert(0, item);
            //if (selItem != null)
            //{
            //    UIHelper.SetSelectedInDropDown(ddlDeposits, selItem.Value);
            //}
        }

        void Process(ref PDeduction entity, PageMode mode)
        {
            int EditSequence = 0;

            if (mode == PageMode.Retrieve)
            {
                entity = new PDeduction();
                entity.CompanyId = SessionManager.CurrentCompanyId;
                EditSequence = hdEditSequence.Value == "" ? 0 : int.Parse(hdEditSequence.Value);
                entity.EditSequence = EditSequence+1;
                entity.Title = txtTitle.Text.Trim();
                entity.Notes = txtNotes.Text.Trim();
                entity.Abbreviation = txtAbbreviation.Text.Trim();
                //if (ddlDeposits.SelectedValue != "-1")
                //    entity.DeductionDepositListId = int.Parse(ddlDeposits.SelectedValue);

                string calculationType = ddlCalculation.SelectedValue;

                entity.Calculation = calculationType;
                entity.IsExemptFromIncomeTax = chkIsExemptFromIncomeTax.Checked;

                //For loan/advance it can not be disabled
                if (calculationType == DeductionCalculation.Advance || calculationType == DeductionCalculation.LOAN)
                    entity.IsEnabled = true;
                else
                    entity.IsEnabled = chkIsDeductionEnabled.Checked;

                //for percent of income
                if (calculationType == DeductionCalculation.PERCENT)
                {

                    if (!string.IsNullOrEmpty(txtRateForAll.Text.Trim()))
                    {
                        entity.RatePercentForAll = double.Parse(txtRateForAll.Text.Trim());
                    }


                    entity.EnableStatusFilter = chkApplyStatusFilter.Checked;
                    if (entity.EnableStatusFilter != null && entity.EnableStatusFilter.Value)
                    {
                        //for status list
                        foreach (ListItem item in chkStatusList.Items)
                        {
                            if (item.Selected)
                            {
                                PDeductionStatus income = new PDeductionStatus();
                                income.StatusId = int.Parse(item.Value);
                                entity.PDeductionStatus.Add(income);
                            }
                        }
                    }

                    //for income list
                    foreach (ListItem item in chkListIncomes.Items)
                    {
                        if (item.Selected)
                        {
                            PDeductionIncome income = new PDeductionIncome();
                            income.IncomeId = int.Parse(item.Value);
                            entity.PDeductionIncomes.Add(income);
                        }
                    }
                }
                else if (calculationType == DeductionCalculation.LOAN || calculationType == DeductionCalculation.Advance)
                {
                    entity.HasVariableAmount = chkHasVariableAmount.Checked;
                }
                if (calculationType == DeductionCalculation.Advance)
                    entity.IsRefundableAdvance = chkIsRefundableAdvance.Checked;

                if (calculationType == DeductionCalculation.LOAN)
                {
                    if (rdbIsEMI.Checked)
                    {
                        entity.IsEMI = true;
                        
                    }
                    else
                        entity.IsEMI = false;
                }
            }
            else
            {
                txtTitle.Text = entity.Title;
                txtAbbreviation.Text = entity.Abbreviation;
                txtNotes.Text = entity.Notes;
                //if (entity.DeductionDepositListId != null)
                //    UIHelper.SetSelectedInDropDown(ddlDeposits, entity.DeductionDepositListId);

                UIHelper.SetSelectedInDropDown(ddlCalculation, entity.Calculation);
                hdEditSequence.Value = entity.EditSequence == null ? "0" : entity.EditSequence.ToString();
                chkIsExemptFromIncomeTax.Checked = entity.IsExemptFromIncomeTax.Value;
                if (entity.IsEnabled == null)
                    chkIsDeductionEnabled.Checked = true;
                else
                    chkIsDeductionEnabled.Checked = entity.IsEnabled.Value;

                string calculationType = entity.Calculation;

                if (calculationType == DeductionCalculation.PERCENT)
                {
                    if (entity.RatePercentForAll != null)
                        txtRateForAll.Text = entity.RatePercentForAll.ToString();

                    if (entity.EnableStatusFilter != null && entity.EnableStatusFilter.Value)
                    {
                        chkApplyStatusFilter.Checked = entity.EnableStatusFilter.Value;
                        //for status list
                        foreach (PDeductionStatus income in entity.PDeductionStatus)
                        {
                            ListItem item = chkStatusList.Items.FindByValue(income.StatusId.ToString());
                            if (item != null)
                                item.Selected = true;
                        }
                    }

                    //for income list
                    foreach (PDeductionIncome income in entity.PDeductionIncomes)
                    {

                        ListItem item = chkListIncomes.Items.FindByValue(income.IncomeId.ToString());
                        if (item != null)
                            item.Selected = true;
                    }
                }
                else if (calculationType == DeductionCalculation.LOAN || calculationType == DeductionCalculation.Advance)
                {
                    chkHasVariableAmount.Checked = entity.HasVariableAmount.Value;
                }
                if (calculationType == DeductionCalculation.Advance && entity.IsRefundableAdvance != null)
                    chkIsRefundableAdvance.Checked = entity.IsRefundableAdvance.Value;

                if (calculationType == DeductionCalculation.LOAN)
                {
                    if (entity.IsEMI == null || entity.IsEMI.Value)
                    {
                        rdbIsEMI.Checked = true;
                        rdbIsSimple.Checked = false;
                    }
                    else
                    {
                        rdbIsEMI.Checked = false;
                        rdbIsSimple.Checked = true;
                    }

                   
                }
            }
        }
    
        bool Process(ref PEmployeeDeduction empDedEntity)
        {
            int EditSequence = 0;
            if (empDedEntity != null)
            {
                //entity = empDedEntity.PDeduction;

                hdEEditSequence.Value = empDedEntity.EditSequence == null ? "0" : empDedEntity.EditSequence.ToString();

                if (empDedEntity.ExcludeLoanInstallment != null)
                    chkExcludeRegularInstallmentInRetirement.Checked = empDedEntity.ExcludeLoanInstallment.Value;

                //show only when valid
                if (empDedEntity.IsValid.Value)
                {

                    string calculationType = ddlCalculation.SelectedValue;
                    //for pnl1
                    if (calculationType == DeductionCalculation.FIXED
                    || calculationType == DeductionCalculation.VARIABLE)
                    {
                        txtAmount1.Text = GetCurrency(empDedEntity.Amount);
                        //txtCutOff1.Text = Util.FormatAmount(empDedEntity.CutOff);
                    }
                    else if (calculationType == DeductionCalculation.PERCENT)
                    {
                        txtRate.Text = Util.FormatForInput(empDedEntity.Rate);
                        txtCutOff2.Text = GetCurrency(empDedEntity.CutOff);
                       
                    }
                    else if (calculationType == DeductionCalculation.LOAN_Repayment)
                    {
                        txtMonthlyRepayment.Text = empDedEntity.Amount == null ? "0" : GetCurrency(empDedEntity.Amount);
                        txtLoanAmount.Text = empDedEntity.AdvanceLoanAmount == null ? "0" : GetCurrency(empDedEntity.AdvanceLoanAmount);
                        txtLoanAccountNo.Text = empDedEntity.LoanAccount;
                        txtInstallmentNo.Text = empDedEntity.InstallmentNo == null ? "" : empDedEntity.InstallmentNo.ToString();
                        txtTotalInstallment.Text = empDedEntity.TotalInstallment == null ? "" : empDedEntity.TotalInstallment.ToString();
                        calFrom.SetSelectedDate(empDedEntity.StartingFrom, IsEnglish);
                        calTo.SetSelectedDate(empDedEntity.LastPayment, IsEnglish);
                        
                        if (empDedEntity.TakenOnEng != null)
                        {
                            CustomDate date = new CustomDate(empDedEntity.TakenOnEng.Value.Day, empDedEntity.TakenOnEng.Value.Month,
                                empDedEntity.TakenOnEng.Value.Year, true);
                            calRepaymentTakenOn.SetSelectedDate(date.ToString(), true);
                        }

                        if (empDedEntity.StartingFromEng != null && empDedEntity.LastPaymentEng != null)
                        {
                            DateTime date1 = empDedEntity.StartingFromEng.Value;
                            DateTime date2 = empDedEntity.LastPaymentEng.Value;

                            txtRepaymentLoanNoOfInstallment.Text =
                                (((date2.Year - date1.Year) * 12) + date2.Month - date1.Month).ToString();
                        }   


                    }
                    else if (calculationType == DeductionCalculation.LOAN
                        || calculationType == DeductionCalculation.Advance)
                    {
                        txtAdvance.Text = GetCurrency(empDedEntity.AdvanceLoanAmount);
                        txtTakenOn.Text = empDedEntity.TakenOn;

                        if (calculationType == DeductionCalculation.LOAN && (empDedEntity.PDeduction.IsEMI == null || empDedEntity.PDeduction.IsEMI == false))
                        {
                            btnMarkAsPaid.Visible = true;

                            int empId = empDedEntity.EmployeeId;
                            int dedId = empDedEntity.DeductionId;
                            if (BLL.BaseBiz.PayrollDataContext.LoanAdjustments.Any(x => x.EmployeeId == empId
                                && x.DeductionId == dedId && x.PayrollPeriodId == CommonManager.GetLastPayrollPeriod().PayrollPeriodId))
                            {
                                btnMarkAsPaid.Text = "Remove Loan Adjustment";
                            }
                        }

                        if (chkHasVariableAmount.Checked)
                            txtAmount2.Text = GetCurrency(empDedEntity.Amount);
                        else
                        {
                            if( calculationType==DeductionCalculation.Advance)
                            {
                                // Get advance installment which could be changed due to Adjustment, if not normal amount
                                decimal? installmentAmount =
                                    PayManager.GetAdvanceInstallmentAmount(empDedEntity.DeductionId,
                                                                           empDedEntity.EmployeeId);
                                if( installmentAmount.HasValue)
                                    txtInstallmentAmount.Text = GetCurrency(installmentAmount);
                                else
                                {
                                    txtInstallmentAmount.Text = GetCurrency(empDedEntity.InstallmentAmount);
                                }
                            }
                            else
                            {
                                txtInstallmentAmount.Text = GetCurrency(empDedEntity.InstallmentAmount);
                            }
                           
                           
                            txtPaid.Text =GetCurrency(empDedEntity.Paid);
                            if (calculationType == DeductionCalculation.LOAN)
                                txtBalance.Text = GetCurrency(empDedEntity.Balance);
                            else
                            {
                                txtBalance.Text = GetCurrency(empDedEntity.AdvanceLoanAmount - empDedEntity.Paid);
                            }
                            txtNoOfInstallments.Text = empDedEntity.NoOfInstallments.ToString();
                            txtRemainingInstallments.Text = empDedEntity.RemainingInstallments.ToString();
                            txtIsFixedInstallment.Value = (empDedEntity.LoanFixedInstallmentAmount == null ? "false" :
                                empDedEntity.LoanFixedInstallmentAmount.ToString().ToLower());
                            if (empDedEntity.SINewPPMTAmount != null && empDedEntity.SINewPPMTAmount != 0)
                            {
                                txtIsFixedInstallment.Value = "true";
                                txtSINewPPMTAmount.Value = empDedEntity.SINewPPMTAmount.ToString();
                                txtInstallmentAmount.Text = GetCurrency(empDedEntity.SINewPPMTAmount);
                            }
                        }
                    }
                }
            }
            else
            {                
                empDedEntity = new PEmployeeDeduction();
                PDeduction entity= new PDeduction();
                Process(ref entity,PageMode.Retrieve);
                empDedEntity.PDeduction = entity;


                empDedEntity.ExcludeLoanInstallment = chkExcludeRegularInstallmentInRetirement.Checked;
                empDedEntity.EmployeeId = employeeId;

                EditSequence = hdEEditSequence.Value == "" ? 0 : int.Parse(hdEEditSequence.Value);
                empDedEntity.EditSequence = EditSequence + 1;

                string calculationType = ddlCalculation.SelectedValue;
                //for pnl1
                if (calculationType==DeductionCalculation.FIXED
                    || calculationType==DeductionCalculation.VARIABLE)
                {
                    empDedEntity.Amount = decimal.Parse(txtAmount1.Text.Trim());
                    //empDedEntity.CutOff = decimal.Parse(txtCutOff1.Text.Trim());
                }
                else if (calculationType == DeductionCalculation.PERCENT)
                {
                    empDedEntity.Rate = double.Parse(txtRate.Text.Trim());
                    empDedEntity.CutOff = decimal.Parse(txtCutOff2.Text.Trim());                    
                }
                else if (calculationType == DeductionCalculation.LOAN_Repayment)
                {
                    empDedEntity.Amount = decimal.Parse(txtMonthlyRepayment.Text.Trim());

                    if (!string.IsNullOrEmpty(txtLoanAmount.Text.Trim()))
                        empDedEntity.AdvanceLoanAmount = decimal.Parse(txtLoanAmount.Text.Trim());

                    empDedEntity.LoanAccount = txtLoanAccountNo.Text.Trim();

                    empDedEntity.TotalInstallment = int.Parse(txtTotalInstallment.Text.Trim());
                    empDedEntity.InstallmentNo = int.Parse(txtInstallmentNo.Text.Trim());

                    CustomDate start = calFrom.SelectedDate;
                    start = new CustomDate(1, start.Month, start.Year, IsEnglish);

                    CustomDate end = calTo.SelectedDate;
                    end = new CustomDate(DateHelper.GetTotalDaysInTheMonth(end.Year, end.Month, IsEnglish), end.Month, end.Year, IsEnglish);

                    if (end.EnglishDate < start.EnglishDate)
                    {
                        WarningMsgCtl1.InnerHtml = "To date should be greater or equal to start date.";
                        WarningMsgCtl1.Hide = false;
                        return false;
                    }

                    empDedEntity.TakenOnEng = calRepaymentTakenOn.SelectedDate.EnglishDate;
                    empDedEntity.TakenOn = BLL.BaseBiz.GetAppropriateDate(empDedEntity.TakenOnEng.Value);

                    empDedEntity.StartingFrom = start.ToString();
                    empDedEntity.StartingFromEng = start.EnglishDate;
                    empDedEntity.LastPayment = end.ToString();
                    empDedEntity.LastPaymentEng = end.EnglishDate;

                    DateTime date1 = empDedEntity.StartingFromEng.Value;
                    DateTime date2 = empDedEntity.LastPaymentEng.Value;

                    empDedEntity.NoOfInstallments =
                        (((date2.Year - date1.Year) * 12) + date2.Month - date1.Month);

                }
                else if (calculationType == DeductionCalculation.LOAN
                    || calculationType == DeductionCalculation.Advance)
                {
                    empDedEntity.AdvanceLoanAmount = decimal.Parse(txtAdvance.Text.Trim());
                    empDedEntity.TakenOn = txtTakenOn.Text.Trim();

                  

                    if (empDedEntity.PDeduction.HasVariableAmount.Value)
                    {
                        empDedEntity.Amount = decimal.Parse(txtAmount2.Text.Trim());
                        empDedEntity.InstallmentAmount = null;
                        empDedEntity.Balance = null;
                        empDedEntity.NoOfInstallments = null;
                        //empDedEntity.RemainingInstallments = null;

                        empDedEntity.InterestRate = null;
                        empDedEntity.MarketRate = null;
                        empDedEntity.ToBePaidOver = null;
                        empDedEntity.PaymentTerms = null;
                        empDedEntity.StartingFrom = null;
                    }
                    else
                    {

                        empDedEntity.InstallmentAmount = decimal.Parse(txtInstallmentAmount.Text.Trim());
                        empDedEntity.Balance = decimal.Parse(txtBalance.Text.Trim());
                        empDedEntity.NoOfInstallments = int.Parse(txtNoOfInstallments.Text.Trim());
                        //empDedEntity.RemainingInstallments = int.Parse(txtRemainingInstallments.Text.Trim());

                        PEmployeeDeduction sessionDeduction = SessionManager.EmployeeDeduction;
                        empDedEntity.AmountWithInitialInterest = sessionDeduction.AmountWithInitialInterest;
                        empDedEntity.InterestRate = sessionDeduction.InterestRate;
                        empDedEntity.MarketRate = sessionDeduction.MarketRate;
                        empDedEntity.ToBePaidOver = sessionDeduction.ToBePaidOver;
                        empDedEntity.PaymentTerms = sessionDeduction.PaymentTerms;
                        empDedEntity.StartingFrom = sessionDeduction.StartingFrom;
                        empDedEntity.LastPayment = sessionDeduction.LastPayment;
                        empDedEntity.InterestAmount = sessionDeduction.InterestAmount;

                        bool isFixed = false;
                        if (bool.TryParse(txtIsFixedInstallment.Value.Trim(), out isFixed))
                            empDedEntity.LoanFixedInstallmentAmount = isFixed;
                    }
                }               
            }

            return true;
        }
        //private void EnableDisableControl(int employeeId)
        //{
        //    payMgr.SetPrivilegesToControls(new Control[] {
        //        ddlDeposits,ddlCalculation,chkIsExemptFromIncomeTax,txtAmount1,txtRate,chkHasVariableAmount,
        //        txtAmount2,txtAdvance,txtTakenOn,txtInstallmentAmount,txtBalance,txtNoOfInstallments,txtRemainingInstallments
        //    }, !payMgr.IsTaxCalculatedForEmployee(employeeId));
        //}

        protected void btnMarkAsPaid_Click(object sender, EventArgs e)
        {
            bool isDeletion = false;
            Status status =  PayManager.TerminateOrMarkAsLoanPaid(this.CustomId,ref isDeletion);
            if (status.IsSuccess)
            {
                if (isDeletion)
                {
                    btnMarkAsPaid.Text = "Mark the Loan as Paid";
                    JavascriptHelper.DisplayClientMsg("Loan termination has been deleted..", Page);
                }
                else
                {
                    btnMarkAsPaid.Text = "Remove Loan Adjustment";
                    JavascriptHelper.DisplayClientMsg("Loan has been marked as paid.", Page);
                }
                
            }
            else
            {
                JavascriptHelper.DisplayClientMsg(status.ErrorMessage, Page);

            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            int? EditSequence = 0;
            DisableValidationForHiddenPnlControls();
            Page.Validate("AEDeduction");

            string invalidText = Util.HasInvalidCharacterForXMLParsingAndImport(txtTitle.Text.Trim());

            if (!string.IsNullOrEmpty(invalidText))
            {
                WarningMsgCtl1.InnerHtml = "\"" + invalidText + "\" character can not be set in the title.";
                WarningMsgCtl1.Hide = false;
                return;
            }

            if (Page.IsValid)
            {
                switch (this.PageModeList)
                {
                    case PageModeList.Insert:
                        PDeduction entity = null;

                        Process(ref entity, PageMode.Retrieve);
                        //entity.Title = txtTitle.Text.Trim();
                        //entity.Abbreviation = txtAbbreviation.Text.Trim();
                        //entity.CompanyId = SessionManager.CurrentCompanyId;

                        
                        payMgr.Save(entity);
                        JavascriptHelper.DisplayClientMsg("Deduction saved.", Page, "closePopup();");
                        break;

                    case PageModeList.UpdateEmployee:
                        PEmployeeDeduction empDeduction = null;

                        bool status =  Process(ref empDeduction);

                        if (status == false)
                            return;

                        empDeduction.IsValid = true;
                        //empDeduction.PDeduction.DeductionId =this.Id;
                        empDeduction.EmployeeDeductionId = this.CustomId;

                        EditSequence = payMgr.GetEmployeeDeduction(empDeduction.EmployeeDeductionId).EditSequence;
                        EditSequence = EditSequence == null ? 0 : EditSequence;
                        if (EditSequence != int.Parse(hdEEditSequence.Value == ""?"0":hdEEditSequence.Value))
                        {
                            JavascriptHelper.DisplayClientMsg(Resources.Messages.ConcurrencyError, Page, "closePopup();");
                            //WarningMsgCtl1.InnerHtml = Resources.Messages.ConcurrencyError;
                            //WarningMsgCtl1.Hide = false;
                            return;
                        }
                        // if new click from calcuation then only generate unique id generating in each update changes UniqueID
                        // as if values changed from LoanAdjustment also
                        empDeduction.GenerateUniqueIDForFreshLoan = bool.Parse(hdnStartNewInstallmentAsInfoChanged.Value);
                        payMgr.Update(empDeduction);
                        hdEEditSequence.Value = (int.Parse(hdEEditSequence.Value) + 1).ToString();
                        JavascriptHelper.DisplayClientMsg("Deduction updated.", Page, "closePopup();");
                        break;
                    case PageModeList.Update:
                        PDeduction entity1 = null;
                        Process(ref entity1, PageMode.Retrieve);
                        entity1.DeductionId = this.DeductionId;

                        EditSequence = payMgr.GetDeductionById(entity1.DeductionId).EditSequence;
                        EditSequence = EditSequence == null ? 0 : EditSequence;
                        if (EditSequence != int.Parse(hdEditSequence.Value == ""?"0":hdEditSequence.Value))
                        {
                            JavascriptHelper.DisplayClientMsg(Resources.Messages.ConcurrencyError, Page, "closePopup();");
                            //WarningMsgCtl1.InnerHtml = Resources.Messages.ConcurrencyError;
                            //WarningMsgCtl1.Hide = false;
                            return;
                        }
                        bool status1 = payMgr.Update(entity1);
                        hdEditSequence.Value = (int.Parse(hdEditSequence.Value) + 1).ToString();
                        if (status1)
                            JavascriptHelper.DisplayClientMsg("Deduction updated.", Page, "closePopup();");
                        else
                            JavascriptHelper.DisplayClientMsg("Deduction update failed.", Page, "closePopup();");
                        break;
                }
            }
            EnableDisableAllValidators(true);
        }


        protected void Page_PreRender(object sender, EventArgs e)
        {
            //make apprpriate pnl visible
            int selIndex = ddlCalculation.SelectedIndex;
           
            if (selIndex == 1 || selIndex == 2) // Variable and Fixed
            {
                pnl1.Style.Remove("display");
                pnl2.Style.Add("display", "none");
                pnl3.Style.Add("display", "none");
                pnlLoanRepayment.Style.Add("display", "none");
                chkIsDeductionEnabled.Style.Remove("display");
            }
            else if (selIndex == 3) // Percent of Income
            {
                pnl1.Style.Add("display", "none");
                pnl2.Style.Remove("display");
                pnl3.Style.Add("display", "none");
                pnlLoanRepayment.Style.Add("display", "none");
                chkIsDeductionEnabled.Style.Remove("display");
                if (this.PageModeList != BLL.PageModeList.UpdateEmployee)
                    rowRateForAll.Style.Remove("display");
            }
            else if (selIndex == 4 || selIndex == 5) // Loan and Advance
            {
                pnl1.Style.Add("display", "none");
                pnl2.Style.Add("display", "none");
                pnl3.Style.Remove("display");
                pnlLoanRepayment.Style.Add("display", "none");
                chkIsDeductionEnabled.Style.Add("display", "none");
            }
            else if (selIndex == 6)
            {
                pnl1.Style.Add("display", "none");
                pnl2.Style.Add("display", "none");
                pnl3.Style.Add("display", "none");
                if (this.PageModeList == BLL.PageModeList.UpdateEmployee)
                    pnlLoanRepayment.Style.Remove("display");
            }

            if (selIndex == 4)
                rowLoanType.Style.Remove("display");
            else
                rowLoanType.Style["display"] = "none";

            if (selIndex == 5)
                rowchkIsRefundableAdvance.Style["display"] = "";
            else
                rowchkIsRefundableAdvance.Style["display"] = "none";

            EnableDisableAllValidators(true);
            txtAmount2.Enabled = chkHasVariableAmount.Checked;

            if (chkApplyStatusFilter.Checked)
                divStatus.Style["display"] = "";

            if (this.PageModeList == BLL.PageModeList.UpdateEmployee)
            {
                if (ddlCalculation.SelectedValue == DeductionCalculation.LOAN ||
                   ddlCalculation.SelectedValue == DeductionCalculation.LOAN_Repayment)
                {
                    chkExcludeRegularInstallmentInRetirement.Visible = true;
                }
            }
        }

        protected void txtRepaymentLoanNoOfInstallment_Change(object sender, EventArgs e)
        {

            int installment = 0;

            if (int.TryParse(txtRepaymentLoanNoOfInstallment.Text, out installment))
            {
                CustomDate startDate = calFrom.SelectedDate;

                DateTime startingDate = new DateTime(startDate.Year, startDate.Month, 1);
                int lastPaymentMonth = (int)((installment - 1) * 1);
                DateTime lastPaymentDate = startingDate.AddMonths(lastPaymentMonth);

                CustomDate endingDate = new CustomDate(1, lastPaymentDate.Month, lastPaymentDate.Year,IsEnglish);
                calTo.SetSelectedDate(endingDate.ToString(), IsEnglish);
            }
            else
            {
                WarningMsgCtl1.InnerHtml = "Invalid No of installments.";
                WarningMsgCtl1.Hide = false;
            }

        }

        protected void chkHasVariableAmount_CheckedChanged(object sender, EventArgs e)
        {

        }


        #region IDisableAfterCalculation Members

        public void DisableControlsAfterSalary()
        {
            if (this.PageModeList == BLL.PageModeList.Update)
            {
                //if (PayManager.HasDeductionIncludedInCalculation(this.DeductionId))
                //{
                //    this.ddlCalculation.Enabled = false;

                //    this.chkIsRefundableAdvance.Enabled = false;
                //    this.rdbIsEMI.Enabled = false;
                //    this.rdbIsSimple.Enabled = false;
                //    //ddlDeposits.Enabled = false;
                //    //chkIsExemptFromIncomeTax.Enabled = false;
                //    chkListIncomes.Enabled = false;
                //    chkHasVariableAmount.Enabled = false;
                //}
            }
            else if (this.CustomId != 0)
            {
                empDeduction = payMgr.GetEmployeeDeduction(this.CustomId);

                if (empDeduction != null && PayManager.HasDeductionIncludedInCalculation(empDeduction.DeductionId))
                {
                    this.ddlCalculation.Enabled = false;

                    this.chkIsRefundableAdvance.Enabled = false;
                    this.rdbIsEMI.Enabled = false;
                    this.rdbIsSimple.Enabled = false;
                    //ddlDeposits.Enabled = false;
                    //chkIsExemptFromIncomeTax.Enabled = false;
                    chkListIncomes.Enabled = false;
                    chkHasVariableAmount.Enabled = false;
                }
            }
           

            //if add mode then only show emi or simple option in other mode disable it
            if (this.PageModeList != BLL.PageModeList.Insert)
            {
                rdbIsEMI.Enabled = false;
                rdbIsSimple.Enabled = false;
            }
        }

        #endregion
    }
}
