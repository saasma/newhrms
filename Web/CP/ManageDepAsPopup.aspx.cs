﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using BLL;
using BLL.Manager;
using DAL;
using Utils.Web;
using Utils.Helper;
using System.Text;
using Utils;

namespace Web.CP
{
    public partial class ManageDepAsPopup : BasePage
    {
        DepartmentManager depMgr = new DepartmentManager();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Initialise();
            }
        }

        void Initialise()
        {
            List<Branch> parentPages = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId);

            //valRegPhone.ValidationExpression = Config.PhoneRegExp;
            valRegFax.ValidationExpression = Config.FaxRegExp;

            if (!string.IsNullOrEmpty(Request.QueryString["Id"]))
            {
                hdnDepartmentId.Value = Request.QueryString["Id"];
                LoadEditData(int.Parse(hdnDepartmentId.Value));
            }
            else
                btnSave.Text = Resources.Messages.Save;

            cmbBranch.DataSource = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId);
            cmbBranch.DataBind();
        }

        void LoadEmployees(int departmentId)
        {
            // ListItem item = ddlDepHead.Items[0];
            //ddlDepHead.DataSource = empMgr.GetEmployeesUnderDepartment(departmentId);
            //ddlDepHead.DataBind();
            //ddlDepHead.Items.Insert(0, item);
            int total = 0;
            storeDepartment.DataSource =
                new EmployeeManager().GetEmployees(SessionManager.CurrentCompanyId);
            storeDepartment.DataBind();

        }

        Department ProcessDepartment(Department b)
        {
            if (b != null)
            {
                txtDepartmentName.Text = b.Name;
                txtDesc.Text = b.Description;
                txtPhone.Text = b.Phone;
                txtFax.Text = b.Fax;
                txtEmail.Text = b.Email;
                if(b.LocationBranchId!=null)
                cmbBranch.SelectedValue = b.LocationBranchId.ToString();

                if (b.Code != null)
                    txtCode.Text = b.Code;
                else
                    txtCode.Text = "";

                if (b.HeadEmployeeId != null)
                {
                    cmbEmployee.SetValue(b.HeadEmployeeId.ToString());
                    // hiddenEmployee.Value = b.HeadEmployeeId.ToString();
                }   //UIHelper.SetSelectedInDropDown(ddlDepHead, b.HeadEmployeeId.ToString());

            }

            else
            {
                b = new Department();
                b.Name = txtDepartmentName.Text.Trim();
                //b.BranchId = int.Parse(ddlBranch.SelectedValue);
                b.Description = txtDesc.Text;
                b.Phone = txtPhone.Text.Trim();
                b.Fax = txtFax.Text.Trim();
                b.Email = txtEmail.Text.Trim();
                b.Code = txtCode.Text.Trim();
                if (!string.IsNullOrEmpty(cmbBranch.SelectedItem.Value))
                    b.LocationBranchId = int.Parse(cmbBranch.SelectedItem.Value);


                int employeeId = 0;



                if (cmbEmployee.SelectedItem == null || cmbEmployee.SelectedItem.Value == "" || cmbEmployee.SelectedItem.Value == "-1")
                    b.HeadEmployeeId = null;
                else
                {
                    if (int.TryParse(cmbEmployee.SelectedItem.Value, out employeeId))
                        b.HeadEmployeeId = int.Parse(cmbEmployee.SelectedItem.Value);
                    else
                        b.HeadEmployeeId = null;
                }
                //if (ddlDepHead.SelectedValue.Equals("-1") == false)
                //    b.HeadEmployeeId = int.Parse(ddlDepHead.SelectedValue);
                //else
                //    b.HeadEmployeeId = null;

                return b;
            }
            return null;
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            //if (ddlBranch.SelectedValue == "-1")
            //{
            //    JavascriptHelper.DisplayClientMsg("Please select a branch.", Page);
            //    return;
            //}

            if (Page.IsValid)
            {
                Department d = ProcessDepartment(null);

                if (!string.IsNullOrEmpty(hdnDepartmentId.Value))
                {
                    int selDepId = int.Parse(hdnDepartmentId.Value);
                    d.DepartmentId = selDepId;
                    bool status = depMgr.Update(d);
                    if (status == false)
                    {
                        divWarningMsg.InnerHtml = "Department name already exists.";//Resources.Messages.DepartmentUpdatedMsg;
                        divWarningMsg.Hide = false;
                        return;
                    }
                    //divMsgCtl.InnerHtml = Resources.Messages.DepartmentUpdatedMsg;
                    //divMsgCtl.Hide = false;

                    JavascriptHelper.DisplayClientMsg(Resources.Messages.DepartmentUpdatedMsg, Page, "closePopup();");

                }
                else
                {
                    bool status = depMgr.Save(d);
                    if (status == false)
                    {
                        divWarningMsg.InnerHtml = "Department name already exists.";//Resources.Messages.DepartmentUpdatedMsg;
                        divWarningMsg.Hide = false;
                        return;
                    }

                    JavascriptHelper.DisplayClientMsg(Resources.Messages.DepartmentSavedMsg, Page, "closePopup();");

                    //divMsgCtl.InnerHtml = Resources.Messages.DepartmentSavedMsg;
                    //divMsgCtl.Hide = false;

                }
                CommonManager.ResetCache();
                ClearDepartmentFields();
            }
            else
            {
                JavascriptHelper.DisplayClientMsg("Validation fail.", Page);
            }
        }

        void ClearDepartmentFields()
        {
            txtDepartmentName.Text = "";
            cmbEmployee.SetValue("-1");
            //cmbEmployee.ClearValue();
            //ListItem item = ddlDepHead.Items[0];
            //ddlDepHead.Items.Clear();
            //ddlDepHead.Items.Insert(0, item);
            txtDesc.Text = "";
            txtPhone.Text = "";
            txtFax.Text = "";
            txtCode.Text = "";
            txtEmail.Text = "";
            btnSave.Text = Resources.Messages.Save;
        }

        private void LoadEditData(int departmentId)
        {            
            Department d = depMgr.GetById(departmentId);
            if (d != null)
            {
                ProcessDepartment(d);
                btnSave.Text = Resources.Messages.Update;
                txtDepartmentName.Focus();
            }
        }


    }
}
