﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Base;
using BLL.Manager;
using DAL;
using Utils.Helper;
using Utils.Web;
using Utils.Calendar;
using System.IO;
using BLL.Entity;


namespace Web.CP
{
    public partial class AEIncome : BasePage,IDisableAfterCalculation
    {
        CommonManager commMgr = new CommonManager();
        PayManager payMgr = new PayManager();
        LeaveAttendanceManager leaveMgr = new LeaveAttendanceManager();
        PEmployeeIncome empIncome = null;
        private bool isBasicSalary = false;
        private PayrollPeriod payrollPeriod = null;


        public int IncomeId
        {
            get
            {
                if (ViewState["IncomeId"] == null)
                    return 0;
                return int.Parse(ViewState["IncomeId"].ToString());
            }
            set
            {
                ViewState["IncomeId"] = value;
            }
        }

        public PageModeList PageModeList
        {
            get
            {
                if (ViewState["PageModeList"] == null)
                    return PageModeList.NotSet;
                return (PageModeList)ViewState["PageModeList"];
            }
            set
            {
                ViewState["PageModeList"] = value;
            }
        }

        #region "Check Eligibility Export"
        public void btnCheckEligibility_Click(object sender, EventArgs e)
        {
            int incomeId = 0;

            switch (this.PageModeList)
            {
                case  BLL.PageModeList.Update:
                    incomeId = this.IncomeId;
                    break;
                case BLL.PageModeList.UpdateEmployee:

                    PEmployeeIncome empInc = new PayManager().GetEmployeeIncome(this.CustomId);
                    incomeId = empInc.IncomeId;
                    break;
            }

            bool readingSumForMiddleFiscalYearStartedReqd = false;
            int lastPeirod = CommonManager.GetLastPayrollPeriod().PayrollPeriodId;
            int startingPayrollPeriodId = 0, endingPayrollPeriodId = 0;
            // int notWorkedMonthInFiscalYear = 0, passedMonthWorked = 0;


            PayrollPeriod startingMonthParollPeriodInFiscalYear =
               CalculationManager. GenerateForPastIncome(
                    lastPeirod, ref readingSumForMiddleFiscalYearStartedReqd,
                    ref startingPayrollPeriodId, ref endingPayrollPeriodId);


            ReportDataSetTableAdapters.Report_GetFestivalEligibilityReportTableAdapter mainAdapter = new ReportDataSetTableAdapters.Report_GetFestivalEligibilityReportTableAdapter();

            Report.Templates.Pay.FestivalEligibility mainReport = new Report.Templates.Pay.FestivalEligibility();

            BLL.BaseBiz.SetConnectionPwd(mainAdapter.Connection);
            ReportDataSet.Report_GetFestivalEligibilityReportDataTable mainTable =
                mainAdapter.GetData(incomeId, SessionManager.CurrentCompanyId,startingPayrollPeriodId,endingPayrollPeriodId);



            PIncome income = new PayManager().GetIncomeById(incomeId);



            if (income != null)
            {
                mainReport.lblAllowanceName.Text = income.Title;
                if (income.OFEmployeeStatus != null)
                    mainReport.lblEmployeeStatus.Text = JobStatus.GetValueForDisplay(income.OFEmployeeStatus.Value);
                if (income.OFEligibilityDateEng != null)
                    mainReport.lblEligibilityDate.Text = income.OFEligibilityDateEng.Value.ToString("dd-MMM-yyyy");
                if (income.OFMinimumDays != null)
                    mainReport.lblMinimumDays.Text = income.OFMinimumDays.ToString();
                if (income.OFProportionateDays != null)
                    mainReport.lblProportionateDays.Text = income.OFProportionateDays.ToString();
                if (income.OFCountWorkdaysFrom != null && income.OFCountWorkdaysFrom == FestivalAllowanceCountWorkdaysFrom.SpecificDate)
                    mainReport.lblCountFrom.Text = income.OFWorkdaysFromDateEng.Value.ToString("dd-MMM-yyyy");
                else if(income.OFCountWorkdaysFrom != null)
                    mainReport.lblCountFrom.Text = income.OFCountWorkdaysFrom;
                //mainReport.Month = payrollPeriod.Month;
                //mainReport.Year = payrollPeriod.Year.Value;
                //mainReport.IsIncome = true;
                //mainReport.CompanyId = SessionManager.CurrentCompanyId;

                mainReport.DataSource = mainTable;
                mainReport.DataMember = "Report";


                using (MemoryStream stream = new MemoryStream())
                {
                    mainReport.ExportToXls(stream);
                    HttpContext.Current.Response.Clear();
                    HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";
                    HttpContext.Current.Response.AppendHeader("Content-Type", "application/vnd.ms-excel");
                    HttpContext.Current.Response.AddHeader("Content-Length", stream.ToArray().Length.ToString());
                    HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment; filename=Eligibility_Report" + "Report" + ".xls");
                    HttpContext.Current.Response.OutputStream.Write(stream.ToArray(), 0, stream.ToArray().Length);
                    HttpContext.Current.Response.End();
                }

              

            }
        }
        #endregion

        protected void Page_Init(object sender, EventArgs e)
        {
            // Load status for PF Effective from
            JobStatus statues = new JobStatus();
            ddlEmployeeStatus.DataSource = statues.GetMembersForHirarchyView();
            ddlEmployeeStatus.DataValueField = "Key";
            ddlEmployeeStatus.DataTextField = "Value";

            ddlEmployeeStatus.DataBind();
            //ddlEmployeeStatus.Items.AddRange(items);
            //ddlEmployeeStatus.Items.RemoveAt(1);

            //if (ddlCalculation.SelectedValue == IncomeCalculation.FESTIVAL_ALLOWANCE
            //    && rdbCalculatedAmount.Checked && IsPostBack)
            {
                //make this enable to preserve the selected value at postback
                //BindFestivalIncomeList();
                //chkListIncomes2.Enabled = true;
            }
        }


        protected void Page_Load(object sender, EventArgs e)
        {


            this.CustomId = UrlHelper.GetIdFromQueryString("Id");
            PEmployeeIncome empIncome = null;
            if (this.CustomId != 0)
            {
                empIncome = new PayManager().GetEmployeeIncome(this.CustomId);
            }
            //if (empIncome == null)
                payrollPeriod = CommonManager.GetLastPayrollPeriod();
            //else
            //    payrollPeriod = CommonManager.GetValidLastPayrollPeriodForIncrement(empIncome.EmployeeId);


            if (!IsPostBack)
            {
               

                Initialise();
            }
           // JavascriptHelper.AttachEnableDisablingJSCode(chkApplyOnceFor, tblStatuesContainer.ClientID, false,"applyOnceStateChanged");
            JavascriptHelper.AttachEnableDisablingJSCode(chkEligibilityDate, calEligibilityDate.ClientID + "_date", false, "eligibilityStateChanged");

            JavascriptHelper.AttachShowHideJSCode(chkApplyStatusFilter, divStatus.ClientID);

            JavascriptHelper.AttachEnableDisablingJSCode(chkDeemedIsYearIncome, ddlDeemedMonth.ClientID, false);
            JavascriptHelper.AttachEnableDisablingJSCode(chkIsIncrementMode, calIncrementDate.ClientID + "_date", false, "modeDateChanged");
            JavascriptHelper.AttachPopUpCode(Page, "incrementHistory", "IncomeIncrementHistory.aspx", 650, 400);
            JavascriptHelper.CheckboxUncheckedConfirm(chkIsIncomeEnabled,
                                                      "Do you really want to disable income for all employees?");
            JavascriptHelper.CheckboxUncheckedConfirm(chkIsEmployeeIncomeEnabled,
                                                      "Do you really want to disable income for this employee?");

            trApplyFestivalIncome.Visible = this.PageModeList == BLL.PageModeList.UpdateEmployee;
            JavascriptHelper.CheckboxCheckedConfirm(chkApplyFestivalIncome, "Do you want to Enable this income to this employee and Override the setting if the employee is not eligible?");
            
            Register();
        }

        /// <summary>
        /// Register javascript like current payroll period starting date to validate for reterospect increment
        /// </summary>
        protected  void Register()
        {
            

           
            if( payrollPeriod==null     ||
                (payrollPeriod != null && empIncome != null && CalculationManager.IsCalculationSavedForEmployee(payrollPeriod.PayrollPeriodId,
                 empIncome.EmployeeId)   )
                )
            {
                DisableChildControls(rowIncrement);


                calEligibilityDate.Enabled = false;

                rowIncrement.Attributes.Add("title", Resources.Messages.ValidPayrollPeriodNotDefined);
                chkIsIncrementMode.ToolTip = Resources.Messages.ValidPayrollPeriodNotDefined;
            }
            else
            {
                string code = string.Format("var startingDate = '{0}';", payrollPeriod.StartDate);

                PEmployeeIncrement lastIncludedIncrement =
                    PayManager.GetLastIncludedIncrement(this.CustomId);

                //If has last included increment set in javascript for the validation of current increment
                if (lastIncludedIncrement != null)
                    code += string.Format("var lastIncludedIncrementDate = '{0}';",
                                          lastIncludedIncrement.EffectiveFromDate);

                


                Page.ClientScript.RegisterClientScriptBlock(
                    this.GetType(), "DateReg123", code, true);

            }



            string addModeCode = "var isAddMode = " + (this.CustomId == 0 ? "true;" : "false;");
            Page.ClientScript.RegisterClientScriptBlock(
                this.GetType(), "DateReg123AddMode", addModeCode, true);

           

        }

      
        void Initialise()
        {
            ddlEvents.DataSource = CommonManager.GetServieEventTypes();
            ddlEvents.DataBind();

            calIncrementDate.IsEnglishCalendar = SessionManager.CurrentCompany.IsEnglishDate;
            calIncrementDate.SelectTodayDate();
            calEligibilityDate.IsEnglishCalendar = SessionManager.CurrentCompany.IsEnglishDate;
            calEligibilityDate.SelectTodayDate();
            //calEligibilityDate.IsSkipYear = true;
            calWorkdaysFromDate.IsEnglishCalendar = SessionManager.CurrentCompany.IsEnglishDate;
            calWorkdaysFromDate.SelectTodayDate();

            fieldSetDefinedType.Visible = CommonManager.CompanySetting.HasLevelGradeSalary;

            // If has valid paryoll period then select first day as Default for incrmentation
            if (payrollPeriod != null)
            {
                calIncrementDate.SetSelectedDate(payrollPeriod.StartDate, IsEnglish);
            }

            this.CustomId = UrlHelper.GetIdFromQueryString("Id");

            this.IncomeId = UrlHelper.GetIdFromQueryString("IId");
            if (this.CustomId != 0)
            {
                chkSkipMinMaxChecking.Visible = true;
                empIncome = payMgr.GetEmployeeIncome(this.CustomId);
                this.PageModeList = PageModeList.UpdateEmployee;
                this.tdNotes.Visible = false;
            }
            else if (this.IncomeId != 0)
            {
                this.PageModeList = PageModeList.Update;
                this.txtUnitRate.Visible = false;
                this.spanUnitRate.Visible = false;
            }
            else
            {
                this.PageModeList = PageModeList.Insert;
                this.txtUnitRate.Visible = false;
                this.spanUnitRate.Visible = false;
            }


            JobStatus obJs = new JobStatus();
            List<KeyValue> list = obJs.GetMembers();
            list.RemoveAt(0);
            chkStatusList.DataSource = list;
            chkStatusList.DataBind();


            //chkListSubjective.Items[0].Selected = (SessionManager.CurrentCompany.CompanyType
            //    == CompanyType.REGULAR);

            //if company is regular, make the income taxable by default
            chkIsTaxable.Checked = !(SessionManager.CurrentCompany.CompanyType
                == CompanyType.REGULAR);

            btnOk.Text = Resources.Messages.Save;
            BizHelper.Load(new IncomeCalculation(), ddlCalculation);
            BizHelper.Load(new FestivalAllowanceCountWorkdaysFrom(), ddlCountWorkdaysFrom);
            ddlApplyOnceFor.DataSource = DateManager.GetCurrentMonthList();
            ddlApplyOnceFor.DataBind();
                
            //chkPaymentMonthList.DataSource = ddlApplyOnceFor.DataSource;
            //chkPaymentMonthList.DataBind();

            ddlDeemedMonth.DataSource = DateManager.GetCurrentMonthList();
            ddlDeemedMonth.DataBind();

            chkListIncomes1.DataSource = payMgr.GetIncomeListForIncome(
                SessionManager.CurrentCompanyId, empIncome != null ? empIncome.PIncome.IncomeId : 0);
            chkListIncomes1.DataBind();

            List<GetIncomeForIncomesResult> list2 = payMgr.GetIncomeListForIncome(
                SessionManager.CurrentCompanyId, empIncome != null ? empIncome.PIncome.IncomeId : 0);

            list2.Add(new GetIncomeForIncomesResult { IncomeId=0,Title="Income PF" });


            chkDeemedIncomeList.DataSource = list2;
            chkDeemedIncomeList.DataBind();


            BindFestivalIncomeList();
            switch (this.PageModeList)
            {
                case PageModeList.UpdateEmployee:
                    PIncome inc = empIncome.PIncome;
                    Process(ref inc);
                    Process(ref empIncome);
                    ProcessIncrement(empIncome, PageMode.Display);
                    isBasicSalary = inc.IsBasicIncome;
                    btnOk.Text = Resources.Messages.Update;
                    hiddenEmployeeId.Value = empIncome.EmployeeId.ToString();

                    
                    break;

                case PageModeList.Update:
                    PIncome income = payMgr.GetIncomeById(this.IncomeId);
                    Process(ref income);
                    //Process(ref empIncome);
                    //ProcessIncrement(empIncome, PageMode.Display);
                    isBasicSalary = income.IsBasicIncome;
                    btnOk.Text = Resources.Messages.Update;
                    InitialiseForSaveState();
                    break;
                case PageModeList.Insert:
                    InitialiseForSaveState();
                    break;
            }

        }

        private void BindFestivalIncomeList()
        {
            chkListIncomes2.DataSource = payMgr.GetIncomeListForIncome(
                SessionManager.CurrentCompanyId, empIncome != null ? empIncome.PIncome.IncomeId : 0);
            chkListIncomes2.DataBind();
        }

        void BasicSalaryDisplay()
        {
            chkPF.Enabled = false;
            chkCountForLeaveEncasement.Enabled = false;
            ddlCalculation.Enabled = false;
            //chkListSubjective.Enabled = false;
            chkIsTaxable.Enabled = false;
            //chkApplyOnceFor.Enabled = false;
            DisableChildControls(tblStatuesContainer);
            chkIsIncomeEnabled.Visible = false;
            chkIsEmployeeIncomeEnabled.Visible = false;
            chkIsGrade.Visible = false;
            chkIsForeignAllowance.Visible = false;
        }

        void InitialiseForSaveState()
        {
            switch (this.PageModeList)
            {
                case PageModeList.Insert:
                    chkBasicIncome.Visible = false;
                    
                    rowIncrement.Visible = false;
                    //Percent of income

                    pnlFixedVariable.Visible = false;
                    pnlPercentOfSales.Visible = false;
                    //pnlConcessionalFacility.Visible = false;
                    colFestival.Visible = false;
                    //pnlUnitRate.Visible = false;

                    //Festival allowance
                    //txtPercent.Visible = false;
                    //RequiredFieldValidator10.Visible = false;
                    //CompareValidator4.Visible = false;
                    //lblFestival.Visible = false;
                    tableDeemedAmount.Style["display"] = "none";

                    chkIsIncomeEnabled.Visible = false;
                    chkIsEmployeeIncomeEnabled.Visible = false;
                    break;

                case PageModeList.Update:
                   
                    rowIncrement.Visible = false;
                    //Percent of income

                    tableDeemedAmount.Style["display"] = "none";


                    pnlFixedVariable.Visible = false;
                    pnlPercentOfSales.Visible = false;
                    //pnlConcessionalFacility.Visible = false;
                    colFestival.Visible = false;
                    //pnlUnitRate.Visible = false;

                    //Festival allowance
                    //txtPercent.Visible = false;
                    //RequiredFieldValidator10.Visible = false;
                    //CompareValidator4.Visible = false;
                    //lblFestival.Visible = false;

                    chkIsEmployeeIncomeEnabled.Visible = false;
                   
                   
                    break;
            }
            
        }

        #region "Input/Output Processing"

        

        /// <summary>
        /// Input/Output processes for PIncome
        /// </summary>
        void Process(ref PIncome entity)
        {
            if (entity == null)
            {
                entity = new PIncome();
                entity.Title = Server.HtmlEncode(txtTitle.Text.Trim());
                entity.Abbreviation = txtAbbreviation.Text.Trim();
                entity.Notes = txtNotes.Text.Trim();
                entity.CompanyId = SessionManager.CurrentCompanyId;
                entity.EditSequence = int.Parse(hdEditSequence.Value == "" ? "0" : hdEditSequence.Value) + 1;
                entity.Calculation = ddlCalculation.SelectedValue;
                entity.IsChildrenAllowance = chkIsChildrenAllowance.Checked;

                // PF will be only for Fixed type
                if (entity.Calculation == IncomeCalculation.FIXED_AMOUNT)
                {
                    if (entity.IsBasicIncome == false)
                        entity.IsGrade = chkIsGrade.Checked;
                    entity.IsPFCalculated = chkPF.Checked;
                }
                else
                    entity.IsPFCalculated = false;

                if (entity.Calculation == IncomeCalculation.FESTIVAL_ALLOWANCE || entity.Calculation==IncomeCalculation.PERCENT_OF_INCOME 
                    || entity.Calculation == IncomeCalculation.Unit_Rate)
                    entity.IsPFCalculated = chkPF.Checked;

                //not used for deemed income types
                if (ddlCalculation.SelectedValue == IncomeCalculation.DEEMED_INCOME)
                {
                    entity.IsCountForLeaveEncasement = false;
                    entity.IsTaxable = true;
                }
                else
                {
                    entity.IsCountForLeaveEncasement = chkCountForLeaveEncasement.Checked;
                    entity.IsTaxable = !chkIsTaxable.Checked;
                }

                if (entity.Calculation == IncomeCalculation.FIXED_AMOUNT 
                    || entity.Calculation == IncomeCalculation.PERCENT_OF_INCOME
                    || entity.Calculation == IncomeCalculation.FESTIVAL_ALLOWANCE
                    || entity.Calculation==IncomeCalculation.Unit_Rate)
                {
                    if (ddlDefinedType.SelectedValue.ToLower().Equals("true"))
                    {
                        entity.IsDefinedType = true;
                        entity.XType = int.Parse(ddlXType.SelectedValue);
                        entity.YType = int.Parse(ddlYType.SelectedValue);
                    }
                }

                entity.IsUpdatedWithLeave = chkUpdateWithLeave.Checked;

                if (entity.Calculation == IncomeCalculation.FESTIVAL_ALLOWANCE)
                {
                    
                    entity.DeductUPLAbsentForFestivalWorkdays = chkDeductUPLAbsentFestival.Checked;
                    entity.DeductAbsentOnlyForFestivalWorkdays = chkDeductAbsentFestival.Checked;
                    entity.DeductStopPaymentForFestivalWorkdays = chkDeductStopPayment.Checked;
                    entity.ExcludeRetiringBeforeEligiblityDate = chkExcludeRetBeforeEligiblityDate.Checked;    

                    entity.IsApplyOnceFor = true;// chkApplyOnceFor.Checked;
                    //if (entity.IsApplyOnceFor)
                    {
                        entity.OFMonth = int.Parse(ddlApplyOnceFor.SelectedValue);
                        entity.DoNotShowThisFestivalTypeInSalary = bool.Parse(ddlFestivalSalaryStatus.SelectedValue);
                        entity.OFHasEligiblity = chkEligibilityDate.Checked;
                        entity.OFEmployeeStatus = int.Parse(ddlEmployeeStatus.SelectedValue);
                        if (entity.OFHasEligiblity.Value)
                        {
                            entity.OFEligibilityDate = calEligibilityDate.SelectedDate.ToString();
                            entity.OFProportionateDays = int.Parse(txtProportionateWorkDays.Text);
                            if (!string.IsNullOrEmpty(txtProportionateWorkDaysBasedOn.Text))
                                entity.ProportionateBased = double.Parse(txtProportionateWorkDaysBasedOn.Text);
                            entity.OFMinimumDays = double.Parse(txtMinimumWorkDays.Text);
                            entity.OFCountWorkdaysFrom = ddlCountWorkdaysFrom.SelectedValue;
                            if (entity.OFCountWorkdaysFrom == FestivalAllowanceCountWorkdaysFrom.SpecificDate)
                            {
                                entity.OFWorkdaysFromDate = calWorkdaysFromDate.SelectedDate.ToString();
                            }
                        }

                    }
                    //else
                    //entity.OFMonth = null;
                }
                else
                    entity.IsApplyOnceFor = false;

                //entity.IsSubjectOfIncomeTax = chkListSubjective.Items[0].Selected;
                
                entity.IsVariableAmount = rdbVariableAmount.Checked;
                entity.IsCalculatedAmount = rdbCalculatedAmount.Checked;

                

                string cal = entity.Calculation;

                if (cal == IncomeCalculation.VARIABLE_AMOUNT)
                {
                    entity.IsTreatLikeOneTimeIncome = chkIsTreatLikeOneTimeIncome.Checked;
                    entity.IsLateIncomeType = chkIsLateIncomeType.Checked;
                }
                if (cal == IncomeCalculation.FIXED_AMOUNT)
                {
                    if (chkIsGrade.Checked == false && chkBasicIncome.Checked == false)
                    {
                        //if (ddlPaymentType.SelectedValue == "true")
                        //{
                        //    foreach (ListItem item in chkPaymentMonthList.Items)
                        //    {
                        //        if (item.Selected)
                        //        {
                        //            entity.PIncomeMonths.Add(new PIncomeMonth { Month = int.Parse(item.Value) });
                        //        }
                        //    }
                        //    entity.IsNotRegular = true;
                        //}
                    }
                }
                if (cal == IncomeCalculation.PERCENT_OF_INCOME)
                {

                    entity.EnableStatusFilter = chkApplyStatusFilter.Checked;
                    if (entity.EnableStatusFilter != null && entity.EnableStatusFilter.Value)
                    {
                        //for status list
                        foreach (ListItem item in chkStatusList.Items)
                        {
                            if (item.Selected)
                            {
                                PIncomeStatus income = new PIncomeStatus();
                                income.Status = int.Parse(item.Value);
                                entity.PIncomeStatus.Add(income);
                            }
                        }
                    }

                    IncomeDeductionUIHelper.SetSelectedIncomes(entity.IncomeId, entity, chkListIncomes1);
                    if (this.PageModeList == PageModeList.Insert || this.PageModeList == PageModeList.Update)
                        entity.RatePercentForAll = float.Parse(txtRatePercent2.Text.Trim());

                    if (!string.IsNullOrEmpty(txtMinAmount.Text.Trim()))
                        entity.MinAmount = Convert.ToDecimal(txtMinAmount.Text.Trim());

                    if (!string.IsNullOrEmpty(txtMaxAmount.Text.Trim()))
                        entity.MaxAmount = Convert.ToDecimal(txtMaxAmount.Text.Trim());
                }
                else if (cal == IncomeCalculation.Unit_Rate)
                {
                    entity.IsTreatLikeOneTimeIncome = chkIsTreatLikeOneTimeIncome.Checked;

                    entity.UnitRateDeductHolidayWeeklyAndNational = chkDeductHoliday.Checked;
                    entity.UnitRateDeductPaidLeaves = chkDeductPaidLeave.Checked;
                    entity.UnitRateDeductUnpaidLeaves = chkDeductUnpaidLeave.Checked;
                    entity.UnitRateDeductHalfdayHoliday = chkDeductHalfdayHoliday.Checked;
                    entity.UnitRateDeductHalfdayPaidLeave = chkDeductHalfdayLeave.Checked;
                    entity.UnitRateProportionatePreviousMonth = chkProportionatePreviousMonth.Checked;
                    entity.UnitRateBasedOnWorkdays = chkUnitRateBasedOnWorkdays.Checked;

                    if (!string.IsNullOrEmpty(txtUnitRateHalfdayValue.Text.Trim()))
                        entity.UnitRateHalfdayCounting = double.Parse(txtUnitRateHalfdayValue.Text.Trim());
                }
                else if (cal == IncomeCalculation.FESTIVAL_ALLOWANCE)
                {
                    if (!rdbVariableAmount.Checked)
                    {
                        IncomeDeductionUIHelper.SetSelectedIncomes(entity.IncomeId, entity, chkListIncomes2);
                        if (this.PageModeList == PageModeList.Insert || this.PageModeList == PageModeList.Update)
                            entity.RatePercentForAll = float.Parse(txtPercent.Text.Trim());
                    }
                }
                else if (cal == IncomeCalculation.DEEMED_INCOME)
                {
                    entity.DeemedIsAmount = rdbDeemedAmount.Checked;

                    if (ddlDeemedCalculationOption.SelectedValue == "true")
                        entity.DeemedIsVariableType = true;
                    else
                        entity.DeemedIsVariableType = false;

                    if (entity.DeemedIsAmount.Value == false)
                    {
                        IncomeDeductionUIHelper.SetSelectedIncomes(entity.IncomeId, entity, chkDeemedIncomeList);
                        if (this.PageModeList == PageModeList.Insert || this.PageModeList == PageModeList.Update)
                            entity.RatePercentForAll = float.Parse(txtDeemedRate.Text.Trim());
                    }
                    entity.DeemedIsYearlyIncome = chkDeemedIsYearIncome.Checked;
                    if (entity.DeemedIsYearlyIncome.Value)
                        entity.DeemedMonth = int.Parse(ddlDeemedMonth.SelectedValue);
                }
                entity.IsEnabled = chkIsIncomeEnabled.Checked;
                entity.AddTaxFromTheBeginning = chkAddTaxFromTheBeginning.Checked;
            }
            else
            {
                hdEditSequence.Value = entity.EditSequence == null ? "" : entity.EditSequence.ToString();
                txtTitle.Text = Server.HtmlDecode( entity.Title);
                txtAbbreviation.Text = entity.Abbreviation;
                txtNotes.Text = entity.Notes;
                chkBasicIncome.Checked = entity.IsBasicIncome;
                if (entity.IsBasicIncome)
                    chkBasicIncome.Visible = true;
                else
                {
                    if (entity.IsGrade.HasValue)
                        chkIsGrade.Checked = entity.IsGrade.Value;
                }

                if (entity.IsChildrenAllowance != null && entity.IsChildrenAllowance.Value)
                    chkIsChildrenAllowance.Checked = entity.IsChildrenAllowance.Value;

                chkPF.Checked = entity.IsPFCalculated;
                chkCountForLeaveEncasement.Checked = entity.IsCountForLeaveEncasement;
                //chkCountAsReim.Checked = entity.IsCountAsReimursement;
                chkUpdateWithLeave.Checked = entity.IsUpdatedWithLeave;
                //chkApplyOnceFor.Checked = entity.IsApplyOnceFor;
                chkIsTaxable.Checked = !entity.IsTaxable;
                if (entity.Calculation == IncomeCalculation.FESTIVAL_ALLOWANCE)
                {
                    if (entity.DeductUPLAbsentForFestivalWorkdays != null)
                        chkDeductUPLAbsentFestival.Checked = entity.DeductUPLAbsentForFestivalWorkdays.Value;
                    if (entity.DeductAbsentOnlyForFestivalWorkdays != null)
                        chkDeductAbsentFestival.Checked = entity.DeductAbsentOnlyForFestivalWorkdays.Value;
                    if (entity.DeductStopPaymentForFestivalWorkdays != null)
                        chkDeductStopPayment.Checked = entity.DeductStopPaymentForFestivalWorkdays.Value;
                    if (entity.ExcludeRetiringBeforeEligiblityDate != null)
                        chkExcludeRetBeforeEligiblityDate.Checked = entity.ExcludeRetiringBeforeEligiblityDate.Value;

                    if (entity.IsApplyOnceFor)
                    {
                        UIHelper.SetSelectedInDropDown(ddlApplyOnceFor, entity.OFMonth);
                        if (entity.DoNotShowThisFestivalTypeInSalary != null)
                            UIHelper.SetSelectedInDropDown(ddlFestivalSalaryStatus, entity.DoNotShowThisFestivalTypeInSalary.Value.ToString().ToLower());
                        chkEligibilityDate.Checked = entity.OFHasEligiblity.Value;
                        UIHelper.SetSelectedInDropDown(ddlEmployeeStatus, entity.OFEmployeeStatus);
                        if (chkEligibilityDate.Checked)
                        {
                            calEligibilityDate.SetSelectedDate(entity.OFEligibilityDate,
                                                               SessionManager.CurrentCompany.IsEnglishDate);

                            txtMinimumWorkDays.Text = entity.OFMinimumDays.ToString();
                            txtProportionateWorkDays.Text = entity.OFProportionateDays.ToString();
                            if (entity.ProportionateBased != null)
                                txtProportionateWorkDaysBasedOn.Text = entity.ProportionateBased.ToString();

                            UIHelper.SetSelectedInDropDown(ddlCountWorkdaysFrom, entity.OFCountWorkdaysFrom);

                            if (entity.OFCountWorkdaysFrom == FestivalAllowanceCountWorkdaysFrom.SpecificDate)
                            {
                                calWorkdaysFromDate.SetSelectedDate(entity.OFWorkdaysFromDate, IsEnglish);
                                
                            }
                        }
                    }
                }


                if (entity.IsDefinedType != null && entity.IsDefinedType.Value)
                {
                    UIHelper.SetSelectedInDropDown(ddlDefinedType, "true");
                    UIHelper.SetSelectedInDropDown(ddlXType, entity.XType.Value);
                    UIHelper.SetSelectedInDropDown(ddlYType, entity.YType.Value);
                }
                

                //chkListSubjective.Items[0].Selected = entity.IsSubjectOfIncomeTax;
                UIHelper.SetSelectedInDropDown(ddlCalculation, entity.Calculation);
                string cal = entity.Calculation;

                if (cal == IncomeCalculation.FIXED_AMOUNT)
                {
                    //if (entity.IsNotRegular != null)
                    //{
                    //    UIHelper.SetSelectedInDropDown(ddlPaymentType, entity.IsNotRegular.ToString().ToLower());
                    //    if (entity.IsNotRegular.Value)
                    //    {
                    //        IncomeDeductionUIHelper.SetSelectedMonths(entity, chkPaymentMonthList);
                    //        divPaymentMonthList.Style["display"] = "";
                    //    }
                    //}
                    //else
                    //    UIHelper.SetSelectedInDropDown(ddlPaymentType, "false");
                }
                if (cal == IncomeCalculation.VARIABLE_AMOUNT)
                {
                    if (entity.IsTreatLikeOneTimeIncome != null)
                        chkIsTreatLikeOneTimeIncome.Checked = entity.IsTreatLikeOneTimeIncome.Value;
                    else
                        chkIsTreatLikeOneTimeIncome.Checked = false;

                    if (entity.IsLateIncomeType != null)
                        chkIsLateIncomeType.Checked = entity.IsLateIncomeType.Value;
                    else
                        chkIsLateIncomeType.Checked = false;

                }

                if (cal == IncomeCalculation.PERCENT_OF_INCOME)
                {
                    IncomeDeductionUIHelper.SetSelectedIncomes(entity, chkListIncomes1);
                    if (this.PageModeList == PageModeList.Insert || this.PageModeList == PageModeList.Update)
                        txtRatePercent2.Text = entity.RatePercentForAll.ToString();

                    if (entity.MinAmount != null)
                        txtMinAmount.Text = GetCurrency(entity.MinAmount);
                    if (entity.MaxAmount != null)
                        txtMaxAmount.Text = GetCurrency(entity.MaxAmount);

                    if (entity.EnableStatusFilter != null && entity.EnableStatusFilter.Value)
                    {
                        chkApplyStatusFilter.Checked = entity.EnableStatusFilter.Value;
                        //for status list
                        foreach (PIncomeStatus income in entity.PIncomeStatus)
                        {
                            ListItem item = chkStatusList.Items.FindByValue(income.Status.ToString());
                            if (item != null)
                                item.Selected = true;
                        }
                    }

                }
                else if (cal == IncomeCalculation.FESTIVAL_ALLOWANCE)
                {
                    rdbVariableAmount.Checked = entity.IsVariableAmount.Value;
                    rdbCalculatedAmount.Checked = entity.IsCalculatedAmount.Value;

                    if (entity.IsCalculatedAmount.Value)
                    {
                        IncomeDeductionUIHelper.SetSelectedIncomes(entity, chkListIncomes2);
                        if (this.PageModeList == PageModeList.Insert || this.PageModeList == PageModeList.Update)
                        {
                            if (entity.RatePercentForAll != null)
                                txtPercent.Text = entity.RatePercentForAll.ToString();
                        }
                    }
                }
                else if (cal == IncomeCalculation.DEEMED_INCOME)
                {

                    if (entity.DeemedIsVariableType != null && entity.DeemedIsVariableType.Value)
                    {
                        ddlDeemedCalculationOption.SelectedValue = "true";
                    }
                    else
                        ddlDeemedCalculationOption.SelectedValue = "false";

                    if( entity.DeemedIsYearlyIncome!= null)
                    {
                        chkDeemedIsYearIncome.Checked = entity.DeemedIsYearlyIncome.Value;
                        if (chkDeemedIsYearIncome.Checked)
                            UIHelper.SetSelectedInDropDown(ddlDeemedMonth, entity.DeemedMonth);
                    }

                    if (entity.DeemedIsAmount == null || entity.DeemedIsAmount.Value)
                    {
                        rdbDeemedAmount.Checked=true;
                        rdbDeemedPercent.Checked=false;
                    }
                    else{
                           rdbDeemedAmount.Checked=false;
                        rdbDeemedPercent.Checked=true;
                    }

                    if (rdbDeemedPercent.Checked)
                    {
                        IncomeDeductionUIHelper.SetSelectedIncomes(entity, chkDeemedIncomeList);
                        if (this.PageModeList == PageModeList.Insert || this.PageModeList == PageModeList.Update)
                            txtDeemedRate.Text = entity.RatePercentForAll.ToString();
                    }
                }
                else if (cal == IncomeCalculation.Unit_Rate)
                {
                    if (entity.IsTreatLikeOneTimeIncome != null)
                        chkIsTreatLikeOneTimeIncome.Checked = entity.IsTreatLikeOneTimeIncome.Value;
                    else
                        chkIsTreatLikeOneTimeIncome.Checked = false;

                    if (entity.UnitRateBasedOnWorkdays != null)
                        chkUnitRateBasedOnWorkdays.Checked = entity.UnitRateBasedOnWorkdays.Value;
                    else
                        chkUnitRateBasedOnWorkdays.Checked = false;

                    if (entity.UnitRateHalfdayCounting != null)
                        txtUnitRateHalfdayValue.Text = entity.UnitRateHalfdayCounting.ToString();

                    if (entity.UnitRateDeductHolidayWeeklyAndNational != null)
                        chkDeductHoliday.Checked = entity.UnitRateDeductHolidayWeeklyAndNational.Value;
                    if (entity.UnitRateDeductPaidLeaves != null)
                        chkDeductPaidLeave.Checked = entity.UnitRateDeductPaidLeaves.Value;
                    if (entity.UnitRateDeductUnpaidLeaves != null)
                        chkDeductUnpaidLeave.Checked = entity.UnitRateDeductUnpaidLeaves.Value;
                    if (entity.UnitRateDeductHalfdayHoliday != null)
                        chkDeductHalfdayHoliday.Checked = entity.UnitRateDeductHalfdayHoliday.Value;
                    if (entity.UnitRateDeductHalfdayPaidLeave != null)
                        chkDeductHalfdayLeave.Checked = entity.UnitRateDeductHalfdayPaidLeave.Value;
                    if (entity.UnitRateProportionatePreviousMonth != null)
                        chkProportionatePreviousMonth.Checked = entity.UnitRateProportionatePreviousMonth.Value;
                }
                chkIsIncomeEnabled.Checked = entity.IsEnabled.Value;
                if (entity.AddTaxFromTheBeginning != null)
                    chkAddTaxFromTheBeginning.Checked = entity.AddTaxFromTheBeginning.Value;
                


                if(entity.Calculation==IncomeCalculation.FESTIVAL_ALLOWANCE)
                {
                    PayrollPeriod period = CommonManager.GetLastPayrollPeriod();
                    if (period != null)
                    {

                        int totalEmployees = CalculationManager.GetTotalApplicableForSalary(period.PayrollPeriodId, false);

                        int applied = CalculationManager.GetTotalApplicableForFestivalIncome(period.PayrollPeriodId, false
                            , this.IncomeId);

                        if (applied > totalEmployees)
                            applied = totalEmployees;

                        Div1.InnerHtml = string.Format(Div1.InnerHtml, totalEmployees, applied);

                    }
                    
                }
            }
        }

        /// <summary>
        /// Input/Output process for PEmployeeIncome
        /// </summary>
        PEmployeeIncrement ProcessIncrement(PEmployeeIncome entity, PageMode mode)
        {
            if (mode == PageMode.Retrieve)
            {
                PEmployeeIncrement inc = new PEmployeeIncrement();
                //inc.IsIncluded = false;
                inc.EffectiveFromDate = calIncrementDate.SelectedDate.ToString();

                //check if resterospect or not
                if( payrollPeriod != null)
                {
                    CustomDate date1 = CustomDate.GetCustomDateFromString(payrollPeriod.StartDate,
                                                                          SessionManager.IsEnglish);
                    CustomDate date2 = CustomDate.GetCustomDateFromString(inc.EffectiveFromDate, IsEnglish);

                    if (payrollPeriod.StartDate != inc.EffectiveFromDate
                        && DateManager.IsSecondDateGreaterThan(date1,date2)==false)
                    {
                        inc.IsReterospect = true;
                    }
                }

                if (ddlEvents.SelectedIndex != 0)
                    inc.EventID = int.Parse(ddlEvents.SelectedValue);

                if (!string.IsNullOrEmpty(hdnIncrementId.Value))
                {
                    inc.Id = int.Parse(hdnIncrementId.Value);
                }
                return inc;
            }
            else
            {

                PEmployeeIncrement inc =
                    PayManager.GetCurrentIncrement(entity.EmployeeIncomeId);
                if (inc != null)
                {
                    chkIsIncrementMode.Checked = true;
                    calIncrementDate.SetSelectedDate(inc.EffectiveFromDate, SessionManager.CurrentCompany.IsEnglishDate);
                    hdnIncrementId.Value = inc.Id.ToString();

                    if (inc.EventID != null)
                        UIHelper.SetSelectedInDropDown(ddlEvents, inc.EventID.ToString());
                }
            }
            return null;
        }

        void Process(ref PEmployeeIncome entity)
        {
            if (entity == null)
            {
                entity = new PEmployeeIncome();
                entity.EmployeeIncomeId = this.CustomId;
                entity.EditSequence = int.Parse(hdEmployeeEditSequence.Value == "" ? "0" : hdEmployeeEditSequence.Value) + 1;
                PIncome income = null;
                Process(ref income);
                entity.PIncome = income;

                //no foreign allowance for deemed type
                if (ddlCalculation.SelectedValue == IncomeCalculation.DEEMED_INCOME)
                    entity.IsForeignAllowance = false;
                else
                    entity.IsForeignAllowance = chkIsForeignAllowance.Checked;

                entity.IsEnabled = chkIsEmployeeIncomeEnabled.Checked;
                string cal = entity.PIncome.Calculation;
                if (cal == IncomeCalculation.VARIABLE_AMOUNT || cal == IncomeCalculation.FIXED_AMOUNT)
                {
                    entity.Amount = decimal.Parse(txtAmount.Text);
                }
                //else if (cal == IncomeCalculation.PERCENTAGE_OF_SALES)
                //{
                //    entity.RatePercent = double.Parse(Util.FormatPercentForInput(txtRatePercent1.Text));
                //    if (!string.IsNullOrEmpty(txtPOSAmount.Text))
                //        entity.Amount = decimal.Parse(txtPOSAmount.Text.Trim());
                //    else
                //        entity.Amount = null;
                //}
                //else if (cal == IncomeCalculation.DAILY || cal == IncomeCalculation.HOURLY)
                //{
                //    entity.Rate = decimal.Parse(txtRate.Text);
                //}
                else if (cal == IncomeCalculation.DEEMED_INCOME)
                {
                    
                    if (rdbDeemedAmount.Checked)
                    {
                        entity.Amount = 0;// decimal.Parse(txtAmountCharged.Text.Trim());
                        entity.MarketValue = decimal.Parse(txtDeemedAmount.Text.Trim());
                    }
                    else
                    {
                        entity.RatePercent = double.Parse(txtDeemedRate.Text.Trim());
                    }
                }
                else if (cal == IncomeCalculation.PERCENT_OF_INCOME)
                {
                    //entity.RatePercent = double.Parse(Util.FormatPercentForInput(txtRatePercent2.Text));
                    //need to change for GMC percent prevision

                    entity.SkipMinMaxChecking = chkSkipMinMaxChecking.Checked;

                    entity.RatePercent = double.Parse((txtRatePercent2.Text));
                }
                else if (cal == IncomeCalculation.Unit_Rate)
                {
                    entity.RatePercent = double.Parse(txtUnitRate.Text.Trim());
                }
                else if (cal == IncomeCalculation.FESTIVAL_ALLOWANCE)
                {
                    entity.ApplyFestivalIncome = chkApplyFestivalIncome.Checked;

                    if (rdbVariableAmount.Checked)
                    {
                        entity.Amount = decimal.Parse(txtVariableAmount.Text);
                    }
                    else
                    {
                        entity.RatePercent = double.Parse(Util.FormatPercentForInput(txtPercent.Text.Trim()));
                    }
                }
            }
            else
            {
                if (entity.IsForeignAllowance != null)
                    chkIsForeignAllowance.Checked = entity.IsForeignAllowance.Value;
                chkIsForeignAllowance.Visible = true;
                chkIsEmployeeIncomeEnabled.Checked = entity.IsEnabled;
                hdEmployeeEditSequence.Value = entity.EditSequence == null ? "0": entity.EditSequence.ToString() ;
                //for the first time values are null, i.e. no amounts
                if (entity.IsValid)
                {
                    string cal = entity.PIncome.Calculation;
                    if (cal == IncomeCalculation.VARIABLE_AMOUNT || cal == IncomeCalculation.FIXED_AMOUNT)
                    {
                        txtAmount.Text = GetCurrency(entity.ActualAmount);
                    }
                    //else if (cal == IncomeCalculation.PERCENTAGE_OF_SALES)
                    //{
                    //    txtRatePercent1.Text = entity.RatePercent.ToString();
                    //    if(entity.Amount != null)
                    //        txtPOSAmount.Text = GetCurrency(entity.Amount.Value);
                    //}
                    //else if (cal == IncomeCalculation.DAILY || cal == IncomeCalculation.HOURLY)
                    //{
                    //    txtRate.Text = Util.FormatForInput(entity.Rate.Value);
                    //}
                    else if (cal == IncomeCalculation.DEEMED_INCOME)
                    {
                        if (entity.PIncome.DeemedIsAmount == null || entity.PIncome.DeemedIsAmount.Value)
                        {
                           
                            txtDeemedAmount.Text = GetCurrency(entity.DeemedAmount);
                        }
                        else
                            txtDeemedRate.Text = entity.RatePercent.ToString();
                    }
                    else if (cal == IncomeCalculation.PERCENT_OF_INCOME)
                    {
                        txtRatePercent2.Text = entity.RatePercent.ToString();
                        if (entity.SkipMinMaxChecking != null && entity.SkipMinMaxChecking.Value)
                            chkSkipMinMaxChecking.Checked = entity.SkipMinMaxChecking.Value;
                    }
                    else if (cal == IncomeCalculation.Unit_Rate)
                    {
                        txtUnitRate.Text = entity.RatePercent.ToString();

                    }
                    else if (cal == IncomeCalculation.FESTIVAL_ALLOWANCE)
                    {
                        if (entity.ApplyFestivalIncome != null)
                            chkApplyFestivalIncome.Checked = entity.ApplyFestivalIncome.Value;

                        if (entity.PIncome.IsVariableAmount.Value)
                        {
                            txtVariableAmount.Text = GetCurrency(entity.Amount);
                        }
                        else if (entity.PIncome.IsCalculatedAmount.Value && entity.RatePercent != null)
                        {
                            txtPercent.Text = entity.RatePercent.Value.ToString();
                        }
                    }
                }
            }
        }

        #endregion


        public bool IsValidTitleForSepecialCharacter()
        {
            if (BLL.BaseBiz.IsInvalidForSavingInXMLForm(txtTitle.Text.Trim()))
            {
                divWarningMsg.InnerHtml = "Text contains invalid character like &,<,>,' or \"";
                divWarningMsg.Hide = false;
                return false;
            }
            return true;
        }

        public bool IsNameNotValid()
        {
            string name = txtTitle.Text.Trim().ToLower();
            if (name == "leave encashment" || name == "retrospect income")
                return true;

            return false;
        }

        protected void btnOk_Click(object sender, EventArgs e)
        {
            //if (!IsValidTitleForSepecialCharacter())
            //    return;

            if (chkIsIncrementMode.Checked && ddlEvents.SelectedIndex == 0)
            {
                divWarningMsg.InnerHtml = "Event type selection is required.";
                divWarningMsg.Hide = false;
                return;
            }
                
            EnableDisableValidators(false);
            Page.Validate("AEIncome");

            if (IsNameNotValid())
            {
                divWarningMsg.InnerHtml = "Income with this name already exists, please change the name.";
                divWarningMsg.Hide = false;
                return;
            }

            string invalidText = Util.HasInvalidCharacterForXMLParsingAndImport(txtTitle.Text.Trim());

            if (!string.IsNullOrEmpty(invalidText))
            {
                divWarningMsg.InnerHtml = "\"" + invalidText + "\" character can not be set in the title.";
                divWarningMsg.Hide = false;
                return;
            }


            if (Page.IsValid)
            {
                int? EditSequence = 0;
                switch (this.PageModeList)
                {                    
                    case PageModeList.Insert:
                        
                        PIncome income = null;
                        Process(ref income);

                        if (income.IsDefinedType != null && income.IsDefinedType.Value && income.YType != null && income.YType == (int)IncomeDefinedTypeEnum.Level)
                        {
                            divWarningMsg.InnerHtml = "Level type can be defined in column only.";
                            divWarningMsg.Hide = false;
                            break;
                        }

                        if (income.IsDefinedType != null && income.IsDefinedType.Value && income.XType == income.YType)
                        {
                            divWarningMsg.InnerHtml = "Same type can not be defined in both row & column.";
                            divWarningMsg.Hide = false;
                            break;
                        }

                        if (PayManager.CheckIfMultipleGradeTypeDefined(income))
                        {
                            divWarningMsg.InnerHtml = Resources.Messages.MultipleIncomeAsGrade;
                            divWarningMsg.Hide = false;
                            break;
                        }
                        if (payMgr.Save(income))
                            JavascriptHelper.DisplayClientMsg(Resources.Messages.IncomeSaved, this, "closePopupFromPay();");

                        break;

                    case PageModeList.Update:

                        PIncome income1 = null;
                        Process(ref income1);
                        income1.IncomeId = this.IncomeId;

                        if (IsCalculationTypeBeingChangedWithHavingIncrement(income1.Calculation))
                        {

                            JavascriptHelper.DisplayClientMsg(Resources.Messages.IncomeTypeCannotBeChangedWithHavingIncrement, this);
                            return;
                        }
                        if (income1.IsDefinedType != null && income1.IsDefinedType.Value && income1.YType != null && income1.YType == (int)IncomeDefinedTypeEnum.Level)
                        {
                            divWarningMsg.InnerHtml = "Level type can be defined in column only.";
                            divWarningMsg.Hide = false;
                            break;
                        }

                        if (income1.IsDefinedType != null && income1.IsDefinedType.Value && income1.XType == income1.YType)
                        {
                            divWarningMsg.InnerHtml = "Same type can not be defined in both row & column.";
                            divWarningMsg.Hide = false;
                            break;
                        }

                        EditSequence = payMgr.GetIncomeById(income1.IncomeId).EditSequence;
                        EditSequence = EditSequence == null ? 0 : EditSequence;
                        if (EditSequence != int.Parse(hdEditSequence.Value == "" ? "0" : hdEditSequence.Value))
                        {
                            divWarningMsg.InnerHtml = Resources.Messages.ConcurrencyError;
                            divWarningMsg.Hide = false;
                            return;
                        }
                        if (PayManager.CheckIfMultipleGradeTypeDefined(income1))
                        {
                            divWarningMsg.InnerHtml = Resources.Messages.MultipleIncomeAsGrade;
                            divWarningMsg.Hide = false;
                            break;
                        }
                        payMgr.Update(income1);
                        if (hdEditSequence.Value == "")
                            hdEditSequence.Value = "1";
                        else
                            hdEditSequence.Value = (int.Parse(hdEditSequence.Value) + 1).ToString();
                        JavascriptHelper.DisplayClientMsg(Resources.Messages.IncomeUpdated, this, "closePopupFromPay();");


                        break;

                    case PageModeList.UpdateEmployee:

                        PEmployeeIncome empIncome = null;
                        Process(ref empIncome);
                        PEmployeeIncrement increment = null;
                        //No increment for festival type & concessional type
                        if (chkIsIncrementMode.Checked 
                            && empIncome.PIncome.Calculation != IncomeCalculation.FESTIVAL_ALLOWANCE
                            && empIncome.PIncome.Calculation != IncomeCalculation.DEEMED_INCOME)
                        //)&& empIncome.PIncome.Calculation != IncomeCalculation.PERCENTAGE_OF_SALES)
                        {
                            increment = ProcessIncrement(empIncome, PageMode.Retrieve);
                        }
                        empIncome.IsValid = true;

                        if (empIncome.PIncome.IsDefinedType != null && empIncome.PIncome.IsDefinedType.Value && empIncome.PIncome.YType != null && empIncome.PIncome.YType == (int)IncomeDefinedTypeEnum.Level)
                        {
                            divWarningMsg.InnerHtml = "Level type can be defined in column only.";
                            divWarningMsg.Hide = false;
                            break;
                        }

                        if (empIncome.PIncome.IsDefinedType != null && empIncome.PIncome.IsDefinedType.Value && empIncome.PIncome.XType == empIncome.PIncome.YType)
                        {
                            divWarningMsg.InnerHtml = "Same type can not be defined in both row & column.";
                            divWarningMsg.Hide = false;
                            break;
                        }

                        // Check if calculation type is being changed & has increment then don't allow update

                        if (IsCalculationTypeBeingChangedWithHavingIncrement(empIncome.PIncome.Calculation))
                        {

                            JavascriptHelper.DisplayClientMsg(Resources.Messages.IncomeTypeCannotBeChangedWithHavingIncrement, this);
                            return;
                        }

                        EditSequence = payMgr.GetEmployeeIncome(empIncome.EmployeeIncomeId).EditSequence;
                        EditSequence = EditSequence == null ? 0 : EditSequence;
                        if (EditSequence != int.Parse(hdEmployeeEditSequence.Value == "" ? "0" : hdEmployeeEditSequence.Value))
                        {
                            divWarningMsg.InnerHtml = Resources.Messages.ConcurrencyError;
                            divWarningMsg.Hide = false;
                            return;
                        }
                        hdEmployeeEditSequence.Value = (int.Parse(hdEmployeeEditSequence.Value) + 1).ToString();
                        if (PayManager.CheckIfMultipleGradeTypeDefined(empIncome))
                        {
                            divWarningMsg.InnerHtml = Resources.Messages.MultipleIncomeAsGrade;
                            divWarningMsg.Hide = false;
                            break;
                        }
                        
                        ResponseStatus status = payMgr.Update(empIncome, increment, payrollPeriod,
                            CommonManager.GetLastPayrollPeriod());
                        if (status.IsSuccessType == false)
                        {
                            divWarningMsg.InnerHtml = status.ErrorMessage;
                            divWarningMsg.Hide = false;
                        }
                        else
                            JavascriptHelper.DisplayClientMsg(Resources.Messages.IncomeUpdatedForEmployee, this, "closePopupFromPay();");

                        break;
                }
                
            }
            EnableDisableValidators(true);
        }


        #region "Enabling/Disabling section"

        /// <summary>
        /// Checks if the calculation type is being changed with having increment or not
        /// </summary>     
        private bool IsCalculationTypeBeingChangedWithHavingIncrement(string newCalcualation)
        {
            PayManager mgr =  new PayManager();
            PEmployeeIncome employeeIncome = mgr.GetEmployeeIncome(this.CustomId);

            if (this.CustomId == 0 || employeeIncome == null)
                return false;

            //PayManager mgr = new PayManager();
            PIncome dbIncome = mgr.GetIncomeById(employeeIncome.PIncome.IncomeId);

            if (dbIncome.Calculation != newCalcualation)
            {
                //if has increment then return true

                return PayManager.HasIncomeIncrement(dbIncome.IncomeId);

            }
            return false;
        }

        void EnableDisableValidators(bool isEnable)
        {
            if (!isEnable)
            {
                string cal = ddlCalculation.SelectedValue;
                DisableChildValidationControls(pnlParent);
                //as inside pnlParent
                valReqdCalculation.Enabled = true;

                //if (chkApplyOnceFor.Checked == false)
                if( cal!=IncomeCalculation.FESTIVAL_ALLOWANCE)
                {
                    DisableChildValidationControls(tblStatuesContainer);
                }
                if (cal == IncomeCalculation.VARIABLE_AMOUNT || cal == IncomeCalculation.FIXED_AMOUNT)
                {
                    RequiredFieldValidator2.Enabled = true;
                    if (!chkIsGrade.Checked)
                        valCompareVariableFixedAmount.Enabled = true;
                    else
                        valCompareVariableFixedAmount.Enabled = false;
                }
                //else if (cal == IncomeCalculation.PERCENTAGE_OF_SALES)
                //{
                //    RequiredFieldValidator6.Enabled = true;
                //    CompareValidator141.Enabled = true;
                //    valCompPOSAmount.Enabled = true;
                //}
                //else if (cal == IncomeCalculation.DAILY || cal == IncomeCalculation.HOURLY)
                //{
                //    RequiredFieldValidator7.Enabled = true;
                //    CompareValidator1.Enabled = true;
                //}
                else if (cal == IncomeCalculation.DEEMED_INCOME)
                {
                    EnabledChildValidationControls(pnlConcessionalFacility);

                    if (chkDeemedIsYearIncome.Checked)
                        reqdValueDeemedMonth.Enabled = true;
                    else
                        reqdValueDeemedMonth.Enabled = false;

                    if (rdbDeemedAmount.Checked)
                        DisableChildValidationControls(tableDeemedPercent);
                    else
                        DisableChildValidationControls(tableDeemedAmount);
                }
                else if (cal == IncomeCalculation.PERCENT_OF_INCOME)
                {
                    //if (this.CustomId == 0 || this.PageModeList==PageModeList.Update)
                    //{
                    RequiredFieldValidator8.Enabled = false;
                    CompareValidator2.Enabled = false;
                    //}
                    //else
                    //{
                    //    RequiredFieldValidator8.Enabled = true;
                    //    CompareValidator2.Enabled = true;
                    //}
                }
                else if (cal == IncomeCalculation.FESTIVAL_ALLOWANCE)
                {
                    if (rdbVariableAmount.Checked)
                    {
                        RequiredFieldValidator9.Enabled = true;
                        CompareValidator3.Enabled = true;
                    }
                    else
                    {
                        RequiredFieldValidator10.Enabled = true;
                        CompareValidator4.Enabled = true;
                    }
                }
            }
            else
            {
                EnabledChildValidationControls(pnlParent);
                DisableChildValidationControls(tblStatuesContainer);
                EnabledChildValidationControls(pnlConcessionalFacility);
            }
        }
        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (chkBasicIncome.Checked || chkIsGrade.Checked)
                rowIsChildrenAllowance.Visible = false;
            else
                rowIsChildrenAllowance.Visible = true;

            //for income editing mode, remove display:none for more options & apply month of
            if (this.PageModeList == BLL.PageModeList.Update || this.PageModeList==BLL.PageModeList.Insert)
            {
                tblStatuesContainer.Style.Remove("display");
                pnlOptions.Style.Remove("display");
                imageOptions.ImageUrl = "../images/collapse.jpg";
            }


            if (ddlCalculation.SelectedValue == IncomeCalculation.PERCENT_OF_INCOME)
            {
                rowStatusFilter1.Style["display"] = "";

                if (chkApplyStatusFilter.Checked)
                    divStatus.Style["display"] = "";
            }

            //if (CommonManager.CompanySetting.IsD2 && ddlCalculation.SelectedValue==IncomeCalculation.VARIABLE_AMOUNT)
            //{
            //    d2VariableLink.Style["display"] = ""; 
            //}

            chkPF.Visible = SessionManager.CurrentCompany.HasPFRFFund;

            tblStatuesContainer.Style["display"] = "none";
            DisableChildControls(tblStatuesContainer);
            tblApplyOnce.Style["display"] = "none";
            btnCheckEligibility.Visible = false;

            calEligibilityDate.Enabled = chkEligibilityDate.Checked;

            ddlDeemedMonth.Enabled = chkDeemedIsYearIncome.Checked;
            if (ddlCalculation.SelectedValue != IncomeCalculation.FIXED_AMOUNT 
                && ddlCalculation.SelectedValue != IncomeCalculation.FESTIVAL_ALLOWANCE
                && ddlCalculation.SelectedValue != IncomeCalculation.PERCENT_OF_INCOME
                && ddlCalculation.SelectedValue != IncomeCalculation.Unit_Rate)
            {
                rowPF.Style.Add("display", "none");
               
            }
            if (ddlCalculation.SelectedValue != IncomeCalculation.FIXED_AMOUNT)
            {
              
                rowIsGrade.Style.Add("display", "none");
            }

            if (chkIsIncrementMode.Enabled)
                calIncrementDate.Enabled = chkIsIncrementMode.Checked;
            else
            {
                calIncrementDate.Enabled = false;
            }

            if (rdbCalculatedAmount.Checked)
                divFestival.Attributes.Remove("disabled");
            else
                divFestival.Attributes.Add("disabled", "disabled");

            if (rdbDeemedAmount.Checked)
            {
                //tableDeemedPercent.Attributes.Add("disabled", "disabled");
                DisableChildControls(tableDeemedPercent);
            }
            else
                DisableChildControls(tableDeemedAmount);


            pnlUnitRate.Style.Add("display", "none");
            pnlFixedVariable.Style.Add("display", "none");
            pnlPercentOfSales.Style.Add("display", "none");
            //pnlHourlyDailyRate.Style.Add("display", "none");
            pnlConcessionalFacility.Style.Add("display", "none");
            pnlPercentOfIncome.Style.Add("display", "none");
            pnlFestivalAllowance.Style.Add("display", "none");
            fieldSetDefinedType.Style.Add("display", "none");
            rowUnitRateWorkdays.Style["display"] = "none";
            rowLateType.Style["display"] = "none";

            string value = ddlCalculation.SelectedValue;
            if (value == IncomeCalculation.VARIABLE_AMOUNT || value==IncomeCalculation.Unit_Rate)
            {
                rowchkIsTreatLikeOneTimeIncome.Style["display"] = "";
                valCompareVariableFixedAmount.ValueToCompare = "0";
                fieldSetDefinedType.Style["display"] = "";
                if (value == IncomeCalculation.Unit_Rate)
                    rowUnitRateWorkdays.Style["display"] = "";
                else
                    rowLateType.Style["display"] = "";
            }
            else
            {
               
                rowchkIsTreatLikeOneTimeIncome.Style["display"] = "none";
            }

            if (value == IncomeCalculation.VARIABLE_AMOUNT || value == IncomeCalculation.FIXED_AMOUNT)
            {
                if (chkIsGrade.Checked)
                {
                    //txtAmount.Enabled=false;
                    //DisableChildControls(rowIncrement);
                    //rowIncrement.Style.Add("display", "none");
                    //if grade is used in the salary then don't allow to disable it
                    if (PayManager.HasIncomeIncludedInCalculation(empIncome != null ? empIncome.IncomeId : this.IncomeId, false))
                        chkIsGrade.Enabled = false;
                }
                pnlFixedVariable.Style.Remove("display");
                if (value == IncomeCalculation.VARIABLE_AMOUNT)
                {
                    rowIncrement.Style.Add("display", "none");
                }
                else
                    fieldSetDefinedType.Style["display"] = "";

                //if (value == IncomeCalculation.FIXED_AMOUNT && chkIsGrade.Checked == false && chkBasicIncome.Checked == false)
                //    rowPayment.Style["display"] = "";
            }
            //else if (value == IncomeCalculation.PERCENTAGE_OF_SALES)
            //{
            //    pnlPercentOfSales.Style.Remove("display");
            //    rowIncrement.Style.Add("display", "none");
            //}
            //else if (value == IncomeCalculation.DAILY || value == IncomeCalculation.HOURLY)
            //    pnlHourlyDailyRate.Style.Remove("display");
            else if (value == IncomeCalculation.DEEMED_INCOME)
            {
                pnlConcessionalFacility.Style.Remove("display");
                rowIncrement.Style.Add("display", "none");
                chkCountForLeaveEncasement.Enabled = false;
                chkIsTaxable.Enabled = false;
                chkIsForeignAllowance.Enabled = false;

                if (ddlDeemedCalculationOption.SelectedValue == "true")
                    rowDeemedPercent.Style["display"] = "none";
            }
            else if (value == IncomeCalculation.PERCENT_OF_INCOME)
            {
                //rowPayment.Style["display"] = "";
                fieldSetDefinedType.Style["display"] = "";
                pnlPercentOfIncome.Style.Remove("display");
                //if (this.CustomId == 0)
                //    rowRateIncomePercent.Style["display"] = "none";
                //else
                //    rowRateIncomePercent.Style.Remove("display");
            }
            else if (value == IncomeCalculation.FESTIVAL_ALLOWANCE)
            {
                fieldSetDefinedType.Style["display"] = "";
                rowIncrement.Style.Add("display", "none");
                pnlFestivalAllowance.Style.Remove("display");
                //don't remove so as to set expand/collapse image
                //for income editing mode, remove display:none for more options & apply month of
                if (this.PageModeList == BLL.PageModeList.Update || this.PageModeList == BLL.PageModeList.Insert)
                {
                    tblStatuesContainer.Style.Remove("display");
                    imageApplyMonth.ImageUrl = "../images/collapse.jpg";
                }
                //tblStatuesContainer.Style.Remove("display");
                tblApplyOnce.Style.Remove("display");
                btnCheckEligibility.Visible = true;
                EnabledChildControls(tblStatuesContainer);

            }
            else if (value == IncomeCalculation.Unit_Rate)
            {
                rowIncrement.Style.Add("display", "none");
                pnlUnitRate.Style["display"] = "";
                pnlUnitRate.Visible = true;
            }
           
            //if not set default
            if (value != IncomeCalculation.FESTIVAL_ALLOWANCE)
            {
                rdbVariableAmount.Checked = true;
                txtVariableAmount.Attributes.Remove("disabled");
                DisableChildControls(divFestival);
                DisableChildControls(tblApplyOnce);
                
            }
            else
            {
                if (rdbVariableAmount.Checked)
                {
                    txtVariableAmount.Attributes.Remove("disabled");
                    DisableChildControls(divFestival);
                }
                else
                {
                    txtVariableAmount.Attributes.Add("disabled", "disabled");
                    EnabledChildControls(divFestival);
                }
            }

            // hide Defined type
            if (ddlCalculation.SelectedValue == IncomeCalculation.FIXED_AMOUNT && (chkBasicIncome.Checked || chkIsGrade.Checked))
            {
                fieldSetDefinedType.Style["display"] = "none";
            }

            //if (ddlCountWorkdaysFrom.SelectedValue == FestivalAllowanceCountWorkdaysFrom.SpecificDate)
            //    calWorkdaysFromDate.Enabled = true;
            //else
            //    calWorkdaysFromDate.Enabled = false;

            //process to display edit/increment mode
            if (chkIsIncrementMode.Checked)
            {
                lblMode.Text = Resources.Messages.IncomeModeIncrement;
            }
            else
                lblMode.Text = Resources.Messages.IncomeModeEdit;

            EnableDisableUsingEligibilityDate();

            // For Level assigned Basic salary for Employee Not editable
            if (this.PageModeList == BLL.PageModeList.UpdateEmployee)
            {
                // comment this code for now as for quest, basic salary need to orveride
                if (CommonManager.CompanySetting.WhichCompany != WhichCompany.Quest
                    && CommonManager.CompanySetting.WhichCompany != WhichCompany.Triveni)
                {
                    PEmployeeIncome employeeIncome = new PayManager().GetEmployeeIncome(this.CustomId);
                    if (employeeIncome != null && employeeIncome.LevelId != null)
                    {
                        divWarningMsg.InnerHtml =
                            "Basic salary not editable for Level/Grade assigned employee.";
                        divWarningMsg.Hide = false;

                        btnOk.Visible = false;
                        return;
                    }

                }

                // in emplooyee update mode disable Defined type setting
                DisableChildControls(fieldSetDefinedType);

            }

            if (ddlDefinedType.SelectedValue == "true")
            {
                pnlOptions1.Style["display"] = "";
                ddlXType.Enabled = true;
                ddlYType.Enabled = true;
            }
            else
            {
                ddlXType.Enabled = false;
                ddlYType.Enabled = false;
            }

            if (CommonManager.CompanySetting.HasLevelGradeSalary == false
                && empIncome != null
                && this.PageModeList == BLL.PageModeList.UpdateEmployee
                  && CalculationManager.IsCalculatedSavedForEmployee(empIncome.EmployeeId) == false
                  && PayManager.IsFixedIncome(empIncome.IncomeId))
            {
                rowIncrement.Style["display"] = "none";
            }
            else if (CommonManager.CompanySetting.HasLevelGradeSalary == false
                && empIncome != null
                && this.PageModeList == BLL.PageModeList.UpdateEmployee
                  && CalculationManager.IsCalculatedSavedForEmployee(empIncome.EmployeeId) == true
                  && CalculationManager.IsEmployeeIncomeSavedInSalary(empIncome.IncomeId,empIncome.EmployeeId)==false
                  && PayManager.IsFixedIncome(empIncome.IncomeId)
                // in case of Oxfam do not force or show increment mode for first time, as amount need to be set and 
                // later for some month we can set 0 so in that case multiple increment may be needed so hide this
                && CommonManager.CompanySetting.WhichCompany != WhichCompany.Oxfam)
            {
                rowIncrement.Style["display"] = "block";
                rowIncrement.Visible = true;

                chkIsIncrementMode.Checked = true;
                chkIsIncrementMode.Style["display"] = "none";

                calIncrementDate.Enabled = true;
            }
        }

        public void EnableDisableUsingEligibilityDate()
        {
            if (chkEligibilityDate.Checked && chkEligibilityDate.Enabled)
            {
                calEligibilityDate.Enabled = true;
                txtMinimumWorkDays.Enabled = true;
                txtProportionateWorkDays.Enabled = true;
                ddlCountWorkdaysFrom.Enabled = true;
                calWorkdaysFromDate.Enabled = true;

                if (ddlCountWorkdaysFrom.SelectedValue == FestivalAllowanceCountWorkdaysFrom.SpecificDate)
                    calWorkdaysFromDate.Enabled = true;
                else
                    calWorkdaysFromDate.Enabled = false;
            }
            else
            {
                calEligibilityDate.Enabled = false;
                txtMinimumWorkDays.Enabled = false;
                txtProportionateWorkDays.Enabled = false;
                ddlCountWorkdaysFrom.Enabled = false;
                calWorkdaysFromDate.Enabled = false;
            }


        }

        #region IDisableAfterCalculation Members

        public void DisableControlsAfterSalary()
        {
            bool isIncomeIncludedInCalculation = false;
            bool isEmployeeIncomeIncludedInCalculation = false;

            if (this.CustomId != 0 || this.IncomeId != 0)
            {
                empIncome = payMgr.GetEmployeeIncome(this.CustomId);

                int employeId = empIncome != null ? empIncome.EmployeeId : 0;
                int incomeId = empIncome != null ? empIncome.IncomeId : this.IncomeId;
                bool isOneTimeIncome = payMgr.GetIncomeById(incomeId).Calculation == IncomeCalculation.FESTIVAL_ALLOWANCE;




                isIncomeIncludedInCalculation =
                    PayManager.HasIncomeIncludedInCalculation(empIncome != null ? empIncome.IncomeId : this.IncomeId, false);

                //For all Incomes including festival incomes
                if ((empIncome != null || this.IncomeId != 0) && isIncomeIncludedInCalculation)
                {
                    if (ddlCalculation.SelectedValue != IncomeCalculation.VARIABLE_AMOUNT)
                        this.ddlCalculation.Enabled = false;

                    //chkApplyOnceFor.Enabled = false;
                    chkIsTaxable.Enabled = false;
                    calEligibilityDate.Enabled = false;
                    DisableChildControls(tblStatuesContainer);
                    //chkListIncomes1.Enabled = false;
                    //chkListIncomes2.Enabled = false;
                    //chkListIncomes2.Enabled = false;
                    //UIHelper.DisableCheckboxList(chkListIncomes2);
                    //rdbVariableAmount.Enabled = false;
                    //rdbCalculatedAmount.Enabled = false;

                    if (ddlCalculation.SelectedValue == IncomeCalculation.FESTIVAL_ALLOWANCE)
                        //|| ddlCalculation.SelectedValue == IncomeCalculation.PERCENTAGE_OF_SALES)
                        rowIncrement.Visible = false;

                }


                //For One time income only,if not included in the current Financial year
                isIncomeIncludedInCalculation =
                   PayManager.HasIncomeIncludedInCalculation(empIncome != null ? empIncome.IncomeId : this.IncomeId, true);
                PIncome income = empIncome != null ? empIncome.PIncome : payMgr.GetIncomeById(this.IncomeId);

                if (isIncomeIncludedInCalculation
                    && income.Calculation == IncomeCalculation.FESTIVAL_ALLOWANCE)
                {
                    //chkApplyOnceFor.Enabled = true;
                    //if (chkApplyOnceFor.Checked)
                    {
                        chkIsTaxable.Enabled = true;
                        //calEligibilityDate.Enabled = true;
                        //EnabledChildControls(tblStatuesContainer);
                        DisableChildControls(tblStatuesContainer);
                    }
                }
                else
                {
                    EnabledChildControls(tblStatuesContainer);
                }


                //for employee enable/disable controls
                if (this.PageModeList == PageModeList.UpdateEmployee)
                {
                    //If Income calculated for the Employee & not increment then disable amount
                    PEmployeeIncrement currentIncrement = PayManager.GetCurrentIncrement(empIncome.EmployeeIncomeId);
                    isEmployeeIncomeIncludedInCalculation =
                        EmployeeManager.HasEmployeeAndIncomeIncludedInCalculation(empIncome.IncomeId,
                                                                                  empIncome.EmployeeId);

                    if (currentIncrement == null && isEmployeeIncomeIncludedInCalculation
                        // &&(ddlCalculation.SelectedValue != IncomeCalculation.PERCENTAGE_OF_SALES
                         && ddlCalculation.SelectedValue != IncomeCalculation.FESTIVAL_ALLOWANCE
                        && ddlCalculation.SelectedValue != IncomeCalculation.VARIABLE_AMOUNT)
                    {
                        //make readonly as can be un-readonly by de-selecting the increment
                        UIHelper.MakeReadonly(txtAmount, txtRatePercent1, txtDeemedAmount,
                                              txtRatePercent2, txtVariableAmount, txtPercent);

                    }

                    else if (isEmployeeIncomeIncludedInCalculation == false)
                    {
                        switch (ddlCalculation.SelectedValue)
                        {
                            case IncomeCalculation.PERCENT_OF_INCOME:
                                UIHelper.RemoveReadonly(txtRatePercent2);
                                break;
                            //  case IncomeCalculation.VARIABLE_AMOUNT:
                            case IncomeCalculation.FIXED_AMOUNT:
                                UIHelper.RemoveReadonly(txtAmount);
                                break;
                            case IncomeCalculation.DEEMED_INCOME:
                                if (rdbDeemedAmount.Checked)
                                    UIHelper.RemoveReadonly(txtDeemedAmount);
                                else
                                    UIHelper.RemoveReadonly(txtDeemedRate);
                                break;
                            case IncomeCalculation.FESTIVAL_ALLOWANCE:
                                if (rdbVariableAmount.Checked)
                                    UIHelper.RemoveReadonly(txtVariableAmount);
                                else
                                    UIHelper.RemoveReadonly(txtPercent);
                                break;

                        }

                    }
                }

                switch (isOneTimeIncome)
                {
                    case false:


                        break;


                    case true:

                        //if employee edit mode & the tax from beginning already used case then disable checkbox
                        if (CalculationManager.IsIncomeTaxAddedFromBeginningCase(employeId, incomeId))
                            chkAddTaxFromTheBeginning.Enabled = false;

                        break;
                }


                //Call for basic salary to disable
                if (isBasicSalary)
                    BasicSalaryDisplay();

                //Call to register script
                RegisterScript(isEmployeeIncomeIncludedInCalculation);

                ///if the income for employee IsValid = false, then disable/hide increment row for all incomes,
                /// as without normal amount/rate increment can't be set otherwise null value will go to PIncomeIncrement table
                if (this.PageModeList == PageModeList.UpdateEmployee && empIncome != null)
                {
                    if (CommonManager.CompanySetting.HasLevelGradeSalary == false
                        && empIncome != null
                        && this.PageModeList == BLL.PageModeList.UpdateEmployee
                     && CalculationManager.IsCalculatedSavedForEmployee(empIncome.EmployeeId) == true
                     && CalculationManager.IsEmployeeIncomeSavedInSalary(empIncome.IncomeId, empIncome.EmployeeId) == false
                     && PayManager.IsFixedIncome(empIncome.IncomeId) )
                       // && CommonManager.CompanySetting.WhichCompany != WhichCompany.Oxfam)
                    {
                        rowIncrement.Style["display"] = "block";
                        rowIncrement.Visible = true;
                    }
                    else if (empIncome.IsValid == false)
                        rowIncrement.Visible = false;

                }
            }

           

            EnableDisableUsingEligibilityDate();
            if (chkIsGrade.Checked)
            {
                //get current grade amount for Employee
                if (empIncome != null)
                {
                    decimal? amount = PositionGradeStepAmountManager.GetPositionGradeAmount(
                        empIncome.PositionId, empIncome.GradeId, empIncome.StepId);
                    if (amount == null)
                        txtAmount.Text = "0";
                    else
                        txtAmount.Text = GetCurrency(amount.ToString());
                }

                if (txtAmount.Text == "")
                    txtAmount.Text = "0";
                txtAmount.ToolTip = "Amount can't be changed for Grade type.";
                //txtAmount.Enabled = false;
            }
        }

       

        private  void RegisterScript(bool isIncluded)
        {

            string code = string.Format("var isIncluded={0};", (isIncluded ? "true" : "false"));

            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "Script123", code, true);
            
            
        }

        #endregion

        #endregion
    }
}
