<%@ Page MaintainScrollPositionOnPostback="true" Title="Allowance pay" Language="C#"
    MasterPageFile="~/Master/Details.Master" AutoEventWireup="true" CodeBehind="HPLAllowance.aspx.cs"
    Inherits="Web.CP.HPLAllowance" %>

<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
   
   
    <script type="text/javascript">
        function refreshWindow() {



            __doPostBack('<%= ddlPayrollPeriods.ClientID %>', 0);


        }

        function isPayrollSelected(btn) {


            if ($('#' + btn.id).prop('disabled'))
                return false;

            var payrollPeriodId = document.getElementById('<%= ddlPayrollPeriods.ClientID %>').value;

            if (payrollPeriodId == -1)
                return;

            var ret = shiftPopup("payrollPeriod=" + payrollPeriodId);
            //            if (typeof (ret) != 'undefined') {
            //                if (ret == 'Reload') {
            //                    //refreshIncomeList(0);


            //                }
            //            }

            return false;
        }



        function selectDeselectAll(chk) {
            $('input[type=checkbox]').each(
                    function (index) {
                        if ($(this).prop('disabled') == false && this.id.indexOf('chkSelect') > 0)
                            this.checked = chk.checked;
                    }
                );
        }
    </script>
  
    <style>
        .excelLikeTable
        {
            /*border: AppWorkspace solid 1px;*/
            font-size: 0.8em;
            text-align: center;
            border-collapse: collapse !important;
        }
        .excelLikeTable th
        {
            font-size: 11px;
        }
        .excelLikeTable tr
        {
        }
        .excelLikeTable tr.header
        {
            font-weight: bold;
            background-color: #ffffef;
        }
        .excelLikeTable td
        {
            border: 1px solid AppWorkspace;
            margin: 0 !important;
            padding: 0px !important;
            height: 19px;
        }
        .excelLikeTable tr td input
        {
            padding: 0;
            margin: 0;
            border: 0px;
            padding-left: 2px;
            padding-right: 2px;
            text-align: right;
            font-size: 11px;
            width: 60px;
            height: 19px !important;
            margin: 0px !important;
            vertical-align: middle !important;
        }
        .inputFocus
        {
            background-color: lightgreen;
        }
    </style>
    <script language="javascript" type="text/javascript">


        $(document).ready(function () {

            setMovementToGrid('#<%= gvwOvertimes.ClientID %>');


            //$(".tiptip").tipTip();
        });
        
        
       
    </script>
    <style type="text/css">
        .empCell
        {
            width: 140px !important;
        }
        .empCell input
        {
            width: 140px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
    <div class="contentArea">
        <h3>
            Allowance</h3>
        <div class="attribute">
            <table>
                <tr>
                    <td>
                        <strong>Payroll Period</strong>
                        <asp:DropDownList AutoPostBack="true" ID="ddlPayrollPeriods" DataTextField="Name"
                            DataValueField="PayrollPeriodId" AppendDataBoundItems="true" Width="150px" runat="server"
                            OnSelectedIndexChanged="ddlPayrollPeriods_SelectedIndexChanged">
                            <asp:ListItem Value="-1" Text=""></asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator InitialValue="-1" Display="None" ControlToValidate="ddlPayrollPeriods"
                            ValidationGroup="Overtime" ID="RequiredFieldValidator2" runat="server" ErrorMessage="Payroll period is required."></asp:RequiredFieldValidator>
                    </td>
                    <td style='padding-left: 10px'>
                       
                        <asp:LinkButton ID="btnExport" runat="server" Text="Excel import" OnClientClick="return isPayrollSelected(this)"
                            CssClass=" excel marginRight tiptip" Style="float: left;" />
                    </td>
                </tr>
            </table>
        </div>
        <uc2:MsgCtl ID="divMsgCtl" EnableViewState="false" Hide="true" runat="server" />
        <uc2:WarningCtl ID="divWarningMsg" EnableViewState="false" Hide="true" runat="server" />
        <div class="gridBlock" style='clear:both'>

        <cc2:EmptyDisplayGridView Style='clear: both' GridLines="None" PagerStyle-CssClass="defaultPagingBar"
            CssClass="excelLikeTable" ShowHeaderWhenEmpty="True" ID="gvwOvertimes" runat="server"
            DataKeyNames="EIN" AutoGenerateColumns="False" ShowFooterWhenEmpty="False" AllowPaging="True"
            PageSize="60" OnPageIndexChanging="gvwOvertimes_PageIndexChanging">
            <Columns>
                <asp:TemplateField HeaderText="EIN" HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%# "&nbsp;" + Eval("EIN")%>' Width="30px" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Employee" HeaderStyle-CssClass="empCell" ItemStyle-CssClass="empCell"
                    ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <asp:Label runat="server" Text='<%# "&nbsp;" + Eval("Employee")%>' Width="140px" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Days at Palati">
                    <ItemTemplate>
                        <asp:TextBox Enabled='<%# IsEnable(Eval("EIN")) %>' ID="Days_At_Palati" runat="server"
                            data-col='0' data-row='<%# Container.DataItemIndex %>' Text='<%# Eval("Days_At_Palati")%>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <%-- <asp:TemplateField HeaderText="Days At Kirne">
                    <ItemTemplate>
                        <asp:TextBox  Enabled='<%# IsEnable(Eval("EIN")) %>'  ID="Days_At_Kirne" data-col='1' data-row='<%# Container.DataItemIndex %>'
                            runat="server" Text='<%# Eval("Days_At_Kirne")%>' />
                    </ItemTemplate>
                </asp:TemplateField>--%>
                <asp:TemplateField HeaderText="On Call">
                    <ItemTemplate>
                        <asp:TextBox Enabled='<%# IsEnable(Eval("EIN")) %>' ID="On_Call" data-col='1' data-row='<%# Container.DataItemIndex %>'
                            runat="server" Text='<%# Eval("On_Call")%>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <%--shift region--%>
                <asp:TemplateField HeaderText="Shift Afternoon">
                    <ItemTemplate>
                        <asp:TextBox Enabled='<%# IsEnable(Eval("EIN")) %>' ID="Shift_Afternoon" data-col='2'
                            data-row='<%# Container.DataItemIndex %>' runat="server" Text='<%# Eval("Shift_Afternoon")%>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Shift Night">
                    <ItemTemplate>
                        <asp:TextBox Enabled='<%# IsEnable(Eval("EIN")) %>' ID="Shift_Night" data-col='3'
                            data-row='<%# Container.DataItemIndex %>' runat="server" Text='<%# Eval("Shift_Night")%>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Split Shift Morning">
                    <ItemTemplate>
                        <asp:TextBox Enabled='<%# IsEnable(Eval("EIN")) %>' ID="Split_Shift_Morning" data-col='4'
                            data-row='<%# Container.DataItemIndex %>' runat="server" Text='<%# Eval("Split_Shift_Morning")%>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Split Shift Evening">
                    <ItemTemplate>
                        <asp:TextBox Enabled='<%# IsEnable(Eval("EIN")) %>' ID="Split_Shift_Evening" data-col='5'
                            data-row='<%# Container.DataItemIndex %>' runat="server" Text='<%# Eval("Split_Shift_Evening")%>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <%--OT--%>
                <asp:TemplateField HeaderText="OT 150">
                    <ItemTemplate>
                        <asp:TextBox Enabled='<%# IsEnable(Eval("EIN")) %>' ID="OT_150" data-col='6' data-row='<%# Container.DataItemIndex %>'
                            runat="server" Text='<%# Eval("OT_150")%>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="OT 200">
                    <ItemTemplate>
                        <asp:TextBox Enabled='<%# IsEnable(Eval("EIN")) %>' ID="OT_200" data-col='7' data-row='<%# Container.DataItemIndex %>'
                            runat="server" Text='<%# Eval("OT_200")%>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="OT 300">
                    <ItemTemplate>
                        <asp:TextBox Enabled='<%# IsEnable(Eval("EIN")) %>' ID="OT_300" data-col='8' data-row='<%# Container.DataItemIndex %>'
                            runat="server" Text='<%# Eval("OT_300")%>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Public Holiday">
                    <ItemTemplate>
                        <asp:TextBox Enabled='<%# IsEnable(Eval("EIN")) %>' ID="Public_Holiday" data-col='9'
                            data-row='<%# Container.DataItemIndex %>' runat="server" Text='<%# Eval("Public_Holiday")%>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <%--leave--%>
                <%-- <asp:TemplateField HeaderText="Leave Add">
                    <ItemTemplate>
                        <asp:TextBox ID="TextBox1" data-col='11' data-row='<%# Container.DataItemIndex %>'
                            runat="server" Text='<%# Eval("Leave_Add")%>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Home Leave">
                    <ItemTemplate>
                        <asp:TextBox ID="TextBox1" data-col='12' data-row='<%# Container.DataItemIndex %>'
                            runat="server" Text='<%# Eval("Home_Leave")%>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Sick Leave">
                    <ItemTemplate>
                        <asp:TextBox ID="TextBox1" data-col='13' data-row='<%# Container.DataItemIndex %>'
                            runat="server" Text='<%# Eval("Sick_Leave")%>' />
                    </ItemTemplate>
                </asp:TemplateField>--%>
                <asp:TemplateField HeaderText="Other Leave">
                    <ItemTemplate>
                        <asp:TextBox Enabled='<%# IsEnable(Eval("EIN")) %>' ID="Other_Leave" data-col='10'
                            data-row='<%# Container.DataItemIndex %>' runat="server" Text='<%# Eval("Other_Leave")%>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <%--allowance--%>
                <asp:TemplateField HeaderText="Snacks Allowance">
                    <ItemTemplate>
                        <asp:TextBox Enabled='<%# IsEnable(Eval("EIN")) %>' ID="Snacks_Allowance" data-col='11'
                            data-row='<%# Container.DataItemIndex %>' runat="server" Text='<%# Eval("Snacks_Allowance")%>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Special Comp. Allowance">
                    <ItemTemplate>
                        <asp:TextBox Enabled='<%# IsEnable(Eval("EIN")) %>' ID="Special_Compensation_Allowance"
                            data-col='12' data-row='<%# Container.DataItemIndex %>' runat="server" Text='<%# Eval("Special_Compensation_Allowance")%>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Meal Allowance">
                    <ItemTemplate>
                        <asp:TextBox Enabled='<%# IsEnable(Eval("EIN")) %>' ID="Meal_Allowance" data-col='13'
                            data-row='<%# Container.DataItemIndex %>' runat="server" Text='<%# Eval("Meal_Allowance")%>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="TADA">
                    <ItemTemplate>
                        <asp:TextBox Enabled='<%# IsEnable(Eval("EIN")) %>' ID="TADA" data-col='14' data-row='<%# Container.DataItemIndex %>'
                            runat="server" Text='<%# Eval("TADA")%>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Vehicle Allowance">
                    <ItemTemplate>
                        <asp:TextBox Enabled='<%# IsEnable(Eval("EIN")) %>' ID="Vehicle_Allowance" data-col='15'
                            data-row='<%# Container.DataItemIndex %>' runat="server" Text='<%# Eval("Vehicle_Allowance")%>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <%-- <asp:TemplateField HeaderText="Select" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="80px"
                    HeaderStyle-HorizontalAlign="Center">
                    <HeaderTemplate>
                        <asp:CheckBox ID="chkSelect" onclick="selectDeselectAll(this)" runat="server" />
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:CheckBox ID="chkSelect" runat="server" />
                    </ItemTemplate>
                </asp:TemplateField>--%>
            </Columns>
            <PagerSettings Mode="NumericFirstLast" NextPageText="Next �" PreviousPageText="� Prev"
                FirstPageText="First" LastPageText="Last" />
            <RowStyle CssClass="odd" />
            <AlternatingRowStyle CssClass="even" />
            <PagerStyle CssClass="defaultPagingBar" />
            <SelectedRowStyle CssClass="selected" />
            <EmptyDataTemplate>
                <strong style='padding-top: 6px; padding-left: 3px'>No allowances. </strong>
            </EmptyDataTemplate>
        </cc2:EmptyDisplayGridView>
        </div>

        <asp:Panel ID="pnlButtons" CssClass="clear buttonsDiv" Style='text-align: right'
            runat="server">
            <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="save" OnClick="btnSave_Click1" />
            <%-- <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="delete" onclick="btnDelete_Click" 
            />--%>
        </asp:Panel>
    </div>
</asp:Content>
