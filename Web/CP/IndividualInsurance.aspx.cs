﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using Utils.Helper;
using DAL;
using BLL.Base;
using Utils.Web;
using Utils.Calendar;

namespace Web.CP
{
    public partial class IndividualInsurance :BasePage
    {
        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.Payroll;
            }
        }

        BranchManager branchMgr = new BranchManager();
        EmployeeManager empMgr = new EmployeeManager();
        InsuranceManager insMgr = new InsuranceManager();
        CommonManager cmpMgr = new CommonManager();

        int yearId = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["YearId"]))
                yearId = int.Parse(Request.QueryString["YearId"]);
            else
                yearId = SessionManager.CurrentCompanyFinancialDate.FinancialDateId;

            FinancialDate year = CommonManager.GetFiancialDateById(yearId);

            if (year == null)
                year = SessionManager.CurrentCompanyFinancialDate;

            year.SetName(IsEnglish);

            lblYear.Text = "Year : " + year.Name; 

            //as it contains data generated from popup
            LoadInstitution();
            if (!IsPostBack)
            {
                Initialise();               
            }
            AttachJSCode();
            JavascriptHelper.AttachEnableDisablingJSCode(chkDeductFromSalary,rdbDeductFromSalary.ClientID,false);
        }
        
        void Initialise()
        {
            //rowHealthInsurance.Visible = CommonManager.Setting.EnableHealthInsurance != null && CommonManager.Setting.EnableHealthInsurance.Value;

            List<FinancialDate> years = new CommonManager().GetAllFinancialDates();
            foreach (FinancialDate date in years)
            {
                date.SetName(IsEnglish);
            }
            ddlFinancialYears.DataSource = years;
            ddlFinancialYears.DataBind();
            UIHelper.SetSelectedInDropDown(ddlFinancialYears, SessionManager.CurrentCompanyFinancialDate.FinancialDateId
                );

            SetDate(SessionManager.CurrentCompanyFinancialDate);

          

            LoadBranches();
            LoadInsuranceName();
           
            LoadPremiumPaymentFrequency();
            SetCalendarInitialDate();

            //load for emp from querystring
            LoadEmpFromQueryString();

            
           
        }

        private void LoadEmpFromQueryString()
        {
            int empId = UrlHelper.GetIdFromQueryString("EmpId");
            int insuranceId = UrlHelper.GetIdFromQueryString("Id");

            if( empId != 0)
            {
                EmployeeManager mgr = new EmployeeManager();
                EEmployee emp = mgr.GetById(empId);

                if ( emp != null)
                {
                    //for branch dropdown
                    //ddlFilterByBranch.ClearSelection();
                    //ListItem item = ddlFilterByBranch.Items.FindByValue(emp.Department.Branch.BranchId.ToString());
                    //if (item != null)
                    {
                        //item.Selected = true;
                        LoadEmployees();
                        LoadEmployeesByBranch();

                        UIHelper.SetSelectedInDropDown(ddlEmployeeList, emp.EmployeeId);
                        LoadInsuranceListing(null, null);

                        if (insuranceId != 0)
                        {
                            buttonsDiv.Style["display"] = "";
                            tblDetails.Style["display"] = "";
                            IIndividualInsurance insurance = InsuranceManager.GetInsuranceDetailsByEmployee(insuranceId);
                            LoadInsuranceDetails(insurance);
                        }
                    }
                }
            }
        }

        void SetCalendarInitialDate()
        {
        
        }
        
        void AttachJSCode()
        {
            JavascriptHelper.AttachPopUpCode(Page, "insuranceNamePopup", "ManageInsuranceName.aspx", 300, 300);
            JavascriptHelper.AttachPopUpCode(Page, "InstitutionPopup", "ManageInstitution.aspx", 450, 230);
        }
        
        void LoadBranches()
        {
            //ddlFilterByBranch.DataSource = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId);
            //ddlFilterByBranch.DataBind();

            //if( ddlFilterByBranch.Items.Count >0)
            //{
            //    ddlFilterByBranch.Items[1].Selected = true;
            //    LoadEmployeesByBranch();
            //}
            LoadEmployeesByBranch();
        }
        
        protected void LoadEmployees(object sender, EventArgs e)
        {
            LoadEmployeesByBranch();
        }
        
        void LoadEmployeesByBranch()
        {
            //if (ddlFilterByBranch.SelectedValue.Equals("-1") == false)
            //{
            ListItem first = ddlEmployeeList.Items[0];
            ddlEmployeeList.DataSource = empMgr.GetEmployeesByBranch(-1,-1);
            ddlEmployeeList.DataBind();
            ddlEmployeeList.Items.Insert(0, first);
            //}
            gvwList.SelectedIndex = -1;
            ClearInsuranceFields();
            gvwList.DataSource = new List<IIndividualInsurance>();
            gvwList.DataBind();
        }
        
        void LoadInsuranceName()
        {
            //ddlInsuranceName.DataSource = insMgr.GetInsuranceName();
            //ddlInsuranceName.DataBind();
        }
        
        void LoadInstitution()
        {
            string id = Request.Form[ddlInstitution.UniqueID];
            ddlInstitution.DataSource = insMgr.GetInsuranceInstitution();
            ddlInstitution.DataBind();
            if (id != null)
                UIHelper.SetSelectedInDropDown(ddlInstitution, id);           
        }
        
        void LoadPremiumPaymentFrequency()
        {
            rdbPaymentPeriod.DataSource = BizHelper.GetMembersAsListItem(new DAL.PremiumPaymentFrequency());
            rdbPaymentPeriod.DataBind();
            rdbPaymentPeriod.SelectedIndex = 0;
        }
        void GetTypeAndId(ref string type, ref int id)
        {
            if (ddlFilterByValues.SelectedValue == "-1")
            {
                type = "";
                id = 0;
            }
            else
            {
                type = ddlFilterBy.SelectedItem.Text;
                id = int.Parse(ddlFilterByValues.SelectedValue);
                
            }
        }

        protected void ddlFilterBy_SelectedIndexChanged(object sender, EventArgs e)
        {
            ListItem item = ddlFilterByValues.Items[0];
            item.Selected = false;
            ddlFilterByValues.Items.Clear();
            switch (int.Parse(ddlFilterBy.SelectedValue))
            {
                case 1:
                    BranchManager bMgr = new BranchManager();
                    ddlFilterByValues.DataSource = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId);
                    ddlFilterByValues.DataTextField = "Name";
                    ddlFilterByValues.DataValueField = "BranchId";

                    break;
                case 2:
                    DepartmentManager dMgr = new DepartmentManager();
                    ddlFilterByValues.DataSource = dMgr.GetDepartmentsByCompany(SessionManager.CurrentCompanyId);
                    ddlFilterByValues.DataValueField = "DepartmentId";
                    ddlFilterByValues.DataTextField = "Name";
                    break;
                //case 3:
                //    ddlFilterByValues.DataSource = cmpMgr.GetAllCostCodes();
                //    ddlFilterByValues.DataValueField = "CostCodeId";
                //    ddlFilterByValues.DataTextField = "Name";
                    //break;
            }
            ddlFilterByValues.DataBind();
            ddlFilterByValues.Items.Insert(0, item);

            LoadEmployees();
        }
        protected void ddlFilterByValues_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadEmployees();
        }
        void LoadEmployees()
        {
            string type = string.Empty; int id = 0;
            GetTypeAndId(ref type, ref id);

            List<GetEmployeesByFilterResult> list = insMgr.GetEmployeesByFilterSelection(SessionManager.CurrentCompanyId, type, id);


            chkList.DataSource = list;
            chkList.DataBind();

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            Page.Validate("Inc");

            if (Page.IsValid)
            {
                IIndividualInsurance ind = ProcessInsurance(null);
                if (ind.StartDateEng != null && ind.EndDateEng != null && ind.StartDateEng > ind.EndDateEng)
                {
                    divWarningMsg.InnerHtml = "Policy start date can not be greater than end date.";
                    divWarningMsg.Hide = false;
                    return;
                }
                if (this.CustomId != 0)
                {
                    ind.Id = this.CustomId;
                    Status status =  insMgr.UpdateIndividualInsurance(ind);
                    if (status.IsSuccess)
                    {
                        divMsgCtl.InnerHtml = "Insurance information is updated.";
                        divMsgCtl.Hide = false;
                    }
                    else
                    {
                        divWarningMsg.InnerHtml = status.ErrorMessage;
                        divWarningMsg.Hide = false;
                        return;
                    }
                    //JavascriptHelper.DisplayClientMsg("Insurance information is updated.", Page);
                }
                else
                {
                    insMgr.Save(ind);
                    divMsgCtl.InnerHtml = "Insurance information is saved.";
                    divMsgCtl.Hide = false;
                    //JavascriptHelper.DisplayClientMsg("Insurance information is saved.", Page);
                    this.CustomId = ind.InstitutionId.Value;
                    this.btnSave.Text = Resources.Messages.Update;
                    fsApplySettings.Visible = false;




                    IIndividualInsurance ins = new IIndividualInsurance();

                    foreach (ListItem item in chkSettings.Items)
                    {
                        //if (item.Selected == true && item.Value.Equals("1"))
                        //    ins.InsuranceNameId = int.Parse(ddlInsuranceName.SelectedValue.ToString());
                        if (item.Selected == true && item.Value.Equals("2"))
                            ins.InstitutionId = ind.InstitutionId;
                        if (item.Selected == true && item.Value.Equals("3"))
                            ins.EmployerPaysPercent = ind.EmployerPaysPercent;
                        if (item.Selected == true && item.Value.Equals("4"))
                            ins.EmployeePaysPercent = ind.EmployeePaysPercent;
                        if (item.Selected == true && item.Value.Equals("5"))
                            ins.PolicyAmount = ind.PolicyAmount;
                        if (item.Selected == true && item.Value.Equals("6"))
                            ins.Premium = ind.Premium;
                        if (item.Selected == true && item.Value.Equals("7"))
                            ins.PremiumPaymentFrequency = ind.PremiumPaymentFrequency;
                        //if (item.Selected == true && item.Value.Equals("8"))
                        //{
                        //    ins.PremiumPaymentFrequency = ind.PremiumPayment
                        //}
                    }
                    ins.IsDeductFromSalary = ind.IsDeductFromSalary;
                    ins.IsMonthlyPayment = ind.IsMonthlyPayment;

                    List<string> empIds = new List<string>();
                    foreach (ListItem emp in chkList.Items)
                    {
                        if (emp.Selected && emp.Value.ToString() != ind.EmployeeId.ToString())
                        {
                            //empIds += emp.Value + ",";
                            empIds.Add(emp.Value);
                        }
                    }
                    string list = string.Join(",", empIds.ToArray());


                    insMgr.AddInsuranceDetailsToMultiple(ins.InstitutionId, list,
                                                         ins.EmployerPaysPercent,
                                                         ins.EmployeePaysPercent, ins.PolicyAmount,
                                                         ins.Premium, ins.PremiumPaymentFrequency, ins.IsDeductFromSalary, ins.IsMonthlyPayment);

                }
                //if (gvInsuranceDetails.SelectedIndex != -1)
                //{
                //    int selectedInsuranceId = (int)gvInsuranceDetails.DataKeys[gvInsuranceDetails.SelectedIndex][0];
                //    ind.Id = selectedInsuranceId;
                //    insMgr.UpdateIndividualInsurance(ind);
                //    JavascriptHelper.DisplayClientMsg("Insurance information is updated.", Page);
                //}
                //else
                //{
                //    insMgr.Save(ind);
                //    JavascriptHelper.DisplayClientMsg("Insurance information is saved.", Page);
                //}
                //gvInsuranceDetails.SelectedIndex = -1;
                //LoadInsuranceDetails(null,null);
                ClearInsuranceFields();
                LoadInsuranceListing(null, null);
                //ClearInsuranceFields();
            }
            else
            {
                JavascriptHelper.DisplayClientMsg("Validation fail.", Page);
            }

        }

        IIndividualInsurance ProcessInsurance(IIndividualInsurance indiv)
        {
            if (indiv != null)
            {
                //ddlEmployeeList.SelectedValue = indiv.EmployeeId.ToString();
                //ddlInsuranceName.SelectedValue = indiv.InsuranceNameId.ToString();
                if( indiv.InstitutionId!=null)
                ddlInstitution.SelectedValue = indiv.InstitutionId.ToString();
                if (indiv.PolicyNo != null)
                    txtPolicyNo.Text = indiv.PolicyNo.ToString();

                if (indiv.FinancialYearId != null)
                    ddlFinancialYears.SelectedValue = indiv.FinancialYearId.ToString();

                if (indiv.StartDateEng != null)
                    dateFrom.SelectedDate = indiv.StartDateEng.Value;
                else
                    dateFrom.Text = "";

                if (indiv.EndDateEng != null)
                    dateTo.SelectedDate = indiv.EndDateEng.Value;
                else
                    dateTo.Text = "";

                // then old data so find out the financial year
                //if (indiv.FinancialYearId != null)
                //{
                //    FinancialDate year = CommonManager.GetFiancialDateById(indiv.FinancialYearId.Value);
                //    SetDate(year);
                //}
                //else
                //{
                //    FinancialDate date = 
                //        //CommonManager.GetFinancialYearByDate(indiv.StartDateEng.Value, indiv.EndDateEng.Value);
                //    if (date != null)
                //    {
                //        SetDate(date);
                //    }
                //}

                if (indiv.EmployeePaysPercent == 100)
                    ddlPaidBy.SelectedIndex = 0;
                else
                    ddlPaidBy.SelectedIndex = 1;

                //if (indiv.EmployeePaysPercent != null)
                //    txtEmployeePays.Text = indiv.EmployeePaysPercent.ToString();
                //if (indiv.EmployerPaysPercent != null)
                //{
                //    txtEmployerPays.Text = indiv.EmployerPaysPercent.ToString();
                //    //if( indiv.EmployerPaysPercent>0)
                //    //{
                //    //    chkDeductFromSalary.Attributes.Remove("disabled");
                //    //    rdbDeductFromSalary.Attributes.Remove("disabled");
                //    //}
                //}

                if (indiv.PolicyAmount != null)
                    txtPolicyAmount.Text = GetCurrency(indiv.PolicyAmount.Value);
                if (indiv.Premium != null)
                    txtPremium.Text = GetCurrency(indiv.Premium);
                // as frequency is always Year
                //if (indiv.PremiumPaymentFrequency != null)
                //    rdbPaymentPeriod.SelectedValue = indiv.PremiumPaymentFrequency.ToString();
                if (indiv.IsDeductFromSalary != null)
                    chkDeductFromSalary.Checked = indiv.IsDeductFromSalary.Value;
                else
                    chkDeductFromSalary.Checked = false;

                if (indiv.IsHealthInsurance != null)
                {
                    chkIsHealthInsurance.Checked = indiv.IsHealthInsurance.Value;
                }
                else
                    chkIsHealthInsurance.Checked = false;

                if (indiv.IsMonthlyPayment != null)
                {
                    if (indiv.IsMonthlyPayment.Value)
                        rdbDeductFromSalary.Items[1].Selected = true;
                    else
                    {
                        rdbDeductFromSalary.Items[0].Selected = true;
                    }
                }

                txtNote.Text = indiv.Note;




            }
            else
            {
                indiv = new IIndividualInsurance();
                indiv.EmployeeId = int.Parse(ddlEmployeeList.SelectedValue);
                //indiv.InsuranceNameId = int.Parse(ddlInsuranceName.SelectedValue);
                indiv.InstitutionId = int.Parse(ddlInstitution.SelectedValue);
                indiv.PolicyNo = txtPolicyNo.Text.Trim();

                indiv.FinancialYearId = int.Parse(ddlFinancialYears.SelectedValue);

                if (!string.IsNullOrEmpty(dateFrom.RawText))
                    indiv.StartDateEng = dateFrom.SelectedDate;

                if (!string.IsNullOrEmpty(dateTo.RawText))
                    indiv.EndDateEng = dateTo.SelectedDate;
                //FinancialDate year = CommonManager.GetFiancialDateById(indiv.FinancialYearId.Value);

                //indiv.StartDate = year.StartingDate;
                //indiv.StartDateEng = year.StartingDateEng;
                //indiv.EndDate = year.EndingDate;
                //indiv.EndDateEng = year.EndingDateEng;

                if( txtPolicyAmount.Text.Trim() != "")
                indiv.PolicyAmount = decimal.Parse(txtPolicyAmount.Text.Trim());
                indiv.Premium = decimal.Parse(txtPremium.Text.Trim());
                indiv.PremiumPaymentFrequency = rdbPaymentPeriod.SelectedValue;
                // as in Employee paid EmployeePaysPercent=100 and EmployerPaysPercent=0 and vice versa
                if (ddlPaidBy.SelectedIndex == 0)
                {
                    indiv.EmployeePaysPercent = 100;
                    indiv.EmployerPaysPercent = 0;
                }
                else
                {
                    indiv.EmployeePaysPercent = 0;
                    indiv.EmployerPaysPercent = 100;
                }
                //indiv.EmployeePaysPercent = double.Parse(txtEmployeePays.Text.Trim());
                //indiv.EmployerPaysPercent = double.Parse(txtEmployerPays.Text.Trim());
                //indiv.IsPersonalInsurance = bool.Parse(ddlIsPersonalInsurance.SelectedValue);

                indiv.IsHealthInsurance = chkIsHealthInsurance.Checked;
                indiv.IsDeductFromSalary = chkDeductFromSalary.Checked;
                if (indiv.IsDeductFromSalary.Value)
                    indiv.IsMonthlyPayment = rdbDeductFromSalary.Items[1].Selected;

                indiv.Note = txtNote.Text.Trim();

                //if (int.Parse(indi.StartDate) > int.Parse(indi.EndDate))
                //    return;

                //if (indiv.EmployeePaysPercent + indiv.EmployerPaysPercent != 100)
                //    return;

                //if (int.Parse(indiv.PolicyAmount) < int.Parse(indiv.Premium))
                //    return;

                return indiv;
            }
            return null;
        }

        protected void LoadInsuranceListing(object sender, EventArgs e)
        {
            if (sender != null)
            {
                ClearInsuranceFields();
            }

            int empId = int.Parse(ddlEmployeeList.SelectedValue);

            List<IIndividualInsurance> list = new InsuranceManager().GetInsuranceListingByEmployee(empId,yearId);
            gvwList.DataSource = list;
            gvwList.DataBind();


        }

        public void btnAdd_Click(object sender, EventArgs e)
        {
            ClearInsuranceFields();
            buttonsDiv.Style["display"] = "";
            tblDetails.Style["display"] = "";
           
        }

        protected void gvw_SelectedIndexChanged(object sender, EventArgs e)
        {
            buttonsDiv.Style["display"] = "";
            tblDetails.Style["display"] = "";

            //first record 
            int insuranceId = (int)gvwList.DataKeys[gvwList.SelectedIndex]["Id"];

            IIndividualInsurance insurance = InsuranceManager.GetInsuranceDetailsByEmployee(insuranceId);

            LoadInsuranceDetails(insurance);

        }

        protected void LoadInsuranceDetails(IIndividualInsurance insurance)
        {
            //gvInsuranceDetails.DataSource = 
            //gvInsuranceDetails.DataBind();
            //ClearInsuranceFields();
            //int empId = int.Parse(ddlEmployeeList.SelectedValue);
           //this.CustomId = empId;
            //insMgr.GetInsuranceDetailsByEmployee(int.Parse(ddlEmployeeList.SelectedValue));           
            //IIndividualInsurance insurance = insMgr.GetInsuranceDetailsByEmployee(empId);

            //int insuranceId = (int)gvInsuranceDetails.DataKeys[gvInsuranceDetails.SelectedIndex][0];
            //IIndividualInsurance insu = insMgr.GetInsuranceById(insuranceId);
            if (insurance != null)
            {
                this.CustomId = insurance.Id;
                ProcessInsurance(insurance);
                btnSave.Text = Resources.Messages.Update;
                fsApplySettings.Visible = false;
            }
            else
            {
                //fsApplySettings.Visible = true;
                btnSave.Text = Resources.Messages.Save;
                this.CustomId = 0;
            }
        }

        protected void ddlFinancialYears_Changed(object sender, EventArgs e)
        {
            int financialYearId = int.Parse(ddlFinancialYears.SelectedValue);
            FinancialDate year = CommonManager.GetFiancialDateById(financialYearId);
            if (year != null)
            {
                SetDate(year);
            }
        }

        void SetDate(FinancialDate year)
        {
            //CustomDate d1 = CustomDate.GetCustomDateFromString(year.StartingDate, IsEnglish);
            //CustomDate d2 = CustomDate.GetCustomDateFromString(year.EndingDate, IsEnglish);

            //lblYearDateRange.Text = d1.ToStringSkipDay() + " - " + d2.ToStringSkipDay();
        }

        protected void gvInsuranceDetails_RowCreated(object sender, GridViewRowEventArgs e)
        {
            //if (e.Row.RowType == DataControlRowType.DataRow)
            //{                          
            //    e.Row.Attributes.Add("onmouseover",
            //    "this.originalstyle=this.style.backgroundColor;this.style.backgroundColor='#8ea4d1';this.title ='click to edit';");

            //    e.Row.Attributes.Add("onmouseout",
            //    "this.style.backgroundColor=this.originalstyle;this.title ='';");
            //}
        }
               
        protected void gvInsuranceDetails_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            


        }

        protected void gvInsuranceDetails_SelectedIndexChanged(object sender, EventArgs e)
        {
            //int insuranceId = (int)gvInsuranceDetails.DataKeys[gvInsuranceDetails.SelectedIndex][0];
            //IIndividualInsurance insu = insMgr.GetInsuranceById(insuranceId);
            //if (insu != null)
            //{
            //    ProcessInsurance(insu);
            //    btnSave.Text = Resources.Messages.Update;
            //}
        }

        protected void gvInsuranceDetails_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            //IIndividualInsurance ins = new IIndividualInsurance();
            //int id = (int)gvInsuranceDetails.DataKeys[e.RowIndex][0];
            //if (id != 0)
            //{
            //    ins.Id = id;
            //    if (insMgr.DeleteIndividualInsurance(ins) == true)
            //    {
            //        JavascriptHelper.DisplayClientMsg("Insurance information is deleted.", Page);
            //        ClearInsuranceFields();
            //    }
            //    else
            //        JavascriptHelper.DisplayClientMsg("Insurance deletion failed", Page);
            //}
            //LoadInsuranceDetails(null, null);
        }

        void ClearInsuranceFields()
        {
            buttonsDiv.Style["display"] = "none";
            tblDetails.Style["display"] = "none";

            txtEmployeePays.Text = "";
            txtEmployerPays.Text = "";
            txtNote.Text = "";
            txtPolicyNo.Text = "";
            txtPremium.Text = "";
            txtPolicyAmount.Text = "";
            this.CustomId = 0;
            btnSave.Text = "Save";
            chkIsHealthInsurance.Checked = false;
            ddlPaidBy.SelectedIndex = 0;
            //ddlIsPersonalInsurance.SelectedIndex = 0;
            ddlInstitution.SelectedIndex = 0;
            dateFrom.Text = "";
            dateTo.Text = "";
            gvwList.SelectedIndex = -1;

            ddlFinancialYears.SelectedValue = SessionManager.CurrentCompanyFinancialDate.FinancialDateId.ToString();
            //FinancialDate currentYear = CommonManager.GetCurrentFinancialYear();
            //calStartDate.SetSelectedDate(currentYear.StartingDate, this.IsEnglish);
            //calEndDate.SetSelectedDate(currentYear.EndingDate, this.IsEnglish);
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            ClearInsuranceFields();
            LoadInsuranceListing(null, null);
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (!chkDeductFromSalary.Checked)
            {
                rdbDeductFromSalary.Items[0].Enabled = false;
                rdbDeductFromSalary.Items[1].Enabled = false;
            }
            else
            {
                rdbDeductFromSalary.Items[0].Enabled = true;
                rdbDeductFromSalary.Items[1].Enabled = true;
            }
        }

        /// <summary>
        /// Button is hidden as for internal use only
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnDelete_Click(object sender, EventArgs e)
        {
            this.CustomId = 0;

            ClearInsuranceFields();

            divMsgCtl.InnerHtml = "Insurance for this employee deleted.";
            divMsgCtl.Hide = false;

            InsuranceManager.DeleteInsurance(int.Parse(ddlEmployeeList.SelectedValue));
        }

        protected void gvwList_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            //if (e.CommandName == "Expired")
            //{
            //    int insuranceId = int.Parse(e.CommandArgument.ToString());
            //    //InsuranceManager.ChangeInsuranceStatus(insuranceId);
            //    LoadInsuranceListing(null, null);
            //    divMsgCtl.InnerHtml = "Insurance status chnaged.";
            //    divMsgCtl.Hide = false;
            //}
        }

    }
}

