﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using Utils.Helper;
using Utils.Web;
using BLL.Base;
using DAL;
using Utils.Base;

namespace Web
{
    public partial class IncomeDeductionAdjustment : BasePage
    {

        public new string GetCurrency(object amount)
        {
            return BaseHelper.GetCurrency(amount, 2);
        }

        //private int _tempCurrentPage;
        //private int _tempCount;
        private PayManager payMgr = new PayManager();

        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (!IsPostBack)
            {
                pagingCtl.Visible = false;
                Initialise();

            }
            RegisterScripts();
            JavascriptHelper.AttachNonDialogPopUpCode(Page, "importPopup", "../ExcelWindow/IncomeDeductionAdjustmentExcel.aspx", 450, 500);
            JavascriptHelper.AttachNonDialogPopUpCode(Page, "adjustmentPopup", "AdjustmentCalculation.aspx", 720, 650);

            LoadEmployeesInDDL();
        }

        void RegisterScripts()
        {
            StringBuilder str = new StringBuilder();
            str.AppendFormat("var decimalPlaces = {0};", SessionManager.DecimalPlaces);
          
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "rounding",
                                                        str.ToString()
                                                        , true);
        }

        public bool IsCalculateVisible(object value)
        {
            int type = int.Parse(value.ToString());
            if (type >= -12 && type <= -3)
            {
                return true;
            }
            return false;
        }

        protected void ddlDepartment_SelectedIndexChanged(object sender, EventArgs e)
        {
            ListItem firstItem = ddlDepartment.Items[0];
            ddlDepartment.Items.Clear();



            ddlDepartment.DataSource
                = DepartmentManager.GetAllDepartmentsByBranch(int.Parse(ddlBranch.SelectedValue));

            ddlDepartment.DataBind();

            ddlDepartment.Items.Insert(0, firstItem);
        }

        void Initialise()
        {
           

            ddlPayrollPeriods.DataSource = CommonManager.GetCurrentYear(SessionManager.CurrentCompanyId);
            ddlPayrollPeriods.DataBind();

            UIHelper.SetSelectedLastItemInDropDown(ddlPayrollPeriods);

            ddlBranch.DataSource
              = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId);
            ddlBranch.DataBind();

            //CommonManager mgr = new CommonManager();
            //ddlShift.DataSource = mgr.GetAllWorkShift();
            //ddlShift.DataBind();


            LoadEmployeesInDDL();

            LoadIncomeDeductionList();

        }
        protected void btnLoad_Click(object sender, EventArgs e)
        {
        
            LoadEmployees();
        }

        protected void LoadEmployees()
        {

            if (ddlIncomeOrDeduction.SelectedValue.Equals("-1"))
            {
                gvw.DataSource = null;
                gvw.DataBind();

                divWarningMsg.InnerHtml = "Income/Deduction should be selected for adjustment.";
                divWarningMsg.Hide = false;

                pagingCtl.Visible = false;

                return;
            }

            string [] values = ddlIncomeOrDeduction.SelectedValue.Split( new char[]{':'});
            int totalRecords = 0;

            int employeeId = -1;
            if (ddlEmployee.SelectedItem != null && ddlEmployee.SelectedItem.Value != null && ddlEmployee.SelectedItem.Value != "")
                employeeId = int.Parse(ddlEmployee.SelectedItem.Value);

            // Negative value adjustment is allowed for non basic income type only
            bool isNegativeValueAllowed = false;
            if (int.Parse(values[1]) == ((int)CalculationColumnType.Income))
            {
                PIncome basicIncome = new PayManager().GetBasicIncome(SessionManager.CurrentCompanyId);
                if (int.Parse(values[0]) != basicIncome.IncomeId)
                {
                    isNegativeValueAllowed = true;
                }
            }
            else if (int.Parse(values[1]) == ((int)CalculationColumnType.SST)
                || int.Parse(values[1]) == ((int)CalculationColumnType.TDS))
            {
                isNegativeValueAllowed = true;
            }
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "sdfsdf2", string.Format("var isNegativeValueAllowed = {0};", isNegativeValueAllowed.ToString().ToLower()), true);

            List<GetIncomeDeductionAdjustmentEmployeesResult> list = PayManager.GetIncomeDeductionAdjustment(
                int.Parse(ddlPayrollPeriods.SelectedValue), int.Parse(ddlBranch.SelectedValue), int.Parse(ddlDepartment.SelectedValue),
                employeeId, int.Parse(values[0]), int.Parse(values[1]),
                pagingCtl.CurrentPage-1, int.Parse(pagingCtl.DDLRecords.SelectedValue), ref totalRecords);

            pagingCtl.Visible = true;

            gvw.DataSource = list;                
            gvw.DataBind();
            pagingCtl.UpdatePagingBar(totalRecords);

            if (IsPayrollFinalSaved(int.Parse(ddlPayrollPeriods.SelectedValue)))
            {
                divWarningMsg.InnerHtml = Resources.Messages.CalcAllEmployeeFinalSavedMsg;
                divWarningMsg.Hide = false;
            }
            else
            {
                
                foreach (GetIncomeDeductionAdjustmentEmployeesResult item in list)
                {
                    if (item.IsCalculationSaved != null && item.IsCalculationSaved == 1)
                    {
                        divWarningMsg.InnerHtml = Resources.Messages.SomeOfTheEmployeeSalaryAlreadySavedMsg;
                        divWarningMsg.Hide = false;
                        break;
                    }
                }
            }
            
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            Page.Validate("Balance");

            decimal existingAmount, adjustmentAmount, payAmount,paidAmount;
            //string notes;
            int employeeId, payrollPeriodId, IncomeDeductionId;
            int type;
            bool isSavedReqd = false;
            if (Page.IsValid)
            {

                StringBuilder str = new StringBuilder("<root>");


                foreach (GridViewRow row in gvw.Rows)
                {
                   

                    employeeId = (int)gvw.DataKeys[row.RowIndex]["EmployeeId"];
                    payrollPeriodId = (int)gvw.DataKeys[row.RowIndex]["PayrollPeriodId"];
                    IncomeDeductionId = (int)gvw.DataKeys[row.RowIndex]["IncomeDeductionId"];
                    type = (int)gvw.DataKeys[row.RowIndex]["Type"];

                    TextBox txtExistingAmount = row.FindControl("txtExistingAmount") as TextBox;
                    TextBox txtAdjustmentAmount = row.FindControl("txtAdjustmentAmount") as TextBox;
                    TextBox txtPaidAmount = row.FindControl("txtPaidAmount") as TextBox;
                    TextBox txtPay = row.FindControl("txtPay") as TextBox;
                    TextBox txtNote = row.FindControl("txtNote") as TextBox;
                    HiddenField txtExpression = row.FindControl("txtExpression") as HiddenField;


                    string invalidText = Util.HasInvalidCharacterForXMLParsingAndImport(txtNote.Text.Trim());

                    if (!string.IsNullOrEmpty(invalidText))
                    {
                        divWarningMsg.InnerHtml = "\"" + invalidText + "\" character can not be set in the note.";
                        divWarningMsg.Hide = false;
                        return;
                    }

                   

                    existingAmount = decimal.Parse(txtExistingAmount.Text.Trim());
                    adjustmentAmount = decimal.Parse(txtAdjustmentAmount.Text.Trim());
                    payAmount = decimal.Parse(txtPay.Text.Trim());
                    paidAmount = txtPaidAmount.Text.Trim() == "" ? decimal.Parse("0") : decimal.Parse(txtPaidAmount.Text.Trim());

                    if (txtNote.Enabled && ( adjustmentAmount != 0 || payAmount != 0))
                    {
                        isSavedReqd = true;

                        str.Append(
                            string.Format("<row PayrollPeriodId=\"{0}\" EmployeeId=\"{1}\" IncomeDeductionId=\"{2}\" Type=\"{3}\"  ExistingAmount=\"{4}\" AdjustmentAmount=\"{5}\"   Note=\"{6}\" PaidAmount=\"{7}\" Exp=\"{8}\" /> ",
                            //overtimeId, payrollPeriodId, employeeId, overtimeHours, payPerHour, txtNote.Text.Trim()
                                payrollPeriodId, employeeId, IncomeDeductionId, type, existingAmount, adjustmentAmount, 
                               Server.HtmlEncode( txtNote.Text.Trim()) ,paidAmount,txtExpression.Value.Trim()
                            ));
                    }
                }
                str.Append("</root>");
                    
                if (isSavedReqd)
                {
                    PayManager.SaveIncomeDeductionAdjustment(str.ToString(),ddlIncomeOrDeduction.SelectedItem.Text);
                    divMsgCtl.InnerHtml = "Saved";
                    divMsgCtl.Hide = false;
                    LoadEmployees();
                }

            }

        }


        protected void btnCancel_Click(object sender, EventArgs e)
        {
            LoadEmployees();
        }
        protected void btnDelete_Click(object sender, EventArgs e)
        {
            StringBuilder xmlEmployees = new StringBuilder();
            xmlEmployees.Append("<root>");
            bool employeeAdded = false;
          
            foreach (GridViewRow row in gvw.Rows)
            {
                

                int employeeId = (int)gvw.DataKeys[row.RowIndex]["EmployeeId"];
                int payrollPeriodId = (int)gvw.DataKeys[row.RowIndex]["PayrollPeriodId"];
                int IncomeDeductionId = (int)gvw.DataKeys[row.RowIndex]["IncomeDeductionId"];
                
                TextBox txtExistingAmount = row.FindControl("txtExistingAmount") as TextBox;
                TextBox txtAdjustmentAmount = row.FindControl("txtAdjustmentAmount") as TextBox;
                TextBox txtNote = row.FindControl("txtNote") as TextBox;


                if (row.FindControl("chkDelete") == null)
                    continue;

                CheckBox chkDelete = (CheckBox)row.FindControl("chkDelete");

                if (chkDelete.Checked)
                {
                    employeeAdded = true;

                    xmlEmployees.Append(
                        string.Format("<row PayrollPeriodId=\"{0}\" EmployeeId=\"{1}\" IncomeDeductionId=\"{2}\"  /> ",
                        //overtimeId, payrollPeriodId, employeeId, overtimeHours, payPerHour, txtNote.Text.Trim()
                            payrollPeriodId, employeeId, IncomeDeductionId
                        ));
                }
            }
            xmlEmployees.Append("</root>");

            if ( employeeAdded)
            {
                PayManager.DeleteIncomeDeductionAdjustment( xmlEmployees.ToString(),ddlIncomeOrDeduction.SelectedItem.Text);
                divMsgCtl.InnerHtml = "Deleted";
                divMsgCtl.Hide = false;
                LoadEmployees();
            }
        }

        //protected void btnPostToSalary_Click(object sender, EventArgs e)
        //{

        //    foreach (GridViewRow row in gvw.Rows)
        //    {


        //        int payrollPeriodId = (int)gvw.DataKeys[row.RowIndex]["PayrollPeriodId"];

        //        PayManager.PostOvertimePay(payrollPeriodId);
        //        msgCtl.InnerHtml = Resources.Messages.OvertimePostedMessage;
        //        msgCtl.Hide = false;
        //        break;
        //    }


        //}
        protected void txtEmployeeName_TextChanged(object sender, EventArgs e)
        {
            

        }

        private void LoadIncomeDeductionList()
        {
            ListItem item = ddlIncomeOrDeduction.Items[0];
            ddlIncomeOrDeduction.Items.Clear();

            ddlIncomeOrDeduction.DataSource = PayManager.GetEmployeesIncomeDeductionList(int.Parse(ddlPayrollPeriods.SelectedValue));
            ddlIncomeOrDeduction.DataBind();

            ddlIncomeOrDeduction.Items.Insert(0, item);
        }

        protected void ddlPayrollPeriods_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadEmployeesInDDL();

            LoadIncomeDeductionList();
        }

        protected void ddlDepartment_SelectedIndexChanged1(object sender, EventArgs e)
        {
            LoadEmployeesInDDL();
        }

        private void LoadEmployeesInDDL()
        {
            //ListItem item = ddlEmployee.Items[0];
            //ddlEmployee.Items.Clear();

            storeEmployee.DataSource = EmployeeManager.GetValidEmployeesForPayrollPeriod(
                int.Parse(ddlPayrollPeriods.SelectedValue), int.Parse(ddlBranch.SelectedValue), int.Parse(ddlDepartment.SelectedValue));
            storeEmployee.DataBind();

            //if (ddlIncomeOrDeduction.SelectedItem.Text.Trim() == CalculationColumnType.TDS.ToString())
            //{
            //    gvw.Columns[3].Visible = true;
            //}
            //else
            //{
            //    gvw.Columns[3].Visible = false;
            //}

            //ddlEmployee.Items.Insert(0, item);
        }

        protected void btnPrevious_Click()
        {
            pagingCtl.CurrentPage -= 1;
            //_tempCurrentPage = int.Parse(pagintCtl.LabelCurrentPage.Text);
            //_tempCurrentPage -= 1;
            LoadEmployees();
        }

        protected void btnNext_Click()
        {
            pagingCtl.CurrentPage += 1;
            //_tempCurrentPage = int.Parse(pagintCtl.LabelCurrentPage.Text);
            //_tempCurrentPage += 1;
            LoadEmployees();
        }

        protected void ddlRecords_SelectedIndexChanged()
        {
            //SessionManager.EmployeeDisplayRecodsPerPage = int.Parse(pagintCtl.DDLRecords.SelectedValue);
            //_tempCurrentPage = 1;
            pagingCtl.CurrentPage = 1;
            LoadEmployees();
        }

    }
}
