﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Base;
using BLL.Manager;
using DAL;
using Utils.Web;
using Utils.Helper;
using Utils.Calendar;

namespace Web.CP
{
    public partial class StopPaymentPG : BasePage
    {
        PayManager payMgr = new PayManager();
        StopPaymentManager stopPayMgr = new StopPaymentManager();
        protected void gvwEmployees_RowCreated(object sender, GridViewRowEventArgs e)
        {

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover",
                "this.originalClass=this.className;this.className='selected'");

                e.Row.Attributes.Add("onmouseout",
                "this.className=this.originalClass;");


            }

        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ddlWorkdays.SelectedIndex = 1;
                Initialise();
            }

            if (Request.Form["__EVENTTARGET"] != null && Request.Form["__EVENTTARGET"].Equals("Reload"))
            {
                gvw.SelectedIndex = -1;
                ClearFields();
                LoadStopPayments();
            }
            JavascriptHelper.AttachPopUpCode(Page, "resumePaymentPopup", "ResumePayment.aspx", 355, 160);
        }

        void Initialise()
        {
            LoadEmployees();
           
            calFromDate.IsEnglishCalendar = SessionManager.CurrentCompany.IsEnglishDate;
            calFromDate.SelectTodayDate();
            calToDate.IsEnglishCalendar = SessionManager.CurrentCompany.IsEnglishDate;
            calToDate.SelectTodayDate();
            LoadStopPayments();
        }

        void LoadEmployees()
        {
            EmployeeManager emgMgr = new EmployeeManager();
            ddlEmployees.DataSource = emgMgr.GetEmployees(SessionManager.CurrentCompanyId);
            ddlEmployees.DataBind();
        }

        void LoadStopPayments()
        {


            gvw.DataSource = stopPayMgr.GetStopPayments(SessionManager.CurrentCompanyId,chkShowArchieve.Checked);
            gvw.DataBind();
        }



       

        protected void ddlEmployees_SelectedIndexChanged(object sender, EventArgs e)
        {
           

        }


        

        void Process(ref StopPayment entity)
        {
            if (entity == null)
            {
                entity = new StopPayment();
                entity.EmployeeId = int.Parse(ddlEmployees.SelectedValue);
                entity.FromDate = calFromDate.SelectedDate.ToString();
                entity.ToDate = calToDate.SelectedDate.ToString();
                //set eng date

                entity.EngFromDate = GetEngDate(entity.FromDate);
                entity.EngToDate = GetEngDate(entity.ToDate);

                entity.Notes = txtNotes.Text.Trim();

                entity.ExcludePeriodInWorkDaysCount = bool.Parse(ddlWorkdays.SelectedValue); ;// chkExcludeWorkdays.Checked;
                entity.TreatLikeRetirementForTax = chkTreatLikeRetForTax.Checked;

            }
            else
            {
                //check if stop payment start date is passed i.e. stop payment already used in the
                // calculation then disable fields
                bool isAlreadyUsed = stopPayMgr.IsStopPaymentAlreadyUsed(entity.StopPaymentId);

                if (isAlreadyUsed)
                {
                    divWarningMsg.InnerHtml = "Stop payment already used, so some fields are disabled.";
                    divWarningMsg.Hide = false;

                    calFromDate.Enabled = false;
                    ddlWorkdays.Enabled = false;
                    //chkExcludeWorkdays.Enabled = false;
                    chkTreatLikeRetForTax.Enabled = false;

                }
                else
                {
                    calFromDate.Enabled = true;
                    ddlWorkdays.Enabled = true;
                    //chkExcludeWorkdays.Enabled = true;
                    chkTreatLikeRetForTax.Enabled = true;
                }

                UIHelper.SetSelectedInDropDown(ddlEmployees, entity.EmployeeId);

                //ddlEmployees_SelectedIndexChanged(null, null);


                calFromDate.SetSelectedDate(entity.FromDate, SessionManager.CurrentCompany.IsEnglishDate);
                
                
                calToDate.SetSelectedDate(entity.ToDate, IsEnglish);
                txtNotes.Text = entity.Notes;

                UIHelper.SetSelectedInDropDown(ddlWorkdays, entity.ExcludePeriodInWorkDaysCount.Value.ToString().ToLower());
                //chkExcludeWorkdays.Checked = entity.ExcludePeriodInWorkDaysCount.Value;
                chkTreatLikeRetForTax.Checked = entity.TreatLikeRetirementForTax.Value;
               
            }

        }

        public bool IsDeletable(object stopPaymentId)
        {
            if (SessionManager.IsReadOnlyUser)
                return false;

            return !stopPayMgr.IsStopPaymentDeletable((int)stopPaymentId);
        }

        public bool IsStopPaymentEditable(object id)
        {
            if (SessionManager.IsReadOnlyUser)
                return false;

            return !stopPayMgr.IsStopPaymentEnded((int)id);
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                int stopPaymentId = 0;

                if (gvw.SelectedIndex != -1)
                {
                    stopPaymentId = (int)gvw.DataKeys[gvw.SelectedIndex]["StopPaymentId"];
                }
                StopPayment stopPayment= null;
                Process(ref stopPayment);

                InsertUpdateStatus status = new InsertUpdateStatus();
                if (stopPaymentId == 0)
                {
                    

                     status =  stopPayMgr.Save(stopPayment);
                    if (status.IsSuccess)
                    {
                        divMsgCtl.InnerHtml = "Stop payment saved for the employee.";
                        divMsgCtl.Hide = false;
                        
                        ClearFields();
                    }
                    else
                    {
                        divWarningMsg.InnerHtml = status.ErrorMessage;
                        divWarningMsg.Hide = false;
                    }
                }
                else
                {
                    stopPayment.StopPaymentId = stopPaymentId;
                    status = stopPayMgr.Update(stopPayment);
                    if (status.IsSuccess)
                    {
                        divMsgCtl.InnerHtml = "Stop payment updated for the employee.";
                        divMsgCtl.Hide = false;
                       
                        ClearFields();
                    }
                    else
                    {
                        divWarningMsg.InnerHtml = status.ErrorMessage;
                        divWarningMsg.Hide = false;
                    }
                }

                if (status.IsSuccess)
                {
                    gvw.SelectedIndex = -1;
                    LoadStopPayments();
                }
            }
        }

        void ClearFields()
        {
            btnSave.Text = Resources.Messages.Save;
            ddlEmployees.ClearSelection();
            ddlEmployees.SelectedIndex = 0;
            txtNotes.Text = "";
            calFromDate.SelectTodayDate();

            ddlWorkdays.SelectedIndex = 1;

            ddlEmployees.Enabled = true;

            calFromDate.Enabled = true;
            ddlWorkdays.Enabled = true;
            //chkExcludeWorkdays.Enabled = true;
            chkTreatLikeRetForTax.Enabled = true;
        }

        protected void gvw_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvw.SelectedIndex = -1;
            gvw.PageIndex = e.NewPageIndex;
            LoadStopPayments();
        }

       

        protected void gvw_SelectedIndexChanged1(object sender, EventArgs e)
        {
            if (gvw.SelectedIndex >= 0)
            {
                int stopPaymentId = (int)gvw.DataKeys[gvw.SelectedIndex]["StopPaymentId"];

                StopPayment entity = stopPayMgr.GetStopPaymentById(stopPaymentId);
                Process(ref entity);
                btnSave.Text = Resources.Messages.Update;
                ddlEmployees.Enabled = false;
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            ClearFields();
            gvw.SelectedIndex = -1;
            LoadStopPayments();
        }

        protected void chkShowArchieve_Changed(object sender, EventArgs e)
        {
            LoadStopPayments();
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (ddlEmployees.SelectedValue != "-1")
            {
                lblEmpId.Text = ddlEmployees.SelectedValue;
            }
            else
                lblEmpId.Text = "";

            btnSave.Visible = !SessionManager.IsReadOnlyUser;
        }

        protected void gvw_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            e.Cancel = true;
            int stopPaymentId = (int)gvw.DataKeys[e.RowIndex]["StopPaymentId"];

            if (IsDeletable(stopPaymentId))
            {
                if (stopPayMgr.Delete(stopPaymentId))
                {
                    divMsgCtl.InnerHtml = "Stop payment deleted.";
                    divMsgCtl.Hide = false;
                }
               
            }
            gvw.SelectedIndex = -1;
            LoadStopPayments();
            ClearFields();
        }
    }
}
