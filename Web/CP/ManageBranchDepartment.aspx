<%@ Page Title="Manage departments for branches" MaintainScrollPositionOnPostback="true"
    Language="C#" ValidateRequest="false" EnableEventValidation="false" MasterPageFile="~/Master/HROld.Master"
    AutoEventWireup="true" CodeBehind="ManageBranchDepartment.aspx.cs" Inherits="Web.CP.ManageBranchDepartment" %>

<%@ Register Src="~/UserControls/MGBranchDepartment.ascx" TagName="MGBranch" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        h2.popstitle
        {
            color: #333333;
            font: bold 13px Arial;
            padding: 0 !important;
            margin: 3px 0 0;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Add Departments to Branch
                </h4>
            </div>
        </div>
    </div>
    <div class="contentArea">
        <%-- <h2>Manage Branches</h2>--%>
        <%--  <div class="bodypart bodypartsmall">--%>
        <%-- <div class="bodypart">--%>
        <uc1:MGBranch Id="MGBranch1" runat="server" />
        <%--   </div>--%>
    </div>
</asp:Content>
<%--edid by sudab--%>