<%@ Page Title=" Attendance Register" MaintainScrollPositionOnPostback="true" ValidateRequest="false"
    EnableEventValidation="false" Language="C#" MasterPageFile="~/Master/HROld.Master"
    AutoEventWireup="true" CodeBehind="AttendanceTracking.aspx.cs" Inherits="Web.CP.AttendanceTracking" %>

<%@ Register Src="../Controls/PagingCtl.ascx" TagName="PagingCtl" TagPrefix="uc1" %>
<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/AttendanceLegendCtl.ascx" TagName="AttendanceLegendCtl"
    TagPrefix="uc1" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="InfoMsgCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/ErrorMsgCtl.ascx" TagName="ErrorMsgCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .tableLightColor td, .tableLightColor th
        {
            padding: 3px 6px 3px 6px !important;
        }
        .tableLightColor th
        {
            color: white;
            font-style: normal !important;
            background-color: #B8CCE4 !important;
            border: 1px solid #4F81BD;
            border-bottom: 2px solid #4F81BD;
        }
        .tableLightColor .odd td
        {
            border: 1px solid #4F81BD;
            background: none repeat scroll 0 0 #DBE5F1;
        }
        .tableLightColor .even td
        {
            border: 1px solid #4F81BD;
        }
        
        .disabledItemsDesign
        {
            color: grey !important;
            font-style: italic !important;
        }
        .payPresentTD
        {
            padding: 0px !important;
        }
        .empDetailsDivStyle
        {
            color: #555555;
            font-style: normal;
            background-color: #FFFFBF;
            border: 1px solid #7F9DB9;
            height: 20px;
            width: 180px;
            padding-left: 4px;
            padding-top: 1px;
        }
        
        #popupAssingBulkLeaves table td
        {
            padding-top: 10px;
        }
        .bodypart
        {
            width: inherit;
        }
        .container
        {
            margin-left: none;
        }
        .attendanceCell
        {
            font-size: smaller;
            font: 10px Tahoma,sans-serif;
            height: 18px;
            border: 1px solid #829CB7;
        }
        .attendanceChangedCell
        {
            background-color: yellow;
        }
        .tableLightColor input
        {
            font-size: 11px !important;
        }
    </style>
    <script type="text/javascript">

        function ACE_item_selected(source, eventArgs) {
            var value = eventArgs.get_value();

            document.getElementById('<%= hiddenEmpId.ClientID %>').value = value;
        }

        function setAbsentDate(value) {
            if (value != "")
                document.getElementById('<%= absentToDate.ClientID %>').innerHTML = value;
        }


        var leaves = new Array();
        //        leaves["LOP"] = new Array();
        //        leaves["LOP"]["Pay"] = 1;
        //        leaves["LOP"]["Present"] = 0.5;
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <img src='<%= Page.ResolveUrl("~/images/saving.gif") %>' id="loadingStartingImage"
        alt="" class="loadingStartingImage" />
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Attendance Register
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel" style="margin-top: 15px !important; padding-top: 0px !important;">
        <div class="alert alert-warning" runat="server" id="divAutoAbsentMarkingDateMsg"
            visible="false">
        </div>
        <div class="contentArea">
            <asp:HiddenField ID="hiddenEmpId" runat="server" Value="" />
            <div id="empDetailsDiv" class="empDetailsDivStyle" style="width: 150; height: 50;
                border: 1ps solid black; display: none;">
            </div>
            <div id="empDetailsDivOnHover" class="empDetailsDivStyle" style="width: 150; height: 50;
                border: 1ps solid black; display: none;">
            </div>
            <div style='display: none1;' id='ddlOptionsDiv'>
                <asp:DropDownList CssClass="attendanceCell" AppendDataBoundItems="true" onclick="stopPropagation(event)"
                    Style="display: none; height: 20px" onchange='setOption(this)' DataTextField="Value"
                    DataValueField="Value" ID="ddlOptions" runat="server">
                </asp:DropDownList>
            </div>
            <div class="attribute" style="padding: 10px">
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td style='padding-bottom: 5px'>
                            <strong>Month/Year</strong>
                        </td>
                        <td>
                            <strong>Filter By</strong>
                        </td>
                        <td style="padding-left: 10px">
                            <strong>Quick Search</strong>
                        </td>
                        <td>
                            <strong>Status</strong>
                        </td>
                        <td runat="server" id="tdUnit1"  style='padding-left:5px;'>
                            <strong>Unit</strong>
                        </td>
                        <td rowspan="2" valign="bottom" style="padding-left: 15px">
                            <asp:Button OnClientClick="fillAttendance();return false;" ID="btnFillAtte" CssClass="update"
                                runat="server" Text="Fill Att." />
                        </td>
                        <td visible="false" runat="server" id="tdImport" rowspan="2" style='padding-left: 15px;padding-bottom: 10px;' valign="bottom">
                            <asp:LinkButton ID="btnImport" Style='text-decoration: underline' runat="server"
                                Text="Import" OnClientClick="importPopupProcess();return false;" />
                        </td>
                        <td rowspan="2" valign="bottom" style="padding-left: 15px">
                            <asp:Button ID="btnGenerateAtte" OnClientClick='return confirm("Are you sure, you want to generate the Attendance and confirm time attendance has been generated from Leave and Attendance -> Generate Attendance Report for auto absent marking?");'
                                Width="100px" Visible="false" OnClick="btnGenerateAtte_Click" CssClass="save"
                                runat="server" Text="Generate Atte" />
                        </td>
                        <td rowspan="2" style="padding-top: 8px;" visible="false" runat="server" id="absent1">
                            <span runat="server" id="absentToDate" style=''></span>
                        </td>
                        <td rowspan="2" style="padding-left: 5px; padding-top: 8px;" visible="false" runat="server"
                            id="absent2">
                            <a href="javascript:void(0)" onclick="currencyHistoryPopup();return false;" runat="server"
                                id="linkCurrencyHistory" style=''>Change Date</a>
                        </td>
                    </tr>
                    <tr>
                        <td class="style1" width="130px">
                            <asp:DropDownList Width="120px" AppendDataBoundItems="true" DataTextField="Name"
                                DataValueField="PayrollPeriodId" ID="ddlPayrollPeriods" runat="server" AutoPostBack="True"
                                OnSelectedIndexChanged="ddlPayrollPeriods_SelectedIndexChanged">
                            </asp:DropDownList>
                        </td>
                        <td>
                            <asp:DropDownList Width="130px" ID="ddlFilterBy" runat="server" AutoPostBack="True"
                                OnSelectedIndexChanged="ddlFilterBy_SelectedIndexChanged">
                                <asp:ListItem Text="--Select Filter By--" Value="-1"></asp:ListItem>
                                <asp:ListItem Text="Branch" Value="1"></asp:ListItem>
                                <asp:ListItem Text="Department" Value="2"></asp:ListItem>
                                <asp:ListItem Text="Cost Code" Value="3"></asp:ListItem>
                            </asp:DropDownList>
                            &nbsp;
                            <asp:DropDownList Width="160px" ID="ddlFilterByValues" runat="server" AutoPostBack="True"
                                OnSelectedIndexChanged="ddlFilterByValues_SelectedIndexChanged">
                                <asp:ListItem Text="--Select Value--" Value="-1"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td class="style1" style="padding-left: 10px" width="175px">
                            <asp:TextBox AutoPostBack="true" Width="160px" ID="txtEmpSearchText" runat="server"
                                OnTextChanged="txtEmpSearchText_TextChanged"></asp:TextBox>
                            <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtEmpSearchText"
                                WatermarkText="Employee Name" WatermarkCssClass="searchBoxText" />
                            <cc1:AutoCompleteExtender EnableCaching="true" ID="AutoCompleteExtenderOrganization"
                                runat="server" MinimumPrefixLength="2" ServiceMethod="GetEmployeeNames" ServicePath="~/PayrollService.asmx"
                                TargetControlID="txtEmpSearchText" CompletionSetCount="10" CompletionInterval="250"
                                OnClientItemSelected="ACE_item_selected" CompletionListCssClass="AutoExtender"
                                CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight">
                            </cc1:AutoCompleteExtender>
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlStatus" AutoPostBack="true" AppendDataBoundItems="true"
                                DataTextField="Name" DataValueField="StatusID" runat="server" Width="150px" OnSelectedIndexChanged="ddlFilterByValues_SelectedIndexChanged">
                                <asp:ListItem Text="--Select Status--" Value="-1" Selected="True" />
                            </asp:DropDownList>
                        </td>
                        <td runat="server" id="tdUnit2" style='padding-left:5px;'>
                            <asp:DropDownList ID="ddlUnit" AutoPostBack="true" AppendDataBoundItems="true"
                                DataTextField="Name" DataValueField="UnitID" runat="server" Width="120px"  OnSelectedIndexChanged="ddlFilterByValues_SelectedIndexChanged">
                                <asp:ListItem Text="--Select Unit--" Value="-1" Selected="True" />
                            </asp:DropDownList>
                        </td>
                    </tr>
                </table>
                <asp:Button ID="btnLoad" Visible="false" runat="server" OnClick="btnLoad_Click" Style="height: 26px;"
                    Text="Load" />
            </div>
            <uc2:InfoMsgCtl runat="server" Hide="true" EnableViewState="false" ID="msgInfo" />
            <uc2:WarningCtl ID="divWarningMsg" EnableViewState="false" Hide="true" runat="server" />
            <uc2:ErrorMsgCtl ID="divErrorMsg" Hide="true" EnableViewState="false" runat="server" />
            <div id="wrapperDiv" class="container1" style='display: none; clear: both; margin-top: 10px;
                overflow: auto; overflow-y: hidden;'>
                <asp:GridView EnableViewState="false" CssClass="tableLightColor" UseAccessibleHeader="true"
                    ID="gvw" runat="server" AutoGenerateColumns="False" DataKeyNames="AttendenceId,EmployeeId,PayrollPeriodId,IsUsedInCalculation,DisableCellRange"
                    ShowFooterWhenEmpty="False" GridLines="None" ShowHeaderWhenEmpty="True">
                    <Columns>
                        <asp:TemplateField Visible="true" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                            <HeaderStyle BorderWidth="1" />
                            <ItemStyle BorderWidth="0" />
                            <ItemTemplate>
                                <asp:Image runat="server" ID="txt0" CssClass='imgSelection' AlternateText="Delete" onclick='<%# "displayBulkLeaveWindow(this," + Eval("EmployeeId") + ")" %>'
                                    ToolTip="Assign Bulk Leave" ImageUrl="~/images/att.png" Style='cursor: pointer' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Delete" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                            <HeaderTemplate>
                                <asp:CheckBox ID="chkDelete" title="Delete" onclick="selectDeselectAll(this)" runat="server" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:CheckBox ID="chkDelete" Enabled='<%# Convert.ToInt32(Eval("AttendenceId")) != 0 %>'
                                    Visible='<%# Convert.ToInt32(Eval("AttendenceId")) != 0 %>' runat="server" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="EIN">
                            <ItemTemplate>
                                <%--don't remove class "CssClass="lblEmployeeId""--%>
                                <asp:Label ID="lblId" CssClass="lblEmployeeId" runat="server" Text='<%# Bind("EmployeeId", "{0:000}") %>'></asp:Label>
                                <asp:HiddenField ID="hfLeaveList" runat="server" Value='<%# Bind("LeaveList") %>' />
                                <asp:HiddenField ID="hfIsFemale" runat="server" Value='<%#  Convert.ToInt32(Eval("IsFemale")) %>' />
                                <asp:HiddenField ID="hfEmployeeId" runat="server" Value='<%# Bind("EmployeeId") %>' />
                                <asp:HiddenField ID="hfLeaveRequest" runat="server" Value='<%# Bind("LeaveRequest") %>' />
                                <asp:HiddenField ID="hfCasteId" runat="server" Value='<%# Bind("CasteId") %>' />
                            </ItemTemplate>
                            <HeaderStyle Width="80px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Name" HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:Label Width="140px" ID="Label2" runat="server" Text='<%# Bind("Name") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-CssClass="payPresentTD" HeaderText="Ded">
                            <ItemTemplate>
                                <asp:HiddenField ID="hfPayDays" Value='<%# Bind("PayDays") %>' runat="server" />
                                <asp:Label ID="lblPayDays" Width="30px" runat="server" Text='<%# Bind("PayDays") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Abs">
                            <ItemTemplate>
                                <asp:HiddenField ID="hfPresentDays" Value='<%# Bind("PresentDays") %>' runat="server" />
                                <asp:Label ID="lblPresentDays" Width="30px" runat="server" Text='<%# Bind("PresentDays") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <RowStyle CssClass="odd" />
                    <AlternatingRowStyle CssClass="even" />
                    <EmptyDataTemplate>
                        <b>No employee records.</b>
                    </EmptyDataTemplate>
                </asp:GridView>
            </div>
            <uc1:PagingCtl ID="pagingCtl" ShowDropDown="true" OnChangePage="ChangePageNumber"
                OnNextRecord="btnNext_Click" OnPrevRecord="btnPrevious_Click" OnDropDownShowRecordsChanged="ddlRecords_SelectedIndexChanged"
                runat="server" />
            <div style='clear: both;' class="buttonsDiv">
                <asp:Panel ID="pnl" runat="server" Style="margin-bottom: 10px">
                    <asp:Button ID="btnApply" runat="server" CssClass="save" Text="Apply" OnClick="btnApply_Click" />
                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="cancel" OnClick="btnCancel_Click" />
                    <asp:Button ID="btnDelete" OnClientClick="if(confirm('Confirm delete attendance?')==false) return false;"
                        runat="server" CssClass="delete" Text="Delete" OnClick="btnDelete_Click" />
                    <asp:Button ID="btnDeleteAll" OnClientClick="if(confirm('Confirm delete all attendance?')==false) return false;"
                        runat="server" CssClass="delete" Text="Delete All" OnClick="btnDeleteAll_Click" />
                </asp:Panel>
                <div style='width: 100%;' class="colRight">
                    <uc1:AttendanceLegendCtl ID="AttendanceLegendCtl1" runat="server" />
                </div>
            </div>
        </div>
        <div id="popupAssingBulkLeaves" style="display: none; width: 300px; height: 300px;
            background-color: white; border: 4px solid #0587b8; padding: 10px; background-color: #e8f1ff">
            <table>
                <tr>
                    <td colspan="2">
                        Employee :
                        <label id="lblBulkAssignEmployeeName">
                        </label>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <select id="ddlBulkLeaves" style='width: 200px' onchange="bulkLeaveTypeChange(this)">
                        </select>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        Balance :
                        <label id="lblBulkLeaveBalance">
                        </label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <input type="text" onkeydown="return onlyIntNumbers(event)" id="bulkStartingFrom"
                            style='width: 50px' />
                        Starting from
                    </td>
                    <td>
                        <input type="checkbox" id="wholeMonth" value="on" />
                        <label for="wholeMonth">
                            Whole month</label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <input type="text" id="bulkDays" onkeydown="return onlyIntNumbers(event)" style='width: 50px' />
                        Days
                    </td>
                    <td>
                        <input type="checkbox" id="includeHoliday" value="on" />
                        <label for="includeHoliday">
                            Include holiday</label>
                    </td>
                </tr>
                 <tr>
                    <td>
                     
                    </td>
                    <td>
                        <input type="checkbox" id="chkAllEmployees" value="on" />
                        <label for="includeHoliday">
                            Copy to all employees</label>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style='padding-top: 25px'>
                        <input type="button" class="save" value="OK" onclick="processBulkLeaveAssignCall()" />
                        <input type="button" class="cancel" value="Cancel" onclick="popupAssingBulkLeaves.trigger('close');" />
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="pageScriptsContent" runat="server">
    <script type="text/javascript">

        function importPopupProcess(btn) {

            var payrollPeriodId = document.getElementById('<%= ddlPayrollPeriods.ClientID %>').value;


            // if (IsProjectInputType)
            if (payrollPeriodId == 0)
                return false;

            var ret = importPopup("payrollPeriod=" + payrollPeriodId);


            return false;
        }

        var popupAssingBulkLeaves = null;
        function bulkLeaveTypeChange(ddl) {
            var values = ddl.value.split(":");
            if (values[0] == "1") //for leave only
            {
                var empKey = bulkEmployeeID + ":" + values[1];
                var balance = parseFloat(leaveBalance[empKey])
                $('#lblBulkLeaveBalance').html(balance);
            }
            else {
                $('#lblBulkLeaveBalance').html('');
            }
        }

        function processBulkLeaveAssignCall() {
            
            var copyToAll = $("#chkAllEmployees").is(":checked");

            if (copyToAll == false)
                processBulkLeaveAssign(bulkLeaveRowID);
            else {
                
                
                $( ".imgSelection" ).each(function() {

                    var rowId = getRowId(this.id);
                    processBulkLeaveAssign(rowId);
                });
            }

        }
        function processBulkLeaveAssign(bulkLeaveRowID) {
            var ddlBulkLeaves = $("#ddlBulkLeaves");
            var isWholeMonth = $("#wholeMonth").is(":checked");
            var isIncludeHoliday = $("#includeHoliday").is(":checked");
            

            if (isWholeMonth == false && $("#bulkDays").val() == "") {
                alert("Day(s) is required.");
                $("#bulkDays").focus();
                return;
            }
            if (isWholeMonth == false && $("#bulkStartingFrom").val() == "") {
                alert("Starting from is required.");
                $("#bulkStartingFrom").focus();
                return;
            }

            var bulkDays = isWholeMonth == true ? 0 : parseFloat($("#bulkDays").val());
            var bulkStartingFrom = isWholeMonth == true ? 1 : parseInt($("#bulkStartingFrom").val());

            if (bulkStartingFrom < 1 || bulkStartingFrom > totalDays) {
                alert("Starting from must lie between 1-" + totalDays + ".");
                $("#bulkStartingFrom").focus();
                return;
            }

            var initialId = null;

            var startingTextboxId = bulkLeaveRowID + "txt" + bulkStartingFrom;
            ifFirstSelectSetForAllCells(startingTextboxId);

            if (isWholeMonth) {
                bulkDays = 35; //set maximum
            }

            for (var i = bulkStartingFrom; i < bulkStartingFrom + bulkDays; i++) {
                var startingTextboxId = bulkLeaveRowID + "txt" + i;
                var currentTxt = document.getElementById(startingTextboxId);
                if (currentTxt == null) // then no cell after this
                    continue;

                initialId = currentTxt.id;

                var currentType = document.getElementById(currentTxt.id.replace("txt", "hfType"));
                //var currentTypeValue = document.getElementById(currentTxt.id.replace("txt", "hfTypeValue"));
                //var currentDeductDays = document.getElementById(currentTxt.id.replace("txt", "hfDeductDays"));

                //if holiday & include holiday is unchecked then skip for this cell
                if (isIncludeHoliday == false && (currentType.value == "2" || currentType.value == "3"))
                    continue;

                //if cell is disabled then skip
                if (currentTxt.disabled)
                    continue;

                /// set option for txt,hfType,hfTypeValue controls       
                currentTxt.value = ddlBulkLeaves[0].options[ddlBulkLeaves[0].selectedIndex].text;
                var value = ddlBulkLeaves[0].value;


                if (value == "P") {
                    currentType.value = "0:0:0";
                    //currentTypeValue.value = "0";
                    //currentDeductDays.value = "0";
                }
                else {
                    var values = value.split(":");
                    currentType.value = values[0]+':' + values[1]+':'+getDeductValue(currentTxt.value); 
//                    currentType.value = values[0];
//                    currentTypeValue.value = values[1];
//                    currentDeductDays.value = getDeductValue(currentTxt.value);
                }

            }

            if (initialId != null) {
                calculateDays(getInitialId(initialId));
            }

            if (popupAssingBulkLeaves == null)
                popupAssingBulkLeaves = $('#popupAssingBulkLeaves');

            popupAssingBulkLeaves.trigger('close');

        }

        var bulkLeaveRowID;
        var bulkEmployeeID;
        function displayBulkLeaveWindow(imgBulkLeave, employeeId) {
            $("#wholeMonth").removeAttr('checked');
            $("#includeHoliday").removeAttr('checked');
            $("#bulkDays").val("");
            $("#bulkStartingFrom").val("");

            bulkEmployeeID = employeeId;
            bulkLeaveID = imgBulkLeave.id;
            $('#lblBulkLeaveBalance').html('');
            $('#lblBulkAssignEmployeeName').html(document.getElementById(imgBulkLeave.id.replace("txt0", "Label2")).innerHTML);
            var ddlBulkLeaves = $("#ddlBulkLeaves");
            var rowId = getRowId(imgBulkLeave.id);
            bulkLeaveRowID = rowId;

            var id = rowId + "hfLeaveList";
            //split each leaves
            var list = document.getElementById(id).value.split(";");
            ddlBulkLeaves[0].options.length = 0;

            if (hasHourlyLeave) {
                addHourlyLeaveOptions("0:0", defaultLeaves[1][0], 1, hoursInADay, ddlBulkLeaves, "")
            }
            else {
                ddlBulkLeaves.append(String.format("<option value='{0}'>{1}</option>", defaultLeaves[1][0], defaultLeaves[1][0]));
                ddlBulkLeaves.append(String.format("<option value='{0}'>{1}</option>", defaultLeaves[2][0], defaultLeaves[2][0]));
            }

            //add leave options       
            //Disable for other leave for now
            //            var listLength = list.length;
            //            for (var i = 0; i < listLength; i++) {
            //                if (list[i] != "") {
            //                    var leaveDetails = list[i].split(":"); ///1=AttendenceCellType.Leave : LeaveTypeId, Abbreviation
            //                    var leaveTypeId = leaveDetails[0];
            //                    var leaveAbbrevation = leaveDetails[1];
            //                    var isHourlyLeave = (hasHourlyLeave && leaves[leaveAbbrevation]['IsHalfDayAllowed'] == true);
            //                    var isCompensatoryLeave = leaves[leaveAbbrevation]['IsCompensatory'];
            //                    var isWorkdayLeave = leaves[leaveAbbrevation]['IsWorkDaysType'];
            //                 
            //                    var allowedLeave = isLeaveValidForEmployee(employeeId, leaveTypeId, leaveAbbrevation, rowId);
            //                    var leaveFullName = leaves[leaveAbbrevation]['FullName'];

            //                    // Skip bulk leave assignment for Compensator and Workday type of leaves
            //                    if (isCompensatoryLeave == true || isWorkdayLeave)
            //                        continue;

            //                    if (!isHourlyLeave && allowedLeave == "All") {
            //                        //add LeaveAbbr-HoursInADay, if the company has hourly leave
            //                        if (hasHourlyLeave) {
            //                            addHourlyLeaveOptions("1:" + leaveTypeId, leaveAbbrevation, hoursInADay, hoursInADay, ddlBulkLeaves, "")
            //                        }
            //                        else {
            //                            var htmlContent = String.format("<option value='{0}'>{1}</option>", "1:" + leaveTypeId, leaveAbbrevation);
            //                            ddlBulkLeaves.append(htmlContent);
            //                        }
            //                    }
            //                }
            //            }

            if (popupAssingBulkLeaves == null)
                popupAssingBulkLeaves = $('#popupAssingBulkLeaves');
            popupAssingBulkLeaves.lightbox_me({ centered: true });
        }


        var totalDays;
        //array holding the leaves subtraction, sample array creation from server side

        var currentTxt;
        var currentType;
        var currentTypeValue;
        var currentDeductDays;

        var ddlOption = '#<%= ddlOptions.ClientID %>';
        var ddlOptionReference = null;
        var ddlOptionDiv = '#ddlOptionsDiv';
        var ddlOptionDivReference = null;

        var radComboOptions = null;
        var radComboOptionsDiv = '#ctl00_mainContent_radComboOptions';
        var radComboContainer = '#radComboContainer';
        var isSavingValid = true;
        var savingInvalidMsg = "";
        var isOptionCreated = false;
        var empDetailsDiv = null;
        var empDetailsDivOnHover = null;

        var WorkDayResourceMsg = "<%= Resources.Messages.AttendaceWorkdayExceedingMsg %>";
        //to track if previous for which emp the options has been created
        var comboItems = null;


        var dropDownInitialise = false;

        $(document).ready(
            function () {

                try {

                    $('.tableLightColor tbody').attr('id', 'scrollMe');
                    var sth = new ScrollTableHeader();

                    sth.addTbody("scrollMe");
                    sth.delayAfterScroll = 150;
                    sth.minTableRows = 10;

                }
                catch (e) { }
                $('#loadingStartingImage').hide();
                $('#wrapperDiv').show();
            }
			);
			
    </script>
    <script src="../Scripts/attendanceScript.js?v=600" type="text/javascript"></script>
    <script src="../Scripts/ScrollTableHeader103.min.js?v=400" type="text/javascript"></script>
    <script src="../Scripts/jquery.lightbox_me.js?v=400" type="text/javascript"></script>
</asp:Content>
