﻿<%@ Page Title="Employee Award List" Language="C#" MasterPageFile="~/Master/NewDetails.Master"
    AutoEventWireup="true" CodeBehind="EmployeeAwardList.aspx.cs" Inherits="Web.CP.EmployeeAwardList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .hideLeftBlockCssInPage
        {
            margin: 0px !important;
            padding-left: 20px !important;
        }
        #menu
        {
            display: none;
        }
        #content
        {
            margin: inherit;
        }
        .tdClass
        {
            width: 210px;
            margin-bottom: 15px;
        }
    </style>
    <script type="text/javascript">
    
    var CommandHandler = function(command, record){
            <%= hdnCashAwardTypeId.ClientID %>.setValue(record.data.CashAwardTypeId);
            <%= hdnEmployeeId.ClientID %>.setValue(record.data.EmployeeId);
            <%= hdnMonth.ClientID %>.setValue(record.data.Month);
            <%= hdnYear.ClientID %>.setValue(record.data.Year);

                if(command=="Edit")
                {
                    <%= btnEdit.ClientID %>.fireEvent('click');
                }
                else
                {
                    <%= btnDelete.ClientID %>.fireEvent('click');
                }

    };

    function importAward(btn)
    {
        var ret = awardImportPopup("CashAwardTypeId=" + <%=cmbAwardImport.ClientID %>.value + "&CalulationTypeId=" + <%=cmbCalculationTypeImport.ClientID %>.value + "&Date="+ <%= txtDateImport.ClientID %>.value);
        return false;
    }

    function refreshWindow() {

            __doPostBack('Refresh', 0);

        }

    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <ext:Hidden ID="hdnCashAwardTypeId" runat="server" />
    <ext:Hidden ID="hdnEmployeeId" runat="server" />
    <ext:Hidden ID="hdnMonth" runat="server" />
    <ext:Hidden ID="hdnYear" runat="server" />
    <ext:LinkButton ID="btnEdit" runat="server" Hidden="true">
        <DirectEvents>
            <Click OnEvent="btnEdit_Click">
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:LinkButton>
    <ext:LinkButton ID="btnDelete" runat="server" Hidden="true">
        <DirectEvents>
            <Click OnEvent="btnDelete_Click">
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:LinkButton>
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Award List
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <div class="panel panel-default">
            <!-- panel-heading -->
            <div class="panel-body">
                <ext:GridPanel ID="gridAward" runat="server" Width="885" Cls="itemgrid">
                    <Store>
                        <ext:Store ID="Store1" runat="server">
                            <Model>
                                <ext:Model ID="Model4" runat="server" IDProperty="SN">
                                    <Fields>
                                        <ext:ModelField Name="SN" Type="String" />
                                        <ext:ModelField Name="CashAwardTypeId" Type="String" />
                                        <ext:ModelField Name="EmployeeId" Type="String" />
                                        <ext:ModelField Name="Month" Type="String" />
                                        <ext:ModelField Name="Year" Type="String" />
                                        <ext:ModelField Name="EmployeeName" Type="String" />
                                        <ext:ModelField Name="AwardName" Type="String" />
                                        <ext:ModelField Name="Description" Type="string" />
                                        <ext:ModelField Name="AmountOrRate" Type="Float" />
                                        <ext:ModelField Name="Date" Type="string" />
                                    </Fields>
                                </ext:Model>
                            </Model>
                        </ext:Store>
                    </Store>
                    <ColumnModel>
                        <Columns>
                            <ext:Column ID="colSN" Sortable="false" MenuDisabled="true" runat="server" Text="SN"
                                Align="Left" Width="60" DataIndex="SN" />
                            <ext:Column ID="colEmployeeName" Sortable="false" MenuDisabled="true" runat="server"
                                Text="Employee Name" Align="Left" Width="160" DataIndex="EmployeeName" />
                            <ext:Column ID="colAwardName" Sortable="false" MenuDisabled="true" runat="server"
                                Text="Award Name" Align="Left" Width="160" DataIndex="AwardName" />
                            <ext:Column ID="colDescription" Sortable="false" MenuDisabled="true" runat="server"
                                Text="Description" Align="Left" Width="230" DataIndex="Description" />
                            <ext:NumberColumn ID="colAmount" runat="server" Sortable="false" Align="Right" MenuDisabled="true"
                                Header="Amount" Width="100" DataIndex="AmountOrRate">
                            </ext:NumberColumn>
                            <ext:Column ID="colDate" Sortable="false" MenuDisabled="true" runat="server" Text="Description"
                                Align="Left" Width="100" DataIndex="Date" />
                            <ext:CommandColumn ID="CommandColumn1" runat="server" Width="75" Text="Actions" Align="Center">
                                <Commands>
                                    <ext:CommandSeparator />
                                    <ext:GridCommand Cls="editGridButton" ToolTip-Text="Edit" Text="<i class='fa fa-pencil'></i>"
                                        CommandName="Edit" />
                                    <ext:GridCommand Cls="editGridButton" ToolTip-Text="Delete" Text="<i class='fa fa-trash-o'></i>"
                                        CommandName="Delete" />
                                </Commands>
                                <Listeners>
                                    <Command Handler="CommandHandler(command,record);" />
                                </Listeners>
                            </ext:CommandColumn>
                        </Columns>
                    </ColumnModel>
                    <SelectionModel>
                        <ext:RowSelectionModel ID="RowSelectionModel2" runat="server" Mode="Single" />
                    </SelectionModel>
                </ext:GridPanel>
                <br />
                <table>
                    <tr>
                        <td style="width: 800px;">
                            <ext:Button runat="server" Cls="btn btn-primary" ID="btnAddNew" Text="<i></i>Add New">
                                <DirectEvents>
                                    <Click OnEvent="btnAddNew_Click">
                                        <EventMask ShowMask="true" />
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                        </td>
                        <td>
                            <ext:Button runat="server" Cls="btn btn-success" ID="btnImport" Text="<i></i>Import">
                                <DirectEvents>
                                    <Click OnEvent="btnImport_Click">
                                        <EventMask ShowMask="true" />
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <ext:Store runat="server" ID="storeSearch" AutoLoad="false">
        <Proxy>
            <ext:AjaxProxy Url="../Handler/EmpSearchID.ashx">
                <ActionMethods Read="GET" />
                <Reader>
                    <ext:JsonReader Root="plants" TotalProperty="total" />
                </Reader>
            </ext:AjaxProxy>
        </Proxy>
        <Model>
            <ext:Model ID="Model2" IDProperty="Name" Name="ItemLineModel" runat="server">
                <Fields>
                    <ext:ModelField Name="Name" Type="String" />
                    <ext:ModelField Name="EmployeeId" Type="String" />
                </Fields>
            </ext:Model>
        </Model>
    </ext:Store>
    <ext:Window ID="WEmployeeAward" runat="server" Title="Add/Edit Employee Award" Icon="Application"
        Width="600" Height="480" BodyPadding="5" Hidden="true" Modal="true">
        <Content>
            <table class="fieldTable">
                <tr>
                    <td class="tdClass" colspan="2">
                        <ext:ComboBox LabelSeparator="" ID="cmbSearch" FieldLabel="Search Employee *" Width="250"
                            LabelAlign="Top" runat="server" DisplayField="Name" ValueField="EmployeeId" StoreID="storeSearch"
                            TypeAhead="false" PageSize="9999" HideBaseTrigger="true" MinChars="1" TriggerAction="All"
                            ForceSelection="true">
                            <ListConfig LoadingText="Searching..." StyleSpec="border-top:1px solid #98c0f4;">
                                <ItemTpl ID="ItemTpl1" runat="server">
                                    <Html>
                                        <tpl>
                                            <div class="search-item">
                                                            <span>{Name}</span>  
                                             </div>
					                </tpl>
                                    </Html>
                                </ItemTpl>
                            </ListConfig>
                        </ext:ComboBox>
                        <ext:TextField FieldLabel="Employee Name" Width="180px" ID="txtEmployeeName" Hidden="true"
                            runat="server" LabelSeparator="" Disabled="true" LabelAlign="Top" />
                    </td>
                </tr>
                <tr>
                    <td class="tdClass">
                        <ext:ComboBox FieldLabel="Award Name *" ID="cmbAwardName" Width="180" runat="server"
                            ValueField="CashAwardTypeId" DisplayField="Name" LabelAlign="Top" LabelSeparator=""
                            ForceSelection="true" QueryMode="Local">
                            <Store>
                                <ext:Store ID="Store2" runat="server">
                                    <Model>
                                        <ext:Model ID="Model1" IDProperty="CashAwardTypeId" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="CashAwardTypeId" Type="String" />
                                                <ext:ModelField Name="Name" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                        </ext:ComboBox>
                        <asp:RequiredFieldValidator Display="None" ID="rfvAwardName" runat="server" ValidationGroup="SaveEmpAward"
                            ControlToValidate="cmbAwardName" ErrorMessage="Award Name is required." />
                    </td>
                </tr>
                <tr>
                    <td class="tdClass">
                        <ext:ComboBox FieldLabel="Award Calculation *" ID="cmbAwardCalcaulation" Width="180"
                            runat="server" ValueField="CashAwardTypeId" DisplayField="Name" LabelAlign="Top"
                            LabelSeparator="" ForceSelection="true" QueryMode="Local">
                            <Items>
                                <ext:ListItem Text="Months Basic Salary" Value="1" />
                                <ext:ListItem Text="Percentage of Basic Salary" Value="2" />
                                <ext:ListItem Text="Fixed Amount" Value="3" />
                            </Items>
                            <DirectEvents>
                                <Change OnEvent="cmbAwardCalcaulation_Change" />
                            </DirectEvents>
                        </ext:ComboBox>
                        <asp:RequiredFieldValidator Display="None" ID="rfvAwardCalculation" runat="server"
                            ValidationGroup="SaveEmpAward" ControlToValidate="cmbAwardCalcaulation" ErrorMessage="Award Calculation is required." />
                    </td>
                    <td class="tdClass">
                        <ext:TextField FieldLabel="Months Basic Salary *" Width="180px" ID="txtMonthsBasicSalary"
                            MaskRe="/[0-9.]/" runat="server" LabelSeparator="" Hidden="true" LabelAlign="Top" />
                        <ext:TextField FieldLabel="Percentage of Basic Salary *" Width="180px" ID="txtPercentageOfBasicSalary"
                            MaskRe="/[0-9.]/" runat="server" LabelSeparator="" Hidden="true" LabelAlign="Top" />
                        <ext:TextField FieldLabel="Fixed Amount *" Width="180px" ID="txtFixedAmount" MaskRe="/[0-9.]/"
                            runat="server" LabelSeparator="" Hidden="true" LabelAlign="Top" />
                    </td>
                    <td style="width: 50px; vertical-align: bottom;">
                        <ext:Label ID="lblMonths" runat="server" Text="Months" Hidden="true" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <pr:CalendarExtControl FieldLabel="Show in Pay Months *" Width="180px" ID="txtPayMonths"
                            runat="server" LabelSeparator="" LabelAlign="Top" />
                        <asp:RequiredFieldValidator Display="None" ID="rfvPayMonths" runat="server" ValidationGroup="SaveEmpAward"
                            ControlToValidate="txtPayMonths" ErrorMessage="Award Name is required." />
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <ext:TextArea ID="txtNote" runat="server" FieldLabel="Note" LabelSeparator="" LabelAlign="Top"
                            Width="390" Height="100" />
                    </td>
                </tr>
                <tr>
                    <td valign="bottom">
                        <div class="popupButtonDiv">
                            <ext:Button runat="server" ID="btnSave" Cls="btn btn-primary" Text="Save">
                                <DirectEvents>
                                    <Click OnEvent="btnSave_Click">
                                        <EventMask ShowMask="true" />
                                    </Click>
                                </DirectEvents>
                                <Listeners>
                                    <Click Handler="valGroup = 'SaveEmpAward'; if(CheckValidation()) return this.disable(); else return false;">
                                    </Click>
                                </Listeners>
                            </ext:Button>
                            <div class="btnFlatOr">
                                or</div>
                            <ext:LinkButton runat="server" Cls="btnFlatLeftGap" ID="LinkButton1" Text="<i></i>Cancel">
                                <Listeners>
                                    <Click Handler="#{WEmployeeAward}.hide();">
                                    </Click>
                                </Listeners>
                            </ext:LinkButton>
                        </div>
                    </td>
                </tr>
            </table>
        </Content>
    </ext:Window>
    <ext:Window ID="WAwardImport" runat="server" Title="Import Employee Award" Icon="Application"
        Width="480" Height="250" BodyPadding="5" Hidden="true" Modal="true">
        <Content>
            <table class="fieldTable">
                <tr>
                    <td class="tdClass">
                        <ext:ComboBox FieldLabel="Award Name" ID="cmbAwardImport" Width="180" runat="server"
                            ValueField="CashAwardTypeId" DisplayField="Name" LabelAlign="Top" LabelSeparator=""
                            ForceSelection="true" QueryMode="Local">
                            <Store>
                                <ext:Store ID="Store3" runat="server">
                                    <Model>
                                        <ext:Model ID="Model3" IDProperty="CashAwardTypeId" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="CashAwardTypeId" Type="String" />
                                                <ext:ModelField Name="Name" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                        </ext:ComboBox>
                        <asp:RequiredFieldValidator Display="None" ID="rfvAwardNameImport" runat="server"
                            ValidationGroup="ImportAward" ControlToValidate="cmbAwardImport" ErrorMessage="Award Name is required." />
                    </td>
                    <td class="tdClass">
                        <ext:ComboBox FieldLabel="Award Calculation" ID="cmbCalculationTypeImport" Width="180"
                            runat="server" ValueField="CashAwardTypeId" DisplayField="Name" LabelAlign="Top"
                            LabelSeparator="" ForceSelection="true" QueryMode="Local">
                            <Items>
                                <ext:ListItem Text="Months Basic Salary" Value="1" />
                                <ext:ListItem Text="Percentage of Basic Salary" Value="2" />
                                <ext:ListItem Text="Fixed Amount" Value="3" />
                            </Items>
                        </ext:ComboBox>
                        <asp:RequiredFieldValidator Display="None" ID="rfvCalculationImport" runat="server"
                            ValidationGroup="ImportAward" ControlToValidate="cmbCalculationTypeImport" ErrorMessage="Award Calculation is required." />
                    </td>
                </tr>
                <tr>
                    <td class="tdClass">
                        <pr:CalendarExtControl FieldLabel="Date" Width="180px" ID="txtDateImport" runat="server"
                            LabelSeparator="" LabelAlign="Top" />
                        <asp:RequiredFieldValidator Display="None" ID="rfvDateImport" runat="server" ValidationGroup="ImportAward"
                            ControlToValidate="txtDateImport" ErrorMessage="Date is required." />
                    </td>
                </tr>
            </table>
            <div style="margin-left: 255px;">
                <%-- <asp:LinkButton ID="btnExport" runat="server" Text="Import from Excel" OnClientClick="valGroup = 'ImportAward'; if(CheckValidation()) return importAward(this); else return false;"
                                CssClass=" excel marginRight tiptip" Style="float: left;" />--%>
                <ext:Button runat="server" Cls="btn btn-success" ID="btnExport" Text="Import from Excel"
                    OnClientClick="valGroup = 'ImportAward'; if(CheckValidation()) return importAward(this); else return false;">
                </ext:Button>
            </div>
        </Content>
    </ext:Window>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
