﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using Utils.Helper;
using Utils.Web;
using BLL.Base;
using DAL;
using Utils.Calendar;
using BLL.BO;

namespace Web.CP
{
    public partial class CITRequestList : BasePage
    {


        //private int _tempCurrentPage;
       // private int _tempCount;
        private PayManager payMgr = new PayManager();

        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (!IsPostBack)
            {
                Initialise();
            }

            JavascriptHelper.AttachNonDialogPopUpCode(Page, "positionHistoryPopup", "AAForwardOvertimePopup.aspx", 525, 200);
        
        }



       


        protected void btnApprove_Click(object sender, EventArgs e)
        {

            string statusText = "Approved";
            int status = (int)CITRequestStatus.Approved;

            if (sender == btnReject)
            {
                status = (int)CITRequestStatus.Rejected;
                statusText = "Rejected";
            }

            bool employeeAdded = false;
            List<CITChangeRequest> requestList = new List<CITChangeRequest>();
            foreach (GridViewRow row in gvw.Rows)
            {


                CITChangeRequest request = new CITChangeRequest();
                int RequestID = int.Parse(gvw.DataKeys[row.RowIndex]["RequestId"].ToString());


                if (row.FindControl("chkDelete") == null)
                    continue;

                CheckBox chkDelete = (CheckBox)row.FindControl("chkDelete");

                if (chkDelete.Checked)
                {



                    request.RequestId = RequestID;
                    requestList.Add(request);
                }
            }

            if (EmployeeManager.ForwardRequestInBulk(requestList,status))
            {
                msgInfo.InnerHtml = "CIT request " + statusText + " for " + requestList.Count + " employees.";
                msgInfo.Hide = false;

                LoadEmployees();
            }
            
        }



        void Initialise()
        {
            LoadEmployees();

        }
        protected void btnLoad_Click(object sender, EventArgs e)
        {
        
            LoadEmployees();
        }
        protected void gvwEmployees_RowCreated(object sender, GridViewRowEventArgs e)
        {

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover",
                "this.originalClass=this.className;this.className='selected'");

                e.Row.Attributes.Add("onmouseout",
                "this.className=this.originalClass;");


            }

        }
        protected void LoadEmployees()
        {
            int type= 1;
           

            if( ddlType.SelectedItem != null)
                type = int.Parse(ddlType.SelectedValue);

            gvw.DataSource = EmployeeManager.GetUnapprovedCITChangeRequest(type,txtEmpSearchText.Text.Trim(),chkShowHistory.Checked);
            gvw.DataBind();
        }

        public bool IsVisible(object status)
        {
            if (int.Parse(status.ToString()) == (int)CITRequestStatus.SaveAndSend)
                return true;

            return false;

        }
        public string GetEngOrNepDate(object value)
        {
            if (value == null)
                return "";

            DateTime date = Convert.ToDateTime(value);

            CustomDate c = new CustomDate(date.Day, date.Month, date.Year, true);

            if (IsEnglish)
                return c.ToString();

            else
                return CustomDate.ConvertEngToNep(c).ToString();
        }

       
        protected void btnExport_Click(object sender, EventArgs e)
        {
            GridViewExportUtil.Export("CIT Requests.xls", gvw);
        }

        //protected void btnPostToSalary_Click(object sender, EventArgs e)
        //{

        //    foreach (GridViewRow row in gvw.Rows)
        //    {


        //        int payrollPeriodId = (int)gvw.DataKeys[row.RowIndex]["PayrollPeriodId"];

        //        PayManager.PostOvertimePay(payrollPeriodId);
        //        msgCtl.InnerHtml = Resources.Messages.OvertimePostedMessage;
        //        msgCtl.Hide = false;
        //        break;
        //    }


        //}
        protected void txtEmployeeName_TextChanged(object sender, EventArgs e)
        {
            

        }

      
       

    }
}
