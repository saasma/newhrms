﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using BLL.BO;
using System.IO;
using Utils.Calendar;
using Utils.Helper;

namespace Web.NewHR
{
    public partial class LeaveFareDetails : BasePage
    {
        
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!X.IsAjaxRequest && !IsPostBack)
            {
                Initialise();

              

            }
        }

        public void Initialise()
        {
            PayrollPeriod period = CommonManager.GetLastPayrollPeriod();

            //calDate.Text = CustomDate.GetTodayDate(IsEnglish).ToString();
            cmbYear.Store[0].DataSource = CommonManager.GetYearList();
            cmbYear.Store[0].DataBind();

            ExtControlHelper.ComboBoxSetSelected(period.Year.ToString(), cmbYear);
            ExtControlHelper.ComboBoxSetSelected(period.Month.ToString(), cmbMonth);

            cmbMonth.Store[0].DataSource = DateManager.GetCurrentMonthList();
            cmbMonth.Store[0].DataBind();
        }


        protected void Store_ReadData(object sender, StoreReadDataEventArgs e)
        {
           

            int year = int.Parse(cmbYear.SelectedItem.Value);
            int month = int.Parse(cmbMonth.SelectedItem.Value);

            DateTime startDate = DateTime.Now, endDate = DateTime.Now;

            PayrollPeriod period = CommonManager.GetPayrollPeriod(month, year);


            if (period == null)
            {
                // 
                if (IsEnglish == false)
                {
                    CustomDate start = new CustomDate(1, month, year, false);
                    CustomDate end = new CustomDate(DateHelper.GetTotalDaysInTheMonth(year, month, false), month, year, false);
                    startDate = start.EnglishDate;
                    endDate = end.EnglishDate;
                }
                else
                {

                }
                //NewMessage.ShowWarningMessage("Period does not exists for this date.");
            }
            else
            {
                startDate = period.StartDateEng.Value;
                endDate = period.EndDateEng.Value;
            }


            title.Html = string.Format("<h4> Leave Fare Details for {0} to {1}, {2}</h4>", startDate.ToString("dd MMM yyyy")
                , endDate.ToString("dd MMM yyyy"), period == null? "" : period.Name);

            int totalRecords = 0;


            bool isApprovedOnly = tabPanel.ActiveTabIndex != 0;

            hdnPeriodId.Text = (period == null ? "0" : period.PayrollPeriodId.ToString());
            hdnIsApprovedOnly.Text = isApprovedOnly.ToString();

            if (period != null && period.PayrollPeriodId == CommonManager.GetLastPayrollPeriod().PayrollPeriodId
                && isApprovedOnly)
            {
                btnPost.Show();
            }
            else
            {
                btnPost.Hide();
            }

            int paymentStatus = int.Parse(cmbStatus.SelectedItem.Value);

            int employeeId = -1;

            int.TryParse(cmbSearch.SelectedItem.Value, out employeeId);

            if (employeeId == 0)
                employeeId = -1;



            hdnSortBy.Text = (e.Sort[0].Property + " " + e.Sort[0].Direction).ToLower();

            List<GetLeaveFareEmployeeListResult> list = EmployeeManager.GetLeaveFareDetails
                ((period == null ? 0  : period.PayrollPeriodId),startDate,endDate, employeeId, e.Page - 1, e.Limit, (e.Sort[0].Property + " " + e.Sort[0].Direction).ToLower(), ref totalRecords
                , isApprovedOnly, paymentStatus);


            e.Total = totalRecords;

            //if (list.Any())
            gridList.Store[0].DataSource = list;
            gridList.Store[0].DataBind();

        }


        public void btnExport_Click(object sender, EventArgs e)
        {
            int year = int.Parse(cmbYear.SelectedItem.Value);
            int month = int.Parse(cmbMonth.SelectedItem.Value);

            PayrollPeriod period = CommonManager.GetPayrollPeriod(month, year);

            DateTime startDate = DateTime.Now, endDate = DateTime.Now;

            if (period == null)
            {
                // 
                if (IsEnglish==false)
                {
                    CustomDate start = new CustomDate(1, month, year, false);
                    CustomDate end = new CustomDate(DateHelper.GetTotalDaysInTheMonth(year, month, false), month, year, false);
                    startDate = start.EnglishDate;
                    endDate = end.EnglishDate;
                }
                else
                {
                    CustomDate start = new CustomDate(1, month, year, true);
                    CustomDate end = new CustomDate(DateHelper.GetTotalDaysInTheMonth(year, month, true), month, year, true);
                    startDate = start.EnglishDate;
                    endDate = end.EnglishDate;
                }
                //NewMessage.ShowWarningMessage("Period does not exists for this date.");
            }
            else
            {
                startDate = period.StartDateEng.Value;
                endDate = period.EndDateEng.Value;
            }

            //if (period == null)
            //{
            //    Bll.ExcelHelper.ExportToExcel("Leave fare details", new List<GetLeaveFareEmployeeListResult>(),
            //       new List<string> { },
            //       new List<String>() { },
            //       new Dictionary<string, string>() { },
            //       new List<string>() { }
            //       , new List<string> { }
            //       , new List<string> { }
            //       , new Dictionary<string, string>() { { "Leave fare details ", "" } }
            //       , new List<string> { });

            //            return;
            //}



            int totalRecords = 0;


            bool isApprovedOnly = tabPanel.ActiveTabIndex != 0;

            

            int paymentStatus = int.Parse(cmbStatus.SelectedItem.Value);

            int employeeId = -1;

            int.TryParse(cmbSearch.SelectedItem.Value, out employeeId);

            if (employeeId == 0)
                employeeId = -1;



            

            List<GetLeaveFareEmployeeListResult> list = EmployeeManager.GetLeaveFareDetails
                ((period == null? 0 : period.PayrollPeriodId),startDate,endDate, employeeId, 0,99999, (hdnSortBy.Text).ToLower(), ref totalRecords
                , isApprovedOnly, paymentStatus);


            string title = string.Format("<h4> Leave Fare Details for {0} to {1}, {2}</h4>", startDate.ToString("dd MMM yyyy")
                , endDate.ToString("dd MMM yyyy"), period == null ? "" : period.Name); ;

            Bll.ExcelHelper.ExportToExcel("Leave fare details", list,
                new List<string> { "TotalRows", "LeaveStatusName", "LeaveRequestId", "ServiceStatusId", "DesignationId", "BranchId", "DepartmentId" },
            new List<String>() { },
            new Dictionary<string, string>() { { "EmployeeId", "EIN" }, { "IsFull", "Eligibility" },{"PaidAddOnAmount","Paid Amount"}},
            new List<string>() { "Amount","PaidAddOnAmount" }
            , new List<string> { "SN","EmployeeId"}
            , new List<string> { "StartDate","EndDate" }
            , new Dictionary<string, string>() { { "Leave fare details ", title } }
            , new List<string> { "SN", "EmployeeId", "Name", "Designation", "Branch", "Department", "ServiceStatus", 
                "LeaveStatus", "StartDate","EndDate","TakenDays","IsFull","Amount","PaidAddOnAmount","PaymentStatus" }); 


        }




        protected void btnPost_Click(object sender, DirectEventArgs e)
        {


            string gridItemsJson = e.ExtraParams["gridItems"];
            List<LeaveFareBO> list = JSON.Deserialize<List<LeaveFareBO>>(gridItemsJson);
            //List<OvertimePeriodDetail> _OvertimePeriodDetail = new List<OvertimePeriodDetail>();
            if (list.Count == 0)
            {
                NewMessage.ShowWarningMessage("Select Leave fares for posting.");
                return;
            }



            Status status = OvertimeManager.SaveUpdateLeaveFareDetails(list,int.Parse(hdnPeriodId.Text));
            if (status.IsSuccess)
            {
                CheckboxSelectionModel1.ClearSelection();
                NewMessage.ShowNormalMessage("Leave fares posted successfully.");
                PagingToolbar1.DoRefresh();
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }

        }
        
        protected void btnApproveByLine_Click(object sender, DirectEventArgs e)
        {
           
        }

        protected void btnReject_Click(object sender, DirectEventArgs e)
        {
            
        }
    }
}
