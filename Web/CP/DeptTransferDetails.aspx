﻿<%@ Page Title="Department Transfer Details" Language="C#" MasterPageFile="~/Master/ForPopupPage.Master"
    AutoEventWireup="true" CodeBehind="DeptTransferDetails.aspx.cs" Inherits="Web.CP.DeptTransferDetails" %>

<%@ Register Src="~/Controls/ContentHeader.ascx" TagName="ContentHeader" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .empName
        {
            font-size: 20px;
            color: #439EC6;
            font-weight: bold;
        }
        .titleDesign
        {
            font-size: 15px;
            color: #000000;
            text-decoration: underline;
            padding-top: 10px;
        }
        
        .items span
        {
            display: block;
            padding-top: 3px;
            font-weight: bold;
        }
        .paddinAll
        {
            padding: 10px;
        }
        <%--table tr td
        {
            padding: 0 10px 8px 0;
        }--%>
        h3
        {
            font: bold 13px Arial;
        }
        .marginTopss
        {
            margin-top: 10px;
            display: block;
        }
        .loanAdvTable
        {
            background-color: white;
            width: 60px;
            border: 1px solid white !important;
            padding: 0px;
        }
        .tableLightColor th
        {
            text-align: left;
        }
        strong
        {
            display: block;
            padding-bottom: 3px;
        }
        .tableLightColor input[type=text]
        {
            height: 20px;
        }
        .clsColor
        {
            background-color: #e8f1ff;
        }
    </style>
    <script type="text/javascript">
        function popupUpdateLeaveCall(LeaveId) {
            var ret = popup('Id=' + LeaveId);

            return false;
        }

        function closePopup() {
            window.opener.reloadDeptTransfer(window);
        }

        function ACE_item_selected(source, eventArgs) {
            var val = eventArgs.get_value();
            if (val != null) {

                var hdnEmpId = document.getElementById('<%= hdnEmployeeId.ClientID %>');
                var btnLoadEmpDetls = document.getElementById('<%= btnLoadEmpDetls.ClientID %>');
                hdnEmpId.value = val;
                btnLoadEmpDetls.click();

            } else {
                alert("First select the employee from the list.");
            }
        }

    </script>
    <link href="../css/core.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="mainContent" runat="server">
    <div style="display: none;">
        <asp:Button ID="btnLoadEmpDetls" OnClick="btnLoadEmpDetls_Click" Text="Load" runat="server" />
    </div>
    <uc1:ContentHeader Id="ContentHeader1" runat="server" />
    <div class="clsColor" style="height: 590px;">
        <div class="popupHeader">
            <h3>
                Department Transfer Details</h3>
        </div>
        <asp:HiddenField ID="hdnEmployeeId" runat="server" />
        <asp:HiddenField ID="hdnMessage" runat="server" />
        <div class=" marginal" style="margin: 5px 20px 0 10px;">
            <uc2:MsgCtl ID="divMsgCtl" Width="900px" EnableViewState="false" Hide="true" runat="server" />
            <uc2:WarningCtl ID="divWarningMsg" Width="900px" EnableViewState="false" Hide="true"
                runat="server" />
            <table cellpadding="4px" style="margin-left: 10px;">
                <tr>
                    <td>
                        Search Employee *
                    </td>
                    <td>
                        <asp:TextBox ID="txtEmpSearchText" Width="170px" Style='width: 200px; border-radius: 2px;
                            margin-top: 4px; margin-left: 10px; height: 20px;' runat="server"></asp:TextBox>
                        <cc1:TextBoxWatermarkExtender ID="TBWE2" runat="server" TargetControlID="txtEmpSearchText"
                            WatermarkText="Employee Search" WatermarkCssClass="searchBoxText" />
                        <cc1:AutoCompleteExtender EnableCaching="true" ID="AutoCompleteExtenderOrganization"
                            runat="server" MinimumPrefixLength="2" ServiceMethod="GetEmployeeNamesWithID"
                            ServicePath="~/PayrollService.asmx" TargetControlID="txtEmpSearchText" OnClientItemSelected="ACE_item_selected"
                            CompletionSetCount="10" CompletionInterval="250" CompletionListCssClass="AutoExtender"
                            CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight">
                        </cc1:AutoCompleteExtender>
                        <asp:RequiredFieldValidator ValidationGroup="AEEmployee" ID="RequiredFieldValidator2"
                            ControlToValidate="txtEmpSearchText" Display="None" runat="server" ErrorMessage="Please select an Employee."></asp:RequiredFieldValidator>
                    </td>
                </tr>
            </table>
            <div id="block" runat="server">
                <table style='clear: both; padding: 10px; margin-top: 10px;'>
                    <tr>
                        <td valign="top" style="padding-left: 10px;">
                            <asp:Image ID="image" runat="server" ImageUrl="~/images/sample.jpg" Width="120px"
                                Height="120px" />
                        </td>
                        <td valign="top" style="padding-left: 15px" class="items">
                            <asp:Label ID="lblName" CssClass="empName" runat="server" />
                            <asp:Label ID="lblWorkingIn" runat="server" Text=""  />
                            <asp:Label ID="lblBranch" runat="server" />
                            <asp:Label ID="lblDepartment" runat="server" />
                            <asp:Label ID="lblSince" runat="server" />
                            <asp:Label ID="lblTime" runat="server" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div class=" marginal" style="margin: 5px 20px 0 10px;">
            <table cellpadding="4px" style="margin-left: 10px;">
                <tr>
                    <td>
                        Event Type
                    </td>
                    <td>
                        <asp:DropDownList Width="138px" AppendDataBoundItems="true" ID="ddlEventType" runat="server" DataValueField="EventID"
                            DataTextField="Name">
                            <asp:ListItem Value="-1" Text=""></asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="reqdEvenType" runat="server" ControlToValidate="ddlEventType"
                            Display="None" ErrorMessage="Event type is required." InitialValue="-1" ValidationGroup="AEEmployee"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="fieldHeader" style="width: 100px;">
                        <My:Label ID="lblLetterNo" runat="server" Text="Letter Number" />
                    </td>
                    <td class="fieldHeader" style="width: 160px;">
                        <asp:TextBox ID="txtLetterNumber" Width="138px" runat="server" />
                    </td>
                    <td class="fieldHeader" style="width: 100px;">
                        <My:Label ID="lblLetterDate" runat="server" Text="Letter Date" ShowAstrick="true" />
                    </td>
                    <td class="fieldHeader" style="width: 160px;">
                        <pr:CalendarExtControl Width="150px" ID="calLetterDate" runat="server" LabelSeparator="" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="calLetterDate"
                            Display="None" ErrorMessage="Letter date is required." ValidationGroup="AEEmployee"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="fieldHeader">
                        <My:Label ID="lblDept" runat="server" Text="Department" ShowAstrick="true" />
                    </td>
                    <td class="fieldHeader">
                        <asp:DropDownList Width="138px" ID="ddlTransferToDepartment" runat="server" DataValueField="DepartmentId"
                            DataTextField="Name">
                            <asp:ListItem Value="-1" Text=""></asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="ddlTransferToDepartment"
                            Display="None" ErrorMessage="Department is required." InitialValue="-1" ValidationGroup="AEEmployee"></asp:RequiredFieldValidator>
                    </td>
                    <td class="fieldHeader">
                        <My:Label ID="Label7" runat="server" Text="Sub Department" />
                    </td>
                    <td class="fieldHeader">
                        <asp:DropDownList Width="138px" ID="ddlSubDepartment" runat="server" DataValueField="SubDepartmentId"
                            DataTextField="Name">
                            <asp:ListItem Value="-1" Text=""></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td class="fieldHeader">
                        <My:Label ID="lblResp" runat="server" Text="Special Responsibility" ShowAstrick="true" />
                    </td>
                    <td class="fieldHeader">
                        <asp:TextBox ID="txtSpecialResp" Width="138px" runat="server" />
                    </td>
                    <td class="fieldHeader">
                        <My:Label ID="lblApplDate" runat="server" Text="Applicable Date" ShowAstrick="true" />
                    </td>
                    <td class="fieldHeader">
                        <pr:CalendarExtControl Width="150px" ID="calFromDate" runat="server" LabelSeparator="" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="calFromDate"
                            Display="None" ErrorMessage="Applicable date is required." ValidationGroup="AEEmployee"></asp:RequiredFieldValidator>
                    </td>
                    <tr>
                        <td class="fieldHeader" valign="top">
                            <My:Label ID="Label11" runat="server" Text="Note" ShowAstrick="true" />
                        </td>
                        <td class="fieldHeader" colspan="3">
                            <asp:TextBox ID="txtNote" TextMode="MultiLine" runat="server" Width="457" Height="50" />
                        </td>
                    </tr>
            </table>
        </div>
        <br />
        <div style='padding-left: 12px; text-align: right; padding-right: 22px' id="buttonsBar"
            runat="server">
            &nbsp; &nbsp;
            <asp:Button ID="btnDelete" CssClass="cancel" CausesValidation="false" runat="server"
                Text="Delete" OnClick="btnDelete_Click" Style='display: none' />
            <asp:Button ID="btnSave" CssClass="save" runat="server" Text="Save" OnClientClick="valGroup='AEEmployee';return CheckValidation()"
                OnClick="btnSave_Click" />
            <asp:Button ID="btnCancel" CssClass="cancel" CausesValidation="false" runat="server"
                Text="Cancel" OnClick="btnCancel_Click" />
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
