<%@ Page MaintainScrollPositionOnPostback="true" Title="Notice List" Language="C#"
    MasterPageFile="~/Master/NewDetails.Master" AutoEventWireup="true" CodeBehind="NoticeList.aspx.cs"
    Inherits="Web.CP.NoticeList" %>

<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/ExtMsgCtl.ascx" TagName="ExtMsgCtl" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../Scripts/jquery-1.3.2.min.js" type="text/javascript"></script>
    <script type="text/javascript">

        var cboBranch = null;
        
        var store;

        Ext.onReady(
            function () {
                
            }
        );


        var CommandHandler = function (command, record) {
             <%= HiddenRowID.ClientID %>.setValue(record.data.NoticeID);

             var pub = record.data.StatusStr;

            if (command == "Publish") {
                if(pub=="Published")
                {
                    <%= btnUnpublish.ClientID %>.fireEvent('click');
                }
                else
                {
                    <%= btnChangeStatus.ClientID %>.fireEvent('click');
                }
            }
            else if (command == "Delete") {
                <%= btnDelete.ClientID %>.fireEvent('click');
            }
            else {
                <%= btnEdit.ClientID %>.fireEvent('click');
            }
        };

     
       
        

    </script>
    <style type="text/css">
        .dirty-row
        {
            background: #FFFDD8;
        }
        
        .new-row
        {
            background: #c8ffc8;
        }
        .divDueDate
        {
            background: silver;
        }
        .x-panel-header
        {
            background-image: none;
        }
        .x-panel-header-text-container-default
        {
            color: White !important;
        }
        .fieldTable td, table tr.up td
        {
            padding-top: 0px;
        }
        .x-toolbar
        {
            padding: inherit;
        }
        #ctl00_mainContent_txtBody-inputCmp-iframeEl
        {
            border: 1px solid lightgray;
        }
        .btnFlatNew:hover
        {
            color: White;
        }
        .btnFlatNew
        {
            background: #3498db;
            background-image: -webkit-linear-gradient(top, #3498db, #2980b9);
            background-image: -moz-linear-gradient(top, #3498db, #2980b9);
            background-image: -ms-linear-gradient(top, #3498db, #2980b9);
            background-image: -o-linear-gradient(top, #3498db, #2980b9);
            background-image: linear-gradient(to bottom, #3498db, #2980b9);
            font-family: Arial;
            color: #ffffff;
            font-size: 14px;
            padding: 7px 11px 7px 11px;
            text-decoration: none;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <ext:Hidden ID="HiddenRowID" runat="server" />
    <ext:LinkButton ID="btnChangeStatus" runat="server" Hidden="true">
        <DirectEvents>
            <Click OnEvent="btnChangeStatus_Click">
                <Confirmation ConfirmRequest="true" Message="Are you sure you want to Publish this Notice?" />
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:LinkButton>
    <ext:LinkButton ID="btnUnpublish" runat="server" Hidden="true">
        <DirectEvents>
            <Click OnEvent="btnChangeStatus_Click">
                <Confirmation ConfirmRequest="true" Message="Are you sure you want to Unpublish this Notice?" />
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:LinkButton>
    <ext:LinkButton ID="btnEdit" runat="server" Hidden="true">
        <DirectEvents>
            <Click OnEvent="btnEdit_Click">
            </Click>
        </DirectEvents>
    </ext:LinkButton>
    <ext:LinkButton ID="btnDelete" runat="server" Hidden="true">
        <DirectEvents>
            <Click OnEvent="btnDelete_Click">
                <Confirmation Message="Are you sure your want to Delete this Notice?" />
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:LinkButton>
   <%-- <ext:ResourceManager runat="server" ScriptMode="Debug" DisableViewState="false" ShowWarningOnAjaxFailure="false"
        Namespace="Rigo">
    </ext:ResourceManager>--%>
    <script type="text/javascript">
        Ext.onReady(
                function () {
                }
        );
    </script>

<div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Notices
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <ext:Hidden ID="hdEmployees" runat="server" />
        
        <table style="clear: both">
            <tr>
                <%--<td >
                    <ext:LinkButton ID="btnPublished" runat="server" Text="Published" style="margin:25px;">
                        <DirectEvents>
                            <Click OnEvent="btnStatus_Click">
                            </Click>
                        </DirectEvents>
                    </ext:LinkButton>
                </td>
                <td>
                    <ext:LinkButton ID="btnUnPublished" runat="server" Text="Unpublished" style="margin:25px;">
                        <DirectEvents>
                            <Click OnEvent="btnStatus_Click">
                            </Click>
                        </DirectEvents>
                    </ext:LinkButton>
                </td>
                <td>
                    <ext:LinkButton ID="btnDraft" runat="server" Text="Draft" style="margin:25px;">
                        <DirectEvents>
                            <Click OnEvent="btnStatus_Click">
                            </Click>
                        </DirectEvents>
                    </ext:LinkButton>
                </td>--%>
                <td align="right">
                    <a href="NoticeCompose.aspx" class="btn btn-success btn-sm" style="margin: 0px;">
                        Compose New Notice</a>
                </td>
            </tr>
        </table>
        <div style="width: 100%; margin-top: 25px;">
            <ext:TabPanel runat="server" Width="1200" MinHeight="500">
                <Items>
                    <ext:Panel runat="server" Title="Published">
                        <Content>
                            <ext:GridPanel ID="gridNoticeList" runat="server" Width="1200">
                                <Store>
                                    <ext:Store runat="server" ID="storeNoticeList">
                                        <Model>
                                            <ext:Model>
                                                <Fields>
                                                    <ext:ModelField Name="NoticeID" />
                                                    <ext:ModelField Name="Title" />
                                                    <ext:ModelField Name="PublishDate" Type="Date" />
                                                    <ext:ModelField Name="ExpiryDate" Type="Date" />
                                                    <ext:ModelField Name="FileName" />
                                                    <ext:ModelField Name="URL" />
                                                    <ext:ModelField Name="StatusStr" />
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <ColumnModel>
                                    <Columns>
                                        <ext:DateColumn runat="server" Text="Publish Date" Width="100" DataIndex="PublishDate" />
                                        <ext:Column runat="server" Text="Notice Title" Width="300" Flex="1" DataIndex="Title" />
                                        <%--<ext:Column  runat="server" Text="Attachment" Width="200" DataIndex="FileName" />--%>
                                        <ext:DateColumn ID="DateColumn5" runat="server" Text="Expiry Date" Width="100" DataIndex="ExpiryDate" />
                                        <ext:TemplateColumn ID="TemplateColumn1" runat="server" Text="Attachment" Width="200"
                                            DataIndex="NoticeID" TemplateString='<a href="../NoticeFileDownloader.ashx?id={URL}"> {FileName}</a>' />
                                        <ext:Column runat="server" Text="Status" Width="100" DataIndex="StatusStr" />

                                        <ext:CommandColumn ID="cmdColumn" runat="server" Width="30" MenuDisabled="true" Sortable="false">
                                            <Commands>
                                                <ext:GridCommand Icon="ApplicationFormEdit" ToolTip-Text="Action">
                                                    <Menu EnableScrolling="false">
                                                    <Items>
                                                        <ext:MenuCommand Text="Unpublish" CommandName="Publish" />        
                                                        <ext:MenuCommand Text="Edit" CommandName="Edit" /> 
                                                        <ext:MenuCommand Text="Delete" CommandName="Delete" />
                                                    </Items>
                                                    </Menu>
                                                                        
                                                </ext:GridCommand>

                                            </Commands>
                                            <Listeners>
                                                <Command Handler="CommandHandler(command,record);" />
                                            </Listeners>
                                        </ext:CommandColumn>


                                    </Columns>
                                </ColumnModel>
                            </ext:GridPanel>
                        </Content>
                    </ext:Panel>
                    <ext:Panel ID="Panel1" runat="server" Title="Unpublished">
                        <Content>
                            <ext:GridPanel ID="GridPanel1" runat="server" Width="1200">
                                <Store>
                                    <ext:Store runat="server" ID="storeUnpublished">
                                        <Model>
                                            <ext:Model runat="server">
                                                <Fields>
                                                    <ext:ModelField Name="NoticeID" />
                                                    <ext:ModelField Name="Title" />
                                                    <ext:ModelField Name="PublishDate" Type="Date" />
                                                    <ext:ModelField Name="ExpiryDate" Type="Date" />
                                                    <ext:ModelField Name="FileName" />
                                                    <ext:ModelField Name="URL" />
                                                    <ext:ModelField Name="StatusStr" />
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <ColumnModel>
                                    <Columns>
                                        <ext:DateColumn ID="DateColumn1" runat="server" Text="Publish Date" Width="100" DataIndex="PublishDate" />
                                        <ext:Column ID="Column1" runat="server" Text="Notice Title" Width="300" Flex="1"
                                            DataIndex="Title" />
                                        <%--<ext:Column  runat="server" Text="Attachment" Width="200" DataIndex="FileName" />--%>
                                        <ext:DateColumn ID="DateColumn4" runat="server" Text="Expiry Date" Width="100" DataIndex="ExpiryDate" />
                                        <ext:TemplateColumn ID="TemplateColumn2" runat="server" Text="Attachment" Width="200"
                                            DataIndex="NoticeID" TemplateString='<a href="../NoticeFileDownloader.ashx?id={URL}"> {FileName}</a>' />
                                        <ext:Column ID="Column2" runat="server" Text="Status" Width="100" DataIndex="StatusStr" />

                                         <ext:CommandColumn ID="CommandColumn3" runat="server" Width="30" MenuDisabled="true" Sortable="false">
                                            <Commands>
                                                <ext:GridCommand Icon="ApplicationFormEdit" ToolTip-Text="Action">
                                                    <Menu EnableScrolling="false">
                                                    <Items>
                                                        <ext:MenuCommand Text="Publish" CommandName="Publish" />        
                                                        <ext:MenuCommand Text="Edit" CommandName="Edit" /> 
                                                        <ext:MenuCommand Text="Delete" CommandName="Delete" />
                                                    </Items>
                                                    </Menu>
                                                                        
                                                </ext:GridCommand>

                                            </Commands>
                                            <Listeners>
                                                <Command Handler="CommandHandler(command,record);" />
                                            </Listeners>
                                        </ext:CommandColumn>

                                    </Columns>
                                </ColumnModel>
                            </ext:GridPanel>
                        </Content>
                    </ext:Panel>
                    <ext:Panel ID="Panel2" runat="server" Title="Draft">
                        <Content>
                            <ext:GridPanel ID="GridPanel2" runat="server" Width="1200">
                                <Store>
                                    <ext:Store runat="server" ID="storeDraft">
                                        <Model>
                                            <ext:Model runat="server">
                                                <Fields>
                                                    <ext:ModelField Name="NoticeID" />
                                                    <ext:ModelField Name="Title" />
                                                    <ext:ModelField Name="PublishDate" Type="Date" />
                                                    <ext:ModelField Name="ExpiryDate" Type="Date" />
                                                    <ext:ModelField Name="FileName" />
                                                    <ext:ModelField Name="URL" />
                                                    <ext:ModelField Name="StatusStr" />
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <ColumnModel>
                                    <Columns>
                                        <ext:DateColumn ID="DateColumn2" runat="server" Text="Publish Date" Width="100" DataIndex="PublishDate" />
                                        <ext:Column ID="Column3" runat="server" Text="Notice Title" Width="300" Flex="1"
                                            DataIndex="Title" />
                                        <%--<ext:Column  runat="server" Text="Attachment" Width="200" DataIndex="FileName" />--%>
                                        <ext:DateColumn ID="DateColumn3" runat="server" Text="Expiry Date" Width="100" DataIndex="ExpiryDate" />
                                        <ext:TemplateColumn ID="TemplateColumn3" runat="server" Text="Attachment" Width="200"
                                            DataIndex="NoticeID" TemplateString='<a href="../NoticeFileDownloader.ashx?id={URL}"> {FileName}</a>' />
                                        <ext:Column ID="Column4" runat="server" Text="Status" Width="100" DataIndex="StatusStr" />

                                        <ext:CommandColumn ID="CommandColumn1" runat="server" Width="30" MenuDisabled="true" Sortable="false">
                                            <Commands>
                                                <ext:GridCommand Icon="ApplicationFormEdit" ToolTip-Text="Action">
                                                    <Menu EnableScrolling="false">
                                                    <Items>
                                                        <ext:MenuCommand Text="Publish" CommandName="Publish" />        
                                                        <ext:MenuCommand Text="Edit" CommandName="Edit" /> 
                                                        <ext:MenuCommand Text="Delete" CommandName="Delete" />
                                                    </Items>
                                                    </Menu>
                                                                        
                                                </ext:GridCommand>

                                            </Commands>
                                            <Listeners>
                                                <Command Handler="CommandHandler(command,record);" />
                                            </Listeners>
                                        </ext:CommandColumn>

                                        
                                    </Columns>
                                </ColumnModel>
                            </ext:GridPanel>
                        </Content>
                    </ext:Panel>
                </Items>
            </ext:TabPanel>
        </div>
    </div>
</asp:Content>
