﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using Utils.Helper;
using Utils.Web;
using BLL.Base;
using DAL;

namespace Web
{
    public partial class Overtime : BasePage
    {
        public int VBranchId
        {
            get
            {
                if (ViewState["VBranchId"] == null)
                    return 0;
                return int.Parse(ViewState["VBranchId"].ToString());
            }
            set { ViewState["VBranchId"] = value; }
        }
        public int VDepartmentId
        {
            get
            {
                if (ViewState["VDepartmentId"] == null)
                    return 0;
                return int.Parse(ViewState["VDepartmentId"].ToString());
            }
            set { ViewState["VDepartmentId"] = value; }
        }
        public int VShiftId
        {
            get
            {
                if (ViewState["VShiftId"] == null)
                    return 0;
                return int.Parse(ViewState["VShiftId"].ToString());
            }
            set { ViewState["VShiftId"] = value; }
        }
        public int VPayrollPeriodId
        {
            get
            {
                if (ViewState["PayrollPeriod"] == null)
                    return 0;
                return int.Parse(ViewState["PayrollPeriod"].ToString());
            }
            set { ViewState["PayrollPeriod"] = value; }
        }
        public string VEmpName
        {
            get
            {
                if (ViewState["VEmpName"] == null)
                    return "";
                return ViewState["VEmpName"].ToString();
            }
            set { ViewState["VEmpName"] = value; }
        }

        //private int _tempCurrentPage;
        //private int _tempCount;
        private PayManager payMgr = new PayManager();

        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (!IsPostBack)
            {
                pagingCtl.Visible = false;
                Initialise();

            }
            RegisterScripts();
        }

        void RegisterScripts()
        {
            StringBuilder str = new StringBuilder();
            str.AppendFormat("var decimalPlaces = {0};", SessionManager.DecimalPlaces);
          
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "rounding",
                                                        str.ToString()
                                                        , true);
        }
        protected void ddlDepartment_SelectedIndexChanged(object sender, EventArgs e)
        {
            ListItem firstItem = ddlDepartment.Items[0];
            ddlDepartment.Items.Clear();



            ddlDepartment.DataSource
                = DepartmentManager.GetAllDepartmentsByBranch(int.Parse(ddlBranch.SelectedValue));

            ddlDepartment.DataBind();

            ddlDepartment.Items.Insert(0, firstItem);
        }

        void Initialise()
        {

            

            ddlPayrollPeriods.DataSource = CommonManager.GetCurrentYear(SessionManager.CurrentCompanyId);
            ddlPayrollPeriods.DataBind();

            UIHelper.SetSelectedLastItemInDropDown(ddlPayrollPeriods);

            ddlBranch.DataSource
              = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId);
            ddlBranch.DataBind();

            CommonManager mgr = new CommonManager();
            ddlShift.DataSource = mgr.GetAllWorkShift();
            ddlShift.DataBind();


          

        }
        protected void btnLoad_Click(object sender, EventArgs e)
        {
            this.VPayrollPeriodId = int.Parse(ddlPayrollPeriods.SelectedValue);
            this.VEmpName = txtEmployeeName.Text.Trim();
            this.VBranchId = int.Parse(ddlBranch.SelectedValue);
            this.VDepartmentId = int.Parse(ddlDepartment.SelectedValue);
            this.VShiftId = int.Parse(ddlShift.SelectedValue);
            LoadEmployees();
        }

        protected void LoadEmployees()
        {
            int totalRecords = 0;
             List<GetOvertimeEmployeesResult> list = PayManager.GetOvertimes(this.VPayrollPeriodId,this.VBranchId,this.VDepartmentId,
                this.VShiftId, this.VEmpName, pagingCtl.CurrentPage - 1, int.Parse(pagingCtl.DDLRecords.SelectedValue), ref totalRecords);

            gvw.DataSource = list;
            gvw.DataBind();
            if (list == null || totalRecords == 0)
                pagingCtl.Visible = false;
            else
                pagingCtl.Visible = true;

            pagingCtl.UpdatePagingBar(totalRecords);
            ShowAppropriateMessage(this.VPayrollPeriodId, list);
        }

        protected void btnPrevious_Click()
        {
            pagingCtl.CurrentPage -= 1;
            //_tempCurrentPage = int.Parse(pagintCtl.LabelCurrentPage.Text);
            //_tempCurrentPage -= 1;
            LoadEmployees();
        }

        protected void btnNext_Click()
        {
            pagingCtl.CurrentPage += 1;
            //_tempCurrentPage = int.Parse(pagintCtl.LabelCurrentPage.Text);
            //_tempCurrentPage += 1;
            LoadEmployees();
        }

        protected void ddlRecords_SelectedIndexChanged()
        {
            //SessionManager.EmployeeDisplayRecodsPerPage = int.Parse(pagintCtl.DDLRecords.SelectedValue);
            //_tempCurrentPage = 1;
            pagingCtl.CurrentPage = 1;
            LoadEmployees();
        }


        /// <summary>
        /// Display appropriate message like Payroll already saved so not editable or some employee salary already saved so not editable.
        /// </summary>
        /// <param name="payrollPeriodId"></param>
        /// <param name="list"></param>
        public void ShowAppropriateMessage(int payrollPeriodId, List<GetOvertimeEmployeesResult>  list)
        {

            if (IsPayrollFinalSaved(payrollPeriodId))
            {
                divWarningMsg.InnerHtml = Resources.Messages.CalcAllEmployeeFinalSavedMsg;
                divWarningMsg.Hide = false;
            }
            else
            {
                foreach (GetOvertimeEmployeesResult item in list)
                {
                    if (item.IsCalculationSaved != null && item.IsCalculationSaved == 1)
                    {
                        divWarningMsg.InnerHtml = Resources.Messages.SomeOfTheEmployeeSalaryAlreadySavedMsg;
                        divWarningMsg.Hide = false;
                        break;
                    }
                }
            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            Page.Validate("Balance");

            decimal overtimeHours, payPerHour;
            string notes;
            int employeeId, payrollPeriodId, overtimeId;
            bool isSavedReqd = false;
            if (Page.IsValid)
            {

                StringBuilder str = new StringBuilder("<root>");


                foreach (GridViewRow row in gvw.Rows)
                {
                    isSavedReqd = true;

                    employeeId = (int)gvw.DataKeys[row.RowIndex]["EmployeeId"];
                    payrollPeriodId = (int)gvw.DataKeys[row.RowIndex]["PayrollPeriodId"];
                    overtimeId = (int)gvw.DataKeys[row.RowIndex]["OverTimeId"];


                    TextBox txtOvertimeHours = row.FindControl("txtOvertimeHours") as TextBox;
                    TextBox txtPayPerHour = row.FindControl("txtPayPerHour") as TextBox;
                    TextBox txtNote = row.FindControl("txtNote") as TextBox;


                    overtimeHours = decimal.Parse(txtOvertimeHours.Text);
                    payPerHour = decimal.Parse(txtPayPerHour.Text);

                    str.Append(
                        string.Format("<row PayrollPeriodId=\"{1}\" EmployeeId=\"{2}\" OverTimeHours=\"{3}\"  PayPerHour=\"{4}\"   Note=\"{5}\" OvertimeId=\"{0}\" /> ",
overtimeId, payrollPeriodId, employeeId, overtimeHours, payPerHour, txtNote.Text.Trim()
                        ));
                }
                str.Append("</root>");
                    
                if (isSavedReqd)
                {
                    PayManager.SaveOvertimePay(str.ToString());
                    msgCtl.InnerHtml = Resources.Messages.OvertimePaySaved;
                    msgCtl.Hide = false;
                    LoadEmployees();

                }
            }
        }


        protected void btnCancel_Click(object sender, EventArgs e)
        {
            LoadEmployees();
        }
        protected void btnDelete_Click(object sender, EventArgs e)
        {
            StringBuilder xmlEmployees = new StringBuilder();
            xmlEmployees.Append("<root>");
            bool employeeAdded = false;
            int payrollPeriodId = 0;
            for (int i = 0; i < this.gvw.Rows.Count; i++)
            {
                GridViewRow row = gvw.Rows[i];

                payrollPeriodId = (int)gvw.DataKeys[row.RowIndex]["PayrollPeriodId"];

                if (row.FindControl("chkDelete") == null)
                    continue;

                CheckBox chkDelete = (CheckBox)row.FindControl("chkDelete");

                if (chkDelete.Checked)
                {
                    int empId = (int)gvw.DataKeys[row.RowIndex]["EmployeeId"];

                    xmlEmployees.AppendFormat("<row EmployeeId='{0}' />", empId);
                    employeeAdded = true;
                }
            }
            xmlEmployees.Append("</root>");

            if ( employeeAdded)
            {
                PayManager.DeleteOvertime(payrollPeriodId, xmlEmployees.ToString());
                msgCtl.InnerHtml = Resources.Messages.OvertimeDeleteMessage;
                msgCtl.Hide = false;
                LoadEmployees();
            }
        }

        protected void btnPostToSalary_Click(object sender, EventArgs e)
        {

            foreach (GridViewRow row in gvw.Rows)
            {


                int payrollPeriodId = (int)gvw.DataKeys[row.RowIndex]["PayrollPeriodId"];

                PayManager.PostOvertimePay(payrollPeriodId);
                msgCtl.InnerHtml = Resources.Messages.OvertimePostedMessage;
                msgCtl.Hide = false;
                break;
            }


        }
        protected void txtEmployeeName_TextChanged(object sender, EventArgs e)
        {
            

        }
    }
}
