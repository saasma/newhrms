﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using BLL;
using BLL.Manager;
using Utils.Helper;
using System.IO;
using Utils;
using DAL;
using System.Drawing;
using Utils.Security;

namespace Web.CP
{
    public partial class ImageUpload : System.Web.UI.Page
    {
        /// <summary>
        /// Property for holding the Id of the editing object/entity
        /// </summary>
        public int CustomId
        {
            get
            {

                if (ViewState["Id"] != null)
                    return int.Parse(ViewState["Id"].ToString());
                return 0;
            }
            set
            {
                ViewState["Id"] = value;
            }
        }
        int employeeId = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                Initialise();
            employeeId = UrlHelper.GetIdFromQueryString("EmpId");
        }

        void Initialise()
        {
            int type = UrlHelper.GetIdFromQueryString("type");
            if (type != 0)
            {
                this.CustomId = type;
                if (this.CustomId == 4)
                    this.Page.Title = "Upload document";
                else
                {
                    this.Page.Title = "Upload image";
                    this.rowDesc.Visible = false;
                }
            }

           
        }

        protected void btnUpload_Click(object sender, EventArgs e)
        {
            if (fup.HasFile)
            {
                string ext = Path.GetExtension(fup.FileName);
                string guidFileName = Guid.NewGuid().ToString() + ext;
                string saveLocation = Path.Combine(Server.MapPath(Config.UploadLocation), guidFileName);


                UploadType type = (UploadType)this.CustomId;
                string code = "closeWindow(\"{0}\");";
                HRManager mgr = new HRManager();
                if (type != UploadType.Document)
                {
                    if (IsImage(ext) == false)
                    {
                        JavascriptHelper.DisplayClientMsg("Invalid image type.", Page);
                        return;
                    }
                    fup.SaveAs(saveLocation);
                    string filename = "";
                    try
                    {
                        mgr.SaveOrUpdateImage(type, guidFileName, employeeId);
                        filename = "../Uploads/" + guidFileName;
                        code = string.Format(code, filename);
                        JavascriptHelper.DisplayClientMsg("Image added.", Page, code);
                    }
                    catch (Exception exp1)
                    {
                        JavascriptHelper.DisplayClientMsg(exp1.Message, Page);
                    }
                    Page.ClientScript.RegisterClientScriptBlock(GetType(), "sdfdsf2333",
                       string.Format("opener.process('{0}');", filename), true);
                }
                else
                {
                    try
                    {
                        //

                        guidFileName =  guidFileName.Replace(Path.GetExtension(guidFileName), ".ashx");
                        saveLocation = saveLocation.Replace(Path.GetExtension(saveLocation), ".ashx");

                  

                        EncryptorDecryptor enc = new EncryptorDecryptor();
                        enc.Encrypt(fup.FileContent, saveLocation);

                        HDocument doc = new HDocument();
                        doc.EmployeeId = employeeId;
                        doc.Name = Path.GetFileName(fup.FileName);

                        if (!string.IsNullOrEmpty(txtDesc.Text.Trim()))
                        {
                            doc.Description = txtDesc.Text.Trim();
                        }
                        else
                            doc.Description = Path.GetFileNameWithoutExtension(fup.FileName);

                        doc.ContentType = fup.PostedFile.ContentType;
                        doc.Url = guidFileName;
                        doc.Size = GetSize(fup.PostedFile.ContentLength);

                        mgr.SaveDocument(doc);


                        code = string.Format(code, mgr.GetHTMLDocuments(doc.EmployeeId.Value));
                        JavascriptHelper.DisplayClientMsg("Document added.", Page,code);
                    }
                    catch (Exception exp1)
                    {
                        JavascriptHelper.DisplayClientMsg(exp1.Message, Page);
                    }
                }
            }
        }

        /// <summary>
        /// Checks if the file has the valid image extension or not
        /// </summary>
        /// <param name="pExtension">Extension type</param>
        /// <returns>Is valid image or not</returns>
        public static bool IsImage(string pExtension)
        {
            if ((pExtension.ToLower() == ".jpg" | pExtension.ToLower() == ".gif" | pExtension.ToLower() == ".bmp" | pExtension.ToLower() == ".jpeg" | pExtension.ToLower() == ".png"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// Helper method to convert Bytes in KiloBytes
        /// </summary>
        /// <param name="byteValue"></param>
        /// <returns></returns>
        public static int ConvertBytesIntoKB(int byteValue)
        {
            double mb = (byteValue / 1024);
            if (mb.ToString("#") == "")
                return 0;
            return Convert.ToInt32(mb.ToString("#"));
        }
        public static string ConvertKBIntoMB(object value)
        {
            long kb = Convert.ToInt64(value);
            double mb = (kb / 1024.0);
            return mb.ToString("#.#");
        }

        public string GetSize(int byteValue)
        {
            if (byteValue < 1024)
                return "1 kb";

            double kb = ConvertBytesIntoKB(byteValue);
            if (kb > 1024)
            {
                return ConvertKBIntoMB(kb) + " mb";
            }
            else
            {
                return kb + " kb";
            }
        }
    }
}
