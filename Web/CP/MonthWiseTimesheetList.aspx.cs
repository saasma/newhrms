﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using Web.CP.Report.Templates.HR;
using BLL.BO;
using System.IO;
using Utils;
using System.Data;
using Utils.Calendar;
using DevExpress.XtraPrinting;
using System.Security.AccessControl;
using Utils.Helper;
using System.Text;


namespace Web.CP
{
    public partial class MonthWiseTimesheetList : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {

                DateTime date = new DateTime(DateTime.Now.Year, DateTime.Now.Month,1);
                txtStartDate.Text = date.ToString();
                txtEndDate.Text = date.AddMonths(1).AddDays(-1).ToString();
                LoadCountStatus();
            }
            if (Request.Form["__EVENTTARGET"] != null && Request.Form["__EVENTTARGET"].ToString().ToLower().Equals("reload"))
            {
                Export();
            }
           
        }


        protected void btnReBindCountStatus_Click(object sender, DirectEventArgs e)
        {
            LoadCountStatus();
        }

        protected void btnView_Click(object sender, DirectEventArgs e)
        {

            int timesheetid = int.Parse(hdnTimeSheetId.Text);
            windowTimesheetDetails.Center();
            windowTimesheetDetails.Show();
            DAL.Timesheet timesheet = NewTimeSheetManager.GetTimeSheetById(timesheetid);
            if (timesheet == null)
            {
                NewMessage.ShowWarningMessage("Time sheet does not exists.");
                return;
            }
            else
            {
                windowTimesheetDetails.Title = "Timeheet of " +
                    new EmployeeManager().GetById(timesheet.EmployeeId).Name;
            }

            LoadCountStatus();
            TimesheetCtrl.LoadMonthWiseTimeSheet(timesheetid);
            TimesheetCtrl.SaveAndSend.Hide();
        }

        protected void LoadCountStatus()
        {

            int EmployeeId = -1;
            string startDate = null, endDate = null; bool isMonthWise = false;
            int status = -2;

            if (txtStartDate.SelectedDate != DateTime.MinValue)
                startDate = txtStartDate.Text.Trim();

            if (txtEndDate.SelectedDate != DateTime.MinValue)
                endDate = txtEndDate.Text.Trim();

            if (!string.IsNullOrEmpty(cmbSearch.SelectedItem.Value))
                EmployeeId = int.Parse(cmbSearch.SelectedItem.Value);


            GetTimeSheetListCountResult _GetTimeSheetListCountResult = NewTimeSheetManager.GetTimeSheetListCount(EmployeeId, startDate, endDate, status, isMonthWise).ToList()[0];
            if (_GetTimeSheetListCountResult != null)
            {
                PanelApproval.Title = "Awaiting Approval (" + _GetTimeSheetListCountResult.awaitingapproval + ")";
                PanelApproved.Title = "Approved (" + _GetTimeSheetListCountResult.approved + ")";
                PanelRejected.Title = "Rejected (" + _GetTimeSheetListCountResult.rejected + ")";
                PanelReviewed.Title = "Reviewed (" + _GetTimeSheetListCountResult.reviewed + ")";
                paneldraft.Title = "Draft (" + _GetTimeSheetListCountResult.Draft + ")";
                panelnotfilled.Title = "Not Filled (" + _GetTimeSheetListCountResult.NotFilled + ")";
            }
            else
            {
                PanelApproval.Title = "Awaiting Approval (0)";
                PanelApproved.Title = "Approved (0)";
                PanelRejected.Title = "Rejected (0)";
                PanelReviewed.Title = "Reviewed (0)";
                paneldraft.Title = "Draft (0)";
                panelnotfilled.Title = "Not Filled (0)";
            }
        }

        protected void btnExportAll_Click(object sender, EventArgs e)
        {

            DateTime? StartDate = DateTime.Now;
            DateTime? EndDate = DateTime.Now;
            if (!string.IsNullOrEmpty(hiddenStartDate.Value))
            {
                //txtStartDate.Text = hdnStartDate.Text;
                StartDate = DateTime.Parse(hiddenStartDate.Value);
                int Days = DateTime.DaysInMonth(StartDate.Value.Year, StartDate.Value.Month);
                EndDate = new DateTime(StartDate.Value.Year, StartDate.Value.Month, Days);
            }

            List<GetTimeSheetListResult> list =
              BLL.BaseBiz.PayrollDataContext.GetTimeSheetList(0, 999999, -1, StartDate, EndDate, -2).ToList();

            foreach (var item in list)
            {
                //if (item.Status == (int)TimeSheetStatus.Approved)
                //    item.StatusText = "Submitted";
                //else
                    item.StatusText =
                       HttpContext.GetGlobalResourceObject("FixedValues", ((TimeSheetStatus)item.Status).ToString()) == null ?
                       ((TimeSheetStatus)item.Status).ToString() :
                       HttpContext.GetGlobalResourceObject("FixedValues", ((TimeSheetStatus)item.Status).ToString()).ToString();
            }


            Bll.ExcelHelper.ExportToExcel("Timesheet " +  StartDate.Value.Year + " - " + DateHelper.GetMonthName(StartDate.Value.Month,IsEnglish), list,
                new List<string> { "TotalHours1","Month","WeekText","TimeSheetIDEmployeeID","TotalRows","RowNumber","TimesheetId","IdCardNo","WeekNo","StartDate","EndDate","CreatedByName","ApprovedBy","TotalHours","ApprovedOn","Status" },
            new List<String>() { },
            new Dictionary<string, string>() { { "EmployeeId", "EIN" }, { "Name", "Name" },
            { "StatusText", "Status" },{"ApprovedByName","Approved By"}},
            new List<string>() {  }
            , new List<string> { }
            , new List<string> { "" }
            , new Dictionary<string, string>() { { "Timesheet ", StartDate.Value.Year + " - " + DateHelper.GetMonthName(StartDate.Value.Month, IsEnglish) } }
            , new List<string> { });

        }

        protected void btnExport_Click(object sender, DirectEventArgs e)
        {
            Export();
        }
        protected void Export()
        {
            DateTime? StartDate = null;
            DateTime? EndDate = null;
            if (!string.IsNullOrEmpty(hdnStartDate.Text))
            {
                txtStartDate.Text = hdnStartDate.Text;
                StartDate = DateTime.Parse(hdnStartDate.Text);
                int Days = DateTime.DaysInMonth(StartDate.Value.Year, StartDate.Value.Month);
                EndDate = StartDate.Value.AddDays(Days);
            }

            int EmployeeID = 0;
            if (!string.IsNullOrEmpty(hdnEmployeeID.Text.Trim()))
                EmployeeID = int.Parse(hdnEmployeeID.Text.Trim());

            ReportDataSetTableAdapters.GetTimesheetForExportTableAdapter adap =
            new ReportDataSetTableAdapters.GetTimesheetForExportTableAdapter();
            adap.Connection.ConnectionString = Config.ConnectionString;
            ReportDataSet.GetTimesheetForExportDataTable MainTable = adap.GetData(EmployeeID, 0, StartDate, EndDate);
            Report.Templates.Pay.ReportTimesheet mainReport = new Report.Templates.Pay.ReportTimesheet();
            EEmployee _Employee = EmployeeManager.GetEmployeeById(EmployeeID);

            mainReport.DataSource = MainTable;
            mainReport.DataMember = "Report";
            mainReport.labelName.Text = _Employee.Name;
            mainReport.lblID.Text = _Employee.EmployeeId.ToString();
            string MonthName = DateHelper.GetMonthName(StartDate.Value.Month, true) + "," + StartDate.Value.Year;
            mainReport.lblThisMonth.Text = MonthName;
            mainReport.lblCompany.Text = SessionManager.CurrentCompany.Name;
            
            using (MemoryStream stream = new MemoryStream())
            {
                mainReport.ExportToXls(stream);
                HttpContext.Current.Response.Clear();
                HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";
                HttpContext.Current.Response.AppendHeader("Content-Type", "application/vnd.ms-excel");
                HttpContext.Current.Response.AddHeader("Content-Length", stream.ToArray().Length.ToString());
                HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment; filename=" + "Report_Employee_MonthwiseTimeSheet" + ".xls");
                HttpContext.Current.Response.OutputStream.Write(stream.ToArray(), 0, stream.ToArray().Length);
                HttpContext.Current.Response.End();
            }

        }

        private void ClearFields()
        {
            txtEmployeeName.Text = "";
            txtWeekNo.Text = "";
            txtWeekDate.Text = "";
            txtTotalHours.Text = "";
            txtNotes.Text = "";
        }

        protected void btnReject_Click(object sender, DirectEventArgs e)
        {
            ClearFields();

            int timeSheetId = int.Parse(hdnTimeSheetId.Text);

            Timesheet obj = NewTimeSheetManager.GetTimeSheetById(timeSheetId);
            if (obj != null && NewTimeSheetManager.IsTimesheetViewableForManager(obj.TimesheetId))
            {
                txtEmployeeName.Text = new EmployeeManager().GetById(obj.EmployeeId).Name;
                if (!string.IsNullOrEmpty(hdnWeekNo.Text))
                    txtWeekNo.Text = hdnWeekNo.Text;
                if (obj.StartDate != null)
                    txtWeekDate.Text = NewTimeSheetManager.GetWeekRangeName(
                        obj.StartDate.Value, obj.EndDate.Value,true);

                if (obj.TotalHours != null)
                    txtTotalHours.Text = obj.TotalHours.Value.ToString();
                WTimeSheet.Show();
            }
        }


        protected void btnRejectSave_Click(object sender, DirectEventArgs e)
        {
            int timeSheetId = int.Parse(hdnTimeSheetId.Text);

            Timesheet obj = new Timesheet()
            {
                TimesheetId = timeSheetId,
                Status = (int)TimeSheetStatus.Rejected,
                Notes = string.IsNullOrEmpty(txtNotes.Text.Trim()) ? null : txtNotes.Text.Trim()
            };

            Status status = NewTimeSheetManager.RejectTimeSheet(obj);
            if(status.IsSuccess)
            {
                WTimeSheet.Close();
                NewMessage.ShowNormalMessage( "Time sheet rejected successfully.");
                PagingToolbar1.DoRefresh();
            }
            else
            {
                NewMessage.ShowWarningMessage( status.ErrorMessage);
            }
        }



        public void btnSetApprovalDate_Click(object sender, DirectEventArgs e)
        {
            //if (Convert.ToDateTime(txtStartDate.Text.Trim()).Month != Convert.ToDateTime(txtEndDate.Text.Trim()).Month)
            //{
            //    NewMessage.ShowNormalMessage("Please select single month.");
            //    return;
            //}

            //if (Convert.ToDateTime(txtStartDate.Text.Trim()).Year != Convert.ToDateTime(txtEndDate.Text.Trim()).Year)
            //{
            //    NewMessage.ShowNormalMessage("Please select single month.");
            //    return;
            //}

            string strTimeSheetID = "";

            RowSelectionModel sm = this.gridTimeSheet.GetSelectionModel() as RowSelectionModel;
            foreach (SelectedRow row in sm.SelectedRows)
            {

                int timesheetId = int.Parse(row.RecordID.Split(',').ToArray()[0]);
                if (timesheetId > 0)
                {
                    if (!string.IsNullOrEmpty(strTimeSheetID))
                        strTimeSheetID += "," + timesheetId;
                    else
                        strTimeSheetID = timesheetId.ToString();
                }
            }

            if (strTimeSheetID == "")
            {
                NewMessage.ShowNormalMessage("Select Timesheet");
                return;
            }

            string[] arrayTimeSheetID = strTimeSheetID.Split(',').ToArray();

            if (calApprovalDate.SelectedDate == DateTime.MinValue)
            {
                NewMessage.ShowNormalMessage("Date is required.");
                return;
            }
            ChangeApprvalDate(arrayTimeSheetID, calApprovalDate.SelectedDate);
           
           
        }
        public void btnApprove_Click(object sender, DirectEventArgs e)
        {
            //if (Convert.ToDateTime(txtStartDate.Text.Trim()).Month != Convert.ToDateTime(txtEndDate.Text.Trim()).Month)
            //{
            //    NewMessage.ShowNormalMessage("Please select single month.");
            //    return;
            //}

            //if (Convert.ToDateTime(txtStartDate.Text.Trim()).Year != Convert.ToDateTime(txtEndDate.Text.Trim()).Year)
            //{
            //    NewMessage.ShowNormalMessage("Please select single month.");
            //    return;
            //}

            string strTimeSheetID = "";

            RowSelectionModel sm = this.gridTimeSheet.GetSelectionModel() as RowSelectionModel;
            foreach (SelectedRow row in sm.SelectedRows)
            {

                int timesheetId = int.Parse(row.RecordID.Split(',').ToArray()[0]);
                if (timesheetId > 0)
                {
                    if (!string.IsNullOrEmpty(strTimeSheetID))
                        strTimeSheetID += "," + timesheetId;
                    else
                        strTimeSheetID = timesheetId.ToString();
                }
            }

            if (strTimeSheetID == "")
            {
                NewMessage.ShowNormalMessage("Select Timesheet with status Awaiting for Approval");
                return;
            }

            string[] arrayTimeSheetID = strTimeSheetID.Split(',').ToArray();
            ChangeToApproveStatus(arrayTimeSheetID);
         
           
        }

        public void btnDraft_Click(object sender, DirectEventArgs e)
        {
            if (Convert.ToDateTime(txtStartDate.Text.Trim()).Month != Convert.ToDateTime(txtEndDate.Text.Trim()).Month)
            {
                NewMessage.ShowNormalMessage("Please select single month.");
                return;
            }

            string strTimeSheetID = "";
            string strEmployeeID = "";
            RowSelectionModel sm = this.gridTimeSheet.GetSelectionModel() as RowSelectionModel;


            foreach (SelectedRow row in sm.SelectedRows)
            {

                int timesheetId = int.Parse(row.RecordID.Split(',').ToArray()[0]);
                string EmployeeID = (row.RecordID.Split(',').ToArray()[1]);

                if (timesheetId > 0)
                {

                    if (!string.IsNullOrEmpty(strTimeSheetID))
                    {
                        strTimeSheetID += "," + timesheetId;
                        strEmployeeID += "," + EmployeeID;
                    }

                    else
                    {
                        strTimeSheetID = timesheetId.ToString();
                        strEmployeeID = EmployeeID.ToString();
                    }
                }
            }

            if (strTimeSheetID == "")
            {
                NewMessage.ShowNormalMessage("Please select Time Sheet");
                return;
            }

            string[] arrayTimeSheetID = strTimeSheetID.Split(',').ToArray();
            string[] arrayEmployeeID = strEmployeeID.Split(',').ToArray();
            ChangeStatus(arrayTimeSheetID);
            SendMailForDraft(arrayEmployeeID);
        }

        protected void ChangeToApproveStatus(string[] timesheetIDs)
        {
            int count = 0;

            foreach (string timesheetId in timesheetIDs)
            {

                Timesheet timesheet = BLL.BaseBiz.PayrollDataContext.Timesheets.FirstOrDefault(
                    x => x.TimesheetId == int.Parse(timesheetId));

                if (timesheet != null)
                {
                    // allow to approve any timesheet as request came to approve for rejected timesheet also
                    //if (timesheet.Status!= (int)TimeSheetStatus.AwaitingApproval)
                    //{
                    //    NewMessage.ShowNormalMessage("You can only approve Timesheet which are awaiting for approval.");
                    //    return;
                    //}

                    ChangeMonetory log = BLL.BaseBiz.GetMonetoryChangeLog(
                           timesheet.EmployeeId, (byte)MonetoryChangeLogTypeEnum.Timesheet, "Timesheet approved for " + DateHelper.GetMonthName( timesheet.StartDate.Value.Month,IsEnglish) + "-" + timesheet.StartDate.Value.Year,( (TimeSheetStatus)timesheet.Status).ToString(),
                           (TimeSheetStatus.Approved.ToString()), LogActionEnum.Update)
                                        ;
                    BLL.BaseBiz.PayrollDataContext.ChangeMonetories.InsertOnSubmit(log);

                    timesheet.Status = (int)TimeSheetStatus.Approved;
                    count+= 1;
                }
            }
            
            BLL.BaseBiz.PayrollDataContext.SubmitChanges();

            if (count > 0)
            {
                NewMessage.ShowNormalMessage((count) + " Timesheet has been approved");
                SendApproveMail(timesheetIDs);
                CheckboxSelectionModel1.ClearSelection();
                X.Js.AddScript("searchList();");
            }
        }

        protected void ChangeApprvalDate(string[] timesheetIDs,DateTime date)
        {
            int count = 0;

            foreach (string timesheetId in timesheetIDs)
            {

                Timesheet timesheet = BLL.BaseBiz.PayrollDataContext.Timesheets.FirstOrDefault(
                    x => x.TimesheetId == int.Parse(timesheetId));

                if (timesheet != null)
                {
                    // allow to approve any timesheet as request came to approve for rejected timesheet also
                    if (timesheet.Status != (int)TimeSheetStatus.Approved)
                    {
                        NewMessage.ShowNormalMessage("Timesheet of " + EmployeeManager.GetEmployeeName(timesheet.EmployeeId) + " of " +
                            timesheet.StartDate.Value.ToString("MMM-YYYY") + " is not approved to change the approval date.");
                        return;
                    }

                    ChangeMonetory log = BLL.BaseBiz.GetMonetoryChangeLog(
                           timesheet.EmployeeId, (byte)MonetoryChangeLogTypeEnum.Timesheet, "Timesheet approval date changed " + DateHelper.GetMonthName(timesheet.StartDate.Value.Month, IsEnglish) + "-" + timesheet.StartDate.Value.Year,
                           timesheet.ApprovedOn == null ? "" : timesheet.ApprovedOn.ToString(),
                           date.ToString(), LogActionEnum.Update)
                                        ;
                    BLL.BaseBiz.PayrollDataContext.ChangeMonetories.InsertOnSubmit(log);

                    timesheet.ApprovedOn = date;
                    count += 1;
                }
            }

            BLL.BaseBiz.PayrollDataContext.SubmitChanges();

            if (count > 0)
            {
                NewMessage.ShowNormalMessage((count) + " Timesheet approval date changed");
                WindowChnageApprovalDate.Hide();
                X.Js.AddScript("searchList();");
            }
        }
        protected void ChangeStatus(string[] timesheetIDs)
        {
            string to = "";
            int count = 0;
            foreach (string timesheetId in timesheetIDs)
            {

                Timesheet timesheet = BLL.BaseBiz.PayrollDataContext.Timesheets.FirstOrDefault(
                    x => x.TimesheetId == int.Parse(timesheetId));

                if (timesheet != null)
                {
                    timesheet.Status = (int)TimeSheetStatus.Draft;
                    
                    count += 1;
                }


            }

            BLL.BaseBiz.PayrollDataContext.SubmitChanges();

            if (count > 0)
            {
                NewMessage.ShowNormalMessage("Status set to draft for " + (count) + " timesheet.");
                X.Js.AddScript("searchList();");
            }

        }

        public void btnReview_Click(object sender, DirectEventArgs e)
        {
            string gridItemsJson = e.ExtraParams["gridItems"];

            List<GetTimeSheetListResult> list = JSON.Deserialize<List<GetTimeSheetListResult>>(gridItemsJson);
            List<Timesheet> timesheetList = new List<Timesheet>();

            foreach (var item in list)
            {
                //if (item.Status!= (int)TimeSheetStatus.Approved)
                //{
                //    NewMessage.ShowNormalMessage("Only approved status can review.");
                //    return;
                //}
                    
                Timesheet obj = new Timesheet()
                {
                    EmployeeId = item.EmployeeId,
                    TimesheetId = item.TimesheetId
                };
                timesheetList.Add(obj);
            }

            if (list.Count == 0)
                return;

            Status status = NewTimeSheetManager.ReviewTimeSheet(timesheetList);
            if (status.IsSuccess)
            {
                CheckboxSelectionModel1.ClearSelection();
                NewMessage.ShowNormalMessage("Time sheets Reviewed successfully.");
                PagingToolbar1.DoRefresh();
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }
        }

        public void btnSendMail(object sender, DirectEventArgs e)
        {

            if (Convert.ToDateTime(txtStartDate.Text.Trim()).Month != Convert.ToDateTime(txtEndDate.Text.Trim()).Month)
            {
                NewMessage.ShowNormalMessage("Please select single month.");
                return;
            }

            string strEmployeeID = "";
            RowSelectionModel sm = this.gridTimeSheet.GetSelectionModel() as RowSelectionModel;


            foreach (SelectedRow row in sm.SelectedRows)
            {

                //timesheet id is zero for not filled status
                if (int.Parse(row.RecordID.Split(',').ToArray()[0])>0)
                {
                    NewMessage.ShowNormalMessage("Please send mail for not filled status only");
                    return;
                }

                if (!string.IsNullOrEmpty(strEmployeeID))
                    strEmployeeID += "," + row.RecordID.Split(',').ToArray()[1].ToString();
                
                else
                    strEmployeeID = row.RecordID.Split(',').ToArray()[1].ToString();
            }

            if (strEmployeeID == "")
            {
                NewMessage.ShowNormalMessage("Please select Time Sheet");
                return;
            }

            string[] arrayEmployeeID = strEmployeeID.Split(',').ToArray();
           SendMail(arrayEmployeeID);

        }

        protected void SendMail(string[] EmployeeIDs)
        {

            SMTPHelper mailSender = new SMTPHelper();
            EmailContent dbMailContent = CommonManager.GetMailContent((int)EmailContentType.ProjectNotfill);

            string MonthName = Convert.ToDateTime(txtStartDate.Text).Year + " - " +
            DateHelper.GetMonthName(Convert.ToDateTime(txtStartDate.Text).Month, true);

            string subject = dbMailContent.Subject.Replace("#YearMonth#", MonthName);

            string body = dbMailContent.Body.Replace("#YearMonth#", MonthName);

            string to = "";
            foreach (string EmployeeID in EmployeeIDs)
            {
                EEmployee _Employeee = EmployeeManager.GetEmployeeById(int.Parse(EmployeeID));
                if (string.IsNullOrEmpty(_Employeee.EAddresses[0].CIEmail))
                {
                    NewMessage.ShowNormalMessage("Email address not found for Employee " + _Employeee.Name);
                    return;
                }
                if (!string.IsNullOrEmpty(to))
                    to += "," + _Employeee.EAddresses[0].CIEmail;
                else
                    to = _Employeee.EAddresses[0].CIEmail;
            }

            string[] ToEmailIDs = to.Split(',');
            int count = 0;
            foreach (string ToEMailId in ToEmailIDs)
            {
                bool isSendSuccess = SMTPHelper.SendMail(ToEMailId, body, subject, "","","");
                if (isSendSuccess)

                    count++;
            }

            if (count > 0)
            {
                NewMessage.ShowNormalMessage(string.Format(Resources.Messages.SentMailSuccessMsg, count));
                return;
            }
            else
            {
                NewMessage.ShowWarningMessage("error while sending email");
                return;
            }
        }



        protected void SendApproveMail(string[] TimesheetID)
        {
            List<Timesheet> _TimesheetList = NewTimeSheetManager.GetAllTimeSheet();
            string strEmployeeID = "";
            foreach (string timeSheetID in TimesheetID)
            {
               Timesheet _Timesheet =  _TimesheetList.SingleOrDefault(t => t.TimesheetId == int.Parse(timeSheetID));
               if (!string.IsNullOrEmpty(strEmployeeID))
                   strEmployeeID += "," + _Timesheet.EmployeeId.ToString();
               else
                   strEmployeeID = _Timesheet.EmployeeId.ToString();
            }

            string[] EmployeeIDs = strEmployeeID.Split(',').ToArray();
            SMTPHelper mailSender = new SMTPHelper();
            EmailContent dbMailContent = CommonManager.GetMailContent((int)EmailContentType.TimeSheetApprovedOrReject);

            string MonthName = Convert.ToDateTime(txtStartDate.Text).Year + " - " +
            DateHelper.GetMonthName(Convert.ToDateTime(txtStartDate.Text).Month, true);

            string subject = dbMailContent.Subject.Replace("#YearMonth#", MonthName).Replace("#Status#", "Approved");

            string body = dbMailContent.Body.Replace("#YearMonth#", MonthName).Replace("#Status#", "Approved").Replace("#Reason#", "");

            string to = "";
            foreach (string EmployeeID in EmployeeIDs)
            {

                EEmployee _Employeee = EmployeeManager.GetEmployeeById(int.Parse(EmployeeID));
                if (string.IsNullOrEmpty(_Employeee.EAddresses[0].CIEmail))
                {
                    //NewMessage.ShowNormalMessage("Email address not found for Employee " + _Employeee.Name);
                    //return;
                }
                else
                {

                    if (!string.IsNullOrEmpty(to))
                        to += "," + _Employeee.EAddresses[0].CIEmail;
                    else
                        to = _Employeee.EAddresses[0].CIEmail;
                }
            }

            string[] ToEmailIDs = to.Split(',');
            int count = 0;
            foreach (string ToEMailId in ToEmailIDs)
            {
                bool isSendSuccess = SMTPHelper.SendMail(ToEMailId, body, subject, "","");
                if (isSendSuccess)
                    count++;
            }

            //if (count > 0)
            //{
            //    NewMessage.ShowNormalMessage(string.Format(Resources.Messages.SentMailSuccessMsg, count));
            //    return;
            //}



        }

        protected void SendMailForDraft(string[] EmployeeIDs)
        {

            SMTPHelper mailSender = new SMTPHelper();
            EmailContent dbMailContent = CommonManager.GetMailContent((int)EmailContentType.TimesheetSetAsDraft);

            if (dbMailContent == null)
            {
                NewMessage.ShowNormalMessage("Template not defined.");
                return;
            }

            string MonthName = Convert.ToDateTime(txtStartDate.Text).Year + " - " +
            DateHelper.GetMonthName(Convert.ToDateTime(txtStartDate.Text).Month, true);

            string subject = dbMailContent.Subject.Replace("#YearMonth#", MonthName);

            string body = dbMailContent.Body.Replace("#YearMonth#", MonthName);

            string to = "";
            foreach (string EmployeeID in EmployeeIDs)
            {

                //string filterExp = "TimeSheetID = '" + TimeSheetID + "'";

                //Timesheet _Timesheet = NewTimeSheetManager.GetTimeSheetById(int.Parse(TimeSheetID));
                EEmployee _Employeee = EmployeeManager.GetEmployeeById(int.Parse(EmployeeID));



                if (string.IsNullOrEmpty(_Employeee.EAddresses[0].CIEmail))
                {
                  //  NewMessage.ShowNormalMessage("Email address not found for Employee " + _Employeee.Name);
                   // return;
                }

                if (!string.IsNullOrEmpty(to))
                    to += "," + _Employeee.EAddresses[0].CIEmail;
                else
                    to = _Employeee.EAddresses[0].CIEmail;

            }


            string[] ToEmailIDs = to.Split(',');
            int count = 0;
            foreach (string ToEMailId in ToEmailIDs)
            {
                bool isSendSuccess = SMTPHelper.SendMail(ToEMailId, body, subject, "","");
                if (isSendSuccess)

                    //NewMessage.ShowNormalMessage(Resources.Messages.SentMailSuccessMsg);
                    count++;
            }

            if (count > 0)
            {
               // NewMessage.ShowNormalMessage(string.Format(Resources.Messages.SentMailSuccessMsg, count));
                return;
            }
            else
            {
                NewMessage.ShowWarningMessage("error while sending email");
                return;
            }


        }
    
    }
}