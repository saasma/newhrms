using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using BLL;
using BLL.Manager;
using Utils.Helper;
using DAL;
using BLL.Base;

namespace Web.CP
{
    public partial class GroupInsurance : BasePage
    {
        BranchManager branchMgr = new BranchManager();
        EmployeeManager empMgr = new EmployeeManager();
        InsuranceManager insMgr = new InsuranceManager();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Initialise();
            }
            AttachJSCode();
        }

        void Initialise()
        {
            LoadBranches();
            LoadInsuranceName();
            LoadInstitution();
            LoadPremiumPaymentFrequency();
            SetCalendarInitialDate();
        }

        void SetCalendarInitialDate()
        {
            calStartDate.IsEnglishCalendar = SessionManager.CurrentCompany.IsEnglishDate;
            calEndDate.IsEnglishCalendar = SessionManager.CurrentCompany.IsEnglishDate;

            calStartDate.SelectTodayDate();
            calEndDate.SelectTodayDate();
        }

        void AttachJSCode()
        {
            JavascriptHelper.AttachPopUpCode(Page, "insuranceNamePopup", "ManageInsuranceName.aspx", 300, 300);
            JavascriptHelper.AttachPopUpCode(Page, "InstitutionPopup", "ManageInstitution.aspx", 300, 300);
        }

        void LoadBranches()
        {
            ddlBranch.DataSource = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId);
            ddlBranch.DataBind();
        }

        void LoadInsuranceName()
        {
            ddlInsuranceName.DataSource = insMgr.GetInsuranceName();
            ddlInsuranceName.DataBind();
        }

        void LoadInstitution()
        {
            ddlInstitution.DataSource = insMgr.GetInsuranceInstitution();
            ddlInstitution.DataBind();
        }

        void LoadPremiumPaymentFrequency()
        {
            rdbPaymentPeriod.DataSource = BizHelper.GetMembersAsListItem(new DAL.PremiumPaymentFrequency());
            rdbPaymentPeriod.DataBind();
            rdbPaymentPeriod.SelectedIndex = 0;
        }

        EmpGroupInsurance ProcessInsurance(EmpGroupInsurance grpIns)
        {
            if (grpIns != null)
            {
                ddlBranch.SelectedValue = grpIns.BranchId.ToString();
                ddlInsuranceName.SelectedValue = grpIns.InsuranceNameId.ToString();
                ddlInstitution.SelectedValue = grpIns.InstitutionId.ToString();
                txtPolicyNo.Text = grpIns.PolicyNo.ToString();
                calStartDate.SetSelectedDate(grpIns.StartDate, IsEnglish);
                calEndDate.SetSelectedDate(grpIns.EndDate, IsEnglish);
                txtPolicyAmount.Text = grpIns.PolicyAmount.ToString();
                txtPremium.Text = grpIns.PremiumAmount.ToString();
                rdbPaymentPeriod.SelectedValue = grpIns.PremiumPaymentFrequency.ToString();
                txtNote.Text = grpIns.Note;
            }
            else
            {
                grpIns = new EmpGroupInsurance();

                grpIns.BranchId = int.Parse(ddlBranch.SelectedValue);
                grpIns.CompanyId = SessionManager.CurrentCompanyId;
                grpIns.InsuranceNameId = int.Parse(ddlInsuranceName.SelectedValue);
                grpIns.InstitutionId = int.Parse(ddlInstitution.SelectedValue);
                grpIns.PolicyNo = txtPolicyNo.Text.Trim();
                grpIns.StartDate = calStartDate.SelectedDate.ToString();
                grpIns.EndDate = calEndDate.SelectedDate.ToString();
                grpIns.PolicyAmount = txtPolicyAmount.Text.Trim();
                grpIns.PremiumAmount = txtPremium.Text.Trim();
                grpIns.PremiumPaymentFrequency = rdbPaymentPeriod.SelectedValue;
                grpIns.Note = txtNote.Text.Trim();

                //if (int.Parse(indi.StartDate) > int.Parse(indi.EndDate))
                //    return;

                //if (indiv.EmployeePaysPercent + indiv.EmployerPaysPercent != 100)
                //    return;

                //if (int.Parse(indiv.PolicyAmount) < int.Parse(indiv.Premium))
                //    return;

                return grpIns;
            }
            return null;
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (!Page.IsValid)
            {
                EmpGroupInsurance grp = ProcessInsurance(null);
                if (gvInsuranceDetails.SelectedIndex != -1)
                {
                    int selectedInsuranceId = int.Parse(ViewState["InsuranceId"].ToString());
                    grp.Id = selectedInsuranceId;
                    insMgr.UpdateGroupInsurance(grp);
                    JavascriptHelper.DisplayClientMsg("Group Insurance information is updated.", Page);
                }
                else
                {
                    insMgr.SaveGroupInsurance(grp);
                    JavascriptHelper.DisplayClientMsg("Group Insurance information is saved.", Page);
                }
                gvInsuranceDetails.SelectedIndex = -1;
                LoadInsuranceDetails(null, null);
                ClearInsuranceFields();
            }
            else
            {
                JavascriptHelper.DisplayClientMsg("Validation fail.", Page);
            }
        }

        protected void LoadInsuranceDetails(object sender, EventArgs e)
        {
            gvInsuranceDetails.DataSource = insMgr.GetGroupDetailsByBranchId(int.Parse(ddlBranch.SelectedValue));
            gvInsuranceDetails.DataBind();
        }

        protected void gvInsuranceDetails_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover",
                "this.originalstyle=this.style.backgroundColor;this.style.backgroundColor='#8ea4d1';this.title ='click to edit';");

                e.Row.Attributes.Add("onmouseout",
                "this.style.backgroundColor=this.originalstyle;this.title ='';");
            }
        }

        protected void gvInsuranceDetails_SelectedIndexChanged(object sender, EventArgs e)
        {
            int insuranceId = (int)gvInsuranceDetails.DataKeys[gvInsuranceDetails.SelectedIndex][0];
            ViewState["InsuranceId"] = insuranceId;
            EmpGroupInsurance insu = insMgr.GetGroupInsuranceById(insuranceId);
            if (insu != null)
            {
                ProcessInsurance(insu);
                btnSave.Text = Resources.Messages.Update;
            }
        }

        protected void gvInsuranceDetails_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            EmpGroupInsurance ins = new EmpGroupInsurance();
            int id = (int)gvInsuranceDetails.DataKeys[e.RowIndex][0];

            if (id != 0)
            {
                ins.Id = id;
                if (insMgr.DeleteGroupInsurance(ins) == true)
                {
                    JavascriptHelper.DisplayClientMsg("Insurance information is deleted.", Page);
                    ClearInsuranceFields();
                }
                else
                    JavascriptHelper.DisplayClientMsg("Insurance deletion failed", Page);
            }
            LoadInsuranceDetails(null, null);
        }
        void ClearInsuranceFields()
        {
            txtNote.Text = "";
            txtPolicyNo.Text = "";
            txtPremium.Text = "";
            txtPolicyAmount.Text = "";
            calStartDate.SelectTodayDate();
            calEndDate.SelectTodayDate();
        }

        protected void ddlBranch_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadInsuranceDetails(null, null);
        }
    }
}
