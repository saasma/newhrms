<%@ Page Title="CIT List" Language="C#" MasterPageFile="~/Master/HROld.Master" AutoEventWireup="true"
    CodeBehind="PrivisionalCITEng.aspx.cs" Inherits="Web.PrivisionalCITEng" %>

<%@ Import Namespace="BLL.Manager" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/PagingCtl.ascx" TagName="PagingCtl" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../Scripts/currency.js" type="text/javascript"></script>
    <style type="text/css">
        .tableLightColor input
        {
            width: 120px;
        }
        #content{overflow:auto;;}
    </style>
    <script src="../Scripts/jquery-1.3.2.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        var decimalPlaces = 0;
        function refreshWindow() {

            __doPostBack('Refresh', 0);

        }
        jQuery(document).ready(
    function () {

        setMovementToGrid('#<%= gvEmployeeIncome.ClientID %>');



        $("select").each(function (index) {
            if (this.id.indexOf("gvEmployeeIncome") >= 0) {
                citTypeChange(document.getElementById(this.id));
            }
        });

    }
);

        function setRemaining(id) {
            //            var plainId = id.replace("txtProvision", "").replace("txtCurrentMonthAddition", "");
            //            var provision = 0;
            //            var currentmonth = 0;

            //            provision = parseFloat(document.getElementById(plainId + "txtProvision").value.replaceAll(",", ""));
            //            currentmonth = parseFloat(document.getElementById(plainId + "txtCurrentMonthAddition").value.replaceAll(",", ""));

            //            if (isNaN(provision))
            //                provision = 0;

            //            if (isNaN(currentmonth))
            //                currentmonth = 0;

            //            document.getElementById(plainId + "txtRemainingProvision").value = getMoney(provision - currentmonth);

        }

        function citTypeChange(ddl) {
            var id = ddl.id;
            var type = ddl.value;

            var txtRegularAmountRate = document.getElementById(id.replace("ddl", "txtRegularAmountRate"));
            var txtRate = document.getElementById(id.replace("ddl", "txtRate"));

            if (txtRate == null)
                return;

            var txtCurrentMonthAdditionalAdjustment = document.getElementById(id.replace("ddl", "txtCurrentMonthAdditionalAdjustment"));
            var isMonthAmountEnabled;

            if (type == "Percent") {
                txtRate.readOnly = false;
                txtRate.style.backgroundColor = '';

                txtRegularAmountRate.readOnly = true;
                txtRegularAmountRate.style.backgroundColor = '#EBEBE4';
            }
            else {
                txtRate.readOnly = true;
                txtRate.style.backgroundColor = '#EBEBE4';
            }

            if (type == "Fixed" || type == "Percent") {

                if (type == "Fixed") {
                    txtRegularAmountRate.readOnly = false;
                    txtRegularAmountRate.style.backgroundColor = '';
                }

                isMonthAmountEnabled = false;

                txtCurrentMonthAdditionalAdjustment.readOnly = false;
                txtCurrentMonthAdditionalAdjustment.style.backgroundColor = '';
            }
            else if (type == "CITTable") {
                txtRegularAmountRate.readOnly = true;
                txtRegularAmountRate.style.backgroundColor = '#EBEBE4';
                isMonthAmountEnabled = true;

                txtCurrentMonthAdditionalAdjustment.readOnly = false;
                txtCurrentMonthAdditionalAdjustment.style.backgroundColor = '';
            }
            //optimum
            else {
                txtRegularAmountRate.readOnly = true;
                txtRegularAmountRate.style.backgroundColor = '#EBEBE4';
                isMonthAmountEnabled = false;

                txtCurrentMonthAdditionalAdjustment.readOnly = true;
                txtCurrentMonthAdditionalAdjustment.style.backgroundColor = '#EBEBE4';
            }

            for (i = 1; i <= 12; i++) {
                var txt = document.getElementById(id.replace("ddl", "txtMonth" + i));
                if (txt.getAttribute("isEnabled").toString() == "True") {
                    txt.readOnly = !isMonthAmountEnabled;

                    if (isMonthAmountEnabled)
                        txt.style.backgroundColor = '';
                    else
                        txt.style.backgroundColor = '#EBEBE4';
                }
            }


            if (ddl.getAttribute('setValue') == ddl.value)
                ddl.style.backgroundColor = '';
            else ddl.style.backgroundColor = 'lightyellow';



        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4 id="citList" runat="server">
                    CIT List for :
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <div class="attribute" style="padding: 10px">
            <table>
                <tr>
                    <td>
                        Search &nbsp;
                        <asp:TextBox ID="txtEmployeeName" runat="server" OnTextChanged="txtEmployeeName_TextChanged"
                            AutoPostBack="true" Width="146px" />
                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtEmployeeName"
                            WatermarkText="Search Employee" WatermarkCssClass="searchBoxText" />
                        <cc1:AutoCompleteExtender EnableCaching="true" ID="AutoCompleteExtenderOrganization"
                            runat="server" MinimumPrefixLength="2" ServiceMethod="GetEmployeeNames" ServicePath="~/PayrollService.asmx"
                            TargetControlID="txtEmployeeName" CompletionSetCount="10" CompletionInterval="250"
                            CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList"
                            CompletionListHighlightedItemCssClass="AutoExtenderHighlight">
                        </cc1:AutoCompleteExtender>
                    </td>
                    <td>
                        <asp:LinkButton ID="btnExport" runat="server" Text="Excel import" OnClientClick="taxImportPopup();return false;"
                            CssClass=" excel marginRight tiptip" Style="float: left;" />
                    </td>
                </tr>
            </table>
        </div>
        <div class="clear" style="width: 1200px; overflow-x: scroll">
            <uc2:MsgCtl ID="divMsgCtl" EnableViewState="false" Hide="true" Width="390px" runat="server" />
            <uc2:WarningCtl ID="divWarningMsg" EnableViewState="false" Hide="true" runat="server" />
            <cc1:TextBoxWatermarkExtender ID="TBWE2" runat="server" TargetControlID="txtEmployeeName"
                WatermarkText="Enter employee name" WatermarkCssClass="watermarked" />
            <cc2:EmptyDisplayGridView CssClass="table table-primary mb30 table-bordered table-hover"
                Style="margin-bottom: 0px" UseAccessibleHeader="true" ShowHeaderWhenEmpty="True"
                ID="gvEmployeeIncome" runat="server" DataKeyNames="EmployeeId,RegularCITType,Name"
                AutoGenerateColumns="False" CellPadding="4" GridLines="None" AllowSorting="True"
                ShowFooterWhenEmpty="False">
                <%--            <RowStyle BackColor="#E3EAEB" />--%>
                <Columns>
                    <asp:BoundField HeaderStyle-Width="20px" DataFormatString="{0:000}" DataField="EmployeeId"
                        HeaderText="EIN"></asp:BoundField>
                    <asp:BoundField HeaderStyle-Width="40px" DataFormatString="{0:000}" DataField="INo"
                        HeaderText="I No"></asp:BoundField>
                    <asp:TemplateField HeaderText="Name" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="120px">
                        <ItemTemplate>
                            <asp:HyperLink CssClass="employeeClass" Target="_blank" Width="150px" ID="Label2"
                                runat="server" NavigateUrl='<%# "~/newhr/EmployeePayroll.aspx?Id=" +  Eval("EmployeeId") %>'
                                Text='<%# "&nbsp;" + Eval("Name") %>'></asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Account No" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="100px">
                        <ItemTemplate>
                            <asp:Label Width="100px" ID="Label22" runat="server" Text='<%#Eval("BankACNo") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="CIT Type" HeaderStyle-Width="70px">
                        <ItemTemplate>
                            <asp:DropDownList onchange='citTypeChange(this);' ID="ddl" Enabled='<%#  Eval("RegularCITType").ToString() == "Optimum" ? false : true%>'
                                setValue='<%#  Eval("RegularCITType")%>' SelectedValue='<%#  Eval("RegularCITType")%>'
                                runat="server" Width="100px">
                                <asp:ListItem Text="Optimum" Value="Optimum" />
                                <asp:ListItem Text="Fixed" Value="Fixed" />
                                <asp:ListItem Text="Percent" Value="Percent" />
                                <asp:ListItem Text="CIT Table" Value="CITTable" />
                            </asp:DropDownList>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="Auto Provision"
                        HeaderStyle-Width="50px">
                        <ItemTemplate>
                            <asp:DropDownList ID="ddlAutoPro" Enabled='<%#  Eval("RegularCITType").ToString() == "Optimum" ? false : true%>'
                                SelectedValue='<%#  Eval("IsCITAutoProvision")%>' runat="server" Width="100px">
                                <asp:ListItem Text="No" Value="False" />
                                <asp:ListItem Text="Yes" Value="True" />
                            </asp:DropDownList>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="Fixed Amount" HeaderStyle-Width="80px">
                        <ItemTemplate>
                            <asp:TextBox Width="80px" CssClass='calculationInput' data-col='5' data-row='<%# Container.DataItemIndex %>'
                                Style='text-align: right' Enabled='<%#  IsTextBoxEnabled( Eval("RegularCITType"))%>'
                                ID="txtRegularAmountRate" Text='<%#  GetCITCurrency( Eval("RegularCITAmountRate"))%>'
                                runat="server"></asp:TextBox>
                            <asp:CompareValidator SetFocusOnError="true" ValueToCompare="0" Display="None" Operator="GreaterThanEqual"
                                Type="Currency" ID="valOpeningBalance25" ControlToValidate="txtRegularAmountRate"
                                ValidationGroup="Balance" runat="server" ErrorMessage="Invalid amount, must be greater than zero."></asp:CompareValidator>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="One Time Adjustment" HeaderStyle-Width="80px">
                        <ItemTemplate>
                            <asp:TextBox Width="80px" CssClass='calculationInput' data-col='5' data-row='<%# Container.DataItemIndex %>'
                                Style='text-align: right' Enabled='<%#  IsTextBoxEnabled( Eval("RegularCITType"))%>'
                                ID="txtCurrentMonthAdditionalAdjustment" Text='<%#  GetCITCurrency( Eval("CurrentMonthAdditionalCIT"))%>'
                                runat="server"></asp:TextBox>
                            <asp:CompareValidator SetFocusOnError="true" ValueToCompare="0" Display="None" Operator="GreaterThanEqual"
                                Type="Currency" ID="valOpeningBalance251" ControlToValidate="txtCurrentMonthAdditionalAdjustment"
                                ValidationGroup="Balance" runat="server" ErrorMessage="Invalid addional adjustment, must be greater than zero."></asp:CompareValidator>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="Rate" HeaderStyle-Width="80px">
                        <ItemTemplate>
                            <asp:TextBox Width="80px" CssClass='calculationInput' data-col='5' data-row='<%# Container.DataItemIndex %>'
                                Style='text-align: right' Enabled='<%#  IsTextBoxEnabled( Eval("RegularCITType"))%>'
                                ID="txtRate" Text='<%#  GetCITCurrency( Eval("CITRate"))%>' runat="server"></asp:TextBox>
                            <asp:CompareValidator SetFocusOnError="true" ValueToCompare="0" Display="None" Operator="GreaterThanEqual"
                                Type="Currency" ID="valOpeningBalance2511" ControlToValidate="txtRate" ValidationGroup="Balance"
                                runat="server" ErrorMessage="Invalid rate, must be greater than or equal to zero."></asp:CompareValidator>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Add-On CIT" HeaderStyle-Width="55px">
                        <ItemTemplate>
                            <asp:Label ID="Label1" Text='<%#  GetCITCurrency(Eval("AddOnCIT")) %>' Style="width: 70px!important;
                                display: block; text-align: right;" runat="server" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <%-- <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="Provision" HeaderStyle-Width="40px">
                    <ItemTemplate>
                        <asp:TextBox Width="80px" onblur='setRemaining(this.id)' Enabled='<%#  IsTextBoxEnabled( Eval("RegularCITType"))%>'
                            CssClass='calculationInput' data-col='5' data-row='<%# Container.DataItemIndex %>'
                            Style='text-align: right' ID="txtProvision" Text='<%#  GetCurrency( Eval("ProvisionalCITAmount"))%>'
                            runat="server"></asp:TextBox>
                        <asp:CompareValidator SetFocusOnError="true" ValueToCompare="0" Display="None" Operator="GreaterThanEqual"
                            Type="Currency" ID="valOpeningBalance245" ControlToValidate="txtProvision" ValidationGroup="Balance"
                            runat="server" ErrorMessage="Invalid amount, must be greater than zero."></asp:CompareValidator>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="Use in Current Month (Additional)"
                    HeaderStyle-Width="80px">
                    <ItemTemplate>
                        <asp:TextBox Width="80px" onblur='setRemaining(this.id)' Enabled='<%#  IsTextBoxEnabled( Eval("RegularCITType"))%>'
                            CssClass='calculationInput' data-col='5' data-row='<%# Container.DataItemIndex %>'
                            Style='text-align: right' ID="txtCurrentMonthAddition" Text='<%#  GetCurrency( Eval("CurrentMonthAdditionCIT"))%>'
                            runat="server"></asp:TextBox>
                        <asp:CompareValidator SetFocusOnError="true" ValueToCompare="0" Display="None" Operator="GreaterThanEqual"
                            Type="Currency" ID="valOpeningBalance24533" ControlToValidate="txtCurrentMonthAddition"
                            ValidationGroup="Balance" runat="server" ErrorMessage="Invalid amount, must be greater than zero."></asp:CompareValidator>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="Remaining Provision"
                    HeaderStyle-Width="40px">
                    <ItemTemplate>
                        <asp:TextBox Width="80px" Enabled='false' CssClass='calculationInput' data-col='5'
                            data-row='<%# Container.DataItemIndex %>' Style='text-align: right' ID="txtRemainingProvision"
                            Text='<%#  GetCurrency( Convert.ToDecimal(Eval("ProvisionalCITAmount")) - Convert.ToDecimal(Eval("CurrentMonthAdditionCIT"))  )%>'
                            runat="server"></asp:TextBox>
                    </ItemTemplate>
                </asp:TemplateField>--%>
                    
                    <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="July" HeaderStyle-Width="55px">
                        <ItemTemplate>
                            <asp:TextBox Width="55px" onblur='setRemaining(this.id)' CssClass='calculationInput'
                                data-col='9' data-row='<%# Container.DataItemIndex %>' Style='text-align: right'
                                ID="txtMonth7" Text='<%#  GetCITCurrency( Eval("Month7"))%>' isEnabled='<%# Eval("Month7Enabled") %>'
                                Enabled='<%# Eval("Month7Enabled") %>' runat="server"></asp:TextBox>
                            <asp:CompareValidator SetFocusOnError="true" ValueToCompare="0" Display="None" Operator="GreaterThanEqual"
                                Type="Currency" ID="valOpeningBalanceM7" ControlToValidate="txtMonth7" ValidationGroup="Balance"
                                runat="server" ErrorMessage="Invalid amount, must be greater than zero."></asp:CompareValidator>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="August" HeaderStyle-Width="55px">
                        <ItemTemplate>
                            <asp:TextBox Width="55px" onblur='setRemaining(this.id)' CssClass='calculationInput'
                                data-col='10' data-row='<%# Container.DataItemIndex %>' Style='text-align: right'
                                ID="txtMonth8" Text='<%#  GetCITCurrency( Eval("Month8"))%>' isEnabled='<%# Eval("Month8Enabled") %>'
                                Enabled='<%# Eval("Month8Enabled") %>' runat="server"></asp:TextBox>
                            <asp:CompareValidator SetFocusOnError="true" ValueToCompare="0" Display="None" Operator="GreaterThanEqual"
                                Type="Currency" ID="valOpeningBalanceM8" ControlToValidate="txtMonth8" ValidationGroup="Balance"
                                runat="server" ErrorMessage="Invalid amount, must be greater than zero."></asp:CompareValidator>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="September" HeaderStyle-Width="55px">
                        <ItemTemplate>
                            <asp:TextBox Width="55px" onblur='setRemaining(this.id)' CssClass='calculationInput'
                                data-col='11' data-row='<%# Container.DataItemIndex %>' Style='text-align: right'
                                ID="txtMonth9" Text='<%#  GetCITCurrency( Eval("Month9"))%>' isEnabled='<%# Eval("Month9Enabled") %>'
                                Enabled='<%# Eval("Month9Enabled") %>' runat="server"></asp:TextBox>
                            <asp:CompareValidator SetFocusOnError="true" ValueToCompare="0" Display="None" Operator="GreaterThanEqual"
                                Type="Currency" ID="valOpeningBalanceM9" ControlToValidate="txtMonth9" ValidationGroup="Balance"
                                runat="server" ErrorMessage="Invalid amount, must be greater than zero."></asp:CompareValidator>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="October" HeaderStyle-Width="55px">
                        <ItemTemplate>
                            <asp:TextBox Width="55px" onblur='setRemaining(this.id)' CssClass='calculationInput'
                                data-col='12' data-row='<%# Container.DataItemIndex %>' Style='text-align: right'
                                ID="txtMonth10" Text='<%#  GetCITCurrency( Eval("Month10"))%>' isEnabled='<%# Eval("Month10Enabled") %>'
                                Enabled='<%# Eval("Month10Enabled") %>' runat="server"></asp:TextBox>
                            <asp:CompareValidator SetFocusOnError="true" ValueToCompare="0" Display="None" Operator="GreaterThanEqual"
                                Type="Currency" ID="valOpeningBalanceM10" ControlToValidate="txtMonth10" ValidationGroup="Balance"
                                runat="server" ErrorMessage="Invalid amount, must be greater than zero."></asp:CompareValidator>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="November" HeaderStyle-Width="55px">
                        <ItemTemplate>
                            <asp:TextBox Width="55px" onblur='setRemaining(this.id)' CssClass='calculationInput'
                                data-col='13' data-row='<%# Container.DataItemIndex %>' Style='text-align: right'
                                ID="txtMonth11" Text='<%#  GetCITCurrency( Eval("Month11"))%>' isEnabled='<%# Eval("Month11Enabled") %>'
                                Enabled='<%# Eval("Month11Enabled") %>' runat="server"></asp:TextBox>
                            <asp:CompareValidator SetFocusOnError="true" ValueToCompare="0" Display="None" Operator="GreaterThanEqual"
                                Type="Currency" ID="valOpeningBalanceM11" ControlToValidate="txtMonth11" ValidationGroup="Balance"
                                runat="server" ErrorMessage="Invalid amount, must be greater than zero."></asp:CompareValidator>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="December" HeaderStyle-Width="55px">
                        <ItemTemplate>
                            <asp:TextBox Width="55px" onblur='setRemaining(this.id)' CssClass='calculationInput'
                                data-col='14' data-row='<%# Container.DataItemIndex %>' Style='text-align: right'
                                ID="txtMonth12" Text='<%#  GetCITCurrency( Eval("Month12"))%>' isEnabled='<%# Eval("Month12Enabled") %>'
                                Enabled='<%# Eval("Month12Enabled") %>' runat="server"></asp:TextBox>
                            <asp:CompareValidator SetFocusOnError="true" ValueToCompare="0" Display="None" Operator="GreaterThanEqual"
                                Type="Currency" ID="valOpeningBalanceM12" ControlToValidate="txtMonth12" ValidationGroup="Balance"
                                runat="server" ErrorMessage="Invalid amount, must be greater than zero."></asp:CompareValidator>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="January" HeaderStyle-Width="55px">
                        <ItemTemplate>
                            <asp:TextBox Width="55px" onblur='setRemaining(this.id)' CssClass='calculationInput'
                                data-col='15' data-row='<%# Container.DataItemIndex %>' Style='text-align: right'
                                ID="txtMonth1" Text='<%#  GetCITCurrency( Eval("Month1"))%>' isEnabled='<%# Eval("Month1Enabled") %>'
                                Enabled='<%# Eval("Month1Enabled") %>' runat="server"></asp:TextBox>
                            <asp:CompareValidator SetFocusOnError="true" ValueToCompare="0" Display="None" Operator="GreaterThanEqual"
                                Type="Currency" ID="valOpeningBalanceM1" ControlToValidate="txtMonth1" ValidationGroup="Balance"
                                runat="server" ErrorMessage="Invalid amount, must be greater than zero."></asp:CompareValidator>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="February" HeaderStyle-Width="55px">
                        <ItemTemplate>
                            <asp:TextBox Width="55px" onblur='setRemaining(this.id)' CssClass='calculationInput'
                                data-col='16' data-row='<%# Container.DataItemIndex %>' Style='text-align: right'
                                ID="txtMonth2" Text='<%#  GetCITCurrency( Eval("Month2"))%>' isEnabled='<%# Eval("Month2Enabled") %>'
                                Enabled='<%# Eval("Month2Enabled") %>' runat="server"></asp:TextBox>
                            <asp:CompareValidator SetFocusOnError="true" ValueToCompare="0" Display="None" Operator="GreaterThanEqual"
                                Type="Currency" ID="valOpeningBalanceM2" ControlToValidate="txtMonth2" ValidationGroup="Balance"
                                runat="server" ErrorMessage="Invalid amount, must be greater than zero."></asp:CompareValidator>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="March" HeaderStyle-Width="55px">
                        <ItemTemplate>
                            <asp:TextBox Width="55px" onblur='setRemaining(this.id)' CssClass='calculationInput'
                                data-col='17' data-row='<%# Container.DataItemIndex %>' Style='text-align: right'
                                ID="txtMonth3" Text='<%#  GetCITCurrency( Eval("Month3"))%>' isEnabled='<%# Eval("Month3Enabled") %>'
                                Enabled='<%# Eval("Month3Enabled") %>' runat="server"></asp:TextBox>
                            <asp:CompareValidator SetFocusOnError="true" ValueToCompare="0" Display="None" Operator="GreaterThanEqual"
                                Type="Currency" ID="valOpeningBalanceM3" ControlToValidate="txtMonth3" ValidationGroup="Balance"
                                runat="server" ErrorMessage="Invalid amount, must be greater than zero."></asp:CompareValidator>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="April" HeaderStyle-Width="55px">
                        <ItemTemplate>
                            <asp:TextBox Width="55px" onblur='setRemaining(this.id)' CssClass='calculationInput'
                                data-col='6' data-row='<%# Container.DataItemIndex %>' Style='text-align: right'
                                ID="txtMonth4" Text='<%#  GetCITCurrency( Eval("Month4"))%>' isEnabled='<%# Eval("Month4Enabled") %>'
                                Enabled='<%# Eval("Month4Enabled") %>' runat="server"></asp:TextBox>
                            <asp:CompareValidator SetFocusOnError="true" ValueToCompare="0" Display="None" Operator="GreaterThanEqual"
                                Type="Currency" ID="valOpeningBalanceM4" ControlToValidate="txtMonth4" ValidationGroup="Balance"
                                runat="server" ErrorMessage="Invalid amount, must be greater than zero."></asp:CompareValidator>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="May" HeaderStyle-Width="55px">
                        <ItemTemplate>
                            <asp:TextBox Width="55px" onblur='setRemaining(this.id)' CssClass='calculationInput'
                                data-col='7' data-row='<%# Container.DataItemIndex %>' Style='text-align: right'
                                ID="txtMonth5" Text='<%#  GetCITCurrency( Eval("Month5"))%>' isEnabled='<%# Eval("Month5Enabled") %>'
                                Enabled='<%# Eval("Month5Enabled") %>' runat="server"></asp:TextBox>
                            <asp:CompareValidator SetFocusOnError="true" ValueToCompare="0" Display="None" Operator="GreaterThanEqual"
                                Type="Currency" ID="valOpeningBalanceM5" ControlToValidate="txtMonth5" ValidationGroup="Balance"
                                runat="server" ErrorMessage="Invalid amount, must be greater than zero."></asp:CompareValidator>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="June" HeaderStyle-Width="55px">
                        <ItemTemplate>
                            <asp:TextBox Width="55px" onblur='setRemaining(this.id)' CssClass='calculationInput'
                                data-col='8' data-row='<%# Container.DataItemIndex %>' Style='text-align: right'
                                ID="txtMonth6" Text='<%#  GetCITCurrency( Eval("Month6"))%>' isEnabled='<%# Eval("Month6Enabled") %>'
                                Enabled='<%# Eval("Month6Enabled") %>' runat="server"></asp:TextBox>
                            <asp:CompareValidator SetFocusOnError="true" ValueToCompare="0" Display="None" Operator="GreaterThanEqual"
                                Type="Currency" ID="valOpeningBalanceM6" ControlToValidate="txtMonth6" ValidationGroup="Balance"
                                runat="server" ErrorMessage="Invalid amount, must be greater than zero."></asp:CompareValidator>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="External CIT" HeaderStyle-Width="40px">
                        <ItemTemplate>
                            <asp:TextBox Width="100px" CssClass='calculationInput' data-col='18' data-row='<%# Container.DataItemIndex %>'
                                Style='text-align: right' ID="txtExternal" Text='<%#  GetCITCurrency( Eval("ExternalSettlementCIT"))%>'
                                runat="server"></asp:TextBox>
                            <asp:CompareValidator SetFocusOnError="true" ValueToCompare="0" Display="None" Operator="GreaterThanEqual"
                                Type="Currency" ID="valOpeningBalance2451" ControlToValidate="txtExternal" ValidationGroup="Balance"
                                runat="server" ErrorMessage="Invalid amount, must be greater than zero."></asp:CompareValidator>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <RowStyle CssClass="odd" />
                <AlternatingRowStyle CssClass="even" />
                <EmptyDataTemplate>
                    <b>No employee list. </b>
                </EmptyDataTemplate>
            </cc2:EmptyDisplayGridView>
            <uc1:PagingCtl ID="pagingCtl" OnNextRecord="btnNext_Click" OnPrevRecord="btnPrevious_Click"
                OnDropDownShowRecordsChanged="ddlRecords_SelectedIndexChanged" runat="server" />
        </div>
        <div class="buttonsDiv">
            <%--    <asp:ValidationSummary ID="ValidationSummary1" ValidationGroup="Balance" DisplayMode="List"
            ShowSummary="false" ShowMessageBox="true" runat="server"></asp:ValidationSummary>--%>
            <asp:Button ID="btnUpdate" CssClass="update" OnClientClick="valGroup='Balance';return CheckValidation();"
                Visible="true" ValidationGroup="Balance" runat="server" Text="Update" OnClick="btnUpdate_Click" />
        </div>
    </div>
</asp:Content>
