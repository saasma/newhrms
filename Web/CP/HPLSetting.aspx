<%@ Page MaintainScrollPositionOnPostback="true" Title="Allowance Setting" Language="C#"
    MasterPageFile="~/Master/Details.Master" AutoEventWireup="true" CodeBehind="HPLSetting.aspx.cs"
    Inherits="Web.CP.HPLSetting" %>

<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
    <div class="contentArea">
        <h3>
            Allowance Rate</h3>
        <uc2:MsgCtl ID="divMsgCtl" EnableViewState="false" Hide="true" runat="server" />
        <uc2:WarningCtl ID="divWarningMsg" EnableViewState="false" Hide="true" runat="server" />
        <cc2:EmptyDisplayGridView Width="700px" Style='clear: both;' GridLines="None" PagerStyle-CssClass="defaultPagingBar"
            CssClass="tableLightColor" ShowHeaderWhenEmpty="True" ID="gvw" runat="server"
            DataKeyNames="Key,Description" AutoGenerateColumns="False" ShowFooterWhenEmpty="False" AllowPaging="True"
            PageSize="20" OnRowCancelingEdit="gvw_RowCancelingEdit" OnRowEditing="gvw_RowEditing"
            OnRowUpdating="gvw_RowUpdating">
            <Columns>
                <asp:TemplateField HeaderText="Description" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="180px">
                    <ItemTemplate>
                        <%# Eval("Description")%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Header Name" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="180px">
                    <ItemTemplate>
                        <asp:TextBox ID="txtHeaderName" runat="server" Text='<%# Eval("HeaderName")%>' Width="150px" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Calculation" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="140px">
                    <ItemTemplate>
                        <%# Eval("Calculation")%></ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Rate" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right"
                    HeaderStyle-Width="60px">
                    <ItemTemplate>
                        <%# Eval("Rate")%>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:TextBox Style='text-align: right' ID="txt" Width="50px" runat="server" Text='<%# Eval("Rate")%>' />
                        <asp:RequiredFieldValidator SetFocusOnError="true" ID="RequiredFieldValidator1" runat="server"
                            ErrorMessage='<%# Eval("Description") + " is required."  %>' ControlToValidate="txt"
                            Display="None" ValidationGroup="Rate"></asp:RequiredFieldValidator>
                        <asp:CompareValidator SetFocusOnError="true" ValueToCompare='0' Display="None" Operator="GreaterThan"
                            Type="Integer" ControlToValidate="txt" ID="CompareValidator1" ValidationGroup="Rate"
                            runat="server" ErrorMessage='<%# Eval("Description") + " should be greater then zero(0)."  %>'></asp:CompareValidator>
                    </EditItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="" HeaderStyle-Width="150px" ItemStyle-HorizontalAlign="Right">
                    <ItemTemplate>
                        <asp:Button runat="server" ValidationGroup="Rate" CommandName="Edit" Text="Edit" />
                        &nbsp;&nbsp;
                    </ItemTemplate>
                    <EditItemTemplate>
                        <%-- <asp:ValidationSummary runat="server" ShowSummary="false" ShowMessageBox="true" DisplayMode="SingleParagraph" ValidationGroup="Rate" />--%>
                        <asp:Button ID="LinkButton1" OnClientClick="valGroup='Rate';return CheckValidation();"
                            runat="server" ValidationGroup="Rate" CommandName="Update" Text="Update" />&nbsp;
                        <asp:Button ID="LinkButton2" runat="server" CausesValidation="false" CommandName="Cancel"
                            Text="Cancel" />&nbsp;&nbsp;
                    </EditItemTemplate>
                </asp:TemplateField>
            </Columns>
            <PagerSettings Mode="NumericFirstLast" NextPageText="Next �" PreviousPageText="� Prev"
                FirstPageText="First" LastPageText="Last" />
            <RowStyle CssClass="odd" />
            <AlternatingRowStyle CssClass="even" />
            <PagerStyle CssClass="defaultPagingBar" />
            <SelectedRowStyle CssClass="selected" />
            <EmptyDataTemplate>
                <strong style='padding-top: 6px; padding-left: 3px'>No employees in the payroll period.
                </strong>
            </EmptyDataTemplate>
        </cc2:EmptyDisplayGridView>
        <br />
        <br />

<asp:CheckBox AutoPostBack="true" OnCheckedChanged="chkHideOtherAllowance_Change" ID="chkHideOtherAllowance" Text="Hide Other Allowance in Import / Pay Calculation" runat="server" />
    </div>
</asp:Content>
