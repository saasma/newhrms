﻿<%@ Page Title="Holiday Pay Details" Language="C#" AutoEventWireup="true" MasterPageFile="~/Master/NewDetails.Master"
    CodeBehind="HolidayPayDetails.aspx.cs" Inherits="Web.NewHR.HolidayPayDetails" %>

<%@ Register Src="~/Controls/FooterCtl.ascx" TagName="FooterCtl" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/HeaderCtl.ascx" TagName="HeaderCtl" TagPrefix="uc3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .holiday, .holiday a, .holiday td
        {
            color: #469146;
            background-color: #F0FFF0 !important;
        }
        .leave, .leave a, .leave td
        {
            color: #D95CA9;
            background-color: #FFF0F5 !important;
        }
        
        .weeklyholiday, .weeklyholiday a, .weeklyholiday td
        {
            color: #8F8F1A;
            background-color: #FAFAD2 !important;
        }
    </style>
    <script type="text/javascript">


   var getRowClass = function (record) {
    
        var status = record.data.PaymentStatus;
        

        if(status  == "Paid")
         {
            return "holiday";
         }
        
         else //if(dayValue=="Working Day")
         {
            return "";
         }
         //else 

      };

  
    var CommandHandler = function (command, record) {
            
        };

    function searchList() {
        <%=PagingToolbar1.ClientID %>.doRefresh();
    }

    var gridRowBeforeSelect = function (record, index,e1,e2,e3) {
         var gridrecord = <%=gridList.ClientID %>.getStore().getAt(index.index);
        
        if(typeof(gridrecord) != 'undefined')
        {
        if(gridrecord.data.PaymentStatus=="Paid")
        {
            return false;
        }
        }
    };
       

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <script type="text/javascript" src="<%= ResolveUrl("~/Employee/override.js?v=") + Web.Helper.WebHelper.Version %>"></script>
    <%-- <ext:ResourceManager ID="ResourceManager1" runat="server" DisableViewState="false"
        ShowWarningOnAjaxFailure="false" ScriptMode="Release" Namespace="CompanyX" />--%>
    <ext:Hidden ID="hdnEmployeeID" runat="server" />
    <ext:Hidden ID="hdnOTDate" runat="server" />
    <ext:Hidden ID="hdnIsApprovedOnly" runat="server" />
    <ext:Hidden ID="hdnPeriodId" runat="server" />
    <ext:Hidden ID="hdnSortBy" runat="server" />
    <div class="contentpanel">
        <div class="innerLR">
            <ext:DisplayField runat="server" StyleSpec="margin-top:0px;margin-bottom:0px;" Html="<h4> Leave Fare</h4>"
                ID="title">
            </ext:DisplayField> 
             <span runat="server"  id="salaryNotSaved" style="color: #FF3400">Note
                : Attendance need to be generated for the period from <a href='../newhr/GenerateAttendanceReportData.aspx'>Generate Attendance</a> before proceding.</span>
            <div class="alert alert-info" style="margin-bottom: 0px;">
                <table>
                    <tr>
                        <td>
                            <ext:ComboBox Width="100" MarginSpec="0 5 0 5" LabelSeparator="" LabelAlign="Top"
                                QueryMode="Local" ID="cmbYear" ForceSelection="true" DisplayField="Year" ValueField="Year"
                                runat="server" FieldLabel="Year">
                                <Store>
                                    <ext:Store ID="Store1" runat="server">
                                        <Model>
                                            <ext:Model ID="Model3" IDProperty="Year" runat="server">
                                                <Fields>
                                                    <ext:ModelField Name="Year" Type="String" />
                                                    <ext:ModelField Name="Year" Type="String" />
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                            </ext:ComboBox>
                        </td>
                        <td>
                            <ext:ComboBox Width="100" MarginSpec="0 5 0 5" LabelSeparator="" LabelAlign="Top"
                                QueryMode="Local" ID="cmbMonth" ForceSelection="true" DisplayField="Value" ValueField="Key"
                                runat="server" FieldLabel="Month">
                                <Store>
                                    <ext:Store ID="Store2" runat="server">
                                        <Model>
                                            <ext:Model ID="Model4" IDProperty="Key" runat="server">
                                                <Fields>
                                                    <ext:ModelField Name="Key" Type="String" />
                                                    <ext:ModelField Name="Value" Type="String" />
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                            </ext:ComboBox>
                        </td>
                        <td style='display:none'>
                            <ext:ComboBox Width="100" MarginSpec="0 5 0 5" LabelSeparator="" LabelAlign="Top"
                                QueryMode="Local" ID="cmbStatus" ForceSelection="true" runat="server" FieldLabel="Payment Status">
                                <Items>
                                    <ext:ListItem Text="All" Value="-1" />
                                    <ext:ListItem Text="Pending" Value="0" />
                                    <ext:ListItem Text="Paid" Value="1" />
                                </Items>
                                <SelectedItems>
                                    <ext:ListItem Index="0" />
                                </SelectedItems>
                            </ext:ComboBox>
                        </td>
                        <td style="width: 200px;">
                            <ext:Store runat="server" ID="storeSearch" AutoLoad="false">
                                <Proxy>
                                    <ext:AjaxProxy Url="../Handler/EmpSearchID.ashx">
                                        <ActionMethods Read="GET" />
                                        <Reader>
                                            <ext:JsonReader Root="plants" TotalProperty="total" />
                                        </Reader>
                                    </ext:AjaxProxy>
                                </Proxy>
                                <Model>
                                    <ext:Model ID="Model2" IDProperty="Name" Name="ItemLineModel" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="Name" Type="String" />
                                            <ext:ModelField Name="EmployeeId" Type="String" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                            <ext:ComboBox LabelSeparator="" FieldLabel="Employee" ID="cmbSearch" LabelAlign="Top"
                                LabelWidth="70" runat="server" DisplayField="Name" ValueField="EmployeeId" EmptyText="Search"
                                StoreID="storeSearch" TypeAhead="false" Width="200" HideBaseTrigger="true" MinChars="1"
                                TriggerAction="All" ForceSelection="true">
                                <ListConfig LoadingText="Searching..." MinWidth="200">
                                    <ItemTpl ID="ItemTpl1" runat="server">
                                        <Html>
                                            <tpl>
                                            <div class="search-item">
                                                            <span>{Name}</span>  
                                             </div>
					                </tpl>
                                        </Html>
                                    </ItemTpl>
                                </ListConfig>
                                <Triggers>
                                    <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                                </Triggers>
                                <Listeners>
                                    <Select Handler="this.getTrigger(0).show();" />
                                    <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                    <TriggerClick Handler="if (index == 0) { 
                                           this.clearValue(); 
                                           this.getTrigger(0).hide();
                                       }" />
                                </Listeners>
                            </ext:ComboBox>
                        </td>
                        <td style="width: 20px">
                        </td>
                        <td valign="bottom">
                            <ext:Button runat="server" Width="100" Height="30" Cls="btn btn-primary" ID="btnLoad"
                                Text="Load">
                                <Listeners>
                                    <Click Handler="searchList();" />
                                </Listeners>
                            </ext:Button>
                        </td>
                    </tr>
                </table>
            </div>
            <br />
            <table>
                <tr>
                    <td>
                        <ext:TabPanel ID="tabPanel" runat="server" MinHeight="30" Width="320">
                            <Items>
                                <ext:Panel ID="pnlPending" runat="server" Title="UnApproved">
                                </ext:Panel>
                                <ext:Panel ID="pnlApproved" runat="server" Title="Approved">
                                </ext:Panel>
                            </Items>
                            <Listeners>
                                <TabChange Handler="searchList();" />
                            </Listeners>
                        </ext:TabPanel>
                    </td>
                    <td style="padding-left: 20px;">
                        <span id='spanText' style="color: blue"></span>
                    </td>
                </tr>
            </table>
            <ext:GridPanel ID="gridList" runat="server" Header="true" AutoScroll="true">
                <Store>
                    <ext:Store ID="Store3" runat="server" OnReadData="Store_ReadData" RemoteSort="true"
                        AutoLoad="true" PageSize="50"  GroupField="Name">
                        <Proxy>
                            <ext:PageProxy />
                        </Proxy>
                        <Sorters>
                            <ext:DataSorter Direction="ASC" Property="Name" />
                        </Sorters>
                        <AutoLoadParams>
                            <ext:Parameter Name="start" Value="0" Mode="Raw" />
                        </AutoLoadParams>
                        <Model>
                            <ext:Model ID="Model1" runat="server" >
                                <Fields>
                                    <ext:ModelField Name="SN" />
                                    <ext:ModelField Name="LeaveRequestId" />
                                    <ext:ModelField Name="EmployeeId" />
                                    <ext:ModelField Name="Name" />
                                    <ext:ModelField Name="Designation" />
                                    <ext:ModelField Name="DesignationId" />
                                    <ext:ModelField Name="Branch" />
                                    <ext:ModelField Name="BranchId" />
                                    <ext:ModelField Name="Department" />
                                    <ext:ModelField Name="DepartmentId" />
                                    <ext:ModelField Name="ServiceStatusId" />
                                    <ext:ModelField Name="ServiceStatus" />
                                    <ext:ModelField Name="WorkedMinText" />
                                    <ext:ModelField Name="DateEng" />
                                    <ext:ModelField Name="DayValueText" />
                                    <ext:ModelField Name="DayValueTextAdded" />
                                    <ext:ModelField Name="ClockInText" />
                                    <ext:ModelField Name="ClockOutText" />
                                    <ext:ModelField Name="ClockIn" />
                                    <ext:ModelField Name="ClockOut" />
                                    <ext:ModelField Name="SalaryType" />
                                    <ext:ModelField Name="Amount" />
                                    <ext:ModelField Name="BasicAmount" />
                                    <ext:ModelField Name="PayrollPeriodId" />
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                <Features>
                    <ext:Grouping StartCollapsed="false" IsDynamic="true" ID="Group1" runat="server"
                        HideGroupedHeader="true" EnableGroupingMenu="true"  GroupHeaderTplString="{name}"/>
                </Features>
                <Plugins>
                    <ext:CellEditing ID="CellEditing1" ClicksToEdit="1" runat="server">
                    </ext:CellEditing>
                </Plugins>
                <ColumnModel ID="ColumnModel2" runat="server">
                    <Columns>
                        <ext:Column ID="Column9" Locked="true" runat="server" Text="SN" Width="50" DataIndex="SN"
                            Align="Center" Sortable="true" MenuDisabled="true">
                        </ext:Column>
                        <ext:Column ID="colEId" Locked="true" runat="server" Text="EIN" Width="50" DataIndex="EmployeeId"
                            Align="Center" Sortable="true" MenuDisabled="true">
                        </ext:Column>
                        <ext:Column ID="Column1" Locked="true" runat="server" Text="Name" Width="150" DataIndex="Name"
                            Sortable="true" MenuDisabled="true">
                        </ext:Column>
                        <ext:Column ID="Column10" Locked="true" runat="server" Text="Designation" Width="110"
                            DataIndex="Designation" Sortable="true" MenuDisabled="true">
                        </ext:Column>
                        <ext:Column ID="Column11" Locked="true" runat="server" Text="Branch" Width="110"
                            DataIndex="Branch" Sortable="true" MenuDisabled="true">
                        </ext:Column>
                        <ext:Column ID="Column12" Locked="true" runat="server" Text="Department" Width="110"
                            DataIndex="Department" Sortable="true" MenuDisabled="true">
                        </ext:Column>
                     <%--   <ext:Column ID="Column2" Locked="true" runat="server" Text="Service Status" Width="100"
                            DataIndex="ServiceStatus" Sortable="true" MenuDisabled="true">
                        </ext:Column>        --%>               
                        <ext:DateColumn ID="colApprovedDate" runat="server" Text="Date" Width="90"
                            DataIndex="DateEng" Sortable="true" MenuDisabled="true" Format="dd-MMM-yyyy" />
                      <ext:Column ID="Column4" runat="server" Text="Present Day" Width="140"
                            DataIndex="DayValueTextAdded" Sortable="true" MenuDisabled="true">
                        </ext:Column>     


                        <ext:Column ID="Column8" runat="server" Align="Center" Text="In" Width="80"
                            DataIndex="ClockInText" Sortable="true" MenuDisabled="true">
                        </ext:Column>
                         <ext:Column ID="Column13" runat="server" Align="Center" Text="Out" Width="80"
                            DataIndex="ClockOutText" Sortable="true" MenuDisabled="true">
                        </ext:Column>
                         <ext:Column ID="Column14" runat="server" Align="Center" Text="Worked Hrs" Width="80"
                            DataIndex="WorkedMinText" Sortable="true" MenuDisabled="true">
                        </ext:Column>
                         <ext:Column ID="Column15" runat="server" StyleSpec='background-color:#FFF2CC;' Align="Center" Text="Additional Salary" Width="120"
                            DataIndex="SalaryType" Sortable="true" MenuDisabled="true">
                            <Editor>
                                <ext:ComboBox runat="server" ID="cmbAddi">
                                    <Items>
                                        <ext:ListItem Text="Full Pay" Value="Full Pay" />
                                        <ext:ListItem Text="Half Pay" Value="Half Pay" />
                                        <ext:ListItem Text="No Pay" Value="No Pay" />
                                    </Items>
                                </ext:ComboBox>
                            </Editor>
                        </ext:Column>
                        <ext:Column ID="Column2" runat="server" Align="Center" Text="Basic Amount" Width="90"
                            DataIndex="BasicAmount" Sortable="true" MenuDisabled="true">
                                <Renderer Fn="getFormattedAmount" />
                        </ext:Column>
                        <ext:Column ID="Column3" runat="server" Align="Center" Text="Pay Amount" Width="90"
                            DataIndex="Amount" Sortable="true" MenuDisabled="true">
                            <Renderer Fn="getFormattedAmount" />
                        </ext:Column>

                    </Columns>
                </ColumnModel>
                <Listeners>
                    <SelectionChange Handler="document.getElementById('spanText').innerHTML = this.getSelectionModel().selected.items.length + ' employees selected';" />
                </Listeners>
                <SelectionModel>
                    <ext:CheckboxSelectionModel ID="CheckboxSelectionModel1" runat="server" Mode="Simple">
                        <Listeners>
                            <BeforeSelect Fn="gridRowBeforeSelect" />
                        </Listeners>
                    </ext:CheckboxSelectionModel>
                </SelectionModel>
                <View>
                    <ext:GridView ID="GridView1" runat="server">
                        <Listeners>
                        </Listeners>
                        <GetRowClass Fn="getRowClass" />
                    </ext:GridView>
                </View>
                <BottomBar>
                     <ext:PagingToolbar ID="PagingToolbar1" runat="server" StoreID="storeEmpList" DisplayInfo="true">
                    </ext:PagingToolbar>
                    
                    <%--<ext:PagingToolbar ID="PagingToolbar1" runat="server" StoreID="storeEmpList" PageSize="50"
                        DisplayInfo="true" DisplayMsg="Displaying employees {0} - {1} of {2}" EmptyMsg="No Employee to display">
                        <Items>
                            <ext:ComboBox runat="server" SelectedIndex="0" Width="80" ID="cmbPageSize">
                                <Items>
                                    <ext:ListItem Text="50" Value="50" />
                                    <ext:ListItem Text="100" Value="100" />
                                </Items>
                                <Listeners>
                                    <Select Handler="#{PagingToolbar1}.pageSize = parseInt( this.getValue()); searchListEL();" />
                                </Listeners>
                            </ext:ComboBox>
                        </Items>
                    </ext:PagingToolbar>--%>
                </BottomBar>
            </ext:GridPanel>
            <div class="buttonBlock" style="width: 68%; margin-top: 20px">
                <table class="fieldTable firsttdskip">
                    <tr>
                        <td style="width: 120px">
                            <ext:Button ID="btnExport" IconAlign="Right" Icon="TableGo" AutoPostBack="true" OnClick="btnExport_Click"
                                runat="server" Text="Export" Width="100" Height="30">
                            </ext:Button>
                        </td>
                        <td style="width: 120px">
                            <ext:Button  ID="btnApprove" Cls="btn btn-primary" runat="server" Text="Approve"
                                Width="100" Height="30">
                                <DirectEvents>
                                    <Click OnEvent="btnApprove_Click">
                                        <EventMask ShowMask="true" />
                                        <Confirmation ConfirmRequest="true" Message="Are you sure, you want to Approve the list?" />
                                        <ExtraParams>
                                            <ext:Parameter Name="gridItems" Value="Ext.encode(#{gridList}.getRowsValues({selectedOnly:true}))"
                                                Mode="Raw" />
                                        </ExtraParams>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                        </td>
                        <td style="width: 120px">
                            <ext:Button Hidden="true" ID="btnPost" Cls="btn btn-primary" runat="server" Text="Post To Salary"
                                Width="120" Height="30">
                                <DirectEvents>
                                    <Click OnEvent="btnPost_Click">
                                        <EventMask ShowMask="true" />
                                        <Confirmation ConfirmRequest="true" Message="Are you sure, you want to Post all approved amounts to salary?" />
                                       <%-- <ExtraParams>
                                            <ext:Parameter Name="gridItems" Value="Ext.encode(#{gridList}.getRowsValues({selectedOnly:true}))"
                                                Mode="Raw" />
                                        </ExtraParams>--%>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                        </td>
                    </tr>
                </table>
            </div>
            <br />
        </div>
    </div>
</asp:Content>
