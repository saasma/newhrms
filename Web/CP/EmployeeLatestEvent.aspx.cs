﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using Utils.Helper;
using Utils.Web;
using BLL.Base;
using DAL;
using Utils.Base;

namespace Web
{


    public partial class EmployeeLatestEvent : BasePage
    {


        protected void Page_Init(object sender, EventArgs e)
        {
            Initialize();
        }

        protected void Page_Load(object sender, EventArgs e)
        {

            
            //if (!IsPostBack)

            if (!Page.IsPostBack)
            {
                //Initialize();

            }

            BindEmployees();
            
        }

        private void Initialize()
        {
            List<Branch> list = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId);

            ddlBranch.DataSource
                = list;
            ddlBranch.DataBind();

            ddlDepartment.DataSource
                = DepartmentManager.GetAllDepartments();

            ddlDepartment.DataBind();

            List<KeyValue> listStatus = new JobStatus().GetMembers();
            listStatus.RemoveAt(0);
            ddlStatus.DataSource = listStatus;
            ddlStatus.DataBind();
        }

        
        

        void BindEmployees()
        {

            gvEmployeeIncome.DataSource = EmployeeManager.GetEmployeeLatestEvent(0, int.MaxValue, "", int.Parse(ddlBranch.SelectedValue), int.Parse(ddlDepartment.SelectedValue), int.Parse(ddlStatus.SelectedValue));
                gvEmployeeIncome.DataBind();

        }


        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            List<GetEmployeeLatestEventDetailsResult> list = 
                EmployeeManager.GetEmployeeLatestEvent(0, int.MaxValue, "", int.Parse(ddlBranch.SelectedValue), int.Parse(ddlDepartment.SelectedValue), int.Parse(ddlStatus.SelectedValue));


            List<string> hiddenList = new List<string>();
            hiddenList.Add("DateOfBirthEng");
            hiddenList.Add("LatestEventDate");
            hiddenList.Add("AppointmentDate");

            Dictionary<string, string> renameList = new Dictionary<string, string>();

            renameList.Add("RowNum", "SN");
            renameList.Add("EmployeeId", "EIN");
            renameList.Add("INo", "I No");
            renameList.Add("DateOfBirthEngText", "Birth Date");
            renameList.Add("LatestEventName", "Latest Event");
            renameList.Add("LatestEventDateText", "Latest Event Date");
            renameList.Add("AppointmentDateText", "Appointment Date");
            renameList.Add("POSITION", "Position");
            renameList.Add("StatusName", "Service Status");

            Bll.ExcelHelper.ExportToExcel("Employee Latest Event", list,
                hiddenList,
            new List<String>() { },
            renameList,
            new List<string>() { }, new List<string> { "Age", "RowNumber", "EmployeeId"}
            , new List<string>() { "DateOfBirthEngText", "LatestEventDateText","AppointmentDateText" }
            , new Dictionary<string, string>() { { "Employee Latest Event", "" } }
           
            , new List<string> { "RowNum", "EmployeeId", "INo", "Name", "DateOfBirthEngText", "LatestEventName", "LatestEventDateText", "AppointmentDateText", "POSITION", "StatusName", "Branch","Department" });

          
        }

        protected void btnLoad_Click(object sender, EventArgs e)
        {
            BindEmployees();
        }
     
    }


}
