﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using BLL.Manager;
using Utils.Base;
using BLL;
using DAL;
using Utils.Web;
using Utils.Helper;
using System.Text.RegularExpressions;
using Ext.Net;

namespace Web.CP
{
    public partial class GradeStep : BasePage
    {
        //LeaveAttendanceManager leaveMgr = new LeaveAttendanceManager();
        List<PositionGradeStepAmount> pgsaList = new List<PositionGradeStepAmount>();
        List<Grade> gradeList = new List<Grade>();
        List<Step> stepList = new List<Step>();
        int companyId = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            companyId = SessionManager.CurrentCompanyId;
            //gradeList = PositionGradeStepAmountManager.GetAllGrades(companyId);
            //stepList = PositionGradeStepAmountManager.GetAllSteps(companyId);
            if (!IsPostBack)
            {
                Initialize();
                //BuildGrid();
            }
            //JavascriptHelper.AttachPopUpCode(Page, "departmentPopup", "ManageDepAsPopup.aspx", 700, 660);
            JavascriptHelper.AttachPopUpCode(Page, "PopUpGrade", "AEGrade.aspx", 600, 600);
            JavascriptHelper.AttachPopUpCode(Page, "PopUpStep", "AEStep.aspx", 600, 600);
            JavascriptHelper.AttachPopUpCode(Page, "PopUpPosition", "AEPosition.aspx", 600, 600);
        }

        private void Initialize()
        {
            ddlPosition.DataSource = PositionGradeStepAmountManager.GetAllPositions(SessionManager.CurrentCompanyId);
            ddlPosition.DataBind();
            ddlPosition.Items.Insert(0, "-- Select Position --");
            ddlPosition.SelectedValue = "0";
        }

        #region Build GridPanel
        void BuildGrid()
        {
            GridPanel grid = this.BuildGridPanel();
            
            //grid.ColumnModel.Columns.Add(new Column { Header = "Month", DataIndex = "MonthName" });
            grid.ColumnModel.Columns.AddRange(GetColumns());
            grid.AddTo(divGrid);
        }

        Column[] GetColumns()
        {

            Column[] cols = new Column[gradeList.Count];
            Column newCol = null;
            int i = 0;
            foreach (Grade grade in gradeList)
            {
                newCol = new Column();
                newCol.Text = grade.Name;
                newCol.DataIndex = "G" + grade.GradeId.ToString();
                cols[i] = newCol;
                i++;

            }
            return cols;
        }

        private GridPanel BuildGridPanel()
        {
            return new GridPanel
            {
                ID = "gridPositionGradeStepAmount",
                Height = 300,
                Width = 400,
                Border = false,
                //StripeRows = true,
                //TrackMouseOver = true,
                Store =  
            {
                this.BuildStore()
            },
                SelectionModel = 
            { 
                new RowSelectionModel {  }
            }
            };
        }

        private Store BuildStore()
        {
            Store store = new Store();
            store.ID = "storePositionGradeStepManger";
            Model arr = new Model();
            //arr.Fields.Add(new RecordField("MonthName"));
            arr.Fields.AddRange(GetRecordField());
            //store.Reader.Add(arr);
            store.Model.Add(arr);
            store.DataSource = GetData();
            store.DataBind();

            return store;
        }

        protected object[] GetData()
        {

            //  List<LeaveAdjustment> leaveAdjustments = LeaveAttendanceManager.GetLeaveSummary(empId);
            //  int startingPayrollPeriodId = -1, endingPayrollPeriodId = -1;
            //  bool readingSumForMiddleFiscalYearStartedReqd = false;

            //CommonManager.GetLastPayrollPeriod(ref readingSumForMiddleFiscalYearStartedReqd, ref startingPayrollPeriodId, ref endingPayrollPeriodId);
            //List<PayrollPeriod> periods = LeaveAttendanceManager.GetPayrollPeriodList(empId);

            //Dictionary<string, string> PayrollLeave = new Dictionary<string, string>();
            Dictionary<string, decimal> gradeStepAmount = new Dictionary<string, decimal>();
            foreach (PositionGradeStepAmount pgsa in pgsaList)
            {
                    gradeStepAmount.Add
                        (
                            string.Concat
                            (
                                string.Concat(pgsa.GradeId.ToString(), "_"),pgsa.StepId.ToString()
                            ),
                            pgsa.Amount
                        );
                    //PayrollLeave.Add
                    //    (
                    //        string.Concat
                    //        (
                    //            string.Concat(lvAdj.PayrollPeriodId.ToString(), "_"), lvAdj.LeaveTypeId.ToString()
                    //        ),
                    //        (string)lvAdj.Taken.ToString()
                    //    );
            }

            //object[] data = new object[periods.Count];
            object[] data = new object[stepList.Count];

            for (int i = 0; i < stepList.Count; i++)
            {
                object[] rowData = new object[gradeList.Count + 1];

                int columnIndex = 0;
                //rowData[columnIndex] = stepList[i].Name.Remove(gradeList[i].Name.IndexOf("/"));


                for (int j = 0; j < gradeList.Count; j++)
                {

                    if (gradeStepAmount.ContainsKey(gradeList[i].GradeId + "_" + stepList[j].StepId))
                    {
                        //       rowData[columnIndex++] = periods[i].Name.Remove(periods[i].Name.IndexOf("/"));
                        rowData[++columnIndex] = gradeStepAmount[gradeList[i].GradeId + "_" + stepList[j].StepId].ToString();
                    }
                    else
                    {

                        rowData[++columnIndex] = null;
                    }
                    //columnIndex++;
                }

                data[i] = rowData;
            }
            //for (int i = 0; i < periods.Count; i++)
            //{
            //    object[] rowData = new object[leaves.Count + 1];

            //    int columnIndex = 0;
            //    rowData[columnIndex] = periods[i].Name.Remove(periods[i].Name.IndexOf("/"));


            //    for (int j = 0; j < leaves.Count; j++)
            //    {

            //        if (PayrollLeave.ContainsKey(periods[i].PayrollPeriodId + "_" + leaves[j].LeaveTypeId))
            //        {
            //            //       rowData[columnIndex++] = periods[i].Name.Remove(periods[i].Name.IndexOf("/"));
            //            rowData[++columnIndex] = PayrollLeave[periods[i].PayrollPeriodId + "_" + leaves[j].LeaveTypeId].ToString();
            //        }
            //        else
            //        {

            //            rowData[++columnIndex] = null;
            //        }
            //        //columnIndex++;
            //    }

            //    data[i] = rowData;
            //}
            return data;


        }

        ModelField[] GetRecordField()
        {
            ModelField[] fields = new ModelField[gradeList.Count];
            int i = 0;
            ModelField recordfield = null;
            foreach (Grade grade in gradeList)
            {
                recordfield = new ModelField("L" + grade.GradeId.ToString());
                fields[i] = recordfield;
                i++;
            }

            return fields;
        }


        #endregion

        

    }
}
