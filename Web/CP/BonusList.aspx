<%@ Page Title="Bonus List" Language="C#" MasterPageFile="~/Master/HROld.Master"
    AutoEventWireup="true" CodeBehind="BonusList.aspx.cs" Inherits="Web.CP.BonusList" %>

<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">

        function insertUpdateGratuityClassCall(classId) {
            var ret = insertUpdateGratuityClass('Id=' + classId);

            if (typeof (ret) != 'undefined') {
                if (ret == 'Reload') {
                    __doPostBack('Reload', '');
                }
            }
        }
        function insertUpdateGratuityRuleCall(gratuityRuleId) {
            var ret = insertUpdateGratuityRule('Id=' + gratuityRuleId);

            if (typeof (ret) != 'undefined') {
                if (ret == 'Reload') {
                    __doPostBack('Reload', '');
                }
            }

            return false;
        }


        function refresh(popupWindow) {
            popupWindow.close();
            __doPostBack('Reload', '');
        }

        function changeStates(chk) {
            var isChecked = $(chkAll).is(':checked');

            $("INPUT[type='checkbox']").attr('checked', isChecked);

        }

        function importPopupProcess(bonusId) {


            var ret = importPopup("bonusId=" + bonusId);


            return false;
        }

        function viewCalc(bonusId) {
            window.location = 'BonusDetails.aspx?id=' + bonusId;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Bonus Distribution
                </h4>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="hdnBonusId" runat="server" />
    <div class="contentpanel">
        <div class="contentArea">
            <asp:LinkButton ID="Button1" class="btn btn-success btn-sm btn-sect" runat="server"
                Text="New Bonus Distribution" Style="height: 30px; width: 150px; margin-bottom: 5px;"
                OnClientClick="insertUpdateGratuityRuleCall();return false;" />
            <uc2:MsgCtl ID="divMsgCtl" EnableViewState="false" Hide="true" runat="server" />
            <uc2:WarningCtl ID="divWarningMsg" EnableViewState="false" Hide="true" runat="server" />
            <br />
            <div>
                <asp:GridView CssClass="table table-primary mb30 table-bordered table-hover" UseAccessibleHeader="true"
                    ID="gvwGratuityRules" showheaderwhenempty="True" runat="server" AutoGenerateColumns="False"
                    GridLines="None" DataKeyNames="BonusId" OnRowDeleting="gvwDepartments_RowDeleting"
                    showfooterwhenempty="True" Width="1100px">
                    <Columns>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"
                             HeaderText="Name">
                            <ItemTemplate>
                                <asp:Label ID="Label1" Text='<%#   Eval("Name")%>' runat="server" Width="100px" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"
                            HeaderStyle-Width="100px" HeaderText="Bonus Year">
                            <ItemTemplate>
                                <%#     GetYear(Eval("YearId"))%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"
                            HeaderStyle-Width="100px" HeaderText="Paid Period">
                            <ItemTemplate>
                                <%#     (Eval("PaidPeriod"))%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"
                            HeaderStyle-Width="120px" HeaderText="Distribution Date">
                            <ItemTemplate>
                                <%#     (Eval("DistributionDateEng","{0:yyyy-MMM-dd}"))%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right"
                            HeaderText="Allocated Bonus">
                            <ItemTemplate>
                                <asp:Label Style='text-align: right' Text='<%#     GetCurrency(Eval("BonusAmount"))%>'
                                    runat="server" Width="100px" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right"
                            HeaderText="Distributed Bonus">
                            <ItemTemplate>
                                <asp:Label ID="Label2" Style='text-align: right' Text='<%#     GetCurrency(Eval("DistributedAmount"))%>'
                                    runat="server" Width="80px" />
                            </ItemTemplate>
                        </asp:TemplateField>
                         <asp:TemplateField ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right"
                            HeaderText="Total Annual Salary">
                            <ItemTemplate>
                                <asp:Label ID="Label22" Style='text-align: right' Text='<%#     GetCurrency(Eval("TotalAnnualSalary"))%>'
                                    runat="server" Width="100px" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right"
                            HeaderText="Bonus %">
                            <ItemTemplate>
                                <asp:Label ID="Label2222" Style='text-align: right' Text='<%#    Convert.ToDecimal(Eval("BonusPercent")) * 100%>'
                                    runat="server" Width="60px" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"
                            HeaderStyle-Width="120px" HeaderText="Eligible Employees">
                            <ItemTemplate>
                                <%#     (Eval("NoOfEmployees"))%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="50px">
                            <ItemTemplate>
                                <asp:HyperLink runat="server" ID="b1" Width="100px" Text="Go to Calculation" NavigateUrl='<%# GetBonusLink(Eval("BonusId"))  %>'
                                    />
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="60px">
                            <ItemTemplate>
                                <input type='image' style='border-width: 0px; vertical-align: sub;' onclick='return insertUpdateGratuityRuleCall(<%# Eval("BonusId") %>);'
                                    src='../images/edit.gif' />
                                &nbsp;
                                <asp:ImageButton ID="ImageButton2" Style='vertical-align: sub' OnClientClick="return confirm('Confirm delete the Bonus?')"
                                    runat="server" CommandName="Delete" ImageUrl="~/images/delet.png" />
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                        </asp:TemplateField>
                    </Columns>
                    <RowStyle CssClass="odd" />
                    <AlternatingRowStyle CssClass="even" />
                </asp:GridView>
                <br />
                <br />
            </div>
        </div>
    </div>
</asp:Content>
