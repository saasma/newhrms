﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL;
using BLL;
using BLL.Manager;
using BLL.Base;

namespace Web.CP
{
    public partial class HREvaluationPeriod : BasePage
        //System.Web.UI.Page
    {
        public string SaveMode
        {
            get { return this.hdSaveMode.Value; }
            set { this.hdSaveMode.Value = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                Initialize();
            }
        }

        protected void gvPosition_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvPeriod.PageIndex = e.NewPageIndex;
            BindPeriod();
        }

        protected void btnOk_Click(object sender, EventArgs e)
        {
            if (SaveMode.Equals("0"))
            {
                string errMsg = string.Empty;
                EvaluationPeriod period = new EvaluationPeriod();
                period.Name = txtFriendlyName.Text.Trim();
                period.FromDate = txtFromDate.SelectedDate.ToString();
               
                period.ToDate = txtToDate.SelectedDate.ToString();
          
                if (StatusCheckBox.Checked == true)
                {
                    period.IsActive = true;
                }
                else
                {
                    period.IsActive = false;
                }
               

                //.Order = int.Parse(txtOrder.Text.Trim() == "" ? "0" : txtOrder.Text.Trim());
                period.CompanyId = SessionManager.CurrentCompanyId;
                period.CreatedOn = System.DateTime.Now;
                period.CreatedBy = SessionManager.CurrentLoggedInEmployeeId;
                if (PositionGradeStepAmountManager.SavePeriod(period, ref errMsg))
                {
                    ClearField();
                    BindPeriod();
                }
                else
                {
                    divWarningMsg.InnerHtml = errMsg;
                    divWarningMsg.Hide = false;
                }
            }
            else
            {
                string errMsg = string.Empty;
                EvaluationPeriod period = new EvaluationPeriod();
                period.EvaluationPeriodId = int.Parse(hdPeriodId.Value);
                period.Name = txtFriendlyName.Text.Trim();
                period.FromDate = txtFromDate.SelectedDate.ToString();
                period.ToDate = txtToDate.SelectedDate.ToString();
                if (StatusCheckBox.Checked==true)
                {
                    period.IsActive = true;
                }
                else
                {
                    period.IsActive = false;
                }
               
                //period.Order = int.Parse(txtOrder.Text.Trim() == "" ? "0" : txtOrder.Text.Trim());
                period.CompanyId = SessionManager.CurrentCompanyId;
                period.ModifiedOn = System.DateTime.Now;
                period.ModifiedBy = SessionManager.CurrentLoggedInEmployeeId;
                if (PositionGradeStepAmountManager.UpdatePeriod(period, ref errMsg))
                {
                    ClearField();
                    BindPeriod();
                    hdPeriodId.Value = "0";
                    SaveMode = "0";
                }
                else
                {
                    divWarningMsg.InnerHtml = errMsg;
                    divWarningMsg.Hide = false;
                }
            }
        }



        protected void gvPeriod_OnRowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            string errMessage = string.Empty;
            int id = (int)gvPeriod.DataKeys[e.RowIndex][0];
            if (!PositionGradeStepAmountManager.CanEvalulationPeriodBeDeleted(id, ref errMessage))
            {
                divWarningMsg.InnerHtml = errMessage;
                divWarningMsg.Hide = false;
                return;

            }
            if (PositionGradeStepAmountManager.DeletePeriod(id))
            {
                ClearField();
                BindPeriod();
            }

        }



        void Initialize()
        {

            //txtFromDate.IsEnglishCalendar = SessionManager.IsEnglish;
            //txtToDate.IsEnglishCalendar = SessionManager.IsEnglish;
            txtFromDate.IsSkipDay = true;
            txtToDate.IsSkipDay = true;
            txtFromDate.SelectTodayDate();
            txtToDate.SelectTodayDate();

            BindPeriod();
           

        }

        void BindPeriod()
        {
            List<EvaluationPeriod> list = PositionGradeStepAmountManager.GetAllPeriods(SessionManager.CurrentCompanyId);
            gvPeriod.DataSource = list;
            gvPeriod.DataBind();
            if (list.Count == 0)
            {
                txtFriendlyName.Text = " ";
            }
            else
            {
                txtFriendlyName.Text = " ";
            }
              
        }

        void ClearField()
        {
            hdPeriodId.Value = "0";
            SaveMode = "0";
            //txtOrder.Text = string.Empty;
            txtFriendlyName.Text = string.Empty;
            //txtFromDate.SetSelectedDate(string datestr , IsEnglish);
            txtToDate.SelectedDate.TestDate();
            StatusCheckBox.Checked = true;
            btnSave.Text = "Save";

            //.SelectedItem.Value = string.Empty;
            divWarningMsg.Hide = true;
        }

        protected void gvPeriod_RowCommand(object sender, GridViewCommandEventArgs e)
        {

            
        }

        protected void gvPeriod_RowEditing(object sender, GridViewEditEventArgs e)
        {

            int EvaluationPeriodId = (int)gvPeriod.DataKeys[e.NewEditIndex]["EvaluationPeriodId"];

            EvaluationPeriod period = PositionGradeStepAmountManager.GetEvaluatioPeriod(EvaluationPeriodId);

            if (period != null)
            {
                hdPeriodId.Value = EvaluationPeriodId.ToString();
                txtFriendlyName.Text = period.Name;

                txtFromDate.SetSelectedDate(period.FromDate, true);
                txtToDate.SetSelectedDate(period.ToDate, true);
                StatusCheckBox.Checked = period.IsActive.Value;

                btnSave.Text = "Update";

                hdSaveMode.Value = "1";
            }

            e.Cancel = true;
        }
    }
}