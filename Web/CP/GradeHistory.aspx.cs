﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using DAL;
using BLL.Base;
using Utils.Web;

namespace Web.CP
{
    public partial class GradeHistory : BasePage
    {
        private CommonManager comm = new CommonManager();
        private List<EGradeHistory> list;

     
        protected void Page_Load(object sender, EventArgs e)
        {
            this.CustomId = int.Parse(Request.QueryString["EIN"]);          
           
            if (!IsPostBack)
            {
                Initialise();               
            }
            Page.ClientScript.RegisterOnSubmitStatement(this.GetType(), "sdfdsffd", "clearUnload();");
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (gvw.SelectedIndex == -1)
                RegisterLastFromDate();
            else
            {
                int currentGradeHistory = (int)gvw.DataKeys[gvw.SelectedIndex][0];

                EGradeHistory prevGradeHistory = CommonManager.GetPreviousGradeHistory(currentGradeHistory);

                if (prevGradeHistory != null)
                {
                    RegisterLastFromDateScript(prevGradeHistory);
                }
            }

            List<EGradeHistory> grades = CommonManager.GetGradeHistory(CustomId);
            int gradeId = -1;
            if (grades.Count > 0)
                gradeId = grades[grades.Count - 1].GradeId.Value;

            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "changedValue", "var value=" + gradeId + ";", true);

        }
        private void Initialise()
        {

            ddlGrade.DataSource = comm.GetAllGrades();
            ddlGrade.DataBind();

            calFromDate.IsEnglishCalendar = IsEnglish; calFromDate.SelectTodayDate();

            BindData();

           

            

        }

        private void RegisterLastFromDate()
        {
            List<EGradeHistory> grades = CommonManager.GetGradeHistory(CustomId);

            if (grades.Count > 0)
            {
                EGradeHistory lastGradeHistory = grades[grades.Count - 1];

                RegisterLastFromDateScript(lastGradeHistory);

            }

        }

        public bool IsEditable(int index)
        {
            if (list != null && list.Count>0)
            {
                if (list.Count - 1 == index)
                    return true;
            }
            return false;
        }

        private void RegisterLastFromDateScript(EGradeHistory lastGradeHistory)
        {
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "regfromdate",
               string.Format("var fromDate = '{0}';", lastGradeHistory.FromDate), true);
        }

        private void BindData()
        {
            PayManager mgr = new PayManager();


            list = CommonManager.GetGradeHistory(CustomId);
            gvw.DataSource = list;
            gvw.DataBind();


           
        }

       

        protected void gvw_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvw.PageIndex = e.NewPageIndex;
            BindData();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {

            if (Page.IsValid)
            {
                EGradeHistory history = Process(null);
                if (gvw.SelectedIndex != -1)
                {
                    
                    history.GradeHistoryId = (int)gvw.DataKeys[gvw.SelectedIndex][0];
                    CommonManager.UpdateGradeHistory(history, gvw.Rows.Count == 1);
                    
                    divMsgCtl.InnerHtml = Resources.Messages.GradeHistoryUpdated;
                }
                else
                {
                    CommonManager.SaveGradeHistory(history);
                    divMsgCtl.InnerHtml = Resources.Messages.GradeHistorySaved;
                }
                divMsgCtl.Hide = false;

                ClearFields();
                gvw.SelectedIndex = -1;
                BindData();
                RegisterLastFromDate();
            }
        }

        private EGradeHistory Process(EGradeHistory entity)
        {
            if (entity == null)
            {
                entity = new EGradeHistory();
                entity.GradeId = int.Parse(ddlGrade.SelectedValue);
                entity.FromDate = calFromDate.SelectedDate.ToString();
                entity.FromDateEng = GetEngDate(entity.FromDate);
                entity.EmployeeId = this.CustomId;
                return entity;
            }
            else
            {
                UIHelper.SetSelectedInDropDown(ddlGrade, entity.GradeId);
                calFromDate.SetSelectedDate(entity.FromDate, IsEnglish);
            }
            return null;
        }

        protected void gvw_SelectedIndexChanged(object sender, EventArgs e)
        {
            //first record 
            int currentGradeHistory = (int)gvw.DataKeys[gvw.SelectedIndex][0];

            //EGradeHistory prevGradeHistory = CommonManager.GetPreviousGradeHistory(currentGradeHistory);

            //if (prevGradeHistory != null)
            //{
            //    RegisterLastFromDateScript(prevGradeHistory);
            //}

            EGradeHistory history = CommonManager.GetGradeHistoryById(currentGradeHistory);
            Process(history);
            btnSave.Text = Resources.Messages.Update;

            //if first one then disable from date as it should be the same of emp first status from date
            if (CommonManager.GetGradeHistory(this.CustomId)[0].GradeHistoryId == history.GradeHistoryId)
            {
                calFromDate.ToolTip = Resources.Messages.HistoryFirstFromDateNotEditable;
                calFromDate.Enabled = false;
            }
            else
            {
                calFromDate.Enabled = true; ;
                calFromDate.ToolTip = "";
            }

        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            gvw.SelectedIndex = -1;
            BindData();
            ClearFields();
        }


        private void ClearFields()
        {
            calFromDate.Enabled = true;
            calFromDate.ToolTip = "";
            btnSave.Text = Resources.Messages.Save;
            UIHelper.SetSelectedInDropDown(ddlGrade, -1);
            calFromDate.SelectTodayDate();
        }

      
    }
}
