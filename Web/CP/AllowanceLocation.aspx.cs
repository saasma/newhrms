﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using System.Xml;
using Utils.Helper;

namespace Web.CP
{
    public partial class AllowanceLocation : BasePage
    {

        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.HumanResource;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();
              
            }

            JavascriptHelper.AttachNonDialogPopUpCode(Page, "allowancePopup", "../ExcelWindow/TravelAllowanceExcel.aspx", 450, 500);
        }

        public void Initialise()
        {

            LoadLocation();
            LoadLevels();
        }
        
        private void LoadLevels()
        {
            GridLevels.GetStore().DataSource = TravelAllowanceManager.GetAllTravelAllowanceLocations();
            GridLevels.GetStore().DataBind();

        }

        Column GetExtGridPanelColumn(string indexId, string headerName)
        {
            Column column = new Column();
            column.DataIndex = indexId;
            column.DataIndex = indexId;
            column.Text = headerName;
            column.Align = Alignment.Right;
            column.ToolTip = headerName;
            column.MenuDisabled = true;
            column.Sortable = false;
            column.Width = new Unit(140);
          

            //if (headerName == "Total")
            column.Renderer.Fn = "getFormattedAmount";


            return column;
        }


        public void LoadLocation()
        {
           

        }
        
        public void ClearLevelFields()
        {
            hiddenValue.Text = "";
            txtName.Text = "";

        }

      
        
        protected void btnAddLevel_Click(object sender, DirectEventArgs e)
        {
            
            ClearLevelFields();
            WindowLevel.Show();
        }

        protected void btnLoadRate_Click(object sender, DirectEventArgs e)
        {

        }
        protected void btnDeleteLevel_Click(object sender, DirectEventArgs e)
        {
            int TAllowance = int.Parse(hiddenValue.Text.Trim());
            Status status = TravelAllowanceManager.DeleteTravelAllowanceLocation(TAllowance);
            if (status.IsSuccess)
            {
                LoadLevels();
                NewMessage.ShowNormalPopup("Location deleted.");
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }
        }

        

        protected void btnEditLevel_Click(object sender, DirectEventArgs e)
        {
            int levelId = int.Parse(hiddenValue.Text.Trim());
            TAAllowanceLocation entity = TravelAllowanceManager.GetTravelAllowanceByLocId(levelId);
            txtName.Text = entity.LocationName;


            WindowLevel.Show();

        }






        protected void btnLevelSaveUpdate_Click(object sender, DirectEventArgs e)
        {

            Page.Validate("InsertUpdateLevel");
            if (Page.IsValid)
            {
                TAAllowanceLocation locInstance = new TAAllowanceLocation();
                int allowanceID;
                locInstance.LocationName = txtName.Text.Trim();


                bool isInsert = true;
                
                if (!string.IsNullOrEmpty(hiddenValue.Text))
                {
                    locInstance.LocationId = int.Parse(hiddenValue.Text.Trim());
                    isInsert = false;
                }

                Status status = TravelAllowanceManager.InsertUpdateAllowanceLocation(locInstance, isInsert);
                if (status.IsSuccess)
                {
                    WindowLevel.Hide();
                    LoadLevels();
                    NewMessage.ShowNormalPopup("Location saved.");
                }
                else
                {
                    NewMessage.ShowWarningMessage(status.ErrorMessage);
                }

            }
             
             
        }
 
    }
}

/*
 
 
 
*/