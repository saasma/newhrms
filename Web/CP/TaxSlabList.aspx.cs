﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using BLL.BO;
using System.IO;
using System.Text;


namespace Web.CP
{
    public partial class TaxSlabList : BasePage
    {
        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.Payroll;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Inititalise();
            }
        }

        private void Inititalise()
        {
            List<TaxDetail> list = NewPayrollManager.GetTaxDetailList();
            gridTaxDetails.Store[0].DataSource = list;
            gridTaxDetails.Store[0].DataBind();

            if (list.Count > 0 && !string.IsNullOrEmpty(list[0].EffectiveDate))
                txtEffectiveDate.Text = list[0].EffectiveDate;
        }

        protected void btnAdd_Click(object sender, DirectEventArgs e)
        {
            List<TaxDetail> list = NewPayrollManager.GetTaxDetailList();
            gridTaxDetailsAdd.Store[0].DataSource = list;
            gridTaxDetailsAdd.Store[0].DataBind();

            calEffectiveDate.Clear();

            WTaxDetails.Center();
            WTaxDetails.Show();
        }

        protected void btnSave_Click(object sender, DirectEventArgs e)
        {
            Page.Validate("SaveUpdate");
            if (Page.IsValid)
            {
                string gridItemsJSON = e.ExtraParams["gridItems"];
                List<TaxDetail> list = JSON.Deserialize<List<TaxDetail>>(gridItemsJSON);

                //SaveUpdateTaxDetails

                string effectiveDate = calEffectiveDate.Text.Trim();
                DateTime effectiveDateEng = GetEngDate(effectiveDate);

                foreach (var item in list)
                {
                    //item.ID = Guid.NewGuid();

                    if (item.EndingRange == 0)
                        item.EndingRange = null;

                    item.EffectiveDate = effectiveDate;
                    item.EffectiveDateEng = effectiveDateEng;
                }

                Status status = NewPayrollManager.SaveUpdateTaxDetails(list);
                if (status.IsSuccess)
                {
                    WTaxDetails.Close();
                    NewMessage.ShowNormalMessage(status.ErrorMessage);

                    Inititalise();
                }
                else
                    NewMessage.ShowWarningMessage(status.ErrorMessage);

                
            }
        }


    }
}