﻿<%@ Page Title="Mannual Leave Carry Forward" Language="C#" AutoEventWireup="true" MasterPageFile="~/Master/NewDetails.Master"
    CodeBehind="MannualLeaveCarryForward.aspx.cs" Inherits="Web.NewHR.MannualLeaveCarryForward" %>

<%@ Register Src="~/Controls/FooterCtl.ascx" TagName="FooterCtl" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/HeaderCtl.ascx" TagName="HeaderCtl" TagPrefix="uc3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .holiday, .holiday a, .holiday td
        {
            color: #469146;
            background-color: #F0FFF0 !important;
        }
        .leave, .leave a, .leave td
        {
            color: #D95CA9;
            background-color: #FFF0F5 !important;
        }
        
        .weeklyholiday, .weeklyholiday a, .weeklyholiday td
        {
            color: #8F8F1A;
            background-color: #FAFAD2 !important;
        }
    </style>
    <script type="text/javascript">


   var getRowClass = function (record) {
    
        var status = record.data.PaymentStatus;
        

        if(status  == "Paid")
         {
            return "holiday";
         }
        
         else //if(dayValue=="Working Day")
         {
            return "";
         }
         //else 

      };

  
    var CommandHandler = function (command, record) {
            
        };

    function searchList() {
        <%=PagingToolbar1.ClientID %>.doRefresh();
    }

    var gridRowBeforeSelect = function (record, index,e1,e2,e3) {
         var gridrecord = <%=gridList.ClientID %>.getStore().getAt(index.index);
        
        if(typeof(gridrecord) != 'undefined')
        {
        if(gridrecord.data.PaymentStatus=="Paid")
        {
            return false;
        }
        }
    };
       
    var carryForwardRenderer = function(e1,e2,e3)
    {
        if(e3.data.MannualCarryForward == null)
            return "";
           
        if(parseFloat(e3.data.Balance) != parseFloat(e3.data.MannualCarryForward))
        {
            return "<span style='color:#994500'>" + e3.data.MannualCarryForward  + "</span>";
        }

        return e3.data.MannualCarryForward;
    }

    var clearAll = function()
    {
        var store = <%= Store3.ClientID %>;

        for(i=0;i<store.data.items.length;i++)
        {
            row = store.data.items[i];

            row.data.MannualCarryForward = null;

            row.commit();
        }
    }

    var fillAll = function()
    {
        var store = <%= Store3.ClientID %>;

        for(i=0;i<store.data.items.length;i++)
        {
            row = store.data.items[i];

            row.data.MannualCarryForward = row.data.Balance;

            row.commit();
        }


    }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <script type="text/javascript" src="<%= ResolveUrl("~/Employee/override.js?v=") + Web.Helper.WebHelper.Version %>"></script>
    <%-- <ext:ResourceManager ID="ResourceManager1" runat="server" DisableViewState="false"
        ShowWarningOnAjaxFailure="false" ScriptMode="Release" Namespace="CompanyX" />--%>
    <ext:Hidden ID="hdnEmployeeID" runat="server" />
    <ext:Hidden ID="hdnOTDate" runat="server" />
    <ext:Hidden ID="hdnIsApprovedOnly" runat="server" />
    <ext:Hidden ID="hdnPeriodId" runat="server" />
    <ext:Hidden ID="hdnSortBy" runat="server" />
    <div class="contentpanel">
        <div class="innerLR">
            <h4>Mannual Leave Carry Forward</h4>
              <span runat="server"  id="salaryNotSaved" style="color: #FF3400">
              Attendance need to be saved from Attendance Registry page for the effect of changes.</span>

            <div class="alert alert-info" style="margin-bottom: 0px;">
                <table>
                    <tr>
                        <td>
                            <ext:ComboBox Width="100" MarginSpec="0 5 0 5" LabelSeparator="" LabelAlign="Top"
                                QueryMode="Local" ID="cmbYear" ForceSelection="true" DisplayField="Year" ValueField="Year"
                                runat="server" FieldLabel="Year">
                                <Store>
                                    <ext:Store ID="Store1" runat="server">
                                        <Model>
                                            <ext:Model ID="Model3" IDProperty="Year" runat="server">
                                                <Fields>
                                                    <ext:ModelField Name="Year" Type="String" />
                                                    <ext:ModelField Name="Year" Type="String" />
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                            </ext:ComboBox>
                        </td>
                        <td>
                            <ext:ComboBox Width="100" MarginSpec="0 5 0 5" LabelSeparator="" LabelAlign="Top"
                                QueryMode="Local" ID="cmbMonth" ForceSelection="true" DisplayField="Value" ValueField="Key"
                                runat="server" FieldLabel="Month">
                                <Store>
                                    <ext:Store ID="Store2" runat="server">
                                        <Model>
                                            <ext:Model ID="Model4" IDProperty="Key" runat="server">
                                                <Fields>
                                                    <ext:ModelField Name="Key" Type="String" />
                                                    <ext:ModelField Name="Value" Type="String" />
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                            </ext:ComboBox>
                        </td>
                       

                        <td style="width: 200px;">
                            <ext:Store runat="server" ID="storeSearch" AutoLoad="false">
                                <Proxy>
                                    <ext:AjaxProxy Url="../Handler/EmpSearchID.ashx">
                                        <ActionMethods Read="GET" />
                                        <Reader>
                                            <ext:JsonReader Root="plants" TotalProperty="total" />
                                        </Reader>
                                    </ext:AjaxProxy>
                                </Proxy>
                                <Model>
                                    <ext:Model ID="Model2" IDProperty="Name" Name="ItemLineModel" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="Name" Type="String" />
                                            <ext:ModelField Name="EmployeeId" Type="String" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                            <ext:ComboBox LabelSeparator="" FieldLabel="Employee" ID="cmbSearch" LabelAlign="Top"
                                LabelWidth="70" runat="server" DisplayField="Name" ValueField="EmployeeId" EmptyText="Search"
                                StoreID="storeSearch" TypeAhead="false" Width="200" HideBaseTrigger="true" MinChars="1"
                                TriggerAction="All" ForceSelection="true">
                                <ListConfig LoadingText="Searching..." MinWidth="200">
                                    <ItemTpl ID="ItemTpl1" runat="server">
                                        <Html>
                                            <tpl>
                                            <div class="search-item">
                                                            <span>{Name}</span>  
                                             </div>
					                </tpl>
                                        </Html>
                                    </ItemTpl>
                                </ListConfig>
                                <Triggers>
                                    <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                                </Triggers>
                                <Listeners>
                                    <Select Handler="this.getTrigger(0).show();" />
                                    <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                    <TriggerClick Handler="if (index == 0) { 
                                           this.clearValue(); 
                                           this.getTrigger(0).hide();
                                       }" />
                                </Listeners>
                            </ext:ComboBox>
                        </td>
                        <td style="width: 20px">
                        </td>
                        <td valign="bottom">
                            <ext:Button runat="server" Width="100" Height="30" Cls="btn btn-primary" ID="btnLoad"
                                Text="Load">
                                <Listeners>
                                    <Click Handler="searchList();" />
                                </Listeners>
                            </ext:Button>
                        </td>
                        <td style='padding-top: 20px;'>
                            <asp:LinkButton OnClientClick='fillAll();return false;' style='padding-left:20px' runat="server" Text="Fill All">
                                
                            </asp:LinkButton>
                            <asp:LinkButton ID="LinkButton1" OnClientClick='clearAll();return false;' style='padding-left:20px' runat="server" Text="Clear All">
                                
                            </asp:LinkButton>
                        </td>
                    </tr>
                </table>
            </div>
            <br />
            

            <ext:GridPanel ID="gridList" runat="server" Header="true" AutoScroll="true">
                <Store>
                    <ext:Store ID="Store3" runat="server" OnReadData="Store_ReadData" RemoteSort="true"
                        AutoLoad="false" PageSize="50"  GroupField="Name">
                        <Proxy>
                            <ext:PageProxy />
                        </Proxy>
                        <Sorters>
                            <ext:DataSorter Direction="ASC" Property="Name" />
                        </Sorters>
                        <AutoLoadParams>
                            <ext:Parameter Name="start" Value="0" Mode="Raw" />
                        </AutoLoadParams>
                        <Model>
                            <ext:Model ID="Model1" runat="server" >
                                <Fields>
                                    <ext:ModelField Name="SN" />
                                    <ext:ModelField Name="EIN" />
                                    <ext:ModelField Name="Name" />

                                    <ext:ModelField Name="Title" />

                                    <ext:ModelField Name="BF" />
                                    <ext:ModelField Name="Earned" />
                                    <ext:ModelField Name="Taken" />
                                    <ext:ModelField Name="Lapsed" />
                                    <ext:ModelField Name="Balance" />
                                  <ext:ModelField Name="MannualCarryForward" />

                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
              
              <Plugins>
                    <ext:CellEditing ID="CellEditing1" ClicksToEdit="1" runat="server">
                    </ext:CellEditing>
                </Plugins>
                <ColumnModel ID="ColumnModel2" runat="server">
                    <Columns>
                       <ext:RowNumbererColumn runat="server" Text="SN" Width="50" />
                        <ext:Column ID="colEId"  runat="server" Text="EIN" Width="60" DataIndex="EIN"
                            Align="Center" Sortable="true" MenuDisabled="true">
                        </ext:Column>
                        <ext:Column ID="Column1"  runat="server" Text="Name" Width="150" DataIndex="Name"
                            Sortable="true" MenuDisabled="true">
                        </ext:Column>
                        <ext:Column ID="Column10"  runat="server" Text="Leave" Width="110"
                            DataIndex="Title" Sortable="true" MenuDisabled="true">
                        </ext:Column>
                         <ext:Column ID="Column3"  Align="Center"  runat="server" Text="Opening" Width="130"
                            DataIndex="BF" Sortable="true" MenuDisabled="true">
                        </ext:Column>
                        <ext:Column ID="Column11"  Align="Center"  runat="server" Text="Accrued for the year" Width="130"
                            DataIndex="Earned" Sortable="true" MenuDisabled="true">
                        </ext:Column>
                        <ext:Column ID="Column12"  Align="Center"  runat="server" Text="Taken during the year" Width="130"
                            DataIndex="Taken" Sortable="true" MenuDisabled="true">
                        </ext:Column>
                          <ext:Column ID="Column5" Align="Center"  runat="server" Text="Lapsed" Width="120"
                            DataIndex="Lapsed" Sortable="true" MenuDisabled="true">
                        </ext:Column>
                         <ext:Column ID="Column6"  Align="Center"  runat="server" Text="Closing Balance" Width="130"
                            DataIndex="Balance" Sortable="true" MenuDisabled="true">
                        </ext:Column>
                    
                     <ext:Column ID="Column2"  Align="Center"  runat="server" Text="Optional carryforward" Width="150"
                            DataIndex="MannualCarryForward" Sortable="true" MenuDisabled="true">
                            <Editor>
                                <ext:NumberField  runat="server" />
                            </Editor>
                            <Renderer Fn="carryForwardRenderer" />
                        </ext:Column>
                    

                    </Columns>
                </ColumnModel>
                

                

                <View>
                    <ext:GridView ID="GridView1" runat="server">
                        <Listeners>
                        </Listeners>

                    </ext:GridView>
                </View>
                <BottomBar>
                    <%-- <ext:PagingToolbar ID="PagingToolbar1" runat="server" StoreID="storeEmpList" DisplayInfo="true">
                    </ext:PagingToolbar>--%>
                    
                    <ext:PagingToolbar ID="PagingToolbar1" runat="server" StoreID="storeEmpList" PageSize="50"
                        DisplayInfo="true" DisplayMsg="Displaying employees {0} - {1} of {2}" EmptyMsg="No Employee to display">
                        <Items>
                            <ext:ComboBox runat="server" SelectedIndex="0" Width="80" ID="cmbPageSize">
                                <Items>
                                    <ext:ListItem Text="50" Value="50" />
                                    <ext:ListItem Text="100" Value="100" />
                                     <ext:ListItem Text="300" Value="300" />
                                </Items>
                                <SelectedItems >
                                 <ext:ListItem Index="0" />
                                </SelectedItems>
                                <Listeners>
                                    <Select Handler="#{PagingToolbar1}.pageSize = parseInt( this.getValue());#{Store3}.pageSize = parseInt( this.getValue()); searchList();" />
                                </Listeners>
                            </ext:ComboBox>
                        </Items>
                    </ext:PagingToolbar>
                </BottomBar>
            </ext:GridPanel>
            <div class="buttonBlock" style="width: 68%; margin-top: 20px">
                <table class="fieldTable firsttdskip">
                    <tr>
                      

                        <td style="width: 120px">
                            <ext:Button ID="btnSave" Cls="btn btn-primary" runat="server" Text="Save"
                                Width="120" Height="30">
                                <DirectEvents>
                                    <Click OnEvent="btnSave_Click">
                                        <EventMask ShowMask="true" />
                                        <Confirmation ConfirmRequest="true" Message="Are you sure, you want to save the changes?" />
                                        <ExtraParams>
                                            <ext:Parameter Name="gridItems" Value="Ext.encode(#{gridList}.getRowsValues({selectedOnly:false}))"
                                                Mode="Raw" />
                                        </ExtraParams>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                        </td>
                          <td style="width: 120px">
                            <ext:Button ID="btnExport"  IconAlign="Right" Icon="TableGo" AutoPostBack="true" OnClick="btnExport_Click"
                                runat="server" Text="Export" Width="100" Height="30">
                            </ext:Button>
                        </td>
                     
                    </tr>
                </table>
            </div>
            <br />
        </div>
    </div>
</asp:Content>
