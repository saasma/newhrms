﻿<%@ Page Title="Manage User Details" Language="C#" MasterPageFile="~/Master/ForPopupPage.Master" AutoEventWireup="true" CodeBehind="ManageUserPopup.aspx.cs" Inherits="Web.CP.ManageUserPopup" %>

<%@ Register Src="~/Controls/ContentHeader.ascx" TagName="ContentHeader" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningMsgCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="InfoMsgCtl" TagPrefix="uc2" %>
<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">

<style type="text/css">
        .empName
        {
            font-size: 20px;
            color: #439EC6;
            font-weight: bold;
        }
        .titleDesign
        {
            font-size: 15px;
            color: #000000;
            text-decoration: underline;
            padding-top: 10px;
        }
        
        .items span
        {
            display: block;
            padding-top: 3px;
            font-weight: bold;
        }
        
        .paddinAll
        {
            padding: 10px;
        }
        table tr td
        {
            padding: 0 10px 8px 0;
        }
        h3
        {
            font: bold 13px Arial;
        }
        .marginTopss
        {
            margin-top: 10px;
            display: block;
        }
        .loanAdvTable
        {
            background-color: white;
            width: 60px;
            border: 1px solid white !important;
            padding: 0px;
        }
        .tableLightColor th
        {
            text-align: left;
        }
        strong
        {
            display: block;
            padding-bottom: 3px;
        }
        .tableLightColor input[type=text]
        {
            height: 20px;
        }
        .clsColor
        {
            background-color: #e8f1ff;
        }
    </style>


    <script type="text/javascript">

        function closePopup() {
            window.opener.reloadManageUsers(window);

        }

        function windowOnly() {
            window.opener.closeChildWindow(window);
        }

    </script>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="mainContent" runat="server">


 <div class="clsColor">
        <div class="popupHeader">
            <h3>
                User Information</h3>
        </div>
        <asp:HiddenField ID="hdnEmployeeId" runat="server" />
        <asp:HiddenField ID="hdnMessage" runat="server" />
          <div class=" marginal" style="margin: 5px 20px 0 10px;">
            <uc2:InfoMsgCtl ID="msgInfo" EnableViewState="false" Hide="true" runat="server" />
            <uc2:WarningMsgCtl ID="msgWarning" EnableViewState="false" Hide="true" runat="server" />
             
            <table cellpadding="4px" style="margin-left: 10px;">
                <tr id="rowName" runat="server">
                    <td class="fieldHeader">
                        Name *
                    </td>
                    <td style='width: 300px'>
                        <asp:TextBox ID="txtName" runat="server" Width="180px"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3333" SetFocusOnError="true"
                            ValidationGroup="user" ControlToValidate="txtName" Display="None" ErrorMessage="Name is required."
                            runat="server" />
                    </td>
                </tr>
                <tr runat="server" id="rowEmployee">
                    <td class="fieldHeader">
                        Employee
                    </td>
                    <td>
                        <asp:DropDownList DataValueField="EmployeeId" Width="250px" DataTextField="NameEIN"
                            ID="ddlEmployeeList" runat="server" AppendDataBoundItems="true" AutoPostBack="True"
                            OnSelectedIndexChanged="ddlEmployeeList_SelectedIndexChanged">
                            <asp:ListItem Text="" Value="-1" />
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="rfvEmp" InitialValue="-1" SetFocusOnError="true"
                            ValidationGroup="user" ControlToValidate="ddlEmployeeList" Display="None" ErrorMessage="Employee is required."
                            runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="fieldHeader">
                        User Name *
                    </td>
                    <td style='width: 300px'>
                        <asp:TextBox ID="txtUserName" runat="server" Width="180px"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" SetFocusOnError="true" ValidationGroup="user" ControlToValidate="txtUserName"
                            Display="None" ErrorMessage="User Name is required." runat="server" />
                    </td>
                </tr>
                <tr id="rowAD" runat="server">
                    <td class="fieldHeader" valign="top">
                        Active Directory User Name
                    </td>
                    <td style='width: 425px'>
                        <asp:TextBox ID="txtActiveDirectoryUserName" runat="server" Width="180px"></asp:TextBox>
                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtActiveDirectoryUserName"
                            WatermarkText="Employee Name" WatermarkCssClass="searchBoxText" />
                        <cc1:AutoCompleteExtender EnableCaching="true" ID="AutoCompleteExtenderOrganization"
                            runat="server" MinimumPrefixLength="1" ServiceMethod="GetActiveDirectoryUser"
                            ServicePath="~/PayrollService.asmx" TargetControlID="txtActiveDirectoryUserName"
                            CompletionSetCount="10" CompletionInterval="250" CompletionListCssClass="AutoExtender"
                            CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight">
                        </cc1:AutoCompleteExtender>
                        <br />
                        If domain name has company name like user.name@company.com.np then place only "user.name" in this field,
                        blank for HR software password
                    </td>
                </tr>
                <tr>
                    <td class="fieldHeader">
                        Email
                    </td>
                    <td>
                        <asp:TextBox ID="txtEmail" runat="server" Width="180px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="fieldHeader">
                        Allowable IP address like(192.168.1,200.1.20)
                    </td>
                    <td>
                        <asp:TextBox ID="txtIPList" runat="server" Width="300px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="fieldHeader">
                        Role *
                    </td>
                    <td>
                        <asp:DropDownList DataTextField="Name" Width="180px" DataValueField="RoleId" ID="ddlRoles"
                            runat="server">
                            <asp:ListItem Text="" Value="-1" />
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" InitialValue="-1" SetFocusOnError="true"
                            ValidationGroup="user" ControlToValidate="ddlRoles" Display="None" ErrorMessage="Role is required."
                            runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="fieldHeader">
                        Is Active
                    </td>
                    <td>
                        <asp:CheckBox Checked="true" ID="chkIsActive" runat="server" />
                    </td>
                </tr>
                <tr id="rowSignOff" runat="server" visible="false">
                    <td class="fieldHeader">
                        Payroll Signoff/Approval
                    </td>
                    <td>
                        <asp:CheckBox Checked="true" ID="chkPayrollSignOffApproval" runat="server" />
                    </td>
                </tr>
                 <tr>
                    <td class="fieldHeader">
                        Documents
                    </td>
                    <td>
                        <asp:DropDownList  Width="180px"  ID="ddlDocumentPermission"
                            runat="server">
                            <asp:ListItem Text="" Value="-1" />
                            <asp:ListItem Text="View Only" Value="true" />
                            <asp:ListItem Text="View and Edit" Value="false" />
                        </asp:DropDownList>                        
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        <%-- <asp:ValidationSummary ValidationGroup="save" DisplayMode="SingleParagraph" ShowSummary="false"
                            ShowMessageBox="true" runat="server"></asp:ValidationSummary>--%>
                        <asp:Button ValidationGroup="save" OnClientClick="valGroup='user';return CheckValidation()"
                            CssClass="save" ID="btnInsertUpdate" runat="server" Text="Save" OnClick="btnInsertUpdate_Click" />
                        <asp:Button ID="Button1" CssClass="cancel" runat="server" Text="Cancel" OnClientClick="window.close();"/>
                    </td>
                </tr>

            </table>
          
        </div>
        <br />
       
    </div>

</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
