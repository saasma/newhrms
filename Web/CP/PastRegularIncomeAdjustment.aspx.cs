﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using Utils.Helper;
using Utils.Web;
using BLL.Base;
using DAL;
using Utils.Base;

namespace Web
{
    public partial class PastRegularIncomeAdjustment : BasePage
    {

        public new string GetCurrency(object amount)
        {
            return BaseHelper.GetCurrency(amount, 2);
        }

        //private int _tempCurrentPage;
        //private int _tempCount;
        private PayManager payMgr = new PayManager();

        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (!IsPostBack)
            {
                pagingCtl.Visible = false;
                Initialise();

            }
            RegisterScripts();
            JavascriptHelper.AttachNonDialogPopUpCode(Page, "importPopup", "../ExcelWindow/IncomeDeductionAdjustmentExcel.aspx", 450, 500);
            JavascriptHelper.AttachNonDialogPopUpCode(Page, "adjustmentPopup", "AdjustmentCalculation.aspx", 720, 650);

            LoadEmployeesInDDL();
        }

        void RegisterScripts()
        {
            StringBuilder str = new StringBuilder();
            str.AppendFormat("var decimalPlaces = {0};", SessionManager.DecimalPlaces);
          
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "rounding",
                                                        str.ToString()
                                                        , true);
        }

        public bool IsCalculateVisible(object value)
        {
            int type = int.Parse(value.ToString());
            if (type >= -12 && type <= -3)
            {
                return true;
            }
            return false;
        }

        protected void ddlDepartment_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        void Initialise()
        {

            List<FinancialDate> list = BLL.BaseBiz.PayrollDataContext.FinancialDates.OrderBy(x => x.FinancialDateId).ToList();
            foreach (var item in list)
                item.SetName(IsEnglish);
            ddlYears.DataSource = list;
            ddlYears.DataBind();

            UIHelper.SetSelectedInDropDown(ddlYears, SessionManager.CurrentCompanyFinancialDate
                .FinancialDateId);

            //CommonManager mgr = new CommonManager();
            //ddlShift.DataSource = mgr.GetAllWorkShift();
            //ddlShift.DataBind();


            LoadEmployeesInDDL();

            LoadIncomeDeductionList();

        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            //int incomeId = int.Parse(ddlIncomeOrDeduction.SelectedValue);
            //int totalRecords = 0;

            //int employeeId = -1;
            //if (ddlEmployee.SelectedItem != null && ddlEmployee.SelectedItem.Value != null && ddlEmployee.SelectedItem.Value != "")
            //    employeeId = int.Parse(ddlEmployee.SelectedItem.Value);


            //string title = PayManager.GetIncome(incomeId) == null ? "" : PayManager.GetIncome(incomeId).Title;

       
            //List<GetFestivalIncomeDeductionAdjustmentEmployeesResult> list = PayManager.GetFestivalIncomeDeductionAdjustment(
            //    int.Parse(ddlYears.SelectedValue),
            //    employeeId, incomeId,
            //    0, 999999, ref totalRecords);





          

            //Bll.ExcelHelper.ExportToExcel(title + " details", list,
            //    new List<string> { "YearId", "IncomeDeductionId", "RowNumber", "TotalRows" },
            //new List<String>() { },
            //new Dictionary<string, string>() { { "EmployeeId", "EIN" }, { "IsFull", "Eligibility" }, { "PaidAddOnAmount", "Paid Amount" } },
            //new List<string>() { "Amount", "PaidAddOnAmount" }
            //, new List<string> { "SN", "EmployeeId" }
            //, new List<string> { "StartDate", "EndDate" }
            //, new Dictionary<string, string>() { { "Income Details ", title + " details" } }
            //, new List<string> { "EmployeeId", "Name", "ExistingAmount", "AdjustmentAmount", "AdjustedAmount", "Note" }); 

        }
        protected void btnLoad_Click(object sender, EventArgs e)
        {
        
            LoadEmployees();
        }

        protected void LoadEmployees()
        {

            //if (ddlIncomeOrDeduction.SelectedValue.Equals("-1"))
            //{
            //    gvw.DataSource = null;
            //    gvw.DataBind();

            //    divWarningMsg.InnerHtml = "Income/Deduction should be selected for adjustment.";
            //    divWarningMsg.Hide = false;

            //    pagingCtl.Visible = false;

            //    return;
            //}

           // int incomeId = int.Parse(ddlIncomeOrDeduction.SelectedValue);
            int totalRecords = 0;

            int employeeId = -1;
            if (ddlEmployee.SelectedItem != null && ddlEmployee.SelectedItem.Value != null && ddlEmployee.SelectedItem.Value != "")
                employeeId = int.Parse(ddlEmployee.SelectedItem.Value);

            // Negative value adjustment is allowed for non basic income type only
            bool isNegativeValueAllowed = false;
            //if (incomeId == ((int)CalculationColumnType.Income))
            //{
            //    PIncome basicIncome = new PayManager().GetBasicIncome(SessionManager.CurrentCompanyId);
            //    if (incomeId != basicIncome.IncomeId)
            //    {
            //        isNegativeValueAllowed = true;
            //    }
            //}
            

            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "sdfsdf2", string.Format("var isNegativeValueAllowed = {0};", isNegativeValueAllowed.ToString().ToLower()), true);

            List<GetPastRegularAdjustmentEmployeesResult> list = PayManager.GetPastRegularAdjustment(
                int.Parse(ddlYears.SelectedValue),
                employeeId,
                pagingCtl.CurrentPage-1, int.Parse(pagingCtl.DDLRecords.SelectedValue), ref totalRecords);

            pagingCtl.Visible = true;

            gvw.DataSource = list;                
            gvw.DataBind();
            pagingCtl.UpdatePagingBar(totalRecords);

            if (CommonManager.GetCurrentFinancialYear().FinancialDateId != (int.Parse(ddlYears.SelectedValue)))
            {
                divWarningMsg.InnerHtml = "Prev year not editable.";
                divWarningMsg.Hide = false;
            }
            else
            {
                

            }
            
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            Page.Validate("Balance");

            decimal existingAmount, adjustmentAmount, payAmount,paidAmount;
            //string notes;
            int employeeId, YearId = 0;//, IncomeDeductionId;

            List<DAL.PastRegularIncomeAdjustment> list = new List<DAL.PastRegularIncomeAdjustment>();

            bool isSavedReqd = false;
            //if (Page.IsValid)
            {

                StringBuilder str = new StringBuilder("<root>");


                foreach (GridViewRow row in gvw.Rows)
                {
                   

                    employeeId = (int)gvw.DataKeys[row.RowIndex]["EmployeeId"];
                    YearId = (int)gvw.DataKeys[row.RowIndex]["YearId"];
                    //IncomeDeductionId = (int)gvw.DataKeys[row.RowIndex]["IncomeDeductionId"];


                    TextBox txtExistingAmount = row.FindControl("txtExistingAmount") as TextBox;
                    TextBox txtAdjustmentAmount = row.FindControl("txtAdjustmentAmount") as TextBox;

                   
                    TextBox txtNote = row.FindControl("txtNote") as TextBox;



                    string invalidText = Util.HasInvalidCharacterForXMLParsingAndImport(txtNote.Text.Trim());

                    if (!string.IsNullOrEmpty(invalidText))
                    {
                        divWarningMsg.InnerHtml = "\"" + invalidText + "\" character can not be set in the note.";
                        divWarningMsg.Hide = false;
                        return;
                    }

                   

                    existingAmount = decimal.Parse(txtExistingAmount.Text.Trim());
                    adjustmentAmount = decimal.Parse(txtAdjustmentAmount.Text.Trim());
                   


                    if (txtNote.Enabled && ( adjustmentAmount != 0 
                        ))
                    {
                        isSavedReqd = true;

                        list.Add(new DAL.PastRegularIncomeAdjustment {  YearId = YearId,EmployeeId = employeeId,
                            AdjustmentAmount = adjustmentAmount,Note = txtNote.Text.Trim()
                        });

                        if(string.IsNullOrEmpty(txtNote.Text.Trim()))
                        {
                            divWarningMsg.InnerHtml = "Notes must be placed for EIN : " + employeeId;
                            divWarningMsg.Hide = false;
                            return;
                        }

                      
                    }
                }
                str.Append("</root>");
                    
                if (isSavedReqd)
                {
                    PayManager.InsertUpdatePastRegularAdjustment(YearId, list);
                    divMsgCtl.InnerHtml = "Saved";
                    divMsgCtl.Hide = false;
                    LoadEmployees();
                }

            }

        }


        protected void btnCancel_Click(object sender, EventArgs e)
        {
            LoadEmployees();
        }
        protected void btnDelete_Click(object sender, EventArgs e)
        {
            List<DAL.PastRegularIncomeAdjustment> list = new List<DAL.PastRegularIncomeAdjustment>();
            bool employeeAdded = false;
          
            foreach (GridViewRow row in gvw.Rows)
            {
                

                int employeeId = (int)gvw.DataKeys[row.RowIndex]["EmployeeId"];
                int yearId = (int)gvw.DataKeys[row.RowIndex]["YearId"];
                //int IncomeDeductionId = (int)gvw.DataKeys[row.RowIndex]["IncomeDeductionId"];
                
                TextBox txtExistingAmount = row.FindControl("txtExistingAmount") as TextBox;
                TextBox txtAdjustmentAmount = row.FindControl("txtAdjustmentAmount") as TextBox;
                TextBox txtNote = row.FindControl("txtNote") as TextBox;


                if (row.FindControl("chkDelete") == null)
                    continue;

                CheckBox chkDelete = (CheckBox)row.FindControl("chkDelete");

                if (chkDelete.Checked)
                {
                    employeeAdded = true;

                    list.Add(new DAL.PastRegularIncomeAdjustment { YearId = yearId,EmployeeId = employeeId});
                }
            }
            


            if ( employeeAdded)
            {
                PayManager.DeletePastRegularIncomeAdjustment(list);
                divMsgCtl.InnerHtml = "Deleted";
                divMsgCtl.Hide = false;
                LoadEmployees();
            }
        }

        //protected void btnPostToSalary_Click(object sender, EventArgs e)
        //{

        //    foreach (GridViewRow row in gvw.Rows)
        //    {


        //        int payrollPeriodId = (int)gvw.DataKeys[row.RowIndex]["PayrollPeriodId"];

        //        PayManager.PostOvertimePay(payrollPeriodId);
        //        msgCtl.InnerHtml = Resources.Messages.OvertimePostedMessage;
        //        msgCtl.Hide = false;
        //        break;
        //    }


        //}
        protected void txtEmployeeName_TextChanged(object sender, EventArgs e)
        {
            

        }

        private void LoadIncomeDeductionList()
        {
            //ListItem item = ddlIncomeOrDeduction.Items[0];
            //ddlIncomeOrDeduction.Items.Clear();

            //ddlIncomeOrDeduction.DataSource =
            //   BLL.BaseBiz.PayrollDataContext.PIncomes.Where(
            //   x => x.Calculation == IncomeCalculation.FESTIVAL_ALLOWANCE && x.AddTaxFromTheBeginning != null &&
            //       x.AddTaxFromTheBeginning.Value).OrderBy(x => x.Title).ToList();

            //ddlIncomeOrDeduction.DataBind();

            //ddlIncomeOrDeduction.Items.Insert(0, item);
        }

        protected void ddlPayrollPeriods_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadEmployeesInDDL();

            LoadIncomeDeductionList();
        }

        protected void ddlDepartment_SelectedIndexChanged1(object sender, EventArgs e)
        {
            LoadEmployeesInDDL();
        }

        private void LoadEmployeesInDDL()
        {
            //ListItem item = ddlEmployee.Items[0];
            //ddlEmployee.Items.Clear();

            storeEmployee.DataSource = EmployeeManager.GetValidEmployeesForPayrollPeriod(
               CommonManager.GetLastPayrollPeriod().PayrollPeriodId, -1,-1);
            storeEmployee.DataBind();

            //if (ddlIncomeOrDeduction.SelectedItem.Text.Trim() == CalculationColumnType.TDS.ToString())
            //{
            //    gvw.Columns[3].Visible = true;
            //}
            //else
            //{
            //    gvw.Columns[3].Visible = false;
            //}

            //ddlEmployee.Items.Insert(0, item);
        }

        protected void btnPrevious_Click()
        {
            pagingCtl.CurrentPage -= 1;
            //_tempCurrentPage = int.Parse(pagintCtl.LabelCurrentPage.Text);
            //_tempCurrentPage -= 1;
            LoadEmployees();
        }

        protected void btnNext_Click()
        {
            pagingCtl.CurrentPage += 1;
            //_tempCurrentPage = int.Parse(pagintCtl.LabelCurrentPage.Text);
            //_tempCurrentPage += 1;
            LoadEmployees();
        }

        protected void ddlRecords_SelectedIndexChanged()
        {
            //SessionManager.EmployeeDisplayRecodsPerPage = int.Parse(pagintCtl.DDLRecords.SelectedValue);
            //_tempCurrentPage = 1;
            pagingCtl.CurrentPage = 1;
            LoadEmployees();
        }

    }
}
