using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using BLL;
using BLL.Manager;
using Utils.Web;
using Utils.Helper;
using DAL;
using BLL.Base;
using System.Collections.Generic;

namespace Web.CP
{
    public partial class EmployeeHRDetails : BasePage
    {
        CommonManager commonMgr = new CommonManager();
        HRManager hrMgr = new HRManager();
        EEmployee employee = null;
        EmployeeManager empMgr = new EmployeeManager();

        protected void Page_Load(object sender, EventArgs e)
        {
            RegisterEmployeeIDInScript(0);
            LoadDynamicLists();

            if (!IsPostBack)
            {
                Initialise();

                if (CommonManager.CompanySetting.IsD2)
                    rowCaste.Visible = false;
            }

            AttachJSCode();
            ucHRDetails.DisplayDocuments(UrlHelper.GetIdFromQueryString("EmpId"));
            SetPagePermission();
        }
        /// <summary>
        /// Set for Custom Role if is in Readonly Mode or Not
        /// </summary>
        public void SetPagePermission()
        {
            if (SessionManager.IsCustomRole && SessionManager.CustomRole.IsReadOnly != null && SessionManager.CustomRole.IsReadOnly.Value)
            {

                btnAddSkillSet.Visible = false;
                btnAddCaste.Visible = false;
                btnSave.Visible = false;
                btnAddCompetencyLevel.Visible = false;
                btnAddEmployeeSkillSet.Visible = false;
            }
        }
        void RegisterEmployeeIDInScript(int empId)
        {
            if (empId == 0)
                empId = UrlHelper.GetIdFromQueryString("EmpId");
            else
                empId = 0;

            if (empId != 0)
            {
                employee = EmployeeManager.GetEmployeeById(empId);
                if (employee != null)
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "EmpIDKey", "\nEmployeeId=" + empId + ";", true);
                else
                {
                    JavascriptHelper.DisplayClientMsg("Employee with the specified Id does not exists.", Page, "window.location='ManageEmployee.aspx';");
                }
            }
        }

        void AttachJSCode()
        {
            

            JavascriptHelper.AttachPopUpCode(Page, "castePopup", "PP/ManageCaste.aspx", 486, 230);

            JavascriptHelper.AttachPopUpCode(Page, "skillSetPopup", "PP/ManageSkillSet.aspx", 700, 660);

            JavascriptHelper.AttachPopUpCode(Page, "competencyLevelPopupCall", "PP/ManageSkillSetLevel.aspx", 700, 660);


            JavascriptHelper.AttachNonDialogPopUpCode(Page, "addSkillSetToEmployee", "PP/AddSkillSetToEmployee.aspx", 700, 660);
            

        }

        void LoadSkillSets()
        {
            //chkListSkillSets.DataSource = new CommonManager().GetAllSkillSet();
            //chkListSkillSets.DataBind();
        }

        void Initialise()
        {
            int employeeId = UrlHelper.GetIdFromQueryString("EmpId");
            if (employee == null)
                return;

            LoadSkillSets();

            txtName.Text = employee.Name;

            ddlBloodGroups.DataSource = commonMgr.GetAllBloodGroups();
            ddlBloodGroups.DataBind();

            ddlQualification.DataSource = commonMgr.GetAllQualifications();
            ddlQualification.DataBind();

            HHumanResource entity = hrMgr.GetHumanResource(employeeId);
           // List<SkillSetEmployee> skillSets = CommonManager.GetEmployeeSkillSet(employeeId);
            if (entity != null)
            {

                Process(ref entity);
                ucHRDetails.Process(entity, true);
            }
            divInnerSkillSet.InnerHtml = CommonManager.GetEmployeeSkillSetHTML(employeeId);
            ucEducation.DisplayEducation(employeeId);
            ucActivity.DisplayCurriculum(employeeId);
            ucFamily.DisplayFamily(employeeId);
            ucDiscipline.DisplayDiscipline(employeeId);
           // ucEvaluation.DisplayEvaluation(employeeId);
            ucAccident.DisplayAccident(employeeId);
            ucTraining.DisplayTraining(employeeId);
            ucSpecialEvent.DisplaySpecialEvent(employeeId);
            ucPreviousEmployment.DisplayPreviousEmployment(employeeId);
        }

        void LoadDynamicLists()
        {
            LoadCaste();
        }

        void LoadCaste()
        {
            string casteId = Request.Form[ddlCastes.UniqueID];  
            ddlCastes.DataSource = commonMgr.GetAllCastes();
            ddlCastes.DataBind();
            if (casteId != null)
                UIHelper.SetSelectedInDropDown(ddlCastes, casteId);      
        }

        void Process(ref HHumanResource entity)
        {
            if (entity == null)
            {
                entity = new HHumanResource();

                if (ddlBloodGroups.SelectedValue != "-1")
                    entity.BloodGroup = ddlBloodGroups.SelectedItem.Text;
                else
                    entity.BloodGroup = null;

                if (ddlCastes.SelectedValue == "-1")
                    entity.CasteId = null;
                else
                    entity.CasteId = int.Parse(ddlCastes.SelectedValue);

                if (ddlQualification.SelectedValue == "-1")
                    entity.Qualification = null;
                else
                    entity.Qualification = ddlQualification.SelectedItem.Text;


                //employeeSkillSetList = new List<SkillSetEmployee>();
                //foreach (ListItem item in chkListSkillSets.Items)
                //{
                //    if (item.Selected)
                //    {
                //        employeeSkillSetList.Add(
                //            new SkillSetEmployee { EmployeeId= employee.EmployeeId,SkillSetId = int.Parse(item.Value) });
                //    }
                //}


                entity.Languages = txtLanguages.Text.Trim();

                ucHRDetails.Process(entity, false);
                    
            }
            else
            {
                UIHelper.SetSelectedInDropDownFromText(ddlBloodGroups, entity.BloodGroup);
                UIHelper.SetSelectedInDropDown(ddlCastes, entity.CasteId);
                UIHelper.SetSelectedInDropDownFromText(ddlQualification, entity.Qualification);
                //txtMajorSubjects.Text = entity.MajorSubjects;
                txtLanguages.Text = entity.Languages;

                ucHRDetails.Process(entity, true);

                //foreach (SkillSetEmployee skillItem in employeeSkillSetList)
                //{
                //    ListItem item = chkListSkillSets.Items.FindByValue(skillItem.SkillSetId.ToString());
                //    if (item != null)
                //        item.Selected = true;
                //}

               

            }               
            
        }


        protected void btnSave_Click(object sender, EventArgs e)
        {
            HHumanResource entity = null;
           // List<SkillSetEmployee> skillSets = null;
            Process(ref entity);

            entity.EmployeeId = UrlHelper.GetIdFromQueryString("empId");
            hrMgr.SaveOrUpdateEmployeeHRDetails(entity,null,false,"");

            divInnerSkillSet.InnerHtml = CommonManager.GetEmployeeSkillSetHTML(entity.EmployeeId.Value);

            msgInfo.InnerHtml = Resources.Messages.HRSavedMsg;
            msgInfo.Hide = false;
            
        }
    }
}
