﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using Utils.Helper;
using Utils.Web;
using BLL.Base;

namespace Web
{
    public partial class OpeningImportPage : BasePage
    {
    
        private int _tempCurrentPage;
        private int _tempCount;
        private PayManager payMgr = new PayManager();

        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.AdministratorTools;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                pagingCtl.CurrentPage = 1;
                BindEmployees();

            }
            if (Request.Form["__EVENTTARGET"] != null && Request.Form["__EVENTTARGET"].ToLower().Contains("refresh"))
                BindEmployees();
            JavascriptHelper.AttachNonDialogPopUpCode(Page, "taxImportPopup", "../ExcelWindow/PFCITOpeningImport.aspx", 450, 500);
        }


        void BindEmployees()
        {
            int totalRecords = 0;
            gvEmployeeIncome.DataSource
                = TaxManager.GetForPFOpening(txtEmployeeName.Text.Trim(), pagingCtl.CurrentPage - 1, int.Parse(pagingCtl.DDLRecords.SelectedValue), ref totalRecords);
            gvEmployeeIncome.DataBind();
            pagingCtl.UpdatePagingBar(totalRecords);

            btnUpdate.Visible = !CommonManager.IsFirstSalaryGenerated();
            //btnExport.Visible = btnUpdate.Visible;
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            BindEmployees();
        }

        public bool IsOpeningRemoteAreaEnabled()
        {
            return CommonManager.CompanySetting.RemoteAreaUsingProportionateDays;
        }

        protected void btnPrevious_Click()
        {
            pagingCtl.CurrentPage -= 1;
            //_tempCurrentPage = int.Parse(pagintCtl.LabelCurrentPage.Text);
            //_tempCurrentPage -= 1;
            BindEmployees();
        }

        protected void btnNext_Click()
        {
            pagingCtl.CurrentPage += 1;
            //_tempCurrentPage = int.Parse(pagintCtl.LabelCurrentPage.Text);
            //_tempCurrentPage += 1;
            BindEmployees();
        }

        protected void ddlRecords_SelectedIndexChanged()
        {
            //SessionManager.EmployeeDisplayRecodsPerPage = int.Parse(pagintCtl.DDLRecords.SelectedValue);
            //_tempCurrentPage = 1;
            pagingCtl.CurrentPage = 1;
            BindEmployees();
        }
        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            Page.Validate("Balance");
            bool isSavedReqd = false;
            decimal balance, pf, cit, insurance, taxpaid, openingSST,remoteArea;
            if (Page.IsValid)
            {
                StringBuilder str = new StringBuilder("<root>");
                int companyId = SessionManager.CurrentCompanyId;

                foreach (GridViewRow row in gvEmployeeIncome.Rows)
                {
                    int employeeId = (int)gvEmployeeIncome.DataKeys[row.RowIndex][0];
                   
                   
                    TextBox txt = row.FindControl("txtAmount") as TextBox;

                    TextBox txtPF = row.FindControl("txtPF") as TextBox;
                    TextBox txtCIT = row.FindControl("txtCIT") as TextBox;
                    TextBox txtInsurance = row.FindControl("txtInsurance") as TextBox; 
                    TextBox txtTaxPaid = row.FindControl("txtTaxPaid") as TextBox;
                    TextBox txtOpeningSST = row.FindControl("txtOpeningSST") as TextBox;
                    TextBox txtRemoteArea = row.FindControl("txtRemoteArea") as TextBox;

                    isSavedReqd = true;

                    if (!decimal.TryParse(txt.Text, out balance))
                        balance = 0;
                    if( !decimal.TryParse(txtPF.Text.Trim(),out pf))
                        pf = 0;

                    if (!decimal.TryParse(txtCIT.Text.Trim(), out cit))
                        cit = 0;

                    if (!decimal.TryParse(txtInsurance.Text.Trim(), out insurance))
                        insurance = 0;
                    if (!decimal.TryParse(txtTaxPaid.Text.Trim(), out taxpaid))
                        taxpaid = 0;
                    if (!decimal.TryParse(txtOpeningSST.Text.Trim(), out openingSST))
                        openingSST = 0;

                    if (!decimal.TryParse(txtRemoteArea.Text.Trim(), out remoteArea))
                        remoteArea = 0;

                    str.Append(
                        string.Format("<row EmployeeId=\"{0}\" Amount=\"{1}\" PF=\"{2}\" CIT=\"{3}\"  Insurance=\"{4}\"  TaxPaid=\"{5}\" OpeningSST=\"{6}\" OpeningRemoteArea=\"{7}\"   /> ",
                             employeeId, balance, pf, cit, insurance, taxpaid,openingSST,remoteArea)
                        );
                }
                str.Append("</root>");

                if (isSavedReqd)
                {
                   TaxManager.SaveForTax(str.ToString());

                    JavascriptHelper.DisplayClientMsg("Information saved.", Page);
                }
            }
        }

        protected void txtEmployeeName_TextChanged(object sender, EventArgs e)
        {
            _tempCurrentPage = 1;
            BindEmployees();

        }
    }
}
