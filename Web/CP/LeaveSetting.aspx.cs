﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using BLL.Manager;
using BLL;
using DAL;
using Utils.Web;
using Utils.Helper;
using System.Text;
using Utils.Calendar;
using DevExpress.Web;

namespace Web.CP
{



    public partial class LeaveSetting : BasePage
    {
        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.AdministratorTools;
            }
        }

        CompanyManager compMgr = new CompanyManager();



        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                Initialise();          

            
        }
        protected void btnLockDateEdit_Click(object sender, EventArgs e)
        {
            calLockDate.Enabled = true;
        }
        
        public void Initialise()
        {

            ddlLeaveStartMonth.DataSource = DateManager.GetCurrentMonthListForCompany(this.IsEnglish);
            ddlLeaveStartMonth.DataBind();

            ddlLapseEncashProcessingMonth.DataSource = DateManager.GetCurrentMonthListForCompany(this.IsEnglish);
            ddlLapseEncashProcessingMonth.DataBind();

            ddlEncashLeavePaysOn.DataSource = (DateManager.GetCurrentMonthList());
            ddlEncashLeavePaysOn.DataTextField = "Value";
            ddlEncashLeavePaysOn.DataValueField = "Key";
            ddlEncashLeavePaysOn.DataBind();
            //ddlEncashmentMonth.Items.Insert(0, "-- Select Month --");
            ListItem item = new ListItem("Retirement", "13".ToString());
            ddlEncashLeavePaysOn.Items.Add(item);



            ddlLapseEncashLeavePaysOn.DataSource = DateManager.GetCurrentMonthList();
            ddlLapseEncashLeavePaysOn.DataBind();


            chkAllowNegative.Checked = !CommonManager.CompanySetting.LeaveRequestNegativeLeaveReqBalanceNotAllowed;
            chkTreatHolidayAsLeave.Checked = !CommonManager.CompanySetting.LeaveRequestCountHolidayInBetweenLeave;
            ddlTakenOrder.SelectedValue = CommonManager.CompanySetting.LeaveHasRequestOrder.ToString().ToLower();

            if (CommonManager.Setting.AttendanceCommentByEmpThreshold != null)
            {
                cmbAttCommentDays.SelectedValue = CommonManager.Setting.AttendanceCommentByEmpThreshold.ToString();
            }

            Company company = compMgr.GetById(SessionManager.CurrentCompanyId);

            if (company != null)
            {
                UIHelper.SetSelectedInDropDown(ddlLeaveStartMonth, company.LeaveStartMonth);
                UIHelper.SetSelectedInDropDown(ddlEncashLeavePaysOn, company.LeaveEncashTypePaysOn);
                UIHelper.SetSelectedInDropDown(ddlLapseEncashLeavePaysOn, company.LeaveLapseEncashTypePaysOn);
                UIHelper.SetSelectedInDropDown(ddlLapseEncashProcessingMonth,
                    company.LeaveLapseEncashProcessingMonth == null ? "-1" : company.LeaveLapseEncashProcessingMonth.ToString());
            }

            leaveApprovalPastDays.Text = CommonManager.CompanySetting.LeaveApprovalMinimumPastDays.ToString();
            leaveRequestPastDays.Text = CommonManager.CompanySetting.LeaveRequestMinimumPastDays.ToString();
            txtLeaveRequestValidMonths.Text = CommonManager.CompanySetting.LeaveRequestValidMonths.ToString();

            //if attendancce already saved for the company then hide this button
            Setting setting = OvertimeManager.GetSetting();
            if (setting != null && setting.AllLeaveOnRetirementBenefit != null && setting.AllLeaveOnRetirementBenefit.Value)
            {
                ddlTreatmentOfEncashment.SelectedIndex = 1;
                //chkAllOnRetirement.Checked = setting.AllLeaveOnRetirementBenefit.Value;
            }
            else if (setting.AllLeaveOnRegularBenefit != null && setting.AllLeaveOnRegularBenefit.Value)
                ddlTreatmentOfEncashment.SelectedIndex = 2;

            if (setting != null && setting.LeaveAllowMultipleReview != null)
                chkMultipleReview.Checked = setting.LeaveAllowMultipleReview.Value;

            if (setting.LeaveRequestSettingUsingPreDefinedList != null && setting.LeaveRequestSettingUsingPreDefinedList.Value)
                ddlRequestManagementMethods.SelectedIndex = 1;
            else if (setting.LeaveRequestSettngUsingOneToOne != null && setting.LeaveRequestSettngUsingOneToOne.Value)
                ddlRequestManagementMethods.SelectedIndex = 2;
            else
                rowSelectionOptionInLeaveApprovalForTeamOptions2.Visible =  rowSelectionOptionInLeaveApprovalForTeamOptions.Visible = true;

            chkSelectionOptionInLeaveApproval.Checked = CommonManager.CompanySetting.LeaveRequestToOnlyOneSupervisor;

            // Leave Rounding
            if (setting.IsLeaveEncashmentRoundingInRetirement != null && setting.IsLeaveEncashmentRoundingInRetirement.Value)
                ddlLeaveBalanceRounding.SelectedIndex = 1;
            else if (setting.IsLeaveBalanceRoundDown != null && setting.IsLeaveBalanceRoundDown.Value)
                ddlLeaveBalanceRounding.SelectedIndex = 2;

            if (setting != null && setting.DisableAttendanceTimeAddEdit != null && setting.DisableAttendanceTimeAddEdit.Value)
                ddlDisableToChangeAtteTime.SelectedIndex = 1;

            if (setting != null && setting.AtteLateDeductionGraceMin != null)
                txtLateGraceMin.Text = setting.AtteLateDeductionGraceMin.ToString();
            if (setting != null && setting.AtteLateDisplayGraceMin != null)
                txtLateDisplayGraceMin.Text = setting.AtteLateDisplayGraceMin.ToString();

            if (CommonManager.Setting.EmpTimeAttRequestThreshold != null)
            {
                ddlETARD.SelectedValue = CommonManager.Setting.EmpTimeAttRequestThreshold.ToString();
            }

            if (setting != null && setting.MultipleOvertimeRequestApproval != null && setting.MultipleOvertimeRequestApproval.Value)
                ddlMORA.SelectedIndex = 1;

            if (setting != null && setting.ApproveLeaveNotificationType != null)
                ddlApprovedLeaveNotificationType.SelectedValue = setting.ApproveLeaveNotificationType.ToString();
            txtApprovedLeaveNotificationMails.Text = setting.ApproveLeaveNotificationMails;
            txtRequestLeaveNotificationMails.Text = setting.RequestLeaveNotificationMails == null ? "" : setting.RequestLeaveNotificationMails;
            txtAbsentNotificationMails.Text = setting.AbsentNotificationMails == null ? "" : setting.AbsentNotificationMails;

            if (setting.ExcludeStatusHierarchyInLeave != null)
                ddlStatusSelectionInLeave.SelectedValue = setting.ExcludeStatusHierarchyInLeave.ToString().ToLower();
            if (setting.DisableEmployeePortal != null)
                ddlDisableEmpoyeePortal.SelectedValue = setting.DisableEmployeePortal.ToString().ToLower();
            if (setting.HideUnpaidLeaveInLeaveRequest != null)
                ddlHideUnapidLeave.SelectedValue = setting.HideUnpaidLeaveInLeaveRequest.ToString().ToLower();

            if (setting.DateLockForLeaveChangeFromEmployee != null)
                calLockDate.Text = BLL.BaseBiz.GetAppropriateDate(setting.DateLockForLeaveChangeFromEmployee.Value);

            if (CommonManager.Setting.EmployeeActvityThreshold != null)
            {
                ddlEmpActivity.SelectedValue = CommonManager.Setting.EmployeeActvityThreshold.ToString();
            }
        }


        protected void btnGraceMinDisplayEdit_Click(object sender, EventArgs e)
        {
            txtLateDisplayGraceMin.Enabled = true;
        }
        protected void btnDisableEmpPortal_Click(object sender, EventArgs e)
        {
            ddlDisableEmpoyeePortal.Enabled = true;
        }
        protected void btnHideUnpaidLeave_Click(object sender, EventArgs e)
        {
            ddlHideUnapidLeave.Enabled = true;
        }
        protected void btnSave_Click1(object sender, EventArgs e)
        {
          
        }

        protected void gvw_RowEditing(object sender, GridViewEditEventArgs e)
        {
            //gvw.EditIndex = e.NewEditIndex;
            //Initialise();
        }

        protected void gvw_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {

            //string key = gvw.DataKeys[e.RowIndex]["Key"].ToString();
            //double value = double.Parse((gvw.Rows[e.RowIndex].FindControl("txt") as TextBox).Text);

            //HPLAllowanceManager.UpdateAllowanceRate(key, value);

            //gvw.EditIndex = -1;
            //Initialise();


            //divMsgCtl.InnerHtml = "Saved";
            //divMsgCtl.Hide = false;

        }

        protected void gvw_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            //gvw.EditIndex = -1;
            //Initialise();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {

                Company company = compMgr.GetById(SessionManager.CurrentCompanyId);

                #region "Change Logs"
                if( company.LeaveStartMonth.ToString() != ddlLeaveStartMonth.SelectedValue)
                    BLL.BaseBiz.PayrollDataContext.ChangeSettings.InsertOnSubmit(BLL.BaseBiz.GetSettingChangeLog(
                        (byte)MonetoryChangeLogTypeEnum.Leave,"Leave Start Month",
                         DateHelper.GetMonthName( company.LeaveStartMonth.Value,IsEnglish),
                         DateHelper.GetMonthName(int.Parse(ddlLeaveStartMonth.SelectedValue), IsEnglish), LogActionEnum.Update));

                if (company.LeaveLapseEncashProcessingMonth.ToString() != ddlLapseEncashProcessingMonth.SelectedValue)
                    BLL.BaseBiz.PayrollDataContext.ChangeSettings.InsertOnSubmit(BLL.BaseBiz.GetSettingChangeLog(
                        (byte)MonetoryChangeLogTypeEnum.Leave, "Lapse Encash Processing Month",
                        company.LeaveLapseEncashProcessingMonth == null || company.LeaveLapseEncashProcessingMonth==-1 ? "Default" : 
                        DateHelper.GetMonthName(company.LeaveLapseEncashProcessingMonth.Value , IsEnglish),
                        int.Parse(ddlLeaveStartMonth.SelectedValue) == -1 ? "Default" : 
                         DateHelper.GetMonthName(int.Parse(ddlLeaveStartMonth.SelectedValue), IsEnglish), LogActionEnum.Update));

                if (company.LeaveEncashTypePaysOn != null)
                {
                    if (company.LeaveEncashTypePaysOn.ToString() != ddlEncashLeavePaysOn.SelectedValue)
                        BLL.BaseBiz.PayrollDataContext.ChangeSettings.InsertOnSubmit(BLL.BaseBiz.GetSettingChangeLog(
                            (byte)MonetoryChangeLogTypeEnum.Leave, "Encash Pays On",
                             company.LeaveEncashTypePaysOn == 13 ? "Retirement" : DateHelper.GetMonthName(company.LeaveEncashTypePaysOn.Value, IsEnglish),
                            int.Parse(ddlEncashLeavePaysOn.SelectedValue) == 13 ? "Retirement" : DateHelper.GetMonthName(int.Parse(ddlEncashLeavePaysOn.SelectedValue), IsEnglish), LogActionEnum.Update));
                }


                if (company.LeaveLapseEncashTypePaysOn != null)
                {
                    if (company.LeaveLapseEncashTypePaysOn.ToString() != ddlLapseEncashLeavePaysOn.SelectedValue)
                        BLL.BaseBiz.PayrollDataContext.ChangeSettings.InsertOnSubmit(BLL.BaseBiz.GetSettingChangeLog(
                            (byte)MonetoryChangeLogTypeEnum.Leave, "Encash Lapse Pays On",
                             DateHelper.GetMonthName(company.LeaveLapseEncashTypePaysOn.Value, IsEnglish),
                             DateHelper.GetMonthName(int.Parse(ddlLapseEncashLeavePaysOn.SelectedValue), IsEnglish), LogActionEnum.Update));
                }
                #endregion

                company.LeaveStartMonth = int.Parse(ddlLeaveStartMonth.SelectedValue);
                company.LeaveEncashTypePaysOn = int.Parse(ddlEncashLeavePaysOn.SelectedValue);
                company.LeaveLapseEncashTypePaysOn = int.Parse(ddlLapseEncashLeavePaysOn.SelectedValue);
                company.LeaveLapseEncashProcessingMonth = int.Parse(ddlLapseEncashProcessingMonth.SelectedValue);

                // Leave request related change
                int value =int.Parse(leaveApprovalPastDays.Text.Trim());
                CommonManager.UpdateCompanySetting("LeaveApprovalMinimumPastDays", value.ToString());

                value = int.Parse(leaveRequestPastDays.Text.Trim());
                CommonManager.UpdateCompanySetting("LeaveRequestMinimumPastDays", value.ToString());

                value = int.Parse(txtLeaveRequestValidMonths.Text.Trim());
                CommonManager.UpdateCompanySetting("LeaveRequestValidMonths", value.ToString());


                CommonManager.UpdateCompanySetting("LeaveRequestNegativeLeaveReqBalanceNotAllowed", 
                    (!chkAllowNegative.Checked).ToString().ToLower());
                CommonManager.UpdateCompanySetting("LeaveRequestCountHolidayInBetweenLeave",
                    (!chkTreatHolidayAsLeave.Checked).ToString().ToLower());

                CommonManager.UpdateCompanySetting("LeaveHasRequestOrder",
                    (ddlTakenOrder.SelectedValue).ToString().ToLower());

                if(cmbAttCommentDays.SelectedItem!=null && cmbAttCommentDays.SelectedIndex!=-1 && cmbAttCommentDays.SelectedItem.Value!= null)
                    CommonManager.UpdateAttendanceCommentThreshold(int.Parse(cmbAttCommentDays.SelectedItem.Value));

                if (ddlETARD.SelectedItem != null && ddlETARD.SelectedItem.Value != null)
                {
                    CommonManager.UpdateEmployeeTimeAttReqDays(int.Parse(ddlETARD.SelectedValue));
                }

                if (ddlEmpActivity.SelectedItem != null && ddlEmpActivity.SelectedIndex != -1 && ddlEmpActivity.SelectedItem.Value != null)
                    CommonManager.UpdateEmployeeActivityThreshold(int.Parse(ddlEmpActivity.SelectedItem.Value));

                MyCache.Reset();

                compMgr.Update(company);

                divMsgCtl.InnerHtml = "Leave setting updated.";
                divMsgCtl.Hide = false;

                SetDefault(btnEdit1, btnCancel1, ddlLeaveStartMonth);
                SetDefault(btnEdit2, btnCancel2, ddlEncashLeavePaysOn);
                SetDefault(btnEdit3, btnCancel3, ddlLapseEncashLeavePaysOn);
                SetDefault(btnEdit4, btnCancel4, leaveApprovalPastDays);
                SetDefault(btnEdit55, btnCancel55, leaveRequestPastDays);
                SetDefault(btnEdit5, btnCancel5, txtLeaveRequestValidMonths);
                SetDefault(btnETARD, btnETARDCancel, ddlETARD);

                bool LeaveRequestSettingUsingPreDefinedList=false, LeaveRequestSettngUsingOneToOne = false;
                if (ddlRequestManagementMethods.SelectedValue == "0")
                {
                }
                else if (ddlRequestManagementMethods.SelectedValue == "1")
                    LeaveRequestSettingUsingPreDefinedList = true;
                else if (ddlRequestManagementMethods.SelectedValue == "2")
                    LeaveRequestSettngUsingOneToOne = true;

                //bool leaveRequestType = bool.Parse(ddlRequestManagementMethods.SelectedValue);

                LeaveEncashmentTreatmentOnRetirement type = (LeaveEncashmentTreatmentOnRetirement)int.Parse(ddlTreatmentOfEncashment.SelectedValue);
                LeaveEncashmentRounding rounding = (LeaveEncashmentRounding)int.Parse(ddlLeaveBalanceRounding.SelectedValue);

                bool disableAtteTimeChange = bool.Parse(ddlDisableToChangeAtteTime.SelectedValue);

                bool multipleOvertimeReqApproval = bool.Parse(ddlMORA.SelectedValue);

                OvertimeManager.UpdateSettings(type, LeaveRequestSettingUsingPreDefinedList, LeaveRequestSettngUsingOneToOne, chkMultipleReview.Checked, 
                    rounding, disableAtteTimeChange, multipleOvertimeReqApproval,txtLateGraceMin.Text.Trim(),txtLateDisplayGraceMin.Text.Trim(),
                    ddlApprovedLeaveNotificationType.SelectedValue,txtApprovedLeaveNotificationMails.Text.Trim(),txtRequestLeaveNotificationMails.Text.Trim(),
                    ddlStatusSelectionInLeave.SelectedValue,ddlDisableEmpoyeePortal.SelectedValue,ddlHideUnapidLeave.SelectedValue,
                    calLockDate.Text, txtAbsentNotificationMails.Text,chkSelectionOptionInLeaveApproval.Checked);

                MyCache.Reset();
            }
            else
            {
                divWarningMsg.InnerHtml = "All settings must be selected for successfull save.";
                divWarningMsg.Hide = false;
            }
        }

        protected void btnMutlipleReview_Click(object sender, EventArgs e)
        {
            chkMultipleReview.Enabled = true;
        }

        protected void ddlRequestManagementMethods_Select(object sender,EventArgs e)
        {
            if (ddlRequestManagementMethods.SelectedIndex == 0)
                rowSelectionOptionInLeaveApprovalForTeamOptions2.Visible=  rowSelectionOptionInLeaveApprovalForTeamOptions.Visible = true;
            else
                rowSelectionOptionInLeaveApprovalForTeamOptions2.Visible=  rowSelectionOptionInLeaveApprovalForTeamOptions.Visible = false;
        }

        protected void btnSelectionOptionInLeaveApproval_Click(object sender, EventArgs e)
        {
            chkSelectionOptionInLeaveApproval.Enabled = true;
        }
        void SetDefault(Button btnEdit, Button btnCancel, DropDownList ddl)
        {
            btnEdit.Visible = true;
            btnCancel.Visible = false;
            ddl.Enabled = false;
        }
        void SetDefault(Button btnEdit, Button btnCancel, TextBox ddl)
        {
            btnEdit.Visible = true;
            btnCancel.Visible = false;
            ddl.Enabled = false;
        }
        protected void btnManagement_Click(object sender, EventArgs e)
        {
            ddlRequestManagementMethods.Enabled = true;
        }

        protected void ddlLeaveBalanceRounding_Click(object sender, EventArgs e)
        {
            ddlLeaveBalanceRounding.Enabled = true;
        }
        protected void ddlTreatmentOfEncashment_Click(object sender, EventArgs e)
        {
            ddlTreatmentOfEncashment.Enabled = true;
        }
        protected void btnEdit1_Click(object sender, EventArgs e)
        {
            btnEdit1.Visible = false;
            btnCancel1.Visible = true;
            ddlLeaveStartMonth.Enabled = true;
        }
        protected void btnEditTakenOrder_Click(object sender, EventArgs e)
        {
            ddlTakenOrder.Enabled = true;
        }
        protected void btnEditLeaveStatusSelection_Click(object sender, EventArgs e)
        {
            ddlStatusSelectionInLeave.Enabled = true;
        }
        protected void btnEditNegative_Click(object sender, EventArgs e)
        {
            chkAllowNegative.Enabled = true;
        }
        protected void btnEditTreatHolidy_Click(object sender, EventArgs e)
        {
            chkTreatHolidayAsLeave.Enabled = true;
        }
        protected void btnCancel1_Click(object sender, EventArgs e)
        {
            btnEdit1.Visible = true;
            btnCancel1.Visible = false;
            ddlLeaveStartMonth.Enabled = false;

            Company company = compMgr.GetById(SessionManager.CurrentCompanyId);
            UIHelper.SetSelectedInDropDown(ddlLeaveStartMonth, company.LeaveStartMonth);
        }

        protected void btnEdit2_Click(object sender, EventArgs e)
        {
            btnEdit2.Visible = false;
            btnCancel2.Visible = true;
            ddlEncashLeavePaysOn.Enabled = true;
        }

        protected void btnCancel2_Click(object sender, EventArgs e)
        {
            btnEdit2.Visible = true;
            btnCancel2.Visible = false;
            ddlEncashLeavePaysOn.Enabled = false;

            Company company = compMgr.GetById(SessionManager.CurrentCompanyId);
            UIHelper.SetSelectedInDropDown(ddlEncashLeavePaysOn, company.LeaveEncashTypePaysOn);
        }

        protected void btnLapseEncashProcessingEnable_Click(object sender, EventArgs e)
        {
            btnLapseEncashProcessingEnable.Visible = false;
            btnLapseEncashProcessingCancel.Visible = true;
            ddlLapseEncashProcessingMonth.Enabled = true;
        }

        protected void btnLapseEncashProcessingCancel_Click(object sender, EventArgs e)
        {
            btnLapseEncashProcessingEnable.Visible = true;
            btnLapseEncashProcessingCancel.Visible = false;
            ddlLapseEncashProcessingMonth.Enabled = false;

            Company company = compMgr.GetById(SessionManager.CurrentCompanyId);
            UIHelper.SetSelectedInDropDown(ddlEncashLeavePaysOn, company.LeaveLapseEncashProcessingMonth == null ? "-1" 
                : company.LeaveLapseEncashProcessingMonth.Value.ToString());
        }
        protected void btnEdit3_Click(object sender, EventArgs e)
        {
            btnEdit3.Visible = false;
            btnCancel3.Visible = true;
            ddlLapseEncashLeavePaysOn.Enabled = true;
        }

        protected void btnCancel3_Click(object sender, EventArgs e)
        {
            btnEdit3.Visible = true;
            btnCancel3.Visible = false;
            ddlLapseEncashLeavePaysOn.Enabled = false;

            Company company = compMgr.GetById(SessionManager.CurrentCompanyId);
            UIHelper.SetSelectedInDropDown(ddlLapseEncashLeavePaysOn, company.LeaveLapseEncashTypePaysOn);
        }

        protected void btnGraceMinEdit_Click(object sender, EventArgs e)
        {
            btnGraceMinEdit.Visible = false;
            btnGraceMinCancel.Visible = true;
            txtLateGraceMin.Enabled = true;
        }

        protected void btnGraceMinCancel_Click(object sender, EventArgs e)
        {
            btnGraceMinEdit.Visible = true;
            btnGraceMinCancel.Visible = false;
            txtLateGraceMin.Enabled = false;
        }
        protected void btnEdit4_Click(object sender, EventArgs e)
        {
            btnEdit4.Visible = false;
            btnCancel4.Visible = true;
            leaveApprovalPastDays.Enabled = true;
        }

        protected void btnCancel4_Click(object sender, EventArgs e)
        {
            btnEdit4.Visible = true;
            btnCancel4.Visible = false;
            leaveApprovalPastDays.Enabled = false;

            leaveApprovalPastDays.Text = CommonManager.CompanySetting.LeaveApprovalMinimumPastDays.ToString();           
        }

        protected void btnEdit55_Click(object sender, EventArgs e)
        {
            btnEdit55.Visible = false;
            btnEdit55.Visible = true;
            leaveRequestPastDays.Enabled = true;
        }

        protected void btnCancel55_Click(object sender, EventArgs e)
        {
            btnEdit55.Visible = true;
            btnCancel55.Visible = false;
            leaveRequestPastDays.Enabled = false;

            leaveRequestPastDays.Text = CommonManager.CompanySetting.LeaveRequestMinimumPastDays.ToString();
        }

        protected void btnEdit5_Click(object sender, EventArgs e)
        {
            btnEdit5.Visible = false;
            btnCancel5.Visible = true;
            txtLeaveRequestValidMonths.Enabled = true;
        }

        protected void btnCancel5_Click(object sender, EventArgs e)
        {
            btnEdit5.Visible = true;
            btnCancel5.Visible = false;
            txtLeaveRequestValidMonths.Enabled = false;

            txtLeaveRequestValidMonths.Text = CommonManager.CompanySetting.LeaveRequestValidMonths.ToString();
        }



        protected void btnEditAttComment_Click(object sender, EventArgs e)
        {
            btnSaveAttCommentDays.Visible = false;
            btnCancelDays.Visible = true;
            cmbAttCommentDays.Enabled = true;
        }

        protected void btnTimeChange_Click(object sender, EventArgs e)
        {
          
            ddlDisableToChangeAtteTime.Enabled = true;
        }

        protected void btnCancelAttComment_Click(object sender, EventArgs e)
        {
            btnSaveAttCommentDays.Visible = true;
            btnCancelDays.Visible = false;
            cmbAttCommentDays.Enabled = false;

            
            UIHelper.SetSelectedInDropDown(cmbAttCommentDays, CommonManager.Setting.AttendanceCommentByEmpThreshold);
            
        }

        protected void btnETARD_Click(object sender, EventArgs e)
        {
            ddlETARD.Enabled = true;
            btnETARD.Visible = true;
            btnETARDCancel.Visible = false;
        }

        protected void btnETARDCancel_Click(object sender, EventArgs e)
        {
            ddlETARD.Enabled = false;
            btnETARD.Visible = false;
            btnETARDCancel.Visible = true;
            UIHelper.SetSelectedInDropDown(ddlETARD, CommonManager.Setting.EmpTimeAttRequestThreshold);
        }

        protected void btnMORA_Click(object sender, EventArgs e)
        {
            ddlMORA.Enabled = true;
        }

        protected void btnEmpActivity_Click(object sender, EventArgs e)
        {
            btnEmpActivity.Visible = false;
            btnCancelEmpActivity.Visible = true;
            ddlEmpActivity.Enabled = true;
        }

        protected void btnCancelEmpActivity_Click(object sender, EventArgs e)
        {
            btnEmpActivity.Visible = true;
            btnCancelEmpActivity.Visible = false;
            ddlEmpActivity.Enabled = false;


            UIHelper.SetSelectedInDropDown(ddlEmpActivity, CommonManager.Setting.EmployeeActvityThreshold);

        }
    }

}

