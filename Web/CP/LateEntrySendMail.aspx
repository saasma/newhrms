<%@ Page Title="Send Mail for Late In Employee." Language="C#" MasterPageFile="~/Master/NewDetails.Master"
    AutoEventWireup="true" CodeBehind="LateEntrySendMail.aspx.cs" Inherits="Web.CP.LateEntrySendMail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <div style="padding: 10px">
        <ext:Button ID="bntSendMail" runat="server" Text="Send Mail for Late In Employee">
            <DirectEvents>
                <Click OnEvent="btn_SendMail">
                    <Confirmation ConfirmRequest="true" Message="Are you sure, you want to send email for Late-In Employee?" />
                    <EventMask ShowMask="true" />
                </Click>
            </DirectEvents>
        </ext:Button>
    </div>
</asp:Content>
