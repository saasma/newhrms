﻿<%@ Page Title="Add Claim Lot" Language="C#" AutoEventWireup="true" MasterPageFile="~/Master/NewDetails.Master" CodeBehind="InsurenceClaimLot.aspx.cs" Inherits="Web.NewHR.InsurenceClaimLot" %>

<%@ Register Src="~/Controls/FooterCtl.ascx" TagName="FooterCtl" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/HeaderCtl.ascx" TagName="HeaderCtl" TagPrefix="uc3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
 
    </style>
    <script type="text/javascript">
        function searchList() {
            <%=GridClaimList.ClientID %>.getStore().pageSize = <%=cmbPageSize.ClientID %>.getValue();
            <%=PagingToolbar1.ClientID%>.doRefresh();
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <script type="text/javascript" src="<%= ResolveUrl("~/Employee/override.js?v=") + Web.Helper.WebHelper.Version %>"></script>
    <ext:Hidden ID="hdnEmployeeID" runat="server" />
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>Create Lot
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <table>
            <tr>
                <td>
                    <ext:TextField runat="server" ID="txtLotNo" LabelAlign="Left" FieldLabel="Lot No." LabelSeparator="" LabelWidth="50" Width="200" Disabled="true">
                    </ext:TextField>
                </td>
            </tr>
        </table>
        <br />
        <ext:GridPanel ID="GridClaimList" runat="server" Header="true" AutoScroll="true" Scroll="Horizontal">
            <Store>
                <ext:Store ID="StoreGridClaimList" runat="server" OnReadData="Store_ReadData" RemoteSort="true" AutoLoad="true">
                    <Proxy>
                        <ext:PageProxy />
                    </Proxy>
                    <Model>
                        <ext:Model ID="ModelGridClaimList" runat="server" IDProperty="ClaimID">
                            <Fields>
                                <ext:ModelField Name="ClaimID" />
                                <ext:ModelField Name="EmployeeID" />
                                <ext:ModelField Name="Name" />
                                <ext:ModelField Name="ClaimValue" />
                                <ext:ModelField Name="ClaimDate" Type="Date" />
                                <ext:ModelField Name="RelationName" />
                                <ext:ModelField Name="TotalClaimAmount" />
                                <ext:ModelField Name="statusValue" />
                            </Fields>
                        </ext:Model>
                    </Model>
                </ext:Store>
            </Store>
            <ColumnModel>
                <Columns>
                    <ext:Column ID="Column1" Sortable="false" MenuDisabled="true" runat="server" Text="EIN" Width="80" DataIndex="EmployeeID" />
                    <ext:Column ID="Column2" Sortable="false" MenuDisabled="true" runat="server" Text="Employee Name" Width="200" DataIndex="Name" />
                    <ext:Column ID="Column3" Sortable="false" MenuDisabled="true" runat="server" Text="Claim Type" Width="120" DataIndex="ClaimValue" />
                    <ext:DateColumn ID="Column4" Sortable="false" MenuDisabled="true" runat="server" Text="Claim Date" Format="yyyy-MM-dd" Width="120" DataIndex="ClaimDate" />
                    <ext:Column ID="Column5" Sortable="false" MenuDisabled="true" runat="server" Text="Claim Of" Width="120" DataIndex="RelationName" />
                    <ext:NumberColumn ID="Column6" Sortable="false" MenuDisabled="true" runat="server" Text="Claim Amount" Width="150" DataIndex="TotalClaimAmount" />
                    <ext:Column ID="Column7" Sortable="false" MenuDisabled="true" runat="server" Text="Status" Width="120" DataIndex="statusValue" />
                    <ext:Column ID="Column8" runat="server" Flex="1" Sortable="false" MenuDisabled="true" />
                </Columns>
            </ColumnModel>
            <SelectionModel>
                <ext:CheckboxSelectionModel runat="server" Mode="Multi" />
            </SelectionModel>
            <BottomBar>
                <ext:PagingToolbar Height="40" ID="PagingToolbar1" runat="server" StoreID="StoreGridClaimList" DisplayInfo="true">
                    <Items>
                        <ext:Label ID="lblPageSize" runat="server" Text="Page size:" />
                        <ext:ToolbarSpacer ID="ToolbarSpacer_listItems" runat="server" Width="10" />
                        <ext:ComboBox ID="cmbPageSize" runat="server" Width="80" SelectOnFocus="true" Selectable="true"
                            ValueField="Value" DisplayField="Text" ForceSelection="true" AllowBlank="false">
                            <Listeners>
                                <Select Handler="searchList()" />
                            </Listeners>
                            <Items>
                                <ext:ListItem Value="100000" Text="All" />
                                <ext:ListItem Value="10" Text="10" />
                                <ext:ListItem Value="20" Text="20" />
                                <ext:ListItem Value="50" Text="50" />
                            </Items>
                            <SelectedItems>
                                <ext:ListItem Index="0">
                                </ext:ListItem>
                            </SelectedItems>
                        </ext:ComboBox>
                    </Items>
                </ext:PagingToolbar>
            </BottomBar>
        </ext:GridPanel>
        <table>
            <tr>
                <td>
                    <ext:Button runat="server" MarginSpec="10 0 0 0" Cls="btn btn-primary" ID="btnSave" Text="<i></i>Save">
                          <DirectEvents>
                            <Click OnEvent="btnSave_Click">
                                <EventMask ShowMask="true" />
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
