﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using DAL;
using BLL.Base;
using Utils.Web;

namespace Web.CP
{
    public partial class Grade1History : BasePage
    {
        private CommonManager comm = new CommonManager();
        private List<BLL.BO.PositionHistoryBO> list;
     
        protected void Page_Load(object sender, EventArgs e)
        {
            this.CustomId = int.Parse(Request.QueryString["EIN"]);           
            if (!IsPostBack)
            {
                this.Initialise();               
            }
        }

        private void Initialise()
        {
            this.BindData();
        }

        private void BindData()
        {
            this.list = PositionGradeStepAmountManager.GetEmployeeGradeHistory(CustomId);
            this.gvw.DataSource = list;
            this.gvw.DataBind();
        }

       

        protected void gvw_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            this.gvw.PageIndex = e.NewPageIndex;
            this.BindData();
        }

        
    }
}
