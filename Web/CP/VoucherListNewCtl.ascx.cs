﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using Utils.Helper;
using Utils.Web;
using BLL.Base;
using DAL;
using Utils.Base;
using Bll;
using Ext.Net;
using BLL.BO;

namespace Web
{


    public partial class VoucherListNewCtl : BaseUserControl
    {

        int payrollPeriodId;
        /// <summary>
        /// For Excel export to work
        /// </summary>
        /// <param name="control"></param>
        /// 

      

        
        void SetPeriod()
        {
            int month = 0;
            int year = 0;

            if (!string.IsNullOrEmpty(ddlPayrollFromMonth.SelectedValue))
            {


                month = int.Parse(ddlPayrollFromMonth.SelectedValue);
                year = int.Parse(ddlPayrollFromYear.SelectedValue);

                PayrollPeriod period = CommonManager.GetPayrollPeriod(month, year);
                if (period != null)
                    payrollPeriodId = period.PayrollPeriodId;
            }
        }

        protected void LoadAddOn()
        {

            System.Web.UI.WebControls.ListItem item = ddlAddOnName.SelectedItem;


            ddlAddOnName.DataSource = PayManager.GetAddOnForPayrollPeriod(payrollPeriodId);
            ddlAddOnName.DataBind();

            if (item != null && item.Selected)
            {
                foreach (System.Web.UI.WebControls.ListItem selItem in ddlAddOnName.Items)
                {
                    if (selItem.Value == item.Value)
                        selItem.Selected = true;
                }
            }
        }

        protected void ddlVoucherType_SelectedIndexChanged(object sender, EventArgs e)
        {
            int typeId = int.Parse(ddlVoucherType.SelectedValue);
            // if cit voucher
            if (typeId == 2 && CommonManager.CompanySetting.WhichCompany==WhichCompany.NIBL)
            {
                tdExternalCIT1.Visible = true;
                tdExternalCIT2.Visible = true;
            }
            else
            {
                tdExternalCIT1.Visible = false;
                tdExternalCIT2.Visible = false;
            }
        }


        public void btnLoadSelectedEmpVoucher_Click(object sender, EventArgs e)
        {
            windowNew.Hide();

            int addOnId = -1;
            if (!string.IsNullOrEmpty(ddlAddOnName.SelectedValue) && ddlAddOnName.SelectedValue != "-1")
                addOnId = int.Parse(ddlAddOnName.SelectedValue);

            CheckboxSelectionModel selection = grid.GetSelectionModel() as CheckboxSelectionModel;
            string empIDList = "";
            foreach (var item in selection.SelectedRows)
            {
                if (empIDList == "")
                    empIDList = item.RecordID.ToString();
                else
                    empIDList += "," + item.RecordID.ToString();
            }

            hiddenSelectedEmp.Value = empIDList;

            if (hiddenAddOnId.Value != "" && hiddenAddOnId.Value != addOnId.ToString())
            {
                hiddenSelectedEmp.Value = "";
            }

            hiddenAddOnId.Value = addOnId.ToString();

            BindEmployees();
        }

        public void btnClearSelection_Click(object sender, EventArgs e)
        {
            windowNew.Hide();

            int addOnId = -1;
            if (!string.IsNullOrEmpty(ddlAddOnName.SelectedValue) && ddlAddOnName.SelectedValue != "-1")
                addOnId = int.Parse(ddlAddOnName.SelectedValue);



            hiddenSelectedEmp.Value = "";



            hiddenAddOnId.Value = addOnId.ToString();

            BindEmployees();
        }

        protected void btnLoad_Click(object sender, EventArgs e)
        {
            BindEmployees();
        }

        public void btnLoadEmployeeFilter_Click(object sender, EventArgs e)
        {
            int addOnId = -1;
            if (!string.IsNullOrEmpty(ddlAddOnName.SelectedValue) && ddlAddOnName.SelectedValue != "-1"
                && isAddOn.Checked)
                addOnId = int.Parse(ddlAddOnName.SelectedValue);

            //if (addOnId == -1)
            //{
            //    NewMessage.ShowWarningMessage("Add on name should be selected for the filter.");
            //    return;
            //}
            //CheckboxSelectionModel

            //if (hiddenAddOnId.Value == addOnId.ToString())
            //{
            //    window.Center();
            //    window.Show();
            //    return;
            //}
            //else
            //{
            //    hiddenSelectedEmp.Value = "";
            //}

            //hiddenAddOnId.Value = addOnId.ToString();
            if (addOnId != -1)
            {
                grid.Store[0].DataSource = PartialAddOnManager.GetAddOnEmpList(addOnId);
                grid.Store[0].DataBind();
            }
            else
            {
                grid.Store[0].DataSource = PartialAddOnManager.GetRetiredEmpList(int.Parse(ddlPayrollFromMonth.SelectedValue),
                    int.Parse(ddlPayrollFromYear.SelectedValue));

                grid.Store[0].DataBind();
            }

            // Select in grid previously selected employees

            RowSelectionModel sm = this.grid.GetSelectionModel() as RowSelectionModel;

            string[] idList = hiddenSelectedEmp.Value.Split(new char[] { ',' });

            sm.SelectedRows.Clear();
            foreach (string id in idList)
            {
                if (!string.IsNullOrEmpty(id))
                    sm.SelectedRows.Add(new SelectedRow(id));
            }



            BindEmployees();

            windowNew.Center();
            windowNew.Show();

        }

        protected void Page_Load(object sender, EventArgs e)
        {

            if (this.Visible)
            {

                if (!IsPostBack && !X.IsAjaxRequest)
                {

                    if (CommonManager.CompanySetting.WhichCompany == WhichCompany.NIBL)
                    {
                        tdCardDepartmentFilter1.Visible = true;
                        tdCardDepartmentFilter2.Visible = true;

                        //ddlCardDepFilter.SelectedIndex = 2;
                    }
                    else
                    {
                        tdCardDepartmentFilter1.Visible = false;
                        tdCardDepartmentFilter2.Visible = false;
                    }


                    ddlVoucherType.DataSource = VouccherManagerNew.GetVoucherTypeList();
                    ddlVoucherType.DataBind();

                    ddlBranch.DataSource = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId);
                    ddlBranch.DataBind();

                    CommonManager.SetFirstAndLastFinalSavedPayrollPeriod(ddlPayrollFromYear, ddlPayrollFromMonth);

                    if (CommonManager.CompanySetting.WhichCompany != WhichCompany.NIBL)
                    {
                        rowDateFilter1.Visible = true;
                        rowDateFilter2.Visible = true;

                        statusRow1.Visible = true;
                        statusRow2.Visible=true;

                        JobStatus statues = new JobStatus();
                        List<KeyValue> statusList = statues.GetMembers();

                        statusList.RemoveAt(0);

                        foreach (KeyValue item in statusList)
                        {
                            MultiCheckCombo1.AddItems(item.Value, item.KeyValueCombined);
                        }
                    }
                }


                SetPeriod();

                LoadAddOn();

                //{

                //    BindEmployees();

                //}
            }
        }


        public void ddlAddOnName_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlAddOnName.SelectedValue))
            {
                List<TextValue> list = PayManager.GetAddOnDateList(int.Parse(ddlAddOnName.SelectedValue)); ;
                list.Insert(0, new TextValue { Text = "--Select Date--", Value = "All" });

                System.Web.UI.WebControls.ListItem selectedAddOnDate = ddlAddOnDateFilter.SelectedItem;

                ddlAddOnDateFilter.DataSource = list;
                ddlAddOnDateFilter.DataBind();

                if (selectedAddOnDate != null && selectedAddOnDate.Selected)
                {
                    foreach (System.Web.UI.WebControls.ListItem selItem in ddlAddOnDateFilter.Items)
                    {
                        if (selItem.Value == selectedAddOnDate.Value)
                        {
                            selItem.Selected = true;
                        }
                    }
                }
            }
        }



       

        //string value = dataSource.GetValue(header);
        void BindEmployees()
        {
            if (payrollPeriodId == 0)
                return;

            bool? showRetiredOnly = null;
            if (ddlRetirementType.SelectedValue != "All")
                showRetiredOnly = bool.Parse(ddlRetirementType.SelectedValue);

            bool? cardDepOnly = null;
            if (ddlCardDepFilter.SelectedValue != "-1")
                cardDepOnly = bool.Parse(ddlCardDepFilter.SelectedValue);

            if (IsPostBack)
            {
                if (ddlVoucherType.SelectedIndex == 0)
                {
                    JavascriptHelper.DisplayClientMsg("Voucher type should be selected.", Page);
                    return;
                }
            }

            int typeId = int.Parse(ddlVoucherType.SelectedValue);

            bool? includeExternalCIT = null;
            // if cit voucher
            if (typeId == 2 && CommonManager.CompanySetting.WhichCompany == WhichCompany.NIBL)
            {
                includeExternalCIT = bool.Parse(ddlExternalCIT.SelectedValue);
            }


            // Process for Status Filter
            JobStatus statues = new JobStatus();
            List<KeyValue> statusList = statues.GetMembers();
            string[] statusTexts = MultiCheckCombo1.Text.Split(new char[] { ',' });
            string statusIDTexts = "";
            foreach (string item in statusTexts)
            {
                string text = item.ToString().ToLower().Trim();
                KeyValue status = statusList.FirstOrDefault(x => x.Value.ToString().ToLower().Trim() == text);
                if (status != null)
                {
                    if (statusIDTexts == "")
                        statusIDTexts = status.Key;
                    else
                        statusIDTexts += "," + status.Key;
                }
            }


            if (isAddOn.Checked == false)
            {
                List<GetNIBLVoucherReportResult> list = EmployeeManager.GetNIBLVoucherReport(payrollPeriodId, showRetiredOnly
                      , typeId, int.Parse(ddlBranch.SelectedValue), cardDepOnly, includeExternalCIT, hiddenSelectedEmp.Value.Trim(), statusIDTexts);

                if (CommonManager.CompanySetting.WhichCompany == WhichCompany.NCC)
                {


                    gvEmployeeIncomeNCCVoucher1.DataSource = EmployeeManager.ConvertNIBLToNCCVoucherList(list);
                    gvEmployeeIncomeNCCVoucher1.DataBind();
                    gvEmployeeIncomeNCCVoucher1.Visible = true;
                }
                else
                {                   
                    gvEmployeeIncome.DataSource = list;
                    gvEmployeeIncome.DataBind();
                }
            }
            else
            {
                if (string.IsNullOrEmpty(ddlAddOnName.SelectedValue))
                {
                    JavascriptHelper.DisplayClientMsg("Add on should be selected.", Page);
                    return;
                }

                bool applyAddOndateFilter = false;
                DateTime? addOnDate = null;

                if (ddlAddOnDateFilter.SelectedIndex != 0)
                {
                    applyAddOndateFilter = true;
                    if (ddlAddOnDateFilter.SelectedItem != null && ddlAddOnDateFilter.SelectedItem.Text != "")
                        addOnDate = Convert.ToDateTime(ddlAddOnDateFilter.SelectedItem.Value);
                }

                List<GetNIBLVoucherMultipleAddOnReportResult> list = EmployeeManager.GetNIBLVoucherReportForAddOn(payrollPeriodId
                    , int.Parse(ddlVoucherType.SelectedValue), int.Parse(ddlAddOnName.SelectedValue), int.Parse(ddlBranch.SelectedValue), cardDepOnly
                    , hiddenSelectedEmp.Value.Trim(), applyAddOndateFilter, addOnDate);

                if (CommonManager.CompanySetting.WhichCompany == WhichCompany.NCC)
                {
                    gvEmployeeIncomeNCCVoucher1.DataSource = EmployeeManager.ConvertNIBLToNCCVoucherList(list);
                    gvEmployeeIncomeNCCVoucher1.DataBind();
                    gvEmployeeIncomeNCCVoucher1.Visible = true;
                }
                else
                {
                    gvEmployeeIncome.DataSource = list;
                    gvEmployeeIncome.DataBind();
                }
                

            }
            

            if (!string.IsNullOrEmpty(hiddenSelectedEmp.Value))
                msg.InnerHtml = "Voucher generated for " + (hiddenSelectedEmp.Value.Split(new char[] { ',' }).Length) + " employees only.";
            else if (!string.IsNullOrEmpty(MultiCheckCombo1.Text))
                msg.InnerHtml = "Voucher generated for the status " + MultiCheckCombo1.Text;
            else//if (hiddenSelectedEmp.Value == "")
                msg.InnerHtml = "Voucher generated for all employees.";

            if (CalculationManager.IsAllEmployeeSavedFinally(payrollPeriodId, false))
            {
                salaryNotSaved.Visible=false;
            }
            else
            {
                int totalEmployees = CalculationManager.GetTotalApplicableForSalary(payrollPeriodId, false);
                int initialSave = new CalculationManager().GetTotalEmployeeInitialSave(payrollPeriodId, false);

                if (totalEmployees != initialSave)
                    salaryNotSaved.Visible = true;
                else
                    salaryNotSaved.Visible = false;

            }


            // Warning if any Income/Deduction head is not mapped
            string headerName = CalculationManager.GetIncomeOrDeductionNotMappedForVoucher(payrollPeriodId);
            if (!string.IsNullOrEmpty(headerName))
            {
                spanWarningForIncomeDeductionHead.InnerHtml = "\"" + headerName + "\"<br> is not mapped from Voucher Setting, if this income/deduction has amount then the voucher will not be correct.";
                spanWarningForIncomeDeductionHead.Visible = true;
            }
            else
                spanWarningForIncomeDeductionHead.Visible = false;

        }



        protected void btnExport_Click(object sender, EventArgs e)
        {

            bool? showRetiredOnly = null;
            if (ddlRetirementType.SelectedValue != "All")
                showRetiredOnly = bool.Parse(ddlRetirementType.SelectedValue);

            bool? cardDepOnly = null;
            if (ddlCardDepFilter.SelectedValue != "-1")
                cardDepOnly = bool.Parse(ddlCardDepFilter.SelectedValue);

            Dictionary<string, string> title = new Dictionary<string, string>();

            PayrollPeriod period = CommonManager.GetPayrollPeriod(payrollPeriodId);

            title["Voucher Report"] = "For " + period.Name + ", Voucher Type : " +  
                (int.Parse(ddlVoucherType.SelectedValue)==-1? "" : ddlVoucherType.SelectedItem.Text) + ", Branch : " +
                (int.Parse(ddlBranch.SelectedValue)==-1?"" : ddlBranch.SelectedItem.Text) + 
                (isAddOn.Checked ? ", Add On : " + ddlAddOnName.SelectedItem.Text : "");



            if (isAddOn.Checked == false)
            {
                // Process for Status Filter
                JobStatus statues = new JobStatus();
                List<KeyValue> statusList = statues.GetMembers();
                string[] statusTexts = MultiCheckCombo1.Text.Split(new char[] { ',' });
                string statusIDTexts = "";
                foreach (string item in statusTexts)
                {
                    string text = item.ToString().ToLower().Trim();
                    KeyValue status = statusList.FirstOrDefault(x => x.Value.ToString().ToLower().Trim() == text);
                    if (status != null)
                    {
                        if (statusIDTexts == "")
                            statusIDTexts = status.Key;
                        else
                            statusIDTexts += "," + status.Key;
                    }
                }

                int typeId = int.Parse(ddlVoucherType.SelectedValue);
                bool? includeExternalCIT = null;
                // if cit voucher
                if (typeId == 2 && CommonManager.CompanySetting.WhichCompany == WhichCompany.NIBL)
                {
                    includeExternalCIT = bool.Parse(ddlExternalCIT.SelectedValue);
                }

                if (CommonManager.CompanySetting.WhichCompany == WhichCompany.Janata)
                {
                    List<FlexCubeVoucherBO> list = EmployeeManager.GetFlexCubeVoucher(payrollPeriodId, showRetiredOnly
                        , int.Parse(ddlVoucherType.SelectedValue), int.Parse(ddlBranch.SelectedValue), cardDepOnly, includeExternalCIT, hiddenSelectedEmp.Value.Trim(), statusIDTexts);

                    ExcelHelper.ExportToExcel<FlexCubeVoucherBO>(
                        "VoucherList.xls",
                        list,
                        new List<string> { },
                        new List<string> { },
                        new Dictionary<string, string> { }
                        , new List<string> { "Amount", "LCY_EQUIVALENT" }
                        , new List<string> { "CURR_NO" }
                        , new List<string> { "INITIATION_DATE", "VALUE_DATE" }
                        , title
                        , new List<string> { }

                         );
                }
                else
                {

                    List<GetNIBLVoucherReportResult> list = EmployeeManager.GetNIBLVoucherReport(payrollPeriodId, showRetiredOnly
                        , int.Parse(ddlVoucherType.SelectedValue), int.Parse(ddlBranch.SelectedValue), cardDepOnly, includeExternalCIT, hiddenSelectedEmp.Value.Trim(), statusIDTexts);

                    if (CommonManager.CompanySetting.WhichCompany == WhichCompany.NCC)
                    {

                        List<NCCVoucherBO> nccList = EmployeeManager.ConvertNIBLToNCCVoucherList(list);
                        ExcelHelper.ExportToExcel<NCCVoucherBO>(
                            "VoucherList.xls",
                            nccList,
                            new List<string> {  },
                            new List<string> { },
                            new Dictionary<string, string> { }
                            , new List<string> { "Amount" }
                            , title
                            , new List<string> { }

                             );
                    }
                    else
                    {
                        ExcelHelper.ExportToExcel<GetNIBLVoucherReportResult>(
                            "VoucherList.xls",
                            list,
                            new List<string> { "Class", "Location", "Entity" },
                            new List<string> { },
                            new Dictionary<string, string> { { "CurrCode", "CYCODE" }, { "BranchCode", "BRCODE" }, { "TranCode", "TRANTYPE" }, { "TranType", "Desc1" } }
                            , new List<string> { "AMOUNT", "LCYamount" }
                            , title
                            , new List<string> { "MainCode", "CurrCode", "BranchCode", "TranCode", "Amount", }

                             );
                    }
                }
            }
            else
            {

                if (CommonManager.CompanySetting.WhichCompany == WhichCompany.Janata)
                {
                    bool applyAddOndateFilter = false;
                    DateTime? addOnDate = null;

                    if (ddlAddOnDateFilter.SelectedIndex != 0)
                    {
                        applyAddOndateFilter = true;
                        if (ddlAddOnDateFilter.SelectedItem != null && ddlAddOnDateFilter.SelectedItem.Text != "")
                            addOnDate = Convert.ToDateTime(ddlAddOnDateFilter.SelectedItem.Value);
                    }

                    List<FlexCubeVoucherBO> list = EmployeeManager.GetFlexCubeVoucherAddon(payrollPeriodId
                        , int.Parse(ddlVoucherType.SelectedValue), int.Parse(ddlAddOnName.SelectedValue), int.Parse(ddlBranch.SelectedValue), cardDepOnly
                        , hiddenSelectedEmp.Value.Trim(),applyAddOndateFilter,addOnDate);

                    ExcelHelper.ExportToExcel<FlexCubeVoucherBO>(
                        "VoucherList.xls",
                        list,
                        new List<string> { },
                        new List<string> { },
                        new Dictionary<string, string> { }
                        , new List<string> { "Amount", "LCY_EQUIVALENT" }
                        , new List<string> { "CURR_NO" }
                        , new List<string> { "INITIATION_DATE", "VALUE_DATE" }
                        , title
                        , new List<string> { }

                         );
                }
                else
                {

                    List<GetNIBLVoucherMultipleAddOnReportResult> list = new List<GetNIBLVoucherMultipleAddOnReportResult>();

                    bool applyAddOndateFilter = false;
                    DateTime? addOnDate = null;

                    if (ddlAddOnDateFilter.SelectedIndex != 0)
                    {
                        applyAddOndateFilter = true;
                        if (ddlAddOnDateFilter.SelectedItem != null && ddlAddOnDateFilter.SelectedItem.Text != "")
                            addOnDate = Convert.ToDateTime(ddlAddOnDateFilter.SelectedItem.Value);
                    }

                    if (!string.IsNullOrEmpty(ddlAddOnName.SelectedValue))
                    {
                        list = EmployeeManager.GetNIBLVoucherReportForAddOn(payrollPeriodId
                        , int.Parse(ddlVoucherType.SelectedValue), int.Parse(ddlAddOnName.SelectedValue), int.Parse(ddlBranch.SelectedValue), cardDepOnly
                        , hiddenSelectedEmp.Value.Trim(),applyAddOndateFilter,addOnDate);

                    }

                    if (CommonManager.CompanySetting.WhichCompany == WhichCompany.NCC)
                    {

                        List<NCCVoucherBO> nccList = EmployeeManager.ConvertNIBLToNCCVoucherList(list);
                        ExcelHelper.ExportToExcel<NCCVoucherBO>(
                            "VoucherList.xls",
                            nccList,
                            new List<string> { },
                            new List<string> { },
                            new Dictionary<string, string> { }
                            , new List<string> { "AMOUNT","LCYamount" }
                            , title
                            , new List<string> { }

                             );
                    }
                    else
                    {
                        ExcelHelper.ExportToExcel<GetNIBLVoucherMultipleAddOnReportResult>(
                         "VoucherList.xls",
                         list,
                         new List<string> { "Class", "Location", "Entity" },
                         new List<string> { },
                         new Dictionary<string, string> { { "CurrCode", "CYCODE" }, { "BranchCode", "BRCODE" }, { "TranCode", "TRANTYPE" }, { "TranType", "Desc1" } }
                         , new List<string> { "Amount" }
                         , title
                         , new List<string> { "MainCode", "CurrCode", "BranchCode", "TranCode", "Amount", }

                          );
                    }
                }
            }


        }
        public void ExportToExcel()
        {
            bool gridViewLastColumnVisibility = gvEmployeeIncome.Columns[gvEmployeeIncome.Columns.Count - 1].Visible;
            GridLines gridViewGridLines = gvEmployeeIncome.GridLines;




            Response.Clear();

            Response.AddHeader("content-disposition", "attachment; filename=\"Voucher List.xls\"");

            //Response.Charset = "";


            Response.ContentType = "application/vnd.xls";
            //Response.ContentType = "application/ms-excel";
            Response.ContentEncoding = System.Text.Encoding.Default;
            //Response.ContentEncoding = Encoding.;
            Response.Charset = "utf-8";

            Response.HeaderEncoding = Encoding.UTF8;
            System.IO.StringWriter stringWrite = new System.IO.StringWriter();

            System.Web.UI.HtmlTextWriter htmlWrite =
            new HtmlTextWriter(stringWrite);
            //  gvw.HeaderStyle.BackColor = System.Drawing.Color.Red;



            //gvw.DataSource = CalculationManager.GetCalculationList(SessionManager.CurrentCompanyId,
            //        this.GetPayrollPeriodId(), 1,
            //        999999, ref _tempCount, chkHasRetiredOrResigned.Checked, null);
            // gvw.DataBind();





            ClearControls(gvEmployeeIncome);


           // gvw.Columns[gvEmployeeIncome.Columns.Count - 1].Visible = false;
            gvEmployeeIncome.GridLines = GridLines.Both;


            foreach (GridViewRow r in gvEmployeeIncome.Rows)
            {
                int columnIndex = 0;
                foreach (TableCell c in r.Cells)
                {
                    //skip two columns
                    if (++columnIndex > 2)
                        c.HorizontalAlign = HorizontalAlign.Right;

                    //c.Attributes.Add("class", "text");
                    // c.Text = r.Cells.ToString();
                    c.Width = 80;

                }

            }


            gvEmployeeIncome.RenderControl(htmlWrite);

            Response.Write(stringWrite.ToString());

            Response.End();

            gvEmployeeIncome.Columns[gvEmployeeIncome.Columns.Count - 1].Visible = gridViewLastColumnVisibility;
            gvEmployeeIncome.GridLines = gridViewGridLines;

            //gvw.DataSource = CalculationManager.GetCalculationList(SessionManager.CurrentCompanyId,
            //        this.GetPayrollPeriodId(), 1,
            //        int.Parse(pagingCtl.DDLRecords.SelectedValue), ref _tempCount, chkHasRetiredOrResigned.Checked, null);

            //gvw.DataBind();


        }

        private void ClearControls(Control control)
        {
            for (int i = control.Controls.Count - 1; i >= 0; i--)
            {
                ClearControls(control.Controls[i]);
            }
            //
            // if(control is Table)

            if (!(control is TableCell))
            {
                if (control.GetType().GetProperty("SelectedItem") != null)
                {
                    LiteralControl literal = new LiteralControl();
                    control.Parent.Controls.Add(literal);
                    try
                    {
                        literal.Text =
                            (string)control.GetType().GetProperty("SelectedItem").
                                GetValue(control, null);
                    }
                    catch
                    { }
                    control.Parent.Controls.Remove(control);
                }
                else if (control.GetType().GetProperty("Text") != null)
                {
                    LiteralControl literal = new LiteralControl();
                    control.Parent.Controls.Add(literal);
                    literal.Text =
                        (string)control.GetType().GetProperty("Text").
                            GetValue(control, null);
                    control.Parent.Controls.Remove(control);
                }
            }
            return;
        }





        /// <summary>
        /// Replace any of the contained controls with literals
        /// </summary>
        /// <param name="control"></param>
        private void PrepareGridViewForExport(Control gv)
        {



            System.Web.UI.WebControls.LinkButton lb = new System.Web.UI.WebControls.LinkButton();

            Literal l = new Literal();

            string name = String.Empty;

            for (int i = 0; i < gv.Controls.Count; i++)
            {


                if (gv.Controls[i].GetType() == typeof(System.Web.UI.WebControls.LinkButton))
                {

                    l.Text = (gv.Controls[i] as System.Web.UI.WebControls.LinkButton).Text;

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }

                else if (gv.Controls[i].GetType() == typeof(TextBox))
                {

                    l.Text = (gv.Controls[i] as TextBox).Text.ToString();

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);


                }

                if (gv.Controls[i].HasControls())
                {

                    PrepareGridViewForExport(gv.Controls[i]);

                }

            }
        }

        protected void isAddOn_CheckedChanged(object sender, EventArgs e)
        {
            if (isAddOn.Checked)
            {
                ddlRetirementType.SelectedValue = "All";
                ddlRetirementType.Enabled = false;
                ddlAddOnName.Enabled = true;

                ddlAddOnName_OnSelectedIndexChanged(null, null);
            }
            else
            {
                ddlRetirementType.SelectedValue = "false";
                ddlRetirementType.Enabled = true;
                ddlAddOnName.Enabled = false;
            }
        }

        protected void gvEmployeeIncome_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvEmployeeIncome.PageIndex = e.NewPageIndex;
            BindEmployees();
        }

    }

 

}
