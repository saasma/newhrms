<%@ Page MaintainScrollPositionOnPostback="true" Title="Contract Settings" Language="C#"
    MasterPageFile="~/Master/HROld.Master" AutoEventWireup="true" CodeBehind="ContractSetting.aspx.cs"
    Inherits="Web.CP.ContractSetting" %>

<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        tr
        {
            padding-top: 10px;
        }
        .headerWithUnderLine
        {
            color: #1F4E78;
            padding-bottom: 2px;
            border-bottom: 1px solid #2F75B5;
            width: 300px;
        }
        .fieldHeader
        {
            padding-top: 15px;
        }
        .fieldHeader span
        {
            color: #000000;
            font-weight: bold;
        }
        input[type=submit]
        {
            cursor: pointer;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <div class="contentArea">
        <div class="pageheader">
            <div class="media">
                <div class="media-body">
                    <h4>
                        Contract Settings
                    </h4>
                </div>
            </div>
        </div>
        <div class="contentpanel">
            <uc2:MsgCtl ID="divMsgCtl" EnableViewState="false" Hide="true" runat="server" />
            <uc2:WarningCtl ID="divWarningMsg" EnableViewState="false" Hide="true" runat="server" />
            <table style='clear: both; float: left'>
                <tr>
                    <td class="fieldHeader" style='width: 190;'>
                        <My:Label ID="Label4" Text="Contract upto day" runat="server" ShowAstrick="false" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <%--<My:Label ID="Label5" Text="IRO name:" runat="server" ShowAstrick="true" /> --%>
                        <asp:TextBox ID="txtContractDay" Enabled="false" runat="server" Width="100" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtContractDay"
                            Display="None" ErrorMessage="Please select day." ValidationGroup="AECompany"></asp:RequiredFieldValidator>
                    </td>
                    <td>
                        <asp:Button ID="btnEdit1" Width="60" Height="22" Style="margin-top: 1px; border: 1px solid gray"
                            Visible="true" runat="server" Text="Edit" OnClick="btnEdit1_Click" />
                        <asp:Button ID="btnCancel1" Width="60" Height="22" Style="margin-top: 1px; border: 1px solid gray"
                            runat="server" Text="Cancel" Visible="false" OnClick="btnCancel1_Click" />
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style='padding-top: 10px'>
                        <asp:Button ID="btnSave" CssClass="update" OnClientClick="valGroup = 'AECompany';if( CheckValidation() ) { return confirm('Are you sure, you want to change the leave setting?');}"
                            runat="server" Text="Save" ValidationGroup="AECompany" OnClick="btnSave_Click" />
                    </td>
                </tr>
            </table>
            <div class="rightHelp" style="float: right; height: 200px;">
                <div class="title">
                    <h2>
                        Help Block</h2>
                </div>
                <div class="content">
                    <p>
                        &nbsp;&nbsp;
                    </p>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
