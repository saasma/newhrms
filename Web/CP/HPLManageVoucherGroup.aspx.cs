﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using System.Text;
using BLL.Manager;
using Utils.Helper;
using DAL;
using System.Drawing;
using BLL.Base;
using BLL.Manager;

namespace Web.CP
{
    public partial class HPLManageVoucherGroup : BasePage
    {
        TaxManager taxMgr = new TaxManager();

        List<CalcGetHeaderListResult> headerList = new List<CalcGetHeaderListResult>();
        

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
               
                Initialise();

            }

            if (Request.Form["__EVENTTARGET"] != null && Request.Form["__EVENTTARGET"].Equals("Reload"))
                LoadGroup();
            
            JavascriptHelper.AttachPopUpCode(Page, "medicalTaxCreditPopup", "Popup/AEVoucherGroup.aspx", 500, 400);
            JavascriptHelper.AttachPopUpCode(Page, "voucherHead", "Popup/AESalaryHeadAssociation.aspx", 500, 400);
        }

        void Initialise()
        {

            LoadGroup();

        }

        public string GetVoucherGroupName(object value)
        {
            if (value == null)
                return "";
            if (int.Parse(value.ToString()) == 0)
                return "";
            return VouccherManager.GetVoucher(int.Parse(value.ToString())).AccountGroupName;
        }

        public string GetVoucherType(object value)
        {
            return ((VoucherGroupTypeEnum)int.Parse(value.ToString())).ToString();
        }

        public string GetVoucherGroup(object value)
        {
            return ((VoucherGroupEnum)int.Parse(value.ToString())).ToString();
        }

        void LoadGroup()
        {



            gvwList.DataSource = VouccherManager.GetVoucherGroupList();
            gvwList.DataBind();




            headerList =   CalculationManager.GetHeaderListForVoucher();

            gridIncomeDeductionList.DataSource = VouccherManager.GetHeadList().ToList();
            gridIncomeDeductionList.DataBind();


        }

        public string GetHeaderName(object type, object value)
        {
            int typev = int.Parse(type.ToString());
            int valuev = int.Parse(value.ToString());
            foreach (CalcGetHeaderListResult item in headerList)
            {

                if (item.Type == typev && item.SourceId == valuev)
                    return item.HeaderName;
            }

            return "";
        }

        protected void btnLoad_Click(object sender, EventArgs e)
        {
            

            //_tempCurrentPage = int.Parse(pagintCtl.LabelCurrentPage.Text);
            //_tempCurrentPage -= 1;
            LoadGroup();;
        }

       



    }
}
