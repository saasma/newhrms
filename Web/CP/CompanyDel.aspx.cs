﻿using System;
using System.Collections.Generic;
using System.Linq;
using BLL;
using BLL.Manager;
using DAL;
using Utils.Helper;

namespace Web.CP
{
    public partial class CompanyDel : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                var userMgr = new UserManager();
                var pwd = (txtPwd.Text.Trim());
                var user = (UUser )null;
                var role = string.Empty;
                var roleId = 0;
                var isEmpRetired = false;
                var isEmpPortalDisabled = false;
                if (userMgr.ValidateUser(User.Identity.Name, pwd, ref role, ref roleId, ref user, false, ref isEmpRetired,ref isEmpPortalDisabled))
                {
                    if (SessionManager.CompanyDeleteId == 0)
                    {
                        Response.Redirect("~/CP/ManageCompany.aspx", true);
                        return;
                    }

                    var mgr = new CompanyManager();
                    var entity = new Company();
                    entity.CompanyId = SessionManager.CompanyDeleteId;
                    if (mgr.Delete(entity))
                    {
                        entity.CompanyId = 0;

                        JavascriptHelper.DisplayClientMsg(Resources.Messages.CompanyDeleteSuccessfull, Page, "window.location='ManageCompany.aspx';");
                    }
                    else
                    {
                        divWarningMsg.InnerHtml = Resources.Messages.CompanyDeleteUnsuccessfull;
                        divWarningMsg.Hide = false;
                    }
                }
                else
                {
                    divWarningMsg.InnerHtml = Resources.Messages.CompanyDeleteInvalidPassword;
                    divWarningMsg.Hide = false;
                }
            }
        }
    }
}
