<%@ Page Title="Manage departments" Language="C#" MasterPageFile="~/Master/ForPopupPage.Master"
    AutoEventWireup="true" CodeBehind="ManageDepAsPopup.aspx.cs" Inherits="Web.CP.ManageDepAsPopup" %>

<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">



        function closePopup() {
            window.opener.reloadDepartment(window);
        }

    </script>
    <style type="text/css">
        .clsColor
        {
            background-color: #e8f1ff;
            height: 500px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
    <ext:ResourceManager ID="ResourceManager1" DisableViewState="false" runat="server"
        ShowWarningOnAjaxFailure="false" ScriptMode="Release" Namespace="Rigo" />
    <asp:HiddenField ID="hdnDepartmentId" runat="server" />
    <div class="clsColor">
        <div class="popupHeader">
            <h3>
                Department Details</h3>
        </div>
        <uc2:MsgCtl Width='600px' ID="divMsgCtl" EnableViewState="false" Hide="true" runat="server" />
        <uc2:WarningCtl Width='600px' ID="divWarningMsg" EnableViewState="false" Hide="true"
            runat="server" />
        <div class="marginal" style="margin: 5px 20px 0 10px;">
            <table cellpadding="4px" style="margin-left: 10px;">
                <tr>
                    <td class="fieldHeader">
                        <My:Label ID="Label2" Text="Department Name" runat="server" ShowAstrick="true" />
                    </td>
                    <td>
                        <asp:TextBox ID="txtDepartmentName" runat="server" Width="180px" />
                        <asp:RequiredFieldValidator ID="valReqdName" runat="server" ControlToValidate="txtDepartmentName"
                            Display="None" ErrorMessage="Department name is required." ValidationGroup="AEDep"></asp:RequiredFieldValidator>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td class="fieldHeader">
                        <My:Label ID="Label1" Text="Department Code" runat="server" ShowAstrick="false" />
                    </td>
                    <td>
                        <asp:TextBox ID="txtCode" runat="server" Width="180px" />
                    </td>
                </tr>
                <tr style="display: none">
                    <td class="fieldHeader">
                        Department Head
                    </td>
                    <td>
                        <ext:Store ID="storeDepartment" runat="server">
                            <Model>
                                <ext:Model>
                                    <Fields>
                                        <ext:ModelField Name="Name" />
                                        <ext:ModelField Name="EmployeeId" />
                                    </Fields>
                                </ext:Model>
                            </Model>
                        </ext:Store>
                        <ext:ComboBox ID="cmbEmployee" Width="180px" ForceSelection="false" runat="server"
                            StoreID="storeDepartment" Editable="true" DisplayField="Name" ValueField="EmployeeId"
                            Mode="Local" EmptyText="">
                            <Items>
                                <ext:ListItem Value="-1" Text="&nbsp;" />
                            </Items>
                            <Listeners>
                                <Focus Handler="if( this.getValue()=='-1' ) this.clearValue();" />
                                <Select Handler="if( this.getValue()=='-1') this.clearValue(); " />
                            </Listeners>
                        </ext:ComboBox>
                        <%-- <asp:DropDownList ID="ddlDepHead" DataTextField="Name" Width="183px" DataValueField="EmployeeId"
                                runat="server">
                                <asp:ListItem Text="--Select head employee--" Value="-1"></asp:ListItem>
                            </asp:DropDownList>--%>
                    </td>
                </tr>
                <tr>
                    <td class="fieldHeader">
                        Description
                    </td>
                    <td>
                        <asp:TextBox ID="txtDesc" runat="server" Width="180px" TextMode="MultiLine" Columns="30"
                            Rows="6"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="fieldHeader">
                        Location
                    </td>
                    <td>
                        <asp:DropDownList ID="cmbBranch" runat="server" AppendDataBoundItems="true" DataTextField="Name" DataValueField="BranchId"
                            Width="180px">
                            <asp:ListItem Text="--Select Branch--" Value="-1" />
                        </asp:DropDownList>
                       
                    </td>
                </tr>
                <tr>
                    <td class="fieldHeader">
                        Phone
                    </td>
                    <td>
                        <asp:TextBox ID="txtPhone" runat="server" Width="180px"></asp:TextBox>
                       <%-- <asp:RegularExpressionValidator ID="valRegPhone" runat="server" ControlToValidate="txtPhone"
                            ErrorMessage="Phone number is invalid." ValidationGroup="AEDep" Display="None"></asp:RegularExpressionValidator>--%>
                    </td>
                </tr>
                <tr id="Tr1" runat="server" visible="false">
                    <td class="fieldHeader">
                        Fax
                    </td>
                    <td>
                        <asp:TextBox ID="txtFax" runat="server" Width="180px"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="valRegFax" runat="server" ControlToValidate="txtFax"
                            ErrorMessage="Fax is invalid." ValidationGroup="AEDep" Display="None"></asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr>
                    <td class="fieldHeader">
                        Email
                    </td>
                    <td>
                        <asp:TextBox ID="txtEmail" runat="server" Width="180px"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                            ControlToValidate="txtEmail" ErrorMessage="Email is invalid." ValidationGroup="AEDep"
                            Display="None"></asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td>
                        <asp:Button ID="btnSave" CssClass="update" OnClientClick="valGroup='AEDep';return CheckValidation()"
                            runat="server" Text="Save" ValidationGroup="AEDep" OnClick="btnSave_Click" />
                        <asp:Button ID="btnCancel" CssClass="cancel" OnClientClick="window.close();" runat="server"
                            Text="Cancel" />
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
