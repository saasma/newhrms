﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using Utils.Helper;
using Utils.Web;
using BLL.Base;
using DAL;
using Utils.Base;

namespace Web
{
    public partial class FixedPFDeduction : BasePage
    {

        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.AdministratorTools;
            }
        }     

        private int _tempCurrentPage;
        private int _tempCount;
        private PayManager payMgr = new PayManager();
        protected void Page_Load(object sender, EventArgs e)
        {
            //RegisterEmployeeSavedMsg();
            if (!IsPostBack)
            {
                PayrollPeriod period = CommonManager.GetLastPayrollPeriod();
                if (period != null)
                {
                    citList.InnerHtml += period.Name;
                }

                pagingCtl.CurrentPage = 1;
                BindEmployees();

            }
            if (Request.Form["__EVENTTARGET"] != null && Request.Form["__EVENTTARGET"].ToLower().Contains("refresh"))
                BindEmployees();
            JavascriptHelper.AttachNonDialogPopUpCode(Page, "taxImportPopup", "../ExcelWindow/FixedPFExcel.aspx", 450, 500);

         
        
        }
        //void RegisterEmployeeSavedMsg()
        //{
        //    if (SessionManager.EmployeeSaved)
        //    {
        //        JavascriptHelper.DisplayClientMsgWithTimeout("Employee Information Updated.", Page);
        //        SessionManager.EmployeeSaved = false;
        //    }
        //}

        void BindEmployees()
        {
            int totalRecords = 0;
            gvEmployeeIncome.DataSource
                = TaxManager.GetFixedPFList
                (txtEmployeeName.Text.Trim(), pagingCtl.CurrentPage - 1, int.Parse(pagingCtl.DDLRecords.SelectedValue), ref totalRecords);
            gvEmployeeIncome.DataBind();
            pagingCtl.UpdatePagingBar(totalRecords);

            btnUpdate.Visible = true;
            btnExport.Visible = btnUpdate.Visible;
        }

        public bool IsTextBoxEnabled(object value)
        {
            return value.ToString().ToLower() != "optimum";
        }

        public string GetCITCurrency(object amount)
        {
            return BaseHelper.GetCurrency(amount, 0);
        }
        protected void btnPrevious_Click()
        {
            pagingCtl.CurrentPage -= 1;
            //_tempCurrentPage = int.Parse(pagintCtl.LabelCurrentPage.Text);
            //_tempCurrentPage -= 1;
            BindEmployees();
        }

        protected void btnNext_Click()
        {
            pagingCtl.CurrentPage += 1;
            //_tempCurrentPage = int.Parse(pagintCtl.LabelCurrentPage.Text);
            //_tempCurrentPage += 1;
            BindEmployees();
        }

        protected void ddlRecords_SelectedIndexChanged()
        {
            //SessionManager.EmployeeDisplayRecodsPerPage = int.Parse(pagintCtl.DDLRecords.SelectedValue);
            //_tempCurrentPage = 1;
            pagingCtl.CurrentPage = 1;
            BindEmployees();
        }
        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            Page.Validate("Balance");
            bool isSavedReqd = false;

           

            //if (Page.IsValid)
            {
                StringBuilder str = new StringBuilder("<root>");
                   int companyId = SessionManager.CurrentCompanyId;

                bool isFixed = false; decimal amount;

                foreach (GridViewRow row in gvEmployeeIncome.Rows)
                {
                    int employeeId = (int)gvEmployeeIncome.DataKeys[row.RowIndex][0];

                    isFixed = false;
                    amount = 0;

                    TextBox txt = row.FindControl("txtAmount") as TextBox;
                    DropDownList ddlType = row.FindControl("ddlType") as DropDownList;

                    if (ddlType.SelectedIndex == 1)
                        isFixed = true;

                    amount = Convert.ToDecimal(txt.Text.Trim());

                    str.Append(
                        string.Format("<row EmployeeId=\"{0}\" IsPFDeductionFixed=\"{1}\" FixedPFDeductionAmount=\"{2}\" /> ",
                             employeeId, isFixed.ToString(),amount)
                        );

                }
                str.Append("</root>");
                //if (isSavedReqd)
                {
                    TaxManager.SaveFixedPFDeduction(str.ToString());

                    divMsgCtl.InnerHtml = "Information saved.";
                    divMsgCtl.Hide = false;

                    //JavascriptHelper.DisplayClientMsg("Information saved.", Page);
                }
            }
        }

        protected void txtEmployeeName_TextChanged(object sender, EventArgs e)
        {
            _tempCurrentPage = 1;
            BindEmployees();

        }
    }
}
