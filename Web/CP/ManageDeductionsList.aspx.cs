﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using BLL.BO;
using System.IO;
using System.Text;
using Utils.Helper;

namespace Web.CP
{
    public partial class ManageDeductionsList : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                BindDeductionGrid();
            }

            JavascriptHelper.AttachPopUpCode(Page, "popupDeduction", "AEDeduction.aspx", 600, 600);

        }

        private void BindDeductionGrid()
        {
            if (BLL.BaseBiz.PayrollDataContext.VoucherGroups.Any() == false)
                voucherAlsoChange.Visible = false;

            //load deductions
            List<PDeduction> list = PayManager.GetDeductionListByCompany(SessionManager.CurrentCompanyId);

            foreach (var item in list)
            {
                item.CalculationType = new DAL.DeductionCalculation().Get(item.Calculation);
                item.DeductionDeletable = PayManager.IsDeductionDeletable(item.DeductionId);
                item.IsEnabledValue = (item.IsEnabled == true ? "Yes" : "");

                if (item.Calculation == DeductionCalculation.Advance || item.Calculation == DeductionCalculation.LOAN)
                    item.CanBeEnabledDisabled = false;
                else
                    item.CanBeEnabledDisabled = true;
            }

            string searchDeduction = txtDeductionSearch.Text.Trim();
            storeDeductions.DataSource = list.Where(x => (searchDeduction == "" || x.Title.ToLower().Contains(searchDeduction.ToLower()))).OrderBy(x => x.Order).ToList();
            storeDeductions.DataBind();
        }

        protected void DeductionData_Refresh(object sender, StoreReadDataEventArgs e)
        {
            BindDeductionGrid();
        }

        protected void btnChangeOrder_Click(object sender, DirectEventArgs e)
        {
            int sourceId = int.Parse(hdnSource.Text.Trim());
            int destId = int.Parse(hdnDest.Text.Trim());
            string mode = hdnDropMode.Text.Trim();

            PayManager.ChangeDeductionOrder(sourceId, destId, mode);

            BindDeductionGrid();
        }

        protected void btnDelete_Click(object sender, DirectEventArgs e)
        {
            int deductionId = int.Parse(hdnDeductionId.Text);
            PayManager.DeleteDeduction(deductionId);
            NewMessage.ShowNormalMessage("Deduction deleted successfully.");
            BindDeductionGrid();
        }

        protected void btnEnableDisable_Click(object sender, DirectEventArgs e)
        {
            int deductionId = int.Parse(hdnDeductionId.Text);
            Status status = PayManager.EnableDisablePDeduction(deductionId);
            if (status.IsSuccess)
            {
                NewMessage.ShowNormalMessage(string.Format("Deduction {0} successfully.", status.ErrorMessage));
                BindDeductionGrid();
            }
            else
                NewMessage.ShowWarningMessage(status.ErrorMessage);
        }

        protected void btnLoad_Click(object sender, DirectEventArgs e)
        {
            BindDeductionGrid();
        }


    }
}