﻿<%@ Page Title="Past Salary Calculation" EnableEventValidation="false" MaintainScrollPositionOnPostBack="true" Language="C#" MasterPageFile="~/Master/Details.Master" AutoEventWireup="true" CodeBehind="PastCalculation.aspx.cs" Inherits="Web.CP.PastCalculation" %>
<%@ Register src="../UserControls/PastCalculationAdjCtl.ascx" tagname="Calculation" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

  
  
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
    <uc1:Calculation Id="Calculation1" runat="server" />
</asp:Content>


<asp:Content ID="Content3" ContentPlaceHolderID="pageScriptsContent" runat="server">
  <script src="../Scripts/currency.js" type="text/javascript"></script>

    <script src="../Scripts/ScrollTableHeader-v1.03.js" type="text/javascript"></script>
</asp:Content>