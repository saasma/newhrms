﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Base;
using BLL.Manager;
using DAL;
using Utils;
using Utils.Helper;
using Utils.Web;
using Web.Controls;

namespace Web.CP
{
    public partial class AddEditCompany : BasePage
    {
        CompanyManager compMgr = new CompanyManager();
        CommonManager commonMgr = new CommonManager();

        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.AdministratorTools;
            }
        }

        public int CompanyId
        {
            get
            {
                if (ViewState["CompanyId"] != null)
                    return int.Parse(ViewState["CompanyId"].ToString());
                return 0;
            }
            set
            {
                ViewState["CompanyId"] = value;
            }
        }

        public void btnAddOtherFund_Click(object sender, EventArgs e)
        {
            tblOtherFund.Visible = true;
        }
        bool ValidateForLicense()
        {
            if (LicenseValidator.isLicenseEnabled)
            {
                if (compMgr.GetTotalCompanies() >= LicenseValidator.MaxCompany)
                {
                    JavascriptHelper.DisplayClientMsg("Licensing allow only " + LicenseValidator.MaxCompany + " companies to be created.", Page,
                        "window.location='ManageCompany.aspx';");

                    return false;
                }
            }
            return true;
        }

        protected void Page_Load(object sender, EventArgs e)
        {

            if (ValidateForLicense() == false)
                return;

            LoadIRO();
            if (!IsPostBack)
            {
                Initialise();
            }


            string code =
                string.Format("var endingDateGreaterThanStarting = '{0}';var yearCannotBelongerThan12 = '{1}';"
                ,Resources.Messages.CompanyCreateFinancialEndingDateGreaterThanStarting,Resources.Messages.CompanyCreateFinancialYearLongerThan12Months);
            RegisterJSCode(code);

            // programatic client-side calls to __doPostBack() can bypass this
            //Page.ClientScript.RegisterOnSubmitStatement(GetType(), "ServerForm",
            //                                        "if (this.submitted) return false; this.submitted = true; return true;");
               
        }

        /// <summary>
        /// Make some fields disable due to in between save
        /// </summary>
        void MakeSomeFieldDisableDueToInitialSave()
        {
            if (IsInitialSaveExists())
            {
                //chkHasPFRFFund.Enabled = false;
                //DisableChildControls(this.ucFunds.ParentControl);
            }
        }

        void Initialise()
        {
            calStartingDate.IsSkipDay = true;
            calEndingDate.IsSkipDay = true;
            txtCompanyName.Focus();
            valRegFax.ValidationExpression = Config.FaxRegExp;
            valRegPhone.ValidationExpression = Config.PhoneRegExp;
            valRegExPanNo.ValidationExpression = Config.PanNoRegExp;


            //chkOtherRFFund.Checked

            
           

            BizHelper.Load(new CompanyNetSalaryRoundOff(), ddlNetSalaryRoundOff);
            LoadType();
            //LoadIRO();

            int id = UrlHelper.GetIdFromQueryString("Id");
            bool isEnglish = true;
            if (id != 0)
                isEnglish = SessionManager.CurrentCompany.IsEnglishDate;

            ddlLeaveStartMonth.DataSource = DateManager.GetCurrentMonthListForCompany(isEnglish);
            ddlLeaveStartMonth.DataBind();

            LoadCompany();

            
            
        }

        void LoadIRO()
        {
            ddlIRO.DataSource = commonMgr.GetAllIRO();
            ddlIRO.DataBind();


            if (ddlIRO.SelectedValue != "-1")
            {
                string id = Request.Form[ddlTSO.UniqueID];    
                ddlTSO.DataSource = CommonManager.GetTSOByIROId(int.Parse(ddlIRO.SelectedValue));
                ddlTSO.DataBind();

                if (id != null)
                    UIHelper.SetSelectedInDropDown(ddlTSO, id);   
            }

            //ddlTSO.data
        }
       
        void LoadType()
        {
            ddlCompanyType.Items.AddRange(BizHelper.GetMembersAsListItem(new CompanyType()));
            ddlCompanyType.SelectedIndex = 1;
        }

        void LoadCompany()
        {
            int cid = UrlHelper.GetIdFromQueryString("Id");
            if (cid != 0)
            {

                DisableChildControls(divSalarySetting);
                // show setting value
                if (CommonManager.CompanySetting.RemoteAreaUsingProportionateDays)
                    ddlRemoteArea.SelectedIndex = 1;
                else
                    ddlRemoteArea.SelectedIndex = 2;


                // fixed days
                if (CommonManager.CompanySetting.MonthlySalaryBasedOnFixedDay != 0)
                {
                    ddlSalaryWorkdaySetting.SelectedIndex = 3;
                    txtFixedWorkdayValue.Text = CommonManager.CompanySetting.MonthlySalaryBasedOnFixedDay.ToString();
                    rowWorkday.Style["display"] = "block";
                }
                else if (CommonManager.CompanySetting.MonthlySalaryBasedOnWeeklyHolidayDeduction)
                {
                    ddlSalaryWorkdaySetting.SelectedIndex = 2;
                }
                else
                    ddlSalaryWorkdaySetting.SelectedIndex = 1;


                // sst recovery 
                if (CommonManager.CompanySetting.DoNotUseSSTRecoveryFirst)
                    ddlSSTRecovery.SelectedIndex = 1;
                else
                    ddlSSTRecovery.SelectedIndex = 2;

                // level wise salary
                if (CommonManager.CompanySetting.HasLevelGradeSalary)
                    ddlEnableLevelwiseSalary.SelectedIndex = 1;
                else
                    ddlEnableLevelwiseSalary.SelectedIndex = 2;


                if (CommonManager.CompanySetting.LeaveDeductPrevMonthUnpaidChange)
                    ddlPreviousMonthUPLInCurrentSalary.SelectedIndex = 1;
                else
                    ddlPreviousMonthUPLInCurrentSalary.SelectedIndex = 2;


                DAL.Company company = null;
                this.CompanyId = cid;
                company = compMgr.GetById(this.CompanyId);
                if (company == null || company.Status == false)
                {
                    Response.Redirect("~/CP/ManageCompany.aspx");
                }
                else
                {
                    ProcessCompany(company);


                    if (company.PFRFFunds.Count > 0 && company.HasPFRFFund)
                    {
                        ucFunds.ProcessPF(company.PFRFFunds[0],company.IsEnglishDate);
                    }



                    btnSave.Text = Resources.Messages.Update;
                    DisableDatesSelection();
                }
               
            }
            else
            {
                //calStartingDate.IsEnglishCalendar = true;
                //calEndingDate.IsEnglishCalendar = true;

                calStartingDate.SelectTodayDate();
                calEndingDate.SelectTodayDate();
            }
        }


        void DisableDatesSelection()
        {
            rdbListDateSystem.Enabled = false;
            calStartingDate.Enabled = false;
            calEndingDate.Enabled = false;
         
        }

        DAL.Company ProcessCompany(DAL.Company c)
        {
            int? EditSequence = 0;
            if (c != null)
            {
                hdEditSequence.Value = c.EditSequence == null ? "0" : c.EditSequence.ToString();
                txtCompanyName.Text = c.Name;
                //Address
                txtResponsiblePerson.Text = c.ResponsiblePerson;
                txtAddress.Text = c.Address;
                txtPhone.Text = c.PhoneNo;
                txtFax.Text = c.FaxNo;
                txtEmail.Text = c.Email;
                txtWebsite.Text = c.Website;
                txtRegNo.Text = c.RegistrationNo;
                UIHelper.SetSelectedInDropDown(ddlNetSalaryRoundOff, c.NetSalaryRoundOff);
                UIHelper.SetSelectedInDropDown(ddlCompanyType, c.CompanyType);
                UIHelper.SetSelectedInDropDown(ddlLeaveStartMonth,c.LeaveStartMonth);
                //ddlLeaveStartMonth.Enabled = false;
                chkHasPFRFFund.Checked = c.HasPFRFFund;

                txtPanNo.Text = c.PANNo;
                UIHelper.SetSelectedInDropDown(ddlIRO, c.IROId);
                if (ddlIRO.SelectedValue != "-1")
                {
                    ddlTSO.DataSource = CommonManager.GetTSOByIROId(int.Parse(ddlIRO.SelectedValue));
                    ddlTSO.DataBind();
                }
                if (c.TSOId != null)
                {
                    UIHelper.SetSelectedInDropDown(ddlTSO, c.TSOId);
                }

                txtPFRFName.Text = c.PFRFName;
                txtPFRFAbbr.Text = c.PFRFAbbreviation;
                if (c.PFRFFunds.Count > 0)
                    txtCITReportDesc.Text = c.PFRFFunds[0].CITReportDescription;
                //Settings
                if (c.IsEnglishDate)
                    rdbListDateSystem.Items[0].Selected = true;
                else
                    rdbListDateSystem.Items[1].Selected = true;

                if (c.CITCompanyCode != null)
                    txtCITCompanyCode.Text = c.CITCompanyCode;
                if (c.CITBankName != null)
                    txtCITBankName.Text = c.CITBankName;

                if (c.OtherFundFullName != null)
                    txtOtherFundName.Text = c.OtherFundFullName;
                if (c.OtherFundAbbreviation != null)
                    txtOtherFundAbbr.Text = c.OtherFundAbbreviation;
                if (c.OtherFundCode != null)
                    txtOtherFundCode.Text = c.OtherFundCode;
               
                if (c.EmployeeSelfPaidOtherFund != null)
                    chkDepositedByEmployee.Checked = c.EmployeeSelfPaidOtherFund.Value;
                else
                    chkDepositedByEmployee.Checked = false;

                if (c.HasOtherFundLikeCIT != null && c.HasOtherFundLikeCIT.Value)
                {
                    tblOtherFund.Visible = true;
                }

                if (c.PFRFFunds.Count >= 1)
                {
                    if (c.PFRFFunds[0].HasOtherPFFund != null && c.PFRFFunds[0].HasOtherPFFund.Value)
                        chkOtherRFFund.Checked = true;
                    else
                        chkOtherRFFund.Checked = false;

                    txtOtherPFRFAbbr.Text = c.PFRFFunds[0].OtherPFFundName;
                }

                if (EmployeeManager.HasOtherFundEmployee())
                {
                    btnAddOtherFund.Enabled = false;
                    //tblOtherFund.Visible = true;
                }
            }
            else
            {
                c = new DAL.Company();
                EditSequence = int.Parse(hdEditSequence.Value == "" ? "0" : hdEditSequence.Value);
                c.EditSequence = EditSequence + 1;
                c.Name = txtCompanyName.Text.Trim();
                //Address
                c.ResponsiblePerson = txtResponsiblePerson.Text.Trim();
                c.Address = txtAddress.Text.Trim();
                c.PhoneNo = txtPhone.Text.Trim();
                c.FaxNo = txtFax.Text.Trim();
                c.Email = txtEmail.Text.Trim();
                c.Website = txtWebsite.Text.Trim();
                c.RegistrationNo = txtRegNo.Text.Trim();
                c.NetSalaryRoundOff = ddlNetSalaryRoundOff.SelectedValue;
                c.CompanyType = ddlCompanyType.SelectedValue;
                c.PANNo = txtPanNo.Text.Trim();
                c.IROId = int.Parse(ddlIRO.SelectedValue);
               
                if (ddlTSO.SelectedValue != "")
                    c.TSOId = int.Parse(ddlTSO.SelectedValue);
                else
                    c.TSOId = null;

                c.LeaveStartMonth = int.Parse(ddlLeaveStartMonth.SelectedValue);

                c.IsEnglishDate = rdbListDateSystem.Items[0].Selected;

                FinancialDate finanDate = new FinancialDate();

                finanDate.StartingDate = calStartingDate.SelectedDateAsFirstDay.ToString();
                finanDate.EndingDate = calEndingDate.SelectedDateAsLastDay.ToString();
                finanDate.IsCurrent = true;

                c.FinancialDates.Add(finanDate);

                c.HasPFRFFund = chkHasPFRFFund.Checked;
                if (c.HasPFRFFund)
                {
                    c.PFRFName = txtPFRFName.Text.Trim();
                    c.PFRFAbbreviation = txtPFRFAbbr.Text.Trim();
                
                }
                //c.HasEmployeeInsurance = chkHasEmpInsurance.Checked;
                //c.HasCIT = chkHasCIT.Checked;

                c.CITBankName = txtCITBankName.Text.Trim();
                c.CITCompanyCode = txtCITCompanyCode.Text.Trim();
                c.OtherFundFullName = txtOtherFundName.Text.Trim();
                c.OtherFundAbbreviation = txtOtherFundAbbr.Text.Trim();
                c.OtherFundCode = txtOtherFundCode.Text.Trim();
                c.EmployeeSelfPaidOtherFund = chkDepositedByEmployee.Checked;
                if (c.OtherFundFullName != "")
                    c.HasOtherFundLikeCIT = true;


                if (chkOtherRFFund.Checked)
                {
                    c.HasOtherPFFund = true;
                    c.OtherPFFundName = txtOtherPFRFAbbr.Text.Trim();
                }

                return c;
            }
            return null;
        }

        
        protected void btnSave_Click(object sender, EventArgs e)
        {
            int? EditSequence = 0;
            if (this.CompanyId == 0)
            {
                if (DateManager.IsSecondDateGreaterThan(calStartingDate.SelectedDate, calEndingDate.SelectedDate) == false)
                {
                    divWarningMsg.InnerHtml = Resources.Messages.CompanyFinancialYearEndingDateGreaterThanStartingDateMsg;
                    divWarningMsg.Hide = false;
                    //JavascriptHelper.DisplayClientMsg("Financial ending date must be greater than the starting date.", Page);
                    return;
                }

                if (ValidateForLicense() == false)
                    return;

                if(SalaryWorkday.FixedDays == (SalaryWorkday)int.Parse(ddlSalaryWorkdaySetting.SelectedValue))
                {
                    if(string.IsNullOrEmpty(txtFixedWorkdayValue.Text.Trim()))
                    {
                        divWarningMsg.InnerHtml = "Fixed workday value is required.";
                        divWarningMsg.Hide = false;
                    }
                    double val = 0;
                    if(double.TryParse(txtFixedWorkdayValue.Text.Trim(),out val) == false)
                    {
                        divWarningMsg.InnerHtml = "Invalid numeric value in fixed workday.";
                        divWarningMsg.Hide = false;
                    }
                }

            }

            if (chkOtherRFFund.Checked)
            {
                if (string.IsNullOrEmpty(txtOtherPFRFAbbr.Text.Trim()))
                {
                    divWarningMsg.InnerHtml = "Other fund name & abbreviation must be entered.";
                    divWarningMsg.Hide = false;
                    return;
                }
            }



            if (txtOtherFundName.Visible == true)
            {
                if (txtOtherFundName.Text.Trim() == "" || txtOtherFundAbbr.Text.Trim() == "")
                {
                    divWarningMsg.InnerHtml = "Other PF Fund Name must be entered.";
                    divWarningMsg.Hide = false;
                    return;
                }
               // return;
            }

            //disable validation controls for hidden controls
            if (chkHasPFRFFund.Checked == false)
                ucFunds.DisableValControls("PFRF");
            //if (chkHasCIT.Checked == false)
            //    ucFunds.DisableValControls("CIT");
            //if (chkHasEmpInsurance.Checked == false)
            //    ucFunds.DisableValControls("EI");

            Page.Validate("AECompany");

            if (Page.IsValid)
            {

                //clear cache mainly for Company Info in Report
                MyCache.Reset();

                DAL.Company c = ProcessCompany(null);



                //if (chkHasCIT.Checked)
                //    c.CitizenInventmentTrusts.Add(ucFunds.ProcessCIT(null));
                //if (chkHasEmpInsurance.Checked)
                //    c.EmployeeInsurances.Add(ucFunds.ProcessEI(null));
                if (chkHasPFRFFund.Checked)
                {
                    c.PFRFFunds.Add(ucFunds.ProcessPF(null, c.IsEnglishDate));
                    c.PFRFFunds[0].CITReportDescription = txtCITReportDesc.Text.Trim();

                    if (chkOtherRFFund.Checked)
                    {
                        c.PFRFFunds[0].HasOtherPFFund = c.HasOtherPFFund;
                        c.PFRFFunds[0].OtherPFFundName = c.OtherPFFundName;
                    }
                }
                if (this.CompanyId != 0)
                {
                    c.CompanyId = this.CompanyId;
                   
                    EditSequence = compMgr.GetById(c.CompanyId).EditSequence;
                    EditSequence = EditSequence == null ? 0 : EditSequence;

                    if (EditSequence != int.Parse(hdEditSequence.Value == "" ? "0" : hdEditSequence.Value))
                    {
                        divWarningMsg.InnerHtml = Resources.Messages.ConcurrencyError;
                        divWarningMsg.Hide = false;
                        return;
                    }
                    compMgr.Update(c);
                    divMsgCtl.InnerHtml = Resources.Messages.CompanyUpdatedMsg;
                    divMsgCtl.Hide = false;
                    hdEditSequence.Value = (int.Parse(hdEditSequence.Value) + 1).ToString();
                    //JavascriptHelper.DisplayClientMsg("Company information updated.", this);
                }
                else
                {
                    MyCache.Reset();

                    //if no company then make it default
                    bool defaultCompanyCreated = false;
                    if (SessionManager.CurrentCompanyId == 0)
                    {
                        defaultCompanyCreated = true;
                        c.IsDefault = true;
                    }
                    
                    if (compMgr.Save(c))
                    {

                        CommonManager.UpdateCompanySettingInCompanyCreation((SalaryWorkday)int.Parse(ddlSalaryWorkdaySetting.SelectedValue),
                            txtFixedWorkdayValue.Text.Trim(),
                            bool.Parse(ddlRemoteArea.SelectedValue),
                            bool.Parse(ddlSSTRecovery.SelectedValue),
                            bool.Parse(ddlEnableLevelwiseSalary.SelectedValue),
                            bool.Parse(ddlPreviousMonthUPLInCurrentSalary.SelectedValue));

                        this.CompanyId = c.CompanyId;
                        //JavascriptHelper.DisplayClientMsg("Company information saved.", this);
                        divMsgCtl.InnerHtml = Resources.Messages.CompanySavedmsg;
                        divMsgCtl.Hide = false;
                        DisableDatesSelection();
                        btnSave.Text = Resources.Messages.Update;

                        //call to reload menu after creating default company
                        if (defaultCompanyCreated)
                        {
                            //(Master.FindControl("HeaderCtl1") as HeaderCtl).ReloadMenu();
                            //btnCancel.Visible = true;
                            Response.Redirect("ManageCompany.aspx");

                        }
                    }
                }
                MyCache.Reset();
            }
            else
            {
                JavascriptHelper.DisplayClientMsg("Validation fail.", this);
            }

            ucFunds.EnableValControls("PFRF");
            ucFunds.EnableValControls("CIT");
            ucFunds.EnableValControls("EI");
        }

        protected void rdbListDateSystem_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rdbListDateSystem.Items[0].Selected)
            {
                calStartingDate.IsEnglishCalendar = rdbListDateSystem.Items[0].Selected;
                calStartingDate.SelectTodayDate();

                calEndingDate.IsEnglishCalendar = calStartingDate.IsEnglishCalendar;
                calEndingDate.SelectTodayDate();
                if (SessionManager.CurrentCompanyId != 0)
                    SessionManager.CurrentCompany.IsEnglishDate = true;
               
                ListItem firstItem = ddlLeaveStartMonth.Items[0];
                ddlLeaveStartMonth.Items.Clear();
                ddlLeaveStartMonth.DataSource = DateManager.GetCurrentMonthListForCompany(true);
                ddlLeaveStartMonth.DataBind();
                ddlLeaveStartMonth.Items.Insert(0, firstItem);
            }

            else if(rdbListDateSystem.Items[1].Selected)
            {
                calStartingDate.IsEnglishCalendar = rdbListDateSystem.Items[0].Selected;
                calStartingDate.SelectTodayDate();
                calEndingDate.IsEnglishCalendar = calStartingDate.IsEnglishCalendar;
                calEndingDate.SelectTodayDate();

                if (SessionManager.CurrentCompanyId != 0)
                    SessionManager.CurrentCompany.IsEnglishDate = false;

                ListItem firstItem = ddlLeaveStartMonth.Items[0];
                ddlLeaveStartMonth.Items.Clear();
                ddlLeaveStartMonth.DataSource = DateManager.GetCurrentMonthListForCompany(false);
                ddlLeaveStartMonth.DataBind();
                ddlLeaveStartMonth.Items.Insert(0, firstItem);
               
            }


            ucFunds.ChangeDateType(calStartingDate.IsEnglishCalendar);
        }

        protected void valCustomRFAC_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (chkHasPFRFFund.Checked)
            {
                if (txtPFRFName.Text.Trim() == string.Empty)
                    args.IsValid = false;
                else

                    args.IsValid = true;
            }
        }

        protected void CustomValidator1_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (chkHasPFRFFund.Checked)
            {
                if (txtPFRFAbbr.Text.Trim() == string.Empty)
                    args.IsValid = false;
                else

                    args.IsValid = true;
            }

        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (chkHasPFRFFund.Checked)
            {
                txtPFRFAbbr.Enabled = true;
                txtPFRFName.Enabled = true;
            }
            else
            {
                txtPFRFAbbr.Enabled = false;
                txtPFRFName.Enabled = false;
            }

            ucFunds.EnableDisableControls(chkHasPFRFFund.Checked);//,false,false);

            int cid = UrlHelper.GetIdFromQueryString("Id");
            if (cid != 0 && CompanyManager.IsPFUsedInSalary(cid))
            {
                chkHasPFRFFund.Enabled = false;
                ucFunds.DisablePFSectionAfterPFGenerationIsSavedInSalary();
            }


            if (this.CompanyId != 0)
            {
                DAL.Company c = compMgr.GetById(this.CompanyId);
                FinancialDate fDate = compMgr.GetCurrentFinancialDate(this.CompanyId);

                calStartingDate.SetSelectedDate(fDate.StartingDate, c.IsEnglishDate);
                calEndingDate.SetSelectedDate(fDate.EndingDate, c.IsEnglishDate);
            }

            //if no company exists
            if (SessionManager.CurrentCompanyId == 0)
            {
                divMsgCtl.InnerHtml = Resources.Messages.NoCompanyExistsCreateNewOne;
                divMsgCtl.Hide = false;

                btnCancel.Visible = false;
            }

            if (this.CompanyId != 0)
            {
                MakeSomeFieldDisableDueToInitialSave();
            }
        }
    }
}
