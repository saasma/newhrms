﻿using System;
using System.Collections.Generic;
using System.Linq;
using BLL.Manager;
using DAL;
using BLL;
using Utils.Web;
using Utils.Helper;

namespace Web.CP
{
    public partial class AETax : System.Web.UI.Page
    {
        private TaxManager taxMgr = new TaxManager();
        private int employeeId = 0;
        public int TaxId
        {
            get
            {
                if (ViewState["TaxId"] != null)
                {
                    return int.Parse(ViewState["TaxId"].ToString());
                }
                return 0;
            }
            set
            {
                ViewState["TaxId"] = value;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            employeeId = UrlHelper.GetIdFromQueryString("EId");
            if (!IsPostBack)
            {
                Initialise();
            }
        }

        private void Initialise()
        {
            BizHelper.Load(new PaidBy(), ddlPaidBy);

            var tax = taxMgr.GetPayTaxByCompany(SessionManager.CurrentCompanyId);
            if (tax != null)
            {
                this.TaxId = tax.TaxId;
                txtTitle.Text = tax.Title;
                txtAbbreviation.Text = tax.Abbreviation;
                UIHelper.SetSelectedInDropDown(ddlPaidBy, tax.PaidBy);
            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                var tax = new PTax();
                tax.CompanyId = SessionManager.CurrentCompanyId;
                tax.Title = txtTitle.Text.Trim();
                tax.Abbreviation = txtAbbreviation.Text.Trim();
                tax.PaidBy = ddlPaidBy.SelectedValue;

                var empTax = new PEmployeeTax();
                empTax.EmployeeId = employeeId;
                if (this.TaxId != 0)
                {
                    tax.TaxId = this.TaxId;
                    taxMgr.Update(tax, empTax);
                    JavascriptHelper.DisplayClientMsg("Tax saved.", Page, "closePopup();");
                }
                else
                {
                    taxMgr.Save(tax, empTax);
                    JavascriptHelper.DisplayClientMsg("Tax saved.", Page, "closePopup();");
                }
            }
        }
    }
}
