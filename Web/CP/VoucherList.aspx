﻿<%@ Page Title="Voucher Report" Language="C#" MasterPageFile="~/Master/HROld.Master"
    AutoEventWireup="true" CodeBehind="VoucherList.aspx.cs" Inherits="Web.CP.VoucherList" %>

<%@ Register Src="VoucherListCtl.ascx" TagName="VoucherListCtl" TagPrefix="uc1" %>
<%@ Register Src="VoucherListNewCtl.ascx" TagName="VoucherListNewCtl" TagPrefix="uc2" %>
<%@ Register Src="VoucherListCtlGlobus.ascx" TagName="VoucherListNewCtlGlobus" TagPrefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
    <script type="text/javascript">
        var skipLoadingCheck = true;
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
<ext:ResourceManager ID="ResourceManager1" runat="server" ScriptMode="Release" DisableViewState="false" />
    <uc2:VoucherListNewCtl ID="voucherCtlNew" visible="false" runat="server" />
    <uc1:VoucherListCtl ID="voucherCtlOld" runat="server" />
     <uc2:VoucherListNewCtlGlobus ID="VoucherListNewGlobus" visible="false" runat="server" />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
