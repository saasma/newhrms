<%@ Page Title="Overtime" Language="C#" MasterPageFile="~/Master/Details.Master"
    AutoEventWireup="true" CodeBehind="Overtime.aspx.cs" Inherits="Web.Overtime" %>

<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<%@ Import Namespace="BLL.Manager" %>
<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/PagingCtl.ascx" TagName="PagingCtl" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">

        function calculatePay(txt1, txtFirstId, txtSecondId, txtPay) {

            var txt2 = document.getElementById(txt1.id.replace(txtFirstId, txtSecondId));
            var result = document.getElementById(txt1.id.replace(txtFirstId, txtPay));


            result.value = getMoney(getNumber(txt1.value) * getNumber(txt2.value));

        }
    
    </script>
    <script src="../Scripts/currency.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
    <div class="contentArea">
        <div class="attribute">
            <asp:UpdatePanel ID="upd" runat="server" UpdateMode="Conditional">
                <Triggers>
                    <asp:PostBackTrigger ControlID="btnLoad" />
                </Triggers>
                <ContentTemplate>
                    <table>
                        <tr>
                            <td>
                                <strong>Payroll Period</strong>
                            </td>
                            <td>
                                <strong>Branch</strong>
                            </td>
                            <td>
                                <strong>Department</strong>
                            </td>
                            <td>
                                <strong>Shift</strong>
                            </td>
                            <td>
                                <strong>Search</strong>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:DropDownList ID="ddlPayrollPeriods" DataTextField="Name" DataValueField="PayrollPeriodId"
                                    AppendDataBoundItems="true" Width="150px" runat="server">
                                    <asp:ListItem Value="-1" Text=""></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td>
                                <asp:DropDownList AutoPostBack="true" OnSelectedIndexChanged="ddlDepartment_SelectedIndexChanged"
                                    ID="ddlBranch" DataTextField="Name" DataValueField="BranchId" AppendDataBoundItems="true"
                                    Width="150px" runat="server">
                                    <asp:ListItem Value="-1" Text=""></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlDepartment" DataTextField="Name" DataValueField="DepartmentId"
                                    runat="server" AppendDataBoundItems="true" Width="150px">
                                    <asp:ListItem Value="-1" Text=""></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlShift" DataTextField="Name" DataValueField="WorkShiftId"
                                    runat="server" AppendDataBoundItems="true" Width="150px">
                                    <asp:ListItem Value="-1" Text=""></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td>
                                <asp:TextBox ID="txtEmployeeName" AutoPostBack="true" runat="server" Width="146px"
                                    OnTextChanged="txtEmployeeName_TextChanged"></asp:TextBox>
                                <cc1:TextBoxWatermarkExtender ID="TBWE2" runat="server" TargetControlID="txtEmployeeName"
                                    WatermarkText="Enter employee name" WatermarkCssClass="watermarked" />
                            </td>
                            <td valign="top">
                                <asp:Button ID="btnLoad" CssClass="load" OnClick="btnLoad_Click" runat="server" Text="Load" />
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <uc2:MsgCtl ID="msgCtl" EnableViewState="false" Hide="true" runat="server" />
        <uc2:WarningCtl ID="divWarningMsg" EnableViewState="false" Hide="true" runat="server" />
        <div class="clear">
            <cc2:EmptyDisplayGridView CssClass="tableLightColor" UseAccessibleHeader="true" ShowHeaderWhenEmpty="True"
                ID="gvw" runat="server" DataKeyNames="EmployeeId,PayrollPeriodId,OverTimeId"
                AutoGenerateColumns="False" CellPadding="4" GridLines="None" AllowSorting="True"
                ShowFooterWhenEmpty="False">
                <RowStyle BackColor="#E3EAEB" />
                <Columns>
                    <asp:BoundField HeaderStyle-Width="20px" DataFormatString="{0:000}" DataField="EmployeeId"
                        HeaderText="EIN"></asp:BoundField>
                    <asp:TemplateField HeaderText="Name" HeaderStyle-Width="150px">
                        <ItemTemplate>
                            <%# Eval("Name") %>
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Left" Width="234px" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="Overtime Hours"
                        HeaderStyle-Width="100px">
                        <ItemTemplate>
                            <asp:TextBox Enabled='<%#((int) Eval("IsCalculationSaved")) == 0 %>' onblur='calculatePay(this,"txtOvertimeHours","txtPayPerHour","txtPay")'
                                CssClass='calculationInput' Style='text-align: right' ID="txtOvertimeHours" Text='<%# Eval("OvertimeHours") %>'
                                runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator SetFocusOnError="true" Display="None" ID="val1" ControlToValidate="txtOvertimeHours"
                                ValidationGroup="Balance" runat="server" ErrorMessage="Overtime hours is required." />
                            <asp:CompareValidator SetFocusOnError="true" ValueToCompare="0" Display="None" Operator="GreaterThanEqual"
                                Type="Double" ID="valOpeningBalance" ControlToValidate="txtOvertimeHours" ValidationGroup="Balance"
                                runat="server" ErrorMessage="Invalid overtime hours."></asp:CompareValidator>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="Pay / Hour" HeaderStyle-Width="100px">
                        <ItemTemplate>
                            <asp:TextBox Enabled='<%#((int) Eval("IsCalculationSaved")) == 0 %>' onblur='calculatePay(this,"txtPayPerHour","txtOvertimeHours","txtPay")'
                                CssClass='calculationInput' Style='text-align: right' ID="txtPayPerHour" Text='<%# GetCurrency( Eval("PayPerHour")) %>'
                                runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator SetFocusOnError="true" Display="None" ID="val2" ControlToValidate="txtPayPerHour"
                                ValidationGroup="Balance" runat="server" ErrorMessage="Pay per hour is required." />
                            <asp:CompareValidator SetFocusOnError="true" ValueToCompare="0" Display="None" Operator="GreaterThanEqual"
                                Type="Currency" ID="valOpeningBalance1" ControlToValidate="txtPayPerHour" ValidationGroup="Balance"
                                runat="server" ErrorMessage="Invalid amount."></asp:CompareValidator>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="Overtime Pay" HeaderStyle-Width="100px">
                        <ItemTemplate>
                            <asp:TextBox Style='text-align: right' Enabled='<%#((int) Eval("IsCalculationSaved")) == 0 %>'
                                CssClass='calculationInput disabled' Text='<%# GetCurrency( Eval("OvertimePay")) %>'
                                ReadOnly="true" ID="txtPay" runat="server"></asp:TextBox>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="Note" HeaderStyle-Width="230px">
                        <ItemTemplate>
                            <asp:TextBox Enabled='<%#((int) Eval("IsCalculationSaved")) == 0 %>' Style='width: 400px;
                                text-align: left;' CssClass='calculationInput' ID="txtNote" Text='<%#  Eval("Note") %>'
                                runat="server"></asp:TextBox>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Delete" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                        <HeaderTemplate>
                            <img alt="Delete" title="Delete" src="../images/delet.png" />
                            <br />
                            <asp:CheckBox ID="chkDelete" onclick="selectDeselectAll(this)" runat="server" />
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:CheckBox Enabled='<%#((int) Eval("IsCalculationSaved")) == 0 %>' ID="chkDelete"
                                Visible='<%# ((int) Eval("OvertimeId"))!=0 %>' runat="server" />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <RowStyle CssClass="odd" />
                <PagerSettings Mode="NumericFirstLast" NextPageText="Next �" PreviousPageText="� Prev"
                    FirstPageText="First" LastPageText="Last" />
                <AlternatingRowStyle CssClass="even" />
                <EmptyDataTemplate>
                    <b>No overtime found. </b>
                </EmptyDataTemplate>
            </cc2:EmptyDisplayGridView>
            <uc1:PagingCtl ID="pagingCtl" OnNextRecord="btnNext_Click" OnPrevRecord="btnPrevious_Click"
                OnDropDownShowRecordsChanged="ddlRecords_SelectedIndexChanged" runat="server" />
        </div>
        <div class="buttonsDiv">
            <%-- <asp:ValidationSummary ID="ValidationSummary1" ValidationGroup="Balance" DisplayMode="List"
                ShowSummary="false" ShowMessageBox="true" runat="server"></asp:ValidationSummary>--%>
            <asp:Button Style='text-align: left' ID="btnPostToSalary" CssClass="update" Visible="true"
                runat="server" Text="Post Salary" OnClick="btnPostToSalary_Click" />
            &nbsp;&nbsp;&nbsp;
            <asp:Button ID="btnUpdate" CssClass="update" Visible="true" OnClientClick="valGroup='Balance';return CheckValidation();"
                ValidationGroup="Balance" runat="server" Text="Apply" OnClick="btnUpdate_Click" />
            <asp:Button ID="btnCancel" CssClass="cancel" Visible="true" CausesValidation="false"
                runat="server" Text="Cancel" OnClick="btnCancel_Click" />
            <asp:Button ID="btnDelete" CssClass="delete" OnClick="btnDelete_Click" Visible="true"
                CausesValidation="false" runat="server" Text="Delete" />
        </div>
    </div>
</asp:Content>
