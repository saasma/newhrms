﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using Utils.Helper;
using DAL;
using BLL.Base;
using Utils.Web;
using Utils.Calendar;
using System.IO;
using Utils;

namespace Web.CP
{
    public partial class DesignationTransferDetls : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Initialise();
            }
        }

        private void Initialise()
        {
            hdnEmployeeId.Value = "";

            BindDesignations();

            ddlDesignation.SelectedIndex = 0;

            ddlEventType.DataSource = CommonManager.GetServieEventTypes();
            ddlEventType.DataBind();

            if (Request.QueryString["Id"] != null)
            {
                int employeeDesignationId = int.Parse(Request.QueryString["Id"].ToString());
                hdnEmployeeDesignationId.Value = Request.QueryString["Id"];
                LoadEmployeeFromQueryString(employeeDesignationId);
            }
        }

        private void BindDesignations()
        {

            List<EDesignation> list = NewHRManager.GetDesignations();
            EDesignation obj = new EDesignation() { DesignationId = 0, Name = "" };
            list.Insert(0, obj);

            ddlDesignation.DataSource = list;
            ddlDesignation.DataBind();
        }

        private void LoadEmployeeFromQueryString(int employeeDesignationId)
        {
            DesignationHistory obj = NewHRManager.GetDesignationHistoryById(employeeDesignationId);
            if (obj != null)
            {
                UIHelper.SetSelectedInDropDown(ddlDesignation, obj.DesignationId.ToString());
                //ddlDesignation.SelectedValue = obj.DesignationId.ToString();
                calFromDate.Text = obj.FromDate;

                if (obj.Note != null)
                    txtNote.Text = obj.Note;

                LoadEmployeeDetails(obj.EmployeeId.Value);

                if (obj.EventID != null && obj.EventID != -1)
                    UIHelper.SetSelectedInDropDown(ddlEventType, obj.EventID.ToString());

                hdnEmployeeId.Value = obj.EmployeeId.Value.ToString();

                btnSave.Text = Resources.Messages.Update;
            }
        }

        public void LoadEmployeeDetails(int employeeId)
        {
            EEmployee emp = EmployeeManager.GetEmployeeById(employeeId);

            if (Request.QueryString["Id"] != null)
            {
                txtEmpSearchText.Text = emp.Name;
                txtEmpSearchText.Enabled = false;
            }

            if (emp == null)
            {
                buttonsBar.Visible = false;
                divWarningMsg.InnerHtml = "Please select an employee.";
                divWarningMsg.Hide = false;
                return;
            }
            else
                buttonsBar.Visible = true;

            NewHRManager.SaveFirstDesignHistoryIfNotExists(employeeId);

            // Photo
            bool hasPhoto = false;
            if (emp.HHumanResources.Count > 0)
            {
                if (!string.IsNullOrEmpty(emp.HHumanResources[0].UrlPhotoThumbnail))
                {
                    image.ImageUrl = Page.ResolveUrl(Config.UploadLocation + "/" + emp.HHumanResources[0].UrlPhotoThumbnail);
                    if (File.Exists(Server.MapPath(image.ImageUrl)))
                        hasPhoto = true;
                }
            }
            if (!hasPhoto)
            {
                image.ImageUrl = Page.ResolveUrl(Web.Helper.WebHelper.GetEmployeeNoImage(employeeId));
            }

            lblName.Text = emp.Title + " " + emp.Name;
            lblBranch.Text = emp.Branch.Name;
            lblDepartment.Text = emp.Department.Name;

            int years, months, days, hours;
            int minutes, seconds, milliseconds;
            ServicePeroid firstStatus = EmployeeManager.GetServicePeroidDate(emp.EmployeeId);
            GetElapsedTime(firstStatus.FromDateEng, BLL.BaseBiz.GetCurrentDateAndTime(), out years, out months, out days, out hours, out minutes, out seconds, out milliseconds);

            lblSince.Text = "Since " +
                (IsEnglish ? firstStatus.FromDateEng.ToShortDateString() : firstStatus.FromDate + " (" + firstStatus.FromDateEng.ToShortDateString() + ")");
            lblTime.Text = string.Format("For {0} years {1} months {2} days",
                years, months, days);

            // Load emp specific Designation under the Level
            //if (NewHRManager.HasEmployeeLevelGrade(emp.EmployeeId) && emp.PEmployeeIncomes[0].LevelId != null)
            //{
            //    BLevel level = NewPayrollManager.GetLevelById(emp.PEmployeeIncomes[0].LevelId.Value);

            //    ddlDesignation.DataSource = NewHRManager.GetDesignationsByLevel(level.LevelId);
            //    ddlDesignation.DataBind();
            //}

        }

        // Return the number of years, months, days, hours, minutes, seconds,
        // and milliseconds you need to add to from_date to get to_date.
        private void GetElapsedTime(DateTime from_date, DateTime to_date,
            out int years, out int months, out int days, out int hours,
            out int minutes, out int seconds, out int milliseconds)
        {
            // If from_date > to_date, switch them around.
            if (from_date > to_date)
            {
                GetElapsedTime(to_date, from_date,
                    out years, out months, out days, out hours,
                    out minutes, out seconds, out milliseconds);
                years = -years;
                months = -months;
                days = -days;
                hours = -hours;
                minutes = -minutes;
                seconds = -seconds;
                milliseconds = -milliseconds;
            }
            else
            {
                // Handle the years.
                years = to_date.Year - from_date.Year;

                // See if we went too far.
                DateTime test_date = from_date.AddMonths(12 * years);
                if (test_date > to_date)
                {
                    years--;
                    test_date = from_date.AddMonths(12 * years);
                }

                // Add months until we go too far.
                months = 0;
                while (test_date <= to_date)
                {
                    months++;
                    test_date = from_date.AddMonths(12 * years + months);
                }
                months--;

                // Subtract to see how many more days,
                // hours, minutes, etc. we need.
                from_date = from_date.AddMonths(12 * years + months);
                TimeSpan remainder = to_date - from_date;
                days = remainder.Days;
                hours = remainder.Hours;
                minutes = remainder.Minutes;
                seconds = remainder.Seconds;
                milliseconds = remainder.Milliseconds;
            }
        }

        protected void btnLoadEmpDetls_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(hdnEmployeeId.Value))
                return;
            int employeeId = int.Parse(hdnEmployeeId.Value);
            LoadEmployeeDetails(employeeId);
        }


        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                DesignationHistory obj = new DesignationHistory();                          
                    
                obj.EmployeeId = int.Parse(hdnEmployeeId.Value);
                obj.DesignationId = int.Parse(ddlDesignation.SelectedValue);

                obj.FromDate = calFromDate.Text.Trim();
                obj.FromDateEng = BLL.BaseBiz.GetEngDate(obj.FromDate, IsEnglish);

                if (!string.IsNullOrEmpty(txtNote.Text.Trim()))
                    obj.Note = txtNote.Text.Trim();

                obj.EventID = int.Parse(ddlEventType.SelectedValue);

                // Validate specific Designation under the Level
                if (NewHRManager.HasEmployeeLevelGrade(obj.EmployeeId.Value))
                {
                    EEmployee emp = EmployeeManager.GetEmployeeById(obj.EmployeeId.Value);
                    if( emp != null)
                    {
                        BLevel level = NewPayrollManager.GetLevelById(emp.PEmployeeIncomes[0].LevelId.Value);
                        EDesignation designationLevel = new CommonManager().GetDesignationById(obj.DesignationId.Value);

                        if (level.LevelId != designationLevel.LevelId)
                        {
                            divWarningMsg.InnerHtml = "Employee level is " + level.Name + ", so desingation applied should be of level " + level.Name + ".";
                            divWarningMsg.Hide = false;
                            return;
                        }
                    }
                }


                if (Request.QueryString["Id"] != null)
                {
                    obj.EmployeeDesignationId = int.Parse(hdnEmployeeDesignationId.Value);

                    NewHRManager.UpdateDesignationHistory(obj);

                    hdnMessage.Value = "Designation transfer updated.";
                }
                else
                {
                    Status status = NewHRManager.SaveDesignationHistory(obj);

                    hdnMessage.Value = "Designation transfer saved.";
                }

                btnCancel_Click(null, null);
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            btnSave.Text = "Save";

            if (string.IsNullOrEmpty(txtEmpSearchText.Text.Trim()))
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "test", "javascript:closePopup();", true);
                return;
            }

            int employeeId = int.Parse(hdnEmployeeId.Value);

            if (employeeId != -1)
            {
                NewHRManager.SaveFirstDesignHistoryIfNotExists(employeeId);
            }

            if (e == null)
                JavascriptHelper.DisplayClientMsg(hdnMessage.Value, Page, "closePopup();");
            else
                Page.ClientScript.RegisterStartupScript(this.GetType(), "test", "javascript:closePopup();", true);
        }

    }
}