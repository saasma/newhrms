﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using Utils.Helper;
using DAL;
using BLL.Base;
using Utils.Web;
using Utils.Calendar;

namespace Web.CP
{
    public partial class DepositDocument : BasePage
    {

        int depositDocumentId = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
             if (Request.QueryString["Id"] != null)
            {
                depositDocumentId = int.Parse(Request.QueryString["Id"]);
            }
            if (!IsPostBack)
            {
                Initialise();               
            }

            if (this.Request.Form["__EVENTTARGET"] != null && this.Request.Form["__EVENTTARGET"].Equals(updPanel.ClientID))
            {

                gvw.DataSource = BLL.BaseBiz.PayrollDataContext.DepositDocumentAttachments.Where(x => x.DepositDocumentId == depositDocumentId).ToList();
                gvw.DataBind();
            }
          
            JavascriptHelper.AttachPopUpCode(Page, "popImageUpload", "DepositDocumentUpload.aspx", 460, 250);
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "sdfdsfds", "var depositId=" + depositDocumentId + ";", true);
        }
        
        void Initialise()
        {

            if (Session["DocSaved"] != null)
            {
                divMsgCtl.InnerHtml = "Document saved.";
                divMsgCtl.Hide = false;
                Session["DocSaved"] = null;
            }

            calDateDeposited.IsEnglishCalendar = this.IsEnglish;
            calDateDeposited.SelectTodayDate();

            List<FinancialDate> years = new CommonManager().GetAllFinancialDates();
            foreach (FinancialDate date in years)
            {
                date.SetName(IsEnglish);
            }
            ddlYears.DataSource = years;
            ddlYears.DataBind();

            ddlYears.SelectedIndex = years.Count;

            ListItem item = ddlPayrollPeriods.Items[0];
            ddlPayrollPeriods.DataSource = CommonManager.GetYearPayrollPeriods(years[years.Count - 1].FinancialDateId);
            ddlPayrollPeriods.DataBind();
            ddlPayrollPeriods.Items.Insert(0, item);


            LoadEmpFromQueryString();

        }

        private void LoadEmpFromQueryString()
        {
            int Id = UrlHelper.GetIdFromQueryString("Id");
            if (Id != 0)
            {
                DAL.DepositDocument doc = HRManager.GetDepositDoc(Id);
                Process(doc);
                btnSave.Text = "Update";
            }
        }

        void SetCalendarInitialDate()
        {
        
        }
        
      
        
        void LoadBranches()
        {
          
        }

        protected void ddlYears_Changed(object sender, EventArgs e)
        {
            ListItem item = ddlPayrollPeriods.Items[0];
            ddlPayrollPeriods.DataSource = CommonManager.GetYearPayrollPeriods(int.Parse(ddlYears.SelectedValue));
            ddlPayrollPeriods.DataBind();

            ddlPayrollPeriods.Items.Insert(0, item);
        }



        protected void btnSave_Click(object sender, EventArgs e)
        {
            Page.Validate("Inc");

            if (Page.IsValid)
            {
                DAL.DepositDocument doc = Process(null);
                if (depositDocumentId == 0)
                {
                    bool status = HRManager.SaveDocument(doc);
                    if (status == false)
                    {
                        divWarningMsg.InnerHtml = "Document type already exists for this payroll period.";
                        divWarningMsg.Hide = false;
                        return;
                    }
                    Session["DocSaved"] = true;
                    Response.Redirect("DepositDocument.aspx?Id=" + doc.DepositDocumentId);
                    divMsgCtl.InnerHtml = "Deposit document saved.";
                }
                else
                {
                   doc.DepositDocumentId = depositDocumentId;
                   bool status = HRManager.UpdateDocument(doc);
                   if (status == false)
                   {
                       divWarningMsg.InnerHtml = "Document type already exists for this payroll period.";
                       divWarningMsg.Hide = false;
                       return;
                   }
                    divMsgCtl.InnerHtml = "Deposit document updated.";
                }
               
                divMsgCtl.Hide = false;
            }
            else
            {
                JavascriptHelper.DisplayClientMsg("Validation fail.", Page);
            }
        }
        protected void gvw_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvw.PageIndex = e.NewPageIndex;

            gvw.DataSource = BLL.BaseBiz.PayrollDataContext.DepositDocumentAttachments.Where(x => x.DepositDocumentId == depositDocumentId).ToList();
            gvw.DataBind();
        }

        protected void btnDelete_Click(object sender, CommandEventArgs e)
        {
            int documentId = int.Parse(e.CommandArgument.ToString());  //(int)gvw.DataKeys[e.RowIndex][0];
            if (documentId != 0)
            {
                HRManager mgr = new HRManager();
                DepositDocumentAttachment doc = new DepositDocumentAttachment();
                doc.DocumentId = documentId;
                mgr.DeleteDepositDocument(doc);


                gvw.DataSource = BLL.BaseBiz.PayrollDataContext.DepositDocumentAttachments.Where(x => x.DepositDocumentId == depositDocumentId).ToList();
                gvw.DataBind();


            }
        }



        DAL.DepositDocument Process(DAL.DepositDocument entity)
        {
            if (entity != null)
            {

                PayrollPeriod period = CommonManager.GetPayrollPeriod(entity.PayrollPeriodId);
                ddlYears.SelectedValue = period.Year.ToString();
                ddlPayrollPeriods.SelectedValue = entity.PayrollPeriodId.ToString();
                ddlPaymentFor.SelectedValue = entity.Type.ToString();
                txtBank.Text = entity.Bank;
                txtAmount.Text = GetCurrency(entity.Amount);
                txtVoucherNo.Text = entity.VoucherNo;
                calDateDeposited.SetSelectedDate(entity.DateDeposited,IsEnglish);
                txtNote.Text = entity.Note;
                txtSubmissionNo.Text = entity.SubmissionNo;
                txtDepositedBy.Text = entity.DepositedBy;
                gvw.DataSource = BLL.BaseBiz.PayrollDataContext.DepositDocumentAttachments.Where(x => x.DepositDocumentId == depositDocumentId).ToList();
                gvw.DataBind();
            }
            else
            {
                entity = new DAL.DepositDocument();

                entity.PayrollPeriodId = int.Parse(ddlPayrollPeriods.SelectedValue);
                entity.Type = int.Parse(ddlPaymentFor.SelectedValue);
                entity.Bank = txtBank.Text.Trim();
                entity.Amount = decimal.Parse(txtAmount.Text.Trim());
                entity.VoucherNo = txtVoucherNo.Text.Trim();
                entity.DateDeposited = calDateDeposited.SelectedDate.ToString();
                entity.DateDepositedEng = calDateDeposited.SelectedDate.EnglishDate;

                entity.DepositedBy = txtDepositedBy.Text.Trim();
                entity.SubmissionNo = txtSubmissionNo.Text.Trim();
                entity.Note = txtNote.Text.Trim();

                return entity;
            }
            return null;
        }
        
        protected void LoadInsuranceDetails(object sender, EventArgs e)
        {
           // //gvInsuranceDetails.DataSource = 
           // //gvInsuranceDetails.DataBind();
           // ClearInsuranceFields();
           //int empId = int.Parse(ddlEmployeeList.SelectedValue);
           ////this.CustomId = empId;
           // //insMgr.GetInsuranceDetailsByEmployee(int.Parse(ddlEmployeeList.SelectedValue));           
           // IIndividualInsurance insurance = insMgr.GetInsuranceDetailsByEmployee(empId);

           // //int insuranceId = (int)gvInsuranceDetails.DataKeys[gvInsuranceDetails.SelectedIndex][0];
           // //IIndividualInsurance insu = insMgr.GetInsuranceById(insuranceId);
           // if (insurance != null)
           // {
           //     this.CustomId = insurance.Id;
           //     ProcessInsurance(insurance);
           //     btnSave.Text = Resources.Messages.Update;
           //     fsApplySettings.Visible = false;
           // }
           // else
           // {
           //     //fsApplySettings.Visible = true;
           //     btnSave.Text = Resources.Messages.Save;
           //     this.CustomId = 0;
           // }
        }

        protected void ddlFinancialYears_Changed(object sender, EventArgs e)
        {
            //int financialYearId = int.Parse(ddlFinancialYears.SelectedValue);
            //FinancialDate year = CommonManager.GetFiancialDateById(financialYearId);
            //if (year != null)
            //{
            //    SetDate(year);
            //}
        }

        void SetDate(FinancialDate year)
        {
            //CustomDate d1 = CustomDate.GetCustomDateFromString(year.StartingDate, IsEnglish);
            //CustomDate d2 = CustomDate.GetCustomDateFromString(year.EndingDate, IsEnglish);

            //lblYearDateRange.Text = d1.ToStringSkipDay() + " - " + d2.ToStringSkipDay();
        }

        protected void gvInsuranceDetails_RowCreated(object sender, GridViewRowEventArgs e)
        {
            //if (e.Row.RowType == DataControlRowType.DataRow)
            //{                          
            //    e.Row.Attributes.Add("onmouseover",
            //    "this.originalstyle=this.style.backgroundColor;this.style.backgroundColor='#8ea4d1';this.title ='click to edit';");

            //    e.Row.Attributes.Add("onmouseout",
            //    "this.style.backgroundColor=this.originalstyle;this.title ='';");
            //}
        }
               
        protected void gvInsuranceDetails_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            


        }

        protected void gvInsuranceDetails_SelectedIndexChanged(object sender, EventArgs e)
        {
            //int insuranceId = (int)gvInsuranceDetails.DataKeys[gvInsuranceDetails.SelectedIndex][0];
            //IIndividualInsurance insu = insMgr.GetInsuranceById(insuranceId);
            //if (insu != null)
            //{
            //    ProcessInsurance(insu);
            //    btnSave.Text = Resources.Messages.Update;
            //}
        }

        protected void gvInsuranceDetails_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            //IIndividualInsurance ins = new IIndividualInsurance();
            //int id = (int)gvInsuranceDetails.DataKeys[e.RowIndex][0];
            //if (id != 0)
            //{
            //    ins.Id = id;
            //    if (insMgr.DeleteIndividualInsurance(ins) == true)
            //    {
            //        JavascriptHelper.DisplayClientMsg("Insurance information is deleted.", Page);
            //        ClearInsuranceFields();
            //    }
            //    else
            //        JavascriptHelper.DisplayClientMsg("Insurance deletion failed", Page);
            //}
            //LoadInsuranceDetails(null, null);
        }

        void ClearInsuranceFields()
        {
            
            //txtEmployeePays.Text = "";
            //txtEmployerPays.Text = "";
            //txtNote.Text = "";
            //txtPolicyNo.Text = "";
            //txtPremium.Text = "";
            //txtPolicyAmount.Text = "";
            
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            
                string Value = ddlYears.SelectedValue;

                Response.Redirect("ManageDepositList.aspx?Id=" + Value);
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            //if (!chkDeductFromSalary.Checked)
            //{
            //    rdbDeductFromSalary.Items[0].Enabled = false;
            //    rdbDeductFromSalary.Items[1].Enabled = false;
            //}
            //else
            //{
            //    rdbDeductFromSalary.Items[0].Enabled = true;
            //    rdbDeductFromSalary.Items[1].Enabled = true;
            //}
        }

        /// <summary>
        /// Button is hidden as for internal use only
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnDelete_Click(object sender, EventArgs e)
        {
            //this.CustomId = 0;

            //ClearInsuranceFields();

            //divMsgCtl.InnerHtml = "Insurance for this employee deleted.";
            //divMsgCtl.Hide = false;

            //InsuranceManager.DeleteInsurance(int.Parse(ddlEmployeeList.SelectedValue));
        }

    }
}

