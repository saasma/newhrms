﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using System.Xml;
using Utils.Helper;

namespace Web.CP
{
    public partial class AllowanceSettings : BasePage
    {
        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.HumanResource;
            }
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();
              
            }

            JavascriptHelper.AttachNonDialogPopUpCode(Page, "allowancePopup", "../ExcelWindow/TravelAllowanceExcel.aspx", 450, 500);
        }

        public void Initialise()
        {

            LoadLocation();
            LoadLevels();
        }
        
        private void LoadLevels()
        {
            GridLevels.GetStore().DataSource = TravelAllowanceManager.GetAllTravelAllowance();
            GridLevels.GetStore().DataBind();

        }

        Column GetExtGridPanelColumn(string indexId, string headerName)
        {
            Column column = new Column();
            column.DataIndex = indexId;
            column.DataIndex = indexId;
            column.Text = headerName;
            column.Align = Alignment.Right;
            column.ToolTip = headerName;
            column.MenuDisabled = true;
            column.Sortable = false;
            column.Width = new Unit(140);
          

            //if (headerName == "Total")
            column.Renderer.Fn = "getFormattedAmount";


            return column;
        }


        public void LoadLocation()
        {
            ArrayReader reader = (storeAllowanceDetail.Reader[0] as ArrayReader);
            //ColumnModel columnModelProjects = gridProjects.ColumnModel;

            List<TAAllowanceLocation> locations = TravelAllowanceManager.GetLocationList();

            string columnId = "P_{0}";
            foreach (var project in locations)
            {
                string gridIndexId = string.Format(columnId, project.LocationId);
                ModelField field = new ModelField(gridIndexId, ModelFieldType.Float);
                field.UseNull = true;
                //field.AllowBlank = true;
                Model1.Fields.Add(gridIndexId, ModelFieldType.Float);
                gridAllowanceRates.ColumnModel.Columns.Add(GetExtGridPanelColumn(gridIndexId, project.LocationName));
            }

        }
        
        public void ClearLevelFields()
        {
            hiddenValue.Text = "";
            txtName.Text = "";
            txtDescription.Text = "";
            chkIsEditable.Checked = false;
        }

      
        
        protected void btnAddLevel_Click(object sender, DirectEventArgs e)
        {
            
            ClearLevelFields();
            WindowLevel.Show();
        }

        protected void btnLoadRate_Click(object sender, DirectEventArgs e)
        {

        }
        protected void btnDeleteLevel_Click(object sender, DirectEventArgs e)
        {
            int TAllowance = int.Parse(hiddenValue.Text.Trim());
            Status status = TravelAllowanceManager.DeleteTravelAllowance(TAllowance);
            if (status.IsSuccess)
            {
                LoadLevels();
                NewMessage.ShowNormalPopup("Travel Allowance deleted.");
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }
        }

        

        protected void btnEditLevel_Click(object sender, DirectEventArgs e)
        {
            int levelId = int.Parse(hiddenValue.Text.Trim());
            TAAllowance entity = TravelAllowanceManager.GetTravelAllowanceById(levelId);
            txtName.Text = entity.Name;
            if (entity.OptionToEdit != null)
                chkIsEditable.Checked = entity.OptionToEdit.Value;
            else
                chkIsEditable.Checked = false;

            txtDescription.Text = entity.Description;

            WindowLevel.Show();

        }



        protected void btnDetailLevel_Click(object sender, DirectEventArgs e)
        {
            int allowanceID = int.Parse(hiddenValue.Text.Trim());
            List<TAAllowanceRate> detailGridList = TravelAllowanceManager.getAllowanceDetailList(allowanceID);

            List<TAAllowanceLocation> locations = TravelAllowanceManager.GetLocationList();
            List<BLevel> levels = NewPayrollManager.GetAllParentLevelList();


            object[] data = new object[levels.Count];


            //int row = 0;
            for (int row = 0; row < levels.Count; row++)
            {
                object[] rowData = new object[ locations.Count + 1 ];

                int columnIndex = 0;

                //if (xlist.Count > 0)
                //rowData[columnIndex++] = locations[i].LocationId;
                rowData[columnIndex++] = levels[row].GroupLevel;

                // Iterate for each Project & Skip first as it represents All Project
                for (int i = 0; i < locations.Count; i++)
                {
                    //first and Second one for Level Id,Level Name


                    TAAllowanceRate rate = detailGridList.FirstOrDefault(
                        x => x.LevelID == levels[row].LevelId && x.LocationId == locations[i].LocationId && x.AllowanceID == allowanceID);



                    if (rate != null)
                    {
                        rowData[columnIndex++] = rate.Rate==null ? 0 : rate.Rate.Value;
                    }
                    else
                    {
                        rowData[columnIndex++] = null;
                    }



                   
                }

                data[row] = rowData;
            }




            storeAllowanceDetail.DataSource = data;
            storeAllowanceDetail.DataBind();

            Window1.Title = "Travel Allowance : " + TravelAllowanceManager.GetTravelAllowanceById(allowanceID).Name;

            Window1.Show();
        }


        protected void btnDetailSaveUpdate_Click(object sender, DirectEventArgs e)
        {
            List<TAAllowanceRate> lines = new List<TAAllowanceRate>();
            string json = e.ExtraParams["Values"];

            if (string.IsNullOrEmpty(json))
            {
                return;
            }

            int TAllowance = int.Parse(hiddenValue.Text.Trim());

            lines = JSON.Deserialize<List<TAAllowanceRate>>(json);

            Status status = TravelAllowanceManager.InsertUpdateAllowanceDetail(lines, TAllowance);
            if (status.IsSuccess)
            {
                Window1.Hide();
                LoadLevels();
                NewMessage.ShowNormalPopup("Allowance rate saved.");
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }


        }


        protected void btnLevelSaveUpdate_Click(object sender, DirectEventArgs e)
        {

            Page.Validate("InsertUpdateLevel");
            if (Page.IsValid)
            {
                TAAllowance allowanceInstance = new TAAllowance();
                int allowanceID;
                allowanceInstance.Name = txtName.Text.Trim();
                allowanceInstance.OptionToEdit = chkIsEditable.Checked;
                allowanceInstance.Description = txtDescription.Text.Trim();

                bool isInsert = true;
                
                if (!string.IsNullOrEmpty(hiddenValue.Text))
                {
                    allowanceInstance.AllowanceID = int.Parse(hiddenValue.Text.Trim());
                    isInsert = false;
                }

                Status status = TravelAllowanceManager.InsertUpdateAllowance(allowanceInstance, isInsert);
                if (status.IsSuccess)
                {
                    WindowLevel.Hide();
                    LoadLevels();
                    NewMessage.ShowNormalPopup("Allowance saved.");
                }
                else
                {
                    NewMessage.ShowWarningMessage(status.ErrorMessage);
                }

            }
             
             
        }
 
    }
}

/*
 
 
 
*/