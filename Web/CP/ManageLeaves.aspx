<%@ Page Title="Leave List" Language="C#" MasterPageFile="~/Master/HROld.Master"
    AutoEventWireup="true" CodeBehind="ManageLeaves.aspx.cs" Inherits="Web.CP.ManageLeave" %>

<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function popupAddLeaveCall(leaveType) {

            if (typeof (leaveType) == 'undefined')
                leaveType = 1;

            var ret = popupLeave('leavetype=' + leaveType);
            if (typeof (ret) != 'undefined') {
                if (ret == 'Reload') {
                    //refreshIncomeList(0);
                    __doPostBack('<%= updPanel.ClientID %>', '')
                }
            }

            return false;
        }

        function reloadLeave(popupWindow) {
            popupWindow.close();
            __doPostBack('<%= updPanel.ClientID %>', '');
        }

        function popupUpdateLeaveCall(LeaveId) {
            var ret = popupLeave('LId=' + LeaveId);

            if (typeof (ret) != 'undefined') {
                if (ret == 'Reload') {
                    //refreshIncomeList(0);
                    __doPostBack('<%= updPanel.ClientID %>', '');
                }
            }
            return false;
        }


    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
     <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Leave list
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
    

    <div class="contentArea">
        
        <div style='clear: both'>
        </div>
        <asp:UpdatePanel ID="updPanel" runat="server">
            <ContentTemplate>
                <uc2:MsgCtl ID="divMsgCtl" EnableViewState="false" Hide="true" runat="server" />
                <uc2:WarningCtl ID="divWarningMsg" EnableViewState="false" Hide="true" runat="server" />
                <asp:GridView PagerStyle-HorizontalAlign="Center" PagerStyle-CssClass="defaultPagingBar"
                    CssClass="table table-primary mb30 table-bordered table-hover" AutoGenerateColumns="false" UseAccessibleHeader="true"
                    ID="gvwLeaves" runat="server" CellPadding="3" DataKeyNames="LeaveTypeId"  GridLines="None" Width="100%" OnRowCreated="gvwEmployees_RowCreated" OnRowDeleting="gvwLeaves_RowDeleting">
                    <Columns>
                        <asp:TemplateField HeaderText="Title" HeaderStyle-Width="170px" HeaderStyle-HorizontalAlign="Left"
                            ItemStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <%# Eval("Title") %>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Short name" HeaderStyle-Width="100px" HeaderStyle-HorizontalAlign="Left"
                            ItemStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <%# Eval("Abbreviation") %>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Accrue" HeaderStyle-Width="130px" HeaderStyle-HorizontalAlign="Left"
                            ItemStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <%# new DAL.LeaveAccrue().Get(Eval("FreqOfAccrual").ToString())%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Leave Per Period" HeaderStyle-Width="80px" HeaderStyle-HorizontalAlign="Left"
                            ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <%# Eval("LeavePerPeriod")%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Payment Type" HeaderStyle-Width="130px" HeaderStyle-HorizontalAlign="Left"
                            ItemStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <%# new DAL.LeaveType().Get(Eval("Type").ToString())%>
                            </ItemTemplate>
                        </asp:TemplateField>
                         <asp:TemplateField HeaderText="Unused Leave" HeaderStyle-Width="140px" HeaderStyle-HorizontalAlign="Left"
                            ItemStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <%# Eval("UnusedLeave") == null ? "" : new DAL.UnusedLeave().Get(Eval("UnusedLeave").ToString())%>
                            </ItemTemplate>
                        </asp:TemplateField>
                          <asp:TemplateField HeaderText="Encashment Incomes" HeaderStyle-HorizontalAlign="Left"
                            ItemStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <%# Eval("EncashmentIncomes") == null ? "" :  Eval("EncashmentIncomes").ToString()%>
                            </ItemTemplate>
                        </asp:TemplateField>
                       <%-- <asp:TemplateField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                        </asp:TemplateField>--%>
                         <asp:TemplateField HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="150px"
                            HeaderText="Single Min Request" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:TextBox data-col='0' data-row='<%# Container.DataItemIndex %>' runat="server"
                                    Style='text-align: right' Width="50" ID="txtMin" Text='<%# Eval("SingleMinRequest") %>'></asp:TextBox>
                                <asp:CompareValidator runat="server" ID="valOrder1" Type="Double" Operator="DataTypeCheck"
                                    Display="None" ValidationGroup="saveupdate" ControlToValidate="txtMin"></asp:CompareValidator>
                            </ItemTemplate>
                        </asp:TemplateField>
                         <asp:TemplateField HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="150px"
                            HeaderText="Single Max Request" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:TextBox data-col='0' data-row='<%# Container.DataItemIndex %>' runat="server"
                                    Style='text-align: right' Width="50" ID="txtMax" Text='<%# Eval("SingleMaxRequest") %>'></asp:TextBox>
                                <asp:CompareValidator runat="server" ID="valOrder2" Type="Double" Operator="DataTypeCheck"
                                    Display="None" ValidationGroup="saveupdate" ControlToValidate="txtMax"></asp:CompareValidator>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="50px"
                            HeaderText="Order" ItemStyle-HorizontalAlign="Right">
                            <ItemTemplate>
                                <asp:TextBox data-col='0' data-row='<%# Container.DataItemIndex %>' runat="server"
                                    Style='text-align: right' Width="50" ID="txtOrder" Text='<%# Eval("Order") %>'></asp:TextBox>
                                <asp:CompareValidator runat="server" ID="valOrder" Type="Integer" Operator="DataTypeCheck"
                                    Display="None" ValidationGroup="saveupdate" ControlToValidate="txtOrder"></asp:CompareValidator>
                            </ItemTemplate>
                        </asp:TemplateField>
                         <asp:TemplateField HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="50px"
                            HeaderText="Absent Marking <br>Order" ItemStyle-HorizontalAlign="Right">
                            <ItemTemplate>
                                <asp:TextBox data-col='0' data-row='<%# Container.DataItemIndex %>' runat="server"
                                    Style='text-align: right' Width="50" ID="txtAbsentOrder" Text='<%# Eval("AbsentMarkingOrder") %>'></asp:TextBox>
                                <asp:CompareValidator runat="server" ID="valOrder111" Type="Integer" Operator="DataTypeCheck"
                                    Display="None" ValidationGroup="saveupdate" ControlToValidate="txtAbsentOrder"></asp:CompareValidator>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderStyle-Width="30px" HeaderText="Edit" HeaderStyle-HorizontalAlign="Center"
                            ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:ImageButton ID="btnEdit" CommandName="Delete" ImageUrl="~/images/edit.gif" runat="server"
                                    OnClientClick='<%# "return popupUpdateLeaveCall(" +  Eval("LeaveTypeId") + ");" %>'
                                    AlternateText="Edit" />
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderStyle-Width="30px" HeaderText="Delete" HeaderStyle-HorizontalAlign="Center"
                            ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:ImageButton ID="btnDelete" Visible='<%# BLL.Manager.LeaveAttendanceManager.IsLeaveDeletable ( (int) Eval( "LeaveTypeId") ) %>'
                                    CommandName="Delete" ImageUrl="~/images/delet.png" runat="server" OnClientClick='return confirm("Do you want to delete the leave?")'
                                    AlternateText="Delete" />
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                        </asp:TemplateField>
                    </Columns>
                    <RowStyle CssClass="odd" />
                    <AlternatingRowStyle CssClass="even" />
                    <EmptyDataTemplate>
                        No leaves
                    </EmptyDataTemplate>
                </asp:GridView>
            </ContentTemplate>
        </asp:UpdatePanel>
        <div class="buttonsDiv">
            <asp:LinkButton ID="btnAddNew" Style="margin-left: 10px;height:30px" OnClientClick="return popupAddLeaveCall();"
                runat="server" CssClass="btn btn-primary btn-sm  floatleft" Text="Add New Leave" />

                <asp:LinkButton ID="btnGroup" Style="margin-left: 10px;height:30px" OnClientClick="return popupAddLeaveCall(2);"
                runat="server" CssClass="btn btn-primary btn-sm  floatleft" Text="Add Group" />

                <asp:LinkButton ID="btnChild" Style="margin-left: 10px;height:30px" OnClientClick="return popupAddLeaveCall(3);"
                runat="server" CssClass="btn btn-primary btn-sm  floatleft" Text="Add Child Leave" />

            <asp:Button ID="Button1" Style="margin-right: 87px" runat="server" CssClass="btn btn-primary btn-sm" Text="Save Changes" OnClick="Unnamed1_Click"
                OnClientClick="valGroup='saveupdate';return CheckValidation();" />
        </div>
    </div>
    </div>
</asp:Content>
