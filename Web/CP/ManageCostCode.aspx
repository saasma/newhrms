﻿<%@ Page Title="Manage cost codes" Language="C#" MasterPageFile="~/Master/HROld.Master"
    AutoEventWireup="true" CodeBehind="ManageCostCode.aspx.cs" Inherits="Web.CP.ManageCostCode" %>

<%@ Register Src="../UserControls/MgCostCode.ascx" TagName="MgCostCode" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Manage Cost Codes
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <uc1:MgCostCode ID="MgCostCode1" runat="server" />
    </div>
</asp:Content>
