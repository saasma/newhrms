﻿<%@ Page Title="Award List" Language="C#" MasterPageFile="~/Master/NewDetails.Master" AutoEventWireup="true" CodeBehind="AwardList.aspx.cs" Inherits="Web.CP.AwardList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">

<style type="text/css">
        .hideLeftBlockCssInPage
        {
            margin: 0px !important;
            padding-left: 20px !important;
        }
        #menu
        {
            display: none;
        }
        #content
        {
            margin: inherit;
        }
    </style>


    <script type="text/javascript">
    
    var CommandHandler = function(command, record){
            <%= hdnAwardId.ClientID %>.setValue(record.data.CashAwardTypeId);
            
                if(command=="Edit")
                {
                    <%= btnEdit.ClientID %>.fireEvent('click');
                }
                else
                {
                    <%= btnDelete.ClientID %>.fireEvent('click');
                }

             };

     
    </script>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">

<ext:Hidden ID="hdnAwardId" runat="server" />

<ext:LinkButton ID="btnEdit" runat="server" Hidden="true">
    <DirectEvents>
        <Click OnEvent="btnEdit_Click">
            <EventMask ShowMask="true" />
        </Click>
    </DirectEvents>
</ext:LinkButton>

<ext:LinkButton ID="btnDelete" runat="server" Hidden="true">
    <DirectEvents>
        <Click OnEvent="btnDelete_Click">
            <EventMask ShowMask="true" />
        </Click>
    </DirectEvents>
</ext:LinkButton>

    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Award List
                </h4>
            </div>
        </div>
    </div>

<div class="contentpanel">
    <div class="panel panel-default">
        <!-- panel-heading -->
        <div class="panel-body">
             <ext:GridPanel ID="gridAward" runat="server" Width="425" Cls="itemgrid">
                    <Store>
                        <ext:Store ID="Store1" runat="server">
                            <Model>
                                <ext:Model ID="Model4" runat="server" IDProperty="ID">
                                    <Fields>
                                        <ext:ModelField Name="CashAwardTypeId" Type="String" />
                                        <ext:ModelField Name="Name" Type="string" />
                                        <ext:ModelField Name="IncomeName" Type="string" />
                                    </Fields>
                                </ext:Model>
                            </Model>
                        </ext:Store>
                    </Store>
                    <ColumnModel>
                        <Columns>                            
                            <ext:Column ID="colAwardName" Sortable="false" MenuDisabled="true" runat="server" Text="Name"
                                Align="Left" Width="200" DataIndex="Name" />                            
                            <ext:Column ID="Column1" Sortable="false" MenuDisabled="true" runat="server" Text="Income Name"
                                Align="Left" Width="150" DataIndex="IncomeName" />
                            <ext:CommandColumn ID="CommandColumn1" runat="server" Width="75" Text="Actions" Align="Center">
                                <Commands>
                                    <ext:CommandSeparator />
                                    <ext:GridCommand Cls="editGridButton" ToolTip-Text="Edit" Text="<i class='fa fa-pencil'></i>"
                                        CommandName="Edit" />
                                    <ext:GridCommand Cls="editGridButton" ToolTip-Text="Delete" Text="<i class='fa fa-trash-o'></i>"
                                        CommandName="Delete" />
                                </Commands>
                                <Listeners>
                                    <Command Handler="CommandHandler(command,record);" />
                                </Listeners>
                            </ext:CommandColumn>
                        </Columns>
                    </ColumnModel>
                    <SelectionModel>
                        <ext:RowSelectionModel ID="RowSelectionModel2" runat="server" Mode="Single" />
                    </SelectionModel>
                </ext:GridPanel>
                <div class="buttonBlock" runat="server" id="buttonBlock">
                    <ext:Button runat="server" Cls="btn btn-primary" ID="btnAddNew" Text="<i></i>Add New">
                        <DirectEvents>
                            <Click OnEvent="btnAddNew_Click">
                                <EventMask ShowMask="true" />
                            </Click>
                        </DirectEvents>
                    </ext:Button>
        </div>
    </div>
</div>
</div>

<ext:Window ID="WAward" runat="server" Title="Add/Edit Award" Icon="Application"
            Width="500" Height="250" BodyPadding="5" Hidden="true" Modal="true">
            <Content>
                <table class="fieldTable">
                    <tr>
                        <td style="width:210px;">
                            <ext:TextField ID="txtAwardName" LabelSeparator="" runat="server" FieldLabel="Name *" Width="180" LabelAlign="Top">
                            </ext:TextField>
                            <asp:RequiredFieldValidator Display="None" ID="rfvAwardName" runat="server" ValidationGroup="SaveUpdateAward"
                                ControlToValidate="txtAwardName" ErrorMessage="Award Name is required." />
                        </td>
                        <td>
                             <ext:ComboBox FieldLabel="Show Award in *" ID="cmbShowAwardIn" Width="180" runat="server" ValueField="IncomeId"
                                DisplayField="Title" LabelAlign="Top" LabelSeparator="" ForceSelection="true"
                                QueryMode="Local">
                                <Store>
                                    <ext:Store ID="Store2" runat="server">
                                        <Model>
                                            <ext:Model ID="Model1" IDProperty="IncomeId" runat="server">
                                                <Fields>
                                                    <ext:ModelField Name="IncomeId" />
                                                    <ext:ModelField Name="Title" />
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                            </ext:ComboBox>
                            <asp:RequiredFieldValidator Display="None" ID="rfvIncomeId" runat="server" ValidationGroup="SaveUpdateAward"
                                ControlToValidate="cmbShowAwardIn" ErrorMessage="Show Award in is required." />
                        </td>
                    </tr>                   
                    <tr>
                        <td valign="bottom">
                            <div class="popupButtonDiv">
                                <ext:Button runat="server" ID="btnSave" Cls="btn btn-primary" Text="Save">
                                    <DirectEvents>
                                        <Click OnEvent="btnSave_Click">
                                            <EventMask ShowMask="true" />
                                        </Click>
                                    </DirectEvents>
                                    <Listeners>
                                        <Click Handler="valGroup = 'SaveUpdateAward'; if(CheckValidation()) return this.disable(); else return false;">
                                        </Click>
                                    </Listeners>
                                </ext:Button>
                                <div class="btnFlatOr">
                                    or</div>
                                <ext:LinkButton runat="server" Cls="btnFlatLeftGap" ID="LinkButton1" Text="<i></i>Cancel">
                                    <Listeners>
                                        <Click Handler="#{WAward}.hide();">
                                        </Click>
                                    </Listeners>
                                </ext:LinkButton>
                            </div>
                        </td>
                    </tr>
                </table>
            </Content>
        </ext:Window>

</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
