﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using BLL.Manager;
using BLL;
using DAL;
using Utils.Web;
using Utils.Helper;
using System.Text;


namespace Web.CP
{



    public partial class AdminSettings : BasePage
    {
        CompanyManager compMgr = new CompanyManager();

        private void CheckPermission()
        {
            if (Page.User.IsInRole(Role.Administrator.ToString())
                && Page.Request.IsLocal)
            {

            }
            else
            {
                //if not accessible then redirect to Default page
                Response.Redirect("~/Default.aspx");
            }

        }


        protected void Page_Load(object sender, EventArgs e)
        {
            

            CheckPermission();

            if (!IsPostBack)
            {
                LoadCalculationSettings();
                BindCompanySetting();
            }
        }



        protected void LoadCalculationSettings()
        {
            txtCompanyMonthlyHours.Text = CommonManager.CalculationConstant.CompanyMonthlyHours.ToString();
            if ((bool)CommonManager.CalculationConstant.CompanyHasHourlyLeave)
                ddlCompanyHasHourlyLeave.SelectedIndex = 1;
            else
                ddlCompanyHasHourlyLeave.SelectedIndex = 2;
            txtCompanyLeaveHoursInDays.Text = CommonManager.CalculationConstant.CompanyLeaveHoursInADay.ToString();
            if ((bool)CommonManager.CalculationConstant.CompanyIsHPL)
                ddlCompanyIsHPL.SelectedIndex = 1;
            else
                ddlCompanyIsHPL.SelectedIndex = 2;

            txtGratuityDaysInYear.Text = CommonManager.CalculationConstant.GratuityDaysInYear.ToString();
            txtGratuityPermanentYears.Text = CommonManager.CalculationConstant.GratuityPermanentYears.ToString();
            
        }

        protected void BindCompanySetting()
        {
            gvwCompanySetting.DataSource = CommonManager.GetCompanySetting();
            gvwCompanySetting.DataBind();
        }

        protected void btnEdit1_Click(object sender, EventArgs e)
        {
            if (btnEdit1.Text.ToLower().Equals("edit"))
            {
                btnEdit1.Text = "Cancel";
                txtCompanyMonthlyHours.Enabled = true;
            }
            else
            {
                btnEdit1.Text = "Edit";
                txtCompanyMonthlyHours.Enabled = false;
            }
        }

        protected void btnEdit2_Click(object sender, EventArgs e)
        {
            if (btnEdit2.Text.ToLower().Equals("edit"))
            {
                btnEdit2.Text = "Cancel";
                ddlCompanyHasHourlyLeave.Enabled = true;
            }
            else
            {
                btnEdit2.Text = "Edit";
                ddlCompanyHasHourlyLeave.Enabled = false;
            }
        }

        protected void btnEdit3_Click(object sender, EventArgs e)
        {
            if (btnEdit3.Text.ToLower().Equals("edit"))
            {
                btnEdit3.Text = "Cancel";
                txtCompanyLeaveHoursInDays.Enabled = true;
            }
            else
            {
                btnEdit3.Text = "Edit";
                txtCompanyLeaveHoursInDays.Enabled = false;
            }
        }

        protected void btnEdit4_Click(object sender, EventArgs e)
        {
            if (btnEdit4.Text.ToLower().Equals("edit"))
            {
                btnEdit4.Text = "Cancel";
                ddlCompanyIsHPL.Enabled = true;
            }
            else
            {
                btnEdit4.Text = "Edit";
                ddlCompanyIsHPL.Enabled = false;
            }
        }

        protected void btnEdit5_Click(object sender, EventArgs e)
        {
            if (btnEdit5.Text.ToLower().Equals("edit"))
            {
                btnEdit5.Text = "Cancel";
                txtGratuityDaysInYear.Enabled = true;
            }
            else
            {
                btnEdit5.Text = "Edit";
                txtGratuityDaysInYear.Enabled = false;
            }
        }

        protected void btnEdit6_Click(object sender, EventArgs e)
        {
            if (btnEdit6.Text.ToLower().Equals("edit"))
            {
                btnEdit6.Text = "Cancel";
                txtGratuityPermanentYears.Enabled = true;
            }
            else
            {
                btnEdit6.Text = "Edit";
                txtGratuityPermanentYears.Enabled = false;
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (txtCompanyLeaveHoursInDays.Text.Trim().Equals("") || txtCompanyLeaveHoursInDays.Text.Trim().Equals("")
                || txtGratuityDaysInYear.Text.Trim().Equals("") || txtGratuityPermanentYears.Text.Trim().Equals(""))
                return;
            if (ddlCompanyIsHPL.SelectedIndex == -1 || ddlCompanyHasHourlyLeave.SelectedIndex == -1)
                return;
            CalculationConstant calcConst = new CalculationConstant();
            calcConst.CompanyMonthlyHours = double.Parse(txtCompanyMonthlyHours.Text.Trim());
            calcConst.CompanyLeaveHoursInADay = int.Parse(txtCompanyLeaveHoursInDays.Text.Trim());
            calcConst.CompanyHasHourlyLeave = bool.Parse(ddlCompanyHasHourlyLeave.SelectedItem.Text);
            calcConst.CompanyIsHPL = bool.Parse(ddlCompanyIsHPL.SelectedItem.Text);
            calcConst.GratuityDaysInYear = int.Parse(txtGratuityDaysInYear.Text.Trim());
            calcConst.GratuityPermanentYears = int.Parse(txtGratuityPermanentYears.Text.Trim());
            if (CommonManager.UpdateCalculationConstant(calcConst))
            {
                divMsgCtl.Hide = false;
                divMsgCtl.InnerHtml = "Constant value successfully updated.";
                LoadCalculationSettings();
            }
            // clear settings
            MyCache.Reset();
        }

        protected void btnSaveCompanySetting_Click(object sender, EventArgs e)
        {
            List<CompanySetting> companySettings = new List<CompanySetting>();
            CompanySetting compsett = null;
            foreach (GridViewRow row in gvwCompanySetting.Rows)
            {
                compsett = new CompanySetting();
                Label lblKey = (Label)row.FindControl("lblKey");
                TextBox txtValue = (TextBox)row.FindControl("txtValue");
                compsett.Key = lblKey.Text;
                compsett.Value = txtValue.Text.Trim();
                companySettings.Add(compsett);
            }

            if (CommonManager.UpdateCompanySetting(companySettings))
            {
                MsgCtl1.Hide = false;
                MsgCtl1.InnerHtml = "Company setting successfully saved.";
                BindCompanySetting();

            }
            else

            {
                MsgCtl1.Hide = false;
                MsgCtl1.InnerHtml = "Company setting save failed.";
            }

            // clear settings
            MyCache.Reset();
        }

        protected void btnEditCompanySetting_Click(object sender, EventArgs e)
        {
            
            GridViewRow row = (GridViewRow)(sender as Button).NamingContainer;
            TextBox txtValue = (TextBox)row.FindControl("txtValue");
            Button btn = (Button)row.FindControl("btnEditCompanySetting");
            if (btn.Text.ToLower().Equals("edit"))
            {
                btn.Text = "Cancel";
                txtValue.Enabled = true;
            }
            else
            {
                btn.Text = "Edit";
                txtValue.Enabled = false;
            }
        }

        public string GetValidationExpression(object datatype)
        {
            if (datatype != null)
            {

                if (datatype.ToString().ToLower().Contains("int"))
                {
                    return @"^\d+$";
                }
                else if (datatype.ToString().ToLower().Contains("varchar"))
                {
                    return @".*";
                }
                else if (datatype.ToString().ToLower().Contains("bit"))
                {
                    return @"^(true|false)";
                }
            }
            return @".*"; 
        }

        public string GetErrorMessage(object datatype)
        {
            if (datatype != null)
            {

                if (datatype.ToString().ToLower().Contains("int"))
                {
                    return @"Please enter a numeric data.";
                }
                else if (datatype.ToString().ToLower().Contains("varchar"))
                {
                    return "please enter a valid string.";
                }
                else if (datatype.ToString().ToLower().Contains("bit"))
                {
                    return "Please enter either true or false.";
                }
            }
            return string.Empty;
        }
    }

}

