<%@ Page Title="Bank list" Language="C#" MasterPageFile="~/Master/HROld.Master" AutoEventWireup="true"
    CodeBehind="ManageBanks.aspx.cs" Inherits="Web.CP.ManageBanks" %>

<%@ Register Src="~/UserControls/ManageBank.ascx" TagName="Manage" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <style type="text/css">
        .marginal
        {
            overflow: hidden;
        }
        
        .</style>
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Salary Payment Banks
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <uc1:Manage ID="Manage1" runat="server" />
    </div>
</asp:Content>
