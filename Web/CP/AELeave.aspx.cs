﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using BLL.Manager;
using Utils.Base;
using BLL;
using DAL;
using Utils.Web;
using Utils.Helper;
using System.Text.RegularExpressions;

namespace Web.CP
{
    public partial class AELeave : BasePage
    {
        LeaveAttendanceManager leaveMgr = new LeaveAttendanceManager();
        int eId = 0;
        private bool isNonEditableWarningVisible = false;

        public LeaveTypeEnum LeaveType
        {
            get
            {
                if (ViewState["LeaveTypeEnum"] == null)
                    return LeaveTypeEnum.Normal;
                return (LeaveTypeEnum) ViewState["LeaveTypeEnum"];
            }
            set
            {
                
                ViewState["LeaveTypeEnum"] = value;
            }
        }

        public int LeaveId
        {
            get
            {
                if (ViewState["LeaveId"] == null)
                    return 0;
                return int.Parse(ViewState["LeaveId"].ToString());
            }
            set
            {
                ViewState["LeaveId"] = value;
            }
        }

        public PageModeList PageModeList
        {
            get
            {
                if (ViewState["PageModeList"] == null)
                    return PageModeList.NotSet;
                return (PageModeList)ViewState["PageModeList"];
            }
            set
            {
                ViewState["PageModeList"] = value;
            }

        }

        protected void ddlGroupName_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlGroupName.SelectedValue != "-1")
            {
                LLeaveType groupLeave = LeaveAttendanceManager.GetLeaveTypeById(int.Parse(ddlGroupName.SelectedValue));
                if (groupLeave != null)
                {
                    Process(groupLeave);
                    txtTitle.Text = "";
                    txtAbbreviation.Text = "";
                    this.LeaveType = LeaveTypeEnum.Child;
                }
            }
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            divWarningMsg.InnerHtml = Resources.Messages.LeaveFormNotEditableDuringIntialSaveMsg;

            eId = UrlHelper.GetIdFromQueryString("EId");
            
            if (!IsPostBack)
            {
                gender.InnerHtml = string.Format(gender.InnerHtml, CommonManager.GetGenderName);
                Initialise();


                if (!string.IsNullOrEmpty(Request.QueryString["leavetype"]))
                {
                    this.LeaveType = (LeaveTypeEnum)int.Parse(Request.QueryString["leavetype"]);
                }

            }
                //if Lapse checkbox list is disabled then load data to preserve last selection
            else
            {
                if (ddlUnusedLeave.SelectedValue==UnusedLeave.LAPSE &&
                    cblLapseOn.Enabled == false )
                {
                    PreserveLapseCheckboxListSelection();
                }
            }
            JavascriptCode();
           // JavascriptHelper.AttachEnableDisablingJSCode(chkMaxEncashOnRetirement, upMaxEnashBalance.ClientID, false);
            //chkMaxEncashOnRetirement.Attributes.Add("onclick", "maxEncashChange(this)");
            //if (!string.IsNullOrEmpty(Request.QueryString["LId"]))
            //{
            //    string LId = Request.QueryString["LId"];
            //    hdnLeaveTypeID.Value = LId;
            //    JavascriptHelper.AttachPopUpCode(Page, "popupCreateNew", "AELeaveBranchDepartments.aspx", 600, 510);
            //}
          
        }

        protected void gvw_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            decimal[] amountArray = new decimal[gvwServicePeriods.Rows.Count];
            decimal amount = 0;
            int i = 0;

            List<LLeaveServicePeriod> list = new List<LLeaveServicePeriod>();

            foreach (GridViewRow row in gvwServicePeriods.Rows)
            {
                if (e.RowIndex != row.RowIndex)
                {
                    TextBox txtPeriodUpto = (TextBox)gvwServicePeriods.Rows[row.RowIndex].FindControl("txtPeriodUpto");
                    TextBox txtLeavePerPeriod = (TextBox)gvwServicePeriods.Rows[row.RowIndex].FindControl("txtLeavePerPeriod");
                    TextBox txtNotes = (TextBox)gvwServicePeriods.Rows[row.RowIndex].FindControl("txtNotes");

                    //TextBox txtItem = (TextBox)gvEducationalEmpReim.Rows[rowIndex].FindControl("txtItem");
                    //TextBox txtAmount = (TextBox)gvEducationalEmpReim.Rows[rowIndex].FindControl("txtAmount");
                    //TextBox txtApprovedAmount = (TextBox)gvEducationalEmpReim.Rows[rowIndex].FindControl("txtApprovedAmount");

                    LLeaveServicePeriod entity = new LLeaveServicePeriod();

                    if (txtPeriodUpto.Text.Trim() != "")
                        entity.PeriodUptoDays = double.Parse(txtPeriodUpto.Text.Trim());

                    if (txtLeavePerPeriod.Text.Trim() != "")
                        entity.LeavePerPeriod = double.Parse(txtLeavePerPeriod.Text.Trim());

                    if (txtNotes.Text.Trim() != "")
                        entity.Notes = txtNotes.Text.Trim();

                    list.Add(entity);
                }
            }

            gvwServicePeriods.DataSource = list;
            gvwServicePeriods.DataBind();

        }

        void PreserveLapseCheckboxListSelection()
        {
            int leaveTypeId = UrlHelper.GetIdFromQueryString("Id");
            
            int LeaveId = UrlHelper.GetIdFromQueryString("LId");
            LLeaveType leave=null;
            LeaveAttendanceManager mgr = new LeaveAttendanceManager();
            if (leaveTypeId != 0)
                leave = mgr.GetLeaveType(leaveTypeId);
            else if (LeaveId != 0)
                leave = mgr.GetLeaveType(LeaveId);

            if (leave != null)
            {
                foreach (LLeaveLapse obLL in leave.LLeaveLapses)
                {
                    ListItem item = cblLapseOn.Items.FindByValue(obLL.LapseOn.ToString());
                    if (item != null)
                        item.Selected = true;
                }
            }
        }

        void ShowHideNotEditableMsg()
        {
            //TODO: for now check for side effect
            return;
            //PayrollPeriod payrollPeriod = CommonManager.GetLastPayrollPeriod();
            //if (payrollPeriod != null && ddlAccrue.SelectedValue != LeaveAccrue.MANUALLY)
            //{
            //    if (CalculationManager.IsCalculationSaved(payrollPeriod.PayrollPeriodId) != null
            //        && !CalculationManager.IsAllEmployeeSavedFinally(payrollPeriod.PayrollPeriodId, false))
            //    {
            //        divWarningMsg.Hide = false;
            //        //divWarningMsg.Style.Remove("display");
            //        isNonEditableWarningVisible = true;
            //        btnSave.Enabled = false;
            //    }
            //}
        }
        protected void btnAddRow_Click(object sender, EventArgs e)
        {
            AddNewRow();
        }

        private void AddNewRow()
        {
            List<LLeaveServicePeriod> list = GetGridList();
            list.Add(new LLeaveServicePeriod { });
            gvwServicePeriods.DataSource = list;
            gvwServicePeriods.DataBind();
        }

        private List<LLeaveServicePeriod> GetGridList()
        {
            int rowIndex = 0;
            List<LLeaveServicePeriod> list = new List<LLeaveServicePeriod>();
            for (int i = 0; i < gvwServicePeriods.Rows.Count; i++)
            {
                TextBox txtPeriodUpto = (TextBox)gvwServicePeriods.Rows[rowIndex].FindControl("txtPeriodUpto");
                TextBox txtLeavePerPeriod = (TextBox)gvwServicePeriods.Rows[rowIndex].FindControl("txtLeavePerPeriod");
                TextBox txtNotes = (TextBox)gvwServicePeriods.Rows[rowIndex].FindControl("txtNotes");
             
                //TextBox txtItem = (TextBox)gvEducationalEmpReim.Rows[rowIndex].FindControl("txtItem");
                //TextBox txtAmount = (TextBox)gvEducationalEmpReim.Rows[rowIndex].FindControl("txtAmount");
                //TextBox txtApprovedAmount = (TextBox)gvEducationalEmpReim.Rows[rowIndex].FindControl("txtApprovedAmount");

                LLeaveServicePeriod entity = new LLeaveServicePeriod();

                if (txtPeriodUpto.Text.Trim() != "")
                    entity.PeriodUptoDays = double.Parse(txtPeriodUpto.Text.Trim());

                if (txtLeavePerPeriod.Text.Trim() != "")
                    entity.LeavePerPeriod = double.Parse(txtLeavePerPeriod.Text.Trim());

                if (txtNotes.Text.Trim() != "")
                    entity.Notes = txtNotes.Text.Trim();

                
                rowIndex++;

                list.Add(entity);
            }
            return list;
        }

        public void chkBasedOnServicePeriod_Click(object sender, EventArgs e)
        {
            if (chkBasedOnServicePeriod.Checked)
            {
                rowLeavePerPeriod.Visible = false;
                rowServicePeriod.Style["display"] = "";
            }
            else
            {
                rowLeavePerPeriod.Visible = true;
                rowServicePeriod.Style["display"] = "none";
            }
        }

        void JavascriptCode()
        {
            //add javascript to enable/disable textbox related to CheckBox
            //JavascriptHelper.AttachCodeToDisableOther(chkIsBalanceUnlimited, txtMaxBalance);

            //JavascriptHelper.AttachEnableDisablingJSCode(rdbIsEncashable, divEncashment.ClientID, false);
            //JavascriptHelper.AttachPopUpCode(Page, "openingBalancePopup", "LeaveOpeningBalAsPopup.aspx", 650, 500);
            //JavascriptHelper.AttachEnableDisablingJSCode(rdbIsResetLeaveOn, chkResetMonths.ClientID, false);
            //reg javascript code setting url for leave opening balace popup
            //if (this.Id != 0)
            //{
            //    string code = "var queryString = 'EId={0}&LId={1}'";
            //    code = string.Format(code, eId, this.Id);
            //    ClientScript.RegisterClientScriptBlock(this.GetType(), "sdfdsfsdf1342", code, true);
            //}
        }

        void Initialise()
        {

            // upMaxEnashBalance.DefaultValue = "";

            ddlLapseCarryForwardMonth.DataSource = (DateManager.GetCurrentMonthList());
            ddlLapseCarryForwardMonth.DataBind();

            ddlGroupName.DataSource = LeaveAttendanceManager.GetLeaveGroupList();
            ddlGroupName.DataBind();

            BizHelper.Load(new LeaveAccrue(), ddlAccrue);
            BizHelper.Load(new UnusedLeave(), ddlUnusedLeave);
            BizHelper.Load(new HalfDayLeave(), ddlAllowHalfDay);
            ddlAllowHalfDay.ClearSelection();
            ddlAllowHalfDay.Items[2].Selected = true;
            BizHelper.Load(new LeaveType(), ddlLeaveType);
            //BizHelper.Load(new LeaveAccrueFrom(), ddlAccrueFrom);         
            //BizHelper.Load(new LeaveEncashment(), ddlEncashment);
            // ddlEncashmentMonth.Items.Clear();
            //ddlEncashmentMonth.DataSource = (DateManager.GetCurrentMonthList());
            //ddlEncashmentMonth.DataTextField = "Value";
            //ddlEncashmentMonth.DataValueField = "Key";
            //ddlEncashmentMonth.DataBind();
            ////ddlEncashmentMonth.Items.Insert(0, "-- Select Month --");
            //ListItem item = new ListItem("Retirement", "13".ToString());
            //ddlEncashmentMonth.Items.Add(item);

            JobStatus obJs = new JobStatus();
            //cblAppliesTo.Items.Clear();
            //cblAppliesTo.DataSource = obJs.GetMembers();
            //cblAppliesTo.DataTextField = "Value";
            //cblAppliesTo.DataValueField = "Key";
            //cblAppliesTo.DataBind();
            ////Remove first All Employees option
            //cblAppliesTo.Items.RemoveAt(0);

            ddlAppliesTo.DataSource = obJs.GetMembersForHirarchyView();
            ddlAppliesTo.DataBind();

            List<KeyValue> list = obJs.GetMembers();
            list.RemoveAt(0);
            chkStatusList.DataSource = list;
            chkStatusList.DataBind();

            if (CommonManager.Setting.ExcludeStatusHierarchyInLeave != null && CommonManager.Setting.ExcludeStatusHierarchyInLeave.Value)
            {
                RequiredFieldValidator5.Enabled = false;
                ddlAppliesTo.Visible = false;
            }
            else
            {
                divStatusList.Visible = false;
                chkStatusList.Visible = false;
            }

            if (CommonManager.CompanySetting.WhichCompany == WhichCompany.Oxfam)
                rowBasedOnServicePeriod.Style["display"] = "";

            //chkEnableContractRenewalSetInStatusList.Visible = 
            //    CommonManager.CompanySetting.WhichCompany == WhichCompany.Prabhu ||
            //     CommonManager.CompanySetting.WhichCompany == WhichCompany.Neco;

            //cblLapseOn.Items.Clear();
            cblLapseOn.DataSource = (DateManager.GetCurrentMonthList());
            cblLapseOn.DataTextField = "Value";
            cblLapseOn.DataValueField = "Key";
            cblLapseOn.DataBind();

            PayManager payMgr = new PayManager();
            List<GetIncomeForIncomesResult> incomeList = payMgr.GetIncomeListForIncome(
               SessionManager.CurrentCompanyId, 0);

            incomeList.Add(new GetIncomeForIncomesResult { Title="PF", IncomeId = 0 });
            chkEncashmentIncomeList.DataSource = incomeList.OrderBy(x => x.Title).ToList();
            chkEncashmentIncomeList.DataBind();





            int leaveTypeId = UrlHelper.GetIdFromQueryString("Id");




            this.LeaveId = UrlHelper.GetIdFromQueryString("LId");

            if (this.LeaveId != 0)
            {
                leaveTypeId = this.LeaveId;
            }

            cblWorkDayInclude.DataSource = leaveMgr.GetWorkDayLeaves(SessionManager.CurrentCompany.CompanyId, leaveTypeId);
            cblWorkDayInclude.DataTextField = "Title";
            cblWorkDayInclude.DataValueField = "LeaveTypeId";
            cblWorkDayInclude.DataBind();


            if (this.LeaveId != 0)
            {
                this.PageModeList = PageModeList.Update;
            }
            else if (eId != 0)
            {
                this.PageModeList = PageModeList.UpdateEmployee;

                txtEmployee.Text = EmployeeManager.GetEmployeeById(eId).Name;

                LEmployeeLeave empLeave = leaveMgr.GetEmployeeLeave(leaveTypeId, eId);
                if (empLeave != null)
                    ProcessEmpLeave(empLeave);

                sectionEmployee.Style.Remove("display");
            }
            else
            {
                this.PageModeList = PageModeList.Insert;
                return;
            }



            if (leaveTypeId != 0)
            {
                LLeaveType entity = leaveMgr.GetLeaveType(leaveTypeId);
                if (entity != null)
                {
                    this.CustomId = leaveTypeId;

                    btnSave.Text = Resources.Messages.Update;

                    txtAbbreviation.Enabled = false;



                    Process(entity);


                }
            }


        }

        protected void ddlAccrue_SelectedIndexChanged(object sender, EventArgs e)
        {
            //rowLeavePerPeriod.Style.Add("display","none");
            //rowWorkdayIncludesLeaves.Style.Add("display", "none");
            //rowWorkDaysPerPeriod.Style.Add("display", "none");

            //if( ddlAccrue.SelectedValue==LeaveAccrue.MONTHLY ||
            //    ddlAccrue.SelectedValue == LeaveAccrue.YEARLY)
            //{
            //    rowLeavePerPeriod.Style.Remove("display");
            //}
            //if (ddlAccrue.SelectedValue==LeaveAccrue.Based_On_WorkDays )
            //{
            //    rowLeavePerPeriod.Style.Remove("display");
            //    rowWorkdayIncludesLeaves.Style.Remove("display");
            //    rowWorkDaysPerPeriod.Style.Remove("display");
            //}
            //else
            //{
            //    cblWorkDayInclude.Visible = false;
            //    lblWI.Visible = false;
            //    lblreqWI.Visible = false;
            //}
        }

        LLeaveType Process(LLeaveType entity)
        {
            if (entity != null)
            {
                hdEditSequence.Value = entity.EditSequence.ToString();
                txtTitle.Text = entity.Title;
                txtAbbreviation.Text = entity.Abbreviation;

                // leave type 
                
                if (entity.IsParentGroupLeave != null && entity.IsParentGroupLeave.Value)
                    this.LeaveType = LeaveTypeEnum.ParentGroup;
                else if (entity.IsChildLeave != null && entity.IsChildLeave.Value)
                {
                    ddlGroupName.Enabled = false;
                    if (entity.ParentLeaveTypeId != null)
                        UIHelper.SetSelectedInDropDown(ddlGroupName, entity.ParentLeaveTypeId.ToString());
                    this.LeaveType = LeaveTypeEnum.Child;
                }
                else
                {
                    this.LeaveType = LeaveTypeEnum.Normal;
                }

                if (entity.AppliesToGender != null)
                    UIHelper.SetSelectedInDropDown(ddlAppliesToGender, entity.AppliesToGender);
                if (string.IsNullOrEmpty(entity.LegendColor) == false)
                    colorPicker.Color = entity.LegendColor;
                UIHelper.SetSelectedInDropDown(ddlAccrue, entity.FreqOfAccrual);
                UIHelper.SetSelectedInDropDown(ddlLeaveType, entity.Type);

                if (entity.SkipProportionateCalculation != null)
                    chkSkipProportionatelyCalculation.Checked = entity.SkipProportionateCalculation.Value;
                else
                    chkSkipProportionatelyCalculation.Checked = false;

                #region Set Accure display Settings

                rowLeavePerPeriod.Style.Add("display", "none");
                rowWorkdayIncludesLeaves.Style.Add("display", "none");
                rowWorkDaysPerPeriod.Style.Add("display", "none");

               
                if (ddlAccrue.SelectedValue == LeaveAccrue.Based_On_WorkDays)
                {
                    upDownWorkDayPeriod.Value = entity.WorkDaysPerPeriod.Value;
                    foreach (LLeaveWorkDay obLWD in entity.LLeaveWorkDays)
                    {
                        ListItem item = cblWorkDayInclude.Items.FindByValue(obLWD.OtherLeaveId.ToString());
                        if (item != null)
                            item.Selected = true;
                    }

                }
                #endregion
                
                upDownLeavePerPeriod.Value = entity.LeavePerPeriod.Value;
                if (entity.ManualLeaveShowInLeaveRequest == null)
                    chkManualLeaveShowInLeaveRequest.Checked = false;
                else
                    chkManualLeaveShowInLeaveRequest.Checked = entity.ManualLeaveShowInLeaveRequest.Value;

                chkStatusList.ClearSelection();
                foreach (LLeaveAppliesTo obLAT in entity.LLeaveAppliesTos)
                {
                    if (CommonManager.Setting.ExcludeStatusHierarchyInLeave != null && CommonManager.Setting.ExcludeStatusHierarchyInLeave.Value)
                    {
                        ListItem item = chkStatusList.Items.FindByValue(obLAT.AppliesTo.ToString());
                        if (item != null)
                            item.Selected = true;
                    }
                    else
                    {
                        ddlAppliesTo.ClearSelection();
                        ListItem item = ddlAppliesTo.Items.FindByValue(obLAT.AppliesTo.ToString());
                        if (item != null)
                            item.Selected = true;
                    }
                }

                if (ddlAccrue.SelectedValue == LeaveAccrue.COMPENSATORY)
                {
                    if (entity.CompensatoryOnHolidayFulldayValue != null)
                        upComensatoryFulldayValue.Text = entity.CompensatoryOnHolidayFulldayValue.ToString();
                    else
                        upComensatoryFulldayValue.Text = "1";

                    if (entity.CompensatoryOnHolidayHalfdayValue != null)
                        upCompensatoryHalfdayValue.Text = entity.CompensatoryOnHolidayHalfdayValue.ToString();
                    else
                        upCompensatoryHalfdayValue.Text = "0.5";
                }

                if (entity.IsHalfDayAllowed.Value)
                    ddlAllowHalfDay.SelectedIndex = 1;
                else
                    ddlAllowHalfDay.SelectedIndex = 2;

                if (entity.CountHolidayAsLeave != null)
                {
                    ddlCountHolidayAsLeave.SelectedValue = entity.CountHolidayAsLeave.Value.ToString().ToLower();

                    if (entity.DoNotCountWeeklySaturdayAsLeave != null)
                        chkDoNotCountWeeklySaturdayAsLeave.Checked = entity.DoNotCountWeeklySaturdayAsLeave.Value;

                    if (entity.DoNotCountPublicHolidayAsLeave != null)
                        chkDoNotCountPublicHolidayAsLeave.Checked = entity.DoNotCountPublicHolidayAsLeave.Value;

                }
                if (entity.AllowNegativeLeaveRequest != null)
                    ddlAllowNegativeBalance.SelectedValue = entity.AllowNegativeLeaveRequest.Value.ToString().ToLower();
                else
                    ddlAllowNegativeBalance.SelectedValue = "false";

                if (entity.PastDaysForLeaveRequest != null)
                {
                    txtAllowablePastDays.Text = entity.PastDaysForLeaveRequest.ToString();
                }
                if (entity.FutureDaysForLeaveRequest != null)
                    txtAllowableFutureDays.Text = entity.FutureDaysForLeaveRequest.ToString();

                IncomeDeductionUIHelper.SetSelectedIncomes(entity, chkEncashmentIncomeList);
                if (ddlAccrue.SelectedValue == LeaveAccrue.MANUALLY)
                {
                    if (entity.MannualMaximumPeriod != null)
                        upDownMannualMaxPeriod.Value = entity.MannualMaximumPeriod.Value;
                }

                // service period based process
                if (entity.LeavePerPeriodBasedOnServiceYear != null)
                {
                    chkBasedOnServicePeriod.Checked = entity.LeavePerPeriodBasedOnServiceYear.Value;

                   
                    if (chkBasedOnServicePeriod.Checked)
                    {
                        gvwServicePeriods.DataSource = entity.LLeaveServicePeriods.OrderBy(x=>x.PeriodUptoDays).ToList();
                        gvwServicePeriods.DataBind();

                        if (entity.ServicePeriodTypeDeductPrevYearAccural != null)
                            chkAccuralDeductingPrevYearAccural.Checked = entity.ServicePeriodTypeDeductPrevYearAccural.Value;

                        if (entity.CarryForwardCustomLapseMonth != null)
                            UIHelper.SetSelectedInDropDown(ddlLapseCarryForwardMonth, entity.CarryForwardCustomLapseMonth.Value);

                    }

                    chkBasedOnServicePeriod_Click(null, null);
                }


                if (ddlAccrue.SelectedValue != LeaveAccrue.MANUALLY && ddlAccrue.SelectedValue != LeaveAccrue.COMPENSATORY)
                {
                    UIHelper.SetSelectedInDropDown(ddlUnusedLeave, entity.UnusedLeave);

                    DisplayUnusedLeavePanel();
                    switch (ddlUnusedLeave.SelectedItem.Value.ToUpper())
                    {
                        case "LAPSE":
                            {
                                foreach (LLeaveLapse obLL in entity.LLeaveLapses)
                                {
                                    ListItem item = cblLapseOn.Items.FindByValue(obLL.LapseOn.ToString());
                                    if (item != null)
                                        item.Selected = true;
                                }

                                if (entity.ProcessForContractStatusWithAccuralInRenew != null)
                                    chkEnableContractRenewalSetInStatusList.Checked = entity.ProcessForContractStatusWithAccuralInRenew.Value;

                                break;
                            }
                        //case "ENCASE":
                        //    {
                        //        UIHelper.SetSelectedInDropDown(ddlEncashmentMonth, entity.EncaseOn);
                        //        break;
                        //    }
                        case "LAPSEENCASE":
                            {

                                upDownMaxBalance.Value = entity.MaximiumBalance.Value;
                                upDownYearlyMaxBalance.Value = entity.YearlyMaximumBalance.Value;
                                upDownEncase.Value = entity.EncaseDays.Value;

                                if (entity.FirstProriotyForEncashment != null)
                                    ddlFirstPriority.SelectedValue = entity.FirstProriotyForEncashment.ToString();

                                if (entity.MaxEncashOnRetirement.HasValue)
                                {
                                    //chkMaxEncashOnRetirement.Checked = true;
                                    upMaxEnashBalance.Value = entity.MaxEncashOnRetirement.Value;
                                }


                                break;
                            }
                    }
                }

            }
            else
            {
                entity = new LLeaveType();
                entity.LeaveTypeId = this.CustomId;
                entity.CompanyId = SessionManager.CurrentCompanyId;
                entity.EditSequence = 1;
                entity.Title = txtTitle.Text.Trim();
                entity.Abbreviation = txtAbbreviation.Text.Trim();
                entity.LegendColor = colorPicker.Color;
                entity.FreqOfAccrual = ddlAccrue.SelectedValue;

                // Leave Type
                switch (this.LeaveType)
                {
                    case LeaveTypeEnum.Normal:
                        entity.IsChildLeave = false;
                        entity.IsParentGroupLeave = false;
                        break;
                    case LeaveTypeEnum.ParentGroup:
                        entity.IsChildLeave = false;
                        entity.IsParentGroupLeave = true;
                        break;
                    case LeaveTypeEnum.Child:
                        entity.IsChildLeave = true;
                        entity.IsParentGroupLeave = false;
                        entity.ParentLeaveTypeId = int.Parse(ddlGroupName.SelectedValue);
                        break;
                }

                entity.SkipProportionateCalculation = chkSkipProportionatelyCalculation.Checked;

                if (ddlAppliesToGender.SelectedIndex != 0)
                    entity.AppliesToGender = int.Parse(ddlAppliesToGender.SelectedValue);

                entity.Type = ddlLeaveType.SelectedItem.Value;
               
                //if (entity.FreqOfAccrual != LeaveAccrue.COMPENSATORY)
                //{
                if (CommonManager.Setting.ExcludeStatusHierarchyInLeave != null && CommonManager.Setting.ExcludeStatusHierarchyInLeave.Value)
                {
                    foreach(ListItem item in chkStatusList.Items)
                    {
                        if(item.Selected)
                        {
                            LLeaveAppliesTo obLAT = new LLeaveAppliesTo();
                            obLAT.LeaveId = this.CustomId;
                            obLAT.AppliesTo = int.Parse(item.Value);
                            entity.LLeaveAppliesTos.Add(obLAT);
                        }
                    }
                }
                else
                {
                    LLeaveAppliesTo obLAT = new LLeaveAppliesTo();
                    obLAT.LeaveId = this.CustomId;
                    obLAT.AppliesTo = int.Parse(ddlAppliesTo.SelectedValue);
                    entity.LLeaveAppliesTos.Add(obLAT);
                }
                //}
                //else //for Compensatory LeaveType=Fully Paid & Applies To=All Employees always
                //{
                //    entity.Type = LeaveType.FULLYPAID;
                //    LLeaveAppliesTo obLAT = new LLeaveAppliesTo();
                //    obLAT.LeaveId = this.CustomId;
                //    obLAT.AppliesTo = (int)JobStatusEnum.Trainee ; // Applies to all status Employees
                //    entity.LLeaveAppliesTos.Add(obLAT);
                //}
                
                switch (entity.FreqOfAccrual)
                {
                    case LeaveAccrue.MONTHLY:
                    case LeaveAccrue.YEARLY:
                    case LeaveAccrue.HALFYEARLY:
                        entity.LeavePerPeriod = upDownLeavePerPeriod.Value;
                        break;
                    case LeaveAccrue.MANUALLY:
                        entity.LeavePerPeriod = upDownLeavePerPeriod.Value;
                        entity.ManualLeaveShowInLeaveRequest=chkManualLeaveShowInLeaveRequest.Checked;
                        entity.MannualMaximumPeriod = upDownMannualMaxPeriod.Value;
                        break;
                    
                    case LeaveAccrue.COMPENSATORY:
                        entity.ManualLeaveShowInLeaveRequest = chkManualLeaveShowInLeaveRequest.Checked;
                        entity.LeavePerPeriod = 0;
                        entity.CompensatoryOnHolidayFulldayValue = double.Parse(upComensatoryFulldayValue.Text);
                        entity.CompensatoryOnHolidayHalfdayValue = double.Parse(upCompensatoryHalfdayValue.Text);
                        break;
                    case LeaveAccrue.Based_On_WorkDays:
                        foreach (ListItem item in cblWorkDayInclude.Items)
                        {
                            if (item.Selected)
                            {
                                LLeaveWorkDay obLWD = new LLeaveWorkDay();
                                obLWD.LeaveId = this.CustomId;
                                obLWD.OtherLeaveId = int.Parse(item.Value);
                                obLWD.LLeaveType = entity;
                                entity.LLeaveWorkDays.Add(obLWD);
                            }
                        }
                        entity.WorkDaysPerPeriod = (int)upDownWorkDayPeriod.Value;
                        entity.LeavePerPeriod = upDownLeavePerPeriod.Value;
                        break;
                }


                if (ddlCountHolidayAsLeave.SelectedIndex != 0)
                {
                    entity.CountHolidayAsLeave = bool.Parse(ddlCountHolidayAsLeave.SelectedValue);
                    entity.DoNotCountWeeklySaturdayAsLeave = chkDoNotCountWeeklySaturdayAsLeave.Checked;
                    entity.DoNotCountPublicHolidayAsLeave = chkDoNotCountPublicHolidayAsLeave.Checked;
                }
                if (ddlAllowNegativeBalance.SelectedIndex != 0)
                {
                    entity.AllowNegativeLeaveRequest = bool.Parse(ddlAllowNegativeBalance.SelectedValue);
                }
                if (!string.IsNullOrEmpty(txtAllowablePastDays.Text.Trim()))
                {
                    entity.PastDaysForLeaveRequest = int.Parse(txtAllowablePastDays.Text.Trim());
                }
                if (!string.IsNullOrEmpty(txtAllowableFutureDays.Text.Trim()))
                {
                    entity.FutureDaysForLeaveRequest = int.Parse(txtAllowableFutureDays.Text.Trim());
                }

                // service period case
                entity.LeavePerPeriodBasedOnServiceYear = chkBasedOnServicePeriod.Checked;
               
                if (entity.LeavePerPeriodBasedOnServiceYear.Value)
                {
                    entity.ServicePeriodTypeDeductPrevYearAccural = chkAccuralDeductingPrevYearAccural.Checked;
                    entity.CarryForwardCustomLapseMonth = int.Parse(ddlLapseCarryForwardMonth.SelectedItem.Value);
                    entity.LLeaveServicePeriods.AddRange(GetGridList());
                }

                //Initially it was disable half day leave for Manual entered, but now enabled for NSET
                //for manually not allowed
                //if (ddlAccrue.SelectedValue == LeaveAccrue.MANUALLY)
                //    entity.IsHalfDayAllowed = false;
                //else
                    entity.IsHalfDayAllowed = ddlAllowHalfDay.SelectedValue == HalfDayLeave.ALLOWED ? true : false;

                if (ddlAccrue.SelectedValue != LeaveAccrue.MANUALLY && ddlAccrue.SelectedValue != LeaveAccrue.COMPENSATORY)
                {
                    entity.UnusedLeave = ddlUnusedLeave.SelectedValue;
                    switch (entity.UnusedLeave)
                    {
                        case UnusedLeave.LAPSE:
                            {
                                foreach (ListItem item in cblLapseOn.Items)
                                {
                                    if (item.Selected)
                                    {
                                        LLeaveLapse obLL = new LLeaveLapse();
                                        obLL.LeaveId = this.CustomId;
                                        obLL.LapseOn = int.Parse(item.Value);
                                        entity.LLeaveLapses.Add(obLL);
                                    }
                                }
                                entity.ProcessForContractStatusWithAccuralInRenew = chkEnableContractRenewalSetInStatusList.Checked;
                                IncomeDeductionUIHelper.AddSelectedIncomes(entity, chkEncashmentIncomeList);
                                break;
                            }
                        case UnusedLeave.ENCASE:
                            {
                                IncomeDeductionUIHelper.AddSelectedIncomes(entity, chkEncashmentIncomeList);
                                break;
                            }
                        case UnusedLeave.LAPSEENCASE:
                            {
                                entity.MaximiumBalance = (int)upDownMaxBalance.Value;
                                entity.YearlyMaximumBalance = (int)upDownYearlyMaxBalance.Value;
                                entity.EncaseDays = (int)upDownEncase.Value;
                                entity.FirstProriotyForEncashment = int.Parse(ddlFirstPriority.SelectedValue);
                                //if (chkMaxEncashOnRetirement.Checked)
                                {
                                    entity.MaxEncashOnRetirement = (int)upMaxEnashBalance.Value;
                                }


                                IncomeDeductionUIHelper.AddSelectedIncomes(entity, chkEncashmentIncomeList);

                                break;
                            }
                    }
                }
                return entity;
            }
            return null;
        }

        public  void EnableDisableValidators(bool isEnable,string unusedLeave)
        {
            

            if (isEnable)
            {
                EnabledChildValidationControls(this);
                EnabledChildValidationControls(rowUnusedLeave);
                EnabledChildValidationControls(rowUnusedLeaveDetails);
                EnabledChildValidationControls(rowHalfDay);
            }
            else
            {
                if (ddlAccrue.SelectedValue == LeaveAccrue.MANUALLY || ddlAccrue.SelectedValue==LeaveAccrue.COMPENSATORY)
                {
                    DisableChildValidationControls(rowUnusedLeaveDetails);
                    DisableChildValidationControls(rowUnusedLeave);
                    DisableChildValidationControls(rowHalfDay);
                }               

                switch (unusedLeave)
                {
                    case UnusedLeave.ENCASE:
                        //reqdEncaseLapse.Enabled = false;
                        //reqdCheckboxlist1.Enabled = false;
                        break;
                    case UnusedLeave.LAPSE:
                        //reqdEncashmentMonth.Enabled = false;
                        //reqdEncaseLapse.Enabled = false;
                        break;
                    case UnusedLeave.LAPSEENCASE:
                        //reqdEncashmentMonth.Enabled = false;
                        //reqdCheckboxlist1.Enabled = false;
                        break;
                }
            }
        }


        public bool ValidateAbbreviationAgainstHolidayConstant()
        {
            string abbr = txtAbbreviation.Text.Trim().ToLower();
            if (abbr == HolidaysConstant.Caste_Holiday.ToLower()
                || abbr == HolidaysConstant.Female_Holiday.ToLower()
                || abbr == HolidaysConstant.National_Holiday.ToLower()
                || abbr == HolidaysConstant.Weekly_Holiday.ToLower()
                || abbr == "p" //Present
                || abbr=="upl" //Unpaid Leave
                || abbr == "wh" //Weekly holiday
                || abbr=="abs")//Absent
                return false;
            return true;
        }

        public bool ValidateAbbreviationForSpecialCharacters()
        {
            string abbr = txtAbbreviation.Text.Trim();
            if (Regex.IsMatch(abbr, "^[A-Z]*$"))
                return true;
            return false;
        }

       

        protected void btnOk_Click(object sender, EventArgs e)
        {

            string invalidText = Util.HasInvalidCharacterForXMLParsingAndImport(txtTitle.Text.Trim());

            if (!string.IsNullOrEmpty(invalidText))
            {
                divWarningMsg.InnerHtml = "\"" + invalidText + "\" character can not be set in the title.";
                divWarningMsg.Hide = false;
                txtTitle.Focus();
                return;
            }


            if (txtTitle.Text.Trim().ToLower() == LeaveAttendanceManager.Unpaid_Leave.ToLower())
            {
                divWarningMsg.InnerHtml = "Title can not be \"Unpaid Leave\".";
                divWarningMsg.Hide = false;
                txtTitle.Focus();
                return;
            }

            if (ValidateAbbreviationAgainstHolidayConstant() == false)
            {
                divWarningMsg.InnerHtml = Resources.Messages.LeaveAbbrConflictedWithHoliday;
                divWarningMsg.Hide = false;
                //JavascriptHelper.DisplayClientMsg("Leave abbreviation conflicted with holidays.", Page);
                txtAbbreviation.Focus();
                return;
            }
            if (ValidateAbbreviationForSpecialCharacters() == false)
            {
                divWarningMsg.InnerHtml = Resources.Messages.LeaveAbbrContainingInvalidCharacters;
                divWarningMsg.Hide = false;
                //JavascriptHelper.DisplayClientMsg("Leave abbreviation contains special or small characters.", Page);
                txtAbbreviation.Focus();
                return;
            }
            if (txtAbbreviation.Text.Trim().Length > 6)
            {
                divWarningMsg.InnerHtml = Resources.Messages.LeaveAbbrCharacterLimit;
                divWarningMsg.Hide = false;
                //JavascriptHelper.DisplayClientMsg("Leave abbreviation contains special or small characters.", Page);
                txtAbbreviation.Focus();
                return;
            }

           
            EnableDisableValidators(false, ddlUnusedLeave.SelectedValue);
            Page.Validate("Leave");


            if (Page.IsValid)
            {
                LLeaveType leave = Process(null);

                switch (this.PageModeList)
                {
                    case PageModeList.Insert:
                        int result = leaveMgr.Save(leave);
                        int? EditSequence = 0;

                        switch (result)
                        {
                            case -2:
                                divWarningMsg.InnerHtml = Resources.Messages.LeaveTitleAlreadyExists;
                                divWarningMsg.Hide = false;
                                //JavascriptHelper.DisplayClientMsg("Leave title already exists.", Page);
                                txtTitle.Focus();
                                break;
                            case -1:
                                divWarningMsg.InnerHtml = Resources.Messages.LeaveAbbrAlreadyExists;
                                divWarningMsg.Hide = false;
                                //JavascriptHelper.DisplayClientMsg("Leave abbreviation already exists.", Page);
                                txtAbbreviation.Focus();
                                break;
                            case 1:
                                JavascriptHelper.DisplayClientMsg("Leave saved.", Page, "closePopup();");
                                break;
                        }
                        break;

                    case PageModeList.UpdateEmployee:
                        leave.LeaveTypeId = this.CustomId;
                        //for LEmployeeLeave
                        LEmployeeLeave empLeave = ProcessEmpLeave(null);

                        empLeave.EmployeeId = eId;
                        empLeave.LeaveTypeId = this.CustomId;
                        empLeave.LLeaveType = leave;
                        EditSequence = leaveMgr.GetEmployeeLeave(empLeave.LeaveTypeId, empLeave.EmployeeId).EditSequence;
                        EditSequence = EditSequence == null ? 0 : EditSequence;
                        if (EditSequence != int.Parse(hdEmployeeEditSequence.Value == "" ? "0" : hdEmployeeEditSequence.Value))
                        {
                            divWarningMsg.InnerHtml = Resources.Messages.ConcurrencyError;
                            divWarningMsg.Hide = false;
                            return;
                        }

                        leaveMgr.UpdateEmployeeLeave(empLeave,false);
                        //if Manual leave & Clear balance is checked then clear the last balance

                        //if (!txtBeginningBalance.Text.Equals("0"))
                        //{
                        //    LeaveAttendanceManager.TryToClearManualLeaveBalance(empLeave, chkClearBalance.Checked);
                        //}

                        JavascriptHelper.DisplayClientMsg("Leave updated.", Page, "closePopup();");
                        break;

                    case PageModeList.Update:
                        LLeaveType leave1;
                        leave1 =  Process(null);
                        leave1.LeaveTypeId = this.LeaveId;
                        leave1.EditSequence = int.Parse(hdEditSequence.Value == "" ? "0" : hdEditSequence.Value)+1;
                        EditSequence = leaveMgr.GetLeaveType(leave.LeaveTypeId).EditSequence;
                        EditSequence = EditSequence == null ? 0 : EditSequence;
                        if (EditSequence != int.Parse(hdEditSequence.Value == "" ? "0" : hdEditSequence.Value))
                        {
                            divWarningMsg.InnerHtml = Resources.Messages.ConcurrencyError;
                            divWarningMsg.Hide = false;
                            return;
                        }
                        leaveMgr.Update(leave1);                        
                        JavascriptHelper.DisplayClientMsg("Leave updated.", Page, "closePopup();");
                        break;
                }
               
                    
                
            }

            EnableDisableValidators(true, null);
            //valReqdEncashmentType.Enabled = true;

            //valReqdEarnedValue.Enabled = true;
            //valCompEarnedValue.Enabled = true;
            //reqdValPartialSalPercent.Enabled = true;
            //EnabledChildControls(sectionEmployee);
        }

        LEmployeeLeave ProcessEmpLeave(LEmployeeLeave leave)
        {
            if (leave == null)
            {
                leave = new LEmployeeLeave();
                string accuredText = txtAccured.Text.Trim();
                float accured;
                if (float.TryParse(accuredText, out accured))
                    leave.Accured = accured;
                else
                    leave.Accured = 0;
                leave.EditSequence = int.Parse(hdEmployeeEditSequence.Value == "" ? "0" : hdEmployeeEditSequence.Value) + 1;

                leave.ApplyLeaveWithOutCheckingStatus = bool.Parse(ddlApplyLeaveWithOutCheckingStatus.SelectedValue);

                return leave;
            }
            else
            {
                //txtBeginningBalance.Text = leave.BeginningBalance.ToString();
                txtBeginningBalance.Text = LeaveAttendanceManager.GetBeginningBalance(
                    leave.EmployeeId, leave.LeaveTypeId).ToString();
                txtAccured.Text = leave.Accured.ToString();
                hdEmployeeEditSequence.Value = leave.EditSequence.ToString();
                if (leave.ApplyLeaveWithOutCheckingStatus != null)
                {
                    UIHelper.SetSelectedInDropDown(ddlApplyLeaveWithOutCheckingStatus, leave.ApplyLeaveWithOutCheckingStatus.ToString().ToLower());
                }
            }
            return null;
        }


        protected void Page_PreRender(object sender, EventArgs e)
        {
            

            ShowHideNotEditableMsg();

            txtAccured.Enabled = false;
            //upMaxEnashBalance.TextBox.Enabled = chkMaxEncashOnRetirement.Checked;

            rowLeavePerPeriod.Style.Add("display", "none");
            rowWorkdayIncludesLeaves.Style.Add("display", "none");
            rowWorkDaysPerPeriod.Style.Add("display", "none");
            rowCompensatoryFulldayValue.Style["display"] = "none";
            rowCompensatoryHalfdayValue.Style["display"] = "none";
            rowProportionatelyCalculation.Style["display"] = "none";
            rowMannualMaxPeriod.Style["display"] = "none";
            //if (ddlAccrue.SelectedValue == LeaveAccrue.MONTHLY || ddlAccrue.SelectedValue == LeaveAccrue.YEARLY)
            //{
            //    rowProportionatelyCalculation.Style["display"] = "";
            //}
            if (ddlAccrue.SelectedValue == LeaveAccrue.MANUALLY || ddlAccrue.SelectedValue==LeaveAccrue.COMPENSATORY) 
            {
                //hide clear balance leave for other than Manual entered leave
                if (ddlAccrue.SelectedValue == LeaveAccrue.MANUALLY)
                {
                    //divClearBalance.Style.Remove("display");
                    rowMannualMaxPeriod.Style.Remove("display");
                    rowLeavePerPeriod.Style.Add("display", "none");
                }
                else if (ddlAccrue.SelectedValue == LeaveAccrue.COMPENSATORY)
                {
                    //ddlLeaveType.Enabled = false;
                    //ddlAppliesTo.Enabled = false;

                    rowCompensatoryFulldayValue.Style["display"] = "";
                    rowCompensatoryHalfdayValue.Style["display"] = "";
                }
                if (ddlAccrue.SelectedValue == LeaveAccrue.MANUALLY)
                {
                    //rowHalfDay.Style["display"] = "none";
                }
            }
            else
            {
                //divClearBalance.Style["display"] = "none";
                rowHalfDay.Style.Remove("display");
            }
            ShowHideAccured();
            DisplayUnusedLeavePanel();
            EnableDisableControls();

            if (ddlCountHolidayAsLeave.SelectedValue.ToLower() == "true")
            {
                rowHolidayDoNotCountWeeklySaturdayAsLeave.Style["display"] = "";
                rowHolidayDoNotCountPublicHolidayAsLeave.Style["display"] = "";
            }
            else
            {
                rowHolidayDoNotCountWeeklySaturdayAsLeave.Style["display"] = "none";
                rowHolidayDoNotCountPublicHolidayAsLeave.Style["display"] = "none";
            }

            //if houly leave company, then set as half day/hourly & disable it
            if (CommonManager.CalculationConstant.CompanyHasHourlyLeave.Value)
            {
                ddlAllowHalfDay.ClearSelection();
                ddlAllowHalfDay.SelectedIndex = 1;
                ddlAllowHalfDay.Enabled = false;
                lblHalfDay.Text = Resources.Messages.HourlyLeaveText;
            }

            EnabledChildControls(upMaxEnashBalance);

            // Leave,Group,Child case
            if (!IsPostBack)
            {
                if (this.LeaveType == LeaveTypeEnum.ParentGroup)
                {
                    rowLeavePerPeriod.Visible = false;

                    this.leaveTitle.InnerText = "Group Name";
                    upDownLeavePerPeriod.Value = 0;

                    ddlUnusedLeave.SelectedIndex = 2; //Encash
                    rowUnusedLeave.Visible = false;

                    //lr1.Visible = false;
                    //lr2.Visible = false;
                    //lr3.Visible = false;
                    //lr4.Visible = false;

                    pnlEncashmentIncomes.Visible = false;
                }

                if (this.LeaveType == LeaveTypeEnum.Child)
                {
                    rowGroupName.Visible = true;
                    ddlAllowHalfDay.Enabled = false;
                    colorPicker.Enabled = false;
                    ddlLeaveType.Enabled = false;
                    //ddlAppliesTo.Enabled = false;
                    ddlAppliesToGender.Enabled = false;

                    ddlAccrue.Enabled = false;
                    
                }
                else
                    rowGroupName.Visible = false;
            }
        }

        private void EnableDisableControls()
        {
            //check if leave used in the attendance or appear in leave adjustment then disable some fields
            if (this.CustomId != 0 && LeaveAttendanceManager.IsLeaveUsedInAttendance(this.CustomId))
            {
                //this.ddlLeaveType.Enabled = false;
                //this.ddlAccrue.Enabled = false;
                //this.ddlUnusedLeave.Enabled = false;
               // this.ddlAllowHalfDay.Enabled = false;
                //DisableChildControls(pnlLapse);
                //DisableChildControls(pnlEncase);
               // DisableChildControls(pnlLapseEncase);
            }
        }

        private void ShowHideAccured()
        {
            switch (ddlAccrue.SelectedValue)
            {
                case LeaveAccrue.MONTHLY:
                case LeaveAccrue.YEARLY:
                case LeaveAccrue.HALFYEARLY:
                
                    rowLeavePerPeriod.Style.Remove("display");
                    break;
                case LeaveAccrue.Based_On_WorkDays:
                    rowLeavePerPeriod.Style.Remove("display");
                    rowWorkdayIncludesLeaves.Style.Remove("display");
                    rowWorkDaysPerPeriod.Style.Remove("display");
                    break;
                case LeaveAccrue.MANUALLY:
                    //rowLeavePerPeriod.Style.Remove("display");
                   // rowManualLeaveShowInLeaveRequest.Style.Remove("display");
                    //txtAccured.Enabled = true;
                    break;
                case LeaveAccrue.COMPENSATORY:
                    //rowLeavePerPeriod.Style.Remove("display");
                    rowManualLeaveShowInLeaveRequest.Style.Remove("display");
                    //txtAccured.Enabled = true;
                    break;
            }
        }

        //protected void ddlUnusedLeave_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    DisplayUnusedLeavePanel();
        //}

        void DisplayUnusedLeavePanel()
        {
            if (ddlAccrue.SelectedValue == LeaveAccrue.MANUALLY || ddlAccrue.SelectedValue==LeaveAccrue.COMPENSATORY)
            {
                rowUnusedLeave.Style["display"] = "none";
                rowUnusedLeaveDetails.Style["display"] = "none";
            }
            else
            {
                rowUnusedLeave.Style.Remove("display");
                rowUnusedLeaveDetails.Style.Remove("display");


                pnlLapse.Style.Add("display", "none");
                //pnlEncase.Style.Add("display", "none");
                pnlLapseEncase.Style.Add("display", "none");
                pnlEncashmentIncomes.Style.Add("display", "none");
                tdUnusedLeave.Style.Add("display", "none");
                switch (ddlUnusedLeave.SelectedValue)
                {
                    case UnusedLeave.LAPSE:
                        {
                            pnlLapse.Style.Remove("display");
                            tdUnusedLeave.Style.Remove("display");
                            pnlEncashmentIncomes.Style.Remove("display");
                            break;
                        }
                    case UnusedLeave.ENCASE:
                        {
                            //pnlLapseEncase.Style.Remove("display");
                            pnlEncashmentIncomes.Style.Remove("display");
                            tdUnusedLeave.Style.Remove("display");
                            break;
                        }
                    case UnusedLeave.LAPSEENCASE:
                        {
                            pnlLapseEncase.Style.Remove("display");
                            pnlEncashmentIncomes.Style.Remove("display");
                            tdUnusedLeave.Style.Remove("display");

                            break;
                        }
                    default: break;
                }
            }
        }

        

    }
}
