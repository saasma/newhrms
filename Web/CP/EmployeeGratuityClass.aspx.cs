﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using Utils.Helper;
using Utils.Web;
using BLL.Base;
using DAL;

namespace Web
{
    public partial class EmployeeGratuityClass : BasePage
    {

        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.LeaveAndAttendance;
            }
        }
    
        private int _tempCurrentPage;
        private int _tempCount;
        List<GratuityClass> classList = new List<GratuityClass>();
        private PayManager payMgr = new PayManager();
        protected void Page_Load(object sender, EventArgs e)
        {
            classList = new CommonManager().GetAllGratuityClass(SessionManager.CurrentCompanyId);

            //RegisterEmployeeSavedMsg();
            if (!IsPostBack)
            {
                pagingCtl.CurrentPage = 1;
                BindEmployees();

            }
            if (Request.Form["__EVENTTARGET"] != null && Request.Form["__EVENTTARGET"].ToLower().Contains("refresh"))
                BindEmployees();
            JavascriptHelper.AttachNonDialogPopUpCode(Page, "taxImportPopup", "../ExcelWindow/DeviceExcel.aspx", 450, 500);

            JavascriptHelper.AttachNonDialogPopUpCode(Page, "empTime", "../ExcelWindow/EmployeeAttendanceTimeExcel.aspx", 450, 500);
            
        }

        protected void gvwRoles_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            //gvwRoles.PageIndex = e.NewPageIndex;
            //gvwRoles.SelectedIndex = -1;
            BindEmployees();
        }
        //void RegisterEmployeeSavedMsg()
        //{
        //    if (SessionManager.EmployeeSaved)
        //    {
        //        JavascriptHelper.DisplayClientMsgWithTimeout("Employee Information Updated.", Page);
        //        SessionManager.EmployeeSaved = false;
        //    }
        //}


        protected void gv_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if ((e.Row.RowState & DataControlRowState.Edit) > 0)
                {

                    int employeeId = (int)gvEmployeeIncome.DataKeys[e.Row.RowIndex]["EmployeeId"];

                    DropDownList ddList = (DropDownList)e.Row.FindControl("ddlGratuityClass");

                    ddList.DataSource = classList;
                    ddList.DataBind();

                    EHumanResource emp = LeaveRequestManager.GetGratuityClassForEmployee(employeeId);
                    if (emp != null && emp.GratuityClassRef_ID != null)
                    {
                        ListItem item = ddList.Items.FindByValue(emp.GratuityClassRef_ID.ToString());
                        if (item != null)
                            item.Selected = true;
                    }
                    else
                        ddList.SelectedIndex = 0;
                }
            }
        }

        protected void gvwProjects_RowEditing(object sender, GridViewEditEventArgs e)
        {
            gvEmployeeIncome.EditIndex = e.NewEditIndex;
            int employeeId = (int)gvEmployeeIncome.DataKeys[e.NewEditIndex]["EmployeeId"];


            BindEmployees();
            //DropDownList ddl = gvwProjects.Rows[e.NewEditIndex].FindControl("ddlProjectOrTeam1") as DropDownList;


        }

        protected void gvwProjects_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {


            int employeeId = (int)gvEmployeeIncome.DataKeys[e.RowIndex]["EmployeeId"];

            DropDownList ddList = gvEmployeeIncome.Rows[e.RowIndex].FindControl("ddlGratuityClass") as DropDownList;

            //if (ddList.SelectedItem == null)
            //{
            //    msgWarning.InnerHtml = "Team should be selected.";
            //    msgWarning.Hide = false;
            //    return;
            //}

         

            CommonManager.UpdateEmpGratuityClass(employeeId, int.Parse(ddList.SelectedValue));

            //if (LeaveRequestManager.SaveUpdateEmployeeProject(project))
            {

                gvEmployeeIncome.EditIndex = -1;
                BindEmployees();
            }

        }


        void BindEmployees()
        {

            int totalRecords = 0;
            gvEmployeeIncome.DataSource
                = TaxManager.GetEmployeeGratuityClassList(txtEmployeeName.Text.Trim(), pagingCtl.CurrentPage - 1, int.Parse(pagingCtl.DDLRecords.SelectedValue), ref totalRecords);
            gvEmployeeIncome.DataBind();
            pagingCtl.UpdatePagingBar(totalRecords);

           // btnUpdate.Visible = true;
           // btnExport.Visible = btnUpdate.Visible;
        }

        protected void btnPrevious_Click()
        {
            pagingCtl.CurrentPage -= 1;
            //_tempCurrentPage = int.Parse(pagintCtl.LabelCurrentPage.Text);
            //_tempCurrentPage -= 1;
            BindEmployees();
        }

        protected void btnNext_Click()
        {
            pagingCtl.CurrentPage += 1;
            //_tempCurrentPage = int.Parse(pagintCtl.LabelCurrentPage.Text);
            //_tempCurrentPage += 1;
            BindEmployees();
        }

        protected void ddlRecords_SelectedIndexChanged()
        {
            //SessionManager.EmployeeDisplayRecodsPerPage = int.Parse(pagintCtl.DDLRecords.SelectedValue);
            //_tempCurrentPage = 1;
            pagingCtl.CurrentPage = 1;
            BindEmployees();
        }
   


        protected void txtEmployeeName_TextChanged(object sender, EventArgs e)
        {
            _tempCurrentPage = 1;
            BindEmployees();

        }
    }
}
