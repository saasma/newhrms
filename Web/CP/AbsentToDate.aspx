<%@ Page Title="Absent Upto Date" MaintainScrollPositionOnPostback="true" EnableEventValidation="false"  ValidateRequest="false"  Language="C#" MasterPageFile="~/Master/ForPopupPage.Master"
    AutoEventWireup="true" CodeBehind="AbsentToDate.aspx.cs" Inherits="Web.CP.AbsentToDate" %>

<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../Scripts/jquery-1.3.2.min.js" type="text/javascript"></script>
   
    <style type="text/css">
        tr
        {
            padding-top: 10px;
        }
    </style>
    <script type="text/javascript">

        function closePopup() {



            window.opener.setAbsentDate(document.getElementById('<%=hdn.ClientID %>').value);
        }


        window.onunload = closePopup;   
        
    </script>
    <%--  <script src="../Scripts/jquery.sexy-combo.pack.js" type="text/javascript"></script>

   <script src="../Scripts/jquery.sexy-combo.min.js" type="tet/javascript"></script>

    <script src="../Scripts/jquery.sexy-combo.js" type="text/javascript"></script>--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
    <div class="popupHeader">
        <h3>
            Absent To Date</h3>
    </div>
    <asp:HiddenField ID="hdn" runat="server" />
    <div class=" marginal" style='margin-top: 15px'>
        
        <ext:ResourceManager runat="server" ViewStateMode="Enabled" />
        <uc2:MsgCtl ID="divMsgCtl" EnableViewState="false" Hide="true" runat="server" />
        <uc2:WarningCtl ID="divWarningMsg" EnableViewState="false" Width="370px" Hide="true"
            runat="server" />
        <pr:CalendarExtControl Width="180px" FieldLabel="Date *" ID="calHireDate" runat="server"
            LabelAlign="Top" LabelSeparator="">
        </pr:CalendarExtControl>
        <asp:Button ID="btnSave" Width="100px" OnClick="btnSave_Click" CssClass="save" runat="server"
            Text="Save" />
       
    </div>
</asp:Content>
