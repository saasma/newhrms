<%@ Page Title="Bonus" Language="C#" MasterPageFile="~/Master/Details.Master" AutoEventWireup="true"
    CodeBehind="Bonus.aspx.cs" Inherits="Web.CP.Bonus" %>

<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningMsgCtl" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/FilterCtl.ascx" TagName="FilterChanged" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .bonus h1
        {
            font-size: 14px !important;
            padding: 0 0 12px;
            margin-bottom: 10px;
        }
        .bonus h1, h2
        {
            color: #333333;
            padding: 0 0 0.25em;
        }
    </style>

    <script type="text/javascript">
        var chkAll = '#<%= chkAll.ClientID %>';
        function changeStates(chk) {
            var isChecked = $(chkAll).is(':checked');

            $("INPUT[type='checkbox']").attr('checked', isChecked);
        }

        function changeAdjustment(txt) {
            txt.getAttribute("idList");
        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
    <div class="contentArea bonus">
        <asp:MultiView ID="multiView" runat="server" ActiveViewIndex="0">
            <asp:View ID="view1" runat="server">
                <h1>
                    Select Bonus Year/Payroll Period</h1>
                <uc1:WarningMsgCtl ID="warning" Hide="true" runat="server" />
                
                <div>
                Payroll Period:
                <asp:Label ID="lblPayrollPeriod" runat="server" Text=""></asp:Label>
                </div>
            </asp:View>
            <asp:View ID="view2" runat="server">
                <h1>
                    Enter Total Amount of Bonus to be distributed</h1>
                    <div>
                <asp:TextBox ID="txtBonusAmount" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator SetFocusOnError="true" ControlToValidate="txtBonusAmount"
                    ID="RequiredFieldValidator1" Display="None" runat="server" ErrorMessage="Amount must be entered."></asp:RequiredFieldValidator>
                <asp:CompareValidator Type="Currency" Operator="GreaterThan" ValueToCompare="0" SetFocusOnError="true"
                    ID="CompareValidator1" ControlToValidate="txtBonusAmount" runat="server" Display="None"
                    ErrorMessage="Valid amount must be entered."></asp:CompareValidator>
                    </div>
            </asp:View>
            <asp:View ID="view3" runat="server">
                <h1>
                    Select the incomes to be included for Bonus Calculation</h1>
                <div class="attribute">
                    <span style="padding: 0pt 0pt 8px 2px; display: block;">
                        <asp:CheckBox ID="chkAll" onclick="changeStates(this)" runat="server" />
                        <b style="font-size: 16px;">Incomes</span>
                    <%--<div style="overflow-y: scroll; height: 110px; width: 95%; border: 1px solid #333">--%>
                    <asp:CheckBoxList ID="chkListSalaries" RepeatColumns="3" RepeatDirection="Horizontal"
                        runat="server" DataTextField="Title" DataValueField="IncomeId">
                    </asp:CheckBoxList>
                    <br />
                    <%--</div>--%>
                </div>
            </asp:View>
            <asp:View ID="view4" runat="server">
                <h1>
                    Enter the days an Employee must have worked to be eligible to receive bonus</h1>
                    
                <div>    
                <asp:TextBox ID="txtEmpEligibility" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator SetFocusOnError="true" ControlToValidate="txtEmpEligibility"
                    ID="RequiredFieldValidator2" Display="None" runat="server" ErrorMessage="Eligibility days must be entered."></asp:RequiredFieldValidator>
                <asp:CompareValidator Type="Double" Operator="GreaterThan" ValueToCompare="0" SetFocusOnError="true"
                    ID="CompareValidator2" ControlToValidate="txtEmpEligibility" runat="server" Display="None"
                    ErrorMessage="Invalid Eligibility days."></asp:CompareValidator>
                <uc1:FilterChanged OnFilterChanged="filter_Changed" ID="filter" runat="server" />
                <cc2:EmptyDisplayGridView Style='clear: both' CssClass="tableLightColor" UseAccessibleHeader="true"
                    ShowHeaderWhenEmpty="True" ID="gvwEligibleEmployees" runat="server" DataKeyNames="EmployeeId"
                    AutoGenerateColumns="False" CellPadding="4" GridLines="None" AllowSorting="True"
                    ShowFooterWhenEmpty="False">
                    <Columns>
                        <asp:BoundField HeaderStyle-Width="20px" DataFormatString="{0:000}" DataField="EmployeeId"
                            HeaderText="EIN"></asp:BoundField>
                        <asp:TemplateField HeaderText="Name" HeaderStyle-Width="150px">
                            <ItemTemplate>
                                <%# Eval("Name") %>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Left" Width="150px" />
                        </asp:TemplateField>
                        <asp:BoundField HeaderStyle-Width="20px" HeaderStyle-HorizontalAlign="Left" DataField="Branch"
                            HeaderText="Branch"></asp:BoundField>
                        <asp:BoundField HeaderStyle-Width="20px" HeaderStyle-HorizontalAlign="Left" DataField="Department"
                            HeaderText="Department"></asp:BoundField>
                        <asp:TemplateField HeaderStyle-Width="20px" ItemStyle-HorizontalAlign="Right" HeaderText="Actual Workdays">
                            <ItemTemplate>
                                <asp:Label ID="lblActualWorkdays" runat="server" Text='<%# Eval("ActualWorkdays") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="Adjustment" HeaderStyle-Width="100px">
                            <ItemTemplate>
                                <asp:TextBox CssClass='calculationInput' Style='text-align: right' ID="txtActualWorkdays"
                                    Text='<%# Eval("Adjustment") %>' onblur="changeAdjustment(this)" runat="server"></asp:TextBox>
                                <asp:CompareValidator SetFocusOnError="true" ValueToCompare="0" Display="None" Operator="GreaterThanEqual"
                                    Type="Double" ID="valOpeningBalance1" ControlToValidate="txtActualWorkdays" runat="server"
                                    ErrorMessage="Invalid workdays adjustment."></asp:CompareValidator>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="Adjusted For" HeaderStyle-Width="100px">
                            <ItemTemplate>
                                <asp:TextBox runat="server" TextMode="MultiLine" Height="20px" ID="txtAdjComment"
                                    Text='<%# Eval("AdjustmentComment") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                          <asp:TemplateField HeaderStyle-Width="20px" ItemStyle-HorizontalAlign="Right" HeaderText="Adjusted Workdays">
                            <ItemTemplate>
                                <asp:Label ID="lblAdjustedWorkdays" runat="server" Text='<%# Eval("AdjustedWorkdays") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                       <asp:TemplateField HeaderStyle-Width="20px" ItemStyle-HorizontalAlign="Right" HeaderText="Eligibility">
                            <ItemTemplate>
                                <asp:Label ID="lblEligibility" runat="server" Text='<%# Eval("Eligibility") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                       
                    </Columns>
                    <RowStyle CssClass="odd" />
                    <AlternatingRowStyle CssClass="even" />
                    <EmptyDataTemplate>
                        <b>No employees found. </b>
                    </EmptyDataTemplate>
                </cc2:EmptyDisplayGridView>
               
                <br />
                <br />
                <asp:Button ID="btnSaveWorkdays" runat="server" CssClass="save" Text="Save" />
                 </div>
            </asp:View>
        </asp:MultiView>
        <asp:ValidationSummary ShowSummary="false" ShowMessageBox="true" ID="ValidationSummary1"
            runat="server" />
        <div style="width: 500px;float:left" class="buttonsDiv" >
            <asp:Button Style="float: left" ID="btnPrevious" CausesValidation="false" Enabled="false"
                CssClass="update" Font-Bold="true" runat="server" Text="<< Previous" OnClick="btnPrevious_Click" />
            <asp:Button Style="float: right" ID="btnNext" CssClass="update" Font-Bold="true"
                runat="server" Text="Next>>" OnClick="btnNext_Click" />
        </div>
    </div>
</asp:Content>
