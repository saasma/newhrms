﻿<%@ Page Title="Salary Calculation" EnableEventValidation="false" MaintainScrollPositionOnPostback="true"
    Language="C#" MasterPageFile="~/Master/HROld.Master" AutoEventWireup="true" CodeBehind="Calculation.aspx.cs"
    Inherits="Web.CP.Calculation" %>

<%@ Register Src="../UserControls/Calculation.ascx" TagName="Calculation" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <uc1:Calculation Id="Calculation1" runat="server" />
     <br />
      <br />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="pageScriptsContent" runat="server">
    <script src="../Scripts/currency.js" type="text/javascript"></script>
    <%--<script src="../Scripts/ScrollTableHeader-v1.03.js" type="text/javascript"></script>--%>
    <script src="../Scripts/jquery-v_1.9.0.min.js"></script>
    <script src="../Scripts/jquery-ui-v_1.9.1.min.js"></script>
    <script src="../Scripts/gridviewScroll.min.js" type="text/javascript"></script>
   
</asp:Content>
