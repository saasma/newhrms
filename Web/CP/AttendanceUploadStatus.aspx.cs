﻿using System;
using System.Collections.Generic;
using System.Linq;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;

namespace Web.CP
{
    public partial class AttendanceUploadStatus : BasePage
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();
            }
        }

        public void Initialise()
        {

        }

        protected void Store_ReadData(object sender, StoreReadDataEventArgs e)
        {
            List<GetAttendanceUploadStatusResult> resultSet = AttendanceManager.GetAttendanceUploadStatus(e.Start, int.Parse(cmbPageSize.SelectedItem.Value), "");
            e.Total = resultSet[0].TotalRows.Value;

            if (resultSet.Any())
            {
                storeDevices.DataSource = resultSet;
                storeDevices.DataBind();
            }
        }

    }
}
