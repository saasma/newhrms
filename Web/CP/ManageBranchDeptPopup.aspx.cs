﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Base;
using BLL.Manager;
using DAL;
using Utils.Helper;
using Utils;
using Utils.Web;
using System.Text;

namespace Web.CP
{
    public partial class ManageBranchDeptPopup : BasePage
    {
        BranchManager branchMgr = new BranchManager();
        List<Branch> source = new List<Branch>();
        List<Department> departmentList = new List<Department>();

        protected void Page_Load(object sender, EventArgs e)
        {
            departmentList = DepartmentManager.GetAllDepartments().OrderBy(x => x.Name).ToList();

            divMsgCtl.DataBind();
            divWarningMsg.DataBind();

            if (!IsPostBack)
            {
                Initialise();

            }
        }

        void Initialise()
        {
            LoadBranches();

            if (Request.QueryString["Id"] != null)
            {
                hdnBranchId.Value = Request.QueryString["Id"].ToString();
                LoadEditData(int.Parse(hdnBranchId.Value));
            }
        }

        void LoadBranches()
        {
            source = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId);

            foreach (Branch branch in source)
            {
                branch.DepartmentList = BranchManager.GetDepartmentListTextForBranch(branch.BranchId);
                branch.DepartmentList = branch.DepartmentList.Replace(",", ", ");
            }
        }

        private void LoadEditData(int branchId)
        {
            Branch branch = branchMgr.GetById(branchId);
            if (branch != null)
            {

                List<Department> source = departmentList;

                List<GetDepartmentListResult> dest = new List<GetDepartmentListResult>();

                foreach (BranchDepartment department in branch.BranchDepartments)
                {
                    Department dep = source.FirstOrDefault(x => x.DepartmentId == department.DepartmentID);
                    if (dep != null)
                    {
                        dest.Add(new GetDepartmentListResult { DepartmentId = dep.DepartmentId, Name = dep.Name });

                        source.Remove(dep);
                    }
                }

                branchTitle.InnerHtml = "Add Departments to branch \"" + branch.Name + "\"";

                lstSource.DataSource = source;
                lstSource.DataBind();
                lstDest.DataSource = dest;
                lstDest.DataBind();


                btnSave.Text = Resources.Messages.Update;
            }
        }

        void ClearBranchFields()
        {
            chkToAll.Checked = false;

            lstSource.DataSource = departmentList;
            lstSource.DataBind();

            lstDest.DataSource = new List<GetDepartmentListResult>();
            lstDest.DataBind();

            btnSave.Text = Resources.Messages.Save;
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(hdnBranchId.Value))
            {
                Branch b = new Branch();
                int branchId = int.Parse(hdnBranchId.Value);
                b.BranchId = branchId;
                b.CompanyId = SessionManager.CurrentCompanyId;

                foreach (ListItem item in lstDest.Items)
                {
                    b.BranchDepartments.Add(new BranchDepartment { DepartmentID = int.Parse(item.Value) });
                }

                Status status = BranchManager.SaveUpdateDepartmentsToBranch(b, chkToAll.Checked);

                if (status.IsSuccess)
                {
                    JavascriptHelper.DisplayClientMsg(Resources.Messages.BranchUpdatedMsg, Page, "closePopup();");
                    //divMsgCtl.InnerHtml = Resources.Messages.BranchUpdatedMsg;
                    //divMsgCtl.Hide = false;
                }
                else
                {
                    divWarningMsg.InnerHtml = status.ErrorMessage;
                    divWarningMsg.Hide = false;
                    return;
                }
                //JavascriptHelper.DisplayClientMsg("Branch information updated.", Page);
            }

            CommonManager.ResetCache();
            LoadBranches();
            ClearBranchFields();
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            for (int i = lstSource.Items.Count - 1; i >= 0; i--)
            {
                ListItem item = lstSource.Items[i];
                if (item.Selected)
                {
                    lstSource.Items.Remove(item);

                    lstDest.Items.Add(item);
                }
            }
        }

        protected void btnRemove_Click(object sender, EventArgs e)
        {
            for (int i = lstDest.Items.Count - 1; i >= 0; i--)
            {
                ListItem item = lstDest.Items[i];
                if (item.Selected)
                {
                    lstDest.Items.Remove(item);

                    lstSource.Items.Add(item);
                }
            }
        }

        protected void btnAddAll_Click(object sender, EventArgs e)
        {
            lstSource.DataSource = new List<GetDepartmentListResult>();
            lstSource.DataBind();

            lstDest.DataSource = departmentList;
            lstDest.DataBind();
        }

        protected void btnRemoveAll_Click(object sender, EventArgs e)
        {
            lstSource.DataSource = departmentList;
            lstSource.DataBind();

            lstDest.DataSource = new List<GetDepartmentListResult>();
            lstDest.DataBind();
        }


    }
}