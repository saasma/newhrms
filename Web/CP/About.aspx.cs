﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Reflection;


namespace Web.CP
{
    public partial class About : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Type t = typeof(About);
            AssemblyName an = new AssemblyName(Assembly.GetAssembly(t).FullName);
            string version = an.Version.ToString();

            lblVersion.InnerHtml = string.Format("\" {0} \"", version);
            
            
        }
    }
}
