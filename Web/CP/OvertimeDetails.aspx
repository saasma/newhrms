<%@ Page Title="Overtime Details" Language="C#" MasterPageFile="~/Master/HROld.Master"
    AutoEventWireup="true" CodeBehind="OvertimeDetails.aspx.cs" Inherits="Web.CP.OvertimeDetails" %>

<%@ Register Src="~/Controls/PagingCtl.ascx" TagName="PagingCtl" TagPrefix="uc1" %>
<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../_assets/themes/yui/tabs.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">

        function insertUpdateGratuityClassCall(classId) {
            var ret = insertUpdateGratuityClass('Id=' + classId);

            if (typeof (ret) != 'undefined') {
                if (ret == 'Reload') {
                    __doPostBack('Reload', '');
                }
            }
        }
        function insertUpdateGratuityRuleCall(gratuityRuleId) {
            var ret = insertUpdateGratuityRule('Id=' + gratuityRuleId);

            if (typeof (ret) != 'undefined') {
                if (ret == 'Reload') {
                    __doPostBack('Reload', '');
                }
            }

        }


        function refresh(popupWindow) {
            popupWindow.close();
            __doPostBack('Reload', '');
        }

        function changeStates(chk) {
            var isChecked = $(chkAll).is(':checked');

            $("INPUT[type='checkbox']").attr('checked', isChecked);

        }

        function updateBonusPopup(bonusId, empId) {
            var ret = updateBonus("bonusId=" + bonusId + "&empid=" + empId);


            return false;
        }
        function importPopupProcess() {


            var ret = importPopup("id=" + overtimeId);


            return false;
        }
        var skipLoadingCheck = true;
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4 runat="server" id="title">
                    Overtime Calculation :
                </h4>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="hdnBonusId" runat="server" />
    <div class="contentpanel">
        <div class="contentArea">
            <uc2:MsgCtl ID="divMsgCtl" EnableViewState="false" Hide="true" runat="server" />
            <uc2:WarningCtl ID="divWarningMsg" EnableViewState="false" Hide="true" runat="server" />
          
            <asp:Panel runat="server" ID="pnl" DefaultButton="btnLoad" class="attribute">
                <table class="fieldTable">
                    <tr>
                        <td>
                            <asp:Button OnClick="btnGenerate_Click" CssClass="save" runat="server" ID="b1" Width="130px"
                                Text="Generate Overtime" OnClientClick='return confirm("Are you sure,you want to generate the overtime, imported value will be lost?");' />
                        </td>
                        <td>
                            <asp:Button OnClick="btnProcessApproved_Click" CssClass="save" runat="server" ID="btnProcessApproved"
                                Width="130px" Text="Process Approved" OnClientClick='return confirm("Are you sure,you want to process approved overtime?");' />
                        </td>
                        <td>
                            <asp:Button OnClick="btnPostToAddOn_Click" CssClass="save" runat="server" ID="btnPostToAddOn"
                                Width="130px" Text="Post To Add-On" OnClientClick='return confirm("Are you sure,you want to post amount?");' />
                        </td>
                        <td style="padding-left: 10px; margin-top: 5px;">
                            <asp:Button ID="btnLoad" CssClass="update" OnClick="btnLoad_Click" runat="server"
                                CommandName="Load" Width="90px" Text="Load" />
                        </td>
                        <td style="padding-left: 20px; margin-top: 5px;">
                            <asp:Button ID="Button4" CssClass="update" OnClick="btnExport_Click" runat="server"
                                CommandName="Export" Width="90px" Text="Export" />
                        </td>
                        <td style="padding-left: 20px; margin-top: 5px;">
                            <asp:Button ID="Button1" CssClass="update" OnClientClick="return importPopupProcess();"
                                runat="server" CommandName="Export" Width="90px" Text="Import" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Employee
                            <br />
                            <asp:TextBox Width="180px" ID="txtEmpSearchText" runat="server"></asp:TextBox>
                            <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtEmpSearchText"
                                WatermarkText="Employee Name" WatermarkCssClass="searchBoxText" />
                            <cc1:AutoCompleteExtender EnableCaching="true" ID="AutoCompleteExtenderOrganization"
                                runat="server" MinimumPrefixLength="2" ServiceMethod="GetEmployeeNames" ServicePath="~/PayrollService.asmx"
                                TargetControlID="txtEmpSearchText" CompletionSetCount="10" CompletionInterval="250"
                                CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList"
                                CompletionListHighlightedItemCssClass="AutoExtenderHighlight">
                            </cc1:AutoCompleteExtender>
                        </td>
                        <td style="padding-left: 10px">
                            Branch
                            <br />
                            <asp:DropDownList ID="ddlBranch" AppendDataBoundItems="true" DataValueField="BranchId"
                                DataTextField="Name" runat="server" Width="150px">
                                <asp:ListItem Text="--Select Branch--" Value="-1" />
                            </asp:DropDownList>
                        </td>
                        <td style="padding-left: 10px">
                            Designation
                            <br />
                            <asp:DropDownList ID="ddlDesignation" DataValueField="ID" DataTextField="Text" runat="server"
                                Width="150px">
                            </asp:DropDownList>
                        </td>
                        <td>
                            <asp:HyperLink ID="details" runat="server" Text="View Details" NavigateUrl="OvertimeEachDayDetails.aspx?periodiId=" />
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <cc2:EmptyDisplayGridView CssClass="table table-primary mb30 table-bordered table-hover"
                Style="margin-bottom: -3px; clear: both" UseAccessibleHeader="true" ShowHeaderWhenEmpty="True"
                ID="gvwEmployees" runat="server" DataKeyNames="EIN" AutoGenerateColumns="False"
                AllowSorting="True" OnSorting="gvwEmployees_Sorting" ShowFooterWhenEmpty="False"
                Width="100%" GridLines="None">
                <Columns>
                    <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"
                        HeaderStyle-Width="50px" HeaderText="SN">
                        <ItemTemplate>
                            <%#     (Eval("SN"))%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"
                        HeaderStyle-Width="60px" HeaderText="EIN">
                        <ItemTemplate>
                            <%#     (Eval("EIN"))%>
                        </ItemTemplate>
                    </asp:TemplateField>
                     <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"
                        HeaderStyle-Width="60px" HeaderText="PAN No">
                        <ItemTemplate>
                            <%#     (Eval("PANNo"))%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"
                        HeaderStyle-Width="70px" HeaderText="Account No">
                        <ItemTemplate>
                            <%#     (Eval("AccountNo"))%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"
                        HeaderStyle-Width="120px" HeaderText="Employee">
                        <ItemTemplate>
                            <%#     (Eval("Name"))%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"
                        HeaderStyle-Width="100px" HeaderText="Level">
                        <ItemTemplate>
                            <%#     (Eval("Level"))%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"
                        HeaderStyle-Width="100px" HeaderText="Designation">
                        <ItemTemplate>
                            <%#     (Eval("Designation"))%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"
                        HeaderStyle-Width="100px" HeaderText="Branch">
                        <ItemTemplate>
                            <%#     (Eval("Branch"))%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right"
                        HeaderStyle-Width="70px" HeaderText="SalaryAmount">
                        <ItemTemplate>
                            <%#     GetCurrency(Eval("SalaryAmount"))%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right"
                        HeaderStyle-Width="70px" HeaderText="On Day Salary">
                        <ItemTemplate>
                            <%#     GetCurrency(Eval("OnDaySalary"))%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right"
                        HeaderStyle-Width="70px" HeaderText="Total OT Hrs">
                        <ItemTemplate>
                            <%#  GetHour(Eval("TotalOTMinute")) %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right"
                        HeaderStyle-Width="70px" HeaderText="OT Amount">
                        <ItemTemplate>
                            <%#     GetCurrency(Eval("OTAmount"))%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right"
                        HeaderStyle-Width="70px" HeaderText="SST">
                        <ItemTemplate>
                            <%#     GetCurrency(Eval("SST"))%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right"
                        HeaderStyle-Width="70px" HeaderText="TDS">
                        <ItemTemplate>
                            <%#     GetCurrency(Eval("TDS"))%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right"
                        HeaderStyle-Width="70px" HeaderText="Net Paid">
                        <ItemTemplate>
                            <%#     GetCurrency(Eval("NetPaid"))%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"
                        HeaderStyle-Width="100px" HeaderText="Remarks">
                        <ItemTemplate>
                            <%#     (Eval("Remarks"))%>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <RowStyle CssClass="odd" />
                <AlternatingRowStyle CssClass="even" />
                <EmptyDataTemplate>
                    <b>No employee records found. </b>
                </EmptyDataTemplate>
            </cc2:EmptyDisplayGridView>
            <uc1:PagingCtl ID="pagingCtl" OnNextRecord="btnNext_Click" OnPrevRecord="btnPrevious_Click"
                OnDropDownShowRecordsChanged="ddlRecords_SelectedIndexChanged" runat="server" />
        </div>
    </div>
</asp:Content>
