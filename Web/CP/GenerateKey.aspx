﻿<%@ Page Title="Backup/Restore" Language="C#" MasterPageFile="~/Master/Details.Master" AutoEventWireup="true"
    CodeBehind="GenerateKey.aspx.cs" Inherits="Web.CP.GenerateKey" %>

<%@ Register Assembly="DevExpress.Web.v16.1, Version=16.1.8.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningMsgCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="InfoMsgCtl" TagPrefix="uc2" %>
<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/PagingCtl.ascx" TagName="Paging" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <%--<script type="text/javascript" src="<%= ResolveUrl("~/Employee/override.js?v=") + Web.Helper.WebHelper.Version %>"></script>--%>



</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
    <%--<ext:ResourceManager ID="ResourceManager1" runat="server" ScriptMode="Release" />--%>
   <asp:HiddenField ID="hdnFileName" runat="server" />

    <div class="contentArea">
        <h3>
            API Key</h3>
       
        <asp:Label runat="server" ID="lblKey" />     
       
    </div>
</asp:Content>
