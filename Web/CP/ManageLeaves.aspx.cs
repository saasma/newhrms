﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Manager;
using BLL;
using Utils.Helper;
using DAL;
using BLL.Base;

namespace Web.CP
{
    public partial class ManageLeave : BasePage
    {
        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.LeaveAndAttendance;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Initialise();
            }
            else if (Request.Form["__EVENTTARGET"].Equals(this.updPanel.ClientID))
            {
                Initialise();
            }
            JavascriptHelper.AttachPopUpCode(Page, "popupLeave", "AELeave.aspx", 600, 650);
        }

        protected void Unnamed1_Click(object sender, EventArgs e)
        {

            foreach (GridViewRow row in this.gvwLeaves.Rows)
            {
                int incomeId = (int)(gvwLeaves.DataKeys[row.RowIndex][0]);
                TextBox txtOrder = row.FindControl("txtOrder") as TextBox;
                TextBox txtAbsentOrder = row.FindControl("txtAbsentOrder") as TextBox;
                TextBox txtMin = row.FindControl("txtMin") as TextBox;
                TextBox txtMax = row.FindControl("txtMax") as TextBox;

                int order = 0, absentOrder = 0;
                float minRequest = 0, maxRequest = 0;

                if (!string.IsNullOrEmpty(txtOrder.Text))
                    order = int.Parse(txtOrder.Text);

                if (!string.IsNullOrEmpty(txtAbsentOrder.Text))
                    absentOrder = int.Parse(txtAbsentOrder.Text);

                if (!string.IsNullOrEmpty(txtMin.Text))
                    minRequest = int.Parse(txtMin.Text);
                if (!string.IsNullOrEmpty(txtMax.Text))
                    maxRequest = int.Parse(txtMax.Text);

                LeaveAttendanceManager.UpdateLeaveOrder(incomeId, order,minRequest,maxRequest,absentOrder);


               

            }

            Initialise();

            divMsgCtl.InnerHtml = "Leave information updated.";
            divMsgCtl.Hide = false;
        }
        
        protected void Initialise()
        {
            if (CommonManager.CompanySetting.WhichCompany != WhichCompany.NIBL)
            {
                btnGroup.Visible = false;
                btnChild.Visible = false;
            }

            //load incomes
            List<LLeaveType> list = LeaveAttendanceManager.GetLeaveListByCompany(SessionManager.CurrentCompanyId)
                .OrderBy(x => x.Order).ToList();

            list = LeaveAttendanceManager.SetOrderUsingGroupAndChild(list);
            gvwLeaves.DataSource = list;


            PayManager payMgr = new PayManager();
            foreach (LLeaveType item in list)
            {

                LLeaveType leaveType = new LeaveAttendanceManager().GetLeaveType(item.LeaveTypeId);
              

                foreach (LLeaveEncashmentIncome income in leaveType.LLeaveEncashmentIncomes)
                {
                    if (item.EncashmentIncomes == null)
                        item.EncashmentIncomes = "";

                    if (item.EncashmentIncomes == "")
                        item.EncashmentIncomes = income.IncomeId==0 ? "PF" :  payMgr.GetIncomeById(income.IncomeId).Title;
                    else
                        item.EncashmentIncomes += ", " + (income.IncomeId==0 ? "PF" : payMgr.GetIncomeById(income.IncomeId).Title);
                }

            }

            gvwLeaves.DataBind();
        }
        protected void gvwEmployees_RowCreated(object sender, GridViewRowEventArgs e)
        {

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover",
                "this.originalClass=this.className;this.className='selected'");

                e.Row.Attributes.Add("onmouseout",
                "this.className=this.originalClass;");


            }

        }
        protected void gvwLeaves_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {

            int id = (int)gvwLeaves.DataKeys[e.RowIndex][0];
            LeaveAttendanceManager.DeleteLeave(id);
            Initialise();
        }
    }
}
