<%@ Page Title="CIT Change Request List" Language="C#" MasterPageFile="~/Master/HROld.Master"
    AutoEventWireup="true" CodeBehind="CITRequestList.aspx.cs" Inherits="Web.CP.CITRequestList" %>

<%@ Import Namespace="BLL.Manager" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="InfoMsgCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../../Scripts/jquery-1.3.2.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        var loadingButtonWhenSubmitting = false;
        var skipLoadingCheck = true;
        function addSkillSetToEmployeePopup(requestid) {
            var theText = requestid.getAttribute("value");
            positionHistoryPopup("isPopup=true&reqid=" + theText);
        }

        //ctl00_mainContent_gvw_ctl02_btnAction var theText = $(requestid).attributes.value.nodeValue();


        function selectDeselectAll(chk) {
            $('input[type=checkbox]').each(
                    function (index) {
                        if ($(this).prop('disabled') == false && this.id.indexOf('chkDelete') > 0)
                            this.checked = chk.checked;
                    }
                );
        }
       
    </script>
    <style type="text/css">
        .tableLightColor a:hover
        {
            color: #048FC2;
            text-decoration: none;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
 <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    CIT Change Request
                </h4>
            </div>
        </div>
    </div>
      <div class="contentpanel">
    <div class="contentArea">
      
        <uc2:InfoMsgCtl runat="server" Hide="true" EnableViewState="false" ID="msgInfo" />
        <uc2:WarningCtl ID="divWarningMsg" EnableViewState="false" Hide="true" runat="server" />
        <div class="attribute" style='padding:10px;'>
            <table>
                <tr>
                    <td>
                        Request Type
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlType" runat="server">
                            <asp:ListItem Value="-1" Selected="True">All</asp:ListItem>
                            <asp:ListItem Text="No CIT" Value="1" />
                            <%--<asp:ListItem Text="Optimum CIT" Value="2" />--%>
                            <asp:ListItem Text="Fixed Amount" Value="3" />
                           <%-- <asp:ListItem Text="Rate" Value="4" />--%>
                        </asp:DropDownList>
                    </td>
                   <td class="style1" style="padding-left: 10px">
                        <asp:TextBox Width="160px" ID="txtEmpSearchText" runat="server"></asp:TextBox>
                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtEmpSearchText"
                            WatermarkText="Employee Name" WatermarkCssClass="searchBoxText" />
                        <cc1:AutoCompleteExtender EnableCaching="true" ID="AutoCompleteExtenderOrganization"
                            runat="server" MinimumPrefixLength="2" ServiceMethod="GetEmployeeNames" ServicePath="~/PayrollService.asmx"
                            TargetControlID="txtEmpSearchText" CompletionSetCount="10" CompletionInterval="250"
                            CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList"
                            CompletionListHighlightedItemCssClass="AutoExtenderHighlight">
                        </cc1:AutoCompleteExtender>
                    </td>
                    <td>
                        <asp:CheckBox runat="server" ID="chkShowHistory" Text="Show History for Employee" />
                    </td>
                    <td valign="top" style='padding-left:20px;'>
                        <asp:Button ID="btnLoad" CssClass="load" OnClick="btnLoad_Click" runat="server" Text="Load" />
                    </td>
                    <%--<td>
                        <input onclick="addSkillSetToEmployeePopup()" id="btnAddEmployeeSkillSet" title="Add Skill Set"
                                    runat="server" type="button" class="browse" />
                    </td>--%>
                </tr>
            </table>
            <%--</ContentTemplate>
            </asp:UpdatePanel>--%>
        </div>
        <div class="clear">
            <cc2:EmptyDisplayGridView  UseAccessibleHeader="true" ShowHeaderWhenEmpty="True"
                ID="gvw" runat="server" DataKeyNames="RequestId" AutoGenerateColumns="False"
                CssClass="table table-primary mb30 table-bordered table-hover"  GridLines="None" ShowFooterWhenEmpty="False" OnRowCreated="gvwEmployees_RowCreated">
                <RowStyle BackColor="#E3EAEB" />
                <Columns>
                    <asp:TemplateField HeaderStyle-Width="30px" HeaderText="Select" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                        <HeaderTemplate>
                            <asp:CheckBox ID="chkDelete" onclick="selectDeselectAll(this)" runat="server" />
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:CheckBox ID="chkDelete" Visible='<%# IsVisible(Eval("Status")) %>' runat="server" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField HeaderStyle-Width="1px" DataField="RequestId" Visible="false"></asp:BoundField>
                    <asp:BoundField HeaderStyle-Width="20px" DataField="EmployeeId" HeaderText="EIN">
                    </asp:BoundField>
                    <asp:BoundField DataField="EmployeeName" HeaderStyle-Width="150px" HeaderStyle-HorizontalAlign="Left"
                        HeaderText="Employee"></asp:BoundField>
                    <asp:BoundField DataField="CreatedOn" DataFormatString="{0:yyyy-MMM-dd}" HeaderStyle-Width="120px"
                        HeaderStyle-HorizontalAlign="Left" HeaderText="Requested Date"></asp:BoundField>
                    <asp:BoundField DataField="Type" HeaderStyle-Width="120px" HeaderStyle-HorizontalAlign="Left"
                        HeaderText="Type"></asp:BoundField>
                    <asp:BoundField DataField="AmountOrRate" ItemStyle-HorizontalAlign="Right" HeaderStyle-Width="120px"
                        HeaderStyle-HorizontalAlign="Right" HeaderText="Amount/Rate" DataFormatString="{0:N2}">
                    </asp:BoundField>
                    <asp:TemplateField>
                    </asp:TemplateField>
                </Columns>
               <RowStyle CssClass="odd" />
            <AlternatingRowStyle CssClass="even" />
            <PagerStyle CssClass="defaultPagingBar" />
            <SelectedRowStyle CssClass="selected" />
                <EmptyDataTemplate>
                    <b>No list. </b>
                </EmptyDataTemplate>
            </cc2:EmptyDisplayGridView>
        </div>
        <div class="buttonsDiv" style='text-align:left'>
            <asp:Button ID="btnApprove" CssClass="delete" OnClick="btnApprove_Click" Visible="true"
                Width="120" CausesValidation="false" runat="server" OnClientClick="return confirm('Are you srue you want to Approve the selected requests?');"
                Text="Approve" />
            <asp:Button ID="btnReject" CssClass="update" OnClick="btnApprove_Click" Visible="true"
                Width="120" CausesValidation="false" runat="server" OnClientClick="return confirm('Are you srue you want to Reject the selected requests?');"
                Text="Reject" />
        <asp:Button ID="btnExport" OnClick="btnExport_Click" Visible="true"
                Width="120" CausesValidation="false" CssClass=" excel marginRight" runat="server" 
                Text="Export" />
        </div>
    </div>
    </div>
</asp:Content>
