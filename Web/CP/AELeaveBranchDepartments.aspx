<%@ Page Language="C#" MaintainScrollPositionOnPostback="true" MasterPageFile="~/Master/ForPopupPage.Master"
    AutoEventWireup="true" CodeBehind="AELeaveBranchDepartments.aspx.cs" Title="Grade"
    Inherits="Web.CP.AELeaveBranchDepartments" %>

<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Assembly="Karpach.WebControls" Namespace="Karpach.WebControls" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/UpDownCtl.ascx" TagName="UpDownCtl" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="InfoMsgCtl" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <ext:ResourceManager ID="ResourceManager1" runat="server" />
    <style type="text/css">
        .colorStyle td
        {
            padding: 2px !important;
        }
        .transparent
        {
            /* this class makes a window partially transparent */
            opacity: 0; /* Standard style for transparency */
            -moz-opacity: 0; /* Transparency for older Mozillas */
            filter: alpha(opacity=00); /* Transparency for IE */
            width: 0;
        }
        .paddinAll
        {
            padding: 10px;
        }
        div.notify
        {
            margin-left: 0;
            width: 41%;
        }
        .lf
        {
            width: 150px;
            padding: 0 0px 0px 0;
        }
        .lfs
        {
            width: 160px;
            padding: 0 10px 0px 0;
            vertical-align: top;
        }
        table tr td
        {
            padding-bottom: 10px;
        }
        .bevel
        {
            margin-bottom: 20px;
        }
        
        div.notify
        {
            width: 510px;
        }
        .displayInline
        {
            display: inline;
        }
        
        .body
        {
             background: #E9F0FB;
        }
        
    </style>
    <script type="text/javascript">
        function closePopup() {
            // alert(window.opener.parentReloadCallbackFunction)
            if ($.browser.msie == false && typeof (window.opener.reloadLeave) != 'undefined' && ($.browser.chrome || $.browser.safari)) {
                window.opener.reloadLeave(window);
            } else {
                window.returnValue = "Reload";
                window.close();
            }

        }


        
     
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server" >
   <div class="body">
        <div class="popupHeader">
            <h3>
                Branch Department
            </h3>
        </div>
        <div class="marginal" style='margin-top: 0px;'>
            <uc2:InfoMsgCtl ID="divWarningMsg" EnableViewState="false" Hide="true" runat="server" />
            <div class="clear" style="padding: 10px 0px 10px 0px">
            </div>
            <div class="bevel paddinAll" style="width: 540px!important; margin-bottom: 5px!important;">
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td valign="top">
                            <ext:GridPanel ID="gridBranch" runat="server" Border="false" AutoScroll="true" Width="200"
                                Height="400" Scroll="Both">
                                <Store>
                                    <ext:Store runat="server" AutoLoad="true" ID="storeGridBranch" RemoteSort="true">
                                        <Model>
                                            <ext:Model ID="Model5" runat="server" IDProperty="BranchId">
                                                <Fields>
                                                    <ext:ModelField Name="Name" Type="String" />
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <ColumnModel ID="GridPanel_ListItems_ColumnModel" runat="server">
                                    <Columns>
                                        <ext:Column ID="Column1" runat="server" Text="Branch" DataIndex="Name" Sortable="true"
                                            Width="150">
                                        </ext:Column>
                                    </Columns>
                                </ColumnModel>
                                <SelectionModel>
                                    <ext:CheckboxSelectionModel ID="RowSelectionModel" Mode="Simple" runat="server">
                                    </ext:CheckboxSelectionModel>
                                </SelectionModel>
                            </ext:GridPanel>
                        </td>
                        <td valign="top" style="padding-left: 50px">
                            <ext:GridPanel ID="gridDepartment" runat="server" Border="false" AutoScroll="true"
                                Width="200" Height="400" Scroll="Both">
                                <Store>
                                    <ext:Store runat="server" AutoLoad="true" ID="storeDepartment" RemoteSort="true">
                                        <Model>
                                            <ext:Model ID="Model1" runat="server" IDProperty="DepartmentId">
                                                <Fields>
                                                    <ext:ModelField Name="Name" Type="String" />
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <ColumnModel ID="ColumnModel1" runat="server">
                                    <Columns>
                                        <ext:Column ID="Column2" runat="server" Text="Department" DataIndex="Name" Sortable="true"
                                            Width="150">
                                        </ext:Column>
                                    </Columns>
                                </ColumnModel>
                                <SelectionModel>
                                    <ext:CheckboxSelectionModel ID="CheckboxSelectionModel1" Mode="Simple" runat="server">
                                    </ext:CheckboxSelectionModel>
                                </SelectionModel>
                            </ext:GridPanel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <ext:Button ID="btnSaveUpdate" runat="server" Text="Save"  Width="100">
                                <Listeners>
                                    <Click Handler="valGroup='saveupdateBranchDept';return CheckValidation();" />
                                </Listeners>
                                <DirectEvents>
                                    <Click OnEvent="btnSaveUpdate_Click">
                                        <EventMask ShowMask="true" />
                                        <ExtraParams>
                                            <ext:Parameter Name="DepartmentList" Value="Ext.encode(#{gridDepartment}.getRowsValues({selectedOnly : true}))"
                                                Mode="Raw" />
                                            <ext:Parameter Name="BranchList" Value="Ext.encode(#{gridBranch}.getRowsValues({selectedOnly : true}))"
                                                Mode="Raw" />
                                        </ExtraParams>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
  </div>
</asp:Content>
