﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using BLL.Manager;
using BLL;
using DAL;
using Utils.Calendar;
using Utils.Web;
using Utils.Helper;
using Utils;
using System.IO;
using Web.ReportDataSetTableAdapters;
using Web.CP.Report.Templates.Pay.Detail;
using BLL.BO;
using BLL.Entity;

namespace Web.CP
{
    public partial class AdvanceAdjustment : BasePage
    {
        CommonManager commonMgr = new CommonManager();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                Initialise();
            
        }


       



        public void Initialise()
        {

           

            //ddlPayrollPeriods.DataSource = CommonManager.GetCurrentYear(SessionManager.CurrentCompanyId);
            //ddlPayrollPeriods.DataBind();
            //UIHelper.SelectListItem(ddlPayrollPeriods);


            ddlLoans.DataSource = PayManager.GetAdvances();
            ddlLoans.DataBind();

            BindEmployees();

            if (!string.IsNullOrEmpty(Request.QueryString["ID"]))
            {
                int empId = int.Parse(Request.QueryString["ID"]);
                string name = EmployeeManager.GetEmployeeName(empId);

                int? siLoanId = EmployeeManager.GetAnyAdvanceIDForEmp(empId);
                if (siLoanId != null)
                {
                    UIHelper.SetSelectedInDropDown(ddlLoans, siLoanId);

                    ddlLoans_SelectedIndexChanged(null, null);

                    UIHelper.SetSelectedInDropDown(ddlEmployees, empId);

                    btnLoad_Click(null, null);
                }

            }
            
        }


        public void BindEmployees()
        {
            //if (ddlPayrollPeriods.SelectedValue.Equals("-1"))
            //{
             

            //    divButtons.Visible = false;

            //}
            //else
            //{
            //    divButtons.Visible = true;

              
            //}

            
        }

      
      
        protected void ddlPayrollPeriods_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindEmployees();
        }


   


        protected void btnSendMail_Click(object sender, EventArgs e)
        {

            bool isSaved = false;

            foreach (GridViewRow row in gvwSimpleLoanInstallments.Rows)
            {
                int payrollPeriodId = (int)gvwSimpleLoanInstallments.DataKeys[row.RowIndex]["PayrollPeriodId"];
                int IsCurrentValidPayrollPeriod = (int)gvwSimpleLoanInstallments.DataKeys[row.RowIndex]["IsCurrentValidPayrollPeriod"];
                int employeeId = (int)gvwSimpleLoanInstallments.DataKeys[row.RowIndex]["EmployeeId"];
                int deductionId = (int)gvwSimpleLoanInstallments.DataKeys[row.RowIndex]["DeductionId"];
                int remainingMonth = (int)gvwSimpleLoanInstallments.DataKeys[row.RowIndex]["RemainingInstallments"];

                // Calculate remaining month
                remainingMonth -= 1;
                

                if (IsCurrentValidPayrollPeriod == 1)
                {
                    TextBox txtAdjustment = row.FindControl("txtAdjustment") as TextBox;
                    //TextBox txtAdjustmentIPMT = row.FindControl("txtAdjustmentIPMT") as TextBox;
                    Label lblInstallment = row.FindControl("lblInstallment") as Label;
                   
                    Label lblOpening = row.FindControl("lblOpening") as Label;

                    CheckBox chkSkipThisMonth = row.FindControl("chkSkipThisMonth") as CheckBox;
                    //CheckBox chkDoNotAdjustLoan = row.FindControl("chkDoNotAdjustLoan") as CheckBox;

                    //decimal ipmt = 0;// PayManager.CalculateSimpleInterestRate(decimal.Parse(lblOpening.Text.Trim())
                        //, float.Parse(txtInterestRate.Text.Trim()));

                    DAL.AdvanceAdjustment loan = new DAL.AdvanceAdjustment();


                    loan.PayrollPeriodId = payrollPeriodId;
                    loan.EmployeeId = employeeId;
                    loan.DeductionId = deductionId;
                    loan.IsSkipThisMonth = chkSkipThisMonth.Checked;

                    if (txtAdjustment.Text.Trim() == "" || txtAdjustment.Text.Trim().Equals("0"))
                        loan.Adjustment = null;
                    else
                        loan.Adjustment = decimal.Parse(txtAdjustment.Text);


                    //if (txtAdjustment.Text.Trim() == "" || txtAdjustment.Text.Trim().Equals("0"))
                    //    loan.IPMTAdjustment = null;
                    //else
                    //    loan.IPMTAdjustment = decimal.Parse(txtAdjustmentIPMT.Text);


                    


                    ResponseStatus status = 
                        PayManager.SaveUpdate(loan,  decimal.Parse(lblOpening.Text), decimal.Parse(lblInstallment.Text),remainingMonth
                        ,ddlLoans.SelectedItem.Text);

                    if (status.IsSuccessType == false)
                    {
                        divWarningMsg.InnerHtml = status.ErrorMessage;
                        divWarningMsg.Hide = false;
                        return;
                    }

                    isSaved = true;
                    break;
                }
            }


            if (isSaved)
            {
                btnLoad_Click(null, null);

                divMsgCtl.InnerHtml = "Saved";
                divMsgCtl.Hide = false;
            }

        }

        protected void btnSendMailForAll_Click(object sender, EventArgs e)
        {
         
        }

        protected void gvwEmployees_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            //gvwEmployees.PageIndex = e.NewPageIndex;
            //BindEmployees();
        }

        protected void ddlLoans_SelectedIndexChanged(object sender, EventArgs e)
        {
            ListItem firstItem =  ddlEmployees.Items[0];
            ddlEmployees.Items.Clear();
           

            ddlEmployees.DataSource = PayManager.GetSimpleLoansEmployees(int.Parse(ddlLoans.SelectedValue));
            ddlEmployees.DataBind();

            ddlEmployees.Items.Insert(0, firstItem);

            btnLoad_Click(null, null);
        }

        public string GetAmount(object value,bool isLabel)
        {
            string currency = GetCurrency(value);
            string str;
            if (currency.ToString() == "0" || currency.ToString() == "0.00")
                str = "";
            else
                str = currency;

            if (isLabel && str == "")
                return "-";


            return str;
        }

        protected void btnLoad_Click(object sender, EventArgs e)
        {
            if (ddlEmployees.SelectedValue == "-1" || ddlLoans.SelectedValue == "-1")
            {
                pnlDetails.Visible = false;
            }
            else
            {
                pnlDetails.Visible = true;

                int employeeId = int.Parse(ddlEmployees.SelectedValue);
                int deductionId = int.Parse(ddlLoans.SelectedValue);
                // int payrollPeriodId = int.Parse(ddlPayrollPeriods.SelectedValue);

                bool hasCurrentPayrollPeriod = false;
                List<GetAdvanceAdjustmentInstallmentsResult> loans =  PayManager.GetAdvanceAdjustments(deductionId, employeeId,ref hasCurrentPayrollPeriod);
                gvwSimpleLoanInstallments.DataSource                  = loans;

                gvwSimpleLoanInstallments.DataBind();


             
                //Process to set row separate for paid ones
                foreach (GridViewRow row in gvwSimpleLoanInstallments.Rows)
                {
                    //GetLoanAdjustmentInstallmentsResult loanItem = row.DataItem as GetLoanAdjustmentInstallmentsResult;
                    bool IsPayrollPeriodSaved = (bool)gvwSimpleLoanInstallments.DataKeys[row.RowIndex]["IsPayrollPeriodSaved"];
                    
                    if (IsPayrollPeriodSaved == false)
                    {
                        int columnIndex = 0;
                        foreach (TableCell cell in row.Cells)
                        {
                            if( columnIndex !=5 )                                
                                cell.CssClass = "hightlightCell";

                            columnIndex += 1;
                        }
                        break;
                    }
                }
                

                //set information
                PEmployeeDeduction empDed = PayManager.GetEmployeeDeduction(employeeId, deductionId);
                if (empDed != null)
                {
                    lblLoanAmount.Text = GetCurrency(empDed.AdvanceLoanAmount);
                    CustomDate date1 = CustomDate.GetCustomDateFromString(empDed.TakenOn, IsEnglish);
                    lblTakenOn.Text = DateHelper.GetMonthName(date1.Month, IsEnglish) + "-" + date1.Year;
                    //txtInterestRate.Text = empDed.InterestRate.ToString();
                    //txtInterestRate.Enabled = hasCurrentPayrollPeriod;
                    btnSave.Visible = hasCurrentPayrollPeriod;
                    lblNoOfPayments.Text = empDed.ToBePaidOver.ToString();
                    CustomDate date = CustomDate.GetCustomDateFromString(empDed.StartingFrom, IsEnglish);
                    lblStartingFrom.Text = DateHelper.GetMonthName(date.Month, IsEnglish) + "-" + date.Year;

                    EEmployee emp = empDed.EEmployee;
                    lblEID.Text = emp.EmployeeId.ToString();
                    lblName.Text = emp.Name;
                    lblDesignation.Text = emp.EDesignation.Name;
                    lblBranch.Text = emp.Branch.Name;
                    lblDepartment.Text = emp.Department.Name;
                }

                if (EmployeeManager.IsRetiredOrResignedIncludedInCalculation(employeeId))
                {
                    divWarningMsg.InnerHtml = "Employee already retired.";
                    divWarningMsg.Hide = false;
                    btnSave.Visible = false;
                    return;
                }

                //if valid payroll period not exists then show message
                if (!hasCurrentPayrollPeriod)
                {
                    divMsgCtl.InnerHtml = Resources.ResourceTooltip.AdvanceAdjustmentNoValidPayrollPeriod;
                    divMsgCtl.Hide = false;
                }

                //if all installment paid then show paid msg

                if (loans != null && loans.Count > 0)
                {
                    bool isAllPaid = true;
                    foreach (GetAdvanceAdjustmentInstallmentsResult item in loans)
                    {
                        if (item.IsPayrollPeriodSaved != null && item.IsPayrollPeriodSaved.Value == false)
                        {
                            isAllPaid = false;
                            break;
                        }
                    }
                    if (isAllPaid)
                    {
                        divMsgCtl.InnerHtml = Resources.Messages.InstallmentComplete;
                        divMsgCtl.Hide = false;
                    }
                }
            }
        }

        protected void gvwSimpleLoanInstallments_RowCommand(object sender, GridViewCommandEventArgs e)
        {

        }

        protected void gvwSimpleLoanInstallments_RowDataBound(object sender, GridViewRowEventArgs e)
        {
           
        }

       

    }
}
