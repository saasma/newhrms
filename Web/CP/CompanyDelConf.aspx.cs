﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Utils.Helper;

namespace Web.CP
{
    public partial class CompanyDelConf : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            JavascriptHelper.AttachEnableDisablingJSCode(chk, btnDelete.ClientID, false);
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/CP/CompanyDel.aspx", true);
        }
    }
}
