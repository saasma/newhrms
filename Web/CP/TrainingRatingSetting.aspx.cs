﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using BLL.BO;
using System.IO;
using System.Text;

namespace Web.CP
{
    public partial class TrainingRatingSetting : BasePage
    {
        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.AdministratorTools;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();
            }
        }

        private void Initialise()
        {
           
            BindGrid();
        }

        private void ClearFields()
        {

            txtRatingName.Text = "";
            hdnAwardId.Text = "";
        }

        private void BindGrid()
        {
            gridAward.Store[0].DataSource = TrainingManager.GetTrainingRatingText();
            gridAward.Store[0].DataBind();
        }

        protected void btnAddNew_Click(object sender, DirectEventArgs e)
        {
            ClearFields();
            hdnAwardId.Text = "";
            WAward.Show();
        }

        protected void btnSave_Click(object sender, DirectEventArgs e)
        {
            Page.Validate("SaveUpdateAward");
            if (Page.IsValid)
            {
                TrainingRatingText obj = new TrainingRatingText();

                if (!string.IsNullOrEmpty(hdnAwardId.Text))
                    obj.RatingID = int.Parse(hdnAwardId.Text);

                obj.RatingName = txtRatingName.Text.Trim();

                Status status = AwardManager.SaveUpdateRatingText(obj);

                if (status.IsSuccess)
                {
                    WAward.Close();
                    BindGrid();
                    if(!string.IsNullOrEmpty(hdnAwardId.Text))
                        NewMessage.ShowNormalMessage("Record updated successfully.");
                    else
                        NewMessage.ShowNormalMessage("Record saved successfully.");
                }
                else
                    NewMessage.ShowWarningMessage(status.ErrorMessage);
            }
        }

        protected void btnEdit_Click(object sender, DirectEventArgs e)
        {
            //ClearFields();
            int ID = int.Parse(hdnAwardId.Text);

            TrainingRatingText obj = TrainingManager.GetTrainingRatingTextByID(ID);
            if (obj != null)
            {
                txtRatingName.Text = obj.RatingName;
                WAward.Show();
            }
        }

        protected void btnDelete_Click(object sender, DirectEventArgs e)
        {
            int RatingID = int.Parse(hdnAwardId.Text);

            Status status = TrainingManager.DeleteTrainingRatingText(RatingID);
            if (status.IsSuccess)
            {
                NewMessage.ShowNormalMessage("Record deleted successfully.");
                BindGrid();
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }

        }

    }
}